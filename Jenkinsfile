@Library('jenkins-shared-library') _

def getCommitSha() {
	return sh(returnStdout: true, script: 'git rev-parse HEAD')
}

def testPackages
pipeline {
	agent {
		label "tests"
	}
    options {
        // This is required if you want to clean before build
        skipDefaultCheckout(true)
    }

	stages {
		stage('Prepare workspace') {
			steps {
				script {
                    // Clean before build
                    cleanWs()
					git credentialsId: 'bitbucket-git', url: 'git@bitbucket.org:nuclos/nuclos.git', branch: "${BRANCH_NAME}"
					echo "\nGet latest changes from master into ${BRANCH_NAME} branch"
					sh 'git-merge-user-branch.sh ${BRANCH_NAME}'
					sh 'echo "maven.home.dir=/usr/share/maven/" > build.properties'
					sh 'echo "launch4j.home.dir=/opt/launch4j/" >> build.properties'
					sh 'echo "3rdparty.dir=/var/3rdparty/" >> build.properties'
					sh 'echo "maven.settings=\\${user.home}/.m2/settings.xml" >> build.properties'
					testPackages = sh(returnStdout: true, script: 'findGroovyFilesAsPackages.sh ${WORKSPACE}/nuclos-integration-tests/integration-tests-core/src/test/groovy')
					echo "Found testPackages: \n$testPackages"
					env.GIT_COMMIT = getCommitSha().trim()
                    echo "Build for commit: $GIT_COMMIT"
				}
			}
		}
		stage('Build & Unittests') {
			steps {
				withMaven(maven: 'LastVersion') {
					script {
						sh "mvn -Ptest,coverage clean verify -Dtest.server.skip=true -Dtest.server.2nd.round.skip=true -Dtest.webclient.skip=true -Dbasedir=${WORKSPACE}/nuclos-integration-tests/"
						junit testResults: '**/target/surefire-reports/*.xml', allowEmptyResults: true
					}
				}
			}
		}
		stage('Parallel webclient tests') {
			steps {
				script {
					splits = collectSplitTest(currentBuild, testPackages, this)
					echo "Created splits: $splits"
					def branches = [:]
					splits.eachWithIndex { split, num -> branches["split$num"] = {
                        stage("Test Section Webtests #${num + 1}") {
                            node('tests') {
                                // call test-job ?
                                script {
									def NUCLOS_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def AJP_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def RMI_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def HYDRA_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def HYDRA_A_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def HYDRA_C_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def HYDRA_D_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def CASTLE_MOCK_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									echo "Executing split: $num -> $split [P: ${NUCLOS_PORT} C: ${BRANCH_NAME}-${EXECUTOR_NUMBER}-${num}]"
                                    git credentialsId: 'bitbucket-git', url: 'git@bitbucket.org:nuclos/nuclos.git', branch: "${BRANCH_NAME}"
                                    echo "\nGet latest changes from master into ${BRANCH_NAME} branch"
                                    sh 'git-merge-user-branch.sh ${BRANCH_NAME}'
                                    sh 'echo "maven.home.dir=/usr/share/maven/" > build.properties'
                                    sh 'echo "launch4j.home.dir=/opt/launch4j/" >> build.properties'
                                    sh 'echo "3rdparty.dir=/var/3rdparty/" >> build.properties'
                                    sh 'echo "maven.settings=\\${user.home}/.m2/settings.xml" >> build.properties'
                                    sh "mvn -Dnuclos.server.port=${NUCLOS_PORT} -Dhydra.port=${HYDRA_PORT} -Dhydra.admin.port=${HYDRA_A_PORT} -Dhydra.consent.port=${HYDRA_C_PORT} -Dhydra.db.port=${HYDRA_D_PORT} -Dcastlemock.port=${CASTLE_MOCK_PORT} -Dcargo.tomcat.ajp.port=${AJP_PORT} -Dcargo.rmi.port=${RMI_PORT} --projects \\!'nuclos-integration-tests/with-glassfish' -Ptest,coverage clean verify -Dbrowser=chrome -Dlocale=de_DE -Dtest.compose.name=${BRANCH_NAME}-${EXECUTOR_NUMBER}-${num} -Dnuclos.server.protocol=http -Dnuclos.server.host=172.17.0.1 -Dtest.server.skip=true -Dtest.server.2nd.round.skip=true -Dtest=NomatchingTest -Dit.test=${split.isEmpty() ? '*' : split.join(",")} -DTEST=${split.isEmpty() ? '*' : split.join(",")} -DfailIfNoTests=false -Dbasedir=${WORKSPACE}/nuclos-integration-tests/"
                                    archiveArtifacts artifacts: 'nuclos-integration-tests/with-tomcat/target/screenshots/**/*, nuclos-integration-tests/with-tomcat/target/videos/**/*, *.log', allowEmptyArchive: true
                                    junit testResults: 'nuclos-integration-tests/with-tomcat/target/failsafe-reports/*.xml', allowEmptyResults: true
                                }
                            }
                        }
					} }
					branches["splitServer"] = {
	                      stage("Test Section Servertests") {
	                          node('tests') {
	                              // call test-job ?
	                              script {
	                                def NUCLOS_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\'| tr -d \'\n\'', returnStdout: true)
									def AJP_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def RMI_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def HYDRA_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def HYDRA_A_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def HYDRA_C_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def HYDRA_D_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
									def CASTLE_MOCK_PORT = sh(script: 'python3 -c \'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()\' | tr -d \'\n\'', returnStdout: true)
	                                echo "Executing split: Servertests [P: ${NUCLOS_PORT} C: ${BRANCH_NAME}-${EXECUTOR_NUMBER}-server]"
	                                  git credentialsId: 'bitbucket-git', url: 'git@bitbucket.org:nuclos/nuclos.git', branch: "${BRANCH_NAME}"
	                                  echo "\nGet latest changes from master into ${BRANCH_NAME} branch"
	                                  sh 'git-merge-user-branch.sh ${BRANCH_NAME}'
	                                  sh 'echo "maven.home.dir=/usr/share/maven/" > build.properties'
	                                  sh 'echo "launch4j.home.dir=/opt/launch4j/" >> build.properties'
	                                  sh 'echo "3rdparty.dir=/var/3rdparty/" >> build.properties'
	                                  sh 'echo "maven.settings=\\${user.home}/.m2/settings.xml" >> build.properties'
	                                  sh "mvn -Dnuclos.server.port=${NUCLOS_PORT} -Dhydra.port=${HYDRA_PORT} -Dhydra.admin.port=${HYDRA_A_PORT} -Dhydra.consent.port=${HYDRA_C_PORT} -Dhydra.db.port=${HYDRA_D_PORT} -Dcastlemock.port=${CASTLE_MOCK_PORT} -Dcargo.tomcat.ajp.port=${AJP_PORT} -Dcargo.rmi.port=${RMI_PORT} -Ptest,coverage clean verify -Dbrowser=chrome -Dlocale=de_DE -Dtest.compose.name=${BRANCH_NAME}-${EXECUTOR_NUMBER}-server -Dnuclos.server.protocol=http -Dnuclos.server.host=172.17.0.1 -Dtest.webclient.skip=true -Dbasedir=${WORKSPACE}/nuclos-integration-tests/"
	                                  archiveArtifacts artifacts: 'nuclos-integration-tests/with-tomcat/target/screenshots/**/*, nuclos-integration-tests/with-tomcat/target/videos/**/*, *.log', allowEmptyArchive: true
	                                  junit testResults: '**/target/failsafe-reports/*.xml', allowEmptyResults: true
	                              }
	                          }
	                      }
	                }
					parallel(branches.collectEntries{ [it.key, it.value ]})
				}
			}
		}
		stage('Coverage') {
			steps {
				withMaven(maven: 'LastVersion') {
					sh 'mvn jacoco:report@coverage-report'
				}
			}
		}
		stage('Sonar') {
			steps {
				withMaven(maven: 'LastVersion') {
					sh """
							export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
							git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
							git fetch --unshallow origin || true
							LATEST_BRANCH=\$(for branch in \$(git for-each-ref --format='%(refname:short)' refs/remotes/origin/); do echo "\$(git log --reverse --format="%h" -1 \$branch) \${branch#origin/}"; done | sort -r | awk '{print \$NF}' | grep -E '^[0-9]+\\.[0-9]{4}\\.[0-9]+\\.x\$' | sort -Vr | head -n 1)
							echo "Latest version branch: \$LATEST_BRANCH"
							mvn -B -ntp sonar:sonar -Dsonar.branch.name="${BRANCH_NAME}" -Dsonar.branch.target="\${LATEST_BRANCH}" -Dsonar.externalIssuesReportPaths="\$(chmod +x nuclos-installer/sonar/*.sh && nuclos-installer/sonar/dependency-check-to-sonar.sh)"
						"""
				}
			}
		}
	}

	post {
			always {
				script {
					def changeSetString = currentBuild.changeSets.collect { changeSet -> changeSet.getLogs().collect { log -> "[" + log.getAuthor() + "] " + log.getComment() }.join("\n") }.join("")
					emailext(to: "${USER_MAIL}", subject: "$JOB_NAME - Build # $BUILD_NUMBER - ${currentBuild.currentResult}!", body: """
Check console output at $BUILD_URL to view the results.

Changes since last success:
${changeSetString}
					""")
				}
			}
			success {
				script {
					if ("${SLACK_CHANNEL}" != "") {
						slackSend channel: "${SLACK_CHANNEL}", color: 'good', message: "$JOB_NAME - #$BUILD_NUMBER ${currentBuild.currentResult} after ${currentBuild.durationString}"
					}

					build job: "nuclos-branch-merge", wait: true, parameters: [
						string(name: "BRANCH_TO_MERGE", value: "$BRANCH_NAME"),
						string(name: "COMMIT_TO_MERGE", value: env.GIT_COMMIT)
					]
				}
			}
			unstable {
				script {
					if ("${SLACK_CHANNEL}" != "") {
						slackSend channel: "${SLACK_CHANNEL}", color: 'warning', message: "$JOB_NAME - #$BUILD_NUMBER ${currentBuild.currentResult} after ${currentBuild.durationString}"
					}
				}
			}
			failure {
				script {
					if ("${SLACK_CHANNEL}" != "") {
						slackSend channel: "${SLACK_CHANNEL}", color: 'danger', message: "$JOB_NAME - #$BUILD_NUMBER ${currentBuild.currentResult} after ${currentBuild.durationString}"
					}
				}
			}
	}
}
