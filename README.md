# Nuclos Open Source ERP system

Nuclos is a click-to-build system for non-technical users to build full-customized java 
enterprise applications. The server is a spring-based web application running on Apache Tomcat. 
There are two clients: A swing-based fat-client running with Java WebStart, and a webclient which can be
used on any modern browser.
Nuclos is released under the GNU Affero General Public License (Version 3.0), 
and hence is 100% open source. This is a git representation of the current nuclos development. 
For more information, visit www.nuclos.de. 

## Continuous Integration status

[![BrowserStack Status](https://www.browserstack.com/automate/badge.svg?badge_key=blVWOVZ3eVFBREw5aElIQXUwMUwrNjVRdHZ6YWlQdEFpem0rdUhEeE4wYz0tLXU4d1RXNmYyNWdlUGgweXd0c21zcFE9PQ==--e1ee329235f1c9a7b4bcc2c395837fcbaca39cd2)](https://www.browserstack.com/)


## Get more information
Sorry to say that most of our information and documentation is currently available in German only.

* Website http://www.nuclos.de/
* Wiki http://wiki.nuclos.de
* Issue tracking http://support.nuclos.de
* Sonatype Nexus Maven Repository Server http://maven.nuclos.de
