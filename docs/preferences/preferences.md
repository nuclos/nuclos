Preferences
===========
Ein Preference kann für andere Benutzer freigegeben werden. Bsp.:


Benutzer A erstellt ein Preference
----------------------------------

`POST → /preferences`


```
Preference A: {
	shared: false,
	customized: false,
	content: 'initialer Inhalt',
	user: <ID Benutzer A>
}
```


Dieses Preference wird einer Benutzergruppe freigegeben
-------------------------------------------------------

`POST → /preferences/{prefId}/share/{userRoleId}`

```
Preference A: {
	shared: true,
	customized: false,
	content: 'initialer Inhalt',
	user: null
}
```

Benutzer B hat Zugriff auf das freigegebene Preference (wenn dieser in
der zugehörigen Benutzergruppe ist)


Benutzer B verändert das freigegebene Preference
------------------------------------------------------------

`PUT → /preferences/{prefId}`

Preference A ändert sich nicht.

```
Preference A: {
	shared: true,
	customized: false,
	content: 'initialer Inhalt',
	user: null
}
```

Es wird ein neues Preference erstellt, auf das nur Benutzer B zugreift:

```
Preference B: {
	shared: true,
	customized: true,
	content: 'Inhalt geändert von Benutzer B'
	sharedPreference: <id von Preference A>,
	user: <ID Benutzer B>
}
```


Benutzer B teilt die Änderungen
-------------------------------

`PUT → /preferences/{prefId}/share`

Inhalt von Preference B wird in Preference A geschrieben, customized
Flag wird zurückgesetzt:

```
Preference A: {
	shared: true,
	customized: false,
	content: 'Inhalt geändert von Benutzer B',
	user: null
}
```

Preference B wird gelöscht und auch alle anderen Customizations anderer
User der gleichen Preference.

Alle Benutzer greifen nun wieder auf das gleiche Preference zu.


Benutzer A verändert erneut das freigegebene Preference
-------------------------------------------------------

`PUT → /preferences/{prefId}`

An Preference A ändert sich nur das customized Flag.

```
Preference A: {
	shared: true,
	customized: false,
	content: 'Inhalt geändert von Benutzer B',
	user: null
}
```

Es wird ein neues Preference erstellt:

```
Preference C: {
	shared: true,
	customized: true,
	content: 'Inhalt erneut geändert von Benutzer A'
	sharedPreference: <id von Preference A>
	user: <ID Benutzer A>
```


Benutzer A  verwirft seine Änderungen
-------------------------------------

`DELETE → /preferences/{prefId}`

An Preference A ändert sich nichts.

```
Preference A: {
	shared: true,
	customized: false,
	content: 'Inhalt geändert von Benutzer B'
	user: null
}
```

Preference B wird gelöscht.


Benutzer A entzieht die Freigabe
--------------------------------
`DELETE → /preferences/{prefId}/{userRoleId}`

An Preference A ändert sich das shared Flag, das Preference wird dem Benutzer A zugewiesen.

```
Preference A: {
	shared: false,
	customized: false,
	content: 'Inhalt geändert von Benutzer B',
	user: <ID Benutzer A>
}
