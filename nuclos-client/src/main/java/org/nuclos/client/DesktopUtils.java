//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.main.MainController;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.RigidUtils;
import org.nuclos.common2.StringUtils;

public class DesktopUtils {

	private static final Logger LOG = Logger.getLogger(DesktopUtils.class);
	
	public static void openURI(String sUri, JComponent parent) {
		if (org.nuclos.common2.StringUtils.isNullOrEmpty(sUri))
			return;
		String str = org.nuclos.common2.StringUtils.emptyIfNull(sUri).toLowerCase();
		boolean bHasProtocol = str.startsWith("http:") || str.startsWith("https:") || str.startsWith("file:") 
				|| str.startsWith("ftp:") || str.startsWith("mailto:");
	
		try {
			if (Desktop.isDesktopSupported()) {
				Desktop desktop = Desktop.getDesktop();
				URI uri;
				if (str.startsWith("file:")) {
					if (desktop.isSupported(Desktop.Action.OPEN)) {
						for (int i = 0; i <= 2; i++) {
						URL url = new URL(sUri);
						File file2 = FileUtils.toFile(url);
							if (file2.exists()) {
								open(file2);
								return;
							} 
							if (i == 2)	{
								String text = "Error opening:" + file2.getAbsolutePath();
								JOptionPane.showMessageDialog(MainController.getMainFrame(), text);
							} else {
								sUri = "file:" + "/" + sUri.substring(5);
							}
						}
					}
					return;
				} else {
					if(bHasProtocol)	
						uri = new URI(sUri);
					else {
						if (sUri.indexOf("@") != -1) //@todo use a pattern here.
							 uri = new URI("mailto:" + sUri);
						else
							uri = new URI("http://" + sUri);
					}
				}
				if (sUri.startsWith("mailto:")) {
					if (desktop.isSupported(Desktop.Action.MAIL)) {
						Desktop.getDesktop().mail(uri);
						return;
					}
				}
				
				if (desktop.isSupported(Desktop.Action.BROWSE)) {
					browse(uri);
				}
			}
			
		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(parent, "URIMouseAdapter.1", ex);
		}
	}

	public static void openURL(String sUrl, JComponent parent) {
		try {
			if (!StringUtils.looksEmpty(sUrl)) {
				if (sUrl.indexOf("://") == -1) {
					sUrl = "http://"+sUrl;
				}
				browse(URI.create(sUrl));
			}
		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(parent, "URIMouseAdapter.1", ex);
		}
	}
	
	public static void newMail(String to, JComponent parent) {
		try {
			if (!StringUtils.looksEmpty(to)) {
				if (to.indexOf("://") == -1) {
					to = "mailto:"+to;
				}
				Desktop.getDesktop().mail(URI.create(to));
			}
		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(parent, "URIMouseAdapter.1", ex);
		}
	}

	public static void openOrBrowseDecoded(URI uri, JComponent component) {
		// this if-clause, handles opening a local file with encoded umlauts in the uri (NUCLOS-10787)
		File file = new File(uri);
		if (file.isFile()) {
			DesktopUtils.openURI(uri.toString(), component);
		} else {
			DesktopUtils.browse(file.toURI());
		}
	}

	public static boolean browse(URI uri) {
		if (forceDefaultBrowserFromEnvironmentVariable(uri) ||
				browseDefaultJava(uri) ||
				runCommand("browse", "%s", uri.toString()) ||
				openExtendedSystemSupport(uri.toString())) {
			return true;
		} else {
			LOG.warn(String.format("Browse not supported: %s", uri));
			return false;
		}
	}

	public static boolean open(File file) {
		if (openDefaultJava(file) || openExtendedSystemSupport(file.getPath())) {
			return true;
		}
		LOG.warn(String.format("Open not supported: %s", file.getAbsolutePath()));
		return false;
	}

	private static boolean openExtendedSystemSupport(String toOpen) {
		if (SystemUtils.IS_OS_LINUX) {
			if (isLinuxXdg() && runCommand("xdg-open", "%s", toOpen)) {
				return true;
			}
			if (isLinuxKde() && runCommand("kde-open", "%s", toOpen)) {
				return true;
			}
			if (isLinuxGnome() && runCommand("gnome-open", "%s", toOpen)) {
				return true;
			}
		}
		if (SystemUtils.IS_OS_MAC && runCommand("open", "%s", toOpen)) {
			return true;
		}
		if (SystemUtils.IS_OS_WINDOWS && runCommand("explorer", "%s", toOpen)) {
			return true;
		}
		return false;
	}

	// for testing and local development very useful
	private static boolean forceDefaultBrowserFromEnvironmentVariable(URI uri) {
		try {
			String sOverrideDefaultBrowser = System.getenv("OVERRIDE_DEFAULT_BROWSER");
			if (!RigidUtils.looksEmpty(sOverrideDefaultBrowser)) {
				File fBrowser = new File(sOverrideDefaultBrowser);
				if (fBrowser.exists() && fBrowser.canRead() && fBrowser.canExecute()) {
					return runCommand(sOverrideDefaultBrowser, "%s", uri.toString(), 10, true);
				}
			}
		} catch (Throwable t) {
			LOG.error("Error during force browser from System property OVERRIDE_DEFAULT_BROWSER (" + uri + "): " + t.getMessage(), t);
		}
		return false;
	}

	private static boolean browseDefaultJava(URI uri) {
		try {
			if (!Desktop.isDesktopSupported() ||
					!Desktop.getDesktop().isSupported(java.awt.Desktop.Action.BROWSE)) {
				return false;
			}
			Desktop.getDesktop().browse(uri);
			return true;
		} catch (Throwable t) {
			LOG.error("Error during Java default Desktop browse (" + uri + "): " + t.getMessage(), t);
			return false;
		}
	}

	private static boolean openDefaultJava(File file) {
		try {
			if (!Desktop.isDesktopSupported() ||
					!Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
				return false;
			}
			Desktop.getDesktop().open(file);
			return true;
		} catch (Throwable t) {
			LOG.error("Error during Java default Desktop open (" + file + "): " + t.getMessage(), t);
			return false;
		}
	}

	public static boolean runCommand(String command, String args, String file) {
		return runCommand(command, args, file, 10);
	}

	public static boolean runCommand(String command, String args, String file, int timeoutInSeconds) {
		return runCommand(command, args, file, timeoutInSeconds, false);
	}

	public static boolean runCommand(String command, String args, String file, int timeoutInSeconds, boolean checkAliveOnlyAndDoNotDestroy) {
		LOG.debug("runCommand=" + command + ",args=" + args + ",%s=" + file);
		String[] parts = prepareCommand(command, args, file);
		Process p = null;
		try {
			p = Runtime.getRuntime().exec(parts);
			if (p == null) {
				return false;
			}
			// waitFor before readLines! (Otherwise the streams threaten to run endlessly!
			// Like "Open in Chrome Browser", when Chrome is not already running)
			if (p.waitFor(timeoutInSeconds, TimeUnit.SECONDS)) {
				IOUtils.readLines(p.getInputStream(), Charset.defaultCharset()).forEach(s -> LOG.info(String.format("%s: %s", command, s)));
				IOUtils.readLines(p.getErrorStream(), Charset.defaultCharset()).forEach(s -> LOG.error(String.format("%s: %s", command, s)));
			}
			return checkAliveOnlyAndDoNotDestroy ? p.isAlive() : p.exitValue() != 0;
		} catch (IOException | InterruptedException ex) {
			LOG.error("Error during run command (" + command + "): " + ex.getMessage(), ex);
			return false;
		} finally {
			if (p != null && !checkAliveOnlyAndDoNotDestroy) {
				p.destroy();
			}
		}
	}

	private static String[] prepareCommand(String command, String args, String file) {
		List<String> parts = new ArrayList<>();
		parts.add(command);
		if (args != null) {
			for (String s : args.split(" ")) {
				s = String.format(s, file);
				parts.add(s.trim());
			}
		}
		return parts.toArray(new String[parts.size()]);
	}

	public static boolean isLinuxXdg() {
		String xdgSessionId = System.getenv("XDG_SESSION_ID");
		return xdgSessionId != null && !xdgSessionId.isEmpty();
	}

	public static boolean isLinuxGnome() {
		String gdmSession = System.getenv("GDMSESSION");
		return gdmSession != null && gdmSession.toLowerCase().contains("gnome");
	}

	public static boolean isLinuxKde() {
		String gdmSession = System.getenv("GDMSESSION");
		return gdmSession != null && gdmSession.toLowerCase().contains("kde");
	}
}
