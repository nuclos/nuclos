package org.nuclos.client.command;

public interface ExecutionResultListener {
	
	void done();
	
	void error(Exception ex);
	
}

