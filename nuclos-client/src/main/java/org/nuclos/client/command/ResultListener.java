//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.command;

import org.nuclos.client.ui.layer.LockingResultListener;

/**
 * Listener for Results
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO: Evaluate if this is needed. Perhaps all ResultListeners are (also)
 * {@link LockingResultListener}s. (tp)
 * </p><p>
 * TODO: Evaluate if there is some error handling method missing (in the lines
 * of {@link ExecutionResultListener}.
 * If so introduce it. (tp)
 * </p>
 * @author	<a href="mailto:maik.stueker@nuclos.de">Maik Stueker</a>
 * @version 01.00.00
 */
public interface ResultListener <R> {

	void done(R result); 
	
}
