//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.nuclos.client.ClientLibLoader;
import org.nuclos.common2.StringUtils;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public abstract class DragAndDropUtils {

	private static final Logger LOG = Logger.getLogger(DragAndDropUtils.class);
	
	/**
	 * save in tempory file
	 * @return List of java.io.File 
	 * @throws IOException 
	 */
	public static abstract class ProcessFileList implements Callable<Boolean> {
		private List<File> files;

		public ProcessFileList() {
		}

		public List<File> getFiles() {
			return files;
		}

		public ProcessFileList setFiles(List<File> files) {
			this.files = files;
			return this;
		}
	}

	public static Boolean mailHandlingWithJacob(ProcessFileList callback) throws Exception {
		try {
			if (!ClientLibLoader.loadJacob()) {
				return callback.setFiles(Collections.emptyList()).call();
			}
		} catch (Exception e) {
			return callback.setFiles(Collections.emptyList()).call();
		}
		
		List<File> lstFiles = new ArrayList<File>();
		LOG.debug("--- Trying to save outlook message");
		
		// String tempDir = IOUtils.getDefaultTempDir().getAbsolutePath() + "\\";

		ActiveXComponent xl = new ActiveXComponent("Outlook.Application");
		LOG.debug("(1) Outlook ist selected");

		Dispatch explorer = Dispatch.get(xl,"ActiveExplorer").toDispatch();
		LOG.debug("(2) Active Explorer identified");
		Dispatch selection = Dispatch.get(explorer, "Selection").toDispatch();
		LOG.debug("(3) Selected Mail(s) identified");
		Variant count = Dispatch.get(selection, "Count");
		LOG.debug("(4) number of selected mails: "+count.getInt());

		String tmpDir = System.getProperty("java.io.tmpdir");

		for (int mailIndex = 1; mailIndex <= count.getInt(); mailIndex++ ) {
			final Dispatch mailItem = Dispatch.call(selection, "Item", new Variant(mailIndex)).toDispatch();
			final Variant subject = Dispatch.get(mailItem, "Subject");
			final String strFileName = StringUtils.trimInvalidCharactersInFilename(subject.toString(), true);
			final File file = Files.createFile(Paths.get(tmpDir, strFileName + ".msg")).toFile();
			final String wholeFileName = file.getPath();
			
			Dispatch.call(mailItem, "SaveAs", wholeFileName);
			
			file.deleteOnExit();
			lstFiles.add(file);
		}

		Boolean result = callback.setFiles(lstFiles).call();
		if (!lstFiles.isEmpty()) {
			for (final File file : lstFiles) {
				file.delete();
			}
		}
		return result;
	}
	
}
