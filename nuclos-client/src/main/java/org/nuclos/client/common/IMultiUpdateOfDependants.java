package org.nuclos.client.common;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.table.TableCellRendererProvider;
import org.nuclos.common.collection.BinaryPredicate;
import org.nuclos.common.collection.CollectionUtils;

public interface IMultiUpdateOfDependants<PK> extends TableCellRendererProvider {

	void transfer(SubForm subForm, DetailsSubFormController<PK,?> dsfCtl);
	
	boolean isTransferPossible(SubForm subForm);
	
	void close(SubFormController sfCtl);

	// builds the equivalence classes of the given set
	static <E1, E extends E1> List<Set<E>> getEquivalenceClasses(Collection<? extends E> collection,
																 BinaryPredicate<E1, E1> predicateEquals) {
		final List<Set<E>> result = new LinkedList<>();

		for (E e : collection) {
			boolean equivalenceClassFound = false;
			for (Set<E> set : result) {
				if (CollectionUtils.contains(set, e, predicateEquals)) {
					set.add(e);
					equivalenceClassFound = true;
					break;
				}
			}
			if (!equivalenceClassFound) {
				Set<E> newEquivalenceClass = new HashSet<>();
				newEquivalenceClass.add(e);
				result.add(newEquivalenceClass);
			}
		}
		return result;
	}

}
