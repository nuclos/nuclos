package org.nuclos.client.common;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.swing.event.TableModelListener;

import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.common.collect.collectable.Collectable;

public interface ISubFormCollectableMap<PK> extends TableModelListener {

	SubFormController getSubformController();
	
	int getParentCollectablesSize();
	
	Map<PK, CollectableEntityObject<PK>> get(Collectable<PK> clct);
	
	Collection<Map<PK, CollectableEntityObject<PK>>> values();
	
	Set<CollectableEntityObject<PK>> keySet();
	
	void close();
	
	boolean allEntitiesHaveDataInRow(int row);
	
	Map<PK, CollectableEntityObject<PK>> transferDataToAllEntities(CollectableEntityObject<PK> prototype);
	
	Collectable<PK> getPrototype(int row);
	
}
