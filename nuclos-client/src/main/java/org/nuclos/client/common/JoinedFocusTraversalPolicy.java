package org.nuclos.client.common;

import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;

/**
 * Behaves like a bridge between 2 containers, which would only cycle within their components.
 * When the last component of containerA is reached, it will jump to the first component of containerB and vice versa.
 * Original use-case:
 * Handles the Focus Management, when a {@link org.nuclos.client.ui.collect.CollectController CollectController} is displayed within an {@link org.nuclos.client.ui.OverlayOptionPane OverlayOptionPane}.
 * It bridges from the {@link NuclosFocusTraversalPolicy} to the {@code OverlayOptionPaneFocusTraversalPolicy} defined in {@link org.nuclos.client.ui.OverlayOptionPane OverlayOptionPane}.
 */
public class JoinedFocusTraversalPolicy extends FocusTraversalPolicy {

	private final Container containerA;
	private final Container containerB;
	private final FocusTraversalPolicy policyA;
	private final FocusTraversalPolicy policyB;

	public JoinedFocusTraversalPolicy(Container containerA, Container containerB) {
		this.containerA = containerA;
		this.containerB = containerB;
		this.policyA = containerA.getFocusTraversalPolicy();
		this.policyB = containerB.getFocusTraversalPolicy();
	}

	@Override
	public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
		if (policyB.getLastComponent(containerB) == aComponent) {
			return policyA.getFirstComponent(containerA);
		}
		if (policyA.getComponentAfter(focusCycleRoot, aComponent) != null) {
			return policyA.getComponentAfter(focusCycleRoot, aComponent);
		} else {
			return policyB.getComponentAfter(focusCycleRoot, aComponent);
		}
	}

	@Override
	public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
		if (aComponent == policyA.getFirstComponent(containerA)) {
			return policyB.getLastComponent(containerB);
		}
		if (policyA.getComponentBefore(focusCycleRoot, aComponent) != null) {
			return policyA.getComponentBefore(focusCycleRoot, aComponent);
		} else {
			return policyB.getComponentBefore(focusCycleRoot, aComponent);
		}
	}

	@Override
	public Component getFirstComponent(Container focusCycleRoot) {
		Component firstComponent = policyA.getFirstComponent(focusCycleRoot);
		return firstComponent != null ? firstComponent : policyB.getFirstComponent(focusCycleRoot);
	}

	@Override
	public Component getLastComponent(Container focusCycleRoot) {
		Component lastComponent = policyA.getLastComponent(focusCycleRoot);
		return lastComponent != null ? lastComponent : policyB.getLastComponent(focusCycleRoot);
	}

	@Override
	public Component getDefaultComponent(Container focusCycleRoot) {
		Component defaultComponent = policyA.getDefaultComponent(focusCycleRoot);
		return defaultComponent != null ? defaultComponent : policyB.getDefaultComponent(focusCycleRoot);
	}

}
