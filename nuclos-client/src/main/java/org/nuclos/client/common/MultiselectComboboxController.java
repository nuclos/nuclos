package org.nuclos.client.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.entityobject.CollectableEntityTemplate;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.CollectableWithDependants;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.collect.component.multiselection.LoadMultiselectEntriesClientWorker;
import org.nuclos.client.ui.collect.component.multiselection.LoadMultiselectSuggestionsClientWorker;
import org.nuclos.client.ui.collect.component.multiselection.MultiSelectComboboxModelListener;
import org.nuclos.client.ui.collect.component.multiselection.MultiselectionCombobox;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;

public class MultiselectComboboxController implements MultiSelectComboboxModelListener {

	private MetaProvider metaProvider = SpringApplicationContextHolder.getBean(MetaProvider.class);

	protected MainFrameTab mainFrameTab;
	protected UID multiselectName;
	protected UID suggestionEntity;
	protected final EntityCollectController<Long,?> clct;
	private LoadMultiselectEntriesClientWorker loadEntriesWorker = new LoadMultiselectEntriesClientWorker(this);
	private LoadMultiselectSuggestionsClientWorker loadSuggestionsWorker = new LoadMultiselectSuggestionsClientWorker(this);
	private MultiselectionCombobox multiselectionCombobox;

	private final CollectableFieldsProviderFactory clctfproviderfactory;

	/**
	 * the id of the (current) parent object.
	 */
	private Object oParentId;
	protected Collectable parent;
	private List<CollectableEntityObject<?>> collectableEntries = new ArrayList<>();

	public MultiselectComboboxController(MainFrameTab mainFrameTab, UID name, MultiselectionCombobox multiselectionCombobox, EntityCollectController<Long,?> clct, CollectableFieldsProviderCache valueListProviderCache) {
		this.mainFrameTab = mainFrameTab;
		this.multiselectName = name;
		this.clct = clct;
		this.multiselectionCombobox = multiselectionCombobox;
		this.clctfproviderfactory = MasterDataCollectableFieldsProviderFactory.newFactory(clct.getCollectableEntity().getUID(), valueListProviderCache);

		EntityMeta entityMeta = metaProvider.getEntity(multiselectionCombobox.getUid());
		FieldMeta referenceFieldMeta = entityMeta.getField(multiselectionCombobox.getDisplayAttributeUid());
		suggestionEntity = referenceFieldMeta.getForeignEntity();
		multiselectionCombobox.setController(this);
		loadData();
	}

	public void setParameterValueListProvider(UID uidField, CollectableField cf) throws CommonBusinessException {
		this.multiselectionCombobox.setParameterValueListProvider(uidField, cf);
	}

	public CollectableFieldsProviderFactory getCollectableFieldsProviderFactory() { return clctfproviderfactory; }

	public EntityCollectController getEntityCollectController() {
		return clct;
	}

	protected UID getSuggestionEntity() {
		return this.suggestionEntity;
	}

	public final UID getCurrentLayoutUid() {
		return this.clct != null ? this.clct.getCurrentLayoutUid() : null;
	}

	public MultiselectionCombobox getMultiselectionCombobox() {
		return multiselectionCombobox;
	}

	public CollectableEntity getCollectableEntity() {
		return DefaultCollectableEntityProvider.getInstance().getCollectableEntity(multiselectionCombobox.getUid());
	}

	public CollectableEntityObject newCollectable() {
		final CollectableEntity clctmde = DefaultCollectableEntityProvider
				.getInstance()
				.getCollectableEntity(multiselectionCombobox.getUid());
		return new CollectableEntityTemplate(clctmde, EntityObjectVO.newObject(clctmde.getUID()));
	}

	public UID getForeignKeyFieldUID() {
		return multiselectionCombobox.getForeignKeyFieldToParent();
	}

	public Object getParentId() {
		return oParentId;
	}

	public void setParentId(Object oParentId) {
		this.oParentId = oParentId;
	}

	/**
	 * sets the parent id of the given <code>Collectable</code>. The
	 * corresponding field in <code>clct</code> is not changed if the parent id
	 * is already equal to the given parent id.
	 *
	 * @param clct
	 * @param oParentId
	 */
	protected void setParentId(Collectable clct, Object oParentId) {
		final UID foreignKeyFieldUid = getForeignKeyFieldUID();
		if (!LangUtils.equal(clct.getField(foreignKeyFieldUid).getValueId(), oParentId)) {
			clct.setField(foreignKeyFieldUid, new CollectableValueIdField(oParentId, null));
		}
	}

	public void setParentObject(Collectable parent) {
		this.parent = parent;
		loadData();
	}

	public Collectable getParentObject() {
		return this.parent;
	}

	public void clearCollectables() {
		collectableEntries.clear();
	}

	public void setCollectables(final Collection<CollectableEntityObject<Object>> dependants) {
		collectableEntries.addAll(dependants);
	}

	/**
	 * Returns all collectables.
	 *
	 * @return All collectables, even the removed ones.
	 */
	public List<CollectableEntityObject<?>> getAllCollectables() {
		return collectableEntries.stream()
				.filter(f -> !f.getEntityObjectVO().isFlagUnchanged())
				.collect(Collectors.toList());
	}

	public CollectableEntityObject insertNewRow(CollectableField suggestionVO) {
		final CollectableEntityObject clctNew = this.newCollectable();
		this.setParentId(clctNew, this.getParentId());
		Utils.setDefaultValues(clctNew, this.getCollectableEntity());
		clctNew.setField(multiselectionCombobox.getForeignKeyFieldToParent(), new CollectableValueIdField(this.getParentId(), parent != null ? parent.getValue(multiselectionCombobox.getReferencedForeignKeyFieldToParent()) : null));
		clctNew.setField(multiselectionCombobox.getDisplayAttributeUid(), new CollectableValueIdField(suggestionVO.getValueId(), null));
		if (parent instanceof CollectableWithDependants) {
			IDependentKey key = DependentDataMap.createDependentKey(this.getForeignKeyFieldUID());
			if (parent instanceof CollectableMasterDataWithDependants) {
				((CollectableMasterDataWithDependants<?>) parent).getDependantCollectableMasterDataMap().addValue(key, clctNew);

			} else if (parent instanceof CollectableGenericObjectWithDependants){
				((CollectableGenericObjectWithDependants) parent).getGenericObjectWithDependantsCVO().getDependents().addData(key, clctNew.getEntityObjectVO());
			}
		}
		return clctNew;
	}

	public void removeRow(CollectableEntityObject entityObject) {
		entityObject.markRemoved();
	}

	public void loadMultiselectData(boolean softreload) {
		loadSuggestions();
		loadEntries();
	}

	public void loadSuggestions() {
		if (loadSuggestionsWorker != null) {
			loadSuggestionsWorker.interrupt();
		}
		loadSuggestionsWorker = new LoadMultiselectSuggestionsClientWorker(this);
		CommonMultiThreader.getInstance().execute(loadSuggestionsWorker);
	}

	public void loadSuggestionsImmediately() throws CommonBusinessException {
		if (loadSuggestionsWorker != null) {
			loadSuggestionsWorker.interrupt();
		}
		loadSuggestionsWorker = new LoadMultiselectSuggestionsClientWorker(this);
		loadSuggestionsWorker.work();
	}

	public void loadEntries() {
		if (loadEntriesWorker != null) {
			loadEntriesWorker.interrupt();
		}
		loadEntriesWorker = new LoadMultiselectEntriesClientWorker(this);
		CommonMultiThreader.getInstance().execute(loadEntriesWorker);
	}

	public void loadData() {
		loadSuggestions();
		loadEntries();
	}

	@Override
	public void onItemSelected(final CollectableField item) {
		Optional<CollectableEntityObject<?>> eovo = collectableEntries.stream()
				.filter(i -> Objects.equals(item.getValueId(), i.getEntityObjectVO().getFieldId(multiselectionCombobox.getDisplayAttributeUid())))
				.findAny();
		if (eovo.isPresent()) {
			eovo.get().getEntityObjectVO().reset();
		} else {
			collectableEntries.add(insertNewRow(item));
		}
	}

	@Override
	public void onItemDeselected(final CollectableField item) {
		Optional<CollectableEntityObject<?>> eovo = collectableEntries.stream()
				.filter(i -> Objects.equals(item.getValueId(), i.getEntityObjectVO().getFieldId(multiselectionCombobox.getDisplayAttributeUid())))
				.findAny();
		if (eovo.isPresent()) {
			eovo.get().markRemoved();
		}
	}

}
