package org.nuclos.client.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.DependentSelection;

public class SubformHierarchie {

	public HashMap<DetailsSubFormController, SubformHierarchie> children = new HashMap<>();

	public SubformHierarchie() {

	}

	public DependentSelection toDependentSelection() {
		DependentSelection dependentSelection = new DependentSelection();
		for (Map.Entry<DetailsSubFormController, SubformHierarchie> entry : children.entrySet()) {
			DetailsSubFormController subFormController = entry.getKey();
			String foreignKeyFQN = MetaProvider.getInstance().getEntityFqn(subFormController.getCollectableEntity().getUID()) + "_"
					+ MetaProvider.getInstance().getEntityField(subFormController.getForeignKeyFieldUID()).getFieldName();
			DependentSelection subSelection = entry.getValue().toDependentSelection();
			subSelection.selectedIds.addAll(subFormController.getSelectedIds());
			dependentSelection.subDependentSelection.put(foreignKeyFQN, subSelection);
		}

		return dependentSelection;
	}

	public static SubformHierarchie buildHierarchie(CollectController<?, ?> controller) {
		ArrayList<? extends DetailsSubFormController<?, ?>> subformControllerList = new ArrayList<>(controller.getDetailsController().getSubFormControllers());
		SubformHierarchie hierarchie = new SubformHierarchie();
		while (subformControllerList.size() > 0) {
			for (int i = subformControllerList.size() - 1; i >= 0; i--) {
				if (subformControllerList.get(i).getSubForm().getParentSubForm() == null) {
					hierarchie.children.put(subformControllerList.get(i), new SubformHierarchie());
					subformControllerList.remove(i);
				} else {
					boolean added = addSubformToHierarchie(hierarchie, subformControllerList.get(i));
					if (added) {
						subformControllerList.remove(i);
					}
				}
			}
		}
		return hierarchie;
	}

	private static boolean addSubformToHierarchie(SubformHierarchie subformHierarchie, DetailsSubFormController subFormController) {
		boolean added = false;
		for (Map.Entry<DetailsSubFormController, SubformHierarchie> entry : subformHierarchie.children.entrySet()) {
			if (subFormController.getSubForm().getParentSubForm().equals(entry.getKey().getSubForm().getEntityUID())) {
				entry.getValue().children.put(subFormController, new SubformHierarchie());
				return true;
			}
			added = added || addSubformToHierarchie(entry.getValue(), subFormController);
		}
		return added;
	}

}
