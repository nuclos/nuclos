//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common.controller;

import org.nuclos.client.searchfilter.EntitySearchFilter;

/**
 * Common state for all the collect controller that cooperate together with
 * {@link org.nuclos.client.common.NuclosCollectController}.
 * <p>
 * Controller includes ResultController, DetailsController, ProfilesController,
 * ...
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.15.3
 */
public class NuclosCollectControllerCommonState {

	/**
	 * The current search filter in the controller. Because of NUCLOS-2182,
	 * this is no longer only of interest for the search, but also for the
	 * result list, as the search filter might be associated with a result
	 * list profile.
	 * <p>
	 * A value of <code>null</code> could mean different things: no search filter 
	 * selected or a search filter reset because the user selected a different
	 * profile in the result list.
	 * </p>
	 */
	private EntitySearchFilter currentSearchFilter;
	
	public NuclosCollectControllerCommonState() {
	}

	public EntitySearchFilter getCurrentSearchFilter() {
		return currentSearchFilter;
	}

	public void setCurrentSearchFilter(EntitySearchFilter currentSearchFilter) {
		this.currentSearchFilter = currentSearchFilter;
	}

}
