package org.nuclos.client.common.controller;

import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;

/**
 * Created by Oliver Brausch on 08.03.18.
 */
public interface SearchConditionSource {

	CollectableSearchCondition getCollectableSearchConditionFromSearchPanel(boolean bMakeConsistent) throws CollectableFieldFormatException;

}
