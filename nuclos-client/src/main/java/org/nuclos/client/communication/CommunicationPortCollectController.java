//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.communication;

import java.io.Serializable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;

public class CommunicationPortCollectController extends MasterDataCollectController<UID> {

	private static final Logger LOG = Logger.getLogger(CommunicationPortCollectController.class);

	public CommunicationPortCollectController(MainFrameTab tabIfAny) {
		super(E.COMMUNICATION_PORT, tabIfAny, null);
	}
	
	/**
	 * invalidates the generator actions cache after successful insert.
	 * @param clctNew
	 * @return
	 * @throws NuclosBusinessException
	 */
	@Override
	protected CollectableMasterDataWithDependants<UID> insertCollectable(CollectableMasterDataWithDependants<UID> clctNew) throws CommonBusinessException {
		final CollectableMasterDataWithDependants<UID> result = super.insertCollectable(clctNew);
		
		EventSupportDelegate.getInstance().invalidateCaches(E.COMMUNICATION_PORT);
		MasterDataCache.getInstance().invalidate(E.COMMUNICATION_PORT.getUID());
		
		return result;
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct, Object oDependantData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		final CollectableMasterDataWithDependants<UID> result = super.updateCollectable(clct, oDependantData, applyMultiEditContext);
		
		EventSupportRepository.getInstance().updateEventSupports();
		EventSupportDelegate.getInstance().invalidateCaches(E.COMMUNICATION_PORT);
		MasterDataCache.getInstance().invalidate(E.COMMUNICATION_PORT.getUID());

		return result;
	}

	/**
	 * invalidates the generator actions cache after successful delete.
	 * @throws org.nuclos.common.NuclosBusinessException
	 */
	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		super.deleteCollectable(clct, applyMultiEditContext);
		MasterDataCache.getInstance().invalidate(E.COMMUNICATION_PORT.getUID());
		EventSupportDelegate.getInstance().invalidateCaches(E.COMMUNICATION_PORT);
	}

}	// class CommunicationPortCollectController
