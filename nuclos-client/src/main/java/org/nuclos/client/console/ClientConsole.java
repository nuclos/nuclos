//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.console;

import static org.nuclos.common.ConsoleCommand.CHECK_DATA_SOURCES;
import static org.nuclos.common.ConsoleCommand.CHECK_DOCUMENT_ATTACHMENTS;
import static org.nuclos.common.ConsoleCommand.CLEANUP_DUPLICATE_DOCUMENTS;
import static org.nuclos.common.ConsoleCommand.CLEAR_USER_PREFERENCES;
import static org.nuclos.common.ConsoleCommand.COMPILE_DB_OBJECTS;
import static org.nuclos.common.ConsoleCommand.DROP_CONSTRAINTS;
import static org.nuclos.common.ConsoleCommand.FIX_RECORDGRANT_PK_ALIAS;
import static org.nuclos.common.ConsoleCommand.FREE_DAL_MEMORY;
import static org.nuclos.common.ConsoleCommand.GENERATE_BO_UID_LIST;
import static org.nuclos.common.ConsoleCommand.GET_CONSTRAINTS;
import static org.nuclos.common.ConsoleCommand.INVALIDATE_ALL_CACHES;
import static org.nuclos.common.ConsoleCommand.KILL_SESSION;
import static org.nuclos.common.ConsoleCommand.MIGRATE_DOCUMENT_ATTACHMENTS_INTO_SUBDIRECTORIES;
import static org.nuclos.common.ConsoleCommand.MIGRATE_JASPER_REPORTS;
import static org.nuclos.common.ConsoleCommand.MIGRATE_SUBFORM_TO_ON_DELETE_CASCADE_CONSTRAINT;
import static org.nuclos.common.ConsoleCommand.QUERY_DAL_MEMORY_LOAD;
import static org.nuclos.common.ConsoleCommand.REBUILD_CLASSES;
import static org.nuclos.common.ConsoleCommand.REBUILD_CONSTRAINTS;
import static org.nuclos.common.ConsoleCommand.REBUILD_CONSTRAINTS_AND_INDEXES;
import static org.nuclos.common.ConsoleCommand.REBUILD_INDEXES;
import static org.nuclos.common.ConsoleCommand.REBUILD_LUCENE_INDEX;
import static org.nuclos.common.ConsoleCommand.SEND_MESSAGE;
import static org.nuclos.common.ConsoleCommand.SET_MANDATOR_LEVEL;
import static org.nuclos.common.ConsoleCommand.SET_SIDEVIEW_MENU_WIDTH;
import static org.nuclos.common.ConsoleCommand.SHOW_MAINTENANCE;
import static org.nuclos.common.ConsoleCommand.SOURCE_SCANNER_RUN_ONCE;
import static org.nuclos.common.ConsoleCommand.START_MAINTENANCE;
import static org.nuclos.common.ConsoleCommand.STOP_MAINTENANCE;

import java.io.PipedOutputStream;
import java.util.Arrays;
import java.util.List;

import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.CommonConsole;
import org.nuclos.common.ConsoleCommand;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * Management console for Nuclos.
 */
public class ClientConsole {

	/**
	 * this list is used by NuclosConsoleGui to show the available commands
	 */
	public static final List<ConsoleCommand> LSTCOMMANDS = Arrays.asList(
			INVALIDATE_ALL_CACHES,
			FREE_DAL_MEMORY,
			QUERY_DAL_MEMORY_LOAD,
			REBUILD_CLASSES,
			SOURCE_SCANNER_RUN_ONCE,
			CLEAR_USER_PREFERENCES,
			COMPILE_DB_OBJECTS,
			CHECK_DATA_SOURCES,
			SEND_MESSAGE,
			KILL_SESSION,
			GET_CONSTRAINTS,
			DROP_CONSTRAINTS,
			REBUILD_CONSTRAINTS,
			REBUILD_INDEXES,
			REBUILD_CONSTRAINTS_AND_INDEXES,
			SET_MANDATOR_LEVEL,
			GENERATE_BO_UID_LIST,
			CHECK_DOCUMENT_ATTACHMENTS,
			CLEANUP_DUPLICATE_DOCUMENTS,
			MIGRATE_DOCUMENT_ATTACHMENTS_INTO_SUBDIRECTORIES,
			SHOW_MAINTENANCE,
			START_MAINTENANCE,
			STOP_MAINTENANCE,
			MIGRATE_JASPER_REPORTS,
			MIGRATE_SUBFORM_TO_ON_DELETE_CASCADE_CONSTRAINT,
			SET_SIDEVIEW_MENU_WIDTH,
			REBUILD_LUCENE_INDEX,
			FIX_RECORDGRANT_PK_ALIAS
	);

	private static ClientConsole INSTANCE;

	public static ClientConsole getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	public ClientConsole() {
	}

	protected static ClientConsole newClientConsole() {
		try {
			final String sClassName = LangUtils.defaultIfNull(
					ApplicationProperties.getInstance().getConsoleClassName(),
					ClientConsole.class.getName());

			INSTANCE = (ClientConsole) LangUtils.getClassLoaderThatWorksForWebStart().loadClass(sClassName).newInstance();
			return INSTANCE;
		} catch (Exception ex) {
			throw new CommonFatalException("Console could not be created.", ex);
		}
	}

	public void parseAndInvoke(PipedOutputStream out, String[] asArgs) throws Exception {
		new CommonConsole(out).parseAndInvoke(asArgs);
	}

	public void parseAndInvoke(PipedOutputStream out, String sCommand, String sArguments) throws Exception {
		new CommonConsole(out).parseAndInvoke(sCommand, sArguments);
	}

}    // class NuclosConsole
