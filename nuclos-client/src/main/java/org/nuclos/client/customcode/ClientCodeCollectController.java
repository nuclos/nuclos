package org.nuclos.client.customcode;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.text.Document;

import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.exception.CollectableValidationException;


public class ClientCodeCollectController extends CodeCollectController  {
	
	public ClientCodeCollectController(UID entity, MainFrameTab tabIfAny) {
		super(entity, tabIfAny);
	}

	@Override
	protected CodeCheckAction getCodeCheckAction() {
		return new CodeCheckAction() {
			
			@Override
			void cmdCheckRuleSource() {}
		};
	}

	@Override
	protected List<Component> getAdditionalToolBarComponents() {
		return new ArrayList<Component>();
	}

	@Override
	protected void readAdditionalValuesFromEditPanel(
			CollectableMasterDataWithDependants<UID> clct, boolean bSearchTab)
			throws CollectableValidationException {
		
		clct.setField(E.CLIENTCODE.source.getUID(), new CollectableValueField(pnlEdit.getJavaEditorPanel().getText()));
		if (pnlEdit.getNucletUid() != null) {
			clct.setField(E.CLIENTCODE.nuclet.getUID(), new CollectableValueIdField(pnlEdit.getNucletUid(), pnlEdit.getNucletUid()));			
		}
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> newCollectableWithDefaultValues(boolean forInsert) {
		CollectableMasterDataWithDependants<UID> result = super.newCollectable();
		result.setField(E.CLIENTCODE.name.getUID(), new CollectableValueField(
				getSpringLocaleDelegate().getText("CodeCollectController.fieldvalue.name.temp", " ")));
		return result;
	}
	
	@Override
	protected void unsafeFillDetailsPanel(CollectableMasterDataWithDependants<UID> clctmd) throws NuclosBusinessException {
		// fill the textfields:
		super.unsafeFillDetailsPanel(clctmd);
		
		Document doc1 = pnlEdit.getJavaEditorPanel().getDocument();
		doc1.removeUndoableEditListener(undoableEditListener);
		
		if (clctmd.getPrimaryKey() != null) {
			pnlEdit.getJavaEditorPanel().setText((String)clctmd.getValue(E.CLIENTCODE.source.getUID()));
			pnlEdit.getJavaEditorPanel().setCaretPosition(0);
			pnlEdit.setUid((UID)clctmd.getPrimaryKey());
		} else {
			if (clctmd.getValue(E.CLIENTCODE.source.getUID()) != null) {
				pnlEdit.getJavaEditorPanel().setText((String)clctmd.getValue(E.CLIENTCODE.source.getUID()));
			} else {
				pnlEdit.getJavaEditorPanel().setText(null);
			}
			pnlEdit.getJavaEditorPanel().setCaretPosition(0);
		}
		if (clctmd.getValueId(E.CLIENTCODE.nuclet.getUID()) != null) {
			pnlEdit.setNucletUid((UID)clctmd.getValueId(E.CLIENTCODE.nuclet.getUID())); 
		}

		doc1.addUndoableEditListener(undoableEditListener);
	}
	
}
