//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.customcomp.resplan;

import java.util.Collection;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.customcomp.resplan.ResPlanResourceVO;
import org.nuclos.common2.LocaleInfo;

//Version
@SuppressWarnings("serial")
public abstract class AbstractTranslationTableModel extends AbstractTableModel {

	protected static String[] columns = {ResPlanResourceVO.LOCALE};
	protected Map<UID, LocaleInfo> localeLabels;

	public AbstractTranslationTableModel(Collection<LocaleInfo> locales) {
		localeLabels = CollectionUtils.transformIntoMap(locales, new Transformer<LocaleInfo, UID>() {
			@Override
			public UID transform(LocaleInfo i) {
				return i.getLocale();
			}
		});
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex != 0;
	}
}
