package org.nuclos.client.customcomp.resplan;

import java.awt.Color;
import java.util.Date;

public interface CalendarBackgroundProvider {
	
	Color BACKGROUND_TODAY = new Color(0xccffcc);
	
	Color BACKGROUND_HOLIDAY = new Color(0xffcccc);
	
	String BGCOLOR_TAG = "<bg color=";

	Color getBackgroundColorForInterval(Date start, Date end);
	
}
