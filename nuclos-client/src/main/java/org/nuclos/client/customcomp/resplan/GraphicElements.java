package org.nuclos.client.customcomp.resplan;

import java.awt.Color;

import javax.swing.ImageIcon;

import org.nuclos.client.resource.ResourceCache;
import org.nuclos.common.UID;

public class GraphicElements {
	private static ResourceCache resourceCache = ResourceCache.getInstance();
	public static Color decodeColor(String sColor) {
		Color c = null;
		if (sColor != null && !sColor.isEmpty()) try {
			c = Color.decode(sColor);
		} catch (NumberFormatException nfe) {
		}
		return c;
	}
	
	public static ImageIcon getIconFromResourceFile(UID sIcon) {
		ImageIcon i = null;
		i = resourceCache.getIconResource(sIcon);
		return i;
	}
}
