//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.customcomp.resplan;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.apache.log4j.Logger;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.customcomp.CustomComponentController;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.customcomp.valueobject.CustomComponentVO;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

//Version
public class ResPlanAction<PK,R,C extends Collectable<PK>> extends AbstractAction {
	
	private static final Logger LOG = Logger.getLogger(ResPlanAction.class);

	public ResPlanAction(CustomComponentVO componentVO) {
		putValue(Action.NAME, SpringLocaleDelegate.getInstance().getTextFallback(
				componentVO.getLabelResourceId(), componentVO.getLabelResourceId()) + "...");
		putValue(Action.SMALL_ICON, MainFrame.resizeAndCacheTabIcon(NuclosResourceCache.getNuclosResourceIcon("org.nuclos.client.resource.icon.glyphish.83-calendar.png")));
		putValue(Action.ACTION_COMMAND_KEY, componentVO.getPrimaryKey().getString());
		boolean enabled = false;
		// TODO: move this into the controller?
		try {
			enabled = hasAccessPermissions(componentVO);
		} catch (Exception e) {
			LOG.warn("ResPlanAction failed: " + e, e);
		}
		setEnabled(enabled);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		final String internalName = evt.getActionCommand();
		UIUtils.runCommand(Main.getInstance().getMainFrame(), new Runnable() {
			@Override
			public void run() {
				try {
					CustomComponentController controller = CustomComponentController.newController(new UID(internalName));
					controller.run();
				} catch (Exception ex) {
					Errors.getInstance().showExceptionDialog(Main.getInstance().getMainFrame(), ex);
				}
			}
		});
	}

	/**
	 * The PlanningTable requires permission to read all of its entities.
	 * Before the code was extracted to this method, users without permission to read the holiday entity, could see the Planning Table
	 * in the menu, but got an exception, when trying to open it. Now users lacking the permissions should not see the menu entry anymore.
	 * @param componentVO
	 * @return
	 */
	private boolean hasAccessPermissions(CustomComponentVO componentVO) {
		final Jaxb2Marshaller marshaller = SpringApplicationContextHolder.getBean(Jaxb2Marshaller.class);
		ClientResPlanConfigVO configVO = ClientResPlanConfigVO.fromBytes(componentVO.getData(), marshaller);
		List<PlanElement> planElements = configVO.getPlanElements();
		ArrayList<UID> bosReadRequired = new ArrayList<>();
		bosReadRequired.add(configVO.getResourceEntity());
		if (configVO.getHolidaysEntity() != null)  {
			bosReadRequired.add(configVO.getHolidaysEntity());
		}
		bosReadRequired.addAll(planElements.stream().map(pe -> pe.getEntity()).collect(Collectors.toCollection(ArrayList::new)));

		boolean allReadAllowed = true;
		for (UID uid : bosReadRequired) {
			allReadAllowed = allReadAllowed && SecurityCache.getInstance().isReadAllowedForEntity(uid);
		}
		return allReadAllowed;
	}

}
