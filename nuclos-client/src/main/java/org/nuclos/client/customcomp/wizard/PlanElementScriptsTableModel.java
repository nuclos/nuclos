package org.nuclos.client.customcomp.wizard;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.customcomp.resplan.PlanElementsTableModel;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common2.SpringLocaleDelegate;

public class PlanElementScriptsTableModel<PK,R,C extends Collectable<PK>> extends AbstractTableModel {

	protected static String[] columns = new String[4];
	private static int SCRIPT_COLUMN = 3;

	static {
		SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
		for (int i = 0; i < columns.length; i++) {
			columns[i] = sld.getMessage("wizard.step.planelemtablecol." + (i + 1), "?");
		}
	}

	static {
		SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
		for (int i = 0; i < columns.length; i++) {
			columns[i] = sld.getMessage("wizard.step.planelemtablecol." + (i + 1), "?");
		}
		columns[SCRIPT_COLUMN] = sld.getMessage("wizard.step.planelemtablecol." + (10), "?");
	}

	protected List<PlanElement<R>> lstRows = new ArrayList<PlanElement<R>>();

	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				return rowIndex + 1;
			case 1:
				int type = lstRows.get(rowIndex).getType();
				if (type > 0 && type <= PlanElementsTableModel.types.length) {
					return PlanElementsTableModel.types[type - 1];
				}
				return "Undef.";
			case 2:
				return lstRows.get(rowIndex).getEntity() == null ? null
						: MetaProvider.getInstance().getEntity(lstRows.get(rowIndex).getEntity()).getEntityName();
			case 3:
				return lstRows.get(rowIndex).getScriptingEntryCellMethod();
			default:
				break;
		}
		return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		boolean isValidRow = (rowIndex >= 0 && rowIndex < lstRows.size());
		boolean isScriptingColumn = SCRIPT_COLUMN == columnIndex;
		if (isValidRow && isScriptingColumn) {
			lstRows.get(rowIndex).setScriptingEntryCellMethod((String) aValue);
		}
	}

	public void setRows(List<PlanElement<R>> rows) {
		lstRows = rows;
		this.fireTableDataChanged();
	}

	public List<PlanElement<R>> getRows() {
		return lstRows;
	}

	@Override
	public int getRowCount() {
		return lstRows.size();
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (SCRIPT_COLUMN == columnIndex) {
			return true;
		}
		return false;
	}

	@Override
	public String getColumnName(int column) {
		return columns[column];
	}

}
