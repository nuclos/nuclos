package org.nuclos.client.customcomp.wizard.table;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;

public class TableComboBoxEditor extends DefaultCellEditor {

	public JComboBox comboBox;

	public TableComboBoxEditor() {
		super(new JComboBox());
		comboBox = (JComboBox) getComponent();
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		return super.getTableCellEditorComponent(table, value, isSelected, row, column);
	}

}

