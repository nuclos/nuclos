package org.nuclos.client.customcomp.wizard.table;

import java.awt.Component;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class TableComboBoxRenderer extends DefaultTableCellRenderer {

	public JComboBox comboBox;

	public TableComboBoxRenderer() {
		super();
		comboBox = new JComboBox();
	}

	@Override
	public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
		comboBox.setSelectedItem(value);
		return comboBox;
	}

}
