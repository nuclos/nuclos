//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.datasource.admin;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectableBean;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;

/**
 * <code>Collectable</code> datasource.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 * todo: can this class be replaced with the masterdata mechanism?
 */
public class CollectableDataSource<T extends DatasourceVO> extends AbstractCollectableBean<T,UID> {

	public final CollectableEntity clcte;

	public CollectableDataSource(T datasourcevo) {
		super(datasourcevo);
		if (datasourcevo instanceof DynamicEntityVO) {
			clcte = new CollectableMasterDataEntity(E.DYNAMICENTITY);
		} else if (datasourcevo instanceof ValuelistProviderVO) {
			clcte = new CollectableMasterDataEntity(E.VALUELISTPROVIDER);
		} else if (datasourcevo instanceof RecordGrantVO) {
			clcte = new CollectableMasterDataEntity(E.RECORDGRANT);
		} else if (datasourcevo instanceof DynamicTasklistVO) {
			clcte = new CollectableMasterDataEntity(E.DYNAMICTASKLIST);
		} else if (datasourcevo instanceof ChartVO) {
			clcte = new CollectableMasterDataEntity(E.CHART);
		} else if (datasourcevo instanceof CalcAttributeVO) {
			clcte = new CollectableMasterDataEntity(E.CALCATTRIBUTE);
		} else {
			clcte = new CollectableMasterDataEntity(E.DATASOURCE);
		}
	}

	public T getDatasourceVO() {
		return this.getBean();
	}

	@Override
	public UID getId() {
		return this.getDatasourceVO().getId();
	}

	@Override
	public void removeId() {
		this.getDatasourceVO().setPrimaryKey(null);
	}

	@Override
	protected CollectableEntity getCollectableEntity() {
		return clcte;
	}

	@Override
	public Object getValue(UID fieldUid) {
		try {
			String sFieldName = MetaProvider.getInstance().getEntityField(fieldUid).getFieldName();
			return PropertyUtils.getProperty(this.getDatasourceVO(), sFieldName);
		}
		catch (IllegalAccessException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (InvocationTargetException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (NoSuchMethodException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	/*
	@Override
	public String getIdentifierLabel() {
		return getDatasourceVO().getName();
	}
	 */

	@Override
	public int getVersion() {
		return getDatasourceVO().getVersion();
	}

	@Override
	public UID getEntityUID() {
		return clcte.getUID();
	}

	@Override
	public UID getPrimaryKey() {
		return getBean().getPrimaryKey();
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(getCollectableEntity());
		result.append(",vo=").append(getBean());
		result.append(",id=").append(getId());
//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//		result.append(",label=").append(getIdentifierLabel());
		result.append(",complete=").append(isComplete());
		result.append("]");
		return result.toString();
	}

	public static class MakeCollectable<T extends DatasourceVO> implements Transformer<T, CollectableDataSource<T>> {
		@Override
		public CollectableDataSource<T> transform(T datasourcevo) {
			return new CollectableDataSource<T>(datasourcevo);
		}
	}

}	// class CollectableDataSource
