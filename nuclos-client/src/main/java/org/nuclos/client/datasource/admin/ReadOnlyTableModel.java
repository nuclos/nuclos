package org.nuclos.client.datasource.admin;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;

/**
 *
 * A read only table model.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version 01.00.00
 */
public class ReadOnlyTableModel implements TableModel {

	private final List<TableModelListener> lstListeners = new ArrayList<>();

	private final ResultVO resultvo;

	ReadOnlyTableModel(ResultVO resultvo) {
		this.resultvo = resultvo;
	}

	@Override
	public int getColumnCount() {
		return resultvo.getColumns().size();
	}

	@Override
	public int getRowCount() {
		return resultvo.getRows().size();
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	public Class<?> getRealColumnClass(int columnIndex) {
		final ResultColumnVO columnVO = resultvo.getColumns().get(columnIndex);
		return columnVO.getColumnClass();
	}

	@Override
	public Object getValueAt(int iRow, int iColumn) {
		final ResultColumnVO columnVO = resultvo.getColumns().get(iColumn);
		return columnVO.format(resultvo.getRows().get(iRow)[iColumn], LocaleDelegate.getInstance().getLocale());
	}

	public Object getUnformattedValueAt(int iRow, int iColumn) {
		return resultvo.getRows().get(iRow)[iColumn];
	}

	@Override
	public void setValueAt(Object oValue, int iRow, int iColumn) {
		// do nothing because this is a read only model
	}

	@Override
	public String getColumnName(int columnIndex) {
		return resultvo.getColumns().get(columnIndex).getColumnLabel();
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		lstListeners.add(l);
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		lstListeners.remove(l);
	}
}
