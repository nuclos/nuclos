//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.entityobject;

import java.util.Map;

import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Removable;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class CollectableEntityObject<PK> extends CollectableMasterData<PK> implements Removable {

	private final EntityObjectVO<PK> vo;

	private final Map<UID, CollectableField> mpFields = CollectionUtils.newHashMap();

	public CollectableEntityObject(CollectableEntity ce, EntityObjectVO<PK> vo) {
		super(ce, null, vo.getDependents());
		
		this.vo = vo;
	}

	@Override
	public Object getValue(UID fieldUid) {
		return this.vo.getFieldValue(fieldUid, Object.class);
	}

	@Override
	public Object getValueId(UID fieldUid) {
		return RigidUtils.firstNonNull(this.vo.getFieldId(fieldUid), this.vo.getFieldUid(fieldUid));
	}
	
	@Override
	public boolean isComplete() {
		return vo.isComplete();
	}

	@Override
	public CollectableField getField(UID fieldUid)	throws CommonFatalException {
		CollectableField field = mpFields.get(fieldUid);
		if(field == null) {
			field = new CollectableEntityObjectField(fieldUid, this);
			mpFields.put(fieldUid, field);
		}

		return field;
	}

	@Override
	public PK getId() {
		if(vo.getPrimaryKey() == null)
			return null;
		return vo.getPrimaryKey();
	}

	@Override
	public void removeId() {
		vo.setPrimaryKey(null);
	}

	@Override
	public int getVersion() {
		return vo.getVersion();
	}

	@Override
	public void setField(UID fieldUid, CollectableField clctfValue) {
		setField(fieldUid, clctfValue, false);
	}

	public void setField(UID fieldUid, CollectableField clctfValue, boolean skipUpdateFlag) {
		mpFields.put(fieldUid, clctfValue);
		vo.setFieldValue(fieldUid, clctfValue.getValue());
		if(clctfValue.isIdField()) {
			if (clctfValue.getValueId() == null) {
				if (vo.getFieldIds().containsKey(fieldUid))
					vo.setFieldId(fieldUid, null);
				else if (vo.getFieldUids().containsKey(fieldUid))
					vo.setFieldUid(fieldUid, null);
			} else {
				if (clctfValue.getValueId() instanceof UID) {
					vo.setFieldUid(fieldUid, (UID) clctfValue.getValueId());
				} else {
					vo.setFieldId(fieldUid, IdUtils.toLongId(clctfValue.getValueId()));
				}
			}
		}
		if (!vo.isFlagRemoved()) {
			if (vo.getId() == null) {
				vo.flagNew();
			}
			else {
				if (!skipUpdateFlag) {
					vo.flagUpdate();					
				}
			}
		}
	}

	@Override
	public boolean isMarkedRemoved() {
		return vo.isFlagRemoved();
	}

	@Override
	public void markRemoved() {
		vo.flagRemove();
	}

	public EntityObjectVO<PK> getEntityObjectVO() {
		return this.vo;
	}
	
	@Override
	public MasterDataVO<PK> getMasterDataCVO() {
		return DalSupportForMD.wrapEntityObjectVO(this.vo);
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(getCollectableEntity());
		result.append(",vo=").append(getEntityObjectVO());
		result.append(",meta=").append(getCollectableEntity());
		result.append(",mdVo=").append(getMasterDataCVO());
		result.append(",dep=").append(getDependantMasterDataMap());
		result.append(",cdep=").append(getDependantCollectableMasterDataMap());
		result.append(",id=").append(getId());
//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//		result.append(",label=").append(getIdentifierLabel());
		result.append(",complete=").append(isComplete());
		result.append("]");
		return result.toString();
	}

	/**
	 * inner class MakeCollectable: makes a <code>MasterDataVO</code> <code>Collectable</code>.
	 */
	public static class MakeCollectable<PK> implements Transformer<EntityObjectVO<PK>, CollectableEntityObject<PK>> {

		private final CollectableEntity clctmde;

		/*public MakeCollectable(CollectableMasterDataEntity clctmde) {
			this.clctmde = Utils.transformCollectableMasterDataEntityTOCollectableEOEntity(clctmde);
		}*/

		public MakeCollectable(CollectableEntity clctmde) {
			this.clctmde = clctmde;
		}

		@Override
		public CollectableEntityObject<PK> transform(EntityObjectVO<PK> mdvo) {
			CollectableEntityObject<PK> collectableEntityObject = new CollectableEntityObject<PK>(this.clctmde, mdvo);
			collectableEntityObject.setDependantMasterDataMap(mdvo.getDependents());
			return collectableEntityObject;
		}

	}

	public static class ExtractAbstractCollectableVO<PK> implements Transformer<AbstractCollectable<PK>, EntityObjectVO<PK>> {
		@Override
		public EntityObjectVO<PK> transform(AbstractCollectable<PK> clctmd) {
			if(clctmd instanceof CollectableEntityObject){
				CollectableEntityObject<PK> ceo = (CollectableEntityObject<PK>) clctmd;
				IDependentDataMap depmdmp = ceo.getDependantMasterDataMap();
				EntityObjectVO<PK> mdVO = ceo.getEntityObjectVO();

				mdVO.setDependents(depmdmp);

				return mdVO;
			}
			else{
				return new EntityObjectVO<PK>(clctmd.getEntityUID());
			}

		}
	}

	/**
	 * inner class ExtractMasterDataVO: the inverse operation of <code>MakeCollectable</code>.
	 */
	public static class ExtractMasterDataVO<PK> implements Transformer<CollectableEntityObject<PK>, EntityObjectVO<PK>> {
		@Override
		public EntityObjectVO<PK> transform(CollectableEntityObject<PK> clctmd) {
			IDependentDataMap depmdmp = clctmd.getDependantMasterDataMap();
			EntityObjectVO<PK> mdVO = clctmd.getEntityObjectVO();

			mdVO.setDependents(depmdmp);
			return mdVO;
		}
	}
	
	@Override
	public boolean isDirty() {
		return !vo.isFlagUnchanged();
	}
	
	public IDataLanguageMap getDataLanguageMap(){
		return vo.getDataLanguageMap();
	}
	
}
