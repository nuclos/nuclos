package org.nuclos.client.entityobject;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.LocalizedCollectableValueField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDataLanguageLocalizedEntityEntry;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.remoting.TypePreservingObjectDeserializer;
import org.nuclos.remoting.TypePreservingObjectSerializer;
import org.nuclos.server.i18n.DataLanguageUtils;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class CollectableEntityObjectField extends LocalizedCollectableValueField {

	private UID fieldUid;
	@JsonSerialize(using = TypePreservingObjectSerializer.class)
	@JsonDeserialize(using = TypePreservingObjectDeserializer.class)
	private Object oValue;
	@JsonSerialize(using = TypePreservingObjectSerializer.class)
	@JsonDeserialize(using = TypePreservingObjectDeserializer.class)
	private Object oValueId;

	private int fieldType;

	private Class<?> javaType;

	private boolean isLocalized;
	private boolean isCalculated;
	private UID foreignEntity;
	private String foreignEntityField;
	private EntityObjectVO eovo;
	private UID datalangUID;
	private UID userDataLangUID;

	/**
	 * for deserialization only
	 */
	protected CollectableEntityObjectField() {
		super();
	}
	
	public CollectableEntityObjectField(UID fieldUid, CollectableEntityObject<?> ceo) {
		super();

		this.fieldUid = fieldUid;
		this.oValue = ceo.getValue(fieldUid);
		this.oValueId = ceo.getValueId(fieldUid);
		this.fieldType = ceo.getCollectableEntity().getEntityField(fieldUid).getFieldType();
		this.javaType = ceo.getCollectableEntity().getEntityField(fieldUid).getJavaClass();
		this.eovo = ceo.getEntityObjectVO();
		this.isLocalized = ceo.getCollectableEntity().getEntityField(fieldUid).isLocalized();
		this.isCalculated = ceo.getCollectableEntity().getEntityField(fieldUid).isCalculated();
		this.foreignEntity = ceo.getCollectableEntity().getEntityField(fieldUid).getReferencedEntityUID();
		this.foreignEntityField = ceo.getCollectableEntity().getEntityField(fieldUid).getReferencedEntityFieldName();
					
		this.datalangUID = DataLanguageContext.getDataSystemLanguage();
		this.userDataLangUID = DataLanguageContext.getDataUserLanguage();
		
		if (this.oValue == null) {
			setLabel("");
		} else if (oValue instanceof UID && UID.isStringifiedUID(this.oValue.toString()) && MetaProvider.getInstance().isEntity((UID)oValue)) { 
			EntityMeta<?> mdmVO = MasterDataDelegate.getInstance().getMetaData(ceo.getEntityUID());
			setLabel(SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(mdmVO));
		} else {
			setLabel(this.oValue.toString());
		}
	}
	
	public UID getFieldUID() {
		return fieldUid;
	}

	@Override
	public int getFieldType() {
		return fieldType;
	}

	@Override
	public Object getValue() {
		
		if (isLocalized && !isCalculated) {
			return getLocalizedValue();
		} else {
			if (foreignEntity != null && foreignEntityField != null) {
				Object fieldValue = this.eovo.getFieldValue(DataLanguageUtils.extractForeignEntityReference(foreignEntity, fieldUid));
				return fieldValue != null ? fieldValue.toString() : this.oValue;
			} else {
				return this.oValue;							
			}
		}
	}

	
	private Object getLocalizedValue() {
		Object retVal = null;
		
		if (this.userDataLangUID == null || this.userDataLangUID.equals(this.datalangUID)) {
			retVal = this.eovo.getFieldValue(this.fieldUid) != null ? this.eovo.getFieldValue(this.fieldUid) : null;
		} else {
			if (this.eovo.getDataLanguageMap() == null) {
				retVal = this.eovo.getFieldValue(this.fieldUid) != null ? this.eovo.getFieldValue(this.fieldUid) : null;	
			} else {
				IDataLanguageLocalizedEntityEntry fieldLanguageData =
						this.eovo.getDataLanguageMap().getDataLanguage(this.userDataLangUID);
				if (fieldLanguageData == null) {
					retVal = this.eovo.getFieldValue(this.fieldUid) != null ? this.eovo.getFieldValue(this.fieldUid) : null;				
				} else {
					retVal = fieldLanguageData.getFieldValue(
							DataLanguageUtils.extractFieldUID(this.fieldUid));
				}
			}
		}
		
		return retVal;
	}

	public Object getEntityObjectId() {
		if (this.oValueId != null) {
			return this.oValueId;
		}
		else if (eovo != null) {
			return this.eovo.getPrimaryKey();
		}

		return null;
	}

	@Override
	public Object getValueId() throws UnsupportedOperationException {
		return this.oValueId;
	}

	@Override
	public String toDescription() {
		final ToStringBuilder b = new ToStringBuilder(this).append(fieldType).append(fieldUid).append(oValueId).append(oValue);
		return b.toString();
	}

	@Override
	public boolean isNull() {
		if (getFieldType() == CollectableField.TYPE_VALUEFIELD) {
			return getValue() == null;
		}
		else if (getFieldType() == CollectableField.TYPE_VALUEIDFIELD) {
			return getValueId() == null;
		}
		throw new IllegalStateException();
	}

	@Override
	public int compareTo(CollectableField that) {
		if (String.class.isAssignableFrom(javaType)) {
			return super.compareTo(that);
		}
		else {
			return LangUtils.compare(this.getValue(), that.getValue());
		}
	}
	
	public IDataLanguageMap getDataLanguageMap() {
		return this.eovo.getDataLanguageMap();
	}
	
	@Override
	public String toString() {
		return getValue() == null ? null : getValue().toString();
	}
}
