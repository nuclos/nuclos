//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.entityobject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class EntityObjectPresenter implements Comparable<EntityObjectPresenter> {

	private final EntityObjectVO<?> eo;

	private final UID presentingFieldUID;

	public EntityObjectPresenter(final EntityObjectVO<?> eo, final UID presentingFieldUID) {
		this.eo = eo;
		this.presentingFieldUID = presentingFieldUID;
	}

	public EntityObjectVO<?> getEo() {
		return eo;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		final EntityObjectPresenter that = (EntityObjectPresenter) o;

		if (eo != null ? !eo.equals(that.eo) : that.eo != null) return false;
		return presentingFieldUID != null ? presentingFieldUID.equals(that.presentingFieldUID) : that.presentingFieldUID == null;
	}

	@Override
	public int hashCode() {
		int result = eo != null ? eo.hashCode() : 0;
		result = 31 * result + (presentingFieldUID != null ? presentingFieldUID.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		if (presentingFieldUID == null || eo == null) {
			return "";
		}
		final Object fieldValue = eo.getFieldValue(presentingFieldUID);
		if (fieldValue == null) {
			return "";
		}
		return StringUtils.defaultIfBlank(fieldValue.toString(), "");
	}

	@Override
	public int compareTo(final EntityObjectPresenter o) {
		return this.toString().compareToIgnoreCase(o.toString());
	}

	public static <T> List<EntityObjectPresenter> transform(Collection<EntityObjectVO<T>> eoList, UID presentingFieldUID, boolean addNullInFront) {
		List<EntityObjectPresenter> result = new ArrayList<>();
		for (EntityObjectVO<?> eo : eoList) {
			result.add(new EntityObjectPresenter(eo, presentingFieldUID));
		}
		Collections.sort(result);
		if (addNullInFront) {
			result.add(0, new EntityObjectPresenter(null, null));
		}
		return result;
	}
}
