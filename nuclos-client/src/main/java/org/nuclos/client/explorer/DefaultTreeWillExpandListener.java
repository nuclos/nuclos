//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer;

import java.awt.Container;
import java.awt.Cursor;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;

import org.apache.log4j.Logger;
import org.nuclos.client.command.CommonClientWorker;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.navigation.treenode.LabelTreeNode;

public class DefaultTreeWillExpandListener implements TreeWillExpandListener {

	private static final Logger LOG = Logger.getLogger(DefaultTreeWillExpandListener.class);
	
	private final JTree tree;

	public DefaultTreeWillExpandListener(JTree tree) {
		super();
		this.tree = tree;
	}

	@Override
    public void treeWillExpand(final TreeExpansionEvent ev) {
		final ExplorerNode<?> explorernode = (ExplorerNode<?>) ev.getPath().getLastPathComponent();
		final boolean bSubNodesHaveBeenLoaded = explorernode.getChildCount() > 0;
		
		if (!bSubNodesHaveBeenLoaded) {
			CommonClientWorker worker = new CommonClientWorker() {
				@Override
				public void init() throws CommonBusinessException {
					UIUtils.setWaitCursor(tree);
					enableCancelButton(true);
				}

				@Override
				public void work() throws CommonBusinessException {
					explorernode.loadChildren(false);
				}

				@Override
				public void paint() throws CommonBusinessException {
					try {
						for (int i = 0; i < explorernode.getChildCount(); i++) {
							final ExplorerNode<?> explorernodeChild = (ExplorerNode<?>) explorernode.getChildAt(i);
							//NUCLOS-3713-4: LabelTreeNode (e.g. used for grouping) does not load children from the DB, so no automatic expanding.
							if (!(explorernodeChild.getUserObject() instanceof LabelTreeNode)) {
								ExplorerController.expandAllLoadedNodes(tree, ev.getPath().pathByAddingChild(explorernodeChild));
							}
						}
						tree.updateUI();
					} finally {
						tree.setCursor(Cursor.getDefaultCursor());
						enableCancelButton(false);
					}

				}

				@Override
				public void handleError(final Exception ex) {
					LOG.error("DefaultTreeWillExpandListener.treeWillExpand: " + ex, ex);
				}

				@Override
				public JComponent getResultsComponent() {
					return tree;
				}

				private void enableCancelButton(final boolean enabled) {
					if (ev.getSource() instanceof Container) {
						Container container = (Container) ev.getSource();
						while (!(container instanceof DefaultExplorerView) && container.getParent() != null) {
							container = container.getParent();
						}
						if (container instanceof DefaultExplorerView) {
							((DefaultExplorerView) container).enableCancelButton(enabled);
						}
					}
				}
			};

			CommonMultiThreader.getInstance().execute(worker);
		}
	}

	@Override
    public void treeWillCollapse(TreeExpansionEvent ev) {
		// do nothing
	}
}
