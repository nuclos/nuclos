//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import javax.swing.JPopupMenu;
import javax.swing.tree.TreePath;

import org.nuclos.client.explorer.NodeActionContext;
import org.nuclos.client.explorer.configuration.ConfigurationExplorerNodeActions.MenuAction;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * ConfigurationExplorerMouseListener
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class ConfigurationExplorerMouseListener extends MouseAdapter {

	private final SpringLocaleDelegate localeDelegate;
	private final DefaultConfigurationExplorerView view;
	private final MainFrameTab parent;
	

	public ConfigurationExplorerMouseListener(final MainFrameTab parent, final DefaultConfigurationExplorerView view) {
		localeDelegate = SpringLocaleDelegate.getInstance();
		this.view = view;
		this.parent = parent;
	}

	@Override
	public void mousePressed(MouseEvent ev) {
		mouseEventOnNode(ev);
	}

	@Override
	public void mouseReleased(MouseEvent ev) {
		mouseEventOnNode(ev);
	}

	@Override
	public void mouseClicked(MouseEvent ev) {
		mouseEventOnNode(ev);
	}

	private void mouseEventOnNode(MouseEvent ev) {
		TreePath tp = view.getJTree().getPathForLocation(ev.getX(), ev.getY());
		if (null != tp) {
			this.mouseEventOnNode(ev, tp);
		}		

	}

	protected void mouseEventOnNode(MouseEvent ev, TreePath path) {
		final ConfigurationTreeNode<?> node = (ConfigurationTreeNode<?>) path.getLastPathComponent();
		if (ev.isPopupTrigger()) {
			showPopup(ev, node);
		} else {
			if (ev.getClickCount() == 2) {
				ConfigurationExplorerNodeActions.editNodeAction(parent, createNodeContext(node), view).actionPerformed(null);
			}
		}
	}

	private void showPopup(MouseEvent ev, ConfigurationTreeNode<?> node) {


		// No actions available by default (opt in)
		final EnumSet<MenuAction> actions = EnumSet.noneOf(MenuAction.class);

		// New nodes allowed if inherit nodes is not set.
		if (node instanceof ConfigurationEntityTreeSubNode) {

			/**
			 * NEW
			 * - node must not define 'inherited nodes' flag
			 */
			if (!((ConfigurationEntityTreeSubNode)node).getContent().getIsInheritNodes()) {
				actions.add(MenuAction.NEW_NODE);		
			}
		} else {
			actions.add(MenuAction.NEW_NODE);
		}

		/**
		 * EDIT, REMOVE
		 * - is subnode
		 */
		if (null != node.getParent()) {
			actions.add(MenuAction.EDIT_NODE);
			actions.add(MenuAction.REMOVE_NODE);
		}

		TreePath[] selectedPaths = view.getJTree().getSelectionPaths();
		if (selectedPaths != null) {
			final List<TreePath> lstSelectedTreePaths = Arrays.asList(selectedPaths);
			final List<ConfigurationTreeNode<?>> lstSelectedNodes = new ArrayList<ConfigurationTreeNode<?>>();
			
			for (final TreePath path: lstSelectedTreePaths) {
				final Object obj = path.getLastPathComponent();
				if (obj instanceof ConfigurationTreeNode) {
					lstSelectedNodes.add((ConfigurationTreeNode<?>)obj);
				}

				buildPopup(actions, lstSelectedNodes).show(ev.getComponent(), ev.getX(), ev.getY());
			}			
		}
	}

	protected List<NodeActionContext<ConfigurationTreeNode<?>>> createNodeContext(final List<ConfigurationTreeNode<?>> lstNode) {
		final List<NodeActionContext<ConfigurationTreeNode<?>>> lstNodeContext = CollectionUtils.transform(lstNode, new Transformer<ConfigurationTreeNode<?>, NodeActionContext<ConfigurationTreeNode<?>>>() {

			@Override
			public NodeActionContext<ConfigurationTreeNode<?>> transform(
					ConfigurationTreeNode<?> node) {
				return createNodeContext(node);
			}

		});
		return lstNodeContext;
	}

	protected NodeActionContext<ConfigurationTreeNode<?>> createNodeContext(final ConfigurationTreeNode<?> node) {
		return new NodeActionContext<ConfigurationTreeNode<?>>() {

			@Override
			public ConfigurationTreeNode<?> getNode() {
				return node;
			}
			
			@Override
			public String toString() {
				return node.toString();
			}


		};
	}

	protected JPopupMenu buildPopup(final EnumSet<MenuAction> setMenuAction, final List<ConfigurationTreeNode<?>> lstNode) {
		final JPopupMenu popup = new JPopupMenu();

		for ( MenuAction action : setMenuAction) {
			switch(action) {
			case NEW_NODE:
				// allowed for single selection
				if (1 == lstNode.size()) {
					popup.add(ConfigurationExplorerNodeActions.newNodeAction(parent, createNodeContext(lstNode.iterator().next()),view));
				}
				break;
			case EDIT_NODE:
				// allowed for single selection
				if (1 == lstNode.size()) {
					popup.add(ConfigurationExplorerNodeActions.editNodeAction(parent, createNodeContext(lstNode.iterator().next()), view));
				}
				break;				
			case REMOVE_NODE:
				// allowed for multi selection
				popup.add(ConfigurationExplorerNodeActions.removeNodeAction(createNodeContext(lstNode), view));
				break;
			default:
			}
		}
		return popup;
	}



}
