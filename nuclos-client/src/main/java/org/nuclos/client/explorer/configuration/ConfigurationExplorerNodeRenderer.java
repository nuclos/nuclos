//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * ConfigurationExplorerNodeRenderer
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class ConfigurationExplorerNodeRenderer extends DefaultTreeCellRenderer {

	
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		// TODO Auto-generated method stub
		JLabel result = (JLabel)super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
				row, hasFocus);
		
		if (value instanceof ConfigurationTreeNode<?>) {
			
			// set entity icon
			ConfigurationTreeNode<?> cn = (ConfigurationTreeNode<?>)value;
			
			result.setIcon(cn.getIcon());
			
			// handle inactive tree paths
			if (value instanceof ConfigurationEntityTreeSubNode) {
				final ConfigurationEntityTreeSubNode node = (ConfigurationEntityTreeSubNode) value;
				ConfigurationTreeModel model = (ConfigurationTreeModel)tree.getModel();
				
				if (model.isMeOrAnyParentInactive(node)) {
					result.setForeground(Color.GRAY);
				}
				
				
				
			}
		}
		return result;
		
		
	}
}
