//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.EntityFacadeDelegate;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.ui.Icons;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;

/**
 * DefaultConfigurationNode
 *
 * @param <T> node type {@link ConfigurationTreeNode}
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public abstract class DefaultConfigurationNode<T> extends DefaultMutableTreeNode implements ConfigurationTreeNode<T> {

	private final T content;
	
	public DefaultConfigurationNode(final DefaultConfigurationNode<T> node) {
		this.content = node.getContent();
	}
	
	public DefaultConfigurationNode(final T content) {
		this.content = content;
	}
	
	@Override
	public T getContent() {
		return this.content;
	}

	@Override
	public String getLabel() {
		return getContent().toString();
	}
	
	@Override
	public Icon getIcon() {
		// fetch icon from entity
		final MetaProvider metaProvider = MetaProvider.getInstance();
		EntityMeta metaEntity = metaProvider.getEntity(getEntity());

		// for dynamic entities use icon of base entity

		if (metaEntity.isDynamic()) {
			final UID uidBaseEntity = EntityFacadeDelegate.getInstance().getBaseEntity(metaEntity.getDataSource());
			if (null != uidBaseEntity) {
				// entity for presentation found
				metaEntity = metaProvider.getEntity(uidBaseEntity);
			}

		}
		Icon icon = null;
		if (null != metaEntity) {
			UID uidResource = metaEntity.getResource();
			String nuclosResource = metaEntity.getNuclosResource();
			if(uidResource != null) {
				ImageIcon standardIcon = ResourceCache.getInstance().getIconResource(uidResource);
				icon = MainFrame.resizeAndCacheTabIcon(standardIcon);
			} else if (nuclosResource != null){
				ImageIcon nuclosIcon = NuclosResourceCache.getNuclosResourceIcon(nuclosResource);
				if (nuclosIcon != null) {
					icon = MainFrame.resizeAndCacheTabIcon(nuclosIcon);
				}
			}
		}
		if (null == icon) {
			// no icon found - use default
			icon = Icons.getInstance().getIconGenericObject16();
		}

		return icon;
	}

	@Override
	public String toString() {
		final ToStringBuilder b = new ToStringBuilder(this);
		b.append(getLabel()).append(getContent());
		return b.toString();
	}
	
	@Override
	public void insert(MutableTreeNode newChild, int childIndex) {
		super.insert(newChild, childIndex);
		// issues with DnD
		// Collections.sort(this.children);
	}

}
