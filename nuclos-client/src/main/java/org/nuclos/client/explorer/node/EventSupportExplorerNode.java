package org.nuclos.client.explorer.node;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JTree;

import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetType;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTreeNode;
import org.nuclos.client.rule.server.EventSupportActionHandler;
import org.nuclos.client.rule.server.EventSupportManagementController;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.ui.Icons;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.navigation.treenode.TreeNode;

public class EventSupportExplorerNode extends AbstractEventSupportExplorerNode {
	
	public EventSupportExplorerNode(TreeNode treenode) {
		super(treenode);
	}
	
	@Override
	public boolean isLeaf() {
		EventSupportTreeNode node = (EventSupportTreeNode)getTreeNode();
		return node.getSubNodes() == null || node.getSubNodes().isEmpty();
	}
	
	public String getPreferenceNodeName () {
		return EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_SOURCES;
	}
	
	/* (non-Javadoc)
	 * On the source side the default action on double-click is to op the eventsupport (if chosen and editable)
	 * in the editor
	 * @see org.nuclos.client.explorer.ExplorerNode#TreeNodeActionCommand(javax.swing.JTree)
	 */
	public String getDefaultTreeNodeActionCommand(JTree tree) {
		
		String retVal = null;
		
		final EventSupportExplorerNode currentNode = (EventSupportExplorerNode) tree.getSelectionPath().getLastPathComponent();
		final EventSupportTreeNode treeNode = currentNode.getTreeNode();
		
		if (EventSupportTargetType.EVENTSUPPORT.equals(treeNode.getTreeNodeType())) {
			retVal = EventSupportActionHandler.OPEN_EVENTSUPPORT;
		}
		return retVal;
	}
	
	@Override
	public Icon getIcon() {
		
		Icon result = null;
		EventSupportTreeNode node = ((EventSupportTreeNode) getUserObject());
		
		EventSupportTargetType treeNodeType = node.getTreeNodeType();
		
		if (treeNodeType == null)
			 return null;
		
		
		switch (treeNodeType) 
		{
		case NUCLET:
			result = Icons.getInstance().getEventSupportNucletIcon();
			break;
		case EVENTSUPPORT:			
			result = Icons.getInstance().getEventSupportRuleIcon();
			try {
				EventSupportSourceVO essVO = 
						EventSupportRepository.getInstance().getEventSupportByClassname(node.getNodeName());
				if (!essVO.isActive()) {
					result = Icons.getInstance().getEventSupportInactiveRuleIcon();
				}
				else {
					if (!node.isCompilable()) {
						result = Icons.getInstance().getEventSupportCodeCompileErrorIcon();	
					}
				}
			} catch (Exception e) {}
			break;
		case STATE_TRANSITION:
			result = Icons.getInstance().getEventSupportTransitionIcon();
			break;
		case ENTITY:
			result = Icons.getInstance().getEventSupportEntityIcon();
			break;
		case ENTITY_INTEGRATION_POINT:
			result = Icons.getInstance().getEventSupportEntityIntegrationPointIcon();
			break;
		default:
			break;
		}

		return result;
	}
	
	@Override
	public Action getTreeNodeActionOnMouseClick(JTree tree) {
		return new EventSupportShowPropertyAction(tree);
	}
	
	private class EventSupportShowPropertyAction extends AbstractAction
	{
		JTree tree;
		
		public EventSupportShowPropertyAction(JTree tree) {
			this.tree = tree;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// selected tree element			
			final EventSupportExplorerNode node= (EventSupportExplorerNode) tree.getSelectionPath().getLastPathComponent();
			// show infos and properties for this node
			EventSupportManagementController controller = node.getTreeNode().getController();
			controller.showSourceSupportProperties(node.getTreeNode());
		}
	}
	

}
