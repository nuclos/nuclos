package org.nuclos.client.explorer.node.eventsupport;

import org.apache.log4j.Logger;
import org.nuclos.client.rule.server.EventSupportManagementController;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

public class EventSupportTargetTreeNode extends EventSupportTreeNode {
	
	private static final Logger LOG = Logger.getLogger(EventSupportTargetTreeNode.class);

	/**
	 * for deserialization only
	 */
	protected EventSupportTargetTreeNode() {
		super();
	}

	public EventSupportTargetTreeNode(EventSupportManagementController ctrl,EventSupportTargetTreeNode parent, 
			Object id, String name, String label, String description,
			EventSupportTargetType type, boolean isRoot, boolean isLeaf, String sSearchString) {
		super(ctrl, parent, id, name, label, description, type, isRoot, isLeaf, sSearchString, true);
	}
	
	public EventSupportTargetTreeNode(EventSupportManagementController ctrl,EventSupportTargetTreeNode parent, 
			Object id, UID name, String label, String description,
			EventSupportTargetType type, boolean isRoot, boolean isLeaf, String sSearchString) {
		super(ctrl, parent, id, name, label, description, type, isRoot, isLeaf, sSearchString);
	}
	@Override
	public void refresh(){
		if (!isLeaf()) {
			try {
				setLstSubNodes(getController().createTargetSubNodesByType(this, getSearchString()));
			} catch (CommonFinderException e) {
				LOG.warn("refresh failed: " + e, e);
			} catch (CommonPermissionException e) {
				LOG.warn("refresh failed: " + e, e);
			}
		}
	}
	
}
