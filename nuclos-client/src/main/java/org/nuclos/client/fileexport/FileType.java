package org.nuclos.client.fileexport;

public enum FileType {

	XLSX("xlsx");

	private String fileExtension;

	private FileType(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public static FileType getFromFileExtension(String ext) {
		if (ext != null) {
			for (FileType result : FileType.class.getEnumConstants()) {
				if (ext.equals(result.getFileExtension())) {
					return result;
				}
			}
		}
		return null;
	}
}
