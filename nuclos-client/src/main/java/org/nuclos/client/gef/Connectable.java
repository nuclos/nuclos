//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.gef;

import java.awt.geom.Point2D;

import org.nuclos.client.gef.shapes.AbstractConnector;

/**
 * Connactable interface.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public interface Connectable {
	/**
	 *
	 */
	int CONNECTION_NW = 0;
	/**
	 *
	 */
	int CONNECTION_NNW = 1;
	/**
	 *
	 */
	int CONNECTION_N = 2;
	/**
	 *
	 */
	int CONNECTION_NNE = 3;
	/**
	 *
	 */
	int CONNECTION_NE = 4;
	/**
	 *
	 */
	int CONNECTION_ENE = 5;
	/**
	 *
	 */
	int CONNECTION_E = 6;
	/**
	 *
	 */
	int CONNECTION_ESE = 7;
	/**
	 *
	 */
	int CONNECTION_SE = 8;
	/**
	 *
	 */
	int CONNECTION_SSE = 9;
	/**
	 *
	 */
	int CONNECTION_S = 10;
	/**
	 *
	 */
	int CONNECTION_SSW = 11;
	/**
	 *
	 */
	int CONNECTION_SW = 12;
	/**
	 *
	 */
	int CONNECTION_WSW = 13;
	/**
	 *
	 */
	int CONNECTION_W = 14;
	/**
	 *
	 */
	int CONNECTION_WNW = 15;
	/**
	 *
	 */
	int CONNECTION_CENTER = 16;
	/**
	 *
	 */
	int CONNECTION_COUNT = 17;

	/**
	 *
	 * @return
	 */
	Point2D[] getConnectionPoints();

	/**
	 *
	 * @return
	 */
	double getConnectionSnapRadius();

	/**
	 *
	 * @param connectionSnapRadius
	 */
	void setConnectionSnapRadius(double connectionSnapRadius);

	/**
	 *
	 * @param index
	 * @return
	 */
	Point2D getConnectionPoint(int index);

	/**
	 *
	 * @return
	 */
	boolean isConnectable();

	/**
	 *
	 * @param pIn
	 * @param pOut
	 * @return
	 */
	int isInsideConnector(Point2D pIn, Point2D pOut);

	/**
	 *
	 * @param value
	 */
	void setConnectable(boolean value);

	/**
	 *
	 * @param connection
	 */
	void addConnector(AbstractConnector connection);

	/**
	 *
	 * @param connection
	 */
	void removeConnector(AbstractConnector connection);

}
