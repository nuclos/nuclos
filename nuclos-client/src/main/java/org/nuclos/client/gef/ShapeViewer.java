//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General License for more details.
//
//You should have received a copy of the GNU Affero General License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Shape viewer.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */

package org.nuclos.client.gef;

import java.awt.Color;
import java.awt.Graphics;

public interface ShapeViewer {
	/**
	 * Gets the current background color of the view
	 * @return
	 */
	Color getBgColor();

	/**
	 * Sets the background color of the view
	 * @param color
	 */
	void setBgColor(Color color);

	/**
	 * Paints the entire model
	 * @param gfx
	 */
	void paint(Graphics gfx);

	/**
	 * Prints the current selected model
	 * @param gfx
	 */
	void print(Graphics gfx);

	/**
	 * Prints the entire model
	 * @param gfx
	 */
	void printAll(Graphics gfx);

	/**
	 * Enables or disables user modifications
	 * @param editable
	 */
	void setEditable(boolean editable);

	/**
	 *
	 * @return
	 */
	boolean isEditable();

	/**
	 *
	 * @return
	 */
	ShapeModel getModel();

	/**
	 *
	 * @return
	 */
	int getWidth();

	/**
	 *
	 * @return
	 */
	int getHeight();

	/**
	 *
	 * @return
	 */
	AbstractController getController();

	/**
	 *
	 * @param newController
	 */
	void setController(AbstractController newController);

	/**
	 *
	 * @return
	 */
	double getZoom();

	/**
	 *
	 */
	void setZoom(double dValue);

	/**
	 *
	 * @return
	 */
	double getGridX();

	/**
	 *
	 * @param dGridX
	 */
	void setGridX(double dGridX);

	/**
	 *
	 * @return
	 */
	double getGridY();

	/**
	 *
	 * @param dGridY
	 */
	void setGridY(double dGridY);

	/**
	 *
	 * @return
	 */
	boolean isSnapGrid();

	/**
	 *
	 * @param bSnapGrid
	 */
	void setSnapGrid(boolean bSnapGrid);
}
