//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.generation;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.nuclos.api.rule.GenerateFinalRule;
import org.nuclos.api.rule.GenerateRule;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.rule.server.panel.EventSupportSelectionDataChangeListener;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;

/**
 * Controller for management of rules in object generation.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Uwe.Allner@novabit.de">Uwe Allner</a>
 * @version 01.00.00
 */
public class GenerationRulesController {

	private final GenerationRulePanel pnlGenerationRules;
	private final GenerationCollectController parentController;
	
	public GenerationRulesController(GenerationRulePanel pnlGenerationRules, MainFrameTab parent, final GenerationCollectController controller) {
		this.pnlGenerationRules = pnlGenerationRules;
		this.parentController = controller;
		
		this.pnlGenerationRules.addEventSupportSelectionDataChangeListener(
				new EventSupportSelectionDataChangeListener() {
					@Override
					public void attachedRuleDataChangeListenerEvent() {
						parentController.detailsChanged(parentController);
					}
				}
		);
	}

	/**
	 * get rule usages in there order
	 */
	public List<EventSupportGenerationVO> getRuleUsages() {
		final List<EventSupportGenerationVO> tblmodel = 
				this.pnlGenerationRules.getGenerationRuleModel().getEntries();
		
		tblmodel.addAll(this.pnlGenerationRules.getGenerationFinalRuleModel().getEntries());
		return tblmodel;
	}
	
	public void setRuleUsages(Collection<EventSupportGenerationVO> collRuleUsages) {
		
		Comparator<EventSupportGenerationVO> genComp = new Comparator<EventSupportGenerationVO>() {
			@Override
			public int compare(EventSupportGenerationVO o1,
					EventSupportGenerationVO o2) {
				return o1.getOrder().compareTo(o2.getOrder());
			}
		};
		
		final List<EventSupportGenerationVO> tblRulesmodel = 
				(List<EventSupportGenerationVO>) this.pnlGenerationRules.getGenerationRuleModel().getEntries();
		
		final List<EventSupportGenerationVO> tblFinalRulesmodel = 
				(List<EventSupportGenerationVO>) this.pnlGenerationRules.getGenerationFinalRuleModel().getEntries();
		
		this.pnlGenerationRules.getGenerationRuleModel().clear();
		this.pnlGenerationRules.getGenerationFinalRuleModel().clear();
		
		for (EventSupportGenerationVO esgVO : collRuleUsages) {
			if (GenerateRule.class.getCanonicalName().equals(esgVO.getEventSupportClassType())) {
				this.pnlGenerationRules.getGenerationRuleModel().addEntry(esgVO);
			}
			else if (GenerateFinalRule.class.getCanonicalName().equals(esgVO.getEventSupportClassType())) {
				this.pnlGenerationRules.getGenerationFinalRuleModel().addEntry(esgVO);
			}
		}
		Collections.sort(tblRulesmodel, genComp);
		Collections.sort(tblFinalRulesmodel, genComp);
	}


}
