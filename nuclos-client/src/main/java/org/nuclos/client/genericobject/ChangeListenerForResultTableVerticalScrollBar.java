//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.awt.Point;

import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.Logger;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.result.ResultPanel;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.genericobject.ProxyList;

/**
 * <code>ChangeListener</code> for the result table's vertical scrollbar.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class ChangeListenerForResultTableVerticalScrollBar<PK> implements ChangeListener {
	
	private static final Logger LOG = Logger.getLogger(ChangeListenerForResultTableVerticalScrollBar.class);

	private final DefaultBoundedRangeModel model;
	private final JViewport viewport;
	private final ProxyList<Object,?> proxylst;
	private final ResultPanel<PK, Collectable<PK>> resultpanel;
	private final MainFrameTab ifrm;

	/**
	 * lock to avoid recursion that might otherwise occur when there are more key down events in the event queue.
	 */
	private boolean bLock;

	public ChangeListenerForResultTableVerticalScrollBar(MainFrameTab aFrame, ResultPanel<PK, Collectable<PK>> aResultPanel, DefaultBoundedRangeModel model,
			JViewport vp, ProxyList<Object,?> proxylst) {
		this.ifrm = aFrame;
		this.resultpanel = aResultPanel;
		this.model = model;
		this.viewport = vp;
		this.proxylst = proxylst;
	}

	@Override
	public synchronized void stateChanged(ChangeEvent ev) {
		if (bLock) {
			LOG.trace("Ignoring stateChanged event because lock is already set.");
		}
		else if (model.getValueIsAdjusting()) {
			LOG.trace("Ignoring stateChanged event because valueIsAdjusting.");
		}
		else {
			try {
				// set lock to avoid unwanted recursion:
				bLock = true;

				LOG.trace("Knob released.");
				LOG.trace("model.getValue() = " + model.getValue());
				final JTable tbl = resultpanel.getResultTable();
				final int iMaxYBeforeMove = UIUtils.getMaxVisibleY(tbl);
				LOG.trace("iMaxYBeforeMove = " + iMaxYBeforeMove);
				final int iLastRowBeforeMove = TableUtils.getLastVisibleRow(tbl);
				LOG.trace("iLastRowBeforeMove = " + iLastRowBeforeMove);

				final Point p = viewport.getViewPosition();
				final int iShiftY = model.getValue() - p.y;
				final int iMaxYAfterMove = iMaxYBeforeMove + iShiftY;
				LOG.trace("iMaxYAfterMove = " + iMaxYAfterMove);
				final int iLastRowAfterMove = TableUtils.getLastVisibleRow(tbl, iMaxYAfterMove);
				LOG.trace("iLastRowAfterMove = " + iLastRowAfterMove);
				
				//NUCLOS-5302
				final boolean bOnlySequentialPaging = proxylst.isOnlySequentialPaging();
				final int iLastRow = bOnlySequentialPaging ? Math.min(iLastRowAfterMove, proxylst.getLastIndexRead() + 1) : iLastRowAfterMove;
				
				LOG.trace("getLastIndexRead() = " + proxylst.getLastIndexRead());
				if (iLastRow != -1  && !proxylst.hasObjectBeenReadForIndex(iLastRow)) {
					LOG.trace("*** NEED TO GET DATA!");
					
					if (iLastRow != iLastRowAfterMove && iLastRowAfterMove > iLastRowBeforeMove) {
						int realShiftY = (int)1L*iShiftY*(iLastRow - iLastRowBeforeMove)/(iLastRowAfterMove - iLastRowBeforeMove);
						model.setValue(p.y + realShiftY);						
					}

					class FetchSearchResultSwingWorker extends SwingWorker<Integer, Integer> {
						
						private final LayerLock lock;
						
						FetchSearchResultSwingWorker(LayerLock lock) {
							this.lock = lock;
						}
						
						@Override
						public Integer doInBackground() {
							try {
								LOG.trace("START FetchSearchResultSwingWorker");
								// since fetchDataIfNecessary never publishes intermediate results (progress)
								// to its ChangeListener, there is no need to install one
								proxylst.fetchDataIfNecessary(iLastRow, null);
								LOG.trace("FINISHED FetchSearchResultSwingWorker");
								return proxylst.getLastIndexRead();
							}
							catch (Exception ex) {
								LOG.error("FetchSearchResultSwingWorker failed: " + ex, ex);
							}
							return null;
						}
						
						@Override
						public void done() {
							ifrm.unlockLayer(lock);
							resultpanel.setStatusBar(proxylst.totalSize(false, null));
							ChangeListenerForResultTableVerticalScrollBar.this.setResultTableEnabled(true);							
							resultpanel.getResultTable().requestFocusInWindow();
							
							try {
								int iLastIndexRead = LangUtils.defaultIfNull(this.get(), 0);
								if (iLastIndexRead >= iLastRow) {
									// move the table:
									p.y = model.getValue();
									viewport.setViewPosition(p);
									LOG.trace("getLastVisibleRow(tbl) = " + TableUtils.getLastVisibleRow(tbl));
								}
								else {
									// move to the last read row:
									LOG.trace("*** MOVE TO THE LAST READ ROW: " + iLastIndexRead);
									model.setValue(getModelValue(tbl, iLastIndexRead));
								}
							} 
							catch(Exception e) {
								// an exception or error occured:
								// reset view position:
								LOG.info("Resetting vertical scrollbar because of " + e);
								model.setValue(0);

								final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
										"ChangeListenerForResultTableVerticalScrollBar.1", "Beim Nachladen von Datens\u00e4tzen ist ein Fehler ist aufgetreten. Die Datens\u00e4tze k\u00f6nnen nicht angezeigt werden.");
								Errors.getInstance().showExceptionDialog(ifrm, sMessage, e);
							}							
						}
					}

					final LayerLock lock = ifrm.lockLayerBusy();
					final FetchSearchResultSwingWorker worker = new FetchSearchResultSwingWorker(lock);
					this.setResultTableEnabled(false);
					worker.execute();
				}
				else {
					// move the table:
					p.y = model.getValue();
					viewport.setViewPosition(p);
					LOG.trace("getLastVisibleRow(tbl) = " + TableUtils.getLastVisibleRow(tbl));
				}
			}
			finally {
				bLock = false;
			}
		}
	}

	private static int getModelValue(final JTable tbl, final int iLastIndexRead) {
		final int iNewLastRow = iLastIndexRead;
		final int iNewMaxY = (iNewLastRow + 1) * tbl.getRowHeight() - 1;
		final int result = iNewMaxY - tbl.getVisibleRect().height;
		return result;
	}

	/**
	 * The dialog is not really modal. We have to disable the table and the vertical scrollbar to avoid
	 * recursion.
	 * @param bEnabled
	 */
	private void setResultTableEnabled(boolean bEnabled) {
		resultpanel.getResultTable().setEnabled(bEnabled);
		resultpanel.getResultTableScrollPane().getVerticalScrollBar().setEnabled(bEnabled);
	}

}	// class ChangeListenerForResultTableVerticalScrollBar
