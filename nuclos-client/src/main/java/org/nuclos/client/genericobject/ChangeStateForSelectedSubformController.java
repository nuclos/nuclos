package org.nuclos.client.genericobject;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ListSelectionModel;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.statemodel.StateWrapper;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.UpdateSelectedIdsController;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.slf4j.LoggerFactory;

/**
 * inner class ChangeStateForSelectedSubformController
 *
 */
public class ChangeStateForSelectedSubformController
extends MultiCollectablesActionController<Long,Long, Object> {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ChangeStateForSelectedSubformController.class);

	private static class ChangeStateAction extends UpdateSelectedIdsController.UpdateAction<Long, CollectableEntityObject<Long>, EntityCollectController<Long, CollectableEntityObject<Long>>> {
		private final EntityCollectController<Long, CollectableEntityObject<Long>> ctl;
		private final MasterDataSubFormController<?> mctl;
		private final List<UID> statesNew;

		private CommonBusinessException ex;

		ChangeStateAction(final MasterDataSubFormController<?> mctl, List<UID> statesNew, final Set<Long> pks) throws CommonBusinessException {
			super(RigidUtils.uncheckedCast(mctl.getCollectController()), pks);
			this.mctl = mctl;
			this.ctl = RigidUtils.uncheckedCast(mctl.getCollectController());
			this.statesNew = statesNew;
		}

		@Override
		protected boolean isResultMode() {
			return true;
		}

		@Override
		public Object perform(Long pk, final Map<String, Serializable> applyMultiEditContext)
				throws CommonBusinessException {
			CollectableEntityObject<Long> govo = getCollectable(pk);
			ChangeStateAction.this.ex = null;

			for (final UID stateNew : statesNew) {
				UIUtils.invokeOnDispatchThread(new Runnable() {
					@Override
					public void run() {
						try {
							ctl.invoke(new CommonRunnable() {
								@Override
								public void run() throws CommonBusinessException {
									final StateDelegate stateDelegate = StateDelegate.getInstance();
									stateDelegate.changeState(
											govo.getEntityUID(), govo.getId(), stateNew,
											false, ctl.getCustomUsage());
								}
							}, applyMultiEditContext);
						} catch (CommonBusinessException e) {
							ChangeStateForSelectedSubformController.ChangeStateAction.this.ex = e;
						}
					}
				});
				if (ChangeStateAction.this.ex != null) {
					throw ChangeStateAction.this.ex;
				}
			}

			return null;
		}

		@Override
		protected CollectableEntityObject<Long> findCollectable(final Long pk) throws CommonBusinessException {
			for (CollectableEntityObject<?> clct : mctl.getSelectedCollectables()) {
				if (clct.getId().equals(pk)) {
					return (CollectableEntityObject<Long>)clct;
				}
			}
			return null;
		}

		@Override
		public String getText(Long pk) {
			final CollectableEntityObject<Long> clctlo = getCollectable(pk);
			assert clctlo != null && ObjectUtils.equals(clctlo.getPrimaryKey(), pk);

			return SpringLocaleDelegate.getInstance().getMessage(
					"GenericObjectCollectController.86",
					"Statuswechsel f\u00fcr Datensatz {0}...",
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clctlo, ctl.getEntityName(),
							MetaProvider.getInstance(), ctl.getLanguage()));
		}

		@Override
		public String getSuccessfulMessage(final Long pk, Object oResult) {
			final CollectableEntityObject<Long> clctlo = getCollectable(pk);
			assert clctlo != null && ObjectUtils.equals(clctlo.getPrimaryKey(), pk);
			return SpringLocaleDelegate.getInstance().getMessage(
							"GenericObjectCollectController.87",
							"Statuswechsel f\u00fcr Datensatz {0} war erfolgreich.",
							SpringLocaleDelegate.getInstance().getIdentifierLabel(clctlo, ctl.getEntityName(),
									MetaProvider.getInstance(), ctl.getLanguage()));
		}

		@Override
		public String getConfirmStopMessage() {
			return SpringLocaleDelegate.getInstance().getMessage(
							"GenericObjectCollectController.101",
							"Wollen Sie den Statuswechsel f\u00fcr die ausgew\u00e4hlten Datens\u00e4tze an dieser Stelle beenden?\n(Die bisher ge\u00e4nderten Datens\u00e4tze bleiben in jedem Fall ge\u00e4ndert.)");
		}

		@Override
		public String getExceptionMessage(final Long pk, Exception ex) {
			final CollectableEntityObject<Long> clctlo = getCollectable(pk);
			assert clctlo != null && ObjectUtils.equals(clctlo.getPrimaryKey(), pk);
			final SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
			final String message = getExcMessage(ex);
			return sld.getMessage(
					"GenericObjectCollectController.89",
					"Statuswechsel ist fehlgeschlagen f\u00fcr Datensatz {0}. {1}",
					sld.getIdentifierLabel(clctlo, ctl.getEntityName(),
							MetaProvider.getInstance(), ctl.getLanguage()), message);
		}

		@Override
		public void executeFinalAction() throws CommonBusinessException {
			super.executeFinalAction();

			mctl.getCollectController().refreshCurrentCollectable();

			// FIXME this looks better than refreshCurrentCollectable, but main record becomes dirty
			// mctl.fillSubForm(ctl.getSelectedCollectableId());

			// TODO This works, but with refreshCurrentCollectable not necessary
			// selectResultsById(pks);

		}

		/**
		 * Selects rows in the result table based on the given entity IDs.
		 *
		 * @param ids
		 */
		private void selectResultsById(final Set<Long> ids) {
			SortableCollectableTableModel<Long, CollectableEntityObject<Long>> resultTableModel = RigidUtils.uncheckedCast(mctl.getCollectableTableModel());
			ListSelectionModel selectionModel = mctl.getJTable().getSelectionModel();
			for (int i=0, rowCount=resultTableModel.getRowCount(); i<rowCount; i++) {
				CollectableEntityObject<Long> clct = resultTableModel.getRow(i);
				for (Long pk: ids) {
					if (pk.equals(clct.getPrimaryKey())) {
						selectionModel.addSelectionInterval(i, i);
						break;
					}
				}
			}
		}
	}

	public ChangeStateForSelectedSubformController(MasterDataSubFormController<?> mctl, StateWrapper stateNew, final List<UID> statesNew, final Set<Long> pks) throws CommonBusinessException {
		super(RigidUtils.uncheckedCast(mctl.getCollectController()),
		SpringLocaleDelegate.getInstance().getMessage("GenericObjectCollectController.88","Statuswechsel in Status \"{0}\"",
		stateNew.getStatusText()),
		new ChangeStateAction(mctl, statesNew,pks),
		pks);
	}

}