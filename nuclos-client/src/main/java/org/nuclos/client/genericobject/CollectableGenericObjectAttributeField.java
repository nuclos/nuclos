//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.AbstractCollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.remoting.TypePreservingObjectDeserializer;
import org.nuclos.remoting.TypePreservingObjectSerializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * <code>DynamicAttributeVO</code> disguised as a <code>CollectableField</code>.
 * This class is immutable.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableGenericObjectAttributeField extends AbstractCollectableField {

	private final int iFieldType;
	@JsonSerialize(using = TypePreservingObjectSerializer.class)
	@JsonDeserialize(using = TypePreservingObjectDeserializer.class)
	private final Object oValue;
	private final Long valueId;
	private final UID valueUid;
	private final String formatOutput;

	/**
	 * for deserialization only
	 */
	protected CollectableGenericObjectAttributeField() {
		super();
		this.iFieldType = 0;
		this.oValue = null;
		this.valueId = null;
		this.valueUid = null;
		this.formatOutput = null;
	}

	public CollectableGenericObjectAttributeField(DynamicAttributeVO attrvo, int iFieldType, final String formatOutput) {
		this.iFieldType = iFieldType;
		this.oValue = attrvo.getValue();
		this.valueId = attrvo.getValueId();
		this.valueUid = attrvo.getValueUid();
		this.formatOutput = formatOutput;
	}

	@Override
	public int getFieldType() {
		return this.iFieldType;
	}

	@Override
	public Object getValue() {
		return this.oValue;
	}

	@Override
	public Object getValueId() throws UnsupportedOperationException {
		if (!this.isIdField()) {
			throw new UnsupportedOperationException("getValueId");
		}
		return this.valueId != null ? this.valueId : this.valueUid;
	}

	@Override
	public String toDescription() {
		final ToStringBuilder b = new ToStringBuilder(this).append(iFieldType).append(oValue);
		return b.toString();
	}

	@Override
	public String toString() {
		final Object oValue = this.getValue();
		return (oValue == null) ? "" : CollectableFieldFormat.getInstance(oValue.getClass()).format(formatOutput, oValue);
	}
}	// class CollectableGenericObjectAttributeField
