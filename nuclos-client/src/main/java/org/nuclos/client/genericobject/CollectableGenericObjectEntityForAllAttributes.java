//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.genericobject.CollectableGenericObjectEntityField;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.attribute.valueobject.AttributeCVO;

/**
 * Contains meta information about leased objects for all attributes.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableGenericObjectEntityForAllAttributes implements CollectableEntity {

	private UID entityUid = null;

	private String sEntityLabel = null;

	/**
	 * caches the dummmy entities - entitynames are keys
	 * @todo this is just a workaround - eliminate!
	 */
	private static Hashtable<String, CollectableGenericObjectEntityForAllAttributes> singletonHash = 
			new Hashtable<String, CollectableGenericObjectEntityForAllAttributes>();

	/**
	 * Map<String, CollectableEntityField> the virtual fields that reference the parent objects in submodules.
	 */
	private final Map<UID, CollectableEntityField> mpVirtualFields = CollectionUtils.newHashMap();

	/**
	 * Use getInstance() to get an instance of this class.
	 */
	protected CollectableGenericObjectEntityForAllAttributes(UID entityUid, String sEntityLabel) {
		this.sEntityLabel = sEntityLabel;
		this.entityUid = entityUid;

		Modules.getInstance().getModules();
	}

	/**
	 * @return the one and only instance of CollectableGenericObjectEntityForAllAttributes
	 */
	public static synchronized CollectableGenericObjectEntityForAllAttributes getInstance(UID entityUid) {
		return singletonHash.get(entityUid);
	}

	private AttributeCVO getAttributeCVO(UID attributeField) {
		return AttributeCache.getInstance().getAttribute(attributeField);
	}
	
	/**
	 * @return the artificial entity "generalsearch"
	 */
	@Override
	public UID getUID() {
		return this.entityUid;
		// This is not 100% clean, however needed, especially for layouts, where we don't know the module yet.
		// A layout can exist without being assigned to a module or being assigned to multiple modules.
		// An alternative would be to return null here, but that would require that getName() may return null
		// generally, and that doesn't look cleaner.
	}

	@Override
	public String getLabel() {
		return sEntityLabel;
	}

	@Override
	public CollectableEntityField getEntityField(UID fieldUid) {
		CollectableEntityField result = null;
		if (this.mpVirtualFields.containsKey(fieldUid)) {
			result = this.mpVirtualFields.get(fieldUid);
		} else {
			MetaProvider mdProv = MetaProvider.getInstance();
			FieldMeta<?> fMeta = mdProv.getEntityField(fieldUid);
			Map<UID, Permission> mpPermissions = null;
			if (fMeta.isCalcAttributeAllowCustomization()) {
				mpPermissions = getAttributeCVO(LangUtils.defaultIfNull(fMeta.getCalcBaseFieldUID(), fMeta.getUID())).getPermissions();
			} else {
				mpPermissions = getAttributeCVO(fieldUid).getPermissions();
			}
			result = new CollectableGenericObjectEntityField(
					mpPermissions,
					fMeta,
					this.entityUid);
		}
		result.setCollectableEntity(this);
		return result;
	}

	@Override
	public Set<UID> getFieldUIDs() {
		// attributes of all modules
		Set<UID> uids = new HashSet<UID>();
		final Collection<AttributeCVO> collattrcvo = 
				GenericObjectMetaDataCache.getInstance().getAttributeCVOsByModule(null, false);
		for (AttributeCVO attrcvo : collattrcvo) {
			uids.add(attrcvo.getId());
		}
	
		// additional system attributes
		uids.add(SF.CREATEDAT.getUID(this.entityUid));
		uids.add(SF.CREATEDBY.getUID(this.entityUid));
		uids.add(SF.CHANGEDAT.getUID(this.entityUid));
		uids.add(SF.CHANGEDBY.getUID(this.entityUid));
		
		return CollectionUtils.union(uids, this.mpVirtualFields.keySet());
	}

	@Override
	public int getFieldCount() {
		return getFieldUIDs().size();
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entityUid=").append(this.entityUid.getString());
		//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
		//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//				result.append(",label=").append(getIdentifierLabel());
		// result.append(",fields=").append(mpclctef);
		result.append("]");
		return result.toString();
	}

	@Override
	public EntityMeta<?> getMeta() {
		// TODO Auto-generated method stub
		return null;
	}

}	// class CollectableGenericObjectEntityForAllAttributes
