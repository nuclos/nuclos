package org.nuclos.client.genericobject.controller;

import java.awt.Component;
import java.util.Objects;

import javax.swing.Icon;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class RemovedTab {
	private final int idx;
	private final Component c;
	private final String title;
	private final String tooltip;
	private final Icon icon;
	private final String titleAttr;
	private final String visibilityAttr;

	public RemovedTab(int idx, Component c, String title, String tooltip, Icon icon, String titleAttr, String visibilityAttr) {
		this.idx = idx;
		this.c = c;
		this.title = title;
		this.tooltip = tooltip;
		this.icon = icon;
		this.titleAttr = titleAttr;
		this.visibilityAttr = visibilityAttr;
	}

	public int getIdx() {
		return idx;
	}

	public Component getC() {
		return c;
	}

	public String getTitle() {
		return title;
	}

	public String getTooltip() {
		return tooltip;
	}

	public Icon getIcon() {
		return icon;
	}

	public String getTitleAttr() { return titleAttr; }

	public String getVisibilityAttr() { return visibilityAttr; }

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final RemovedTab that = (RemovedTab) o;
		return Objects.equals(title, that.title) && Objects.equals(tooltip, that.tooltip) && Objects.equals(icon, that.icon) && Objects.equals(titleAttr, that.titleAttr) && Objects.equals(visibilityAttr, that.visibilityAttr);
	}

	@Override
	public int hashCode() {
		return Objects.hash(title, tooltip, icon, titleAttr, visibilityAttr);
	}

}
