//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Value list provider to get processes by usage.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">Christoph Radig</a>
 * @version 00.01.000
 */
public class MandatorCollectableFieldsProvider implements CollectableFieldsProvider {
	
	private static final Logger LOG = Logger.getLogger(MandatorCollectableFieldsProvider.class);

	public static final String ENTITY_UID = "entityUid";
	public static final String FIELD_UID = "fieldUid";
	
	//	
	public static final String NAME = "mandator"; // used in wysiwyg
	//
	
	private UID entityUid;
	private UID fieldUid;
	private UID levelUid;
	
	public MandatorCollectableFieldsProvider(UID entityUid, UID fieldUid) {
		if (entityUid != null) {
			setParameter(ENTITY_UID, entityUid);
		}
		if (fieldUid != null) {
			setParameter(FIELD_UID, fieldUid);
		}
	}

	/**
	 * valid parameters:
	 * <ul>
	 *   <li>"module" = name of module entity</li>
	 *   <li>"moduleId" = module id</li>
	 *   <li>"_searchmode" = collectable in search mask? 
	 *       (now org.nuclos.common.NuclosConstants.VLP_SEARCHMODE_PARAMETER)
	 * </ul>
	 * @param sName parameter name
	 * @param oValue parameter value
	 * 
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		//LOG.debug("setParameter - sName = " + sName + " - oValue = " + oValue);

		if (sName.equals(ENTITY_UID)) {
			try {
				if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
					entityUid = UID.parseUID((String) oValue);
				} else if (oValue instanceof UID) {
					entityUid = oValue == null ? null : (UID) oValue;
				}
				if (entityUid == null) {
					throw new IllegalArgumentException("oValue");
				}
				levelUid = MetaProvider.getInstance().getEntity(entityUid).getMandatorLevel();
			} catch (Exception ex) {
				throw new NuclosFatalException(ex);
			}
		} else if (sName.equals(FIELD_UID)) {
			try {
				if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
					fieldUid = UID.parseUID((String) oValue);
				} else if (oValue instanceof UID) {
					fieldUid = oValue == null ? null : (UID) oValue;
				}
				if (fieldUid == null) {
					throw new IllegalArgumentException("oValue");
				}
				entityUid = MetaProvider.getInstance().getEntityField(fieldUid).getEntity();
				levelUid = MetaProvider.getInstance().getEntity(entityUid).getMandatorLevel();
			} catch (Exception ex) {
				throw new NuclosFatalException(ex);
			}
		} else {
			// ignore
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		List<CollectableField> lstFields = new ArrayList<CollectableField>();
		for (MandatorVO mandator : SecurityCache.getInstance().getMandatorsByLevel(levelUid)) {
			lstFields.add(new CollectableValueIdField(mandator.getUID(), mandator.toString()));
		}
		return lstFields;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MandatorCollectableFieldsProvider) {
			MandatorCollectableFieldsProvider pcfp = (MandatorCollectableFieldsProvider) obj;
			return LangUtils.equal(entityUid, pcfp.entityUid);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hashCode(entityUid);
	}
}	// class MandatorCollectableFieldsProvider
