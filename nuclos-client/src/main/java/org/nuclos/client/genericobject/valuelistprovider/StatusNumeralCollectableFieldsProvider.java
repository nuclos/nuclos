//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Value list provider to get all states for the given module entity and process.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @version 00.01.000
 * @author    <a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 */
public class StatusNumeralCollectableFieldsProvider extends MultiStatusCollectableFieldsProvider {

	/**
	 * @deprecated
	 */
	public StatusNumeralCollectableFieldsProvider() {
	}

	public StatusNumeralCollectableFieldsProvider(UID entityWithStatemodelUid, UID processUid) {
		setParameter(MODULE_UID, entityWithStatemodelUid);
		setParameter(PROCESS_UID, processUid);
	}

	@Override
	public List<CollectableField> getCollectableStatusFields() {
		final Map<UID, StateVO> statusById = getStatusById();
		final List<CollectableField> result = new ArrayList<>();

		for (StateVO statevo: statusById.values()) {
			result.add(new CollectableValueIdField(
					statevo.getId(),
					statevo.getNumeral()
			));
		}

		Collections.sort(result);
		return result;
	}

}
