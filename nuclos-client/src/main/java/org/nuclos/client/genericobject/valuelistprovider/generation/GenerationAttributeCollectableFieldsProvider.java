//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider.generation;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.valuelistprovider.EntityFieldsCollectableFieldsProvider;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Value list provider to get attributes by source and target modules.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">Christoph Radig</a>
 * @version 00.01.000
 */
public class GenerationAttributeCollectableFieldsProvider implements CollectableFieldsProvider {
	
	private static final Logger LOG = Logger.getLogger(GenerationAttributeCollectableFieldsProvider.class);
	
	public static final String COLUMN = "column";
	
	public static final String SOURCE = "sourcemodule";
	
	public static final String TARGET = "targetmodule";
	
	public static final String SOURCE_ATTRIBUTE = "sourceattribute";
	
	public static final String TARGET_ATTRIBUTE = "targetattribute";
	
	public static final String SOURCE_TYPE = "sourceType";
	
	public static final String PARAMETER_ENTITY = "parameterEntity";
	
	public static final String VALUE_SOURCE_TYPE_PARAMETER = "parameter";
	
	public static final String VALUE_COLUMN_SOURCE = "source";
	
	public static final String VALUE_COLUMN_TARGET = "target";
	
	public static final String TARGET_MANDATOR_UID = "targetMandatorUid";
	
	//

	private String column;
	private UID iSourceModuleId;
	private UID iTargetModuleId;
	private UID iSourceAttributeId;
	private UID iTargetAttributeId;
	private String sourceType;
	private UID parameterEntityId;
	private UID targetMandatorUid;
	
	public GenerationAttributeCollectableFieldsProvider() {
	}

	/**
	 * valid parameters:
	 * <ul>
	 *   <li>"sourcemodule" = source module id</li>
	 *   <li>"targetmodule" = target module id</li>
	 *   <li>"sourceattribute" = source attribute id</li>
	 *   <li>"targetattribute" = target attribute id</li>
	 * </ul>
	 * @param sName parameter name
	 * @param oValue parameter value
	 * 
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (sName.equals(COLUMN)) {
			this.column = (String) oValue;
		} else if (sName.equals(SOURCE)) {
			this.iSourceModuleId = (UID) oValue;
		} else if (sName.equals(TARGET)) {
			this.iTargetModuleId =(UID) oValue;
		} else if (sName.equals(SOURCE_ATTRIBUTE)) {
			this.iSourceAttributeId = (UID) oValue;
		} else if (sName.equals(TARGET_ATTRIBUTE)) {
			this.iTargetAttributeId =(UID) oValue;
		} else if (sName.equals(SOURCE_TYPE)) {
			this.sourceType = (String) oValue;
		} else if (sName.equals(PARAMETER_ENTITY)) {
			this.parameterEntityId = (UID) oValue;
		} else if (sName.equals(TARGET_MANDATOR_UID)) {
			targetMandatorUid = (UID) oValue;
		}	
	}


	private List<FieldMeta<?>> getFields(UID entityId, UID restrictionEntityId, UID restrictionFieldId) {
		if (entityId == null) {
			return null;
		}
		MetaProvider provider = MetaProvider.getInstance();
		EntityMeta<?> entity = provider.getEntity(entityId);
		Map<UID, FieldMeta<?>> fields = MetaProvider.getInstance().getAllEntityFieldsByEntity(entity.getUID());

		final String restrictionDataType;
		if (restrictionEntityId != null && restrictionFieldId != null) {
			EntityMeta<?> restrictionEntity = provider.getEntity(restrictionEntityId);
			FieldMeta<?> restrictionField = provider.getEntityField(restrictionFieldId);
			restrictionDataType = restrictionField.getDataType();
		} else {
			restrictionDataType = null;
		}

		return CollectionUtils.applyFilter(fields.values(), new Predicate<FieldMeta<?>>() {
			@Override public boolean evaluate(FieldMeta<?> f) {
				if (SF.isEOField(f.getEntity(), f.getUID())) {
					return false;
				}
				if (f.getEntity() == null) {
					return false;					
				}
				if (restrictionDataType != null && !LangUtils.equal(restrictionDataType, f.getDataType())) {
					return false;					
				}
				return true;
			}
		});
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// LOG.debug("getCollectableFields");
		List<FieldMeta<?>> fields = null;
		UID realSourceId = VALUE_SOURCE_TYPE_PARAMETER.equals(sourceType) ? parameterEntityId : iSourceModuleId;
		if (VALUE_COLUMN_SOURCE.equals(column)) {
			fields = getFields(realSourceId, iTargetModuleId, iTargetAttributeId);
		} else if (VALUE_COLUMN_TARGET.equals(column)) {
			fields = getFields(iTargetModuleId, realSourceId, iSourceAttributeId);
		}
		if (fields == null) {
			return Collections.emptyList();
		}
		Collections.sort(fields, new Comparator<FieldMeta<?>>() {
			@Override
			public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {				
				return LangUtils.compare(o1.getFieldName(), o2.getFieldName());
			}
		});
		List<CollectableField> result = CollectionUtils.transform(fields, new Transformer<FieldMeta<?>, CollectableField>() {
			@Override public CollectableField transform(FieldMeta<?> f) {
				return new CollectableValueIdField(f.getUID(), f.getFieldName());
			}
		});
		result = EntityFieldsCollectableFieldsProvider.validateMandatorRestrictions(result, this.targetMandatorUid);
		return result;
	}

}	// class GenerationAttributeCollectableFieldsProvider
