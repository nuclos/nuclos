package org.nuclos.client.i18n.language.data;

import org.nuclos.common.UID;

public class DataLanguageContext {

	private static UID dataUserLanguage;
	private static UID dataSystemLanguage;
	
	public static UID getDataUserLanguage() {
		return dataUserLanguage;
	}
	
	public static void setDataUserLanguage(UID dataLang) {
		dataUserLanguage = dataLang;
	}
	
	public static UID getDataSystemLanguage() {
		return dataSystemLanguage;
	}
	
	public static void setDataSystemLanguage(UID dataLang) {
		dataSystemLanguage = dataLang;
	}
	
	public static UID getLanguageToUse() {
		return dataUserLanguage == null ? dataSystemLanguage : dataUserLanguage;
	}
	
}
