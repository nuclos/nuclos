package org.nuclos.client.i18n.language.data;

import javax.swing.Icon;

import org.nuclos.api.context.LayoutComponentContext;
import org.nuclos.api.ui.Alignment;
import org.nuclos.api.ui.annotation.NucletComponent;
import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.api.ui.layout.LayoutComponentFactory;
import org.nuclos.client.ui.Icons;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;

@NucletComponent
public class DataLanguageLayoutFactory implements
		LayoutComponentFactory<UID> {

	final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate
			.getInstance();

	@Override
	public LayoutComponent<UID> newInstance(LayoutComponentContext context) {
		return new DataLanguageController(context);
	}

	@Override
	public String getName() {
		return localeDelegate.getMessage("miDataLanguageManagement",
				"Datensprache");
	}

	@Override
	public Icon getIcon() {
		return Icons.getInstance().getDataLanguageIconBlue();
	}

	@Override
	public Alignment getDefaulAlignment() {
		return null;
	}

	@Override
	public Object getDefaultPropertyValue(String property) {
		return null;
	}

}
