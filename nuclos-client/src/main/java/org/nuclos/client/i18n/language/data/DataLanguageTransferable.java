package org.nuclos.client.i18n.language.data;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.nuclos.server.i18n.DataLanguageVO;

public class DataLanguageTransferable implements Transferable {

	public static final DataFlavor FLAVOR = new DataFlavor(DataLanguageVO.class, "DataLanguage");
	
	private List<DataLanguageVO> elements;
	
	public DataLanguageTransferable() {
		this.elements = new ArrayList<DataLanguageVO>();
	}
	
	public List<DataLanguageVO> getAllDataLanguageVO() {
		return this.elements;
	}
	
	public void addDataLanguage(DataLanguageVO dlvo) {
		this.elements.add(dlvo);
	}
	
	public DataLanguageTransferable(List<DataLanguageVO> elements) {
		this.elements = elements;
	}
	
	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[] {FLAVOR};
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return FLAVOR.equals(flavor);
	}

	@Override
	public Object getTransferData(DataFlavor flavor)
			throws UnsupportedFlavorException, IOException {
		return this.elements;
	}

}
