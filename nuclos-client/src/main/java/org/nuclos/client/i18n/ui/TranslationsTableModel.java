//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.i18n.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.collections15.map.LinkedMap;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * The {@link javax.swing.table.TableModel} used for displaying system entity translations.
 * 
 * @see TranslationsController
 * @author Thomas Pasch (javadoc)
 */
public class TranslationsTableModel extends AbstractTableModel {

	private final Map<UID, FieldMeta<?>> entityfields;
	
	private final LinkedMap<UID,String> field2Name;
	
	private List<TranslationVO> lstRows;

	public TranslationsTableModel(Map<UID, FieldMeta<?>> entityfields) {
		this.entityfields = entityfields;
		this.field2Name = new LinkedMap<UID,String>();
		for (FieldMeta<?> field : entityfields.values()) {
			if (field.isResourceField()) {
				field2Name.put(field.getUID(), field.getFieldName());
			}
		}
		lstRows = new ArrayList<TranslationVO>();
	}

	public void setRows(List<TranslationVO> rows) {
		lstRows = rows;
		this.fireTableDataChanged();
	}

	public List<TranslationVO> getRows() {
		return lstRows;
	}

	public TranslationVO getTranslationByName(String sName) {
		for (TranslationVO translation : lstRows) {
			if (translation.getLanguage().equals(sName))
				return translation;
		}
		return null;
	}

	@Override
	public int getColumnCount() {
		return field2Name.size() + 1;
	}

	@Override
	public int getRowCount() {
		return lstRows.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		final TranslationVO row = lstRows.get(rowIndex);
		switch (columnIndex) {
			case 0:
				return row.getCountry();
			default:
				final UID uid = field2Name.get(columnIndex - 1);
				return row.getLabels().get(field2Name.get(uid));
		}
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		final UID uid = field2Name.get(columnIndex - 1);
		lstRows.get(rowIndex).getLabels().put(field2Name.get(uid), (String) aValue);
		fireTableCellUpdated(rowIndex, columnIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex != 0;
	}

	@Override
	public String getColumnName(int column) {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		switch (column) {
			case 0:
				return localeDelegate.getMessage("wizard.step.entitytranslationstable.3", "Sprache");
			default:
				final UID uid = field2Name.get(column - 1);
				return localeDelegate.getText(entityfields.get(uid).getLocaleResourceIdForLabel());
		}
	}
}
