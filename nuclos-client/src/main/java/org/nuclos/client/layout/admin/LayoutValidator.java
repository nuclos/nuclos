package org.nuclos.client.layout.admin;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.xml.dtm.DTM;
import org.apache.xml.dtm.DTMIterator;
import org.apache.xml.dtm.ref.DTMNodeList;
import org.apache.xml.utils.XMLString;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.XMLUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class LayoutValidator {

	final String layoutMl;

	public LayoutValidator(String layoutMl) {
		this.layoutMl = layoutMl;
	}

	/**
	 * validate layout
	 *
	 * @throws NuclosBusinessException
	 */
	public void validate() throws NuclosBusinessException, CommonValidationException {

		try {
			if (layoutMl != null) {
				Document doc = getXmlDocument(layoutMl);
				uniqueNameCheck(doc, "splitpane");
				NodeList nodeListSubform = doc.getElementsByTagName("subform");
				for(int i = 0; i < nodeListSubform.getLength(); i++){
					Node subform = nodeListSubform.item(i);
					if(subform.getAttributes().getNamedItem("foreignkeyfield-to-parent") == null){
						throw new CommonValidationException(
								SpringLocaleDelegate.getInstance().getMessage("layout.validation.foreignkey.missing",
										"Jede Subform muss einen Fremdschlüssel besitzen."));
					}
				}
			}
		} catch (ParserConfigurationException | IOException | SAXException e) {
			throw new RuntimeException("Unable to parse layout.");
		}
	}

	private Document getXmlDocument(final String layoutMl) throws ParserConfigurationException, SAXException, IOException {
		if (layoutMl == null) {
			return null;
		}
		return XMLUtils.newDocument(layoutMl);
	}

	/**
	 * allow only XML elements with an unique name attribute
	 *
	 * @param doc
	 * @param elementName
	 * @throws NuclosBusinessException
	 */
	private void uniqueNameCheck(Document doc, String elementName) throws NuclosBusinessException {
		try {
			Set<String> duplicates = findDuplicates(getNameAttributeList(doc, elementName));
			if (duplicates.size() > 0) {
				throw new NuclosBusinessException(
						"Use unique names for these components: " + StringUtils.join(", ", duplicates)
				);
			}
		} catch (XPathExpressionException e) {
			throw new NuclosBusinessException("Unable to validate layout.", e);
		}
	}

	private List<String> getNameAttributeList(Document doc, String elementName) throws XPathExpressionException {
		XPath xpath = XPathFactory.newInstance().newXPath();
		DTMIterator it = ((DTMNodeList) xpath.compile("//" + elementName + "/@name").evaluate(doc, XPathConstants.NODESET)).getDTMIterator();
		List<String> result = new ArrayList<>();
		int pos;
		while (DTM.NULL != (pos = it.nextNode())) {
			DTM dtm = it.getDTM(pos);
			XMLString s = dtm.getStringValue(pos);
			result.add(s.toString());
		}
		return result;
	}


	private Set<String> findDuplicates(List<String> list) {
		return list
				.stream()
				.filter(a -> list.stream().filter(b -> b.equals(a)).count() > 1)
				.collect(Collectors.toSet());
	}
}
