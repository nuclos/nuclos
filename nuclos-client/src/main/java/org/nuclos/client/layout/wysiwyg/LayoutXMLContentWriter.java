package org.nuclos.client.layout.wysiwyg;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.nuclos.common2.XMLUtils;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LayoutXMLContentWriter implements LayoutMLConstants{

	private static final Logger LOG = Logger.getLogger(LayoutXMLContentWriter.class);

	private Document xmlDocument;
	
	public LayoutXMLContentWriter() {
		this.xmlDocument = null;
	}
	
	public synchronized void parse(String layoutAsString) throws ParserConfigurationException, SAXException, IOException{
		// Parse document
		this.xmlDocument = XMLUtils.newDocument(layoutAsString);
	}
	
	public NodeList getButtons() throws IllegalStateException{
		
		NodeList retVal = null;
		
		if (this.xmlDocument == null) 
			throw new IllegalStateException("XML Document is null");
		
		retVal = this.xmlDocument.getElementsByTagName(ELEMENT_BUTTON);
	
		return retVal;
	}
	
	public Node getAttribute(Node elm, String attribute)throws IllegalStateException{
		
		if (this.xmlDocument == null) 
			throw new IllegalStateException("XML Document is null");
		
		if (elm == null) 
			throw new IllegalStateException("Node is null");
		
		if (attribute == null) 
			throw new IllegalStateException("Attribute is null");
		
		if (!elm.hasAttributes())
			return null;
		
		return getAttributes(elm).getNamedItem(attribute);
	}
	
	public NamedNodeMap getAttributes(Node elm) throws IllegalStateException{
		if (this.xmlDocument == null) 
			throw new IllegalStateException("XML Document is null");
		
		return elm.getAttributes();
	}
	
	public void setAttribute(Node node, Node attribute) throws IllegalStateException {
		if (this.xmlDocument == null) 
			throw new IllegalStateException("XML Document is null");
		
		if (node == null) 
			throw new IllegalStateException("Node is null");
		
		if (attribute == null) 
			throw new IllegalStateException("New attribute is null");
		
		
		node.getAttributes().setNamedItem(attribute);
	}

	public Node getChild(Node parent, String nodeName) throws IllegalStateException {
		if (this.xmlDocument == null) 
			throw new IllegalStateException("XML Document is null");
		if (parent == null) 
			throw new IllegalStateException("Node is null");
		if (nodeName == null) 
			throw new IllegalStateException("NodeName is null");
		
		if (!parent.hasChildNodes())
			return null;
		
		NodeList childNodes = parent.getChildNodes();
		for (int idx=0; idx < childNodes.getLength(); idx++) {
			Node item = childNodes.item(idx);
			if(nodeName.equals(item.getNodeName())) {
				return item;
			}
		}
		return null;
		
	}
	
}
