package org.nuclos.client.layout.wysiwyg.component;

public class TabConfiguration {

	public TranslationMap tabTranslations;
	public String tabTitle;
	public String tabTitleAttribute;
	public String tabVisibilityAttribute;

	public TabConfiguration() {

	}

	public TabConfiguration(String title, String titleAttribute, String visibilityAttribute, TranslationMap tabTranslations) {
		initialize(title, titleAttribute, visibilityAttribute, tabTranslations);
	}

	public void initialize(TabConfiguration tabConfiguration) {
		initialize(tabConfiguration.tabTitle, tabConfiguration.tabTitleAttribute, tabConfiguration.tabVisibilityAttribute, tabConfiguration.tabTranslations);
	}

	public void initialize(String title, String titleAttribute, String visibilityAttribute, TranslationMap tabTranslations) {
		this.tabTitle = title;
		this.tabTitleAttribute = titleAttribute;
		this.tabVisibilityAttribute = visibilityAttribute;
		this.tabTranslations = tabTranslations;
	}

}
