package org.nuclos.client.layout.wysiwyg.component;

/**
 * An interface for layout components that support cloning.
 */
public interface WYSIWYGCloneableLayoutComponent extends WYSIWYGComponent {

	/**
	 * Creates and returns a copy of this component.
	 * <p>
	 * This method is used to create a new instance of the component
	 * that is a duplicate of the current one. The exact behavior of the
	 * clone operation may vary depending on the specific implementation.
	 * </p>
	 *
	 * @return a new {@link WYSIWYGComponent} that is a copy of this component.
	 */
	WYSIWYGCloneableLayoutComponent cloneLayoutComponent();

}
