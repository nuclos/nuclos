//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JMenuItem;

import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.component.properties.ComponentProperties;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue;
import org.nuclos.client.layout.wysiwyg.editor.util.TableLayoutUtil;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRules;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common2.LangUtils;

/**
 *
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @version 01.00.00
 */
public class WYSIWYGCollectableHTMLField extends WYSIWYGCollectableComponent {

	private WYSIWYGStaticHTMLField component = new WYSIWYGStaticHTMLField();

	private WYSIWYGMetaInformation meta;

	public WYSIWYGCollectableHTMLField(WYSIWYGMetaInformation meta) {
		this.meta = meta;
		propertyNames.add(PROPERTY_FONT);
		propertySetMethods.put(PROPERTY_TEXTCOLOR, new PropertySetMethod(PROPERTY_TEXTCOLOR, "setForeground"));
		propertySetMethods.put(PROPERTY_NAME, new PropertySetMethod(PROPERTY_NAME, "setName"));
		
		propertyFilters.put(PROPERTY_SHOWONLY, new PropertyFilter(PROPERTY_SHOWONLY, DISABLED));
		propertyFilters.put(PROPERTY_VALUELISTPROVIDER, new PropertyFilter(PROPERTY_VALUELISTPROVIDER, DISABLED));
		propertyFilters.put(PROPERTY_CUSTOM_USAGE_SEARCH, new PropertyFilter(PROPERTY_CUSTOM_USAGE_SEARCH, DISABLED));
		propertyFilters.put(PROPERTY_CONTROLTYPECLASS, new PropertyFilter(PROPERTY_CONTROLTYPECLASS, DISABLED));
		propertyFilters.put(PROPERTY_ROWS, new PropertyFilter(PROPERTY_ROWS, DISABLED));
		propertyFilters.put(PROPERTY_COLUMNS, new PropertyFilter(PROPERTY_COLUMNS, DISABLED));
		propertyFilters.put(PROPERTY_INSERTABLE, new PropertyFilter(PROPERTY_INSERTABLE, DISABLED));
		propertyFilters.put(PROPERTY_FILL_CONTROL_HORIZONTALLY, new PropertyFilter(PROPERTY_FILL_CONTROL_HORIZONTALLY, DISABLED));

		propertyFilters.put(PROPERTY_MULTIEDITABLE, new PropertyFilter(PROPERTY_MULTIEDITABLE, DISABLED));
		propertyFilters.put(PROPERTY_NOT_CLONEABLE, new PropertyFilter(PROPERTY_NOT_CLONEABLE, DISABLED));
		propertyFilters.put(PROPERTY_ENABLED_DYNAMIC, new PropertyFilter(PROPERTY_ENABLED_DYNAMIC, DISABLED));
		propertyFilters.put(PROPERTY_MNEMONIC, new PropertyFilter(PROPERTY_MNEMONIC, DISABLED));

		propertyFilters.put(PROPERTY_TEXTCOLOR, new PropertyFilter(PROPERTY_TEXTCOLOR, ENABLED));

		this.setLayout(new BorderLayout());
		this.add(component, BorderLayout.CENTER);
		this.addMouseListener();
		this.addDragGestureListener();    
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableComponent#getAdditionalContextMenuItems(int)
	 */
	@Override
	public List<JMenuItem> getAdditionalContextMenuItems(int xClick) {
		List<JMenuItem> list = new ArrayList<JMenuItem>();
		return list;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableComponent#render()
	 */
	@Override
	protected void render() {
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.JComponent#setFont(java.awt.Font)
	 */
	@Override
	public void setFont(Font font) {
		//FIX NUCLEUSINT-192
		if (this.component != null)
			this.component.setFont(font);
	}

	@Override
	public void setName(String name) {
		super.setName(name);
		if (this.component != null) {
			this.component.setName(name);
			this.component.setText(name);
		}
	}

	@Override
	public void setProperties(final ComponentProperties properties) {
		super.setProperties(properties);
		if (component != null) {
			component.setText((String) getProperties().getProperty(PROPERTY_NAME).getValue());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableComponent#validateProperties(java.util.Map)
	 */
	@Override
	public void validateProperties(Map<String, PropertyValue<Object>> values) throws NuclosBusinessException {
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableComponent#getLayoutMLRulesIfCapable()
	 */
	@Override
	public LayoutMLRules getLayoutMLRulesIfCapable() {
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.JComponent#setToolTipText(java.lang.String)
	 */
	@Override
	public void setToolTipText(String toolTipText) {
		if (component != null) {
			component.setToolTipText(toolTipText);
			this.addMouseListener();
			this.addDragGestureListener();
		}
	}

	@Override
	public void setForeground(Color foreground) {
		if (component != null) {
			component.setForeground(LangUtils.defaultIfNull(foreground, Color.BLACK));
		}
	}

	public WYSIWYGCollectableHTMLField cloneLayoutComponent() {
		WYSIWYGCollectableHTMLField clone = new WYSIWYGCollectableHTMLField(meta);
		TableLayoutUtil.copyComponentProperties(this, clone);
		return clone;
	}

}
