//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels;
import org.nuclos.client.layout.wysiwyg.component.properties.ComponentProperties;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertiesPanel;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueString;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.DnDUtil;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.TableLayoutPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGValuelistProvider;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRules;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;

/**
 *
 *
 *
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class WYSIWYGMultiselectionCombobox extends JComboBox<String> implements WYSIWYGComponent {

	public static final String PROPERTY_NAME = WYSIWYGStringsAndLabels.PROPERTY_LABELS.NAME;
	public static final String PROPERTY_ENTITY = WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY;
	public static final String PROPERTY_FOREIGNKEY = WYSIWYGStringsAndLabels.PROPERTY_LABELS.FOREIGNKEY;
	public static final String PROPERTY_LABEL_ATTRIBUTE = WYSIWYGStringsAndLabels.PROPERTY_LABELS.MULTISELECT_LABEL;
	public static final String PROPERTY_VALUELISTPROVIDER = WYSIWYGStringsAndLabels.PROPERTY_LABELS.VALUELISTPROVIDER;
	public static final String PROPERTY_DESCRIPTION = WYSIWYGStringsAndLabels.PROPERTY_LABELS.DESCRIPTION;

	public static final String[][] PROPERTIES_TO_LAYOUTML_ATTRIBUTES = new String[][]{
			{PROPERTY_NAME, ATTRIBUTE_NAME},
			{PROPERTY_ENTITY, ATTRIBUTE_ENTITY},
			{PROPERTY_FOREIGNKEY, ATTRIBUTE_FOREIGNKEYFIELDTOPARENT},
			{PROPERTY_LABEL_ATTRIBUTE, ATTRIBUTE_LABEL_ATTRIBUTE},
			{PROPERTY_ENABLED, ATTRIBUTE_ENABLED},
			{PROPERTY_DESCRIPTION_ATTRIBUTE, ATTRIBUTE_DESCRIPTION_ATTR},
	};

	public static final String[][] PROPERTY_VALUES_STATIC = new String[][]{{
			ATTRIBUTEVALUE_HORIZONTAL, ATTRIBUTEVALUE_VERTICAL, ATTRIBUTEVALUE_HIDE}};

	private static final String[] PROPERTY_NAMES = new String[]{
			PROPERTY_NAME,
			PROPERTY_FONT,
			PROPERTY_ENTITY,
			PROPERTY_FOREIGNKEY,
			PROPERTY_LABEL_ATTRIBUTE,
			PROPERTY_PREFFEREDSIZE,
			PROPERTY_ENABLED,
			PROPERTY_BACKGROUNDCOLOR,
			PROPERTY_BORDER,
			PROPERTY_VALUELISTPROVIDER,
			PROPERTY_DESCRIPTION,
			PROPERTY_DESCRIPTION_ATTRIBUTE,
	};

	private static final PropertyClass[] PROPERTY_CLASSES = new PropertyClass[]{
			new PropertyClass(PROPERTY_NAME, String.class),
			new PropertyClass(PROPERTY_ENTITY, String.class),
			new PropertyClass(PROPERTY_FOREIGNKEY, String.class),
			new PropertyClass(PROPERTY_LABEL_ATTRIBUTE, String.class),
			new PropertyClass(PROPERTY_PREFFEREDSIZE, Dimension.class),
			new PropertyClass(PROPERTY_ENABLED, boolean.class),
			new PropertyClass(PROPERTY_BACKGROUNDCOLOR, Color.class),
			new PropertyClass(PROPERTY_BORDER, Border.class),
			new PropertyClass(PROPERTY_FONT, Font.class),
			new PropertyClass(PROPERTY_VALUELISTPROVIDER, WYSIWYGValuelistProvider.class),
			new PropertyClass(PROPERTY_DESCRIPTION, String.class),
			new PropertyClass(PROPERTY_DESCRIPTION_ATTRIBUTE, String.class),
	};


	private static final PropertySetMethod[] PROPERTY_SETMETHODS = new PropertySetMethod[]{
			new PropertySetMethod(PROPERTY_NAME, "setName"),
			new PropertySetMethod(PROPERTY_ENTITY, "setEntity"),
			new PropertySetMethod(PROPERTY_FOREIGNKEY, "setForeignkey"),
			new PropertySetMethod(PROPERTY_LABEL_ATTRIBUTE, "setLabelAttribute"),
			new PropertySetMethod(PROPERTY_PREFFEREDSIZE, "setPreferredSize"),
			new PropertySetMethod(PROPERTY_ENABLED, "setEnabled"),
			new PropertySetMethod(PROPERTY_BACKGROUNDCOLOR, "setBackground"),
			new PropertySetMethod(PROPERTY_BORDER, "setBorder"),
			new PropertySetMethod(PROPERTY_FONT, "setFont"),
			new PropertySetMethod(PROPERTY_DESCRIPTION, "setToolTipText"),
	};

	private static PropertyFilter[] PROPERTY_FILTERS = new PropertyFilter[]{
			new PropertyFilter(PROPERTY_NAME, ENABLED),
			new PropertyFilter(PROPERTY_ENTITY, ENABLED),
			new PropertyFilter(PROPERTY_FOREIGNKEY, ENABLED),
			new PropertyFilter(PROPERTY_LABEL_ATTRIBUTE, ENABLED),
			new PropertyFilter(PROPERTY_PREFFEREDSIZE, ENABLED),
			new PropertyFilter(PROPERTY_ENABLED, ENABLED),
			new PropertyFilter(PROPERTY_BACKGROUNDCOLOR, ENABLED),
			new PropertyFilter(PROPERTY_BORDER, ENABLED),
			new PropertyFilter(PROPERTY_FONT, ENABLED),
			new PropertyFilter(PROPERTY_VALUELISTPROVIDER, ENABLED),
			new PropertyFilter(PROPERTY_DESCRIPTION, ENABLED),
			new PropertyFilter(PROPERTY_DESCRIPTION_ATTRIBUTE, ENABLED),
	};

	public static final String[][] PROPERTY_VALUES_FROM_METAINFORMATION = new String[][]{
			{PROPERTY_ENTITY, WYSIWYGMetaInformation.META_ENTITY_NAMES},
			{PROPERTY_FOREIGNKEY, WYSIWYGMetaInformation.META_ENTITY_FIELD_NAMES_REFERENCING},
			{PROPERTY_LABEL_ATTRIBUTE, WYSIWYGMetaInformation.META_MULTISELECT_ENTITY_FIELD_NAMES_REFERENCING},
			{PROPERTY_DESCRIPTION_ATTRIBUTE, WYSIWYGMetaInformation.META_FIELD_NAMES},
	};

	private ComponentProperties properties;
	protected LayoutMLRules componentsRules = new LayoutMLRules();

	public WYSIWYGMultiselectionCombobox() {
		setOpaque(false);
		setBackground(Color.white);
		DnDUtil.addDragGestureListener(this);
	}

	@Override
	public synchronized void addMouseListener(MouseListener l) {
		super.addMouseListener(l);

		Component[] comps = getComponents();

		for (int i = 0; i < comps.length; i++){
			comps[i].addMouseListener(l);
		}
	}

	/**
	 * This Method draws a small red box on the {@link WYSIWYGComponent} to indicate existing {@link LayoutMLRules}
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		if (this.componentsRules.getSize() > 0) {
			Graphics2D g2d = (Graphics2D)g;

			g2d.setColor(Color.RED);
			g2d.fillRect(this.getWidth() - 10, 0, 10, 10);
		}

		if (bSelected) {
			UIUtils.fillRectangleForWysiwyg(g, getSize());
		}
	}

	public UID getEntityUID() {
		return UID.parseUID((String) getProperties().getProperty(PROPERTY_ENTITY).getValue());
	}

	public List<FieldMeta<?>> getValueListProviderFields() {
		final List<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();
		WYSIWYGValuelistProvider value = (WYSIWYGValuelistProvider) getProperties().getProperty(PROPERTY_VALUELISTPROVIDER).getValue();
		UID displayAttributeUid = UID.parseUID((String) getProperties().getProperty(PROPERTY_LABEL_ATTRIBUTE).getValue());
		if (value != null && value.getType() != null) {
			result.add(MetaProvider.getInstance().getEntityField(displayAttributeUid));
			return result;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.Component#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		super.setName(name);
		removeAllItems();
		addItem(name);
		setSelectedItem(name);
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getProperties()
	 */
	@Override
	public ComponentProperties getProperties() {
		return properties;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#setProperties(org.nuclos.client.layout.wysiwyg.component.properties.ComponentProperties)
	 */
	@Override
	public void setProperties(ComponentProperties properties) {
		this.properties = properties;
		String name = (String) getProperties().getProperty(PROPERTY_NAME).getValue();
		removeAllItems();
		addItem(name);
		setSelectedItem(name);
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#setProperty(java.lang.String, org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue, java.lang.Class)
	 */
	@Override
	public void setProperty(String property, PropertyValue<?> value, Class<?> valueClass) throws CommonBusinessException {
		properties.setProperty(property, value, valueClass);
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyNames()
	 */
	@Override
	public String[] getPropertyNames() {
		return PROPERTY_NAMES;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertySetMethods()
	 */
	@Override
	public PropertySetMethod[] getPropertySetMethods() {
		return PROPERTY_SETMETHODS;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyClasses()
	 */
	@Override
	public PropertyClass[] getPropertyClasses() {
		return PROPERTY_CLASSES;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getParentEditor()
	 */
	@Override
	public WYSIWYGLayoutEditorPanel getParentEditor() {
		if (super.getParent() instanceof TableLayoutPanel) {
			return (WYSIWYGLayoutEditorPanel) super.getParent().getParent();
		}

		throw new CommonFatalException(WYSIWYGStringsAndLabels.ERROR_MESSAGES.PARENT_NO_WYSIWYG);
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyAttributeLink()
	 */
	@Override
	public String[][] getPropertyAttributeLink() {
		return PROPERTIES_TO_LAYOUTML_ATTRIBUTES;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getAdditionalContextMenuItems(int)
	 */
	@Override
	public List<JMenuItem> getAdditionalContextMenuItems(final int xClick) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyValuesFromMetaInformation()
	 */
	@Override
	public String[][] getPropertyValuesFromMetaInformation() {
		return PROPERTY_VALUES_FROM_METAINFORMATION;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyValuesStatic()
	 */
	@Override
	public String[][] getPropertyValuesStatic() {
		return PROPERTY_VALUES_STATIC;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#validateProperties(java.util.Map)
	 */
	@Override
	public void validateProperties(Map<String, PropertyValue<Object>> values) throws NuclosBusinessException {
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyFilters()
	 */
	@Override
	public PropertyFilter[] getPropertyFilters() {
		return PROPERTY_FILTERS;
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.Component#removeMouseListener(java.awt.event.MouseListener)
	 */
	@Override
	public synchronized void removeMouseListener(MouseListener l) {
		listenerList.remove(MouseListener.class, l);
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getLayoutMLRulesIfCapable()
	 */
	@Override
	public LayoutMLRules getLayoutMLRulesIfCapable() {
		return componentsRules;
	}

	private void pickSolelyAvailableForeignKey() {
		UID entity = UID.parseUID((String) getProperties().getProperty(PROPERTY_ENTITY).getValue());
		if (entity != null && getProperties() != null) {
			entity = MetaProvider.getInstance().getEntity(entity).getUID();
			for (String[] valueFromMeta : this.getPropertyValuesFromMetaInformation()){
				if (valueFromMeta[0].equals(PROPERTY_FOREIGNKEY)){
					final List<WYSIWYGMetaInformation.StringResourceIdPair> values = this.getProperties().getMetaInformation().getListOfMetaValues(this, valueFromMeta, entity);
					if(values.size() == 1) {
						//There is only one foreign key to choose from, so we select that one automatically
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								((PropertyValueString) getProperties().getProperty(PROPERTY_FOREIGNKEY)).setValue(values.get(0).getX());
								PropertiesPanel.showPropertiesForComponent(WYSIWYGMultiselectionCombobox.this, getParentEditor().getTableLayoutUtil());
								pickLabelAttribute();
							}});
					}
					else {
						//There is multiple foreign keys to choose from, so we select none.
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								(getProperties().getProperty(PROPERTY_FOREIGNKEY)).setValue(null);
								PropertiesPanel.showPropertiesForComponent(WYSIWYGMultiselectionCombobox.this, getParentEditor().getTableLayoutUtil());
								pickLabelAttribute();
							}
						});
					}
				}
			}
		}
	}

	private void pickLabelAttribute() {
		UID entity = UID.parseUID((String) getProperties().getProperty(PROPERTY_ENTITY).getValue());
		UID foreignKey = UID.parseUID((String) getProperties().getProperty(PROPERTY_FOREIGNKEY).getValue());
		if (entity != null && foreignKey != null && getProperties() != null) {
			for (String[] valueFromMeta : this.getPropertyValuesFromMetaInformation()){
				if (valueFromMeta[0].equals(PROPERTY_LABEL_ATTRIBUTE)){
					final List<WYSIWYGMetaInformation.StringResourceIdPair> values = this.getProperties().getMetaInformation().getMultiselectLabelReferences(this);
					if(values.size() == 1) {
						//There is only one foreign key to choose from, so we select that one automatically
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								((PropertyValueString) getProperties().getProperty(PROPERTY_LABEL_ATTRIBUTE)).setValue(values.get(0).getX());
								PropertiesPanel.showPropertiesForComponent(WYSIWYGMultiselectionCombobox.this, getParentEditor().getTableLayoutUtil());
							}});
					}
					else {
						//There is multiple foreign keys to choose from, so we select none.
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								(getProperties().getProperty(PROPERTY_LABEL_ATTRIBUTE)).setValue(null);
								PropertiesPanel.showPropertiesForComponent(WYSIWYGMultiselectionCombobox.this, getParentEditor().getTableLayoutUtil());
							}
						});
					}
				}
			}
		}
	}

	public void setEntity(String entityName) {
		if (!StringUtils.isNullOrEmpty(entityName)) {
			String name = MetaProvider.getInstance().getEntity(UID.parseUID(entityName)).getEntityName();
			setName(name);
		}
		pickSolelyAvailableForeignKey();
	}

	public void setForeignkey(String foreignkey) {

	}

	public void setLabelAttribute(String labelAttribute) {

	}

	private boolean bSelected;
	public boolean isSelected() {
		return bSelected;
	}
	public void setSelected(boolean bSelected) {
		this.bSelected = bSelected;
	}
}
