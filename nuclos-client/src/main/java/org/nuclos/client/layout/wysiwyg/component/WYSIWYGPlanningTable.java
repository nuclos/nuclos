//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuItem;
import javax.swing.border.Border;
import javax.swing.event.EventListenerList;

import org.apache.log4j.Logger;
import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.ERROR_MESSAGES;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.PROPERTY_LABELS;
import org.nuclos.client.layout.wysiwyg.component.properties.ComponentProperties;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.TableLayoutPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRules;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.ToolTipsTableHeader;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;

/**
 *
 *
 *
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class WYSIWYGPlanningTable extends JLayeredPane implements WYSIWYGComponent, MouseListener, WYSIWYGScriptComponent {

	private static final Logger LOG = Logger.getLogger(WYSIWYGPlanningTable.class);

	private boolean isInitialLoading = true;

	public static final String PROPERTY_NAME = PROPERTY_LABELS.NAME;
	public static final String PROPERTY_PLANNINGTABLE = PROPERTY_LABELS.PLANNINGTABLE;
	public static final String PROPERTY_RESOURCE_FOREIGNKEY = PROPERTY_LABELS.RESOURCE_FOREIGNKEY;
	public static final String PROPERTY_DATE_FROM = PROPERTY_LABELS.DATE_FROM;
	public static final String PROPERTY_DATE_UNTIL = PROPERTY_LABELS.DATE_UNTIL;
	public static final String PROPERTY_FIELD_DATE_FROM = PROPERTY_LABELS.FIELD_DATE_FROM;
	public static final String PROPERTY_FIELD_DATE_UNTIL = PROPERTY_LABELS.FIELD_DATE_UNTIL;

	public static final String[][] PROPERTIES_TO_LAYOUTML_ATTRIBUTES = new String[][]{
			{PROPERTY_NAME, ATTRIBUTE_NAME},
			{PROPERTY_PLANNINGTABLE, ATTRIBUTE_PLANNINGTABLE},
			{PROPERTY_RESOURCE_FOREIGNKEY, ATTRIBUTE_FOREIGNKEYFIELDTOPARENT},
			{PROPERTY_ENABLED, ATTRIBUTE_ENABLED},
			{PROPERTY_DATE_FROM, ATTRIBUTE_DATE_FROM},
			{PROPERTY_DATE_UNTIL, ATTRIBUTE_DATE_UNTIL},
			{PROPERTY_FIELD_DATE_FROM, ATTRIBUTE_FIELD_DATE_FROM},
			{PROPERTY_FIELD_DATE_UNTIL, ATTRIBUTE_FIELD_DATE_UNTIL}
	};

	public static final String[][] PROPERTY_VALUES_STATIC = new String[][]{{
			ATTRIBUTEVALUE_HORIZONTAL, ATTRIBUTEVALUE_VERTICAL, ATTRIBUTEVALUE_HIDE}};

	private static final String[] PROPERTY_NAMES = new String[]{
			PROPERTY_NAME,
			PROPERTY_FONT,
			PROPERTY_PLANNINGTABLE,
			PROPERTY_RESOURCE_FOREIGNKEY,
			PROPERTY_DATE_FROM,
			PROPERTY_DATE_UNTIL,
			PROPERTY_FIELD_DATE_FROM,
			PROPERTY_FIELD_DATE_UNTIL,
			PROPERTY_PREFFEREDSIZE,
			PROPERTY_ENABLED,
			PROPERTY_BACKGROUNDCOLOR,
			PROPERTY_BORDER
	};

	private static final PropertyClass[] PROPERTY_CLASSES = new PropertyClass[]{
			new PropertyClass(PROPERTY_NAME, String.class),
			new PropertyClass(PROPERTY_PLANNINGTABLE, String.class),
			new PropertyClass(PROPERTY_RESOURCE_FOREIGNKEY, String.class),
			new PropertyClass(PROPERTY_DATE_FROM, String.class),
			new PropertyClass(PROPERTY_DATE_UNTIL, String.class),
			new PropertyClass(PROPERTY_FIELD_DATE_FROM, String.class),
			new PropertyClass(PROPERTY_FIELD_DATE_UNTIL, String.class),
			new PropertyClass(PROPERTY_PREFFEREDSIZE, Dimension.class),
			new PropertyClass(PROPERTY_ENABLED, boolean.class),
			new PropertyClass(PROPERTY_BACKGROUNDCOLOR, Color.class),
			new PropertyClass(PROPERTY_BORDER, Border.class),
			new PropertyClass(PROPERTY_FONT, Font.class)};


	private static final PropertySetMethod[] PROPERTY_SETMETHODS = new PropertySetMethod[]{
			new PropertySetMethod(PROPERTY_NAME, "setName"),
			new PropertySetMethod(PROPERTY_PLANNINGTABLE, "setPlanningTable"),
			new PropertySetMethod(PROPERTY_RESOURCE_FOREIGNKEY, "setResourceForeignKey"),
			new PropertySetMethod(PROPERTY_DATE_FROM, "setDateFrom"),
			new PropertySetMethod(PROPERTY_DATE_UNTIL, "setDateUntil"),
			new PropertySetMethod(PROPERTY_FIELD_DATE_FROM, "setFieldDateFrom"),
			new PropertySetMethod(PROPERTY_FIELD_DATE_UNTIL, "setFieldDateUntil"),
			new PropertySetMethod(PROPERTY_PREFFEREDSIZE, "setPreferredSize"),
			new PropertySetMethod(PROPERTY_ENABLED, "setEnabled"),
			new PropertySetMethod(PROPERTY_BACKGROUNDCOLOR, "setBackground"),
			new PropertySetMethod(PROPERTY_BORDER, "setBorder"),
			new PropertySetMethod(PROPERTY_FONT, "setFont"),
			new PropertySetMethod(PROPERTY_DESCRIPTION, "setToolTipText"),
	};

	private static PropertyFilter[] PROPERTY_FILTERS = new PropertyFilter[]{
			new PropertyFilter(PROPERTY_NAME, ENABLED),
			new PropertyFilter(PROPERTY_PLANNINGTABLE, ENABLED),
			new PropertyFilter(PROPERTY_RESOURCE_FOREIGNKEY, ENABLED),
			new PropertyFilter(PROPERTY_DATE_FROM, ENABLED),
			new PropertyFilter(PROPERTY_DATE_UNTIL, ENABLED),
			new PropertyFilter(PROPERTY_FIELD_DATE_FROM, ENABLED),
			new PropertyFilter(PROPERTY_FIELD_DATE_UNTIL, ENABLED),
			new PropertyFilter(PROPERTY_PREFFEREDSIZE, ENABLED),
			new PropertyFilter(PROPERTY_ENABLED, ENABLED),
			new PropertyFilter(PROPERTY_BACKGROUNDCOLOR, ENABLED),
			new PropertyFilter(PROPERTY_BORDER, ENABLED),
			new PropertyFilter(PROPERTY_FONT, ENABLED),
	};

	public static final String[][] PROPERTY_VALUES_FROM_METAINFORMATION = new String[][]{{PROPERTY_PLANNINGTABLE, WYSIWYGMetaInformation.META_PLANNINGTABLE_NAMES}, {PROPERTY_RESOURCE_FOREIGNKEY, WYSIWYGMetaInformation.META_ENTITY_FIELD_NAMES_REFERENCING}, {PROPERTY_FIELD_DATE_FROM, WYSIWYGMetaInformation.META_DATE_FIELD_NAMES}, {PROPERTY_FIELD_DATE_UNTIL, WYSIWYGMetaInformation.META_DATE_FIELD_NAMES}};

	public static final String[][] PROPERTIES_TO_SCRIPT_ELEMENTS = new String[][]{
	};

	private ComponentProperties properties;

	private LayoutMLRules subformColumnRules = new LayoutMLRules();

	private JLabel message;

	private EventListenerList listenerList = new EventListenerList();

	public WYSIWYGPlanningTable() {
		this.message = new JLabel(WYSIWYGStringsAndLabels.COLLECTABLE_PLANNINGTABLE.LABEL_NO_ENTITY_ASSIGNED);
		this.message.setToolTipText(WYSIWYGStringsAndLabels.COLLECTABLE_PLANNINGTABLE.LABEL_NO_ENTITY_ASSIGNED);
		this.message.setHorizontalAlignment(JLabel.CENTER);
		this.message.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.message.addMouseListener(this);

		this.setLayout(new BorderLayout());
		this.add(message);
	}

	/**
	 *
	 * @return the entity the subform is related to
	 */
	public UID getEntityUID() {
		return UID.parseUID((String) getProperties().getProperty(PROPERTY_PLANNINGTABLE).getValue());
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getProperties()
	 */
	@Override
	public ComponentProperties getProperties() {
		return properties;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#setProperties(org.nuclos.client.layout.wysiwyg.component.properties.ComponentProperties)
	 */
	@Override
	public void setProperties(ComponentProperties properties) {
		this.properties = properties;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#setProperty(java.lang.String, org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue, java.lang.Class)
	 */
	@Override
	public void setProperty(String property, PropertyValue<?> value, Class<?> valueClass) throws CommonBusinessException {
		properties.setProperty(property, value, valueClass);
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyNames()
	 */
	@Override
	public String[] getPropertyNames() {
		return PROPERTY_NAMES;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertySetMethods()
	 */
	@Override
	public PropertySetMethod[] getPropertySetMethods() {
		return PROPERTY_SETMETHODS;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyClasses()
	 */
	@Override
	public PropertyClass[] getPropertyClasses() {
		return PROPERTY_CLASSES;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getParentEditor()
	 */
	@Override
	public WYSIWYGLayoutEditorPanel getParentEditor() {
		if (super.getParent() instanceof TableLayoutPanel) {
			return (WYSIWYGLayoutEditorPanel) super.getParent().getParent();
		}

		throw new CommonFatalException(ERROR_MESSAGES.PARENT_NO_WYSIWYG);
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyAttributeLink()
	 */
	@Override
	public String[][] getPropertyAttributeLink() {
		return PROPERTIES_TO_LAYOUTML_ATTRIBUTES;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getAdditionalContextMenuItems(int)
	 */
	@Override
	public List<JMenuItem> getAdditionalContextMenuItems(final int xClick) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyValuesFromMetaInformation()
	 */
	@Override
	public String[][] getPropertyValuesFromMetaInformation() {
		return PROPERTY_VALUES_FROM_METAINFORMATION;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyValuesStatic()
	 */
	@Override
	public String[][] getPropertyValuesStatic() {
		return PROPERTY_VALUES_STATIC;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#validateProperties(java.util.Map)
	 */
	@Override
	public void validateProperties(Map<String, PropertyValue<Object>> values) throws NuclosBusinessException {
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getPropertyFilters()
	 */
	@Override
	public PropertyFilter[] getPropertyFilters() {
		return PROPERTY_FILTERS;
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.Component#addMouseListener(java.awt.event.MouseListener)
	 */
	@Override
	public synchronized void addMouseListener(MouseListener l) {
		listenerList.add(MouseListener.class, l);
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.Component#removeMouseListener(java.awt.event.MouseListener)
	 */
	@Override
	public synchronized void removeMouseListener(MouseListener l) {
		listenerList.remove(MouseListener.class, l);
	}

	public void setPlanningTable(String planningTable) {
		if (planningTable != null) {
			this.message.setText(null);
			this.message.setToolTipText(null);
			this.message.setIcon(NuclosResourceCache.getNuclosResourceIcon("org.nuclos.client.resource.icon.glyphish-blue.83-calendar.png"));
		} else {
			this.message.setText(WYSIWYGStringsAndLabels.COLLECTABLE_PLANNINGTABLE.LABEL_NO_ENTITY_ASSIGNED);
			this.message.setToolTipText(WYSIWYGStringsAndLabels.COLLECTABLE_PLANNINGTABLE.LABEL_NO_ENTITY_ASSIGNED);
			this.message.setIcon(null);
		}
	}

	public void setResourceForeignKey(String key) {

	}

	public void setDateFrom(String dateFrom) {
		if (this.properties != null) {
			((PropertyValue<String>) this.properties.getProperty(PROPERTY_DATE_FROM)).setValue(dateFrom);
		}
	}

	public void setDateUntil(String dateUntil) {
		if (this.properties != null) {
			((PropertyValue<String>) this.properties.getProperty(PROPERTY_DATE_UNTIL)).setValue(dateUntil);
		}
	}

	public void setFieldDateFrom(String dateFrom) {

	}

	public void setFieldDateUntil(String dateUntil) {

	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	@Override
	public synchronized void mouseClicked(MouseEvent e) {
		for(MouseListener l : listenerList.getListeners(MouseListener.class)) {
			// NUCLEUSINT-556
			if(e.getComponent() instanceof ToolTipsTableHeader) {

			}
			else
				l.mouseClicked(e);

		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		for (MouseListener l : listenerList.getListeners(MouseListener.class)) {
			l.mouseEntered(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		for (MouseListener l : listenerList.getListeners(MouseListener.class)) {
			l.mouseExited(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		for (MouseListener l : listenerList.getListeners(MouseListener.class)) {
			l.mousePressed(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		for (MouseListener l : listenerList.getListeners(MouseListener.class)) {
			l.mouseReleased(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent#getLayoutMLRulesIfCapable()
	 */
	@Override
	public LayoutMLRules getLayoutMLRulesIfCapable() {
		return subformColumnRules;
	}

	/**
	 * This Method draws a small red box on the Component to indicate existing {@link LayoutMLRules}
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		renderGrayedOutIfNotEnabled((Graphics2D) g);

		//Note: this i a replace for false, as this block is essentially commented out
		if (getAlignmentX() == 4.711) {
			UIUtils.fillRectangleForWysiwyg(g, getSize());
		}
	}

	/**
	 * Renders a black rectangle with some transparency over the whole subform, if it is not enabled.
	 * @param g2d
	 */
	private void renderGrayedOutIfNotEnabled(Graphics2D g2d) {
		if(!(Boolean) getProperties().getProperty(PROPERTY_ENABLED).getValue()) {
			final Composite compositeOriginal = g2d.getComposite();
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f));
			g2d.setColor(Color.black);
			g2d.fillRect(0, 0, getWidth(), getHeight());
			g2d.setComposite(compositeOriginal);
		}
	}

	public boolean isInitialLoading() {
		return isInitialLoading;
	}

	public void finalizeInitialLoading() {
		this.isInitialLoading = false;
	}

	@Override
	public String[][] getPropertyScriptElementLink() {
		return PROPERTIES_TO_SCRIPT_ELEMENTS;
	}

	private boolean bSelected;
	public boolean isSelected() {
		return bSelected;
	}
	public void setSelected(boolean bSelected) {
		this.bSelected = bSelected;
	}
}
