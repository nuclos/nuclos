package org.nuclos.client.layout.wysiwyg.component.properties;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Objects;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGMetaInformationPicker;
import org.nuclos.client.layout.wysiwyg.editor.util.InterfaceGuidelines;
import org.nuclos.common.FieldMeta;
import org.nuclos.common2.LangUtils;
import org.xml.sax.Attributes;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

public class PropertyValueEntityField implements PropertyValue<String> {

	private String value;

	public PropertyValueEntityField() {

	}

	public PropertyValueEntityField(String value) {
		this.value = value;
	}

	@Override
	public void setValue(final String value) {
		this.value = value;
	}

	@Override
	public String getValue() {
		return this.value;
	}

	@Override
	public void setValue(final String attributeName, final Attributes attributes) {
		this.value = attributes.getValue(attributeName);
	}

	@Override
	public Object getValue(final Class<?> cls, final WYSIWYGComponent c) {
		if (cls != null && cls.equals(String.class)) {
			return value;
		}
		else {
			return null;
		}
	}

	@Override
	public TableCellEditor getTableCellEditor(final WYSIWYGComponent c, final String property, final PropertiesPanel dialog) {
		return new PropertyEditorValueEntityField(c);
	}

	@Override
	public TableCellRenderer getTableCellRenderer(final WYSIWYGComponent c, final String property, final PropertiesPanel dialog) {
		return new PropertyEditorValueEntityField(c);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PropertyValueEntityField) {
			PropertyValueEntityField pvs = (PropertyValueEntityField) obj;
			return LangUtils.equal(value, pvs.value);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return LangUtils.hashCode(this.value);
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new PropertyValueEntityField(value);
	}

	class PropertyEditorValueEntityField extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {

		private JLabel entityFieldLabel = null;
		private WYSIWYGComponent component;
		private FieldMeta<?> chosenEntityField;

		public PropertyEditorValueEntityField(final WYSIWYGComponent c) {
			this.component = c;
		}

		private Component getComponent(boolean editable) {
			JPanel panel = new JPanel();
			panel.setOpaque(true);
			panel.setBackground(Color.WHITE);
			panel.setLayout(new TableLayout(new double[][]{
					{
							InterfaceGuidelines.CELL_MARGIN_LEFT,
							TableLayout.FILL,
							TableLayout.PREFERRED,
							InterfaceGuidelines.MARGIN_RIGHT
					},
					{
							InterfaceGuidelines.CELL_MARGIN_TOP,
							TableLayout.PREFERRED,
							InterfaceGuidelines.CELL_MARGIN_BOTTOM
					}
			}));

			entityFieldLabel = new JLabel();
			entityFieldLabel.setText(PropertyValueEntityField.this.value);

			TableLayoutConstraints constraint = new TableLayoutConstraints(1, 1, 1, 1, TableLayout.FULL, TableLayout.CENTER);
			panel.add(entityFieldLabel, constraint);

			if (editable) {
				JButton launchEditor = new JButton("...");
				launchEditor.setPreferredSize(new Dimension(30, InterfaceGuidelines.CELL_BUTTON_MAXHEIGHT));
				launchEditor.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						launchEditor();
					}
				});
				constraint = new TableLayoutConstraints(2, 1);
				panel.add(launchEditor, constraint);
			} else {
				entityFieldLabel.setForeground(Color.GRAY);
			}
			return panel;
		}

		private void launchEditor(){
			String control = WYSIWYGMetaInformation.detectControlType(component);
			List<FieldMeta<?>> values = component.getParentEditor().getMetaInformation().getFieldsByControlType(control);
			final List<FieldMeta<?>> allValues = component.getParentEditor().getMetaInformation().getAllFields();
			FieldMeta<?> selectedValue = allValues.stream()
					.filter(fm -> Objects.equals(fm.getFieldName(), PropertyValueEntityField.this.value))
					.findFirst().orElse(null);

			chosenEntityField = WYSIWYGMetaInformationPicker.showPickDialog(component.getParentEditor(), allValues, values, selectedValue, null);
			this.stopCellEditing();

		}

		@Override
		public boolean stopCellEditing() {
			if (chosenEntityField != null) {
				PropertyValueEntityField.this.setValue(chosenEntityField.getFieldName());
				entityFieldLabel.setText(chosenEntityField.getFieldName());
			}
			return super.stopCellEditing();
		}

		@Override
		public Object getCellEditorValue() {
			return PropertyValueEntityField.this;
		}

		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			return getComponent(true);
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			return getComponent(true);
		}

	}

}
