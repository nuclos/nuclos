//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
/**
 *
 */
package org.nuclos.client.layout.wysiwyg.component.properties;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.common2.LangUtils;
import org.xml.sax.Attributes;

/**
 *
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:thomas.schiffmann@novabit.de">thomas.schiffmann</a>
 * @version 01.00.00
 */
public class PropertyValueReadOnly implements PropertyValue<Object> {

	private Object value = "";

	/**
	 * Constructor
	 */
	public PropertyValueReadOnly() { }

	/**
	 * Constructor
	 * @param value the value to be restored
	 */
	public PropertyValueReadOnly(Object value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue#getValue()
	 */
	@Override
	public Object getValue() {
		return value;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(Object value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue#getTableCellEditor(org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent, java.lang.String, org.nuclos.client.layout.wysiwyg.component.properties.PropertiesDialog)
	 */
	@Override
	public TableCellEditor getTableCellEditor(WYSIWYGComponent c, String property, PropertiesPanel dialog) {
		return new PropertyEditorReadOnly();
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue#getTableCellRenderer(org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent, java.lang.String, org.nuclos.client.layout.wysiwyg.component.properties.PropertiesDialog)
	 */
	@Override
	public TableCellRenderer getTableCellRenderer(WYSIWYGComponent c, String property, PropertiesPanel dialog) {
		return new PropertyEditorReadOnly();
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue#getValue(java.lang.Class, org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent)
	 */
	@Override
	public Object getValue(Class<?> cls, WYSIWYGComponent c) {
		if (cls != null && cls.equals(String.class)) {
			return value.toString();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue#setValue(java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void setValue(String attributeName, Attributes attributes) {
		this.value = attributes.getValue(attributeName);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PropertyValueReadOnly) {
			PropertyValueReadOnly pvb = (PropertyValueReadOnly) obj;
			return LangUtils.equal(value, pvb.value);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return LangUtils.hashCode(value);
	}

	@Override
	public String toString() {
		if (value == null) {
			return "null";
		}
		return value.toString();
	}
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return new PropertyValueReadOnly(value);
	}


	/**
	 *
	 *
	 *
	 * <br>
	 * Created by Novabit Informationssysteme GmbH <br>
	 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
	 *
	 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
	 * @version 01.00.00
	 */
	public class PropertyEditorReadOnly extends AbstractCellEditor implements TableCellEditor, TableCellRenderer {

		private JTextField displayLabel;

		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			displayLabel = new JTextField();
			displayLabel.setText(getDisplayValue());
			displayLabel.setEditable(false);
			displayLabel.setBorder(null);
			displayLabel.addMouseListener(this.createClipboardMouseAdapter());

			return displayLabel;
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			displayLabel = new JTextField();
			displayLabel.setText(getDisplayValue());
			displayLabel.setEditable(false);
			displayLabel.setBorder(null);
			displayLabel.addMouseListener(this.createClipboardMouseAdapter());

			return displayLabel;
		}

		private MouseAdapter createClipboardMouseAdapter() {
			MouseAdapter clipboardAdapter = new MouseAdapter() {
				@Override
				public void mouseReleased(final MouseEvent e) {
					Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipboard.setContents(new StringSelection(getDisplayValue()), null);
				}
			};
			return clipboardAdapter;
		}

		private String getDisplayValue() {
			if (value != null) {
				String strValue = value.toString();
				if (strValue.startsWith("uid{")) {
					strValue = strValue.replace("uid{", "").replace("}", "");
				}
				return strValue;
			}
			return null;
		}

		@Override
		public Object getCellEditorValue() {
			return PropertyValueReadOnly.this;
		}

		@Override
		public boolean stopCellEditing() {
			return super.stopCellEditing();
		}

	}
}
