package org.nuclos.client.layout.wysiwyg.editor.ui;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;

import org.nuclos.client.NuclosIcons;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.InterfaceGuidelines;
import org.nuclos.client.main.mainframe.MainFrameDialog;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 * Small Class providing a {@link JDialog} for choosing a {@link Color}
 *
 *
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class ColorDialog extends MainFrameDialog implements SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable {

	private static final long serialVersionUID = 2806498099173674503L;

	public Color color;
	private JColorChooser colorChooser;
	private JButton remove;

	public ColorDialog(Color color, WYSIWYGComponent component) {
		this.setIconImage(NuclosIcons.getInstance().getScaledDialogIcon(48).getImage());

		this.setLayout(new TableLayout(new double[][]{
				{
						InterfaceGuidelines.MARGIN_BETWEEN,
						TableLayout.FILL,
						TableLayout.PREFERRED,
						InterfaceGuidelines.DISTANCE_TO_OTHER_OBJECTS,
						TableLayout.PREFERRED,
						InterfaceGuidelines.DISTANCE_TO_OTHER_OBJECTS,
						TableLayout.PREFERRED,
						TableLayout.FILL,
						InterfaceGuidelines.MARGIN_BETWEEN
				},
				{
						InterfaceGuidelines.MARGIN_BETWEEN,
						TableLayout.FILL,
						InterfaceGuidelines.MARGIN_BETWEEN,
						TableLayout.PREFERRED,
						InterfaceGuidelines.MARGIN_BETWEEN
				}
		}));

		this.color = color;
		this.colorChooser = new JColorChooser(this.color==null?Color.WHITE:this.color);

		this.add(colorChooser, new TableLayoutConstraints(1,1,7,1));

		this.remove = new JButton(WYSIWYGStringsAndLabels.PROPERTY_VALUE_COLOR.LABEL_REMOVE_COLOR);
		this.remove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ColorDialog.this.color = null;
				ColorDialog.this.setVisible(false);

				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						try {
							if (component != null)
								component.getParentEditor().getController().safePendingPropertyChanges(true);
						} catch (Exception e) {
							// ignore. if something went wrong it will be shown in safePendingPropertyChanges
						}
					}
				});
			}
		});

		List<AbstractButton> additionalButtons = new ArrayList<AbstractButton>();
		additionalButtons.add(remove);

		SaveAndCancelButtonPanel saveandcancel = new SaveAndCancelButtonPanel(this.getBackground(), this, component, additionalButtons);
		this.add(saveandcancel, new TableLayoutConstraints(2,3,6,3));

		setTitle(WYSIWYGStringsAndLabels.PROPERTY_VALUE_COLOR.COLOR_PICKER_TITLE);

		int width = 640;
		int height = 480;
		this.setSize(width, height);
		this.setModal(true);
		this.setVisible(true);
	}

	/**
	 * @return the {@link Color} set
	 */
	public Color getColor() {
		return this.color;
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable#performCancelAction()
	 */
	@Override
	public void performCancelAction() {
		ColorDialog.this.dispose();
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable#performSaveAction()
	 */
	@Override
	public void performSaveAction() {
		ColorDialog.this.color = colorChooser.getColor();
		ColorDialog.this.setVisible(false);
	}
}
