package org.nuclos.client.layout.wysiwyg.editor.ui.panels.editor;

import java.awt.Color;
import java.awt.Dimension;
import java.nio.charset.StandardCharsets;

import javax.swing.JDialog;
import javax.swing.JTextArea;

import org.apache.commons.codec.binary.Base64;
import org.nuclos.client.NuclosIcons;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.elementalcomponents.SaveAndCancelButtonPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.InterfaceGuidelines;

import info.clearthought.layout.TableLayout;

public class HTMLEditor extends JDialog implements SaveAndCancelButtonPanel.SaveAndCancelButtonPanelControllable {

	private final int height = 300;
	private final int width = 350;

	public static String returnHTMLEncoded;

	private final String initialHTMLEncoded;

	private final JTextArea htmlEditorField;

	public HTMLEditor(String initialHTMLEncoded, WYSIWYGComponent cmp) {
		this.setIconImage(NuclosIcons.getInstance().getScaledDialogIcon(48).getImage());
		this.setTitle(WYSIWYGStringsAndLabels.STATIC_HTMLFIELD.PALETTE_ELEMENTNAME);
		double[][] layout = {
				{TableLayout.FILL},
				{TableLayout.FILL, InterfaceGuidelines.DEFAULT_ROW_HEIGHT}
		};
		this.setLayout(new TableLayout(layout));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.initialHTMLEncoded = initialHTMLEncoded;


		this.htmlEditorField = new JTextArea();
		this.htmlEditorField.setEditable(true);
		this.htmlEditorField.setLineWrap(true);
		this.htmlEditorField.setBackground(Color.WHITE);

		if (this.initialHTMLEncoded != null) {
			this.htmlEditorField.setText(
					new String(
							Base64.decodeBase64(this.initialHTMLEncoded),
							StandardCharsets.UTF_8
					)
			);
		}

		this.add(
				this.htmlEditorField,
				"0,0"
		);

		this.add(
				new SaveAndCancelButtonPanel(this.getBackground(), this, cmp, null),
				"0,1"
		);

		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screenSize.width - WIDTH) / 2;
		int y = (screenSize.height - HEIGHT) / 2;

		this.setLocation(x - 175, y - 150);

		this.setSize(width, height);
		this.setModal(true);
		this.setVisible(true);
	}

	@Override
	public void performSaveAction() {
		returnHTMLEncoded = new String(
				Base64.encodeBase64(this.htmlEditorField.getText().getBytes(StandardCharsets.UTF_8)),
				StandardCharsets.UTF_8
		);
		this.dispose();
	}

	@Override
	public void performCancelAction() {
		returnHTMLEncoded = initialHTMLEncoded;
		this.dispose();
	}

	public static String showHTMLEditor(String initialHTML, WYSIWYGComponent cmp) {
		new HTMLEditor(initialHTML, cmp);

		return returnHTMLEncoded;
	}
}
