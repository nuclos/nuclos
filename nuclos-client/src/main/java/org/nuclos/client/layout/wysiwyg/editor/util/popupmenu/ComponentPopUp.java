//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.editor.util.popupmenu;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.IllegalComponentStateException;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;
import org.nuclos.api.ui.MenuItem;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.COMPONENT_POPUP;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGChart;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCloneableLayoutComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableHTMLField;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGMatrix;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticHTMLField;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubForm;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGTabbedPane;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.LayoutMLRuleEditorDialog;
import org.nuclos.client.layout.wysiwyg.editor.util.InterfaceGuidelines;
import org.nuclos.client.layout.wysiwyg.editor.util.TableLayoutUtil;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.LayoutCell;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.TableLayoutPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRules;

/**
 * This class provides the ContextMenu shown for every {@link WYSIWYGComponent}.<br>
 * There are some standard actions nearly every {@link WYSIWYGComponent} has like:
 * <ul>
 * <li> Move </li>
 * <li> Change the Alignment </li>
 * <li> Delete </li>
 * </ul>
 * It does load additional {@link MenuItem} from {@link WYSIWYGComponent#getAdditionalContextMenuItems(int)}
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class ComponentPopUp {

	private static final Logger LOG = Logger.getLogger(ComponentPopUp.class);

	private JPopupMenu contextMenu = new JPopupMenu();
	private JMenuItem deleteComponent;
	private TableLayoutPanel contentPane = null;
	private WYSIWYGComponent wysiwygcomponent = null;
	private int xClick;
	private TableLayoutUtil tableLayoutUtil;

	/**
	 * 
	 * @param tableLayoutUtil
	 * @param component
	 */
	public ComponentPopUp(TableLayoutUtil tableLayoutUtil, Component component) {
		this(tableLayoutUtil, component, 0);
	}

	/**
	 * Constructs a ComponentPopUp with a list of MenuItems of options for the clicked {@link WYSIWYGComponent}
	 * @param containerTableLayoutUtil {@link TableLayoutUtil} of the container, where the {@link ComponentPopUp} was triggered
	 * @param component {@link Component} of the {@link java.awt.event.MouseEvent MouseEvent}, which triggered this ComponentPopup
	 * @param x Mouse X-Coordinate at the time of opening this ComponentPopup
	 */
	public ComponentPopUp(final TableLayoutUtil containerTableLayoutUtil, Component component, int x) {
		this.tableLayoutUtil = containerTableLayoutUtil;
		if (component instanceof WYSIWYGTabbedPane) {
			Component comp = ((WYSIWYGTabbedPane) component).getSelectedComponent();
			if (comp instanceof WYSIWYGLayoutEditorPanel) {
				this.tableLayoutUtil = ((WYSIWYGLayoutEditorPanel)comp).getTableLayoutUtil();
			}
		}
		this.contentPane = this.tableLayoutUtil.getContainer();
		
		/** find the fitting component */
		if (component instanceof WYSIWYGLayoutEditorPanel) {
			if (((WYSIWYGLayoutEditorPanel) component).getParentWrappingComponent() == null)
				wysiwygcomponent = (WYSIWYGLayoutEditorPanel) component;
			else
				wysiwygcomponent = ((WYSIWYGLayoutEditorPanel) component).getParentWrappingComponent();
		} else {
			while (!(component instanceof WYSIWYGComponent) && component != null) {
				component = component.getParent();
			}
			if (component != null) {
				wysiwygcomponent = (WYSIWYGComponent) component;
			}
		}

		this.xClick = x;

		/**
		 * Enable Movement of Component
		 */
		JMenuItem moveComponent = new JMenuItem(COMPONENT_POPUP.LABEL_MOVE_COMPONENT);
		moveComponent.addActionListener(e -> {
			contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
			moveComponent();
			contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		});

		/**
		 * Enable Movement of Component
		 */
		JMenuItem cloneComponent = new JMenuItem(COMPONENT_POPUP.LABEL_CLONE_COMPONENT);
		cloneComponent.addActionListener(e -> {
			contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
			cloneComponent();
			contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		});

		boolean hasParentEditor = ((WYSIWYGComponent)component).getParentEditor() != null;
		boolean isCloneableLayoutComponent = wysiwygcomponent instanceof WYSIWYGCloneableLayoutComponent;
		boolean noMultiSelection = !this.hasMultiSelection();
		if (hasParentEditor && noMultiSelection) {
			contextMenu.add(moveComponent);
			if (isCloneableLayoutComponent) {
				contextMenu.add(cloneComponent);
			}
		}

		/**
		 * If the Component is a WYSIWYGLayoutEditorPanel the Standardborder can be hidden
		 */
		if (!this.hasMultiSelection() && (component instanceof WYSIWYGLayoutEditorPanel || component instanceof WYSIWYGTabbedPane)) {
			LayoutCell layoutCell = this.tableLayoutUtil.getLayoutCellByPosition(0, 0);
			
			boolean borderIsShown = true;
			if (layoutCell.getCellHeight() == 0 && layoutCell.getCellWidth() == 0){
				borderIsShown = false;
			}


			String toggleStandardBorderText = borderIsShown ? COMPONENT_POPUP.LABEL_HIDE_STANDARD_BORDER : COMPONENT_POPUP.LABEL_SHOW_STANDARD_BORDER;
			JMenuItem toggleStandardBorder = new JMenuItem(toggleStandardBorderText);

			toggleStandardBorder.addActionListener(e -> tableLayoutUtil.toggleStandardBorderVisible());
			
			contextMenu.add(toggleStandardBorder);
		}

		/**
		 * Add Rules if Component can be Target of LayoutMLRules 
		 */
		if (wysiwygcomponent.getLayoutMLRulesIfCapable() != null
				&& !this.hasMultiSelection()
				&& (!(wysiwygcomponent instanceof WYSIWYGSubForm))
				&& (!(wysiwygcomponent instanceof WYSIWYGChart))
				&& (!(wysiwygcomponent instanceof WYSIWYGMatrix))) {
			/** if the component can be target of layoutml rules this item is added */
			JMenuItem addLayoutMLRule = new JMenuItem(COMPONENT_POPUP.LABEL_EDIT_RULES_FOR_COMPONENT);
			addLayoutMLRule.addActionListener(e -> addLayoutMLRuleToComponent(wysiwygcomponent.getParentEditor()));

			contextMenu.add(addLayoutMLRule);
		}

		/**
		 * Delete the Component
		 */
		deleteComponent = new JMenuItem(COMPONENT_POPUP.LABEL_DELETE_COMPONENT);
		deleteComponent.addActionListener(e -> deleteSelectedComponents());

		contextMenu.add(deleteComponent);

		/**
		 * Add a Separator (after this Component specific Contextmenu Items are added
		 */
		if (component instanceof WYSIWYGLayoutEditorPanel) {
			if (((WYSIWYGLayoutEditorPanel) component).getParentEditor() == null) {
				// if it is not usable, just remove it
				contextMenu.remove(deleteComponent);
			}
		}
		
		/**
		 * there is no need to display move and delete for a subform column
		 */
		if((wysiwygcomponent instanceof WYSIWYGSubForm)) {
			if(((WYSIWYGSubForm) wysiwygcomponent).getColumnAtX(xClick) != null) {
				contextMenu.remove(moveComponent);
				contextMenu.remove(cloneComponent);
				contextMenu.remove(deleteComponent);
			}
		}

		/**
		 * Loading additional Menuitems from each WYSIWYG Component.
		 */
		if (wysiwygcomponent != null && !this.hasMultiSelection()) {
			List<JMenuItem> additionalItems = wysiwygcomponent.getAdditionalContextMenuItems(xClick);
			if (additionalItems != null && !additionalItems.isEmpty()) {

				for (Iterator<JMenuItem> it = additionalItems.iterator(); it.hasNext();) {
					JMenuItem mi = it.next();
					if ("-".equals(mi.getLabel())) {
						contextMenu.addSeparator();
					} else {
						contextMenu.add(mi);
					}
				}
			}
		}
	}

	private boolean hasMultiSelection() {
		return (wysiwygcomponent != null && wysiwygcomponent.getParentEditor() != null && wysiwygcomponent.getParentEditor().getSelectedComponents().size() > 1);
	}

	/**
	 * This Method is opening the {@link LayoutMLRuleEditorDialog} and adds 
	 * {@link LayoutMLRules} to this {@link WYSIWYGComponent}
	 * 
	 * @param editorPanel
	 */
	protected void addLayoutMLRuleToComponent(WYSIWYGLayoutEditorPanel editorPanel) {
		LayoutMLRuleEditorDialog ruleDialog = new LayoutMLRuleEditorDialog(wysiwygcomponent, editorPanel);
		if (ruleDialog.getExitStatus() == LayoutMLRuleEditorDialog.EXIT_SAVE)
			tableLayoutUtil.notifyThatSomethingChanged();
	}

	/**
	 * Shows the Contextmenu for the Component
	 * 
	 * @param mouseLoc the position of the Mouse (needed for SubformColumns)
	 * 
	 */
	public void showComponentPropertiesPopup(Point mouseLoc) {
		try {
			contextMenu.show(contentPane, mouseLoc.x, mouseLoc.y);			
		} catch (IllegalComponentStateException e) {
			LOG.warn("showComponentPropertiesPopup", e);
		}
	}

	/**
	 * Not using the immediate tableLayoutUtil, but instead looking for a TableLayoutUtil higher up in the hierarchie
	 * prevents encountering a NPE, when deleting a TabbedPane/SplitPane.
	 * see NUCLOS-10228
	 * @return
	 */
	private TableLayoutUtil getPreferredTableLayoutUtilForDeletion() {
		TableLayoutUtil preferredTableLayoutUtil = tableLayoutUtil;
		WYSIWYGLayoutEditorPanel layoutEditorPanel = tableLayoutUtil.getContainer().getParentEditorPanel();
		while (layoutEditorPanel != null && layoutEditorPanel.getParentEditor() != null) {
			layoutEditorPanel = layoutEditorPanel.getParentEditor();
		}
		if (layoutEditorPanel != null) {
			preferredTableLayoutUtil = layoutEditorPanel.getTableLayoutUtil();
		}
		return preferredTableLayoutUtil;
	}

	/**
	 * The Action to perform to delete a {@link WYSIWYGComponent} from the {@link TableLayoutPanel}
	 */
	private void deleteSelectedComponents() {
		getPreferredTableLayoutUtilForDeletion()
				.removeComponentsFromLayout(wysiwygcomponent.getParentEditor().getSelectedComponents(), true);
	}

	/**
	 * This Method handles Moving a {@link WYSIWYGComponent}
	 */
	private void moveComponent() {
		WYSIWYGComponent compToBeMoved = wysiwygcomponent;
		if (wysiwygcomponent instanceof WYSIWYGStaticHTMLField  && wysiwygcomponent.getParent() instanceof WYSIWYGCollectableHTMLField) {
			compToBeMoved = (WYSIWYGComponent) wysiwygcomponent.getParent();
		}
		tableLayoutUtil.getContainer().setComponentToMove(compToBeMoved);
		((Component) compToBeMoved).setEnabled(false);
	}

	/**
	 * This Method handles Cloning a {@link WYSIWYGComponent}
	 */
	private void cloneComponent() {
		WYSIWYGComponent compToBeCloned = wysiwygcomponent;
		if (wysiwygcomponent instanceof WYSIWYGStaticHTMLField && wysiwygcomponent.getParent() instanceof WYSIWYGCollectableHTMLField) {
			compToBeCloned = (WYSIWYGComponent) wysiwygcomponent.getParent();
		}
		tableLayoutUtil.getContainer().setComponentToClone((WYSIWYGCloneableLayoutComponent) compToBeCloned);
		((Component) compToBeCloned).setEnabled(false);
	}
}
