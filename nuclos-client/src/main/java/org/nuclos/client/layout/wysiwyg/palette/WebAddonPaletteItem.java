//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.palette;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.PaletteItemElement;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.ui.Icons;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class WebAddonPaletteItem implements PaletteItemElement {

	private final EntityObjectVO<UID> eoWebAddon;

	public WebAddonPaletteItem(EntityObjectVO<UID> eoWebAddon) {
		super();
		this.eoWebAddon = eoWebAddon;
	}

	@Override
	public String getToolTip() {
		return eoWebAddon.getFieldValue(E.WEBADDON.nuclet.getUID(), String.class);
	}

	@Override
	public String getLabel() {
		return eoWebAddon.getFieldValue(E.WEBADDON.name);
	}

	@Override
	public Icon getIcon() {
		return getWebAddonIcon();
	}

	public static Icon getWebAddonIcon() {
		ImageIcon nuclosIcon = NuclosResourceCache.getNuclosResourceIcon(E.WEBADDON.getNuclosResource());
		if (nuclosIcon != null) {
			return MainFrame.resizeAndCacheTabIcon(nuclosIcon);
		}
		return Icons.getInstance().getIconGenericObject16();
	}

	@Override
	public boolean displayLabelAndIcon() {
		return true;
	}

	@Override
	public boolean isLabeledComponent() {
		return false;
	}
}
