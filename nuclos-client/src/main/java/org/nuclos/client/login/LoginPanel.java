//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
// Copyright (C) 2010 Novabit Informationssysteme GmbH
//
// This file is part of Nuclos.
//
// Nuclos is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nuclos is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Nuclos. If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.ToolTipManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.nuclos.client.LocalUserProperties;
import org.nuclos.client.StartIcons;
import org.nuclos.client.common.Utils;
import org.nuclos.client.ui.BackgroundPanel;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.TitledSeparator;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.schema.rest.SsoAuthEndpoint;
import org.nuclos.server.common.ejb3.SecurityFacadeRemote;
import org.springframework.remoting.RemoteAccessException;

import jiconfont.icons.FontAwesome;
import jiconfont.swing.IconFontSwing;

/**
 * Login panel. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class LoginPanel extends BackgroundPanel {
	
	private static final Logger LOG = Logger.getLogger(LoginPanel.class);
	
	private static LoginPanel INSTANCE = null;

	//
	
	private final JPanel	   pnlLogin	     = new JPanel();
	private final JPanel	   pnlLogo	     = new JPanel();

	private final JLabel	   labUserName;
	private final JLabel	   labPassword;
	private final JLabel	   labMsgSpacer;
	private final JLabel	   labLanguage;
	private final JLabel	   labDataLanguage;
	private final TitledSeparator	   labMandator;
	private final SSLCheckLabel labSSLCheck;

	private Bubble bubble;

	private final JTextField tfUserName;
	private final JPasswordField	   tfPassword	 = new JPasswordField();
	private final JComboBox			   cmbbxMandator = new JComboBox() {
		public Dimension getPreferredSize() {return new Dimension(super.getPreferredSize().width, tfPassword.getPreferredSize().height);}
	};
	private final JComboBox	           cmbbxLanguage = new JComboBox() {
		public Dimension getPreferredSize() {return new Dimension(super.getPreferredSize().width, tfPassword.getPreferredSize().height);}
	};
	private final JComboBox	           cmbbxDataLanguage = new JComboBox() {
		public Dimension getPreferredSize() {return new Dimension(super.getPreferredSize().width, tfPassword.getPreferredSize().height);}
	};
	
	private final JCheckBox	           rememberPass	 = new JCheckBox();

	private final List<SsoAuthEndpoint> lstSsoAuthEndpoints = new ArrayList<>();
	private final Map<String, JButton> mapSsoButtonWebLogin = new HashMap<>();

	private final JProgressBar	progressbar	 = new JProgressBar();

	private LoginPanel(final SecurityFacadeRemote sfr) {
		lstSsoAuthEndpoints.addAll(sfr.getSsoAuthEndpoints());

		tfUserName	 = new JTextField();
		
		Icon iconCustomer = StartIcons.getInstance().getIconCustomer();
		JLabel labLogo = new JLabel(iconCustomer);
		labLogo.setBorder(null);
		labLogo.setPreferredSize(new Dimension(iconCustomer.getIconWidth(),
		    iconCustomer.getIconHeight()));
		labLogo.setMinimumSize(new Dimension(iconCustomer.getIconWidth(),
		    iconCustomer.getIconHeight()));
		labLogo.setMaximumSize(new Dimension(iconCustomer.getIconWidth(),
		    iconCustomer.getIconHeight()));

		LocalUserProperties props = LocalUserProperties.getInstance();
		labUserName = new JLabel(
		    props.getLoginResource(LocalUserProperties.KEY_LAB_USERNAME));
		labPassword = new JLabel(
		    props.getLoginResource(LocalUserProperties.KEY_LAB_PASSWORD));
		labMandator = new TitledSeparator(props.getLoginResource(LocalUserProperties.KEY_LAB_MANDATOR));
		labMsgSpacer = new JLabel(" ");
		labLanguage = new JLabel(
		    props.getLoginResource(LocalUserProperties.KEY_LANG_REGION));
		labDataLanguage = new JLabel(
			    props.getLoginResource(LocalUserProperties.KEY_LANG_DATA_LOCALE));
		labSSLCheck = new SSLCheckLabel();
		rememberPass.setText(props.getLoginResource(LocalUserProperties.KEY_LANG_AUTOLOGIN));
		rememberPass.setOpaque(false);
		rememberPass.setEnabled(false);
		rememberPass.setVisible(false);
		//rememberPass.addFocusListener(new BackgroundListener());
		
		labMandator.setVisible(false);
		cmbbxMandator.setVisible(false);
		cmbbxMandator.setEnabled(false);

		Color tx; 
		if (ApplicationProperties.arePropertiesAvailaible()) {
			tx = ApplicationProperties.getInstance().getLoginPanelTextColor(
				    Color.BLACK);			
		} else {
			tx = Color.BLACK;
		}

		for(JLabel lab : new JLabel[] { labUserName, labPassword, /* labSpacer, */
		    labLanguage })
			lab.setForeground(tx);

//		Color bhi = ApplicationProperties.getInstance().getLoginPanelBorderHiColor(
//		    null);
//		Color bsh = ApplicationProperties.getInstance().getLoginPanelBorderShadeColor(
//		    null);

		this.setName("pnlLogin");
		this.setLayout(new BorderLayout());
		this.setOpaque(true);
		this.setBackground(new Color(0, 0, 0, 0));
		/*this.setBorder(bhi != null && bsh != null
		    ? BorderFactory.createEtchedBorder(bhi, bsh)
		    : BorderFactory.createEtchedBorder());
		*/
		this.add(pnlLogo, BorderLayout.NORTH);
		this.add(pnlLogin, BorderLayout.CENTER);

		pnlLogo.setLayout(new GridBagLayout());
		pnlLogo.setOpaque(false);
		pnlLogo.add(labLogo, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
		    GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,
		        0, 0, 0), 0, 0));

		pnlLogin.setLayout(new GridBagLayout());
		pnlLogin.setOpaque(false);
		pnlLogin.setBorder(BorderFactory.createEmptyBorder(1, 10, 1, 10));

		final int iInsetBottom = 10;
		int y = 0;

		pnlLogin.add(labUserName, new GridBagConstraints(0, y, 1, 1, 0.0, 0.0,
		    GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0,
		        iInsetBottom, 10), 0, 0));
		pnlLogin.add(tfUserName, new GridBagConstraints(1, y, 1, 1, 0.0, 0.0,
		    GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(
		        0, 0, iInsetBottom, 0), 0, 0));
		y++;
		pnlLogin.add(labPassword, new GridBagConstraints(0, y, 1, 1, 0.0, 0.0,
		    GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0,
		        iInsetBottom, 10), 0, 0));
		pnlLogin.add(tfPassword, new GridBagConstraints(1, y, 1, 1, 0.0, 0.0,
		    GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(
		        0, 0, iInsetBottom, 0), 0, 0));
		y++;
		pnlLogin.add(rememberPass, new GridBagConstraints(1, y, 1, 1, 0.0,
			0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
			new Insets(0, 0, iInsetBottom, 0), 0, 0));
		y++;
		pnlLogin.add(labLanguage, new GridBagConstraints(0, y, 1, 1, 0.0, 0.0,
		    GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0,
		        iInsetBottom, 10), 0, 0));
		pnlLogin.add(cmbbxLanguage, new GridBagConstraints(1, y, 1, 1, 0.0,
		    0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
		    new Insets(0, 0, iInsetBottom, 0), 0, 0));
		y++;
		pnlLogin.add(labDataLanguage, new GridBagConstraints(0, y, 1, 1, 0.0, 0.0,
		    GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0,
		        iInsetBottom, 10), 0, 0));
		pnlLogin.add(cmbbxDataLanguage, new GridBagConstraints(1, y, 1, 1, 0.0,
		    0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
		    new Insets(0, 0, iInsetBottom, 0), 0, 0));
		y++;

		if (lstSsoAuthEndpoints.size() > 0) {
			pnlLogin.add(new TitledSeparator(props.getLoginResource(LocalUserProperties.KEY_LAB_SSOAUTH)),
					new GridBagConstraints(0, y, 2, 1, 0.0, 0.0,
					GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					new Insets(0, 0, iInsetBottom, 0), 0, 0));
			y++;
			for (SsoAuthEndpoint ssoAuthEndpoint : lstSsoAuthEndpoints) {
				JButton btnSsoWebLogin = new JButton(ssoAuthEndpoint.getLabel()) {
					@Override
					public Dimension getPreferredSize() {
						return new Dimension(super.getPreferredSize().width, 40);
					}
				};
				if (ssoAuthEndpoint.getColor() != null) {
					Color color = Color.decode(ssoAuthEndpoint.getColor());
					btnSsoWebLogin.setBackground(color);
					btnSsoWebLogin.setForeground(Utils.getBestForegroundColor(color));
					btnSsoWebLogin.setOpaque(true);
					btnSsoWebLogin.setBorderPainted(false);
				}
				if (ssoAuthEndpoint.getImageIcon() != null) {
					byte[] bytes = Base64.decodeBase64(ssoAuthEndpoint.getImageIcon());
					ImageIcon icon = new ImageIcon(bytes);
					btnSsoWebLogin.setIcon(UIUtils.resizeImageIcon(icon, 26));
				} else if (!org.nuclos.common2.StringUtils.looksEmpty(ssoAuthEndpoint.getFontAwesomeIcon())) {
					String fontAwesomeIconCssClass = ssoAuthEndpoint.getFontAwesomeIcon();
					try {
						String fontAwesomeIconJavaEnum = fontAwesomeIconCssClass.replace('-', '_').toUpperCase();
						if (fontAwesomeIconJavaEnum.startsWith("FA_")) {
							fontAwesomeIconJavaEnum = fontAwesomeIconJavaEnum.substring(3);
						}
						FontAwesome fa = FontAwesome.valueOf(fontAwesomeIconJavaEnum);
						Icon icon = IconFontSwing.buildIcon(fa, 26);
						btnSsoWebLogin.setIcon(icon);
					} catch (Exception ex) {
						LOG.error("Could not show SSO Font Awesome icon " + ssoAuthEndpoint.getFontAwesomeIcon() + ": " + ex.getMessage(), ex);
					}
				}
				mapSsoButtonWebLogin.put(ssoAuthEndpoint.getSsoAuthId(), btnSsoWebLogin);
				pnlLogin.add(btnSsoWebLogin, new GridBagConstraints(0, y, 2, 1, 0.0, 0.0,
						GridBagConstraints.WEST, GridBagConstraints.BOTH,
						new Insets(0, 0, iInsetBottom, 0), 0, 0));
				y++;
			}
		}
		pnlLogin.add(labMandator, new GridBagConstraints(0, y, 2, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0,
				iInsetBottom, 10), 0, 0));
		y++;
		pnlLogin.add(cmbbxMandator, new GridBagConstraints(0, y, 2, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(
				0, 0, iInsetBottom, 0), 0, 0));
		y++;
		pnlLogin.add(progressbar, new GridBagConstraints(0, y, 2, 1, 0.0, 0.0,
		    GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(
		        0, 0, iInsetBottom, 0), 0, 0));
		labMsgSpacer.setPreferredSize(progressbar.getPreferredSize());
		pnlLogin.add(labMsgSpacer, new GridBagConstraints(0, y, 2, 1, 0.0, 0.0,
		    GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(
		        0, 0, iInsetBottom, 0), 0, 0));
		y++;
		pnlLogin.add(labSSLCheck, new GridBagConstraints(0, y, 2, 1, 0.0,
				0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, iInsetBottom, 0), 0, 0));
	
		tfUserName.setName("tfUserName");
		tfUserName.setText("");
		tfUserName.setColumns(10);
		tfUserName.addFocusListener(new BackgroundListener());
		tfPassword.setName("tfPassword");
		tfPassword.setText("");
		tfPassword.setColumns(10);
		tfPassword.addFocusListener(new BackgroundListener());
		tfPassword.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {}
			@Override
			public void insertUpdate(DocumentEvent e) {
				if(bubble != null)
					bubble.dispose();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				if(bubble != null)
					bubble.dispose();
			}
		});
		cmbbxLanguage.setName("cmbbxLanguage");
		cmbbxLanguage.addFocusListener(new BackgroundListener());
		
		cmbbxDataLanguage.setName("cmbbxDataLanguage");
		cmbbxDataLanguage.addFocusListener(new BackgroundListener());
		
		progressbar.setVisible(false);

		Color pbfg;
		if (ApplicationProperties.arePropertiesAvailaible()) {
			pbfg = ApplicationProperties.getInstance().getSplashProgressColor(
					null);
		} else {
			pbfg = Color.BLUE;
		}
		if(pbfg != null)
			progressbar.setForeground(pbfg);		
		
		try {
			Object oTooltipsEnabled = props.get("tooltips.enabled");
			if (oTooltipsEnabled != null) {
				if ("false".equalsIgnoreCase((String) oTooltipsEnabled)) {
					ToolTipManager.sharedInstance().setEnabled(false);
				}
			}
		} catch (Exception ex) {
			// ignore, setting for developing only
		}
	}
	
	public static final synchronized LoginPanel getInstance(SecurityFacadeRemote sfr) {
		if (INSTANCE == null) {
			try {
				INSTANCE = new LoginPanel(sfr);
			} catch (RemoteAccessException e) {
				String message = e.getMessage();
				if (message != null && message.contains("status code = 403")) {
					message = "This client is not available (from your location).";
				}
				JOptionPane.showMessageDialog(null, String.format("Connection to the server could not be established.\n%s", message), "Nuclos", JOptionPane.ERROR_MESSAGE);
			}
		}
		return INSTANCE;
	}
	
	void enableRememberCheckbox(boolean withRememberCheckbox) {
		rememberPass.setEnabled(withRememberCheckbox);
		rememberPass.setVisible(withRememberCheckbox);
	}

	void enablePasswordBasedLogin(boolean withPasswordBasedLogin) {
		labUserName.setVisible(withPasswordBasedLogin);
		tfUserName.setVisible(withPasswordBasedLogin);
		labPassword.setVisible(withPasswordBasedLogin);
		tfPassword.setVisible(withPasswordBasedLogin);
	}
	
	JComboBox getMandatorComboBox() {
		return cmbbxMandator;
	}
	
	JComboBox getLanguageComboBox() {
		return cmbbxLanguage;
	}
	
	JComboBox getDataLanguageComboBox() {
		return cmbbxDataLanguage;
	}
	
	JCheckBox getRememberPwCheckBox() {
		return rememberPass;
	}
	
	JPasswordField getPasswordField() {
		return tfPassword;
	}
	
	JTextField getUsernameField() {
		return tfUserName;
	}

	public List<SsoAuthEndpoint> getSsoAuthEndpoints() {
		return lstSsoAuthEndpoints;
	}

	JButton getSsoWebLoginButton(String ssoAuthId) {
		return mapSsoButtonWebLogin.get(ssoAuthId);
	}

	public void setProgressVisible(boolean b) {
		if(progressbar.isVisible() != b) {
			labMsgSpacer.setVisible(!b);
			progressbar.setVisible(b);
			validate();
		}
	}

	public void resetProgress() {
		setProgressVisible(false);
		progressbar.setValue(0);
		UIUtils.paintImmediately(progressbar);
	}

	public void setProgress(int iProgress) {
		setProgressVisible(true);
		progressbar.setValue(iProgress);
		UIUtils.paintImmediately(progressbar);
	}

	public void increaseProgress(int iProgress) {
		setProgressVisible(true);
		progressbar.setValue(progressbar.getValue() + iProgress);
		UIUtils.paintImmediately(progressbar);
	}

	public void hideLanguageSelection() {
		cmbbxLanguage.setVisible(false);
		labLanguage.setVisible(false);
	}

	public void setPasswordError(String msg) {
		if(bubble == null || !bubble.isVisible()) {
			bubble = new Bubble(tfPassword, "<html>" + msg + "</html>", 20);
			bubble.setVisible(true);
		}
	}

	private class BackgroundListener implements FocusListener {
		
		private Color selBg = new Color(255,255,200);
		private Color bg;
		
		@Override
        public void focusGained(FocusEvent e) {
			Component c = e.getComponent();
			if(c != null) {
				if(bg == null) bg = c.getBackground();
				c.setBackground(selBg);
			}
        }
		
		@Override
        public void focusLost(FocusEvent e) {
			Component c = e.getComponent();
			if(c != null) {
				c.setBackground(bg);
			}
        }
	}

	public void mandatorIsPresent() {
		labMandator.setVisible(true);
		cmbbxMandator.setVisible(true);
	}

	public void enableMandator() {
		cmbbxMandator.setEnabled(true);
		cmbbxMandator.requestFocusInWindow();
		cmbbxLanguage.setEnabled(false);
		tfUserName.setEnabled(false);
		tfPassword.setEnabled(false);
		rememberPass.setEnabled(false);
	}

	public void hideDataLanguageSelection() {
		this.cmbbxDataLanguage.setSelectedItem(null);
		this.cmbbxDataLanguage.setVisible(false);
		this.labDataLanguage.setVisible(false);
	}

	public void setSSLCheckState(SSLCheckLabel.STATE state) {
		labSSLCheck.setState(state);
	}
} // class LoginPanel
