package org.nuclos.client.login;

import static org.nuclos.client.ui.UIUtils.findVisibleFrame;

import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.news.NewsBean;
import org.nuclos.schema.rest.News;

/**
 * Checks if there are any News for the current user that require confirmation.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class NewsConfirmationChecker implements Runnable {
	private static final Logger LOG = Logger.getLogger(NewsConfirmationChecker.class);

	private final NewsBean newsBean;

	public NewsConfirmationChecker(final NewsBean newsBean) {
		this.newsBean = newsBean;
	}

	public void run() {
		List<News> unconfirmedNews = newsBean.loadUnconfirmedNews();

		unconfirmedNews.forEach(news -> {
			boolean confirmed = newsBean.showNewsAndConfirm(
					findVisibleFrame(),
					news
			);

			if (!confirmed) {
				LOG.warn("Required news declined by user - exiting");
				shutdown();
			}
		});

		newsBean.refresh();
	}

	private void shutdown() {
		System.exit(0);
	}
}
