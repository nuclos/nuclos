package org.nuclos.client.main;

import java.io.Closeable;

import javax.swing.JComponent;

import org.nuclos.client.main.mainframe.workspace.ITabStoreController;
import org.nuclos.client.ui.MainFrameTabListener;
import org.nuclos.client.ui.layer.LayerLock;

public interface ComponentHolder extends Closeable {
	
	void setName(String s);

	String getTitle();

	void close();
	
	void setLayeredComponent(JComponent layerComponent);

	void setTitle(String sTitle);
	
	void unlockLayer(LayerLock lock);

	void setVisible(boolean b);

	LayerLock lockLayer();
	
	void setTabStoreController(ITabStoreController storeController);
	void addMainFrameTabListener(MainFrameTabListener listener);
	void removeMainFrameTabListener(MainFrameTabListener listener);
}
