package org.nuclos.client.main;

import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.common2.StringUtils;

/**
 * TabRecyclePolicy defines the 3 supported states for TabRecycling. It is used when the user clicks on the "Show details"
 * popup item of a subform entry or a reference field and in the explorer tree.
 */
public enum TabRecyclePolicy {

	/* 1. State: Just generate a new tab to show the entry, even if the same entry is already open. */
	CREATE_NEW_TAB,
	/* 2. State: Only allows one tab per business object. */
	RECYCLE_TAB_PER_BUSINESS_OBJECT,
	/* 3. State: Only allows one tab per entry of a business object. */
	RECYCLE_TAB_PER_ENTRY;

	public static TabRecyclePolicy getTabRecyclePolicyByClientParameter() {
		String paramValue = ClientParameterProvider.getInstance().getValue("SHOW_DETAILS_RECYCLE_TABS");
		if (!StringUtils.isNullOrEmpty(paramValue)) {
			/* Let's be nice to the user and also detect the correct policy, when lower case letters were entered */
			paramValue = StringUtils.toUpperCase(paramValue);
			try {
				TabRecyclePolicy valueOfPolicy = TabRecyclePolicy.valueOf(paramValue);
				return valueOfPolicy;
			} catch(IllegalArgumentException e) { }
		}
		return CREATE_NEW_TAB;
	}

}
