package org.nuclos.client.main.mainframe;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.nuclos.client.main.ComponentHolder;
import org.nuclos.client.main.mainframe.workspace.ITabStoreController;
import org.nuclos.client.ui.MainFrameTabListener;
import org.nuclos.client.ui.layer.LayerLock;

@SuppressWarnings("serial")
public class EmbeddedPanel extends JPanel implements ComponentHolder {
	@Override
	public String getTitle() {
		return super.getName();
	}

	@Override
	public void close() {
	}

	@Override
	public void setLayeredComponent(JComponent layerComponent) {
	}

	@Override
	public void setTitle(String sTitle) {
		super.setName(sTitle);
	}

	@Override
	public void unlockLayer(LayerLock lock) {
	}

	@Override
	public LayerLock lockLayer() {
		return null;
	}

	@Override
	public void setTabStoreController(ITabStoreController storeController) {
	}

	@Override
	public void addMainFrameTabListener(MainFrameTabListener listener) {
	}

	@Override
	public void removeMainFrameTabListener(MainFrameTabListener listener) {
	}

}
