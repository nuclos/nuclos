package org.nuclos.client.main.mainframe;

import java.awt.Component;

import javax.swing.JDialog;

/**
 * A JDialog that always gets positioned on the Monitor where the MainFrame of Nuclos currently is or
 * where a specified component within the MainFrame is located.
 */
public class MainFrameDialog extends JDialog {

	/** if null: the dialog gets centered in the mainframe   otherwise: the dialog gets positioned relative to this component **/
	private Component relativeComponent;

	public MainFrameDialog() {
		super(MainFrame.getInstance());
	}

	public MainFrameDialog(String title) {
		super(MainFrame.getInstance(), title);
	}

	public MainFrameDialog(String title, boolean modal) {
		super(MainFrame.getInstance(), title, modal);
	}

	@Override
	public void setLocationRelativeTo(Component component){
		//we keep track of the component
		//when the dialog gets set to visible on another screen, we can reposition by this reference again
		relativeComponent = component;
		super.setLocationRelativeTo(component);
	}

	@Override
	public void setVisible(boolean visible){
		if(!isVisible()) {
			if (relativeComponent == null) {
				setLocationRelativeTo(MainFrame.getInstance());
			} else {
				setLocationRelativeTo(relativeComponent);
			}
		}
		super.setVisible(visible);
	}

}
