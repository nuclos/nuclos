//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.main.mainframe.desktop;

import java.awt.Color;
import java.awt.Graphics2D;

import javax.swing.ImageIcon;

import org.nuclos.client.theme.NuclosThemeSettings;
import org.nuclos.common2.LangUtils;

public class DesktopBackgroundPainter {
	
	public static final DesktopBackgroundPainter DEFAULT = new DesktopBackgroundPainter(null, null, null, null, null, null, null, null, null, null, null);
	public static final DesktopBackgroundPainter TRANSPARENT = new DesktopBackgroundPainter(null, null, null, null, null, null, null, null, null, null, null, true);

	private final Color backgroundColor;
	private final ImageIcon backgroundImage;
	private final ImageIcon bgNorthWestImage;
	private final ImageIcon bgNorthImage;
	private final ImageIcon bgNorthEastImage;
	private final ImageIcon bgWestImage;
	private final ImageIcon bgCenterImage;
	private final ImageIcon bgEastImage;
	private final ImageIcon bgSouthWestImage;
	private final ImageIcon bgSouthImage;
	private final ImageIcon bgSouthEastImage;
	private final boolean transparent;
	
	public DesktopBackgroundPainter(Color backgroundColor, ImageIcon backgroundImage, ImageIcon bgNorthWestImage, ImageIcon bgNorthImage, ImageIcon bgNorthEastImage, ImageIcon bgWestImage, ImageIcon bgCenterImage, ImageIcon bgEastImage, ImageIcon bgSouthWestImage, ImageIcon bgSouthImage, ImageIcon bgSouthEastImage) {
		this(backgroundColor, backgroundImage, bgNorthWestImage, bgNorthImage, bgNorthEastImage, bgWestImage, bgCenterImage, bgEastImage, bgSouthWestImage, bgSouthImage, bgSouthEastImage, false);
	}
	
	public DesktopBackgroundPainter(Color backgroundColor, ImageIcon backgroundImage, ImageIcon bgNorthWestImage, ImageIcon bgNorthImage, ImageIcon bgNorthEastImage, ImageIcon bgWestImage, ImageIcon bgCenterImage, ImageIcon bgEastImage, ImageIcon bgSouthWestImage, ImageIcon bgSouthImage, ImageIcon bgSouthEastImage, boolean transparent) {
		super();
		this.backgroundColor = backgroundColor;
		this.backgroundImage = backgroundImage;
		this.bgNorthWestImage = bgNorthWestImage;
		this.bgNorthImage = bgNorthImage;
		this.bgNorthEastImage = bgNorthEastImage;
		this.bgWestImage = bgWestImage;
		this.bgCenterImage = bgCenterImage;
		this.bgEastImage = bgEastImage;
		this.bgSouthWestImage = bgSouthWestImage;
		this.bgSouthImage = bgSouthImage;
		this.bgSouthEastImage = bgSouthEastImage;
		this.transparent = transparent;
	}

	public void paint(Graphics2D g, int width, int height) {
		if (!transparent) {
			g.setPaint(backgroundColor != null ? backgroundColor : NuclosThemeSettings.BACKGROUND_PANEL);
			g.fillRect(0, 0, width, height);
		}
		
		if (backgroundImage != null) {
			final int icoHeight = backgroundImage.getIconHeight();
			final int icoWidth = backgroundImage.getIconWidth();
			for (int i = 0; i < width / icoWidth + 1; i++) {
				for (int j = 0; j < height / icoHeight + 1; j++) {
					g.drawImage(backgroundImage.getImage(), i * icoWidth, j * icoHeight, null);
				}
			}
		}
		if (bgNorthWestImage != null) {
			g.drawImage(bgNorthWestImage.getImage(), 0, 0, null);
		}
		if (bgNorthImage != null) {
			final int icoWidth = bgNorthImage.getIconWidth();
			g.drawImage(bgNorthImage.getImage(), width/2 - icoWidth/2, 0, null);
		}
		if (bgNorthEastImage != null) {
			final int icoWidth = bgNorthEastImage.getIconWidth();
			g.drawImage(bgNorthEastImage.getImage(), width - icoWidth, 0, null);
		}
		if (bgWestImage != null) {
			final int icoHeight = bgWestImage.getIconHeight();
			g.drawImage(bgWestImage.getImage(), 0, height/2 - icoHeight/2, null);
		}
		if (bgCenterImage != null) {
			final int icoHeight = bgCenterImage.getIconHeight();
			final int icoWidth = bgCenterImage.getIconWidth();
			g.drawImage(bgCenterImage.getImage(), width/2 - icoWidth/2, height/2 - icoHeight/2, null);
		}
		if (bgEastImage != null) {
			final int icoHeight = bgEastImage.getIconHeight();
			final int icoWidth = bgEastImage.getIconWidth();
			g.drawImage(bgEastImage.getImage(), width - icoWidth, height/2 - icoHeight/2, null);
		}
		if (bgSouthWestImage != null) {
			final int icoHeight = bgSouthWestImage.getIconHeight();
			g.drawImage(bgSouthWestImage.getImage(), 0, height - icoHeight, null);
		}
		if (bgSouthImage != null) {
			final int icoHeight = bgSouthImage.getIconHeight();
			final int icoWidth = bgSouthImage.getIconWidth();
			g.drawImage(bgSouthImage.getImage(), width/2 - icoWidth/2, height - icoHeight, null);
		}
		if (bgSouthEastImage != null) {
			final int icoHeight = bgSouthEastImage.getIconHeight();
			final int icoWidth = bgSouthEastImage.getIconWidth();
			g.drawImage(bgSouthEastImage.getImage(), width - icoWidth, height - icoHeight, null);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof DesktopBackgroundPainter) {
			return LangUtils.equal(((DesktopBackgroundPainter) obj).backgroundColor, this.backgroundColor) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).backgroundImage, this.backgroundImage) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).bgNorthWestImage, this.bgNorthWestImage) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).bgNorthImage, this.bgNorthImage) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).bgNorthEastImage, this.bgNorthEastImage) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).bgWestImage, this.bgWestImage) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).bgCenterImage, this.bgCenterImage) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).bgEastImage, this.bgEastImage) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).bgSouthWestImage, this.bgSouthWestImage) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).bgSouthImage, this.bgSouthImage) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).bgSouthEastImage, this.bgSouthEastImage) &&
				   LangUtils.equal(((DesktopBackgroundPainter) obj).transparent, this.transparent);
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hashCode(backgroundColor) ^ LangUtils.hashCode(backgroundImage) ^ LangUtils.hashCode(bgNorthWestImage)
				^ LangUtils.hashCode(bgNorthImage) ^ LangUtils.hashCode(bgNorthEastImage) ^LangUtils.hashCode(bgWestImage) ^ LangUtils.hashCode(bgCenterImage)
				^ LangUtils.hashCode(bgEastImage) ^ LangUtils.hashCode(bgSouthWestImage) ^ LangUtils.hashCode(bgSouthImage) ^ LangUtils.hashCode(bgSouthEastImage) ^ LangUtils.hashCode(transparent);
	}
}
