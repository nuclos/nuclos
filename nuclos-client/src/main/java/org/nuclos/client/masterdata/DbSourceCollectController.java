//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.awt.Font;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.nuclos.client.common.NuclosCollectableTabbingTextArea;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.CollectStateListener;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

/**
 * <code>CollectController</code> for entity "Nuclet".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Maik.Stueker@novabit.de">Maik Stueker</a>
 * @version 01.00.00
 */
public class DbSourceCollectController extends MasterDataCollectController<UID> {

	private MasterDataVO<UID> voDbObj;

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public DbSourceCollectController(MainFrameTab tabIfAny) {
		super(E.DBSOURCE.getUID(), tabIfAny, null);
		this.getCollectStateModel().addCollectStateListener(new CollectStateListener() {

			@Override
			public void searchModeLeft(CollectStateEvent ev)
					throws CommonBusinessException {
			}

			@Override
			public void searchModeEntered(CollectStateEvent ev)
					throws CommonBusinessException {
				((NuclosCollectableTabbingTextArea)((List<CollectableComponent>) getSearchPanel().getEditView()
						.getCollectableComponentsFor(E.DBSOURCE.source.getUID())).get(0)).getJTextArea().setEditable(true);
				((NuclosCollectableTabbingTextArea)((List<CollectableComponent>) getSearchPanel().getEditView()
						.getCollectableComponentsFor(E.DBSOURCE.dropstatement.getUID())).get(0)).getJTextArea().setEditable(true);
			}

			@Override
			public void resultModeLeft(CollectStateEvent ev)
					throws CommonBusinessException {
			}

			@Override
			public void resultModeEntered(CollectStateEvent ev)
					throws CommonBusinessException {				
			}

			@Override
			public void detailsModeLeft(CollectStateEvent ev)
					throws CommonBusinessException {
			}

			@Override
			public void detailsModeEntered(CollectStateEvent ev)
					throws CommonBusinessException {
				boolean bEnable = ev.getNewCollectState().isDetailsModeNew();
				getDetailCollectableComponentsFor(E.DBSOURCE.dbtype.getUID()).get(0).setEnabled(bEnable);
				getDetailCollectableComponentsFor(E.DBSOURCE.dbobject.getUID()).get(0).setEnabled(bEnable);
			}
		});
		
		((NuclosCollectableTabbingTextArea)((List<CollectableComponent>) getSearchPanel().getEditView()
				.getCollectableComponentsFor(E.DBSOURCE.source.getUID())).get(0)).getJTextArea().setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
		((NuclosCollectableTabbingTextArea)((List<CollectableComponent>) getSearchPanel().getEditView()
				.getCollectableComponentsFor(E.DBSOURCE.dropstatement.getUID())).get(0)).getJTextArea().setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
		((NuclosCollectableTabbingTextArea)((List<CollectableComponent>) getDetailsPanel().getEditView()
				.getCollectableComponentsFor(E.DBSOURCE.source.getUID())).get(0)).getJTextArea().setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
		((NuclosCollectableTabbingTextArea)((List<CollectableComponent>) getDetailsPanel().getEditView()
				.getCollectableComponentsFor(E.DBSOURCE.dropstatement.getUID())).get(0)).getJTextArea().setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> insertCollectable(
			CollectableMasterDataWithDependants<UID> clctNew)
					throws CommonBusinessException {
		try {
			final UID uidDbSource = (UID)clctNew.getField(E.DBSOURCE.dbobject.getUID()).getValueId();
			if (null == uidDbSource) {
				throw new IllegalStateException("missing uid for db object");
			}

			this.voDbObj = MasterDataDelegate.getInstance().get(E.DBOBJECT.getUID(), uidDbSource);
			// Validation
			// @FIXME it might be better to access the ValidationSupport
			validateSource(clctNew, E.DBSOURCE.source.getUID());
			validateSource(clctNew, E.DBSOURCE.dropstatement.getUID());
			return super.insertCollectable(clctNew);
		} catch (CommonFinderException e) {
			throw new NuclosFatalException(e.getMessage(), e);
		} catch (CommonPermissionException e) {
			throw new NuclosFatalException(e.getMessage(), e);
		}
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(
			CollectableMasterDataWithDependants<UID> clct,
			Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {


		try {
			final UID uidDbSource = (UID)clct.getField(E.DBSOURCE.dbobject.getUID()).getValueId();
			if (null == uidDbSource) {
				throw new IllegalStateException("missing uid for db object");
			}

			this.voDbObj = MasterDataDelegate.getInstance().get(E.DBOBJECT.getUID(), uidDbSource);

			// Validation
			// @FIXME it might be better to access the ValidationSupport
			validateSource(clct, E.DBSOURCE.source.getUID());
			validateSource(clct, E.DBSOURCE.dropstatement.getUID());

			return super.updateCollectable(clct, oAdditionalData, applyMultiEditContext);
		} catch (CommonFinderException e) {
			throw new NuclosFatalException(e.getMessage(), e);
		} catch (CommonPermissionException e) {
			throw new NuclosFatalException(e.getMessage(), e);
		}


	}
	/**
	 * validate if field contains the name of the database object
	 * 
	 * @param clct		CollectableMasterDataWithDependants
	 * @param uidField	field {@link UID}
	 * 
	 * @throws NuclosBusinessException
	 */
	private final void validateSource(CollectableMasterDataWithDependants<UID> clct, UID uidField) throws NuclosBusinessException {
		CollectableComponentModel model = getDetailsPanel().getLayoutRoot().getCollectableComponentModelFor(uidField);
		String value = (String)model.getField().getValue();
		// check if field is not empty/not null
		if (!StringUtils.looksEmpty(value)) {
			if (null == this.voDbObj) {
				throw new IllegalStateException();
			}

			// check if field contains the name of the database object
			final String name = this.voDbObj.getFieldValue(E.DBOBJECT.name);
			int idx = ((String)value).indexOf(name);
			if (idx < 0) {
				// name not found
				final String fieldName = MetaDataDelegate.getInstance().getEntityField(uidField).getFieldName();
				throw new NuclosBusinessRuleException(StringUtils.getParameterizedExceptionMessage("masterdata.dbsource.error.validation.format", fieldName, name));
			}
		}
	}
}	 
