//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.user;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.common.SelectUserController;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.ldap.LDAPDataDelegate;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.ui.CommonAbstractAction;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.model.ChoiceList;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ejb3.PreferencesFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVOWrapper;

/**
 * <code>CollectController</code> for entity "user".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version 01.00.00
 */
public class UserCollectController extends MasterDataCollectController<UID> {

	private static final Logger LOG = Logger.getLogger(UserCollectController.class);

	public final static String FIELD_PREFERENCES = "preferences";
	public final static String FIELD_PASSWORD = "password";

	protected final boolean ldapAuthentication = isLdapAuthenticationEnabled();
	protected final boolean ldapSynchronization = isLdapSynchronizationEnabled();

	private final UserDelegate userDelegate = UserDelegate.getInstance();

	protected LDAPDataDelegate ldapdelegate = null;
	private List<MasterDataVO<UID>> ldapRegisteredUsers = null;

	private final CollectableComponentModelListener ccml_locked = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			Boolean locked = LangUtils.defaultIfNull((Boolean)ev.getOldValue().getValue(), Boolean.FALSE);
			Integer attempts = LangUtils.defaultIfNull((Integer) 
					getDetailsComponentModel(E.USER.loginAttempts.getUID()).getField().getValue(), new Integer(0));
			if (locked && attempts > 0) {
				getDetailsComponentModel(E.USER.loginAttempts.getUID()).setField(new CollectableValueField(new Integer(0)));
			}
		}
	};

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
 	 * @deprecated You should normally do sth. like this:<pre><code>
 	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
     */
	public UserCollectController(MainFrameTab tabIfAny) {
		super(E.USER, tabIfAny, null);
		if (this.ldapSynchronization) {
			this.ldapdelegate = LDAPDataDelegate.getInstance();
		}
	}

	@Override
	protected void initialize(CollectPanel<UID,CollectableMasterDataWithDependants<UID>> pnlCollect) {
		super.initialize(pnlCollect);
		final UID entity = E.USER.getUID();

		this.getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {

			@Override
			public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.locked.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.locked.getUID())
							.addCollectableComponentModelListener(null, ccml_locked);
				}
			}

			@Override
			public void detailsModeLeft(CollectStateEvent ev) throws CommonBusinessException {
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.locked.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.locked.getUID()).removeCollectableComponentModelListener(ccml_locked);
				}
			}
		});
	}

	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		userDelegate.remove(new UserVO(clct.getMasterDataCVO()));
	}

	protected void setupDetailsToolBar() {
		if(this.ldapSynchronization){
			final JButton btnSynchronizeWithLDAP = new JButton();
			btnSynchronizeWithLDAP.setName("btnSynchronizeWithLDAP");
			btnSynchronizeWithLDAP.setIcon(Icons.getInstance().getIconLDAP());
			btnSynchronizeWithLDAP.setToolTipText(getSpringLocaleDelegate().getMessage(
					"UserCollectController.1", "Mit LDAP synchronisieren"));

			// action: Select Columns
			btnSynchronizeWithLDAP.setAction(new CommonAbstractAction(btnSynchronizeWithLDAP) {
				@Override
				public void actionPerformed(ActionEvent ev) {
					UserCollectController.this.cmdSynchronizeUser();
				}
			});

			this.getDetailsPanel().addToolBarComponent(btnSynchronizeWithLDAP);
		}
	}

	/**
	 * command: select columns
	 * Lets the user select the columns to show in the result list.
	 * @throws CommonBusinessException
	 *
	 * @deprecated Move to ResultController hierarchy.
	 */
	private void cmdSynchronizeUser() {
		if(!synchronizeWithLDAP()){
			return;
		}
		final SelectUserController<MasterDataVO<UID>> ctl = new SelectUserController<MasterDataVO<UID>>(getTab(), 
				getSpringLocaleDelegate().getMessage("UserCollectController.2", "LDAP Benutzer"),
				getSpringLocaleDelegate().getMessage(
						"UserCollectController.3", "Ausgew\u00e4hlte Benutzer synchronisieren"), null, null);

		final List<MasterDataVO<UID>> lstAvailable = ldapRegisteredUsers;

		final JTable tbl = getResultTable();

		final ChoiceList<MasterDataVO<UID>> ro = new ChoiceList<MasterDataVO<UID>>();
		ro.set(lstAvailable, new Comparator<MasterDataVO<UID>>() {

			@Override
			public int compare(MasterDataVO<UID> o1, MasterDataVO<UID> o2) {
				return RigidUtils.compare(o1.toString(), o2.toString());
			}			
			
		});
		ctl.setModel(ro);
		final boolean bOK = ctl.run(getSpringLocaleDelegate().getMessage(
				"SelectUserController.7", "Mit LDAP Synchronisieren"));

		if (bOK) {
			UIUtils.runCommand(getTab(), new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					final int iSelectedRow = tbl.getSelectedRow();
					final List<MasterDataVOWrapper<UID>> selected = ctl.getSelectedColumns();
					for(MasterDataVOWrapper<UID> selectedWrapper : selected){
						if(selectedWrapper.isWrapped()){
							selectedWrapper.setPrimaryKey(null);
							if(selectedWrapper.getDependents() != null){
								for(IDependentKey dependentKey : selectedWrapper.getDependents().getKeySet()) {
									for(EntityObjectVO<?> dependant : selectedWrapper.getDependents().getData(dependentKey)) {
										dependant.setPrimaryKey(null);
									}
								}
							}

							if (selectedWrapper.getFieldValue(E.USER.superuser) == null) {
								selectedWrapper.setFieldValue(E.USER.superuser, false);
							}
							if (selectedWrapper.getFieldValue(E.USER.loginWithEmailAllowed) == null) {
								selectedWrapper.setFieldValue(E.USER.loginWithEmailAllowed, false);
							}
							if (selectedWrapper.getFieldValue(E.USER.privacyConsentAccepted) == null) {
								selectedWrapper.setFieldValue(E.USER.privacyConsentAccepted, false);
							}

							UserVO user = new UserVO(selectedWrapper);
							user.setSetPassword(true);
							user.setNewPassword(RandomStringUtils.random(20));
							userDelegate.create(user, selectedWrapper.getDependents());
						} else {
							if(selectedWrapper.isMapped()){
								selectedWrapper.replaceNativeFields();
								//TODO collective processing possible?
								mddelegate.update(E.USER.getUID(), selectedWrapper, selectedWrapper.getDependents(), null, false);
							} else {
								if(selectedWrapper.isNative()){
									mddelegate.remove(E.USER.getUID(), selectedWrapper, null);
								}
							}
						}

					}
					// refresh the result:
					getResultController().getSearchResultStrategy().refreshResult();

					// reselect the previously selected row (which gets lost be refreshing the model)
					if (iSelectedRow != -1) {
						tbl.setRowSelectionInterval(iSelectedRow, iSelectedRow);
					}

					//restoreColumnWidths(ctl.getSelectedColumns(), mpWidths);
				}
			});
		}
	}

	private boolean synchronizeWithLDAP() {
		final boolean[] synchronizedWithLDAP = new boolean[] {false};
		UIUtils.runCommand(this.getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				try {
					Collection<MasterDataVOWrapper<UID>> mdwrapperlst = ldapdelegate.getUsers();
					filterOutLDAPUsers(mdwrapperlst);
					synchronizedWithLDAP[0] = true;
				} catch (CollectableFieldFormatException e) {
					LOG.warn("synchronizeWithLDAP failed: " + e, e);
					final String sMessage = getSpringLocaleDelegate().getMessage("UserCollectController.4",
						"LDAP Synchronisierung ist gescheitert.\nEine Liste der in LDAP registrierten Benutzer kann nicht dargestellt werden.");
					Errors.getInstance().showExceptionDialog(getTab(), sMessage, e);
					synchronizedWithLDAP[0] = false;
				}
			}
		});
		return synchronizedWithLDAP[0];
	}

	private void filterOutLDAPUsers(Collection<MasterDataVOWrapper<UID>> ldapusers) throws CommonBusinessException {
		final CollectableSearchCondition currentcondition = getSearchStrategy().getCollectableSearchCondition();

		if (currentcondition != null) {
			final CollectableSearchCondition condition = SearchConditionUtils.not(currentcondition);
			final Collection<MasterDataVO<UID>> usersToRemove = mddelegate.getMasterData(getEntityUid(), condition);

			final List<UID> namestoremove = CollectionUtils.transform(usersToRemove, new Transformer<MasterDataVO<UID>, UID>() {
				@Override
				public UID transform(MasterDataVO<UID> i) {
					return i.getPrimaryKey();
				}
			});

			CollectionUtils.removeAll(ldapusers, new Predicate<MasterDataVOWrapper<UID>>() {
				@Override
				public boolean evaluate(MasterDataVOWrapper<UID> t) {
					return namestoremove.contains(t.getPrimaryKey());
				}
			});
		}

		this.ldapRegisteredUsers = new ArrayList<MasterDataVO<UID>>(ldapusers);
	}

	private static boolean isLdapAuthenticationEnabled() {
		return SecurityDelegate.getInstance().isLdapAuthenticationActive();
	}

	private static boolean isLdapSynchronizationEnabled() {
	 	return SecurityDelegate.getInstance().isLdapSynchronizationActive();
	}
}	// class UserCollectController
