//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Collectable fields provider for masterdata fields belonging to a certain (leased object) module. 
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:uwe.allner@novabit.de">uwe.allner</a>
 * @version 01.00.00
 */
public class AllCollectableEntityFieldsCollectableFieldsProvider implements CollectableFieldsProvider  {
	
	private static final Logger LOG = Logger.getLogger(AllCollectableEntityFieldsCollectableFieldsProvider.class);
	
	public static final String ENTITY_UID = "moduleId";
	
	//
	
	private UID entityUID;
	
	/**
	 * @deprecated
	 */
	AllCollectableEntityFieldsCollectableFieldsProvider() {
	}
	
	public AllCollectableEntityFieldsCollectableFieldsProvider(UID entityUID) {
		setParameter(ENTITY_UID, entityUID);
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (entityUID == null) {
			throw new NullPointerException();
		}
		if (sName.equals(ENTITY_UID)) {
			this.entityUID = (UID) oValue;
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> result = new ArrayList<CollectableField>();

		// add subentities' fields, if any:
		//@TODO MultiNuclet. method is missing.
		/*final Collection<EntityMeta<?>> collSubEntities = MasterDataDelegate.getInstance().getMetaDataByModuleId(iModuleId);
		for (EntityMeta<?> mdmcvoSubEntity : collSubEntities) {
			if(!mdmcvoSubEntity.isDynamic() && mdmcvoSubEntity.isEditable()) {
				final Collection<FieldMeta<?>> lstFieldVO = mdmcvoSubEntity.getFields();
				for(FieldMeta<?> fieldVO : lstFieldVO) {
					//result.add(new CollectableValueIdField(fieldVO.getId(), mdmcvoSubEntity.getLabel() + "." + fieldVO.getLabel()));
					result.add(new CollectableValueIdField(fieldVO.getUID(), 
							SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(mdmcvoSubEntity) + "." 
							+ SpringLocaleDelegate.getInstance().getResource(fieldVO.getLocaleResourceIdForLabel(), fieldVO.getFallbackLabel())));
				}
			}
		}*/

		Collections.sort(result);
		return result;
	}
}
