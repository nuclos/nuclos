package org.nuclos.client.masterdata.valuelistprovider;

import java.util.List;

import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common2.exception.CommonBusinessException;

public class AttributeColorSQLWhitelistCollectableFieldsProvider implements CollectableFieldsProvider {
	@Override
	public void setParameter(final String sName, final Object oValue) {
		//do nothing
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// this is just a dummy VLP
		return null;
	}
}
