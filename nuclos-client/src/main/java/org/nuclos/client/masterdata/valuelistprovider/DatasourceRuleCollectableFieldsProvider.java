package org.nuclos.client.masterdata.valuelistprovider;

import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DatasourceRuleCollectableFieldsProvider implements CollectableFieldsProvider {
    @Override
    public List<CollectableField> getCollectableFields() throws CommonBusinessException {
        return _getCollectableFields();
    }

    private <PK> List<CollectableField> _getCollectableFields() {
        List<CollectableField> result = new ArrayList<>();

        try {
            for (EventSupportSourceVO essVO : EventSupportRepository.getInstance().getAllEventSupports()) {
                if (!essVO.isActive()) {
                    continue;
                }
                if (essVO.getInterface().contains("org.nuclos.api.rule.DatasourceRule")) {
                    result.add(new CollectableValueIdField(essVO.getPrimaryKey(), essVO.getClassname()));
                }
            }
        } catch (CommonBusinessException cbe) {
            throw new CommonFatalException(cbe);
        }

        Collections.sort(result);
        return result;
    }

    @Override
    public void setParameter(String sName, Object oValue) {
    }
}
