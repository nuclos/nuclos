//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.NuclosVLPException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value list provider to get dependant data of an entity.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 * @version 00.01.000
 */
public class DependantsCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(DependantsCollectableFieldsProvider.class);
	
	public static final String FOREIGNKEY_FIELD_UID = "foreignKeyFieldName";
	
	public static final String RELATED_ENTITY_ID = "relatedEntityId";
	
	public static final String ENTITY_UID = "entityField";
	
	public static final String ENTITY_FIELD_UID = "entityField";
	
	private Object relatedEntityId = null;
	private UID foreignKeyFieldUid = null;
	private UID entityUid = null;
	private UID entityFieldUid = null;
	
	/**
	 * @deprecated
	 */
	DependantsCollectableFieldsProvider() {
	}
	
	public DependantsCollectableFieldsProvider(UID entityUid, UID entityFieldUid, UID foreignKeyFieldUid, Object relatedEntityId) {
		setParameter(ENTITY_UID, entityUid);
		setParameter(ENTITY_FIELD_UID, entityFieldUid);
		setParameter(FOREIGNKEY_FIELD_UID, foreignKeyFieldUid);
		setParameter(RELATED_ENTITY_ID, relatedEntityId);
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (ENTITY_UID.equals(sName)) {
			this.entityUid = (UID) oValue;
		}
		else if (FOREIGNKEY_FIELD_UID.equals(sName)) {
			this.foreignKeyFieldUid = (UID) oValue;
		}
		else if(RELATED_ENTITY_ID.equals(sName)) {
			this.relatedEntityId = oValue;
		}
		else if (ENTITY_FIELD_UID.equals(sName)) {
			this.entityFieldUid = (UID) oValue;
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws NuclosVLPException {
		LOG.debug("getCollectableFields");
		
		final List<CollectableField> result = new ArrayList<CollectableField>();
		
		if (entityUid == null) {
			throw new NuclosVLPException(SpringLocaleDelegate.getInstance().getMessage(
					"DependantsCollectableFieldsProvider.1", 
					"Der Parameter 'entity' des Valuelistproviders 'dependants' ist nicht gef\u00fcllt!"));
		}
		if (foreignKeyFieldUid == null) {
			throw new NuclosVLPException(SpringLocaleDelegate.getInstance().getMessage(
					"DependantsCollectableFieldsProvider.2",
					"Der Parameter 'foreignKeyFieldName' des Valuelistproviders 'dependants' ist nicht gef\u00fcllt!"));
		}
		if (entityFieldUid == null) {
			throw new NuclosVLPException(SpringLocaleDelegate.getInstance().getMessage(
					"DependantsCollectableFieldsProvider.3",
					"Der Parameter 'entityField' des Valuelistproviders 'dependants' ist nicht gef\u00fcllt!"));
		}
		
		if (relatedEntityId == null) {
			for (MasterDataVO<?> mdVO : MasterDataCache.getInstance().get(entityUid)) {
				result.add(new CollectableValueIdField(mdVO.getId(), mdVO.getFieldValue(entityFieldUid)));
			}
		}
		else {
			for (EntityObjectVO<?> mdVO : MasterDataDelegate.getInstance().getDependentDataCollection(
					entityUid, foreignKeyFieldUid, null, relatedEntityId)) {
				result.add(new CollectableValueIdField(mdVO.getId(), mdVO.getFieldValue(entityFieldUid, Object.class)));
			}
		}	
		Collections.sort(result);
		return result;
	}
	
}
