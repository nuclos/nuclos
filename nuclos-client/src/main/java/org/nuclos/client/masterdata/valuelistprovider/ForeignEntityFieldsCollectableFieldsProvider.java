//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Value list provider for all foreign entity fields (reference fields) of an entity
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version	01.00.00
 */
public class ForeignEntityFieldsCollectableFieldsProvider implements CollectableFieldsProvider{

	private static final Logger LOG = Logger.getLogger(ForeignEntityFieldsCollectableFieldsProvider.class);
	
	public static final String SUBFORM_UID = "subform";
	
	//
	
	private UID entityUid;
	private UID subformUid;
	
	/**
	 * @deprecated
	 */
	ForeignEntityFieldsCollectableFieldsProvider() {
	}
	
	public ForeignEntityFieldsCollectableFieldsProvider(UID entityUid, UID subformUid) {
		setParameter(NuclosConstants.VLP_ENTITY_UID_PARAMETER, entityUid);
		setParameter(SUBFORM_UID, subformUid);
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (sName.equals(NuclosConstants.VLP_ENTITY_UID_PARAMETER)) {
			this.entityUid = (UID) oValue;
		} else if (sName.equals(SUBFORM_UID)) {
			this.subformUid = (UID) oValue;
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}
	
	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// LOG.debug("getCollectableFields");
		final MetaProvider mProv = MetaProvider.getInstance();
		final Collection<FieldMeta<?>> collmdmetafieldsvo = new ArrayList<FieldMeta<?>>();
		if (subformUid != null) {
			for (FieldMeta<?> fieldVO: mProv.getEntity(subformUid).getFields()) {
				if (fieldVO.getForeignEntity() != null && fieldVO.getForeignEntity().equals(entityUid)) {
					collmdmetafieldsvo.add(fieldVO);
				}
			}
		}
		final List<CollectableField> result = CollectionUtils.transform(collmdmetafieldsvo,
				new Transformer<FieldMeta<?>, CollectableField>() {
					@Override
					public CollectableField transform(FieldMeta<?> mdmetafieldvo) {
						return new CollectableValueField(mdmetafieldvo.getUID());
					}
				});
		Collections.sort(result);
		return result;
	}
}
