//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.valuelistprovider.cache.ManagedCollectableFieldsProvider;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * A <code>CollectableFieldsProvider</code> for master data entities in Nucleus.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class MasterDataCollectableFieldsProvider extends ManagedCollectableFieldsProvider implements CacheableCollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(MasterDataCollectableFieldsProvider.class);
	
	//
	
	private final UID entityUid;
	private String stringifiedFieldDefinition;
	
	public MasterDataCollectableFieldsProvider(UID entityUid, String stringifiedFieldDefinition, boolean ignoreValidity, UID mandator) {
		this(entityUid);
		setParameter(NuclosConstants.VLP_STRINGIFIED_FIELD_DEFINITION_PARAMETER, stringifiedFieldDefinition);
		setParameter(NuclosConstants.VLP_IGNORE_VALIDITY_PARAMETER, ignoreValidity);
		setMandator(mandator);
	}

	/**
	 * §precondition entityUid != null
	 * 
	 * @param entityUid
	 * 
	 * @deprecated Use {@link #MasterDataCollectableFieldsProvider(UID, String, boolean)} instead.
	 */
	public MasterDataCollectableFieldsProvider(UID entityUid) {
		if (entityUid == null) {
			throw new NullArgumentException("entityUid");
		}
		this.entityUid = entityUid;
		
	}

	/**
	 * valid parameters:
	 * <ul>
	 *   <li>"_searchmode" = collectable in search mask?</li>
	 *       (now org.nuclos.common.NuclosConstants.VLP_SEARCHMODE_PARAMETER)
	 *   <li>"fieldName" = referenced field name. Default is "name".</li>
	 *   	 (now NuclosConstants.VLP_STRINGIFIED_FIELD_DEFINITION_PARAMETER)
	 * </ul>
	 * @param sName parameter name
	 * @param oValue parameter value
	 * 
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (sName.equals(NuclosConstants.VLP_SEARCHMODE_PARAMETER)) {
			setIgnoreValidity((Boolean) oValue);
		} else if (sName.equals(NuclosConstants.VLP_STRINGIFIED_FIELD_DEFINITION_PARAMETER)) {
			stringifiedFieldDefinition = (String) oValue;
		} else if (sName.equals(NuclosConstants.VLP_IGNORE_VALIDITY_PARAMETER)) {
			setIgnoreValidity(Boolean.valueOf(oValue.toString()));
		} else if (sName.equals(NuclosConstants.VLP_MANDATOR_PARAMETER)) {
			setMandator((UID) oValue);
		} else if (sName.equals(NuclosConstants.VLP_ADDITIONAL_CONDITION_FOR_RESULTFILTER)) {
			additionalCondition = RigidUtils.uncheckedCast(oValue);
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	private Pair<UID, CollectableSearchCondition> additionalCondition;

	@Override
	public Object getCacheKey() {
		return Arrays.<Object>asList(
			entityUid,
			stringifiedFieldDefinition,
			this.getIgnoreValidity(),
			getMandator());
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		return (stringifiedFieldDefinition == null) 
				? getCollectableFields(this.entityUid, !this.getIgnoreValidity(), getMandator()) 
				: getCollectableFieldsByName(this.entityUid, stringifiedFieldDefinition, !this.getIgnoreValidity(), getMandator());
	}

	public List<CollectableField> getCollectableFields(boolean bValid) throws CommonBusinessException {
		return getCollectableFields(this.entityUid, !this.getIgnoreValidity() && bValid, getMandator());
	}

	public List<CollectableField> getCollectableFieldsByName(String stringifiedFieldDefinition, boolean bValid) throws CommonBusinessException {
		return getCollectableFieldsByName(this.entityUid, stringifiedFieldDefinition, !this.getIgnoreValidity() && bValid, getMandator());
	}

	private static List<CollectableField> getCollectableFields(UID entityUid, boolean bValid, UID mandator) throws CommonBusinessException {
		List<CollectableField> result = MasterDataCache.getInstance().getCollectableFields(entityUid, bValid, mandator);
		Collections.sort(result);
		return result;
	}

	private List<CollectableField> getCollectableFieldsByName(UID entityUid, String stringifiedFieldDefinition, boolean bValid, UID mandator) throws CommonBusinessException {
		List<CollectableField> result =  MasterDataCache.getInstance().getCollectableFieldsByName(entityUid,
				stringifiedFieldDefinition, bValid, mandator, additionalCondition);
		Collections.sort(result);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MasterDataCollectableFieldsProvider) {
			MasterDataCollectableFieldsProvider that = (MasterDataCollectableFieldsProvider)obj;
			return LangUtils.equal(getCacheKey(), that.getCacheKey());
			
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hashCode(getCacheKey());
	}
	
	@Override
	public String toString() {
		return "MDFieldsProvider[" + getCacheKey() + "] (" + hashCode() + ")";
	}
	
}	// class MasterDataCollectableFieldsProvider
