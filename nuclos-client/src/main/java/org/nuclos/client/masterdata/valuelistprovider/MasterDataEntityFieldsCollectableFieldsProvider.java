//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;

public class MasterDataEntityFieldsCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(MasterDataEntityFieldsCollectableFieldsProvider.class);
	
	// 
	
	// private HashMap<String, String> params = null;

	public MasterDataEntityFieldsCollectableFieldsProvider(){
		// params = new HashMap<String, String>();
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		// params.put(sName, oValue.toString());
		// LOG.info("setParameter : "+sName + " = " + oValue);
		LOG.info("Unknown parameter " + sName + " with value " + oValue);
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// LOG.debug("getCollectableFields");
		Collection<EntityMeta<?>> colmdmVO_menupath = new ArrayList<EntityMeta<?>>();
		Collection<EntityMeta<?>> colmdmVO = MetaProvider.getInstance().getAllEntities();
		
		// get and add entities with menupath
		for(EntityMeta<?> mdmVO : colmdmVO) {
			if (mdmVO.getLocaleResourceIdForMenuPath() != null) {
				colmdmVO_menupath.add(mdmVO);
			}
		}
		colmdmVO_menupath.add(E.GENERATIONSUBENTITY);
		final List<CollectableField> result = CollectionUtils.transform(colmdmVO_menupath, new Transformer<EntityMeta<?>, CollectableField>() {
			@Override
			public CollectableField transform(EntityMeta<?> mdmVO) {
				return new CollectableValueIdField(mdmVO.getUID(), 
						SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(mdmVO));
			}
		});
		Collections.sort(result);
		return result;
	}

}
