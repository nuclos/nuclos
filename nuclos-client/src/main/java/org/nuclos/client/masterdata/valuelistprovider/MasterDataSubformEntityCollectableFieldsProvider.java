//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Value list provider for all subform entity names of a masterdata entity
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version	01.00.00
 */
public class MasterDataSubformEntityCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(MasterDataSubformEntityCollectableFieldsProvider.class);
	
	public static final String ENTITY_UID = "masterdata";
	
	private UID masterdataUid = null;
	
	/**
	 * @deprecated
	 */
	MasterDataSubformEntityCollectableFieldsProvider() {
	}
	
	public MasterDataSubformEntityCollectableFieldsProvider(UID masterdataUid) {
		setParameter(ENTITY_UID, masterdataUid);
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (sName.equals(ENTITY_UID)) {
			this.masterdataUid = (UID) oValue;
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}
	
	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		LOG.debug("getCollectableFields");
		Collection<EntityMeta<?>> collmdmetavo = new ArrayList<EntityMeta<?>>();
		for (UID subformUid : MasterDataDelegate.getInstance().getSubFormEntityUidsByMasterDataEntity(
				masterdataUid, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY))) {
			collmdmetavo.add(MetaProvider.getInstance().getEntity(subformUid));
		}				

		final List<CollectableField> result = CollectionUtils.transform(collmdmetavo, new Transformer<EntityMeta<?>, CollectableField>() {
			@Override
			public CollectableField transform(EntityMeta<?> mdmetavo) {
				return new CollectableValueField(mdmetavo.getUID());
			}
		});
		Collections.sort(result);
		return result;
	}
 	
}

