package org.nuclos.client.masterdata.valuelistprovider;

import org.nuclos.common.collect.collectable.CollectableFieldsProvider;

public interface NonCacheableCollectableFieldsProvider extends CollectableFieldsProvider {

}
