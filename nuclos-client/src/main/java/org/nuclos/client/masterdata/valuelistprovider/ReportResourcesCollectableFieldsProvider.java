package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.resource.ResourceDelegate;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.resource.valueobject.ResourceVO;

public class ReportResourcesCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(ReportResourcesCollectableFieldsProvider.class);

	public ReportResourcesCollectableFieldsProvider() {

	}

	@Override
	public void setParameter(final String sName, final Object oValue) {
		LOG.info("Unknown parameter " + sName + " with value " + oValue);
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		List<CollectableField> result = new ArrayList<>();
		for (UID resourceUID : ResourceDelegate.getInstance().getResourceUIDs()) {
			ResourceVO resource = ResourceDelegate.getInstance().getResourceById(resourceUID);
			if (resource.getFileName().endsWith(".xml") || resource.getFileName().endsWith(".jrxml")) {
				result.add(new CollectableValueIdField(resource.getId(), resource.getName()));
			}
		}

		Collections.sort(result);

		return result;
	}

}
