//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;

public class SubFormFieldsCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(SubFormFieldsCollectableFieldsProvider.class);
	
	//Do not change this string-identifier
	public static final String ENTITIY_UID = "entityId";
	
	//
	
	private UID entityUid;
	
	/**
	 * @deprecated
	 */
	SubFormFieldsCollectableFieldsProvider() {
	}
	
	public SubFormFieldsCollectableFieldsProvider(UID entityUid) {
		setParameter(ENTITIY_UID, entityUid);
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String name, Object value) {
		if (name.equals(ENTITIY_UID) && value != null) {
			entityUid = (UID) value;
		} else {
			LOG.info("Unknown parameter " + name + " with value " + value);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> result = new ArrayList<CollectableField>();
		Collection<EntityMeta<?>> collSubEntities = null;
		if (Modules.getInstance().isModule(entityUid)) {
			// collSubEntities = MasterDataDelegate.getInstance().getMetaDataByModuleId(entityUid);
			throw new UnsupportedOperationException();
		}
		else {
			collSubEntities = new ArrayList<EntityMeta<?>>();
			final Set<UID> subEntities = MasterDataDelegate.getInstance().getSubFormEntityUidsByMasterDataEntity(
					entityUid, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			for (UID subEntity : subEntities) {
				collSubEntities.add(MasterDataDelegate.getInstance().getMetaData(subEntity));
			}
		}

		for (EntityMeta<?> mdmcvoSubEntity : collSubEntities) {
			if(!mdmcvoSubEntity.isDynamic() && mdmcvoSubEntity.isEditable()) {
				final Collection<FieldMeta<?>> lstFieldVO = mdmcvoSubEntity.getFields();
				for(FieldMeta<?> fieldVO : lstFieldVO) {
					//result.add(new CollectableValueIdField(fieldVO.getId(), mdmcvoSubEntity.getLabel() + "." + fieldVO.getLabel()));
					result.add(new CollectableValueIdField(fieldVO.getUID(), 
							SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(
									mdmcvoSubEntity) + "." 
									+ SpringLocaleDelegate.getInstance().getResource(
									fieldVO.getLocaleResourceIdForLabel(), fieldVO.getFallbackLabel())));
				}
			}
		}
		Collections.sort(result);
		return result;
	}
}
