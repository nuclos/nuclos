//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.GenericObjectMetaDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Value list provider for all subform entity names of a module
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version	01.00.00
 */
public class SubformEntityCollectableFieldsProvider implements CollectableFieldsProvider{

	private static final Logger LOG = Logger.getLogger(SubformEntityCollectableFieldsProvider.class);
	
	public static final String ENTITIY_UID = "module";
	
	//
	
	private UID entityUid = null;
	
	/**
	 * @deprecated
	 */
	SubformEntityCollectableFieldsProvider() {
	}
	
	public SubformEntityCollectableFieldsProvider(UID entityUid) {
		setParameter(ENTITIY_UID, entityUid);
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
    public void setParameter(String sName, Object oValue) {
		if (sName.equals(ENTITIY_UID)) {
			this.entityUid = (UID) oValue;
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}
	
	@Override
    public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// LOG.debug("getCollectableFields");
		final MetaProvider mProv = MetaProvider.getInstance();
		final EntityMeta<?> entityMeta = mProv.getEntity(entityUid);
		Collection<EntityMeta<?>> collmdmetavo = new ArrayList<EntityMeta<?>>();
		/*
		if (Modules.getInstance().existModule(entityUid)) {
			iModuleId = Modules.getInstance().getModuleIdByEntityName(entityUid);
		}
	    else {
			//if no such module, then no subforms for this module (the module is new)
			iModuleId = -1;
		}
		 */
		// TODO multinuclet: Can (entityUid == null) ever be true?
		if (entityUid == null) {
			final GenericObjectMetaDataCache gomdc = GenericObjectMetaDataCache.getInstance();
			final MetaProvider mp = MetaProvider.getInstance();
			// TODO multinuclet: What does 'GenericObjectMetaDataCache.getInstance().getSubFormEntityNamesByModuleId(null)' return?
			for(UID subformUid : gomdc.getSubFormEntityNamesByModuleId(null)) {
				try {
					collmdmetavo.add(mp.getEntity(subformUid));
				} catch (Exception e) {
					// ignore! Subform does not exists any more...
				}
			}				
			final List<CollectableField> result = CollectionUtils.transform(collmdmetavo, new Transformer<EntityMeta<?>, CollectableField>() {
				@Override
                public CollectableField transform(EntityMeta<?> mdmetavo) {
					return new CollectableValueField(mdmetavo.getUID());
				}
			});
			Collections.sort(result);
			return result;
		}
		else {
			for(UID subformUid : GenericObjectMetaDataCache.getInstance().getSubFormEntityNamesByModuleId(entityUid)) {
				collmdmetavo.add(MasterDataDelegate.getInstance().getMetaData(subformUid));				
			}				
			final List<CollectableField> result = CollectionUtils.transform(collmdmetavo, new Transformer<EntityMeta<?>, CollectableField>() {
				@Override
                public CollectableField transform(EntityMeta<?> mdmetavo) {
					return new CollectableValueField(mdmetavo.getUID());
				}
			});
			Collections.sort(result);
			return result;
		}
	}
 	
}

