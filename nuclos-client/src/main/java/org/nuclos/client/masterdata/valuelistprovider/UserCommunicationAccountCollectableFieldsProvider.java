//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.client.genericobject.valuelistprovider.ISelfIdCollectableFieldsProvider;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.ejb3.SecurityFacadeRemote;

public class UserCommunicationAccountCollectableFieldsProvider implements
		CollectableFieldsProvider,
		ISelfIdCollectableFieldsProvider {
	
	public static final String REQUEST_CONTEXT = "RequestContext";
	
	public static final String USER = "User";
	
	private String requestContextClassName;
	private UID userUID;

	@Override
	public void setParameter(String sName, Object oValue) {
		if (REQUEST_CONTEXT.equals(sName)) {
			requestContextClassName = "org.nuclos.api.context.communication." + oValue;
		} else if (SELF_ID.equals(sName)) {
			if (oValue instanceof UID) {
				userUID = (UID) oValue;
			}
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		List<CollectableField> result = new ArrayList<CollectableField>();
		if (requestContextClassName != null && userUID != null) {
			result.addAll(SpringApplicationContextHolder.getBean(SecurityFacadeRemote.class).getUserCommunicationAccounts(userUID, requestContextClassName));
		}
		return result;
	}

}
