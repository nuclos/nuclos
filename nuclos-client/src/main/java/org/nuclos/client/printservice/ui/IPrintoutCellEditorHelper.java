package org.nuclos.client.printservice.ui;

import org.nuclos.common.report.valueobject.PrintServiceTO;

public interface IPrintoutCellEditorHelper {

	PrintServiceTO printServiceAtRow(int iRow);
	
}
