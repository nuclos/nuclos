//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.printservice.ui;

import javax.swing.tree.TreeNode;

import org.nuclos.common.UID;

/**
 * {@link PrintoutNodeUpdateVisitor} is used to 
 * trigger certain events on node update
 * 
 * @see NuclosPrintoutModel
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface PrintoutNodeUpdateVisitor {
	
	/**
	 * visit column update
	 * 
	 * @param idColumn column id
	 * @param node	   {@link TreeNode} node
	 * @param oldValue value old
	 * @param newValue value new
	 */
	public void visit(final UID idColumn, TreeNode node, Object oldValue, Object newValue);
}
