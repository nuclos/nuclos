//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.printservice.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.printout.Printout;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.common.E._Form.Outputtype;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintoutTO;
import org.nuclos.common.report.valueobject.ReportVO.OutputType;

/**
 * {@link PrintoutTreeNode} is {@link TreeNode} for {@link PrintoutTO}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintoutTreeNode implements TreeNode {

	private final List<OutputFormatTreeNode> children;
	private final PrintoutTO printout;

	private boolean choose;
	private final TreeNode fakeParent = new DefaultMutableTreeNode();
	
	/**
	 * get id of {@link Printout} {@link TreeNode}
	 * 
	 * @return id of {@link Printout} {@link TreeNode}
	 */
	public UID getId() {
		return printout.getId();
	}

	/**
	 * set id of {@link BusinessObject}
	 * 
	 * @param idBo {@link BusinessObject} id
	 */
	public void setBusinessObjectId(Long idBo) {
		printout.setBusinessObjectId(idBo);
	}

	/**
	 * get id of {@link BusinessObject}
	 * 
	 * @return {@link BusinessObject} id
	 */
	public Long getBusinessObjectId() {
		return printout.getBusinessObjectId();
	}
	
	/**
	 * get {@link Outputtype}
	 * 
	 * @return {@link Outputtype}
	 */
	public OutputType getOutputType() {
		return printout.getOutputType();
	}

	/**
	 * get list of {@link OutputFormat}
	 * 
	 * @return list of {@link OutputFormat}
	 */
	public List<OutputFormatTreeNode> getOutputFormats() {
		return children;
	}

	/**
	 * get {@link Printout} {@link TreeNode} name
	 * 
	 * @return {@link Printout} node name
	 */
	public String getName() {
		return printout.getName();
	}

	/**
	 * node to string
	 */
	public String toString() {
		return printout.getName();
	}

	/**
	 * create {@link Printout} {@link TreeNode}
	 * @param printout
	 */
	public PrintoutTreeNode(final PrintoutTO printout) {
		super();
		this.printout = printout;
		this.children = new ArrayList<OutputFormatTreeNode>();
		for (final OutputFormatTO outputFormat : printout.getOutputFormats()) {
			this.children.add(new OutputFormatTreeNode(this, outputFormat));
		}
	}
	
	/**
	 * get {@link PrintoutTO}
	 * 
	 * @return {@link PrintoutTO}
	 */
	public PrintoutTO getPrintout() {
		return this.printout;
	}

	/**
	 * is selected for printing by the user
	 * 
	 * @return is selected
	 */
	public boolean isChoose() {
		return choose;
	}

	/**
	 * set selected for printing by the user
	 * 
	 * @param choose is selected
	 */
	public void setChoose(boolean choose) {
		this.choose = choose;
		for (OutputFormatTreeNode outputVO : getOutputFormats()) {
			outputVO.setChoose(choose);
		}
	}

	/**
	 * get child {@link TreeNode}
	 */
	@Override
	public TreeNode getChildAt(int idxChild) {
		return children.get(idxChild);
	}

	/**
	 * get count of children
	 * 
	 * @return child count
	 */
	@Override
	public int getChildCount() {
		if (getOutputType() == OutputType.EXCEL) {
			return 0;
		}
		return children.size();
	}

	/**
	 * get parent {@link TreeNode}
	 * 
	 * @return {@link TreeNode}
	 */
	@Override
	public TreeNode getParent() {
		return fakeParent;
	}

	/**
	 * get index of {@link TreeNode}
	 * 
	 * @param node {@link TreeNode}
	 * 
	 * @return index of {@link TreeNode}
	 */
	@Override
	public int getIndex(TreeNode node) {
		return 0;
	}

	/**
	 * is {@link TreeNode} allowed to provide Children
	 * 
	 * @return allowed true/false
	 */
	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	/**
	 * is this node a leaf {@link TreeNode}
	 * 
	 * @return is leaf {@link TreeNode}
	 */
	@Override
	public boolean isLeaf() {
		if (getOutputType() == OutputType.EXCEL) {
			return true;
		}
		return false;
	}

	
	/**
	 * get children {@link TreeNode} as {@link Enumeration}
	 * 
	 * @return {@link Enumeration} of {@link TreeNode}
	 */
	@Override
	public Enumeration<OutputFormatTreeNode> children() {
		return Collections.enumeration(children);
	}

}
