package org.nuclos.client.remote;

import java.util.concurrent.Callable;

import org.nuclos.common.http.RequestConfigHolder;

public class NoConnectionTimeoutRunner {
	public static synchronized <T> T runSynchronized(Callable<T> callable) throws Exception {
		RequestConfigHolder.getInstance().setSocketTimeoutForCurrentThread(-1);
		try {
			return callable.call();
		} finally {
			RequestConfigHolder.getInstance().resetSocketTimeoutForCurrentThread();
		}
	}
}
