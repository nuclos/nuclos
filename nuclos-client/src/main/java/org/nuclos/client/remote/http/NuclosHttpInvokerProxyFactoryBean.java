//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.remote.http;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.mandator.ClientMandatorContext;
import org.nuclos.client.remote.NuclosHttpInvokerAttributeContext;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;
import org.springframework.remoting.support.RemoteInvocation;
import org.springframework.remoting.support.RemoteInvocationResult;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

/**
 * An extension to Spring HttpInvokerProxyFactoryBean used for remote calls from 
 * nuclos client to server.
 */
public class NuclosHttpInvokerProxyFactoryBean extends HttpInvokerProxyFactoryBean {

	private static final Logger LOG = Logger.getLogger(NuclosHttpInvokerProxyFactoryBean.class);
	
	/**
	 * Whether to turn INFO logging of profiling data on.
	 * It should be true only in production environments.
	 */
	private static final boolean PROFILE = false;
	
	/**
	 * Minimum delay for calls in order to appear in LOG. 
	 */
	private static final long PROFILE_MIN_MS = 300L;

	private static boolean exitDialogShowing = false;
	
	//
	
	private NuclosHttpInvokerAttributeContext ctx;
	
	public NuclosHttpInvokerProxyFactoryBean() {
	}

	public static boolean isExitDialogShowing() {
		return exitDialogShowing;
	}
	
	// @Autowired
	public void setNuclosHttpInvokerAttributeContext(NuclosHttpInvokerAttributeContext ctx) {
		this.ctx = ctx;
	}

	@Override
	protected RemoteInvocationResult executeRequest(RemoteInvocation invocation) throws Exception {
		if (ClientMandatorContext.getMandatorUID() != null) {
			invocation.addAttribute(NuclosConstants.MANDATOR, ClientMandatorContext.getMandatorUID().getString());
		}
		if (DataLanguageContext.getDataUserLanguage() != null) {
			invocation.addAttribute(NuclosConstants.DATA_LOCALE, DataLanguageContext.getDataUserLanguage().toString());			
		}
		
		invocation.addAttribute(NuclosConstants.USER_TIMEZONE, Main.getInitialTimeZone().getID());
		invocation.addAttribute(NuclosConstants.INPUT_CONTEXT_SUPPORTED, ctx.isSupported());
		final HashMap<String, Serializable> map = ctx.get();
		invocation.addAttribute(NuclosConstants.INPUT_CONTEXT, map);
		if (LOG.isDebugEnabled() && map.size() > 0) {
			LOG.debug("Sending call with dynamic context:");
			for (Map.Entry<String, Serializable> entry : map.entrySet()) {
				LOG.debug(entry.getKey() + ": " + String.valueOf(entry.getValue()));
			}
		}
		invocation.addAttribute(NuclosConstants.MESSAGE_RECEIVER_CONTEXT, ctx.getMessageReceiver());
		MainFrameTab target = null;
		if (ctx.getMessageReceiver() != null) {
			target = MainFrameTab.getMainFrameTab((Integer) ctx.getMessageReceiver());
			if (target != null) target.acceptLockMessages(true);
		}
		if(ApplicationProperties.getInstance().isFunctionBlockDev()) {
			invocation.addAttribute(NuclosConstants.CLIENT_STACK_TRACE, Thread.currentThread().getStackTrace());
		}
		// only temporarily storage for the differentiation in writeRemoteInvocation
		invocation.addAttribute("serviceInterface", getServiceInterface());
		
		final long before;
		if (PROFILE) {
			before = System.currentTimeMillis();
		}
		else {
			before = 0L;
		}
		
		try {
			RemoteInvocationResult result = super.executeRequest(invocation);
			if (result.getException() instanceof SessionAuthenticationException) {
				// SSO token expired or revoked
				handleSessionExpired();
			}
			if (isPermissionException(result.getException())
					&& !isAuthenticated()) {
				// NUCLOS-9153: Not authenticated any more? Must be an expired session...
				handleSessionExpired();
			}
			return result;
		} catch (OutOfMemoryError e) {
			Throwable thr = e;
			while (thr.getCause() != null) {
				thr = thr.getCause();
			}
			LOG.error("remote invocation of " + invocation + " failed: " + e, thr);
			throw e;
		} catch (Exception e) {
			throw e;
		} finally {
			if (PROFILE) {
				final long call = System.currentTimeMillis() - before;
				if (call >= PROFILE_MIN_MS) {
					LOG.info("remote invocation of " + invocation + " on " + getServiceInterface() + " took " + call + " ms");
				}
			}
			
			if (target != null) {
				target.acceptLockMessages(false);
			}
		}
	}

	private boolean isAuthenticated() {
		boolean result = false;
		try {
			result = StringUtils.equalsIgnoreCase(
						Main.getInstance().getMainController().getUserName(),
						SecurityDelegate.getInstance().getUserName()
			);
		} catch (NullPointerException | CommonFatalException ex) {
			// too early (MainController)
		}
		return result;
	}

	private boolean isPermissionException(Throwable ex) {
		return ex != null &&
			(
					ex instanceof CommonPermissionException ||
					ex.getCause() instanceof CommonPermissionException ||
					(
						ex.getCause() != null && ex.getCause().getCause() instanceof CommonPermissionException
					)
			);
	}

	private synchronized void handleSessionExpired() {
		if (exitDialogShowing) {
			return;
		}
		exitDialogShowing = true;
		String sMessage = "Sorry, your session has expired. The application will be closed.";
		String sTitle = "Expired session";
		try {
			sMessage = SpringLocaleDelegate.getInstance().getMessage(
					"server.session.expired.error.message", "Entschuldigung, Ihre Sitzung ist abgelaufen. Die Anwendung wird geschlossen.");
			sTitle = SpringLocaleDelegate.getInstance().getMessage(
					"server.session.expired.error.title", "Abgelaufene Sitzung");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			final String sFinalMessage = sMessage;
			final String sFinalTitle = sTitle;
			if (SwingUtilities.isEventDispatchThread()) {
				JOptionPane.showMessageDialog(MainFrame.getInstance(), sFinalMessage, sFinalTitle, JOptionPane.OK_OPTION);
				new Thread(() -> {
					// new thread prevents deadlock from shutdown hooks
					Main.getInstance().exit(Main.ExitResult.SESSION_EXPIRED);
				}).start();
			} else {
				SwingUtilities.invokeLater(() -> {
					JOptionPane.showMessageDialog(MainFrame.getInstance(), sFinalMessage, sFinalTitle, JOptionPane.OK_OPTION);
					Main.getInstance().exit(Main.ExitResult.SESSION_EXPIRED);
				});
			}
		}
	}

}
