//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.remote.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.security.RemoteAuthenticationManager;
import org.nuclos.common2.LangUtils;
import org.nuclos.remoting.NuclosRemotingInterface;
import org.nuclos.remoting.NuclosRemotingInvocation;
import org.nuclos.remoting.NuclosRemotingInvocationResult;
import org.nuclos.remoting.NuclosRemotingMapperFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.remoting.httpinvoker.HttpComponentsHttpInvokerRequestExecutor;
import org.springframework.remoting.httpinvoker.HttpInvokerClientConfiguration;
import org.springframework.remoting.support.RemoteInvocation;
import org.springframework.remoting.support.RemoteInvocationResult;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.databind.ObjectMapper;

public class NuclosHttpInvokerRequestExecutor extends HttpComponentsHttpInvokerRequestExecutor implements InitializingBean {

	private static final Logger LOG = Logger.getLogger(NuclosHttpInvokerRequestExecutor.class);

	//

	private HttpRequestRetryHandler retryHandler;
	
	// Spring injection
	
	private ApplicationProperties applicationProperties;
	
	// end of Spring injection

	private static final ObjectMapper jsonMapper = NuclosRemotingMapperFactory.createObjectMapper();
	
	public NuclosHttpInvokerRequestExecutor(HttpClient httpClient) {
		super(httpClient);
	}
	
	// @Autowired
	public final void setApplicationProperties(ApplicationProperties applicationProperties) {
		this.applicationProperties = applicationProperties;
	}
	
	// @PostConstruct
	public final void afterPropertiesSet() {
		// timeout disable for dev
		// timeout 30 minutes (requested for entity transfer)
		setReadTimeout(applicationProperties.isFunctionBlockDev() ? 0 : 1000 * 60 * 30);
	}

	@Override
	protected HttpPost createHttpPost(HttpInvokerClientConfiguration config) throws IOException {
		HttpPost postMethod = super.createHttpPost(config);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if ((auth != null) && (auth.getName() != null)) {
			String base64 = auth.getName() + ":" + LangUtils.defaultIfNull(auth.getCredentials(), "");
			postMethod.setHeader("Authorization", "Basic " + new String(Base64.encodeBase64(base64.getBytes())));
		}
		return postMethod;
	}

	@Override
	public void setHttpClient(HttpClient httpClient) {
		if (this.retryHandler != null) {
			((DefaultHttpClient) httpClient).setHttpRequestRetryHandler(retryHandler);
		}
		super.setHttpClient(httpClient);
	}

	public void setRetryHandler(HttpRequestRetryHandler retryHandler) {
		this.retryHandler = retryHandler;
		if (getHttpClient() != null) {
			((DefaultHttpClient) getHttpClient()).setHttpRequestRetryHandler(retryHandler);
		}
	}

	@Override
	protected RemoteInvocationByteArrayOutputStream getByteArrayOutputStream(RemoteInvocation invocation) throws IOException {
		RemoteInvocationByteArrayOutputStream baos = new RemoteInvocationByteArrayOutputStream(1024, invocation);
		this.writeRemoteInvocation(invocation, baos);
		return baos;
	}

	@Override
	protected void writeRemoteInvocation(RemoteInvocation invocation, OutputStream os) throws IOException {
		Class<?> serviceInterface = null;
		final Map<String, Serializable> attributes = invocation.getAttributes();
		if (attributes != null) {
			serviceInterface = (Class<?>) attributes.get("serviceInterface");
			attributes.remove("serviceInterface");
		}
		if (serviceInterface == null || // <- RemoteAuthenticationProvider
				serviceInterface.isAnnotationPresent(NuclosRemotingInterface.class)) {
			jsonMapper.writeValue(os, new NuclosRemotingInvocation().set(invocation));
		} else {
			ObjectOutputStream oos = new ObjectOutputStream(this.decorateOutputStream(os));
			try {
				this.doWriteRemoteInvocation(invocation, oos);
			} finally {
				oos.close();
			}
		}
	}

	@Override
	protected RemoteInvocationResult doExecuteRequest(HttpInvokerClientConfiguration config, ByteArrayOutputStream baos) throws IOException, ClassNotFoundException {
		HttpPost postMethod = this.createHttpPost(config);
		this.setRequestBody(config, postMethod, baos);

		RemoteInvocationResult result;
		try {
			HttpResponse response = this.executeHttpPost(config, this.getHttpClient(), postMethod);
			this.validateResponse(config, response);
			InputStream responseBody = this.getResponseBody(config, response);
			Class<?> serviceInterface = null;
			if (config instanceof NuclosHttpInvokerProxyFactoryBean) {
				final NuclosHttpInvokerProxyFactoryBean nuclosConfig = (NuclosHttpInvokerProxyFactoryBean) config;
				serviceInterface = nuclosConfig.getServiceInterface();
			} else if (baos instanceof RemoteInvocationByteArrayOutputStream) {
				// not a NuclosHttpInvokerProxyFactoryBean? Must be authentication
				serviceInterface = RemoteAuthenticationManager.class;
			}
			if (baos instanceof RemoteInvocationByteArrayOutputStream && serviceInterface != null && serviceInterface.isAnnotationPresent(NuclosRemotingInterface.class)) {
				result = this.readRemoteInvocationResult(
						serviceInterface,
						((RemoteInvocationByteArrayOutputStream) baos).invocation,
						responseBody, config.getCodebaseUrl());
			} else {
				result = this.readRemoteInvocationResult(
						responseBody, config.getCodebaseUrl());
			}
		} finally {
			postMethod.releaseConnection();
		}

		return result;
	}

	protected RemoteInvocationResult readRemoteInvocationResult(Class<?> serviceInterface, RemoteInvocation invocation, InputStream is, String codebaseUrl) throws IOException, ClassNotFoundException {
		final Class<?> returnType;
		final Type genericReturnType;
		try {
			returnType = serviceInterface.getMethod(invocation.getMethodName(), invocation.getParameterTypes()).getReturnType();
			genericReturnType = serviceInterface.getMethod(invocation.getMethodName(), invocation.getParameterTypes()).getGenericReturnType();
		} catch (NoSuchMethodException e) {
			throw new ClassNotFoundException(String.format("ClassMethod %s.%s not found: ", serviceInterface.getCanonicalName(), invocation.getMethodName(), e.getMessage()), e);
		}
		return jsonMapper.readValue(is, NuclosRemotingInvocationResult.class).getRemoteInvocationResult(returnType, genericReturnType);
	}

	/**
	 * This overridden doExecuteRequest could be used for performance tracking of remote calls.
	 * It should commented out in production environments.
	 * 
	 * @author Thomas Pasch
	 * @since Nuclos 3.8
	 */
	/*
	@Override
	protected RemoteInvocationResult doExecuteRequest(
			HttpInvokerClientConfiguration config, ByteArrayOutputStream baos)
			throws IOException, ClassNotFoundException {

		long start = System.currentTimeMillis();
		
		HttpPost postMethod = createHttpPost(config);
		setRequestBody(config, postMethod, baos);
		HttpResponse response = executeHttpPost(config, getHttpClient(), postMethod);
		
		long reqLength = postMethod.getEntity().getContentLength();
		long exec = System.currentTimeMillis();
		
		validateResponse(config, response);
		InputStream responseBody = getResponseBody(config, response);
		RemoteInvocationResult result = readRemoteInvocationResult(responseBody, config.getCodebaseUrl());
		
		long respLength = response.getEntity().getContentLength();
		long stop = System.currentTimeMillis();
		
		LOG.info("request: " + reqLength + " response: " + respLength + " time: " + (stop - start) 
				+ " (" + (exec - start) + " + " + (stop - exec) + ")");
		return result;
	}
	 */

	private static class RemoteInvocationByteArrayOutputStream extends ByteArrayOutputStream {

		final RemoteInvocation invocation;

		private RemoteInvocationByteArrayOutputStream(final int var1, final RemoteInvocation invocation) {
			super(var1);
			this.invocation = invocation;
		}
	}

}
