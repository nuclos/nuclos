//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.report.reportrunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

/**
 * class which represents an entry in the BackgroundProcessStatusPanel.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @author	<a href="mailto:rostislav.maksymovskyi@novabit.de">rostislav.maksymovskyi</a>
 * @version 02.00.00
 */
public class BackgroundProcessTableEntry implements BackgroundProcessInfo {

	private static final Logger LOG = Logger.getLogger(BackgroundProcessTableEntry.class);
	
	private final List<ProcessListener> listeners = new LinkedList<ProcessListener>();

	private final String sJobName;
	private final Date dateStartedAt;
	private Status status;
	private String sMessage;
	private Throwable tThrowable;
	private Future<?> processFuture;
	
	@Deprecated
	private Observable observable = null;

	public BackgroundProcessTableEntry(String sJobName, Status status, Date dateStartedAt) {
		this.sJobName = sJobName;
		this.dateStartedAt = dateStartedAt;
		this.status = status;
		this.processFuture = null;
	}

	public BackgroundProcessTableEntry(String sJobName, Status status, Date dateStartedAt, Future<?> processFuture) {
		this.sJobName = sJobName;
		this.dateStartedAt = dateStartedAt;
		this.status = status;
		this.processFuture = processFuture;
	}
	
	@Override
	public Date getStartedAt() {
		return dateStartedAt;
	}

	@Override
	public Status getStatus() {
		return status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = status;
		if (this.observable != null){
			this.observable.notifyObservers();
		}
		
		// bail out if not finished
		if (!status.isFinished()) {
			return;
		}
		final List<ProcessListener> copy;
		synchronized (listeners) {
			// bail out if empty
			if (listeners.isEmpty()) {
				return;
			}
			// defensive copy
			copy = new ArrayList<ProcessListener>(listeners);
		}
		final ProcessEvent event = new ProcessEvent(this);
		for (ProcessListener l: copy) {
			switch (status) {
			case DONE:
				l.onDone(event);
				break;
			case CANCELLED:
				l.onCancel(event);
				break;
			case ERROR:
				l.onError(event);
				break;
			default:
				throw new IllegalStateException(status.name());
			}
		}
	}

	/**
	 * @return the current exception if any
	 */
	@Override
	public Throwable getException() {
		return this.tThrowable;
	}

	@Override
	public void setException(Throwable throwable) {
		this.tThrowable = throwable;
	}

	@Override
	public String getJobName() {
		return sJobName;
	}

	@Override
	public String getMessage() {
		return sMessage;
	}

	@Override
	public void setMessage(String sMessage) {
		this.sMessage = sMessage;
	}

	@Override
	public Future<?> getProcessFuture() {
		return processFuture;
	}

	@Override
	public void cancelProzess(){
		if(this.processFuture != null){
			boolean cancelled = this.processFuture.cancel(true);
			LOG.debug("cancelProzess>>>>>>>>>> cancelled future: "+cancelled);
		} 
	}

	@Override
	@Deprecated
	public void addObservable(Observable observable){
		this.observable = observable;
	}

	@Override
	public void addProcessListener(ProcessListener listener) {
		if (listener == null) {
			throw new NullPointerException();
		}
		synchronized (listeners) {
			if (!listeners.contains(listener)) {
				listeners.add(listener);
			}
		}
	}

	@Override
	public void removeProcessListener(ProcessListener listener) {
		synchronized (listeners) {
			listeners.remove(listener);
		}
	}
	
}	// class BackgroundProcessTableEntry
