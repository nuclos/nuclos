package org.nuclos.client.report.reportrunner;

import java.util.EventObject;

public class ProcessEvent extends EventObject {
	
	public ProcessEvent(BackgroundProcessInfo info) {
		super(info);
	}
	
	public BackgroundProcessInfo getBackgroundProcessInfo() {
		return (BackgroundProcessInfo) getSource();
	}

}
