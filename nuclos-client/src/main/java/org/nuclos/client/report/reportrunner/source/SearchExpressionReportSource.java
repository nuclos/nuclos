//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.report.reportrunner.source;

import java.util.List;

import org.nuclos.client.report.ReportDelegate;
import org.nuclos.client.report.reportrunner.ReportSource;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO.Format;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

public class SearchExpressionReportSource implements ReportSource {

	private final CollectableSearchExpression expr;
	private final List<? extends CollectableEntityField> lstclctefweSelected;
	private final List<Integer> selectedFieldWidth;
	private final UID entityUID;
	private final ReportOutputVO.Format format;
	private final String customUsage;
	private final ReportOutputVO.PageOrientation orientation;
	private final boolean columnScaled;
	
	public SearchExpressionReportSource(CollectableSearchExpression expr, List<? extends CollectableEntityField> lstclctefweSelected, List<Integer> selectedFieldWidth, UID entityUID, Format format, String customUsage, ReportOutputVO.PageOrientation orientation, boolean columnScaled) {
		super();
		this.expr = expr;
		this.lstclctefweSelected = lstclctefweSelected;
		this.selectedFieldWidth = selectedFieldWidth;
		this.entityUID = entityUID;
		this.format = format;
		this.customUsage = customUsage;
		this.orientation = orientation;
		this.columnScaled = columnScaled;
	}

	@Override
	public NuclosFile getReport() throws NuclosReportException {
		try {
			return ReportDelegate.getInstance().prepareSearchResult(expr, lstclctefweSelected, selectedFieldWidth, entityUID, format, customUsage, orientation, columnScaled);
		}
		catch (CommonBusinessException e) {
			throw new NuclosReportException(e);
		}
	}
}
