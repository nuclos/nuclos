package org.nuclos.client.rule.client.explorer;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.remoting.TypePreservingObjectDeserializer;
import org.nuclos.remoting.TypePreservingObjectSerializer;
import org.nuclos.server.navigation.treenode.TreeNode;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public abstract class AbstractClientRuleSelectionNode implements TreeNode {

	@JsonSerialize(using = TypePreservingObjectSerializer.class)
	@JsonDeserialize(using = TypePreservingObjectDeserializer.class)
	protected Object id;
	@JsonSerialize(using = TypePreservingObjectSerializer.class)
	@JsonDeserialize(using = TypePreservingObjectDeserializer.class)
	protected Object rootId;
	protected UID nodeId;
	protected UID entityUID;
	protected String label;
	protected String description;
	protected String identifier;

	/**
	 * for deserialization only
	 */
	protected AbstractClientRuleSelectionNode() {
		super();
	}
	
	public AbstractClientRuleSelectionNode(Object id, Object rootId, UID nodeId, UID entityUID, String label, String description, String identifier) {
		this.id = id;
		this.rootId = rootId;
		this.nodeId = nodeId;
		this.entityUID = entityUID;
		this.label = label;
		this.description = description;
		this.identifier = identifier;
	}
	
	public abstract boolean isSelectable();
	
	@Override
	public Object getId() {
		return this.id;
	}

	@Override
	public Object getRootId() {
		return this.rootId;
	}

	@Override
	public UID getNodeId() {
		return this.nodeId;
	}

	@Override
	public UID getEntityUID() {
		return this.entityUID;
	}

	@Override
	public String getLabel() {
		return this.label;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public String getIdentifier() {
		return this.identifier;
	}
	
	@Override
	public Boolean hasSubNodes() {
		return getSubNodes() != null ? getSubNodes().size() > 0 : Boolean.FALSE;
	}

	@Override
	public void removeSubNodes() {}

	@Override
	@Deprecated
	public void refresh() {}

	@Override
	public boolean implementsNewRefreshMethod() {
		return false;
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
		return null;
	}

	@Override
	public boolean needsParent() {
		return false;
	}
	
	@Override
	public String toString() {
		return this.getLabel();
	}
}
