package org.nuclos.client.rule.client.panel;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.tree.TreeSelectionModel;

import org.nuclos.client.explorer.DefaultKeyListener;
import org.nuclos.client.explorer.DefaultMouseListener;
import org.nuclos.client.explorer.DefaultTreeWillExpandListener;
import org.nuclos.client.explorer.ExplorerController;
import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.client.explorer.ExplorerView;
import org.nuclos.client.explorer.ui.ExplorerNodeRenderer;
import org.nuclos.client.rule.client.explorer.ClientRuleRootNode;
import org.nuclos.client.ui.UIUtils;

public class ClientRuleManagerView extends JPanel implements ExplorerView  {

	private JTree tree;
	private JToolBar toolBar;
	private JPanel content;
	
	public ClientRuleManagerView() {
		super(new BorderLayout());
		
		ClientRuleRootNode root = new ClientRuleRootNode();
		
		toolBar = UIUtils.createNonFloatableToolBar();
		content = new JPanel(new BorderLayout());
		JScrollPane scrlpn = new JScrollPane();
		
		this.setOpaque(false);
		this.setBorder(BorderFactory.createEmptyBorder());
		
		final ExplorerNode<?> explorernodeRoot = ExplorerController.newExplorerTree(root, true);
		this.tree = new JTree(explorernodeRoot, true);
		this.tree.putClientProperty("JTree.lineStyle", "Angled");
		this.tree.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
		this.tree.setBackground(Color.WHITE);
		this.tree.setRootVisible(true);
		this.tree.setShowsRootHandles(true);
		this.tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		
		//addDNDFunctionality(this.tree);
		
		// don't expand on double click:
		this.tree.setToggleClickCount(0);

		this.tree.addKeyListener(new DefaultKeyListener(tree));
		this.tree.addMouseListener(new DefaultMouseListener(tree));

		// enable tool tips:
		ToolTipManager.sharedInstance().registerComponent(this.tree);

		this.tree.setCellRenderer(new ExplorerNodeRenderer());
		this.tree.addTreeWillExpandListener(new DefaultTreeWillExpandListener(tree));
		
		scrlpn.getViewport().add(tree);
		content.add(scrlpn);
		this.add(this.toolBar, BorderLayout.NORTH);
		this.add(this.content, BorderLayout.CENTER);
		
		
	}
	@Override
	public JComponent getViewComponent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JTree getJTree() {
		return tree;
	}

	@Override
	public ExplorerNode<?> getRootNode() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean isRestoreExpandendState() {
		// TODO Auto-generated method stub
		return false;
	}

}
