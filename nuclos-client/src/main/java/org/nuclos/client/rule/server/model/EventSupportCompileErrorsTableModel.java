package org.nuclos.client.rule.server.model;

import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

import org.nuclos.client.ui.Icons;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;

public class EventSupportCompileErrorsTableModel extends AbstractTableModel {

	private List<ErrorMessage> msgs;
	
	private static final String COL_EVENTSUPPORT_COMPILEERROR_ICONF = null;
	private static final String COL_EVENTSUPPORT_COMPILEERROR_CLASS = SpringLocaleDelegate.getInstance().getMessage(
			"EventSupportErrorMessageModelColumn.1","Klasse");
	private static final String COL_EVENTSUPPORT_COMPILEERROR_LINE = SpringLocaleDelegate.getInstance().getMessage(
			"EventSupportErrorMessageModelColumn.2","Zeile");
	private static final String COL_EVENTSUPPORT_COMPILEERROR_MESSAGE = SpringLocaleDelegate.getInstance().getMessage(
			"EventSupportErrorMessageModelColumn.3","Beschreibung");
	
	private static final String[] COLUMNS = new String[] {COL_EVENTSUPPORT_COMPILEERROR_ICONF, COL_EVENTSUPPORT_COMPILEERROR_CLASS, COL_EVENTSUPPORT_COMPILEERROR_LINE, COL_EVENTSUPPORT_COMPILEERROR_MESSAGE};

	
	public EventSupportCompileErrorsTableModel(List<ErrorMessage> pMsgs) {
		this.msgs = pMsgs;
	}
	
	public void loadMessages(List<ErrorMessage> pMsgs) {
		this.msgs = pMsgs;
		fireTableDataChanged();
	}
	
	public ErrorMessage getRow(int rowidx) {
		return this.msgs.get(rowidx);
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 0) {
			return ImageIcon.class;
		}
		else {
			return String.class;
		}
	}
	
	@Override
	public int getRowCount() {
		return msgs.size();
	}

	@Override
	public String getColumnName(int column) {
		if (column == 1) {
			return COLUMNS[1];
		}
		else if (column == 2) {
			return COLUMNS[2];			
		}
		else if (column == 3) {
			return COLUMNS[3];			
		}
		else {
			return null;
		}
			
	}
	
	@Override
	public int getColumnCount() {
		return COLUMNS.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (msgs.size() > rowIndex) {
			ErrorMessage errorMessage = msgs.get(rowIndex);
			
			if (columnIndex == 0) {
				return Icons.getInstance().getEventSupportCodeCompileErrorIcon();
			}
			else if(columnIndex == 1) {
				return errorMessage.getSource();
			}
			else if(columnIndex == 2) {
				return errorMessage.getLineNumber();
			}
			else if(columnIndex == 3) {
				return errorMessage.getMessage(null);
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}			
	}

}
