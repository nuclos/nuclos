package org.nuclos.client.rule.server.model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import org.apache.log4j.Logger;
import org.nuclos.client.rule.server.panel.AbstractEventSupportPropertyPanel.TableRowMoveListener;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;

public abstract class EventSupportPropertiesTableModel<T extends EventSupportVO> extends AbstractTableModel {
	
	private static final Logger LOG = Logger.getLogger(EventSupportPropertiesTableModel.class);
	private static final Color DEFAULT_COLOR = Color.GRAY;
	private final List<TableRowMoveListener> rowMoveListener = new LinkedList<TableRowMoveListener>();
	private final List<Color> rowColors = new ArrayList<Color>();
	private JComponent panel;
	
	public abstract List<T> getEntries();
	public abstract String[] getColumns();
	public abstract void addEntry(int rowId, T elm);
	
	protected EventSupportPropertiesTableModel(JComponent pPanel) {
		this.panel = pPanel;
	}
	
	public void setRowColour(int row, Color c) {
		boolean fire = false;
		final int len = rowColors.size();
		if (row > len - 1) {
			// add default color for 'missing' rows
			for (int i = len; i < row; ++i) {
				rowColors.add(i, DEFAULT_COLOR);
				LOG.info("filling missing row " + row + " with default color");
			}
			rowColors.add(row, c);
			fire = true;
		}
		else {
			final Color old = rowColors.set(row, c);
			fire = !c.equals(old);
		}
		if (fire) {
			fireTableRowsUpdated(row, row);
		}
    }

  	public Color getRowColour(int row) {
  		if (row >= rowColors.size()) {
  			LOG.info("color not set yet for row " + row);
  			return DEFAULT_COLOR;
  		}
    	return rowColors.get(row);
    }
			  	
	@Override
	public int getRowCount() {
		return getEntries() != null ? getEntries().size() : 0;
	}

	@Override
	public int getColumnCount() {
		return getColumns().length;
	}

	@Override
	public String getColumnName(int column) {
		return getColumns()[column];
	}
	
	public void clear() {
		if (getEntries() != null && !getEntries().isEmpty())
		{
			int count = getEntries().size();
			getEntries().clear();
			fireTableRowsDeleted(0, count);
		}
	}
	
	
	public void removeEntry(int row) {
		if (row >= 0 && row < getEntries().size()) {
			getEntries().remove(row);
		}
	}
	
	public void moveDown(int selectedRow) {
		if (selectedRow < getEntries().size() - 1) {
			for (int idx = 0; idx < getEntries().size(); idx++) {
				if (idx == selectedRow) {
					getEntries().get(idx).setOrder(idx + 2);
				}
				else if (idx == selectedRow + 1) {
					getEntries().get(idx).setOrder(idx);
				}
				else {
					getEntries().get(idx).setOrder(idx+1);
				}
			}
		
			Collections.sort(getEntries(), new Comparator<T>() {		
				@Override
				public int compare(T o1, T o2) {
					return o1.getOrder() > o2.getOrder() ? 1 : o1.getOrder() < o2.getOrder() ? -1: 0;
				}
			});
			fireTableRowsUpdated(selectedRow, selectedRow+1);
			fireTableRowMoved(selectedRow+1);
		}
	}

	public void switchRows(int rowOne, int rowTwo) {
		T eseVOOne = getEntries().get(rowOne);
		T eseVOTwo = getEntries().get(rowTwo);
		int order = eseVOOne.getOrder();
		eseVOOne.setOrder(eseVOTwo.getOrder());
		eseVOTwo.setOrder(order);
		
		Collections.sort(getEntries(), new Comparator<T>() {		
			@Override
			public int compare(T o1, T o2) {
				return o1.getOrder() > o2.getOrder() ? 1 : o1.getOrder() < o2.getOrder() ? -1: 0;
			}
		});
		
		if (rowOne <= rowTwo) {
			fireTableRowsUpdated(rowOne,  rowTwo);			
			fireTableRowMoved(rowOne);
		}
		else {
			fireTableRowsUpdated(rowTwo, rowOne);
			fireTableRowMoved(rowTwo);
		}
	}
	
	public void moveUp(int selectedRow) {
		if (selectedRow != 0) {
			for (int idx = 0; idx < getEntries().size(); idx++) {
				if (idx == selectedRow) {
					getEntries().get(idx).setOrder(idx);
				}
				else if (idx == selectedRow - 1) {
					getEntries().get(idx).setOrder(idx + 2);
				}
				else {
					getEntries().get(idx).setOrder(idx+1);
				}
			}

			Collections.sort(getEntries(), new Comparator<T>() {		
				@Override
				public int compare(T o1, T o2) {
					return o1.getOrder() > o2.getOrder() ? 1 : o1.getOrder() < o2.getOrder() ? -1: 0;
				}
			});
			fireTableRowsUpdated(selectedRow-1, selectedRow);
			fireTableRowMoved(selectedRow-1);
		}
	}
	
	public T getEntryByRowIndex(int id) {
		T retVal = null;
		if (id >= 0 && id < getEntries().size())
			retVal = getEntries().get(id);
		
		return retVal;
	}
		
	public void addTableRowMoveListener(TableRowMoveListener newListener) {
		synchronized (rowMoveListener) {
			rowMoveListener.add(newListener);
		}
	}
	
	public void fireTableRowMoved(int rowId) {
		final List<TableRowMoveListener> list;
		synchronized (rowMoveListener) {
			list = new ArrayList<TableRowMoveListener>(rowMoveListener);
		}
		for (TableRowMoveListener listener : list) {
			listener.rowSelected(rowId);
		}
	}

	protected void showAndLogException(Exception e) {
		String msg = e.getMessage() != null ? e.getMessage() : "Error at runtime. Please check Log-files";		
		JOptionPane.showMessageDialog(this.panel, 
				msg, "Fehler", JOptionPane.ERROR_MESSAGE);
		LOG.error(msg);
	}
}
