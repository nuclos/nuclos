package org.nuclos.client.rule.server.panel;

import java.awt.BorderLayout;
import java.util.Map;

import javax.swing.AbstractAction;

import org.nuclos.client.rule.server.EventSupportActionHandler.EventSupportActions;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.rule.server.model.EventSupportJobPropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportPropertiesTableModel;

public class EventSupportJobProperyPanel extends AbstractEventSupportPropertyPanel {

	private EventSupportJobPropertiesTableModel model;
	private Map<EventSupportActions, AbstractAction> actionMapping;
	
	public EventSupportJobProperyPanel(Map<EventSupportActions, AbstractAction> pActionMapping) {

		this.model = new EventSupportJobPropertiesTableModel(this);
		this.actionMapping = pActionMapping;
		
		setLayout(new BorderLayout());
		
		createPropertiesTable();		
	}
	
	@Override
	protected EventSupportPropertiesTableModel getPropertyModel() {
		return this.model;
	}

	@Override
	public Map<EventSupportActions, AbstractAction> getActionMapping() {
		return this.actionMapping;
	}

	@Override
	public ActionToolBar[] getActionToolbarMapping() {
		return new ActionToolBar[] {
				new ActionToolBar(EventSupportActions.ACTION_DELETE_JOB, true),
				new ActionToolBar(EventSupportActions.ACTION_MOVE_UP_JOB, true),
				new ActionToolBar(EventSupportActions.ACTION_MOVE_DOWN_JOB, true),
		};
	}

	@Override
	protected String getPreferenceNodeName() {
		return EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_TARGET_PROPERTIES_JOB;
	}
}
