package org.nuclos.client.rule.server.panel;

public interface EventSupportSelectionExceptionListener {
	
	void fireException(Exception firedException);
}
