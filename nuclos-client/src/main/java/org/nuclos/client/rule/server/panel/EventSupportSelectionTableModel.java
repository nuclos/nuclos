package org.nuclos.client.rule.server.panel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JComponent;

import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.rule.server.model.EventSupportPropertiesTableModel;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;

public class EventSupportSelectionTableModel<T extends EventSupportVO> extends EventSupportPropertiesTableModel<T> {

	private static final String COL_TRANSRULE_RULENAME = SpringLocaleDelegate.getInstance().getMessage("JobControlRuleColumn.1","Regel");
	private static final String COL_TRANSRULE__RULEDESCRIPTION = SpringLocaleDelegate.getInstance().getMessage("JobControlRuleColumn.2","Beschreibung");
	
	private static final String[] COLUMNS = new String[] {COL_TRANSRULE_RULENAME, COL_TRANSRULE__RULEDESCRIPTION};
	
	private List<T> gens = new ArrayList<T>();
	private List<EventSupportSelectionDataChangeListener> listener = new ArrayList<EventSupportSelectionDataChangeListener>();

	public EventSupportSelectionTableModel(JComponent pPanel) {
		super(pPanel);
	}

	public void addRuleDataChangeListener(EventSupportSelectionDataChangeListener listener) {
		this.listener.add(listener);
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		String retVal = null;
		
		T estVO = this.gens.get(rowIndex);
		
		if (estVO != null) {
			EventSupportSourceVO esClass = 
					EventSupportRepository.getInstance().getEventSupportByClassname(estVO.getEventSupportClass());
			if (esClass != null) {
				if (columnIndex == 0) {
					retVal = esClass.getName();
				}
				else {
					retVal = esClass.getDescription();
				}
			}
		}
		
		return retVal;
	}

	@Override
	public List<T> getEntries() {
		return gens;
	}
	
	@Override
	public String[] getColumns() {
		return COLUMNS;
	}

	public void addEntries(List<T> loadRules) {
		this.gens.clear();
		
		Collections.sort(loadRules, new Comparator<T>() {
			@Override
			public int compare(T o1,
					T o2) {
				return o1.getOrder().compareTo(o2.getOrder());
			}
		} );
		
		for (T tesVO : loadRules) {
			this.gens.add(tesVO);
		}
		this.fireTableRowsInserted(0, this.gens.size()-1);
		this.fireDataChangeListener();
	}
	
	@Override
	public void addEntry(int rowId, T elm) {
		gens.add(rowId, elm);
		this.fireTableRowsInserted(rowId, rowId);
		this.fireDataChangeListener();
	}

	public void addEntry(T elm) {
		gens.add(gens.size(), elm);
		this.fireTableRowsInserted(0,gens.size());
	}
	
	public void reloadTable(List<T> pTrans) {
		this.gens.clear();
//		
//		Collections.sort(pTrans, new Comparator<T>() {
//			@Override
//			public int compare(T o1, T o2) {
//				return o1.getOrder().compareTo(o2.getOrder());
//			}			
//		});
		
		if (pTrans.size() > 0) {
			for (int idx = 0 ; idx < pTrans.size(); idx++) {
				this.gens.add(idx, pTrans.get(idx));
				this.fireTableRowsInserted(0, this.gens.size());
			}					
		}
		else {
			this.fireTableDataChanged();
		}
	}
	
	public void removeRow(int idx) {
		if (idx >= 0 && idx < this.gens.size()) {
			this.gens.remove(idx);
			this.fireTableRowsDeleted(idx, idx);			
			this.fireDataChangeListener();
		}
	}
		
	
	public void moveUp(int selectedRow) {
		super.moveUp(selectedRow);
		this.fireDataChangeListener();
	}
	
	public void moveDown(int selectedRow) {
		super.moveDown(selectedRow);
		this.fireDataChangeListener();
	}
	
	private void fireDataChangeListener() {
		for (EventSupportSelectionDataChangeListener list : this.listener) {
//			(this.gens, this.ruleType);
			list.attachedRuleDataChangeListenerEvent();
		}
	}

	
}
