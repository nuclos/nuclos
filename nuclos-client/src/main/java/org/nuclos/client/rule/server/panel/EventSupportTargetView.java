package org.nuclos.client.rule.server.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;

import org.nuclos.client.explorer.EventSupportTargetExplorerView;
import org.nuclos.client.explorer.ExplorerView;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.rule.server.model.EventSupportPropertiesTableModel;

public class EventSupportTargetView extends JPanel {

	private EventSupportTargetExplorerView explorerView;
	private EventSupportView esView;
	private JSplitPane splitPanelEventSupport;

	private EventSupportStatePropertyPanel statePropertiesPanel;
	private EventSupportEntityPropertyPanel entityPropertiesPanel;
	private EventSupportJobProperyPanel jobPropertiesPanel;
	private EventSupportGenerationPropertyPanel generationPropertiesPanel;
	private EventSupportCommunicationPortPropertyPanel communicationPortPropertiesPanel;
	private EventSupportButtonProperyPanel buttonPropertiesPanel;
	
	public EventSupportTargetView(EventSupportView view, Border b) {
		super(new BorderLayout());
		
		esView = view;
		explorerView = new EventSupportTargetExplorerView(esView.getTreeEventSupportTargets(), view.getActionsMap());
		
		splitPanelEventSupport = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		
		// Tree		
		final JPanel pnlEventTypes = new JPanel();
		pnlEventTypes.setLayout(new BorderLayout());
		pnlEventTypes.add(explorerView.getViewComponent(), BorderLayout.CENTER);
		pnlEventTypes.setBorder(b);
				
		splitPanelEventSupport.setTopComponent(pnlEventTypes);
		// Properties
		splitPanelEventSupport.setBottomComponent(null);
		splitPanelEventSupport.setResizeWeight(1d);
		
		int sliderSize = EventSupportPreferenceHandler.getInstance().getSliderSize(EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_TARGET_SLIDER, "mainSlider");
		if (sliderSize > 0) {
			splitPanelEventSupport.setDividerLocation(sliderSize);
		} else {
			splitPanelEventSupport.setResizeWeight(1d);
		}
		
		splitPanelEventSupport.addPropertyChangeListener(new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("dividerLocation")) {
					Component bottomComponent = ((JSplitPane) evt.getSource()).getBottomComponent();
					if (evt.getNewValue() != null && bottomComponent != null) {
						EventSupportPreferenceHandler.getInstance().addSliderSize(
								EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_TARGET_SLIDER, "mainSlider", (Integer) evt.getNewValue()); 
					}
				}
			}
		});
		add(splitPanelEventSupport, BorderLayout.CENTER);
	}
	
	public ExplorerView getExplorerView() {
		return this.explorerView;
	}
	
	public JTree getTree() {
		return this.explorerView.getJTree();
	}
	
	public void showPropertyPanel(AbstractEventSupportPropertyPanel propertyPanelToShow) {
		if (splitPanelEventSupport.getBottomComponent() != null) {
			AbstractEventSupportPropertyPanel p = (AbstractEventSupportPropertyPanel) splitPanelEventSupport.getBottomComponent();
			if (p != null && p.getPropertyTable().getCellEditor() != null)
				p.getPropertyTable().getCellEditor().cancelCellEditing();		
		}
		
		// Remove old Property Panel 
		if (splitPanelEventSupport.getBottomComponent() != null)
			splitPanelEventSupport.remove(splitPanelEventSupport.getBottomComponent());
		
		// ...and load the new according to the given modeltype
		splitPanelEventSupport.setBottomComponent(propertyPanelToShow);
		
		final int sliderSize = EventSupportPreferenceHandler.getInstance().getSliderSize(
				EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_TARGET_SLIDER, "mainSlider");
		if (sliderSize > 0) {
			splitPanelEventSupport.setDividerLocation(sliderSize);
		} else {
			splitPanelEventSupport.setResizeWeight(1d);
		}
		
		if (propertyPanelToShow != null) {
			for (int idx = 0; idx < propertyPanelToShow.getPropertyModel().getColumnCount(); idx++) {
				propertyPanelToShow.getPropertyTable().setDefaultRenderer(
						propertyPanelToShow.getPropertyTable().getColumnClass(idx), new ActiveRulesCellRenderer(propertyPanelToShow.getPropertyModel()));		
			}			
		}		
		// probably unneeded
		// splitPanelEventSupport.repaint();
	}
	
	public EventSupportGenerationPropertyPanel getGenerationPropertiesPanel() {
		if (generationPropertiesPanel == null) {
			generationPropertiesPanel = new EventSupportGenerationPropertyPanel(this.esView.getActionsMap());
		}
		return this.generationPropertiesPanel;
	}
	
	public EventSupportCommunicationPortPropertyPanel getCommunicationPropertiesPanel() {
		if (communicationPortPropertiesPanel == null) {
			communicationPortPropertiesPanel = new EventSupportCommunicationPortPropertyPanel(this.esView.getActionsMap());
		}
		return this.communicationPortPropertiesPanel;
	}
	
	public EventSupportButtonProperyPanel getButtonPropertiesPanel() {
		// Use cached panel if exists
		if (buttonPropertiesPanel == null) {
			buttonPropertiesPanel = new EventSupportButtonProperyPanel();
		}
		return this.buttonPropertiesPanel;
	}
	
	public EventSupportJobProperyPanel getJobPropertiesPanel() {
		// Use cached panel if exists
		if (jobPropertiesPanel == null) {
			jobPropertiesPanel = new EventSupportJobProperyPanel(this.esView.getActionsMap());
		}
		return this.jobPropertiesPanel;
	}
	
	public EventSupportStatePropertyPanel getStatePropertiesPanel() {
		// Use cached panel if exists
		if (statePropertiesPanel == null) {
			statePropertiesPanel = new EventSupportStatePropertyPanel(this.esView.getActionsMap());
		} else {
			statePropertiesPanel.reloadTransitions();
		}
		return this.statePropertiesPanel;
	}
	
	public EventSupportEntityPropertyPanel getEntityPropertiesPanel() {
		// Use cached panel if exists
		if (entityPropertiesPanel == null) {
			this.entityPropertiesPanel = new EventSupportEntityPropertyPanel(this.esView.getActionsMap());
		}
		return entityPropertiesPanel;	
	}	
	
	private static class ActiveRulesCellRenderer extends DefaultTableCellRenderer {

		private final EventSupportPropertiesTableModel model;
		
		public ActiveRulesCellRenderer(EventSupportPropertiesTableModel pModel) {
			model = pModel;
		}
		
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	        Color rowColour = model.getRowColour(row);
	        c.setForeground(rowColour);
	        
	        if (Color.GRAY.equals(rowColour)) {
	        	c.setEnabled(false);
	        } else {
	        	c.setEnabled(true);
	        }
	        return c;
	    }
	}
	
}
