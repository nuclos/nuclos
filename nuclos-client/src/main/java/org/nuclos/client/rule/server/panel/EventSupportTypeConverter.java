package org.nuclos.client.rule.server.panel;

import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;

public interface EventSupportTypeConverter<T extends EventSupportVO> {
	
	T convertData(EventSupportSourceVO esVO);
	
	String getRuleTypeAsString();
}
