package org.nuclos.client.scripting;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.SortedSet;
import java.util.TreeSet;

import org.codehaus.groovy.runtime.InvokerHelper;

import groovy.lang.Binding;
import groovy.lang.MetaClass;
import groovy.lang.MetaMethod;

/**
 * A representation of (java byte code) compiled groovy code, i.e. a Class of the compiled source.
 * <p>
 * In addition an instance of this class holds an (lazy created) instance of the Class.
 * </p>
 * @author Thomas Pasch (refactored from GroovySupport)
 */
public class CompiledGroovy {
	
	private static final Class[] NO_ARG_TYPES = new Class[0];

	private static final Class[] BINDING_ARG_TYPES = new Class[] { Binding.class };

	private final Class<?> groovyClass;
	
	private Object groovyObject;
	
	CompiledGroovy(Class<?> groovyClass) {
		if (groovyClass == null) {
			throw new NullPointerException();
		}
		this.groovyClass = groovyClass;
	}
	
	private MetaMethod getMethod(String name, Class<?>... argumentTypes) {
		if (groovyClass == null || name == null) {
			return null;
		}
		final MetaClass metaClass = InvokerHelper.getMetaClass(groovyClass);
		return metaClass.pickMethod(name, argumentTypes);
	}

	public SortedSet<String> findMethodNames(Class<?>... argumentTypes) {
		final SortedSet<String> methods = new TreeSet<String>();
		if (groovyClass != null) {
			MetaClass metaClass = InvokerHelper.getMetaClass(groovyClass);
			//for (MetaMethod method : metaClass.getMetaMethods()) {
			for (MetaMethod method : metaClass.getMethods()) {
				if (method.isValidMethod(argumentTypes)) {
					methods.add(method.getName());
				}
			}
		}
		return methods;
	}

	public boolean methodExists(String name) {
		if (groovyClass != null) {
			MetaClass metaClass = InvokerHelper.getMetaClass(groovyClass);
			for (MetaMethod method : metaClass.getMetaMethods()) {
				if (name.equals(method.getName())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Get an Method object representation with the given signature.
	 * 
	 * @param name of the method
	 * @param signature type of arguments of the method
	 * @return Method object representation
	 * @throws InstantiationException 
	 * @throws IllegalAccessException
	 */
	public InvocableMethod getInvocable(String name, Class<?>... signature) throws InstantiationException, IllegalAccessException {
		if (name != null) {
			MetaMethod method = getMethod(name, signature);
			return new InvocableMethod(getGroovyObject(), method);
		}
		return null;
	}
	
	/**
	 * Run the (compiled) groovy script code. 
	 *  
	 * @param binding variable context to run the groovy script in
	 * @return the return value of the script, i.e. the value of the last 
	 * 		executed statement/function in the script
	 * @throws InvocationTargetException
	 */
	public Object runScript(Binding binding) throws InvocationTargetException {
		// http://groovy.codehaus.org/Scripts+and+Classes
		try {
			final Constructor<?> constr = groovyClass.getConstructor(BINDING_ARG_TYPES);
			final Method run = groovyClass.getMethod("run", NO_ARG_TYPES);
			final Object scriptInstance = constr.newInstance(binding);
			return run.invoke(scriptInstance);
		} catch (SecurityException e) {
			throw new IllegalStateException(e);
		} catch (NoSuchMethodException e) {
			throw new IllegalStateException(e);
		} catch (IllegalArgumentException e) {
			throw new IllegalStateException(e);
		} catch (InstantiationException e) {
			throw new IllegalStateException(e);
		} catch (IllegalAccessException e) {
			throw new IllegalStateException(e);
		} 
	}

	private Object getGroovyObject() throws InstantiationException, IllegalAccessException {
		if (groovyObject != null) {
			return groovyObject;
		}
		groovyObject = groovyClass.newInstance();
		if (groovyObject instanceof Runnable) {
			((Runnable) groovyObject).run();
		}
		return groovyObject;
	}
}
