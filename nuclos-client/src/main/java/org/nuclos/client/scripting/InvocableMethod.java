//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.scripting;

import org.nuclos.client.main.Main;
import org.nuclos.client.ui.Errors;

import groovy.lang.MetaMethod;

/**
 * A representation of Method with the object to invoke it on.
 * <p>
 * The only thing that is missing are the concrete method arguments.
 * </p>
 * @author Thomas Pasch (refactored from GroovySupport)
 */
public class InvocableMethod {

	private final Object delegate;
	private final MetaMethod groovyMethod;
	private boolean hasErrors;
	
	InvocableMethod(Object delegate, MetaMethod method) {
		this.delegate = delegate;
		this.groovyMethod = method;
	}

	public Object invoke(Object... args) {
		if (delegate != null && groovyMethod != null && !hasErrors) {
			try {
				return groovyMethod.invoke(delegate, args);
			} catch (Exception ex) {
				hasErrors = true;
				Errors.getInstance().showExceptionDialog(Main.getInstance().getMainFrame(), "Fehler in Skriptmethode " + groovyMethod.getName(), ex);
			}
		}
		return null;
	}

	public boolean hasErrors() {
		return hasErrors;
	}
}
