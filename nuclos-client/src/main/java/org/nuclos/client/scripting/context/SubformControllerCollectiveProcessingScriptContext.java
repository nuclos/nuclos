package org.nuclos.client.scripting.context;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.nuclos.client.common.AbstractDetailsSubFormController;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.scripting.expressions.FieldIdExpression;
import org.nuclos.client.scripting.expressions.FieldPkExpression;
import org.nuclos.client.scripting.expressions.FieldUidExpression;
import org.nuclos.client.scripting.expressions.FieldValueExpression;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;

/**
 * Created by guenthse on 4/12/2017.
 */
public class SubformControllerCollectiveProcessingScriptContext<PK> extends SubformControllerScriptContext<PK> {

    private static final Logger LOG = Logger.getLogger(SubformControllerCollectiveProcessingScriptContext.class);

    private final Collection<CollectableEntityObject<PK>> clcts;

    public SubformControllerCollectiveProcessingScriptContext(CollectController parent, AbstractDetailsSubFormController parentSfc, AbstractDetailsSubFormController sfc, Collection<CollectableEntityObject<PK>> clcts) {
        super(parent, parentSfc, sfc, null);
        this.clcts = clcts;
    }

    public SubformControllerCollectiveProcessingScriptContext(CollectController<?,?> parent, AbstractDetailsSubFormController<?,?> sfc, Collection<CollectableEntityObject<PK>> clcts) {
        this(parent, null, sfc, clcts);
    }

    @Override
    public Object evaluate(FieldValueExpression exp) {
        Object oldValue = null;
        for (CollectableEntityObject<?> clct : clcts) {
            Object newValue = clct.getValue(exp.getField());
            if (oldValue == null) {
                oldValue = newValue;
            } else {
                if (!oldValue.equals(newValue)) {
                    return null;
                }
            }
        }
        return oldValue;
    }

    @Override
    public PK evaluate(FieldIdExpression exp) {
        PK oldId = null;
        for (CollectableEntityObject<?> clct : clcts) {
            PK newId = (PK) clct.getValueId(exp.getField());
            if (oldId == null) {
                oldId = newId;
            } else {
                if (!oldId.equals(newId)) {
                    return null;
                }
            }
        }
        return oldId;
    }

    @Override
    public PK evaluate(FieldPkExpression exp) {
        PK oldPK = null;
        for (CollectableEntityObject<?> clct : clcts) {
            PK newPK = (PK) clct.getPrimaryKey();
            if (oldPK == null) {
                oldPK = newPK;
            } else {
                if (!oldPK.equals(newPK)) {
                    return null;
                }
            }
        }
        return oldPK;
    }
    @Override
    public UID evaluate(FieldUidExpression exp) {
        UID oldUID = null;
        for (CollectableEntityObject<?> clct : clcts) {
            UID newUID = (UID) clct.getValueId(exp.getField());
            if (oldUID == null) {
                oldUID = newUID;
            } else {
                if (!oldUID.equals(newUID)) {
                    return null;
                }
            }
        }
        return oldUID;
    }

    public Collectable<?> getCollectable() {
        return clcts != null ? clcts.iterator().hasNext() ? clcts.iterator().next() : null : null;
    }
}
