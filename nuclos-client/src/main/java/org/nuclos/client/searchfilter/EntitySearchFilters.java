//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.searchfilter;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.nuclos.client.genericobject.Modules;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.genericobject.searchcondition.CollectableGenericObjectSearchExpression;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

/**
 * A set of search filters.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class EntitySearchFilters {

	private final List<ChangeListener> lstChangeListeners = new LinkedList<ChangeListener>();

	private final UID entityUID;

	public EntitySearchFilters(UID entityUID) {
		this.entityUID = entityUID;
	}

	/**
	 * @return the search filters for the module with the given id.
	 */
	public static EntitySearchFilters forEntity(UID sEntity) {
		return new EntitySearchFilters(sEntity);
	}
	
	public static EntitySearchFilters forAllEntities() {
		return forEntity(null);
	}
	
	/**
	 * @return the list of search filters.
	 * @throws PreferencesException
	 */
	public List<EntitySearchFilter> getAll() throws PreferencesException {
		List<EntitySearchFilter> result = CollectionUtils.sorted(
			SearchFilterCache.getInstance().getEntitySearchFilterByEntity(this.entityUID), 
			new Comparator<EntitySearchFilter>() {

				@Override
				public int compare(EntitySearchFilter o1, EntitySearchFilter o2) {
					final SearchFilterVO vo1 = o1.getSearchFilterVO();
					final SearchFilterVO vo2 = o2.getSearchFilterVO();
					final Integer order1 = vo1.getOrder();
					final Integer order2 = vo2.getOrder();
					if (order1 != null && order2 == null) {
						return -1;
					}
					if (order1 == null && order2 != null) {
						return 1;
					}
					if (order1 != null && order2 != null) {
						return LangUtils.compare(order1, order2);
					}
					return LangUtils.compare(vo1.getFilterName(), vo2.getFilterName());
				}});
		
		assert result != null;
		return result;
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return a new default filter
	 */
	public EntitySearchFilter newDefaultFilter() {
		final EntitySearchFilter result = EntitySearchFilter.newDefaultFilter();
		final SearchFilterVO vo = result.getSearchFilterVO();
		vo.setEntity(this.entityUID);
		// vo.setEntity(sEntityName);
		
		if (Modules.getInstance().isModule(entityUID)) {
			vo.setSearchDeleted(CollectableGenericObjectSearchExpression.SEARCH_UNDELETED);
		}

		return result;
	}

	/**
	 * @return the names of all search filters.
	 * @throws PreferencesException
	 */
	List<String> getFilterNames() throws PreferencesException {
		final List<String> result;

		final Transformer<EntitySearchFilter, String> decodeDecodeFilterName = new Transformer<EntitySearchFilter, String>() {
			@Override
			public String transform(EntitySearchFilter searchFilter) {
				return SearchFilter.decoded(searchFilter.getSearchFilterVO().getFilterName());
			}
		};
		result = CollectionUtils.transform(SearchFilterCache.getInstance().getAllEntitySearchFilters(), decodeDecodeFilterName);

		return result;
	}

	/**
	 * @param sFilterName
	 * @return Is the given filter name already used for a personal search filter?
	 */
	public boolean contains(String sFilterName, String sOwner, UID sEntity) {
		return (SearchFilterCache.getInstance().getEntitySearchFilter(sFilterName, sOwner, sEntity)) == null ? false : true;
	}

	/**
	 * reads the search filter with the given name from the preferences.
	 * 
	 * §postcondition result != null
	 */
	public EntitySearchFilter get(String sSearchFilter, String sOwner, UID sEntity) throws PreferencesException {
		return SearchFilterCache.getInstance().getEntitySearchFilter(sSearchFilter, sOwner, sEntity);
	}

	/**
	 * stores the given search filter in the preferences.
	 * @param filter
	 * @throws IllegalArgumentException if the filter (name) is empty or invalid
	 */
	public void put(SearchFilter filter) throws PreferencesException, NuclosBusinessException {
		SearchFilterDelegate.getInstance().insertSearchFilter(filter);
		this.fireChangedEvent();
	}

	/**
	 * removes the filter with the given name from the personal filters
	 * 
	 * §todo refactor: SearchFilter.remove()
	 */
	public void remove(SearchFilter searchFilter) throws NuclosBusinessException {
		SearchFilterDelegate.getInstance().removeSearchFilter(searchFilter);
		this.fireChangedEvent();
	}

	/**
	 * adds the given change listener.
	 * @param cl is notified when the search filters have changed.
	 */
	public synchronized void addChangeListener(ChangeListener cl) {
		this.lstChangeListeners.add(cl);
	}

	/**
	 * removes the given change listener.
	 * @param cl
	 */
	public synchronized void removeChangeListener(ChangeListener cl) {
		this.lstChangeListeners.remove(cl);
	}

	/**
	 * notifies the change listeners that the search filters have changed.
	 */
	private synchronized void fireChangedEvent() {
		for (ChangeListener cl : this.lstChangeListeners) {
			cl.stateChanged(new ChangeEvent(this));
		}
	}

}	// class SearchFilters
