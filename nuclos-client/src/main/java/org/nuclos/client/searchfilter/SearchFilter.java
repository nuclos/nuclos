//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.searchfilter;

import java.io.Serializable;

import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

/**
 * An abstract search filter (base class for local and global search filters).
 * <p>
 * TODO: Re-verify if this class is really necessary, as there is only <em>one</em>
 * concrete implementation, EntitySearchFilter.
 * </p>
 * §invariant this.isDefaultFilter() --&gt; this.getSearchCondition() == null
 * 
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 */
public abstract class SearchFilter implements Serializable, Cloneable {

	private boolean bValid = true;

	private SearchFilterVO searchfilterVO;

	private CollectableSearchCondition searchcond;

	public SearchFilter() {
		searchfilterVO = new SearchFilterVO();
	}
	
	@Override
	public Object clone() {
		SearchFilter clone;
		try {
			clone = (SearchFilter) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new IllegalStateException(e);
		}
		// clone.bValid = this.bValid;
		clone.searchfilterVO = new SearchFilterVO(searchfilterVO.getId(), searchfilterVO.getVersion(), searchfilterVO);
		// clone.searchcond = this.searchcond;
		return clone;
	}

	/**
	 * @return Is this filter a default filter? The default filter's search condition is always <code>null</code>.
	 */
	public boolean isDefaultFilter() {
		return false;
	}

	public void setSearchFilterVO(SearchFilterVO searchfilterVO) {
		this.searchfilterVO = searchfilterVO;
	}

	public SearchFilterVO getSearchFilterVO() {
		return searchfilterVO;
	}

	/**
	 * @return this filter's search condition
	 */
	public CollectableSearchCondition getSearchCondition() {
		return searchcond;
	}

	public void setSearchCondition(CollectableSearchCondition searchcond) {
		this.searchcond = searchcond;
	}

	/**
	 * @return the internal search condition that is to be used for the actual search.
	 */
	public abstract CollectableSearchCondition getInternalSearchCondition();

	@Override
	public String toString() {
		return getSearchFilterVO().getFilterName();
	}

	/**
	 * Two <code>SearchFilter</code>s are equal iff their names are equal.
	 * @param o
	 * @return {@inheritDoc}
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof SearchFilter)) {
			return false;
		}

		return LangUtils.equal(getSearchFilterVO().getId(), ((SearchFilter) o).getSearchFilterVO().getId());
	}

	@Override
	public int hashCode() {
		return LangUtils.hashCode(getSearchFilterVO().getFilterName());
	}

	public void validate() throws IllegalStateException {
		/** @todo use custom business exception */
		SearchFilter.validate(getSearchFilterVO().getFilterName());
	}

	public static void validate(String sFilterName) throws IllegalStateException {
		if (StringUtils.isNullOrEmpty(sFilterName)) {
			throw new IllegalStateException(SpringLocaleDelegate.getInstance().getMessage(
					"SearchFilter.1", "Filtername darf nicht leer sein."));
		}
		if (sFilterName.matches(".*\\\\.*")) {
			throw new IllegalStateException(SpringLocaleDelegate.getInstance().getMessage(
					"SearchFilter.2", "Filtername darf keinen Backslash (\"\\\") enthalten."));
		}
	}

	void put() throws NuclosBusinessException {
		try {
			this.validate();
		}
		catch (Exception ex) {
			throw new NuclosBusinessException(ex);
		}

		SearchFilterDelegate.getInstance().insertSearchFilter(this);
	}

	/**
	 * §precondition sFilterName != null
	 */
	static String encoded(String sFilterName) {
		return sFilterName.replace('/', '\\');
	}

	/**
	 * §precondition sFilterName != null
	 */
	static String decoded(String sFilterName) {
		return sFilterName.replace('\\', '/');
	}

	public boolean isValid() {
		return bValid;
	}

	public void setValid(boolean bValid) {
		this.bValid = bValid;
	}

}	// class SearchFilter
