//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.searchfilter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.log4j.Logger;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collect.collectable.searchcondition.visit.PutSearchConditionToPrefsVisitor;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.preferences.DOMPreferencesFactory;
import org.nuclos.common.preferences.PreferencesSupport;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.common2.searchfilter.EntitySearchFilter2Support;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.searchfilter.ejb3.SearchFilterFacadeRemote;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Business Delegate for <code>SearchFilterFacadeBean</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 * @version 01.00.00
 */
public class SearchFilterDelegate {

	private static final Logger LOG = Logger.getLogger(SearchFilterDelegate.class);

	/**
	 * New way to save/load sorting column prefs: As {@link org.nuclos.common.collect.collectable.CollectableSorting}.
	 */
	public static final String PREFS_NODE_COLLECTABLESORTING = "collectableSorting";
	
	/**
	 * @deprecated Old way to save/load sorting column prefs: only sorting columns *names* (String).
	 */
	private static final String PREFS_NODE_SORTINGCOLUMNS = "sortingColumns";
	
	private static SearchFilterDelegate INSTANCE;
	
	//
	
	// Spring injection

	@Autowired
	private SpringLocaleDelegate localeDelegate;

	@Autowired
	private SearchFilterFacadeRemote searchFilterFacade;
	
	@Autowired
	private PreferencesSupport preferencesSupport;
	
	@Autowired
	private DOMPreferencesFactory domPreferencesFactory;
	
	// end of Spring injection

	SearchFilterDelegate() {
		INSTANCE = this;
	}
	
	/**
	 * @return the one (and only) instance of SearchFilterDelegate
	 */
	public static SearchFilterDelegate getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	public Object update(UID sEntityName, MasterDataVO<UID> mdvo, IDependentDataMap mpDependants, List<TranslationVO> resources) throws CommonBusinessException {
		try {
			return this.searchFilterFacade.modify(mdvo, mpDependants, resources);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * inserts the given searchfilter
	 * @param filter
	 */
	public void insertSearchFilter(SearchFilter filter) {
		try {
			filter = insertOrUpdateFilter(filter);
			final SearchFilterVO vo = filter.getSearchFilterVO();
			final SearchFilter searchFilter = makeSearchFilter(searchFilterFacade.createSearchFilter(
					new SearchFilterVO(null, 0, vo)));
			SearchFilterCache.getInstance().addFilter(searchFilter);
			// write back to filter
			filter.setSearchFilterVO(searchFilter.getSearchFilterVO());
		}
		catch (Exception e) {
			String sMessage = localeDelegate.getMessage("SearchFilterDelegate.1", "Ein Fehler beim Speichern des Suchfilters ist aufgetreten!");
			LOG.error(sMessage);
			throw new NuclosFatalException(sMessage, e);
		}
	}

	/**
	 * updates the searchfilter with the given oldFilterIdentifier
	 */
	public void updateSearchFilter(SearchFilter newFilter, String sOldFilterName, String sOwner, UID sEntity) throws NuclosBusinessException {
		SearchFilter oldSearchFilter = SearchFilterCache.getInstance().getSearchFilter(sOldFilterName, sOwner, sEntity);

		if (!oldSearchFilter.getSearchFilterVO().isEditable()) {
			throw new NuclosBusinessException(localeDelegate.getMessage(
					"SearchFilterDelegate.2", "Der Suchfilter darf von Ihnen nicht ge\u00e4ndert werden."));
		}

		assert newFilter.getSearchFilterVO().getFilterPrefs() != null;

		try {
			SearchFilter searchFilter = makeSearchFilter(searchFilterFacade.modifySearchFilter(insertOrUpdateFilter(newFilter).getSearchFilterVO()));
			SearchFilterCache.getInstance().removeFilter(sOldFilterName, sOwner, sEntity);
			SearchFilterCache.getInstance().addFilter(searchFilter);
			// write back update SearchFilterVO
			newFilter.setSearchFilterVO(searchFilter.getSearchFilterVO());
		}
		catch (CommonStaleVersionException e) {
			String sMessage = localeDelegate.getMessage(
					"SearchFilterDelegate.4", "Der Suchfilter wurde zwischenzeitlich von einem anderen Benutzer ge\u00e4ndert. Bitte initialisieren Sie den Client.");
			LOG.info(sMessage);
			throw new NuclosBusinessException(sMessage, e);
		}
		catch (Exception e) {
			String sMessage = localeDelegate.getMessage(
					"SearchFilterDelegate.1", "Ein Fehler beim Speichern des Suchfilters ist aufgetreten!");
			LOG.error(sMessage);
			throw new NuclosFatalException(sMessage, e);
		}
	}

	private SearchFilter insertOrUpdateFilter(SearchFilter filter) {
		final Preferences root = domPreferencesFactory.userRoot();
		final Preferences node = root.node(filter.getSearchFilterVO().getFilterName());
		try {
			return createPrefs(filter, node);
		} catch (PreferencesException e) {
			String sMessage = localeDelegate.getMessage(
					"SearchFilterDelegate.1", "Ein Fehler beim Speichern des Suchfilters ist aufgetreten!");
			LOG.error(sMessage);
			throw new NuclosFatalException(sMessage, e);
		} catch (IOException e) {
			String sMessage = localeDelegate.getMessage(
					"SearchFilterDelegate.1", "Ein Fehler beim Speichern des Suchfilters ist aufgetreten!");
			LOG.error(sMessage);
			throw new NuclosFatalException(sMessage, e);
		} catch (BackingStoreException e) {
			String sMessage = localeDelegate.getMessage(
					"SearchFilterDelegate.1", "Ein Fehler beim Speichern des Suchfilters ist aufgetreten!");
			LOG.error(sMessage);
			throw new NuclosFatalException(sMessage, e);
		}
	}	
	
	private SearchFilter createPrefs(SearchFilter filter, Preferences prefs) throws PreferencesException, IOException, BackingStoreException {
		PutSearchConditionToPrefsVisitor.putSearchCondition(prefs.node(EntitySearchFilter2Support.PREFS_NODE_SEARCHCONDITION), 
				filter.getSearchCondition());
		if (filter instanceof EntitySearchFilter) {
			final EntitySearchFilter f = (EntitySearchFilter) filter;
			EntitySearchFilter.writeCollectableEntityFieldsToPreferences(prefs, f.getVisibleColumns(), 
					EntitySearchFilter2Support.PREFS_NODE_VISIBLECOLUMNS, EntitySearchFilter2Support.PREFS_NODE_VISIBLECOLUMNENTITIES);
			PreferencesUtils.putSerializableListXML(prefs, PREFS_NODE_COLLECTABLESORTING, f.getSortingOrder());
		}

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		prefs.exportSubtree(baos);
		filter.getSearchFilterVO().setFilterPrefs(baos.toString("UTF-8"));
		return filter;		
	}

	/**
	 * removes the given search filter
	 * @param filter
	 * @throws NuclosBusinessException
	 */
	public void removeSearchFilter(SearchFilter filter) throws NuclosBusinessException {
		if (!filter.getSearchFilterVO().isEditable()) {
			throw new NuclosBusinessException(localeDelegate.getMessage(
					"SearchFilterDelegate.5", "Der Suchfilter darf von Ihnen nicht gel\u00f6scht werden."));
		}

		try {
			searchFilterFacade.removeSearchFilter(filter.getSearchFilterVO());
			SearchFilterCache.getInstance().removeFilter(filter);
		}
		catch (CommonStaleVersionException e) {
			throw new NuclosBusinessException(localeDelegate.getMessage(
					"SearchFilterDelegate.7", "Ein Suchfilter konnte nicht gel\u00f6scht werden, da er zwischenzeitlich von einem anderen Benutzer ge\u00e4ndert wurde.\n" +
					"Bitte aktualisieren Sie den Datensatz und versuchen es erneut."));
		}
		catch (Exception e) {
			throw new NuclosFatalException(localeDelegate.getMessage(
					"SearchFilterDelegate.8", "Ein Suchfilter konnte nicht gel\u00f6scht werden"), e);
		}
	}

	/**
	 * get all entity searchfilter for the given user
	 * 
	 * @return Collection&lt;SearchFilter&gt;
	 */
	public Collection<SearchFilter> getAllSearchFilterByCurrentUser() {
		final Collection<SearchFilter> collSearchFilter = new ArrayList<SearchFilter>();
		try {
			for (SearchFilterVO filterVO : searchFilterFacade.getAllSearchFilterByCurrentUser()) {
				try {
					SearchFilter sf = makeSearchFilter(filterVO);
					if (sf != null)
						collSearchFilter.add(sf);
				}
				catch (Exception e) {
					LOG.error(localeDelegate.getMessage(
							"SearchFilterDelegate.9", "Ein Suchfilter konnte nicht geladen werden"));
				}
			}
		}
		catch (Exception e) {
			LOG.error(localeDelegate.getMessage(
					"SearchFilterDelegate.9", "Ein Suchfilter konnte nicht geladen werden"));
		}

		return collSearchFilter;
	}

	/**
	 * transforms a SearchFilterVO to a SearchFilter
	 * @param searchFilterVO
	 * @return SearchFilter
	 */
	private SearchFilter makeSearchFilter(SearchFilterVO searchFilterVO) {
		SearchFilter result = null;
		try {
			result = new EntitySearchFilter();
			result.setSearchFilterVO(searchFilterVO);

			final ByteArrayInputStream is = new ByteArrayInputStream(searchFilterVO.getFilterPrefs().getBytes("UTF-8"));

			Preferences prefs = null;
			try {
				prefs = domPreferencesFactory.read(is, false);
				
				// Fix for search filters that have not been saved in 'raw' (DOMPreferences) format. (tp)
				final String path = "org/nuclos/client/" + EntitySearchFilter2Support.PREFS_NODE_SEARCHFILTERS + "/" + searchFilterVO.getFilterName();
				if (prefs.nodeExists(path)) {
					prefs = prefs.node(path);
				}
			} catch (IOException e) {
				// parsing problem etc.
				LOG.warn("failed to parse search filter " + searchFilterVO.getFilterName() + ":\n" + searchFilterVO.getFilterPrefs());
				prefs = null;
			}
			if (prefs == null) {
				return null;
			}
			final UID sEntityName = searchFilterVO.getEntity();
			// SearchFilter properties
			result.setSearchCondition(SearchConditionUtils.getSearchCondition(
					prefs.node(EntitySearchFilter2Support.PREFS_NODE_SEARCHCONDITION),sEntityName));
			// EntitySearchFilter properties
			final EntitySearchFilter f = (EntitySearchFilter) result;
			f.setVisibleColumns(preferencesSupport.readCollectableEntityFieldsFromPreferences(prefs,
					DefaultCollectableEntityProvider.getInstance().getCollectableEntity(sEntityName),
					EntitySearchFilter2Support.PREFS_NODE_VISIBLECOLUMNS, EntitySearchFilter2Support.PREFS_NODE_VISIBLECOLUMNENTITIES));
			
			if (PreferencesUtils.nodeExists(prefs, PREFS_NODE_COLLECTABLESORTING)) {
				f.setSortingOrder((List<CollectableSorting>) PreferencesUtils.getSerializableListXML(prefs, PREFS_NODE_COLLECTABLESORTING, true));
			}
			// backward compatibility
			else {
				final List<CollectableSorting> sorting = new ArrayList<CollectableSorting>();
				for (String n: PreferencesUtils.getStringList(prefs, PREFS_NODE_SORTINGCOLUMNS)) {					
					sorting.add(new CollectableSorting(SystemFields.BASE_ALIAS, sEntityName, true, new UID(n), true));
				}
				f.setSortingOrder(sorting);
			}
			
			// prefsSearchfilter.removeNode();
		}
		catch (Exception e) {
			LOG.error(localeDelegate.getMessage("SearchFilterDelegate.11", "Fehler beim Transformieren des Filters") + ": " + e, e);
			if (result != null) {
				result.setValid(false);
				final SearchFilterVO vo = result.getSearchFilterVO();
				vo.setFilterName((vo.getFilterName() != null ? (vo.getFilterName()+" - ") : "") 
						+ localeDelegate.getMessage("SearchFilterDelegate.12", "Filter ist ung\u00fcltig"));
			}
		}

		return result;
	}

	public List<TranslationVO> getResources(UID id) throws CommonBusinessException {
		return searchFilterFacade.getResources(id);
	}
	
}
