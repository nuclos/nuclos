package org.nuclos.client.startup;

public abstract class AbstractLocalUserCache implements LocalUserCache {
	
	private transient boolean blnDeserialized = false;
	
	// Spring injection
	
	// end of Spring injection
	
	protected AbstractLocalUserCache() {
	}
	
	@Override
	public final void setDeserialized(boolean blnDeserialized) {
		this.blnDeserialized = blnDeserialized;
	}
	
	@Override
	public final boolean wasDeserialized() {
		return blnDeserialized;
	}
	
	@Override
	public final boolean isValid() {
		return wasDeserialized();
	}

}
