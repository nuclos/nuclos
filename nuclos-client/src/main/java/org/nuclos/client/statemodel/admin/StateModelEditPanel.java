//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.admin;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;

import org.nuclos.client.statemodel.StateModelEditor;
import org.nuclos.client.ui.collect.component.CollectableCheckBox;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.common.E;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * Details edit panel for state model administration. Contains the state model editor.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class StateModelEditPanel extends JPanel {

	private final JPanel pnlStateModelEditor;
	private final StateModelHeaderPanel pnlHeader;
	private final StateModelEditor statemodeleditor;
	
	public final JSplitPane splitpn;
	public final JSplitPane splitpnMain;

	private final CollectableCheckBox clctcbShowNumeral = new CollectableCheckBox(
			CollectableStateModel.clcte.getEntityField(E.STATEMODEL.showNumeral.getUID()));

	private final CollectableCheckBox clctcbShowIcon = new CollectableCheckBox(
			CollectableStateModel.clcte.getEntityField(E.STATEMODEL.showIcon.getUID()));

	public StateModelEditPanel(StateModelCollectController stateModelCollectController, SubForm subformUsages) {
		super(new BorderLayout());

		pnlStateModelEditor = new JPanel(new BorderLayout());
		pnlHeader = new StateModelHeaderPanel();
		statemodeleditor = new StateModelEditor(stateModelCollectController);
		
		splitpn = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, pnlStateModelEditor, newUsagePanel(subformUsages));
		splitpn.setOneTouchExpandable(true);
		splitpn.setResizeWeight(1d);
		splitpnMain = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, splitpn, statemodeleditor.getPropertiesPanel());
		splitpnMain.setOneTouchExpandable(true);
		splitpnMain.setResizeWeight(1d);
		
		this.add(splitpnMain, BorderLayout.CENTER);
		
		statemodeleditor.getPropertiesPanel().getStatePropertiesPanel().getStateDependantRightsPanel().setActionListenerForWidthChanged(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent ev) {
				splitpnMain.setDividerLocation(getSize().width
					- UIManager.getInt("SplitPane.dividerSize")
					- 2 // maybe a border
					- statemodeleditor.getPropertiesPanel().getStatePropertiesPanel().getStateDependantRightsPanel().LEFT_BORDER
					- statemodeleditor.getPropertiesPanel().getStatePropertiesPanel().getStateDependantRightsPanel().getPreferredSize().width);
			}
		});

		pnlStateModelEditor.add(pnlHeader, BorderLayout.NORTH);
		pnlStateModelEditor.add(statemodeleditor, BorderLayout.CENTER);
	}
	
	StateModelHeaderPanel getHeader() {
		return pnlHeader;
	}

	protected ArrayList<CollectableComponent> getCollectableComponents() {
		ArrayList<CollectableComponent> collectableComponents = new ArrayList<>();
		collectableComponents.add(clctcbShowIcon);
		collectableComponents.add(clctcbShowNumeral);
		return collectableComponents;
	}

	private Container newUsagePanel(JComponent subformUsages) {
		clctcbShowNumeral.setLabelText(SpringLocaleDelegate.getInstance().getMessage(
				E.STATEMODEL.showNumeral.getLocaleResourceIdForLabel(),""));
		clctcbShowNumeral.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				E.STATEMODEL.showNumeral.getLocaleResourceIdForDescription(),""));

		clctcbShowIcon.setLabelText(SpringLocaleDelegate.getInstance().getMessage(
				E.STATEMODEL.showIcon.getLocaleResourceIdForLabel(),""));
		clctcbShowIcon.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				E.STATEMODEL.showIcon.getLocaleResourceIdForDescription(),""));

		final JPanel pnlWrapper = new JPanel(new BorderLayout());
		final JLabel labSettings = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"StateModelEditPanel.2","Weitere Einstellungen"));
		final JPanel pnlSettingsContent = new JPanel();
		pnlSettingsContent.setBorder(BorderFactory.createEmptyBorder(4, 0, 0, 0));
		pnlSettingsContent.setLayout(new BoxLayout(pnlSettingsContent, BoxLayout.Y_AXIS));
		pnlSettingsContent.add(clctcbShowNumeral.getJComponent());
		pnlSettingsContent.add(clctcbShowIcon.getJComponent());
		final JPanel pnlModelSettings = new JPanel(new BorderLayout());
		pnlModelSettings.setBorder(BorderFactory.createEmptyBorder(0,4, 0, 32));
		pnlModelSettings.add(labSettings, BorderLayout.NORTH);
		pnlModelSettings.add(pnlSettingsContent, BorderLayout.CENTER);
		final JPanel pnlUsages = new JPanel(new BorderLayout());
		final JLabel labUsages = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"StateModelEditPanel.1","Verwendungen"));
		labUsages.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
		pnlUsages.add(labUsages, BorderLayout.NORTH);
		pnlUsages.add(subformUsages, BorderLayout.CENTER);
		pnlWrapper.add(pnlUsages, BorderLayout.CENTER);
		pnlWrapper.add(pnlModelSettings, BorderLayout.EAST);

		return pnlWrapper;
	}

	public StateModelEditor getStateModelEditor() {
		return this.statemodeleditor;
	}

}	// class StateModelEditPanel
