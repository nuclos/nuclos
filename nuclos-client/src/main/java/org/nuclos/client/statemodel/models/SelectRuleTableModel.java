//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.models;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;

/**
 * Table model for selection of rules
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class SelectRuleTableModel extends AbstractTableModel {

	private static final String[] asColumnNames = {
		SpringLocaleDelegate.getInstance().getMessage("SelectRuleTableModel.2","Regel"), 
		SpringLocaleDelegate.getInstance().getMessage("SelectRuleTableModel.1","Beschreibung")};

	private List<EventSupportSourceVO> lstRules;
	private String[] ruleTypesToUse;
	
	public SelectRuleTableModel(List<String> lstRuleTypesToUse) throws RemoteException {
		EventSupportRepository.getInstance().updateEventSupports();
		this.ruleTypesToUse = lstRuleTypesToUse.toArray(new String[lstRuleTypesToUse.size()]);
	}

	@Override
	public int getRowCount() {
		return lstRules != null ? lstRules.size() : 0;
	}

	public EventSupportSourceVO getRow(int iRow) {
		return lstRules.get(iRow);
	}

	@Override
	public int getColumnCount() {
		return asColumnNames.length;
	}

	@Override
	public String getColumnName(int column) {
		return asColumnNames[column];
	}

	@Override
	public Object getValueAt(int iRow, int iColumn) {
		Object result = null;
		switch (iColumn) {
			case 0:
				result = getRow(iRow).getName();
				break;
			case 1:
				result = getRow(iRow).getDescription();
				break;
		}
		return result;
	}

	public void setExcludeRules(List<EventSupportSourceVO> lstExcludeRules) throws RemoteException {
		List<EventSupportSourceVO> eventSupportsByTypes = 
				EventSupportRepository.getInstance().getEventSupportsByTypes(this.ruleTypesToUse);
		
		this.lstRules = new ArrayList<EventSupportSourceVO>();
		
		for (EventSupportSourceVO eseVO : eventSupportsByTypes) {
			if (!lstExcludeRules.contains(eseVO)) {
				this.lstRules.add(eseVO);
			}
		}
	}

	public List<EventSupportSourceVO> getRules() {
		return lstRules;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

}  // class SelectRuleTableModel
