package org.nuclos.client.statemodel.panels;

import java.rmi.RemoteException;

import org.nuclos.client.rule.server.panel.EventSupportSelectionDataChangeListener;


public interface TransitionDataChangeListener extends EventSupportSelectionDataChangeListener{
	// automatic transition
	void transitionAutomaticDataChange(boolean isAutomaticTransition) throws RemoteException;
	
	// default transition
	void transitionDefaultDataChange(boolean isDefaultTransition) throws RemoteException;

	// nonstop transition
	void transitionNonstopDataChange(boolean isDefaultTransition) throws RemoteException;

}