//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.panels.rights;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.multimap.MultiListHashMap;

public abstract class RightTransfer implements Transferable{
	
	public abstract RoleRights getAllRoleRights();
	
	public abstract RoleRight getRoleRight(UID role);
	
	public abstract void setAllRoleRights(RoleRights rr);
	
	public abstract void setRoleRight(RoleRight rr);
	
	/**
	 * 
	 * holder for all role rights
	 */
	public static class RoleRights {
		public final Map<UID, RoleRight> rights = new HashMap<UID, RightTransfer.RoleRight>();
		public final Set<UID> mandatoryFields = new HashSet<UID>();
		public final Set<Pair<UID, String>> mandatoryColumns = new HashSet<Pair<UID, String>>();
		public final Set<Pair<UID, UID>> rightsEnabled = new HashSet<Pair<UID, UID>>();
	}
	
	/**
	 * 
	 * holder for rights from one role
	 */
	public static class RoleRight {
		public final Map<UID, Object> groupRights = new HashMap<>();
		public final Map<UID, Integer> subformRights = new HashMap<UID, Integer>();
		public final Map<Pair<UID, UID>, Boolean> subformGroupRights = new HashMap<Pair<UID, UID>, Boolean>();
		/** for saving to statevo: */
		public final Set<UID> groupIsSubform = new HashSet<UID>();
		public final Map<UID, String> groupNames = new HashMap<UID, String>();
		public final MultiListHashMap<UID, Pair<UID, String>> subformGroups = new MultiListHashMap<UID, Pair<UID, String>>();
	}
	
	public static class OneRoleRightsDataFlavor extends DataFlavor {
		
		public static final OneRoleRightsDataFlavor FLAVOR = new OneRoleRightsDataFlavor();
		
		private OneRoleRightsDataFlavor() {
			super(RoleRight.class, "OneRoleRights");
		}
	}
	
	public static class AllRoleRightsDataFlavor extends DataFlavor {
		
		public final static AllRoleRightsDataFlavor FLAVOR = new AllRoleRightsDataFlavor();
		
		private AllRoleRightsDataFlavor() {
			super(RoleRights.class, "AllRoleRights");
		}
	}
	
	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[]{ RightTransfer.AllRoleRightsDataFlavor.FLAVOR };
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return flavor instanceof RightTransfer.AllRoleRightsDataFlavor;
	}

	@Override
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
		return getAllRoleRights();
	}
}
