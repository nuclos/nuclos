//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.task;

import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.table.CommonJTableWithIcons;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.tasklist.TasklistDefinition;

@SuppressWarnings("serial")
public class DynamicTaskView extends TaskView {

	private final TasklistDefinition def;
	private final DynamicTasklistVO dynamicTasklist;

	private final JMenuItem btnReset = new JMenuItem();

	private final JTable tbl = new CommonJTableWithIcons();

	private final List<ChangeListener> listeners = new ArrayList<>();

	private boolean bLock = false;

	public DynamicTaskView(TasklistDefinition def, DynamicTasklistVO dynamicTasklist) {
		this.def = def;
		this.dynamicTasklist = dynamicTasklist;
	}

	@Override
	public void init() {
		super.init();

		scrlpn.getVerticalScrollBar().getModel().addChangeListener(ev -> {
			if (bLock) {
				//LOG.trace("Ignoring stateChanged event because lock is already set.");
			}
			else if (scrlpn.getVerticalScrollBar().getModel().getValueIsAdjusting()) {
				//LOG.trace("Ignoring stateChanged event because valueIsAdjusting.");
			}
			else {
				try {
					// set lock to avoid unwanted recursion:
					bLock = true;

					final JTable tbl = this.getTable();
					final int iMaxYBeforeMove = UIUtils.getMaxVisibleY(tbl);
					final int iLastRowBeforeMove = TableUtils.getLastVisibleRow(tbl);

					final Point p = scrlpn.getViewport().getViewPosition();
					final int iShiftY = scrlpn.getVerticalScrollBar().getModel().getValue() - p.y;
					final int iMaxYAfterMove = iMaxYBeforeMove + iShiftY;
					final int iLastRowAfterMove = TableUtils.getLastVisibleRow(tbl, iMaxYAfterMove);

					final int iLastRow = iLastRowAfterMove;

					if (iLastRow != -1 && getTable().getRowCount() == iLastRow + 1) {
						fireScrollEndEvent(new ChangeEvent(scrlpn.getVerticalScrollBar()));
					}
				} finally {
					bLock = false;
				}
			}
		});

		getTable().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				if (KeyEvent.VK_DOWN == e.getKeyCode() && getTable().isRowSelected(getTable().getRowCount() - 1)) {
					fireScrollEndEvent(new ChangeEvent(getTable()));
				}
			}
		});

		getTable().addMouseWheelListener(e -> {
			if (e.getWheelRotation() > 0 && !scrlpn.getVerticalScrollBar().isVisible()) {
				fireScrollEndEvent(new ChangeEvent(getTable()));
			} else {
				scrlpn.dispatchEvent(e);
			}
		});

		scrlpn.addMouseWheelListener(e -> {
			if (e.getWheelRotation() > 0 && !scrlpn.getVerticalScrollBar().isVisible()) {
				fireScrollEndEvent(new ChangeEvent(getTable()));
			}
		});
	}

	public JMenuItem getResetMenuItem() {
		return btnReset;
	}

	public TasklistDefinition getDef() {
		return def;
	}

	public DynamicTasklistVO getDynamicTasklist() {
		return dynamicTasklist;
	}

	@Override
	protected List<JComponent> getToolbarComponents() {
		return null;
	}

	@Override
	protected List<JComponent> getExtrasMenuComponents() {
		List<JComponent> result = new ArrayList<JComponent>();
		result.add(btnReset);
		return result;
	}

	@Override
	protected JTable getTable() {
		return tbl;
	}

	public void addScrollEndListener(ChangeListener cl) {
		this.listeners.add(cl);
	}

	public void removeScrollEndListener(ChangeListener cl) {
		this.listeners.remove(cl);
	}

	private void fireScrollEndEvent(ChangeEvent ev) {
		for (ChangeListener cl : this.listeners) {
			cl.stateChanged(ev);
		}
	}
}
