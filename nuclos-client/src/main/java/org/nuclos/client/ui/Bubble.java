//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.IllegalComponentStateException;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import org.apache.log4j.Logger;

/*
 * Graphic element, that is used to display information for the User.
 * @arg JComponent parent: parent component - the Bubble will be positioned relative to this
 * @arg String text: the text to be displayed
 * @arg Integer timeout: the amount of time in seconds after which the Bubble will be closed
 * @arg BubbleUtils.Position pos: where the Bubble should be positioned relative to the parent. This Object also
 * 		implements the Shape of the Bubble.
 *
 * NOTE: There is a Java BUG in Java Versions < 16. When the Bubble gets spawned on the secondary monitor, the position
 * for a fraction of a second is wrong, then it gets corrected.
 * If there comes a day, where no one uses a Version below 16, it should be checked, if the relocating
 * in the setVisible() Method and the windowOpened() Method is still really necessary.
 */
public class Bubble extends JWindow implements AncestorListener, WindowListener {

	private static final Logger LOG = Logger.getLogger(Bubble.class);

	private static final long serialVersionUID = 2444945538413906843L;

	protected JComponent parent;
	private Window windowAncestor;

	private BubbleUtils.Position pos;
	private String text;
	private long doNotDisposeBefore;
	private Shape windowShape;
	private Dimension prefSize;

	public Bubble(String text) {
		this(null, text, null, BubbleUtils.Position.NE, null);
	}

	public Bubble(JComponent parent, String text) {
		this(parent, text, null, BubbleUtils.Position.NE, null);
	}

	public Bubble(JComponent parent, String text, Integer timeout) {
		this(parent, text, timeout, BubbleUtils.Position.NE, null);
	}

	public Bubble(JComponent parent, String text, Integer timeout, BubbleUtils.Position pos) {
		this(parent, text, timeout, pos, null);
	}

	/*
	 * Initialises the Bubble.
	 * A JLabel is used to determine the size of the Bubble.
	 * A BubbleContent-Object is created and appended to the contentPane of the Bubble.
	 * A MouseListener is created to make sure the Bubble stays close to the parent Object when it moves.
	 * A Timer is created to regulate when the Bubble is to be closed.
	 * The pos-Object is used to determine the exact Shape of the Bubble.
	 */
	public Bubble(JComponent parent, String text, Integer timeout, BubbleUtils.Position pos, Long minTimeMs) {
		super(parent.getGraphicsConfiguration());  // use graphics configuration of parent - so it won't default to monitor 0
		this.pos = pos;
		this.text = text;
		this.parent = parent;
		this.initializeContent();
		try{
			this.setOpacity(0.9f);
		}catch(UnsupportedOperationException e){
			LOG.warn("Transparency not supported.");
		}

		setAlwaysOnTop(true);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(final WindowEvent e) {
				super.windowOpened(e);
				relocate(Bubble.this.parent);
			}
		});
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { Bubble.this.dispose(); }
		});
		if(parent != null) {
			parent.addAncestorListener(this);
			this.windowAncestor = SwingUtilities.getWindowAncestor(parent);
			if(windowAncestor != null) {
				windowAncestor.addWindowListener(this);
			}
			relocate(parent);
		}

		this.doNotDisposeBefore = System.currentTimeMillis();
		if (timeout != null) {

			Long _minTimeMs = minTimeMs != null ? minTimeMs : Math.min(timeout*1000L, 1000L);
			this.doNotDisposeBefore += _minTimeMs;

			final Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						dispose();
					} catch (Exception e) {
						LOG.error("bubble dispose timer task failed: " + e, e);
					}
				}}, timeout * 1000L);

			disposalTimerInitialized = true;
		}
	}

	private void initializeContent() {
		this.getContentPane().removeAll();
		JLabel textLabel = new JLabel();
		textLabel.setFont(new Font("System", Font.PLAIN, 11));
		textLabel.setText(text);
		Dimension textSize = textLabel.getPreferredSize();
		int width = textSize.width + 2 * BubbleUtils.MAX_ARC_SIZE;
		int height = textSize.height + 2 * BubbleUtils.MAX_ARC_SIZE + (int) (textSize.height * pos.arrowRelLength);
		super.setSize(width,height);
		prefSize = new Dimension(width, height);
		this.windowShape = pos.getShape(width, height, 1);
		this.setShape(windowShape);
		this.getContentPane().add(new BubbleContent(textLabel, pos, this));
		revalidate();
	}

	private boolean disposalTimerInitialized = false;

	/*
	 * Disposes the Bubble.
	 */
	@Override
	public void dispose() {
		final Timer timer = new Timer();
		long timeToWait = doNotDisposeBefore - System.currentTimeMillis();
		if (timeToWait < 0) timeToWait = 0;
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					if (parent != null) {
						parent.removeAncestorListener(Bubble.this);
					}
					if (windowAncestor != null) {
						windowAncestor.removeWindowListener(Bubble.this);
					}
				} catch (Exception e) {
					LOG.warn(e.getMessage(), e);
				} finally {
					Bubble.super.dispose();
				}
			}}, timeToWait);
	}

	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			super.setVisible(true);
			relocate(this.parent);
		} else {
			super.setVisible(false);
		}
	}

	/*
	 * Relocates the Bubble relative to its parent.
	 * NUCLOS-10023: relocating the bubble now checks, if the new location is completely on screen
	 * If relocating leads to the bubble spawning (partly) outside the screen, it loops through all possible positions,
	 * in order to find a valid placement.
	 */
	private void relocate(Component parent) {
		try {
			Point target = new Point(parent.getLocationOnScreen());
			pos.relocate(this, parent, target, parent.getSize(), prefSize);
			for (int i = 0; i < BubbleUtils.Position.values().length && !isBubbleLocationValid(); i++) {
				// hide the bubble while relocating
				super.setVisible(false);

				pos = BubbleUtils.Position.values()[i];
				pos.relocate(this, parent, target, parent.getSize(), prefSize);
			}
			if (!isVisible()) {
				this.initializeContent();
				// note the "super" keyword -> using "this" would throw a stackoverflow exception
				super.setVisible(true);
			}
			this.setShape(windowShape);
			this.repaint();
		} catch (IllegalComponentStateException ex) {
			dispose();
		}
	}

	/**
	 * Checks if the bubble is fully inside the screen area.
	 * It takes the monitor, which holds the parent, into account.
	 * @return
	 */
	private boolean isBubbleLocationValid() {
		return !(this.getLocation().x < parent.getGraphicsConfiguration().getBounds().x
				|| this.getLocation().y < parent.getGraphicsConfiguration().getBounds().y
				|| (this.getLocation().x + this.getWidth()) > parent.getGraphicsConfiguration().getBounds().getMaxX()
				|| (this.getLocation().y + this.getHeight()) > parent.getGraphicsConfiguration().getBounds().getMaxY());
	}

	@Override
	public void ancestorAdded(AncestorEvent event) {}

	@Override
	public void ancestorRemoved(AncestorEvent event) {
		if (!disposalTimerInitialized) {
			Bubble.this.dispose();
		} else {
			//NUCLOS-5230 Do nothing, if disposal timer is running anyway.
		}
	}

	@Override
	public void ancestorMoved(AncestorEvent event) {
		relocate(event.getComponent());
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		Bubble.this.dispose();
	}

	@Override
	public void windowIconified(WindowEvent e) {
		Bubble.this.dispose();
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		Bubble.this.dispose();
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}


	/*
	 * Just for testing.
	 */
	/*public static void main(String[] args) {
		//https://docs.oracle.com/javase/8/docs/technotes/guides/2d/flags.html#xrender
		System.out.println(System.getProperty("os.name"));
		System.out.println(System.getProperty("java.version"));
		System.out.println(System.getProperty("sun.java2d.xrender"));

		System.setProperty("sun.java2d.xrender", "false");

		final String text = "<html>"
				+ "Das Hähnchenfleisch in ca. 2 cm große Würfel schneiden und zusammen<br>"
				+ "mit Weißwein, Rosmarin, Salz und Pfeffer in eine Schüssel<br>"
				+ "geben. Abdecken und mindestens 1 Stunde marinieren.  Danach das<br>"
				+ "Fleisch abtropfen lassen und auf 8 Spieße stecken, die Marinade<br>"
				+ "aufheben. Die Spieße werden nun 10 min. auf dem Grill (falls keiner<br>"
				+ "vorhanden unter dem vorgeheizten Backofengrill) gegrillt.<br>"
				+ "</html>";


		JFrame f = new JFrame();
		f.add(
				new JButton(
						new AbstractAction("Show") {

							@Override
							public void actionPerformed(ActionEvent e) {
								new Bubble(
										(JComponent) e.getSource(),
										text,
										10,
										BubbleUtils.Position.NW)
										.setVisible(true);
								new Bubble(
										(JComponent) e.getSource(),
										text,
										10,
										BubbleUtils.Position.NE)
										.setVisible(true);
								new Bubble(
										(JComponent) e.getSource(),
										text,
										10,
										BubbleUtils.Position.CENTER)
										.setVisible(true);
								new Bubble(
										(JComponent) e.getSource(),
										text,
										10,
										BubbleUtils.Position.SW)
										.setVisible(true);
								new Bubble(
										(JComponent) e.getSource(),
										text,
										10,
										BubbleUtils.Position.SE)
										.setVisible(true);
							}
						}
				),
				BorderLayout.CENTER);
		f.pack();
		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		f.setBounds(600, 400, 200, 100);
		f.setVisible(true);
	}*/

}
