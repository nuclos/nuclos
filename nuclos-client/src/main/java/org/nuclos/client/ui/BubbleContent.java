package org.nuclos.client.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.nuclos.client.theme.NuclosThemeSettings;

/*
 * Used in conjunction with the Bubble-class. This Object implements the Content of the Bubble, as the 'paint'
 * -method of classes extending JWindow should not be overwritten.
 * The Object is meant to be appended to the contentPane of the Bubble.
 * @arg JLabel textLabel: The Label containing the information to be displayed.
 * @arg int width, height: Width and height of the surrounding Bubble-Object.
 * @arg BubbleUtils.Position pos: where the Bubble should be positioned relative to the parent. This Object also
 * 		implements the Shape of the Bubble.
 * @arg JComponent parent: The parent of the Bubble-Object.
 */
public class BubbleContent extends JPanel{

    private JLabel textLabel;
    private BubbleUtils.Position pos;
    private Bubble bubble;

    /*
     * Initializes the BubbleContent-Object.
     * Importantly the bubbleShape-Object is calculated, which is later used to paint the BubbleContent.
     */
    BubbleContent(JLabel textLabel, BubbleUtils.Position pos, Bubble bubble) {
        this.pos = pos;
        this.textLabel = textLabel;
        this.bubble = bubble;
	}

    /*
     * This method creates the actual content.
     * First, the background is painted.
     * Second, the Shape is outlined.
     * Lastly, the text is written.
     */
    @Override
    public void paint(Graphics g){
        Graphics2D g2 = (Graphics2D) g;

        //This line can be removed, if Java >= 16
		//In prior versions Java has a bug, where the panel for some reason has the Configuration of the primary screen
		//even though it is created for the secondary monitor
        g2.setTransform(bubble.parent.getGraphicsConfiguration().getDefaultTransform());

        Bubble b = (Bubble) this.getParent().getParent().getParent().getParent();

		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setClip(b.getShape());
		g2.setColor(NuclosThemeSettings.BUBBLE_FILL_COLOR);
        g2.fillRect(0, 0, getWidth(), getHeight());

		BasicStroke bs = new BasicStroke(6f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND);
        g2.setStroke(bs);

        g2.setColor(NuclosThemeSettings.BUBBLE_BORDER_COLOR);
		g2.draw(b.getShape());

		BasicStroke bs2 = new BasicStroke(3f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND);
		g2.setStroke(bs2);

		g2.setColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
		g2.draw(b.getShape());

        Graphics gtl = g2.create();
        pos.translateForLabel(gtl, getWidth(), getHeight(), BubbleUtils.MAX_ARC_SIZE);

        gtl.translate(
                Math.min(Math.min(getWidth(), getHeight()), BubbleUtils.MAX_ARC_SIZE)/2,
                Math.min(Math.min(getWidth(), getHeight()), BubbleUtils.MAX_ARC_SIZE)/4);

        textLabel.setSize(textLabel.getPreferredSize());
        textLabel.paint(gtl);
    }

}
