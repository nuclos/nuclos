//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JScrollPane;

import org.nuclos.client.ui.labeled.LabeledTextArea;

/**
 * <code>JScrollPane</code> that may not be smaller than its preferred size.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 * 
 * @deprecated This is the main reason for many swing initialization problems with {@link LabeledTextArea}. Avoid! (tp)
 */
public class CommonJScrollPane extends JScrollPane {

	private final Component compView;

	public CommonJScrollPane() {
		super();
		compView = null;
	}

	public CommonJScrollPane(Component compView) {
		super(compView);
		this.compView = compView;
	}

	/*
	public CommonJScrollPane(int iVerticalScrollBarPolicy, int iHorizontalScrollBarPolicy) {
		super(iVerticalScrollBarPolicy, iHorizontalScrollBarPolicy);
		this.compView = null;
	}
	 */

	public CommonJScrollPane(Component compView, int iVerticalScrollBarPolicy, int iHorizontalScrollBarPolicy) {
		super(compView, iVerticalScrollBarPolicy,  iHorizontalScrollBarPolicy);
		this.compView = compView;
	}

	@Override
	public Dimension getMinimumSize() {
		return super.getPreferredSize();
	}
	
	protected Component getView() {
		return compView;
	}

}  // class CommonJScrollPane
