package org.nuclos.client.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JTextField;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.PreferencesException;

public class FormatUtils {

	private static final Logger LOG = Logger.getLogger(FormatUtils.class);

	private FormatUtils() {
		// Never invoked.
	}

	/**
	 * sets up a contained textfield according to the type of this component.
	 * Takes special care for dates and numbers.
	 * @param ntf
	 */
	public static void setupTextField(CollectableEntityField clctef, CommonJTextField ntf, boolean bSearchable) {
		// set the preferred width for numbers and dates:
		final Class<?> cls = clctef.getJavaClass();
		if (Number.class.isAssignableFrom(cls)) {
			ntf.setColumnWidthChar('0');
			// numbers are right aligned:
			ntf.setHorizontalAlignment(JTextField.TRAILING);
			ntf.setFormat(getCollectableFieldFormat(clctef), clctef);
			ntf.setPattern(clctef.getFormatOutput());
		}
		else if (Date.class.isAssignableFrom(cls)) {
			/** @todo this could be enhanced by calculating the width of "01.01.2000" */
			ntf.setColumnWidthChar('0');
			ntf.setFormat(getCollectableFieldFormat(clctef), clctef);
			ntf.setPattern(clctef.getFormatOutput());
		}
		else if (Boolean.class.isAssignableFrom(cls)) {
			ntf.setColumnWidthChar('0');
			ntf.setFormat(getCollectableFieldFormat(clctef), clctef);
		}
		else if (String.class.isAssignableFrom(cls)) {
			ntf.setMaxTextLength(clctef.getMaxLength());
			ntf.setFormat(getCollectableFieldFormat(clctef), clctef);
		}

		if (bSearchable)
			addAutoComplete(clctef, ntf, getAutoCompletePreferences(clctef));
	}
	
	public static CollectableFieldFormat getCollectableFieldFormat(CollectableEntityField clctef) {
		// set the preferred width for numbers and dates:
		final Class<?> cls = clctef.getJavaClass();
		final CollectableFieldFormat result = CollectableFieldFormat.getInstance(cls);
		return result;
	}

	public static CollectableFieldFormat getCollectableFieldFormat(FieldMeta<?> field) {
		// set the preferred width for numbers and dates:
		try {
			final Class<?> cls = LangUtils.getClassLoaderThatWorksForWebStart().loadClass(field.getDataType());
			final CollectableFieldFormat result = CollectableFieldFormat.getInstance(cls);
			return result;
		}
		catch (ClassNotFoundException e) {
			throw new IllegalStateException(e);
		}
	}

	public static CollectableFieldFormat getCollectableFieldFormat(UID field) {
		final IMetaProvider mprov = MetaProvider.getInstance();
		return getCollectableFieldFormat(mprov.getEntityField(field));
	}
	
	public static Preferences getAutoCompletePreferences(CollectableEntityField clctef) {
		if (clctef.getCollectableEntity() != null) {
			return ClientPreferences.getInstance().getUserPreferences()
					.node("collect").node("entity").node(clctef.getCollectableEntity().getUID().getString())
					.node("fields");
		}
		return null;
	}

	public static void addAutoComplete(CollectableEntityField clctef, CommonJTextField ntf, Preferences prefs) {
		if (prefs != null) {
			List<String> items = new ArrayList<String>();
			try {
				items = PreferencesUtils.getStringList(prefs, clctef.getUID().toString());
			}
			catch (PreferencesException e) {
				LOG.warn("addAutoComplete: " + e);
			}
			ntf.addAutoCompleteItems(items);
		}
	}

}
