package org.nuclos.client.ui;

public interface IWriteComponent {
	
	void write(String s);

}
