// Copyright (C) 2011 Novabit Informationssysteme GmbH
//
// This file is part of Nuclos.
//
// Nuclos is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nuclos is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Nuclos. If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.client.common.Utils;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.layoutml.MnemonicUtils;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;

/**
 * JInfoTabbedPane is an extension of JTabbedPane that is able to display
 * additional (size) information on each tab.
 * <p>
 * In Nuclos it is used to display the element size of a (one and only) subform
 * embedded onto a tab (NUCLOSINT-63). LayoutMLParser now only uses
 * JInfoTabbedPane (instead of JTabbedPane).
 * </p>
 *
 * @see SizeKnownListener
 * @author Thomas Pasch
 * @since Nuclos 3.1.00
 */
public class JInfoTabbedPane extends JTabbedPane implements ChangeListener {

	private final List<TabInfo> tabInformation = new ArrayList<>();

	public JInfoTabbedPane() {
		super();

		this.addChangeListener(this);
		setupTabTraversalKeys();
	}

	@Override
	public void insertTab(String title, Icon icon, Component component, String tip, int index) {
		super.insertTab(title, icon, component, tip, index);
		TabInfo tabInfo = new TabInfo(title);
		tabInformation.add(index, tabInfo);
		setTabComponent(title, index);
		componentAdded(component);

		notifyForInitialTabSelection();
	}
	
	@Override
	public void removeAll() {
		super.removeAll();

		tabInformation.clear();
	}
	
	@Override
	public void remove(int index) {
		super.remove(index);

		tabInformation.remove(index);
	}
	
	@Override
	public void remove(Component component) {
		int idx = indexOfComponent(component);
		super.remove(component);
		if (idx != -1) {
			tabInformation.remove(idx);
		}
	}

	@Override
	public void setTitleAt(int index, String title) {
		super.setTitleAt(index, title);
		setTabComponent(title, index);
	}

	private void setTabComponent(String text, int index) {
		JLabel tabComponent = new JLabel(text);
		int height = tabComponent.getFontMetrics(tabComponent.getFont()).getHeight();
		int width = tabComponent.getFontMetrics(tabComponent.getFont()).stringWidth(text + " (>999) XX"); // text + " (>999) X" (+"X" as an small overhead) ...so we get the max length.
		tabComponent.setPreferredSize(new Dimension(width, height));
		tabComponent.setHorizontalAlignment(SwingConstants.CENTER);
		setTabComponentAt(index, tabComponent);
	}

	public TabInfo getTabInfoAt(int tab) {
		return tabInformation.get(tab);
	}

	/**
	 * Set the additional (size) information of the tab.
	 */
	public void setTabInfoAt(int tab, Integer info) {
		TabInfo tabInfo = tabInformation.get(tab);
		final Integer oldInfo = tabInfo.tabInfo;
		if (!ObjectUtils.equals(info, oldInfo)) {
			tabInfo.tabInfo = info;
			updateTab(tab, true);
		}
	}

	public void setTabTitleAttribute(int tab, String attribute) {
		TabInfo tabInfo = tabInformation.get(tab);
		tabInfo.tabTitleAttr = attribute;
	}

	public void setTabVisibilityAttribute(int tab, String attribute) {
		TabInfo tabInfo = tabInformation.get(tab);
		tabInfo.visibilityAttr = attribute;
	}

	public void updateTab(int tab) {
		updateTab(tab, false);
	}

	private void updateTab(int tab, boolean bAddCount) {
		if (tab >= getTabCount())
			return;
		TabInfo tabInfo = tabInformation.get(tab);
		tabInfo.tabWithSizeInfo = bAddCount;
		final JLabel label = (JLabel) getTabComponentAt(tab);
		final String title = tabInfo.tabTitle;
		String text;
		if (bAddCount) {
			final Integer info = tabInfo.tabInfo;
			if (info == null) {
				text = title + " *";
			} else {
				int j = info;
				if (j < 0) {
					if (j > -1000) {
						text = title + " (" + j + ")";
					}
					else {
						text = title + " -?";
					}
				}
				else if (j > 999) {
					text = title + " (>999)";
				}
				else {
					text = title + " (" + j + ")";
				}

				setBackgroundAt(tab, j == 0 ? Color.WHITE : Utils.translateColorFromParameter(ParameterProvider.TAB_BACKGROUND_COLOR, Color.WHITE));
			}
		} else {
			text = title;
		}

		text = getMnemonicTextIfAny(text, tab);
		label.setText(text);
	}

	/**
	 * parse text for Mnemonic and underline it
	 * @param text
	 * @param tab
	 * @return
	 */
	private String getMnemonicTextIfAny(String text, int tab) {
		int keycode = this.getMnemonicAt(tab);
		try {
			if(keycode > 0) {
				/*
				keycode += 32;
				byte b[] = {(byte)keycode};
				String sKey = new String(b);
				int index = text.indexOf(sKey);
				 */
				
				// case-insensitive replacement of tab hot key
				String sKey = MnemonicUtils.keycode2String(keycode);
				int index = text.indexOf(sKey);
				if(index >= 0) {
					String sBeforeMnemonic = text.substring(0, index);
					String sAfterMnemonic = text.substring(index+1);
					text = "<html>" + sBeforeMnemonic + "<u>" + sKey + "</u>" + sAfterMnemonic + "</html>";
				}
				else {
					text = "<html>" + text + "&nbsp;<u>" + sKey + "</u>" + "</html>";
				}

			}
		}
		catch (Exception e) {
			// if any Exception return default text
		}
		return text;
	}
	
	private void componentAdded(Component comp) {
		findAndNotifySubforms(comp, SubForm.NOTIFY_ADDED_TO_TABBEDPANE);
	}

	public void notifyForInitialTabSelection() {
		notifySelections(true);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() instanceof JInfoTabbedPane) {
			JInfoTabbedPane pane = (JInfoTabbedPane) e.getSource();
			pane.notifySelections(false);
		}
	}

	private void notifySelections(boolean initial) {
		Component selected = getSelectedComponent();
		for (int i = 0; i < getTabCount(); i++) {
			Component comp = getComponentAt(i);
			boolean bCompIsSelected = comp == selected;
			int n = bCompIsSelected ? SubForm.NOTIFY_TABBEDPANE_SELECTION : SubForm.NOTIFY_TABBEDPANE_DESELECTION;
			if (initial && bCompIsSelected) {
				findAndNotifySubformsLater(comp, n);
			} else {
				findAndNotifySubforms(comp, n);
			}
		}
	}
	
	private void findAndNotifySubformsLater(final Component comp, final int message) {
		Runnable r = () -> findAndNotifySubforms(comp, message);
		SwingUtilities.invokeLater(r);
	}
	
	private void findAndNotifySubforms(Component comp, int message) {
		if (comp instanceof SubForm) {
			((SubForm)comp).notifySelection(message);
			
		} else if (comp instanceof Container) {
			Container container = (Container)comp;
			for (Component child : container.getComponents()) {
				findAndNotifySubforms(child, message);
			}
			
		}
	}

	private  void setupTabTraversalKeys() {
		KeyStroke ctrlTab =KeyStroke.getKeyStroke(KeyEvent.VK_TAB, KeyEvent.CTRL_MASK);
		Set<AWTKeyStroke> forwardKeys = new HashSet<>(this.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));
		forwardKeys.remove(ctrlTab);
		this.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, forwardKeys);
		InputMap inputMap = this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		InputMap inputMap2 = this.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(ctrlTab, "navigateNext");
	    inputMap2.put(ctrlTab, "navigateNext");
		  
	}

	public <Clct extends Collectable> void refreshTabTitleAttribute(final Clct selectedCollectable, final UID language) {
		if (selectedCollectable != null) {
			for (int i = 0; i < tabInformation.size(); i++) {
				TabInfo tabInfo = tabInformation.get(i);
				if (tabInfo == null || tabInfo.tabTitleAttr == null) {
					continue;
				}

				final CollectableField field = selectedCollectable.getField(UID.parseUID(tabInfo.tabTitleAttr));

				if (field.getValue() != null) {
					tabInfo.tabTitle = field.toString();
					this.updateTab(i, tabInfo.tabWithSizeInfo);
				}
			}
		}
	}
}
