//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.lang.ref.WeakReference;
import java.util.LinkedList;

import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.main.mainframe.MainFrameTabbedPane;
import org.nuclos.client.ui.collect.SearchOrDetailsPanel;

public class MainFrameTabController extends Controller<MainFrameTab> {

	private final LinkedList<WeakReference<SearchOrDetailsPanel>> panelsToClose = new LinkedList<WeakReference<SearchOrDetailsPanel>>();
	
	public MainFrameTabController(MainFrameTab parent) {
		super(parent);
	}
	
	public MainFrameTab getTab() {
		return getParent();
	}
	
	public MainFrameTabbedPane getTabbedPane() {
		return MainFrame.getTabbedPane(getParent());
	}
	
	protected void registerPanelForClosing(SearchOrDetailsPanel panel) {
		synchronized (panelsToClose) {
			panelsToClose.add(new WeakReference<SearchOrDetailsPanel>(panel));
		}
	}
	
	protected void closeRegisteredPanels() {
		synchronized (panelsToClose) {
			for (WeakReference<SearchOrDetailsPanel> ref: panelsToClose) {
				final SearchOrDetailsPanel panel = ref.get();
				if (panel != null) {
					panel.close();
				}
			}
		}
	}

}
