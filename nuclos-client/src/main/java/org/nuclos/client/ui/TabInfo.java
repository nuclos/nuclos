package org.nuclos.client.ui;

public class TabInfo {

	/**
	 * Tab title text without the additional (size) information.
	 */
	public String tabTitle;

	/**
	 * Attribute name to use for the tab title. If null, the constant {@code tabTitle} attribute is used.
	 */
	public String tabTitleAttr;

	/**
	 * Specifies a boolean attribute of the main entity object, which should determine the visibility of a tab.
	 */
	public String visibilityAttr;

	public boolean tabWithSizeInfo;

	/**
	 * The additional (size) information for each tab. If <code>null</code> and
	 * the corresponding displayTabInfo is <code>true</code> the 'size is
	 * loading' state is displayed.
	 */
	public Integer tabInfo;

	TabInfo() {

	}

	TabInfo(String tabTitle) {
		this.tabTitle = tabTitle;
	}

}
