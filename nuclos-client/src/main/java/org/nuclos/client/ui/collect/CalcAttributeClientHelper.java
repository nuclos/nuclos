package org.nuclos.client.ui.collect;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.nuclos.client.datasource.admin.ParameterPanel;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;

public class CalcAttributeClientHelper {

	public static String showCalcAttributesParamValueEditor(Component parent, String sLabel, UID calcAttributeDS, String initialCalcAttributeParamValues) {
		if (calcAttributeDS == null) {
			return null;
		}
		final List<DatasourceParameterVO> liParamsEmpty = new ArrayList<DatasourceParameterVO>();
		
		final DatasourceFacadeRemote datasourceFacadeRemote = SpringApplicationContextHolder.getBean(DatasourceFacadeRemote.class);
		try {
			final Map<String, Object> mpParams = CalcAttributeUtils.stringToMap(initialCalcAttributeParamValues);
			boolean hasIntidParameter = false;
			for (DatasourceParameterVO paramvo : datasourceFacadeRemote.getCalcAttributeParameters(calcAttributeDS)) {
				if ("intid".equals(paramvo.getParameter())) {
					hasIntidParameter = true;
				} else {
					liParamsEmpty.add(paramvo);
				}
			}
			if (!hasIntidParameter) {
				throw new NuclosBusinessException(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.34", "Die Datenquelle muss den Parameter intid definieren."));
			}
			
			if (!liParamsEmpty.isEmpty()) {
				final ParameterPanel panel = new ParameterPanel(liParamsEmpty, null, null, mpParams);
				boolean result = (panel.showOptionDialog(parent, panel, 
						SpringLocaleDelegate.getInstance().getMessage("ReportRunner.8", "Parameter") + (sLabel==null?"":(" "+sLabel))
						, JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null) == JOptionPane.OK_OPTION);
				if (result) {
					try {
						panel.fillParameterMap(liParamsEmpty, mpParams);
					} catch (CommonValidationException e) {
						Errors.getInstance().showExceptionDialog(parent, e);
					}
					String mapToString = CalcAttributeUtils.mapToString(mpParams);
					if (StringUtils.looksEmpty(mapToString)) {
						return null;
					}
					return mapToString;
				}
				return null;
			}
		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(parent, ex);
		}
		return initialCalcAttributeParamValues;
	}
	
}
