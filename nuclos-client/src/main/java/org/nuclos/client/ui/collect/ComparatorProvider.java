package org.nuclos.client.ui.collect;

import java.util.Comparator;

public interface ComparatorProvider<T> {

	Comparator<T> getComparator(Class<?> clazz);
}
