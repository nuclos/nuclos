//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect;

import java.util.Collection;

import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;

public class DefaultLayoutComponentsProvider<PK> implements LayoutComponentsProvider<PK> {
	
	private final MultiListMap<String, LayoutComponent<PK>> mmpComponents;
	
	public DefaultLayoutComponentsProvider() {
		this(new MultiListHashMap<String, LayoutComponent<PK>>());
	}
	public DefaultLayoutComponentsProvider(
			MultiListHashMap<String, LayoutComponent<PK>> multiListHashMap) {
		this.mmpComponents = multiListHashMap;
	}
	@Override
	public Collection<LayoutComponent<PK>> getLayoutComponents() {
		return mmpComponents.getAllValues();
	}

	@Override
	public Collection<LayoutComponent<PK>> getLayoutComponentsFor(String name) {
		return mmpComponents.getValues(name);
	}
	
	public void addComponent(LayoutComponent<PK> component) {
		mmpComponents.addValue(component.getClass().toString(), component);
	}

}
