package org.nuclos.client.ui.collect.component;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.StringUtils;

public class CollectableHtmlField extends AbstractCollectableComponent {

	/**
	 * §precondition clctef != null
	 * §precondition comp != null
	 * §postcondition isSearchComponent() == bSearchable
	 *
	 * @param clctef
	 * @param comp
	 * @param bSearchable
	 */
	protected CollectableHtmlField(final CollectableEntityField clctef, final JComponent comp, final boolean bSearchable) {
		super(clctef, comp, bSearchable);
	}

	public CollectableHtmlField(final CollectableEntityField clctef, final boolean bSearchable) {
		this(clctef, new JLabel(), bSearchable);
	}

	@Override
	protected void updateView(final CollectableField clctfValue) {
		String htmlValue = (String) clctfValue.getValue();

		((JLabel) this.getJComponent()).setVerticalAlignment(SwingConstants.TOP);

		if (htmlValue == null) {
			((JLabel) this.getJComponent()).setText("");
		} else {
			((JLabel) this.getJComponent()).setText(
					"<html>" + htmlValue + "</html>"
			);
		}
	}

	@Override
	public CollectableField getFieldFromView() throws CollectableFieldFormatException {
		return getField(((JLabel) getJComponent()).getText());
	}

	private CollectableField getField(String sText) throws CollectableFieldFormatException {
		final Object oValue = CollectableFieldFormat.getInstance(this.getEntityField().getJavaClass()).parse(getEntityField().getFormatOutput(),
				StringUtils.nullIfEmpty(sText));
		return CollectableUtils.newCollectableFieldForValue(this.getEntityField(), oValue);
	}

	@Override
	public void setInsertable(final boolean bInsertable) {

	}
}
