package org.nuclos.client.ui.collect.component;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.KeyStroke;

import org.nuclos.client.ui.LayoutNavigationCollectable;
import org.nuclos.client.ui.LayoutNavigationProcessor;
import org.nuclos.client.ui.ToolTipTextProvider;
import org.nuclos.client.ui.TriStateCheckBox;
import org.nuclos.client.ui.collect.DefaultLayoutNavigationSupportContext;
import org.nuclos.client.ui.collect.LayoutNavigationSupport;
import org.nuclos.client.ui.collect.LayoutNavigationSupport.ExecutionPoint;
import org.nuclos.client.ui.collect.component.AbstractCollectableComponent.BackgroundColorProvider;
import org.nuclos.common2.StringUtils;

/**
 * TriStateCheckBox with support for dynamic tooltips.
 */
public class InnerCheckBox extends TriStateCheckBox implements LayoutNavigationProcessor {
	private BackgroundColorProvider bgColorProvider;

	private ToolTipTextProvider provider;
	private LayoutNavigationCollectable lnc;
	private boolean forceDisabled;

	public void setToolTipTextProvider(ToolTipTextProvider provider) {
		this.provider = provider;
	}

	@Override
	public String getToolTipText(MouseEvent ev) {
		String sToolTipText = (provider != null) ? provider.getDynamicToolTipText() : super.getToolTipText(ev);
		return StringUtils.isNullOrEmpty(sToolTipText) ? null : sToolTipText;
	}
	
	@Override
	public void setEnabled(boolean b) {
		if (forceDisabled==true) {
			super.setEnabled(false);
		} else {
			super.setEnabled(b);
		}
	}
	
	public void setForceDisabled(boolean forceDisabled) {
		this.forceDisabled = forceDisabled;
		if (forceDisabled==true) {
			setEnabled(false);
		}
	}

	@Override
	protected boolean processKeyBinding(final KeyStroke ks, final KeyEvent e, final int condition, final boolean pressed) {

		boolean processed = false;
		if (null != lnc) {
			final LayoutNavigationSupport lns = lnc.getLayoutNavigationSupport();
			if (lns != null) {
				final DefaultLayoutNavigationSupportContext ctx = new DefaultLayoutNavigationSupportContext(pressed, ks, e, condition, this, lnc);
				processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.BEFORE);
				if (!processed) {
					processed = super.processKeyBinding(ks, e, condition, pressed);
					// if (!processed) {
					ctx.setProcessed(processed);
					processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.AFTER);
					// }
				}
			} else {
				processed = super.processKeyBinding(ks, e, condition, pressed);
			}
		} else {
			processed = super.processKeyBinding(ks, e, condition, pressed);
		}
		return processed;
	}

	@Override
	public void setLayoutNavigationCollectable(LayoutNavigationCollectable lnc) {
		this.lnc = lnc;

	}

	@Override
	public Color getBackground() {
		final Color colorDefault = super.getBackground();
		return (bgColorProvider != null) ? bgColorProvider.getColor(colorDefault) : colorDefault;
	}

	public Color getBackgroundOld() {
		return super.getBackground();
	}

	public void setBackgroundColorProvider(BackgroundColorProvider backgroundColorProvider) {
		bgColorProvider = backgroundColorProvider;

	}
}
