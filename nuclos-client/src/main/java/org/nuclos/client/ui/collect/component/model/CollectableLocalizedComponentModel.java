package org.nuclos.client.ui.collect.component.model;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.dal.vo.IDataLanguageMap;

public interface CollectableLocalizedComponentModel<PK> {

	void initDataLanguageMap(IDataLanguageMap dataLanguageMap, MainFrameTab mainTab);
	
	IDataLanguageMap getDataLanguageMap();

	void setPrimaryKey(PK pk);

	Long getPrimaryKey();

	void setField(final CollectableField clctfValue); 

	UID getUserPrimaryDataLanguage();

	UID getSystemPrimaryDataLanguage();

	MetaProvider getMetaProvider();

	MainFrameTab getTab();

	void setDataLanguageMap(IDataLanguageMap fieldLanguageMap);

	void setField(CollectableValueField collectableValueField, boolean skipReset, boolean setDirty);

	boolean isDataLanguageMapAsDirty();

	void setSubForm(SubForm subForm);
	
	public void stopEditing();
	public void cancelEditing();
}
