package org.nuclos.client.ui.collect.component.multiselection;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxEditor;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.nuclos.client.ui.Icons;
import org.nuclos.common.collect.collectable.CollectableField;

public class ChipComboBoxEditor extends JPanel implements ComboBoxEditor {

	MultiSelectComboboxModel multiSelectComboboxModel;
	MultiselectionCombobox multiselectCombobox;
	ArrayList<ChipComponent> chipComponents = new ArrayList<ChipComponent>();
	JPanel chipsViewport = new JPanel() {
		@Override
		public String getToolTipText() {
			return multiselectCombobox.getToolTipText();
		}
	};
	JScrollPane chipsScrollPane = new JScrollPane(chipsViewport);
	TooManyElementsIndicator tooManyElementsIndicator;
	JButton addEntryButton = new JButton();

	public ChipComboBoxEditor(MultiselectionCombobox multiSelectionCombobox, MultiSelectComboboxModel multiSelectComboboxModel) {
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		setBorder(BorderFactory.createEmptyBorder());
		setBackground(Color.white);

		add(chipsScrollPane);
		add(addEntryButton);

		MouseAdapter mouseAdapter = new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1 && multiselectCombobox.isEnabled()) {
					ChipComboBoxEditor.this.requestFocus();
					multiselectCombobox.showPopup();
				}
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					multiselectCombobox.showPopupMenu(e);
				}
			}
		};
		chipsViewport.addMouseListener(mouseAdapter);
		this.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					togglePopup();
				}
			}
		});

		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(final ComponentEvent e) {
				super.componentResized(e);
				boolean hiddenChip = chipComponents.stream()
						.anyMatch(chipComponent -> !chipComponent.isVisible());
				if (tooManyElementsIndicator != null) {
					tooManyElementsIndicator.setVisible(hiddenChip);
				}
			}
		});

		this.chipsViewport.setLayout(new BoxLayout(chipsViewport, BoxLayout.LINE_AXIS));
		this.chipsScrollPane.getViewport().setBackground(Color.red);
		this.chipsScrollPane.getViewport().setOpaque(true);

		this.addEntryButton.setBorder(BorderFactory.createEmptyBorder(16, 4, 16, 4));
		this.addEntryButton.setIcon(Icons.getInstance().getIconTextFieldButtonLOV());
		this.addEntryButton.setText(null);
		this.addEntryButton.setFocusPainted(false);
		this.addEntryButton.addActionListener(e -> this.multiselectCombobox.insertNewSuggestion());
		this.addEntryButton.setVisible(false);

		this.chipsScrollPane.setBorder(BorderFactory.createEmptyBorder());

		// allow horizontal scrolling without showing the scroll bar
		this.chipsScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.chipsScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);

		this.multiselectCombobox = multiSelectionCombobox;
		this.multiSelectComboboxModel = multiSelectComboboxModel;
	}

	@Override
	public Color getBackground() {
		if (multiselectCombobox != null) {
			return multiselectCombobox.getBackground();
		}
		return super.getBackground();
	}

	public void togglePopup() {
		if (multiselectCombobox.isPopupVisible()) {
			multiselectCombobox.hidePopup();
		} else {
			multiselectCombobox.showPopup();
		}
	}

	/**
	 * Adds a {@link ChipComponent} to the viewport.
	 * @param entry
	 */
	public void addChipComponent(CollectableField entry) {
		String displayText = entry.toString();
		ChipComponent chipComponent = new ChipComponent(this, this.chipsScrollPane);
		chipComponent.setText(displayText);
		chipComponent.addRemoveAction((e) -> this.multiselectCombobox.toggleItemSelection(entry));
		chipComponent.getRemoveButton().setEnabled(this.multiselectCombobox.isEnabled());
		this.chipsViewport.add(chipComponent);
		this.chipComponents.add(chipComponent);
	}

	/**
	 * Updates the editor ui of this component. <br>
	 * Removes all displayed chip elements and recreates them from the {@link MultiSelectComboboxModel MultiSelectComboboxModels} items.
	 */
	public void updateEditor() {
		this.chipComponents.clear();
		this.chipsViewport.removeAll();
		multiSelectComboboxModel.getSelectedItems()
				.forEach(item -> addChipComponent(item));
		addTooManyElementsIndicator();
		this.chipsViewport.add(Box.createHorizontalGlue());
		revalidate();
		repaint();
	}

	public void addTooManyElementsIndicator() {
		tooManyElementsIndicator = new TooManyElementsIndicator();
		this.chipsViewport.add(tooManyElementsIndicator);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Component getEditorComponent() {
		return this;
	}

	@Override
	public void setItem(final Object anObject) { }

	@Override
	public Object getItem() {
		return multiSelectComboboxModel.getSelectedItems();
	}

	@Override
	public void selectAll() { }

	@Override
	public void addActionListener(final ActionListener l) { }

	@Override
	public void removeActionListener(final ActionListener l) { }

}
