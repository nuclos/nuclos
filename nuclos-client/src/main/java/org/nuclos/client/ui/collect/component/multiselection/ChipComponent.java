package org.nuclos.client.ui.collect.component.multiselection;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.plaf.basic.BasicGraphicsUtils;

import org.nuclos.client.common.Utils;
import org.nuclos.client.ui.Icons;
import org.nuclos.common.ParameterProvider;

public class ChipComponent extends JPanel {

	private Color focusColor = Utils.translateColorFromParameter(ParameterProvider.KEY_FOCUSSED_ITEM_BACKGROUND_COLOR);
	private Color chipColor = new Color(17, 144, 254);

	private String text;
	private JLabel lblText = new JLabel();
	private JButton btnRemove;
	private boolean mouseInside = false;
	private boolean focussed = false;

	public ChipComponent(ChipComboBoxEditor editor, JScrollPane scrollPane) {
		initRemoveButton();
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		setOpaque(true);
		lblText.setForeground(Color.WHITE);

		add(Box.createRigidArea(new Dimension(10, 0)));
		add(lblText);
		add(Box.createRigidArea(new Dimension(10, 0)));
		add(btnRemove);
		add(Box.createRigidArea(new Dimension(10, 0)));

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(final ComponentEvent e) {
				super.componentResized(e);
				setVisible(isPanelFullyVisible(scrollPane, ChipComponent.this));
				if (!isVisible()) {
					editor.tooManyElementsIndicator.setVisible(true);
				}
			}
		});
	}

	public static boolean isPanelFullyVisible(JScrollPane scrollPane, JPanel panel) {
		JViewport viewport = scrollPane.getViewport();
		Rectangle viewRect = viewport.getViewRect();
		Rectangle panelBounds = panel.getBounds();
		// add some width, so it will definitely show the indicator
		panelBounds.width += 16;

		return viewRect.contains(panelBounds);
	}

	public void initRemoveButton() {
		btnRemove = new JButton() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				if (isFocusOwner()) {
					Graphics2D g2d = (Graphics2D) g.create();
					g2d.setColor(Color.black);
					g2d.setStroke(new BasicStroke(2));
					BasicGraphicsUtils.drawDashedRect(g2d, 0, 0, getWidth() - 1, getHeight() - 1);
					g2d.dispose();
				}
			}
			@Override
			public Color getBackground() {
				if (hasFocus()) {
					return focusColor;
				}
				return chipColor;
			}
		};
		btnRemove.setText("X");
		btnRemove.setOpaque(true);
		btnRemove.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
		btnRemove.setForeground(Color.white);
		btnRemove.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(final FocusEvent e) {
				focussed = true;
				btnRemove.setIcon(Icons.getInstance().getIconTextFieldButtonHover());
				calculateRemoveButtonForeground();
				scrollViewToChip();
			}

			@Override
			public void focusLost(final FocusEvent e) {
				focussed = false;
				btnRemove.setIcon(Icons.getInstance().getIconTextFieldButton());
				calculateRemoveButtonForeground();
			}
		});
		btnRemove.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(final MouseEvent e) {
				mouseInside = true;
				calculateRemoveButtonForeground();
			}

			@Override
			public void mouseExited(final MouseEvent e) {
				mouseInside = false;
				calculateRemoveButtonForeground();
			}
		});

		btnRemove.setContentAreaFilled(false);
		btnRemove.setHorizontalTextPosition(JButton.CENTER);
		btnRemove.setIcon(Icons.getInstance().getIconTextFieldButton());
		btnRemove.setRolloverIcon(Icons.getInstance().getIconTextFieldButtonHover());
		btnRemove.setPressedIcon(Icons.getInstance().getIconTextFieldButtonPressed());
		btnRemove.setFocusable(false);
	}

	public void scrollViewToChip() {
		scrollRectToVisible(new Rectangle(0, 0, 0, 0));
		scrollRectToVisible(getBounds());
		getRemoveButton().revalidate();
		getRemoveButton().repaint();
	}

	public void calculateRemoveButtonForeground() {
		if (mouseInside || focussed) {
			btnRemove.setForeground(Color.black);
		} else {
			btnRemove.setForeground(Color.white);
		}
		btnRemove.revalidate();
		btnRemove.repaint();
	}

	public void addRemoveAction(ActionListener actionListener) {
		btnRemove.addActionListener(actionListener);
	}

	public JButton getRemoveButton() {
		return btnRemove;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(chipColor);
		g.fillRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
	}

	public void setText(String text) {
		this.text = text;
		lblText.setText(text);
	}

	public String getText() {
		return text;
	}

}
