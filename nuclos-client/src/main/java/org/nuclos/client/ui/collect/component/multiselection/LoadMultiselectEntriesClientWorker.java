package org.nuclos.client.ui.collect.component.multiselection;

import java.util.Collection;

import javax.swing.JComponent;

import org.nuclos.client.command.CommonClientWorker;
import org.nuclos.client.common.MultiselectComboboxController;
import org.nuclos.client.entityobject.CollectableEOEntityClientProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common2.exception.CommonBusinessException;

public class LoadMultiselectEntriesClientWorker implements CommonClientWorker {

	protected volatile boolean interrupted = false;
	public MultiselectionCombobox multiselectionCombobox;
	MultiselectComboboxController multiselectComboboxController;

	public LoadMultiselectEntriesClientWorker(MultiselectComboboxController multiselectComboboxController) {
		this.multiselectComboboxController = multiselectComboboxController;
		multiselectionCombobox = multiselectComboboxController.getMultiselectionCombobox();
	}

	public MultiselectionCombobox getMultiselectionCombobox() {
		return multiselectionCombobox;
	}

	public void interrupt(){
		this.interrupted = true;
	}

	@Override
	public void init() throws CommonBusinessException {

	}

	@Override
	public void work() throws CommonBusinessException {
		if (interrupted || multiselectionCombobox == null) {
			return;
		}
		synchronized (multiselectionCombobox) {
			UID currentLayoutUid = multiselectComboboxController.getCurrentLayoutUid();

			multiselectionCombobox.getMultiSelectComboboxModel().clearSelection();
			multiselectComboboxController.clearCollectables();
			multiselectionCombobox
					.getMultiSelectComboboxModel()
					.clearSelection();

			if (multiselectComboboxController.getParentId() != null) {
				Collection collmdvo = MasterDataDelegate.getInstance().getDependentDataCollectionWithLimit(
						multiselectionCombobox.getForeignKeyFieldToParent(), null, null,
						currentLayoutUid, null, multiselectComboboxController.getParentId());
				CollectableEOEntity eo = (CollectableEOEntity) CollectableEOEntityClientProvider.getInstance().getCollectableEntity(multiselectionCombobox.getUid());
				Collection<CollectableEntityObject<Object>> dependants = CollectionUtils.transform(collmdvo, new CollectableEntityObject.MakeCollectable<>(eo));
				multiselectComboboxController.setCollectables(dependants);
				multiselectionCombobox.getMultiSelectComboboxModel().setSelectionFromCollectables(dependants);
				multiselectionCombobox.updateEditor();
			}
		}
	}

	@Override
	public void paint() throws CommonBusinessException {

	}

	@Override
	public void handleError(final Exception ex) {
		if (!interrupted) {
			Errors.getInstance().showExceptionDialog(getResultsComponent(), ex);
		}
	}

	@Override
	public JComponent getResultsComponent() {
		return getMultiselectionCombobox();
	}

}
