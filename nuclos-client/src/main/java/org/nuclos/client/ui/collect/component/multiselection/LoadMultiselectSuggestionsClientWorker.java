package org.nuclos.client.ui.collect.component.multiselection;

import java.util.List;

import javax.swing.JComponent;

import org.nuclos.client.command.CommonClientWorker;
import org.nuclos.client.common.MultiselectComboboxController;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common2.exception.CommonBusinessException;

public class LoadMultiselectSuggestionsClientWorker implements CommonClientWorker {

	protected volatile boolean interrupted = false;
	public MultiselectionCombobox multiselectionCombobox;
	MultiselectComboboxController multiselectComboboxController;

	private final CollectableFieldsProviderFactory clctfproviderfactory;

	public LoadMultiselectSuggestionsClientWorker(MultiselectComboboxController multiselectComboboxController) {
		this.multiselectComboboxController = multiselectComboboxController;
		multiselectionCombobox = multiselectComboboxController.getMultiselectionCombobox();
		clctfproviderfactory = multiselectComboboxController.getCollectableFieldsProviderFactory();
	}

	private CollectableFieldsProviderFactory getCollectableFieldsProviderFactory() {
		return clctfproviderfactory;
	}

	public MultiselectionCombobox getMultiselectionCombobox() {
		return multiselectionCombobox;
	}

	public void interrupt(){
		this.interrupted = true;
	}


	@Override
	public void init() throws CommonBusinessException {

	}

	@Override
	public void work() throws CommonBusinessException {
		if (interrupted || multiselectionCombobox == null) {
			return;
		}
		synchronized (multiselectionCombobox) {
			if (multiselectionCombobox.getValueListProvider() == null) {
				CollectableFieldsProvider valuelistprovider = getCollectableFieldsProviderFactory().newDefaultCollectableFieldsProvider(multiselectionCombobox.getDisplayAttributeUid());
				multiselectionCombobox.setValueListProvider(valuelistprovider);
			}
			List<CollectableField> fields = multiselectionCombobox.getValueListProvider().getCollectableFields();
			multiselectionCombobox.setSuggestions(fields);
			multiselectionCombobox.updateEditor();
		}
	}

	@Override
	public void paint() throws CommonBusinessException {

	}

	@Override
	public void handleError(final Exception ex) {
		if (!interrupted) {
			Errors.getInstance().showExceptionDialog(getResultsComponent(), ex);
		}
	}

	@Override
	public JComponent getResultsComponent() {
		return getMultiselectionCombobox();
	}

}
