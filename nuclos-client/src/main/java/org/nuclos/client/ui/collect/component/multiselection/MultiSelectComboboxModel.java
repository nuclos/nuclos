package org.nuclos.client.ui.collect.component.multiselection;

import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collection.CollectionUtils;

public class MultiSelectComboboxModel extends AbstractListModel<CollectableField> implements ComboBoxModel<CollectableField> {

	private UID	displayAttributeUid;
	private List<CollectableField> items;
	private List<CollectableField> selectedItems;
	private ArrayList<MultiSelectComboboxModelListener> listeners = new ArrayList<>();

	public MultiSelectComboboxModel(List<CollectableField> allItems, UID displayAttributeUid) {
		this.items = allItems;
		this.selectedItems = new ArrayList<>();
		this.displayAttributeUid = displayAttributeUid;
	}

	public MultiSelectComboboxModel(List<CollectableField> allItems, List<CollectableField> selectedItems, UID displayAttributeUid) {
		this.items = allItems;
		this.selectedItems = selectedItems;
		this.displayAttributeUid = displayAttributeUid;
	}

	public void addMultiSelectComboboxModelListener(MultiSelectComboboxModelListener listener) {
		listeners.add(listener);
	}

	public void removeMultiSelectComboboxModelListener(MultiSelectComboboxModelListener listener) {
		listeners.remove(listener);
	}

	public void setSelectionFromCollectables(Collection<CollectableEntityObject<Object>> items) {
		items.forEach(item -> setSelectionFromCollectable(item));
	}

	public void setSelectionFromCollectable(CollectableEntityObject<Object> item) {
		Optional<CollectableField> eovo = items.stream()
				.filter(i -> Objects.equals(i.getValueId(), item.getEntityObjectVO().getFieldId(displayAttributeUid)))
				.findAny();
		if (eovo.isPresent()) {
			selectedItems.add(eovo.get());
			sortSelection();
		}
	}

	public void clearSelection() {
		selectedItems.clear();
	}

	private void sortSelection() {
		selectedItems = CollectionUtils.sorted(selectedItems, (o1, o2) -> ObjectUtils.compare(o1.toString(), o2.toString()));
	}

	@Override
	public int getSize() {
		return items.size();
	}

	@Override
	public CollectableField getElementAt(int index) {
		return items.get(index);
	}

	@Override
	public void setSelectedItem(Object anItem) {

	}

	@Override
	public Object getSelectedItem() {
		return selectedItems.isEmpty() ? null : selectedItems;
	}

	public List<CollectableField> getAvailableItems() {
		return items;
	}

	public List<CollectableField> getSelectedItems() {
		return selectedItems;
	}

	public void toggleItem(CollectableField item) {
		if (selectedItems.contains(item)) {
			removeItem(item);
		} else {
			addItem(item);
		}
		fireContentsChanged(this, -1, -1);
	}

	public void addItem(CollectableField item) {
		selectedItems.add(item);
		sortSelection();
		this.listeners.forEach(listener -> listener.onItemSelected(item));
	}

	public void removeItem(CollectableField item) {
		selectedItems.remove(item);
		this.listeners.forEach(listener -> listener.onItemDeselected(item));
	}

}
