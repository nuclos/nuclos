package org.nuclos.client.ui.collect.component.multiselection;

import org.nuclos.common.collect.collectable.CollectableField;

public interface MultiSelectComboboxModelListener {

	void onItemSelected(CollectableField item);
	void onItemDeselected(CollectableField item);

}
