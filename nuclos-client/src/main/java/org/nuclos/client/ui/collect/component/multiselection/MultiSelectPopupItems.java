package org.nuclos.client.ui.collect.component.multiselection;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.MultiselectComboboxController;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common2.SpringLocaleDelegate;

public class MultiSelectPopupItems {

	private static final SpringLocaleDelegate LOCALE_DELEGATE = SpringLocaleDelegate.getInstance();

	protected static final String TEXT_CLEAR = LOCALE_DELEGATE.getMessage("AbstractCollectableComponent.11","Feld leeren");
	protected static final String TEXT_NEW = LOCALE_DELEGATE.getMessage("AbstractCollectableComponent.context.new","Neu...");
	protected static final String TEXT_REFRESH = LOCALE_DELEGATE.getMessage("AbstractCollectableComponent.context.refresh","Aktualisieren...");

	private MultiselectComboboxController controller;
	private MultiselectionCombobox component;

	public MultiSelectPopupItems(MultiselectComboboxController controller) {
		this.controller = controller;
		this.component = controller.getMultiselectionCombobox();
	}

	/**
	 * Creates a JPopupMenu and adds all required actions for the multi select to it.
	 */
	public JPopupMenu newJPopupMenu() {
		JPopupMenu result = new JPopupMenu();

		result.add(newInsertEntry());
		result.add(newRefreshEntry());
		result.add(newClearEntry());

		return result;
	}

	/**
	 * §precondition getEntityField().isReferencing()
	 * @return a new "refresh" entry for the context menu in edit mode
	 */
	protected final JMenuItem newRefreshEntry() {
		final JMenuItem result = new JMenuItem(TEXT_REFRESH);
		result.addActionListener(ev -> controller.loadSuggestions());
		result.setEnabled(component.isEnabled());
		return result;
	}

	/**
	 * @return a new "clear" entry for the context menu in edit mode
	 */
	protected final JMenuItem newClearEntry() {
		final JMenuItem result = new JMenuItem(TEXT_CLEAR);
		result.addActionListener(ev -> component.clear());
		result.setEnabled(component.isEnabled());
		return result;
	}

	private boolean isInsertEnabled() {
		final UID referencedEntity = getEntityField().getReferencedEntityUID();
		boolean bInsertEnabled = isReferencedEntityDisplayable();
		if (bInsertEnabled) {
			if (Modules.getInstance().isModule(referencedEntity)) {
				bInsertEnabled = SecurityCache.getInstance().isNewAllowedForModule(referencedEntity);
			} else {
				bInsertEnabled = SecurityCache.getInstance().isWriteAllowedForMasterData(referencedEntity);
			}
			boolean blnEntityIsEditable = MetaProvider.getInstance().getEntity(referencedEntity).isEditable();
			bInsertEnabled = bInsertEnabled && blnEntityIsEditable;
		}
		return bInsertEnabled;
	}

	/**
	 * §precondition getEntityField().isReferencing()
	 *
	 * @return a new "new" entry for the context menu in edit mode
	 */
	protected final JMenuItem newInsertEntry() {
		if (!getEntityField().isReferencing()) {
			throw new IllegalStateException();
		}
		boolean insertEnabled = isInsertEnabled();
		final JMenuItem result = new JMenuItem(TEXT_NEW);
		result.setEnabled(insertEnabled && component.isEnabled());

		result.addActionListener(ev -> component.insertNewSuggestion());
		return result;
	}

	public CollectableEntityField getEntityField() {
		return component.getSuggestionReferenceField();
	}

	/**
	 * §precondition getEntityField().isReferencing()
	 *
	 * @return true, if the referenced entity is displayable
	 */
	private boolean isReferencedEntityDisplayable() {
		final CollectableEntityField clctef = getEntityField();
		return clctef.isReferencedEntityDisplayable() && MasterDataLayoutHelper.isLayoutMLAvailable(clctef.getReferencedEntityUID(), false);
	}

}
