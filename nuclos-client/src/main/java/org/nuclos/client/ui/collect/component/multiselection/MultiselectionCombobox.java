package org.nuclos.client.ui.collect.component.multiselection;

import java.awt.Color;
import java.awt.Component;
import java.awt.IllegalComponentStateException;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.MultiselectComboboxController;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.Utils;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.subform.RefreshValueListAction;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.exception.CommonBusinessException;

public class MultiselectionCombobox extends JComboBox<CollectableField> {

	private Color focusColor = Utils.translateColorFromParameter(ParameterProvider.KEY_FOCUSSED_ITEM_BACKGROUND_COLOR);

	private List<ChangeListener> lstchangelistener = new LinkedList<>();

	private final UID entityUid;
	private final UID foreignKeyFieldToParent;
	private final UID displayAttributeUid;
	private final UID suggestionEntityUid;

	private CollectableFieldsProvider valuelistprovider;
	private Collection<RefreshValueListAction> collRefreshValueListActions;

	private CollectableEntity collectableEntity;
	private CollectableEntityField referencingField;
	private CollectableEntityField displayAttributeField;
	private MultiselectComboboxController multiselectionComboboxController;

	private ChipComboBoxEditor chipComboBoxEditor;
	private MultiSelectComboboxModel multiSelectComboboxModel;
	private MultiSelectPopupItems popupMenuGenerator;

	public MultiselectionCombobox(UID entityUid, UID foreignKeyFieldToParent, UID displayAttributeUid) {
		this.entityUid = entityUid;
		this.foreignKeyFieldToParent = foreignKeyFieldToParent;
		this.displayAttributeUid = displayAttributeUid;
		collectableEntity = DefaultCollectableEntityProvider.getInstance().getCollectableEntity(entityUid);
		collectableEntity.getEntityField(foreignKeyFieldToParent);
		referencingField = collectableEntity.getEntityField(foreignKeyFieldToParent);
		displayAttributeField = collectableEntity.getEntityField(displayAttributeUid);
		suggestionEntityUid = displayAttributeField.getReferencedEntityUID();
		chipComboBoxEditor = new ChipComboBoxEditor(this, new MultiSelectComboboxModel(new ArrayList<>(), displayAttributeUid));
		initializeNewModel(new ArrayList<>());
		setEditable(true);
		setEditor(chipComboBoxEditor);
		setRenderer(new CheckBoxListRenderer());
		addActionListener(e -> {
			Object toggleItem = getSelectedItem();
			toggleItemSelection(toggleItem);
		});
	}

	public void setParameterValueListProvider(UID uidField, CollectableField cf) throws CommonBusinessException {
		for(RefreshValueListAction action : getRefreshValueListActions()) {
			if(action.getParentComponentUID().equals(uidField)) {
				String paraName = action.getParameterNameForSourceComponent();
				UID uidTargetField = action.getTargetComponentUID();
				if(getValueListProvider() != null) {
					CollectableFieldsProvider cfp = getValueListProvider();
					cfp.setParameter(paraName, cf.getValueId());
				}
			}
		}
	}

	public Collection<RefreshValueListAction> getRefreshValueListActions() {
		return collRefreshValueListActions != null ? Collections.unmodifiableCollection(collRefreshValueListActions) : Collections.emptyList();
	}

	public void addRefreshValueListAction(RefreshValueListAction act) {
		if (collRefreshValueListActions == null) {
			collRefreshValueListActions = new LinkedList<>();
		}
		collRefreshValueListActions.add(act);
	}

	public void showPopupMenu(MouseEvent e) {
		if (popupMenuGenerator != null) {
			popupMenuGenerator.newJPopupMenu()
					.show(e.getComponent(), e.getX(), e.getY());
		}
	}

	@Override
	public Color getBackground() {
		if (getEditor() != null && getEditor().getEditorComponent().hasFocus()/* || addEntryButton.hasFocus()*/) {
			return focusColor;
		}
		return super.getBackground();
	}

	public void setValueListProvider(CollectableFieldsProvider valuelistprovider) {
		this.valuelistprovider = valuelistprovider;
	}

	public CollectableFieldsProvider getValueListProvider() {
		return this.valuelistprovider;
	}

	private void fireChangeListener() {
		for(ChangeListener cl : lstchangelistener) {
			final ChangeEvent ev = new ChangeEvent(this);
			cl.stateChanged(ev);
		}
	}

	public void updateEditor() {
		chipComboBoxEditor.updateEditor();
	}

	/**
	 * @param listener
	 */
	public synchronized void addChangeListener(ChangeListener listener) {
		this.lstchangelistener.add(listener);
	}

	@Override
	public void setPopupVisible(boolean visible) {
		if (visible) {
			super.setPopupVisible(true);
		}
	}

	public void clear() {
		this.multiSelectComboboxModel.getSelectedItems()
				.forEach(item -> this.multiSelectComboboxModel.removeItem(item));
		updateEditor();
	}

	public void toggleItemSelection(Object selected) {
		if (!isEnabled()) {
			return;
		}
		if (selected instanceof CollectableField) {
			CollectableField selectedVO = (CollectableField) selected;
			multiSelectComboboxModel.toggleItem(selectedVO);
			chipComboBoxEditor.updateEditor();
			fireChangeListener();
		}
		if (selected instanceof Collectable) {
			Optional<CollectableField> targetItem = multiSelectComboboxModel.getAvailableItems().stream()
					.filter(field -> Objects.equals(field.getValueId(), ((Collectable) selected).getId()))
					.findAny();
			if (targetItem.isPresent()) {
				multiSelectComboboxModel.toggleItem(targetItem.get());
				chipComboBoxEditor.updateEditor();
				fireChangeListener();
			}
		}
	}

	public MultiSelectComboboxModel getMultiSelectComboboxModel() {
		return multiSelectComboboxModel;
	}

	public void setSuggestions(Collection<CollectableField> suggestions) {
		initializeNewModel(suggestions);
		this.chipComboBoxEditor = new ChipComboBoxEditor(this, this.multiSelectComboboxModel);
		setModel(new DefaultComboBoxModel<>(suggestions.toArray(new CollectableField[0])));
		setEditor(this.chipComboBoxEditor);
	}

	public UID getUid() {
		return entityUid;
	}

	public UID getForeignKeyFieldToParent() {
		return foreignKeyFieldToParent;
	}

	public UID getReferencedForeignKeyFieldToParent() {
		return UID.parseUID(referencingField.getReferencedEntityFieldName());
	}

	public UID getSuggestionEntityUid() {
		return suggestionEntityUid;
	}

	public UID getDisplayAttributeUid() {
		return displayAttributeUid;
	}

	public UID getReferencedDisplayAttributeUid() {
		return displayAttributeField.getReferencedEntityUID();
	}

	public void setController(MultiselectComboboxController multiselectionComboboxController) {
		this.multiselectionComboboxController = multiselectionComboboxController;
		popupMenuGenerator = new MultiSelectPopupItems(this.multiselectionComboboxController);
	}

	public CollectableEntityField getSuggestionReferenceField() {
		return this.displayAttributeField;
	}

	private void initializeNewModel(Collection<CollectableField> suggestions) {
		List<CollectableField> selectedItems = multiSelectComboboxModel != null ? multiSelectComboboxModel.getSelectedItems() : new ArrayList<>();
		destroyPreviousModel();
		this.multiSelectComboboxModel = new MultiSelectComboboxModel(new ArrayList<>(suggestions), selectedItems, displayAttributeUid);
		this.multiSelectComboboxModel.addMultiSelectComboboxModelListener(multiselectionComboboxController);
		this.updateEditor();
	}

	private void destroyPreviousModel() {
		if (multiSelectComboboxModel != null) {
			this.multiSelectComboboxModel.removeMultiSelectComboboxModelListener(multiselectionComboboxController);
		}
	}

	private class CheckBoxListRenderer extends JCheckBox implements ListCellRenderer<CollectableField> {
		@Override
		public Component getListCellRendererComponent(JList<? extends CollectableField> list, CollectableField entityObjectVO, int index,
													  boolean isSelected, boolean cellHasFocus) {

			String text = entityObjectVO.toString();
			setText(text);
			setOpaque(true);
			setFont(list.getFont());
			setEnabled(list.isEnabled());
			setSelected(multiSelectComboboxModel.getSelectedItems().contains(entityObjectVO));
			setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
			setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());
			return this;
		}
	}

	public void insertNewSuggestion() {
		Component c = UIUtils.getTabOrWindowForComponent(this);
		final MainFrameTab tab;
		if (c instanceof MainFrameTab) {
			tab = (MainFrameTab) c;
		} else {
			MainFrameTab selectedTab = null;
			try {
				selectedTab = MainFrame.getSelectedTab(this.getLocationOnScreen());
			} catch (IllegalComponentStateException e) {
				//
			} finally {
				tab = selectedTab;
			}
		}
		UIUtils.runCommandLater(getParent(), (CommonRunnable) () -> {
			final UID referencedEntity = getSuggestionEntityUid();
			final MainFrameTab overlay = new MainFrameTab();
			final CollectController<Long,?> ctl = (CollectController<Long, ?>) NuclosCollectControllerFactory.getInstance().newCollectController(referencedEntity, overlay, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			Main.getInstance().getMainController().initMainFrameTab(ctl, overlay);
			tab.add(overlay);
			ctl.addCollectableEventListener(new CollectControllerEventHandler(multiselectionComboboxController, ctl, multiselectionComboboxController.getEntityCollectController()));
			ctl.runNew();
		});
	}

	private final static class CollectControllerEventHandler implements CollectController.CollectableEventListener {

		private final CollectController collectController;

		private final EntityCollectController mainController;

		private MultiselectComboboxController comboboxController;

		public CollectControllerEventHandler(MultiselectComboboxController comboboxController, CollectController clctCntrl, EntityCollectController controller) {
			this.comboboxController = comboboxController;
			this.collectController = clctCntrl;
			this.mainController = controller;
		}

		@Override
		public void handleCollectableEvent(Collectable collectable, CollectController.MessageType messageType) {
			switch (messageType) {
				case EDIT_DONE:
				case NEW_DONE :
				case DELETE_DONE:
					try {
						collectController.getTab().close();
						CollectableFieldsProvider collectableFieldsProvider = comboboxController.getMultiselectionCombobox().getValueListProvider();
						if (collectableFieldsProvider instanceof CollectableFieldsProviderCache.CachingCollectableFieldsProvider) {
							((CollectableFieldsProviderCache.CachingCollectableFieldsProvider) collectableFieldsProvider).clear();
						}
						this.comboboxController.loadSuggestionsImmediately();
						this.comboboxController.getMultiselectionCombobox().toggleItemSelection(collectable);
					} catch (CommonBusinessException e) {
						throw new RuntimeException(e);
					}
					break;
				case STATECHANGE_DONE:
			}
		}
	}

}

