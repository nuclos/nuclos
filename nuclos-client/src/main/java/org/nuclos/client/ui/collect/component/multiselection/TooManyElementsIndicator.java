package org.nuclos.client.ui.collect.component.multiselection;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class TooManyElementsIndicator extends JPanel {

	private JLabel lblText = new JLabel("...");
	private Color chipColor = new Color(17, 144, 254);

	public TooManyElementsIndicator() {
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		lblText.setForeground(Color.WHITE);
		lblText.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 4));
		add(lblText);
		setVisible(false);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(chipColor);
		g.fillRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
	}

}
