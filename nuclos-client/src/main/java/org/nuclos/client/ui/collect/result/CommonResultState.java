package org.nuclos.client.ui.collect.result;

import java.util.ArrayList;
import java.util.Collection;

class CommonResultState {

	private boolean toggleSelection = false;
	
	private boolean alternateSelectionToggle = true;
	
	private final Collection<ResultKeyListener> keyListener = new ArrayList<ResultKeyListener>();

	public CommonResultState() {			
	}

	public boolean isToggleSelection() {
		return toggleSelection;
	}

	public void setToggleSelection(boolean toggleSelection) {
		this.toggleSelection = toggleSelection;
	}

	public boolean isAlternateSelectionToggle() {
		return alternateSelectionToggle;
	}

	public void setAlternateSelectionToggle(boolean alternateSelectionToggle) {
		this.alternateSelectionToggle = alternateSelectionToggle;
	}

	public Collection<ResultKeyListener> getKeyListener() {
		return keyListener;
	}
	
}
