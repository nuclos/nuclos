//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.result;

import java.util.List;
import java.util.Map;

import javax.swing.JTable;

import org.apache.log4j.Logger;
import org.nuclos.client.common.TablePreferencesUtils;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.preferences.TablePreferencesManager;

/**
 * A specialization of ResultController for use with an {@link GenericObjectCollectController}.
 * <p>
 * At present the feature to include rows from a subform in the base entity result list 
 * is only available for GenericObjects. The support for finding the support fields is 
 * implemented in {@link #getFieldsAvailableForResult}. 
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.1.01
 */
public class GenericObjectResultController<Clct extends CollectableGenericObjectWithDependants> extends NuclosResultController<Long,Clct> {
	
	private static final Logger LOG = Logger.getLogger(GenericObjectResultController.class);
	
	public GenericObjectResultController(CollectableEntity clcte, ISearchResultStrategy<Long,Clct> srs, NuclosCollectControllerCommonState state) {
		super(clcte, srs, state);
	}
	
	/**
	 * reads the selected fields and their entities from the user preferences.
	 * 
	 * @return the list of previously selected fields
	 */
	@Override
	protected List<? extends CollectableEntityField> readSelectedFieldsFromPreferences() {
		final List<? extends CollectableEntityField> result = TablePreferencesUtils.getCollectableEntityFieldsForGenericObject(
				getTablePreferencesManager());
		CollectionUtils.removeDublicates(result);
		
		return result;
	}
	
	@Override
	protected void writeSelectedFieldsAndWidthsToPreferences(
			final TablePreferencesManager tblprefManager, List<? extends CollectableEntityField> lstclctefSelected, Map<UID, Integer> mpWidths) {
		TablePreferencesUtils.setCollectableEntityFieldsForGenericObject(tblprefManager, lstclctefSelected, getFieldWidthsForPreferences(),
				CollectableUtils.getFieldUidsFromCollectableEntityFields(getNuclosResultPanel().getFixedColumns()));
	}

	/**
	 * sets all column widths to user preferences; set optimal width if no preferences yet saved
	 * Unfinalized in the CollectController
	 * @param tbl
	 * @deprecated Remove this.
	 */
	@Override
	public void setColumnWidths(final JTable tbl) {
		final GenericObjectCollectController controller = getGenericObjectCollectController();
		if(controller.getSearchResultTemplateController() == null || controller.getSearchResultTemplateController().isSelectedDefaultSearchResultTemplate()) {
			super.setColumnWidths(tbl);
		}
	}

	private GenericObjectCollectController getGenericObjectCollectController() {
		return (GenericObjectCollectController) getCollectController();
	}

}
