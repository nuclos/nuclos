package org.nuclos.client.ui.collect.result;

import static java.awt.event.ActionEvent.ACTION_PERFORMED;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.text.JTextComponent;

import org.jdesktop.swingx.autocomplete.AutoCompleteDocument;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.controller.SearchConditionSource;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.genericobject.valuelistprovider.MandatorCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.ProcessCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.StatusCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.StatusNumeralCollectableFieldsProvider;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.valuelistprovider.DatasourceBasedCollectableFieldsProvider;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.ListOfValues;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectableResultComponent;
import org.nuclos.client.ui.collect.component.AbstractCollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.SF;
import org.nuclos.common.SFE;
import org.nuclos.common.UID;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.meta.entity.EntityFieldClientRuleStatement;
import org.nuclos.schema.meta.entity.EntityFieldClientRuleStatementClearValue;
import org.nuclos.schema.meta.entity.EntityFieldClientRuleStatementTransferLookedupValue;
import org.nuclos.schema.meta.entity.EntityFieldClientRules;
import org.nuclos.schema.meta.entity.EntityFieldVlpConfig;
import org.nuclos.schema.meta.entity.EntityFieldVlpContext;
import org.nuclos.schema.meta.entity.EntityFieldVlpParam;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by guenthse on 3/6/2017.
 */
public class ResultPanelFilter extends ResultFilter {

    private Action actFilter;
    private SearchConditionSource searchConditionSource;

    ResultPanelFilter(ResultPanel pnlResult, Action actFilter, SearchConditionSource searchConditionSource) {
        super(pnlResult);
        this.actFilter = actFilter;
        this.searchConditionSource = searchConditionSource;
    }

    @Override
    protected JTable getFixedTable() {
        if (!(resultComponent instanceof NuclosResultPanel)) {
            return null;
        }

        NuclosResultPanel pnlResult = (NuclosResultPanel) resultComponent;
        return pnlResult.getFixedResultTable();
    }

    @Override
    protected FilterableTable getExternalTable() {
        if (!(resultComponent instanceof ResultPanel)) {
            return null;
        }

        ResultPanel pnlResult = (ResultPanel) resultComponent;

        return pnlResult.getResultTable();
    }

    /**
     * actionlistener to collapse or expand the searchfilter panels
     */
    protected void addActionListener() {
        //do nothing
//        if (!(resultComponent instanceof ResultPanel)) {
//            return;
//        }
//
//        ResultPanel pnlResult = (ResultPanel) resultComponent;
//
//        // action: Filter
//
//        pnlResult.registerToolBarAction(NuclosToolBarItems.FILTER, new CommonAbstractAction() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                if (!getFixedResultFilter().isCollapsed() || !getExternalResultFilter().isCollapsed()) {
//                    clearFilter();
//
//                    filterButton.setSelected(false);
//                } else {
//                    filter();
//
//                    filterButton.setSelected(true);
//                }
//
//                getFixedResultFilter().setCollapsed(!getFixedResultFilter().isCollapsed());
//                getExternalResultFilter().setCollapsed(!getExternalResultFilter().isCollapsed());
//            }
//        });
    }

    @Override
    protected void filter() {
        if (getFixedResultFilter() == null || getExternalResultFilter() == null ||  !(resultComponent instanceof ResultPanel))
            return;


        Map<UID, CollectableComponent> filterComponents = getAllFilterComponents();
        if (filterComponents.isEmpty()) {
            return;
        }

        getFixedResultFilter().setVisible(true);
        getExternalResultFilter().setVisible(true);

        if (actFilter != null) {
            actFilter.actionPerformed(new ActionEvent(this, ACTION_PERFORMED, "filter"));
        }

    }

    @Override
    public void clearFilter() {
        Icon icon = Icons.getInstance().getIconFilter16();
        this.filterButton.setIcon(icon);

		Map<UID, CollectableComponent> columnFilters = getAllFilterComponents();
        for (CollectableComponent clctcomp : columnFilters.values()) {
            if (clctcomp instanceof AbstractCollectableComponent) {
                ((AbstractCollectableComponent) clctcomp).setSearchCondition(null);
            }
        }

        filteringActive = false;
    }

    @Override
    protected void loadTableFilter() {

    }

	@Override
    public void close() {
		super.close();
    	actFilter = null;
    	searchConditionSource = null;
	}

    @Override
    public JToggleButton getToggleButton() {
        if (!(resultComponent instanceof ResultPanel)) {
            return null;
        }

        ResultPanel pnlResult = (ResultPanel) resultComponent;
        JComponent[] components = pnlResult.getToolBar().getComponents(NuclosToolBarItems.FILTER);
        if (components == null) {
        	return filterButton;
		}
        for (JComponent component : components) {
            if (component instanceof JToggleButton) {
                return (JToggleButton) component;
            }
        }
        return null;
    }

    @Override
    protected JCheckBoxMenuItem getMenuItem() {
        return null;
    }

    private void setAdditionalSearchCondition(CollectableFieldsProvider valuelistprovider, UID refField) {
		// NUCLOS-6975 Try to restrict the content of the filters.
		if (searchConditionSource != null) {
			try {
				CollectableSearchCondition cond = searchConditionSource.getCollectableSearchConditionFromSearchPanel(false);
				if (cond != null) {
					Pair<UID, CollectableSearchCondition> par = new Pair<>(refField, cond);
					valuelistprovider.setParameter(NuclosConstants.VLP_ADDITIONAL_CONDITION_FOR_RESULTFILTER, par);
				}

			} catch (CollectableFieldFormatException cffe) {
				LOG.warn(cffe.getMessage(), cffe);
			}
		}
	}

	/**
	 * Checks, whether a {@link EntityFieldVlpConfig} has been set for the given {@link FieldMeta} and returns it after deserializing its json string.
	 *
	 * @param fieldVO fieldMeta potentially holding an EntityFieldVlpConfig
	 * @return {@link EntityFieldVlpConfig} deserialized from the given {@link FieldMeta} or null, if no vlp config was set via the entity wizard.
	 */
	private EntityFieldVlpConfig getEntityFieldVlpConfig(final FieldMeta<?> fieldVO) {
		UID valuelistProvider = fieldVO.getValuelistProvider();
		if (valuelistProvider != null) {
			String sJson = fieldVO.getValuelistProviderConfig();
			EntityFieldVlpConfig vlpConfig = new EntityFieldVlpConfig();
			if (sJson != null) {
				ObjectMapper mapper = new ObjectMapper();
				try {
					vlpConfig = mapper.readValue(sJson, EntityFieldVlpConfig.class);
				} catch (Exception e) {
					Errors.getInstance().showDetailedExceptionDialog(MainFrame.getInstance().getRootPane(), e);
				}
			}
			vlpConfig.setVlpId(valuelistProvider.getString());
			return vlpConfig;
		}
		return null;
	}

	/**
	 * Attempts to retrieve the best {@link EntityFieldVlpContext} from the given {@link EntityFieldVlpConfig} for the current BusinessObject.
	 * It first searches for a context matching the {@code entityUid} parameter. If no match is found, it will look for an EntityFieldVlpContext
	 * for no specific entityUid. If there is no config for a {@code null} entityClassId, this method will return {@code null}.
	 *
	 * @param entityUid the uid of the business object, to prefer for the context
	 * @param entityFieldVlpConfig config containing contexts to find the best match within
	 * @return the best context to use, when evaluating the vlp for the given entity. {@code null}, if there is no match.
	 */
	private EntityFieldVlpContext getMatchingContext(UID entityUid, EntityFieldVlpConfig entityFieldVlpConfig) {
		if (entityFieldVlpConfig != null) {
			Optional<EntityFieldVlpContext> contextOptional = entityFieldVlpConfig.getVlpContexts().stream()
					.filter(context ->
								Objects.equals(context.getEntityClassId(), entityUid.getStringifiedDefinitionWithEntity(E.ENTITY))
								&& context.isList()
					).findFirst();
			if (contextOptional.isPresent()) {
				return contextOptional.get();
			} else {
				return entityFieldVlpConfig.getVlpContexts().stream()
						.filter(context -> (context.getEntityClassId() == null || StringUtils.isNullOrEmpty(context.getEntityClassId())) && context.isList())
						.findFirst().orElse(null);
			}
		}
		return null;
	}

	/**
	 * Returns the string value a vlp parameter should use for the specified entityFieldId.
	 * The values to use for vlp parameters, are the values, the user filled in the search view.
	 * Because the result panel filter has no direct access to the search view, this method reads the values back from the searchCondition built
	 * by the search view.
	 *
	 * @param valueFromEntityFieldId string uid of an entity field
	 * @return string value a vlp parameter should use for the specified entityFieldId.
	 */
	private String getValueFromCurrentFilter(String valueFromEntityFieldId) {
		if (searchConditionSource instanceof CollectController) {
			CollectController collectController = (CollectController) searchConditionSource;
			final CollectableSearchCondition searchCondition = collectController.isSearchPanelAvailable() ? collectController.getSearchPanel().getEditModel().getSearchCondition() : null;
			if (searchCondition instanceof CompositeCollectableSearchCondition) {
				CompositeCollectableSearchCondition compCondition = (CompositeCollectableSearchCondition) searchCondition;
				ArrayList<CollectableSearchCondition> matchingSubConditions = getAllConditionsForField(compCondition, valueFromEntityFieldId);
				if (matchingSubConditions.size() == 1) {
					return ((AtomicCollectableSearchCondition) matchingSubConditions.get(0)).getComparandAsString();
				}
			} else if (searchCondition instanceof AtomicCollectableSearchCondition){
				if (searchCondition instanceof CollectableComparison && ((CollectableComparison) searchCondition).getComparand().getValueId() != null) {
					return ((CollectableComparison) searchCondition).getComparand().getValueId().toString();
				}
				return ((AtomicCollectableSearchCondition) searchCondition).getComparandAsString();
			}
		}
		return null;
	}

	/**
	 * Returns an {@link ArrayList} of all {@link CollectableSearchCondition}, which the specified {@link CompositeCollectableSearchCondition} contains for the {@code entityFieldId}.
	 *
	 * @param compCondition a {@link CompositeCollectableSearchCondition} containing operands to search through
	 * @param entityFieldId uid of a field for which the list of {@link CollectableSearchCondition CollectableSearchConditions} should be gathered
	 * @return {@link ArrayList} containing all {@link CollectableSearchCondition CollectableSearchConditions} from the {@link CompositeCollectableSearchCondition} operands, which apply for the given entityFieldId
	 */
	public ArrayList<CollectableSearchCondition> getAllConditionsForField(CompositeCollectableSearchCondition compCondition, String entityFieldId) {
		ArrayList<CollectableSearchCondition> matchList = new ArrayList<>();
		for (CollectableSearchCondition subCondition : compCondition.getOperands()) {
			if (subCondition instanceof AtomicCollectableSearchCondition) {
				AtomicCollectableSearchCondition atomicCondition = (AtomicCollectableSearchCondition) subCondition;
				if (Objects.equals(atomicCondition.getFieldUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), entityFieldId)) {
					matchList.add(subCondition);
				}
			}
		}
		return matchList;
	}

    @Override
    protected void handleVLP(final UID sfEntityUid, final CollectableEntityField cef, final UID columnName, final CollectableComponent clctcomp, final CollectableResultComponent clctResultComponent) {

        if (!(clctResultComponent instanceof ResultPanel)) {
            return;
        }

		//BMWFDM-322 et.al: In the search (and only there) the ValueListProviders will not be considered for List of Values (LOV).
        //This has been already the case for standard search mask and from now for Subform-Search-Filters as well.

        // handle valuelistprovider
        final LabeledCollectableComponentWithVLP clctWithVLP = (LabeledCollectableComponentWithVLP) clctcomp;
        final UID fieldUid = clctWithVLP.getFieldUID();
        CollectableFieldsProvider valuelistprovider = null;
		EntityFieldVlpConfig entityFieldVlpConfig = getEntityFieldVlpConfig(cef.getCollectableEntity().getMeta().getField(cef.getUID()));
		if (entityFieldVlpConfig != null) {
			valuelistprovider = generateDatasourceBasedCollectableFieldsProvider(cef, entityFieldVlpConfig, fieldUid);
			setAdditionalSearchCondition(valuelistprovider, fieldUid);
		} else if (cef.isReferencing()) {
			valuelistprovider = collectableFieldsProviderFactory.newDefaultCollectableFieldsProvider(fieldUid);
			setAdditionalSearchCondition(valuelistprovider, fieldUid);

        }
        clctWithVLP.setValueListProvider(valuelistprovider);
        clctWithVLP.refreshValueList(true);
		Set<UID> focusAlreadyGained = new HashSet<>();
        final FocusAdapter refreshVLPAdapter = new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
            	if (focusAlreadyGained.contains(fieldUid)) {
            		// No need to reload the data
            		return;
				}
				focusAlreadyGained.add(fieldUid);
                // set the value list provider (dynamically):
                CollectableFieldsProvider valuelistprovider;

				EntityFieldVlpConfig entityFieldVlpConfig = getEntityFieldVlpConfig(cef.getCollectableEntity().getMeta().getField(cef.getUID()));
				// If no provider was set, use the default provider for static cell editors by default:
				if (LangUtils.equal(SF.STATE.getUID(sfEntityUid), fieldUid)) {
					valuelistprovider = new StatusCollectableFieldsProvider(sfEntityUid, null);
				} else if (LangUtils.equal(SF.STATENUMBER.getUID(sfEntityUid),
						fieldUid)) {
					valuelistprovider = new StatusNumeralCollectableFieldsProvider(sfEntityUid, null);
				} else if (LangUtils.equal(SFE.PROCESS.getUID(sfEntityUid),
						fieldUid)) {
					valuelistprovider = new ProcessCollectableFieldsProvider(sfEntityUid);
				} else if (LangUtils.equal(SF.MANDATOR.getUID(sfEntityUid),
						fieldUid)) {
					valuelistprovider = new MandatorCollectableFieldsProvider(sfEntityUid, null);
				} else if (entityFieldVlpConfig != null) {
					valuelistprovider = generateDatasourceBasedCollectableFieldsProvider(cef, entityFieldVlpConfig, fieldUid);
					setAdditionalSearchCondition(valuelistprovider, fieldUid);
				}
				else {
					valuelistprovider = collectableFieldsProviderFactory.newDefaultCollectableFieldsProvider(fieldUid);
					setAdditionalSearchCondition(valuelistprovider, fieldUid);
				}

                clctWithVLP.setValueListProvider(valuelistprovider);

                JTextComponent compText = getJTextComponentFromRefComm(clctcomp.getControlComponent());

                // remember old value here.
                String clctfValue = compText.getText();

				// refresh value list:
                clctWithVLP.refreshValueList(false);

				try {
					if (compText.getDocument() instanceof AutoCompleteDocument) {

						Field fSelecting = compText.getDocument().getClass().getDeclaredField("selecting");
						Method mSetText = compText.getDocument().getClass().getDeclaredMethod("setText", String.class);
						fSelecting.setAccessible(true);
						mSetText.setAccessible(true);
						fSelecting.set(compText.getDocument(), true);
						compText.setText(clctfValue);
						mSetText.invoke(compText.getDocument(), clctfValue);
						fSelecting.set(compText.getDocument(), false);

					} else {
						compText.setText(clctfValue);
					}
				} catch (NoSuchFieldException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
					LOG.error(ex.getMessage(), ex);
					compText.setText(clctfValue);
				}
			}

			@Override
			public void focusLost(final FocusEvent e) {
            	focusAlreadyGained.remove(fieldUid);
			}
		};
		clctcomp.getModel().addCollectableComponentModelListener(null, new ChangeListenerTriggerClientRules(cef.getCollectableEntity().getMeta().getField(cef.getUID())));
        JComponent comp = clctcomp.getControlComponent();
        if (comp instanceof ListOfValues) {
            ((ListOfValues) comp).getJTextField().addFocusListener(refreshVLPAdapter);
        } else if (comp instanceof JComboBox) {
        	for (Component c : comp.getComponents()) {
                if (c instanceof JButton)
                    c.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(final MouseEvent e) {
							refreshVLPAdapter.focusGained(null);
						}

//						@Override
//                        public void mouseEntered(MouseEvent e) {
//                            refreshVLPAdapter.focusGained(null);
//                        }
                    });
            }
            comp.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseExited(final MouseEvent e) {
					refreshVLPAdapter.focusLost(null);
				}
			});
            ((JComboBox) comp).getEditor().getEditorComponent().addFocusListener(refreshVLPAdapter);
        }
    }

	/**
	 * Generates a DatasourceBasedCollectableFieldsProvider from the specified EntityFieldVlpConfig, if a matching context is found.
	 * Otherwise, it will return a DefaultCollectableFieldsProvider for the specified fieldUid.
	 *
	 * @param cef
	 * @param entityFieldVlpConfig
	 * @param fieldUid
	 * @return
	 */
	private CollectableFieldsProvider generateDatasourceBasedCollectableFieldsProvider(CollectableEntityField cef,
																					   EntityFieldVlpConfig entityFieldVlpConfig,
																					   final UID fieldUid) {
		EntityFieldVlpContext entityFieldVlpContext = getMatchingContext(cef.getEntityUID(), entityFieldVlpConfig);
		CollectableFieldsProvider valuelistprovider;
		if (entityFieldVlpConfig != null) {
			valuelistprovider = new DatasourceBasedCollectableFieldsProvider(true, new UID(entityFieldVlpConfig.getVlpId()), DatasourceDelegate.getInstance(), cef.getEntityUID(), cef.getUID());
			valuelistprovider.setParameter(ValuelistProviderVO.DATASOURCE_NAMEFIELD, entityFieldVlpConfig.getValueField());
			valuelistprovider.setParameter(ValuelistProviderVO.DATASOURCE_IDFIELD, entityFieldVlpConfig.getIdField());
			valuelistprovider.setParameter("reffield", entityFieldVlpConfig.getVlpId());
			if (entityFieldVlpContext != null) {
				for (EntityFieldVlpParam p : entityFieldVlpContext.getVlpParams()) {
					boolean valueFromSearchFilter = false;
					if (p.getValueFromEntityFieldId() != null) {
						String valueFromCurrentFilter = getValueFromCurrentFilter(p.getValueFromEntityFieldId());
						if (valueFromCurrentFilter != null) {
							valuelistprovider.setParameter(p.getName(), valueFromCurrentFilter);
							valueFromSearchFilter = true;
						}
					}
					if (!valueFromSearchFilter && p.getValue() != null) {
						valuelistprovider.setParameter(p.getName(), p.getValue());
					}
				}
			}
		} else {
			valuelistprovider = collectableFieldsProviderFactory.newDefaultCollectableFieldsProvider(fieldUid);
		}
		return valuelistprovider;
	}

	/**
	 * A {@link CollectableComponentModelListener}, which triggers the execution of client rules, whenever a field change is associated with a rule.
	 * Currently, the {@link ResultPanelFilter} is the only location in the Richclient, where {@link EntityFieldClientRules} are executed; hence, this private class is directly embedded here.
	 */
	private class ChangeListenerTriggerClientRules implements CollectableComponentModelListener {

		private FieldMeta<?> fieldMeta;
		private ArrayList<EntityFieldClientRuleStatement> ruleList = new ArrayList<>();

		public ChangeListenerTriggerClientRules(FieldMeta<?> fm) {
			this.fieldMeta = fm;
			if (fm.getClientRules() != null) {
				final ObjectMapper mapper = new ObjectMapper();
				try {
					EntityFieldClientRules entityFieldClientRules = mapper.readValue(fm.getClientRules(), EntityFieldClientRules.class);
					entityFieldClientRules.getRules().forEach(rule -> {
						rule.getClearValueStatements().forEach(stmt -> {
							stmt.setTargetEntityFieldId(stmt.getTargetEntityFieldId());
							ruleList.add(stmt);
						});
						rule.getTransferLookedupValueStatements().forEach(stmt -> {
							stmt.setSourceEntityFieldId(stmt.getSourceEntityFieldId());
							stmt.setTargetEntityFieldId(stmt.getTargetEntityFieldId());
							ruleList.add(stmt);
						});
					});
					// sort by order ascending
					Collections.sort(ruleList, Comparator.comparing(EntityFieldClientRuleStatement::getOrder));
				} catch (Exception e) {
					Errors.getInstance().showExceptionDialog(MainFrame.getInstance(), e);
				}
			}
		}

		private void performEntityFieldClientRuleStatementClearValue(EntityFieldClientRuleStatementClearValue clearValueStatement) {
			UID clearUid = new UID(clearValueStatement.getTargetEntityFieldId());

			CollectableComponent compToClear = getExternalResultFilter().getActiveFilterComponents().get(clearUid);
			if (compToClear != null) {
				compToClear.getModel().setField(CollectableValueField.NULL);
			}
		}

		private void performEntityFieldClientRuleStatementTransferLookedupValue(EntityFieldClientRuleStatementTransferLookedupValue transferLookedupValue, final CollectableComponentModelEvent ev) {
			UID sourceUid = UID.parseUID(transferLookedupValue.getSourceEntityFieldId());
			UID targetUid = UID.parseUID(transferLookedupValue.getTargetEntityFieldId());

			final MasterDataVO sourceMdvo;
			try {
				if (ev.getNewValue().getValueId() != null) {
					sourceMdvo = MasterDataDelegate.getInstance().get(fieldMeta.getForeignEntity(), ev.getNewValue().getValueId());
				} else {
					sourceMdvo = null;
				}
			} catch (CommonFinderException | CommonPermissionException e) {
				Errors.getInstance().showDetailedExceptionDialog(MainFrame.getInstance(), e);
				return;
			}

			CollectableComponent transferTarget = getExternalResultFilter().getActiveFilterComponents().get(targetUid);

			if (transferTarget != null) {
				CollectableValueField transferValue;
				if (ev.getNewValue().getValueId() != null) {
					transferValue = new CollectableValueField(sourceMdvo.getFieldValue(sourceUid));
				} else {
					transferValue = new CollectableValueField(null);
				}

				transferTarget.getModel().setField(transferValue);
			}
		}

		@Override
		public void collectableFieldChangedInModel(final CollectableComponentModelEvent ev) {
			for (EntityFieldClientRuleStatement entityFieldClientRuleStatement : ruleList) {
				if (entityFieldClientRuleStatement instanceof EntityFieldClientRuleStatementClearValue) {
					performEntityFieldClientRuleStatementClearValue((EntityFieldClientRuleStatementClearValue) entityFieldClientRuleStatement);
				}
				else {
					performEntityFieldClientRuleStatementTransferLookedupValue((EntityFieldClientRuleStatementTransferLookedupValue) entityFieldClientRuleStatement, ev);
				}
			}
		}

		@Override
		public void searchConditionChangedInModel(final SearchComponentModelEvent ev) { }

		@Override
		public void valueToBeChanged(final DetailsComponentModelEvent ev) { }

	}

}
