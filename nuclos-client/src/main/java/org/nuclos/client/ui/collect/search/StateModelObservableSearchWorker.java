//Copyright (C) 2021  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.search;

import java.util.List;
import java.util.Observable;

import org.nuclos.client.statemodel.admin.CollectableStateModel;
import org.nuclos.client.statemodel.admin.StateModelCollectController;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.genericobject.ProxyList;

public class StateModelObservableSearchWorker extends Observable implements SearchWorker<UID, CollectableStateModel> {

	private final StateModelCollectController cc;

	public StateModelObservableSearchWorker(StateModelCollectController stateModelCollectController){
		cc = stateModelCollectController;
	}

	@Override
	public void startSearch() throws CommonBusinessException {
		startSearch(false);
	}

	@Override
	public void startSearch(final boolean bRefreshOnly) throws CommonBusinessException {

	}

	@Override
	public ProxyList<UID, CollectableStateModel> getResult() throws CommonBusinessException {
		return cc.getSearchStrategy().getSearchResult();
	}

	@Override
	public void finishSearch(final List<CollectableStateModel> lstclctResult) throws CommonBusinessException {
		super.setChanged();
		super.notifyObservers("search finished");
	}

}
