//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.strategy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.NullArgumentException;
import org.nuclos.client.common.Utils;
import org.nuclos.client.genericobject.CollectableGenericObject;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectDelegate;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * inner class <code>CompleteGenericObjectsStrategy</code>
 */
public class CompleteGenericObjectsStrategy implements
		CompleteCollectablesStrategy<Long,CollectableGenericObjectWithDependants> {
	
	private final static Logger LOG = LoggerFactory.getLogger(CompleteGenericObjectsStrategy.class);

	@Override
	public boolean isComplete(CollectableGenericObjectWithDependants clct) {
		/** @todo add "contains all required dependants */
		return clct.isComplete();
	}

	@Override
	public boolean getCollectablesInResultAreAlwaysComplete() {
		return false;
	}

	/**
	 * reads a bunch of <code>CollectableGenericObjectWithDependants</code> from the database.
	 * 
	 * §precondition collclctlo != null
	 * §postcondition result != null
	 * §postcondition result.size() == collclct.size()
	 * 
	 * @param collclctlo Collection&lt;Collectable&gt;
	 * @param bLoadThumbnailsOnly
	 * @return Collection&lt;Collectable&gt; contains the read <code>CollectableGenericObjectWithDependants</code>.
	 */
	@Override
	public Collection<CollectableGenericObjectWithDependants> getCompleteCollectables(
			Collection<CollectableGenericObjectWithDependants> collclctlo, String customUsage,
			final Boolean bLoadThumbnailsOnly) throws CommonBusinessException {
		if (collclctlo == null)
			throw new NullArgumentException("collclctlo");
		final Collection<CollectableGenericObjectWithDependants> result = new ArrayList<CollectableGenericObjectWithDependants>();

		final Collection<CollectableGenericObject> collclctIncomplete = new ArrayList<CollectableGenericObject>();
		CollectionUtils.split(collclctlo, new Collectable.IsComplete(), result, collclctIncomplete);

        LOG.debug("split " + collclctlo.size() + " collectables into " + collclctIncomplete.size() + " incomplete and " + result.size() + " complete");
		if (!collclctIncomplete.isEmpty()) {
			LOG.debug("list of incomplete collectables contains " + collclctIncomplete.size() + " entries");

			final Collection<Object> collIds = CollectionUtils.transform(collclctIncomplete, new Collectable.GetId());
			// FIXME this condition ignores the users sorting criteria => unpredictable processing order
			final CollectableSearchCondition cond = SearchConditionUtils.getCollectableSearchConditionForIds(collIds);

			final UID iCommonModuleId = getCommonModuleId(collclctlo);

			final Set<UID> stRequiredSubEntityNames =
					Collections.<UID> emptySet();
			//(iCommonModuleId == null) ? Collections.<UID> emptySet()
			//		: GenericObjectMetaDataCache.getInstance().getSubFormEntityNamesByModuleId(iCommonModuleId);

			final List<GenericObjectWithDependantsVO> lstlowdcvo = GenericObjectDelegate.getInstance()
					.getCompleteGenericObjectsWithDependants(iCommonModuleId, cond, stRequiredSubEntityNames, customUsage, bLoadThumbnailsOnly);

			final List<CollectableGenericObjectWithDependants> lstTransformedCollectables = CollectionUtils.transform(lstlowdcvo,
					new CollectableGenericObjectWithDependants.MakeCollectable());
			LOG.debug("add " + lstTransformedCollectables.size() + " transformed collectables to result list");
			result.addAll(lstTransformedCollectables);
		}
		
		if (null == result) {
			LOG.error("result must not be null");

		}

		assert result != null;


		if (result.size() != collclctlo.size()) {
			LOG.error("expected " + collclctlo.size() + " but received " + result.size());
		}

		assert result.size() == collclctlo.size();
		return result;
	}

	/**
	 * @param collclctlo Collection<CollectableGenericObject>
	 * @return the common module id, if any, of the given leased objects.
	 */
	private static UID getCommonModuleId(Collection<? extends CollectableGenericObject> collclctlo) {
		return Utils.getCommonObject(CollectionUtils.transform(collclctlo,
				new Transformer<CollectableGenericObject, UID>() {
					@Override
					public UID transform(CollectableGenericObject clctlo) {
						return clctlo.getGenericObjectCVO().getModule();
					}
				}));
	}

	/**
	 * @return the identifier, status, status numeral and all quintuple fields.
	 */
	@Override
	public Set<UID> getRequiredFieldUidsForResult() {
		throw new NotImplementedException("not implemented");
		//TODO MULTINUCLET
		/*
		final Set<String> result = new HashSet<String>();
		result.add(SF.SYSTEMIDENTIFIER.getMetaData().getFieldUID());
		result.add(SF.STATE.getMetaData().getFieldUID());
		result.add(SF.STATENUMBER.getMetaData().getFieldUID());
		result.add(SF.STATEICON.getMetaData().getFieldUID());
		result.addAll(GenericObjectCollectController.getUsageCriteriaFieldNames());
		return result;
		*/
	}

} // inner class CompleteGenericObjectsStrategy
