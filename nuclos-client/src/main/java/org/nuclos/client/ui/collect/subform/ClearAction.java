package org.nuclos.client.ui.collect.subform;

import org.nuclos.common.UID;

/**
 * inner class ClearAction.
 */
public class ClearAction {
	private final UID sTargetComponentName;

	public ClearAction(UID sTargetComponentName) {
		this.sTargetComponentName = sTargetComponentName;
	}

	public UID getTargetComponentName() {
		return this.sTargetComponentName;
	}
}
