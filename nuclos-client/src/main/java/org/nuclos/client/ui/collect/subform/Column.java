package org.nuclos.client.ui.collect.subform;

/**
 * Created by oliver brausch on 17.07.17.
 */

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

import org.nuclos.client.ui.collect.CollectableResultComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common2.LangUtils;

/**
 * A column in a <code>SubForm</code>.
 */
public class Column extends CollectableResultComponent.ResultComponentColumn {
	private final UID uid;
	private String sLabel;
	private final CollectableComponentType clctcomptype;
	private final boolean bVisible;
	private final boolean bEnabled;
	private final boolean bInsertable;
	private final boolean bCloneable;

	private final boolean bMultiselectsearchfilter;
	private final boolean bLovSearch;

	private final Integer iRows;
	private final Integer iColumns;
	private final Integer width;
	private final Integer initialPosition;
	private final UID nextFocusField;
	private final String customUsageSearch;
	private Map<String, Object> mpProperties;

	/**
	 * Collection<TransferLookedUpValueAction>
	 */
	private Collection<TransferLookedUpValueAction> collTransferLookedUpValueActions;

	/**
	 * Collection<ClearAction>
	 */
	private Collection<ClearAction> collClearActions;

	/**
	 * Collection<RefreshValueListAction>
	 */
	private Collection<RefreshValueListAction> collRefreshValueListActions;

	private CollectableFieldsProvider valuelistprovider;

	public Column(UID uid) {
		//start merge
		this(uid, null, new CollectableComponentType(null, null), true, true, true,false,false, false, null, null, null); //merged
	}

	public Column(UID uid, String sLabel, CollectableComponentType clctcomptype, boolean bVisible, boolean bEnabled, boolean bCloneable,
				  boolean bInsertable, Integer iRows, Integer iColumns, String customUsageSearch) {
		this(uid, sLabel, clctcomptype, bVisible, bEnabled, bCloneable, bInsertable,false,false, iRows, iColumns, customUsageSearch, null, null);
	}
	public Column(UID uid, String sLabel, CollectableComponentType clctcomptype, boolean bVisible, boolean bEnabled, boolean bCloneable,boolean bInsertable,boolean bMultiselectsearchfilter,
				  boolean bLovSearch, Integer iRows, Integer iColumns, String customUsageSearch) {
		this(uid, sLabel, clctcomptype, bVisible, bEnabled, bCloneable, bInsertable,bMultiselectsearchfilter,bLovSearch, iRows, iColumns, customUsageSearch, null, null);
	}

	public Column(UID uid, String sLabel, CollectableComponentType clctcomptype, boolean bVisible, boolean bEnabled, boolean bCloneable,boolean bInsertable,
				  boolean bMultiselectsearchfilter,boolean bLovSearch,Integer iRows, Integer iColumns, String customUsageSearch, Integer width, UID nextFocusField) {

		super(uid, clctcomptype);
		this.uid = uid;
		this.sLabel = sLabel;
		this.clctcomptype = clctcomptype;
		this.bVisible = bVisible;
		this.bEnabled = bEnabled;
		this.bInsertable = bInsertable;
		this.bCloneable = bCloneable;
		this.bMultiselectsearchfilter=bMultiselectsearchfilter;
		this.bLovSearch = bLovSearch;
		this.iRows = iRows;
		this.iColumns = iColumns;
		this.width = width;
		this.initialPosition = null;
		this.nextFocusField = nextFocusField;
		this.customUsageSearch = customUsageSearch;
	}

	/**
	 * use {@link getUID()} instead
	 * @return
	 */
	@Deprecated
	public UID getName() {
		return this.getUID();
	}

	public UID getUID() {
		return this.uid;
	}

	public String getLabel() {
		return this.sLabel;
	}

	public void setLabel(String label) {
		this.sLabel = label;
	}

	public CollectableComponentType getCollectableComponentType() {
		return this.clctcomptype;
	}

	public boolean isVisible() {
		return this.bVisible;
	}

	public boolean isEnabled() {
		return this.bEnabled;
	}

	public boolean isInsertable() {
		return this.bInsertable;
	}

	public boolean isCloneable() {
		return this.bCloneable;
	}

	public boolean isMultiselectsearchfilter() {
		return this.bMultiselectsearchfilter;
	}

	public boolean isLovSearch() {
		return this.bLovSearch;
	}


	/**
	 * @return the number of rows used in the renderer and editor
	 */
	public Integer getRows() {
		return this.iRows;
	}

	/**
	 * @return the number of columns used in the renderer and editor
	 */
	public Integer getColumns() {
		return this.iColumns;
	}

	/**
	 * Returns the preferred width of this column.
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * Returns the nextfocuscomponent of this column.
	 */
	public UID getNextFocusField() {
		return nextFocusField;
	}

	public String getCustomUsageSearch() {
		return customUsageSearch;
	}

	/**
	 * Returns the initial position of this column (relative to the other columns).
	 */
	public Integer getInitialPosition() {
		return initialPosition;
	}

	@Override
	public boolean equals(Object oValue) {
		if (this == oValue) {
			return true;
		}
		if (!(oValue instanceof Column)) {
			return false;
		}
		return LangUtils.equal(getName(), ((Column) oValue).getName());
	}

	@Override
	public int hashCode() {
		return LangUtils.hashCode(getName());
	}

	public Collection<TransferLookedUpValueAction> getTransferLookedUpValueActions() {
		return (collTransferLookedUpValueActions != null) ? Collections.unmodifiableCollection(collTransferLookedUpValueActions) : Collections.<TransferLookedUpValueAction>emptyList();
	}

	public void addTransferLookedUpValueAction(TransferLookedUpValueAction act) {
		if (collTransferLookedUpValueActions == null) {
			collTransferLookedUpValueActions = new LinkedList<TransferLookedUpValueAction>();
		}
		collTransferLookedUpValueActions.add(act);
	}

	public Collection<ClearAction> getClearActions() {
		return collClearActions != null ? Collections.unmodifiableCollection(collClearActions) : Collections.<ClearAction>emptyList();
	}

	public void addClearAction(ClearAction act) {
		if (collClearActions == null) {
			collClearActions = new LinkedList<ClearAction>();
		}
		collClearActions.add(act);
	}

	public Collection<RefreshValueListAction> getRefreshValueListActions() {
		return collRefreshValueListActions != null ? Collections.unmodifiableCollection(collRefreshValueListActions) : Collections.<RefreshValueListAction>emptyList();
	}

	public void addRefreshValueListAction(RefreshValueListAction act) {
		if (collRefreshValueListActions == null) {
			collRefreshValueListActions = new LinkedList<RefreshValueListAction>();
		}
		collRefreshValueListActions.add(act);
	}

	public void setValueListProvider(CollectableFieldsProvider valuelistprovider) {
		this.valuelistprovider = valuelistprovider;
	}

	public CollectableFieldsProvider getValueListProvider() {
		return this.valuelistprovider;
	}

	public Object getProperty(String sName) {
		return getProperties().get(sName);
	}

	public void setProperty(String sName, Object oValue) {
		synchronized (this) {
			if (mpProperties == null) {
				mpProperties = new TreeMap<String, Object>();
			}
		}
		mpProperties.put(sName, oValue);

		assert LangUtils.equal(getProperty(sName), oValue);
	}

	public synchronized Map<String, Object> getProperties() {
		final Map<String, Object> result = (mpProperties == null) ? Collections.<String, Object>emptyMap() : Collections.unmodifiableMap(mpProperties);
		assert result != null;
		return result;
	}

}	// not anyore "inner class Column"
