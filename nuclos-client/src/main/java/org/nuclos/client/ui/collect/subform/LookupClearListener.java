package org.nuclos.client.ui.collect.subform;

import java.util.Collection;

import org.nuclos.client.ui.collect.component.LookupEvent;
import org.nuclos.client.ui.collect.component.LookupListener;

/**
 * Created by Oliver Brausch on 17.07.17.
 */
class LookupClearListener<PK2> implements LookupListener<PK2> {

	private final SubFormTableModel subformtblmdl;

	private final SubFormTable subformtbl;

	final Collection<ClearAction> collClearActions;

	LookupClearListener(SubFormTableModel subformtblmdl,
						SubFormTable subformtbl, Collection<ClearAction> collClearActions) {

		this.subformtblmdl = subformtblmdl;
		this.subformtbl = subformtbl;
		this.collClearActions = collClearActions;
	}

	@Override
	public void lookupSuccessful(LookupEvent<PK2> ev) {
		SubForm.clearValues(subformtbl, subformtblmdl, subformtbl.getEditingRow(), collClearActions);
	}

	@Override
	public int getPriority() {
		return 1;
	}
}
