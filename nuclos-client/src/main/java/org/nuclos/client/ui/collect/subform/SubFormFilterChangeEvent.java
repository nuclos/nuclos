package org.nuclos.client.ui.collect.subform;

import java.util.EventObject;

public class SubFormFilterChangeEvent extends EventObject {

    private Object filter;

    public SubFormFilterChangeEvent(Object source, Object filter) {
        super(source);
        this.filter = filter;
    }

    public Object getFilter() {
        return filter;
    }

    public void setFilter(Object filter) {
        this.filter = filter;
    }
}
