package org.nuclos.client.ui.collect.subform;

public interface SubFormFilterChangeListener {
    void filterChanged(SubFormFilterChangeEvent e);
}
