//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.subform;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.profile.ProfileItem;

public interface SubFormParameterProvider {

	/**
	 * @param iRow
	 * @param parentComponentUid
	 * @param parentComponentEntityUid
	 * @return
	 */
	CollectableField getParameterForRefreshValueList(final SubFormTableModel subformtblmdl, int iRow,
													 final UID parentComponentUid, final UID parentComponentEntityUid);
	
	/**
	 * Could be null, i.e. only non-null for DetailsSubFormController at present.
	 */
	ProfileItem getSelectedProfile();
	
	/**
	 * Return the main entity this subform is bound to.
	 */
	UID getParentEntityUid();

	/**
	 * Notify: The selected profile is updated
	 */
	void updateSelectedProfile();
}
