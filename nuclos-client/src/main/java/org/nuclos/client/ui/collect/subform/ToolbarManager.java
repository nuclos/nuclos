package org.nuclos.client.ui.collect.subform;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Oliver Brausch on 17.07.17.
 */
public class ToolbarManager {
	private final Map<ToolbarFunction, ToolbarFunctionState> mpToolbarStateListener = new ConcurrentHashMap<ToolbarFunction, ToolbarFunctionState>();

	public void setState(final ToolbarFunction function, ToolbarFunctionState state) {
		this.mpToolbarStateListener.put(function, state);
	}

	public ToolbarFunctionState getStateIfAvailable(final ToolbarFunction function) {
		if (this.mpToolbarStateListener.containsKey(function)) {
			return this.mpToolbarStateListener.get(function);
		}
		return null;
	}
}
