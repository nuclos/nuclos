package org.nuclos.client.ui.collect.toolbar;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenuItem;

import org.nuclos.common2.SpringLocaleDelegate;

public class NuclosToolBarMenuAction extends JMenuItem implements INuclosToolBarItem {
	
	private final String resourceIdLabel;
	
	private final String resourceIdToolTip;
	
	public NuclosToolBarMenuAction(String resourceIdLabel, String resourceIdToolTip, Icon icon) {
		super();
		final SpringLocaleDelegate ld = SpringLocaleDelegate.getInstance();
		setIcon(icon);
		this.resourceIdLabel = resourceIdLabel;
		this.resourceIdToolTip = resourceIdToolTip;
		
		if (resourceIdLabel != null) {
			setText(ld.getResource(resourceIdLabel, resourceIdLabel));
		}
	}
	
	public NuclosToolBarMenuAction(String resourceIdLabel, Icon icon) {
		this(resourceIdLabel, null, icon);
	}

	@Override
	public String getTooltip() {
		return null;
	}

	@Override
	public Action init(Action action) {
		setAction(action);
		return getAction();
	}

}
