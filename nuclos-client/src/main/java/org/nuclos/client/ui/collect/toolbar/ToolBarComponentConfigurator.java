//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JComponent;

import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.collect.ToolBarItem.Type;
import org.nuclos.common.collect.ToolBarItemGroup;

public abstract class ToolBarComponentConfigurator extends ToolBarItemGroupComponent {
	
	private final Map<ToolBarItem, Action> registeredActions = new HashMap<ToolBarItem, Action>();
	
	private final Map<ToolBarItem, JComponent[]> registeredComponents = new HashMap<ToolBarItem, JComponent[]>();
	
	private final Map<ToolBarItem, ToolBarItemGroupComponent> registeredGroupComponents = new HashMap<ToolBarItem, ToolBarItemGroupComponent>();
	
	private final Collection<JComponent> createdComponents = new ArrayList<JComponent>();
	
	public ToolBarComponentConfigurator(JComponent toolBarComponent) {
		super(toolBarComponent);
	}
	
	public void registerAction(ToolBarItem item, Action action) {
		registeredActions.put(item, action);
	}
	
	/**
	 * 
	 * @param jcomp
	 * @return 
	 * 		true = newly registered
	 * 		false = otherwise (inclusive already registered)
	 */
	public boolean registerComponent(JComponent jcomp) {
		for (JComponent[] compArray : registeredComponents.values()) {
			for (JComponent i : compArray) {
				if (i == jcomp) {
					return false;
				}
			}
		}
		
		ToolBarItem item = validateComponent(jcomp);
		
		if (item != null) {
			registeredComponents.put(item, new JComponent[] {jcomp});
			return true;
		}
		
		return false;
	}
	
	public void setup(ToolBarItemGroup toolBarGroup) {
		// remove components from last setup, if any
		Collection<JComponent> clearComponents = new ArrayList<JComponent>(createdComponents);
		for (JComponent[] compArray : registeredComponents.values()) {
			for (JComponent comp : compArray) {
				clearComponents.add(comp);
			}
		}
		clear(clearComponents);
		for (ToolBarItemGroupComponent groupComponent : registeredGroupComponents.values()) {
			groupComponent.clear(clearComponents);
		}
		createdComponents.clear();
		
		// add components recursively
		Map<ToolBarItem, JComponent[]> registeredComponentsWorkingCopy = new HashMap<ToolBarItem, JComponent[]>(registeredComponents);
		setup(this, toolBarGroup, 
				Collections.unmodifiableMap(registeredGroupComponents), 
				Collections.unmodifiableMap(registeredActions),
				registeredComponentsWorkingCopy,
				createdComponents);
	}
	
	public void close() {
		for (JComponent created : createdComponents) {
			if (created instanceof AbstractButton) {
				((AbstractButton) created).setAction(null);
			}
		}
	}
	
	private static boolean setup(
			ToolBarItemGroupComponent groupComponent, 
			ToolBarItemGroup toolBarGroup, 
			Map<ToolBarItem, ToolBarItemGroupComponent> registeredGroupComponents, 
			Map<ToolBarItem, Action> registeredActions,
			Map<ToolBarItem, JComponent[]> registeredComponentsWorkingCopy,
			Collection<JComponent> createdComponents) {
		
		if (groupComponent == null) {
			return false;
		}
		
		boolean result = false;
		
		List<ToolBarItem> reversedItems = new ArrayList<ToolBarItem>(toolBarGroup.getItems());
		Collections.reverse(reversedItems);
		for (ToolBarItem item : reversedItems) {
			JComponent[] jcomp = registeredComponentsWorkingCopy.get(item);
			if (jcomp != null) {
				registeredComponentsWorkingCopy.remove(item);
			}
			if (jcomp == null) {
				Action action = registeredActions.get(item);
				if (action != null) {
					jcomp = groupComponent.create(item, action);
					if (jcomp != null) {
						for (JComponent c : jcomp) {
							createdComponents.add(c);
						}
					}
				}
			}
			if (jcomp == null) {
				jcomp = groupComponent.createStatic(item);
				if (jcomp != null) {
					for (JComponent c : jcomp) {
						createdComponents.add(c);
					}
				}
			}
			
			if (jcomp != null) {
				if (item instanceof ToolBarItemGroup) {
					boolean groupResult = setup(
							registeredGroupComponents.get(item), 
							(ToolBarItemGroup) item, 
							registeredGroupComponents, 
							registeredActions,
							registeredComponentsWorkingCopy,
							createdComponents);
					if (groupResult) {
						result = true;
						add(groupComponent, item, jcomp);
					}
				} else {
					result = true;
					add(groupComponent, item, jcomp);
				}
			}
			
		}
		
		groupComponent.invalidate();
		return result;
	}
	
	private static void add(ToolBarItemGroupComponent groupComponent, ToolBarItem item, JComponent[] jcomp) {
		for (int i = jcomp.length-1; i >= 0; i--) {
			if (groupComponent.isHideLabelPossible() && (item.getType() == Type.ACTION || item.getType() == Type.TOGGLE_ACTION)) {
				String sShowText = item.getProperty(ToolBarItem.PROPERTY_SHOW_TEXT);
				Boolean bShowText = sShowText != null || Boolean.parseBoolean(sShowText);
				jcomp[0].putClientProperty("hideActionText", !bShowText);
			}
			groupComponent.addInFront(jcomp[i]);
		}
	}
	
	protected ToolBarItem validateComponent(JComponent jcomp) {
		if (jcomp != null) {
			ToolBarItem itemIfExist = (ToolBarItem) jcomp.getClientProperty(ToolBarItem.COMPONENT_PROPERTY_KEY);
			if (itemIfExist != null) {
				if (registeredComponents.containsKey(itemIfExist)) {
					throw new IllegalArgumentException(String.format("ToolBarItem %s already registered! Component1=%s, Component2=%s", itemIfExist, jcomp, registeredComponents.get(itemIfExist)));
				}
				if (itemIfExist.getType() == Type.GROUP) {
					ToolBarItemGroupComponent groupComponent = (ToolBarItemGroupComponent) jcomp.getClientProperty(PROPERTY_KEY);
					if (groupComponent == null) {
						throw new IllegalArgumentException(String.format("Component is of type GROUP but does not contain a group component! ToolBarItem=%s, JComponent=%s", itemIfExist, jcomp));
					}
					registeredGroupComponents.put(itemIfExist, groupComponent);
				}
				return itemIfExist;
			}
		}
		return null;
	}
	
}
