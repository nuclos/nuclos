//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar.editor;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.util.ITableLayoutBuilder;
import org.nuclos.client.ui.util.TableLayoutBuilder;

import info.clearthought.layout.TableLayout;

class MenuSeparator extends JPanel implements IToolBarComponent, MouseListener {
		private final ToolBarObject tbo;
		private final JLabel jlbRemove;
		public MenuSeparator(ToolBarObject tbo) {
			super();
			this.tbo = tbo;
//			setBackground(NuclosThemeSettings.BACKGROUND_COLOR4);
			setOpaque(false);
			jlbRemove = new JLabel(MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconPriorityCancel16(), 10));
			jlbRemove.addMouseListener(this);
			ITableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayout.FILL, TableLayout.PREFERRED);
			tbllay.newRow();
			tbllay.add(new JSeparator(), 1, TableLayout.FULL, TableLayout.CENTER);
			tbllay.add(jlbRemove);
		}
		@Override
		public ToolBarObject getObject() {
			return this.tbo;
		}
		@Override
		public Dimension getPreferredSize() {
			return new Dimension(Math.max(super.getPreferredSize().width, 100), 10);
		}
		@Override public void mouseClicked(MouseEvent e) {
			JPopupMenu parent = (JPopupMenu) getParent();
			parent.remove(this);
			parent.validate();
			parent.pack();
			parent.repaint();
		}
		@Override public void mousePressed(MouseEvent e) {}
		@Override public void mouseReleased(MouseEvent e) {}
		@Override public void mouseEntered(MouseEvent e) {}
		@Override public void mouseExited(MouseEvent e) {}
		@Override
		public void reset() {
		}
		@Override
		public JComponent getJComponent() {
			return this;
		}
	}