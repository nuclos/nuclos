//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar.editor;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.nuclos.client.ui.collect.indicator.CollectPanelIndicator;

class ToolBarCollectIndicator extends JPanel implements IToolBarComponent {
	private final ToolBarObject tbo;
	private final CollectPanelIndicator indicator;
	public ToolBarCollectIndicator(ToolBarObject tbo) {
		super(new BorderLayout());
		this.tbo = tbo;
		this.setOpaque(false);
		this.indicator = CollectPanelIndicator.createDummy();
		this.add(indicator.getJPanel(), BorderLayout.CENTER);
	}
	@Override
	public ToolBarObject getObject() {
		return tbo;
	}
	@Override
	public void reset() {
	}
	@Override
	public JComponent getJComponent() {
		return this;
	}
	
}