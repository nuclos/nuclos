//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar.editor;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.IllegalComponentStateException;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.MenuDragMouseEvent;
import javax.swing.event.MenuDragMouseListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.nuclos.client.theme.NuclosThemeSettings;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.WrapLayout;
import org.nuclos.client.ui.util.ITableLayoutBuilder;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.ToolBarConfiguration.Context;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.collect.ToolBarItemGroup;
import org.nuclos.common2.SpringLocaleDelegate;

import info.clearthought.layout.TableLayout;

public abstract class ToolBarConfigurationEditor implements MouseListener, MouseMotionListener, PopupMenuListener, MenuDragMouseListener, AncestorListener, ComponentListener {
	
		private JPanel jpnEditor;
		
		private int pos = -1;
		private int pos2 = -1;
		private int pos3 = -1;
		private boolean remove = false;
		
		private ToolBarObject dragged;
		
		private JToolBar jtbToolbar; 
		private JPanel jpnAvailable;
		private JToolBar jtbAvailable; 
		private JLabel jlbDrop;
		
		private JLabel jlbAddToExtra;
		private JButton btnDefault;
		private ToolBarConfiguration configuration;
		private Context context;
		
		private JLabel jlbExtra;
		private JPopupMenu jpmExtra;
		private int postExtraComponents;
		private ToolBarObject btnExtra;
		
		private boolean isDefault = false;
		
		public ToolBarConfigurationEditor() {
			jtbToolbar = new JToolBar() {
				@Override
				public void paint(Graphics g) {
					super.paint(g);
					if (pos > -1) {
						g.setColor(Color.RED);
						g.drawLine(pos, 0, pos, 100);
					}
				}
			};
			jpnAvailable = new JPanel(new BorderLayout()) {
				Color BLUE = new Color(0, 0, 255, 64);
				Color WHITE = new Color(255, 255, 255, 128);
				@Override
				public void paint(Graphics g) {
					super.paint(g);
					if (remove) {
						g.setColor(remove?BLUE:WHITE);
						Rectangle bounds = jpnAvailable.getBounds();
						g.fillRect(0, 0, bounds.width, bounds.height);
					}
				}
			};
			jlbDrop = new JLabel(SpringLocaleDelegate.getInstance().getText("ToolBarConfigurationEditor.1"));
			jtbAvailable = new JToolBar();
			jpmExtra = new JPopupMenu() {
				public void paint(Graphics g) {
					if (!isExtra()) {
						Graphics2D g2d = (Graphics2D) g.create(); 
		                g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.33f)); 
		                super.paint(g2d); 
		                g2d.dispose(); 
					} else {
						super.paint(g);
					}
					if (pos2 > -1) {
						g.setColor(Color.RED);
						g.drawLine(0, pos2, 400, pos2);
					}
				}
			};
			btnDefault = new JButton(new AbstractAction(SpringLocaleDelegate.getInstance().getText("reset_to_default")) {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (configuration != null) {
						setConfiguration(configuration.getDefaultConfiguration(), context);
						isDefault = true;
						UIUtils.getWindowForComponent(jpnEditor).pack();
					}
				}
			});	
			jlbAddToExtra = new JLabel(SpringLocaleDelegate.getInstance().getText("ToolBarConfigurationEditor.2"), Icons.getInstance().getDirectionEast(), JLabel.CENTER) {
				Color WHITE = new Color(255, 255, 255, 128);
				@Override
				public void paint(Graphics g) {
					if (pos3 == 1) {
						g.setColor(WHITE);
						Rectangle bounds = jpnAvailable.getBounds();
						g.fillRect(0, 0, bounds.width, bounds.height);
					}
					super.paint(g);
				}
			};
			
			jpmExtra.setFocusable(false);
			jpmExtra.addPopupMenuListener(this);
			
			jtbToolbar.setLayout(new WrapLayout(WrapLayout.LEFT));
			jtbToolbar.setFloatable(false);
			jtbToolbar.addAncestorListener(this);
			jtbToolbar.setBorder(BorderFactory.createLineBorder(NuclosThemeSettings.BACKGROUND_ROOTPANE, 1));
			
			jlbDrop.setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
			
			jtbAvailable.setLayout(new WrapLayout(WrapLayout.LEFT));
			jtbAvailable.setFloatable(false);
			jtbAvailable.setOpaque(false);
			jtbAvailable.putClientProperty("Synthetica.opaque", false);
			
			JLabel jlbDummy = new JLabel(SpringLocaleDelegate.getInstance().getText("hidden")+":");
			jlbDummy.setVerticalAlignment(JLabel.TOP);
			jlbDummy.setForeground(Color.WHITE);	
			jlbDummy.setBackground(NuclosThemeSettings.BACKGROUND_ROOTPANE);
			jlbDummy.setOpaque(true);
			jlbDummy.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			
			jlbExtra = new JLabel("Extras");
			jlbExtra.setVerticalAlignment(JLabel.TOP);
			jlbExtra.setForeground(Color.WHITE);
			jlbExtra.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			
			JPanel jpnCenter = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
			jpnCenter.setOpaque(false);
			jpnCenter.add(jlbDrop);
			jpnAvailable.setBackground(NuclosThemeSettings.BACKGROUND_COLOR4);
			jpnAvailable.add(jpnCenter, BorderLayout.NORTH);
			jpnAvailable.add(jtbAvailable, BorderLayout.CENTER);
			
			btnDefault.setFocusable(false);
			
			JPanel jpnDefault = new JPanel(new FlowLayout(FlowLayout.LEFT));
			jpnDefault.setBackground(NuclosThemeSettings.BACKGROUND_ROOTPANE);
			jpnDefault.add(btnDefault);
			
			jlbAddToExtra.setForeground(Color.WHITE);
			jlbAddToExtra.setHorizontalTextPosition(JLabel.LEFT);
			jlbAddToExtra.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
			
			jpnEditor = new JPanel();
			jpnEditor.setBackground(NuclosThemeSettings.BACKGROUND_ROOTPANE);
			ITableLayoutBuilder tbllay = new TableLayoutBuilder(jpnEditor).columns(100d, 500d, TableLayout.PREFERRED, 1d, 10d, 275d);
			tbllay.newRow().add(jtbToolbar, 4);
			tbllay.newRow(44d).newRow(TableLayout.FILL).newRow().newRow(1d);
			jpnEditor.add(jlbDummy, "0,1 0,2");
			jpnEditor.add(jpnAvailable, "1,1 2,2");
			jpnEditor.add(jpnDefault, "1,3");
			jpnEditor.add(jlbAddToExtra, "2,3");
			jpnEditor.add(jlbExtra, "5,0");
		}
		
		public JComponent getComponent() {
			return jpnEditor;
		}
		
		public void setConfiguration(ToolBarConfiguration configuration, Context context) {
			this.configuration = configuration;
			this.context = context;
			this.isDefault = false;
			
			jtbToolbar.removeAll();
			jtbAvailable.removeAll();
			jpmExtra.removeAll();
			btnExtra = null;
			
			final List<ToolBarItem> availableItems = new ArrayList<ToolBarItem>(configuration.getConfigurableItems(context));		
			for (ToolBarItem item : configuration.getMainToolBar().getItems()) {
				if (!availableItems.contains(item)) {
					continue;
				}
				availableItems.remove(item);
				ToolBarObject tbObject = null;
				if (NuclosToolBarItems.EXTRAS.equals(item)) {
					for (ToolBarItem extraItem : ((ToolBarItemGroup)item).getItems()) {
						if (!availableItems.contains(extraItem)) {
							continue;
						}
						if (!NuclosToolBarItems.MENU_SEPARATOR.equals(extraItem)) {
							availableItems.remove(extraItem);
						}
						jpmExtra.add(ToolBarObjectHelper.createToolBarObject(extraItem, this).getMenuComponent());
					}
					item = NuclosToolBarItems.newExtras();
					tbObject = ToolBarObjectHelper.createToolBarObject(item, this);
					btnExtra = tbObject;
				} else {
					tbObject = ToolBarObjectHelper.createToolBarObject(item, this);
				}
				jtbToolbar.add(tbObject.getToolBarComponent());
			}
			
			for (ToolBarItem item : availableItems) {
				ToolBarObject tbObject = null;
				if (NuclosToolBarItems.EXTRAS.equals(item)) {
					item = NuclosToolBarItems.newExtras();
					tbObject = ToolBarObjectHelper.createToolBarObject(item, this);
					btnExtra = tbObject;
				} else {
					tbObject = ToolBarObjectHelper.createToolBarObject(item, this);
				}
				jtbAvailable.add(tbObject.getToolBarComponent());
			}
			
			postExtra();
			
			jtbToolbar.validate();
			jpnAvailable.validate();
			jtbAvailable.validate();
			jpmExtra.validate();
			jpmExtra.pack();
			jtbToolbar.repaint();
			jpnAvailable.repaint();
			jtbAvailable.repaint();
			jlbAddToExtra.repaint();
			jpmExtra.repaint();
			
			UIUtils.getWindowForComponent(jpnEditor).pack();
			
			jpmExtra.setVisible(false);
			showExtra();
		}
		
		protected abstract ToolBarConfiguration createEmptyToolBarConfiguration();
		
		public ToolBarConfiguration getConfiguration() {
			if (isDefault) {
				return null;
			}
			final ToolBarConfiguration result = createEmptyToolBarConfiguration();
			for (Component c : jtbToolbar.getComponents()) {
				if (c instanceof IToolBarComponent) {
					IToolBarComponent tbc = (IToolBarComponent) c;
					ToolBarItem item = tbc.getObject().getToolBarItem();
					
					if (NuclosToolBarItems.EXTRAS.equals(item)) {
						ToolBarItemGroup extras = NuclosToolBarItems.newExtras();
						item = extras;
						
						for (Component ce : jpmExtra.getComponents()) {
							if (ce instanceof IToolBarComponent) {
								IToolBarComponent tbce = (IToolBarComponent) ce;
								ToolBarItem extraItem = tbce.getObject().getToolBarItem();
								extras.addItem(extraItem);
							}
						}
					}
					
					result.getMainToolBar().addItem(item);
				}
			}
			return result;
		}
		
		private void postExtra() {
			postExtraComponents = 2;
			jpmExtra.add(new MenuStopper());
			jpmExtra.add(new MenuAddSeparatorButton(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					jpmExtra.insert(ToolBarObjectHelper.createToolBarObject(NuclosToolBarItems.MENU_SEPARATOR, ToolBarConfigurationEditor.this).getMenuComponent(),
							jpmExtra.getComponentCount()-2);
					jpmExtra.validate();
					jpmExtra.pack();
					jpmExtra.repaint();
				}}));
		}
		
		private Component getToolBarComponentAtPosition(MouseEvent arg0) {
			final int x = arg0.getXOnScreen()-jtbToolbar.getLocationOnScreen().x;
			final int y = arg0.getYOnScreen()-jtbToolbar.getLocationOnScreen().y;
			Component result = jtbToolbar.getComponentAt(x, y);
			if (result == jtbToolbar) {
				// test gap between components
				result = jtbToolbar.getComponentAt(x-5, y);
				if (result == jtbToolbar) {
					return null;
				}
			}
			return result;
		}
		
		private Component getExtraComponentAtPosition(MouseEvent arg0, Point extraOnScreen) {
			final int x = arg0.getXOnScreen()-extraOnScreen.x;
			final int y = arg0.getYOnScreen()-extraOnScreen.y;
			Component result = jpmExtra.getComponentAt(x, y);
			if (result == jpmExtra) {
				// test gap between components
				result = jpmExtra.getComponentAt(x, y-5);
				if (result == jpmExtra) {
					return null;
				}
			}
			if (result instanceof MenuAddSeparatorButton) {
				// return the "stopper"
				return jpmExtra.getComponent(jpmExtra.getComponentCount()-2);
			}
			return result;
		}
		
		private void showExtra() {
			if (!getComponent().isShowing()) return;
			if (isExtra()) {
				jlbExtra.setVisible(btnExtra != null);
				jlbAddToExtra.setVisible(btnExtra != null);
				if (btnExtra != null) {
					jpmExtra.setVisible(true);
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								jpmExtra.show(jtbToolbar, jtbToolbar.getWidth()+10, 20);
							} catch (IllegalComponentStateException ex) {
								// not showing any more... ignore
							}
						}
					});
				}
			}
		}
		
		private boolean isExtra() {
			return btnExtra!=null && btnExtra.getToolBarComponent().getParent() == jtbToolbar;
		}

		@Override
		public void mouseDragged(MouseEvent event) {
//			System.out.println("Dragged " + arg0);
			Component cDrag = event.getComponent();
			if (dragged == null && cDrag instanceof IToolBarComponent) {
				dragged = ((IToolBarComponent) cDrag).getObject();
				
				Image image = new BufferedImage(cDrag.getWidth(), cDrag.getHeight(), BufferedImage.TYPE_INT_ARGB);
				Graphics graphics = image.getGraphics();
				cDrag.paint(graphics);

				Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(image, new Point(0,0), ((IToolBarComponent)cDrag).getObject().getToolBarItem().toString());
				jtbToolbar.setCursor(cursor);
				jpnAvailable.setCursor(cursor);
				jtbAvailable.setCursor(cursor);
				jpmExtra.setCursor(cursor);
				jlbAddToExtra.setCursor(cursor);
			}
			
			if (new Rectangle(jpnAvailable.getLocationOnScreen(), jpnAvailable.getSize()).contains(event.getLocationOnScreen())) {
				remove = true;
				pos = -1;
				pos2 = -1;
				pos3 = -1;
			} else if (new Rectangle(jtbToolbar.getLocationOnScreen(), jtbToolbar.getSize()).contains(event.getLocationOnScreen())) {
				remove = false;
				pos = -1;
				pos2 = -1;
				pos3 = -1;
				if (!dragged.isMenuOnly()) {
					final Component c = getToolBarComponentAtPosition(event);
					if (c != null) {
						if (c != event.getComponent()) {
							pos = c.getX();
						}
					} else {
						Component lastComponent = jtbToolbar.getComponentAtIndex(jtbToolbar.getComponentCount()-1);
						if (lastComponent == null) {
							pos = 6;
						} else {
							pos = lastComponent.getX()+lastComponent.getWidth()-1;
						}
					}
				}
			} else if (new Rectangle(jlbAddToExtra.getLocationOnScreen(), jlbAddToExtra.getSize()).contains(event.getLocationOnScreen())) {
				remove = false;
				pos = -1;
				pos2 = -1;
				pos3 = -1;
				if (!dragged.isToolbarOnly()) {
					pos3 = 1;
				}
			} else if (new Rectangle(jpmExtra.getLocationOnScreen(), jpmExtra.getSize()).contains(event.getLocationOnScreen())) {
				remove = false;
				pos = -1;
				pos2 = -1;
				pos3 = -1;
				if (!dragged.isToolbarOnly()) {
					final Component c = getExtraComponentAtPosition(event, jpmExtra.getLocationOnScreen());
					if (c != null) {
						if (c != dragged.getToolBarComponent()) {
							pos2 = c.getY();
						}
					}
				}
			}
			
			jpnAvailable.repaint();
			jpnAvailable.repaint();
			jtbToolbar.repaint();
			jlbAddToExtra.repaint();
			jpmExtra.repaint();
		}

		@Override
		public void mouseReleased(final MouseEvent event) {
//			System.out.println("Released " + arg0);
			
			jtbToolbar.setCursor(null);
			jpnAvailable.setCursor(null);
			jtbAvailable.setCursor(null);
			jpmExtra.setCursor(null);
			jlbAddToExtra.setCursor(null);
			
			if (dragged == null) {
				return ;
			}
			
			pos = -1;
			pos2 = -1;
			pos3 = -1;
			remove = false;
			isDefault = false;
			
			Point extraOnScreen = null;
			try {
				extraOnScreen = jpmExtra.getLocationOnScreen();
			} catch (IllegalComponentStateException ex) {
				// closed during mouse release on popupmenu...
			}
			final Point extraOnScreen1 = extraOnScreen;
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					if (dragged == null) {
						return;
					}
					
					Point extraOnScreen2 = extraOnScreen1; 
					if (extraOnScreen2 == null) {
						try {
							extraOnScreen2 = jpmExtra.getLocationOnScreen();
						} catch (IllegalComponentStateException ex) {
							// closed during mouse release on toolbar...
						}
					}
					if (new Rectangle(jpnAvailable.getLocationOnScreen(), jpnAvailable.getSize()).contains(event.getLocationOnScreen())) {
						jpmExtra.remove(dragged.getMenuComponent());
						jtbToolbar.remove(dragged.getToolBarComponent());
						jtbAvailable.remove(dragged.getToolBarComponent());
						if (!(dragged.getMenuComponent() instanceof MenuSeparator)) {
							dragged.resetComponents();
							jtbAvailable.add(dragged.getToolBarComponent());
						}
						
					} else if (new Rectangle(jtbToolbar.getLocationOnScreen(), jtbToolbar.getSize()).contains(event.getLocationOnScreen())) {
						if (dragged.isMenuOnly()) {
							return;
						}
						Component cDrop = getToolBarComponentAtPosition(event);
						if (dragged.getToolBarComponent() != cDrop) {
							jpmExtra.remove(dragged.getMenuComponent());
							jtbToolbar.remove(dragged.getToolBarComponent());
							jtbAvailable.remove(dragged.getToolBarComponent());
							dragged.resetComponents();
							jtbToolbar.add(dragged.getToolBarComponent(), cDrop==null?-1:jtbToolbar.getComponentIndex(cDrop));
							if (dragged == btnExtra) {
								showExtra();
							}
						}
					} else if (new Rectangle(jlbAddToExtra.getLocationOnScreen(), jlbAddToExtra.getSize()).contains(event.getLocationOnScreen())) {
						if (dragged.isToolbarOnly()) {
							return;
						}
						jpmExtra.remove(dragged.getMenuComponent());
						jtbToolbar.remove(dragged.getToolBarComponent());
						jtbAvailable.remove(dragged.getToolBarComponent());
						dragged.resetComponents();
						jpmExtra.add(dragged.getMenuComponent(), 
								jpmExtra.getComponentCount()-postExtraComponents);
						if (dragged == btnExtra) {
							showExtra();
						}
					} else if (extraOnScreen2 != null && new Rectangle(extraOnScreen2, jpmExtra.getSize()).contains(event.getLocationOnScreen())) {
						if (dragged.isToolbarOnly()) {
							return;
						}
						Component cDrop = getExtraComponentAtPosition(event, extraOnScreen2);
						if (dragged.getMenuComponent() != cDrop) {
							jpmExtra.remove(dragged.getMenuComponent());
							jtbToolbar.remove(dragged.getToolBarComponent());
							jtbAvailable.remove(dragged.getToolBarComponent());
							dragged.resetComponents();
							jpmExtra.add(dragged.getMenuComponent(), cDrop==null?
									jpmExtra.getComponentCount()-postExtraComponents
									:jpmExtra.getComponentIndex(cDrop));
						}
					}
					
					jtbToolbar.validate();
					jpnAvailable.validate();
					jtbAvailable.validate();
					jpmExtra.validate();
					jpmExtra.pack();
					jtbToolbar.repaint();
					jpnAvailable.repaint();
					jtbAvailable.repaint();
					jlbAddToExtra.repaint();
					jpmExtra.repaint();
					
					dragged = null;
					
					UIUtils.getWindowForComponent(jpnEditor).pack();
				}
			});
		}

		@Override
		public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
			showExtra();
		}

		@Override 
		public void popupMenuCanceled(PopupMenuEvent e) {
			showExtra();
		}

		@Override
		public void menuDragMouseDragged(MenuDragMouseEvent e) {
//			System.out.println("MenuDragged " + e);
			mouseDragged(e);
		}

		@Override
		public void menuDragMouseReleased(MenuDragMouseEvent e) {
//			System.out.println("MenuReleased " + e);
			mouseReleased(e);
		}
		
		@Override
		public void ancestorMoved(AncestorEvent event) {
			showExtra();
		}
		
		@Override
		public void componentResized(ComponentEvent e) {
			UIUtils.getWindowForComponent(jpnEditor).pack();
		}
		
		public boolean isDefault() {
			return isDefault;
		}
		
		@Override public void mouseMoved(MouseEvent event) {}
		@Override public void mouseClicked(MouseEvent arg0) {}
		@Override public void mouseEntered(MouseEvent arg0) {}
		@Override public void mouseExited(MouseEvent arg0) {}
		@Override public void mousePressed(MouseEvent arg0) {}
		@Override public void menuDragMouseEntered(MenuDragMouseEvent e) {}
		@Override public void menuDragMouseExited(MenuDragMouseEvent e) {}
		@Override public void popupMenuWillBecomeVisible(PopupMenuEvent e) {}
		@Override public void ancestorRemoved(AncestorEvent event) {}
		@Override public void ancestorAdded(AncestorEvent event) {}
		@Override public void componentMoved(ComponentEvent e) {}
		@Override public void componentShown(ComponentEvent e) {}
		@Override public void componentHidden(ComponentEvent e) {}
	}