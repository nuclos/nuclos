//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.ui.dnd;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

import org.nuclos.client.common.Utils;
import org.nuclos.client.masterdata.datatransfer.MasterDataVORow;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.client.ui.util.CommonTransferable.CommonDataFlavor;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 
 * Transfer Handler for {@link SubFormTable} row
 * 
 * @author Moritz Neuäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class TableRowCopyTransferHandler extends TransferHandler {

	private static final long serialVersionUID = 1L;

	final static Logger LOG = LoggerFactory.getLogger(TableRowCopyTransferHandler.class);
	
	/**
	 * Row Transferable
	 *
	 */
	public static class RowTransferable implements Transferable, Serializable {

		/**
		 * DataFlavor
		 */
		private static CommonDataFlavor<RowTransferable> SUBFORM_TABLE_ROW_FLAVOR = new CommonDataFlavor<RowTransferable>(
				RowTransferable.class, "SubFormTable Row Transferable");
		
		private final Map<Integer, MasterDataVORow<CollectableField>> mpColumnsByRow;
		private final UID entity;
		private final DataFlavor[] arrSupportedDataFlavors;
		private final SubFormTable sfTable;

		/**
		 * create new RowTransferable
		 * 
		 * @param entity		entity name
		 */
		public RowTransferable(final UID entity, final SubFormTable sfTable) {
			this.entity = entity;
			this.mpColumnsByRow = new HashMap<Integer, MasterDataVORow<CollectableField>>();
			this.arrSupportedDataFlavors = new DataFlavor[]{SUBFORM_TABLE_ROW_FLAVOR};
			this.sfTable = sfTable;
		}
		
		public static DataFlavor getDataFlavor() {
			return SUBFORM_TABLE_ROW_FLAVOR;
		}
		
		
		public void newRow(final Integer row, final List<CollectableField> lstFields) {
			final MasterDataVORow<CollectableField> mdvoRow = new MasterDataVORow<CollectableField>(
					entity, new MasterDataVORow.Row<CollectableField>(lstFields));
			this.mpColumnsByRow.put(row, mdvoRow);
		}
		
		public UID getEntity() {
			return this.entity;
		}
		
		public SubFormTable getSubFormTableSrc() {
			return this.sfTable;
		}
		
		public Map<Integer, MasterDataVORow<CollectableField>> getRows() {
			return Collections.unmodifiableMap(mpColumnsByRow);
		}

		@Override
		public DataFlavor[] getTransferDataFlavors() {
			return arrSupportedDataFlavors;
		}

		@Override
		public boolean isDataFlavorSupported(DataFlavor flavor) {
			for (DataFlavor dataFlavor : arrSupportedDataFlavors) {
				if (dataFlavor.equals(flavor)) {
					return true;
				}
			}
			return false;
		}

		@Override
		public Object getTransferData(DataFlavor flavor)
				throws UnsupportedFlavorException, IOException {
			return new TransferableWrapper<SubFormTable, RowTransferable>() {

				@Override
				public SubFormTable getCustomObject() {
					return (SubFormTable) RowTransferable.this.getSubFormTableSrc();
				}

				@Override
				public RowTransferable getTransferable() {
					return RowTransferable.this;
				}

			};
		}

	};

	private final SubFormTable srcTable;

	public TableRowCopyTransferHandler(final SubFormTable srcTable) {
		this.srcTable = srcTable;
	}

	@Override
	protected Transferable createTransferable(final JComponent component) {
		// expect the source table
		//assert component == srcTable;

		final SubFormTableModel model = (SubFormTableModel) srcTable.getModel();
		final RowTransferable transferable = new RowTransferable(model.getBaseEntityUid(), getSrcTable());

		int[] arrSelectedRows = srcTable.getSelectedRows();

		for (int iRow : arrSelectedRows) {
			assert iRow > -1;
			// catch the whole row
			transferable.newRow(new Integer(iRow), Utils.copyRow(iRow, model));

		}
		LOG.debug("created transferable with {} rows for copy", transferable.getRows().size());
		return transferable;
	}
	
	@Override
	public int getSourceActions(final JComponent c) {
		/*
		int result = NONE;
		if (c == srcTable) {
			result = COPY;
		}
		return result;

		*/
		return COPY_OR_MOVE;
		
	}

	public SubFormTable getSrcTable() {
		return srcTable;
	}
	
	
}
