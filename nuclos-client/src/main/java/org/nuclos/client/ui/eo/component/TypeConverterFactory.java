//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.eo.component;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;

import org.apache.commons.lang.StringUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.ui.eo.mvc.ITypeConverter;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;
import org.springframework.beans.factory.annotation.Autowired;

public class TypeConverterFactory {
	
	private static TypeConverterFactory INSTANCE;
	
	// Spring injection
	
	@Autowired
	private MetaProvider metaProvider;
	
	@Autowired
	private SpringLocaleDelegate localeDelegate;
	
	// end of Spring injection
	
	TypeConverterFactory() {
		INSTANCE = this;
	}
	
	/**
	 * @deprecated Use spring injection instead.
	 */
	public static TypeConverterFactory getInstance() {
		return INSTANCE;
	}
	
	public <S,T> ITypeConverter<S, T> getTypeConverterForField(UID field) {
		final FieldMeta<?> meta = metaProvider.getEntityField(field);		
		final String type = meta.getDataType();
		final ITypeConverter<S, T> result;
		if ("java.lang.String".equals(type)) {
			result = (ITypeConverter<S, T>) new TrivialTypeConverter<S>();
		} else if ("java.lang.Double".equals(type)) {
			final NumberFormat nf = getNumberFormat(meta);
			result = (ITypeConverter<S, T>) new DoubleConverter(nf);
		} else if ("java.lang.Integer".equals(type)) {
			final NumberFormat nf = getNumberFormat(meta);
			result = (ITypeConverter<S, T>) new IntegerConverter(nf);
		} else if ("java.lang.Long".equals(type)) {
			final NumberFormat nf = getNumberFormat(meta);
			result = (ITypeConverter<S, T>) new LongConverter(nf);
		} else {
			throw new IllegalArgumentException(type);
		}
		return result;
	}
	
	private NumberFormat getNumberFormat(FieldMeta<?> field) {
		final String numberPattern = StringUtils.trimToNull(field.getFormatOutput());
		if (numberPattern == null) {
			return null;
		}
		return new DecimalFormat(numberPattern, new DecimalFormatSymbols(localeDelegate.getLocale()));
	}
	
	private static class TrivialTypeConverter<T> implements ITypeConverter<T, T> {
		
		@Override
		public T fromComponentToModel(T value) {
			if (value == null || "".equals(value)) {
				return null;
			}
			return value;
		}

		@Override
		public T fromModelToComponent(T value) {
			if (value == null) {
				return null;
			}
			return value;
		}
		
	}

	private static class StringToTConverter<T> implements ITypeConverter<String, T> {
		
		private final Class<T> clazz;
		
		private StringToTConverter(Class<T> clazz) {
			this.clazz = clazz;
		}

		@Override
		public T fromComponentToModel(String value) {
			value = StringUtils.trimToNull(value);
			if (value == null) {
				return null;
			}
			final T result;
			if (clazz.isAssignableFrom(String.class)) {
				result = (T) value;
			} else if (Enum.class.isAssignableFrom(clazz)){
				result = (T) Enum.valueOf((Class<Enum>) clazz, value);
			} else {
				result = (T) value;
			}
			return result;
		}

		@Override
		public String fromModelToComponent(T value) {
			if (value == null) {
				return null;
			}
			final String result;
			if (String.class.isAssignableFrom(clazz)) {
				result = (String) value;
			} else {
				// generic toString
				result = value.toString();
			}
			return result;
		}
		
	}

	private static class DoubleConverter implements ITypeConverter<String, Double> {
		
		private final NumberFormat format;
		
		private DoubleConverter(NumberFormat format) {
			this.format = format;
		}

		@Override
		public Double fromComponentToModel(String value) throws ParseException {
			value = StringUtils.trimToNull(value);
			if (value == null) {
				return null;
			}
			final Double result;
			if (format == null) {
				result = Double.valueOf(value);
			} else {
				result = (Double) format.parse(value).doubleValue();
			}
			return result;
		}

		@Override
		public String fromModelToComponent(Double value) {
			if (value == null) {
				return "";
			}
			final String result;
			if (format == null) {
				result = value.toString();
			} else {
				result = format.format(value);
			}
			return result;
		}
		
	}
	
	private static class IntegerConverter implements ITypeConverter<String, Integer> {

		private final NumberFormat format;
		
		private IntegerConverter(NumberFormat format) {
			this.format = format;
		}
		
		@Override
		public Integer fromComponentToModel(String value) throws ParseException {
			value = StringUtils.trimToNull(value);
			if (value == null) {
				return null;
			}
			final Integer result;
			if (format == null) {
				result = Integer.valueOf(value);
			} else {
				result = (Integer) format.parse(value).intValue();
			}
			return result;
		}

		@Override
		public String fromModelToComponent(Integer value) {
			if (value == null) {
				return "";
			}
			return value.toString();
		}
		
	}
	
	private static class LongConverter implements ITypeConverter<String, Long> {

		private final NumberFormat format;
		
		private LongConverter(NumberFormat format) {
			this.format = format;
		}
		
		@Override
		public Long fromComponentToModel(String value) throws ParseException {
			value = StringUtils.trimToNull(value);
			if (value == null) {
				return null;
			}
			final Long result;
			if (format == null) {
				result = Long.valueOf(value);
			} else {
				result = (Long) format.parse(value).longValue();
			}
			return result;
		}

		@Override
		public String fromModelToComponent(Long value) {
			if (value == null) {
				return "";
			}
			return value.toString();
		}
		
	}
	
}
