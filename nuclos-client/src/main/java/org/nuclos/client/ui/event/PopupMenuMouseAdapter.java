//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.event;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
/**
 * @author Thomas Pasch (javadoc, deprecation)
 * @deprecated Superseded by the {@link TablePopupMenuEvent} infrastructure,
 * 		also see {@link Mouse2TableHeaderPopupMenuAdapter}.
 */

public abstract class PopupMenuMouseAdapter implements MouseListener, IPopupListener {
	
	public PopupMenuMouseAdapter() {
	}

	boolean checkAndDo(MouseEvent e) {
		if (e.isPopupTrigger()) {
			return doPopup(e, null);
		}
		return false;
	}

	@Override
    public final void mouseClicked(MouseEvent e) {
		checkAndDo(e);
    }

	@Override
    public final void mousePressed(MouseEvent e) {
		checkAndDo(e);
    }

	@Override
    public final void mouseReleased(MouseEvent e) {
		checkAndDo(e);
    }

	@Override
    public final void mouseEntered(MouseEvent e) {
    }

	@Override
    public final void mouseExited(MouseEvent e) {
    }
}
