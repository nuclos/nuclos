package org.nuclos.client.ui.layoutml;

import java.util.Map;

import org.nuclos.common.UID;

public interface IField2Tag {
	Map<UID, String> mpField2Tag();
}
