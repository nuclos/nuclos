package org.nuclos.client.ui.layoutml;

import org.nuclos.common.UID;

class TargetComponentAction {

	private final UID sTargetComponentName;

	private TargetComponentAction(UID sTargetComponentName) {
		this.sTargetComponentName = sTargetComponentName;
	}

	public UID getTargetComponentName() {
		return sTargetComponentName;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("TargetComponentAction[");
		result.append("target=");
		result.append(sTargetComponentName);
		result.append("]");
		return result.toString();
	}
	
	static class ClearAction extends TargetComponentAction {

		ClearAction(UID sTargetComponentName) {
			super(sTargetComponentName);
		}

		@Override
		public String toString() {
			final StringBuilder result = new StringBuilder();
			result.append("ClearAction[");
			result.append("target=");
			result.append(getTargetComponentName());
			result.append("]");
			return result.toString();
		}
	}

	static class ReinitSubformAction extends TargetComponentAction {

		private UID subformEntity;
		ReinitSubformAction(UID sTargetComponentName, UID subformEntity) {
			super(sTargetComponentName);
			this.subformEntity = subformEntity;
		}

		public UID getSubformEntity() {
			return this.subformEntity;
		}
		
		@Override
		public String toString() {
			final StringBuilder result = new StringBuilder();
			result.append("ClearAction[");
			result.append("target=");
			result.append(getTargetComponentName());
			result.append("]");
			return result.toString();
		}
	}
	
	static class TransferLookedUpValueAction extends TargetComponentAction {

		private final UID sSourceFieldName;

		TransferLookedUpValueAction(UID sTargetComponentName, UID sSourceFieldName) {
			super(sTargetComponentName);
			this.sSourceFieldName = sSourceFieldName;
		}

		public UID getSourceFieldName() {
			return sSourceFieldName;
		}

		@Override
		public String toString() {
			final StringBuilder result = new StringBuilder();
			result.append("TransferLookedUpValueAction[");
			result.append("target=");
			result.append(getTargetComponentName());
			result.append("srcField=");
			result.append(sSourceFieldName);
			result.append("]");
			return result.toString();
		}
	}

	static class RefreshValueListAction extends TargetComponentAction {

		final UID sTargetComponentEntityName;
		private final String sParameterNameForSourceComponent;

		RefreshValueListAction(UID sTargetComponentName, UID sTargetComponentEntityName,
				String sParameterNameForSourceComponent) {
			super(sTargetComponentName);
			this.sTargetComponentEntityName = sTargetComponentEntityName;
			this.sParameterNameForSourceComponent = sParameterNameForSourceComponent;
		}

		public UID getTargetComponentEntityName() {
			return sTargetComponentEntityName;
		}

		/**
		 * @return the name of the parameter in the valuelistprovider for the source component.
		 */
		public String getParameterNameForSourceComponent() {
			return sParameterNameForSourceComponent;
		}

		@Override
		public String toString() {
			final StringBuilder result = new StringBuilder();
			result.append("RefreshValueListAction[");
			result.append("target=");
			result.append(getTargetComponentName());
			result.append(",targetEntity=");
			result.append(sTargetComponentEntityName);
			result.append(",parameterNameSrc=");
			result.append(sParameterNameForSourceComponent);
			result.append("]");
			return result.toString();
		}
	}
}
