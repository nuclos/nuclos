package org.nuclos.client.ui.matrix;

import java.util.Comparator;

import org.nuclos.common.FieldMeta;

public class FieldMetaComparator implements Comparator<FieldMeta<?>> {

	@Override
	public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
		
		return o1.getUID().compareTo(o2.getUID());
	}

}
