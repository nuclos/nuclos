package org.nuclos.client.ui.matrix;

import java.util.Comparator;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;

public class UIDComparator implements Comparator<UID> {

	@Override
	public int compare(UID o1, UID o2) {
		FieldMeta<?> f1 = MetaProvider.getInstance().getEntityField(o1);
		FieldMeta<?> f2 = MetaProvider.getInstance().getEntityField(o2);
		String s1 = SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(f1);
		String s2 = SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(f2);
		
		return s1.compareTo(s2);
	}

}
