package org.nuclos.client.ui.news;

import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.model.AbstractListTableModel;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.schema.rest.News;
import org.springframework.stereotype.Component;

@Component
public class NewsOverviewTableModel extends AbstractListTableModel<News> {
	private static final Logger LOG = Logger.getLogger(NewsOverviewTableModel.class);

	private final SpringLocaleDelegate sld;
	private final NewsBean newsBean;

	NewsOverviewTableModel(
			final SpringLocaleDelegate sld,
			final NewsBean newsBean
	) {
		this.sld = sld;
		this.newsBean = newsBean;
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex >= getRowCount()) {
			return null;
		}

		News news = this.newsBean.getNews().get(rowIndex);

		final Object result;
		switch (columnIndex) {
			case 0:
				result = news.getName();
				break;
			case 1:
				result = news.getTitle();
				break;
			case 2:
				result = news.getRevision();
				break;
			case 3:
				result = newsBean.isUnread(news);
				break;
			default:
				throw new IllegalArgumentException();
		}
		return result;
	}

	@Override
	public String getColumnName(int column) {
		final String result;
		switch (column) {
			case 0:
				result = sld.getMessage("news.column.1", "news.column.1");
				break;
			case 1:
				result = sld.getMessage("news.column.2", "news.column.2");
				break;
			case 2:
				result = sld.getMessage("news.column.3", "news.column.3");
				break;
			case 3:
				result = sld.getMessage("news.column.4", "news.column.4");
				break;
			default:
				throw new IllegalStateException();
		}
		return result;
	}

	@Override
	public Class<?> getColumnClass(int col) {
		if (col == 3) {
			return Boolean.class;
		}
		return super.getColumnClass(col);
	}

	@Override
	public int getRowCount() {
		return getRows().size();
	}

	@Override
	protected List<News> getRows() {
		return newsBean.getNews();
	}

}
