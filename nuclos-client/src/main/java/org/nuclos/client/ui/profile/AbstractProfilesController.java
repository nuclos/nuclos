//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.profile;

import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.Actions;
import org.nuclos.common.UID;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common.profile.ProfileItem;
import org.nuclos.common.profile.PublishType;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.common2.exception.ProfileException;
import org.nuclos.common2.exception.WorkspaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * ProfilesController
 * 
 *  NUCLOS-1479 Profiles
 *
 */
public abstract class AbstractProfilesController {

	private final static Logger LOG = LoggerFactory.getLogger(AbstractProfilesController.class);
	
	private final TablePreferencesManager tblprefManager;
	private final ProfileSupport profileSupport;

	private final ProfileModel profileModel;
	
	private final UID targetEntity;
	
	public AbstractProfilesController(final TablePreferencesManager tblprefManager, ProfileSupport profileSupport, UID targetEntity) {
		this.tblprefManager = tblprefManager;
		this.profileSupport = profileSupport;
		this.profileModel = new ProfileModel(tblprefManager);
		this.targetEntity = targetEntity;
	}

	public UID getTargetEntity() {
		return targetEntity;
	}
	
	public ProfileModel getModel() {
		return profileModel;
	}
	
	ProfileSupport getProfileSupport() {
		return profileSupport;
	}

	public void createNewProfile(final String name) throws ProfileException {
		if (!isCreatePersonalProfileAllowed()) {
			LOG.error("create new personal profile " + name + " failed. No Permission.");
			throw new ProfileException(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfilesController.1", "Keine Berechtigung!"));
		}
		
		ProfileItem currentActiveProfile= getModel().getSelectedProfile();
		if (null == currentActiveProfile) {
			currentActiveProfile = new ProfileItem(new TablePreferences(tblprefManager.getType(), new UID()));
		}
		final TablePreferences newProfile = new TablePreferences(tblprefManager.getType(), null);
		newProfile.clearAndImport(currentActiveProfile.getPreferences());
		newProfile.setUID(new UID());
		newProfile.setName(name);
		newProfile.setUserdefinedName(true);
		newProfile.setShared(false);
		newProfile.setCustomized(false);
		
		final ProfileItem profile = new ProfileItem(newProfile);
		
			// add to model
		getModel().addPersonalProfile(profile);

		LOG.info("created new personal profile " + name);
		
		switchToProfile(profile);
	}
	
	public abstract void publishProfile(final ProfileItem profile, final PublishType publishType) throws CommonPermissionException, PreferencesException;
	
	public abstract void resetSelectedProfile() throws CommonPermissionException, WorkspaceException;

	public void removeSelectedProfile() throws CommonPermissionException, PreferencesException {
		ProfileItem profile = getModel().getSelectedProfile();
		if (!isRemoveAllowed(profile)) {
			LOG.error("remove profile " + getModel().getSelectedProfile() + " failed. No Permission.");
			throw new PreferencesException(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfilesController.1", "Keine Berechtigung!"));
		}
		if (!getModel().removeProfile(profile)) {
			throw new PreferencesException(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfilesController.2", "Das Profile konnte nicht gelöscht werden!"));
		}
		if (profile.isShared()) {
			publishProfile(profile, PublishType.REMOVE);
		}
		UIUtils.invokeOnDispatchThread(new Runnable() {
			
			@Override
			public void run() {
				profileSupport.updateBySelectedProfile();	
			}
		});
		LOG.info("removed profile " + getModel().getSelectedProfile());
	}
	
	public void renameSelectedProfile(String name) throws CommonPermissionException, PreferencesException {
		final ProfileItem profile = getModel().getSelectedProfile();
		if (!isRenameAllowed(profile)) {
			LOG.error("rename profile " + getModel().getSelectedProfile() + " failed. No Permission.");
			throw new PreferencesException(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfilesController.1", "Keine Berechtigung!"));
		}
		final String oldName = profile.getName();
		profile.getPreferences().setName(name);
		getModel().updateProfile(profile);
		UIUtils.invokeOnDispatchThread(new Runnable() {
			
			@Override
			public void run() {
				profileSupport.updateBySelectedProfile();
				
			}
		});
		LOG.info("renamed profile " + oldName + " => " + name);
	}
	
	public abstract void switchToProfile(final ProfileItem profile);
	
	public TablePreferencesManager getTablePreferencesManager() {
		return this.tblprefManager;
	}
	
	private final boolean isCustomizeEntityAndSubFormColumnsAllowed() {
		return SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS);
	}
	
	public boolean isCreatePersonalProfileAllowed() {
		return isCustomizeEntityAndSubFormColumnsAllowed();
	}
	
	public boolean isRemoveAllowed(ProfileItem profile) {
		boolean result = false;
		if (!profile.getPreferences().isShared()) {
			// always allow deletion of personal preferences
			result = true;
		} else {
			result = isCustomizeEntityAndSubFormColumnsAllowed() && isPublishAllowed(profile);
		}
		return result;
	}
	
	public boolean isPublishAllowed(final ProfileItem profile) {
		//return SecurityCache.getInstance().isActionAllowed(Actions.ACTION_SHARE_PREFERENCES)
		//		&& SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS);
		// TODO sharing not implemented yet
		return false;
	}
	
	public boolean isResetPossible(final ProfileItem profile) {
		if (profile.getPreferences().isShared()) {
			if (isCustomizeEntityAndSubFormColumnsAllowed()) {
				return profile.getPreferences().isCustomized();
			} else {
				// no customized check! If the user has no right, it is never customized.
				return true;
			}
		}
		return false;
	}

	public boolean isRenameAllowed(ProfileItem profile) {
		boolean result = false;
		if (!profile.getPreferences().isShared()) {
			result = isCustomizeEntityAndSubFormColumnsAllowed();
		} else {
			result = isCustomizeEntityAndSubFormColumnsAllowed() && isPublishAllowed(profile);
		}
		return result;
	}
	
	public boolean isAvailable() {
		// for selection
		boolean result = getModel().getPersonalProfiles().size() + getModel().getPublicProfiles().size() > 1;
		if (isCustomizeEntityAndSubFormColumnsAllowed()) {
			// for share
			result = true;
		} else {
			if (getModel().getPublicProfiles().size() > 0) {
				// for reset
				result = true;
			}
		}
		return result;
	}
}
