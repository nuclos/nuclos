package org.nuclos.client.ui.profile;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.nuclos.common.ProfileUtils;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common.profile.ProfileItem;

public class ProfileModel {

	private final TablePreferencesManager tblprefManager;
	
	public ProfileModel(final TablePreferencesManager tblprefManager) {
		this.tblprefManager = tblprefManager;
	}

	public ProfileItem getSelectedProfile() {		
		final TablePreferences tp = tblprefManager.getSelected();
		return new ProfileItem(tp);
	}
	
	public void addPersonalProfile(final ProfileItem profile) {
		tblprefManager.insert(profile.getPreferences());
	}
	
	public boolean removePersonalProfile(final ProfileItem profile) {
		final TablePreferences tp = ProfileUtils.findInProfiles(tblprefManager.getPrivate(), profile.getPreferences().getUID());
		if (null != tp) {
			if (tblprefManager.delete(tp)) {
				return true;
			}
		}
		return false;
	}
	
	public List<ProfileItem> getPersonalProfiles() {
		return CollectionUtils.transform(tblprefManager.getPrivate(),
				new Transformer<TablePreferences, ProfileItem>() {
					@Override
					public ProfileItem transform(TablePreferences tp) {
						return new ProfileItem(tp);
					}
				});
	}
	
	public boolean removeProfile(final ProfileItem profile) {
		if (tblprefManager.delete(profile.getPreferences())) {
			return true;
		}
		return false;
	}

	public void updateProfile(final ProfileItem profile) {
		tblprefManager.update(profile.getPreferences());
	}
	
	public List<ProfileItem> getPublicProfiles() {
		return CollectionUtils.transform(tblprefManager.getShared(),
				new Transformer<TablePreferences, ProfileItem>() {
					@Override
					public ProfileItem transform(TablePreferences tp) {
						return new ProfileItem(tp);
					}
				});
	}
	
	public Set<String> getAllProfilesNames() {
		final Set<String> result = new TreeSet<String>();
		for (TablePreferences tp: tblprefManager.getShared()) {
			final String name = tp.getName();
			if (name != null) {
				result.add(name);
			}
		}
		for (TablePreferences tp: tblprefManager.getPrivate()) {
			final String name = tp.getName();
			if (name != null) {
				result.add(name);
			}
		}
		return result;
	}
}
