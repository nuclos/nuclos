//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.profile;

import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.UID;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common.profile.ProfileItem;
import org.nuclos.common.profile.PublishType;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.PreferencesException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * ProfilesController
 * 
 *  NUCLOS-1479 Profiles
 *
 */
public final class SubformProfilesController extends AbstractProfilesController {

	private final static Logger LOG = LoggerFactory.getLogger(SubformProfilesController.class);
	
	private final DetailsSubFormController<?,?> dsfc;
	
	public SubformProfilesController(final TablePreferencesManager tblprefManager, ProfileSupport profileSupport, DetailsSubFormController<?,?> dsfc) {
		super(tblprefManager, profileSupport, dsfc.getSubForm().getEntityUID());
		this.dsfc = dsfc;
	}

	@Override
	public void publishProfile(final ProfileItem profile, final PublishType publishType) throws PreferencesException {
		if (!isPublishAllowed(profile)) {
			LOG.error("publish profile " + profile + " failed. No Permission.");
			throw new PreferencesException(SpringLocaleDelegate.getInstance().getMessage("Workspace.Profile.ProfilesController.1", "Keine Berechtigung!"));
		}
		LOG.info("published profile " + profile);
		final UID mainEntity = dsfc.getCollectController().getEntityUid();
		getTablePreferencesManager().share(profile.getPreferences(), publishType);
	}
	
	public void resetSelectedProfile() throws CommonPermissionException {
		if (!isResetPossible(getModel().getSelectedProfile())) {
			LOG.error("restore profile " + getModel().getSelectedProfile() + " failed. No Permission.");
			throw new CommonPermissionException(SpringLocaleDelegate.getInstance().getMessage(
					"Workspace.Profile.ProfilesController.1", "Keine Berechtigung!"));
		}

		getTablePreferencesManager().reset(getModel().getSelectedProfile().getPreferences());
		switchToProfile(getModel().getSelectedProfile());
		LOG.info("restored profile " + getModel().getSelectedProfile());
	}

	public void switchToProfile(final ProfileItem profile) {
		getTablePreferencesManager().select(profile.getPreferences());
		UIUtils.invokeOnDispatchThread(new Runnable() {
			
			@Override
			public void run() {
				getProfileSupport().updateBySelectedProfile();	
			}
		});
	}
	
}
