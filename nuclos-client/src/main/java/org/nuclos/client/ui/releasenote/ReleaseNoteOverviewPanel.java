//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.releasenote;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;

import org.jdesktop.swingx.JXTable;
import org.nuclos.client.ui.NuclosToolBar;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.SpringLocaleDelegate;

public class ReleaseNoteOverviewPanel extends JPanel {
	
	private final ReleaseNoteOverviewTableModel model;
	
	private final JXTable table;
	
	private final TableRowSorter sorter;
	
	private final SpringLocaleDelegate sld;
	
	public ReleaseNoteOverviewPanel(ReleaseNoteOverviewTableModel model) {
		if (model == null) {
			throw new NullPointerException();
		}
		this.model = model;
		final Frame frame = UIUtils.getFrameForComponent(this);
		
		sld = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
		sorter = new TableRowSorter(model);
		table = new JXTable(model);
		table.setFillsViewportHeight(true);
		table.setRowSorter(sorter);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setEditable(false);
		table.setSortable(true);
		table.addMouseListener(new MouseAdapter() {			
			@Override
			public void mouseReleased(MouseEvent event) {
				final Point p = event.getPoint();
				if (!event.isPopupTrigger()) {
					final int row = table.rowAtPoint(p);
					// final int column = table.convertColumnIndexToModel(table.columnAtPoint(p));
					if (row >= 0) {
						final Component comp = UIUtils.getTabOrWindowForComponent(ReleaseNoteOverviewPanel.this);
						final JDialog dialog = new JDialog(frame, true);
						
						// dialog.setLocationRelativeTo(comp);
						final Point ul = new Point(
								(int) comp.getLocation().getX() + 200, (int) comp.getLocation().getY() + 100);
						dialog.setLocation(ul);
						dialog.setLayout(new BorderLayout());
						dialog.add(new ReleaseNoteDetailsPanel(
								ReleaseNoteOverviewPanel.this.model, table.getSelectionModel(), sorter, row));
						
						final Dimension wh = new Dimension(
								(int) comp.getSize().getWidth() - 200, (int) comp.getSize().getHeight() - 200);
						dialog.setSize(wh);
						dialog.setMaximumSize(comp.getMaximumSize());
						dialog.setPreferredSize(wh);
						
						dialog.pack();
						dialog.setVisible(true);
					}
				}
			}
		});
		
		final TableColumnModel tcm = table.getColumnModel();
		int i = 0;
		for (TableColumn tc: CollectionUtils.iterableEnum(tcm.getColumns())) {
			switch (i) {
			case 0:
				tc.setPreferredWidth(300);
				break;
			case 1:
				tc.setPreferredWidth(100);
				break;
			case 2:
				tc.setPreferredWidth(100);
				break;
			case 3:
				tc.setPreferredWidth(150);
				break;
			default:
				tc.setMinWidth(100);
				tc.setPreferredWidth(200);
				tc.setMaxWidth(300);
			}
			++i;
		}
		
		TableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayoutBuilder.FILL, 500.0, TableLayoutBuilder.FILL);
		tbllay.newRow(20.0).addFullSpan(new NuclosToolBar());
		tbllay.newRow(TableLayoutBuilder.FILL).skip().add(new JScrollPane(table));
		tbllay.newRow(10.0);
	}

	private JPanel getInfoLabel() {
		JPanel pnl = new JPanel();
		pnl.setLayout(new BorderLayout());		
		pnl.setPreferredSize(new Dimension(0, 40));
		final String label = sld.getMessage("nuclet.release.note.overview.label", "nuclet.release.note.overview.label");
		pnl.add(new JLabel(label), BorderLayout.CENTER);
		return pnl;
	}
	
}
