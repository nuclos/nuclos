//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.ui.resplan;

import java.util.List;

import org.jdesktop.swingx.renderer.ComponentProvider;
import org.nuclos.client.customcomp.resplan.ClientPlanElement;
import org.nuclos.client.customcomp.resplan.EntryWPElem;
import org.nuclos.client.ui.resplan.JResPlanComponent.JMilestone;
import org.nuclos.client.ui.resplan.JResPlanComponent.JRelation;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.interval.Interval;

/**
 * @param <R> resource
 * @param <T> time unit
 * @param <L> relation
 */
public interface ResPlanModel<PK, R, T extends Comparable<? super T>, E, L> {

	/**
	 * Returns the concrete entry type of this model. 
	 */
	Class getEntryType();

	/**
	 * Retrieves all resources.
	 */
	List<? extends R> getResources();

	/**
	 * Retrieves all entries to the given resource.
	 */
	List<EntryWPElem<R>> getWPElements(R resource);
	
	List<? extends E> getEntries(R resource);
	
	/**
	 * Retrieves the interval for the given entry.
	 */
	Interval<T> getInterval(E entry, ClientPlanElement<PK,R,Collectable<PK>> p);

	/**
	 * Creates a new entry for the given resource and interval.
	 * Note that ehis method returns intentionally void. It is up to the model to decide
	 * what to do and to propagte the changes (if any) as events.
	 */
	void createEntry(R resource, Interval<T> interval, Object value, ClientPlanElement<PK,R,Collectable<PK>> pElement);
	
	/**
	 * Updates the given entry.
	 */
	void updateEntry(E entry, R resource, Interval<T> interval, ClientPlanElement<PK,R,Collectable<PK>> pElement);

	/**
	 * Removes the given entry.
	 */
	void removeEntry(E entry, ClientPlanElement<PK,R,Collectable<PK>> pElement);
	
	void createRelation(E entryFrom, E entryTo, ClientPlanElement<PK,R,Collectable<PK>> pFrom, ClientPlanElement<PK,R,Collectable<PK>> pTo);
	
	void removeRelation(L relation);

	Object getEntryId(E entry);
	
	Object getRelationId(L relation);
	
	boolean isCreateEntryAllowed(E entry, ClientPlanElement<PK,R,Collectable<PK>> pElement);

	boolean isUpdateEntryTimeAllowed(E entry, ClientPlanElement<PK,R,Collectable<PK>> pElement);
	
	boolean isUpdateEntryResourceAllowed(E entry, ClientPlanElement<PK,R,Collectable<PK>> pElement);
	
	boolean isRemoveEntryAllowed(E entry, ClientPlanElement<PK,R,Collectable<PK>> pElement);
	
	boolean isCreateRelationAllowed();

	boolean isUpdateRelationAllowed(L relation);
	
	boolean isRemoveRelationAllowed(L relation);
	
	List<? extends L> getAllRelations();
	
	List<? extends L> getRelations(E entry);
	
	Object getRelationFromId(L relation);
	
	Object getRelationToId(L relation);
	
	R getResourceFromEntry(E entry, ClientPlanElement<PK,R,Collectable<PK>> p);
	
	void addResPlanModelListener(ResPlanModelListener listener);
	
	void removeResPlanModelListener(ResPlanModelListener listener);

	ResPlanModelListener[] getResPlanModelListeners();
	
	ComponentProvider<?> getComponentProvider(ClientPlanElement<PK,R,Collectable<PK>> p);
	
	JMilestone getMileStoneRenderer(ClientPlanElement<PK,R,Collectable<PK>> p);
	
	JRelation getRelationRenderer(L relation);
	
	void notifyInvalidHeader();
}
