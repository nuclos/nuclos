//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.ui.resplan;

import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.NuclosCollectableEntityProvider;
import org.nuclos.client.customcomp.resplan.ClientPlanElement;
import org.nuclos.client.dal.DalSupportForGO;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.genericobject.CollectableGenericObject;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.ui.resplan.JResPlanComponent.CellView;
import org.nuclos.client.ui.util.CommonTransferable;
import org.nuclos.client.ui.util.CommonTransferable.CommonDataFlavor;
import org.nuclos.common.DependentSelection;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.collect.collectable.AbstractCollectable;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.interval.Interval;
import org.nuclos.server.genericobject.context.GenerationContext;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class ResPlanTransferHandler<PK,R, T extends Comparable<? super T>, E, L> extends TransferHandler {
	
	private CellView<PK, R, T, E, L> pressedCellView = null;
	private JResPlanComponent<PK, R, T, E, L> resPlanComponent;

	public static class EntryWrapper<PK2,R2,E2,C2 extends Collectable<PK2>> implements Serializable {
		
		private final E2 entry;
		private final ClientPlanElement<PK2,R2,C2> planElement;
		
		public EntryWrapper(E2 entry, ClientPlanElement<PK2,R2,C2> planElement) {
			this.entry = entry;
			this.planElement = planElement;
		}
		
		public E2 unwrap(Class<E2> clazz) {
			if (clazz.isInstance(entry)) {
				return (E2) entry;
			}
			return null;
		}
		
		public ClientPlanElement<PK2,R2,C2> getPlanElement() {
			return planElement;
		}
	}
	
	public static CommonDataFlavor<EntryWrapper> RESPLAN_ENTRY_FLAVOR = new CommonDataFlavor<EntryWrapper>(
			EntryWrapper.class, "Resource plan entry");
	
	public static CommonDataFlavor<Pair<EntryWrapper, List<EntryWrapper>>> RESPLAN_ENTRY_LIST_FLAVOR = new CommonDataFlavor<Pair<EntryWrapper, List<EntryWrapper>>>(
			getListDataFlavorClass(), "Resource plan entry");
	
	private static Class<Pair<EntryWrapper, List<EntryWrapper>>> getListDataFlavorClass() {
		Pair<EntryWrapper, List<EntryWrapper>> pair = new Pair<EntryWrapper, List<EntryWrapper>>();
		return (Class<Pair<EntryWrapper, List<EntryWrapper>>>) pair.getClass();
	}

	public ResPlanTransferHandler(JResPlanComponent resPlanComponent) {
		this.resPlanComponent = resPlanComponent;
	}
	
	@Override
	public void exportAsDrag(JComponent comp, InputEvent e, int action) {
		super.exportAsDrag(comp, e, action);
	}
	
	@Override
	public int getSourceActions(JComponent c) {
		return MOVE | COPY;
	}
	
	@Override
	protected Transferable createTransferable(JComponent c) {
		if (c instanceof JResPlanComponent<?, ?, ?, ?, ?>) {
			return createResPlanTransferable((JResPlanComponent) c);
		} else {
			return super.createTransferable(c);
		}
	}
	
	protected Transferable createResPlanTransferable(JResPlanComponent<PK, R, T, E, L> resPlan) {
		boolean mileStone = false;
		if (resPlan.getSelectedCellViews().size() == 1) {
			CellView<PK,R,T,E,L> cellView = CollectionUtils.getFirst(resPlan.getSelectedCellViews());
			if (cellView != null) {
				// TODO_RESPLAN: should interval/duration part of the wrapper ???
	//			Interval<T> interval = resPlan.getModel().getInterval(entry);
	//			long duration = resPlan.getTimeModel().getDuration(interval.getStart(), interval.getEnd());
				EntryWrapper<PK,R,E,Collectable<PK>> wrapper = new EntryWrapper<PK,R,E,Collectable<PK>>(cellView.getEntry(), cellView.getPlanElement());
				CommonTransferable<?> transferable = new CommonTransferable<EntryWrapper>(RESPLAN_ENTRY_FLAVOR, wrapper);	;
				transferable.registerToStringFlavor();
				return transferable;
			}
		} else if (resPlan.getSelectedCellViews().size() > 1){
			List<CellView<PK,R,T,E,L>> lstCellViews = resPlan.getSelectedCellViews();
			
			Pair<EntryWrapper, List<EntryWrapper>> pair= new Pair<>();
			pair.setX(new EntryWrapper<PK,R,E,Collectable<PK>>(resPlan.getDraggedCellView().getEntry(), resPlan.getDraggedCellView().getPlanElement()));
			List<EntryWrapper> lstEntries = new ArrayList<>();
			for (CellView<PK,R,T,E,L> cellView : lstCellViews) {
				if (cellView != null) {
					// TODO_RESPLAN: should interval/duration part of the wrapper ???
		//			Interval<T> interval = resPlan.getModel().getInterval(entry);
		//			long duration = resPlan.getTimeModel().getDuration(interval.getStart(), interval.getEnd());
					EntryWrapper<PK,R,E,Collectable<PK>> wrapper = new EntryWrapper<PK,R,E,Collectable<PK>>(cellView.getEntry(), cellView.getPlanElement());
					lstEntries.add(wrapper);
				}
			}
			pair.setY(lstEntries);
			CommonTransferable<?> transferable = new CommonTransferable<Pair<EntryWrapper, List<EntryWrapper>>>(RESPLAN_ENTRY_LIST_FLAVOR, pair);
			transferable.registerToStringFlavor();
			return transferable;
		}
		return null;		
	}
	
	@Override
	protected void exportDone(JComponent source, Transferable data, int action) {
		resPlanComponent.setMouseDown(false);
		if (action == MOVE) {
			JResPlanComponent<PK,R,T,E,L> resPlan = (JResPlanComponent<PK,R,T,E,L>) source;
			if (data.isDataFlavorSupported(RESPLAN_ENTRY_FLAVOR)) {
				try {
					EntryWrapper<PK,R,E,Collectable<PK>> wrapper = RESPLAN_ENTRY_FLAVOR.extractTransferData(data);
					// ???
					resPlan.getModel().removeEntry((E) wrapper.entry, wrapper.planElement);
				} catch (UnsupportedFlavorException e) {
					throw new NuclosFatalException(e);
				} catch (IOException e) {
					throw new NuclosFatalException(e);
				}
			} else if (data.isDataFlavorSupported(RESPLAN_ENTRY_LIST_FLAVOR)) {
				try {
					for (EntryWrapper<PK,R,E,Collectable<PK>> wrapper : RESPLAN_ENTRY_LIST_FLAVOR.extractTransferData(data).getY()) {
						resPlan.getModel().removeEntry((E) wrapper.entry, wrapper.planElement);
					}
				} catch (UnsupportedFlavorException | IOException e) {
					throw new NuclosFatalException(e);
				}
			}
		}
	}
	
	@Override
	public boolean canImport(TransferSupport support) {
		if (support.isDataFlavorSupported(RESPLAN_ENTRY_FLAVOR) || support.isDataFlavorSupported(RESPLAN_ENTRY_LIST_FLAVOR)) {
			JResPlanComponent<PK,R,T,E,L> resPlan = (JResPlanComponent<PK,R,T,E,L>) support.getComponent();
			return getTargetResource(resPlan, support) != null && getTargetInterval(resPlan, support, null) != null;
		} else {
			return super.importData(support);
		}
	}
	
	@Override
	public boolean importData(TransferSupport support) {
		if (support.isDataFlavorSupported(RESPLAN_ENTRY_FLAVOR) || support.isDataFlavorSupported(RESPLAN_ENTRY_LIST_FLAVOR)) {
			JResPlanComponent<PK,R,T,E,L> resPlan = (JResPlanComponent<PK,R,T,E,L>) support.getComponent();
			return importData(resPlan, support);
		} else {
			return super.importData(support);
		}
	}
	
	protected boolean importData(JResPlanComponent<PK, R, T, E, L> resPlan, 
			TransferSupport support) {
		ResPlanModel<PK, R, T, E, L> resPlanModel = resPlan.getModel();
		try {
			if (support.isDataFlavorSupported(RESPLAN_ENTRY_FLAVOR)) {
				EntryWrapper<PK,R,E,Collectable<PK>> wrapper = RESPLAN_ENTRY_FLAVOR.extractTransferData(support.getTransferable());
				pressedCellView = null;
				return importData(resPlan, support, wrapper, wrapper);
			} else if (support.isDataFlavorSupported(RESPLAN_ENTRY_LIST_FLAVOR)) {
				Pair<EntryWrapper, List<EntryWrapper>> pair = RESPLAN_ENTRY_LIST_FLAVOR.extractTransferData(support.getTransferable());
				boolean bUpdate = false;
				boolean bCopy = false;
				pressedCellView = null;
				for (EntryWrapper<PK,R,E,Collectable<PK>> wrapper : pair.getY()) {
					bUpdate = importData(resPlan, support, wrapper, pair.getX());
				}
				if (bUpdate) {
					return false;
				}
				if (bCopy) {
					return true;
				}
			}
		} catch (UnsupportedFlavorException e) {
			throw new NuclosFatalException(e);
		} catch (IOException e) {
			throw new NuclosFatalException(e);
		}
		return false;
	}
	
	private boolean importData(JResPlanComponent<PK, R, T, E, L> resPlan, TransferSupport support, EntryWrapper wrapper, EntryWrapper pressedWrapper) {
		ResPlanModel<PK, R, T, E, L> resPlanModel = resPlan.getModel();
		final E entry = (E) wrapper.unwrap(resPlanModel.getEntryType());
		ClientPlanElement<PK,R,Collectable<PK>> pElement = wrapper.getPlanElement();
		CellView<PK, R, T, E, L> transferredCellView = resPlan.findCellView(entry, pElement);
//		CellView<PK, R, T, E, L> pressedCellView = resPlan.findCellView((E) pressedWrapper.unwrap(resPlanModel.getEntryType()), pressedWrapper.getPlanElement());
		if (pressedCellView == null) {
			pressedCellView = resPlan.findCellView((E) pressedWrapper.unwrap(resPlanModel.getEntryType()), pressedWrapper.getPlanElement());
		}
		int diffx = transferredCellView.getRect().x - pressedCellView.getRect().x;
		int diffy = transferredCellView.getRect().y - pressedCellView.getRect().y;
		R resource = getTargetResource(resPlan, support, diffx, diffy);
		Interval<T> interval = getTargetInterval(resPlan, support, diffx, diffy, transferredCellView.getInterval(), transferredCellView.equals(pressedCellView) ? pressedCellView : null);
		if (resource == null || interval == null) {
			return false;
		}
		switch (support.getUserDropAction()) {
		case MOVE:
			if (resPlanModel.isUpdateEntryTimeAllowed(entry, pElement) || resPlanModel.isUpdateEntryResourceAllowed(entry, pElement)) {
				boolean bUpdate = true;
				if (!resPlanModel.isUpdateEntryResourceAllowed(entry, pElement)) {
					if (resource instanceof AbstractCollectable) {
						if (entry instanceof AbstractCollectable) {
							if (!((PK)((AbstractCollectable)resource).getId()).equals(((AbstractCollectable)entry).getValueId(pElement.getPrimaryField()))) {
								bUpdate = false;
							}
						}
					}
				}
				if (!resPlanModel.isUpdateEntryTimeAllowed(entry, pElement)) {
					Calendar calStart = Calendar.getInstance();
					Calendar calEnd = Calendar.getInstance();
					if (entry instanceof AbstractCollectable) {
						AbstractCollectable clct = (CollectableGenericObject) entry;
						Date datFrom = (Date)clct.getValue(pElement.getDateFromField());
						Date datUntil = (Date)clct.getValue(pElement.getDateUntilField());
						String timeFrom = (String)clct.getValue(pElement.getTimeFromField());
						String timeUntil = (String)clct.getValue(pElement.getTimeUntilField());
						
						calStart.setTime(datFrom);
						calStart.add(Calendar.HOUR, Integer.parseInt(timeFrom.split(":")[0]));
						calStart.add(Calendar.MINUTE, Integer.parseInt(timeFrom.split(":")[1]));
						calEnd.setTime(datFrom);
						calEnd.add(Calendar.HOUR, Integer.parseInt(timeUntil.split(":")[0]));
						calEnd.add(Calendar.MINUTE, Integer.parseInt(timeUntil.split(":")[1]));
					}
					if (interval.getStart() instanceof Date) {
						Interval<Date> sourceInterval = new Interval<Date>(calStart.getTime(), calEnd.getTime());
						if (!sourceInterval.equals(interval)) {
							bUpdate = false;
						}
					}
				}
				
				if (bUpdate) {
					resPlanModel.updateEntry(entry, resource, interval, pElement);
					// Return false because we just update the entry internally (it's not a real Transferable import).
					return false;
				}
			}
			break;
		case COPY:
			if (resPlanModel.isCreateEntryAllowed(entry, pElement)) {
				try {
					if (entry instanceof EntityObjectVO || entry instanceof GenericObjectVO
							|| entry instanceof CollectableMasterDataWithDependants || entry instanceof CollectableGenericObjectWithDependants) {
						final EntityObjectVO eovo = unpackEntityObjectFromEntry(entry);
						final GenerationContext<?> generationContext = EntityUtils.buildGenerationContext(eovo, new DependentSelection());
						final MainFrameTab mainFrameTab = MainFrame.getSelectedTab(resPlanComponent.getLocationOnScreen());
						EntityUtils.handleWithInputRequiredSupport(() -> {
							EntityObjectVO<?> eovoClone = EntityObjectDelegate.getInstance().clone(eovo, null, true, generationContext);
							E clonedEntry = (E) convertCloneResult(entry, eovoClone, pElement);
							// insert cloned entry into the model
							if (clonedEntry != null) {
								resPlan.getModel().createEntry(resource, interval, clonedEntry, pElement);
							}
						}, mainFrameTab);
					}
				} catch (CommonBusinessException e) {
					throw new NuclosFatalException("resplan entry could not be cloned.", e);
				}
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * Helper method to unpack an EntityObjectVO from an entry during the cloning process
	 * @param entry an entry, either of type {@link EntityObjectVO}, {@link GenericObjectVO}, {@link CollectableMasterDataWithDependants}, {@link CollectableGenericObjectWithDependants}
	 * @return unpacked EntityObjectVO
	 */
	protected EntityObjectVO unpackEntityObjectFromEntry(Object entry) {
		if (entry instanceof EntityObjectVO) {
			return (EntityObjectVO) entry;
		} else if (entry instanceof GenericObjectVO) {
			return DalSupportForGO.wrapGenericObjectVO((GenericObjectVO) entry);
		} else if (entry instanceof CollectableMasterDataWithDependants) {
			return ((CollectableMasterDataWithDependants) entry).getMasterDataWithDependantsCVO().getEntityObject();
		} else if (entry instanceof CollectableGenericObjectWithDependants) {
			return DalSupportForGO.wrapGenericObjectVO(((CollectableGenericObjectWithDependants) entry).getGenericObjectWithDependantsCVO());
		}
		throw new NuclosFatalException("Unexpected class type " + entry.getClass());
	}

	protected Object convertCloneResult(Object entry, EntityObjectVO<?> eovoClone, ClientPlanElement pElement) throws CommonBusinessException {
		if (entry instanceof EntityObjectVO) {
			return eovoClone;
		} else if (entry instanceof CollectableMasterDataWithDependants) {
			return new CollectableMasterDataWithDependants(((CollectableMasterDataWithDependants) entry).getCollectableEntity(), new MasterDataVO(eovoClone));
		} else if (entry instanceof CollectableGenericObjectWithDependants) {
			return new CollectableGenericObjectWithDependants(DalSupportForGO.getGenericObjectWithDependantsVO((EntityObjectVO<Long>) eovoClone, ((CollectableGenericObjectWithDependants) entry).getCollectableEntity()));
		} else if (entry instanceof GenericObjectVO) {
			return DalSupportForGO.getGenericObjectWithDependantsVO((EntityObjectVO<Long>) eovoClone, NuclosCollectableEntityProvider.getInstance().getCollectableEntity(pElement.getEntity()));
		}
		return null;
	}

	protected R getTargetResource(JResPlanComponent<PK,R,T,E,L> resPlan, TransferSupport support) {
		return getTargetResource(resPlan, support, 0, 0);
	}
	
	protected R getTargetResource(JResPlanComponent<PK,R,T,E,L> resPlan, TransferSupport support, int diffx, int diffy) {
		if (support.isDrop()) {
			Point dropPoint = support.getDropLocation().getDropPoint();
			dropPoint.x += diffx;
			dropPoint.y += diffy;
			return resPlan.getResourceAt(dropPoint);
		}
		return null;
	}
	
	protected Interval<T> getTargetInterval(JResPlanComponent<PK,R,T,E,L> resPlan, TransferSupport support, CellView originView) {
		return getTargetInterval(resPlan, support, 0, 0, null, originView);
	}
	
	protected Interval<T> getTargetInterval(JResPlanComponent<PK,R,T,E,L> resPlan, TransferSupport support, int diffx, int diffy, Interval<T> interval, CellView originView) {
		if (support.isDrop()) {
			Interval<T> intervalNew = null;
			if (originView != null) {
				intervalNew = resPlan.getDropInterval(originView);
			}
			if (intervalNew == null) {
				Point dropPoint = support.getDropLocation().getDropPoint();
				dropPoint.x += diffx;
				dropPoint.y += diffy;
				intervalNew = resPlan.getTimeIntervalAt(dropPoint);
				if (interval != null) {
					intervalNew = resPlan.getTimeModel().shiftInterval(interval, intervalNew.getStart());
				}
			}
			return intervalNew;
		}
		return null;
	}
}
