package org.nuclos.client.ui.util;


import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.SwingUtilities;

/**
 * Redispatches all the mouse events of a component to the parent.
 * This is useful, when a component hinders the parent component from receiving mouse events.
 * see the second answer below:
 * https://stackoverflow.com/questions/3818246/passing-events-to-parent/32204965
 */
public class MouseRedispatcher implements MouseListener {

	@Override
	public void mouseClicked(final MouseEvent e) {
		redispatchToParent(e);
	}

	@Override
	public void mousePressed(final MouseEvent e) {
		redispatchToParent(e);
	}

	@Override
	public void mouseReleased(final MouseEvent e) {
		redispatchToParent(e);
	}

	@Override
	public void mouseEntered(final MouseEvent e) {
		redispatchToParent(e);
	}

	@Override
	public void mouseExited(final MouseEvent e) {
		redispatchToParent(e);
	}

	private void redispatchToParent(MouseEvent e){
		Component source = (Component) e.getSource();
		MouseEvent parentEvent = SwingUtilities.convertMouseEvent(source, e, source.getParent());
		source.getParent().dispatchEvent(parentEvent);
	}
}
