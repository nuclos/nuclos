//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.E;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value list provider to get all db object types.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:maik.stueker@novabit.de">Maik Stueker</a>
 * @version 00.01.000
 */
public class DBObjectCollectableFieldsProvider implements CollectableFieldsProvider {

	private boolean bValueIds = true;
	public DBObjectCollectableFieldsProvider() {	
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String parameter, Object oValue) {
		if (LangUtils.equal(parameter, "value-ids")) {
			bValueIds = Boolean.valueOf((String)oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> result = new ArrayList<CollectableField>();
		for (MasterDataVO<?> dbobject : MasterDataCache.getInstance().get(E.DBOBJECT.getUID())) {
			if (!bValueIds)
				result.add(new CollectableValueField(dbobject.getFieldValue(E.DBOBJECT.name)));
			else
				result.add(new CollectableValueIdField(dbobject.getPrimaryKey(), dbobject.getFieldValue(E.DBOBJECT.name)));
		}
		Collections.sort(result);
		return result;
	}

}
