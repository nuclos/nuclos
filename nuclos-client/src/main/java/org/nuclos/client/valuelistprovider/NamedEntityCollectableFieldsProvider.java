//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.valuelistprovider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value list provider to get all connection security types for email
 */
public class NamedEntityCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(NamedEntityCollectableFieldsProvider.class);
	private final UID entityUID;
	private final FieldMeta.Valueable<String> fieldValuable;
	private final Set<String> excludedValues = new HashSet<String>();

	//

	public NamedEntityCollectableFieldsProvider(final UID entityUID, final FieldMeta.Valueable<String> fieldValueable) {
		this.entityUID = entityUID;
		this.fieldValuable = fieldValueable;
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String parameter, Object oValue) {
		if ("excludedValues".equals(parameter)) {
			excludedValues.addAll(Arrays.asList(oValue.toString().split(",")));
		} else {
			LOG.info("Unknown parameter " + parameter + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		List<CollectableField> result = new ArrayList<>();
		for (MasterDataVO<?> entity : MasterDataCache.getInstance().get(entityUID)) {
			if (excludedValues.isEmpty() || !excludedValues.contains(entity.getFieldValue(fieldValuable))) {
				result.add(new CollectableValueField(entity.getFieldValue(fieldValuable)));
			}
		}
		Collections.sort(result);
		return result;
	}

}
