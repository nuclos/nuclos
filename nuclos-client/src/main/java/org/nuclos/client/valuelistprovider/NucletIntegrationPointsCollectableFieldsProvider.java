//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value list provider to get nuclet integration points.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:maik.stueker@novabit.de">Maik Stueker</a>
 * @version 00.01.000
 */
public class NucletIntegrationPointsCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(NucletIntegrationPointsCollectableFieldsProvider.class);

	public static final String PARAM_NUCLET_ID = "nucletId";

	private UID nucletUID = null;

	public NucletIntegrationPointsCollectableFieldsProvider() {
	}

	@Override
	public void setParameter(String parameter, Object oValue) {
		if (parameter.equalsIgnoreCase(PARAM_NUCLET_ID)) {
			if (oValue instanceof UID) {
				nucletUID = (UID) oValue;
			} else if (oValue instanceof String) {
				nucletUID = UID.parseUID((String) oValue);
			} else if (oValue == null) {
				nucletUID = null;
			} else {
				throw new IllegalArgumentException("Class " + oValue.getClass().getCanonicalName() + " not supported");
			}
		} else {
			LOG.info("Unknown parameter " + parameter + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> result = new ArrayList<>();
		for (MasterDataVO<?> ipMd : MasterDataCache.getInstance().get(E.NUCLET_INTEGRATION_POINT.getUID())) {
			if (nucletUID != null) {
				final UID ipNucletUID = ipMd.getFieldUid(E.NUCLET_INTEGRATION_POINT.nuclet);
				if (!ipNucletUID.equals(nucletUID)) {
					continue;
				}
			}
			result.add(makeCollectableField(ipMd));
		}

		Collections.sort(result);
		return result;
	}
	
	protected CollectableField makeCollectableField(MasterDataVO<?> ipMd) {
		final UID id = (UID) ipMd.getPrimaryKey();
		final String value = ipMd.getFieldValue(E.NUCLET_INTEGRATION_POINT.name);
		return new CollectableValueIdField(id, value);
	}
}
