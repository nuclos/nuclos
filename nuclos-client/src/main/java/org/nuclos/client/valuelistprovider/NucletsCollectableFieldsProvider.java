//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.E;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Value list provider to get all entities containing a menupath.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:maik.stueker@novabit.de">Maik Stueker</a>
 * @version 00.01.000
 */
public class NucletsCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(NucletsCollectableFieldsProvider.class);

	public static final String EXCLUDE_NUCLET_ID = "excludeNucletId";

	public static final String ONLY_SOURCE_NUCLETS = "onlySourceNuclets";

	//

	private UID excludeNucletUID = null;

	private boolean bOnlySourceNuclets = false;

	public NucletsCollectableFieldsProvider() {
	}

	@Override
	public void setParameter(String parameter, Object oValue) {
		if (parameter.equalsIgnoreCase(EXCLUDE_NUCLET_ID)) {
			if (oValue instanceof UID) {
				excludeNucletUID = (UID) oValue;
			} else if (oValue instanceof String) {
				excludeNucletUID = UID.parseUID((String) oValue);
			} else if (oValue == null) {
				excludeNucletUID = null;
			} else {
				throw new IllegalArgumentException("Class " + oValue.getClass().getCanonicalName() + " not supported");
			}
		} else if (parameter.equalsIgnoreCase(ONLY_SOURCE_NUCLETS)) {
			bOnlySourceNuclets = RigidUtils.parseBoolean(oValue);
		} else {
			LOG.info("Unknown parameter " + parameter + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// LOG.debug("getCollectableFields");
		final Collection<EntityObjectVO<UID>> nuclets = new ArrayList<EntityObjectVO<UID>>();
		for (EntityObjectVO<UID> eoNuclet : MetaProvider.getInstance().getNuclets()) {
			if (excludeNucletUID != null && ObjectUtils.equals(eoNuclet.getPrimaryKey(), excludeNucletUID)) {
				continue;
			}
			if (bOnlySourceNuclets && !Boolean.TRUE.equals(eoNuclet.getFieldValue(E.NUCLET.source))) {
				continue;
			}
			nuclets.add(eoNuclet);
		}
		final List<CollectableField> result = CollectionUtils.transform(nuclets, new Transformer<EntityObjectVO<UID>, CollectableField>() {
			@Override
			public CollectableField transform(EntityObjectVO<UID> eoNuclet) {
				return makeCollectableField(eoNuclet);
			}
		});
		Collections.sort(result);
		return result;
	}
	
	protected CollectableField makeCollectableField(EntityObjectVO<UID> eoNuclet) {
		final UID id = eoNuclet.getPrimaryKey();
		final String value = eoNuclet.getFieldValue(E.NUCLET.packagefield);
		return new CollectableValueIdField(id, value);
	}
}
