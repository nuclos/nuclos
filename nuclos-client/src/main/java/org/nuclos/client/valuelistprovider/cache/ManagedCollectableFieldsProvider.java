//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.valuelistprovider.cache;

import org.nuclos.client.masterdata.valuelistprovider.MandatorRestrictionCollectableFieldsProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;

public abstract class ManagedCollectableFieldsProvider implements CollectableFieldsProvider, MandatorRestrictionCollectableFieldsProvider {

	private boolean bIgnoreValidity = false;
	
	private UID mandator;

	public void setIgnoreValidity(Boolean iIgnoreValidity) {
		this.bIgnoreValidity = iIgnoreValidity;
	}

	public Boolean getIgnoreValidity() {
		return bIgnoreValidity;
	}

	@Override
	public UID getMandator() {
		return mandator;
	}

	public void setMandator(UID mandator) {
		this.mandator = mandator;
	}
	
	

}
