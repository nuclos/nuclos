package org.nuclos.client.wizard;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.JButton;

import org.apache.log4j.Logger;
import org.nuclos.common2.SpringLocaleDelegate;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.Wizard;
import org.pietschy.wizard.WizardAction;
import org.pietschy.wizard.WizardStep;

public class NuclosAttributeImportWizardButtonBar extends NuclosWizardAbstractButtonBar {

	private static final Logger LOG = Logger.getLogger(NuclosAttributeWizardButtonBar.class);

	private Wizard mywizard;

	private JButton btFinish;
	private JButton btCancel;
	private JButton btLast;

	/**
	 * @param wizard
	 */
	public NuclosAttributeImportWizardButtonBar(Wizard wizard) {
		super(wizard);
		this.mywizard = wizard;

		post();
		setCancelWithESC();
	}

	protected void post() {
		btFinish.setAction(new WizardAction("finish", this.mywizard) {

			@Override
			protected void updateState() {
				WizardStep activeStep = getActiveStep();
				setEnabled(activeStep != null && getModel().isLastStep(activeStep) && activeStep.isComplete() && !activeStep.isBusy());
			}

			@Override
			public void doAction(final ActionEvent e) throws InvalidStateException {
				WizardStep finishStep = getModel().getActiveStep();
				try {
					finishStep.applyState();
				} catch (InvalidStateException e1) {
					LOG.warn("doAction: " + e1);
					updateState();
					return;
				}
				int defaultCloseOperation = getWizard().getDefaultExitMode();

				if (defaultCloseOperation == Wizard.EXIT_ON_FINISH)
					getWizard().getCloseAction().actionPerformed(e);
				else if (defaultCloseOperation == Wizard.EXIT_ON_CLOSE)
					getWizard().getCloseAction().setEnabled(true);
				else
					throw new InvalidStateException("Invalid finish operation: " + defaultCloseOperation);
			}

		});
		btFinish.setText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.buttonbar.finish", "Fertig"));
		btFinish.setEnabled(true);

		btLast.setVisible(false);
	}


	@Override
	protected void layoutButtons(JButton helpButton,
								 final JButton previousButton, JButton nextButton, JButton lastButton,
								 JButton finishButton, JButton cancelButton, JButton closeButton) {

		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		nextButton.setText(localeDelegate.getMessage(
				"wizard.buttonbar.next", "weiter"));
		previousButton.setText(localeDelegate.getMessage(
				"wizard.buttonbar.previous", "zur\u00fcck"));
		lastButton.setText(">>");
		finishButton.setText(localeDelegate.getMessage(
				"wizard.buttonbar.finish", "Fertig"));
		cancelButton.setText(localeDelegate.getMessage(
				"wizard.buttonbar.cancel", "Verwerfen"));
		closeButton.setText(localeDelegate.getMessage(
				"wizard.buttonbar.close", "Schliessen"));

		super.layoutButtons(helpButton, previousButton, nextButton, lastButton,
				finishButton, cancelButton, closeButton);

		this.equalizeButtonWidths(helpButton, previousButton, nextButton, lastButton, finishButton, cancelButton, closeButton);
		cancelButton.setPreferredSize(new Dimension(90, 25));

		nextButton.setVisible(false);
		previousButton.setVisible(false);
		lastButton.setVisible(false);

		this.btFinish = finishButton;
		this.btCancel = cancelButton;
		this.btLast = lastButton;

		this.btFinish.setEnabled(true);
	}

}
