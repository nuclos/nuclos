package org.nuclos.client.wizard;

import java.util.Iterator;

import org.nuclos.client.wizard.steps.NuclosEntityAttributeImportStep;
import org.pietschy.wizard.ButtonBar;
import org.pietschy.wizard.Wizard;
import org.pietschy.wizard.WizardModel;

public class NuclosEntityAttributeImportWizard extends Wizard {

	/**
	 * Creates a new Wizard that uses the specified {@link WizardModel}.
	 *
	 * @param model the model that the wizard is to use.
	 */
	public NuclosEntityAttributeImportWizard(final WizardModel model) {
		super(model);
		this.setDefaultExitMode(EXIT_ON_FINISH);
	}

	@Override
	protected ButtonBar createButtonBar() {
		return new NuclosAttributeImportWizardButtonBar(this);
	}

	@Override
	public void cancel() {
		super.cancel();
		close();
	}

	@Override
	public void close() {
		super.close();
		for (Iterator<NuclosEntityAttributeImportStep> it = getModel().stepIterator(); it.hasNext();) {
			final NuclosEntityAttributeImportStep step = it.next();
			step.close();
		}
	}

}
