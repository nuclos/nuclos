package org.nuclos.client.wizard;

import javax.swing.Icon;

import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.PanelWizardStep;
import org.pietschy.wizard.WizardModel;

public abstract class NuclosWizardAbstractStep extends PanelWizardStep {

	private WizardModel model;

	public NuclosWizardAbstractStep() {
	}

	public NuclosWizardAbstractStep(String name, String summary) {
		super(name, summary);
	}

	public NuclosWizardAbstractStep(String name, String summary, Icon icon) {
		super(name, summary, icon);
	}

	@Override
	public void init(WizardModel model) {
		super.init(model);
		this.model = model;
	}

	protected void nextIfComplete() {
		if (!isComplete()) {
			return;
		}
		try {
			applyState();
			model.nextStep();
		} catch (InvalidStateException e1) {
			// ignore
		}
	}
}
