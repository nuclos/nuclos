//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.DefaultComponentTypes;
import org.nuclos.common.E;
import org.nuclos.common.NuclosDateTime;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.dbtransfer.TransferFacadeRemote;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;


/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/

public class DataTyp implements Cloneable {
	
	public static enum JavaTypeEnum {
		String {
			@Override
			public String getClazz() {
				return String.class.getName();
			}
			@Override
			public String toString() {
				return String.class.getName();
			}
		},
		Integer {
			@Override
			public String getClazz() {
				return Integer.class.getName();
			}
			@Override
			public String toString() {
				return Integer.class.getName();
			}
		},
		Long {
			@Override
			public String getClazz() {
				return Long.class.getName();
			}
			@Override
			public String toString() {
				return Long.class.getName();
			}
		},
		Double {
			@Override
			public String getClazz() {
				return Double.class.getName();
			}
			@Override
			public String toString() {
				return Double.class.getName();
			}
		},
		Date {
			@Override
			public String getClazz() {
				return Date.class.getName();
			}
			@Override
			public String toString() {
				return Date.class.getName();
			}
		},
		NuclosDateTime {
			@Override
			public String getClazz() {
				return NuclosDateTime.class.getName();
			}
			@Override
			public String toString() {
				return NuclosDateTime.class.getName();
			}
		},
		Boolean {
			@Override
			public String getClazz() {
				return Boolean.class.getName();
			}
			@Override
			public String toString() {
				return Boolean.class.getName();
			}
		},
		GenericObjectDocumentFile {
			@Override
			public String getClazz() {
				return GenericObjectDocumentFile.class.getName();
			}
			@Override
			public String toString() {
				return GenericObjectDocumentFile.class.getName();
			}
		},
		ByteArray {
			@Override
			public String getClazz() {
				return byte[].class.getName();
			}
			@Override
			public String toString() {
				return "byte[]";
			}
		},
		NuclosPassword {
			@Override
			public String getClazz() {
				return NuclosPassword.class.getName();
			}
			@Override
			public String toString() {
				return NuclosPassword.class.getName();
			}
		},
		NuclosImage {
			@Override
			public String getClazz() {
				return NuclosImage.class.getName();
			}
			@Override
			public String toString() {
				return NuclosImage.class.getName();
			}
		};
		public abstract String getClazz();
		public static JavaTypeEnum fromClazz(String clazz) {
			JavaTypeEnum[] types = values(); 
			for (int i = 0; i < types.length; i++) {
				if (types[i].getClazz().equals(clazz))
					return types[i];
			}
			return null;
		}
	}

	String name;
	String inputFormat;
	String outputFormat;
	String databaseTyp;
	Integer precision;
	Integer scale;
	String javaType;
	String defaultComponentType;
	static String nameReferenzFeld;
	
	public DataTyp() {

	}

	public DataTyp(String name, String inputFormat, String outputFormat,
			String databaseTyp, String javaTyp, String defaultComponentType) {
		super();
		this.name = name;
		this.inputFormat = inputFormat;
		this.outputFormat = outputFormat;
		this.databaseTyp = databaseTyp;
		this.javaType = javaTyp;
		this.defaultComponentType = defaultComponentType;
	}

	public DataTyp(String name, String inputFormat, String outputFormat,
			String databaseTyp, Integer scale,Integer precision, String javaTyp, String defaultComponentType) {
		super();
		this.name = name;
		this.inputFormat = inputFormat;
		this.outputFormat = outputFormat;
		this.databaseTyp = databaseTyp;
		this.precision = precision;
		this.scale = scale;
		this.javaType = javaTyp;
		this.defaultComponentType = defaultComponentType;
	}

	public DataTyp(MasterDataVO voDataType) {
		super();
		this.name = (String)voDataType.getFieldValue(E.DATATYPE.name.getUID());
		this.inputFormat =(String)voDataType.getFieldValue(E.DATATYPE.inputformat.getUID());
		this.outputFormat =(String)voDataType.getFieldValue(E.DATATYPE.outputformat.getUID());
		this.precision = (Integer)voDataType.getFieldValue(E.DATATYPE.precision.getUID());
		this.scale = (Integer)voDataType.getFieldValue(E.DATATYPE.scale.getUID());
		this.javaType = (String)voDataType.getFieldValue(E.DATATYPE.javatyp.getUID());
		this.databaseTyp = (String)voDataType.getFieldValue(E.DATATYPE.databasetyp.getUID());
		this.defaultComponentType = (String)voDataType.getFieldValue(E.DATATYPE.defaultcomponenttype.getUID());
	}

	public boolean isRefenceTyp() {
		if(name.equals(getNameReferenzFeld()) || isDocumentFileType()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean isDocumentFileType() {
		if(name.equals("Dokumentenanhang")) {
			return true;
		}
		return false;
	}

	public boolean isLookupTyp() {
		if(name.equals("Nachschlagefeld")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean isMemoTyp() {
		if(name.equals("Memo")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean isLargTextObjectTyp() {
		if(name.equals("Text (Large Object)")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean isAutoNumberTyp() {
		return (name.equals(DefaultComponentTypes.AUTONUMBER));
		
	}

	public boolean isValueListTyp() {
		if(name.equals("Werteliste")) {
			return true;
		}
		else {
			return false;
		}
	}

	public static String getDefaultTypForJavaClass(Class<?> clazz) {
		if(clazz.isAssignableFrom(String.class)) {
			return "Text";
		}
		else if(clazz.isAssignableFrom(Integer.class)) {
			return "Ganzzahl";
		}
		else if(clazz.isAssignableFrom(Double.class)) {
			return "Kommazahl (9,2)";
		}
		else if(clazz.isAssignableFrom(Boolean.class)) {
			return "Ja/Nein";
		}
		else if(clazz.isAssignableFrom(java.util.Date.class)) {
			return "Datum";
		}
		else if(clazz.isAssignableFrom(org.nuclos.common.report.ByteArrayCarrier.class)) {
			return "Bild";
		}
		else if(clazz.isAssignableFrom(org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile.class)) {
			return "Dokumentenanhang";
		}
		else {
			return "Text";
		}
	}

	public static boolean isConversionSupported(DataTyp source, DataTyp target) {
		int sourcescale = LangUtils.defaultIfNull(source.getScale(), 0);
		int sourceprecision = LangUtils.defaultIfNull(source.getPrecision(), 0);
		int targetscale = LangUtils.defaultIfNull(target.getScale(), 0);
		int targetprecision = LangUtils.defaultIfNull(target.getPrecision(), 0);

		if (!String.class.getName().equals(target.getJavaType())) {
			// no to-string conversion, check if source type equals target type
			if (source.getJavaType().equals(target.getJavaType())) {
				// check scale and precision if required
				if (Double.class.getName().equals(target.getJavaType())) {
					if (sourceprecision - targetprecision > 0) {
						return false;
					}
				}
				return true;
			}
			else {
				return (sourcescale <= targetscale) && LangUtils.equal(source.getDatabaseTyp(), target.getDatabaseTyp());
			}
		}
		else {
			if (!LangUtils.equal(source.isRefenceTyp(), target.isRefenceTyp())) {
				return false;
			}
			if (!LangUtils.equal(source.isValueListTyp(), target.isValueListTyp())) {
				return false;
			}
			if (!LangUtils.equal(source.isLookupTyp(), target.isLookupTyp())) {
				return false;
			}
			if ("POSTGRESQL".equals(SpringApplicationContextHolder.getBean(TransferFacadeRemote.class).getDatabaseType())
					&& ("varchar".equals(source.getDatabaseTyp()) || (source.getDatabaseTyp() == null && String.class.getName().equals(target.getJavaType())))
					&& "clob".equals(target.getDatabaseTyp())) {
				return true;
			}
			return (sourcescale <= targetscale) && LangUtils.equal(source.getDatabaseTyp(), target.getDatabaseTyp());
		}
	}

	public static List<DataTyp> getConvertibleTypes(DataTyp dataTyp, boolean isGeneric) {
		List<DataTyp> lst = new ArrayList<DataTyp>();
		for(DataTyp typ : getAllDataTyps(isGeneric)) {
			if(isConversionSupported(dataTyp, typ)) {
				lst.add(typ);
			}
		}
		return lst;
	}

	public String getName() {
		return name;
	}

	public void setName(String typ) {
		this.name = typ;
	}

	public String getInputFormat() {
		return inputFormat;
	}

	public void setInputFormat(String inputFormat) {
		this.inputFormat = inputFormat;
	}

	public String getOutputFormat() {
		return outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	public String getDatabaseTyp() {
		return databaseTyp;
	}

	public void setDatabaseTyp(String databaseTyp) {
		this.databaseTyp = databaseTyp;
	}

	public Integer getPrecision() {
		if(precision != null && precision.intValue() == 0 && !Double.class.getName().equals(getJavaType()))
			return null;
		return precision;
	}

	public void setPrecision(Integer precision) {
		this.precision = precision;
	}

	public Integer getScale() {
		if(scale != null && scale.intValue() == 0)
			return null;
		return scale;
	}

	public void setScale(Integer scale) {
		this.scale = scale;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DataTyp) {
			return hashCode() == obj.hashCode();
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hashCode(name);
	}

	public String getShortJavaType() {
		if (JavaTypeEnum.fromClazz(javaType) == null) {
			return "";
		}
		String sjavatype = JavaTypeEnum.fromClazz(javaType).toString();
		if (javaType != null && javaType.indexOf('.') > -1) 
			sjavatype = javaType.substring(javaType.lastIndexOf('.') + 1);
		return sjavatype;
	}
	
	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public String getDefaultComponentType() {
		return defaultComponentType;
	}

	public void setDefaultComponentType(String defaultComponentType) {
		this.defaultComponentType = defaultComponentType;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new DataTyp(name, inputFormat, outputFormat, databaseTyp, javaType, defaultComponentType);
	}

	public static List<DataTyp> getAllDataTyps(boolean isGeneric) {
		List<DataTyp> lst = new ArrayList<DataTyp>();

		for (DataTyp type : DataTyp.getAllDataTypsFromDB()) {
			// Allow Autonumbers (NUCLOS-9004)
			//if (isGeneric && type.getName().equals("Autonummer")) {
			//	continue;
			//}
			lst.add(type);
		}
		lst.add(getReferenzTyp());
		if (!isGeneric) {
			lst.add(getLookupTyp());
		}

		return lst;
	}

	private static List<DataTyp> getAllDataTypsFromDB() {
		final List<DataTyp> lst = new ArrayList<DataTyp>();
		final Collection<MasterDataVO<?>> colVO = MasterDataCache.getInstance().get(E.DATATYPE.getUID());
		for(MasterDataVO<?> vo : colVO) {
			DataTyp typ = new DataTyp((String)vo.getFieldValue(E.DATATYPE.name.getUID()), (String)vo.getFieldValue(E.DATATYPE.inputformat.getUID()), (String)vo.getFieldValue(E.DATATYPE.outputformat.getUID()),
				(String)vo.getFieldValue(E.DATATYPE.databasetyp.getUID()), (Integer)vo.getFieldValue(E.DATATYPE.scale.getUID()), (Integer)vo.getFieldValue(E.DATATYPE.precision.getUID()), (String)vo.getFieldValue(E.DATATYPE.javatyp.getUID()), 
				(String)vo.getFieldValue(E.DATATYPE.defaultcomponenttype.getUID()));
			if(typ.getName().equals("Referenzfeld"))
				continue;
			if(typ.getName().equals("Nachschlagefeld"))
				continue;
			lst.add(typ);
		}
		
		return lst;
	}

	public static DataTyp getReferenzTyp() {
		return new DataTyp(getNameReferenzFeld(), null, null, "varchar", 255, 0, "java.lang.String", null);
	}
	
	public static String  getNameReferenzFeld()
	{
		if(nameReferenzFeld==null)
		{
			nameReferenzFeld=SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.reference", "Referenzfeld");
		}
		return nameReferenzFeld;
	}

	public static DataTyp getLookupTyp() {
		return new DataTyp("Nachschlagefeld", null, null, "varchar", 255, 0, "java.lang.String", null);
	}

	public static DataTyp getValueListTyp() {
		return new DataTyp("explizite Werteliste", null, null, "varchar", 255, 0, "java.lang.String", null);
	}

	public static DataTyp getDefaultDataTyp() throws CommonFinderException, CommonPermissionException  {
		MasterDataVO<?> vo = null;
		for(MasterDataVO<?> voDataType : MasterDataCache.getInstance().get(E.DATATYPE.getUID())) {
			if("Text".equals(voDataType.getFieldValue(E.DATATYPE.name.getUID()))) {
				vo = voDataType;
				break;
			}
		}
		DataTyp typ = new DataTyp((String)vo.getFieldValue(E.DATATYPE.name.getUID()), (String)vo.getFieldValue(E.DATATYPE.inputformat.getUID()), (String)vo.getFieldValue(E.DATATYPE.outputformat.getUID()),
			(String)vo.getFieldValue(E.DATATYPE.databasetyp.getUID()), (Integer)vo.getFieldValue(E.DATATYPE.scale.getUID()), (Integer)vo.getFieldValue(E.DATATYPE.precision.getUID()), (String)vo.getFieldValue(E.DATATYPE.javatyp.getUID()),
			(String)vo.getFieldValue(E.DATATYPE.defaultcomponenttype.getUID()));
		return typ;
	}

	public static DataTyp getDefaultStringTyp() {
		return new DataTyp("Text", null, null, "varchar", 255, 0, "java.lang.String", null);
	}

	public static DataTyp getDefaultDateTyp() {
		return new DataTyp("Date", null, null, "varchar", null, null, "java.util.Date", null);
	}

}
