package org.nuclos.client.wizard.steps;

import org.pietschy.wizard.InvalidStateException;

/**
 * Created by Sebastian Debring on 3/20/2019.
 */
public class InvalidReferenceException extends InvalidStateException {

	private final boolean isSearchField;

	public InvalidReferenceException(final String message, boolean isSearchField) {
		super(message);
		this.isSearchField = isSearchField;
	}

	public boolean isSearchField() {
		return isSearchField;
	}
}
