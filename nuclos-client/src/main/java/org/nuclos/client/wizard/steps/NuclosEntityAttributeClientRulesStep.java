package org.nuclos.client.wizard.steps;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.util.ITableLayoutBuilder;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.client.wizard.NuclosEntityWizardStaticModel;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.schema.meta.entity.EntityFieldClientRule;
import org.nuclos.schema.meta.entity.EntityFieldClientRuleStatement;
import org.nuclos.schema.meta.entity.EntityFieldClientRuleStatementClearValue;
import org.nuclos.schema.meta.entity.EntityFieldClientRuleStatementTransferLookedupValue;
import org.nuclos.schema.meta.entity.EntityFieldClientRules;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

public class NuclosEntityAttributeClientRulesStep extends NuclosEntityAttributeAbstractStep {

	private NuclosEntityWizardStaticModel parentWizardModel;

	private JComboBox cbRuleTrigger;

	private JPanel jpnStatements;
	private JScrollPane scrollStatements;

	private MultiListMap<Integer, JComponent> revalidatingComponents;

	private TableLayoutBuilder tblStatements;
	private List<EntityFieldClientRuleStatement> lstStatements;

	private JLabel lbInfo;

	private EntityFieldClientRules clientRules;
	private EntityFieldClientRule rule;

	private List<Attribute> otherAttributes;
	private List<FieldMeta<?>> foreignAttributes;

	public NuclosEntityAttributeClientRulesStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	private static String msg(String resId, Object ... params) {
		return SpringLocaleDelegate.getInstance().getMessage(resId, "", params);
	}

	@Override
	protected void initComponents() {
		TableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayout.PREFERRED,40, TableLayout.FILL, TableLayout.FILL);
		tbllay.getTableLayout().setVGap(3);
		tbllay.getTableLayout().setHGap(12);

		cbRuleTrigger = new JComboBox();
		cbRuleTrigger.addItem("on-value-change");
		cbRuleTrigger.setEnabled(false);
		tbllay.addLocalizedLabel("wizard.step.attributeclientrules.1").skip().add(cbRuleTrigger);

		jpnStatements = new JPanel();
		jpnStatements.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		scrollStatements = new JScrollPane(jpnStatements, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tbllay.newRow(TableLayout.FILL).addFullSpan(scrollStatements);

		lbInfo = new JLabel();
		lbInfo.setForeground(Color.RED);
		tbllay.newRow().addFullSpan(lbInfo);
	}

	@Override
	public void prepare() {
		super.prepare();

		lstStatements = new ArrayList<>();
		revalidatingComponents = new MultiListHashMap<>();

		clientRules = this.model.getAttribute().getClientRules();
		if (clientRules == null) {
			clientRules = new EntityFieldClientRules();
			this.model.getAttribute().setClientRules(clientRules);
		}
		if (clientRules.getRules().isEmpty()) {
			rule = new EntityFieldClientRule();
			rule.setTriggerOnValueChange(true);
			clientRules.getRules().add(rule);
		} else {
			rule = clientRules.getRules().get(0);
		}

		otherAttributes = new ArrayList<>(parentWizardModel.getAttributeModel().getNucletAttributes());
		otherAttributes.remove(getModel().getAttribute());
		Collections.sort(otherAttributes, new Comparator<Attribute>() {
			@Override
			public int compare(final Attribute a1, final Attribute a2) {
				return StringUtils.compareIgnoreCase(a1.getInternalName(), a2.getInternalName());
			}
		});

		foreignAttributes = new ArrayList<>();
		if (this.model.isReferenceType() && getModel().getAttribute().getMetaVO() != null) {
			foreignAttributes.addAll(this.model.getAttribute().getMetaVO().getFields());
			Collections.sort(foreignAttributes, new Comparator<FieldMeta>() {
				@Override
				public int compare(final FieldMeta f1, final FieldMeta f2) {
					return StringUtils.compareIgnoreCase(f1.getFieldName(), f2.getFieldName());
				}
			});
		}

		setupStatementsPanel();

		List<EntityFieldClientRuleStatement> allStatementsOrdered = new ArrayList<>();
		allStatementsOrdered.addAll(rule.getClearValueStatements());
		allStatementsOrdered.addAll(rule.getTransferLookedupValueStatements());
		Collections.sort(allStatementsOrdered, new Comparator<EntityFieldClientRuleStatement>() {
			@Override
			public int compare(final EntityFieldClientRuleStatement stmt1, final EntityFieldClientRuleStatement stmt2) {
				return LangUtils.compare(stmt1.getOrder(), stmt2.getOrder());
			}
		});
		allStatementsOrdered.forEach(this::addStatementRow);

		validateStep();
	}

	private void setupStatementsPanel() {
		jpnStatements.removeAll();

		tblStatements = new TableLayoutBuilder(jpnStatements).columns(
				TableLayout.PREFERRED, // remove
				10d,
				TableLayout.PREFERRED, // up
				TableLayout.PREFERRED, // down
				TableLayout.PREFERRED, // add btn 1
				TableLayout.PREFERRED, // add btn 2
				TableLayout.FILL);
		tblStatements.getTableLayout().setVGap(3);
		tblStatements.getTableLayout().setHGap(5);

		tblStatements.newRow(5);
		tblStatements.newRow(5);
		tblStatements.newRow().skip(4).add(newAddNewClearValueButton());
		if (this.model.isReferenceType()) {
			tblStatements.add(newAddNewTransferLookedupValueButton());
		}
		tblStatements.newRow(5);

		jpnStatements.invalidate();
		jpnStatements.revalidate();
		jpnStatements.repaint();
	}

	private JComponent newAddNewClearValueButton() {
		JButton btnNew = new JButton(msg("wizard.step.attributeclientrules.2"), Icons.getInstance().getIconPlus16());
		btnNew.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				EntityFieldClientRuleStatementClearValue stmt = new EntityFieldClientRuleStatementClearValue();
				rule.getClearValueStatements().add(stmt);
				addStatementRow(stmt);
			}
		});
		return btnNew;
	}

	private JComponent newAddNewTransferLookedupValueButton() {
		JButton btnNew = new JButton(msg("wizard.step.attributeclientrules.3"), Icons.getInstance().getIconPlus16());
		btnNew.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				EntityFieldClientRuleStatementTransferLookedupValue stmt = new EntityFieldClientRuleStatementTransferLookedupValue();
				rule.getTransferLookedupValueStatements().add(stmt);
				addStatementRow(stmt);
			}
		});
		return btnNew;
	}

	private void addStatementRow(EntityFieldClientRuleStatement stmt) {
		final int iRow = lstStatements.size();
		lstStatements.add(stmt);
		if (stmt.getOrder() == null) {
			stmt.setOrder(BigInteger.valueOf(lstStatements.size()));
		}
		StatementEditor statementEditor = new StatementEditor(new EditorHelper(this), iRow);
		ITableLayoutBuilder newRow = tblStatements.insertRowAt(tblStatements.getRowSize() - 4)
				.add(newRemoveStatementButton(iRow))
				.skip()
				.add(newUpDownButton(iRow, true)) // up
				.add(newUpDownButton(iRow, false)) // down
				.addFullSpan(statementEditor);
		revalidatingComponents.addValue(iRow, statementEditor);
		validateStep();
		repaintStep(this);
	}

	private JComponent newRemoveStatementButton(final int iRow) {
		JLabel removeIcon = new JLabel(Icons.getInstance().getIconMinus16());
		removeIcon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
					if (isStatementEmpty(lstStatements.get(iRow))) {
						removeStatement(iRow);
					} else {
						if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(NuclosEntityAttributeClientRulesStep.this,
								msg("wizard.step.attributeclientrules.4", iRow + 1),
								null,
								JOptionPane.YES_NO_OPTION)) {
							removeStatement(iRow);
						}
					}
					e.consume();
				}
			}
		});
		return removeIcon;
	}

	private JComponent newUpDownButton(final int iRow, final boolean up) {
		JLabel jLabel = new JLabel( up ?
				Icons.getInstance().getIconUp16() :
				Icons.getInstance().getIconDown16()) {
			@Override
			public void revalidate() {
				boolean bVisible = true;
				if (up && iRow == 0) {
					bVisible = false;
				}
				if (!up && iRow == lstStatements.size()-1) {
					bVisible = false;
				}
				setVisible(bVisible);
				super.revalidate();
			}
		};
		jLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
					EntityFieldClientRuleStatement removed = lstStatements.remove(iRow);
					if (up) {
						lstStatements.add(iRow-1, removed);
					} else {
						lstStatements.add(iRow+1, removed);
					}
					syncOrderToStatements();
					repaintStep(NuclosEntityAttributeClientRulesStep.this);
					e.consume();
				}
			}
		});
		revalidatingComponents.addValue(iRow, jLabel);
		return jLabel;
	}

	private void removeStatement(final int iRow) {
		EntityFieldClientRuleStatement stmt = lstStatements.get(iRow);
		if (stmt instanceof EntityFieldClientRuleStatementClearValue) {
			rule.getClearValueStatements().remove(stmt);
		} else if (stmt instanceof  EntityFieldClientRuleStatementTransferLookedupValue) {
			rule.getTransferLookedupValueStatements().remove(stmt);
		}
		lstStatements.remove(iRow);
		syncOrderToStatements();
		final int lastRowIndex = lstStatements.size();
		tblStatements.deleteRow(lastRowIndex);
		revalidatingComponents.removeKey(lastRowIndex);
		validateStep();
		repaintStep(this);
	}

	private void syncOrderToStatements() {
		for (int i = 0; i < lstStatements.size(); i++) {
			lstStatements.get(i).setOrder(BigInteger.valueOf(i+1));
		}
	}

	private static void repaintStep(NuclosEntityAttributeClientRulesStep rulesStep) {
		if (rulesStep != null) {
			rulesStep.revalidatingComponents.getAllValues().forEach(JComponent::revalidate);
			rulesStep.jpnStatements.invalidate();
			rulesStep.jpnStatements.revalidate();
			rulesStep.jpnStatements.repaint();
			rulesStep.invalidate();
			rulesStep.revalidate();
			rulesStep.repaint();
		}
	}

	public void setParentWizardModel(NuclosEntityWizardStaticModel model) {
		this.parentWizardModel = model;
	}

	@Override
	public void close() {
		parentWizardModel = null;

		cbRuleTrigger = null;

		scrollStatements = null;
		jpnStatements = null;

		if (revalidatingComponents != null) {
			revalidatingComponents.clear();
		}
		revalidatingComponents = null;

		tblStatements = null;
		if (lstStatements != null) {
			lstStatements.clear();
		}
		lstStatements = null;

		lbInfo = null;

		clientRules = null;
		rule = null;

		if (otherAttributes != null) {
			otherAttributes.clear();
		}
		otherAttributes = null;
		if (foreignAttributes != null) {
			foreignAttributes.clear();
		}
		foreignAttributes = null;

		super.close();
	}

	private boolean validateStep() {
		boolean bValid = rule.getClearValueStatements().stream().noneMatch(stmt -> StringUtils.looksEmpty(stmt.getTargetEntityFieldId()));
		if (bValid) {
			bValid = rule.getTransferLookedupValueStatements().stream().noneMatch(stmt ->
					StringUtils.looksEmpty(stmt.getSourceEntityFieldId()) ||
							StringUtils.looksEmpty(stmt.getTargetEntityFieldId()));
		}
		if (bValid) {
			lbInfo.setText("");
		} else {
			lbInfo.setText(msg("wizard.step.attributeclientrules.7"));
		}
		setComplete(bValid);
		return bValid;
	}

	private boolean hasCurrentRuleContent() {
		if (this.clientRules == null || this.clientRules.getRules().isEmpty()) {
			return false;
		}
		EntityFieldClientRule rule = this.clientRules.getRules().get(0);
		return !rule.getClearValueStatements().isEmpty() ||
				!rule.getTransferLookedupValueStatements().isEmpty();
	}

	private boolean isStatementEmpty(EntityFieldClientRuleStatement stmt) {
		EntityFieldClientRuleStatement newInstance = (EntityFieldClientRuleStatement) stmt.createNewInstance();
		newInstance.setOrder(stmt.getOrder());
		return newInstance.equals(stmt);
	}

	@Override
	public void applyState() throws InvalidStateException {
		this.model.getAttribute().setClientRules(hasCurrentRuleContent() ? this.clientRules : null);
		super.applyState();
	}

	private static class EditorHelper {

		private final WeakReference<NuclosEntityAttributeClientRulesStep> weakStep;

		public EditorHelper(NuclosEntityAttributeClientRulesStep step) {
			this.weakStep = new WeakReference<>(step);
		}

		public void validate() {
			NuclosEntityAttributeClientRulesStep step = this.weakStep.get();
			if (step != null) {
				step.validateStep();
			}
		}

		public EntityMeta<?> getForeignEntityMeta() {
			NuclosEntityAttributeClientRulesStep step = this.weakStep.get();
			if (step != null) {
				return step.getModel().getAttribute().getMetaVO();
			}
			return null;
		}

		public List<FieldMeta<?>> getForeignAttributes() {
			NuclosEntityAttributeClientRulesStep step = this.weakStep.get();
			if (step != null) {
				return step.foreignAttributes;
			}
			return null;
		}

		public List<Attribute> getOtherAttributes() {
			NuclosEntityAttributeClientRulesStep step = this.weakStep.get();
			if (step != null) {
				return step.otherAttributes;
			}
			return null;
		}

		public EntityFieldClientRuleStatement getStatement(int iRow) {
			NuclosEntityAttributeClientRulesStep step = this.weakStep.get();
			if (step != null) {
				return step.lstStatements.get(iRow);
			}
			return null;
		}

	}

	private static class StatementEditor extends JPanel {

		private final int iRow;

		private final EditorHelper helper;

		private StatementEditor(final EditorHelper helper, final int iRow) {
			super(new BorderLayout());
			this.helper = helper;
			this.iRow = iRow;
		}

		@Override
		public void revalidate() {
			super.revalidate();
			if (helper != null) {
				EntityFieldClientRuleStatement stmt = helper.getStatement(iRow);
				if (stmt != null) {
					this.removeAll();
					if (stmt instanceof EntityFieldClientRuleStatementClearValue) {
						this.add(new ClearValueStatementEditor((EntityFieldClientRuleStatementClearValue) stmt, helper), BorderLayout.WEST);
					} else if (stmt instanceof EntityFieldClientRuleStatementTransferLookedupValue) {
						this.add(new TransferLookedupValueStatementEditor((EntityFieldClientRuleStatementTransferLookedupValue) stmt, helper), BorderLayout.WEST);
					}
				}
			}
		}
	}

	private static class ClearValueStatementEditor extends JPanel {

		private final WeakReference<EntityFieldClientRuleStatementClearValue> weakStmt;

		private ClearValueStatementEditor(EntityFieldClientRuleStatementClearValue stmt, EditorHelper helper) {
			this.weakStmt = new WeakReference<>(stmt);

			List<Attribute> attributes = helper.getOtherAttributes();
			if (attributes == null) {
				return;
			}

			JComboBox cbxAttributes = new JComboBox();
			cbxAttributes.addItem("");
			attributes.forEach(attr -> cbxAttributes.addItem(attr));
			if (stmt.getTargetEntityFieldId() != null) {
				UID uid = UID.parseUID(stmt.getTargetEntityFieldId());
				attributes.stream().filter(attr -> attr.getUID().equals(uid)).findFirst().ifPresent(attr -> cbxAttributes.setSelectedItem(attr));
			}
			cbxAttributes.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						Object selectedItem = e.getItem();
						EntityFieldClientRuleStatementClearValue stmt = weakStmt.get();
						if (stmt != null) {
							if (selectedItem instanceof Attribute) {
								stmt.setTargetEntityFieldId(((Attribute) selectedItem).getUID().getString());
							} else {
								stmt.setTargetEntityFieldId(null);
							}
							helper.validate();
						}
					}
				}
			});

			TableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED);
			String[] labelParts = msg("wizard.step.attributeclientrules.5", "cbxAttributes").split("cbxAttributes");
			tbllay.addLabel(labelParts[0]);
			tbllay.add(cbxAttributes);
			if (labelParts.length > 1) {
				tbllay.addLabel(labelParts[1]);
			}
		}
	}

	private static class TransferLookedupValueStatementEditor extends JPanel {

		private final WeakReference<EntityFieldClientRuleStatementTransferLookedupValue> weakStmt;

		private TransferLookedupValueStatementEditor(EntityFieldClientRuleStatementTransferLookedupValue stmt, EditorHelper helper) {
			this.weakStmt = new WeakReference<>(stmt);

			EntityMeta<?> sourceEntityMeta = helper.getForeignEntityMeta();
			List<FieldMeta<?>> sourceAttributes = helper.getForeignAttributes();
			List<Attribute> targetAttributes = helper.getOtherAttributes();
			if (sourceEntityMeta == null || sourceAttributes == null || targetAttributes == null) {
				return;
			}

			JComboBox cbxSources = new JComboBox();
			JComboBox cbxTargets = new JComboBox();
			cbxSources.addItem("");
			sourceAttributes.forEach(attr -> cbxSources.addItem(attr));

			cbxSources.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						Object selectedItem = e.getItem();
						EntityFieldClientRuleStatementTransferLookedupValue stmt = weakStmt.get();
						if (stmt != null) {
							if (selectedItem instanceof FieldMeta<?>) {
								FieldMeta<?> sourceFieldMeta = (FieldMeta<?>) selectedItem;
								stmt.setSourceEntityFieldId(sourceFieldMeta.getUID().getString());
								cbxTargets.removeAllItems();
								cbxTargets.addItem("");
								targetAttributes.stream().filter(attr -> {
									if (attr.isReadonly() || attr.getCalcFunction() != null || attr.getCalcAttributeDS() != null) {
										return false;
									}
									boolean bIsSameType = true;
									if (!LangUtils.equal(attr.getDatatyp().getJavaType(), sourceFieldMeta.getDataType())) {
										bIsSameType = false;
									}
									if (bIsSameType) {
										UID sourceForeignEntityUID = sourceFieldMeta.getForeignEntity();
										UID targetForeignEntityUID = attr.getMetaVO() == null ? null : attr.getMetaVO().getUID();
										if (!LangUtils.equal(targetForeignEntityUID, sourceForeignEntityUID)) {
											bIsSameType = false;
										}
									}
									return bIsSameType;
								}).forEach(attr -> cbxTargets.addItem(attr));
								if (stmt.getTargetEntityFieldId() != null) {
									UID uid = UID.parseUID(stmt.getTargetEntityFieldId());
									targetAttributes.stream().filter(attr -> attr.getUID().equals(uid)).findFirst().ifPresent(attr -> cbxTargets.setSelectedItem(attr));
								}
							} else {
								stmt.setSourceEntityFieldId(null);
							}
							helper.validate();
						}
					}
				}
			});

			if (stmt.getSourceEntityFieldId() != null) {
				UID uid = UID.parseUID(stmt.getSourceEntityFieldId());
				sourceAttributes.stream().filter(attr -> attr.getUID().equals(uid)).findFirst().ifPresent(attr -> cbxSources.setSelectedItem(attr));
			}

			cbxTargets.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(final ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						Object selectedItem = e.getItem();
						EntityFieldClientRuleStatementTransferLookedupValue stmt = weakStmt.get();
						if (stmt != null) {
							if (selectedItem instanceof Attribute) {
								stmt.setTargetEntityFieldId(((Attribute) selectedItem).getUID().getString());
							} else {
								stmt.setTargetEntityFieldId(null);
							}
							helper.validate();
						}
					}
				}
			 });

			TableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED);
			String[] labelParts = msg("wizard.step.attributeclientrules.6", "CBX", sourceEntityMeta.getEntityName(), "CBX").split("CBX");
			tbllay.addLabel(labelParts[0]);
			tbllay.add(cbxSources);
			if (labelParts.length > 1) {
				tbllay.addLabel(labelParts[1]);
			}
			tbllay.add(cbxTargets);
			if (labelParts.length > 2) {
				tbllay.addLabel(labelParts[2]);
			}
		}
	}
}
