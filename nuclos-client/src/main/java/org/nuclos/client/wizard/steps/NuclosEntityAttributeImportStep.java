package org.nuclos.client.wizard.steps;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.wizard.NuclosEntityWizardStaticModel;
import org.nuclos.client.wizard.NuclosWizardAbstractStep;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.DataTyp;
import org.nuclos.client.wizard.model.EntityAttributeSelectTableModel;
import org.nuclos.client.wizard.model.EntityAttributeTableModel;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

public class NuclosEntityAttributeImportStep extends NuclosWizardAbstractStep implements Closeable {

	private static final Logger LOG = Logger.getLogger(NuclosEntityOptionStep.class);

	private NuclosEntityWizardStaticModel model;
	private EntityAttributeTableModel attributeTableModel;
	private JLabel lbEntity;
	private JComboBox cbxEntity;
	private JScrollPane scrollFields;
	private JTable tblFields;

	public NuclosEntityAttributeImportStep(NuclosEntityWizardStaticModel model, String name, String summary) {
		super(name, summary);
		this.model = model;
		attributeTableModel = model.getAttributeModel();
		initComponents();
	}

	public void initComponents() {
		double size [][] = {{TableLayout.PREFERRED, TableLayout.FILL}, {20,20,20,TableLayout.FILL}};

		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		this.setLayout(layout);

		lbEntity = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityoption.16", "Bitte w\u00e4hlen Sie eine Entit\u00e4t aus")+":");

		cbxEntity = new JComboBox();
		tblFields = new JTable(new EntityAttributeSelectTableModel());
		tblFields.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tblFields.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblFields.getTableHeader().setReorderingAllowed(false);

		scrollFields = new JScrollPane(tblFields);

		cbxEntity.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(final ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					final Object obj = e.getItem();
					try {
						if(obj instanceof EntityMeta) {
							final EntityAttributeSelectTableModel model = new EntityAttributeSelectTableModel();

							final EntityMeta<?> vo = (EntityMeta<?>)obj;
							final Collection<FieldMeta<?>> fields = MetaProvider.getInstance().getAllEntityFieldsByEntity(vo.getUID()).values();

							final ArrayList<FieldMeta<?>> lstSorted = CollectionUtils.sorted(fields, new Comparator<FieldMeta<?>>() {
								@Override
								public int compare(FieldMeta o1, FieldMeta o2) {
									Integer order1 = (o1.getOrder()==null)?0:o1.getOrder();
									Integer order2 = (o2.getOrder()==null)?0:o2.getOrder();
									return order1.compareTo(order2);
								}
							});
							for(FieldMeta<?> field: lstSorted) {
								if(NuclosWizardUtils.isSystemField(field)) {
									continue;
								}
								boolean fieldNameAlreadyTaken = NuclosEntityAttributeImportStep.this.attributeTableModel.getNucletAttributes().stream()
										.anyMatch(attribute -> Objects.equals(field.getFieldName(), attribute.getField()));
								if (fieldNameAlreadyTaken) {
									continue;
								}

								Attribute attr = new Attribute(false);
								attr.setUID(field.getUID());
								if(attr.getUID() == null)
									attr.setUID(new UID());
								attr.setResume(true);
								attr.setLabel(SpringLocaleDelegate.getInstance().getResource(
										field.getLocaleResourceIdForLabel(), field.getFallbackLabel()));
								attr.setLabelResource(field.getLocaleResourceIdForLabel());
								attr.setDescription(SpringLocaleDelegate.getInstance().getResource(
										field.getLocaleResourceIdForDescription(), ""));
								attr.setDescriptionResource(field.getLocaleResourceIdForDescription());
								attr.setDistinct(field.isUnique());
								attr.setMandatory(!field.isNullable());
								attr.setLogBook(field.isLogBookTracking());
								attr.setHidden(field.isHidden());
								attr.setInternalName(field.getFieldName());
								attr.setDbName(field.getDbColumn());
								attr.setField(field.getFieldName());
								attr.setOldInternalName(field.getFieldName());
								attr.setSearchField(field.getSearchField());
								attr.setValueListProvider(field.isSearchable());
								attr.setModifiable(field.isModifiable());
								attr.setDefaultValue(field.getDefaultValue());
								attr.setCalcFunction(field.getCalcFunction());
								attr.setCalcAttributeDS(field.getCalcAttributeDS());
								attr.setCalcAttributeParamValues(field.getCalcAttributeParamValues());
								attr.setCalcAttributeAllowCustomization(field.isCalcAttributeAllowCustomization());
								attr.setCalcOndemand(field.isCalcOndemand());
								attr.setIsLocalized(field.isLocalized());
								attr.setOutputFormat(field.getFormatOutput());
								attr.setInputValidation(field.getFormatInput());
								attr.setIndexed(Boolean.TRUE.equals(field.isIndexed()));
								attr.setCalculationScript(field.getCalculationScript());
								attr.setBackgroundColorScript(field.getBackgroundColorScript());
								attr.setBackgroundColorDatasource(field.getColorDatasource());
								UID foreignEntity = field.getForeignEntity();
								UID lookupEntity = field.getLookupEntity();
								if (field.isFileDataType()) {
									attr.setDatatyp(NuclosWizardUtils.getDataTyp(field.getDataType(), field.getDefaultComponentType(),
											field.getScale(), field.getPrecision(), field.getFormatInput(),
											field.getFormatOutput()));
									attr.setField(field.getForeignEntityField());
									attr.setForeignIntegrationPoint(field.getForeignIntegrationPoint());
									EntityMeta<?> voForeignEntity = MetaProvider.getInstance().getEntity(foreignEntity);
									attr.setMetaVO(voForeignEntity);
								}
								else if(foreignEntity != null) {
									attr.setForeignIntegrationPoint(field.getForeignIntegrationPoint());
									EntityMeta<?> voForeignEntity = MetaProvider.getInstance().getEntity(foreignEntity);
									attr.setMetaVO(voForeignEntity);
									attr.setOnDeleteCascade(field.isOnDeleteCascade());
									attr.setField(field.getForeignEntityField());
									attr.setDatatyp(DataTyp.getReferenzTyp());
									if(!Modules.getInstance().isModule(foreignEntity) && field.getForeignEntityField() != null) {
										String sForeignField = field.getForeignEntityField();
										if(sForeignField.indexOf("uid{") >= 0) {
											attr.setDatatyp(DataTyp.getReferenzTyp());
										}
										else {
											FieldMeta<?> voField = MetaProvider.getInstance().getEntityField(UID.parseUID(field.getForeignEntityField()));
											attr.getDatatyp().setJavaType(voField.getDataType());
											if(voField.getPrecision() != null)
												attr.getDatatyp().setPrecision(voField.getPrecision());
											if(voField.getScale() != null)
												attr.getDatatyp().setScale(voField.getScale());
											if(voField.getFormatOutput() != null)
												attr.getDatatyp().setOutputFormat(voField.getFormatOutput());
										}
									}
									if(voForeignEntity.isFieldValueEntity()) {
										attr.setDatatyp(DataTyp.getDefaultStringTyp());
										attr.setValueListNew(false);
										NuclosEntityNameStep.loadValueList(attr);
									}
								} else if(lookupEntity != null) {
									attr.setLookupMetaVO(MetaProvider.getInstance().getEntity(lookupEntity));
									attr.setOnDeleteCascade(field.isOnDeleteCascade());
									attr.setField(field.getLookupEntityField());
									attr.setDatatyp(DataTyp.getLookupTyp());
									if(!Modules.getInstance().isModule(lookupEntity) && field.getLookupEntityField() != null) {
										String sLookupField = field.getLookupEntityField();
										if(sLookupField.indexOf("uid{") >= 0) {
											attr.setDatatyp(DataTyp.getLookupTyp());
										}
										else {
											FieldMeta voField = MetaProvider.getInstance().getEntityField(UID.parseUID(field.getLookupEntityField()));

											attr.getDatatyp().setJavaType(voField.getDataType());
											if(voField.getPrecision() != null)
												attr.getDatatyp().setPrecision(voField.getPrecision());
											if(voField.getScale() != null)
												attr.getDatatyp().setScale(voField.getScale());
											if(voField.getFormatOutput() != null)
												attr.getDatatyp().setOutputFormat(voField.getFormatOutput());
										}
									}
								} else {
									attr.setDatatyp(NuclosWizardUtils.getDataTyp(field.getDataType(), field.getDefaultComponentType(),
											field.getScale(), field.getPrecision(), field.getFormatInput(),
											field.getFormatOutput()));
								}

								attr.setAttributeGroup(field.getFieldGroup());

								model.addAttribute(attr);
							}
							SwingUtilities.invokeLater(new Runnable() {

								@Override
								public void run() {
									tblFields.setModel(model);
									TableUtils.setOptimalColumnWidths(tblFields);
								}
							});
						}
						else if(obj instanceof String) {
							EntityAttributeSelectTableModel model = new EntityAttributeSelectTableModel();
							tblFields.setModel(model);
						}
					}
					catch(Exception e1) {
						LOG.info("itemStateChanged failed: " + e1, e1);
					}
				}
			}
		});

		this.add(lbEntity, "0,0");
		this.add(cbxEntity, "0,1");
		this.add(scrollFields, "0,3 ,1,3");

		fillEntityCombobox();
	}

	private void fillEntityCombobox() {
		final List<EntityMeta<?>> lstMasterdata = new ArrayList<>(MetaProvider.getInstance().getAllEntities());

		Collections.sort(lstMasterdata, EntityUtils.getMetaComparator(EntityMeta.class));

		cbxEntity.addItem(EntityUtils.wrapMetaData(EntityMeta.NULL));

		for(EntityMeta<?> vo : lstMasterdata) {
			if(!E.isNuclosEntity(vo.getUID())) {
				if (vo.getUID() != model.getUID()) { // exclude the bo that is currently being edited
					cbxEntity.addItem(EntityUtils.wrapMetaData(vo));
				}
			}
		}
	}

	private boolean existAttribute(Attribute attr) {
		boolean exist = false;
		if(this.model.getAttributeModel() != null) {
			for(Attribute attrexist : this.model.getAttributeModel().getNucletAttributes()) {
				if(attrexist.getInternalName().equals(attr.getInternalName())) {
					exist = true;
				}
			}
		}
		return exist;
	}

	private void setTranslationForAttribute(Attribute attr, EntityAttributeTableModel model) {
		List<TranslationVO> lstTranslation = new ArrayList<TranslationVO>();

		for(LocaleInfo info : LocaleDelegate.getInstance().getAllLocales(false)){
			Map<String, String> mpValues = new HashMap<String, String>();
			mpValues.put(TranslationVO.LABELS_FIELD[0], LocaleDelegate.getInstance().getResourceByStringId(info, attr.getLabelResource()));
			mpValues.put(TranslationVO.LABELS_FIELD[1], LocaleDelegate.getInstance().getResourceByStringId(info, attr.getDescriptionResource()));
			TranslationVO voTranslation = new TranslationVO(info, mpValues);
			lstTranslation.add(voTranslation);
		}
		model.addTranslation(attr, lstTranslation);
	}

	@Override
	public void applyState() throws InvalidStateException {
		super.applyState();
		EntityAttributeSelectTableModel tableModel = (EntityAttributeSelectTableModel) tblFields.getModel();
		List<Attribute> lstAttribute = tableModel.getAttributes();
		EntityAttributeTableModel modelAttribute = new EntityAttributeTableModel(null);
		for(Attribute attr : lstAttribute) {
			if(attr.isForResume())
				modelAttribute.addAttribute(attr);
		}
		for(Attribute attr : modelAttribute.getNucletAttributes()) {
			if(!existAttribute(attr)) {
				attr.setUID(new UID());
				if (this.model.isEditMode()) {
					attr.setMandatory(false);
				}
				this.model.getAttributeModel().addAttribute(attr);
				setTranslationForAttribute(attr, this.model.getAttributeModel());
				attr.setLabelResource(null);
				attr.setDescriptionResource(null);
			}
		}
	}

	@Override
	public void close() {
		model = null;
		attributeTableModel = null;
		lbEntity = null;
		cbxEntity = null;
		scrollFields = null;
		tblFields = null;
	}

}
