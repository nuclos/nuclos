//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.i18n.language.data.DataLanguageDelegate;
import org.nuclos.client.i18n.language.data.DataLanguageHelper;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.EntityTranslationTableModel;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SFE;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
* 
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosEntityTranslationStep extends NuclosEntityAbstractStep {

	private JScrollPane scrolPane;
	private JTable attributeTable;
	private JLabel lblRefEntityOutputFormat;
	private JComboBox cmbRefEntityOutputFormat;
	
	private List<Attribute> lstAttribute;
	
	private EntityTranslationTableModel tablemodel;

	public NuclosEntityTranslationStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	protected void initComponents() {
		double size [][] = {{TableLayout.FILL, 130,130, TableLayout.FILL, 10}, {300, 300}};
		
		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		setLayout(layout);
		
		tablemodel = new EntityTranslationTableModel();
		
		final List<TranslationVO> lstTranslation = new ArrayList<TranslationVO>();
		
		for(LocaleInfo voLocale : loadLocales()) {
			final Map<String, String> map = new HashMap<String, String>();
			final TranslationVO translation = new TranslationVO(voLocale, map);
			for(String sLabel : TranslationVO.LABELS_ENTITY) {									
				translation.getLabels().put(sLabel, "");
			}
			lstTranslation.add(translation);
		}
		tablemodel.setRows(lstTranslation);
		
		attributeTable = new JTable(tablemodel);
		attributeTable.getTableHeader().setReorderingAllowed(false);
		lblRefEntityOutputFormat = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.79", "Referenz zur Ausgabesprache"));
		cmbRefEntityOutputFormat = new JComboBox();
		cmbRefEntityOutputFormat.setRenderer(new DefaultListCellRenderer() {
			
			 public Component getListCellRendererComponent(
				        JList list,
				        Object value,
				        int index,
				        boolean isSelected,
				        boolean cellHasFocus)
		    {
				
				setForeground(isSelected ? list.getSelectionForeground() : list.getForeground());				
				setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
				
				if (value != null) {
					String sValue = value.toString().trim();
					setText(!sValue.equals("") ? getNamedFields(( (String) value).split(";")): "");					 
				}					
				
				setPreferredSize(new Dimension(0, 16));				 
				return this;
		    }

			
		});
		cmbRefEntityOutputFormat.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED && e.getItem() != null) {
					String value = cmbRefEntityOutputFormat.getSelectedItem().toString().trim();
					if (value.isEmpty()) {
						model.setDataLangRefPath(null);
					} else {
						model.setDataLangRefPath(value);
					}
				}
			}
		});
		cmbRefEntityOutputFormat.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.79", 
				"Wählen Sie hier die referenzierte Entität, die die Ausgabesprache z.B. beim Druck von Formularen festlegt."));
		
		JTextField txtField = new JTextField();
		txtField.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		DefaultCellEditor editor = new DefaultCellEditor(txtField);
		editor.setClickCountToStart(1);
		
		for(TableColumn col : CollectionUtils.iterableEnum(attributeTable.getColumnModel().getColumns()))
			col.setCellEditor(editor);
		
		attributeTable.getTableHeader().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				stopCellEditing();
			}
		});
		
		scrolPane = new JScrollPane(attributeTable);
		
		add(scrolPane, new TableLayoutConstraints(0, 0, 3, 0));
		

		JPanel pnlLocalisation = new JPanel(new GridBagLayout());
	
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 0, 5, 10);
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0;
		c.weighty = 1;
		c.anchor = GridBagConstraints.NORTH;
		pnlLocalisation.add(lblRefEntityOutputFormat, c);
		c.insets = new Insets(10, 0, 0, 0);
		c.gridx = 1;
		c.weightx = 1;
		pnlLocalisation.add(cmbRefEntityOutputFormat, c);
		
		this.add(pnlLocalisation, new TableLayoutConstraints(0, 1, 3, 1));
		
	}
	
	private String getNamedFields(String[] split) {
		
		StringBuilder sb = new StringBuilder();
		
		for (int idx=0; idx < split.length; idx++) {
			String val = split[idx];
			if (idx == 0) {
				sb.append(this.model.getAttributeModel().getAttributeNameByUID(new UID(val)));
			} else {
				String attributeNameByUID = this.model.getAttributeModel().getAttributeNameByUID(new UID(val));
				sb.append(" -> " + (attributeNameByUID == null ? MetaProvider.getInstance().getEntityField(new UID(val)).getFieldName() : attributeNameByUID));
			}
		}
		return sb.toString();
	}

	
	private boolean hasLocalizedFields() {
		boolean retVal = false;
		
		for (Attribute a : this.model.getAttributeModel().getNucletAttributes()) {
			if (a.isLocalized()) {
				retVal = true;
				break;
			}
		}
		
		return retVal;
	}
	private Collection<LocaleInfo> loadLocales() {
		return LocaleDelegate.getInstance().getAllLocales(false);
	}
	
	private void stopCellEditing() {
        for(TableColumn col : CollectionUtils.iterableEnum(attributeTable.getColumnModel().getColumns())) {
        	TableCellEditor cellEditor = col.getCellEditor();
			if(cellEditor != null)
        		cellEditor.stopCellEditing();
        }
	}

	@Override
	public void prepare() {
		super.prepare();		
		
		lstAttribute = new ArrayList<Attribute>(model.getAttributeModel().getNucletAttributes());

		try {
	        lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.CHANGEDAT.getMetaData(model.getUID()), true));
	        lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.CHANGEDBY.getMetaData(model.getUID()), true));
	        lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.CREATEDAT.getMetaData(model.getUID()), true));
	        lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.CREATEDBY.getMetaData(model.getUID()), true));
	        if(this.model.isStateModel()) {
	        	lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.STATENUMBER.getMetaData(model.getUID()), true));
	        	lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.STATEICON.getMetaData(model.getUID()), true));
	        	lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.STATE.getMetaData(model.getUID()), true));
	        	lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SFE.PROCESS.getLimitedMetaData(model.getUID()), true));
	        	lstAttribute.add(NuclosEntityNameStep.wrapEntityMetaFieldVO(SF.SYSTEMIDENTIFIER.getMetaData(model.getUID()), true));
	        }
        }
        catch(CommonFinderException e) {
	        throw new CommonFatalException(e);
        }
        catch(CommonPermissionException e) {
        	throw new CommonFatalException(e);
        }

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				attributeTable.requestFocusInWindow();
			}
		});

		final List<TranslationVO> lst = model.getTranslation();
		if(lst != null && lst.size() > 0) {
			tablemodel.setRows(lst);
			for(LocaleInfo voLocale : loadLocales()) {
				final String sLocaleLabel = voLocale.getLanguage(); 
				tablemodel.getTranslationByName(sLocaleLabel).getLabels().put(TranslationVO.LABELS_ENTITY[2], 
						NuclosEntityTreeValueStep.transformStringFromUid(lstAttribute, 
								tablemodel.getTranslationByName(sLocaleLabel).getLabels().get(TranslationVO.LABELS_ENTITY[2])));
				tablemodel.getTranslationByName(sLocaleLabel).getLabels().put(TranslationVO.LABELS_ENTITY[3], 
						NuclosEntityTreeValueStep.transformStringFromUid(lstAttribute, 
								 tablemodel.getTranslationByName(sLocaleLabel).getLabels().get(TranslationVO.LABELS_ENTITY[3])));
			}
			return;
		}
		
		if (DataLanguageDelegate.getInstance().getPrimaryDataLanguage() != null) {
			lblRefEntityOutputFormat.setVisible(true);
			cmbRefEntityOutputFormat.setEnabled(true);
		}
	}

	public void initRefEntityOutputFormat(boolean localizedAttsFound, List<Attribute> atts) {
		if(localizedAttsFound) {
			ItemListener[] itemListeners = cmbRefEntityOutputFormat.getItemListeners();
			
			for (ItemListener l : itemListeners) {
				cmbRefEntityOutputFormat.removeItemListener(l);
			}
		
			cmbRefEntityOutputFormat.removeAllItems();
			cmbRefEntityOutputFormat.addItem("");
			for (String s : DataLanguageHelper.getDataLanguageReferencePath(atts)) {
				cmbRefEntityOutputFormat.addItem(s);
			}
			
			if (this.model.getDataLangRefPath() != null && 
					( (DefaultComboBoxModel)cmbRefEntityOutputFormat.getModel()).getIndexOf(this.model.getDataLangRefPath()) >= 0) {
				cmbRefEntityOutputFormat.setSelectedItem(this.model.getDataLangRefPath());
			} else {
				cmbRefEntityOutputFormat.setSelectedItem(null);
			}
			
			if (!cmbRefEntityOutputFormat.isEnabled()) {
				cmbRefEntityOutputFormat.setEnabled(true);
			}
		
			for (ItemListener l : itemListeners) {
				cmbRefEntityOutputFormat.addItemListener(l);
			}
			
			cmbRefEntityOutputFormat.setVisible(cmbRefEntityOutputFormat.isEnabled());
			lblRefEntityOutputFormat.setVisible(cmbRefEntityOutputFormat.isEnabled());
			
		} else {

			cmbRefEntityOutputFormat.setEnabled(false);
			cmbRefEntityOutputFormat.setSelectedItem(null);
			cmbRefEntityOutputFormat.setVisible(false);
			lblRefEntityOutputFormat.setVisible(false);
		} 
			
	}
	
	@Override
	public void close() {
		scrolPane = null;
		attributeTable = null;
		
		tablemodel = null;
		
		super.close();
	}

	@Override
	public void applyState() throws InvalidStateException {
		stopCellEditing();
		model.setTranslation(tablemodel.getRows());
		boolean delResource = checkTranslationForNull(TranslationVO.LABELS_ENTITY[0]);
		if(delResource)
			model.setLabelSingular(null);
		
		delResource = checkTranslationForNull(TranslationVO.LABELS_ENTITY[1]);
		if(delResource)
			model.setMenuPathResource(null);
		
		delResource = checkTranslationForNull(TranslationVO.LABELS_ENTITY[2]);
		if(delResource)
			model.setNodeLabelResource(null);
		
		delResource = checkTranslationForNull(TranslationVO.LABELS_ENTITY[3]);
		if(delResource)
			model.setNodeTooltipResource(null);

		delResource = checkTranslationForNull(TranslationVO.LABELS_ENTITY[4]);
		if(delResource)
			model.setProcessResource(null);

		delResource = checkTranslationForNull(TranslationVO.LABELS_ENTITY[5]);
		if(delResource)
			model.setProcessDescriptionResource(null);

		delResource = checkTranslationForNull(TranslationVO.LABELS_ENTITY[6]);
		if(delResource)
			model.setObjgenListingsResource(null);
		
		super.applyState();
	}

	private boolean checkTranslationForNull(String key) {
	    boolean delResource = false;
		for(LocaleInfo voLocale : loadLocales()) {
			String sLocale = voLocale.getLanguage();
			String sLabel = tablemodel.getTranslationByName(sLocale).getLabels().get(key);
			tablemodel.getTranslationByName(sLocale).getLabels().put(key, 
					NuclosEntityTreeValueStep.transformStringToUid(lstAttribute, sLabel));
			sLabel = tablemodel.getTranslationByName(sLocale).getLabels().get(key);
			if(sLabel == null || sLabel.length() == 0)
				delResource = true;				
			else 
				delResource = false;
		}
	    return delResource;
    }
}
