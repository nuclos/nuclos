//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.nuclos.client.rest.RestClient;
import org.nuclos.common.http.NuclosHttpClientFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ClassUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RemotingClassListDownloader implements TypePreservingTypeIdResolver.IRemotingClassListDownloader {

	private static final Logger LOG = LoggerFactory.getLogger(RemotingClassListDownloader.class);

	private TypePreservingTypeIdResolver.RemotingClassLists remotingClassLists = null;

	private IOException ioException = null;

	/**
	 * Loads list of classes allowed for remoting from server. The creation of this list in the client takes 1-2 minutes for signed jars
	 * (see also https://github.com/spring-projects/spring-framework/issues/9371), is possible, but only a fallback.
	 */
	RemotingClassListDownloader() {
		final HttpClient httpClient = NuclosHttpClientFactory.getInstance().getObject();
		final URL restUrl = RestClient.getRestUrl();
		try {
			HttpResponse response = httpClient.execute(new HttpGet(restUrl + "/remotingclasslists"));
			HttpEntity entity = response.getEntity();
			try (InputStream stream = entity.getContent()) {
				remotingClassLists = new ObjectMapper().readValue(stream, TypePreservingTypeIdResolver.RemotingClassLists.class);
			}
		} catch (IOException ex) {
			this.ioException = ex;
		}
	}

	@Override
	public Collection<Class<?>> getRegularRemotingClasses() throws IOException, ClassNotFoundException {
		if (ioException != null) {
			throw ioException;
		}
		return getClassList(remotingClassLists.regularClasses);
	}

	@Override
	public Collection<Class<?>> getInterfaceOrAbstractRemotingClasses() throws IOException, ClassNotFoundException {
		if (ioException != null) {
			throw ioException;
		}
		return getClassList(remotingClassLists.interfaceOrAbstractClasses);
	}

	private static Collection<Class<?>> getClassList(String[] qualifiedNames) throws ClassNotFoundException {
		List<Class<?>> result = new ArrayList<>();
		for (String name : qualifiedNames) {
			try {
				result.add(ClassUtils.forName(name, Thread.currentThread().getContextClassLoader()));
			} catch (ClassNotFoundException ex) {
				LOG.warn(String.format("Class \"%s\" not found for remoting purposes.", name));
			}
		}
		return result;
	}

}
