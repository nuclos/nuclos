//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.Collection;

import org.nuclos.common.collection.Transformer;
import org.nuclos.server.attribute.valueobject.AttributeCVO;

/**
 * Provides <code>AttributeCVO</code>s by their ids or their names.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 * 
 * @deprecated AttributeCVO is deprecated, hence this is deprecated, too.
 */
public interface AttributeProvider {

	/**
	 * §postcondition result != null
	 * @param attributeUid
	 * @return the AttributeCVO with the given id.
	 * @throws NuclosAttributeNotFoundException if there is no attribute with the given id.
	 */
	AttributeCVO getAttribute(UID attributeUid) throws NuclosAttributeNotFoundException;

	FieldMeta getEntityField(UID field) throws NuclosAttributeNotFoundException;
	
	/**
	 * get all attributes for this provider
	 * @return Collection&lt;AttributeCVO&gt; of attribute vos
	 */
	Collection<AttributeCVO> getAttributes();

	/**
	 * Transformer that gets an attribute by id.
	 * 
	 * @deprecated AttributeProvider is deprecated, hence this is deprecated, too.
	 */
	public static class GetAttributeByUID implements Transformer<UID, AttributeCVO> {
		private final AttributeProvider attrprovider;

		/**
		 * @deprecated AttributeProvider is deprecated, hence this is deprecated, too.
		 */
		public GetAttributeByUID(AttributeProvider attrprovider) {
			this.attrprovider = attrprovider;
		}

		@Override
		public AttributeCVO transform(UID attribute) {
			return this.attrprovider.getAttribute(attribute);
		}
	}

}	// interface AttributeProvider
