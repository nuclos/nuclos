//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import static org.nuclos.common.ConsoleCommand.CHECK_DATA_SOURCES;
import static org.nuclos.common.ConsoleCommand.CHECK_DOCUMENT_ATTACHMENTS;
import static org.nuclos.common.ConsoleCommand.CLEANUP_DUPLICATE_DOCUMENTS;
import static org.nuclos.common.ConsoleCommand.CLEAR_USER_PREFERENCES;
import static org.nuclos.common.ConsoleCommand.COMPILE_DB_OBJECTS;
import static org.nuclos.common.ConsoleCommand.DROP_CONSTRAINTS;
import static org.nuclos.common.ConsoleCommand.FREE_DAL_MEMORY;
import static org.nuclos.common.ConsoleCommand.GET_CONSTRAINTS;
import static org.nuclos.common.ConsoleCommand.INVALIDATE_ALL_CACHES;
import static org.nuclos.common.ConsoleCommand.KILL_SESSION;
import static org.nuclos.common.ConsoleCommand.MIGRATE_DOCUMENT_ATTACHMENTS_INTO_SUBDIRECTORIES;
import static org.nuclos.common.ConsoleCommand.MIGRATE_JASPER_REPORTS;
import static org.nuclos.common.ConsoleCommand.MIGRATE_SUBFORM_TO_ON_DELETE_CASCADE_CONSTRAINT;
import static org.nuclos.common.ConsoleCommand.QUERY_DAL_MEMORY_LOAD;
import static org.nuclos.common.ConsoleCommand.REBUILD_CLASSES;
import static org.nuclos.common.ConsoleCommand.REBUILD_CONSTRAINTS;
import static org.nuclos.common.ConsoleCommand.REBUILD_CONSTRAINTS_AND_INDEXES;
import static org.nuclos.common.ConsoleCommand.REBUILD_INDEXES;
import static org.nuclos.common.ConsoleCommand.SEND_MESSAGE;
import static org.nuclos.common.ConsoleCommand.SET_DATA_LANGUAGE;
import static org.nuclos.common.ConsoleCommand.SET_MANDATOR_LEVEL;
import static org.nuclos.common.ConsoleCommand.SET_SIDEVIEW_MENU_WIDTH;
import static org.nuclos.common.ConsoleCommand.SHOW_MAINTENANCE;
import static org.nuclos.common.ConsoleCommand.SOURCE_SCANNER_RUN_ONCE;
import static org.nuclos.common.ConsoleCommand.START_MAINTENANCE;
import static org.nuclos.common.ConsoleCommand.STOP_MAINTENANCE;
import static org.nuclos.common.ConsoleCommand.SYNC_ACTIVE_WEBADDONS;

import java.io.ByteArrayOutputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.fileimport.CommonParseException;
import org.nuclos.server.common.ejb3.PreferencesFacadeRemote;
import org.nuclos.server.console.ejb3.ConsoleFacadeRemote;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeRemote;
import org.nuclos.server.i18n.DataLanguageFacadeRemote;
import org.nuclos.server.i18n.DataLanguageVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Management console for Nuclos.
 */
public class CommonConsole {

	private static final Logger LOG = LoggerFactory.getLogger(CommonConsole.class);

	private PreferencesFacadeRemote preferencesFacadeRemote = SpringApplicationContextHolder.getBean(PreferencesFacadeRemote.class);

	private ConsoleFacadeRemote consoleFacadeRemote = SpringApplicationContextHolder.getBean(ConsoleFacadeRemote.class);

	private EventSupportFacadeRemote eventSupportFacadeRemote = SpringApplicationContextHolder.getBean(EventSupportFacadeRemote.class);

	private IMetaProvider metaProvider = SpringApplicationContextHolder.getBean(IMetaProvider.class);
	
	private PrintStream out = null;
	private PrintStream err = null;

	public String getUsage() {
		final StringBuilder sb = new StringBuilder();

		TreeMap<String, String[]> sortedUsage = new TreeMap<>();
		addUsage(sortedUsage, INVALIDATE_ALL_CACHES);
		addUsage(sortedUsage, REBUILD_CLASSES);
		addUsage(sortedUsage, FREE_DAL_MEMORY);
		addUsage(sortedUsage, QUERY_DAL_MEMORY_LOAD);
		addUsage(sortedUsage, CLEAR_USER_PREFERENCES);
		addUsage(sortedUsage, CHECK_DATA_SOURCES);
		addUsage(sortedUsage, SEND_MESSAGE);
		addUsage(sortedUsage, KILL_SESSION);
		addUsage(sortedUsage, COMPILE_DB_OBJECTS);
		addUsage(sortedUsage, GET_CONSTRAINTS);
		addUsage(sortedUsage, DROP_CONSTRAINTS);
		addUsage(sortedUsage, REBUILD_CONSTRAINTS);
		addUsage(sortedUsage, REBUILD_INDEXES);
		addUsage(sortedUsage, REBUILD_CONSTRAINTS_AND_INDEXES);
		addUsage(sortedUsage, SET_MANDATOR_LEVEL);
		addUsage(sortedUsage, CHECK_DOCUMENT_ATTACHMENTS);
		addUsage(sortedUsage, CLEANUP_DUPLICATE_DOCUMENTS);
		addUsage(sortedUsage, MIGRATE_DOCUMENT_ATTACHMENTS_INTO_SUBDIRECTORIES);
		addUsage(sortedUsage, SHOW_MAINTENANCE);
		addUsage(sortedUsage, START_MAINTENANCE);
		addUsage(sortedUsage, STOP_MAINTENANCE);
		addUsage(sortedUsage, SOURCE_SCANNER_RUN_ONCE);
		addUsage(sortedUsage, SYNC_ACTIVE_WEBADDONS);
		addUsage(sortedUsage, MIGRATE_JASPER_REPORTS);
		addUsage(sortedUsage, MIGRATE_SUBFORM_TO_ON_DELETE_CASCADE_CONSTRAINT);
		addUsage(sortedUsage, SET_SIDEVIEW_MENU_WIDTH);

		for (String sCommand : sortedUsage.keySet()) {
			sb.append("\t").append(sCommand).append("\n");
			String[] sUsageLines = sortedUsage.get(sCommand);
			for (String sLine : sUsageLines) {
				sb.append("\t\t").append(sLine).append("\n");
			}
		}

		return sb.toString();
	}

	private static void addUsage(TreeMap<String, String[]> sortedUsage, ConsoleCommand cmd) {
		sortedUsage.put(cmd.getUsage(), cmd.getDescriptionLines());
	}

	public CommonConsole(PipedOutputStream out) {
		this(out, out);
	}

	public CommonConsole(PipedOutputStream out, PipedOutputStream err) {
		this.out = new PrintStream(out, true);
		this.err = new PrintStream(err, true);
	}

	/**
	 * directly called by GUI (already logged in) - called by console's main method
	 *
	 * @param asArgs
	 */
	public void parseAndInvoke(String[] asArgs) throws Exception {
		final String sCommand;
		if (asArgs != null && asArgs.length > 0) {
			sCommand = asArgs[0];
		} else {
			sCommand = null;
		}
		parseAndInvoke(sCommand, asArgs);
	}

	/**
	 * @param sCommand
	 * @param sArguments
	 * @throws Exception
	 */
	public void parseAndInvoke(final String sCommand, final String sArguments) throws Exception {
		// asArgs
		final List<String> lstAsArgs = new ArrayList<>();
		if (sCommand != null) {
			lstAsArgs.add(sCommand);
		}
		// Added handling of blanks in arguments, i.g. for path and file names
		if (sArguments != null) {
			StringTokenizer stArgument = new StringTokenizer(sArguments, "\"", true);
			boolean bHasSpaceCharacters = false;
			String sArgumentContent;

			while (stArgument.hasMoreTokens()) {
				sArgumentContent = stArgument.nextToken();
				if (sArgumentContent.equals("\"")) {
					bHasSpaceCharacters = !bHasSpaceCharacters;
				} else {
					if (bHasSpaceCharacters) {
						lstAsArgs.add(sArgumentContent);
					} else {
						StringTokenizer stToken = new StringTokenizer(sArgumentContent, " ");
						while (stToken.hasMoreTokens()) {
							lstAsArgs.add(stToken.nextToken());
						}
					}
				}
			}
		}

		parseAndInvoke(sCommand, lstAsArgs.toArray(new String[]{}));
	}

	protected void parseAndInvoke(final String sCommand, final String[] asArgs)
			throws CommonBusinessException, SQLException, InterruptedException {

		final long lStartTime = new Date().getTime();
		if (sCommand == null) {
			outPrintln(getUsage());
			return;
		}

		final ConsoleCommand constant = ConsoleCommand.fromValue(sCommand);

		// TODO: Implement this generically, get rid of huge switch block!
		switch (constant) {
			case GENERATE_BO_UID_LIST:
				generateBoUidList(asArgs);
				break;
			case CLEAR_USER_PREFERENCES:
				if (asArgs.length < 2) {
					outPrintln(getUsage());
				}
				final String sUserName = asArgs[1];
				clearUserPreferences(sUserName);
				break;
			case COMPILE_DB_OBJECTS:
				compileDbObjects();
				break;
			case GET_CONSTRAINTS:
				Set<String> tables = new HashSet<>();
				if (asArgs.length > 1 && "-tables".equals(asArgs[1])) {
					for (int i = 2; i < asArgs.length; i++) {
						tables.add(asArgs[i]);
					}
				}
				getConstraints(tables);
				break;
			case DROP_CONSTRAINTS:
				Set<String> constraintToDrop = new HashSet<>();
				if (asArgs.length > 1) {
					for (int i = 1; i < asArgs.length; i++) {
						constraintToDrop.add(asArgs[i]);
					}
				}
				dropConstraints(constraintToDrop);
				break;
			case REBUILD_CONSTRAINTS:
				rebuildConstraints();
				break;
			case REBUILD_INDEXES:
				rebuildIndexes();
				break;
			case REBUILD_CONSTRAINTS_AND_INDEXES:
				rebuildConstraints();
				rebuildIndexes();
				break;
			case SET_MANDATOR_LEVEL:
				setMandatorLevel(asArgs);
				break;
			case SET_DATA_LANGUAGE:
				setDataLanguage(asArgs);
				break;
			case SEND_MESSAGE:
				sendMessage(asArgs);
				break;
			case KILL_SESSION:
				killSession(asArgs);
				break;
			case INVALIDATE_ALL_CACHES:
				invalidateAllCaches();
				break;
			case FREE_DAL_MEMORY:
				freeDalMemory();
				break;
			case QUERY_DAL_MEMORY_LOAD:
				queryDalMemoryLoad();
				break;
			case REBUILD_CLASSES:
				rebuildClasses();
				break;
			case SOURCE_SCANNER_RUN_ONCE:
				sourceScannerRunOnce();
				break;
			case MIGRATE_DOCUMENT_ATTACHMENTS_INTO_SUBDIRECTORIES:
				long iChunkSize = 100L;
				long iMaxChunksToMigrate = 0L;
				List<String> ignoredExtensions = new ArrayList<>();
				for (int i = 1; i < asArgs.length; i++) {
					if (asArgs[i].equalsIgnoreCase("-chunkSize")) {
						if (asArgs.length > i+1) {
							iChunkSize = Long.parseLong(asArgs[i+1]);
						}
					} else if (asArgs[i].equalsIgnoreCase("-maxChunksToMigrate")) {
						if (asArgs.length > i+1) {
							iMaxChunksToMigrate = Long.parseLong(asArgs[i+1]);
						}
					} else if (asArgs[i].equalsIgnoreCase("-ignoreExtension")) {
						// primary for testing (undocumented)
						if (asArgs.length > i+1) {
							ignoredExtensions.add(asArgs[i+1]);
						}
					}
				}
				migrateDocumentAttachmentsIntoSubDirs(iChunkSize, iMaxChunksToMigrate, ignoredExtensions.toArray(new String[]{}));
				break;
			case CHECK_DOCUMENT_ATTACHMENTS:
				boolean deleteUnusedFiles = false;
				Set<String> deleteEntityObjects = new HashSet<>();
				if (asArgs.length > 1) {
					for (int i = 1; i < asArgs.length; i++) {
						if (asArgs[i].equalsIgnoreCase("-deleteUnusedFiles")) {
							deleteUnusedFiles = true;
						} else {
							if (!asArgs[i].equalsIgnoreCase("-deleteDatabaseRecords")) {
								deleteEntityObjects.add(asArgs[i]);
							}
						}
					}
				}
				checkDocumentAttachments(deleteUnusedFiles, deleteEntityObjects);
				break;
			case CHECK_DATA_SOURCES:
				checkDatasources();
				break;
			case CLEANUP_DUPLICATE_DOCUMENTS:
				cleanupDuplicateDocuments();
				break;
			case RESET_COMPLETE_MANDATOR_SETUP:
				resetCompleteMandatorSetup();
				break;
			case DISABLE_INDEXER:
				consoleFacadeRemote.setIndexerEnabled(false, false);
				outPrintln("Indexer disabled.");
				break;
			case ENABLE_INDEXER:
				consoleFacadeRemote.setIndexerEnabled(true, false);
				outPrintln("Indexer enabled.");
				break;
			case ENABLE_INDEXER_SYNCHRONOUSLY:
				consoleFacadeRemote.setIndexerEnabled(true, true);
				outPrintln("Indexer enabled synchronously.");
				break;
			case REBUILD_LUCENE_INDEX:
				consoleFacadeRemote.rebuildLuceneIndex();
				outPrintln("Rebuilding Lucene index.");
				break;
			case SHOW_MAINTENANCE:
				outPrintln(consoleFacadeRemote.getMaintenanceMode());
				break;
			case START_MAINTENANCE:
				outPrintln("Starting maintenance: " + consoleFacadeRemote.startMaintenance(asArgs));
				break;
			case STOP_MAINTENANCE:
				consoleFacadeRemote.stopMaintenance();
				outPrintln("Maintenance stopped");
				break;
			case SYNC_ACTIVE_WEBADDONS:
				consoleFacadeRemote.syncActiveWebAddons();
				outPrintln("Sync active webaddons");
				break;
			case MIGRATE_JASPER_REPORTS:
				migrateJasperReports(asArgs);
				break;
			case MIGRATE_SUBFORM_TO_ON_DELETE_CASCADE_CONSTRAINT:
				if (asArgs.length > 2 || (asArgs.length > 1 && !"-p".equals(asArgs[1]))) {
					outPrintln(getUsage());
					break;
				}

				boolean bPretend = asArgs.length > 1 && "-p".equals(asArgs[1]);
				outPrintln("Migrate subform usages to \"on delete cascade constraints\"");
				outPrintln(Arrays.stream(
						consoleFacadeRemote.migrateSubformToOnDeleteCascadeConstraint(bPretend))
						.collect(Collectors.joining("\n")));
				if (!bPretend) {
					outPrintln("Invalidate caches");
					consoleFacadeRemote.invalidateAllCaches();
					outPrintln("Rebuild constraints");
					consoleFacadeRemote.rebuildConstraints();
				}
				break;
			case SET_SIDEVIEW_MENU_WIDTH:
				if (asArgs.length < 2 || asArgs.length > 4 || (asArgs.length == 3 && !"-s".equals(asArgs[2]) && !"-p".equals(asArgs[2])) || (asArgs.length == 4 && !"-l".equals(asArgs[2]))) {
					outPrintln(getUsage());
					break;
				}

				int width = Integer.valueOf(asArgs[1]);
				boolean bShared = asArgs.length == 3 && "-s".equals(asArgs[2]);
				boolean bPrivate = asArgs.length == 3 && "-p".equals(asArgs[2]);
				UID[] prefs = asArgs.length == 4 ? Arrays.stream(asArgs[3].split(",")).map(sUid -> new UID(sUid)).collect(Collectors.toList()).toArray(new UID[0]) : null;
				outPrintln("Set sideview menu width to " + width);
				outPrintln(Arrays.stream(
						consoleFacadeRemote.setSideviewMenuWidth(width, bShared, bPrivate, prefs))
						.collect(Collectors.joining("\n")));
				break;
			case FIX_RECORDGRANT_PK_ALIAS:
				outPrintln(Arrays.stream(consoleFacadeRemote.fixRecordGrantPkAlias()).collect(Collectors.joining("\n")));
				break;
			default:
				throw new CommonParseException("Unknown command: " + sCommand + "\n" + getUsage());
		}

		outPrintln("NuclosConsole finished in " + (DateUtils.now().getTime() - lStartTime) + " ms");
	}

	private void killSession(final String[] asArgs) {
		String sUser = null;
		if (asArgs.length >= 2) {
			for (int i = 1; i < asArgs.length; i++) {
				if (asArgs[i].equalsIgnoreCase("-user") && i + 1 < asArgs.length) {
					sUser = asArgs[i + 1];
				}
			}
		}
		killSession(sUser);
	}

	private void sendMessage(final String[] asArgs) throws CommonParseException {
		String sMessage = null;
		String sUser = null;
		String sAuthor = "Administrator";
		Priority priority = Priority.HIGH;

		if (asArgs.length >= 2) {
			for (int i = 1; i < asArgs.length; i++) {
				if (asArgs[i].equalsIgnoreCase("-user") && i + 1 < asArgs.length) {
					sUser = asArgs[i + 1];
				} else if (asArgs[i].equalsIgnoreCase("-priority") && i + 1 < asArgs.length) {
					String sPrio = asArgs[i + 1];
					if (sPrio.equalsIgnoreCase("hoch") || sPrio.equalsIgnoreCase("high")) {
						priority = Priority.HIGH;
					} else if (sPrio.equalsIgnoreCase("normal")) {
						priority = Priority.NORMAL;
					} else if (sPrio.equalsIgnoreCase("niedrig") || sPrio.equalsIgnoreCase("low")) {
						priority = Priority.LOW;
					} else {
						throw new CommonParseException(
								"Unknown priority \"" + sPrio + "\". Use one of: " + Priority.HIGH + ", " + Priority.NORMAL + ", " + Priority.LOW);
					}
				} else if (asArgs[i].equalsIgnoreCase("-message") && i + 1 < asArgs.length) {
					sMessage = asArgs[i + 1];
				} else if (asArgs[i].equalsIgnoreCase("-author") && i + 1 < asArgs.length) {
					sAuthor = asArgs[i + 1];
				}
			}

			if (sMessage != null) {
				sendMessage(sMessage, sUser, priority, sAuthor);
			} else {
				throw new CommonParseException("Missing argument -message\n");
			}
		} else {
			throw new CommonParseException("Missing arguments for sendMessage\n" + getUsage());
		}
	}

	private void generateBoUidList(String[] args) {

		String wordseparator = args.length > 1 ? args[1] : "";

		outPrintln("// Generating BO UID list ...");
		StringBuilder builder = new StringBuilder();

		List<EntityObjectVO<UID>> nuclets = metaProvider.getNuclets();
		builder
				.append("/** ").append(new Date()).append(" */\n")
				.append("import org.nuclos.common.UID;\n\n");

		builder.append("public final class BoUidMapping {\n\n");


		for (EntityObjectVO<UID> nuclet : nuclets) {
			UID nucletId = nuclet.getPrimaryKey();
			builder.append("\tpublic static final class " + format(nuclet.getFieldValue(E.NUCLET.name).toString(), wordseparator) + " {\n");

			for (EntityMeta<?> entityMeta : metaProvider.getAllEntities()) {

				if (entityMeta.getNuclet() == null || !entityMeta.getNuclet().equals(nucletId) || E.isNuclosEntity(entityMeta.getUID())) {
					continue;
				}
				appendClass(builder, entityMeta, wordseparator);
			}
			builder.append("\t}\n\n");
		}


		// Objects which are not assigned to a nuclet
		builder.append("\tpublic static final class UnasignedEntities {\n");

		for (EntityMeta<?> entityMeta : metaProvider.getAllEntities()) {
			if (entityMeta.getNuclet() != null || E.isNuclosEntity(entityMeta.getUID())) {
				continue;
			}
			appendClass(builder, entityMeta, wordseparator);
		}

		builder.append("\t}\n\n");
		builder.append("}\n\n");

		outPrintln(builder.toString());
		outPrintln("// done");
		outPrint("//");
	}

	private static String format(String name, String wordseparator) {
		return name.replaceAll(" ", wordseparator).replaceAll("-", wordseparator);
	}

	private void appendClass(StringBuilder builder, EntityMeta<?> entityMeta, String wordseparator) {
		builder
				.append("\t\tpublic static final class ").append(format(entityMeta.getEntityName(), wordseparator)).append(" {\n")
				.append("\t\t\tpublic static final UID ID = new UID(\"").append(entityMeta.getUID()).append("\");\n")
				.append("\t\t\tpublic static final class Field {\n");
		for (FieldMeta<?> fieldMeta : entityMeta.getFields()) {
			builder
					.append("\t\t\t\t/** ").append(fieldMeta.getDataType()).append(" ").append(fieldMeta.getDbColumn()).append(" */\n")
					.append("\t\t\t\tpublic static final UID ")
					.append(format(fieldMeta.getFieldName(), wordseparator))
					.append(" = new UID(\"").append(fieldMeta.getUID()).append("\");\n");
		}
		builder.append("\t\t\t}\n");
		builder.append("\t\t}\n\n");
	}

	private void clearUserPreferences(String sUserName) throws CommonFinderException {
		preferencesFacadeRemote.setPreferencesForUser(sUserName, null);
		consoleFacadeRemote.clearUserPreferences(sUserName);
	}

	private void invalidateAllCaches() {
		outPrintln("Invalidating all caches...");
		consoleFacadeRemote.invalidateAllCaches();
		outPrintln("done");
	}

	private void freeDalMemory() {
		outPrintln("freeDalMemory...");
		String[] result = consoleFacadeRemote.freeDalMemory();
		if (result != null) {
			for (String s : result) {
				outPrintln(s);
			}
		}
		outPrintln("done");
	}

	private void queryDalMemoryLoad() {
		outPrintln("queryDalMemoryLoad...");
		String[] result = consoleFacadeRemote.gatDalMemoryLoad();
		if (result != null) {
			for (String s : result) {
				outPrintln(s);
			}
		}
		outPrintln("done");
	}

	private void rebuildClasses() throws NuclosCompileException, InterruptedException, CommonPermissionException {
		outPrintln("Rebuild all Java Classes...");
		consoleFacadeRemote.rebuildAllClasses();
		outPrintln("done");
	}

	private void sourceScannerRunOnce() throws CommonPermissionException {
		outPrintln("Source scanner run once...");
		eventSupportFacadeRemote.readSourcesAndCompile();
		outPrintln("done");
	}

	private void killSession(String user) {
		outPrintln("Kill session " + user + "...");
		consoleFacadeRemote.killSession(user);
		outPrintln("done");
	}

	private void sendMessage(String sMessage, String sUser, Priority priority, String sAuthor) {
		outPrintln("sendMessage...");
		consoleFacadeRemote.sendClientNotification(sMessage, sUser, priority, sAuthor);
		outPrintln("done");
	}

	private void compileDbObjects() throws SQLException {
		outPrintln("compileInvalidDbObjects...");
		consoleFacadeRemote.compileInvalidDbObjects();
		outPrintln("done");
	}

	private void getConstraints(final Set<String> tables) {
		outPrintln("getConstraints...");
		outPrintln("for tables: " + (tables.isEmpty() ? "all" : tables.stream().collect(Collectors.joining(", "))));
		String[] result = consoleFacadeRemote.getConstraints(tables);
		if (result != null) {
			for (String s : result) {
				outPrintln(s);
			}
		}
		outPrintln("done");
	}

	private void dropConstraints(Set<String> constraintToDrop) {
		outPrintln("dropConstraints...");
		String[] result = consoleFacadeRemote.dropConstraints(constraintToDrop);
		if (result != null) {
			for (String s : result) {
				outPrintln(s);
			}
		}
		outPrintln("done");
	}

	private void rebuildConstraints() {
		outPrintln("rebuildConstraints...");
		String[] result = consoleFacadeRemote.rebuildConstraints();
		if (result != null) {
			for (String s : result) {
				outPrintln(s);
			}
		}
		outPrintln("done");
	}

	private void rebuildIndexes() {
		outPrintln("rebuildIndexes...");
		String[] result = consoleFacadeRemote.rebuildIndexes();
		if (result != null) {
			for (String s : result) {
				outPrintln(s);
			}
		}
		outPrintln("done");
	}

	private void checkDatasources() {
		outPrintln("checkDataSources...");
		String[] result;
		try {
			result = consoleFacadeRemote.checkDatasources();
			if (result != null) {
				for (String s : result) {
					outPrintln(s);
				}
			}
		} catch (CommonPermissionException e) {
			printStackTrace(e);
		}
		outPrintln("done");
	}

	private void migrateJasperReports(String[] asArgs) {
		outPrintln("migrateJasperReports...");
		String[] result;
		try {
			result = consoleFacadeRemote.migrateJasperReports(asArgs);
			if (result != null) {
				for (String s : result) {
					outPrintln(s);
				}
			}
		} catch (CommonPermissionException e) {
			printStackTrace(e);
		}
		outPrintln("done");
	}

	private void setMandatorLevel(String[] asArgs) {
		outPrintln("setMandatorLevel...");
		String[] result = consoleFacadeRemote.setMandatorLevel(asArgs);
		if (result != null) {
			for (String s : result) {
				outPrintln(s);
			}
		}
		outPrintln("done");
	}

	private void setDataLanguage(String[] asArgs) {
		outPrintln("setDataLanguage...");
		final DataLanguageFacadeRemote dlFacade = SpringApplicationContextHolder.getBean(DataLanguageFacadeRemote.class);
		final Collection<DataLanguageVO> allDataLanguages = dlFacade.getAllDataLanguages();
		final List<DataLanguageVO> result = new ArrayList<>();
		int i = 1;
		for (String sArg : asArgs) {
			outPrint(sArg);
			if (sArg.endsWith(SET_DATA_LANGUAGE.getCommand())) {
				continue;
			}
			final boolean bPrimary = sArg.contains("(primary)");
			sArg = sArg.replace("(primary)", "");
			boolean bFound = false;
			for (DataLanguageVO dl : allDataLanguages) {
				final String expectedArg = dl.getLanguage() + "_" + dl.getCountry();
				if (sArg.equals(expectedArg)) {
					bFound = true;
					outPrintln(" -> DL found with UID " + dl.getPrimaryKey().getString() + (bPrimary ? " (with primary flag)" : ""));
					dl = dl.copy();
					dl.setIsPrimary(bPrimary);
					dl.setOrder(i);
					result.add(dl);
					i++;
					break;
				}
			}
			if (!bFound) {
				outPrintln(" -> NOT found!");
			}
		}
		try {
			dlFacade.save(result);
		} catch (Exception e) {
			printStackTrace(e);
		}
		outPrintln("done");
	}

	private void migrateDocumentAttachmentsIntoSubDirs(long iChunkSize, long iMaxChunksToMigrate, String...ignoredExtensions) {
		outPrintln("migrateDocumentAttachmentsIntoSubdirectories...");
		String[] result = consoleFacadeRemote.migrateDocumentAttachmentsIntoSubDirs(iChunkSize, iMaxChunksToMigrate, ignoredExtensions);
		if (result != null) {
			Arrays.stream(result).forEach(this::outPrintln);
		}
	}

	private void checkDocumentAttachments(boolean deleteUnusedFiles, Set<String> deleteEntityObjects) {
		outPrintln("checkDocumentAttachments...");
		String[] result = consoleFacadeRemote.checkDocumentAttachments(deleteUnusedFiles, deleteEntityObjects);
		if (result != null) {
			for (String s : result) {
				outPrintln(s);
			}
			if (result.length == 0) {
				outPrintln("No problems found.");
			}
		}
	}

	private void cleanupDuplicateDocuments() {
		outPrintln("cleanupDuplicateDocuments...");
		String result = consoleFacadeRemote.cleanupDuplicateDocuments();
		outPrintln("cleanupDuplicateDocuments finished.");
		outPrintln(result);
	}

	private void resetCompleteMandatorSetup() {
		outPrintln("resetCompleteMandatorSetup...");
		String[] result = consoleFacadeRemote.resetCompleteMandatorSetup();
		if (result != null) {
			for (String s : result) {
				outPrintln(s);
			}
		}
		outPrintln("done");
	}
	
	private void outPrint(String s) {
		if (out != null) {
			out.print(s);
		}
	}
	
	private void outPrintln(String s) {
		if (out != null) {
			out.println(s);
		}
	}
	
	private void errPrintln(String s) {
		if (err != null) {
			err.println(s);
		}
	}
	
	private void printStackTrace(Exception e) {
		if (err != null) {
			errPrintln(ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Unattractive warnings are printed to system out, which are later visible in the result of the management console.
	 * This is why we temporarily set our own SysOut.
	 * Static class to allow in forbidden apis.
	 */
	public static class SysOutSuppressor {
		private final String suppress;
		public SysOutSuppressor(final String suppress) {
			if (suppress == null) {
				throw new IllegalArgumentException("suppress cannot be null");
			}
			this.suppress = suppress;
		}
		public <T> T suppressSysOut(final Supplier<T> supplier) {
			class FilteredPrintStream extends PrintStream {
				private final PrintStream original;

				public FilteredPrintStream(PrintStream original) {
					super(original);
					this.original = original;
				}

				@Override
				public void println(String x) {
					if (x != null && !x.contains(suppress)) {
						original.println(x);
					}
				}

				@Override
				public void print(String x) {
					if (x != null && !x.contains(suppress)) {
						original.print(x);
					}
				}
			}
			final PrintStream originalOut = System.out;
			final PrintStream originalErr = System.err;
			try {
				System.setOut(new FilteredPrintStream(originalOut));
				System.setOut(new FilteredPrintStream(originalErr));
				return supplier.get();
			} finally {
				System.setOut(originalOut);
				System.setErr(originalErr);
			}
		}
	}

}    // class NuclosConsole
