//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.CaseFormat;

/**
 * TODO: I18n for description
 * TODO: Proper type for arguments with "optional" flag, instead of simple Strings
 */
public enum ConsoleCommand {
	CHECK_DOCUMENT_ATTACHMENTS("[-deleteUnusedFiles] [-deleteDatabaseRecords <full qualified business object name 1> <fqbon 2> ...]", "Checks the validity of all document attachments."),
	MIGRATE_DOCUMENT_ATTACHMENTS_INTO_SUBDIRECTORIES("[-chunkSize <size>] [-maxChunksToMigrate <max>]", "Migrate (move) document attachments to subdirectories, see also system parameter DOCUMENTS_GUIDELINE_FOR_TOTAL_NUMBER_OF_FILES_PER_DIR.\nPreviously migrated attachments will be ignored.\nThe size of the attachments to be processed simultaneously can be set using \"-chunksize\".\nAdditionally, the migration can be limited to a certain number of chunks\nto spread the load over several runs with \"-maxChunksToMigrate\"."),
	CHECK_DATA_SOURCES(null, "Checks the validity of all datasources."),
	CLEANUP_DUPLICATE_DOCUMENTS(null, "Generates a hash value per file and remove duplicates."),
	CLEAR_USER_PREFERENCES("<user name>", "Resets the preferences for the user with the given name."),
	SET_SIDEVIEW_MENU_WIDTH("<width> [-s | -p | -l <list of preferences uids>]", "Sets the sideview menu width of all (shared or private) preferences.\n -s\tAdjust width for shared preferences only.\n -p\tAdjust width for private preferences only.\n -l\tAdjust width for listed preferences only."),
	COMPILE_DB_OBJECTS(null, "Compile all invalid db functins and views."),
	DISABLE_INDEXER(null, null),
	ENABLE_INDEXER(null, null),
	ENABLE_INDEXER_SYNCHRONOUSLY(null, null),
	GENERATE_BO_UID_LIST(null, null),
	INVALIDATE_ALL_CACHES(null, "Invalidates all system caches (Like Nuclet import)"),
	FREE_DAL_MEMORY(null, "Frees as much memory as possible in the data access layer (DAL)"),
	QUERY_DAL_MEMORY_LOAD(null, "Queries the current memory load of the data access layer (DAL)"),
	KILL_SESSION("[-user <username>]", "Kill the client of the specified user (or all users if no one is specified)."),
	MIGRATE_SUBFORM_TO_ON_DELETE_CASCADE_CONSTRAINT("[-p]", "Sets flag \"on delete cascade\" in reference field attribute for every subform relation used in a layout. (Nuclos does not delete dependents implicitly anymore)\n -p\tJust pretends, does not really update."),
	MIGRATE_JASPER_REPORTS("[-patch | -forceAll | -removeDeprecatedPdfFonts | -overwriteReportFonts <\"fontNameInQuotes\">]", "Runs the report migration process, which is used to upgrade old reports to the current jasper version.\n -patch\t Flag is needed to actually patch, instead of only showing information.\n -forceAll\tflag can be used to run the migration on reports, that are already up to date.\n -replaceDeprecatedPdfFonts\tReplaces all deprecated pdfFontTags from the JasperReport.\n -overwriteReportFonts\tSpecify a font in quotes, that should replace all report fonts. Default: \"DejaVu Sans\""),
	REBUILD_CLASSES(null, "Rebuilds all java classes (Like Nuclet import)"),
	SOURCE_SCANNER_RUN_ONCE(null, "Import file changes made to the source code in the code generator directory."),
	GET_CONSTRAINTS("[-tables (<table1> <table2> ...)]", "Gets constraints that are defined via entity wizard."),
	DROP_CONSTRAINTS("(<constraint1> <constraint2> ...)", "Drops given constraints."),
	REBUILD_CONSTRAINTS(null, "Rebuilds unique and foreign constraints that are defined via entity wizard."),
	REBUILD_CONSTRAINTS_AND_INDEXES(null, "Rebuilds both at once."),
	REBUILD_INDEXES(null, "Rebuilds all indexes that are defined via entity wizard."),
	REBUILD_LUCENE_INDEX(null, null),
	RESET_COMPLETE_MANDATOR_SETUP(null, null),
	SEND_MESSAGE("-message <message text> [-user <username>] [-priority <high, normal, low>] [-author <author>]", "Send a message to the specified user (or all users if no one is specified)."),
	SET_DATA_LANGUAGE("-setDataLanguage <data languages>", "Separate multiple data languages with a blank. The primary language with '(primary)', for example 'de_DE(primary)'."),
	SET_MANDATOR_LEVEL("-package <nuclet package> -bo <name of the business object> -level <mandator level (1, 2, ...)> [-initial <path of the initial mandator>] [-uniqueMandator]", "Set mandator level in multiple business objects.\nDo not change order of the arguments. Separate multiple updates with a blank or a new line."),
	SHOW_MAINTENANCE(null, "Shows the status of the maintenance mode."),
	START_MAINTENANCE(null, "Starts the maintenance mode."),
	STOP_MAINTENANCE(null, "Ends the maintenance mode."),
	SYNC_ACTIVE_WEBADDONS(null, "Sync all active web addons files from file system to database (Nuclet) for development."),
	FIX_RECORDGRANT_PK_ALIAS(null, "Removes quotation of primary key (INTID or STRUID) column name from generated SQL for record grant datasources. This should be needed for PostgreSQL only.");

	private final String command;
	private final String arguments;
	private final String[] descriptionLines;

	ConsoleCommand(
			final String arguments,
			final String description
	) {
		this.command = "-" + CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, this.name());
		this.arguments = arguments;
		this.descriptionLines = StringUtils.split(description, "\n");
	}

	public String getCommand() {
		return command;
	}

	public String[] getDescriptionLines() {
		return descriptionLines;
	}

	public String getArguments() {
		return arguments;
	}

	/**
	 * Usage = commands + arguments
	 */
	public String getUsage() {
		String usage = command;

		if (StringUtils.isNotBlank(arguments)) {
			usage += " " + arguments;
		}

		return usage;
	}

	static ConsoleCommand fromValue(String command) {
		for (ConsoleCommand constant : values()) {
			if (StringUtils.equalsIgnoreCase(constant.getCommand(), command)) {
				return constant;
			}
		}
		throw new IllegalArgumentException("Unknown command: " + command);
	}
}
