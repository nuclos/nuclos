package org.nuclos.common;

/**
 * General Nucleus fatal Exception.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:oliver.brausch@novabit.de">oliver.brausch</a>
 * @version 00.01.000
 */
public class NuclosAttributeNotInRuleException extends NuclosAttributeNotFoundException {

	/**
	 *
	 */
	private static final long serialVersionUID = -2339678180241888426L;

	/**
	 * @param sMessage exception message
	 */
	public NuclosAttributeNotInRuleException(String sMessage) {
		super(sMessage);
	}

}	// class NuclosAttributeNotInRuleException
