//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

/**
 * @author Thomas Pasch (javadoc)
 */
public interface NuclosConstants {
	
	/**
	 * Propagate server stack trace to client.
	 */
	String CLIENT_STACK_TRACE = "client.stack.trace";
	
	/**
	 * User time zone propagated to server.
	 */
	String USER_TIMEZONE = "user.timezone";
	
	/**
	 * Propagate mandator from client.
	 */
	String MANDATOR = "org.nuclos.mandator";
	
	/**
	 * Propagate mandator from client.
	 */
	String DATA_LOCALE = "org.nuclos.datalocale";
	
	/**
	 * Potential a Boolean object indicating if InputContext is supported.
	 */
	String INPUT_CONTEXT_SUPPORTED = "org.nuclos.api.context.InputContextSupported";
	
	/**
	 * Potential a Map<String, Serializable> object of the input context.
	 */
	String INPUT_CONTEXT = "org.nuclos.api.context.InputContext";
	
	/**
	 * Potential a Integer object of the message receiver context 
	 * (for dynamic allocated progress bars over JMS). 
	 */
	String MESSAGE_RECEIVER_CONTEXT = "org.nuclos.api.context.MessageReceiverContext";

	
	/**
	 * Default nuclos menu configuration XML file.
	 * <p>
	 * Only <em>one</em> such a file is supported.
	 */
	String NUCLOS_MENUCONFIG = "nuclos-menuconfig.xml";
	
	/**
	 * Additional nuclos menu configuration, normally from nuclos extensions.
	 * <p>
	 * If several such files are found, all are loaded.
	 */
	String MENUCONFIG = "menuconfig.xml";
	
	/**
	 * Extensions override for {@link #NUCLOS_MENUCONFIG}.
	 * <p>
	 * Only <em>one</em> such a file is supported.
	 * <p>
	 * Needed for http://project.nuclos.de/browse/LIN-67
	 * 
	 * @author Thomas Pasch
	 * @since Nuclos 4.3.0
	 */
	String NUCLOS_MENUCONFIG_OVERRIDE = "nuclos-menuconfig-override.xml";
	
	/**
	 * Boolean parameter indicating the search mode in many value list providers.
	 */
	String VLP_SEARCHMODE_PARAMETER = "_searchmode";
	
	/**
	 * UID (sometimes String) parameter indicating the mandator in many value list providers.
	 */
	String VLP_MANDATOR_PARAMETER = "mandator";
	
	/**
	 * UID parameter indicating the entity in many value list providers.
	 */
	String VLP_ENTITY_UID_PARAMETER = "entity";
	
	/**
	 * Stringified Ref parameter indicating the (UID) field in many value list providers.
	 */
	String VLP_STRINGIFIED_FIELD_DEFINITION_PARAMETER = "fieldName";
	
	/**
	 * Boolean parameter indicating if the validity should NOT be checked (in many value list providers).
	 */
	String VLP_IGNORE_VALIDITY_PARAMETER = "ignoreValidity";

	String VLP_ADDITIONAL_CONDITION_FOR_RESULTFILTER = "additionalConditionResultFilter";
	
	String WEB_LAYOUT_CUSTOM_USAGE = "weblayout";
	
}
