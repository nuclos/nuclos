package org.nuclos.common;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;

/**
 * Utility to find a random free local port.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class PortAllocator {
	public static int allocate() throws IOException {
		final ServerSocket serverSocket = new ServerSocket();
		serverSocket.setReuseAddress(true);
		serverSocket.bind(new InetSocketAddress(0));

		int port = serverSocket.getLocalPort();

		serverSocket.close();

		return port;
	}
}
