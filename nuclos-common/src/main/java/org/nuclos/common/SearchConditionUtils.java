//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonDateValues;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithOtherField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithParameter;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIsNullCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common.security.UserDetails;
import org.nuclos.common2.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Utility methods for CollectableSearchConditions.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class SearchConditionUtils extends org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils {

	private final static Logger LOG = LoggerFactory.getLogger(SearchConditionUtils.class);

	public static final String RELATIVE_DATE_CALCULATION_REGEX_PATTERN = "^(?i)(today|heute)?\\s*([+-]?\\s*[0-9]+\\s*[dwmy])?$";
	public static final String USER_REGEX_PATTERN = "^(?i)(\\$user|\\$username|\\$current_user|\\$current_username)?$";

	private SearchConditionUtils() {
	}

	public static boolean isMatching(String sValue, final String regexToTest) {
		if (sValue != null) {
			sValue = sValue.trim().toLowerCase();
			if (sValue.length() > 0) {
				return Pattern.compile(regexToTest).matcher(sValue).matches();
			}
		}
		return false;
	}
	
	/**
	 * Fill only when needed...
	 */
	private static transient IMetaProvider MDP;
	
	public static CollectableComparison newComparison(EntityMeta<?> entity, SF<?> sf, ComparisonOperator compop, Object oValue) {
		return newComparison(sf.getMetaData(entity), compop, oValue);
	}
	
	public static CollectableComparison newComparison(UID entity, SF<?> sf, ComparisonOperator compop, Object oValue) {
		return newComparison(sf.getMetaData(entity), compop, oValue);
	}
	
	public static CollectableComparison newComparison(UID field, ComparisonOperator compop, Object oValue) {
		return newComparison(getMetaProvider().getEntityField(field), compop, oValue);
	}
	
	public static CollectableComparison newComparison(FieldMeta<?> field, ComparisonOperator compop, Object oValue) {
		if (oValue instanceof UID && (field.getForeignEntity() != null || field.getUnreferencedForeignEntity() != null)) {
			final EntityMeta<Object> eMeta = MDP.getEntity(field.getEntity());
			LOG.warn("Default VALUE-comparison for a UID created (Field=" + eMeta.getEntityName() + "." + field.getFieldName() + "). For a higher compatibility, we recommend using a ID-comparison (newUidComparison...).");
		}
		final CollectableEntityField clctef = newEntityField(field);
		if (java.util.Date.class.isAssignableFrom(clctef.getJavaClass()) && oValue instanceof String) {
			String sValue = (String) oValue;
			sValue = sValue.trim().toLowerCase();
			if (isMatching(sValue, RELATIVE_DATE_CALCULATION_REGEX_PATTERN)) {
				// Relative date search found
				Date dSearchValue = DateUtils.today();
				if ("today".equals(sValue) || "heute".equals(sValue)) {
					// nothing to do
				} else {
					if (sValue.startsWith("today") || sValue.startsWith("heute")) {
						sValue = sValue.substring(5);
					}
					char intervalPart = sValue.charAt(sValue.length() - 1);
					int numericPart = Integer.parseInt(sValue.substring(0, sValue.length() - 1));
					switch (intervalPart) {
						case 'y':
							dSearchValue = DateUtils.addMonths(dSearchValue, numericPart * 12);
							break;
						case 'm':
							dSearchValue = DateUtils.addMonths(dSearchValue, numericPart);
							break;
						case 'w':
							dSearchValue = DateUtils.addWeeks(dSearchValue, numericPart);
							break;
						default: // days
							dSearchValue = DateUtils.addDays(dSearchValue, numericPart);
					}
				}
				//if (InternalTimestamp.class.isAssignableFrom(clctef.getJavaClass())) {
				//	    oValue = InternalTimestamp.toInternalTimestamp(dSearchValue);
				// when transforming to InternalTimestamp, the search includes the timestamp
				// and a condition e.g. "example_rest_Order_createdAt = 'today'" will return nothing.
				oValue = dSearchValue;
			}
		} else if (oValue instanceof String && isMatching(((String)oValue).trim().toLowerCase(), USER_REGEX_PATTERN)) {
			if (SecurityContextHolder.getContext().getAuthentication() != null
					&& SecurityContextHolder.getContext().getAuthentication().getDetails() != null) {
				UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
				if (org.nuclos.common.UID.class.isAssignableFrom(clctef.getJavaClass())) {
					oValue = details.getUserUID();
				} else {
					oValue = details.getUsername();
				}
			}
		}
		return new CollectableComparison(clctef, compop, CollectableUtils.newCollectableFieldForValue(clctef, oValue));
	}
	
	
	
	public static CollectableLikeCondition newLikeCondition(EntityMeta<?> entity, SF<?> sf, String sLikeValue) {
		return newLikeCondition(sf.getMetaData(entity), sLikeValue);
	}
	
	public static CollectableLikeCondition newLikeCondition(UID entity, SF<?> sf, String sLikeValue) {
		return newLikeCondition(sf.getMetaData(entity), sLikeValue);
	}
	
	public static CollectableLikeCondition newLikeCondition(UID field, String sLikeValue) {
		return newLikeCondition(getMetaProvider().getEntityField(field), sLikeValue);
	}
	
	public static CollectableLikeCondition newLikeCondition(FieldMeta<?> field, String sLikeValue) {
		return new CollectableLikeCondition(newEntityField(field), sLikeValue);
	}
	
	
	
	public static CollectableComparison newKeyComparison(EntityMeta<?> entity, SF<?> sf, ComparisonOperator compop, Object id) {
		return newKeyComparison(sf.getMetaData(entity), compop, id);
	}
	
	public static CollectableComparison newKeyComparison(UID entity, SF<?> sf, ComparisonOperator compop, Object id) {
		return newKeyComparison(sf.getMetaData(entity), compop, id);
	}
	
	public static CollectableComparison newKeyComparison(UID field, ComparisonOperator compop, Object id) {
		return newKeyComparison(getMetaProvider().getEntityField(field), compop, id);
	}
	
	public static CollectableComparison newKeyComparison(FieldMeta<?> field, ComparisonOperator compop, Object id) {
		final CollectableEntityField clctef = newEntityField(field);
		return new CollectableComparison(clctef, compop, CollectableUtils.newCollectableValueIdFieldForKey(clctef, id));
	}
	
	
	
	public static CollectableIsNullCondition newIsNullCondition(EntityMeta<?> entity, SF<?> sf) {
		return newIsNullCondition(sf.getMetaData(entity));
	}
	
	public static CollectableIsNullCondition newIsNullCondition(UID entity, SF<?> sf) {
		return newIsNullCondition(sf.getMetaData(entity));
	}
	
	public static CollectableIsNullCondition newIsNullCondition(UID field) {
		return newIsNullCondition(getMetaProvider().getEntityField(field));
	}
	
	public static CollectableIsNullCondition newIsNullCondition(FieldMeta<?> field) {
		return new CollectableIsNullCondition(newEntityField(field));
	}
	
	
	
	public static CollectableIsNullCondition newIsNotNullCondition(EntityMeta<?> entity, SF<?> sf) {
		return newIsNotNullComparison(sf.getMetaData(entity));
	}
	
	public static CollectableIsNullCondition newIsNotNullCondition(UID entity, SF<?> sf) {
		return newIsNotNullComparison(sf.getMetaData(entity));
	}
	
	public static CollectableIsNullCondition newIsNotNullCondition(UID field) {
		return newIsNotNullComparison(getMetaProvider().getEntityField(field));
	}
	
	public static CollectableIsNullCondition newIsNotNullComparison(FieldMeta<?> field) {
		return new CollectableIsNullCondition(newEntityField(field), ComparisonOperator.IS_NOT_NULL);
	}
	
	
	
	public static CollectableComparison newIdComparison(EntityMeta<?> entity, SF<?> sf, ComparisonOperator compop, Long id) {
		return newIdComparison(sf.getMetaData(entity), compop, id);
	}
	
	public static CollectableComparison newIdComparison(UID entity, SF<?> sf, ComparisonOperator compop, Long id) {
		return newIdComparison(sf.getMetaData(entity), compop, id);
	}
	
	public static CollectableComparison newIdComparison(UID field, ComparisonOperator compop, Long id) {
		return newIdComparison(getMetaProvider().getEntityField(field), compop, id);
	}
	
	public static CollectableComparison newIdComparison(FieldMeta<?> field, ComparisonOperator compop, Long id) {
		final CollectableEntityField clctef = newEntityField(field);
		return new CollectableComparison(clctef, compop, CollectableUtils.newCollectableValueIdFieldForValueId(clctef, id));
	}
	
	public static CollectableComparison newIdComparison(UID field, ComparisonOperator compop, Long id, Object value) {
		final CollectableEntityField clctef = newEntityField(getMetaProvider().getEntityField(field));
		return new CollectableComparison(clctef, compop, CollectableUtils.newCollectableValueIdField(clctef, id, value));
	}
	
	public static CollectableComparison newUidComparison(EntityMeta<?> entity, SF<?> sf, ComparisonOperator compop, UID uid) {
		return newUidComparison(sf.getMetaData(entity), compop, uid);
	}
	
	public static CollectableComparison newUidComparison(UID entity, SF<?> sf, ComparisonOperator compop, UID uid) {
		return newUidComparison(sf.getMetaData(entity), compop, uid);
	}
	
	public static CollectableComparison newUidComparison(UID field, ComparisonOperator compop, UID uid) {
		return newUidComparison(getMetaProvider().getEntityField(field), compop, uid);
	}
	
	public static CollectableComparison newUidComparison(FieldMeta<?> field, ComparisonOperator compop, UID uid) {
		final CollectableEntityField clctef = newEntityField(field);
		return new CollectableComparison(clctef, compop, CollectableUtils.newCollectableValueIdFieldForValueId(clctef, uid));
	}

	public static <PK> CollectableComparison newPkComparison(EntityMeta<?> entity, SF<PK> sf, ComparisonOperator compop, PK uid) {
		return newPkComparison(sf.getMetaData(entity), compop, uid);
	}
	
	public static <PK> CollectableComparison newPkComparison(UID entity, SF<PK> sf, ComparisonOperator compop, PK uid) {
		return newPkComparison(sf.getMetaData(entity), compop, uid);
	}
	
	public static <PK> CollectableComparison newPkComparison(UID field, ComparisonOperator compop, PK uid) {
		return newPkComparison(getMetaProvider().getEntityField(field), compop, uid);
	}
	
	public static <PK> CollectableComparison newPkComparison(FieldMeta<?> field, ComparisonOperator compop, PK uid) {
		final CollectableEntityField clctef = newEntityField(field);
		if (uid instanceof UID) {
			if (clctef.isIdField()) {
				return new CollectableComparison(clctef, compop, CollectableUtils.newCollectableValueIdFieldForValueId(clctef, (UID) uid));				
			} else {
				return new CollectableComparison(clctef, compop, new CollectableValueField((UID) uid));
			}
		}
		else if (uid instanceof Long) {
			if (clctef.isIdField()) {
				return new CollectableComparison(clctef, compop, CollectableUtils.newCollectableValueIdFieldForValueId(clctef, (Long) uid));				
			} else {
				return new CollectableComparison(clctef, compop, new CollectableValueField((Long) uid));
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}

	public static <PK> CollectableInIdCondition newPkInCondition(EntityMeta<PK> entity, List<PK> pks) {
		final CollectableEntityField field = newEntityField(entity.getPk().getMetaData(entity));
		return new CollectableInIdCondition<PK>(field, pks);
	}

	
	public static CollectableEntityField newEntityField(FieldMeta<?> field) {
		return new CollectableEOEntity(getMetaProvider().getEntity(field.getEntity())).getEntityField(field.getUID());
	}
	
	protected static IMetaProvider getMetaProvider() {
		if (MDP == null) {// && SpringApplicationContextHolder.isSpringReady()) {
			MDP = (IMetaProvider) SpringApplicationContextHolder.getBean("metaDataProvider");
		}
		return MDP;
	}

	public static <T extends AtomicCollectableSearchCondition> T copyAtomicCollectableSearchCondition(T clctsc) {
		if (clctsc instanceof CollectableIsNullCondition) {
			CollectableIsNullCondition condition = (CollectableIsNullCondition) clctsc;
			return (T) new CollectableIsNullCondition(condition.getEntityField(), condition.getComparisonOperator());
		}
		if (clctsc instanceof CollectableComparisonWithParameter) {
			CollectableComparisonWithParameter condition = (CollectableComparisonWithParameter) clctsc;
			return (T) new CollectableComparisonWithParameter(condition.getEntityField(), condition.getComparisonOperator(), condition.getParameter());
		}
		if (clctsc instanceof CollectableInCondition) {
			CollectableInCondition condition = (CollectableInCondition) clctsc;
			return (T) new CollectableInCondition(condition.getEntityField(), condition.getComparisonOperator(), condition.getInComparands(), null, condition.getKeySet());
		}
		if (clctsc instanceof CollectableInIdCondition) {
			CollectableInIdCondition condition = (CollectableInIdCondition) clctsc;
			return (T) new CollectableInIdCondition(condition.getEntityField(), condition.getInComparands());
		}
		if (clctsc instanceof CollectableComparisonDateValues) {
			CollectableComparisonDateValues condition = (CollectableComparisonDateValues) clctsc;
			return (T) new CollectableComparisonDateValues(condition.getEntityField(), condition.getComparisonOperator(), condition.getDateValues());
		}
		if (clctsc instanceof CollectableComparison) {
			CollectableComparison condition = (CollectableComparison) clctsc;
			return (T) new CollectableComparison(condition.getEntityField(), condition.getComparisonOperator(), condition.getComparand());
		}
		if (clctsc instanceof CollectableComparisonWithOtherField) {
			CollectableComparisonWithOtherField condition = (CollectableComparisonWithOtherField) clctsc;
			return (T) new CollectableComparisonWithOtherField(condition.getEntityField(), condition.getComparisonOperator(), condition.getOtherField());
		}
		if (clctsc instanceof CollectableLikeCondition) {
			CollectableLikeCondition condition = (CollectableLikeCondition) clctsc;
			return (T) new CollectableLikeCondition(condition.getEntityField(), condition.getComparisonOperator(), condition.getLikeComparand());
		}
		return null;
	}
}	// class SearchConditionUtils
