package org.nuclos.common;

import org.nuclos.common.collection.Transformer;

public class UIDTransformer {

	public static class ToUID implements Transformer<String, UID> {
		@Override
		public UID transform(String i) {
			return UID.parseUID(i);
		}
	}
	
	public static class ToString implements Transformer<UID, String> {
		@Override
		public String transform(UID i) {
			return i.getStringifiedDefinition();
		}
	}
	
}
