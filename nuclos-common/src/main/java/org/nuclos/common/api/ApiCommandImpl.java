package org.nuclos.common.api;

import java.io.Serializable;
import java.util.Optional;

import org.nuclos.remoting.TypePreservingObjectWrapper;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Implementation of an API command.
 *
 * TODO: Should be ApiCommandImpl<T extends Command>, API has to  be refactored first. See NUCLOS-5221
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class ApiCommandImpl<T> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2570990279337323566L;
	private final Integer receiverId;

	@JsonProperty(value = "command")
	private final TypePreservingObjectWrapper wrapper;

	/**
	 * for deserialization only
	 */
	protected ApiCommandImpl() {
		this(null, null);
	}

	public ApiCommandImpl(final Integer receiverId, final T command) {
		this.receiverId = receiverId;
		this.wrapper = Optional.ofNullable(command).map(TypePreservingObjectWrapper::new).orElse(null);
	}

	public Integer getReceiverId() {
		return receiverId;
	}

	public T getCommand() {
		return (T) Optional.ofNullable(wrapper).map(TypePreservingObjectWrapper::get).orElse(null);
	}
}
