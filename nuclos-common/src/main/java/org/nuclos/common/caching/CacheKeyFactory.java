package org.nuclos.common.caching;

import org.nuclos.common.Versionable;

public class CacheKeyFactory {
	
	private CacheKeyFactory() {
		// Never invoked
	}
	
	public static Object makeKey(Versionable<? extends Object> o) {
		final StringBuilder result = new StringBuilder();
		result.append(o.getPrimaryKey());
		return result.toString();
	}
	
	public static Object makeKey(String... strings) {
		final StringBuilder result = new StringBuilder();
		for (String s: strings) {
			result.append(s).append(':');
		}
		return result.toString();
	}

}
