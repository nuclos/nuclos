//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * Abstract implementation of <code>CollectableEntity</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public abstract class AbstractCollectableEntity implements CollectableEntity {
	
	private final UID uid;
	private final String sLabel;
	private final Map<UID, CollectableEntityField> mpclctef = new HashMap<UID, CollectableEntityField>();
	
	public AbstractCollectableEntity(UID uid, String sLabel) {
		this.uid = uid;
		this.sLabel = sLabel;
	}
	
	protected SpringLocaleDelegate getSpringLocaleDelegate() {
		return SpringLocaleDelegate.getInstance();
	}

	protected void addCollectableEntityField(CollectableEntityField clctef) {
		this.mpclctef.put(clctef.getUID(), clctef);
	}

	@Override
	public UID getUID() {
		return this.uid;
	}

	@Override
	public String getLabel() {
		return this.sLabel;
	}

	@Override
	public int getFieldCount() {
		return this.mpclctef.size();
	}

	@Override
	public Set<UID> getFieldUIDs() {
		return this.mpclctef.keySet();
	}

	@Override
	public CollectableEntityField getEntityField(UID field) throws CommonFatalException {
		final CollectableEntityField result = this.mpclctef.get(field);
		if (result == null) {
			throw new CommonFatalException(StringUtils.getParameterizedExceptionMessage("field.not.available", field));
		}
		assert result != null;
		return result;
	}

	@Override
	public EntityMeta<?> getMeta() {
		return null;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("uid=").append(uid);
		//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
		//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//				result.append(",label=").append(getIdentifierLabel());
		result.append(", fields=").append(mpclctef);
		result.append("]");
		return result.toString();
	}

}  // class AbstractCollectableEntity
