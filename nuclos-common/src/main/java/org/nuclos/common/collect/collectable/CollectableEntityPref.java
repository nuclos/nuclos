package org.nuclos.common.collect.collectable;

import java.util.prefs.Preferences;

import org.nuclos.common.UID;

/**
 * A representation of {@link CollectableEntity} that is more suited 
 * to be stored in {@link Preferences}.
 * <p>
 * Attention: Consider using {@link org.nuclos.common.WorkspaceDescription2}
 * when you want to store Workspace setting (in contrast to user preferences).
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.1.0-rc5
 */
public class CollectableEntityPref {

	private final String type;
	
	private final UID entity;
	
	public CollectableEntityPref(String type, UID entity) {
		this.type = type;
		this.entity = entity;
	}
	
	public String getType() {
		return type;
	}
	
	public UID getEntity() {
		return entity;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("CEPref[").append(entity);
		result.append(", ").append(type);
		result.append(']');
		return result.toString();
	}
	
}
