//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable;

import java.util.Collection;

import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.format.RefValueExtractor;

/**
 * Transformer that makes a value id field out of a <code>EntityObjectVO</code>,
 * with the eo's id as value id and with the eo's field with the given name as value.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:maik.stueker@nuclos.de">maik.stueker</a>
 * @version 01.00.00
 */
public class MakeCollectableValueIdField<PK> implements Transformer<EntityObjectVO<PK>, CollectableField> {

	private final boolean bFormat;
	private final String stringifiedFieldDefinition;
	private final UID dataLanguage;
	
	public MakeCollectableValueIdField(String stringifiedFieldDefinition, UID dataLanguage) {
		this(stringifiedFieldDefinition, true, dataLanguage);
	}

	public MakeCollectableValueIdField(String stringifiedFieldDefinition, boolean bFormat, UID dataLanguage) {
		this.stringifiedFieldDefinition = stringifiedFieldDefinition;
		this.bFormat = bFormat;
		this.dataLanguage = dataLanguage;
	}

	@Override
	public CollectableField transform(final EntityObjectVO<PK> eovo) {
		final String value = RefValueExtractor.get(eovo, stringifiedFieldDefinition, dataLanguage, bFormat);
		return new CollectableValueIdField(eovo.getId(), value);
	}

	/**
	 *
	 * @return
	 */
	public Collection<UID> getFields() {
		final Collection<UID> fields = RefValueExtractor.getAttributes(stringifiedFieldDefinition);
		return fields;
	}

}	// class MakeValueIdField
