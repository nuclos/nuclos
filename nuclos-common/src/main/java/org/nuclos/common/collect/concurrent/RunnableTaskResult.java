package org.nuclos.common.collect.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

public class RunnableTaskResult {

	private List<Future> lstFutures;
	private String name;
	
	public RunnableTaskResult(String name) {
		this.lstFutures = new ArrayList<Future>();
		this.name = name;
	}
	
	public void addFuture(Future f) {
		this.lstFutures.add(f);
	}
	
	public List<Future> getFutures() {
		return this.lstFutures;
	}
	
	public String getName() {
		return this.name;
	}
}
