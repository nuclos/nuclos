//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collection;

import java.util.Collection;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.remoting.TypePreservingObjectArrayDeserializer;
import org.nuclos.remoting.TypePreservingObjectArraySerializer;
import org.nuclos.server.genericobject.ProxyListProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ProxyListTransport {

	@JsonSerialize(using = TypePreservingObjectArraySerializer.class)
	@JsonDeserialize(using = TypePreservingObjectArrayDeserializer.class)
	private final Object[] theArray;
	private final Integer lastIndexRead;
	private final Integer mdsize;

	private final UID entity;
	private final CollectableSearchExpression clctexpr;
	private final Collection<UID> fields;
	private final ProxyListProvider proxyListProvider;

	private String customUsage;
	private Set<UID> requiredSubEntities;
	private Boolean loadThumbnailsOnly;

	/**
	 * for deserialization only
	 */
	protected ProxyListTransport() {
		this.theArray = null;
		this.lastIndexRead = null;
		this.mdsize = null;
		this.entity = null;
		this.clctexpr = null;
		this.fields = null;
		this.proxyListProvider = null;
	}

	public ProxyListTransport(final Object[] theArray, final int iLastIndexRead, final int mdsize,
							  final UID entity, final CollectableSearchExpression clctexpr, final Collection<UID> fields, final ProxyListProvider proxyListProvider) {
		this.theArray = theArray;
		this.lastIndexRead = iLastIndexRead;
		this.mdsize = mdsize;
		this.entity = entity;
		this.clctexpr = clctexpr;
		this.fields = fields;
		this.proxyListProvider = proxyListProvider;
	}

	public Object[] getTheArray() {
		return theArray;
	}

	public int getLastIndexRead() {
		return lastIndexRead;
	}

	public int getMdsize() {
		return mdsize;
	}

	public UID getEntity() {
		return entity;
	}

	public CollectableSearchExpression getClctexpr() {
		return clctexpr;
	}

	public Collection<UID> getFields() {
		return fields;
	}

	public ProxyListProvider getProxyListProvider() {
		return proxyListProvider;
	}

	public String getCustomUsage() {
		return customUsage;
	}

	public void setCustomUsage(final String customUsage) {
		this.customUsage = customUsage;
	}

	public Set<UID> getRequiredSubEntities() {
		return requiredSubEntities;
	}

	public void setRequiredSubEntities(final Set<UID> requiredSubEntities) {
		this.requiredSubEntities = requiredSubEntities;
	}

	public Boolean getLoadThumbnailsOnly() {
		return loadThumbnailsOnly;
	}

	public void setLoadThumbnailsOnly(final Boolean loadThumbnailsOnly) {
		this.loadThumbnailsOnly = loadThumbnailsOnly;
	}
}
