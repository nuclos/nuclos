//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.common.customcomp.resplan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.nuclos.common.SourceResultHelper;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.LocaleInfo;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

//Version
@SuppressWarnings("serial")
@XmlType
@XmlRootElement(name="resplan")
public class ResPlanConfigVO<PK,R,C extends Collectable<PK>> extends AbstractResPlanConfigVO<PK,R,C,PlanElement<R>> {

	private static final Logger LOG = Logger.getLogger(ResPlanConfigVO.class);

	public ResPlanConfigVO() {
	}
	
	public PlanElement<R> newPlanElementInstance() {
		return new PlanElement<R>();
	}

	public static <PK,R,C extends Collectable<PK>> ResPlanConfigVO<PK,R,C> fromBytes(byte[] b, Jaxb2Marshaller marshaller) {
		final StreamSource in = SourceResultHelper.newSource(b);
		final ResPlanConfigVO<PK,R,C> result;
		try {
			result = (ResPlanConfigVO<PK,R,C>) marshaller.unmarshal(in);
		} catch (OutOfMemoryError e) {
			LOG.error("JAXB unmarshal failed: xml is:\n" + new String(b), e);
			throw e;
		}

		if (result.getPlanElements() != null && result.getPlanElements().size() > 0) {
			// transform PlanElement to PlanElement. JAXB does not handle this...
			List<PlanElement<R>> ples = new ArrayList<PlanElement<R>>();
			for (int i = 0; i < result.getPlanElements().size(); i++) {
				PlanElement ple = result.getPlanElements().get(i);
				ples.add(new PlanElement(ple));
			}
			result.setPlanElements(ples);
		}

		// transfer resources from template
		if (result.getResources() == null || result.getResources().size() == 0) {
			List<ResPlanResourceVO> resources = new ArrayList<ResPlanResourceVO>();
//			Collection<LocaleInfo> locales = LocaleDelegate.getInstance().getAllLocales(false);
//			for (LocaleInfo li : locales) {
//				ResPlanResourceVO vo = new ResPlanResourceVO();
//				vo.setLocaleId(li.getLocale());
//				vo.setResourceLabel(result.getResourceLabelText());
//				vo.setResourceTooltip(result.getResourceToolTipText());
//				vo.setBookingLabel(result.getEntryLabelText());
//				vo.setBookingTooltip(result.getEntryToolTipText());
//				vo.setLegendLabel(result.getCornerLabelText());
//				vo.setLegendTooltip(result.getCornerToolTipText());
//				resources.add(vo);
//			}
//			result.setResources(resources);
		}
		return result;
	}

}

