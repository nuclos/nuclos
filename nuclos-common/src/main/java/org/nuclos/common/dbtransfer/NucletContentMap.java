//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dbtransfer;

import java.io.IOException;
import java.util.Iterator;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.multimap.MultiListMap;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = NucletContentMap.NucletContentMapJsonSerializer.class)
@JsonDeserialize(using = NucletContentMap.NucletContentMapJsonDeserializer.class)
public interface NucletContentMap extends MultiListMap<EntityMeta<UID>, TransferEO> {

	void addAll(NucletContentMap map);

	void add(TransferEO eo);

	class NucletContentMapJsonSerializer extends JsonSerializer<NucletContentMap> {
		@Override
		public void serialize(final NucletContentMap map, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
			jgen.writeStartObject();
			for (EntityMeta<UID> key : map.keySet()) {
				jgen.writeArrayFieldStart(key.getUID().getString());
				for (TransferEO teo : map.getValues(key)) {
					jgen.writeObject(teo);
				}
				jgen.writeEndArray();
			}
			jgen.writeEndObject();
		}
	}

	class NucletContentMapJsonDeserializer extends JsonDeserializer<NucletContentMap> {
		@Override
		public NucletContentMap deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
			final NucletContentMap result = new NucletContentHashMap();
			final ObjectCodec codec = jpar.getCodec();
			final TreeNode contentNode = codec.readTree(jpar);
			final Iterator<String> fieldNameIt = contentNode.fieldNames();
			while (fieldNameIt.hasNext()) {
				final String fieldName = fieldNameIt.next();
				final TreeNode contentListNode = contentNode.get(fieldName);
				final int size = contentListNode.size();
				for (int i = 0; i < size; i++) {
					final TreeNode entryNode = contentListNode.get(i);
					final TransferEO teo = codec.treeToValue(entryNode, TransferEO.class);
					result.add(teo);
				}
			}
			return result;
		}
	}

}
