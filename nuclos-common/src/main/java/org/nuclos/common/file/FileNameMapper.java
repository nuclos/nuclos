package org.nuclos.common.file;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

/**
 * Provides utility methods for escaping and unescaping file names.
 * An escaped filename can be safely used as a filename on the filesystem.
 * <p>
 * The {@link #unescape(String)} reverses the effect of {@link #escape(String)},
 * i.e. unescape(escape(name)) == name
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class FileNameMapper {
	public static final char ESCAPE_CHARACTER = '%';

	public static boolean isAllowedChar(byte b) {
		return b == ' '
				|| b == '-'
				|| b == '.'
				|| b >= '0' && b <= '9'
				|| b >= 'A' && b <= 'Z'
				|| b == '_'
				|| b >= 'a' && b <= 'z';
	}

	public static boolean mustBeEscaped(String fileName) {
		for (byte b: fileName.getBytes()) {
			if (!isAllowedChar(b)) {
				return true;
			}
		}

		return false;
	}

	private static byte[] escapeByte(byte b) {
		String result = "%" + String.format("%02X", b);

		return result.getBytes();
	}

	public static String escape(String name) {
		List<Byte> byteList = new ArrayList<>();

		for (byte b : name.getBytes()) {
			if (isAllowedChar(b)) {
				byteList.add(b);
			} else {
				byte[] escapedByte = escapeByte(b);
				addByteArray(byteList, escapedByte);
			}
		}

		return new String(toByteArray(byteList));
	}

	public static String unescape(String name) {
		List<Byte> byteList = new ArrayList<>();

		byte[] bytes = name.getBytes();
		for (int i = 0; i < bytes.length; i++) {
			byte b = bytes[i];
			if (b == ESCAPE_CHARACTER) {
				String hexValue = "" + (char) bytes[i + 1] + (char) bytes[i + 2];
				b = (byte) Integer.parseInt(hexValue, 16);
				i += 2;
			}
			byteList.add(b);
		}

		return new String(toByteArray(byteList));
	}

	private static byte[] toByteArray(List<Byte> bytes) {
		return ArrayUtils.toPrimitive(bytes.toArray(new Byte[bytes.size()]));
	}

	private static void addByteArray(List<Byte> byteList, byte[] byteArray) {
		for (byte b : byteArray) {
			byteList.add(b);
		}
	}
}
