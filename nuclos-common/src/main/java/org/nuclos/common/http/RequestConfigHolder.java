package org.nuclos.common.http;

import org.apache.http.client.config.RequestConfig;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class RequestConfigHolder {
	/**
	 * Looks like a very long timeout is needed here for reports and such,
	 * where a long calculation takes place on the server before any data
	 * can be sent to the client.
	 */
	public static final int SO_TIMEOUT_MILLIS = 30 * 60 * 1000;

	private static final int CONNECT_TIMEOUT_MILLIS = 20 * 1000;    // 20 seconds

	private RequestConfig defaultConfig = RequestConfig.custom()
			.setConnectionRequestTimeout(CONNECT_TIMEOUT_MILLIS)
			.setSocketTimeout(SO_TIMEOUT_MILLIS)
			.setConnectTimeout(CONNECT_TIMEOUT_MILLIS)
			.build();

	private ThreadLocal<RequestConfig> configForCurrentThread = new ThreadLocal<>();

	private static RequestConfigHolder INSTANCE;

	public static RequestConfigHolder getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new RequestConfigHolder();
		}

		return INSTANCE;
	}

	/**
	 * Determines the RequestConfig that should currently be used for any Requests.
	 */
	RequestConfig getCurrentConfig() {
		RequestConfig requestConfig = configForCurrentThread.get();

		if (requestConfig == null) {
			requestConfig = defaultConfig;
		}

		return requestConfig;
	}

	public void setDefaultSocketTimeout(int timeoutInMillis) {
		defaultConfig = RequestConfig.copy(defaultConfig)
				.setSocketTimeout(timeoutInMillis)
				.build();
	}

	public void setSocketTimeoutForCurrentThread(int timeoutInMillis) {
		RequestConfig requestConfig = configForCurrentThread.get();
		if (requestConfig == null) {
			requestConfig = defaultConfig;
		}

		requestConfig = RequestConfig.copy(requestConfig)
				.setSocketTimeout(timeoutInMillis)
				.build();

		configForCurrentThread.set(requestConfig);
	}

	public void resetSocketTimeoutForCurrentThread() {
		configForCurrentThread.remove();
	}
}
