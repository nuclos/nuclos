package org.nuclos.common.lucene;

public class CompleteTransaction extends IndexStep {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2301771646067109524L;
	private final boolean success;
	private final boolean close;

	public CompleteTransaction(String transactId, boolean success, boolean close) {
		super(transactId);
		this.success = success;
		this.close = close;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public boolean isClose() {
		return close;
	}

}
