package org.nuclos.common.lucene;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.nuclos.common.IMetaProvider;
import org.nuclos.common.UID;


public class NuclosCoordinate {
	final static public String ATTACHED = "attached:";
	final static public UID ATTKEY = new UID(ATTACHED);
	
	final private Long eoid;
	final private UID field;
	final private AttachInfo attachinfo;
	
	public NuclosCoordinate(Long eoid, String field) {
		this.eoid = eoid;
		this.field = new UID(field);
		this.attachinfo = new AttachInfo(field);
	}

	public Long getEoid() {
		return eoid;
	}

	public UID getField() {
		return field;
	}
	
	public AttachInfo getAttachInfo() {
		return attachinfo;
	}
	
	public UID getEntity(IMetaProvider provider) {
		if (attachinfo.hasAttachment) {
			return attachinfo.getEntity();
		}
		return provider.getEntityField(field).getEntity();
	}
	
	public static class AttachInfo implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1942733599807567437L;
		final private boolean hasAttachment;
		final private String pk;
		final private UID entity;
		final private Set<String> filenames;
		
		private AttachInfo(String field) {
			this.filenames = new HashSet<String>();
			this.hasAttachment = field.startsWith(NuclosCoordinate.ATTACHED);
			if (this.hasAttachment) {
				int n1 = field.indexOf(':');
				int n2 = field.lastIndexOf(':');
				this.pk = field.substring(n1 + 1, n2);
				this.entity = new UID(field.substring(n2 + 1));				
			} else {
				this.pk = null;
				this.entity = null;
			}
		}

		public Set<String> getFilenames() {
			return Collections.unmodifiableSet(filenames);
		}

		public void addFilename(String filename) {
			filenames.add(filename);
		}
		
		public void addFilenames(Set<String> filenames) {
			this.filenames.addAll(filenames);
		}

		public boolean hasAttachment() {
			return hasAttachment;
		}

		public String getPk() {
			return pk;
		}

		public UID getEntity() {
			return entity;
		}
		
		@Override
		public String toString() {
			return filenames.toString();
		}
	}
}
