//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.masterdata;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityField;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.util.DalTransformations;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * Provides structural (meta) information about a field in a master data table.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableMasterDataEntity implements CollectableEntity {

	private static final Logger LOG = Logger.getLogger(CollectableMasterDataEntity.class);

	private final EntityMeta<?> entityMeta;
	private final Map<UID, FieldMeta<?>> mpFields;
	private final Map<UID, CollectableEntityField> mpclctef = CollectionUtils.newHashMap();

	/**
	 * §precondition mdmetavo != null
	 */
	public CollectableMasterDataEntity(EntityMeta<?> entityMeta) {
		if (entityMeta == null) {
			throw new NullArgumentException("entityMeta");
		}
		this.entityMeta = entityMeta;
		mpFields = DalTransformations.transformFieldMap(entityMeta.getFields());
	}

	@Override
	public UID getUID() {
		return this.entityMeta.getUID();
	}

	@Override
	public String getLabel() {
		return SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(entityMeta);
	}

	public EntityMeta<?> getMeta() {
		return this.entityMeta;
	}

	/**
	 * @param field
	 * @return non-null meta field
	 * @throws NuclosFatalException if there is no such field
	 */
	private FieldMeta<?> getMetaFieldVO(UID field) {
		FieldMeta<?> result = mpFields.get(field);
		if (result == null) {
			if (CalcAttributeUtils.isCalcAttributeCustomization(field)) {
				try {
					result = SpringApplicationContextHolder.getBean(IMetaProvider.class).getEntityField(field);
					if (result != null) {
						return result;
					}
				} catch (Exception ex) {
					// ignore here
				}
			}
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("clctmdef.missing.field.error", field, this.entityMeta.getUID()));
		}
		return result;
	}

	@Override
	public int getFieldCount() {
		return this.entityMeta.getFields().size();
	}

	@Override
	public Set<UID> getFieldUIDs() {
		return mpFields.keySet();
	}

	@Override
	public CollectableEntityField getEntityField(UID field) {
		CollectableEntityField result = mpclctef.get(field);
		if (result == null) {
			if (getMetaFieldVO(field).getForeignEntity() != null)
				result = new CollectableMasterDataForeignKeyEntityField(mpFields.get((field)));
			else if (getMetaFieldVO(field).getLookupEntity() != null)
				result = new CollectableMasterDataLookupKeyEntityField(mpFields.get(field));
			else if (getMetaFieldVO(field).getUnreferencedForeignEntity() != null)
				result = new CollectableMasterDataForeignKeyEntityField(mpFields.get(field));
			else {
				FieldMeta<?> fieldMeta = getMetaFieldVO(field);

				int fieldtype = CollectableField.TYPE_VALUEFIELD;

				CollectableField cfDefault = null;
				Class<?> javaClass;
				try {
					javaClass = Class.forName(fieldMeta.getDataType());
				} catch (ClassNotFoundException e1) {
					throw new CommonFatalException(e1);
				}

				if (fieldMeta.getDefaultValue() == null) {					
					cfDefault = CollectableUtils.getNullField(fieldtype, fieldMeta.isLocalized());
				}
				else {
					try {
						cfDefault = new CollectableValueField(
								CollectableFieldFormat.getInstance(javaClass).parse(null,fieldMeta.getDefaultValue()));	
					}
					catch(CollectableFieldFormatException e) {
						throw new CommonFatalException(e);
					}
				}
				
				SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
				String label = localeDelegate.getLabelFromMetaFieldDataVO(fieldMeta);
				String description = localeDelegate.getTextFallback(fieldMeta.getLocaleResourceIdForDescription(), "");

				result = new DefaultCollectableEntityField(
							field,
							javaClass,
							label,
							description,
							fieldMeta.getScale(),
							fieldMeta.getPrecision(),
							fieldMeta.isNullable(),
							fieldtype,
							null,
							cfDefault,
							fieldMeta.getFormatInput(),
							fieldMeta.getFormatOutput(),
							entityMeta.getUID(),
							fieldMeta.getDefaultComponentType(),
							fieldMeta.isCalculated(),
							fieldMeta.isCalcOndemand(),
							fieldMeta.isCalcAttributeAllowCustomization(),
							fieldMeta.isLocalized(),
							fieldMeta.isHidden(),
							fieldMeta.isModifiable()
						);
			}
			mpclctef.put(field, result);
		}
		result.setCollectableEntity(this);
		return result;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CollectableMasterDataEntity)) return false;
		final CollectableMasterDataEntity other = (CollectableMasterDataEntity) o;
		return entityMeta.equals(other.entityMeta);
	}
	
	@Override
	public int hashCode() {
		return entityMeta.hashCode() + 1871;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(entityMeta);
		result.append(",fields=").append(mpclctef);
		result.append("]");
		return result.toString();
	}

}	// class CollectableMasterDataEntity
