package org.nuclos.common.preferences;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.ServiceLocator;
import org.nuclos.common2.XMLUtils;
import org.nuclos.server.common.ejb3.PreferencesFacadeRemote;
import org.nuclos.server.common.valueobject.PreferencesVO;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Node;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * ATTENTION: This PreferencesFactory will give you a NEW userRoot each time
 * {@link #userRoot()} is called.
 * 
 * Refer to {@link org.nuclos.common.preferences.NuclosPreferencesSingleton} for a more 'sane'
 * implementation.
 */
@Component("domPreferencesFactory")
public class DOMPreferencesFactory implements PreferencesFactory {
	
	private static final Logger LOG = Logger.getLogger(DOMPreferencesFactory.class);
	
	private static final MyErrorHandler ERROR_HANDLER = new MyErrorHandler();
	
	private static final MyEntityResolver ENTITY_RESOLVER = new MyEntityResolver();
	
	private static DOMPreferencesFactory INSTANCE;
	
	//
	
	private final DocumentBuilderFactory dbf;
	
	private final TransformerFactory tf = TransformerFactory.newInstance();
		
	DOMPreferencesFactory() {
		try {
            tf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			dbf = XMLUtils.newSecuredDocumentBuilderFactory();
			dbf.setCoalescing(false);
			dbf.setIgnoringComments(true);
			dbf.setIgnoringElementContentWhitespace(true);
		}
		catch (ParserConfigurationException | TransformerConfigurationException e) {
			throw new ExceptionInInitializerError(e);
		}
        INSTANCE = this;
	}
	
	static DOMPreferencesFactory getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	@Override
	public Preferences systemRoot() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Create a NEW userRoot.
	 */
	@Override
	public Preferences userRoot() {
		final DocumentBuilder db = newDocumentBuilder();
		final Document document = db.newDocument();
		// http://stackoverflow.com/questions/1409708/how-can-produce-a-doctype-declaration-with-dom-level-3-serialization-api
		final DocumentType dt = db.getDOMImplementation().createDocumentType(
				"preferences", null, "http://java.sun.com/dtd/preferences.dtd");
		document.appendChild(dt);
		final DOMPreferences result = new DOMPreferences(document, true);
		return result;
	}
	
	/**
	 * Write a complete preferences tree (?xml/!DOCTYPE/preferences/root/...) to DB.
	 */
	void save(DOMPreferences prefs) throws BackingStoreException {
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			// not implemented as well
			// prefsUser.exportSubtree(baos);
			write(prefs.getDOMDocument(), baos, false);
			byte[] bytes = baos.toByteArray();
			
			final PreferencesFacadeRemote facade = ServiceLocator.getInstance().getFacade(PreferencesFacadeRemote.class);
			facade.modifyUserPreferences(new PreferencesVO(bytes));
		}
		catch (Exception ex) {
			throw new BackingStoreException(ex);
		}
	}
	
	/**
	 * Write a complete preferences tree (?xml/!DOCTYPE/preferences/root/...).
	 */
	public void write(DOMPreferences prefs, OutputStream out, boolean pretty) throws IOException {
		write(prefs.getDOMDocument(), out, pretty);
	}
	
	/**
	 * Write a complete preferences tree (?xml/!DOCTYPE/preferences/root/...).
	 */
	void write(Document document, OutputStream out, boolean pretty) throws IOException {
		Transformer trans;
		try {
			trans = tf.newTransformer();
			// http://stackoverflow.com/questions/8438105/how-to-remove-standalone-attribute-declaration-in-xml-document
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			trans.setOutputProperty(OutputKeys.VERSION, "1.0");
			trans.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
			// http://stackoverflow.com/questions/1264849/pretty-printing-output-from-javax-xml-transform-transformer-with-only-standard-j
			if (pretty) {
				trans.setOutputProperty(OutputKeys.INDENT, "yes");
				trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			}

			trans.transform(new DOMSource(document), new StreamResult(out));
		}
		catch (TransformerConfigurationException e) {
			throw new IllegalStateException(e);
		}
		catch (TransformerException e) {
			throw new IOException(e);
		}
	}
	
	/**
	 * Write a node and all in descendants (without a XML declaration nor doctype).
	 */
	void writeRawSubtree(Node node, OutputStream out, boolean pretty) throws IOException {
		Transformer trans;
		try {
			trans = tf.newTransformer();
			// http://stackoverflow.com/questions/8438105/how-to-remove-standalone-attribute-declaration-in-xml-document
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			trans.setOutputProperty(OutputKeys.VERSION, "1.0");
			trans.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
			// http://stackoverflow.com/questions/1264849/pretty-printing-output-from-javax-xml-transform-transformer-with-only-standard-j
			if (pretty) {
				trans.setOutputProperty(OutputKeys.INDENT, "yes");
				trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			}
			trans.transform(new DOMSource(node), new StreamResult(out));
		}
		catch (TransformerConfigurationException e) {
			throw new IllegalStateException(e);
		}
		catch (TransformerException e) {
			throw new IOException(e);
		}
	}
	
	/**
	 * Read a complete preferences tree (?xml/!DOCTYPE/preferences/root/...).
	 */
	public DOMPreferences read(InputStream in, boolean createRoot) throws IOException {
		return new DOMPreferences(newDocumentBuilder(), in, true, createRoot);
	}
	
	private DocumentBuilder newDocumentBuilder() {
		DocumentBuilder result;
		try {
			result = dbf.newDocumentBuilder();
		}
		catch (ParserConfigurationException e) {
			throw new IllegalStateException(e);
		}
		result.setErrorHandler(ERROR_HANDLER);
		result.setEntityResolver(ENTITY_RESOLVER);
		return result;
	}
	
	private static class MyErrorHandler implements ErrorHandler {
		
		private MyErrorHandler() {
		}

		@Override
		public void warning(SAXParseException exception) throws SAXException {
			LOG.warn(exception.toString());
		}

		@Override
		public void error(SAXParseException exception) throws SAXException {
			LOG.warn(exception.toString(), exception);
		}

		@Override
		public void fatalError(SAXParseException exception) throws SAXException {
			LOG.error(exception.toString(), exception);
		}
		
	}
	
	private static class MyEntityResolver implements EntityResolver {
		
		private MyEntityResolver() {
		}

		@Override
		public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
			final InputSource result;
			if (DOMPreferences.DOCTYPE_SYSTEMID.equals(systemId)) {
				URL url = LangUtils.getClassLoaderThatWorksForWebStart().getResource(
						"org/nuclos/common/preferences/preferences.dtd");
				if (url != null) {
					result = new InputSource(url.openStream());
					result.setSystemId(DOMPreferences.DOCTYPE_SYSTEMID);
				} else {
					LOG.warn("resolveEntity: no local dtd found for system=" + systemId);
					result = null;
				}
			} else {
				LOG.warn("resolveEntity: Unknown Entity in preferences: public=" + publicId + " system=" + systemId);
				result = null;
			}
			return result;
		}
		
	}
	
}
