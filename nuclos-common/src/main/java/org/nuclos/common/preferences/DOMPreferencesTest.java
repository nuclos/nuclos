package org.nuclos.common.preferences;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.prefs.Preferences;

import org.apache.xmlbeans.SystemProperties;
import org.nuclos.common2.searchfilter.EntitySearchFilter2Support;

public class DOMPreferencesTest {
	
	private static final File HOME = new File(SystemProperties.getProperty("user.home"));
	
	public static void main(String[] args) throws Exception {
		test3();
	}
	
	private static void test3() throws Exception {
		// final DOMPreferencesFactory prefsFactory = new DOMPreferencesFactory();
		final File pref = new File(HOME, "searchfilter.xml");
		// final Preferences root = prefsFactory.read(new FileInputStream(pref), true);
		final Preferences root = Preferences.userRoot();
		Preferences.importPreferences(new FileInputStream(pref));
		
		Preferences prefs = root.node("org/nuclos/client");
		Preferences prefsSearchfilter = null;
		if (prefs.nodeExists(EntitySearchFilter2Support.PREFS_NODE_SEARCHFILTERS)) {
			prefsSearchfilter = prefs.node(EntitySearchFilter2Support.PREFS_NODE_SEARCHFILTERS);
		}

		if (prefsSearchfilter.nodeExists("angenommen")) {
			prefs = prefsSearchfilter.node("angenommen");
			
			prefs.exportNode(new FileOutputStream(new File(HOME, "exportNode.xml")));
			prefs.exportSubtree(new FileOutputStream(new File(HOME, "exportSubtree.xml")));
		}
	}
	
	private static void test2() throws Exception {
		final DOMPreferencesFactory prefsFactory = new DOMPreferencesFactory();
		final File pref = new File(HOME, "searchfilter.xml");
		
		final Preferences root = prefsFactory.read(new FileInputStream(pref), true);
		Preferences prefs = root.node("org/nuclos/client");
		Preferences prefsSearchfilter = null;
		if (prefs.nodeExists(EntitySearchFilter2Support.PREFS_NODE_SEARCHFILTERS)) {
			prefsSearchfilter = prefs.node(EntitySearchFilter2Support.PREFS_NODE_SEARCHFILTERS);
		}

		if (prefsSearchfilter.nodeExists("angenommen")) {
			prefs = prefsSearchfilter.node("angenommen");
			
			// prefs.exportNode(new FileOutputStream(new File(HOME, "exportNode.xml")));
			prefs.exportSubtree(new FileOutputStream(new File(HOME, "exportSubtree.xml")));
		}
	}
	
	private static void test1() throws Exception {
		final DOMPreferencesFactory fac = new DOMPreferencesFactory();
		final File emptyPref = new File(HOME, "emptyPref.xml");
		final File pref = new File(HOME, "pref.xml");
		final File pref2 = new File(HOME, "pref2.xml");
		
		final DOMPreferences userRoot = (DOMPreferences) fac.userRoot();
		fac.write(userRoot, new FileOutputStream(emptyPref), true);
		
		userRoot.put("root1", "1");
		userRoot.put("root2", "2");
		Preferences sub = userRoot.node("sub");
		sub.put("sub1", "1");
		sub.put("sub2", "2");
		
		fac.write((DOMPreferences) sub, new FileOutputStream(pref), true);
		
		final DOMPreferences read = fac.read(new FileInputStream(pref), true);
		
		read.node("sub").removeNode();
		read.remove("root1");
		
		fac.write((DOMPreferences) read, new FileOutputStream(pref2), true);
	}

}
