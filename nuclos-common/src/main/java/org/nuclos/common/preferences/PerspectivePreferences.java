package org.nuclos.common.preferences;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.UID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Sebastian Debring on 3/19/2019.
 */
public class PerspectivePreferences implements Serializable {

	private static final long serialVersionUID = -8250369867580667904L;

	final NuclosPreferenceType preftype = NuclosPreferenceType.PERSPECTIVE;

	private final UID id;
	private boolean shared;
	private boolean selected;

	private String name;
	private UID layoutId;
	private UID searchTemplatePrefId;
	private UID sideviewMenuPrefId;
	private Map<String, UID> subformTablePrefIds;
	private boolean layoutForNew;

	public PerspectivePreferences(final UID id) {
		this.id = id;
	}

	public void readFromDto(PerspectivePreferencesDTO dto) {
		this.name = dto.getName();
		this.layoutId = dto.getLayoutId();
		this.searchTemplatePrefId = dto.getSearchTemplatePrefId();
		this.sideviewMenuPrefId = dto.getSideviewMenuPrefId();
		this.subformTablePrefIds = dto.getSubformTablePrefIds();
		this.layoutForNew = dto.isLayoutForNew();
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public NuclosPreferenceType getPreftype() {
		return preftype;
	}

	public UID getId() {
		return id;
	}

	public boolean isShared() {
		return shared;
	}

	public void setShared(final boolean shared) {
		this.shared = shared;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(final boolean selected) {
		this.selected = selected;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public UID getLayoutId() {
		return layoutId;
	}

	public void setLayoutId(final UID layoutId) {
		this.layoutId = layoutId;
	}

	public UID getSearchTemplatePrefId() {
		return searchTemplatePrefId;
	}

	public void setSearchTemplatePrefId(final UID searchTemplatePrefId) {
		this.searchTemplatePrefId = searchTemplatePrefId;
	}

	public UID getSideviewMenuPrefId() {
		return sideviewMenuPrefId;
	}

	public void setSideviewMenuPrefId(final UID sideviewMenuPrefId) {
		this.sideviewMenuPrefId = sideviewMenuPrefId;
	}

	public Map<String, UID> getSubformTablePrefIds() {
		return subformTablePrefIds;
	}

	public void setSubformTablePrefIds(final Map<String, UID> subformTablePrefIds) {
		this.subformTablePrefIds = subformTablePrefIds;
	}

	public boolean isLayoutForNew() {
		return layoutForNew;
	}

	public void setLayoutForNew(final boolean layoutForNew) {
		this.layoutForNew = layoutForNew;
	}
}
