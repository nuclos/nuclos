//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.util.List;

import org.nuclos.common.CommonSecurityCache;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

public class SearchFilterTaskListTablePreferencesManager extends TablePreferencesManager {

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(SearchFilterTaskListTablePreferencesManager.class);

	private final String resultProfile;

	public SearchFilterTaskListTablePreferencesManager(final SearchFilterVO searchFilterVO,
								   final IPreferencesProvider prefProv,
								   final IMetaProvider metaProv,
								   final CommonSecurityCache securityCache,
								   final String username,
								   final UID mandator
	) {
		super(searchFilterVO.getEntity(), null, null, searchFilterVO.getId(), NuclosPreferenceType.SEARCHFILTER_TASKLIST_TABLE,
				prefProv, metaProv, securityCache, username, mandator);
		resultProfile = searchFilterVO.getResultProfile();
	}

	@Override
	public TablePreferences createDefault() {
		TablePreferences tp = null;

		// no tasklist prefs yet, try to load a "profile" if set in searchfilter
		if (resultProfile != null) {
			final List<TablePreferences> entityTablePreferences = prefProv.getTablePreferences(null, NuclosPreferenceType.TABLE,
					entity, layout);
			for (TablePreferences etp : entityTablePreferences) {
				if (resultProfile.equals(etp.getName())) {
					// profile for searchfilter found. Import as default...
					tp = etp.copy();
					tp.setUID(new UID());
					tp.setType(NuclosPreferenceType.SEARCHFILTER_TASKLIST_TABLE);
					tp.setSearchFilter(searchFilter);
					tp.setLayout(layout);
					break;
				}
			}
		}

		if (tp == null) {
			// get last selected from entity as default
			final TablePreferencesManager entityTablePreferencesManager = new TablePreferencesManager(entity, layout, null, NuclosPreferenceType.TABLE,
					prefProv, metaProv, securityCache, username, mandator);
			tp = entityTablePreferencesManager.getSelected().copy();
			tp.setUID(new UID());
			tp.setType(NuclosPreferenceType.SEARCHFILTER_TASKLIST_TABLE);
			tp.setSearchFilter(searchFilter);
			tp.setLayout(layout);
		}

		if (tp == null) {
			tp = super.createDefault();
		}

		return tp;
	}

	public void transferFromEntity(TablePreferences tp) {
		// like not shared reset
		final UID id = tp.getUID();
		tp.clearAndImport(createDefault());
		tp.setUID(id);
		try {
			prefProv.update(tp, getEntity());
		} catch (CommonFinderException e) {
			LOG.warn("TransferFromEntity ot table preference " + tp.getUID() + " failed. Not found!");
		} catch (CommonPermissionException e) {
			LOG.warn("TransferFromEntity of table preference " + tp.getUID() + " is not allowed.");
		}
	}
}
