package org.nuclos.common.printservice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.Media;
import javax.print.attribute.standard.MediaTray;
import javax.print.attribute.standard.Sides;

import org.apache.log4j.Logger;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonPrintException;
import org.nuclos.server.ruleengine.NuclosFatalRuleException;

public class PrintServiceLocator {
	
	private static final Logger LOG = Logger.getLogger(PrintServiceLocator.class);

	public static javax.print.PrintService lookupPrintServiceByPrinterName(final String printerName) {
		javax.print.PrintService result = PrintServiceLookup.lookupDefaultPrintService();
		javax.print.PrintService[] prservices = PrintServiceLookup.lookupPrintServices(null, null);
		if (null == prservices || 0 >= prservices.length) {
			if (null != result) {
				prservices = new javax.print.PrintService[] { result };
			}
			else {
				throw new NuclosFatalRuleException(SpringLocaleDelegate.getInstance().getMessage("AbstractReportExporter.5", "Es ist kein Print-Service installiert."));
			}
		}

		if (printerName != null) {
			for (int i = 0; i < prservices.length; i++) {
				if (prservices[i].getName().equals(printerName)) {
					result = prservices[i];
					break;
				}
			}
			if (result == null) {
				throw new NuclosFatalRuleException(SpringLocaleDelegate.getInstance().getMessage("AbstractReportExporter.5", "Es ist kein passender Print-Service installiert.") + " (" + printerName + ")");
			}
		}
		return result;
	}
	
	public static javax.print.PrintService[] lookupPrintServices() {
		javax.print.PrintService def = PrintServiceLookup.lookupDefaultPrintService();
		javax.print.PrintService[] result = PrintServiceLookup.lookupPrintServices(null, null);
		if (result == null || result.length == 0) {
			if (def != null) {
				result = new javax.print.PrintService[] { def };
			}
			else {
				result = new javax.print.PrintService[] {};
			}
		}
		return result;
	}
	
	public static PrintRequestAttributeSet createAttributeSet(final PrintService ps,
			final Integer trayNumber, final Integer copies, final Boolean isDuplex) throws CommonPrintException {
				final PrintRequestAttributeSet attributes = new HashPrintRequestAttributeSet();

		// setup duplex/one page attribute
		if (Boolean.TRUE.equals(isDuplex)) {
			attributes.add(Sides.DUPLEX);
		}

		// setup media tray attribute
		if (trayNumber != null) {
			final DocFlavor flavor = null;
			final Map<Integer, MediaTray> trays = createTrayMap(ps, flavor);
			final MediaTray tray = trays.get(trayNumber);
			if (tray != null) {
				attributes.add(tray);
			}
		}
		
		// setup quantity of copies attribute
		attributes.add(new Copies(RigidUtils.defaultIfNull(copies, 1)));
		return attributes;
	}
	
	/**
	 * 
	 * @param ps
	 *            {@link PrintService}
	 * @param flavor
	 *            {@link DocFlavor}
	 * @return
	 */
	public static Map<Integer, MediaTray> createTrayMap(PrintService ps, DocFlavor flavor) {
		final Map<Integer, MediaTray> result = new HashMap<Integer, MediaTray>(10);
		Object o = ps.getSupportedAttributeValues(Media.class, flavor, null);
		LOG.debug("supported attributes " + o);
		int i = 0;
		if (o != null && (o instanceof Media[])) {

			Media[] mediaEntries = (Media[]) o;
			for (final Media m : mediaEntries) {
				if (m instanceof MediaTray) {
					// result.put(m.getValue(), m);
					result.put(i, (MediaTray) m);
					++i;
				}

			}
		}
		return result;
	}
	
	public static List<PrintServiceTO.Tray> createTrayList(PrintService ps) {
		List<PrintServiceTO.Tray> result = new ArrayList<PrintServiceTO.Tray>();
		
		final String name = ps.getName();
		final Set<String> usedNames = new HashSet<String>();
		for (Entry<Integer, MediaTray> entry : createTrayMap(ps, null).entrySet()) {
			MediaTray tray = entry.getValue();
			Integer traynumber = entry.getKey();
			String trayname = traynumber.toString(); 
			try {
				// toString() returns something like 'Auto Select', getName() only 'auto'
				String testname = tray.toString();
				if (testname != null && !usedNames.contains(testname)) {
					trayname = testname;
					usedNames.add(trayname);
				}
			} catch (Exception ex) {
				LOG.error(ex.getMessage(), ex);
			}
			LOG.debug("Print service " + name + ": Tray " + traynumber + " with name '" + trayname + "' found.");
			PrintServiceTO.Tray trayTO = new PrintServiceTO.Tray(traynumber, trayname, name);
			result.add(trayTO);
		}
		Collections.sort(result);
		return result;
	}
	
}
