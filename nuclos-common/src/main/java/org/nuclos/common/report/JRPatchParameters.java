package org.nuclos.common.report;

public class JRPatchParameters {

	/**
	 * Whether the console command, should patch or just show informations about the jasper reports.
	 */
	public boolean patch;

	/**
	 * Whether all deprecated pdfFonts should be removed and replaced by a normal font.
	 */
	public boolean replaceDeprecatedPdfFonts;

	/**
	 * Whether to override all font tags in the report with a custom chosen one.
	 */
	public boolean overwriteReportFonts;

	/**
	 * If a replacement font is set, it will be used to override all fonts in the report.
	 */
	public String replacementFont;

	/**
	 * Whether the patch should be forced on reports that have already been patched.
	 * If not set, reports that match the current jasper version of nuclos, will not be patched.
	 */
	public boolean forceAll;

	public JRPatchParameters() {

	}

}
