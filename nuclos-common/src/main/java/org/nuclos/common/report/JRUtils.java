package org.nuclos.common.report;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;

public class JRUtils {

	private static final Logger LOG = Logger.getLogger(JRUtils.class);

	public static final JRVersion NUCLOS_JASPER_VERSION = JRVersion.getNuclosJasperReportsVersion();

	private static final String DEFAULT_PDF_FONT_REPLACEMENT = "DejaVu Sans";

	private static final boolean PATCH_PDF_FONT = true;

	/**
	 * This Method checks if a .jrxml can/needs to be patched to the current JasperReports Version used by Nuclos.
	 * If there is a version string present, that matches the Jasper Version currently used by Nuclos, a patch is not
	 * required. Otherwise Nuclos should attempt to patch the .jrxml file.
	 *
	 * @param xml
	 * @return false, if a version string is present that matches the Jasper Version used by Nuclos - false, otherwise
	 */
	public static boolean isPatchRequired(String xml) {
		if(xml.contains("JasperReports Library version ")) {
			JRVersion reportVersion = parseJasperVersion(xml);
			//If the reportVersion is higher or equal to nuclos jasper version
			//there is no need to upgrade the report
			if(reportVersion != null && reportVersion.compareProjectVersions(NUCLOS_JASPER_VERSION) >= 0) {
				return false;
			}
		}
		//If we do not find a version tag, we try to upgrade the .jrxml by default.
		return true;
	}

	public static boolean isPatchRequired(DefaultReportOutputVO reportOutputVO) {
		return isPatchRequired(new String(reportOutputVO.getSourceFileContent().getData()));
	}

	/**
	 * This method could be used, when importing a new report.
	 * It checks if the report is compatible with Nuclos Jasper Version.
	 * Or if it was made for a newer library version.
	 * @param xml
	 * @return
	 */
	public static boolean isVersionTooRecent(String xml) {
		if(xml.contains("JasperReports Library version ")) {
			JRVersion reportVersion = parseJasperVersion(xml);
			//If the reportVersion is higher or equal to nuclos jasper version
			//there is no need to upgrade the report
			if(reportVersion != null
					&& reportVersion.compareProjectVersions(NUCLOS_JASPER_VERSION) > 0) {
				LOG.info(reportVersion + " is more recent than " + NUCLOS_JASPER_VERSION);
				return true;
			}
		}
		//If we do not find a version tag or the reportVersion is lower than the Jasper Version
		// used by Nuclos, we try to upgrade the .jrxml by default.
		return false;
	}

	public static String getReportVersion(DefaultReportOutputVO reportOutputVO) {
		String xml = new String(reportOutputVO.getSourceFileContent().getData());
		if(xml.contains("JasperReports Library version")) {
			JRVersion reportVersion = parseJasperVersion(xml);

			if(reportVersion != null) {
				return reportVersion.toString();
			}
		}
		return "Unknown";
	}

	public static String getReportVersion(String xml) {
		if(xml.contains("JasperReports Library version")) {
			JRVersion reportVersion = parseJasperVersion(xml);

			if(reportVersion != null) {
				return reportVersion.toString();
			}
		}
		return "Unknown";
	}

	private static JRVersion parseJasperVersion(String xml) {
		Pattern p = Pattern.compile("^(.*) JasperReports Library version ((a-zA-Z0-9.-)*)(.*)-->$", Pattern.MULTILINE);
		Matcher m = p.matcher(xml);
		final JRVersion reportVersion;
		if (m.find()) {
			String wholeVersionString = m.group(4);
			if (wholeVersionString.contains("-")) {
				//the version string contains a projectVersion AND a buildNumber
				String[] split = wholeVersionString.split("-");
				reportVersion = new JRVersion(split[0], split[1]);
			} else {
				//the version string ONLY contains a projectVersion
				reportVersion = new JRVersion(wholeVersionString, null);
			}
		} else {
			reportVersion = null;
		}
		return reportVersion;
	}

	public static String patchXML(String xml, JRPatchParameters patchParameters) {
		xml = xml.replaceAll("<!-- Created with.*", "");
		xml = xml.replaceFirst("<jasperReport",
				"<!-- Created with Nuclos using JasperReports Library version " + NUCLOS_JASPER_VERSION +"  -->\n<jasperReport");

		if(patchParameters.replaceDeprecatedPdfFonts) {
			xml = removePdfFontAttributes(xml);
			LOG.info("Replacing deprecated PDF Fonts");
		}
		if(patchParameters.overwriteReportFonts) {
			xml = overwriteReportFonts(xml, patchParameters.replacementFont);
			LOG.info("Replacing all fonts with " + patchParameters.replacementFont);
		}

		return xml;
	}

	public static boolean containsDeprecatedPdfFontAttributes(String xml) {
		return xml.contains("pdfFontName=\"");
	}

	/**
	 * Removes all pdfFontName="..." from the xml string and adds a fontName attribute, if not already present.
	 *
	 * Reason for removing the attribute:
	 * The pdfFontName attribute is deprecated and leads to a problem where text is not rendered.
	 * @param xml the string representation of a .jrxml file
	 * @return transformed string with pdfFontName replaced by fontName
	 */
	private static String removePdfFontAttributes(String xml) {
		Pattern fontPattern = Pattern.compile("(<[^<]*(font )[^>]*>)");
		Pattern reportFontPattern = Pattern.compile("(<[^<]*(reportFont )[^>]*>)");

		return removePdfFontAttributes(reportFontPattern, removePdfFontAttributes(fontPattern, xml));
	}

	private static String overwriteReportFonts(String xml, String fontReplacement) {
		Pattern fontPattern = Pattern.compile("(<[^<]*(font )[^>]*>)");
		Pattern reportFontPattern = Pattern.compile("(<[^<]*(reportFont )[^>]*>)");

		if(fontReplacement == null) {
			fontReplacement = DEFAULT_PDF_FONT_REPLACEMENT;
		}

		return overwriteReportFonts(reportFontPattern, overwriteReportFonts(fontPattern, xml, fontReplacement), fontReplacement);
	}

	private static String removePdfFontAttributes(Pattern pattern, String xml) {
		Matcher matcher = pattern.matcher(xml);
		StringBuilder output = new StringBuilder();
		int lastIndex = 0;
		while(matcher.find()) {
			String wholeTag = matcher.group(1);
			wholeTag = wholeTag.replaceAll("( )?pdfFontName=\".*\"", "");

			output.append(xml, lastIndex, matcher.start()).append(wholeTag);
			lastIndex = matcher.end();
		}
		if(lastIndex < xml.length()) {
			output.append(xml, lastIndex, xml.length());
		}
		return output.toString();
	}

	private static String overwriteReportFonts(Pattern pattern, String xml, String fontReplacement) {
		Matcher matcher = pattern.matcher(xml);
		StringBuilder output = new StringBuilder();
		int lastIndex = 0;
		while(matcher.find()) {
			String wholeTag = matcher.group(1);

			if(!wholeTag.matches(".*fontName=\".*\".*")) {
				wholeTag = wholeTag.replaceFirst("<font", "<font fontName=\"" + fontReplacement + "\"");
			} else {
				wholeTag = wholeTag.replaceFirst("fontName=\"[a-zA-Z0-9\\s\\-]+\"", "fontName=\"" + fontReplacement + "\"");
			}

			output.append(xml, lastIndex, matcher.start()).append(wholeTag);
			lastIndex = matcher.end();
		}
		if(lastIndex < xml.length()) {
			output.append(xml, lastIndex, xml.length());
		}
		return output.toString();
	}

}
