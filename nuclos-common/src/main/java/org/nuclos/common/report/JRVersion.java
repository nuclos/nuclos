package org.nuclos.common.report;

import org.apache.maven.artifact.versioning.ComparableVersion;

/**
 * This class is used to compare JasperReports library versions and thus to determine, whether a JasperReport
 * should be patched to match the JasperReports version currently used in Nuclos or not.
 *
 * A JasperReport Library Version consists of a project Version and a optional build number.
 * E.g. 6.17.0 and 6.17.0-6d93193241dd8cc42629e188b94f9e0bc5722efd are both valid Jasper Versions.
 *
 * Obviously a report should be upgraded if the project version is smaller. But what if a report only has a
 * projectVersion and no buildNumber but the JasperLibrary in Nuclos has both?
 * For now we only patch reports, when their project Version is not defined or lower than in Nuclos.
 */
public class JRVersion implements Comparable<JRVersion> {

	private ComparableVersion projectVersion;
	private String buildNumber;

	public JRVersion(String projectVersion, String buildNumber) {
		this.projectVersion = new ComparableVersion(projectVersion);
		this.buildNumber = buildNumber;

		assert projectVersion != null;
	}

	public static JRVersion getNuclosJasperReportsVersion() {
		String versionString = net.sf.jasperreports.engine.JasperCompileManager.class.getPackage().getImplementationVersion();
		String [] split = versionString.split("-");
		String projectVersion = split[0];
		String buildNumber = null;
		if(split.length == 2) {
			buildNumber = split[1];
		}
		JRVersion version = new JRVersion(projectVersion, buildNumber);
		return version;
	}

	public int compareProjectVersions(JRVersion jv) {
		return projectVersion.compareTo(jv.projectVersion);
	}

	@Override
	public int compareTo(final JRVersion o) {
		int cmpProjectVersion = projectVersion.compareTo(o.projectVersion);
		if(cmpProjectVersion == 0) {
			if(buildNumber != null && o.buildNumber != null) {
				return buildNumber.compareTo(o.buildNumber);
			}
		}
		return cmpProjectVersion;
	}

	@Override
	public String toString() {
		if(buildNumber != null) {
			return projectVersion + "-" + buildNumber;
		} else {
			return projectVersion.toString().trim();
		}
	}
}
