//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value object representing a parameter that will be included in the report it is assigned to.
 * A parameter can have one of the following values: Text, Resource (File), Sourcefile (include a .jrxml directly)
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:thomas.schiffmann@novabit.de">thomas.schiffmann</a>
 * @version 00.01.000
 */
public class ReportParameterVO extends NuclosValueObject<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5338263425735084306L;
	private UID reportOutputUID;
	private String parameterName;
	private String parameterValueText;
	private UID parameterValueResource;
	private String sourcefileName;
	private ByteArrayCarrier sourcefileContent;
	private ByteArrayCarrier reportCLS;

	/**
	 * for deserialization only
	 */
	protected ReportParameterVO() {
		super();
	}

	/**
	 * constructor used by client only
	 */
	public ReportParameterVO(final NuclosValueObject<UID> nvo, final UID reportOutputUID, final String parameterName,
							 final String parameterValueText, final UID parameterValueResource, final String sourcefileName,
							 final ByteArrayCarrier sourcefileContent, final ByteArrayCarrier reportCLS) {
		
		super(nvo);

		this.reportOutputUID = reportOutputUID;
		this.parameterName = parameterName;
		this.parameterValueText = parameterValueText;
		this.parameterValueResource = parameterValueResource;
		this.sourcefileName = sourcefileName;
		this.sourcefileContent = sourcefileContent;
		this.reportCLS = reportCLS;
	}

	/**
	 * creates a report vo from a suitable masterdata cvo.
	 * @param mdvo
	 */
	public ReportParameterVO(MasterDataVO<UID> mdvo) {
		this(mdvo.getNuclosValueObject(),
				mdvo.getFieldUid(E.SUBREPORT.reportoutput),
				mdvo.getFieldValue(E.SUBREPORT.parametername),
				mdvo.getFieldValue(E.SUBREPORT.parameterText),
				mdvo.getFieldUid(E.SUBREPORT.parameterResource),
				mdvo.getFieldValue(E.SUBREPORT.sourcefilename),
				mdvo.getFieldValue(E.SUBREPORT.sourcefileContent),
				mdvo.getFieldValue(E.SUBREPORT.reportCLS));
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getParameterValueText() { return parameterValueText;}

	public void setParameterValueText(final String parameterValueText) { this.parameterValueText = parameterValueText; }

	public UID getParameterResource() { return parameterValueResource; }

	public void setParameterValueResource(UID resourceUid) { this.parameterValueResource = resourceUid; }

	public String getSourcefileName() {
		return sourcefileName;
	}

	public void setSourcefileName(String sourcefileName) {
		this.sourcefileName = sourcefileName;
	}

	public ByteArrayCarrier getSourcefileContent() {
		return sourcefileContent;
	}

	public void setSourcefileContent(ByteArrayCarrier sourcefileContent) {
		this.sourcefileContent = sourcefileContent;
	}

	public ByteArrayCarrier getReportCLS() {
		return reportCLS;
	}

	public void setReportCLS(ByteArrayCarrier reportCLS) {
		this.reportCLS = reportCLS;
	}

	public UID getReportOutputUID() {
		return reportOutputUID;
	}

	@Override
	public void validate() throws CommonValidationException {
		if (StringUtils.isNullOrEmpty(this.getParameterName())) {
			throw new CommonValidationException("report.error.validation.value");
		}
		boolean noValueSet = StringUtils.isNullOrEmpty(this.getParameterValueText())
				&& StringUtils.isNullOrEmpty(this.getSourcefileName()) && this.getParameterResource() == null;
		if (noValueSet) {
			throw new CommonValidationException("No value set for parameter " + this.getParameterName());
		}
		if ((!StringUtils.isNullOrEmpty(this.getSourcefileName()) && !StringUtils.isNullOrEmpty(this.getParameterValueText()))
				|| (!StringUtils.isNullOrEmpty(this.getSourcefileName()) && this.getParameterResource() != null)
				|| (!StringUtils.isNullOrEmpty(this.getParameterValueText()) && this.getParameterResource() != null)) {
			throw new CommonValidationException("Only one value allowed per parameter.");
		}

		if (StringUtils.isNullOrEmpty(this.getSourcefileName())) {
			throw new CommonValidationException("report.error.validation.description");
		}
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("id=").append(getId());
		result.append(",paramName=").append(getParameterName());
		result.append(",valueText=").append(getParameterValueText());
		result.append(",valueResource=").append(getParameterResource());
		result.append(",valueSrcFile=").append(getSourcefileName());
		result.append("]");
		return result.toString();
	}

}	// class ReportParameterVO
