//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.UID;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.Localizable;
import org.nuclos.common2.exception.CommonValidationException;

public interface ReportVO {

	public static enum OutputType implements KeyEnum<String>, Localizable {
		// Note that the order of these constants is significant.

		/** single report */
		SINGLE("Single", "reportOutputType.single"),

		/** collective report: multiple reports that go to different files, but are run as one report. */
		COLLECTIVE("Collective", "reportOutputType.collective"),

		/** Excel report: multiple reports go to different sheets in the same Excel file */
		EXCEL("Excel", "reportOutputType.excel");

		private final String value;
		private final String resourceId;
		
		private OutputType(String value, String resourceId) {
			this.value = value;
			this.resourceId = resourceId;
		}
		
		@Override
		public String getValue() {
			return this.value;
		}
		
		@Override
		public String getResourceId() {
			return resourceId;
		}
	}	// enum OutputType

	public static enum ReportType implements KeyEnum<Integer>, Localizable {
		REPORT(0, "reportType.report"),
		FORM(1, "reportType.form");

		private final Integer id;
		private final String resourceId;

		private ReportType(int id, String resourceId) {
			this.id = id;
			this.resourceId = resourceId;
		}

		@Override
		public Integer getValue() {
			return id;
		}
		
		@Override
		public String getResourceId() {
			return resourceId;
		}
	}


	public ReportType getType();

	/**
	 * @param type
	 */
	public void setType(ReportType type);

	public String getName();

	public void setName(String sName);

	public String getDescription();

	public void setDescription(String sDescription);

	public UID getDatasourceId();

	public void setDatasourceId(UID dataSourceUID);

	public OutputType getOutputType();

	public void setOutputType(OutputType outputtype);

	public String getParameter();

	public void setParameter(String sParameter);

	public String getFilename();

	public void setFilename(String sFilename);

	public boolean getAttachDocument();

	public void setAttachDocument(boolean bAttach);

	public void validate() throws CommonValidationException;

}