package org.nuclos.common.restapis;

import org.nuclos.common2.KeyEnum;

public enum DateLibs implements KeyEnum<String> {
	JODA("joda"),
	LEGACY("legacy"),
	JAVA8LOCALDATETIME("java8-localdatetime"),
	JAVA8("java8");

	private String value;

	DateLibs(final String value) {
		this.value = value;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}
}
