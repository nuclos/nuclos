package org.nuclos.common.restapis;

import org.nuclos.common2.KeyEnum;

public enum HttpClientLibs implements KeyEnum<String> {

	JERSEY1("jersey1"),
	JERSEY2("jersey2"),
	JERSEY3("jersey3"),
	FEIGN("feign"),
	OKHTTPGSON("okhttp-gson"),
	RETROFIT2("retrofit2"),
	RESTTEMPLATE("resttemplate"),
	WEBCLIENT("webclient"),
	RESTEASY("resteasy"),
	VERTX("vertx"),
	GOOGLEAPICLIENT("google-api-client"),
	RESTASSURED("rest-assured"),
	NATIVE("native"),
	MICROPROFILE("microprofile"),
	APACHEHTTPCLIENT("apache-httpclient");

	private final String value;

	HttpClientLibs(final String value) {
		this.value = value;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}
}
