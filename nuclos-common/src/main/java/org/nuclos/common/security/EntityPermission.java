package org.nuclos.common.security;

public class EntityPermission {
	private final boolean readAllowed;
	private final boolean updateAllowed;
	private final boolean insertAllowed;
	private final boolean deleteAllowed;
	
	public EntityPermission(boolean readAllowed, boolean updateAllowed, boolean insertAllowed, boolean deleteAllowed) {
		this.readAllowed = readAllowed;
		this.updateAllowed = updateAllowed;
		this.insertAllowed = insertAllowed;
		this.deleteAllowed = deleteAllowed;
	}

	public boolean isReadAllowed() {
		return readAllowed;
	}

	public boolean isUpdateAllowed() {
		return updateAllowed;
	}

	public boolean isInsertAllowed() {
		return insertAllowed;
	}

	public boolean isDeleteAllowed() {
		return deleteAllowed;
	}
	
	@Override
	public String toString() {
		return "Permission: Read=" + readAllowed + " Update=" + updateAllowed + " Insert=" + insertAllowed + " Delete=" + deleteAllowed;
	}
	
}
