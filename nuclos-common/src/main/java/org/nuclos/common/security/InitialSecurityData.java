//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.security;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.MandatorVO;
import org.nuclos.common.UID;
import org.nuclos.server.common.MasterDataPermissions;
import org.nuclos.server.common.ModulePermissions;

public class InitialSecurityData {

	private String userName;
	private UID userUid;
	private boolean superUser;
	private boolean withCommunicationAccount;
	private Set<String> allowedActions;
	private Set<UID> allowedCustomActions;
	private ModulePermissions modulePermissions;
	private MasterDataPermissions masterDataPermissions;
	private Set<UID> dynamicTasklists;
	private Map<UID, String> userRoles;
	private Map<UID, List<MandatorVO>> mandatoryByLevel;

	public String getUserName() {
		return userName;
	}

	public void setUserName(final String userName) {
		this.userName = userName;
	}

	public UID getUserUid() {
		return userUid;
	}

	public void setUserUid(final UID userUid) {
		this.userUid = userUid;
	}

	public boolean isSuperUser() {
		return superUser;
	}

	public void setSuperUser(final boolean superUser) {
		this.superUser = superUser;
	}

	public boolean isWithCommunicationAccount() {
		return withCommunicationAccount;
	}

	public void setWithCommunicationAccount(final boolean withCommunicationAccount) {
		this.withCommunicationAccount = withCommunicationAccount;
	}

	public Set<String> getAllowedActions() {
		return allowedActions;
	}

	public void setAllowedActions(final Set<String> allowedActions) {
		this.allowedActions = allowedActions;
	}

	public Set<UID> getAllowedCustomActions() {
		return allowedCustomActions;
	}

	public void setAllowedCustomActions(final Set<UID> allowedCustomActions) {
		this.allowedCustomActions = allowedCustomActions;
	}

	public ModulePermissions getModulePermissions() {
		return modulePermissions;
	}

	public void setModulePermissions(final ModulePermissions modulePermissions) {
		this.modulePermissions = modulePermissions;
	}

	public MasterDataPermissions getMasterDataPermissions() {
		return masterDataPermissions;
	}

	public void setMasterDataPermissions(final MasterDataPermissions masterDataPermissions) {
		this.masterDataPermissions = masterDataPermissions;
	}

	public Set<UID> getDynamicTasklists() {
		return dynamicTasklists;
	}

	public void setDynamicTasklists(final Set<UID> dynamicTasklists) {
		this.dynamicTasklists = dynamicTasklists;
	}

	public Map<UID, String> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(final Map<UID, String> userRoles) {
		this.userRoles = userRoles;
	}

	public Map<UID, List<MandatorVO>> getMandatoryByLevel() {
		return mandatoryByLevel;
	}

	public void setMandatoryByLevel(final Map<UID, List<MandatorVO>> mandatoryByLevel) {
		this.mandatoryByLevel = mandatoryByLevel;
	}
}
