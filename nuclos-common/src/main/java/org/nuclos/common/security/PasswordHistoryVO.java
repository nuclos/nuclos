package org.nuclos.common.security;

public class PasswordHistoryVO {

	private final String encryptedPassword;
	private final String salt;
	private final Integer hashAlgorithm;

	public PasswordHistoryVO(final String encryptedPassword, final String salt, final Integer hashAlgorithm) {
		this.encryptedPassword = encryptedPassword;
		this.salt = salt;
		this.hashAlgorithm = hashAlgorithm;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public String getSalt() {
		return salt;
	}

	public Integer getHashAlgorithm() {
		return hashAlgorithm;
	}
}
