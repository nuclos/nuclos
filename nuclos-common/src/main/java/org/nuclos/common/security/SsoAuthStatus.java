package org.nuclos.common.security;

public enum SsoAuthStatus {
	INITIATED, LOGGED_IN, UNKNOWN
}
