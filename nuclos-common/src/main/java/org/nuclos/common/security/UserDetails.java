package org.nuclos.common.security;

import org.nuclos.common.UID;

public interface UserDetails {
	UID getUserUID();
	String getEmail();
	String getUsername();
}
