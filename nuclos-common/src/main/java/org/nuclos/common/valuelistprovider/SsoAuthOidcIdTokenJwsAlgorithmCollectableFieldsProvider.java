//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.valuelistprovider;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common2.exception.CommonBusinessException;

public class SsoAuthOidcIdTokenJwsAlgorithmCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(SsoAuthOidcIdTokenJwsAlgorithmCollectableFieldsProvider.class);

	public SsoAuthOidcIdTokenJwsAlgorithmCollectableFieldsProvider() {
	}

	@Override
	public void setParameter(String parameter, Object oValue) {
		LOG.info("Unknown parameter " + parameter + " with value " + oValue);
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> result = new ArrayList<>();
		// Values from com.nimbusds.jose.JWSAlgorithm
		result.add(new CollectableValueField("HS256"));
		result.add(new CollectableValueField("HS384"));
		result.add(new CollectableValueField("HS512"));
		result.add(new CollectableValueField("RS256"));
		result.add(new CollectableValueField("RS384"));
		result.add(new CollectableValueField("RS512"));
		result.add(new CollectableValueField("ES256"));
		result.add(new CollectableValueField("ES256K"));
		result.add(new CollectableValueField("ES384"));
		result.add(new CollectableValueField("ES512"));
		result.add(new CollectableValueField("PS256"));
		result.add(new CollectableValueField("PS384"));
		result.add(new CollectableValueField("PS512"));
		result.add(new CollectableValueField("EdDSA"));
		return result;
	}

}
