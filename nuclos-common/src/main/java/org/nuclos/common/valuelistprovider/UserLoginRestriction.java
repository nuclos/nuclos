package org.nuclos.common.valuelistprovider;

import org.nuclos.common2.KeyEnum;

public enum UserLoginRestriction implements KeyEnum<String> {
    SSO("SSO"),
    PASSWORD("PASSWORD");

    private String value;

    UserLoginRestriction(final String value) {
        this.value = value;
    }

    @Override
    public String getValue() { return this.value; }

    @Override
    public String toString() {
        return value;
    }
}
