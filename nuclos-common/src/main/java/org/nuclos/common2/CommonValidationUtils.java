//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.PointerCollection;
import org.nuclos.common.UID;
import org.nuclos.common.validation.FieldValidationError;
import org.nuclos.common2.exception.CommonValidationException;

public class CommonValidationUtils {

	/**
	 *
	 * @param localeDelegate, SpringLocaleDelegate
	 * @param metaProvider, MetaDataProvider
	 * @param mainEntity, String, can be null
	 * @param pointerCollection, PointerCollection, can be null
	 * @return String: Full ErrorMessage in HTML
	 */
	private static List<String> getFullMessages(CommonValidationException cve, SpringLocaleDelegate localeDelegate, IRigidMetaProvider metaProvider, UID mainEntity, PointerCollection pointerCollection) {
		List<String> messages = new ArrayList<String>();
		messages.add(localeDelegate.getText("common.exception.novabitvalidationexception"));

		if (cve.getErrors() != null) {
			for (String error : cve.getErrors()) {
				messages.add(localeDelegate.getMessageFromResource(error));
			}
		}

		if (cve.getFieldErrors() != null) {
			for (FieldValidationError error : cve.getFieldErrors()) {
				if (!error.getEntity().equals(mainEntity)) {
					messages.add(localeDelegate.getMessage("EntityCollectController.Subform", "Subform \"{0}\": ",
							localeDelegate.getResource(metaProvider.getEntity(error.getEntity()).getLocaleResourceIdForLabel(),
									localeDelegate.getLabelFromMetaDataVO(metaProvider.getEntity(error.getEntity()))))
							+ localeDelegate.getMessageFromResource(error.getMessage()));
				}
				else {
					if (pointerCollection != null) pointerCollection.addEmptyFieldPointer(error.getField());
					messages.add(localeDelegate.getMessageFromResource(error.getMessage()));
				}
			}
		}

		if (messages.size() == 1) {
			messages.add(localeDelegate.getMessageFromResource(cve.getMessage()));
		}

		return messages;
	}


	public static String getFullMessage(CommonValidationException cve, SpringLocaleDelegate localeDelegate, IRigidMetaProvider metaProvider, UID mainEntity, PointerCollection pointerCollection) {
		List<String> messages = getFullMessages(cve, localeDelegate, metaProvider, mainEntity, pointerCollection);
		String message = StringUtils.concatHtml(true, messages.toArray(new String[messages.size()]));
		if (pointerCollection != null) pointerCollection.setMainPointer(message);
		return message;
	}

	public static void setFullMessage(CommonValidationException cve, SpringLocaleDelegate localeDelegate, IRigidMetaProvider metaProvider, UID mainEntity) {
		List<String> messages = getFullMessages(cve, localeDelegate, metaProvider, mainEntity, null);
		cve.setFullErrorMessage(StringUtils.concatHtml(true, messages.toArray(new String[messages.size()])));
		cve.setFullLogMessage(messages.toString());
	}

}
