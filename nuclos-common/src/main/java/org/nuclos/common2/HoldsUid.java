package org.nuclos.common2;

import org.nuclos.common.UID;

public interface HoldsUid {
	
	UID getUid();
	
	void setUid(UID uid);
	
}
