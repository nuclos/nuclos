package org.nuclos.common2;


/**
 * Multi-language support for parameterized messages.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.4.1
 */
public interface ILocalizable extends Localizable {
	
	/**
	 * Parameters to the real message.
	 * 
	 * @see java.text.MessageFormat
	 */
	Object[] getParams();

}
