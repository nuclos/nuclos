//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import org.apache.log4j.Logger;
import org.nuclos.common.NuclosFatalException;

public class SystemUtils {

	private static final Logger LOG = Logger.getLogger(SystemUtils.class);

	public static enum OS {
		WINDOWS, LINUX, MAC, OTHER;
	}
	
	private SystemUtils() {
		// Never invoked.
	}

	public static OS getOsName() {
		final OS os;
		final String name = System.getProperty("os.name").toLowerCase();
		if (name.indexOf("windows") > -1) {
			os = OS.WINDOWS;
		} else if (name.indexOf("linux") > -1) {
			os = OS.LINUX;
		} else if (name.indexOf("mac") > -1) {
			os = OS.MAC;
		} else {
			os = OS.OTHER;
		}
		return os;
	}

	public static String getPrefix(String filename) {
		if (filename == null) {
			return null;
		}
		int n = filename.lastIndexOf('.');
		if (n > -1) {
			return filename.substring(0, n);
		}
		return filename;
	}
	
	public static String getSuffix(String filename) {
		if (filename == null) {
			return null;
		}
		int n = filename.lastIndexOf('.');
		if (n > -1) {
			return filename.substring(n + 1);
		}
		return "";
	}

}
