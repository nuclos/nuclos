package org.nuclos.common2;

import java.util.Collection;

import org.nuclos.remoting.TypePreservingCollectionDeserializer;
import org.nuclos.remoting.TypePreservingCollectionSerializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class TruncatableCollectionTransport {

	@JsonSerialize(using = TypePreservingCollectionSerializer.class)
	@JsonDeserialize(using = TypePreservingCollectionDeserializer.class)
	private final Collection<?> theCollection;
	private final boolean truncated;
	private final int totalSize;

	/**
	 * for deserialization only
	 */
	protected TruncatableCollectionTransport() {
		this.theCollection = null;
		this.truncated = false;
		this.totalSize = 0;
	}

	public TruncatableCollectionTransport(final Collection<?> theCollection, final boolean bTruncated, final int iTotalSize) {
		this.theCollection = theCollection;
		this.truncated = bTruncated;
		this.totalSize = iTotalSize;
	}

	public Collection<?> getTheCollection() {
		return theCollection;
	}

	public boolean isTruncated() {
		return truncated;
	}

	public int getTotalSize() {
		return totalSize;
	}
}
