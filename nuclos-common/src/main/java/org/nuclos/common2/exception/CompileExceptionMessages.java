//Copyright (C) 2023 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2.exception;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.remoting.TypePreservingMapDeserializer;
import org.nuclos.remoting.TypePreservingMapSerializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class CompileExceptionMessages {

	@JsonSerialize(using = TypePreservingMapSerializer.class)
	@JsonDeserialize(using = TypePreservingMapDeserializer.class)
	private final Map<String, NuclosCompileException.ErrorMessage> messages = new HashMap<String, NuclosCompileException.ErrorMessage>();

	public CompileExceptionMessages() {
		super();
	}

	public void setMessages(Map<String, NuclosCompileException.ErrorMessage> messages) {
		synchronized (this.messages) {
			this.messages.clear();
			this.messages.putAll(messages);
		}
	}

	public Map<String, NuclosCompileException.ErrorMessage> getMessages() {
		synchronized (this.messages) {
			return Collections.unmodifiableMap(this.messages);
		}
	}

	public static CompileExceptionMessages newMessages(Map<String, NuclosCompileException.ErrorMessage> messages) {
		CompileExceptionMessages result = new CompileExceptionMessages();
		result.setMessages(messages);
		return result;
	}

}
