//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2.exception;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.nuclos.common2.StringUtils;

public class InternalErrorException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = -3730260899750858111L;

	private String sIdentifier;

	private String sMessage;

	/**
	 * for deserialization only
	 */
	protected InternalErrorException() {
	}

	public InternalErrorException(int hashCode, String sMessage) {
		super(null, null, false, false);
		initIdentifier(String.format("#%d%s %s",
				hashCode < 0 ? 0 : 1,
				Integer.toHexString(Math.abs(hashCode)).toUpperCase(),
				createTimestamp()));
		this.sMessage = sMessage;
		if (StringUtils.looksEmpty(this.sMessage)) {
			this.sMessage = "An internal server error occurred. Please try again later or contact the administrator.";
		}
	}

	@Override
	public String getMessage() {
		return String.format("%s (%s)", sMessage, sIdentifier);
	}

	@Override
	public String toString() {
		return "InternalError '" + sIdentifier + "'";
	}

	private void initIdentifier(String sIdentifier) {
		this.sIdentifier = sIdentifier;
	}

	private static String createTimestamp() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

	public String getIdentifier() {
		return sIdentifier;
	}
}
