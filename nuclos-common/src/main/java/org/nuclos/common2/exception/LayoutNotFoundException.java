package org.nuclos.common2.exception;

/**
 * Created by Sebastian Debring on 3/25/2019.
 */
public class LayoutNotFoundException extends CommonBusinessException {

	/**
	 * for deserialization only
	 */
	protected LayoutNotFoundException() {
		super();
	}

	public LayoutNotFoundException(final String sMessage, final CommonFinderException ex) {
		super(sMessage, ex);
	}
}
