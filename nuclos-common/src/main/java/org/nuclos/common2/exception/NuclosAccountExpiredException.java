package org.nuclos.common2.exception;

import org.springframework.security.authentication.AccountExpiredException;

/**
 * Created by Oliver Brausch on 02.05.19.
 */
public class NuclosAccountExpiredException extends AccountExpiredException {

	/**
	 * for deserialization only
	 */
	protected NuclosAccountExpiredException() {
		super("");
	}

	public NuclosAccountExpiredException(String msg) {
		super(msg);
		NuclosExceptions.shortenStackTrace(this, 1);
	}
}
