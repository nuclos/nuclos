package org.nuclos.common2.exception;

import javax.ws.rs.core.Response;

import org.springframework.security.core.AuthenticationException;

public class NuclosPasswordBasedLoginForbiddenException extends AuthenticationException {
	public NuclosPasswordBasedLoginForbiddenException() {
		super(Response.Status.FORBIDDEN.getReasonPhrase());
		NuclosExceptions.shortenStackTrace(this, 1);
	}
}
