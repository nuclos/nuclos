package org.nuclos.common2.hyperlink;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nuclos.common2.StringUtils;

/**
 * This class represents an HTML anchor tag.
 * It manages the title and the destination of a hyperlink.
 * Other attributes are currently being ignored by it.
 */
public class AnchorTagHyperlink {

	public final String title;
	public final String destination;

	public AnchorTagHyperlink(String tag) {
		if (StringUtils.isNullOrEmpty(tag))
			throw new IllegalArgumentException("String is not a valid <a> tag.");
		Pattern pattern = Pattern.compile(HyperlinkUtils.ANCHOR_TAG_REGEX);
		Matcher matcher = pattern.matcher(tag);
		if (!matcher.matches())
			throw new IllegalArgumentException("String is not a valid <a> tag.");
		this.destination = matcher.group(1);
		this.title = matcher.group(2);
	}

}

