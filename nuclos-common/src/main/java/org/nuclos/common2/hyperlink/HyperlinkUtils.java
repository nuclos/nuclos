package org.nuclos.common2.hyperlink;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nuclos.common2.LangUtils;

public class HyperlinkUtils {

	/**
	 * RegEx representation of an HTML anchor tag.
	 */
	public static final String ANCHOR_TAG_REGEX = "[ ]*<a.*href=\"(.*)\"[^>]*>(.*)</a>[ ]*";

	/**
	 * Checks if the string is a valid HTML "<a>" tag.
	 * The tag is only valid, if the hyperlink provided in it, is also valid.
	 * @param tag
	 * @return
	 */
	public static boolean isValidATag(String tag) {
		if (org.nuclos.common2.StringUtils.isNullOrEmpty(tag))
			return false;
		Pattern pattern = Pattern.compile(ANCHOR_TAG_REGEX);
		Matcher matcher = pattern.matcher(tag);
		if (!matcher.matches())
			return false;
		return LangUtils.isValidURI(matcher.group(1));
	}

	/**
	 * Extracts and returns the title of a html a-tag.
	 * @param aTag
	 * @throws IllegalArgumentException - If the given string is not a valid a tag.
	 * @return
	 */
	public static String aTagGetTitle(String aTag) {
		if (org.nuclos.common2.StringUtils.isNullOrEmpty(aTag))
			return null;
		Pattern pattern = Pattern.compile(ANCHOR_TAG_REGEX);
		Matcher matcher = pattern.matcher(aTag);
		if (!matcher.matches())
			throw new IllegalArgumentException("\"" + aTag + "\"" + " is not an valid <a> - tag.");
		return matcher.group(2);
	}

	/**
	 * Extracts and returns the destination of a html a-tag.
	 * The destination of a a-tag is defined by its "href"-attribute.
	 * @param aTag
	 * @throws IllegalArgumentException - If the given string is not a valid tag.
	 * @return
	 */
	public static String aTagGetLink(String aTag) {
		if (org.nuclos.common2.StringUtils.isNullOrEmpty(aTag))
			return null;
		Pattern pattern = Pattern.compile(ANCHOR_TAG_REGEX);
		Matcher matcher = pattern.matcher(aTag);
		if (!matcher.matches())
			throw new IllegalArgumentException("\"" + aTag + "\"" + " is not an valid <a> - tag.");
		return matcher.group(1);
	}

}
