package org.nuclos.common2.interval;

import java.util.Calendar;

import org.nuclos.common2.DateUtils;
import org.nuclos.common2.KeyEnum;

public enum GranularityType implements KeyEnum<String> {
	
	YEAR("year", -2, Calendar.YEAR),
	QUARTER("quarter", -1, DateUtils.QUARTER),
	MONTH("month", 0, Calendar.MONTH),
	WEEK("week", 1, Calendar.WEEK_OF_YEAR),
	@Deprecated
	TIME("time", 4, -1),
	DAY("day", 3, Calendar.DAY_OF_WEEK),
	WEEKDAY("weekday", 2, Calendar.DAY_OF_WEEK);

	private final String name;
	private final int level;
	private final int calendarQuantizer;

	private GranularityType(String name, int level, int calendarQuantizer) {
		this.name = name;
		this.level = level;
		this.calendarQuantizer = calendarQuantizer;
	}

	@Override
	public String getValue() {
		return name;
	}
	
	public int getLevel() {
		return level;
	}
	
	public int getCalendarQuantizer() {
		return calendarQuantizer;
	}
	
	public static GranularityType getGranularityForLevel(int level) {
		for (GranularityType result: GranularityType.class.getEnumConstants()) {
			if (result.getLevel() == level) {
				return result;
			}
		}
		return null;
	}
}


