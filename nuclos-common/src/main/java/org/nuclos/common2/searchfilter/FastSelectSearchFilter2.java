package org.nuclos.common2.searchfilter;

import java.util.List;

import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.springframework.core.io.Resource;

public class FastSelectSearchFilter2 extends EntitySearchFilter2 {

	private Resource icon;
	
	public FastSelectSearchFilter2(SearchFilterVO searchFilterVO, CollectableSearchExpression searchExpr, 
			List<? extends CollectableEntityField> visibleFields) {
		
		super(searchFilterVO, searchExpr, visibleFields);
	}
	
	public Resource getIcon() {
		return icon;
	}
	
	public void setIcon(Resource icon) {
		this.icon = icon;
	}
	
}
