//Copyright (C) 2022  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import static org.nuclos.remoting.JsonObjectSerializer.VALUE;

import java.io.IOException;

import javax.json.JsonObject;

import org.nuclos.common.JsonUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class JsonObjectDeserializer extends JsonDeserializer<JsonObject> {
	@Override
	public JsonObject deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
		final ObjectCodec codec = jpar.getCodec();
		final TreeNode objectNode = codec.readTree(jpar);
		return JsonUtils.stringToObject(codec.treeToValue(objectNode.get(VALUE), String.class));
	}
}
