//Copyright (C) 2022  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;

import javax.json.JsonObject;

import org.nuclos.common.JsonUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

public class JsonObjectSerializer extends JsonSerializer<JsonObject> {

	static final String TYPE = "@type";

	static final String VALUE = "value";

	private final static TypePreservingTypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	@Override
	public void serialize(final JsonObject value, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
		jgen.writeStartObject();
		jgen.writeStringField(VALUE, JsonUtils.objectToString(value));
		jgen.writeEndObject();
	}

	@Override
	public void serializeWithType(final JsonObject value, final JsonGenerator jgen, final SerializerProvider prov, final TypeSerializer typeSer) throws IOException {
		final String valueType = typeIdResolver.idFromValue(value);
		jgen.writeStartObject();
		jgen.writeFieldName(TYPE);
		jgen.writeString(valueType);
		jgen.writeStringField(VALUE, JsonUtils.objectToString(value));
		jgen.writeEndObject();
	}
}
