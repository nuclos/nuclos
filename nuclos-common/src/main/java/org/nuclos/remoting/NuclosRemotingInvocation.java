//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import org.nuclos.common.NuclosConstants;
import org.springframework.remoting.support.RemoteInvocation;
import org.springframework.util.ClassUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * This class converts Spring's RemoteInvocation into Nuclos' own format.
 *
 * @see NuclosRemotingInterface
 */
public class NuclosRemotingInvocation {

	protected String methodName;

	protected String[] argumentTypes;

	protected String[] arguments;

	protected String userTimeZone;

	protected String mandator;

	protected String dataLocale;

	protected Boolean inputContextSupported;

	@JsonSerialize(using = TypePreservingMapSerializer.class)
	@JsonDeserialize(using = TypePreservingMapDeserializer.class)
	protected Map<String, Serializable> inputContext;

	protected Integer messageReceiverContext;

	protected StackTraceElement[] stackTraceElements;

	private static final ObjectMapper jsonMapper = NuclosRemotingMapperFactory.createObjectMapper();

	/**
	 * @return the complete deserialized data.
	 * (this method deserializes the argument values)
	 * @throws ClassNotFoundException
	 * @throws JsonProcessingException
	 */
	public RemoteInvocation getRemoteInvocation() throws ClassNotFoundException, JsonProcessingException {
		RemoteInvocationWithArgumentQuarantine result = new RemoteInvocationWithArgumentQuarantine(this.arguments);
		result.setMethodName(methodName);
		Class<?>[] parameterTypes = new Class<?>[this.argumentTypes.length];
		for (int i = 0; i < this.argumentTypes.length; i++) {
			final Class<?> paramClass = ClassUtils.forName(
					this.argumentTypes[i],
					Thread.currentThread().getContextClassLoader()
			);
			parameterTypes[i] = paramClass;
		}
		result.setParameterTypes(parameterTypes);
		result.addAttribute(NuclosConstants.USER_TIMEZONE, userTimeZone);
		result.addAttribute(NuclosConstants.MANDATOR, mandator);
		result.addAttribute(NuclosConstants.DATA_LOCALE, dataLocale);
		result.addAttribute(NuclosConstants.INPUT_CONTEXT_SUPPORTED, inputContextSupported);
		result.addAttribute(NuclosConstants.INPUT_CONTEXT, (Serializable) inputContext);
		result.addAttribute(NuclosConstants.MESSAGE_RECEIVER_CONTEXT, messageReceiverContext);
		result.addAttribute(NuclosConstants.CLIENT_STACK_TRACE, stackTraceElements);
		return result;
	}

	/**
	 * Converts the invocation data into a more Jackson compatible object,
	 * already serializes the argument values.
	 *
	 * @param invocation
	 * @return
	 * @throws JsonProcessingException
	 */
	public NuclosRemotingInvocation set(final RemoteInvocation invocation) throws JsonProcessingException {
		this.methodName = invocation.getMethodName();
		this.argumentTypes = Arrays.stream(invocation.getParameterTypes()).map(cls -> cls.getCanonicalName()).collect(Collectors.toList()).toArray(new String[]{});
		this.arguments = new String[invocation.getArguments().length];
		for (int i = 0; i < argumentTypes.length; i++) {
			if (invocation.getArguments()[i] != null) {
				this.arguments[i] = jsonMapper.writeValueAsString(invocation.getArguments()[i]);
			}
		}
		this.userTimeZone = (String) invocation.getAttribute(NuclosConstants.USER_TIMEZONE);
		this.mandator = (String) invocation.getAttribute(NuclosConstants.MANDATOR);
		this.dataLocale = (String) invocation.getAttribute(NuclosConstants.DATA_LOCALE);
		this.inputContextSupported = (Boolean) invocation.getAttribute(NuclosConstants.INPUT_CONTEXT_SUPPORTED);
		this.inputContext = (Map<String, Serializable>) invocation.getAttribute(NuclosConstants.INPUT_CONTEXT);
		this.messageReceiverContext = (Integer) invocation.getAttribute(NuclosConstants.MESSAGE_RECEIVER_CONTEXT);
		this.stackTraceElements = (StackTraceElement[]) invocation.getAttribute(NuclosConstants.CLIENT_STACK_TRACE);
		return this;
	}

	private static class RemoteInvocationWithArgumentQuarantine extends RemoteInvocation {

		private final String[] rawArguments;

		private RemoteInvocationWithArgumentQuarantine(final String[] rawArguments) {
			this.rawArguments = rawArguments;
		}

		@Override
		public Object invoke(final Object targetObject) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
			// The method is identified first, then the arguments are deserialized.
			// With this we prevent unexpected potentially dangerous objects from being deserialized.
			Method method = targetObject.getClass().getMethod(getMethodName(), getParameterTypes());
			return method.invoke(targetObject, getArguments());
		}

		@Override
		public Object[] getArguments() {
			final Class<?>[] parameterTypes = getParameterTypes();
			Object[] parameterValues = new Object[parameterTypes.length];
			for (int i = 0; i < parameterTypes.length; i++) {
				final String paramValue = rawArguments[i];
				if (paramValue != null) {
					try {
						parameterValues[i] = jsonMapper.readValue(paramValue, parameterTypes[i]);
					} catch (JsonProcessingException e) {
						throw new RuntimeException(String.format("Deserialization of type %s (value=%s) not possible: %s", parameterTypes[i].getCanonicalName(), paramValue, e.getMessage()), e);
					}
				}
			}
			return parameterValues;
		}
	}
}
