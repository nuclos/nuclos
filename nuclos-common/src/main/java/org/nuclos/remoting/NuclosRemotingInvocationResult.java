//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Collection;
import java.util.Map;

import org.springframework.remoting.support.RemoteInvocationResult;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

/**
 * This class converts Spring's RemoteInvocationResult into Nuclos' own format.
 *
 * @see NuclosRemotingInterface
 */
public class NuclosRemotingInvocationResult {

	protected String valueType;

	protected String value;

	protected Throwable exception;

	private static NuclosRemotingMapperFactory.RemotingJsonMapper jsonMapper = NuclosRemotingMapperFactory.createObjectMapper();

	private static final TypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	/**
	 * Converts the invocation result into a more Jackson compatible object,
	 * already serializes the result value itself.
	 *
	 * @param result
	 * @return
	 * @throws JsonProcessingException
	 */
	public NuclosRemotingInvocationResult set(final RemoteInvocationResult result) throws JsonProcessingException {
		if (result.getValue() != null) {
			valueType = typeIdResolver.idFromValue(result.getValue());
			value = jsonMapper.writeValueAsString(result.getValue());
		}
		if (result.getException() != null) {
			exception = result.getException();
		}
		return this;
	}

	/**
	 * @param returnType
	 * @param genericReturnType
	 * @return the complete deserialized result.
	 * (this method deserializes the result value itself)
	 * @throws JsonProcessingException
	 */
	public RemoteInvocationResult getRemoteInvocationResult(final Class<?> returnType, final Type genericReturnType) throws JsonProcessingException {
		RemoteInvocationResult result = new RemoteInvocationResult();
		if (value != null) {
			if (returnType.isArray()) {
				result.setValue(jsonMapper.readValue(value, jsonMapper.getTypeFactory().constructArrayType(returnType.getComponentType())));
			} else if (Collection.class.isAssignableFrom(returnType)) {
				ParameterizedType parameterizedType = (ParameterizedType) genericReturnType;
				if (parameterizedType.getActualTypeArguments().length > 0) {
					Class<?> colTypeClass;
					if (parameterizedType.getActualTypeArguments()[0] instanceof Class) {
						colTypeClass = (Class<?>) parameterizedType.getActualTypeArguments()[0];
					} else if (parameterizedType.getActualTypeArguments()[0] instanceof ParameterizedType) {
						colTypeClass = (Class<?>) ((ParameterizedType) parameterizedType.getActualTypeArguments()[0]).getRawType();
					} else if (parameterizedType.getActualTypeArguments()[0] instanceof WildcardType) { // e.g. java.util.Collection<? extends org.springframework.security.core.GrantedAuthority>
						colTypeClass = (Class<?>) ((WildcardType) parameterizedType.getActualTypeArguments()[0]).getUpperBounds()[0];
					} else {
						throw new JsonProcessingException(String.format("%s: unknown type hierarchy", returnType)) {
							private static final long serialVersionUID = -637863346770440420L;
						};
					}

					result.setValue(jsonMapper.readValue(value, jsonMapper.getTypeFactory().constructCollectionType(
							(Class<? extends Collection>) returnType,
							colTypeClass)));
				} else {
					throw new JsonProcessingException(String.format("%s: missing generic to deserialize", returnType)) {
						private static final long serialVersionUID = -637863346770440420L;
					};
				}
			} else if (Map.class.isAssignableFrom(returnType)) {
				ParameterizedType parameterizedType = (ParameterizedType) genericReturnType;
				Class<?> keyTypeClass = null;
				Class<?> valueTypeClass = null;
				if (parameterizedType.getActualTypeArguments().length > 1) {
					if (parameterizedType.getActualTypeArguments()[0] instanceof Class) {
						keyTypeClass = (Class<?>) parameterizedType.getActualTypeArguments()[0];
					} else if (parameterizedType.getActualTypeArguments()[0] instanceof ParameterizedType &&
							((ParameterizedType) parameterizedType.getActualTypeArguments()[0]).getRawType() instanceof Class) {
						keyTypeClass = (Class<?>) ((ParameterizedType) parameterizedType.getActualTypeArguments()[0]).getRawType();
					}
					if (parameterizedType.getActualTypeArguments()[1] instanceof Class) {
						valueTypeClass = (Class<?>) parameterizedType.getActualTypeArguments()[1];
					} else if (parameterizedType.getActualTypeArguments()[1] instanceof ParameterizedType &&
							((ParameterizedType) parameterizedType.getActualTypeArguments()[1]).getRawType() instanceof Class) {
						valueTypeClass = (Class<?>) ((ParameterizedType) parameterizedType.getActualTypeArguments()[1]).getRawType();
					}
				}
				if (keyTypeClass != null && valueTypeClass != null) {
					result.setValue(jsonMapper.readValue(value, jsonMapper.getTypeFactory().constructMapType(
							(Class<? extends Map>) returnType,
							keyTypeClass,
							valueTypeClass)));
				} else {
					throw new JsonProcessingException(String.format("%s: missing generic to deserialize", returnType)) {
						private static final long serialVersionUID = -637863346770440420L;
					};
				}
			} else {
				Class<?> resultType = returnType;
				if (valueType != null) {
					try {
						resultType = typeIdResolver.typeFromId(jsonMapper.createDeserializationContext(), valueType).getRawClass();
					} catch (IOException e) {
						// just a try, we also have the method return type
					}
				}
				result.setValue(jsonMapper.readValue(value, resultType));
			}
		}
		if (exception != null) {
			result.setException(exception);
		}
		return result;
	}
}
