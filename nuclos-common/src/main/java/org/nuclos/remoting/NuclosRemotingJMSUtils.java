//Copyright (C) 2022  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class NuclosRemotingJMSUtils {

	private static final String JSON_IDENTIFICATION_MARK = String.format("\"%s\":", TypePreservingObjectWrapperSerializer.TYPE);

	private static ObjectMapper jsonMapper = NuclosRemotingMapperFactory.createObjectMapper();


	/**
	 * Delivers only objects that were previously sent as objects (JSON).
	 * Pure text messages (sendTextMessage) are ignored and return null.
	 * @param msg
	 * @return
	 * @throws JMSException
	 */
	public static Object getObjectFromMessage(Message msg) throws JMSException {
		return getObjectFromMessage(msg, Object.class);
	}

	/**
	 * Delivers only objects that were previously sent as objects (JSON).
	 * Pure text messages (sendTextMessage) are ignored and return null.
	 * @param msg
	 * @param cls
	 * @return
	 * @param <T>
	 * @throws JMSException
	 */
	public static <T> T getObjectFromMessage(Message msg,
											 Class<T> cls) throws JMSException {
		if (msg == null) {
			return null;
		}
		if (msg instanceof TextMessage) {
			try {
				String s = ((TextMessage) msg).getText();
				if (s != null && s.contains(JSON_IDENTIFICATION_MARK)) { // <- just to be on the safe side, because some topics send text as well as objects
					final TypePreservingObjectWrapper wrapper = jsonMapper.readValue(s, TypePreservingObjectWrapper.class);
					return wrapper.get(cls);
				} else {
					return null;
				}
			} catch (JsonProcessingException e) {
				JMSException jmsEx = new JMSException(String.format("Text message could not be deserialized: %s", e.getMessage()));
				jmsEx.setLinkedException(e);
				throw jmsEx;
			}
		} else {
			throw new JMSException(String.format("JMS message type %s not supported, only text messages are allowed!", msg.getClass().getCanonicalName()));
		}
	}

}
