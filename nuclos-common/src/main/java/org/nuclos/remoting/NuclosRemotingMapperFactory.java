//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Vector;

import javax.json.JsonObject;

import org.apache.commons.lang.ClassUtils;
import org.nuclos.api.Settings;
import org.nuclos.api.SettingsImpl;
import org.nuclos.api.context.InputDelegateSpecification;
import org.nuclos.common.CollectableEntityFieldWithEntity;
import org.nuclos.common.CollectableEntityFieldWithEntityForExternal;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LocaleInfo;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.StreamReadConstraints;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.cfg.CacheProvider;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.deser.DefaultDeserializationContext;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

public interface NuclosRemotingMapperFactory {

	public static final String MODULE_NAME = "nuclosRemoting";

	/**
	 * @see NuclosRemotingInterface
	 * @return a mapper preconfigured for the new Nuclos remoting.
	 */
	static RemotingJsonMapper createObjectMapper() {
		RemotingJsonMapper jsonMapper = new RemotingJsonMapper();

		try {
			//NUCLOS-10716 (there's a new limit for the length of string values since Jackson 2.15)
			StreamReadConstraints streamReadConstraints = StreamReadConstraints
					.builder()
					.maxStringLength(Integer.MAX_VALUE)
					.build();
			jsonMapper.getFactory().setStreamReadConstraints(streamReadConstraints);
		} catch (NoSuchMethodError e) {
			// that's just a quick and dirty hack to mitigate the problem that Glassfish is using an older jackson version
		}

		jsonMapper.registerModule(new JaxbAnnotationModule());
		jsonMapper.configure(MapperFeature.PROPAGATE_TRANSIENT_MARKER, true);
		jsonMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		// by default only serialize fields, NO getters or other methods
		jsonMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
		jsonMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

		jsonMapper.addMixIn(TypePreservingObjectWrapper.class, TypePreservingObjectWrapperMixInClass.class);
		jsonMapper.addMixIn(Settings.class, SettingMixInClass.class);
		jsonMapper.addMixIn(SettingsImpl.class, SettingMixInClass.class);

		RemotingModule module = new RemotingModule();

		TypePreservingCollectionDeserializer collectionDeserializer = new TypePreservingCollectionDeserializer();
		ThrowableDeserializer throwableDeserializer = new ThrowableDeserializer();
		StringBufferDeserializer stringBufferDeserializer = new StringBufferDeserializer();
		// Our own Deserializers must have a higher prio than the internal ones.
		// Otherwise the internal one is used during recursively calls!
		module.setDeserializerModifier(new BeanDeserializerModifier() {
			@Override
			public JsonDeserializer<?> modifyDeserializer(final DeserializationConfig config, final BeanDescription beanDesc, final JsonDeserializer<?> deserializer) {
				if (Collection.class.isAssignableFrom(beanDesc.getBeanClass())) {
					return collectionDeserializer;
				} else if (Throwable.class.isAssignableFrom(beanDesc.getBeanClass())) {
					return throwableDeserializer;
				} else if (StringBuffer.class.isAssignableFrom(beanDesc.getBeanClass())) {
					return stringBufferDeserializer;
				}
				return super.modifyDeserializer(config, beanDesc, deserializer);
			}
		});

		module.addSerializer(Throwable.class, new ThrowableSerializer());
		module.addDeserializer(Throwable.class, throwableDeserializer);
		module.addDeserializer(StringBuffer.class, stringBufferDeserializer);

		module.addSerializer(org.nuclos.api.UID.class, new UID.UIDJsonSerializer(false));
		module.addDeserializer(org.nuclos.api.UID.class, new UID.UIDJsonDeserializer());
		module.addKeySerializer(org.nuclos.api.UID.class, new UID.UIDJsonSerializer(true));
		module.addKeyDeserializer(org.nuclos.api.UID.class, new UID.UIDKeyDeserializer());
		module.addSerializer(UID.class, new UID.UIDJsonSerializer(false));
		module.addDeserializer(UID.class, new UID.UIDImplJsonDeserializer());
		module.addKeySerializer(UID.class, new UID.UIDJsonSerializer(true));
		module.addKeyDeserializer(UID.class, new UID.UIDKeyDeserializer());

		module.addSerializer(TypePreservingObjectWrapper.class, new TypePreservingObjectWrapperSerializer(jsonMapper));
		module.addDeserializer(TypePreservingObjectWrapper.class, new TypePreservingObjectWrapperDeserializer());

		module.addSerializer(InputDelegateSpecification.InputDelegateDataMap.class, new TypePreservingMapSerializer());
		module.addDeserializer(InputDelegateSpecification.InputDelegateDataMap.class, RigidUtils.uncheckedCast(new TypePreservingMapDeserializer()));

		module.addSerializer(Collection.class, new TypePreservingCollectionSerializer());
		module.addDeserializer(Collection.class, collectionDeserializer);
		module.addSerializer(List.class, new TypePreservingCollectionSerializer());
		module.addDeserializer(List.class, RigidUtils.uncheckedCast(collectionDeserializer));
		module.addSerializer(Set.class, new TypePreservingCollectionSerializer());
		module.addDeserializer(Set.class, RigidUtils.uncheckedCast(collectionDeserializer));
		module.addSerializer(Vector.class, new TypePreservingCollectionSerializer());
		module.addDeserializer(Vector.class, RigidUtils.uncheckedCast(collectionDeserializer));
		module.addSerializer(ArrayList.class, new TypePreservingCollectionSerializer());
		module.addDeserializer(ArrayList.class, RigidUtils.uncheckedCast(collectionDeserializer));
		module.addSerializer(Object[].class, new TypePreservingObjectArraySerializer());
		module.addDeserializer(Object[].class, new TypePreservingObjectArrayDeserializer());

		module.addSerializer(JsonObject.class, new JsonObjectSerializer());
		module.addDeserializer(JsonObject.class, new JsonObjectDeserializer());

		module.addDeserializer(InternalTimestamp.class, new InternalTimestamp.InternalTimestampDeserializer());
		module.addKeyDeserializer(FieldMeta.class, new SystemFieldMetaKeyDeserializer());
		module.addKeySerializer(FieldMeta.class, new SystemFieldMetaKeySerializer());
		module.addKeySerializer(LocaleInfo.class, new LocaleInfo.LocaleInfoKeySerializer());
		module.addKeyDeserializer(LocaleInfo.class, new LocaleInfo.LocaleInfoKeyDeserializer());
		module.addSerializer(CollectableEntityFieldWithEntity.class, new CollectableEntityFieldWithEntity.CollectableEntityFieldWithEntitySerializer(CollectableEntityFieldWithEntity.class));
		module.addDeserializer(CollectableEntityFieldWithEntity.class, new CollectableEntityFieldWithEntity.CollectableEntityFieldWithEntityDeserializer(CollectableEntityFieldWithEntity.class));
		module.addSerializer(CollectableEntityFieldWithEntityForExternal.class, new CollectableEntityFieldWithEntity.CollectableEntityFieldWithEntitySerializer(CollectableEntityFieldWithEntityForExternal.class));
		module.addDeserializer(CollectableEntityFieldWithEntityForExternal.class, new CollectableEntityFieldWithEntity.CollectableEntityFieldWithEntityDeserializer(CollectableEntityFieldWithEntityForExternal.class));
		module.addKeySerializer(UsageCriteria.class, new UsageCriteria.UsageCriteriaKeySerializer());
		module.addKeyDeserializer(UsageCriteria.class, new UsageCriteria.UsageCriteriaKeyDeserializer());

		module.addSerializer(org.springframework.security.core.authority.SimpleGrantedAuthority.class, new SimpleGrantedAuthoritySerializer());
		module.addDeserializer(org.springframework.security.core.authority.SimpleGrantedAuthority.class, new SimpleGrantedAuthorityDeserializer());

		jsonMapper.registerModule(module);

		return jsonMapper;
	}

	public static class RemotingJsonMapper extends ObjectMapper {

		private static final long serialVersionUID = -2125115516535711446L;

		public RemotingJsonMapper() {
			super(null, new RemotingSerializerProvider(), null);
		}

		@Override
		public ObjectMapper registerModule(final Module module) {
			if (module instanceof RemotingModule) {
				TypePreservingTypeIdResolver.addMixIns(this, (RemotingModule) module);
			}
			return super.registerModule(module);
		}

		public DefaultDeserializationContext createDeserializationContext() {
			return super.createDeserializationContext(null, getDeserializationConfig());
		}
	}

	static class RemotingSerializerProvider extends DefaultSerializerProvider {

		private static final long serialVersionUID = -1717666207569568041L;

		private final JsonSerializer<org.nuclos.api.UID> defaultUidKeySerializer = new UID.UIDJsonSerializer(true);

		public RemotingSerializerProvider() {
			super();
		}

		public RemotingSerializerProvider(RemotingSerializerProvider src) {
			super(src);
		}

		protected RemotingSerializerProvider(SerializerProvider src, SerializationConfig config, SerializerFactory f) {
			super(src, config, f);
		}

		@Override
		public DefaultSerializerProvider copy() {
			if (!Objects.equals(getClass(), DefaultSerializerProvider.class)) {
				return super.copy();
			}
			return new RemotingSerializerProvider(this);
		}

		@Override
		public DefaultSerializerProvider withCaches(final CacheProvider cacheProvider) {
			return this;
		}

		@Override
		public DefaultSerializerProvider createInstance(final SerializationConfig config, final SerializerFactory jsf) {
			return new RemotingSerializerProvider(this, config, jsf);
		}

		@Override
		public JsonSerializer<Object> findNullKeySerializer(final JavaType serializationType, final BeanProperty property) throws JsonMappingException {
			if (ClassUtils.isAssignable(serializationType.getRawClass(), org.nuclos.api.UID.class)) {
				return RigidUtils.uncheckedCast(defaultUidKeySerializer);
			}
			return super.findNullKeySerializer(serializationType, property);
		}
	}

	static class RemotingModule extends SimpleModule {

		private static final long serialVersionUID = 1041371130015799956L;
		private final Set<Class<?>> classesWithOwnSerializer = new HashSet<>();

		RemotingModule() {
			super(MODULE_NAME);
		}

		public SimpleSerializers getSerializers() {
			return _serializers;
		}

		@Override
		public <T> SimpleModule addSerializer(final Class<? extends T> type, final JsonSerializer<T> ser) {
			synchronized (classesWithOwnSerializer) {
				classesWithOwnSerializer.add(type);
			}
			return super.addSerializer(type, ser);
		}

		public <T> boolean isClassWithOwnSerializer(final Class<? extends T> type) {
			boolean result = false;
			synchronized (classesWithOwnSerializer) {
				result = classesWithOwnSerializer.contains(type);
			}
			return result;
		}
	}

	@JsonSerialize(using = TypePreservingMapSerializer.class)
	@JsonDeserialize(using = TypePreservingMapDeserializer.class)
	public static class SettingMixInClass {
	}

	@JsonSerialize(using = TypePreservingObjectWrapperSerializer.class)
	@JsonDeserialize(using = TypePreservingObjectWrapperDeserializer.class)
	public static class TypePreservingObjectWrapperMixInClass {
	}

}
