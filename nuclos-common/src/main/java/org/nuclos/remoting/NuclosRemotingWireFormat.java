//Copyright (C) 2022  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.io.Reader;

import org.apache.activemq.command.DataStructure;
import org.apache.activemq.transport.util.TextWireFormat;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.NotImplementedException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class NuclosRemotingWireFormat extends TextWireFormat {

	private static final String NULL_OBJECT = "NuclosRemotingWireFormat[null]";

	private int version;

	private static final ObjectMapper jsonMapper = NuclosRemotingMapperFactory.createObjectMapper();

	@Override
	public String marshalText(final Object o) throws IOException {
		if (o == null) {
			return NULL_OBJECT;
		}
		if (o instanceof DataStructure) {
			return jsonMapper.writeValueAsString(o);
		} else {
			throw new NotImplementedException(String.format("Serialization of object not implemented: %s", o));
		}
	}

	@Override
	public Object unmarshalText(final String s) throws IOException {
		if (NULL_OBJECT.equals(s)) {
			return null;
		}
		return jsonMapper.readValue(s, DataStructure.class);
	}

	@Override
	public Object unmarshalText(final Reader reader) throws IOException {
		return unmarshalText(IOUtils.toString(reader));
	}

	@Override
	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public int getVersion() {
		return this.version;
	}

}
