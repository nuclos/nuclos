package org.nuclos.remoting;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class StringBufferDeserializer extends StdDeserializer<StringBuffer> {

	protected StringBufferDeserializer() {
		super(StringBuffer.class);
	}

	@Override
	public StringBuffer deserialize(final JsonParser jsonParser, final DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
		return new StringBuffer(jsonParser.getText());
	}
}
