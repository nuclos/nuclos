package org.nuclos.remoting;

import java.io.IOException;

import org.nuclos.common.E;
import org.nuclos.common.UID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;

public class SystemFieldMetaKeyDeserializer extends KeyDeserializer {
	@Override
	public Object deserializeKey(final String key, final DeserializationContext ctx) throws IOException, JsonProcessingException {
		final String[] entityAndFieldUID = key.split(";");
		if (entityAndFieldUID.length == 2) {
			return E.getByUID(new UID(entityAndFieldUID[0])).getField(new UID(entityAndFieldUID[1]));
		} else {
			throw new IllegalArgumentException("Wrong key serialization: " + key);
		}
	}
}
