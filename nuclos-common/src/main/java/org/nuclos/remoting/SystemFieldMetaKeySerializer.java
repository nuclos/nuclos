package org.nuclos.remoting;

import java.io.IOException;

import org.nuclos.common.FieldMeta;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class SystemFieldMetaKeySerializer extends StdSerializer<FieldMeta> {
	public SystemFieldMetaKeySerializer() {
		super(FieldMeta.class);
	}

	@Override
	public void serialize(final FieldMeta key, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
		jgen.writeFieldName(String.format("%s;%s", key.getEntity().getString(), key.getUID().getString()));
	}
}
