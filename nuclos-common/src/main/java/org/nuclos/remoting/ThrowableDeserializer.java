//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import static org.nuclos.remoting.ThrowableSerializer.BACKUP_EXCEPTION_TYPE;
import static org.nuclos.remoting.ThrowableSerializer.FIELD_TYPE;
import static org.nuclos.remoting.ThrowableSerializer.TYPE;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.lang.ClassUtils;
import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Specialized deserializer for the remoting interface throwables.
 *
 * @see NuclosRemotingInterface
 */
public class ThrowableDeserializer extends StdDeserializer<Throwable> {

	private final static TypePreservingTypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	protected ThrowableDeserializer() {
		super(Throwable.class);
	}

	@Override
	public Throwable deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
		try {
			final ObjectCodec codec = jpar.getCodec();
			final TreeNode tNode = codec.readTree(jpar);
			String sType = codec.treeToValue(tNode.get(TYPE), String.class);
			if (sType == null) {
				sType = codec.treeToValue(tNode.get(BACKUP_EXCEPTION_TYPE), String.class);
			}
			final Class<?> tClass = typeIdResolver.typeFromId(ctx, sType).getRawClass();
			final Constructor<?> ctor = tClass.getDeclaredConstructor();
			ReflectionUtils.makeAccessible(ctor);
			final Object result = ctor.newInstance();
			final Iterator<String> fieldIt = tNode.fieldNames();
			while (fieldIt.hasNext()) {
				final String nextField = fieldIt.next();
				if (nextField.startsWith(TYPE)) {
					continue;
				}
				// next field class
				final String sFieldClass = codec.treeToValue(tNode.get(FIELD_TYPE + nextField), String.class);
				if (sFieldClass != null) {
					final Class<?> fieldClass = typeIdResolver.typeFromId(ctx, sFieldClass).getRawClass();
					final Object value;
					if (ClassUtils.isAssignable(fieldClass, Collection.class)) {
						value = new TypePreservingCollectionDeserializer().deserialize(tNode.get(nextField).traverse(jpar.getCodec()), ctx);
					} else {
						value = codec.treeToValue(tNode.get(nextField), fieldClass);
					}
					// Do not use this deserializer again, uses the internal throwable one, which unfortunately has problems with the invocation exception:
					//final Object value = jsonMapper.readValue(tNode.get(nextField).toString(), fieldClass);
					final Field field = ReflectionUtils.findField(tClass, nextField);
					ReflectionUtils.makeAccessible(field);
					ReflectionUtils.setField(field, result, value);
				}
			}
			return (Throwable) result;
		} catch (NoSuchMethodException | InstantiationException | IllegalAccessException |
				InvocationTargetException ex) {
			return ex;
		}
	}

}
