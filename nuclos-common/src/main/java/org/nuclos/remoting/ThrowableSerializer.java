//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.nuclos.common.collection.Pair;
import org.nuclos.common2.exception.NuclosBadCredentialsException;
import org.nuclos.common2.exception.NuclosUsernameNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

/**
 * Specialized serializer for the remoting interface throwables.
 * @see NuclosRemotingInterface
 *
 * The standard Jackson implementation for throwables has problems with nested throwables of the RemoteInvocation,
 * so let's write our own.
 *
 * If the excepted default constructor of a throwable class is missing,
 * these class can be mapped to an others via ThrowableReplacements.
 */
public class ThrowableSerializer extends JsonSerializer<Throwable> {

	static final String TYPE = "@type";

	static final String BACKUP_EXCEPTION_TYPE = "@backupType";

	static final String FIELD_TYPE = TYPE + ":";

	private static final Map<String, Class<?>> THROWABLE_REPLACEMENTS = Arrays.stream(new Pair[] {
			Pair.makePair(BadCredentialsException.class, NuclosBadCredentialsException.class),
			Pair.makePair(UsernameNotFoundException.class, NuclosUsernameNotFoundException.class),
	}).collect(Collectors.toMap(pair -> ClassUtils.getQualifiedName((Class<?>) pair.getX()), pair -> (Class<?>) pair.getY()));

	static final Set<String> IGNORED_FIELDS = new HashSet<>();
	static {
		IGNORED_FIELDS.add("stackTrace");
		IGNORED_FIELDS.add("suppressedExceptions");
	}

	private final static TypePreservingTypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	@Override
	public void serializeWithType(final Throwable t, final JsonGenerator jgen, final SerializerProvider prov, final TypeSerializer typeSer) throws IOException {
		serialize(t, jgen, prov);
	}

	@Override
	public void serialize(final Throwable t, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
		final String exceptionType = typeIdResolver.idFromValueAndType(t, getThrowableToUse(t.getClass()));
		jgen.writeStartObject();
		jgen.writeFieldName(TYPE);
		jgen.writeString(exceptionType);
		// write twice, once for jackson once for us:
		jgen.writeFieldName(BACKUP_EXCEPTION_TYPE);
		jgen.writeString(exceptionType);
		final List<Field> allFields = new ArrayList<>();
		ReflectionUtils.doWithFields(t.getClass(), new ReflectionUtils.FieldCallback() {
			@Override
			public void doWith(final Field field) throws IllegalArgumentException, IllegalAccessException {
				ReflectionUtils.makeAccessible(field);
				allFields.add(field);
			}
		}, new ReflectionUtils.FieldFilter() {
			@Override
			public boolean matches(final Field field) {
				return !Modifier.isStatic(field.getModifiers()) &&
						!Modifier.isTransient(field.getModifiers()) &&
						!IGNORED_FIELDS.contains(field.getName());
			}
		});
		for (Field field : allFields) {
			final Object o = ReflectionUtils.getField(field, t);
			if (o != null
				&& o != t // Infinite loop prevention
			) {
				jgen.writeFieldName(FIELD_TYPE + field.getName());
				jgen.writeString(typeIdResolver.idFromValueAndType(o, getThrowableToUse(o.getClass())));
				jgen.writeFieldName(field.getName());
				jgen.writeObject(o);
			}
		}
		jgen.writeEndObject();
	}

	private static Class<?> getThrowableToUse(Class<?> tClass) {
		final Class<?> result = THROWABLE_REPLACEMENTS.get(ClassUtils.getQualifiedName(tClass));
		if (result != null) {
			return result;
		}
		return tClass;
	}

}
