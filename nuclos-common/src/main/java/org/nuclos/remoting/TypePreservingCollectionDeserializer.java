//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import static org.nuclos.remoting.TypePreservingCollectionSerializer.TYPE;
import static org.nuclos.remoting.TypePreservingCollectionSerializer.TYPE_INDEX_MAP;
import static org.nuclos.remoting.TypePreservingCollectionSerializer.VALUES;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;

import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

/**
 * @see TypePreservingCollectionSerializer
 */
public class TypePreservingCollectionDeserializer extends JsonDeserializer<Collection<?>> {

	private static final TypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	@Override
	public Collection<?> deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
		final Collection<Object> result;
		final ObjectCodec codec = jpar.getCodec();
		final TreeNode treeNode = codec.readTree(jpar);
		final TreeNode listNode;
		if (treeNode == null) {
			throw new IOException("Missing list JSON node");
		} else {
			listNode = treeNode.get(0);
		}
		final String sTypeId = codec.treeToValue(listNode.get(TYPE), String.class);
		try {
			final Class<?> listClass = typeIdResolver.typeFromId(ctx, sTypeId).getRawClass();
			if (!Collection.class.isAssignableFrom(listClass)) {
				throw new ClassNotFoundException(String.format("Class %s is not a collection!", sTypeId));
			}
			final Constructor<?> ctor = listClass.getDeclaredConstructor();
			ReflectionUtils.makeAccessible(ctor);
			result = (Collection<Object>) ctor.newInstance();
		} catch (ClassNotFoundException | NoSuchMethodException | InstantiationException
				 | IllegalAccessException | ClassCastException | InvocationTargetException e) {
			throw new JsonProcessingException(String.format("Deserialization of collection %s not possible: %s", sTypeId, e.getMessage()), e) {
				private static final long serialVersionUID = 2435985987711244456L;
			};
		}
		final Map<String, Integer> typeIndexMap = codec.treeToValue(listNode.get(TYPE_INDEX_MAP), Map.class);
		final TreeNode valuesArray = listNode.get(VALUES);
		if (typeIndexMap != null && valuesArray != null) {
			final Map<Integer, Class<?>> indexTypeMap = TypePreservingObjectArrayDeserializer.getIndexTypeMap(typeIndexMap, ctx);
			for (int i = 0; i < valuesArray.size(); i++) {
				final TreeNode valueArray = valuesArray.get(i);
				final Integer typeIndex = codec.treeToValue(valueArray.get(0), Integer.class);
				if (indexTypeMap.containsKey(typeIndex)) { // regular value
					final Object value = codec.treeToValue(valueArray.get(1), indexTypeMap.get(typeIndex));
					result.add(value);
				} else { // null value
					result.add(null);
				}
			}
		}
		return result;
	}
}
