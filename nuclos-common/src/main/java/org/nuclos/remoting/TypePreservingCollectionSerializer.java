//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import org.springframework.util.ClassUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

/**
 *  Adds data type information for a collection of different types of values.
 *  The values are each based on a pair (array[2]) consisting of the type index
 *  (look up in @typeIndexMap) and the serialized value.
 *  The type of the list itself can be found in @type.
 *
 *  Example result:
 *  "inComparands":
 *             [
 *                 {
 *                     "@type": "ArrayList#",
 *                     "values":
 *                     [
 *                         [
 *                             0,
 *                             10020140
 *                         ]
 *                     ],
 *                     "@typeIndexMap":
 *                     {
 *                         "Integer#": 0
 *                     }
 *                 }
 *             ]
 *
 *
 * See org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition for example usage:
 * 		<pre>
 *      @JsonSerialize(using = TypePreservingCollectionSerializer.class)
 *      @JsonDeserialize(using = TypePreservingCollectionDeserializer.class)
 * 		private final List<T> inComparands;
 * 		</pre>
 */
public class TypePreservingCollectionSerializer extends JsonSerializer<Collection> {

	static final String TYPE = TypePreservingObjectArraySerializer.TYPE;

	static final String VALUES = TypePreservingObjectArraySerializer.VALUES;

	static final String TYPE_INDEX_MAP = TypePreservingObjectArraySerializer.TYPE_INDEX_MAP;

	static final String NULL_VALUE = TypePreservingObjectArraySerializer.NULL_VALUE;

	private static final TypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	@Override
	public void serializeWithType(final Collection list, final JsonGenerator jgen, final SerializerProvider prov, final TypeSerializer typeSer) throws IOException {
		this.serialize(list, jgen, prov);
	}

	@Override
	public void serialize(final Collection list, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
		jgen.writeStartArray();
		jgen.writeStartObject();
		final String shortName = ClassUtils.getShortName(list.getClass());
		Class<?> listClass = shortName.contains("SingletonSet") ? HashSet.class :
							 shortName.contains("UnmodifiableSet") ? HashSet.class :
							 shortName.contains("Singleton") ? ArrayList.class :
							 shortName.contains("Unmodifiable") ? ArrayList.class :
							 list.getClass();
		jgen.writeStringField(TYPE, typeIdResolver.idFromValueAndType(list, listClass));
		if (!list.isEmpty()) {
			TypePreservingObjectArraySerializer.serializeContent(list.stream(), jgen, prov);
		}
		jgen.writeEndObject();
		jgen.writeEndArray();
	}

}
