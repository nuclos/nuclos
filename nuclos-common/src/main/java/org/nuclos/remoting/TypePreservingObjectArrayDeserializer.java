//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import static org.nuclos.remoting.TypePreservingCollectionSerializer.NULL_VALUE;
import static org.nuclos.remoting.TypePreservingCollectionSerializer.TYPE;
import static org.nuclos.remoting.TypePreservingCollectionSerializer.TYPE_INDEX_MAP;
import static org.nuclos.remoting.TypePreservingCollectionSerializer.VALUES;
import static org.nuclos.remoting.TypePreservingObjectArraySerializer.LENGTH;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

/**
 * @see TypePreservingObjectArraySerializer
 */
public class TypePreservingObjectArrayDeserializer extends JsonDeserializer<Object[]> {

	private static final TypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	@Override
	public Object[] deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
		final ObjectCodec codec = jpar.getCodec();
		final TreeNode listNode = codec.readTree(jpar).get(0);
		final String sTypeId = codec.treeToValue(listNode.get(TYPE), String.class);
		final Class<?> arrayClass = typeIdResolver.typeFromId(ctx, sTypeId).getRawClass();
		final int iLength = Optional.ofNullable(codec.treeToValue(listNode.get(LENGTH), Integer.class)).orElse(0);
		final Object[] result = Object[].class.equals(arrayClass)?
				new Object[iLength]:
				(Object[]) Array.newInstance(arrayClass, iLength);
		final Map<String, Integer> typeIndexMap = codec.treeToValue(listNode.get(TYPE_INDEX_MAP), Map.class);
		final TreeNode valuesArray = listNode.get(VALUES);
		if (typeIndexMap != null && valuesArray != null && iLength > 0) {
			final Map<Integer, Class<?>> indexTypeMap = getIndexTypeMap(typeIndexMap, ctx);
			for (int i = 0; i < valuesArray.size(); i++) {
				final TreeNode valueArray = valuesArray.get(i);
				final Integer typeIndex = codec.treeToValue(valueArray.get(0), Integer.class);
				if (indexTypeMap.containsKey(typeIndex)) { // regular value
					final Object value = codec.treeToValue(valueArray.get(1), indexTypeMap.get(typeIndex));
					result[i] = value;
				} // else: null value
			}
		}
		return result;
	}

	static Map<Integer, Class<?>> getIndexTypeMap(final Map<String, Integer> typeIndexMap, final DeserializationContext ctx) throws JsonProcessingException {
		final Map<Integer, Class<?>> indexTypeMap = new HashMap<>();
		for (Map.Entry<String, Integer> entry : typeIndexMap.entrySet()) {
			if (NULL_VALUE.equals(entry.getKey())) {
				continue;
			}
			Class<?> valueClass;
			try {
				valueClass = typeIdResolver.typeFromId(ctx, entry.getKey()).getRawClass();
			} catch (IOException e) {
				throw new JsonProcessingException(String.format("Deserialization of objects of type %s not possible: %s", entry.getKey(), e.getMessage()), e) {
					private static final long serialVersionUID = -2630578829458502637L;
				};
			}
			indexTypeMap.put(entry.getValue(), valueClass);
		}
		return indexTypeMap;
	}
}
