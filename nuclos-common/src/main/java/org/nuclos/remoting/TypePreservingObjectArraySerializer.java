//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

/**
 *  Adds data type information for an array of different types of values.
 *  The values are each based on a pair (array[2]) consisting of the type index
 *  (look up in @typeIndexMap) and the serialized value.
 *  The type of the list itself can be found in @type.
 *
 *  Example result:
 *  "inComparands":
 *             [
 *                 {
 *                     "@type": "Object[]#",
 *                     "@length": 1,
 *                     "values":
 *                     [
 *                         [
 *                             0,
 *                             10020140
 *                         ]
 *                     ],
 *                     "@typeIndexMap":
 *                     {
 *                         "Integer#": 0
 *                     }
 *                 }
 *             ]
 *
 */
public class TypePreservingObjectArraySerializer extends JsonSerializer<Object[]> {

	static final String TYPE = "@type";

	static final String LENGTH = "@length";

	static final String TYPE_INDEX_MAP = "@typeIndexMap";

	static final String VALUES = "values";

	static final String NULL_VALUE = "null";

	private static final TypePreservingTypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	@Override
	public void serialize(final Object[] array, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
		jgen.writeStartArray();
		jgen.writeStartObject();
		jgen.writeStringField(TYPE, typeIdResolver.idFromType(Object[].class.equals(array.getClass())?
				array.getClass():
				array.getClass().getComponentType()));
		jgen.writeNumberField(LENGTH, array.length);
		if (array.length > 0) {
			serializeContent(Arrays.stream(array), jgen, prov);
		}
		jgen.writeEndObject();
		jgen.writeEndArray();
	}

	static void serializeContent(final Stream<Object> objectStream, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
		jgen.writeFieldName(VALUES);
		jgen.writeStartArray();
		final Map<String, Integer> typeIndexMap = new HashMap<>();
		final AtomicInteger indexer = new AtomicInteger();
		final Iterator<Object> it = objectStream.iterator();
		while (it.hasNext()) {
			final Object o = it.next();
			final String sTypeId = o == null ? NULL_VALUE : typeIdResolver.idFromValue(o);
			Integer index = typeIndexMap.get(sTypeId);
			if (index == null) {
				index = indexer.getAndIncrement();
				typeIndexMap.put(sTypeId, index);
			}
			jgen.writeStartArray();
			jgen.writeNumber(index);
			if (o == null) {
				jgen.writeNull();
			} else {
				jgen.writeObject(o);
			}
			jgen.writeEndArray();
		}
		jgen.writeEndArray();
		jgen.writeObjectField(TYPE_INDEX_MAP, typeIndexMap);
	}

}
