//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.lang.ref.WeakReference;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import com.fasterxml.jackson.databind.node.ContainerNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;

/**
 *  Needed to extend the rigid module classes for TypePreserving
 */
public class TypePreservingObjectWrapperSerializer extends JsonSerializer<TypePreservingObjectWrapper> {

	static final String TYPE = "@type";

	static final String VALUE = "value";

	private final WeakReference<ObjectMapper> _jsonMapper;

	private static final TypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	/**
	 * for internal Jackson usage
	 */
	public TypePreservingObjectWrapperSerializer() {
		this._jsonMapper = new WeakReference<>(null);
	}

	TypePreservingObjectWrapperSerializer(ObjectMapper jsonMapper) {
		this._jsonMapper = new WeakReference<>(jsonMapper);
	}

	@Override
	public void serialize(final TypePreservingObjectWrapper value, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
		final ObjectMapper jsonMapper = getJsonMapper();
		final Object o = value.get();
		final ObjectNode resultNode = jsonMapper.createObjectNode();
		if (o == null) {
			resultNode.putNull(TYPE);
		} else {
			resultNode.put(TYPE, typeIdResolver.idFromValue(o));
			final JsonNode jsonNode = jsonMapper.valueToTree(o);
			if (jsonNode instanceof ValueNode) {
				// Text like UID -> String -> UID
				// or other values like an int, returned from groovy function
				resultNode.set(VALUE, jsonNode);
			} else {
				final ObjectNode wrappedNode = (ObjectNode) jsonNode;
				resultNode.setAll(wrappedNode);
			}
		}
		jgen.writeTree(resultNode);
	}

	private ObjectMapper getJsonMapper() {
		ObjectMapper jsonMapper = _jsonMapper.get();
		if (jsonMapper == null) {
			jsonMapper = NuclosRemotingMapperFactory.createObjectMapper();
		}
		return jsonMapper;
	}

}
