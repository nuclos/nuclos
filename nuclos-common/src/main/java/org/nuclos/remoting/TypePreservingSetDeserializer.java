//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

/**
 * @see TypePreservingSetSerializer
 */
public class TypePreservingSetDeserializer extends JsonDeserializer<Set<?>> {

	private static final TypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	@Override
	public Set<?> deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
		final Set<Object> result = new HashSet<>();
		final ObjectCodec codec = jpar.getCodec();
		final TreeNode typeNode = codec.readTree(jpar);
		final Iterator<String> typeIt = typeNode.fieldNames();
		while (typeIt.hasNext()) {
			final String sTypeId = typeIt.next();
			final Class<?> typeClass;
			try {
				typeClass = typeIdResolver.typeFromId(ctx, sTypeId).getRawClass();
			} catch (IOException e) {
				throw new JsonProcessingException(String.format("Deserialization of objects of type %s not possible: %s", sTypeId, e.getMessage()), e) {
					private static final long serialVersionUID = -2630578829458502637L;
				};
			}

			codec.readValues(jpar, typeClass)
					.forEachRemaining(result::add);
		}
		return result;
	}
}
