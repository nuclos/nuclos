//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

/**
 * See org.nuclos.server.dbtransfer.TransferFacadeRemote#updateNucletContents(org.nuclos.common.UID, java.util.Set, java.util.Set) for example usage:
 * 		<pre>
 * 		@JsonSerialize(using = TypePreservingSetSerializer.class)
 * 		@JsonDeserialize(using = TypePreservingSetDeserializer.class)
 * 		updateNucletContents(UID nuclet, Set<AbstractNucletContentEntryTreeNode> contentsToAdd, Set<AbstractNucletContentEntryTreeNode> contentsToRemove)
 * 		</pre>
 */
public class TypePreservingSetSerializer extends JsonSerializer<Set<?>> {

	private static final TypeIdResolver typeIdResolver = TypePreservingTypeIdResolver.getInstance();

	@Override
	public void serialize(final Set<?> set, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
		jgen.writeStartArray();
		if (!set.isEmpty()) {
			final Map<String, ? extends Set<?>> splitByValueTypesMap = set.stream()
					.filter(Objects::nonNull)
					.collect(Collectors.groupingBy(
							entry -> typeIdResolver.idFromValue(entry),
							Collectors.toSet()
					));

			for( String sClass : splitByValueTypesMap.keySet()) {
				jgen.writeStartObject();
				jgen.writeFieldName(sClass);
				jgen.writeStartArray();
				for (Object entry : splitByValueTypesMap.get(sClass)) {
					jgen.writeObject(entry);
				}
				jgen.writeEndArray();
				jgen.writeEndObject();
			}
		}
		jgen.writeEndArray();
	}

}
