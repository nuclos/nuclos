//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.api.Direction;
import org.nuclos.api.Message;
import org.nuclos.api.Progress;
import org.nuclos.api.Settings;
import org.nuclos.api.command.Command;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.context.InputDelegateSpecification;
import org.nuclos.api.context.InputRequiredException;
import org.nuclos.api.context.InputSpecification;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.api.remoting.TransportObject;
import org.nuclos.common.ClientNotification;
import org.nuclos.common.CommandMessage;
import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMapTransport;
import org.nuclos.common.GenericObjectMetaDataVO;
import org.nuclos.common.INuclosImageProducer;
import org.nuclos.common.LafParameter;
import org.nuclos.common.LafParameterByEntityMap;
import org.nuclos.common.LafParameterMap;
import org.nuclos.common.LockedTabProgressNotification;
import org.nuclos.common.LoginResult;
import org.nuclos.common.MutableMap;
import org.nuclos.common.NuclosDateTime;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.NuclosTranslationMap;
import org.nuclos.common.ProgressNotification;
import org.nuclos.common.SearchFilterNotification;
import org.nuclos.common.SplitViewSettings;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.Version;
import org.nuclos.common.WhitelistedString;
import org.nuclos.common.api.ApiCommandImpl;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.DetailsPresentation;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.ProxyListTransport;
import org.nuclos.common.dal.vo.DataLanguageMap;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.database.query.definition.Column;
import org.nuclos.common.database.query.definition.Constraint;
import org.nuclos.common.database.query.definition.ConstraintEmumerationType;
import org.nuclos.common.database.query.definition.DataType;
import org.nuclos.common.database.query.definition.Join;
import org.nuclos.common.database.query.definition.ReferentialContraint;
import org.nuclos.common.database.query.definition.Schema;
import org.nuclos.common.database.query.definition.Table;
import org.nuclos.common.dbtransfer.NucletExport;
import org.nuclos.common.dbtransfer.PreviewPart;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common.dbtransfer.TransferNuclet;
import org.nuclos.common.dbtransfer.TransferOption;
import org.nuclos.common.dbtransfer.TransferTreeNode;
import org.nuclos.common.lucene.LuceneSearchResult;
import org.nuclos.common.preferences.Preference;
import org.nuclos.common.preferences.PreferenceShareVO;
import org.nuclos.common.printservice.PrintExecutionContext;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common.report.NuclosReportPrintJob;
import org.nuclos.common.report.NuclosReportRemotePrintService;
import org.nuclos.common.report.ejb3.IJobKey;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceParameterValuelistproviderVO;
import org.nuclos.common.report.valueobject.DatasourceQueryExecutionParameters;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintPropertiesTO;
import org.nuclos.common.report.valueobject.PrintResultTO;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.common.report.valueobject.PrintoutTO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.security.InitialSecurityData;
import org.nuclos.common.security.Permission;
import org.nuclos.common.security.SsoAuthStatus;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.statemodel.Statemodel;
import org.nuclos.common.statemodel.StatemodelClosure;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common.textmodule.TextModule;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.common.transport.vo.EntityMetaTransport;
import org.nuclos.common.transport.vo.FieldMetaTransport;
import org.nuclos.common.validation.FieldValidationError;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.EntityAndFieldWithParent;
import org.nuclos.common2.File;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.TruncatableCollectionTransport;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CompileExceptionMessages;
import org.nuclos.common2.exception.InternalErrorException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.schema.rest.SsoAuthEndpoint;
import org.nuclos.server.common.MasterDataPermissions;
import org.nuclos.server.common.ModulePermissions;
import org.nuclos.server.common.valueobject.INuclosValueObject;
import org.nuclos.server.common.valueobject.PreferencesVO;
import org.nuclos.server.dblayer.DbBusinessException;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.DbNotNullableException;
import org.nuclos.server.dblayer.DbNotUniqueException;
import org.nuclos.server.dblayer.DbReferentialIntegrityException;
import org.nuclos.server.genericobject.ProxyListProvider;
import org.nuclos.server.genericobject.context.GenerationContext;
import org.nuclos.server.genericobject.ejb3.GenerationGroupResult;
import org.nuclos.server.genericobject.ejb3.GenerationResult;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.IResultParams;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorUsageVO;
import org.nuclos.server.genericobject.valueobject.GeneratorVO;
import org.nuclos.server.masterdata.ejb3.CollectableFieldsByNameParams;
import org.nuclos.server.masterdata.ejb3.ColumnsFromTableTransport;
import org.nuclos.server.navigation.treenode.RelationRemoveTransport;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.ruleengine.NuclosInputRequiredException;
import org.nuclos.server.statemodel.valueobject.NoteLayout;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;
import com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase;

/**
 *
 */
public class TypePreservingTypeIdResolver extends TypeIdResolverBase {

	private static final Logger LOG = LoggerFactory.getLogger(TypePreservingTypeIdResolver.class);

	public static final String JSON_TYPE_PROPERTY = "@type";

	@JsonTypeInfo(
			use = JsonTypeInfo.Id.CUSTOM,
			include = JsonTypeInfo.As.PROPERTY,
			property = JSON_TYPE_PROPERTY)
	@JsonTypeIdResolver(TypePreservingTypeIdResolver.class)
	public static class DefaultPropertyMixInClass {
	}

	/**
	 * 	These classes are added to the resolver only, no mixin
	 */
	private static final Class<?>[] CLASSES_NO_MIXIN = new Class<?>[]{
			TypePreservingObjectWrapper.class,
			WhitelistedString.class,
			String.class,
			String[].class,
			Long.class,
			Long[].class,
			Integer.class,
			Integer[].class,
			Double.class,
			Double[].class,
			BigDecimal.class,
			BigDecimal[].class,
			BigInteger.class,
			BigInteger[].class,
			Boolean.class,
			Date.class,
			RelativeDate.class,
			java.sql.Date.class,
			org.nuclos.common2.DateTime.class,
			InternalTimestamp.class,
			NuclosDateTime.class,
			NuclosImage.class,
			NuclosPassword.class,
			HashSet.class,
			HashMap.class,
			Hashtable.class,
			ArrayList.class,
			Vector.class,
			Object[].class,
			EntityObjectVO[].class,
			ProxyListTransport.class,
			TruncatableCollectionTransport.class,
			DependentDataMap.class,
			DataLanguageMap.class,
			byte[].class,
			ByteArrayCarrier.class,
			URI.class,
			UID.class,
			UID[].class,
			FieldMapTransport.class,
			ColumnsFromTableTransport.class,
			Preference.class,
			Preference.WritablePreference.class,
			PreferenceShareVO.class,
			PreferencesVO.class,
			PrintProperties.class,
			PrintPropertiesTO.class,
			PrintoutTO.class,
			PrintResultTO.class,
			PrintExecutionContext.class,
			OutputFormatTO.class,
			PrintServiceTO.class,
			PrintServiceTO.Tray.class,
			NuclosReportRemotePrintService.class,
			NuclosReportRemotePrintService[].class,
			ReportVO.OutputType.class,
			ReportVO.ReportType.class,
			ReportOutputVO.Destination.class,
			ReportOutputVO.Format.class,
			ReportOutputVO.PageOrientation.class,
			javax.print.attribute.PrintRequestAttributeSet.class,
			javax.print.attribute.HashPrintServiceAttributeSet.class,
			org.springframework.security.core.authority.SimpleGrantedAuthority.class,
			LoginResult.class,
			Version.class,
			SsoAuthEndpoint.class,
			SsoAuthStatus.class,
			InitialSecurityData.class,
			Permission.class,
			ModulePermissions.class,
			MasterDataPermissions.class,
			SubformPermission.class,
			Statemodel.class,
			StateGraphVO.class,
			StatemodelClosure.class,
			NoteLayout.class,
			TasklistDefinition.class,
			TextModule.class,
			TextModuleSettings.class,
			GenerationResult.class,
			GenerationGroupResult.class,
			GenerationContext.class,
			GeneratorVO.class,
			GeneratorActionVO.class,
			GeneratorUsageVO.class,
			CollectableFieldsByNameParams.class,
			DynamicAttributeVO.class,
			GenericObjectMetaDataVO.class,
			RelationRemoveTransport.class,
			DetailsPresentation.class,
			SplitViewSettings.class
	};

	/**
	 * 	These classes are added to the resolver and get the default mixin (DefaultPropertyMixInClass)
	 */
	private static final Class<?>[] CLASSES = new Class<?>[]{
			LinkedList.class,
			Pair.class,
			EntityAndField.class,
			EntityAndFieldWithParent.class,
			FieldMetaTransport.class,
			EntityMetaTransport.class,
			UsageCriteria.class,
			CollectableSorting.class,
//			ResultParams.class,
			NuclosScript.class,
			Transfer.class,
			Transfer.Result.class,
			TransferOption.class,
			TransferNuclet.class,
			TransferEO.class,
			TransferEO.Transformer.class,
			TransferEO.LocaleResources.class,
			TransferTreeNode.class,
			NucletExport.class,
			PreviewPart.class,
			LocaleInfo.class,
			SearchFilterNotification.class,
			LockedTabProgressNotification.class,
			ProgressNotification.class,
			org.nuclos.common.metadata.NotifyObject.class,
			org.nuclos.common.security.NotifyObject.class,
			ResultVO.class,
			ResultColumnVO.class,
			DatasourceParameterValuelistproviderVO.class,
			DatasourceQueryExecutionParameters.class,
			VLPQuery.class,
			Schema.class,
			Table.class,
			Column.class,
			Constraint.class,
			ReferentialContraint.class,
			ConstraintEmumerationType.class,
			DataType.class,
			Join.class,
			LuceneSearchResult.class,
			InputSpecification.class,
			InputDelegateSpecification.class,
			InputDelegateSpecification.InputDelegateDataMap.class,
			NuclosInputRequiredException.class,
			InternalErrorException.class,
			DbException.class,
			DbBusinessException.class,
			DbInvalidResultSizeException.class,
			DbNotNullableException.class,
			DbNotUniqueException.class,
			DbReferentialIntegrityException.class,
			java.sql.SQLException.class,
            java.lang.IllegalStateException.class,
			java.lang.reflect.InvocationTargetException.class,
			java.lang.RuntimeException.class, // <- only the last fallback
			javax.xml.stream.XMLStreamException.class,
			NuclosCompileException.ErrorMessage.class,
			CompileExceptionMessages.class,
			FieldValidationError.class,
	};

	/**
	 * These package and class(es) combinations initiate a search for hierarchies.
	 * All found classes are added to the resolver.
	 * Both classes and interfaces get the default mixin (DefaultPropertyMixInClass).
	 */
	private static final Pair[] SUBCLASS_SEARCH = new Pair[]{
			Pair.makePair(new PackageInJars("org.nuclos",
							"nuclos-rigid-api-*.jar",
							"nuclos-rigid-common-*.jar",
							"nuclos-dal-common-*.jar",
							"nuclos-dal-common-*.jar",
							"nuclos-meta-common-*.jar",
							"nuclos-layout-common-*.jar",
							"nuclos-datasource-common-*.jar",
							"nuclos-common-*.jar" // <-- includes "nuclos-common-api-*.jar"
					),
					new Class<?>[]{
					EntityMeta.class,
					DbField.class, // <-- FieldMeta
					IDalVO.class, // <-- EntityObjectVO
					INuclosValueObject.class, // <-- MasterDataVO, GenericObjectVO
					IResultParams.class,
					DatasourceVO.class,
					DatasourceParameterVO.class,
					TranslationVO.class,
					NuclosTranslationMap.class,
					File.class,
					NuclosFile.class,
					CollectableField.class,
					CollectableEntityField.class,
					CollectableSearchCondition.class,
					CollectableSearchExpression.class,
					ProxyListProvider.class,
					Settings.class,
					TreeNode.class,
					ClientNotification.class,
					Command.class,
					CommandMessage.class,
					ApiCommandImpl.class,
					Direction.class,
					Message.class,
					Progress.class,
					INuclosImageProducer.class,
					IJobKey.class,
					LafParameter.class,
					LafParameterMap.class,
					LafParameterByEntityMap.class,
					LafParameterByEntityMap.UnmodifiableLafParameterByEntityMap.class,
					ToolBarConfiguration.class,
					ToolBarItem.class,
					NuclosReportPrintJob.class,
					BusinessException.class,
					CommonBusinessException.class,
					InputRequiredException.class,
					CommonFatalException.class,
					org.springframework.security.core.AuthenticationException.class
			}),
			Pair.makePair(new PackageInJars("org.nuclet",
					"*-remoting-*.jar",
							"*-common-*.jar",
							"*-integration-*.jar"), TransportObject.class),
			Pair.makePair(new PackageInJars("org.glassfish.json", "jakarta.json-*.jar"), javax.json.JsonObject.class),
			Pair.makePair(new PackageInJars("org.apache.activemq",
					"activemq-broker-*.jar",
							"activemq-client-*.jar",
							"activemq-http-*.jar"
			), org.apache.activemq.command.DataStructure.class),
			Pair.makePair(new PackageInJars("org.springframework.security.core", "spring-security-core-*.jar"),
					new Class<?>[] {
					org.springframework.security.core.AuthenticationException.class
			}),
	};

	/**
	 * These packages are excluded from the search
	 */
	private static final String[] IGNORE_PACKAGE_SEARCH = new String[] {
			"org.nuclos.client.",
			"org.nuclos.server.autosync.", // <- no ..migration.E_?????? classes
			"org.nuclos.server.dal.processor.",
			"org.nuclos.server.dblayer.",

			"antlr.", "cglib.",
			"commons.codec.", "commons.io.", "commons.logging.", "commons.lang",
			"com.fasterxml.", "com.google.", "com.h2database.",
			"junit.",
			"org.aspectj.", "org.bouncycastle.", "org.dom4j.", "org.easymock.",
			"org.hamcrest.", "org.hibernate.", "org.javassist.", "org.jboss.",
			"org.jsoup.", "org.seleniumhq.", "org.slf4j.", "org.atmosphere.",
			"org.yaml.",

			"java.", "javax.", "javafx.", "com.sun.", "oracle.deploy.",
			"oracle.javafx.", "oracle.jrockit.", "oracle.jvm.", "oracle.net.",
			"oracle.nio.", "oracle.tools.", "oracle.util.", "oracle.webservices.",
			"oracle.xmlns.",

			"com.intellij.", "org.jetbrains."
	};

	/**
	 * These special classes are not added to the resolver
	 */
	private static final Set<String> IGNORE_LIST = new HashSet<>(Arrays.asList(
			"org.springframework.security.core.userdetails.UsernameNotFoundException",
			"org.apache.activemq.filter.AnyDestination",
			"org.apache.activemq.state.Tracked",
			"org.apache.activemq.transport.stomp.StompFrameError"
	));

	/**
	 * These special classes are not accessible and therefore have to be mapped
	 */
	private static final Map<String, Class<?>> CLASS_REPLACEMENTS = Arrays.stream(new Pair[]{
			Pair.makePair(MutableMap.class.getCanonicalName(), HashMap.class),
			Pair.makePair("org.nuclos.common.dal.vo.CachedEntityObjectVO", EntityObjectVO.class),
			Pair.makePair("java.sql.Timestamp", InternalTimestamp.class),
			Pair.makePair("java.util.Arrays$ArrayList", ArrayList.class),
			Pair.makePair("java.util.Collections$EmptyList", ArrayList.class),
			Pair.makePair("java.util.Collections$EmptyMap", HashMap.class),
			Pair.makePair("java.util.Collections$EmptySet", HashSet.class),
			Pair.makePair("java.util.Collections$SingletonList", ArrayList.class),
			Pair.makePair("java.util.Collections$SingletonMap", HashMap.class),
			Pair.makePair("java.util.Collections$SingletonSet", HashSet.class),
			Pair.makePair("java.util.Collections$SynchronizedCollection", ArrayList.class),
			Pair.makePair("java.util.Collections$SynchronizedList", ArrayList.class),
			Pair.makePair("java.util.Collections$SynchronizedMap", HashMap.class),
			Pair.makePair("java.util.Collections$SynchronizedSet", HashSet.class),
			Pair.makePair("java.util.Collections$UnmodifiableCollection", ArrayList.class),
			Pair.makePair("java.util.Collections$UnmodifiableList", ArrayList.class),
			Pair.makePair("java.util.Collections$UnmodifiableRandomAccessList", ArrayList.class),
			Pair.makePair("java.util.Collections$UnmodifiableMap", HashMap.class),
			Pair.makePair("java.util.Collections$UnmodifiableSet", HashSet.class),
	}).collect(Collectors.toMap(
			p -> (String)p.getX(),
			p -> (Class<?>)p.getY()
	));

	/**
	 * These classes are not checked by the TypeIdResolver.checkDeserializable thread
	 */
	private static final Set<String> DO_NOT_CHECK_INSTANTIATION = new HashSet<>(Arrays.asList(
			ClassUtils.getQualifiedName(TypePreservingObjectWrapper.class),
			ClassUtils.getQualifiedName(InvocationTargetException.class),
			ClassUtils.getQualifiedName(Long.class),
			ClassUtils.getQualifiedName(Integer.class),
			ClassUtils.getQualifiedName(Double.class),
			ClassUtils.getQualifiedName(BigDecimal.class),
			ClassUtils.getQualifiedName(BigInteger.class),
			ClassUtils.getQualifiedName(Boolean.class),
			ClassUtils.getQualifiedName(java.sql.Date.class),
			ClassUtils.getQualifiedName(InternalTimestamp.class), // <-- has custom Deserializer
			ClassUtils.getQualifiedName(java.net.URI.class),
			ClassUtils.getQualifiedName(org.springframework.security.core.authority.SimpleGrantedAuthority.class), // <-- has custom Deserializer
			"org.glassfish.json.JsonObjectBuilderImpl$JsonObjectImpl"
	));

	/**
	 * Loads list of classes allowed for remoting from server. The creation of this list in the client takes 1-2 minutes for signed jars
	 * (see also https://github.com/spring-projects/spring-framework/issues/9371), is possible, but only a fallback.
	 */
	public interface IRemotingClassListDownloader {
		Collection<Class<?>> getRegularRemotingClasses() throws IOException, ClassNotFoundException;
		Collection<Class<?>> getInterfaceOrAbstractRemotingClasses() throws IOException, ClassNotFoundException;
	}

	public static class RemotingClassLists {
		public final String[] regularClasses;
		public final String[] interfaceOrAbstractClasses;
		/**
		 * for deserialization only
		 */
		protected RemotingClassLists() {
			super();
			regularClasses = null;
			interfaceOrAbstractClasses = null;
		}
		public RemotingClassLists(final String[] regularClasses, final String[] interfaceOrAbstractClasses) {
			super();
			this.regularClasses = regularClasses;
			this.interfaceOrAbstractClasses = interfaceOrAbstractClasses;
		}
	}

	private static final Map<String, Class<?>> MAP_NAME_TO_INTERFACE_OR_ABSTRACT_CLASS = new HashMap<>();
	private static final Map<String, Class<?>> MAP_ID_TO_CLASS = new HashMap<>();
	private static final Map<String, String> MAP_CLASS_TO_ID = new HashMap<>();
	static {
		final long start = System.currentTimeMillis();
		try {
			Class<?> downloaderClass = Thread.currentThread().getContextClassLoader().loadClass("org.nuclos.remoting.RemotingClassListDownloader");
			LOG.debug("Start downloading of class id mapping");
			try {
				final Constructor<?> ctor = downloaderClass.getDeclaredConstructor();
				ReflectionUtils.makeAccessible(ctor);
				IRemotingClassListDownloader downloader = (IRemotingClassListDownloader) ctor.newInstance();
				downloader.getRegularRemotingClasses().forEach(TypePreservingTypeIdResolver::addClass);
				downloader.getInterfaceOrAbstractRemotingClasses().forEach(TypePreservingTypeIdResolver::addInterfaceOrAbstractClass);
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | InvocationTargetException |
					 NoSuchMethodException | IOException ex) {
				LOG.error("Downloading of remoting class list failed: {}. Create the class id mapping instead (possibly very slow due to signed jars)", ex.getMessage(), ex);
				throw new ClassNotFoundException("JumpToCreationOfClassIdMapping");
			}
			LOG.info("Finished downloading of class id mapping in {} sec ({} classes)",
					(System.currentTimeMillis() - start) / 1000.0,
					MAP_ID_TO_CLASS.size());
		} catch (ClassNotFoundException ex) {
			// ignore... Server creates the list and has no downloader or an error occurred during download of remoting class list
			LOG.debug("Start creation of class id mapping");
			Stream.concat(Arrays.stream(CLASSES_NO_MIXIN), Arrays.stream(CLASSES))
					.filter(TypePreservingTypeIdResolver::isClassDeserializableSimpleCheck)
					.forEach(TypePreservingTypeIdResolver::addClass);
			Stream.concat(Arrays.stream(CLASSES_NO_MIXIN), Arrays.stream(CLASSES))
					.filter(TypePreservingTypeIdResolver::isInterfaceOrAbstractClass)
					.forEach(TypePreservingTypeIdResolver::addInterfaceOrAbstractClass);
			Arrays.stream(SUBCLASS_SEARCH).forEach(pair -> {
				addClassHierarchies((PackageInJars) pair.getX(), asClassArray(pair.getY()));
			});
			LOG.info("Finished creation of class id mapping in {} sec ({} classes)",
					(System.currentTimeMillis() - start) / 1000.0,
					MAP_ID_TO_CLASS.size());
			new Thread("TypeIdResolver.checkDeserializable") {
				@Override
				public void run() {
					MAP_ID_TO_CLASS.values().forEach(TypePreservingTypeIdResolver::checkAndLogClassForDeserialization);
				}
			}.start();
		}
	}
	private static RemotingClassLists remotingClassLists = null;

	private static Class<?>[] asClassArray(Object o) {
		if (o instanceof Class<?>) {
			return new Class<?>[]{(Class<?>) o};
		} else if (o instanceof Class<?>[]) {
			return (Class<?>[]) o;
		} else {
			throw new NotImplementedException();
		}
	}

	private static void addInterfaceOrAbstractClass(Class<?> aInterface) {
		synchronized (MAP_NAME_TO_INTERFACE_OR_ABSTRACT_CLASS) {
			final String qualifiedName = ClassUtils.getQualifiedName(aInterface);
			LOG.trace("Interface or abstract class {} registered for mixin only", qualifiedName);
			MAP_NAME_TO_INTERFACE_OR_ABSTRACT_CLASS.put(qualifiedName, aInterface);
			remotingClassLists = null;
		}
	}

	public static void addClass(final Class<?> aClass) {
		synchronized (MAP_ID_TO_CLASS) {
			String qualifiedName = ClassUtils.getQualifiedName(aClass);
			String shortName = ClassUtils.getShortName(aClass);
			int iHash = qualifiedName.hashCode();
			boolean bAddIdToClass = true;
			if (CLASS_REPLACEMENTS.containsKey(qualifiedName)) {
				final Class<?> replacement = CLASS_REPLACEMENTS.get(qualifiedName);
				shortName = ClassUtils.getShortName(replacement);
				iHash = ClassUtils.getQualifiedName(replacement).hashCode();
				bAddIdToClass = false;
			}
			final String id = String.format("%s#%s", shortName, Integer.toHexString(iHash));
			LOG.trace("Class {} with id {} added for (de)serialization", qualifiedName, id);
			if (bAddIdToClass) {
				MAP_ID_TO_CLASS.put(id, aClass);
			}
			MAP_CLASS_TO_ID.put(qualifiedName, id);
			remotingClassLists = null;
		}
	}

	/**
	 * Scans for subclasses in the given package and jars (for a faster rich client startup).
	 * Returns only deserializable classes. No interfaces or abstract classes. Also includes the base class.
	 * @param sPackage
	 * @param jarList
	 * @param baseClass
	 * @return
	 */
	public static Map<String, Class<?>> addClassHierarchy(final String sPackage, final String[] jarList, Class<?> baseClass) {
		return addClassHierarchies(sPackage, jarList, new Class<?>[]{baseClass});
	}

	/**
	 * Scans for subclasses in the given package and jars (for a faster rich client startup).
	 * Returns only deserializable classes. No interfaces or abstract classes. Also includes the base class.
	 * @param sPackage
	 * @param jarList
	 * @param baseClasses
	 * @return
	 */
	public static Map<String, Class<?>> addClassHierarchies(final String sPackage, final String[] jarList, Class<?>...baseClasses) {
		return addClassHierarchies(new PackageInJars(sPackage, jarList), baseClasses);
	}

	private static Map<String, Class<?>> addClassHierarchies(PackageInJars pija, Class<?>...baseClasses) {
		final String sPackage = pija.sPackage;
		final List<Pattern> patternJarList = pija.jarSet.stream()
				.map(sJarFile -> Pattern.compile(String.format("^file:/*/%s!/*$", sJarFile)
						.replace(".", "\\.")
						.replace("*", ".*")))
				.collect(Collectors.toList());
		ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
		PathMatchingResourcePatternResolver resourceLoader = (PathMatchingResourcePatternResolver) provider.getResourceLoader();
		provider.setResourceLoader(new PathMatchingResourcePatternResolver() {
			@Override
			protected Set<Resource> doFindAllClassPathResources(final String sPath) throws IOException {
				Set<Resource> resources = super.doFindAllClassPathResources(sPath);
				LOG.trace("ClassPathScanner.doFindAllClassPathResources(\"{}\") = SUPER_RESULT:[{}]", sPath, resources.stream().map(Resource::toString).collect(Collectors.joining("; ")));
				resources = resources.stream().filter(r -> {
					if (r instanceof UrlResource) {
						try {
							final URI uri = r.getURI();
							if ("jar".equals(uri.getScheme())) {
								for (Pattern p : patternJarList) {
									if (p.matcher(uri.getSchemeSpecificPart()).find()) {
										return true;
									}
								}
								return false;
							}
						} catch (IOException ex) {
							LOG.error("ClassPath scan for package {} encountered in {} an error: {}", sPackage, sPath, ex.getMessage(), ex);
						}
					}
					return true; // files only (no jar) is always ok (IDE environments)
				}).collect(Collectors.toSet());
				LOG.debug("ClassPathScanner.doFindAllClassPathResources(\"{}\") = [{}]", sPath, resources.stream().map(Resource::toString).collect(Collectors.joining("; ")));
				return resources;
			}
			@Override
			public Resource getResource(final String location) {
				return resourceLoader.getResource(location);
			}
			@Override
			public ClassLoader getClassLoader() {
				return resourceLoader.getClassLoader();
			}
		});
		Arrays.asList(baseClasses).forEach(aBaseClass -> provider.addIncludeFilter(new AssignableTypeFilter(aBaseClass)));
		Arrays.asList(IGNORE_PACKAGE_SEARCH).forEach(ignore ->
			provider.addExcludeFilter(new RegexPatternTypeFilter(Pattern.compile(String.format("^%s.*$", ignore.replace(".", "\\.")))))
		);
		Map<String, Class<?>> result = new HashMap<>();
		try {
			Set<BeanDefinition> components = provider.findCandidateComponents(sPackage.replace('.', '/'));
			for (BeanDefinition component : components) {
				try {
					final Class<?> aClass = ClassUtils.forName(
							component.getBeanClassName(),
							Thread.currentThread().getContextClassLoader());
					if (isClassDeserializableSimpleCheck(aClass)) {
						result.put(component.getBeanClassName(), aClass);
					} else if (isInterfaceOrAbstractClass(aClass)) {
						addInterfaceOrAbstractClass(aClass);
					}
				} catch (Exception | Error ex) {
					LOG.warn("Subclass {} determination [package={}, baseClasses={}] encountered an error: {}",
							component.getBeanClassName(),
							sPackage,
							Arrays.stream(baseClasses).map(ClassUtils::getQualifiedName).collect(Collectors.joining(";")),
							ex.getMessage());
				}
			}
			Arrays.stream(baseClasses)
					.filter(TypePreservingTypeIdResolver::isClassDeserializableSimpleCheck)
					.forEach(aBaseClass -> result.put(ClassUtils.getQualifiedName(aBaseClass), aBaseClass));
			Arrays.stream(baseClasses)
					.filter(TypePreservingTypeIdResolver::isInterfaceOrAbstractClass)
					.forEach(TypePreservingTypeIdResolver::addInterfaceOrAbstractClass);
		} catch (Exception | Error ex) {
			LOG.warn("Subclass determination [package={}, baseClasses={}] encountered an error: {}",
					sPackage,
					Arrays.stream(baseClasses).map(ClassUtils::getQualifiedName).collect(Collectors.joining(";")),
					ex.getMessage());
		}
		result.values().forEach(TypePreservingTypeIdResolver::addClass);
		return result;
	}

	public static boolean checkAndLogClassForDeserialization(Class<?> aClass) {
		try {
			return isClassDeserializableCompleteCheck(aClass);
		} catch (Exception | Error ex) {
			LOG.warn("Class {} is not deserializable: {}", ClassUtils.getQualifiedName(aClass), ex.getMessage());
			return false;
		}
	}

	/**
	 * Returns the list of all registered classes allowed for remoting
	 */
	public static RemotingClassLists getRemotingClassLists() {
		RemotingClassLists result = remotingClassLists;
		if (result == null) {
			result = new RemotingClassLists(
					MAP_ID_TO_CLASS.values().stream()
							.map(aClass -> ClassUtils.getQualifiedName(aClass))
							.sorted()
							.collect(Collectors.toList())
							.toArray(new String[]{}),
					MAP_NAME_TO_INTERFACE_OR_ABSTRACT_CLASS.values().stream()
							.map(aClass -> ClassUtils.getQualifiedName(aClass))
							.sorted()
							.collect(Collectors.toList())
							.toArray(new String[]{})
			);
			remotingClassLists = result;
		}
		return result;
	}

	public static void addMixIns(final NuclosRemotingMapperFactory.RemotingJsonMapper jsonMapper, final NuclosRemotingMapperFactory.RemotingModule module) {
		final Set<String> setNoMixin = Arrays.stream(CLASSES_NO_MIXIN).map(ClassUtils::getQualifiedName).collect(Collectors.toSet());
		addMixIn(jsonMapper, module, setNoMixin, MAP_NAME_TO_INTERFACE_OR_ABSTRACT_CLASS.values().stream());
		addMixIn(jsonMapper, module, setNoMixin, MAP_ID_TO_CLASS.values().stream());
	}

	private static void addMixIn(final NuclosRemotingMapperFactory.RemotingJsonMapper jsonMapper,
								 final NuclosRemotingMapperFactory.RemotingModule module,
								 final Set<String> setNoMixin, Stream<Class<?>> classStream) {
		classStream
				.filter(aClass ->
						!setNoMixin.contains(ClassUtils.getQualifiedName(aClass))
						&& !module.isClassWithOwnSerializer(aClass) // <- mixIn only for classes WITHOUT own Serializer!
				)
				.forEach(aInterface -> jsonMapper.addMixIn(aInterface, DefaultPropertyMixInClass.class));
	}

	private static boolean isInterfaceOrAbstractClass(Class<?> aClass) {
		return aClass.isInterface()
				|| (Modifier.isAbstract(aClass.getModifiers()) && !aClass.isArray()); // Arrays are abstract!
	}

	private static boolean isClassDeserializableSimpleCheck(Class<?> aClass) {
		return !IGNORE_LIST.contains(ClassUtils.getQualifiedName(aClass))
				&& !isInterfaceOrAbstractClass(aClass);
	}

	private static boolean isClassDeserializableCompleteCheck(Class<?> aClass) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		if (isClassDeserializableSimpleCheck(aClass)) {
			if (!DO_NOT_CHECK_INSTANTIATION.contains(ClassUtils.getQualifiedName(aClass))
				&& !aClass.isArray()
				&& !aClass.isEnum()) {
				// check default instantiation
				final Constructor<?> ctor = aClass.getDeclaredConstructor();
				ReflectionUtils.makeAccessible(ctor);
				ctor.newInstance();
			}
			return true;
		}
		return false;
	}

	private static TypePreservingTypeIdResolver INSTANCE;

	private TypePreservingTypeIdResolver() {
	}

	public static TypePreservingTypeIdResolver getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new TypePreservingTypeIdResolver();
			TypePreservingMapDeserializer.setTypeIdResolver(INSTANCE);
			TypePreservingMapSerializer.setTypeIdResolver(INSTANCE);
			TypePreservingObjectDeserializer.setTypeIdResolver(INSTANCE);
			TypePreservingObjectSerializer.setTypeIdResolver(INSTANCE);
		}
		return INSTANCE;
	}

	@Override
	public JavaType typeFromId(final DatabindContext context, final String id) throws IOException {
		return context.constructType(Optional.ofNullable(MAP_ID_TO_CLASS.get(id))
				.orElseThrow(() -> new NotImplementedException(String.format("Suggested bean id %s is unknown", id))));
	}

	@Override
	public String idFromValue(final Object value) {
		return idFromValueAndType(value, value.getClass());
	}

	@Override
	public String idFromValueAndType(final Object notUsed, final Class<?> suggestedType) {
		try {
			return idFromType(suggestedType);
		} catch (NotImplementedException e) {
			if (notUsed instanceof Exception) {
				throw new NotImplementedException(e.getMessage(), (Exception) notUsed);
			}
			throw e;
		}
	}

	public String idFromType(final Class<?> suggestedType) {
		final String qualifiedName = ClassUtils.getQualifiedName(suggestedType);
		String typeId = MAP_CLASS_TO_ID.get(qualifiedName);
		if (typeId == null) {
			final Class<?> replacementClass = CLASS_REPLACEMENTS.get(qualifiedName);
			if (replacementClass != null) {
				typeId = MAP_CLASS_TO_ID.get(ClassUtils.getQualifiedName(replacementClass));
			}
		}
		return Optional.ofNullable(typeId)
				.orElseThrow(() -> new NotImplementedException(String.format("Suggested type %s is not registered for remoting", qualifiedName)));
	}

	@Override
	public JsonTypeInfo.Id getMechanism() {
		return JsonTypeInfo.Id.CUSTOM;
	}

	private static class PackageInJars {
		private final String sPackage;
		private final Set<String> jarSet;
		private PackageInJars(final String sPackage, final String...jarList) {
			this.sPackage = sPackage;
			this.jarSet = new HashSet<>(Arrays.asList(jarList));
		}
	}
}
