package org.nuclos.server.dbtransfer;

public enum ConstraintAction {
    CLEAN_REBUILD,
    CLEAN,
    REBUILD;

    @Override
    public String toString() {
        switch(this.ordinal()) {
            case 0:
                return "CLEAN_REBUILD";
            case 1:
                return "CLEAN";
            case 2:
                return "REBUILD";
        }
        return "NOT_FOUND";
    }
}
