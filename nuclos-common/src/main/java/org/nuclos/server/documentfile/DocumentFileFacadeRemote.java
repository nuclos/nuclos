package org.nuclos.server.documentfile;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.remoting.NuclosRemotingInterface;

@NuclosRemotingInterface
public interface DocumentFileFacadeRemote {
	
	@RolesAllowed("Login")
	public NuclosFile loadContentAsNuclosFile(UID documentFileUID);
	
	@RolesAllowed("Login")
	public byte[] loadContent(UID documentFileUID);
}
