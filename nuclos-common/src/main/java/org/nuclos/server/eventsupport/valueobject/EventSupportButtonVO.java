package org.nuclos.server.eventsupport.valueobject;

public class EventSupportButtonVO extends EventSupportVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6832711556222727230L;

	/**
	 * for deserialization only
	 */
	protected EventSupportButtonVO() {
		super();
	}

	public EventSupportButtonVO(Integer pOrder, String pEventsupportClass,
			String pEventSupportClassType) {
		super(pOrder, pEventsupportClass, pEventSupportClassType);
		}

	
}
