package org.nuclos.server.genericobject.context;

import java.util.Collection;

import org.nuclos.common.DependentSelection;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class GenerationContextBuilder<PK> {
	private final Collection<EntityObjectVO<PK>> sourceEos;
	private final GeneratorActionVO generator;

	private DependentSelection dependentSelection;
	private Long parameterObjectId = null;
	private boolean cloning = false;
	private String customUsage = null;

	public GenerationContextBuilder(
			final Collection<EntityObjectVO<PK>> sourceEos,
			final GeneratorActionVO generator
	) {
		this.sourceEos = sourceEos;
		this.generator = generator;
	}

	public GenerationContext<PK> build() {
		return new GenerationContext<>(
				sourceEos,
				dependentSelection,
				generator,
				parameterObjectId,
				cloning,
				customUsage
		);
	}

	public GenerationContextBuilder<PK> parameterObjectId(Long parameterObjectId) {
		this.parameterObjectId = parameterObjectId;
		return this;
	}

	public GenerationContextBuilder<PK> cloning(boolean cloning) {
		this.cloning = cloning;
		return this;
	}

	public GenerationContextBuilder<PK> customUsage(String customUsage) {
		this.customUsage = customUsage;
		return this;
	}

	public GenerationContextBuilder<PK> dependentSelection(DependentSelection dependentSelection) {
		this.dependentSelection = dependentSelection;
		return this;
	}
}
