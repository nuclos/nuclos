//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.Collection;
import java.util.Map;

import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.remoting.TypePreservingMapDeserializer;
import org.nuclos.remoting.TypePreservingMapSerializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class GenerationGroupResult<PK> {

	@JsonSerialize(using = TypePreservingMapSerializer.class)
	@JsonDeserialize(using = TypePreservingMapDeserializer.class)
	private final Map<String, Collection<EntityObjectVO<PK>>> result;

	/**
	 * for deserialization only
	 */
	protected GenerationGroupResult() {
		super();
		this.result = null;
	}

	public GenerationGroupResult(final Map<String, Collection<EntityObjectVO<PK>>> result) {
		this.result = result;
	}

	public Map<String, Collection<EntityObjectVO<PK>>> getResult() {
		return result;
	}

}
