//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import org.nuclos.common.UID;

/**
 * Value object representing a leased object attribute to generate.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.000
 */
public class GeneratorGenericObjectAttributeVO {

	private UID attribute = null;
	private Long valueId = null;
	private String sValue = null;

	/**
	 * constructor to be called by server only
	 * @param attribute attribute to generate
	 * @param valueId value id for attribute
	 * @param sValue value for attribute
	 */
	GeneratorGenericObjectAttributeVO(UID attribute, Long valueId, String sValue) {
		this.attribute = attribute;
		this.valueId = valueId;
		this.sValue = sValue;
	}

	/**
	 * get attribute uid
	 * @return attribute uid
	 */
	public UID getAttribute() {
		return attribute;
	}

	/**
	 * get value id
	 * @return value id
	 */
	public Long getValueId() {
		return valueId;
	}

	/**
	 * get value
	 * @return value
	 */
	public String getValue() {
		return sValue;
	}
}
