package org.nuclos.server.genericobject.searchcondition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.nuclos.common.UID;

public class ResultParams implements IResultParams {
	/**
	 * 
	 */
	private static final long serialVersionUID = -525770922219591470L;

	private static final Long ROWCOUNTLIMIT = 250000L;
	
	private final Collection<UID> fields;
	private final Long lOffset;
	private final Long lLimit;
	private final boolean bSortResult;
	private final List<Object> anchor;

	private boolean bLoadThumbnailsOnly = true;

	private boolean bIdOnlySelection = false;
	private boolean forceDistinct = false;

	/**
	 * for deserialization only
	 */
	protected ResultParams() {
		super();
		fields = null;
		lOffset = null;
		lLimit = null;
		bSortResult = false;
		anchor = null;
	}

	public ResultParams(Collection<UID> fields, Long lOffset, Long lLimit, boolean bSortResult) {
		this.fields = fields;
		this.lOffset = lOffset;
		this.lLimit = lLimit;
		this.bSortResult = bSortResult;
		this.anchor = new ArrayList<>();
	}

	public ResultParams(Long lLimit, boolean bSortResult) {
		this(null, null, lLimit, bSortResult);
	}

	public ResultParams(Collection<UID> fields) {
		this(fields, null, ROWCOUNTLIMIT, false);
	}

	@Override
	public Collection<UID> getFields() {
		return fields;
	}

	@Override
	public Long getOffset() {
		return lOffset;
	}

	@Override
	public Long getLimit() {
		return lLimit;
	}

	@Override
	public boolean isSortResult() {
		return bSortResult;
	}

	public List<Object> getAnchor() {
		return anchor;
	}

	@Override
	public boolean isIdOnlySelection() {
		return bIdOnlySelection;
	}

	@Override
	public void setIdOnlySelection(final boolean bIdOnlySelection) {
		this.bIdOnlySelection = bIdOnlySelection;
	}

	@Override
	public boolean loadThumbnailsOnly() {
		return bLoadThumbnailsOnly;
	}

	@Override
	public void setLoadThumbnailsOnly(final boolean bLoadThumbnailsOnly) {
		this.bLoadThumbnailsOnly = bLoadThumbnailsOnly;
	}

	@Override
	public ResultParams forceDistinct() {
		forceDistinct = true;
		return this;
	}

	@Override
	public boolean isForceDistinct() {
		return forceDistinct;
	}

	public static ResultParams DEFAULT_RESULT_PARAMS = new ResultParams(null, false);

}
