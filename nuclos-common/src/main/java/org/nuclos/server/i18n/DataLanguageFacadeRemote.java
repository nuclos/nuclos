package org.nuclos.server.i18n;

import java.util.Collection;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.remoting.NuclosRemotingInterface;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

@NuclosRemotingInterface
public interface DataLanguageFacadeRemote {

	Collection<DataLanguageVO> getDataLanguages();
	Collection<DataLanguageVO> getAllDataLanguages();
	
	void setPrimaryDataLanguage(DataLanguageVO dlvo);
	
	void removeDataLanguage(DataLanguageVO dlvo)
			throws NuclosBusinessRuleException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonPermissionException;
	
	void addDataLanguage(DataLanguageVO dlvo);
	
	void save(List<DataLanguageVO> selectDataLanguages) throws 
		NuclosBusinessRuleException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonPermissionException,CommonCreateException, CommonValidationException;
	
	UID getPrimaryDataLanguageUID();
	
	UID getCurrentUserDataLanguageUID();
	
	UID getLanguageToUse();
}
