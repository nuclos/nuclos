//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.maintenance;

import java.util.List;

import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.remoting.NuclosRemotingInterface;

@NuclosRemotingInterface
public interface MaintenanceFacadeRemote extends MaintenanceConstants {
	
	int MAINTENANCE_INIT_WAIT_TIME_IN_MINUTES = 5;
	int MAINTENANCE_INIT_WAIT_TIME_IN_MINUTES_FOR_DEVMODE = 1;

	String getMaintenanceMode();
	
	/**
	 * @return true if user is blocked because the server is in maintenance mode
	 */
	boolean blockUserLogin(String username);

	String getMaintenanceSuperUserName();

	String enterMaintenanceMode(String maintenanceSuperUserName);

	void forceMaintenanceMode();

	String exitMaintenanceMode() throws CommonValidationException;

	Integer getWaittimeInSeconds();

	default int getCompleteWaittimeInSeconds() {
		return (isDevelopmentEnvironment() ? MAINTENANCE_INIT_WAIT_TIME_IN_MINUTES_FOR_DEVMODE : getMaintenaceInitWaitTimeInMinutes()) * 60;
	}

	long getNumberOfRecentActiveSessions();

	long getNumberOfRecentActiveUsers(String excludeUsername);

	List<String> getRecentActiveUsers();

	Integer getNumberOfRunningJobs();

	boolean isProductionEnvironment();

	boolean isDevelopmentEnvironment();

	public int getMaintenaceInitWaitTimeInMinutes();
}
