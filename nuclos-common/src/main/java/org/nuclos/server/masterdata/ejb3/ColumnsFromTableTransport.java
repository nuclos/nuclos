//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.ejb3;

import java.util.Map;

import org.nuclos.remoting.TypePreservingMapDeserializer;
import org.nuclos.remoting.TypePreservingMapSerializer;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ColumnsFromTableTransport {

	@JsonSerialize(using = TypePreservingMapSerializer.class)
 	@JsonDeserialize(using = TypePreservingMapDeserializer.class)
	public Map<String, MasterDataVO<?>> mapColumnsFromTable;

	/**
	 * for deserialization only
	 */
	protected ColumnsFromTableTransport() {
		super();
		this.mapColumnsFromTable = null;
	}

	public ColumnsFromTableTransport(final Map<String, MasterDataVO<?>> mapColumnsFromTable) {
		super();
		this.mapColumnsFromTable = mapColumnsFromTable;
	}
}
