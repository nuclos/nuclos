//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.ejb3;


import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.api.context.SaveFlag;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.ProxyListTransport;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.report.valueobject.DatasourceQueryExecutionParameters;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.TruncatableCollectionTransport;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.remoting.NuclosRemotingInterface;
import org.nuclos.remoting.TypePreservingObjectWrapper;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


// @Remote
@NuclosRemotingInterface
public interface MasterDataFacadeRemote extends CommonMasterDataFacade {

	/**
	 * §todo restrict permissions by entity name
	 * 
	 * @return a proxy list containing the search result for the given search expression.
	 */
	@RolesAllowed("Login")
	ProxyListTransport getMasterDataProxyListTransport(UID entity, Collection<UID> fields, CollectableSearchExpression clctexpr)
		throws CommonPermissionException;

	/**
	 * gets the ids of all masterdata objects that match a given search expression (ordered, when necessary)
	 * 
	 * @param cse condition that the masterdata objects to be found must satisfy
	 * @return List&lt;Integer&gt; list of masterdata ids
	 */
	@RolesAllowed("Login")
	List<Object> getMasterDataIds(UID entity, CollectableSearchExpression cse) throws CommonPermissionException;

	@RolesAllowed("Login")
	<PK> List<MasterDataVO<PK>> getMasterDataChunk(UID entity, final CollectableSearchExpression clctexpr, ResultParams resultParams)
			throws CommonPermissionException;
	
    @RolesAllowed("Login")
	Long countMasterDataRows(UID entity, final CollectableSearchExpression clctexpr) throws CommonPermissionException;

	@RolesAllowed("Login")
	Long countMasterDataRowsWithLimit(UID entity, final CollectableSearchExpression clctexpr, Long limit) throws CommonPermissionException;

	/**
	 * Convenience function to get all reports or forms used in AllReportsCollectableFieldsProvider.
	 * 
	 * @return TruncatableCollection&lt;MasterDataVO&gt; collection of master data value objects
	 * @throws CommonFinderException if a row was deleted in the time between executing the search and fetching the single rows.
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	TruncatableCollectionTransport getAllReportsTransport() throws CommonFinderException,
		CommonPermissionException;
	/**
	 * Convenience function to get all generations used in AllGenerationsCollectableFieldsProvider.
	 * 
	 * @return TruncatableCollection&lt;MasterDataVO&gt; collection of master data value objects
	 * @throws CommonFinderException if a row was deleted in the time between executing the search and fetching the single rows.
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	TruncatableCollectionTransport getAllGenerationsTransport() throws CommonFinderException,
		CommonPermissionException;
	
	/**
	 * convinience function to get all recordgrants used in AllRecordgrantsCollectableFieldsProvider.
	 * 
	 * @return TruncatableCollection&lt;MasterDataVO&gt; collection of master data value objects
	 * @throws CommonFinderException if a row was deleted in the time between executing the search and fetching the single rows.
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	TruncatableCollectionTransport getAllRecordgrantsTransport() throws CommonFinderException,
		CommonPermissionException;

	/**
	 * loadDependents and subDependends for Matrix
	 * @param foreignKeyField
	 * @param oRelatedId
	 * @param lstChildSubform
	 * @return
	 */
	@RolesAllowed("Login")
	IDependentDataMap loadMatrixData(UID foreignKeyField, TypePreservingObjectWrapper oRelatedId, List<EntityAndField> lstChildSubform,
									 UID layoutUID) throws CommonBusinessException;

	@RolesAllowed("Login")
	Collection<MasterDataVO<Long>> loadMatrixXAxis(UID entity, UID layoutUID)
		throws CommonBusinessException;


	/**
	 * method to get master data records for a given entity and search condition
	 *
	 * §postcondition result != null
	 * §todo restrict permissions by entity name
	 *
	 * @param entityUID UID of the entity to get master data records for
	 * @param cond search condition
	 * @return TruncatableCollection&lt;MasterDataVO&gt; collection of master data value objects
	 */
	@RolesAllowed("Login")
	TruncatableCollectionTransport getMasterDataWithCheckTransport(
			UID entityUID, CollectableSearchCondition cond, boolean bAll) throws CommonPermissionException;

	/**
	 * gets the dependant master data records for the given entity, using the given foreign key field and the given id as foreign key.
	 * 
	 * §precondition oRelatedId != null
	 * §todo restrict permissions by entity name
	 *  @param entity UID of the entity to get all dependant master data records for
	 * @param sForeignKeyField UID of the field relating to the foreign entity
     * @param filterCondition
     * @param oRelatedIds id by which sEntityName and sParentEntity are related
     */
	@RolesAllowed("Login")
	<PK> Collection<EntityObjectVO<PK>> getDependentDataCollection(
			UID sForeignKeyField, CollectableSearchCondition filterCondition, DatasourceQueryExecutionParameters queryParams, List<Object> oRelatedIds) throws CommonPermissionException;

	/**
	 * method to get a master data value object for given primary key id
	 * 
	 * @param entity UID of the entity to get record for
	 * @param oId primary key id of master data record
	 * @return master data value object
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	<PK> MasterDataVO<PK> getRemote(UID entity, TypePreservingObjectWrapper oId)
		throws CommonFinderException, CommonPermissionException;

	/**
	 * method to get a master data value object with dependents for given primary key id
	 *
	 * @param entity UID of the entity to get record for
	 * @param oId primary key id of master data record
	 * @param dependantsDepth
	 * @return master data value object
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	<PK> MasterDataVO<PK> getWithDependentsRemote(UID entity, TypePreservingObjectWrapper oId, final int dependantsDepth)
			throws CommonFinderException, CommonPermissionException;

	/**
	 * @param sEntityName
	 * @param oId
	 * @return the version of the given masterdata id.
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	Integer getVersionRemote(UID sEntityName, TypePreservingObjectWrapper oId)
		throws CommonFinderException, CommonPermissionException;

	/**
	 * create a new master data record
	 * 
	 * §precondition sEntityName != null
	 * §precondition mdvo.getId() == null
	 * §precondition (mpDependants != null) --&gt; mpDependants.areAllDependantsNew()
	 * §nucleus.permission checkWriteAllowed(sEntityName)
	 * 
	 * @param mdvo the master data record to be created
	 * @return master data value object containing the newly created record
	 */
	@RolesAllowed("Login")
	<PK> MasterDataVO<PK> create(MasterDataVO<PK> mdvo, String customUsage) throws CommonValidationException, CommonCreateException,
		CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * modifies an existing master data record.
	 * 
	 * §precondition sEntityName != null
	 * §nucleus.permission checkWriteAllowed(sEntityName)
	 * 
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
	@RolesAllowed("Login")
	@Deprecated
	<PK> PK modify(MasterDataVO<PK> mdvo) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonStaleVersionException, CommonValidationException,
		CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * modifies an existing master data record.
	 * 
	 * §precondition sEntityName != null
	 * §nucleus.permission checkWriteAllowed(sEntityName)
	 * 
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
	@RolesAllowed("Login")
	@Deprecated
	<PK> PK modify(MasterDataVO<PK> mdvo, boolean isCollectiveProcessing) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonStaleVersionException, CommonValidationException,
		CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * modifies an existing master data record.
	 * 
	 * §precondition sEntityName != null
	 * §nucleus.permission checkWriteAllowed(sEntityName)
	 * 
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
	@RolesAllowed("Login")
	<PK> PK modify(MasterDataVO<PK> mdvo, String customUsage, boolean isCollectiveProcessing, SaveFlag... saveFlags) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonStaleVersionException, CommonValidationException,
		CommonPermissionException, NuclosBusinessRuleException;
	
	/**
	 * modifies an existing master data record.
	 * 
	 * §precondition sEntityName != null
	 * §nucleus.permission checkWriteAllowed(sEntityName)
	 * 
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
	@RolesAllowed("Login")
	<PK> PK modify(MasterDataVO<PK> mdvo, String customUsage) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonStaleVersionException, CommonValidationException,
		CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * method to delete an existing master data record
	 * 
	 * §precondition sEntityName != null
	 * §nucleus.permission checkDeleteAllowed(sEntityName)
	 *  @param mdvo containing the master data record
	 *
	 */
	@RolesAllowed("Login")
	void removeRemote(UID entity, TypePreservingObjectWrapper pk) throws CommonFinderException,
		CommonRemoveException, CommonStaleVersionException,
		CommonPermissionException, NuclosBusinessRuleException;
	/**
	 * method to delete an existing master data record
	 * 
	 * §precondition sEntityName != null
	 * §nucleus.permission checkDeleteAllowed(sEntityName)
	 *  @param mdvo containing the master data record
	 *
	 */
	@RolesAllowed("Login")
	void removeRemote(UID entity, TypePreservingObjectWrapper pk,
					 String customUsage) throws CommonFinderException,
		CommonRemoveException, CommonStaleVersionException,
		CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * Get all subform entities of a masterdata entity
	 */
	@RolesAllowed("Login")
	Set<EntityAndField> getSubFormEntitiesByMasterDataEntity(
		UsageCriteria usage);

	/**
	 * value list provider function (get processes by usage)
	 * @param entityUid module id of usage criteria
	 * @param bSearchMode when true, validity dates and/or active sign will not be considered in the search.
	 * @return collection of master data value objects
	 */
	@RolesAllowed("Login")
	List<CollectableValueIdField> getProcessByEntity(UID entityUid, boolean bSearchMode);

	/**
	 * @param iModuleId the id of the module whose subentities we are looking for
	 * @return Collection&lt;MasterDataMetaVO&gt; the masterdata meta 
	 * 		information for all entities having foreign keys to the given module.
	 */
	List<CollectableField> getSubEntities(UID iModuleId);

	/**
	 * @param user - the user for which to get subordinated users
	 * @return List&lt;MasterDataVO&gt; list of masterdata valueobjects
	 */
	Collection<MasterDataVO<UID>> getUserHierarchy(String user);
	
	/**
	  * @param entityUid
	  * @return Does the entity with the given uid use the rule engine?
	  */
	boolean getUsesRuleEngine(UID entityUid);

	@RolesAllowed("Login")
	boolean exist(UID entityUid, TypePreservingObjectWrapper id);

	Integer getShadowID(TypePreservingObjectWrapper id);

	<PK> void fillDependentsForSubformColumns(
			List<EntityObjectVO<PK>> eos,
			Collection<UID> fields,
			UID entity,
			String customUsage
	);

	<PK> Collection<MasterDataVO<PK>> getWithDependents(UID entityUid, List<PK> oIds, int dependantsDepth) throws CommonFinderException, CommonPermissionException;
}
