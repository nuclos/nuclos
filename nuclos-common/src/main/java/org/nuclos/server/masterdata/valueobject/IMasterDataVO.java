//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.valueobject;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.server.common.valueobject.INuclosValueObject;
import org.nuclos.server.common.valueobject.NuclosValueObject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Interface to {@link org.nuclos.server.masterdata.valueobject.MasterDataVO}.
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.8
 * @param <PK> primary key type
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public interface IMasterDataVO<PK> extends INuclosValueObject<PK> {

	/**
	 * Return the underlying EntityObjectVO.
	 * @since Nuclos 3.8
	 * @author Thomas Pasch
	 */
	@JsonIgnore
	EntityObjectVO<PK> getEntityObject();

	/**
	 * §postcondition result.isChanged() == this.isChanged()
	 * §postcondition result.isRemoved() == this.isRemoved()
	 * §postcondition result.getFields().equals(this.getFields())
	 * §postcondition result.getId() == this.getId()
	 * 
	 * @return a clone of <code>this</code>.
	 * @see #copy()
	 */
	MasterDataVO<PK> clone();

	void setChanged(boolean changed);

	boolean equals(Object obj);

	/**
	 * §postcondition !result.isChanged()
	 * §postcondition result.getFields().equals(this.getFields())
	 * §postcondition result.getId() == null
	 * 
	 * @return a new copy of <code>this</code>, with <code>null</code> id.
	 * @see #clone()
	 */
	MasterDataVO<PK> copy();

	/**
	 * §postcondition !result.isChanged()
	 * §postcondition result.getFields().equals(this.getFields())
	 * §postcondition result.getId() == null
	 * 
	 * @return a new copy of <code>this</code>, with <code>null</code> id.
	 * @see #clone()
	 */
	MasterDataVO<PK> copy(boolean blnWithDependants);
	
	/**
	 * @return this object's primary key
	 */
	PK getPrimaryKey();
	
	void setPrimaryKey(PK pk);

	/**
	 * Returns true if this record is a system record.
	 */
	boolean isSystemRecord();


	/**
	 * CAUTION: this method should only be used in the context of reports, to hide the data
	 * of subforms if the user has no read permission on it
	 *
	 * set all fields to null
	 */
	void clearFields(Collection<UID> fields);

	/**
	 * @return Has this object been changed since its creation?
	 */
	boolean isChanged();

	@JsonIgnore
	String getDebugInfo();

	String toDescription();

	/**
	 * @return the common fields of this object. Note that this may only be called for entities which have an Integer id.
	 * @see #getId()
	 */
	@JsonIgnore
	NuclosValueObject<PK> getNuclosValueObject();

	void setDependents(IDependentDataMap mpDependents);

	IDependentDataMap getDependents();

	List<TranslationVO> getResources();

	void setResources(List<TranslationVO> resources);

	Object getFieldValue(UID fieldUID);
	
	Long getFieldId(UID field);

	UID getFieldUid(UID field);

	<T> T getFieldValue(UID fieldUID, Class<T> cls);

	void setFieldValue(UID fieldUID, Object oValue);

	void setFieldId(UID fieldUID, Long id);

	void setFieldUid(UID fieldUID, UID uid);

	@JsonIgnore
	Map<UID, Object> getFieldValues();

	@JsonIgnore
	Map<UID, Long> getFieldIds();

	@JsonIgnore
	Map<UID, UID> getFieldUids();

}
