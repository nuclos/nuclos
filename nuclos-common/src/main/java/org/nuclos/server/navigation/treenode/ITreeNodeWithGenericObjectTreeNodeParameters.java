package org.nuclos.server.navigation.treenode;


public interface ITreeNodeWithGenericObjectTreeNodeParameters extends TreeNode {
	
	GenericObjectTreeNodeParameters getGenericObjectTreeNodeParameters();

}
