//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import org.nuclos.common.UID;

public class RelationRemoveTransport{

	private final Long relationId;
	private final Long objectId;
	private final UID entityUID;

	/**
	 * for deserialization only
	 */
	protected RelationRemoveTransport() {
		this.relationId = null;
		this.objectId = null;
		this.entityUID = null;
	}

	public RelationRemoveTransport(final Long relationId, final Long objectId, final UID entityUID) {
		this.relationId = relationId;
		this.objectId = objectId;
		this.entityUID = entityUID;
	}

	public Long getRelationId() {
		return relationId;
	}

	public Long getObjectId() {
		return objectId;
	}

	public UID getEntityUID() {
		return entityUID;
	}
}
