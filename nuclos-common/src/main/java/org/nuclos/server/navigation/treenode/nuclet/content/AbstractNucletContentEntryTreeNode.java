//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode.nuclet.content;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.navigation.treenode.TreeNode;

/**
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 *
 * @author	<a href="mailto:maik.stueker@nuclos.de">maik.stueker</a>
 * @version 00.01.000
 */
public abstract class AbstractNucletContentEntryTreeNode implements TreeNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4948016888723119891L;

	protected final UID entityUID;
	protected final EntityObjectVO<UID> eo;

	/**
	 * @deprecated See org.nuclos.server.navigation.ejb3.TreeNodeFacadeBean.getRefToNuclet(EntityMeta) what to do (tp).
	 */
	private static final String FOREIGN_FIELD_TO_NUCLET = "nuclet";

	public static final String NAME_FIELD = "name";

	public static List<EntityMeta<UID>> getNucletContentEntities() {
		List<EntityMeta<UID>> result = new ArrayList<EntityMeta<UID>>();
		result.add(E.ENTITY);
		result.add(E.PROCESS);
		result.add(E.LAYOUT);
		result.add(E.STATEMODEL);
		result.add(E.GENERATION);
		result.add(E.ENTITYFIELDGROUP);
		result.add(E.CUSTOMCOMPONENT);
		result.add(E.WEBADDON);

		result.add(E.REPORT);
		result.add(E.DATASOURCE);
		result.add(E.DYNAMICENTITY);
		result.add(E.VALUELISTPROVIDER);
		result.add(E.RECORDGRANT);
		result.add(E.CHART);
		result.add(E.CALCATTRIBUTE);

		result.add(E.DYNAMICTASKLIST);
		result.add(E.TASKLIST);

		result.add(E.SERVERCODE);

		result.add(E.DBOBJECT);

		result.add(E.RELATIONTYPE);
		result.add(E.RESOURCE);
		result.add(E.REPORTGROUP);

		result.add(E.ROLE);
		result.add(E.WORKSPACE);
		result.add(E.PREFERENCE);
		
		result.add(E.JOBCONTROLLER);
		result.add(E.SEARCHFILTER);
		result.add(E.WEBSERVICE);
        result.add(E.RESTAPIS);

		result.add(E.IMPORT);
		result.add(E.XML_IMPORT);

		result.add(E.BUSINESSTEST);

		result.add(E.EMAILINCOMINGSERVER);
		result.add(E.EMAILOUTGOINGSERVER);

		return result;
	}

	/**
	 * for deserialization only
	 */
	protected AbstractNucletContentEntryTreeNode() {
		super();
		this.entityUID = null;
		this.eo = null;
	}

	public AbstractNucletContentEntryTreeNode(EntityObjectVO<UID> eo) {
		this.entityUID = eo.getDalEntity();
		this.eo = eo;
		EntityMeta<?> eMeta = E.getByUID(this.entityUID);
		if (eMeta == null) {
			throw new IllegalArgumentException("entity must not be null, entityUID=" + this.entityUID);
		}
		if (!getNucletContentEntities().contains(eMeta)) {
			throw new IllegalArgumentException("entity " + eMeta.getEntityName() + " must be nuclet content");
		}
	}

	/**
	 * client only
	 */
	@Override
	public final String getLabel() {
		return getName();
	}

	/**
	 * client only
	 * @return
	 */
	public final String getLabelWithEntity() {
		return SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(E.getByUID(this.entityUID)) + ": " + getName();
	}

	/**
	 * client only
	 */
	public String getName() {
		UID field = null;
		for (FieldMeta<?> fm : E.getByUID(this.entityUID).getFields()) {
			if ("name".equals(fm.getFieldName())) {
				field = fm.getUID();
			}
		}

		String name = null;
		if (field != null) {
			name = eo.getFieldValue(field, String.class);
		}
		return StringUtils.defaultIfNull(name, "<[NO NAME]> " + eo.getPrimaryKey());
	}

	@Override
	public String getDescription() {
		return "";
	}

	@Override
	public AbstractNucletContentEntryTreeNode refreshed() throws CommonFinderException {
		try {
			return null;//Utils.getTreeNodeFacade().getNucletTreeNode(this.getId(), false);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public final EntityObjectVO<UID> getEntityObjectVO() {
		return eo;
	}

	public final EntityMeta<UID> getEntity() {
		return E.getByUID(this.entityUID);
	}

	@Override
	public final UID getId() {
		return eo.getPrimaryKey();
	}

	@Override
	public final String getIdentifier() {
		return getLabelWithEntity();
	}

	@Override
	public List<? extends TreeNode> getSubNodes() {
		return Collections.emptyList();
	}

	@Override
	public Boolean hasSubNodes() {
		return false;
	}

	@Override
	public void removeSubNodes() {
	}

	@Override
	public void refresh() {
	}

	@Override
	public boolean implementsNewRefreshMethod() {
		return false;
	}

	@Override
	public boolean needsParent() {
		return true;
	}

	@Override
	public String toString() {
		return getLabelWithEntity();
	}

	protected static EntityMeta<?> getMetaData(UID entity) {
		return SpringApplicationContextHolder.getBean(IMetaProvider.class).getEntity(entity);
	}

	@Override
	public UID getEntityUID() {
		return this.entityUID;
	}

	public static class Comparator implements java.util.Comparator<AbstractNucletContentEntryTreeNode> {
		@Override
		public int compare(AbstractNucletContentEntryTreeNode o1, AbstractNucletContentEntryTreeNode o2) {
			if (o1.entityUID.equals(o2.entityUID)) {
				if (LangUtils.equal(o1.getLabel(), o2.getLabel())) {
					// when the labels are equal, we have to compare the primary key otherwise NUCLOS-7009 happens
					return LangUtils.compare(o1.eo.getPrimaryKey(), o2.eo.getPrimaryKey());
				} else {
					// compare lexically, when the labels are different
					return LangUtils.compare(o1.getLabel(), o2.getLabel());
				}
			} else {
				return LangUtils.compare(getNucletContentEntities().indexOf(E.getByUID(o1.entityUID)),
								getNucletContentEntities().indexOf(E.getByUID(o2.entityUID)));
			}
		};
	}
	
	@Override
	public UID getNodeId() {
		throw new NotImplementedException("getNodeId not implemented for " + this.getClass().getSimpleName());
	}
	
	@Override
	public Long getRootId() {
		throw new NotImplementedException("getRootId not implemented for " + this.getClass().getSimpleName());
	}
}	// class NucletTreeNode
