package org.nuclos.server.rule.client;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.rule.client.vo.ClientRuleSourceVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

public interface ClientRuleFacadeRemote {

	public UID createClientRule(ClientRuleSourceVO crSource) 
			throws CommonCreateException, CommonPermissionException, NuclosBusinessRuleException, CommonValidationException;
	
	public void removeClientRule(ClientRuleSourceVO crSource) 
			throws CommonRemoveException, NuclosBusinessRuleException, CommonFinderException, 
			CommonStaleVersionException, CommonPermissionException;
	
	public void insertFieldBackgroundColorRule(UID ruleId, UID fieldId) 
			throws CommonCreateException;
	
	public void insertFieldCalculationRule(UID ruleId, UID fieldId) 
			throws CommonCreateException;
}
