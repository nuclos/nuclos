package org.nuclos.server.rule.client.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.common.collection.Pair;

public class ClientRuleContextParameter implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2405114498664037538L;

	private Map<UID, Object> attributes =
			new HashMap<UID, Object>();
	
	private Map<Pair<UID,UID>, Map<UID, Object>> references =
			new HashMap<Pair<UID,UID>, Map<UID, Object>>();
	
	private Map<Pair<UID,UID>, List<ClientRuleContextParameterDependent>> dependents =
			new HashMap<Pair<UID,UID>, List<ClientRuleContextParameterDependent>>();
	
	
	// references
	public void addReference(UID referencingField, UID referencedBO, Map<UID, Object> refAttributeMap) {
		Pair<UID, UID> pair = new Pair<UID, UID>(referencingField, referencedBO);
		this.references.put(pair, refAttributeMap);
	}
	
	public Map<UID, Object> getReference(UID referencingField, UID referencedBO) {
		Map<UID, Object> retVal = null;
		
		Pair<UID, UID> pair = new Pair<UID, UID>(referencingField, referencedBO);
		
		if (this.references.containsKey(pair))
			retVal = this.references.get(pair);
		
		return retVal;
	}
	
	public Object getReferenceAttribute(UID refField, UID field) {
		Object retVal = null;
		// TODO ClientRules ... refBO ??
		UID refBO = null;
		Pair<UID, UID> pair = new Pair<UID, UID>(refField, refBO);
		if (this.references.containsKey(pair) && 
				this.references.get(pair) != null && this.references.get(pair).containsKey(field))
					retVal = this.references.get(pair).get(field);
		
		return retVal;
	}
	
	// attributes
	public void addAttribute(UID field, Object element) {
		this.attributes.put(field, element);
	}
	public Object getAttribute(UID field) {
		return this.attributes.get(field);
	}
	public Map<UID, Object> getAttributes() {
		return this.attributes;
	}
	
	// dependents
	public void addDependent(UID refFieldUID, UID refBOUID, Map<UID, Object> depAttributes, Flag depState) {
		
		Pair<UID, UID> pair = new Pair<UID, UID>(refFieldUID, refBOUID);
		
		if (!this.dependents.containsKey(pair)) {
			this.dependents.put(pair, new ArrayList<ClientRuleContextParameterDependent>());
		}
	
		ClientRuleContextParameterDependent dep = 
				new ClientRuleContextParameterDependent(pair, depAttributes, depState);
		
		this.dependents.get(pair).add(dep);		
	}
	
	public List<Map<UID, Object>> getDependent(UID refFieldUID, UID refBOUID, Flag... flags) {
		 List<Map<UID, Object>> retVal = null;
		 
		 Pair<UID, UID> pair = new Pair<UID, UID>(refFieldUID, refBOUID);
		 
		 if (this.dependents.containsKey(pair)) {
			 
			 retVal = new ArrayList<Map<UID,Object>>();
			 
			 for (ClientRuleContextParameterDependent dep : this.dependents.get(pair)) {
				 retVal.add(dep.getAttributes());
			 }
		 }
		 
		 return retVal;
	}
	
	class ClientRuleContextParameterDependent {
		
		private Pair<UID,UID> reference;
		private Map<UID, Object> attributes;
		private Flag state;
			
		/**
		 * @param reference
		 * @param attributes
		 * @param state
		 * @param id
		 */
		public ClientRuleContextParameterDependent(Pair<UID, UID> reference,
				Map<UID, Object> attributes, Flag state) {
			super();
			this.reference = reference;
			this.attributes = attributes;
			this.state = state;
		
		}

		public Pair<UID, UID> getReference() {
			return reference;
		}

		public Map<UID, Object> getAttributes() {
			return attributes;
		}

		public Flag getState() {
			return state;
		}
		
	}
}
