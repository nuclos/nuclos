package org.nuclos.server.ruleengine;

public interface AutomaticRuleCompilation {

	boolean isAutomaticRuleCompilation();
	
	void setAutomaticRuleCompilation(boolean ruleCompilation);	
	
}
