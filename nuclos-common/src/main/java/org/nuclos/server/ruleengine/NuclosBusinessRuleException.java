//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.ruleengine;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.NuclosBusinessException;

/**
 * Base exception for all business exceptions occurring within a rule.
 * <p>
 * This is the exception to wrapped {@link BusinessException} within
 * in order to propagate it unaltered to the nuclos client.
 * <p>
 * ATTENTION:
 * Don't broaden the exception that could wrapped within! You will
 * trash Nuclos! Also see {@link #getCausingBusinessException()}. (tp)
 * 
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @author Thomas Pasch (improved javadoc)
 * @version 00.01.000
 */
public class NuclosBusinessRuleException extends NuclosBusinessException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5836134344011224669L;

	public NuclosBusinessRuleException() {
		super("ruleengine.error.exception.nucleusbusinessruleexception");
	}

	public NuclosBusinessRuleException(BusinessException tCause) {
		super(tCause.getMessage() == null ? "ruleengine.error.exception.nucleusbusinessruleexception" : tCause.getMessage(), tCause);
	}

	public NuclosBusinessRuleException(String sMessage) {
		super(sMessage);
	}
	
	public NuclosBusinessRuleException(String sMessage, BusinessException tCause) {
		super(sMessage, tCause);
	}

	public static NuclosBusinessRuleException extractNuclosBusinessRuleExceptionIfAny(Exception ex) {
		final Throwable cause = ex.getCause();
		if (ex instanceof NuclosBusinessRuleException) {
			return (NuclosBusinessRuleException) ex;
		} else if (cause != null && cause instanceof Exception) {
			return extractNuclosBusinessRuleExceptionIfAny((Exception) cause);
		} else {
			return null;
		}
	}
	
	public BusinessException getCausingBusinessException() {
		return (BusinessException) getCause();
	}
	
}
