//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.valueobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;

/**
 * The layout of a StateModel. Serialized for persistence. Though this is bad style in general,
 * a state model layout contains no critical data.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class StateModelLayout implements Serializable {
	private static final long serialVersionUID = 8097817704354917878L;

	private final Map<UID, StateLayout> states = CollectionUtils.newHashMap();

	private final Map<UID, TransitionLayout> transitions = CollectionUtils.newHashMap();

	private final List<NoteLayout> notes = new ArrayList<NoteLayout>();

	public StateModelLayout() {
	}

	public StateLayout getStateLayout(final UID stateUid) {
		return this.states.get(stateUid);
	}

	public TransitionLayout getTransitionLayout(final UID transitionUid) {
		return this.transitions.get(transitionUid);
	}

	public void insertStateLayout(final UID stateUid, final StateLayout layout) {
		this.states.put(stateUid, layout);
	}

	public void insertTransitionLayout(final UID transitionUid, final TransitionLayout layout) {
		this.transitions.put(transitionUid, layout);
	}

	public void updateState(final UID stateUid, double dX, double dY, double dWidth, double dHeight) {
		final StateLayout statelayout = this.getStateLayout(stateUid);
		if (statelayout != null) {
			statelayout.setX(dX);
			statelayout.setY(dY);
			statelayout.setWidth(dWidth);
			statelayout.setHeight(dHeight);
		}
	}

	public void updateTransition(final UID transitionUid, int iStart, int iEnd) {
		final TransitionLayout transitionlayout = this.getTransitionLayout(transitionUid);
		if (transitionlayout != null) {
			transitionlayout.setConnectionStart(iStart);
			transitionlayout.setConnectionEnd(iEnd);
		}
	}

	public void updateStateId(final UID oldStateUid, final UID newStateUid) {
		final StateLayout statelayout = this.getStateLayout(oldStateUid);
		if (statelayout == null) {
			return;
		}
		this.insertStateLayout(newStateUid, new StateLayout(statelayout.getX(), statelayout.getY(), statelayout.getWidth(), statelayout.getHeight()));
		this.removeState(oldStateUid);
	}

	public void updateTransitionId(final UID oldTransitionUid, final UID newTransitionUid) {
		final TransitionLayout transitionlayout = this.getTransitionLayout(oldTransitionUid);
		this.insertTransitionLayout(newTransitionUid, new TransitionLayout(newTransitionUid, transitionlayout.getConnectionStart(), transitionlayout.getConnectionEnd()));
		this.removeTransition(oldTransitionUid);
	}

	public void removeState(final UID stateUid) {
		this.states.remove(stateUid);
	}

	public void removeTransition(final UID transitionUid) {
		this.transitions.remove(transitionUid);
	}

	public List<NoteLayout> getNotes() {
		return this.notes;
	}

	public Map<UID, StateLayout> getStates() {
		return states;
	}

	public Map<UID, TransitionLayout> getTransitions() {
		return transitions;
	}

}	// class StateModelLayout
