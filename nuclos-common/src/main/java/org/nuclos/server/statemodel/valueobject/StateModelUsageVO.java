package org.nuclos.server.statemodel.valueobject;

import java.io.Serializable;

import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;

public class StateModelUsageVO implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 6281869890543920549L;

	private final UID stateModelUID;
	private final UID initialStateUID;
	private final UsageCriteria usagecriteria;

	public StateModelUsageVO(UID stateModelUID, UID initialStateUID, UsageCriteria usagecriteria) {
		this.stateModelUID = stateModelUID;
		this.initialStateUID = initialStateUID;
		this.usagecriteria = usagecriteria;
	}

	public UID getStateModelUID() {
		return stateModelUID;
	}

	public UID getInitialStateUID() {
		return initialStateUID;
	}

	public UsageCriteria getUsageCriteria() {
		return this.usagecriteria;
	}

}