//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.valueobject;

import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * Value object representing a state model.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.00
 */
public class StateModelVO extends NuclosValueObject<UID> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1729279911631730133L;
	private String sName;
	private String sDescription;
	private StateModelLayout layout;
	private boolean showNumeral;
	private boolean showIcon;

	private UID nuclet;
	private String nucletLabel;

	public StateModelVO() {
		this(null, null, false, true, null, null, null);
	}

	/**
	 * constructor to be called by server only
	 * @param nvo
	 * @param sName model name of underlying database record
	 * @param sDescription model description of underlying database record
	 * @param layout model layout information of underlying database record
	 * @param showNumeral
	 * @param showIcon
	 */
	public StateModelVO(NuclosValueObject<UID> nvo, String sName, String sDescription, StateModelLayout layout, final boolean showNumeral, final boolean showIcon, UID nuclet, String nucletLabel) {
		super(nvo);
		this.sName = sName;
		this.sDescription = sDescription;
		this.layout = layout;
		this.showNumeral = showNumeral;
		this.showIcon = showIcon;
		this.nuclet = nuclet;
		this.nucletLabel = nucletLabel;
	}

	/**
	 * constructor to be called by client only
	 * @param sName model name of underlying database record
	 * @param sDescription model description of underlying database record
	 * @param showNumeral
	 * @param showIcon
	 * @param layout model layout information of underlying database record
	 */
	public StateModelVO(String sName, String sDescription, final boolean showNumeral, final boolean showIcon, StateModelLayout layout, UID nuclet, String nucletLabel) {
		super();
		this.sName = sName;
		this.sDescription = sDescription;
		this.layout = layout;
		this.showNumeral = showNumeral;
		this.showIcon = showIcon;
		this.nuclet = nuclet;
		this.nucletLabel = nucletLabel;
	}

	/**
	 * get model name of underlying database record
	 * @return model name of underlying database record
	 */
	public String getName() {
		return sName;
	}

	/**
	 * set model name of underlying database record
	 * @param sName model name of underlying database record
	 */
	public void setName(String sName) {
		this.sName = sName;
	}

	/**
	 * get model description of underlying database record
	 * @return model description of underlying database record
	 */
	public String getDescription() {
		return sDescription;
	}

	/**
	 * set model description of underlying database record
	 * @param sDescription model description of underlying database record
	 */
	public void setDescription(String sDescription) {
		this.sDescription = sDescription;
	}

	/**
	 * get model layout of underlying database record
	 * @return model layout of underlying database record
	 */
	public StateModelLayout getLayout() {
		return this.layout;
	}

	/**
	 * set model layout of underlying database record
	 * @param layoutinfo model layout of underlying database record
	 */
	public void setLayout(StateModelLayout layoutinfo) {
		this.layout = layoutinfo;
	}

	public boolean getShowNumeral() {
		return showNumeral;
	}

	public void setShowNumeral(final boolean showNumeral) {
		this.showNumeral = showNumeral;
	}

	public boolean getShowIcon() { return showIcon; }

	public void setShowIcon(final boolean showIcon) { this.showIcon = showIcon; }

	public String getNuclet() {
		return nucletLabel;
	}

	public void setNuclet(String nucletLabel) {
		this.nucletLabel = nucletLabel;
	}

	public UID getNucletUID() {
		return nuclet;
	}

	public void setNucletUID(UID nuclet) {
		this.nuclet = nuclet;
	}

	@Override
	public int hashCode() {
		return Objects.hash(sName, sDescription, showNumeral, showIcon, nuclet);
	}

	@Override
	public boolean equals(Object o) {
		if(o == this)
			return true;

		if(getClass() != o.getClass())
			return false;

		StateModelVO stateModelVO = (StateModelVO) o;
		return (StringUtils.equals(this.sName, stateModelVO.sName) && StringUtils.equals(this.sDescription, stateModelVO.sDescription)
				&& Objects.equals(this.getId(), stateModelVO.getId()) && this.showNumeral == stateModelVO.getShowNumeral()
				&& this.getShowIcon() == stateModelVO.getShowIcon());
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("uid=").append(getId());
		result.append(",name=").append(getName());
		result.append("]");
		return result.toString();
	}

}	// class StateModelVO
