package org.nuclos.common;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class ConsoleCommandTest {

	@Test
	public void testCommand() {
		assert "-startMaintenance".equals(ConsoleCommand.START_MAINTENANCE.getCommand());
	}
}