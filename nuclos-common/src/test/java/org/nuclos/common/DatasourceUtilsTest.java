package org.nuclos.common;

import java.util.List;

import org.junit.Test;
import org.nuclos.common.querybuilder.DatasourceUtils;

/**
 * @author Lucas Welscher <lucas.welscher@nuclos.de>
 */
public class DatasourceUtilsTest {

	/**
	 * Test for NUCLOS-8446
	 */
	@Test
	public void testDatasourceColumnParser() {

		String sql = "SELECT a.a, a.b FROM table a";
		List<String> columns = DatasourceUtils.getColumns(sql);

		assert columns.size() == 2;
		assert columns.contains("a.a");
		assert columns.contains("a.b");

		columns = DatasourceUtils.getColumnsWithoutQuotes(columns);

		assert columns.size() == 2;
		assert columns.contains("a");
		assert columns.contains("b");

		sql = "SELECT a.a \"A\", a.b \"B\" FROM table a";
		columns = DatasourceUtils.getColumns(sql);

		assert columns.size() == 2;
		assert columns.contains("\"A\"");
		assert columns.contains("\"B\"");

		columns = DatasourceUtils.getColumnsWithoutQuotes(columns);

		assert columns.size() == 2;
		assert columns.contains("A");
		assert columns.contains("B");

		sql = "SELECT a.a \"A,B\", a.b FROM table a";
		columns = DatasourceUtils.getColumns(sql);

		assert columns.size() == 2;
		assert columns.contains("\"A,B\"");
		assert columns.contains("a.b");

		columns = DatasourceUtils.getColumnsWithoutQuotes(columns);

		assert  columns.size() == 2;
		assert columns.contains("A,B");
		assert columns.contains("b");

		sql = "SELECT a.a as A, a.b as B FROM table a";
		columns = DatasourceUtils.getColumns(sql);

		assert columns.size() == 2;
		assert columns.contains("A");
		assert columns.contains("B");

		columns = DatasourceUtils.getColumnsWithoutQuotes(columns);

		assert columns.size() == 2;
		assert columns.contains("A");
		assert columns.contains("B");

		sql = "SELECT a.a as A, a.b \"B\", a.c"; //no FROM
		columns = DatasourceUtils.getColumns(sql);

		assert columns.size() == 3;
		assert columns.contains("A");
		assert columns.contains("\"B\"");
		assert columns.contains("a.c");

		columns = DatasourceUtils.getColumnsWithoutQuotes(columns);

		assert columns.size() == 3;
		assert columns.contains("A");
		assert columns.contains("B");
		assert columns.contains("c");

		sql = "SELECT COALESCE(a.strnachname, '') || ', ' || COALESCE(a.strvorname, '') \"Ansprechpartner\" FROM mitarbeiter a";
		columns = DatasourceUtils.getColumns(sql);

		assert columns.size() == 1;
		assert columns.contains("\"Ansprechpartner\"");

		columns = DatasourceUtils.getColumnsWithoutQuotes(columns);

		assert columns.size() == 1;
		assert columns.contains("Ansprechpartner");

		sql = "SELECT a.a \"',''\" FROM a";
		columns = DatasourceUtils.getColumns(sql);

		assert columns.size() == 1;
		assert columns.contains("\"',''\"");

		columns = DatasourceUtils.getColumnsWithoutQuotes(columns);

		assert columns.size() == 1;
		assert columns.contains("',''");

		sql = "SELECT NULLIF(a.name, ',\",') as NULLIF";
		columns = DatasourceUtils.getColumns(sql);

		assert columns.size() == 1;
		assert columns.contains("NULLIF");

		columns = DatasourceUtils.getColumnsWithoutQuotes(columns);

		assert columns.size() == 1;
		assert columns.contains("NULLIF");

		sql = "SELECT a A, b B, c as C, d \"D\" FROM table t";
		columns = DatasourceUtils.getColumns(sql);

		assert columns.size() == 4;
		assert columns.contains("A");
		assert columns.contains("B");
		assert columns.contains("C");
		assert columns.contains("\"D\"");

		columns = DatasourceUtils.getColumnsWithoutQuotes(columns);

		assert columns.size() == 4;
		assert columns.contains("A");
		assert columns.contains("B");
		assert columns.contains("C");
		assert columns.contains("D");

		sql = "SELECT --a A,\n" +
				"b B,--c,\n" +
				"D as D\n" +
				"FROM table t";
		columns = DatasourceUtils.getColumns(sql);

		assert columns.size() == 2;
		assert columns.contains("B");
		assert columns.contains("D");
	}
}
