package org.nuclos.common;

import org.junit.Test;

public class SchemaVersionTest {

	/**
	 * Compares application and schema version.
	 * At least major and minor version should match.
	 */
	@Test
	public void testVersions() {
		new ApplicationProperties().afterPropertiesSet();

		Version appVersion = new Version(ApplicationProperties.getInstance().getCurrentVersion().getVersionNumber());
		Version schemaVersion = new Version(E.getSchemaVersion());

		assert appVersion.getMajor() == schemaVersion.getMajor();
		assert appVersion.getMinor() == schemaVersion.getMinor();
	}

	private class Version {
		int major;
		int minor;
		String revision;

		public Version(String versionString) {
			String[] parts = versionString.split("\\.");

			assert parts.length >= 3;

			major = Integer.parseInt(parts[0]);
			minor = Integer.parseInt(parts[1]);
			revision = parts[2];
		}

		public int getMajor() {
			return major;
		}

		public int getMinor() {
			return minor;
		}

		public String getRevision() {
			return revision;
		}
	}
}
