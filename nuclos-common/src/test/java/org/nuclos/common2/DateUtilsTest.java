package org.nuclos.common2;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class DateUtilsTest {
	@Test
	public void formatDateWithFullYear() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2017);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DATE, 31);

		Date date = cal.getTime();

		assert "31.01.2017".equals(DateUtils.formatDateWithFullYear(date, Locale.GERMAN));
		assert "01/31/2017".equals(DateUtils.formatDateWithFullYear(date, Locale.ENGLISH));
	}
}