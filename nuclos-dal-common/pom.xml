<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<parent>
		<groupId>org.nuclos</groupId>
		<artifactId>nuclos</artifactId>
		<version>4.2025.9-SNAPSHOT</version>
		<relativePath>../pom.xml</relativePath>
	</parent>
	<modelVersion>4.0.0</modelVersion>

	<artifactId>nuclos-dal-common</artifactId>
	<packaging>jar</packaging>

	<name>nuclos-dal-common</name>
	<url>http://www.nuclos.de/</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<nuclos.javadoc.prefix>../../../</nuclos.javadoc.prefix>

		<sonar.moduleKey>${project.artifactId}</sonar.moduleKey>
	</properties>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<archive>
						<!-- index/ -->
						<!-- manifest>
							<addClasspath>true</addClasspath>
							<mainClass>org.nuclos.client.main.Main</mainClass>
							<packageName>com.nuclos.client</packageName>
						</manifest -->
						<manifestEntries>
							<Application-Name>nuclos.de ERP</Application-Name>
							<Permissions>all-permissions</Permissions>
							<Codebase>*</Codebase>
							<Trusted-Only>true</Trusted-Only>
							<Trusted-Library>true</Trusted-Library>
						</manifestEntries>
					</archive>
				</configuration>
			</plugin>
		</plugins>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
			</resource>
		</resources>
	</build>

	<dependencies>
		<dependency>
			<groupId>org.nuclos</groupId>
			<artifactId>nuclos-schema</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>org.nuclos</groupId>
			<artifactId>nuclos-common-api</artifactId>
		</dependency>
		<dependency>
			<groupId>org.nuclos</groupId>
			<artifactId>nuclos-rigid-common</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- We need the V1 XmlPullParser API and implementation -->
		<!-- xmlpull does _not_ work -->
		<!-- Please don't change the sequence !!! (tp) -->
		<dependency>
			<groupId>org.apache.servicemix.bundles</groupId>
			<artifactId>org.apache.servicemix.bundles.xmlpull</artifactId>
		</dependency>
		<dependency>
			<groupId>xpp3</groupId>
			<artifactId>xpp3_min</artifactId>
		</dependency>
		<dependency>
			<artifactId>slf4j-api</artifactId>
			<groupId>org.slf4j</groupId>
		</dependency>
		<dependency>
			<artifactId>slf4j-reload4j</artifactId>
			<groupId>org.slf4j</groupId>
		</dependency>
		<!-- Don't delegate log4j to slf4j for the moment. (tp) -->
		<!-- dependency>
			<artifactId>log4j-over-slf4j</artifactId>
			<groupId>org.slf4j</groupId>
		</dependency -->
		<dependency>
			<!-- groupId>javax.annotation</groupId> <artifactId>com.springsource.javax.annotation</artifactId>
				<version>1.0.0</version -->
			<groupId>javax.annotation</groupId>
			<artifactId>jsr250-api</artifactId>
		</dependency>
		<!-- dependency>
			<groupId>javax.ejb</groupId>
			<artifactId>ejb-api</artifactId>
			<version>3.0</version>
		</dependency -->
		<dependency>
			<groupId>net.sourceforge.collections</groupId>
			<artifactId>collections-generic</artifactId>
		</dependency>
		<dependency>
			<groupId>commons-digester</groupId>
			<artifactId>commons-digester</artifactId>
		</dependency>
		<dependency>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
		</dependency>
		<dependency>
			<groupId>commons-pool</groupId>
			<artifactId>commons-pool</artifactId>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.maven</groupId>
			<artifactId>maven-artifact</artifactId>
		</dependency>
		<dependency>
			<groupId>com.googlecode.json-simple</groupId>
			<artifactId>json-simple</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aop</artifactId>
		</dependency>
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjrt</artifactId>
		</dependency>
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjweaver</artifactId>
		</dependency>
		<!--
			This is only needed because of
			https://jira.springsource.org/browse/SPR-6819
		 -->
		<dependency>
			<groupId>javax.persistence</groupId>
			<artifactId>persistence-api</artifactId>
			<scope>provided</scope>
		</dependency>
		<!-- dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-asm</artifactId>
		</dependency -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-beans</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-expression</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-remoting</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
		</dependency>
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
		</dependency>
		<!-- needed only for ltw - hence provided -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-instrument</artifactId>
			<scope>provided</scope>
		</dependency>
		<!-- testing -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-inline</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.easymock</groupId>
			<artifactId>easymock</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.kubek2k</groupId>
			<artifactId>springockito</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.kubek2k</groupId>
			<artifactId>springockito-annotations</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-math3</artifactId>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
		</dependency>
		<dependency>
			<groupId>org.jvnet.jaxb2_commons</groupId>
			<artifactId>jaxb2-basics-runtime</artifactId>
			<version>0.6.4</version>
		</dependency>
		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>2.8.9</version>
		</dependency>

		<!-- Needed for Java 9 -->
		<dependency>
			<groupId>javax.xml.bind</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>2.3.1</version>
		</dependency>
		<dependency>
			<groupId>com.sun.xml.bind</groupId>
			<artifactId>jaxb-core</artifactId>
			<version>2.3.0.1</version>
		</dependency>
		<dependency>
			<groupId>com.sun.xml.bind</groupId>
			<artifactId>jaxb-impl</artifactId>
			<version>2.3.9</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.module</groupId>
			<artifactId>jackson-module-jaxb-annotations</artifactId>
		</dependency>

	</dependencies>

</project>
