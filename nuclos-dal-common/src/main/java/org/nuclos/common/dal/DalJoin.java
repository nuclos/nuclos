//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal;

import org.nuclos.common.FieldMeta;

public class DalJoin {

	/**
	 *
	 */
	private static final long serialVersionUID = -5138319963757820348L;
	private final String tableAliasLeft;
	private final String tableAliasRight;

	private final FieldMeta field;

	/**
	 * for deserialization only
	 */
	protected DalJoin() {
		super();
		this.tableAliasLeft = null;
		this.tableAliasRight = null;
		this.field = null;
	}

	/**
	 * §precondition sSubEntityName != null
	 * §precondition sForeignKeyFieldName != null
	 */
	public DalJoin(FieldMeta<?> field, String tableAlias) {
		this(field, "t", tableAlias);
	}

	public DalJoin(FieldMeta<?> field, String tableAliasLeft, String tableAliasRight) {
		if (field == null || tableAliasLeft == null || tableAliasRight == null) {
			throw new NullPointerException();
		}
		this.tableAliasLeft = tableAliasLeft;
		this.tableAliasRight = tableAliasRight;
		this.field = field;
	}

	public FieldMeta<?> getField() {
		return field;
	}

	public String getTableAliasLeft() {
		return tableAliasLeft;
	}

	public String getTableAliasRight() {
		return tableAliasRight;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof DalJoin)) {
			return false;
		}
		final DalJoin that = (DalJoin) o;
		return field.equals(that.getField()) && tableAliasLeft.equals(that.getTableAliasLeft()) && tableAliasRight.equals(that.getTableAliasRight());
	}

	@Override
	public int hashCode() {
		return field.hashCode();
	}

	@Override
	public String toString() {
		return getClass().getName() + ":" + field + ":" + getTableAliasLeft() + "/" + getTableAliasRight();
	}
}
