//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.nuclos.api.businessobject.Dependent;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;

@JsonSerialize(using = CachedEntityObjectVO.CachedEntityObjectVOSerializer.class)
public class CachedEntityObjectVO<PK> extends EntityObjectVO<PK> {

	private final EntityObjectVO<PK> cachedValue;

	private final Set<UID> ignoredCachedFields = new HashSet<>(0);

	private final CachedDependentMap cachedDependentMap;

	public CachedEntityObjectVO(final EntityObjectVO<PK> cachedValue) {
		this(cachedValue, cachedValue.getDependents());
	}

	protected CachedEntityObjectVO(final EntityObjectVO<PK> cachedValue, IDependentDataMap cachedDependents) {
		super(cachedValue.getDalEntity(), 0, 0, 0);
		this.cachedValue = cachedValue;
		this.cachedDependentMap = new CachedDependentMap(
				super.getDependents(),
				cachedDependents == null ? new DependentDataMap() : cachedDependents);
		this.primaryKey = cachedValue.getPrimaryKey();
		this.version = cachedValue.getVersion();
		this.createdBy = cachedValue.getCreatedBy();
		this.createdAt = cachedValue.getCreatedAt();
		this.changedBy = cachedValue.getChangedBy();
		this.changedAt = cachedValue.getChangedAt();
		this.canWrite = cachedValue.canWrite;
		this.canStateChange = cachedValue.canStateChange;
		this.canDelete = cachedValue.canDelete;
		this.setComplete(cachedValue.isComplete());
	}

	@Override
	public Map<UID, Long> getFieldIds() {
		final Map<UID, Long> result = new HashMap<>(cachedValue.getFieldIds());
		ignoredCachedFields.forEach(result::remove);
		result.putAll(super.getFieldIds());
		return Collections.unmodifiableMap(result);
	}

	@Override
	public Map<UID, UID> getFieldUids() {
		final Map<UID, UID> result = new HashMap<>(cachedValue.getFieldUids());
		ignoredCachedFields.forEach(result::remove);
		result.putAll(super.getFieldUids());
		return Collections.unmodifiableMap(result);
	}

	@Override
	public Map<UID, Object> getFieldValues() {
		final Map<UID, Object> result = new HashMap<>(cachedValue.getFieldValues());
		ignoredCachedFields.forEach(result::remove);
		result.putAll(super.getFieldValues());
		return Collections.unmodifiableMap(result);
	}

	@Override
	public IDependentDataMap getDependents() {
		return cachedDependentMap;
	}

	@Override
	public void setDependents(final IDependentDataMap mpDependents, final Set<Dependent<?>> apiCacheAlreadyLoadedDependents) {
		cachedDependentMap.set(mpDependents);
		setApiCacheAlreadyLoadedDependents(apiCacheAlreadyLoadedDependents);
	}

	@Override
	public Object getFieldValue(final UID field) {
		if (super.getFieldValues().containsKey(field) || ignoredCachedFields.contains(field)) {
			return super.getFieldValue(field);
		}
		return cachedValue.getFieldValue(field);
	}

	@Override
	public void removeFieldValue(final UID field) {
		ignoredCachedFields.add(field);
		super.removeFieldValue(field);
	}

	@Override
	public UID getFieldUid(final UID field) {
		if (super.getFieldUids().containsKey(field) || ignoredCachedFields.contains(field)) {
			return super.getFieldUid(field);
		}
		return cachedValue.getFieldUid(field);
	}

	@Override
	public void removeFieldUid(final UID field) {
		ignoredCachedFields.add(field);
		super.removeFieldUid(field);
	}

	@Override
	public Long getFieldId(final UID field) {
		if (super.getFieldIds().containsKey(field) || ignoredCachedFields.contains(field)) {
			return super.getFieldId(field);
		}
		return cachedValue.getFieldId(field);
	}

	@Override
	public void removeFieldId(final UID field) {
		ignoredCachedFields.add(field);
		super.removeFieldId(field);
	}

	@Override
	public EntityObjectVO<PK> copy() {
		return this.copy(true);
	}

	@Override
	public EntityObjectVO<PK> copyFlat() {
		return this.copy(false);
	}

	private CachedEntityObjectVO<PK> copy(boolean bWithDependents) {
		final CachedEntityObjectVO<PK> result;
		if (bWithDependents) {
			result = new CachedEntityObjectVO<>(cachedValue);
			result.cachedDependentMap.ignoredCachedKeys.addAll(this.cachedDependentMap.ignoredCachedKeys);
		} else {
			result = new CachedEntityObjectVO<>(cachedValue, null);
		}
		result.ignoredCachedFields.addAll(this.ignoredCachedFields);
		super.copyFlat(result);
		return result;
	}

	public EntityObjectVO<PK> mergeWithCached() {
		EntityObjectVO<PK> result = cachedValue.copyFlat();
		super.copyFlat(result);
		result.primaryKey = this.primaryKey;
		result.version = this.version;
		result.createdAt = this.createdAt;
		result.createdBy = this.createdBy;
		result.changedAt = this.changedAt;
		result.changedBy = this.changedBy;
		result.setDependents(cachedDependentMap.merge());
		return result;
	}

	/**
	 *
	 */
	private static class CachedDependentMap implements IDependentDataMap {

		private IDependentDataMap selfValue;
		private final IDependentDataMap cachedValue;
		private final Set<IDependentKey> ignoredCachedKeys = new HashSet<>(0);

		private CachedDependentMap(final IDependentDataMap selfDependents, final IDependentDataMap cachedDependents) {
			this.selfValue = selfDependents;
			this.cachedValue = new DependentDataMap();
			cachedDependents.getKeySet().forEach(
					k -> this.cachedValue.setData(k, cachedDependents.getData(k).stream()
							.map(CachedEntityObjectVO::new)
							.collect(Collectors.toList())));
		}

		private boolean cached(final FieldMeta<?> referencingField) {
			return !ignored(referencingField);
		}

		private boolean ignored(final FieldMeta<?> referencingField) {
			return ignored(DependentDataMap.createDependentKey(referencingField));
		}

		private boolean cached(final IDependentKey dependentKey) {
			return !ignored(dependentKey);
		}

		private boolean ignored(final IDependentKey dependentKey) {
			return ignoredCachedKeys.contains(dependentKey);
		}

		@Override
		public boolean hasData(final FieldMeta<?> referencingField) {
			return selfValue.hasData(referencingField) ||
					(cached(referencingField) && cachedValue.hasData(referencingField));
		}

		@Override
		public Collection<EntityObjectVO<?>> getData(final FieldMeta<?> referencingField) {
			if (ignored(referencingField)) {
				return selfValue.getData(referencingField);
			}
			return Stream.concat(selfValue.getData(referencingField).stream(),
							cachedValue.getData(referencingField).stream())
					.collect(Collectors.toList());
		}

		@Override
		public <PK> Collection<EntityObjectVO<PK>> getDataPk(final FieldMeta<?> referencingField) {
			if (ignored(referencingField)) {
				return selfValue.getDataPk(referencingField);
			}
			return RigidUtils.uncheckedCast(Stream.concat(selfValue.getDataPk(referencingField).stream(),
							cachedValue.getDataPk(referencingField).stream())
					.collect(Collectors.toList()));
		}

		@Override
		public <PK> void addData(final FieldMeta<?> referencingField, final EntityObjectVO<PK> eoDependent) {
			selfValue.addData(referencingField, eoDependent);
		}

		@Override
		public <PK> void addAllData(final FieldMeta<?> referencingField, final Collection<EntityObjectVO<PK>> colleoDependents) {
			selfValue.addAllData(referencingField, colleoDependents);
		}

		@Override
		public <PK> void setData(final FieldMeta<?> referencingField, final Collection<EntityObjectVO<PK>> colleoDependents) {
			ignoredCachedKeys.add(DependentDataMap.createDependentKey(referencingField));
			selfValue.setData(referencingField, colleoDependents);
		}

		@Override
		public void removeKey(final FieldMeta<?> referencingField) {
			selfValue.removeKey(referencingField);
		}

		@Override
		public boolean hasData(final IDependentKey dependentKey) {
			return selfValue.hasData(dependentKey) ||
					(cached(dependentKey) && cachedValue.hasData(dependentKey));
		}

		@Override
		public Collection<EntityObjectVO<?>> getData(final IDependentKey dependentKey, final Flag... flags) {
			if (ignored(dependentKey)) {
				return selfValue.getData(dependentKey, flags);
			}
			return Stream.concat(selfValue.getData(dependentKey, flags).stream(),
							cachedValue.getData(dependentKey, flags).stream())
					.collect(Collectors.toList());
		}

		@Override
		public <PK> Collection<EntityObjectVO<PK>> getDataPk(final IDependentKey dependentKey) {
			if (ignored(dependentKey)) {
				return selfValue.getDataPk(dependentKey);
			}
			return RigidUtils.uncheckedCast(Stream.concat(selfValue.getDataPk(dependentKey).stream(),
							cachedValue.getDataPk(dependentKey).stream())
					.collect(Collectors.toList()));
		}

		@Override
		public <PK> void addData(final IDependentKey dependentKey, final EntityObjectVO<PK> eoDependent) {
			selfValue.addData(dependentKey, eoDependent);
		}

		@Override
		public <PK> void addAllData(final IDependentKey dependentKey, final Collection<EntityObjectVO<PK>> colleoDependents) {
			selfValue.addAllData(dependentKey, colleoDependents);
		}

		@Override
		public <PK> void setData(final IDependentKey dependentKey, final Collection<EntityObjectVO<PK>> colleoDependents) {
			ignoredCachedKeys.add(dependentKey);
			selfValue.setData(dependentKey, colleoDependents);
		}

		@Override
		public void removeKey(final IDependentKey dependentKey) {
			ignoredCachedKeys.add(dependentKey);
			selfValue.removeKey(dependentKey);
		}

		public void set(IDependentDataMap mpDependents) {
			clear();
			if (mpDependents != null) {
				selfValue = mpDependents;
			}
		}

		@Override
		public Map<IDependentKey, List<EntityObjectVO<?>>> getRoDataMap() {
			Map<IDependentKey, List<EntityObjectVO<?>>> result = cachedValue.getRoDataMap().entrySet().stream()
					.filter(entry -> !ignoredCachedKeys.contains(entry.getKey()))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
			selfValue.getRoDataMap().forEach((k, v) -> {
				List<EntityObjectVO<?>> list = result.computeIfAbsent(k, k1 -> new ArrayList<>());
				list.addAll(v);
			});
			return Collections.unmodifiableMap(result);
		}

		@Override
		public void clear() {
			ignoredCachedKeys.addAll(cachedValue.getKeySet());
			selfValue.clear();
		}

		@Override
		public boolean isEmpty() {
			final boolean selfEmpty = selfValue.isEmpty();
			if (!selfEmpty) {
				return false;
			}
			if (cachedValue.getKeySet().stream()
					.filter(this::cached)
					.anyMatch(k -> cachedValue.hasData(k) && !cachedValue.getData(k).isEmpty())) {
				// at leased one cached dependent is not empty!
				return false;
			}
			return true;
		}

		@Override
		public Set<IDependentKey> getKeySet() {
			return Stream.concat(cachedValue.getKeySet().stream().filter(this::cached),
							selfValue.getKeySet().stream())
					.collect(Collectors.toSet());
		}

		@Override
		public Set<UID> getReferencingFieldUids() {
			return getKeySet().stream().map(IDependentKey::getDependentRefFieldUID)
					.collect(Collectors.toSet());
		}

		@Override
		public boolean areAllDependentsNew() {
			final boolean selfAllDependentsNew = selfValue.areAllDependentsNew();
			if (!selfAllDependentsNew) {
				return false;
			}
			if (cachedValue.getKeySet().stream()
					.filter(this::cached)
					.anyMatch(k -> cachedValue.getData(k).stream()
							.map(AbstractDalVOBasic::getPrimaryKey)
							.anyMatch(Objects::nonNull))) {
				// at leased one cached dependent has an entry with id!
				return false;
			}
			return true;
		}

		@Override
		public boolean getPendingChanges() {
			if (selfValue.getPendingChanges()) {
				return true;
			}
			if (cachedValue.getKeySet().stream()
					.filter(this::cached)
					.anyMatch(k -> cachedValue.getData(k).stream()
							.anyMatch(dep ->
									dep.isFlagNew() || dep.isFlagUpdated() || dep.isFlagRemoved() ||
									dep.getDependents().getPendingChanges()))) {
				// at leased one cached dependent (or sub-(sub-)dependent) is in pending state!
				return true;
			}
			return false;
		}

		private <PK> IDependentDataMap merge() {
			IDependentDataMap result = new DependentDataMap();
			getRoDataMap().entrySet().stream()
					.forEach(entry -> {
						final List<EntityObjectVO<PK>> mergedDeps = entry.getValue().parallelStream()
								.map(eo -> {
									if (eo instanceof CachedEntityObjectVO) {
										return ((CachedEntityObjectVO<PK>) eo).mergeWithCached();
									}
									return (EntityObjectVO<PK>)eo;
								})
								.collect(Collectors.toList());
						result.setData(entry.getKey(), mergedDeps);
					});
			return result;
		}
	}

	@SuppressWarnings("rawtypes")
	public static class CachedEntityObjectVOSerializer extends JsonSerializer<CachedEntityObjectVO> {
		@Override
		public void serializeWithType(final CachedEntityObjectVO value, final JsonGenerator jgen, final SerializerProvider prov, final TypeSerializer typeSer) throws IOException {
			this.serialize(value, jgen, prov);
		}
		@Override
		public void serialize(final CachedEntityObjectVO value, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
			prov.defaultSerializeValue(value.mergeWithCached(), jgen);
		}
	}
}
