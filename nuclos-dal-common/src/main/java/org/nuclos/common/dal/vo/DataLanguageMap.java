package org.nuclos.common.dal.vo;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.remoting.TypePreservingMapDeserializer;
import org.nuclos.remoting.TypePreservingMapSerializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class DataLanguageMap implements IDataLanguageMap {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6939548668431799515L;

	@JsonSerialize(using = TypePreservingMapSerializer.class)
	@JsonDeserialize(using = TypePreservingMapDeserializer.class)
	private Map<UID, IDataLanguageLocalizedEntityEntry> dataLanguageMap;
	
	public DataLanguageMap() {
		this.dataLanguageMap = new HashMap<UID, IDataLanguageLocalizedEntityEntry>();
	}

	@Override
	public Map<UID, IDataLanguageLocalizedEntityEntry> getLanguageMap() {
		return dataLanguageMap;
	}

	@Override
	public void setDataLanguage(UID DataLanguage,
								IDataLanguageLocalizedEntityEntry entity) {
		this.dataLanguageMap.put(DataLanguage, entity);
	}

	@Override
	public IDataLanguageLocalizedEntityEntry getDataLanguage(UID DataLanguage) {
		return this.dataLanguageMap.get(DataLanguage);
	}

	@Override
	public void clear() {
		if(this.dataLanguageMap != null) {
			this.dataLanguageMap.clear();
		}
	}

	@Override
	public IDataLanguageMap clone() {
		return this.copy(true);
	}
	
	@Override
	public IDataLanguageMap copy() {
		return this.copy(false);
	}
	
	private IDataLanguageMap copy(boolean isCloneWithKeys) {
		IDataLanguageMap retVal = new DataLanguageMap();
		
		for (UID language : this.getLanguageMap().keySet()) {
			IDataLanguageLocalizedEntityEntry dataLanguageLocalizedEntityEntry = this.getLanguageMap().get(language);
			if(dataLanguageLocalizedEntityEntry != null) {
				retVal.getLanguageMap().put(language, 
						isCloneWithKeys ? dataLanguageLocalizedEntityEntry.clone() : dataLanguageLocalizedEntityEntry.copy());										
			} else {
				retVal.getLanguageMap().put(language, null);
			}
		}
		
		return retVal;
	}

	 public boolean equals(Object obj) {
		 boolean retVal = true;
				 
		 if (obj != null && obj instanceof DataLanguageMap) {
			 DataLanguageMap toCompare = (DataLanguageMap) obj;
			 
			 for (UID lang : toCompare.getLanguageMap().keySet()) {
				 IDataLanguageLocalizedEntityEntry entry = toCompare.getLanguageMap().get(lang);
				 if (this.getDataLanguage(lang) == null) {
					 return false;
				 }
				 
				 for (Object fieldObj : entry.getFieldValues().keySet()) {
					 if (!this.getDataLanguage(lang).getFieldValue((UID) fieldObj).equals(
							 entry.getFieldValue((UID) fieldObj))) {
						 return false;
					 }
				 }
			 }
		 } else {
			 return false;
		 }
		 
		 return retVal;
	 }
	
}
