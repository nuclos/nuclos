package org.nuclos.common.dal.vo;

import java.util.Collection;
import java.util.Map;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;

public interface IDataLanguageLocalizedEntityEntry extends IDalWithFieldsVO, IVersionVO {
	String getValue(UID field);

	Map<UID, String> getValueMap();

	void setValue(UID field, String value);

	boolean isInitialValueSet(UID field);

	void setEntityEntryPrimaryKey(Long value);

	Long getEntityEntryPrimaryKey();

	void setLanguage(UID value);

	UID getLanguage();

	IDataLanguageLocalizedEntityEntry copy();

	IDataLanguageLocalizedEntityEntry clone();

	void resetInitialFlags(Collection<FieldMeta<?>> fields);

	UID getFieldEntityEntryUID();
}
