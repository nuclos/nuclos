package org.nuclos.common.dal.vo;

import java.io.Serializable;
import java.util.Map;

import org.nuclos.common.UID;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as = DataLanguageMap.class)
@JsonDeserialize(as = DataLanguageMap.class)
public interface IDataLanguageMap extends Serializable {

	Map<UID, IDataLanguageLocalizedEntityEntry> getLanguageMap();
	
	void setDataLanguage(UID DataLanguage, IDataLanguageLocalizedEntityEntry entity);

	IDataLanguageLocalizedEntityEntry getDataLanguage(UID DataLanguage);
	
	void clear();
	
	IDataLanguageMap copy();
	
	IDataLanguageMap clone();

}
