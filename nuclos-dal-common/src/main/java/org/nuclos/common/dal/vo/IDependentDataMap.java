//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.api.businessobject.Flag;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Interface to {@link org.nuclos.common.dal.vo.DependentDataMap}.
 * 
 * @since Nuclos 4.0
 */
@JsonSerialize(as = DependentDataMap.class, using = IDependentDataMap.IDependentDataMapJsonSerializer.class)
@JsonDeserialize(as = DependentDataMap.class, using = IDependentDataMap.IDependentDataMapJsonDeserializer.class)
public interface IDependentDataMap extends Serializable {
		
	// removed, to find compatibility problems in extension and nuclos...
	/*
	boolean hasData(UID referencingField);
	
	Collection<EntityObjectVO<?>> getData(UID referencingField);
	
	<PK> Collection<EntityObjectVO<PK>> getDataPk(UID referencingField);
	
	<PK> void addData(UID referencingField, EntityObjectVO<PK> eoDependent);

	<PK> void addAllData(UID referencingField, Collection<EntityObjectVO<PK>> colleoDependents);

	<PK> void setData(UID referencingField, Collection<EntityObjectVO<PK>> colleoDependents);
	
	void removeKey(UID referencingField);
	*/
	

	/**
	 * §postcondition result != null
	 * 
	 * @param referencingField
	 * @return the dependents belonging to the given referencingField, if any.
	 */
	boolean hasData(FieldMeta<?> referencingField);

	/**
	 * §postcondition result != null
	 * 
	 * @param referencingField
	 * @return the dependents belonging to the given referencingField, if any.
	 */
	Collection<EntityObjectVO<?>> getData(FieldMeta<?> referencingField);
	
	/**
	 * §postcondition result != null
	 * 
	 * @param referencingField
	 * @return the dependents belonging to the given referencingField, if any.
	 */
	<PK> Collection<EntityObjectVO<PK>> getDataPk(FieldMeta<?> referencingField);

	/**
	 * puts the given <code>EntityObjectVO</code> into this map.
	 * @param referencingField
	 * @param eoDependent
	 */
	<PK> void addData(FieldMeta<?> referencingField, EntityObjectVO<PK> eoDependent);

	/**
	 * adds all elements of <code>colleoDependents</code> to this map.
	 * Note that if the given <code>colleoDependents</code> is empty, nothing will be added.
	 * 
	 * §precondition colleoDependents != null
	 * §postcondition this.getData(referencingField).containsAll(colleoDependents)
	 * 
	 * @param referencingField
	 * @param colleoDependents
	 */
	<PK> void addAllData(FieldMeta<?> referencingField,
			Collection<EntityObjectVO<PK>> colleoDependents);

	/**
	 * §precondition colleoDependents != null
	 * §postcondition this.getData(referencingField).size() == colleoDependents.size()
	 * §postcondition this.getData(referencingField).containsAll(colleoDependents)
	 */
	<PK> void setData(FieldMeta<?> referencingField,
			Collection<EntityObjectVO<PK>> colleoDependents);

	void removeKey(FieldMeta<?> referencingField);
	
	
	
	
	
	
	/**
	 * §postcondition result != null
	 * 
	 * @param dependentKey
	 * @return the dependents belonging to the given dependentKey, if any.
	 */
	boolean hasData(IDependentKey dependentKey);

	/**
	 * Returns the dependents for the given dependentKey.
	 * If {@link Flag}s are given, the result is filtered by them.
	 *
	 * @param dependentKey
	 * @return the (possibly filtered) dependents belonging to the given dependentKey
	 */
	Collection<EntityObjectVO<?>> getData(IDependentKey dependentKey, Flag... flags);

	/**
	 * §postcondition result != null
	 * 
	 * @param dependentKey
	 * @return the dependents belonging to the given dependentKey, if any.
	 */
	<PK> Collection<EntityObjectVO<PK>> getDataPk(IDependentKey dependentKey);
	
	/**
	 * puts the given <code>EntityObjectVO</code> into this map.
	 * @param dependentKey
	 * @param eoDependent
	 */
	<PK> void addData(IDependentKey dependentKey, EntityObjectVO<PK> eoDependent);

	/**
	 * adds all elements of <code>colleoDependents</code> to this map.
	 * Note that if the given <code>colleoDependents</code> is empty, nothing will be added.
	 * 
	 * §precondition colleoDependents != null
	 * §postcondition this.getData(dependentKey).containsAll(colleoDependents)
	 * 
	 * @param dependentKey
	 * @param colleoDependents
	 */
	<PK> void addAllData(IDependentKey dependentKey, Collection<EntityObjectVO<PK>> colleoDependents);

	/**
	 * §precondition colleoDependents != null
	 * §postcondition this.getData(dependentKey).size() == colleoDependents.size()
	 * §postcondition this.getData(dependentKey).containsAll(colleoDependents)
	 * 
	 * @param dependentKey
	 * @param colleoDependents
	 */
	<PK> void setData(IDependentKey dependentKey, Collection<EntityObjectVO<PK>> colleoDependents);

	void removeKey(IDependentKey dependentKey);
	
	
	
	
	
	
	
	
	/**
	 * Get a read-only map of all data that is contained.
	 * 
	 * @return
	 */
	Map<IDependentKey, List<EntityObjectVO<?>>> getRoDataMap();
	
	/**
	 * remove all entries
	 */
	void clear();

	boolean isEmpty();
	
	Set<IDependentKey> getKeySet();

	/**
	 * @return the UID of referencing fields that this map contains values for.
	 */
	Set<UID> getReferencingFieldUids();

	/**
	 * @return Are all dependents new? That means: Do they all have <code>null</code> primary keys?
	 */
	boolean areAllDependentsNew();

	boolean getPendingChanges();

	class IDependentDataMapJsonSerializer extends JsonSerializer<IDependentDataMap> {
		@Override
		public void serialize(final IDependentDataMap map, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
			jgen.writeStartObject();
			for (IDependentKey key : map.getKeySet()) {
				jgen.writeArrayFieldStart(key.getDependentRefFieldUID().getString());
				for (EntityObjectVO<Object> eo : map.getDataPk(key)) {
					jgen.writeObject(eo);
				}
				jgen.writeEndArray();
			}
			jgen.writeEndObject();
		}
	}

	class IDependentDataMapJsonDeserializer extends JsonDeserializer<IDependentDataMap> {
		@Override
		public IDependentDataMap deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
			final DependentDataMap result = new DependentDataMap();
			final ObjectCodec codec = jpar.getCodec();
			final TreeNode depNode = codec.readTree(jpar);
			final Iterator<String> fieldNameIt = depNode.fieldNames();
			while (fieldNameIt.hasNext()) {
				final String fieldName = fieldNameIt.next();
				final DependentDataMap.DependentKey key = new DependentDataMap.DependentKey(UID.parseUID(fieldName));
				final TreeNode depListNode = depNode.get(fieldName);
				final int size = depListNode.size();
				for (int i = 0; i < size; i++) {
					final TreeNode entryNode = depListNode.get(i);
					final EntityObjectVO<?> eo = codec.treeToValue(entryNode, EntityObjectVO.class);
					result.addData(key, eo);
				}
			}
			return result;
		}
	}

}
