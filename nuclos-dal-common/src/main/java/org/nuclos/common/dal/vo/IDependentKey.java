package org.nuclos.common.dal.vo;

import java.io.IOException;
import java.io.Serializable;

import org.nuclos.common.UID;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Interface to {@link org.nuclos.common.dal.vo.DependentDataMap.DependenceKey}.
 * 
 * @since Nuclos 4.11
 */
@JsonSerialize(using = IDependentKey.IDependentKeyJsonSerializer.class, keyUsing = IDependentKey.IDependentKeyJsonSerializer.class)
@JsonDeserialize(using = IDependentKey.IDependentKeyJsonDeserializer.class, keyUsing = IDependentKey.IDependentKeyKeyDeserializer.class)
public interface IDependentKey extends Serializable {

	UID getDependentRefFieldUID();

	class IDependentKeyJsonSerializer extends JsonSerializer<IDependentKey> {
		@Override
		public void serialize(final IDependentKey key, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
			 jgen.writeString(key.getDependentRefFieldUID().getString());
		}
	}

	class IDependentKeyJsonDeserializer extends JsonDeserializer<IDependentKey> {
		@Override
		public IDependentKey deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
			return new DependentDataMap.DependentKey(UID.parseUID(jpar.getText()));
		}
	}

	class IDependentKeyKeyDeserializer extends KeyDeserializer {
		@Override
		public Object deserializeKey(final String key, final DeserializationContext ctx) throws IOException, JsonProcessingException {
			return new DependentDataMap.DependentKey(UID.parseUID(key));
		}
	}

}
