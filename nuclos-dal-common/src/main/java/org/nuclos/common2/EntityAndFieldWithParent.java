//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import java.util.Objects;

import org.nuclos.common.UID;

/**
 * Only a simple wrapper for the new Remoting interface
 */
public class EntityAndFieldWithParent {

	public EntityAndField entityAndField;
	public UID parent;

	public static EntityAndFieldWithParent build(EntityAndField entityAndField, UID parent) {
		EntityAndFieldWithParent result = new EntityAndFieldWithParent();
		result.entityAndField = entityAndField;
		result.parent = parent;
		return result;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final EntityAndFieldWithParent that = (EntityAndFieldWithParent) o;
		return Objects.equals(entityAndField, that.entityAndField) && Objects.equals(parent, that.parent);
	}

	@Override
	public int hashCode() {
		return Objects.hash(entityAndField, parent);
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("EntityAndFieldWithParent{");
		sb.append("entityAndField=").append(entityAndField);
		sb.append(", parent=").append(parent);
		sb.append('}');
		return sb.toString();
	}
}
