package org.nuclos.common2.jackson;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Advises the Jackson mapper to include type information (simple class name)
 * in a '_type' property of the resulting JSON object.
 */
@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		include = JsonTypeInfo.As.PROPERTY,
		property = "_type"
)
public abstract class MinimalTypeNameMixIn {
}