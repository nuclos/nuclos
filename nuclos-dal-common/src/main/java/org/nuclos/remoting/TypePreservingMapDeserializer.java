//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import static org.nuclos.remoting.TypePreservingMapSerializer.KEY_TYPE_ID;
import static org.nuclos.remoting.TypePreservingMapSerializer.NULL_VALUE;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Map;

import javax.json.JsonObject;

import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

/**
 * @see TypePreservingMapSerializer
 */
public class TypePreservingMapDeserializer extends JsonDeserializer<Map<?, ?>> {

	private static TypeIdResolver typeIdResolver = null;

	private static TypeIdResolver getTypeIdResolver() {
		if (typeIdResolver == null) {
			throw new RuntimeException("Missing TypeIdResolver!");
		}
		return typeIdResolver;
	}

	public static void setTypeIdResolver(final TypeIdResolver typeIdResolver) {
		TypePreservingMapDeserializer.typeIdResolver = typeIdResolver;
	}

	@Override
	public Map<?, ?> deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
		final Map<Object, Object> result;
		final ObjectCodec codec = jpar.getCodec();
		final TreeNode typeNode = codec.readTree(jpar);
		final Iterator<String> typeIt = typeNode.fieldNames();
		final String sMapTypeId = codec.treeToValue(typeNode.get(typeIt.next()), String.class);
		try {
			final Class<?> mapClass = typeIdResolver.typeFromId(ctx, sMapTypeId).getRawClass();
			final Constructor<?> ctor = mapClass.getDeclaredConstructor();
			ReflectionUtils.makeAccessible(ctor);
			result = (Map<Object, Object>) ctor.newInstance();
		} catch (IOException | NoSuchMethodException | InstantiationException
				 | IllegalAccessException | ClassCastException | InvocationTargetException e) {
			throw new JsonProcessingException(String.format("Deserialization of map %s not possible: %s", sMapTypeId, e.getMessage()), e) {
				private static final long serialVersionUID = 2435985987711244456L;
			};
		}
		KeyDeserializer keyDeserializer = null;
		boolean bConstructNullValues = false;
		while (typeIt.hasNext()) {
			final String sTypeId = typeIt.next();
			if (KEY_TYPE_ID.equals(sTypeId)) {
				final String sKeyTypeId = codec.treeToValue(typeNode.get(sTypeId), String.class);
				JavaType keyJavaType = typeIdResolver.typeFromId(ctx, sKeyTypeId);
				keyDeserializer = ctx.findKeyDeserializer(keyJavaType, null);
				continue;
			}
			if (keyDeserializer == null) {
				throw new JsonProcessingException(String.format("Missing key deserializer %s in the object to be deserialized", KEY_TYPE_ID)) {
					private static final long serialVersionUID = 2435985987711244456L;
				};
			}
			Class<?> typeClass;
			if (NULL_VALUE.equals(sTypeId)) {
				typeClass = null;
				bConstructNullValues = true;
			} else {
				bConstructNullValues = false;
				try {
					typeClass = typeIdResolver.typeFromId(ctx, sTypeId).getRawClass();
					// special handling for deserializing JSON from e.g. preferences
					if (JsonObject.class.isAssignableFrom(typeClass)) {
						typeClass = JsonObject.class;
					}
				} catch (IOException e) {
					throw new JsonProcessingException(String.format("Deserialization of objects of type %s not possible: %s", sTypeId, e.getMessage()), e) {
						private static final long serialVersionUID = 1533451276814787801L;
					};
				}
			}
			final TreeNode fieldMapNode = typeNode.get(sTypeId);
			final Iterator<String> fieldIt = fieldMapNode.fieldNames();
			while (fieldIt.hasNext()) {
				final String nextField = fieldIt.next();
				final Object key = keyDeserializer.deserializeKey(nextField, ctx);
				if (bConstructNullValues) {
					result.put(key, null);
				} else {
					final TreeNode valueNode = fieldMapNode.get(nextField);
					final Object value;
					if (Map.class.isAssignableFrom(typeClass) &&
							!JsonObject.class.isAssignableFrom(typeClass)) {
						final JsonParser traversedPar = valueNode.traverse(codec);
						traversedPar.nextToken();
						value = this.deserialize(traversedPar, ctx);
					} else {
						value = codec.treeToValue(valueNode, typeClass);
					}
					result.put(key, value);
				}
			}
		}
		return result;
	}
}
