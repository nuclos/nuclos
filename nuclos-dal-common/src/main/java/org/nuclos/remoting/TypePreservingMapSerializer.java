//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.json.JsonObject;

import org.springframework.util.ClassUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

/**
 *  Groups a value map with different value data types into a map of data types,
 *  with the respective values of that type and the correct key.
 *  Example:
 *  "mapField":
 *                 {
 *                     "8o640": "48Gu9JjrfgS9c6g5oy3d",
 *                     "8o6413": 3,
 *                     "8o641": 1648542358565,
 *                     "8o642": "Nuclet Import",
 *                     "8o64b": "V594_NUCLET_SOURCE_ACTIVATION",
 *                     "8o643": 1648542358565,
 *                     "8o64c": "Prozedur",
 *                     "8o644": "Nuclet Import",
 *                     "8o64d": 1,
 *                     "8o645": 3
 *                 }
 *  becomes
 *  "mapField":
 *                 {
 *                     "@keyType": "UID#",
 *                     "UID#":
 *                     {
 *                         "8o640": "48Gu9JjrfgS9c6g5oy3d"
 *                     },
 *                     "InternalTimestamp#":
 *                     {
 *                         "8o641": 1648542358565,
 *                         "8o643": 1648542358565
 *                     },
 *                     "String#":
 *                     {
 *                         "8o64b": "V594_NUCLET_SOURCE_ACTIVATION",
 *                         "8o642": "Nuclet Import",
 *                         "8o64c": "Prozedur",
 *                         "8o644": "Nuclet Import"
 *                     },
 *                     "Integer#":
 *                     {
 *                         "8o6413": 3,
 *                         "8o64d": 1,
 *                         "8o645": 3
 *                     }
 *                 }
 *
 *
 * See org.nuclos.common.dal.vo.AbstractDalVOWithFields#mapField for example usage:
 * 		<pre>
 * 		@JsonSerialize(using = TypePreservingMapSerializer.class)
 * 		@JsonDeserialize(using = TypePreservingMapDeserializer.class)
 * 		protected Map<UID, T> mapField = new HashMap<UID, T>();
 * 		</pre>
 */
public class TypePreservingMapSerializer extends JsonSerializer<Map<?, ?>> {

	static final String TYPE = "@type";

	static final String KEY_TYPE_ID = "@keyType";

	static final String NULL_VALUE = "null";

	private static TypeIdResolver _typeIdResolver = null;

	private static TypeIdResolver getTypeIdResolver() {
		if (_typeIdResolver == null) {
			throw new RuntimeException("Missing TypeIdResolver!");
		}
		return _typeIdResolver;
	}

	public static void setTypeIdResolver(final TypeIdResolver typeIdResolver) {
		TypePreservingMapSerializer._typeIdResolver = typeIdResolver;
	}

	@Override
	public void serialize(final Map<?, ?> map, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
		jgen.writeStartObject();
		String shortName = ClassUtils.getShortName(map.getClass());
		Class<?> mapClass = shortName.contains("Unmodifiable") ? HashMap.class :
							map.getClass();
		jgen.writeStringField(TYPE, getTypeIdResolver().idFromValueAndType(map, mapClass));
		if (!map.isEmpty()) {
			final Map<String, ? extends Map<?, ?>> splitByValueTypesMap = map.entrySet().stream()
					.collect(Collectors.groupingBy(
							entry -> entry.getValue() == null ? NULL_VALUE : getTypeIdResolver().idFromValue(entry.getValue()),
							Collectors.toMap(
									Entry::getKey,
									entry -> entry.getValue() == null ? NULL_VALUE : entry.getValue()
							)
					));
			jgen.writeFieldName(KEY_TYPE_ID);
			jgen.writeString(getTypeIdResolver().idFromValue(map.keySet().iterator().next()));
			for (String sTypeId : splitByValueTypesMap.keySet()) {
				jgen.writeFieldName(sTypeId);
				jgen.writeStartObject();
				for (Entry<?, ?> entry : splitByValueTypesMap.get(sTypeId).entrySet()) {
					// write key
					prov.findKeySerializer(entry.getKey().getClass(), null).serialize(entry.getKey(), jgen, prov);
					// write value
					if (entry.getValue() == NULL_VALUE) {
						jgen.writeNull();
					} else {
						if (Map.class.isAssignableFrom(entry.getValue().getClass()) &&
								!JsonObject.class.isAssignableFrom(entry.getValue().getClass())) {
							this.serialize((Map<?, ?>) entry.getValue(), jgen, prov);
						} else {
							jgen.writeObject(entry.getValue());
						}
					}
				}
				jgen.writeEndObject();
			}
		}
		jgen.writeEndObject();
	}

}
