//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import static org.nuclos.remoting.TypePreservingObjectSerializer.TYPE;
import static org.nuclos.remoting.TypePreservingObjectSerializer.VALUE;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

/**
 * @see TypePreservingObjectSerializer
 */
public class TypePreservingObjectDeserializer<T> extends JsonDeserializer<T> {

	private static TypeIdResolver _typeIdResolver = null;

	private static TypeIdResolver getTypeIdResolver() {
		if (_typeIdResolver == null) {
			throw new RuntimeException("Missing TypeIdResolver!");
		}
		return _typeIdResolver;
	}

	public static void setTypeIdResolver(final TypeIdResolver typeIdResolver) {
		TypePreservingObjectDeserializer._typeIdResolver = typeIdResolver;
	}

	@Override
	public T deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
		final ObjectCodec codec = jpar.getCodec();
		final TreeNode objectNode = codec.readTree(jpar);
		final String sTypeId =  codec.treeToValue(objectNode.get(TYPE), String.class);
		final Class<?> typeClass;
		try {
			typeClass = getTypeIdResolver().typeFromId(ctx, sTypeId).getRawClass();
		} catch (IOException e) {
			throw new MyJsonProcessingException(String.format("Deserialization of type %s not possible: %s", sTypeId, e.getMessage()), e);
		}
		return (T) codec.treeToValue(objectNode.get(VALUE), typeClass);
	}

	// needed for generic throw within deserialize above
	private static class MyJsonProcessingException extends JsonProcessingException {
		private static final long serialVersionUID = -2630578829458502637L;
		protected MyJsonProcessingException(final String msg, final Throwable rootCause) {
			super(msg, rootCause);
		}
	}
}
