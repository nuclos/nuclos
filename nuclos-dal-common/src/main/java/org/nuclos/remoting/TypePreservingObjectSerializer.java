//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;

/**
 *  Saves the type of an object in another property
 *  Example:
 *  "primaryKey": "48Gu9JjrfgS9c6g5oy3d"
 *  becomes
 *  "primaryKey":
 *                 {
 *                     "@type": "UID#",
 *                     "value": "48Gu9JjrfgS9c6g5oy3d"
 *                 }
 *
 * This is the only way for the deserializer to restore the correct value.
 *
 * See org.nuclos.common.dal.vo.AbstractDalVOBasic#primaryKey for example usage:
 * 		<pre>
 * 		@JsonSerialize(using = TypePreservingObjectSerializer.class)
 * 		@JsonDeserialize(using = TypePreservingObjectDeserializer.class)
 * 		private PK primaryKey;
 * 		</pre>
 */
public class TypePreservingObjectSerializer extends JsonSerializer<Object> {

	static final String TYPE = "@type";

	static final String VALUE = "value";

	private static TypeIdResolver _typeIdResolver = null;

	private static TypeIdResolver getTypeIdResolver() {
		if (_typeIdResolver == null) {
			throw new RuntimeException("Missing TypeIdResolver!");
		}
		return _typeIdResolver;
	}

	public static void setTypeIdResolver(final TypeIdResolver typeIdResolver) {
		TypePreservingObjectSerializer._typeIdResolver = typeIdResolver;
	}

	@Override
	public void serialize(final Object value, final JsonGenerator jgen, final SerializerProvider prov) throws IOException {
		jgen.writeStartObject();
		jgen.writeFieldName(TYPE);
		jgen.writeString(getTypeIdResolver().idFromValue(value));
		jgen.writeFieldName(VALUE);
		jgen.writeObject(value);
		jgen.writeEndObject();
	}

}
