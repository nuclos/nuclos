package org.nuclos.common.dal.vo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import java.util.Collections;
import java.util.Map;

import org.junit.Test;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.CachedEntityObjectVO;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;

public class CachedEntityObjectVOTest {

	@Test
	public void testToString() {
		final UID field1 = new UID("field1");
		final UID field2 = new UID("field2");
		EntityObjectVO<UID> eo1 = new EntityObjectVO<>(UID.UID_NULL);
		eo1.setFieldId(field1, 1000L);
		eo1.setFieldId(field2, 1000L);
		EntityObjectVO<UID> eo1Cached = new CachedEntityObjectVO<>(eo1);
		eo1Cached.setFieldId(field1, 2000L);
		assert eo1Cached.toString().contains("field1=2000");
		assert eo1Cached.toString().contains("field2=1000");
	}

	@Test
	public void testEditedBeforeCachedAttributes() {
		final UID field1 = new UID("field1");
		final UID field2 = new UID("field2");
		EntityObjectVO<UID> eo1 = new EntityObjectVO<>(UID.UID_NULL);
		eo1.setFieldId(field1, 1000L);
		eo1.setFieldId(field2, 1000L);

		EntityObjectVO<UID> eo1Cached = new CachedEntityObjectVO<>(eo1);
		eo1Cached.setFieldId(field1, 2000L);
		assertEquals ((Object)1000L, eo1.getFieldId(field1));
		assertEquals ((Object)2000L, eo1Cached.getFieldId(field1));

		final Map<UID, Long> cachedFieldIds = eo1Cached.getFieldIds();
		assertEquals ((Object)2000L, cachedFieldIds.get(field1));
		assertEquals ((Object)1000L, cachedFieldIds.get(field2));

		final EntityObjectVO<UID> eo1CachedCopied = eo1Cached.copyFlat();
		assertEquals ((Object)1000L, eo1.getFieldId(field1));
		assertEquals ((Object)2000L, eo1CachedCopied.getFieldId(field1));
		assertEquals ((Object)1000L, eo1CachedCopied.getFieldId(field2));

		eo1Cached.removeFieldId(field2);
		assertEquals ((Object)1000L, cachedFieldIds.get(field2));
		final Map<UID, Long> cachedFieldIds2 = eo1Cached.getFieldIds();
		assertNull (cachedFieldIds2.get(field2));
		assertNull (eo1Cached.getFieldId(field2));
		assertEquals ((Object)1000L, eo1CachedCopied.getFieldId(field2));
		eo1CachedCopied.removeFieldId(field2);
		assertNull (eo1CachedCopied.getFieldId(field2));
		assertEquals ((Object)1000L, eo1.getFieldId(field2));
	}

	@Test
	public void testEditedBeforeCachedDependents() {
		UID uidEntity1 = new UID();
		UID uidEntity2 = new UID();
		UID uidField1 = new UID();
		UID uidField2 = new UID();
		IDependentKey depKey = DependentDataMap.createDependentKey(new UID());
		EntityObjectVO<UID> eo1 = new EntityObjectVO<>(uidEntity1);
		EntityObjectVO<UID> dep1 = new EntityObjectVO<>(uidEntity2);
		UID pkDep1 = new UID();
		dep1.setPrimaryKey(pkDep1);
		dep1.setFieldValue(uidField1, "OriginValue");
		dep1.setFieldUid(uidField1, UID.parseUID("OriginValue"));
		EntityObjectVO<UID> dep2 = new EntityObjectVO<>(uidEntity2);
		UID pkDep2 = new UID();
		dep2.setPrimaryKey(pkDep2);
		eo1.getDependents().addData(depKey, dep1);

		EntityObjectVO<UID> eo1Cached = new CachedEntityObjectVO<>(eo1);
		eo1Cached.getDependents().addData(depKey, dep2);
		assertEquals ((Object)1, eo1.getDependents().getData(depKey).size());
		assertEquals ((Object)2, eo1Cached.getDependents().getData(depKey).size());
		final EntityObjectVO<?> dep1Cached = eo1Cached.getDependents().getData(depKey).stream().filter(eo -> pkDep1.equals(eo.getPrimaryKey())).findAny().orElseThrow(() -> new RuntimeException("dep1 not found"));
		assertNotSame (dep1Cached, dep1);
		dep1Cached.flagUpdate();
		assertSame ((Object)dep1.getFieldValue(uidField1), dep1Cached.getFieldValue(uidField1));
		assert dep1.isFlagUnchanged();
		assert dep1Cached.isFlagUpdated();

		dep1Cached.setFieldValue(uidField1, "ChangedValue");
		dep1Cached.setFieldUid(uidField1, UID.parseUID("ChangedValue"));
		assertEquals ((Object)"ChangedValue", dep1Cached.getFieldValue(uidField1));
		assertEquals ((Object)"ChangedValue", dep1Cached.getFieldUid(uidField1).getString());
		assertEquals ((Object)"OriginValue", dep1.getFieldValue(uidField1));
		assertEquals ((Object)"OriginValue", dep1.getFieldUid(uidField1).getString());
		assertSame (pkDep1, dep1.getPrimaryKey());
		assertSame (pkDep1, dep1Cached.getPrimaryKey());

		eo1Cached.getDependents().removeKey(depKey);
		assertEquals ((Object)1, eo1.getDependents().getData(depKey).size());
		assertEquals ((Object)0, eo1Cached.getDependents().getData(depKey).size());
		assert eo1.getDependents().hasData(depKey);
		assert !eo1Cached.getDependents().hasData(depKey);

		EntityObjectVO<UID> eo1Cached2 = new CachedEntityObjectVO<>(eo1);
		eo1Cached2.getDependents().addData(depKey, dep2);
		dep2.setFieldValue(uidField1, "OriginValue");
		assertEquals ((Object)1, eo1.getDependents().getData(depKey).size());
		assertEquals ((Object)2, eo1Cached2.getDependents().getData(depKey).size());

		eo1Cached2.getDependents().setData(depKey, Collections.singletonList(dep2));
		assertEquals ((Object)1, eo1Cached2.getDependents().getData(depKey).size());
		dep2.setFieldValue(uidField2, "new Value");
		final EntityObjectVO<?> dep2Cached2 = eo1Cached2.getDependents().getData(depKey).stream().filter(eo -> pkDep2.equals(eo.getPrimaryKey())).findAny().orElseThrow(() -> new RuntimeException("dep2 not found"));
		assertEquals ((Object)"new Value", dep2Cached2.getFieldValue(uidField2));
		assertEquals ((Object)"OriginValue", dep2Cached2.getFieldValue(uidField1));

		eo1.getDependents().addData(depKey, dep2);
		EntityObjectVO<UID> eo1Cached3 = new CachedEntityObjectVO<>(eo1);
		final EntityObjectVO<?> dep2Cached3 = eo1Cached3.getDependents().getData(depKey).stream().filter(eo -> pkDep2.equals(eo.getPrimaryKey())).findAny().orElseThrow(() -> new RuntimeException("dep2 not found"));
		IDependentDataMap eo1Cached3DepMap = new DependentDataMap();
		eo1Cached3DepMap.addData(depKey, dep2);
		eo1Cached3.setDependents(eo1Cached3DepMap, Collections.emptySet());
		assertNull (eo1.getApiCacheAlreadyLoadedDependents());
		assertNotNull (eo1Cached3.getApiCacheAlreadyLoadedDependents());
		assertEquals ((Object)2, eo1.getDependents().getData(depKey).size());
		assertEquals ((Object)1, eo1Cached3.getDependents().getData(depKey).size());
		eo1.getDependents().getData(depKey).stream().filter(eo -> pkDep1.equals(eo.getPrimaryKey())).findAny().orElseThrow(() -> new RuntimeException("dep1 not found"));
		final EntityObjectVO<?> dep2ACached3 = eo1Cached3.getDependents().getData(depKey).stream().filter(eo -> pkDep2.equals(eo.getPrimaryKey())).findAny().orElseThrow(() -> new RuntimeException("dep2 not found"));
		assertNotSame (dep2Cached3, dep2ACached3);
		dep2Cached3.setFieldValue(uidField2, null);
		assertNull (dep2Cached3.getFieldValue(uidField2));
		assertNotNull (dep2.getFieldValue(uidField2));
		assertNotNull (dep2ACached3.getFieldValue(uidField2));
	}

	@Test
	public void testMetas() {
		UID uidEntity1 = new UID();
		UID uidEntity2 = new UID();
		IDependentKey depKey = DependentDataMap.createDependentKey(new UID());
		EntityObjectVO<UID> eo1 = new EntityObjectVO<>(uidEntity1);
		EntityObjectVO<UID> dep1 = new EntityObjectVO<>(uidEntity2);
		UID pkDep1 = new UID();
		dep1.setPrimaryKey(pkDep1);
		EntityObjectVO<UID> dep2 = new EntityObjectVO<>(uidEntity2);
		UID pkDep2 = new UID();
		dep2.setPrimaryKey(pkDep2);

		EntityObjectVO<UID> eo1Cached = new CachedEntityObjectVO<>(eo1);
		eo1Cached.getDependents().addData(depKey, dep1);

		assert !eo1Cached.getDependents().isEmpty();
		assertEquals ((Object)1, eo1Cached.getDependents().getKeySet().size());
		assertEquals ((Object)1, eo1Cached.getDependents().getReferencingFieldUids().size());
		assert eo1.getDependents().isEmpty();
		assertEquals ((Object)0, eo1.getDependents().getKeySet().size());
		assertEquals ((Object)0, eo1.getDependents().getReferencingFieldUids().size());

		assert eo1.getDependents().areAllDependentsNew(); // no deps == all new!?
		eo1.getDependents().addData(depKey, dep2);
		assert !eo1.getDependents().areAllDependentsNew();
		//dep1.flagNew();
		dep2.setPrimaryKey(null);
		assert eo1.getDependents().areAllDependentsNew();
		assert !eo1Cached.getDependents().areAllDependentsNew();
		//dep2.flagNew();
		dep1.setPrimaryKey(null);
		assert eo1Cached.getDependents().areAllDependentsNew();

		dep1.flagUpdate();
		assert !eo1.getDependents().getPendingChanges();
		assert eo1Cached.getDependents().getPendingChanges();
		dep1.reset();
		assert !eo1.getDependents().getPendingChanges();
		assert !eo1Cached.getDependents().getPendingChanges();
		dep2.flagUpdate();
		assert eo1.getDependents().getPendingChanges();
		assert !eo1Cached.getDependents().getPendingChanges();
		dep1.flagUpdate();
		assert eo1.getDependents().getPendingChanges();
		assert eo1Cached.getDependents().getPendingChanges();
	}

	@Test
	public void testMerge() {
		UID uidEntity1 = new UID();
		UID uidEntity2 = new UID();
		UID uidField1 = new UID();
		UID uidField2 = new UID();
		IDependentKey depKey = DependentDataMap.createDependentKey(new UID());
		EntityObjectVO<UID> eo1 = new EntityObjectVO<>(uidEntity1);
		eo1.setFieldValue(uidField1, "OriginValue");
		eo1.setFieldValue(uidField2, "OriginValue");
		EntityObjectVO<UID> dep1 = new EntityObjectVO<>(uidEntity2);
		UID pkDep1 = new UID();
		dep1.setPrimaryKey(pkDep1);
		dep1.setFieldValue(uidField1, "OriginValue");
		dep1.setFieldUid(uidField1, UID.parseUID("OriginValue"));
		dep1.setFieldValue(uidField2, "OriginValue");
		EntityObjectVO<UID> dep2 = new EntityObjectVO<>(uidEntity2);
		UID pkDep2 = new UID();
		dep2.setPrimaryKey(pkDep2);
		eo1.getDependents().addData(depKey, dep1);

		CachedEntityObjectVO<UID> eo1Cached = new CachedEntityObjectVO<>(eo1);
		eo1Cached.setFieldValue(uidField1, "ChangedValue");
		final EntityObjectVO<?> dep1Cached = eo1Cached.getDependents().getData(depKey).stream().filter(eo -> pkDep1.equals(eo.getPrimaryKey())).findAny().orElseThrow(() -> new RuntimeException("dep1 not found"));
		dep1Cached.setFieldValue(uidField1, "ChangedValue");
		dep1Cached.setFieldUid(uidField1, UID.parseUID("ChangedValue"));
		eo1Cached.getDependents().addData(depKey, dep2);

		final EntityObjectVO<UID> eo1Merged = eo1Cached.mergeWithCached();
		final EntityObjectVO<?> dep1Merged = eo1Merged.getDependents().getData(depKey).stream().filter(eo -> pkDep1.equals(eo.getPrimaryKey())).findAny().orElseThrow(() -> new RuntimeException("dep1 not found"));
		final EntityObjectVO<?> dep2Merged = eo1Merged.getDependents().getData(depKey).stream().filter(eo -> pkDep2.equals(eo.getPrimaryKey())).findAny().orElseThrow(() -> new RuntimeException("dep2 not found"));
		assertSame (dep2Merged, dep2);
		assertEquals ((Object)"OriginValue", eo1.getFieldValue(uidField1));
		assertEquals ((Object)"OriginValue", eo1.getFieldValue(uidField2));
		assertEquals ((Object)"OriginValue", dep1.getFieldValue(uidField1));
		assertEquals ((Object)"OriginValue", dep1.getFieldValue(uidField2));
		assertEquals ((Object)"OriginValue", dep1.getFieldUid(uidField1).getString());
		assert !(eo1Merged instanceof CachedEntityObjectVO);
		assertEquals ((Object)"ChangedValue", eo1Merged.getFieldValue(uidField1));
		assertEquals ((Object)"OriginValue", eo1Merged.getFieldValue(uidField2));
		assertEquals ((Object)"ChangedValue", dep1Merged.getFieldValue(uidField1));
		assertEquals ((Object)"OriginValue", dep1Merged.getFieldValue(uidField2));
		assertEquals ((Object)"ChangedValue", dep1Merged.getFieldUid(uidField1).getString());
	}

}
