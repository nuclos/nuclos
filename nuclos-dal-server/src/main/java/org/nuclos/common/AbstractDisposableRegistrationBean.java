package org.nuclos.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import io.reactivex.rxjava3.disposables.Disposable;

public class AbstractDisposableRegistrationBean implements DisposableRegistrationBean {

	private final Collection<Disposable> synchronizedDisposableRegistration = Collections.synchronizedCollection(new ArrayList<>());

	@Override
	public Collection<Disposable> getDisposableRegistration() {
		return synchronizedDisposableRegistration;
	}
}
