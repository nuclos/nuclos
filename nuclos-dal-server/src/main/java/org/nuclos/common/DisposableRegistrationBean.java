//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.Collection;

import org.springframework.beans.factory.DisposableBean;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;

public interface DisposableRegistrationBean extends DisposableBean {

	Collection<Disposable> getDisposableRegistration();

	default <T> Disposable createSyncFlowableWithoutStrategyAndRegister(
			final @NonNull ObservableSource<T> source,
			final @NonNull Consumer<? super T> onNext) {
		return createFlowableAndRegister(source, BackpressureStrategy.MISSING, onNext, true);
	}

	default <T> Disposable createAsyncFlowableWithoutStrategyAndRegister(
			final @NonNull ObservableSource<T> source,
			final @NonNull Consumer<? super T> onNext) {
		return createFlowableAndRegister(source, BackpressureStrategy.MISSING, onNext, false);
	}

	default <T> Disposable createSyncFlowableWithBufferStrategyAndRegister(
			final @NonNull ObservableSource<T> source,
			final @NonNull Consumer<? super T> onNext) {
		return createFlowableAndRegister(source, BackpressureStrategy.BUFFER, onNext, true);
	}

	default <T> Disposable createAsyncFlowableWithBufferStrategyAndRegister(
			final @NonNull ObservableSource<T> source,
			final @NonNull Consumer<? super T> onNext) {
		return createFlowableAndRegister(source, BackpressureStrategy.BUFFER, onNext, false);
	}
	
	default <T> Disposable createSyncFlowableWithDropStrategyAndRegister(
			final @NonNull ObservableSource<T> source,
			final @NonNull Consumer<? super T> onNext) {
		return createFlowableAndRegister(source, BackpressureStrategy.DROP, onNext, true);
	}

	default <T> Disposable createAsyncFlowableWithDropStrategyAndRegister(
			final @NonNull ObservableSource<T> source,
			final @NonNull Consumer<? super T> onNext) {
		return createFlowableAndRegister(source, BackpressureStrategy.DROP, onNext, false);
	}

	default <T> Disposable createFlowableAndRegister(
			@NonNull ObservableSource<T> source,
			@NonNull BackpressureStrategy strategy,
			@NonNull Consumer<? super T> onNext,
			final boolean synchronize) {
		Flowable<T> flowable = Flowable.fromObservable(source, strategy);
		if (synchronize) {
			flowable = flowable.serialize();
		}
		return registerDisposable(flowable.subscribe(onNext));
	}

	default Disposable registerDisposable(Disposable disposable) {
		if (disposable != null) {
			getDisposableRegistration().add(disposable);
		}
		return disposable;
	}

	@Override
	default void destroy() throws Exception {
		getDisposableRegistration().removeIf(disposable -> {
			if (!disposable.isDisposed()) {
				disposable.dispose();
			}
			return true;
		});
	}

}
