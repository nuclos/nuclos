//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal;

import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class DalUtils {

	private static ThreadLocal<Boolean> disableVersionUpdates = new ThreadLocal<Boolean>() {
		protected Boolean initialValue() { return false; };
	};

	public static void disableVersionUpdates() {
		disableVersionUpdates.set(true);
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCompletion(int status) {
				disableVersionUpdates.set(false);
			}
		});
	}

	public static boolean isVersionUpdateDisabled() {
		return disableVersionUpdates.get();
	}

	public static Class<?> getDbType(Class<?> javaType) {
		if (javaType == ByteArrayCarrier.class || javaType == Object.class || javaType == NuclosImage.class) {
			javaType = byte[].class; // Column stores data (blob)
		} else if (DocumentFileBase.class.isAssignableFrom(javaType)) {
			return String.class; // Column stores filename
		} else if (javaType == DateTime.class) {
			return InternalTimestamp.class;
		} else if (javaType == NuclosPassword.class) {
			return NuclosPassword.class;
		}
		return javaType;
	}

}
