//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import javax.json.JsonObject;

import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.JsonUtils;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;

/**
 * Type parameter T is the java type
 */
abstract class AbstractColumnToVOMapping<T, PK> implements IColumnToVOMapping<T, PK> {

	private final String tableAlias;
	private final String column;
	private final UID field;
	private final Class<T> dataType;
	private final boolean isReadonly;
	private final boolean caseSensitive;

	AbstractColumnToVOMapping(String tableAlias, String column, UID field, String dataType, boolean isReadonly, boolean caseSensitive) throws ClassNotFoundException {
		this(tableAlias, column, field, (Class<T>) Class.forName(dataType), isReadonly, caseSensitive);
	}

	AbstractColumnToVOMapping(String tableAlias, String column, UID field, Class<T> dataType, boolean isReadonly, boolean caseSensitive) {
		if (tableAlias == null) throw new NullPointerException();
		if (column == null) throw new NullPointerException();
		if (field == null) throw new NullPointerException();
		this.tableAlias = tableAlias;
		this.column = column;
		this.field = field;
		this.dataType = dataType;
		this.isReadonly = isReadonly;
		this.caseSensitive = caseSensitive;
	}

	@Override
	public String getTableAlias() {
		return tableAlias;
	}

	@Override
	public final String getColumn() {
		return column;
	}

	@Override
	public UID getUID() {
		return field;
	}

	@Override
	public final boolean isCaseSensitive() {
		return caseSensitive;
	}

	@Override
	public final boolean isReadonly() {
		return isReadonly;
	}

	@Override
	public final Class<T> getDataType() {
		return dataType;
	}

	@Override
	public String getDbColumn() {
		return getColumn();
	}

	@Override
	public Class<T> getJavaClass() {
		return (Class<T>) DbUtils.getDbType(getDataType());
	}

	@Override
	public DbExpression<T> getDbColumn(DbFrom<PK> table, final IRigidMetaProvider metaProv) {
		if (isCaseSensitive()) {
			return table.columnCaseSensitive(getTableAlias(), this);
		}
		else {
			return table.column(getTableAlias(), this);
		}
	}

	<S> S convertFromDbValue(Object value, String column, final Class<S> dataType, final PK recordPk, final FromDbValueConversionParams params, final IDbValueConverter converter) {
		if (dataType == JsonObject.class) {
			if (value == null) {
				return null;
			} else if (value instanceof JsonObject) {
				return (S) value;
			} else if (value instanceof String) {
				return (S) JsonUtils.stringToObject((String) value);
			} else {
				throw new IllegalArgumentException("Value " + value + " could not be converted to " + JsonObject.class.getCanonicalName());
			}

		} else {
			if (converter != null) {
				final S result = converter.convertFromDbValue(value, column, dataType, recordPk, params);
				if (result != null) {
					return result;
				}
			}

			return dataType.cast(value);
		}
	}

	static Object convertToDbValue(Class<?> javaType, Object value, IDbValueConverter converter) {
		if (value == null) {
			return DbNull.forType(DalUtils.getDbType(javaType));
		} else if (value instanceof ByteArrayCarrier) {
			return ((ByteArrayCarrier) value).getData();
		} else if (value instanceof org.nuclos.api.NuclosImage) {
			org.nuclos.api.NuclosImage ni = (org.nuclos.api.NuclosImage) value;
			if (ni.getContent() != null) {
				ByteArrayCarrier bac = new ByteArrayCarrier(ni.getContent());
				return bac.getData();
			} else {
				return DbNull.forType(DalUtils.getDbType(javaType));
			}
		} else if (value instanceof DateTime) {
			return new InternalTimestamp(((DateTime) value).getTime());
		} else if (value instanceof JsonObject) {
			return JsonUtils.objectToString((JsonObject) value);
		} else {
			if (converter != null) {
				final Object result = converter.convertToDbValue(javaType, value);
				if (result != null) {
					return result;
				}
			}
			return value;
		}
	}

	String generateString(String fieldName, Object field) {
		return getClass().getName() + "["
				+ "col=" + getColumn()
				+ ", tableAlias=" + getTableAlias()
				+ ", " + fieldName + "=" + field
				+ (getDataType() != null ? (", type=" + getDataType().getName()) : "")
				+ "]";
	}
}
