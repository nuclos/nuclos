//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.nuclos.common.AbstractDisposableRegistrationBean;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.cluster.ClusterMessageType;
import org.nuclos.server.cluster.RigidClusterHelper;
import org.nuclos.server.dal.specification.IDalReadSpecification;
import org.openjdk.jol.info.GraphLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.reactivex.rxjava3.subjects.PublishSubject;

@Configurable
public abstract class AbstractDalProcessor<DalVO extends IDalVO<PK>, PK> extends AbstractDisposableRegistrationBean
		implements IDalReadSpecification<DalVO, PK> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractDalProcessor.class);

	private static final ObjectMapper objectMapper = new ObjectMapper();

	private final UID entity;
	
	private final Class<DalVO> dalVOClzz;
	
	private final Class<PK> pkClzz;

	/**
	 * Data Types
	 */
	public static final Class<Boolean>	        DT_BOOLEAN	         = java.lang.Boolean.class;

	private final ThreadLocal<DalChangeTransactionSynchronization<?, ?>> transactionSynchronizations = new ThreadLocal<>();
	private final ThreadLocal<DalChangeTransactionSynchronization<?, ?>> transactionSynchronizationsClusterWide = new ThreadLocal<>();

	private enum ObservableType {
		AFTER_COMMIT,
		BEFORE_COMMIT,
		ROLLBACK_ONLY,
		ALL_CHANGES_IMMEDIATELY,
		INSERT_ONLY_IMMEDIATELY,
		UPDATE_ONLY_IMMEDIATELY,
		DELETE_ONLY_IMMEDIATELY}

	private static final ConcurrentMap<Pair<UID, ObservableType>, PublishSubject<DalChangeNotification<?, ?>>>
		OBSERVABLES = new ConcurrentHashMap<>();
	private final static ConcurrentMap<UID, Boolean> CLUSTER_WIDE_NOTIFICATIONS = new ConcurrentHashMap<>();

	private final PublishSubject<DalChangeNotificationForTransactionSync<PK>> observableAfterCommit;
	private final PublishSubject<DalChangeNotificationForTransactionSync<PK>> observableBeforeCommit;
	private final PublishSubject<DalChangeNotification<DalVO, PK>> observableRollbackOnly;
	private final PublishSubject<DalChangeNotification<DalVO, PK>> observableAllChangesImmediately;
	private final PublishSubject<DalChangeNotification<DalVO, PK>> observableInsertOnlyImmediately;
	private final PublishSubject<DalChangeNotification<DalVO, PK>> observableUpdateOnlyImmediately;
	private final PublishSubject<DalChangeNotification<DalVO, PK>> observableDeleteOnlyImmediately;

	private final static ConcurrentMap<UID, List<WeakReference<? extends AbstractDalProcessor<?, ?>>>> PROCESSORS_BY_ENTITY = new ConcurrentHashMap<>();

	private final ConcurrentMap<Integer, CacheMap<PK, DalVO>> cache = new ConcurrentHashMap<>();

	private CachedObjectBuilder<DalVO, PK> cachedObjectBuilder;

	protected AbstractDalProcessor(UID entity, Class<DalVO> type, Class<PK> pkType) {
		this.entity = entity;
		this.dalVOClzz = type;
		pkClzz = pkType;
		observableAfterCommit = RigidUtils.uncheckedCast(OBSERVABLES.computeIfAbsent(Pair.makePair(entity, ObservableType.AFTER_COMMIT), k -> PublishSubject.create()));
		observableBeforeCommit = RigidUtils.uncheckedCast(OBSERVABLES.computeIfAbsent(Pair.makePair(entity, ObservableType.BEFORE_COMMIT), k -> PublishSubject.create()));
		observableRollbackOnly = RigidUtils.uncheckedCast(OBSERVABLES.computeIfAbsent(Pair.makePair(entity, ObservableType.ROLLBACK_ONLY), k -> PublishSubject.create()));
		observableAllChangesImmediately = RigidUtils.uncheckedCast(OBSERVABLES.computeIfAbsent(Pair.makePair(entity, ObservableType.ALL_CHANGES_IMMEDIATELY), k -> PublishSubject.create()));
		observableInsertOnlyImmediately = RigidUtils.uncheckedCast(OBSERVABLES.computeIfAbsent(Pair.makePair(entity, ObservableType.INSERT_ONLY_IMMEDIATELY), k -> PublishSubject.create()));
		observableUpdateOnlyImmediately = RigidUtils.uncheckedCast(OBSERVABLES.computeIfAbsent(Pair.makePair(entity, ObservableType.UPDATE_ONLY_IMMEDIATELY), k -> PublishSubject.create()));
		observableDeleteOnlyImmediately = RigidUtils.uncheckedCast(OBSERVABLES.computeIfAbsent(Pair.makePair(entity, ObservableType.DELETE_ONLY_IMMEDIATELY), k -> PublishSubject.create()));
		createSyncFlowableWithBufferStrategyAndRegister(observeChangesNotifyRollbackOnly(), this::rollbackChanges);
		PROCESSORS_BY_ENTITY.computeIfAbsent(entity, k -> Collections.synchronizedList(new ArrayList<>())).add(new WeakReference<>(this, null));
	}

	@SuppressWarnings("unchecked")
	private <Proc extends AbstractDalProcessor<?,?>> List<Proc> getLinkedProcessors() {
		final List<Proc> result = new ArrayList<>();
		final AtomicBoolean bFoundNull = new AtomicBoolean(false);
		PROCESSORS_BY_ENTITY.computeIfAbsent(entity, k -> Collections.synchronizedList(new ArrayList<>()))
				.forEach(weakProc -> {
					final AbstractDalProcessor<?, ?> proc = weakProc.get();
					if (proc == null) {
						bFoundNull.set(true);
					} else if (proc != AbstractDalProcessor.this) {
						result.add((Proc) proc);
					}
				});
		if (bFoundNull.get()) {
			PROCESSORS_BY_ENTITY.get(entity).removeIf(ref -> ref.get() == null);
		}
		return result;
	}

	protected void setCachedObjectBuilder(final CachedObjectBuilder<DalVO, PK> cachedObjectBuilder) {
		this.cachedObjectBuilder = cachedObjectBuilder;
	}

	protected boolean isClusterWideNotification() {
		return isCached() || Optional
				.ofNullable(CLUSTER_WIDE_NOTIFICATIONS.get(getEntityUID()))
				.orElse(false);
	}

	protected final boolean isCached() {
		return cachedObjectBuilder != null;
	}

	protected Integer getDefaultCache() {
		return 0;
	}

	protected DalVO getByPrimaryKey(final Integer iCache, final PK pk) {
		if (isCached() && iCache != null && pk != null) {
			return getCache(iCache).get(pk);
		}
		return null;
	}

	protected List<DalVO> getByPrimaryKeys(final Integer iCache, final List<PK> ids) {
		if (isCached() && iCache != null && ids != null) {
			final CacheMap<PK, DalVO> cache = getCache(iCache);
			return ids.parallelStream()
					.map(cache::get)
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
		}
		return null;
	}

	protected List<DalVO> getAll(final Integer iCache) {
		if (isCached() && iCache != null) {
			return getCache(iCache).getAllCached();
		}
		return null;
	}

	protected DalVO storeInCache(final Integer iCache, final DalVO dalVO) {
		if (isCached() && iCache != null && dalVO != null && dalVO.getPrimaryKey() != null) {
			return getCache(iCache).putDalVO(dalVO);
		}
		return dalVO;
	}

	protected List<DalVO> storeInCache(final Integer iCache, final List<DalVO> dalVOList) {
		if (isCached() && iCache != null && dalVOList != null) {
			final CacheMap<PK, DalVO> cache = getCache(iCache);
			return dalVOList.stream().map(cache::putDalVO).collect(Collectors.toList());
		}
		return dalVOList;
	}

	protected List<DalVO> storeAllInCache(final Integer iCache, final List<DalVO> allDalVO) {
		if (isCached() && iCache != null && allDalVO != null) {
			return getCache(iCache).fillWithAllCached(allDalVO);
		}
		return allDalVO;
	}

	private void removeFromAllCaches(final PK pk) {
		if (isCached() && pk != null) {
			cache.values().parallelStream()
					.forEach(cache -> cache.removePK(pk));
		}
	}

	protected void clearAllInCache(final Integer iCache) {
		if (isCached() && iCache != null) {
			getCache(iCache).clearAllCachedOnly();
		}
	}

	private void clearAllCachedOnlyInAllCaches() {
		if (isCached()) {
			cache.values().parallelStream()
					.forEach(CacheMap::clearAllCachedOnly);
		}
	}

	private void clearAllCaches() {
		if (isCached()) {
			cache.values().parallelStream()
					.forEach(CacheMap::clear);
		}
	}

	private CacheMap<PK, DalVO> getCache(final Integer iCache) {
		return cache.computeIfAbsent(iCache, k -> new CacheMap<>(getEntityUID(), cachedObjectBuilder));
	}

	protected DalChangeTransactionSynchronization<DalVO, PK> getTransactionSynchronizationForCurrentThread(final boolean bNotifyCluster) {
		DalChangeTransactionSynchronization<DalVO, PK> result = RigidUtils.uncheckedCast(bNotifyCluster ?
				transactionSynchronizationsClusterWide.get() : transactionSynchronizations.get());
		if (result == null) {
			result = new DalChangeTransactionSynchronization<>(this, bNotifyCluster);
			if (bNotifyCluster) {
				transactionSynchronizationsClusterWide.set(result);
			} else {
				transactionSynchronizations.set(result);
			}
		}
		TransactionSynchronizationManager.registerSynchronization(result);
		return result;
	}

	public void handleIncomingClusterNotification(final DalChangeNotificationForTransactionSync<PK> notificationFromCluster) {
		if (notificationFromCluster != null) {
			if (notificationFromCluster.hasUnidentifiedInserts()) {
				notifyUnidentifiedInsert(false);
			}
			if (notificationFromCluster.getInserted() != null) {
				notificationFromCluster.getInserted()
						.forEach(pk -> notifyInsert(getByPrimaryKey(pk), false));
			}
			if (notificationFromCluster.hasUnidentifiedUpdates()) {
				notifyUnidentifiedUpdate(false);
			}
			if (notificationFromCluster.getUpdated() != null) {
				notificationFromCluster.getUpdated()
						.forEach(pk -> notifyUpdate(getByPrimaryKey(pk), false));
			}
			if (notificationFromCluster.hasUnidentifiedDeletes()) {
				notifyUnidentifiedDelete(false);
			}
			if (notificationFromCluster.getDeleted() != null) {
				notificationFromCluster.getDeleted()
						.forEach(pk -> notifyDelete(new Delete<>(pk), false));
			}
		}
	}

	private void rollbackChanges(final DalChangeNotification<DalVO, PK> notification) {
		if (notification != null) {
			if (!notification.isRollbackOnly()) {
				throw new IllegalArgumentException("Only notifications marked for rollback can be rolled back!");
			}
			boolean bClear = false;
			boolean bClearAllCachedOnly = false;
			if (notification.hasUnidentifiedInserts()) {
				bClear = true;
			}
			if (notification.getInserted() != null && !bClear) {
				notification.getInserted().forEach(this::removeFromAllCaches);
				getLinkedProcessors().forEach(proc -> notification.getInserted().forEach(pk -> proc.removeFromAllCaches(RigidUtils.uncheckedCast(pk))));
			}
			if (notification.hasUnidentifiedUpdates()) {
				bClear = true;
			}
			if (notification.getUpdated() != null && !bClear) {
				clearAllCachedOnlyInAllCaches();
				notification.getUpdated().forEach(this::removeFromAllCaches);
				getLinkedProcessors().forEach(proc -> {
					proc.clearAllCachedOnlyInAllCaches();
					notification.getUpdated().forEach(pk -> {
						proc.removeFromAllCaches(RigidUtils.uncheckedCast(pk));
					});
				});
			}
			if (notification.hasDeletes() && !bClear) {
				bClearAllCachedOnly = true;
			}
			if (bClear) {
				clearAllCaches();
				getLinkedProcessors().forEach(AbstractDalProcessor::clearAllCaches);
			} else if (bClearAllCachedOnly) {
				clearAllCachedOnlyInAllCaches();
				getLinkedProcessors().forEach(AbstractDalProcessor::clearAllCachedOnlyInAllCaches);
			}
			if (observableAllChangesImmediately.hasObservers()) {
				observableAllChangesImmediately.onNext(notification);
			}
			if (observableInsertOnlyImmediately.hasObservers()) {
				observableInsertOnlyImmediately.onNext(notification);
			}
			if (observableUpdateOnlyImmediately.hasObservers()) {
				observableUpdateOnlyImmediately.onNext(notification);
			}
			if (observableDeleteOnlyImmediately.hasObservers()) {
				observableDeleteOnlyImmediately.onNext(notification);
			}
			if (observableBeforeCommit.hasObservers()) {
				observableBeforeCommit.onNext(notification);
			}
		}
	}

	public void notifyUnidentifiedInsert(final boolean bNotifyCluster) {
		clearAllCachedOnlyInAllCaches();
		getLinkedProcessors().forEach(AbstractDalProcessor::clearAllCachedOnlyInAllCaches);
		if (observableAllChangesImmediately.hasObservers()) {
			observableAllChangesImmediately.onNext(DalChangeNotification.withUnidentifiedInserts(getEntityUID(), UID.class.isAssignableFrom(getPkType())));
		}
		if (observableInsertOnlyImmediately.hasObservers()) {
			observableInsertOnlyImmediately.onNext(DalChangeNotification.withUnidentifiedInserts(getEntityUID(), UID.class.isAssignableFrom(getPkType())));
		}
		if (TransactionSynchronizationManager.isSynchronizationActive()) {
			getTransactionSynchronizationForCurrentThread(bNotifyCluster).setUnidentifiedInserts();
		} else {
			// Fallback
			final DalChangeTransactionSynchronization<DalVO, PK> ts = new DalChangeTransactionSynchronization<>(this, bNotifyCluster);
			ts.setUnidentifiedInserts();
			ts.afterCompletion(TransactionSynchronization.STATUS_COMMITTED);
		}
	}

	protected void notifyInsert(DalVO dalVO, final boolean bNotifyCluster) {
		if (dalVO == null) {
			return;
		}
		final PK pk = dalVO.getPrimaryKey();
		if (pk == null) {
			return;
		}
		if (isCached()) {
			// ... clear 'allList'
			cache.keySet().parallelStream()
					.forEach(this::clearAllInCache);
		}
		getLinkedProcessors().forEach(AbstractDalProcessor::clearAllCachedOnlyInAllCaches);
		if (observableAllChangesImmediately.hasObservers()) {
			observableAllChangesImmediately.onNext(DalChangeNotification.withSingleInsert(dalVO));
		}
		if (observableInsertOnlyImmediately.hasObservers()) {
			observableInsertOnlyImmediately.onNext(DalChangeNotification.withSingleInsert(dalVO));
		}
		if (TransactionSynchronizationManager.isSynchronizationActive()) {
			getTransactionSynchronizationForCurrentThread(bNotifyCluster).addInserted(pk);
		} else {
			// Fallback
			final DalChangeTransactionSynchronization<DalVO, PK> ts = new DalChangeTransactionSynchronization<>(this, bNotifyCluster);
			ts.addInserted(pk);
			ts.afterCompletion(TransactionSynchronization.STATUS_COMMITTED);
		}
	}

	public void notifyUnidentifiedUpdate(final boolean bNotifyCluster) {
		clearAllCaches();
		getLinkedProcessors().forEach(AbstractDalProcessor::clearAllCaches);
		if (observableAllChangesImmediately.hasObservers()) {
			observableAllChangesImmediately.onNext(DalChangeNotification.withUnidentifiedUpdates(getEntityUID(), UID.class.isAssignableFrom(getPkType())));
		}
		if (observableInsertOnlyImmediately.hasObservers()) {
			observableInsertOnlyImmediately.onNext(DalChangeNotification.withUnidentifiedUpdates(getEntityUID(), UID.class.isAssignableFrom(getPkType())));
		}
		if (TransactionSynchronizationManager.isSynchronizationActive()) {
			getTransactionSynchronizationForCurrentThread(bNotifyCluster).setUnidentifiedUpdates();
		} else {
			// Fallback
			final DalChangeTransactionSynchronization<DalVO, PK> ts = new DalChangeTransactionSynchronization<>(this, bNotifyCluster);
			ts.setUnidentifiedUpdates();
			ts.afterCompletion(TransactionSynchronization.STATUS_COMMITTED);
		}
	}

	protected void notifyUpdate(DalVO dalVO, final boolean bNotifyCluster) {
		if (dalVO == null) {
			return;
		}
		final PK pk = dalVO.getPrimaryKey();
		if (pk == null) {
			return;
		}
		if (isCached()) {
			// ... clear 'allList' and remove
			cache.keySet().parallelStream()
					.forEach(iCache -> {
						final CacheMap<PK, DalVO> cache = getCache(iCache);
						cache.clearAllCachedOnly();
						cache.removePK(pk);
					});
		}
		getLinkedProcessors().forEach(proc -> {
			proc.clearAllCachedOnlyInAllCaches();
			proc.removeFromAllCaches(RigidUtils.uncheckedCast(pk));
		});
		if (observableAllChangesImmediately.hasObservers()) {
			observableAllChangesImmediately.onNext(DalChangeNotification.withSingleUpdate(dalVO));
		}
		if (observableUpdateOnlyImmediately.hasObservers()) {
			observableUpdateOnlyImmediately.onNext(DalChangeNotification.withSingleUpdate(dalVO));
		}
		if (TransactionSynchronizationManager.isSynchronizationActive()) {
		getTransactionSynchronizationForCurrentThread(bNotifyCluster).addUpdated(pk);
		} else {
			// Fallback
			final DalChangeTransactionSynchronization<DalVO, PK> ts = new DalChangeTransactionSynchronization<>(this, bNotifyCluster);
			ts.addUpdated(pk);
			ts.afterCompletion(TransactionSynchronization.STATUS_COMMITTED);
		}
	}

	public void notifyUnidentifiedDelete(final boolean bNotifyCluster) {
		clearAllCaches();
		getLinkedProcessors().forEach(AbstractDalProcessor::clearAllCaches);
		if (observableAllChangesImmediately.hasObservers()) {
			observableAllChangesImmediately.onNext(DalChangeNotification.withUnidentifiedDeletes(getEntityUID(), UID.class.isAssignableFrom(getPkType())));
		}
		if (observableInsertOnlyImmediately.hasObservers()) {
			observableInsertOnlyImmediately.onNext(DalChangeNotification.withUnidentifiedDeletes(getEntityUID(), UID.class.isAssignableFrom(getPkType())));
		}
		if (TransactionSynchronizationManager.isSynchronizationActive()) {
			getTransactionSynchronizationForCurrentThread(bNotifyCluster).setUnidentifiedDeletes();
		} else {
			// Fallback
			final DalChangeTransactionSynchronization<DalVO, PK> ts = new DalChangeTransactionSynchronization<>(this, bNotifyCluster);
			ts.setUnidentifiedDeletes();
			ts.afterCompletion(TransactionSynchronization.STATUS_COMMITTED);
		}
	}

	protected void notifyDelete(final Delete<PK> del, final boolean bNotifyCluster) {
		if (del == null || del.getPrimaryKey() == null) {
			return;
		}
		removeFromAllCaches(del.getPrimaryKey());
		getLinkedProcessors().forEach(proc -> proc.removeFromAllCaches(RigidUtils.uncheckedCast(del.getPrimaryKey())));
		if (observableAllChangesImmediately.hasObservers()) {
			observableAllChangesImmediately.onNext(DalChangeNotification.withSingleDelete(del, UID.class.isAssignableFrom(getPkType())));
		}
		if (observableDeleteOnlyImmediately.hasObservers()) {
			observableDeleteOnlyImmediately.onNext(DalChangeNotification.withSingleDelete(del, UID.class.isAssignableFrom(getPkType())));
		}
		if (TransactionSynchronizationManager.isSynchronizationActive()) {
			getTransactionSynchronizationForCurrentThread(bNotifyCluster).addDeleted(del.getPrimaryKey());
		} else {
			// Fallback
			final DalChangeTransactionSynchronization<DalVO, PK> ts = new DalChangeTransactionSynchronization<>(this, bNotifyCluster);
			ts.addDeleted(del.getPrimaryKey());
			ts.afterCompletion(TransactionSynchronization.STATUS_COMMITTED);
		}
	}

	public PublishSubject<DalChangeNotification<DalVO, PK>> observeAllChangesImmediately(boolean bNotifyClusterWide) {
		enableClusterWideNotification(bNotifyClusterWide);
		return observableAllChangesImmediately;
	}

	public PublishSubject<DalChangeNotification<DalVO, PK>> observeInsertOnlyImmediately(boolean bNotifyClusterWide) {
		enableClusterWideNotification(bNotifyClusterWide);
		return observableInsertOnlyImmediately;
	}

	public PublishSubject<DalChangeNotification<DalVO, PK>> observeUpdateOnlyImmediately(boolean bNotifyClusterWide) {
		enableClusterWideNotification(bNotifyClusterWide);
		return observableUpdateOnlyImmediately;
	}

	public PublishSubject<DalChangeNotification<DalVO, PK>> observeDeleteOnlyImmediately(boolean bNotifyClusterWide) {
		enableClusterWideNotification(bNotifyClusterWide);
		return observableDeleteOnlyImmediately;
	}

	public PublishSubject<DalChangeNotificationForTransactionSync<PK>> observeChangesNotifyBeforeCommit(boolean bNotifyClusterWide) {
		enableClusterWideNotification(bNotifyClusterWide);
		return observableBeforeCommit;
	}

	public PublishSubject<DalChangeNotificationForTransactionSync<PK>> observeChangesNotifyAfterCommit(boolean bNotifyClusterWide) {
		enableClusterWideNotification(bNotifyClusterWide);
		return observableAfterCommit;
	}

	public PublishSubject<DalChangeNotification<DalVO, PK>> observeChangesNotifyRollbackOnly() {
		return observableRollbackOnly;
	}

	private void enableClusterWideNotification(boolean bNotifyClusterWide) {
		if (bNotifyClusterWide) {
			CLUSTER_WIDE_NOTIFICATIONS.putIfAbsent(getEntityUID(), true);
		}
	}
	
	public UID getEntityUID() {
		return entity;
	}

	public final String getProcessor() {
		Class<?>[] interfaces = getClass().getInterfaces();
		if (interfaces.length == 0)
			return "<[none]>";
		return interfaces[0].getName();
	}

	public final Class<DalVO> getDalType() {
		return dalVOClzz;
	}
	
	public final Class<PK> getPkType() {
		return pkClzz;
	}

	protected DalVO newDalVOInstance() {
		try {
			DalVO newInstance = getDalType().getConstructor(UID.class).newInstance(getEntityUID());
			return newInstance;
		} catch (Exception e) {
			throw new CommonFatalException(e);
		}
	}

	/**
	 * Free the memory / clear caches
	 * @param includeLinkedProcessors
	 * 			Free also all other processors for this entity, e.g. the Extended
	 */
	@Override
	public void freeMemory(final boolean includeLinkedProcessors) {
		clearAllCaches();
		if (includeLinkedProcessors) {
			getLinkedProcessors().forEach(proc -> proc.freeMemory(false));
		}
	}

	/**
	 * Answer the total instance footprint
	 * @param includeLinkedProcessors
	 * 			Query also all other processors for this entity, e.g. the Extended
	 * @return total instance footprint, bytes
	 */
	@Override
	public long getTotalMemoryUsage(final boolean includeLinkedProcessors) {
		final GraphLayout graphLayout;
		try {
			if (isCached()) {
				graphLayout = GraphLayout.parseInstance(cache);
			} else {
				graphLayout = new GraphLayout();
			}
			if (includeLinkedProcessors) {
				getLinkedProcessors().stream()
						.filter(AbstractDalProcessor::isCached)
						.forEach(proc -> graphLayout.add(GraphLayout.parseInstance(proc.cache)));
			}
			return graphLayout.totalSize();
		} catch (Exception ex) {
			LOG.error("Error during total memory usage calculation: " + ex.getMessage(), ex);
			return 0L;
		}
	}

	private static class DalChangeTransactionSynchronization<DalVO extends IDalVO<PK>, PK>
			extends TransactionSynchronizationAdapter
			implements ITransactionSynchronizationSavepointExtension{

		private final DalChangeNotificationForTransactionSync<PK> changeNotification;

		private final WeakReference<AbstractDalProcessor<DalVO, PK>> weakProcessor;

		private final boolean bNotifyCluster;

		DalChangeTransactionSynchronization(final AbstractDalProcessor<DalVO, PK> processor, final boolean bNotifyCluster) {
			weakProcessor = new WeakReference<>(processor);
			this.bNotifyCluster = bNotifyCluster;
			changeNotification = new DalChangeNotificationForTransactionSync<>(processor.getEntityUID(), UID.class.isAssignableFrom(processor.getPkType()));
		}

		@Override
		public int getOrder() {
			return bNotifyCluster ? -99 : -98;
		}

		@Override
		public void beforeCommit(final boolean readOnly) {
			if (changeNotification.isEmpty()) {
				return;
			}
			Optional.ofNullable(weakProcessor.get())
					.ifPresent(proc -> {
						proc.observeChangesNotifyBeforeCommit(false).onNext(changeNotification);
					});
		}

		@Override
		public void afterCompletion(final int status) {
			if (changeNotification.isEmpty()) {
				return;
			}
			try {
				if (status == TransactionSynchronization.STATUS_COMMITTED) {
					Optional.ofNullable(weakProcessor.get())
							.ifPresent(proc -> {
								proc.observeChangesNotifyAfterCommit(false).onNext(changeNotification);
								if (bNotifyCluster && proc.isClusterWideNotification() && RigidClusterHelper.isClusterEnabled()) {
									try {
										RigidClusterHelper.sendMessage(
												ClusterMessageType.DAL_CHANGE_NOTIFICATION,
												objectMapper.writeValueAsString(changeNotification.compressForClusterMessage()));
									} catch (Exception e) {
										LOG.error(String.format("Cluster message could not be send for entity %s!", proc.getEntityUID()), e);
									}
								}
							});
				} else if (status == TransactionSynchronization.STATUS_ROLLED_BACK) {
					if (bNotifyCluster) { // yes only when NotifyCluster is true (default operations)
						// roll back cache operations und notify all observables...
						Optional.ofNullable(weakProcessor.get())
								.ifPresent(proc -> proc.observeChangesNotifyRollbackOnly().onNext(changeNotification.toRollbackOnly()));
					}
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			reset();
		}

		@Override
		public void afterSavepointRollback() {
			afterCompletion(TransactionSynchronization.STATUS_ROLLED_BACK);
		}

		private void addInserted(PK pk) {
			changeNotification.addInserted(pk);
		}

		private void addUpdated(PK pk) {
			changeNotification.addUpdated(pk);
		}

		private void addDeleted(PK pk) {
			changeNotification.addDeleted(pk);
		}

		private void setUnidentifiedInserts() {
			changeNotification.setUnidentifiedInserts();
		}

		private void setUnidentifiedUpdates() {
			changeNotification.setUnidentifiedUpdates();
		}

		private void setUnidentifiedDeletes() {
			changeNotification.setUnidentifiedDeletes();
		}

		public void reset() {
			changeNotification.reset();
		}
	}

	private static class CacheMap<PK, DalVO extends IDalVO<PK>> extends ConcurrentHashMap<PK, DalVO> {

		private final UID entityUID;

		private final CachedObjectBuilder<DalVO, PK> cachedObjectBuilder;

		private List<DalVO> allList;

		public CacheMap(final UID entityUID, final CachedObjectBuilder<DalVO, PK> cachedObjectBuilder) {
			this.entityUID = entityUID;
			this.cachedObjectBuilder = cachedObjectBuilder;
		}

		@Override
		public DalVO get(final Object o) {
			return Optional.ofNullable(super.get(o))
					.map(cachedObjectBuilder::buildCachedObject)
					.orElse(null);
		}

		public List<DalVO> getAllCached() {
			synchronized (this) {
				return allList == null ? null : Collections.unmodifiableList(allList.parallelStream()
						.map(cachedObjectBuilder::buildCachedObject)
						.collect(Collectors.toList()));
			}
		}

		public void clearAllCachedOnly() {
			synchronized (this) {
				this.allList = null;
			}
		}

		public List<DalVO> fillWithAllCached(final List<DalVO> newAllList) {
			synchronized (this) {
				super.clear();
				this.allList = null;
				if (newAllList != null) {
					if (newAllList.parallelStream().anyMatch(dalVO -> dalVO.getPrimaryKey() == null)) {
						throw new IllegalArgumentException(String.format("dalVO without PK found (entity %s)", entityUID));
					}
					this.allList = new ArrayList<>(newAllList);
					super.putAll(this.allList.stream()
							.collect(Collectors.toMap(
									HasPrimaryKey::getPrimaryKey,
									dalVO -> dalVO,
									(v1, v2) -> v2)));
				}
				return getAllCached();
			}
		}

		public DalVO putDalVO(final DalVO dalVO) {
			if (dalVO == null) {
				throw new IllegalArgumentException(String.format("dalVO must not be null (entity %s)", entityUID));
			}
			if (dalVO.getPrimaryKey() == null) {
				throw new IllegalArgumentException(String.format("dalVO.getPrimaryKey must not be null (entity %s)", entityUID));
			}
			final DalVO prevDalVO = super.put(dalVO.getPrimaryKey(), dalVO);
			synchronized (this) {
				if (allList != null) {
					if (prevDalVO != null) {
						// updated, replace (remove + insert) allList...
						allList.remove(prevDalVO);
					}
					allList.add(dalVO);
				}
			}
			return cachedObjectBuilder.buildCachedObject(dalVO);
		}

		public void removePK(final PK pk) {
			if (pk == null) {
				throw new IllegalArgumentException(String.format("pk must not be null (entity %s)", entityUID));
			}
			final DalVO prevDalVO = super.remove(pk);
			synchronized (this) {
				if (allList != null) {
					if (prevDalVO != null) {
						// remove the value for the primary key
						allList.remove(prevDalVO);
					}
				}
			}
		}

		@Override
		public void clear() {
			fillWithAllCached(null);
		}

	}

}
