//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import static org.nuclos.server.dal.processor.AbstractDalProcessor.DT_BOOLEAN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.query.DbReferencedCompoundColumnExpression;

public abstract class AbstractProcessorFactory {

	protected abstract TableAliasSingleton getTableAliasSingleton();

	protected <DalVO extends IDalVO<PK>, PK> ProcessorConfiguration<DalVO, PK>
			newProcessorConfiguration(Class<DalVO> type,
									  EntityMeta<PK> eMeta,
									  Collection<FieldMeta<?>> colEfMeta,
									  boolean addSystemColumns,
									  CachedObjectBuilder<DalVO, PK> cachedObjectBuilder) {
		final List<IColumnToVOMapping<? extends Object, PK>> allColumns = new ArrayList<IColumnToVOMapping<? extends Object, PK>>();
		final DataSourceCaseSensivity dataSourceCaseSensivity = new DataSourceCaseSensivity(eMeta);

		Set<UID> staticSystemFields = new HashSet<UID>();

		IColumnToVOMapping<?, PK> pkColumn = null;

		if (!eMeta.isDynamic() && !eMeta.isChart()) {

			if (eMeta.isUidEntity()) {
				pkColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, eMeta.getUID());
			} else {
				pkColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_ID, eMeta.getUID());
			}

			allColumns.add(pkColumn);

		} else if (!eMeta.isChart()) {

			if (eMeta.getPkClass().equals(SF.PK_UID.getJavaClass())) {
				boolean bCaseSensitive = dataSourceCaseSensivity.isCaseSensitive(SF.PK_UID.getMetaData(eMeta));
				pkColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, eMeta.getUID(), bCaseSensitive);

			} else if (eMeta.getPkClass().equals(SF.PK_ID.getJavaClass())) {
				boolean bCaseSensitive = dataSourceCaseSensivity.isCaseSensitive(SF.PK_ID.getMetaData(eMeta));
				pkColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_ID, eMeta.getUID(), bCaseSensitive);

			} else {
				// FIXME handle this case
			}

			if (null != pkColumn) {
				allColumns.add(pkColumn);
			}

		}
		final IColumnToVOMapping<Integer, PK> versionColumn;

		if (addSystemColumns) {
			allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, eMeta.getUID()));
			allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, eMeta.getUID()));
			allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, eMeta.getUID()));
			allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, eMeta.getUID()));
			versionColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, eMeta.getUID());
			allColumns.add(versionColumn);
			if (eMeta.isUidEntity()) {
				colEfMeta = new ArrayList<>(colEfMeta);
				colEfMeta.add(SF.IMPORTVERSION.getMetaData(eMeta));
				colEfMeta.add(SF.ORIGINUID.getMetaData(eMeta));
			}
		} else {
			versionColumn = null;
		}

		for (IColumnToVOMapping<?, ?> col : allColumns) {
			staticSystemFields.add(col.getUID());
		}

		for (FieldMeta<?> efMeta : colEfMeta) {
			if (efMeta.getDbColumn() == null) {
				continue;
			}

			if (staticSystemFields.contains(efMeta.getUID())
					|| efMeta.getDbColumn().equalsIgnoreCase(SF.PK_ID.getDbColumn())) {
				// hier nur dynamische Zuweisungen
				continue;
			}

			if (efMeta.isCalcAttributeAllowCustomization()) {
				continue;
			}

			boolean bCaseSensitive = dataSourceCaseSensivity.isCaseSensitive(efMeta);

			// normal (non-reference) field
			if (efMeta.getForeignEntity() == null && efMeta.getUnreferencedForeignEntity() == null) {
				allColumns.add(this.<Object, PK>createFieldMapping(SystemFields.BASE_ALIAS, efMeta, bCaseSensitive));

			}
			// column is ref to foreign table
			else {
				boolean bUidEntity = false;
				EntityMeta<?> sysMeta = E.getByUID(RigidUtils.defaultIfNull(efMeta.getForeignEntity(), efMeta.getUnreferencedForeignEntity()));
				if (sysMeta != null) {
					bUidEntity = sysMeta.isUidEntity();
				}

				// only an primary key ref to foreign table
				if (efMeta.getJavaClass() == Long.class && efMeta.getDbColumn().toUpperCase().startsWith("INTID_")) {
					// kein join nötig!
					if (!isColumnInList(allColumns, efMeta.getDbColumn())) {
						allColumns.add(this.<Object, PK>createFieldIdMapping(SystemFields.BASE_ALIAS, efMeta, bUidEntity, bCaseSensitive));
					}
				} else if (efMeta.getJavaClass() == UID.class && efMeta.getForeignEntityField() == null && efMeta.getUnreferencedForeignEntityField() == null) {
					// kein join nötig!
					if (!isColumnInList(allColumns, efMeta.getDbColumn())) {
						allColumns.add(this.<Object, PK>createFieldIdMapping(SystemFields.BASE_ALIAS, efMeta, bUidEntity, bCaseSensitive));
					}
				}
				// normal case: key ref and 'stringified' ref to foreign table
				else {

					// Add foreign key first
					final String dbIdFieldName = DbUtils.getDbIdFieldName(efMeta, bUidEntity);
					if (!isColumnInList(allColumns, dbIdFieldName)) {
						allColumns.add(this.<Object, PK>createFieldIdMapping(SystemFields.BASE_ALIAS, efMeta, bUidEntity, bCaseSensitive));
					}
					// id column is already in allColumns:
					// Replace the id column if the one present is read-only and the current is not read-only
					// This in effect only switched the read-only flag to false.
					else {
						final IColumnToVOMapping<?, ?> col = getColumnFromList(allColumns, dbIdFieldName);
						if (col.isReadonly() && !efMeta.isReadonly()) {
							allColumns.remove(col);
							allColumns.add(this.<Object, PK>createFieldIdMapping(SystemFields.BASE_ALIAS, efMeta, bUidEntity, bCaseSensitive));
						}
					}

					// Also add 'stringified' ref to column mapping
					String stringifiedColumn = efMeta.getDbColumn();
					// Adjust column name if stringified value is given
					if (RigidUtils.equal(dbIdFieldName, stringifiedColumn) && efMeta.getForeignEntityField() != null) {
						stringifiedColumn = DbReferencedCompoundColumnExpression.getRefAlias(efMeta);
					}
					// In case the ref column does not want an 'stringified' column (= INTID_...)
					// check if column is already in list
					if (!RigidUtils.equal(dbIdFieldName, stringifiedColumn) && !isColumnInList(allColumns, stringifiedColumn)) {
						if (eMeta.getReadDelegate() != null) {
							allColumns.add(this.<Object, PK>createFieldMapping(SystemFields.BASE_ALIAS, efMeta, /*readonly=*/true));
						} else {
							allColumns.add(this.<Object, PK>createRefFieldMapping((FieldMeta<Object>)efMeta));
						}
					}
				}
			}
		}

		return new ProcessorConfiguration<DalVO, PK>(
				type,
				eMeta,
				allColumns,
				RigidUtils.uncheckedCast(pkColumn),
				versionColumn,
				addSystemColumns,
				dataSourceCaseSensivity,
				cachedObjectBuilder);
	}

	public static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, FieldMeta<S> ef, String methodRadical) {
		try {
			return createBeanMapping(alias, type, ef.getDbColumn(), methodRadical, ef.getUID(), (Class<S>)Class.forName(ef.getDataType()), false, false);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}

	public static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, SF<S> sf, UID entityUID) {
		Class<S> javaClass = sf.getJavaClass();
		return createBeanMapping(alias, type, sf.getDbColumn(), sf.getFieldName(), entityUID == null ? null : sf.getUID(entityUID), javaClass, false, false);
	}

	public static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, SF<S> sf, UID entityUID, boolean bCaseSensitive) {
		Class<S> javaClass = sf.getJavaClass();
		return createBeanMapping(alias, type, sf.getDbColumn(), sf.getFieldName(), entityUID == null ? null : sf.getUID(entityUID), javaClass, false, bCaseSensitive);
	}

	public static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, FieldMeta<S> fieldMeta) {
		try {
			return createBeanMapping(alias, type, fieldMeta.getDbColumn(), fieldMeta.getFieldName(), fieldMeta.getUID(), (Class<S>)Class.forName(fieldMeta.getDataType()), false, false);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}

	private static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, String column, String methodRadical,
																	   UID fieldUID, Class<S> dataType, boolean isReadonly, boolean bCaseSensitive) {
		final String xetterSuffix = methodRadical.substring(0, 1).toUpperCase() + methodRadical.substring(1);

		Class<?> methodParameterType = dataType;
		if ("primaryKey".equals(methodRadical)) {
			methodParameterType = Object.class;
		}
		try {
			return new ColumnToBeanVOMapping<S, PK>(alias, column, fieldUID, type.getMethod("set" + xetterSuffix, methodParameterType),
					type.getMethod((DT_BOOLEAN.equals(dataType) ? "is" : "get") + xetterSuffix), dataType, isReadonly, bCaseSensitive);
		} catch (Exception e) {
			throw new CommonFatalException("On " + type + ": " + e);
		}
	}

	protected static <S extends Object, PK> IColumnToVOMapping<S, PK> createFieldMapping(String alias, FieldMeta<?> field, boolean bCaseSensitive) {
		return createFieldMapping(alias, field, field.isReadonly(), bCaseSensitive);
	}

	protected static <S extends Object, PK> IColumnToVOMapping<S, PK> createFieldMapping(String alias, FieldMeta<?> field, boolean readonly, boolean bCaseSensitive) {
		try {
			return new ColumnToFieldVOMapping<S, PK>(alias, field, readonly, bCaseSensitive);
		} catch (ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}

	protected <S extends Object, PK> IColumnToVOMapping<S, PK> createRefFieldMapping(FieldMeta<S> field) {
		try {
			final String alias = getTableAliasSingleton().getAlias(field);
			return new ColumnToRefFieldVOMapping<S, PK>(alias, field);
		} catch (ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}

	protected static <S extends Object, PK> IColumnToVOMapping<S, PK> createFieldIdMapping(String alias, FieldMeta<?> field, boolean bUidEntity, boolean bCaseSensitive) {
		try {
			return new ColumnToFieldIdVOMapping<S, PK>(alias, field, bUidEntity, bCaseSensitive);
		} catch (ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}

	protected static <PK> boolean isColumnInList(List<IColumnToVOMapping<?, PK>> list, String columnName) {
		return getColumnFromList(list, columnName) != null;
	}

	protected static <PK> IColumnToVOMapping<?, ?> getColumnFromList(List<IColumnToVOMapping<?, PK>> list, String columnName) {
		for (IColumnToVOMapping<?, ?> column : list) {
			if (column.getColumn().equals(columnName)) {
				return column;
			}
		}
		return null;
	}

}
