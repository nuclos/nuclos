//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import java.text.MessageFormat;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SFE;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.IDalWithFieldsVO;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Map a database column to a reference entity field representation.
 * <p>
 * This mapping is only used for primary key refs to foreign table now.
 * In all other ref cases it is replaced by 
 * {@link org.nuclos.server.dal.processor.ColumnToRefFieldVOMapping}.
 * Perhaps we should consider this as deprecated ?!?
 * </p>
 * @see org.nuclos.common.dal.vo.IDalWithFieldsVO#getFieldIds()
 *
 * @param <T> Java type for the data in this column of the database.
 */
public final class ColumnToFieldIdVOMapping<T, PK> extends AbstractColumnToVOMapping<T, PK> 
	implements IColumnToVOMapping<T, PK> {
	
	private static final Logger LOG = LoggerFactory.getLogger(ColumnToFieldIdVOMapping.class);

	private final FieldMeta field;

	/**
	 * Konstruktor für dynamische VO Werte (Die Werte werden in einer "FieldIds"-Liste gespeichert)
	 */
	public ColumnToFieldIdVOMapping(String tableAlias, FieldMeta<?> field, boolean isUidEntity, boolean caseSensitive) throws ClassNotFoundException {
		// This is the reference from the (base) entity to the foreign table.
		//TODO: Check about field.isDyanmic(). It doesn't make any sense. First of all, there are no dynamic fields. And if it would mean that
		//the entity is dynamic, it doesn't mean automatically that it is case case sensitive. Moreover field.isDynamic() seems to be always false.
		
		super(tableAlias, DbUtils.getDbIdFieldName(field, isUidEntity), field.getUID(), isUidEntity ? UID.class.getName() : Long.class.getName(), 
				field.isReadonly(), caseSensitive || field.isDynamic());
		this.field = field;
	}

	// @Override
	public FieldMeta getMeta() {
		return field;
	}
	
	public static <PK, T> DbExpression<T> getOwnerIdColumn(FieldMeta<?> ownerFieldMeta, DbFrom<PK> from, boolean withColumnName) {
		final String tableAlias = from.getAlias();
		final DbQueryBuilder builder = from.getQuery().getBuilder();
		return getOwnerIdColumn(ownerFieldMeta, tableAlias, builder, withColumnName);
	}
	
	public static <PK, T> DbExpression<T> getOwnerIdColumn(FieldMeta<?> ownerFieldMeta, String tableAlias, DbQueryBuilder builder, boolean withColumnName) {
		final String subsql = MessageFormat.format(ownerFieldMeta.getCalcFunction(), tableAlias, ownerFieldMeta.getEntity().getString());
		return (DbExpression<T>) builder.plainExpression(UID.class, 
			withColumnName ? 
				(subsql + " " + DbUtils.getDbIdFieldName(ownerFieldMeta, true)) : 
				(subsql));
	}
	
	@Override
	public DbExpression<T> getDbColumn(DbFrom<PK> from, final IRigidMetaProvider metaProv) {
		if (SFE.OWNER.checkField(field.getEntity(), field.getUID())) {
			return getOwnerIdColumn(field, from, true);
		} else {
			return super.getDbColumn(from, metaProv);
		}
	}

	@Override
	public String toString() {
		return generateString("field", field);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ColumnToFieldIdVOMapping)) return false;
		final ColumnToFieldIdVOMapping<T, PK> other = (ColumnToFieldIdVOMapping<T, PK>) o;
		return getColumn().equals(other.getColumn()) && field.equals(other.field);
	}
	
	@Override
	public int hashCode() {
		int result = getColumn().hashCode();
		result += 3 * field.hashCode();
		return result;
	}
	
	public UID getUID() {
		return field.getUID();
	}

	@Override
	public Object convertFromDalFieldToDbValue(IDalVO<PK> dal, final IDbValueConverter converter) {
		final IDalWithFieldsVO<?, PK> realDal = (IDalWithFieldsVO<?, PK>) dal;
		try {
			if (getDataType() == UID.class) {
				return convertToDbValue(getDataType(), realDal.getFieldUid(field.getUID()), converter);
			} else {
				return convertToDbValue(getDataType(), realDal.getFieldId(field.getUID()), converter);
			}
		} catch (Exception e) {
			throw new CommonFatalException(e);
		}
	}

	@Override
	public void convertFromDbValueToDalField(IDalVO<PK> result, T o, final FromDbValueConversionParams params, final IRigidMetaProvider metaProv, final IDbValueConverter converter) {
		final IDalWithFieldsVO<?, PK> realDal = (IDalWithFieldsVO<?, PK>) result;
		if (getDataType() == UID.class) {
			realDal.setFieldUid(field.getUID(),	(UID) o);
		} else {
			realDal.setFieldId(field.getUID(), (Long) o);
		}
		// if field handles NUCLOSSTATE issues...
		// handle special case with INTVALUE_NUCLOSSTATE and OBJVALUE_NUCLOSSTATE
		if (RigidUtils.equal(field.getUID(), SF.STATE.getUID(field.getEntity()))) {
			//realDal.setFieldUid(SF.STATEICON.getUID(field.getEntity()), (UID)o); // will be added at client. performance issues...
			realDal.setFieldUid(SF.STATENUMBER.getUID(field.getEntity()), (UID)o);
		}

		if (field.getUnreferencedForeignEntity() != null &&
				field.getUnreferencedForeignEntityField() != null &&
				realDal.getFieldValue(field.getUID()) == null) {
			if (o != null) {
				UID fieldUidIfAny = null;
				try {
					fieldUidIfAny = UID.parseUID(field.getUnreferencedForeignEntityField());
				} catch (Exception e) {/*ignore*/}
				if (new UID("5E8q").equals(field.getUnreferencedForeignEntity()) && // Entity
						fieldUidIfAny != null &&
						fieldUidIfAny.equals(new UID("5E8qa"))) {
					// check entity first, entity may be missing during nuclet import
					if (metaProv.checkEntity((UID) o)) {
						EntityMeta<?> eMeta = metaProv.getEntity((UID) o);
						((IDalWithFieldsVO) result).setFieldValue(field.getUID(), eMeta.getEntityName());
					}
				} else if (new UID("Khi5").equals(field.getUnreferencedForeignEntity()) &&
						fieldUidIfAny != null &&
						fieldUidIfAny.equals(new UID("Khi5c"))) {
					// check entity field first, field may be missing during nuclet import
					if (metaProv.checkEntityField((UID) o)) {
						FieldMeta<?> fMeta = metaProv.getEntityField((UID) o);
						((IDalWithFieldsVO) result).setFieldValue(field.getUID(), fMeta.getFieldName());
					}
				} else {
					LOG.debug("Stringified value for unreferenced foreign entity field {} not "
							  + "implemented.", field.getUID()); // Do not log error: Otherwise Nuclet export is full of these messages
				}
			}
		}
	}

}
