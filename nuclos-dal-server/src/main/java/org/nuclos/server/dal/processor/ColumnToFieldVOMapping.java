//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.IDalWithFieldsVO;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dal.processor.jdbc.ICalcAttributeBuilder;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQueryBuilder;

/**
 * Map a database column to a simple value reference entity field representation.
 * 
 * @see org.nuclos.common.dal.vo.IDalWithFieldsVO#getFieldValues()
 *
 * @param <T> Java type for the data in this column of the database.
 */
public class ColumnToFieldVOMapping<T extends Object, PK> extends AbstractColumnToVOMapping<T, PK>
	implements IColumnWithMdToVOMapping<T, PK>
{

	private final FieldMeta<?> field;
	
	/**
	 * Konstruktor für dynamische VO Werte (Die Werte werden in einer "Fields"-Liste gespeichert).
	 * @throws ClassNotFoundException 
	 */
	public ColumnToFieldVOMapping(String tableAlias, FieldMeta<?> field) throws ClassNotFoundException {
		this(tableAlias, field, field.isReadonly(), false);
	}
	
	public ColumnToFieldVOMapping(String tableAlias, FieldMeta<?> field, boolean readonly, boolean bCaseSensitive) throws ClassNotFoundException {
		super(tableAlias, field.getDbColumn(), field.getUID(), field.getDataType(), readonly, bCaseSensitive || field.isDynamic());
		this.field = field;
	}

	@Override
	public DbExpression<T> getDbColumn(DbFrom<PK> from, final IRigidMetaProvider metaProv) {
		final DbExpression<T> result;
		if (!field.isCalculated()) {
			result = super.getDbColumn(from, metaProv);
		}
		else {
			String localeAdd = field.isLocalized() ? " , '" + ((IDataLanguageLocator) FieldMeta.getDataLanguageLocator(field, metaProv)).getDataLanguageToUse() + "'"  : "";

			if (field.getCalcAttributeDS() != null) {
				final DbQueryBuilder builder = from.getQuery().getBuilder();
				final Object calcAttributeBuilder = FieldMeta.getCalcAttributeBuilder(field, metaProv);
				final String dsSQL = ((ICalcAttributeBuilder) calcAttributeBuilder).buildCalcAttributeSQL(from);
				result = (DbExpression<T>) builder.plainExpression(
						DbUtils.getDbType(field.getDataType()), 
						" (" + dsSQL + ")");
				result.alias(field.getDbColumn());
			} else {
				final DbQueryBuilder builder = from.getQuery().getBuilder();
				result = (DbExpression<T>) builder.plainExpression(
						DbUtils.getDbType(field.getDataType()), 
						builder.getDBAccess().getSchemaName() + "." 
						+ field.getCalcFunction() + "(" + from.basePk().getSqlColumnExpr(false) + localeAdd + ")");
				result.alias(field.getDbColumn());
			}
		}
		return result;
	}
	
	@Override
	public String toString() {
		return generateString("field", field);
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ColumnToFieldVOMapping)) return false;
		final ColumnToFieldVOMapping<T, PK> other = (ColumnToFieldVOMapping<T, PK>) o;
		return field.equals(other.field);
	}
	
	@Override
	public int hashCode() {
		int result = getColumn().hashCode();
		result += 3 * field.hashCode();
		return result;
	}
	
	public UID getUID() {
		return field.getUID();
	}

	@Override
	public FieldMeta<?> getMeta() {
		return field;
	}

	@Override
	public Object convertFromDalFieldToDbValue(IDalVO<PK> dal, final IDbValueConverter converter) {
		final IDalWithFieldsVO<?, PK> realDal = (IDalWithFieldsVO<?, PK>) dal;
		try {
			return convertToDbValue(getDataType(), realDal.getFieldValue(field.getUID()), converter);
		} catch (Exception e) {
			throw new CommonFatalException(e);
		}
	}

	@Override
	public void convertFromDbValueToDalField(IDalVO<PK> result, T o, final FromDbValueConversionParams params, final IRigidMetaProvider metaProv, final IDbValueConverter converter) {
		final IDalWithFieldsVO<Object, PK> realDal = (IDalWithFieldsVO<Object, PK>) result;
		try {
			realDal.setFieldValue(field.getUID(),
					convertFromDbValue(o, getColumn(), getDataType(), result.getPrimaryKey(), params, converter));
		} catch (Exception e) {
			throw new CommonFatalException(e);
		}
	}

}
