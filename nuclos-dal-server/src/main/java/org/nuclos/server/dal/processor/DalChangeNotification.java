//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.IDalVO;

public class DalChangeNotification<DalVO extends IDalVO<PK>, PK> extends DalChangeNotificationForTransactionSync<PK> {

	private DalVO singleChange;
	private boolean singleInsert;
	private boolean singleUpdate;

	public DalChangeNotification(final UID dalEntity, final boolean uidPks) {
		super(dalEntity, uidPks);
	}

	protected DalChangeNotification(final UID dalEntity, final boolean uidPks, final boolean rollbackOnly) {
		super(dalEntity, uidPks, rollbackOnly);
	}

	public void reset() {
		super.reset();
		singleChange = null;
		singleInsert = false;
		singleUpdate = false;
	}

	public static <DalVO extends IDalVO<PK>, PK> DalChangeNotification<DalVO, PK> withSingleInsert(DalVO dalVO) {
		DalChangeNotification<DalVO, PK> result = new DalChangeNotification<>(dalVO.getDalEntity(), dalVO.getPrimaryKey() instanceof UID);
		result.setSingleInsert(dalVO);
		return result;
	}

	public static <DalVO extends IDalVO<PK>, PK> DalChangeNotification<DalVO, PK> withUnidentifiedInserts(UID entityUID, boolean uidPks) {
		DalChangeNotification<DalVO, PK> result = new DalChangeNotification<>(entityUID, uidPks);
		result.setUnidentifiedInserts();
		return result;
	}

	public static <DalVO extends IDalVO<PK>, PK> DalChangeNotification<DalVO, PK> withSingleUpdate(DalVO dalVO) {
		DalChangeNotification<DalVO, PK> result = new DalChangeNotification<>(dalVO.getDalEntity(), dalVO.getPrimaryKey() instanceof UID);
		result.setSingleUpdate(dalVO);
		return result;
	}

	public static <DalVO extends IDalVO<PK>, PK> DalChangeNotification<DalVO, PK> withUnidentifiedUpdates(UID entityUID, boolean uidPks) {
		DalChangeNotification<DalVO, PK> result = new DalChangeNotification<>(entityUID, uidPks);
		result.setUnidentifiedUpdates();
		return result;
	}

	public static <DalVO extends IDalVO<PK>, PK> DalChangeNotification<DalVO, PK> withSingleDelete(Delete<PK> del, boolean uidPks) {
		DalChangeNotification<DalVO, PK> result = new DalChangeNotification<>(del.getDalEntity(), uidPks);
		if (del.getPrimaryKey() != null) {
			result.addDeleted(del.getPrimaryKey());
		}
		return result;
	}

	public static <DalVO extends IDalVO<PK>, PK> DalChangeNotification<DalVO, PK> withUnidentifiedDeletes(UID entityUID, boolean uidPks) {
		DalChangeNotification<DalVO, PK> result = new DalChangeNotification<>(entityUID, uidPks);
		result.setUnidentifiedDeletes();
		return result;
	}

	public void setSingleInsert(DalVO dalVO) {
		if (singleUpdate) {
			throw new RuntimeException("Multiple settings not possible");
		}
		singleChange = dalVO;
		singleInsert = true;
	}

	public void setSingleUpdate(DalVO dalVO) {
		if (singleInsert) {
			throw new RuntimeException("Multiple settings not possible");
		}
		singleChange = dalVO;
		singleUpdate = true;
	}

	public boolean isSingleChangeOnly() {
		return (singleInsert || singleUpdate) && !hasUnidentified();
	}

	public DalVO getSingleChange() {
		return singleChange;
	}

	public DalVO getSingleInsert() {
		if (singleInsert) {
			return singleChange;
		}
		return null;
	}

	public DalVO getSingleUpdate() {
		if (singleUpdate) {
			return singleChange;
		}
		return null;
	}

	@Override
	public Set<PK> getInserted() {
		return getPks(super.getInserted(), singleInsert);
	}

	@Override
	public Set<PK> getUpdated() {
		return getPks(super.getUpdated(), singleUpdate);
	}

	private Set<PK> getPks(final Set<PK> pks, final boolean mergeWithSinglePk) {
		if (mergeWithSinglePk) {
			if (pks == null) {
				return Collections.singleton(singleChange.getPrimaryKey());
			} else {
				Set<PK> result = new HashSet<>(pks);
				result.add(singleChange.getPrimaryKey());
				return result;
			}
		}
		return pks;
	}

	@Override
	public boolean hasInserts() {
		return singleInsert || super.hasInserts();
	}

	@Override
	public boolean hasUpdates() {
		return singleUpdate || super.hasUpdates();
	}

}
