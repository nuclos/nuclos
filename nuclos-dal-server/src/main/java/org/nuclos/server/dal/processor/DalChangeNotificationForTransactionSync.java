//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDalVO;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.google.common.collect.Sets;

@JsonSerialize(using = DalChangeNotificationForTransactionSync.NotificationJsonSerializer.class)
@JsonDeserialize(using = DalChangeNotificationForTransactionSync.NotificationJsonDeserializer.class)
public class DalChangeNotificationForTransactionSync<PK> {

	private final UID dalEntity;
	private final boolean uidPks;
	private final boolean rollbackOnly;

	protected Set<PK> insertedSet;
	protected Set<PK> updatedSet;
	protected Set<PK> deletedSet;

	protected boolean unidentifiedInserts;
	protected boolean unidentifiedUpdates;
	protected boolean unidentifiedDeletes;

	public DalChangeNotificationForTransactionSync(final UID dalEntity, final boolean uidPks) {
		this(dalEntity, uidPks, false);
	}

	protected DalChangeNotificationForTransactionSync(final UID dalEntity, final boolean uidPks, final boolean rollbackOnly) {
		this.dalEntity = dalEntity;
		this.uidPks = uidPks;
		this.rollbackOnly = rollbackOnly;
	}

	public void reset() {
		insertedSet = null;
		updatedSet = null;
		deletedSet = null;
		unidentifiedInserts = false;
		unidentifiedUpdates = false;
		unidentifiedDeletes = false;
	}

	public void addInserted(PK pk) {
		if (insertedSet == null) {
			insertedSet = new HashSet<>();
		}
		insertedSet.add(pk);
	}

	public void addUpdated(PK pk) {
		if (updatedSet == null) {
			updatedSet = new HashSet<>();
		}
		updatedSet.add(pk);
	}

	public void addDeleted(PK pk) {
		if (deletedSet == null) {
			deletedSet = new HashSet<>();
		}
		deletedSet.add(pk);
	}

	void setUnidentifiedInserts() {
		this.unidentifiedInserts = true;
	}

	void setUnidentifiedUpdates() {
		this.unidentifiedUpdates = true;
	}

	void setUnidentifiedDeletes() {
		this.unidentifiedDeletes = true;
	}

	public UID getDalEntity() {
		return dalEntity;
	}

	public Set<PK> getInsertedAndUpdated() {
		final Set<PK> inserted = getInserted();
		final Set<PK> updated = getUpdated();
		if (inserted == null) {
			return updated;
		} else {
			if (updated == null) {
				return inserted;
			} else {
				return Sets.union(inserted, updated);
			}
		}
	}

	public Set<PK> getInserted() {
		return insertedSet;
	}

	public Set<PK> getUpdated() {
		return updatedSet;
	}

	public Set<PK> getDeleted() {
		return deletedSet;
	}

	public boolean hasUnidentifiedInserts() {
		return unidentifiedInserts;
	}

	public boolean hasUnidentifiedUpdates() {
		return unidentifiedUpdates;
	}

	public boolean hasUnidentifiedDeletes() {
		return unidentifiedDeletes;
	}

	public boolean hasUnidentified() {
		return hasUnidentifiedInserts() || hasUnidentifiedUpdates() || hasUnidentifiedDeletes();
	}

	public boolean hasInserts() {
		return (getInserted() != null && !getInserted().isEmpty()) || hasUnidentifiedInserts();
	}

	public boolean hasUpdates() {
		return (getUpdated() != null && !getUpdated().isEmpty()) || hasUnidentifiedUpdates();
	}

	public boolean hasDeletes() {
		return (getDeleted() != null && !getDeleted().isEmpty()) || hasUnidentifiedDeletes();
	}

	public boolean isRollbackOnly() {
		return rollbackOnly;
	}

	public boolean isEmpty() {
		return !hasInserts() && !hasUpdates() && !hasDeletes();
	}

	public final <DalVO extends IDalVO<PK>> DalChangeNotification<DalVO, PK> toRollbackOnly() {
		DalChangeNotification<DalVO, PK> result = new DalChangeNotification<DalVO, PK>(this.dalEntity, this.uidPks, true);
		result.unidentifiedInserts = this.unidentifiedInserts;
		result.unidentifiedUpdates = this.unidentifiedUpdates;
		result.unidentifiedDeletes = this.unidentifiedDeletes;
		result.insertedSet = Optional.ofNullable(this.insertedSet).map(HashSet::new).orElse(null);
		result.updatedSet = Optional.ofNullable(this.updatedSet).map(HashSet::new).orElse(null);
		result.deletedSet = Optional.ofNullable(this.deletedSet).map(HashSet::new).orElse(null);
		return result;
	}

	/**
	 * Produces a compressed version of the message specifically for clustering, since we cannot send an unlimited number of ids through this system.
	 * @return New compressed (only with 'unidentified' flags) notification, if necessary only!
	 */
	public final DalChangeNotificationForTransactionSync<PK> compressForClusterMessage() {
		if (isRollbackOnly()) {
			throw new IllegalArgumentException("Rolled back notifications should not be send to the cluster!");
		}
		int pkCount = 0;
		if (getInserted() != null) {
			pkCount += getInserted().size();
		}
		if (getUpdated() != null) {
			pkCount += getUpdated().size();
		}
		if (getDeleted() != null) {
			pkCount += getDeleted().size();
		}
		if (pkCount > 50) {
			DalChangeNotificationForTransactionSync<PK> result = new DalChangeNotificationForTransactionSync<PK>(dalEntity, uidPks);
			if (hasInserts()) {
				result.setUnidentifiedInserts();
			}
			if (hasUpdates()) {
				result.setUnidentifiedUpdates();
			}
			if (hasDeletes()) {
				result.setUnidentifiedDeletes();
			}
			return result;
		}
		return this;
	}

	@SuppressWarnings("rawtypes")
	public static class NotificationJsonSerializer extends StdSerializer<DalChangeNotificationForTransactionSync> {
		public NotificationJsonSerializer() {
			super(DalChangeNotificationForTransactionSync.class);
		}
		@Override
		public void serialize(final DalChangeNotificationForTransactionSync notification, final com.fasterxml.jackson.core.JsonGenerator jgen, final SerializerProvider prov) throws IOException {
			jgen.writeStartObject();
			jgen.writeStringField("entity", notification.dalEntity.getString());
			if (notification.uidPks) {
				jgen.writeBooleanField("uid", true);
			}
			if (notification.unidentifiedInserts) {
				jgen.writeBooleanField("unidentifiedInserts", true);
			}
			if (notification.unidentifiedUpdates) {
				jgen.writeBooleanField("unidentifiedUpdates", true);
			}
			if (notification.unidentifiedDeletes) {
				jgen.writeBooleanField("unidentifiedDeletes", true);
			}
			writePkSet(notification.insertedSet, notification.uidPks, "inserted", jgen);
			writePkSet(notification.updatedSet, notification.uidPks, "updated", jgen);
			writePkSet(notification.deletedSet, notification.uidPks, "deleted", jgen);
			jgen.writeEndObject();
		}
	}

	@SuppressWarnings("rawtypes")
	private static void writePkSet(Set pkSet, boolean uidPks, String fieldName, com.fasterxml.jackson.core.JsonGenerator jgen) throws IOException {
		if (pkSet != null) {
			jgen.writeFieldName(fieldName);
			jgen.writeStartArray();
			for (Object pk : pkSet) {
				if (uidPks) {
					jgen.writeString(((UID)pk).getString());
				} else {
					jgen.writeNumber((Long)pk);
				}
			}
			jgen.writeEndArray();
		}
	}

	@SuppressWarnings("rawtypes")
	public static class NotificationJsonDeserializer extends StdDeserializer<DalChangeNotificationForTransactionSync> {
		public NotificationJsonDeserializer() {
			super(DalChangeNotificationForTransactionSync.class);
		}
		@Override
		public DalChangeNotificationForTransactionSync deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
			final ObjectCodec codec = jpar.getCodec();
			final TreeNode jsonNode = codec.readTree(jpar);
			final UID dalEntity = new UID(codec.treeToValue(jsonNode.get("entity"), String.class));
			final boolean uidPks = readBoolean(codec, jsonNode, "uid");
			DalChangeNotificationForTransactionSync result = new DalChangeNotificationForTransactionSync(dalEntity, uidPks);
			result.unidentifiedInserts = readBoolean(codec, jsonNode, "unidentifiedInserts");
			result.unidentifiedUpdates = readBoolean(codec, jsonNode, "unidentifiedUpdates");
			result.unidentifiedDeletes = readBoolean(codec, jsonNode, "unidentifiedDeletes");
			result.insertedSet = readSet(jsonNode.get("inserted"), uidPks, codec);
			result.updatedSet = readSet(jsonNode.get("updated"), uidPks, codec);
			result.deletedSet = readSet(jsonNode.get("deleted"), uidPks, codec);
			return result;
		}
	}

	public static boolean readBoolean(ObjectCodec codec, TreeNode node, String fieldName) throws JsonProcessingException {
		final TreeNode booleanNode = node.get(fieldName);
		if (booleanNode != null) {
			return codec.treeToValue(booleanNode, boolean.class);
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public static <PK> Set<PK> readSet(TreeNode node, boolean uidPks, ObjectCodec codec) throws JsonProcessingException {
		Set<PK> result = null;
		if (node != null && node.isArray()) {
			result = new HashSet<>();
			for (int i = 0; i < node.size(); i++) {
				if (uidPks) {
					result.add((PK) new UID(codec.treeToValue(node.get(i), String.class)));
				} else {
					result.add((PK) codec.treeToValue(node.get(i), Long.class));
				}
			}
		}
		return result;
	}

}
