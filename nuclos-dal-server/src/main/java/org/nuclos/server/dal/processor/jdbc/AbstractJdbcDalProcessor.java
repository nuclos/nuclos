//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.nuclos.common.CloneUtils;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.Mutable;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosLogicalUniqueViolationException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SFE;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.JdbcTransformerParams;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.DalJoin;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.SystemMetaProvider;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.processor.AbstractDalProcessor;
import org.nuclos.server.dal.processor.ColumnToBeanVOMapping;
import org.nuclos.server.dal.processor.ColumnToFieldVOMapping;
import org.nuclos.server.dal.processor.ColumnToRefFieldVOMapping;
import org.nuclos.server.dal.processor.FromDbValueConversionParams;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IColumnWithMdToVOMapping;
import org.nuclos.server.dal.processor.IDbValueConverter;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.IBatch;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbReferencedSubselectExpression;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbInsertWithSelectStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.statements.DbTableStatement;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractJdbcDalProcessor<DalVO extends IDalVO<PK>, PK> extends AbstractDalProcessor<DalVO, PK> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractJdbcDalProcessor.class);

	protected static final SystemMetaProvider SYSTEM_META_PROVIDER = new SystemMetaProvider(E.getThis());

	// Spring injection
	
	protected SpringDataBaseHelper dataBaseHelper;

	// end of Spring injection

	// This must be cloned and hence cannot be final.
	protected List<IColumnToVOMapping<?, PK>> allColumns;
	private Set<IColumnToVOMapping<?, PK>> allColumnsAsSet;

	private Integer iAllColumnsCache = null;

	public AbstractJdbcDalProcessor(UID entity, Class<? extends IDalVO<PK>> type, Class<PK> pkType, List<IColumnToVOMapping<?, PK>> allColumns) {
		super(entity, (Class<DalVO>) type, pkType);

		this.allColumns = allColumns;
		this.allColumnsAsSet = new HashSet<>(allColumns);
		checkColumns();
	}
	
	@Autowired
	public void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	public Object clone() {
		final AbstractJdbcDalProcessor<DalVO, PK> clone;
		try {
			clone = (AbstractJdbcDalProcessor<DalVO, PK>) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new IllegalStateException(e.toString());
		}
		clone.allColumns = CloneUtils.cloneCollection(allColumns);
		clone.allColumnsAsSet = CloneUtils.cloneCollection(allColumnsAsSet);
		return clone;
	}

   private void checkColumns() {
      if (allColumns.size() != allColumnsAsSet.size()) {
    	  LOG.warn("Duplicates in columns, size is "  + allColumns.size() + " but only " + allColumnsAsSet.size() + " unique elements");
    	  assert false;
      }
   }

   protected abstract EntityMeta<PK> getMetaData();

   protected abstract IRigidMetaProvider getMetaProvider();

   protected abstract IDbValueConverter getDbValueConverter();

   protected abstract IColumnToVOMapping<PK, PK> getPrimaryKeyColumn();

   private List<IColumnToVOMapping<?, PK>> getColumnsAndCache(final List<IColumnToVOMapping<?, PK>> columns, final Mutable<Integer> iCache) {
	   if (columns == null) {
		   Optional.ofNullable(iCache).ifPresent(cache -> cache.setValue(getDefaultCache()));
		   return getDefaultColumns();
	   } else {
		   Optional.ofNullable(iCache).ifPresent(cache -> cache.setValue(getCache(columns)));
		   return columns;
	   }
   }

	protected boolean columnsContainsRecordGrant(final List<IColumnToVOMapping<?, PK>> columns) {
		return columns.parallelStream().anyMatch(c ->
				SF.CANWRITE.getDbColumn().equals(c.getDbColumn()) ||
						SF.CANDELETE.getDbColumn().equals(c.getDbColumn()) ||
						SF.CANSTATECHANGE.getDbColumn().equals(c.getDbColumn()));
	}

   public List<DalVO> getAll() {
      return getAll((List<IColumnToVOMapping<?, PK>>)null);
   }

	public List<PK> getAllIds() {
		DbQuery<PK> query = createSingleColumnQuery(getPrimaryKeyColumn(), true);
		return dataBaseHelper.getDbAccess().executeQuery(query);
	}

	protected List<DalVO> getAll(final List<IColumnToVOMapping<?, PK>> pColumns) {
	    final Mutable<Integer> iCache = new Mutable<>();
		final List<IColumnToVOMapping<?, PK>> columns = getColumnsAndCache(pColumns, iCache);
		List<DalVO> result = super.getAll(iCache.getValue());
		if (result == null) {
			DbQuery<Object[]> query = createQuery(columns, null, true, null);
			result = super.storeAllInCache(iCache.getValue(), dataBaseHelper.getDbAccess().executeQuery(query, createResultTransformer(columns)));
		}
		return new ArrayList<>(result);
	}

	protected Integer getAllColumnsCache() {
		if (isCached() && iAllColumnsCache == null) {
			iAllColumnsCache = getCache(allColumns);
		}
		return iAllColumnsCache;
	}

	protected List<IColumnToVOMapping<?, PK>> getDefaultColumns() {
		return allColumns;
	}

	@Override
	protected Integer getDefaultCache() {
		return getAllColumnsCache();
	}

	public DalVO getByPrimaryKey(final PK pk, Collection<FieldMeta<?>> fields) {
		return getByPrimaryKey(pk);
	}

	public DalVO getByPrimaryKey(final PK pk) {
		return getByPrimaryKey((List<IColumnToVOMapping<?, PK>>)null, pk);
	}

	@Override
	protected List<DalVO> getAll(final Integer iCache) {
		return super.getAll(iCache);
	}

	protected DalVO getByPrimaryKey(final List<IColumnToVOMapping<?, PK>> pColumns, final PK pk) {
		final List<IColumnToVOMapping<?, PK>> columns;
		final Integer iCache;
		if (pColumns == null) {
			columns = getDefaultColumns();
			iCache = getDefaultCache();
		} else {
			columns = pColumns;
			iCache = getCache(pColumns);
		}
		DalVO result = super.getByPrimaryKey(iCache, pk);
		if (result == null) {
			final List<DalVO> resultList = getByColumn(columns, getPrimaryKeyColumn(), pk);
			if (resultList.size() == 1) {
				result = super.storeInCache(iCache, resultList.get(0));
			}
			if (resultList.size() > 1) {
				throw new CommonFatalException("Primary key is not unique!");
			}
		}
		return result;
   }

	protected <T> List<DalVO> getByColumn(final List<IColumnToVOMapping<?, PK>> columns, IColumnToVOMapping<T, PK> column, final T t) {
		DbQuery<Object[]> query = createQuery(columns, null, true, null);
		DbFrom<PK> from = (DbFrom<PK>) CollectionUtils.getFirst(query.getRoots());
		query.where(query.getBuilder().equalValue(column.getDbColumn(from, getMetaProvider()), t));
		return dataBaseHelper.getDbAccess().executeQuery(query, createResultTransformer(columns));
	}

	protected List<DalVO> getByPrimaryKeys(final List<IColumnToVOMapping<?, PK>> pColumns, final List<PK> ids) {
		final Mutable<Integer> iCache = new Mutable<>();
		final List<IColumnToVOMapping<?, PK>> columns = getColumnsAndCache(pColumns, iCache);
		List<DalVO> result = super.getByPrimaryKeys(iCache.getValue(), ids);
		if (result == null || result.size() != ids.size()) {
			DbQuery<Object[]> query = createQuery(columns, null, true, null);
			DbFrom<PK> from = (DbFrom<PK>) CollectionUtils.getFirst(query.getRoots());
			DbExpression<?> pkExpr = getPrimaryKeyColumn().getDbColumn(from, getMetaProvider());
			Transformer<Object[], DalVO> transformer = createResultTransformer(columns);

			List<PK> idsToLoad = ids;
			if (result == null) {
				result = new ArrayList<>(ids.size());
			} else {
				result = new ArrayList<>(result);
				final Set<PK> cachedIds = result.parallelStream()
						.map(HasPrimaryKey::getPrimaryKey)
						.collect(Collectors.toSet());
				idsToLoad = ids.parallelStream()
						.filter(id -> !cachedIds.contains(id))
						.collect(Collectors.toList());
			}

			for (List<PK> idSubList : CollectionUtils.splitEvery(idsToLoad, query.getBuilder().getInLimit())) {
				query.where(pkExpr.as(getPkType()).in(idSubList));
				result.addAll(storeInCache(iCache.getValue(), dataBaseHelper.getDbAccess().executeQuery(query, transformer)));
			}
		}
	   return result;
	}

	protected Integer getCache(final List<IColumnToVOMapping<?, PK>> columns) {
		if (isCached() && !columnsContainsRecordGrant(columns)) {
		   	return columns.parallelStream()
				   .map(this::getValueForHashing)
				   .sorted()
				   .collect(Collectors.joining())
				   .hashCode();
		}
		return null;
	}

	private String getValueForHashing(final IColumnToVOMapping<?, PK> column) {
		final UID uid = column.getUID();
		final String dbColumn = column.getDbColumn();
		if (uid != null) {
			return uid.getString() + ";";
		}
		if (dbColumn != null) {
			return dbColumn + ";";
		}
		return ";";
	}

    /*
     *
     */
	protected Object insertOrUpdate(final DalVO dalVO) {
	   return this.insertOrUpdateImpl((List<IColumnToVOMapping<?, PK>>)null, dalVO, null);
   }

   protected <S> void insertOrUpdate(final List<IColumnToVOMapping<?, PK>> columns, final DalVO dalVO) {
	   this.insertOrUpdateImpl(columns, dalVO, null);
   }

   //This method can be used by a future "batchInsertOrUpdate" with DalCallResult dcr provided. If null, the first exception is thrown

	protected Object insertOrUpdateImpl(final List<IColumnToVOMapping<?, PK>> pColumns, DalVO dalVO, final DalCallResult dcr) {
		final List<IColumnToVOMapping<?, PK>> columns = getColumnsAndCache(pColumns, null);
         DbMap columnValueMap = getColumnValuesMap(columns, dalVO);
         DbTableStatement<PK> stmt = null;
         if (dalVO.isFlagNew()) {
            stmt = new DbInsertStatement<>(getMetaData(), columnValueMap);
         } else if (dalVO.isFlagUpdated()) {
            stmt = new DbUpdateStatement<>(getMetaData(), columnValueMap, getPrimaryKeyMap(dalVO.getPrimaryKey()));
         }
         if (stmt != null) {
            try {
               dataBaseHelper.getDbAccess().execute(stmt);
			   if (dalVO.isFlagNew()) {
				   notifyInsert(dalVO, true);
			   }
			   if (dalVO.isFlagUpdated()) {
				   notifyUpdate(dalVO, true);
			   }
            } catch (DbException ex) {
            	ex = transformInsertOrUpdateException(ex, dalVO);
            	if (dcr == null) {
            		throw ex;
            	}
            	ex.setPkIfNull(dalVO.getPrimaryKey());
            	ex.setStatementsIfNull(getLogStatements(stmt));
				dcr.addRuntimeException(ex);
            }
         }
         return dalVO.getPrimaryKey();
	   }
    //This method can be used by a future "batchDelete" with DalCallResult dcr provided. If null, the first exception is thrown

	protected void deleteImpl(final Delete<PK> del, final DalCallResult dcr) {
		PK pk = del.getPrimaryKey();
		DbStatement stmt = new DbDeleteStatement<>(getMetaData(), getPrimaryKeyMap(pk));
		try {
			dataBaseHelper.getDbAccess().execute(stmt);
			super.notifyDelete(del, true);
		} catch(DbException ex) {
			ex = transformDeleteException(ex, pk);
			if (dcr == null) {
				throw ex;
			}
        	ex.setPkIfNull(pk);
        	ex.setStatementsIfNull(getLogStatements(stmt));
			dcr.addRuntimeException(ex);
		}
   }
   protected void delete(final Delete<PK> del) throws DbException {
	   this.deleteImpl(del, null);
   }

   public DalCallResult insertWithSelect(final DbMap fieldsMap, final DbMap fieldConditionMap, EntityMeta<?> metaEntitySource) {

	   if (getMetaData().getVirtualEntity() != null || metaEntitySource.getVirtualEntity() != null) {
		   throw new CommonFatalException("Virtual entities are not allowed in this InsertWithSlect-Statements");
	   }

	   DalCallResult dcr = new DalCallResult();

	   DbTableStatement<PK> stmt = new DbInsertWithSelectStatement<PK>(
			   getMetaData().getDbTable(), metaEntitySource.getDbTable(), fieldsMap, fieldConditionMap);

	   try {
		  dataBaseHelper.getDbAccess().execute(stmt);
		  notifyUnidentifiedInsert(true);
	   } catch (DbException ex) {
		   dcr.addRuntimeException(ex);
	   }

	   return dcr;
   }

	protected <T> DbQuery<T> createQueryImpl(Class<T> clazz) {
	   DbQuery<T> query = dataBaseHelper.getDbAccess().getQueryBuilder().createQuery(clazz, false);
	   return query;
	}

	protected <S> DbQuery<S> createSingleColumnQuery(IColumnToVOMapping<S, PK> column, boolean overrideDbSourceUseDML) {
		DbQuery<S> query = createQueryImpl(column.getDataType());
		DbFrom<PK> from = query.from(getMetaData(), overrideDbSourceUseDML);
		adjustFrom(from);
		query.select(column.getDbColumn(from, getMetaProvider()));
		return query;
	}

	protected DbQuery<Long> createCountQuery(IColumnToVOMapping<PK, PK> column) {
		DbQuery<Long> query = createQueryImpl(Long.class);
		DbFrom<PK> from = query.from(getMetaData());
		adjustFrom(from);
		adjustFromForCount(from);

		query.setSimpleCount(true);
		query.select(query.getBuilder().count(column.getDbColumn(from, getMetaProvider())));
		return query;
    }

    protected DbQuery<Long> createCountWithLimitQuery(IColumnToVOMapping<PK, PK> column, final Long limit, final Map<String, Object> mpParameters) {
   		DbQuery<Long> query = createQueryImpl(Long.class);
   		DbQuery<PK> subQuery = createQueryImpl(getPkType());

   		if (!query.getBuilder().isCountWithLimitSupported()) {
   			return null;
		}

   		DbFrom<PK> subFrom = subQuery.from(getMetaData());
   		adjustFrom(subFrom);
   		adjustFromForCount(subFrom);

		subQuery.select(column.getDbColumn(subFrom, getMetaProvider()));
		subQuery.limit(limit + 1);

		adjustCountWithLimitSubQuery(subQuery, mpParameters);

		DbFrom<PK> from = query.from(subQuery, "t");

		query.setSimpleCount(true);
		query.select(query.getBuilder().count(column.getDbColumn(from, getMetaProvider())));
   		return query;
	}

	protected void adjustCountWithLimitSubQuery(final DbQuery<PK> subQuery, final Map<String, Object> mpParameters) {
	}

	protected DbQuery<Object[]> createQuery(List<IColumnToVOMapping<?, PK>> columns,
											Collection<IColumnWithMdToVOMapping<?, PK>> referenceColumnsAsSubselect,
											boolean bAddJoins,
											Set<String> queryHints) {
		DbQuery<Object[]> query = createQueryImpl(Object[].class);
		DbFrom<PK> from = query.from(getMetaData(), false);
		adjustFrom(from);

		List<IColumnToVOMapping<?, PK>> referenceColumns = getReferenceColumns(columns);
		if (referenceColumnsAsSubselect == null) {
			referenceColumnsAsSubselect = getSubselectableReferenceColumns(referenceColumns);
			referenceColumns.removeAll(referenceColumnsAsSubselect);
		}
		if (bAddJoins) {
			addJoins(query, getJoinsForReferenceColumns(referenceColumns));
		}

		List<DbExpression<?>> selections = new ArrayList<>();
		for (IColumnToVOMapping<?, PK> column : columns) {
			DbExpression<?> dbExpression = column.getDbColumn(from, getMetaProvider());

			if (referenceColumnsAsSubselect.contains(column)) {
				final boolean useExprSqlForCond = RigidUtils.equal(column.getUID(), SFE.OWNER.getUID(getMetaData()));
				DbExpression<?> dbSubselectExpression = new DbReferencedSubselectExpression(dbExpression, from, (IColumnWithMdToVOMapping<?, PK>)column, useExprSqlForCond, getMetaProvider());
				selections.add(dbSubselectExpression);

			} else {
				selections.add(column.getDbColumn(from, getMetaProvider()));
			}
		}

		query.multiselect(selections);
		return query;
	}

	protected void addJoins(DbQuery<Object[]> query, List<DalJoin> joins) {
		final DbFrom<Object> from = query.getDbFrom();
		for (DalJoin join : joins) {
			final FieldMeta<?> field = join.getField();
			final UID refEntityUID = RigidUtils.firstNonNull(field.getForeignEntity(), field.getUnreferencedForeignEntity());
			if (getMetaProvider().checkEntity(refEntityUID)) {
				final EntityMeta<Object> refEntity = getMetaProvider().getEntity(refEntityUID);
				if (!from.containsAlias(join.getTableAliasRight())) {
					if (from.getAlias().equals(join.getTableAliasLeft())) {
						from.join(refEntity, JoinType.LEFT, join.getTableAliasRight()).on(
								SimpleDbField.create(DbUtils.getDbIdFieldName(field, refEntity.isUidEntity()),
										RigidUtils.uncheckedCast(refEntity.isUidEntity() ? UID.class : Long.class)),
								RigidUtils.uncheckedCast(refEntity.isUidEntity() ? SF.PK_UID : SF.PK_ID));
					} else if (!from.getAlias().equals(join.getTableAliasRight())) {
						DbJoin<?> joinForNextJoin = from.getJoins().stream()
								.filter(j -> j.getAlias().equals(join.getTableAliasLeft()))
								.findFirst().orElseThrow(() ->
										new NuclosFatalException(String.format("Join for alias \"%s\" does not exist", join.getTableAliasLeft())));
						joinForNextJoin.join(refEntity, JoinType.LEFT, join.getTableAliasRight()).on(
								SimpleDbField.create(DbUtils.getDbIdFieldName(field, refEntity.isUidEntity()),
										RigidUtils.uncheckedCast(refEntity.isUidEntity() ? UID.class : Long.class)),
								RigidUtils.uncheckedCast(refEntity.isUidEntity() ? SF.PK_UID : SF.PK_ID));
					}
				}
			}
		}
	}

	private List<IColumnToVOMapping<?, PK>> getReferenceColumns(final List<IColumnToVOMapping<?, PK>> columns) {
		final List<IColumnToVOMapping<?, PK>> result = new ArrayList<>();

		for (IColumnToVOMapping<?, PK> m : columns) {
			boolean add = false;
			final String f = m.getColumn();
			if (m instanceof IColumnWithMdToVOMapping) {
				final IColumnWithMdToVOMapping<?, PK> mapping = (IColumnWithMdToVOMapping<?, PK>) m;
				final FieldMeta<?> meta = mapping.getMeta();
				final UID fentity = RigidUtils.firstNonNull(meta.getForeignEntity(), meta.getUnreferencedForeignEntity());
				if (fentity != null) {
					add = true;
				}
			}
			if (add) {
				result.add(m);
			}
		}
		return result;
	}

	private Collection<IColumnWithMdToVOMapping<?, PK>> getSubselectableReferenceColumns(
			List<IColumnToVOMapping<?, PK>> referenceColumns) {
		Collection<IColumnWithMdToVOMapping<?, PK>> ret = new HashSet<>();

		for (IColumnToVOMapping<?, PK> column : referenceColumns) {
			if (column instanceof IColumnWithMdToVOMapping) {
				IColumnWithMdToVOMapping<?, PK> col = RigidUtils.uncheckedCast(column);
				if (DalJoinUtils.getJoins(col.getMeta(), getMetaProvider()).size() == 1) {
					ret.add(col);
				}
			}
		}

		return ret;
	}

	protected List<DalJoin> getJoinsForReferenceColumns(final List<IColumnToVOMapping<?, PK>> columns) {
		final List<DalJoin> result = new ArrayList<>();
		if (getMetaData().getReadDelegate() != null) {
			return result;
		}

		for (IColumnToVOMapping<?, PK> m : columns) {
			String alias;
			if (m instanceof IColumnWithMdToVOMapping) {
				final IColumnWithMdToVOMapping<?, PK> mapping = (IColumnWithMdToVOMapping<?, PK>) m;
				final FieldMeta<?> meta = mapping.getMeta();
				if (meta.isCalculated()) {
					continue;
				}
				alias = TableAliasSingleton.getInstance().getAlias(meta);
			}
			// ???
			else {
				alias = TableAliasSingleton.getInstance().getAlias(m);
			}

			LOG.debug("getJoinsForColumns: apply RefJoinCondition for {} alias {}", m, alias);
			result.addAll(DalJoinUtils.getJoins(m, getMetaProvider()));
		}
		return result;
	}

   protected <S, T> DbMap getColumnValuesMap(List<IColumnToVOMapping<?, PK>> columns, DalVO dalVO) {
      DbMap map = new DbMap();
      for (Entry<IColumnToVOMapping<?, PK>, Object> entry : getColumnValuesMapWithMapping(columns, dalVO).entrySet()) {
    	  DbField<T> dbField = (DbField<T>) entry.getKey();
    	  if ((T) entry.getValue() == null) {
    		  map.put(dbField, new DbNull<T>(dbField.getJavaClass()));
    	  } else {
    		  map.put(dbField, (T) entry.getValue());
    	  }

      }
      return map;
   }

   protected <S> Map<IColumnToVOMapping<?, PK>, Object> getColumnValuesMapWithMapping(List<IColumnToVOMapping<?, PK>> columns, DalVO dalVO) {
		final Map<IColumnToVOMapping<?, PK>, Object> map = new LinkedHashMap<>();
		final boolean bUpdate = !dalVO.isFlagNew() && dalVO.isFlagUpdated();

		for (IColumnToVOMapping<?, PK> column : columns) {
			if (column.isReadonly()) {
				continue;
			}

			// also ignore stringified references
			if (column instanceof ColumnToRefFieldVOMapping) {
				final FieldMeta<?> meta = ((ColumnToRefFieldVOMapping<?, PK>) column).getMeta();
				if (meta.isReadonly()) {
					continue;
				}
				if (meta.getForeignEntity() != null) {
					continue;
				}
				if (meta.getLookupEntity() != null) {
					continue;
				}
				if (meta.getUnreferencedForeignEntity() != null) {
					continue;
				}
			} else if (bUpdate && column instanceof ColumnToBeanVOMapping) {
				final String col = column.getColumn();
				if (SF.PK_ID.getDbColumn().equals(col) || SF.PK_UID.getDbColumn().equals(col) || SF.CREATEDAT.getDbColumn().equals(col)) {
					continue;
				}
				if (DalUtils.isVersionUpdateDisabled()) {
					if (SF.VERSION.getDbColumn().equals(col)) {
						continue;
					}
				} else {
					if (SF.CREATEDBY.getDbColumn().equals(col)) {
						continue;
					}
				}
			}

			map.put(column, column.convertFromDalFieldToDbValue(dalVO, getDbValueConverter()));
		}
		return map;
	}

   private DbMap getPrimaryKeyMap(PK pk) {
	   final DbMap result = new DbMap();
	   result.put(getPrimaryKeyColumn(), pk);
	   return result;
   }

   protected Transformer<Object[], DalVO> createResultTransformer(final JdbcTransformerParams params, List<IColumnToVOMapping<?, PK>> columns) {
	   return getResultTransformer(params, columns.toArray(new IColumnToVOMapping[columns.size()]));
   }

	protected Transformer<Object[], DalVO> createResultTransformer(final List<IColumnToVOMapping<?,PK>> columns) {
		return this.createResultTransformer(new JdbcTransformerParams(), columns);
   }

   protected <S> Transformer<Object[], DalVO> getResultTransformer(final JdbcTransformerParams params, final IColumnToVOMapping<Object, PK>... columns) {
		return result -> {
			try {
				final DalVO dalVO = newDalVOInstance();
				for (int i = 0, n = columns.length; i < n; i++) {
					final IColumnToVOMapping<Object, PK> column = columns[i];
					final Object value = transformValue(result, i, column, columns);
					column.convertFromDbValueToDalField(dalVO, value, new FromDbValueConversionParams(params.includeThumbnailsOnly()), getMetaProvider(), getDbValueConverter());
				}

				dalVO.processor(getProcessor());
				setDebugInfo(dalVO);
				return dalVO;
			} catch (Exception e) {
				throw new CommonFatalException(e);
			}
		};
	}

	protected Object transformValue(final Object[] dsResult, final int i, final IColumnToVOMapping<Object, PK> column, final IColumnToVOMapping<Object, PK>[] columns) {
		Object value = dsResult[i];
		if (!getMetaData().isDynamic()) {
			if (column instanceof ColumnToRefFieldVOMapping && i > 0 && columns[i - 1].getUID().equals(column.getUID())) {
				if (value == null && dsResult[i - 1] instanceof Long) {
					value = DbAccess.getMaskedId((Long) dsResult[i - 1]); // Show ID when display-string is null
				}
			} else if (column instanceof ColumnToFieldVOMapping && i < columns.length - 1
					&& columns[i + 1] instanceof ColumnToBeanVOMapping
					&& columns[i + 1].getUID().equals(column.getUID())) {
				if (value == null && dsResult[i + 1] instanceof Long) {
					value = DbAccess.getMaskedId((Long) dsResult[i + 1]); // Show ID when display-string is null
				}
			}
		}
		return value;
	}

   	protected void setDebugInfo(DalVO dalVO) {
   	}

	protected NuclosLogicalUniqueViolationException checkLogicalUniqueConstraint(
			final Map<IColumnToVOMapping<?, PK>, Object> values, final PK id) {
		DbQuery<Long> query = createQueryImpl(Long.class);
		DbFrom<PK> from = query.from(getMetaData());
		adjustFrom(from);
		query.select(query.getBuilder().countRows());
		List<DbCondition> conditions = new ArrayList<>();

		boolean bFullIsNullCondition = true;
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		for (Map.Entry<IColumnToVOMapping<?, PK>, Object> e : values.entrySet()) {
			Object value = e.getValue();
			DbExpression<Object> c = (DbExpression<Object>) e.getKey().getDbColumn(from, getMetaProvider());
			if (DbNull.isNull(value)) {
				conditions.add(builder.isNull(c));
			} else {
				conditions.add(builder.equalValue(c, value));
				bFullIsNullCondition = false;
			}
		}

		if (bFullIsNullCondition) {
			// If all unique key fields are null, no exception is thrown (Reference: Oracle)
			return null;
		}

		query.where(builder.and(conditions.toArray(new DbCondition[conditions.size()])));
		Long count = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
		if (count > 1L) {
			return new NuclosLogicalUniqueViolationException("Unique constraint violated in query '"
					+ query + "' with id=" + id + ", number of result is " + count);
		}
		return null;
	}

	private List<String> getLogStatements(DbStatement stmt) {
		List<String> statements = null;
		try {
			final DbAccess dbAccess = dataBaseHelper.getDbAccess();
			final IBatch batch = dbAccess.getBatchFor(stmt);
			statements = dbAccess.getStatementsForLogging(batch);
		} catch (SQLException e) {
			LOG.warn("getLogStatements failed", e);
		}
		return statements;
	}

	public void addToColumns(IColumnToVOMapping<?, PK> column) {
		if (allColumnsAsSet.add(column)) {
			allColumns.add(column);
		}
	}

	public void setAllColumns(List<IColumnToVOMapping<?, PK>> columns) {
		allColumns.clear();
		allColumns.addAll(columns);

		allColumnsAsSet.clear();
		allColumnsAsSet.addAll(columns);

		checkColumns();
	}

	protected DbException transformInsertOrUpdateException(DbException ex, final DalVO dalVO) {
		return ex;
	}

	protected DbException transformDeleteException(DbException ex, final PK pk) {
		return ex;
	}

	protected void adjustFrom(DbFrom<PK> from) {}

	protected void adjustFromForCount(DbFrom<PK> from) {}
}
