//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common.dal.DalJoin;
import org.nuclos.server.dal.processor.IColumnToVOMapping;

public class DalJoinUtils {

	public static List<DalJoin> getJoins(IColumnToVOMapping<?, ?> mapping, IRigidMetaProvider metaProv) {
		final FieldMeta<?> meta = TableAliasSingleton.getMeta(mapping);
		return getJoins(meta, metaProv);
	}

	public static List<DalJoin> getJoins(FieldMeta<?> meta, IRigidMetaProvider metaProv) {
		return getJoins(meta, "t", metaProv);
	}

	public static List<DalJoin> getJoins(FieldMeta<?> meta, boolean isSearch, IRigidMetaProvider metaProv) {
		return getJoins(meta, "t", isSearch, metaProv);
	}

	public static List<DalJoin> getJoins(FieldMeta<?> meta, String tableAliasLeft, IRigidMetaProvider metaProv) {
		return getJoins(meta, tableAliasLeft, false, metaProv);
	}

	public static List<DalJoin> getJoins(FieldMeta<?> meta, String tableAliasLeft, boolean isSearch, IRigidMetaProvider metaProv) {
		final List<DalJoin> result = new ArrayList<>();
		final String tableAliasRight = TableAliasSingleton.getInstance().getAlias(meta);

		result.add(new DalJoin(meta, tableAliasLeft, tableAliasRight));

		final Iterator<IFieldUIDRef> it = new ForeignEntityFieldUIDParser(meta, metaProv, isSearch).iterator();
		while (it.hasNext()) {
			IFieldUIDRef refPart = it.next();
			if (refPart.isUID()) {
				FieldMeta<?> metaRef = metaProv.getEntityField(refPart.getUID());
				if (metaRef.getForeignEntity() != null || metaRef.getUnreferencedForeignEntity() != null) {
					final String tableAliasRef = TableAliasSingleton.getInstance().getAlias(meta, metaRef);
					result.add(new DalJoin(metaRef, tableAliasRight, tableAliasRef));
				}
			}
		}
		return result;
	}

}
