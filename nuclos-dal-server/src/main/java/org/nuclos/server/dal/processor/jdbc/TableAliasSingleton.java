//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IColumnWithMdToVOMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * A singleton for defining the table join aliases for 'stringified' references.
 * <p>
 * This is part of the effort to deprecate all views in Nuclos.
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.2.01
 */
@Component
public class TableAliasSingleton {
	
	private static final Logger LOG = LoggerFactory.getLogger(TableAliasSingleton.class);
	
	private static final String ALIAS_RECORDGRANT = "a_nuclosRgt809";
	
	private static TableAliasSingleton INSTANCE;
	
	//
	
	private final AtomicInteger AI = new AtomicInteger(0);
	
	private final Map<String, String> TABLE_ALIASES = new ConcurrentHashMap<>();
	

	private TableAliasSingleton() {
		INSTANCE = this;
	}
	
	public static TableAliasSingleton getInstance() {
		return INSTANCE;
	}
	
	public final String getRecordGrantAlias() {
		return ALIAS_RECORDGRANT;
	}
	
	/**
	 * @deprecated Try to avoid this and use {@link #getAlias(IColumnWithMdToVOMapping)} instead.
	 */
	public String getAlias(IColumnToVOMapping<?, ?> mapping) {
		final FieldMeta<?> meta = getMeta(mapping);
		return getAlias(meta);
	}
	
	public String getAlias(IColumnWithMdToVOMapping<?, ?> mapping) {
		final FieldMeta<?> meta = mapping.getMeta();
		return getAlias(meta);
	}

	static FieldMeta<?> getMeta(IColumnToVOMapping<?, ?> mapping) {
		final FieldMeta<?> meta;
		if (mapping instanceof IColumnWithMdToVOMapping) {
			meta = ((IColumnWithMdToVOMapping<?, ?>) mapping).getMeta();
		}
		else {
			throw new IllegalArgumentException("Unexpected column mapping: " + mapping + " of " + mapping.getClass().getName());
		}
		return meta;
	}

	public String getAlias(FieldMeta<?> meta, String upperAlias){
		String key = "";
		upperAlias = upperAlias != null ? upperAlias : "";
		
		final UID fentity = RigidUtils.firstNonNull(meta.getForeignEntity(), meta.getUnreferencedForeignEntity(), meta.getLookupEntity());
		if (fentity == null) {
			throw new IllegalArgumentException("Field " + meta + " is not a reference to a foreign entity");
		}
	
		key += meta.getUID().getString();
		key += upperAlias;
		
		String alias = TABLE_ALIASES.get(key);
		if (alias == null) {
			// alias is only build for the first field, that is enough
			alias = StringUtils.makeSQLIdentifierFrom("a_", meta.getUID().getString(), upperAlias, Integer.toString(AI.incrementAndGet()));
			if (RigidUtils.equal(getRecordGrantAlias(), alias)) {
				throw new IllegalArgumentException("Field " +  meta.getUID().getString() + " returns alias of internal field record grant.");
			}
			TABLE_ALIASES.put(key, alias);
		}
		
		TABLE_ALIASES.put(key, alias);		
		return alias;
	}
	
	public String getAlias(FieldMeta<?> ... metas) {
		String key = "";
		FieldMeta<?> firstField = null;
		
		for (FieldMeta<?> meta : metas) {
			final UID fentity = RigidUtils.firstNonNull(meta.getForeignEntity(), meta.getUnreferencedForeignEntity(), meta.getLookupEntity());
			if (fentity == null) {
				throw new IllegalArgumentException("Field " + meta + " is not a reference to a foreign entity");
			}
			
			if (firstField == null) {
				firstField = meta;
			}
			
			//For System-field-references (Process, State) there is no need to create a new table alias each time.
			//Note: This automatically solves the problem with the two joins for StateNumber and State
			if (SF.isEOField(meta.getEntity(), meta.getUID())) {
				key += fentity.getString();
				continue;
			}

			if (meta.getUID() == null) {
				//field in creation (NUCLOS-8530)
				key += meta.getFieldName();
				continue;
			}
			
			key += meta.getUID().getString();
		}
		
		String alias = TABLE_ALIASES.get(key);
		if (alias == null) {
			// alias is only build for the first field, that is enough
			alias = StringUtils.makeSQLIdentifierFrom("a_", firstField.getFieldName(), Integer.toString(AI.incrementAndGet()));
			if (RigidUtils.equal(getRecordGrantAlias(), alias)) {
				throw new IllegalArgumentException("Field " + firstField + " returns alias of internal field record grant.");
			}
			TABLE_ALIASES.put(key, alias);
		}
		
		TABLE_ALIASES.put(key, alias);		
		return alias;
	}

}
