//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.nuclos.api.context.SaveFlag;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosLogicalUniqueViolationException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dal.processor.ColumnToFieldVOMapping;
import org.nuclos.server.dal.processor.ColumnToRefFieldVOMapping;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.ProcessorConfiguration;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcWithFieldsDalProcessor;
import org.nuclos.server.dal.processor.nuclos.JdbcEntityObjectProcessor;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.statements.DbMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * TODO: @Autowired (and @Configurable ?) do not work here - but why?
 * Instead we hard-wire in ProcessorFactorySingleton at present. (tp)
 */
public abstract class EntityObjectProcessor<PK>
		extends AbstractJdbcWithFieldsDalProcessor<EntityObjectVO<PK>, PK>
		implements JdbcEntityObjectProcessor<PK> {

	private static final Logger LOG = LoggerFactory.getLogger(EntityObjectProcessor.class);

	private final Collection<List<IColumnToVOMapping<?, PK>>> logicalUniqueConstraintCombinations = new ArrayList<>();

	protected final EntityMeta<PK> eMeta;

	private final IColumnToVOMapping<PK, PK> pkColumn;
	private final IColumnToVOMapping<Integer, PK> versionColumn;

	private final ThreadLocal<Boolean> thinReadEnabled = new ThreadLocal<>();

	private Integer iDefaultCache = null;
	private Integer iDefaultThinCache = null;

	public EntityObjectProcessor(ProcessorConfiguration<EntityObjectVO<PK>, PK> config) {
		super(config.getMetaData().getUID(),
				config.getDalType(),
				config.getPkColumn() == null ? null : config.getPkColumn().getJavaClass(),
				config.getAllColumns());

		this.eMeta = config.getMetaData();
		this.pkColumn = config.getPkColumn();
		this.versionColumn = config.getVersionColumn();
		setCachedObjectBuilder(config.getCachedObjectBuilder());

		/**
		 * Logical Unique Constraints
		 */
		if (eMeta.getLogicalUniqueFieldCombinations() != null) {
			for (UID[] lufCombination : eMeta.getLogicalUniqueFieldCombinations()) {
				List<IColumnToVOMapping<?, PK>> ctovMappings =
						new ArrayList<>(lufCombination.length);

				for (UID luField : lufCombination) {
					for (IColumnToVOMapping<?, PK> ctovMap : allColumns) {
						if (!ctovMap.isReadonly() && luField.equals(ctovMap.getUID())) {
							ctovMappings.add(ctovMap);
						}
					}
				}
				logicalUniqueConstraintCombinations.add(ctovMappings);
			}
		}
	}

	@Override
	public EntityMeta<PK> getMetaData() {
		return eMeta;
	}

	@Override
	protected IColumnToVOMapping<PK, PK> getPrimaryKeyColumn() {
		return pkColumn;
	}

	/* BLOCK A: Insert/Update/Delete - Block */

	@Override
	public DalCallResult insertWithSelect(final DbMap fieldsMap, final DbMap fieldConditionMap, final EntityMeta<?> metaEntitySource) {
		final DalCallResult result = super.insertWithSelect(fieldsMap, fieldConditionMap, metaEntitySource);
		return result;
	}

	@Override
	public Object insertOrUpdate(EntityObjectVO<PK> dalVO) throws DbException {
		return this.insertOrUpdateWithOrWithoutForce(dalVO, false);
	}

	/**
	 * @param dalVO
	 * @param force true: do not check logical unique constraints
	 *              false: check!
	 */
	public Object insertOrUpdateWithOrWithoutForce(EntityObjectVO<PK> dalVO, boolean force, SaveFlag... saveFlags) {
		Object pk;
		if (SaveFlag.PATCH.isIn(saveFlags) && dalVO.isFlagUpdated()) {
			List<IColumnToVOMapping<?, PK>> pColumns = allColumns.stream().filter(k -> dalVO.exist(k.getUID())).collect(Collectors.toList());
			pk = super.insertOrUpdateImpl(pColumns, dalVO, null);
		} else {
			pk = super.insertOrUpdate(dalVO);
		}

		if (!force) {
			final DalCallResult result = new DalCallResult();
			checkLogicalUniqueConstraint(result, dalVO);
			result.throwFirstException();
		}
		return pk;
	}

	@Override
	public void delete(Delete<PK> id) throws DbException {
		super.delete(id);
	}

	private void checkLogicalUniqueConstraint(DalCallResult result, EntityObjectVO<PK> dalVO) {
		if (!logicalUniqueConstraintCombinations.isEmpty()) {
			for (List<IColumnToVOMapping<?, PK>> columns : logicalUniqueConstraintCombinations) {
				final Map<IColumnToVOMapping<?, PK>, Object> checkValues = super.getColumnValuesMapWithMapping(columns, dalVO);
				final NuclosLogicalUniqueViolationException checkResult = super.checkLogicalUniqueConstraint(checkValues, dalVO.getPrimaryKey());
				if (checkResult != null)
					result.addDbException(dalVO.getPrimaryKey(), null, checkResult);
			}
		}
	}

	@Override
	public EntityObjectVO<PK> getByPrimaryKey(PK id) {
		return getByPrimaryKey(id, null);
	}

	@Override
	public EntityObjectVO<PK> getByPrimaryKey(PK id, Collection<FieldMeta<?>> fields) {
		return CollectionUtils.getSingleIfExist(executeQueryFor(Collections.singletonList(id), fields));
	}

	@Override
	public List<EntityObjectVO<PK>> getByPrimaryKeys(List<PK> pks) {
		return executeQueryFor(pks, null);
	}

	@Override
	public List<EntityObjectVO<PK>> getAll() {
		return executeQueryFor(null, null);
	}

	public List<EntityObjectVO<PK>> executeQueryFor(List<PK> pks, Collection<FieldMeta<?>> fields) {
		final boolean bWithDefaultColumns = fields == null;
		List<IColumnToVOMapping<?, PK>> columns = bWithDefaultColumns ? getDefaultColumns() : getColumnsByMeta(fields);

		if (fields != null && !columns.contains(pkColumn)) {
			columns.add(pkColumn);
		}

		final boolean bHasAllColumns = hasAllColumns(columns);
		final DbQuery<Object[]> query = createQuery(columns, null, true, null);
		final Transformer<Object[], EntityObjectVO<PK>> transformer = createResultTransformer(columns);
		final Integer iCache = getCache(bWithDefaultColumns, columns);

		List<EntityObjectVO<PK>> result;

		if (pks != null) {
			result = super.getByPrimaryKeys(iCache, pks);
			if (result != null) {
				final Set<PK> cachedPks = result.parallelStream()
						.map(HasPrimaryKey::getPrimaryKey)
						.collect(Collectors.toSet());
				pks = pks.parallelStream()
						.filter(pk -> !cachedPks.contains(pk))
						.collect(Collectors.toList());
			}

			if (result == null || result.isEmpty()) {
				result = new ArrayList<>(pks.size());
			} else {
				result = new ArrayList<>(result);
			}

			DbFrom<PK> from = (DbFrom<PK>) CollectionUtils.getFirst(query.getRoots());
			DbExpression<?> pkExpr = getPrimaryKeyColumn().getDbColumn(from, getMetaProvider());
			for (List<PK> pkSubList : CollectionUtils.splitEvery(pks, query.getBuilder().getInLimit())) {
				query.replaceWhere(pkExpr.as(getPkType()).in(pkSubList));
				final List<EntityObjectVO<PK>> eosFromDb = dataBaseHelper.getDbAccess().executeQuery(query, transformer);
				result.addAll(storeInCache(iCache, eosFromDb));
			}
		} else {
			result = super.getAll(iCache);
			if (result == null) {
				result = new ArrayList<>(storeAllInCache(iCache, dataBaseHelper.getDbAccess().executeQuery(query, transformer)));
			}
		}

		result.parallelStream().forEach(eo -> eo.setComplete(bHasAllColumns));
		return result;
	}

	@Override
	public List<PK> getAllIds() {
		DbQuery<PK> query = createSingleColumnQuery(getPrimaryKeyColumn(), true);
		return dataBaseHelper.getDbAccess().executeQuery(query);
	}

	@Override
	public Integer getVersion(PK pk) {
		if (pk == null || versionColumn == null)
			return null;

		final EntityObjectVO<PK> eoFromCache = super.getByPrimaryKey(getDefaultCache(), pk);
		if (eoFromCache != null) {
			return eoFromCache.getVersion();
		}

		DbQuery<Integer> query = createSingleColumnQuery(versionColumn, true);
		DbFrom<PK> from = (DbFrom<PK>) CollectionUtils.getFirst(query.getRoots());
		query.where(query.getBuilder().equalValue(getPrimaryKeyColumn().getDbColumn(from, getMetaProvider()), pk));
		try {
			return dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
		} catch (DbInvalidResultSizeException e) {
			if (e.wasEmpty()) {
				throw new CommonFatalException("No record with pk " + pk + " in table " + eMeta.getDbTable());
			} else {
				throw new CommonFatalException("Primary key is not unique!");
			}
		}
	}

	@Override
	protected Integer getDefaultCache() {
		if (!isCached()) {
			return null;
		}
		if (isThinReadEnabled()) {
			if (iDefaultThinCache == null) {
				iDefaultThinCache = getCache(getDefaultColumns());
			}
			return iDefaultThinCache;
		} else {
			if (iDefaultCache == null) {
				iDefaultCache = getCache(getDefaultColumns());
			}
			return iDefaultCache;
		}
	}

	@Override
	protected List<IColumnToVOMapping<?, PK>> getDefaultColumns() {
		final boolean thinReadEnabled = isThinReadEnabled();
		return getDefaultColumns(thinReadEnabled, thinReadEnabled);
	}

	protected List<IColumnToVOMapping<?, PK>> getDefaultColumns(final boolean skipCalculated, final boolean skipStringifiedReferences) {
		List<IColumnToVOMapping<?, PK>> result = new ArrayList<>(allColumns.size());

		for (IColumnToVOMapping<?, PK> col : allColumns) {
			boolean add = true;

			if (col instanceof ColumnToFieldVOMapping) {
				FieldMeta<?> fieldMeta = ((ColumnToFieldVOMapping<?, PK>) col).getMeta();

				if (fieldMeta.isCalcOndemand()) {
					add = false;
				} else if (skipCalculated && fieldMeta.isCalculated()) {
					add = false;
				}
			} else if (col instanceof ColumnToRefFieldVOMapping) {
				if (skipStringifiedReferences) {
					add = false;
				}
			}

			if (add) {
				result.add(col);
			}
		}
		return result;
	}

	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<PK> dalVO) throws DbException {
		final DalCallResult result = new DalCallResult();
		checkLogicalUniqueConstraint(result, dalVO);
		result.throwFirstException();
	}

	protected List<IColumnToVOMapping<?, PK>> getColumnsByMeta(final Collection<FieldMeta<?>> fields) {
		Collection<UID> fieldUIDs = new ArrayList<>();
		for (FieldMeta<?> fMeta : fields) {
			fieldUIDs.add(fMeta.getUID());
		}
		return getColumns(fieldUIDs);
	}

	protected List<IColumnToVOMapping<?, PK>> getColumns(final Collection<UID> fields) {
		final Set<UID> notFound = new HashSet<>(fields);
		List<IColumnToVOMapping<?, PK>> result =
				CollectionUtils.select(new ArrayList<>(allColumns), (IColumnToVOMapping<?, PK> column) -> {
					notFound.remove(column.getUID());
					return fields.contains(column.getUID());
				});
		for (UID nfUID : notFound) {
			LOG.error(String.format("Column with ID %s not found for entity %s", nfUID.getString(), getEntityUID().getString()));
		}
		return result;
	}

	protected boolean hasAllColumns(List<IColumnToVOMapping<?, PK>> selectedColumns) {
		Set<UID> selUIDs = new HashSet<UID>();
		for (IColumnToVOMapping<?, PK> selCol : selectedColumns) {
            if (selCol != null && selCol.getUID() != null) {
			    selUIDs.add(selCol.getUID());
            }
		}
		for (IColumnToVOMapping<?, PK> defCol : getDefaultColumns(false, false)) {
			if (!selUIDs.contains(defCol.getUID())) {
				return false;
			}
		}
		return true;
	}

	protected Integer getCache(final boolean bWithDefaultColumns, final List<IColumnToVOMapping<?, PK>> columns) {
		if (!isCached() || columnsContainsRecordGrant(columns)) {
			// No caching
			return null;
		}
		return bWithDefaultColumns ? getDefaultCache() : getCache(columns);
	}

	@Override
	public void setThinReadEnabled(boolean enabled) {
		synchronized (thinReadEnabled) {
			if (eMeta.isThin() && enabled && !Boolean.TRUE.equals(thinReadEnabled.get())
					&& TransactionSynchronizationManager.isSynchronizationActive()) {
				// Security fallback only...
				TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
					@Override
					public void afterCompletion(int status) {
						synchronized (thinReadEnabled) {
							thinReadEnabled.set(false);
						}
					}
				});
			}
			thinReadEnabled.set(eMeta.isThin() && enabled);
		}
	}

	@Override
	public boolean isThinReadEnabled() {
		return Boolean.TRUE.equals(thinReadEnabled.get());
	}

	@Override
	protected void adjustFrom(final DbFrom<PK> from) {
		// do nothing
	}
}
