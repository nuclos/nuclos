//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.provider;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.CachedEntityObjectVO;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dal.processor.AbstractProcessorFactory;
import org.nuclos.server.dal.processor.CachedObjectBuilder;
import org.nuclos.server.dal.processor.IDbValueConverter;
import org.nuclos.server.dal.processor.ProcessorConfiguration;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dal.processor.jdbc.impl.EntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclos.IEntityObjectProcessor;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.stereotype.Component;

@Component
public class NuclosDalProvider extends AbstractProcessorFactory {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosDalProvider.class);

	private static final ConcurrentMap<UID, Object> BLOCKER = new ConcurrentHashMap<>();

	// Spring injection

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private TableAliasSingleton tableAliasSingleton;

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	// end of Spring injection

	NuclosDalProvider() {}

	protected TableAliasSingleton getTableAliasSingleton() {
		return tableAliasSingleton;
	}

	//@Cacheable annotation does not work during startup. Workaround via cacheManager...
	//@Cacheable(value = "nuclosDalProviderAllEntityMetas")
	public Map<UID, EntityMeta<?>> getAllNuclosEntityMetasMap() {
		Map<UID, EntityMeta<?>> result = null;
		Cache.ValueWrapper valueWrapper = cacheManager.getCache("nuclosDalProviderAllEntityMetas").get("");
		if (valueWrapper != null) {
			result = (Map<UID, EntityMeta<?>>) valueWrapper.get();
		}
		if (result == null) {
			result = createAllNuclosEntityMetasMap();
			cacheManager.getCache("nuclosDalProviderAllEntityMetas").put("", result);
		}
		return result;
	}

	private Map<UID, EntityMeta<?>> createAllNuclosEntityMetasMap() {
		ConcurrentMap<UID, EntityMeta<?>> result = new ConcurrentHashMap<>();

		/**
		 * Nuclos Entities
		 */
		E.getAllEntities().parallelStream().forEach(m -> result.put(m.getUID(), m));

		return Collections.unmodifiableMap(result);
	}

	public <PK> IEntityObjectProcessor<PK> getEntityObjectProcessor(@NotNull EntityMeta<PK> entity) {
		return getEntityObjectProcessor(entity.getUID());
	}

	//@Cacheable annotation does not work during startup. Workaround via cacheManager...
	//@Cacheable(value = "nuclosDalProviderEntityObjectProcessor", key = "#entity.getString()")
	public <PK> IEntityObjectProcessor<PK> getEntityObjectProcessor(@NotNull UID entity) {
		IEntityObjectProcessor<PK> result = RigidUtils.uncheckedCast(Optional.ofNullable(cacheManager
						.getCache("nuclosDalProviderEntityObjectProcessor")
						.get(entity.getString())).map(Cache.ValueWrapper::get)
						.orElse(null));
		if (result == null) {
			synchronized (syncObject(entity)) { // only sync creation and check if created already...
				result = RigidUtils.uncheckedCast(Optional.ofNullable(cacheManager
						.getCache("nuclosDalProviderEntityObjectProcessor")
						.get(entity.getString())).map(Cache.ValueWrapper::get)
						.orElse(null));
				if (result == null) {
					result = createEntityObjectProcessor(entity);
					EntityObjectProcessor<PK> eoProc = (EntityObjectProcessor<PK>) result;
					eoProc.setDataBaseHelper(dataBaseHelper);
					cacheManager.getCache("nuclosDalProviderEntityObjectProcessor").put(entity.getString(), result);
				}
			}
		}
		return result;
	}

	private <PK> IEntityObjectProcessor<PK> createEntityObjectProcessor(@NotNull UID entity) {
		final EntityMeta<PK> eMeta = RigidUtils.uncheckedCast(getAllNuclosEntityMetasMap().get(entity));
		final Class<EntityObjectVO<PK>> type = RigidUtils.getGenericClass(EntityObjectVO.class);

		if (eMeta == null) {
			throw new CommonFatalException(String.format("Entity for UID \"%s\" not found", entity));
		}
		if (eMeta.isProxy() || eMeta.isDatasourceBased()) {
			throw new NotImplementedException(String.format("Entity \"%s\" is not supported", eMeta.getEntityName()));
		}
		try {
			final ProcessorConfiguration<EntityObjectVO<PK>, PK> config = newProcessorConfiguration(
					type,
					eMeta,
					eMeta.getFields(),
					true,
					createCachedObjectBuilder(eMeta));
			return new EntityObjectProcessor<PK>(config) {
				@Override
				protected IRigidMetaProvider getMetaProvider() {
					return SYSTEM_META_PROVIDER;
				}

				@Override
				protected IDbValueConverter getDbValueConverter() {
					return null;
				}
			};
		} catch (Exception ex) {
			throw new CommonFatalException(ex);
		}
	}

	@Caching(evict = {
			@CacheEvict(value = "nuclosDalProviderEntityObjectProcessor", allEntries = true)
	})
	public <PK> void invalidate() {
		LOG.debug("Invalidating NuclosDalProvider");
		ConcurrentMapCache cache = RigidUtils.uncheckedCast(cacheManager.getCache("nuclosDalProviderEntityObjectProcessor"));
		Map<String, IEntityObjectProcessor<?>> nativeCache = RigidUtils.uncheckedCast(cache.getNativeCache());
		for (IEntityObjectProcessor<?> eoProc : nativeCache.values()) {
			try {
				eoProc.destroy();
			} catch (Exception e) {
				LOG.error(String.format("Error destruction of EntityObjectProcessor for entity \"%s\": %s", eoProc.getEntityUID(), e.getMessage()), e);
			}
		}
	}

	public static <PK> CachedObjectBuilder<EntityObjectVO<PK>, PK> createCachedObjectBuilder(EntityMeta<PK> eMeta) {
		return eMeta.isCacheable() && !eMeta.isOwner() && !eMeta.IsLocalized() ?  new CachedObjectBuilder<EntityObjectVO<PK>, PK>() {
			@Override
			public EntityObjectVO<PK> buildCachedObject(final EntityObjectVO<PK> dalVO) {
				return new CachedEntityObjectVO<>(dalVO);
			}
		} : null;
	}

	private static Object syncObject(UID entityUID) {
		return BLOCKER.computeIfAbsent(entityUID, key -> new UID());
	}

}
