//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.specification;

import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.server.dal.processor.DalChangeNotificationForTransactionSync;
import org.nuclos.server.dal.processor.DalChangeNotification;
import org.springframework.beans.factory.DisposableBean;

import io.reactivex.rxjava3.subjects.PublishSubject;

public interface IDalObservableSpecification<DalVO extends IDalVO<PK>, PK> extends DisposableBean {

	PublishSubject<DalChangeNotification<DalVO, PK>> observeAllChangesImmediately(boolean bNotifyClusterWide);
	PublishSubject<DalChangeNotification<DalVO, PK>> observeInsertOnlyImmediately(boolean bNotifyClusterWide);
	PublishSubject<DalChangeNotification<DalVO, PK>> observeUpdateOnlyImmediately(boolean bNotifyClusterWide);
	PublishSubject<DalChangeNotification<DalVO, PK>> observeDeleteOnlyImmediately(boolean bNotifyClusterWide);
	PublishSubject<DalChangeNotificationForTransactionSync<PK>> observeChangesNotifyAfterCommit(boolean bNotifyClusterWide);
	PublishSubject<DalChangeNotificationForTransactionSync<PK>> observeChangesNotifyBeforeCommit(boolean bNotifyClusterWide);
	void handleIncomingClusterNotification(DalChangeNotificationForTransactionSync<PK> notification);

}
