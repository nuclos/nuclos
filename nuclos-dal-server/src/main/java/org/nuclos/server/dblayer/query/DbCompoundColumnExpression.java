//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SFE;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.server.dal.processor.ColumnToFieldIdVOMapping;
import org.nuclos.server.dal.processor.ColumnToFieldVOMapping;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.impl.standard.StandardSqlDBAccess;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType;

/**
 * Column mapping for compound (one-)table expressions. This is used for nuclos 'stringified'
 * references.
 *
 * @author Thomas Pasch
 * @since Nuclos 3.2.01
 * @see IColumnToVOMapping#getDbColumn(DbFrom, org.nuclos.common.IRigidMetaProvider)
 *
 * @param <T> java type of the expression result, must be String for real compound
 * 		expressions but maybe different for 'singleton' expressions.
 */
public class DbCompoundColumnExpression<T> extends DbExpression<T> implements IDbCompoundColumnExpression {

	public DbCompoundColumnExpression(DbFrom<?> from, FieldMeta<?> field, boolean setAlias, boolean bSearch, IRigidMetaProvider metaProv) {
		super(from.getQuery().getBuilder(), (Class<T>) DbUtils.getDbType(field.getDataType()), setAlias ? field.getDbColumn() : null, mkConcat(from, field, bSearch, null, null, metaProv));
		if (!field.hasAnyForeignEntity()) {
			throw new IllegalArgumentException();
		}
	}

	public DbCompoundColumnExpression(DbFrom<?> from, FieldMeta<?> field, Class<T> dbType, boolean setAlias, boolean bSearch, IRigidMetaProvider metaProv) {
		super(from.getQuery().getBuilder(), dbType, setAlias ? field.getDbColumn() : null, mkConcat(from, field, bSearch, null, null, metaProv));
		if (!field.hasAnyForeignEntity()) {
			throw new IllegalArgumentException();
		}
	}

	public DbCompoundColumnExpression(DbFrom<T> from, FieldMeta<T> field, String sAlias, Map<UID, String> aliasMap, Map<UID, FieldMeta> langFieldsMap, boolean bSearch, IRigidMetaProvider metaProv) {
		super(from.getQuery().getBuilder(), (Class<T>) DbUtils.getDbType(field.getDataType()), sAlias, mkConcat(from, field, bSearch, aliasMap, langFieldsMap, metaProv));
		if (!field.hasAnyForeignEntity()) {
			throw new IllegalArgumentException();
		}
	}
	
	static final PreparedStringBuilder mkConcat(DbFrom<?> from, FieldMeta<?> field, boolean bSearch, Map<UID, String> aliasMap,  Map<UID, FieldMeta> langFieldsMap, IRigidMetaProvider metaProv) {
		return mkConcat(from, field, bSearch, null, aliasMap, langFieldsMap, metaProv);
	}
	
	private static PreparedStringBuilder mkConcat(
			DbFrom<?> from,
			FieldMeta<?> field,
			boolean bSearch,
			final String extJoinAlias,
			Map<UID, String> aliasMap,
			Map<UID, FieldMeta> langFieldsMap,
			IRigidMetaProvider metaProv
	) {
		if (!field.hasAnyForeignEntity()) {
			throw new IllegalArgumentException();
		}
		if (field.getDbColumn().startsWith("INTID_T_")) {
			return DbColumnExpression.mkQualifiedColumnName(SystemFields.BASE_ALIAS, field.getDbColumn(), false);
		}
		final TableAliasSingleton tas = TableAliasSingleton.getInstance();
		final boolean isInheritReference = extJoinAlias != null;
		
		final List<PreparedStringBuilder> toConcat = new ArrayList<>();
		final StandardSqlDBAccess dbAccess = from.getQuery().getBuilder().getDBAccess();
		String uncastedQualifiedName = null;
		
		for (IFieldUIDRef ref: new ForeignEntityFieldUIDParser(field, metaProv, bSearch)) {
			if (ref.isConstant()) {
				toConcat.add(new PreparedStringBuilder("'" + ref.getConstant() + "'"));
			} else {
				boolean isLangField = langFieldsMap != null && langFieldsMap.containsKey(ref.getUID());
				final FieldMeta<?> mdField = getReferenceMeta(ref, isLangField, langFieldsMap, metaProv);
				PreparedStringBuilder reference;
				String qualifiedName;
				if ((mdField.getForeignEntity() != null || mdField.getUnreferencedForeignEntity() != null) &&
						(mdField.getDbColumn().startsWith("STRVALUE_") || mdField.getDbColumn().startsWith("INTVALUE_") || mdField.getDbColumn().startsWith("OBJVALUE_"))) {
					// RSWORGA-105
					if (isInheritReference && !isLangField) {
						continue;
					}
					String joinAlias = tas.getAlias(field, mdField);
					reference = mkConcat(from, mdField, bSearch, joinAlias, aliasMap, langFieldsMap, metaProv);
					if (reference == null) {
						continue;
					}
					qualifiedName = reference.toString();
				} else {
					if (!mdField.isCalculated()) {
						if ((qualifiedName = buildQualifiedName(field, mdField, extJoinAlias, isInheritReference, aliasMap)) == null) {
							continue;
						}
					} else {
						qualifiedName = getExpressionForCalculatedField(from, field, mdField, extJoinAlias, metaProv).toString();
					}
				}

				uncastedQualifiedName = qualifiedName;
				PreparedStringBuilder sqlColumn = null;
				final DbGenericType dgt = DbUtils.getDbColumnType(mdField).getGenericType();
				if (dgt == DbGenericType.DATE || dgt == DbGenericType.DATETIME) {
					sqlColumn = new PreparedStringBuilder(dbAccess.getSqlForSubstituteNull(dbAccess.getSqlForCastAsString(qualifiedName, DbUtils.getDbColumnType(mdField)), "''"));
				} else {
					final DbColumnType type = dgt != DbGenericType.VARCHAR
							? new DbColumnType(DbGenericType.VARCHAR, 255) : DbUtils.getDbColumnType(mdField);
					sqlColumn = new PreparedStringBuilder(dbAccess.getSqlForSubstituteNull(dbAccess.getSqlForCast(qualifiedName, type), "''"));
				}
				
				if (mdField.getForeignEntity() != null || mdField.getUnreferencedForeignEntity() != null) {
					String tableAliasToUse = aliasMap == null || !aliasMap.containsKey(mdField.getUID()) ? tas.getAlias(field) : aliasMap.get(mdField.getUID());
					String idSql = null;
					if (SFE.OWNER.checkField(mdField.getEntity(), mdField.getUID())) {
						idSql = ColumnToFieldIdVOMapping.getOwnerIdColumn(mdField, tableAliasToUse, from.getQuery().getBuilder(), false).getSqlColumnExpr();
					}					
					sqlColumn = DbReferencedCompoundColumnExpression.mkConcat(sqlColumn, tableAliasToUse, mdField, idSql, metaProv);
				}
				toConcat.add(sqlColumn);
			}
		}
		if (toConcat.isEmpty() && isInheritReference) {
			return null;
		}
		if (toConcat.size() == 1) {
			return new PreparedStringBuilder(uncastedQualifiedName);
		} else {
			List<String> toConcatStrings = toConcat.stream().map(Object::toString).collect(Collectors.toList());
			return new PreparedStringBuilder(dbAccess.getSqlForConcat(toConcatStrings));
		}
	}

	private static FieldMeta<?> getReferenceMeta(
			IFieldUIDRef ref,
			boolean isLangField,
			Map<UID, FieldMeta> langFieldsMap,
			IRigidMetaProvider metaProv
	) {
		if (isLangField) {
			return langFieldsMap.get(ref.getUID());
		} else {
			return metaProv.getEntityField(ref.getUID());
		}
	}

	private static String buildQualifiedName(FieldMeta<?> field, FieldMeta<?> mdField, final String extJoinAlias, final boolean isInheritReference, Map<UID, String> aliasMap) {
		boolean referencingMdField = mdField.getForeignEntity() != null && mdField.getUnreferencedForeignEntity() != null;
		FieldMeta<?> qualifyField = referencingMdField ? field : mdField;
		String tableAliasToUse = null;
		if (aliasMap != null) {
			// Localized inherit references: force aliasMap values instead of extJoinAlias!
			tableAliasToUse = aliasMap.get(qualifyField.getUID()); //field or mdField?
		}
		if (aliasMap != null && tableAliasToUse == null && mdField.isLocalized() && isInheritReference) {
			// inherit -> inherit -> lang: NOT Supported
			return null;
		}
		if (tableAliasToUse == null) {
			tableAliasToUse = extJoinAlias;
		}
		if (tableAliasToUse == null) {
			tableAliasToUse = TableAliasSingleton.getInstance().getAlias(field);
		}
		return DbColumnExpression.mkQualifiedColumnName(tableAliasToUse, new PreparedStringBuilder(qualifyField.getDbColumn()), false).toString();
	}

	private static PreparedStringBuilder getExpressionForCalculatedField(final DbFrom from, FieldMeta<?> field, FieldMeta<?> mdField, final String extJoinAlias, final IRigidMetaProvider metaProv) {
		final ColumnToFieldVOMapping<Object, Long> column;
		try {
			String tableAliasToUse = extJoinAlias;
			if (tableAliasToUse == null) {
				tableAliasToUse = TableAliasSingleton.getInstance().getAlias(field);
			}
			column = new ColumnToFieldVOMapping<>(tableAliasToUse, mdField, true, false);
			final DbQuery<Object[]> query = from.getQuery().getBuilder().createQuery(Object[].class, true);
			final DbFrom<?> subFrom = query.from(metaProv.getEntity(mdField.getEntity()), tableAliasToUse, true);
			return column.getDbColumn((DbFrom<Long>) subFrom, metaProv).getSqlString();
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}

}
