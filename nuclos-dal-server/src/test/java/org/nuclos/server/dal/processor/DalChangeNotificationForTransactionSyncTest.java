package org.nuclos.server.dal.processor;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DalChangeNotificationForTransactionSyncTest {

	@Test
	public void testJsonSerializationWithUids() throws JsonProcessingException {
		final UID dalEntity = new UID("uid1");
		final UID insertedUid = new UID("uid2");
		final UID updatedUid = new UID("uid3");
		final UID deletedUid = new UID("uid4");
		DalChangeNotificationForTransactionSync<UID> notification = new DalChangeNotificationForTransactionSync<>(dalEntity, true);
		notification.setUnidentifiedInserts();
		notification.setUnidentifiedUpdates();
		notification.setUnidentifiedDeletes();
		notification.addInserted(insertedUid);
		notification.addUpdated(updatedUid);
		notification.addDeleted(deletedUid);

		ObjectMapper mapper = new ObjectMapper();
		final String sJson = mapper.writeValueAsString(notification);
		assertEquals ((Object)"{\"entity\":\"uid1\",\"uid\":true," +
				"\"unidentifiedInserts\":true,\"unidentifiedUpdates\":true,\"unidentifiedDeletes\":true," +
				"\"inserted\":[\"uid2\"],\"updated\":[\"uid3\"],\"deleted\":[\"uid4\"]}", sJson);

		final DalChangeNotificationForTransactionSync<UID> notificationWrittenAndRead = RigidUtils.uncheckedCast(mapper.readValue(sJson, DalChangeNotificationForTransactionSync.class));
		assert notificationWrittenAndRead.getDalEntity().equals(dalEntity);
		assert notificationWrittenAndRead.hasUnidentifiedInserts();
		assert notificationWrittenAndRead.hasUnidentifiedUpdates();
		assert notificationWrittenAndRead.hasUnidentifiedDeletes();
		assert notificationWrittenAndRead.getInserted() != null;
		assert notificationWrittenAndRead.getInserted().size() == 1;
		assert notificationWrittenAndRead.getInserted().contains(insertedUid);
		assert notificationWrittenAndRead.getUpdated() != null;
		assert notificationWrittenAndRead.getUpdated().size() == 1;
		assert notificationWrittenAndRead.getUpdated().contains(updatedUid);
		assert notificationWrittenAndRead.getDeleted() != null;
		assert notificationWrittenAndRead.getDeleted().size() == 1;
		assert notificationWrittenAndRead.getDeleted().contains(deletedUid);
	}

	@Test
	public void testJsonSerializationWithIds() throws JsonProcessingException {
		final UID dalEntity = new UID("uid1");
		final Long deletedId = 1001L;
		DalChangeNotificationForTransactionSync<Long> notification = new DalChangeNotificationForTransactionSync<>(dalEntity, false);
		notification.setUnidentifiedDeletes();
		notification.addDeleted(deletedId);

		ObjectMapper mapper = new ObjectMapper();
		final String sJson = mapper.writeValueAsString(notification);
		assertEquals ((Object)"{\"entity\":\"uid1\"," +
				"\"unidentifiedDeletes\":true," +
				"\"deleted\":[1001]}", sJson);

		final DalChangeNotificationForTransactionSync<UID> notificationWrittenAndRead = RigidUtils.uncheckedCast(mapper.readValue(sJson, DalChangeNotificationForTransactionSync.class));
		assert notificationWrittenAndRead.getDalEntity().equals(dalEntity);
		assert !notificationWrittenAndRead.hasUnidentifiedInserts();
		assert !notificationWrittenAndRead.hasUnidentifiedUpdates();
		assert notificationWrittenAndRead.hasUnidentifiedDeletes();
		assert notificationWrittenAndRead.getInserted() == null;
		assert notificationWrittenAndRead.getUpdated() == null;
		assert notificationWrittenAndRead.getDeleted() != null;
		assert notificationWrittenAndRead.getDeleted().size() == 1;
		assert notificationWrittenAndRead.getDeleted().contains(deletedId);
	}

}
