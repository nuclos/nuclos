package org.nuclos.common;

import java.util.Locale;

public interface ILocaleProvider {
	Locale getLocale();
}
