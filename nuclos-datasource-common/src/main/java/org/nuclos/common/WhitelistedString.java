package org.nuclos.common;

import java.io.Serializable;

import com.google.common.base.Objects;

public class WhitelistedString implements Serializable {

	private static final long serialVersionUID = -956763788345080224L;

	private final String s;

	/**
	 * for deserialization only
	 */
	protected WhitelistedString() {
		this.s = null;
	}

	public WhitelistedString(final String s) {
		this.s = s;
	}

	public String get() {
		return s;
	}

	@Override
	public String toString() {
		return "WhitelistedString{'" + s + "'}";
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final WhitelistedString that = (WhitelistedString) o;
		return Objects.equal(s, that.s);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(s);
	}
}
