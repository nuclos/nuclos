package org.nuclos.common.database.query.definition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;

public interface ISchema extends Serializable {
	ArrayList<Table> getTables(Set<String> setQueryTypes);

	Table getTable(String sName);

	void addTable(Table table);

	Constraint getConstraint(String name);

	@Override
	String toString();

	void removeMandatorColumns();

	void removeMandatorColumn(Table table);

	Long getTimestamp();
}
