//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class CalcAttributeVO extends DatasourceVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3678641682100940197L;

	/**
	 * for deserialization only
	 */
	protected CalcAttributeVO() {
		super(E.CALCATTRIBUTE);
	}

	public CalcAttributeVO(final EntityObjectVO<UID> eovo, final String name,
						   final String sDescription, final Boolean bValid, final String sDatasourceXML, final UID nucletUID) {
		super(eovo, name, sDescription, bValid, sDatasourceXML, nucletUID, Permission.PERMISSION_NONE);
	}

	public CalcAttributeVO(final String name, String sDescription,
			final String sDatasourceXML, final Boolean bValid, final UID nucletUID) {
		super(E.CALCATTRIBUTE, name, sDescription, sDatasourceXML, bValid, nucletUID);
	}

	protected FieldMeta.Valueable<String> getNameField() {
		return E.CALCATTRIBUTE.name;
	}

	protected FieldMeta.Valueable<String> getDescriptionField() {
		return E.CALCATTRIBUTE.description;
	}

	protected FieldMeta.Valueable<Boolean> getValidField() {
		return E.CALCATTRIBUTE.valid;
	}

	protected FieldMeta.Valueable<String> getSourceField() {
		return E.CALCATTRIBUTE.source;
	}

	protected FieldMeta<UID> getNucletField() {
		return E.CALCATTRIBUTE.nuclet;
	}
	
}
