//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class ChartVO extends DatasourceVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7023993521496503932L;
	private String detailsEntity;
	
	private String parentEntity;

	/**
	 * for deserialization only
	 */
	protected ChartVO() {
		super(E.CHART);
	}
	
	public ChartVO(final EntityObjectVO<UID> eovo, final String name,
				   final String sDescription, final UID detailsEntityUID, UID parentEntityUID, final Boolean bValid, final String sDatasourceXML, final UID nucletUID) {
		super(eovo, name, sDescription, bValid, sDatasourceXML, nucletUID, Permission.PERMISSION_NONE);
		setDetailsEntityUID(detailsEntityUID);
		setParentEntityUID(parentEntityUID);
	}

	public ChartVO(final String name, String sDescription,
			final String sDatasourceXML, final Boolean bValid, final UID nucletUID) {
		super(E.CHART, name, sDescription, sDatasourceXML, bValid, nucletUID);
	}
	
	/*
	 * Method is needed!
	 *  - @see handling as BeanPropertyCollectableField
	 * @deprecated
	 */
	public String getDetailsEntity() {
    	return detailsEntity;
    }
	public void setDetailsEntity(final String entity) {
    	this.detailsEntity = entity;
    }
	
	public UID getDetailsEntityUID() {
    	return eovo.getFieldUid(E.CHART.detailsEntity);
    }
	public void setDetailsEntityUID(final UID entityUID) {
    	eovo.setFieldUid(E.CHART.detailsEntity, entityUID);
    }
	
	/*
	 * Method is needed!
	 *  - @see handling as BeanPropertyCollectableField
	 * @deprecated
	 */
	public String getParentEntity() {
    	return parentEntity;
    }
	public void setParentEntity(final String entity) {
    	this.parentEntity = entity;
    }
	
	public UID getParentEntityUID() {
    	return eovo.getFieldUid(E.CHART.parentEntity);
    }
	public void setParentEntityUID(final UID entityUID) {
    	eovo.setFieldUid(E.CHART.parentEntity, entityUID);
    }

	@Override
	protected FieldMeta.Valueable<String> getNameField() {
		return E.CHART.name;
	}

	@Override
	protected FieldMeta.Valueable<String> getDescriptionField() {
		return E.CHART.description;
	}

	@Override
	protected FieldMeta.Valueable<Boolean> getValidField() {
		return E.CHART.valid;
	}

	@Override
	protected FieldMeta.Valueable<String> getSourceField() {
		return E.CHART.source;
	}

	@Override
	protected FieldMeta<UID> getNucletField() {
		return E.CHART.nuclet;
	}
}
