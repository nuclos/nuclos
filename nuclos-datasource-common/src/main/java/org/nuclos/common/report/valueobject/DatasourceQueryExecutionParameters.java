//Copyright (C) 2022 Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.remoting.TypePreservingMapDeserializer;
import org.nuclos.remoting.TypePreservingMapSerializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class DatasourceQueryExecutionParameters {

	@JsonSerialize(using = TypePreservingMapSerializer.class)
	@JsonDeserialize(using = TypePreservingMapDeserializer.class)
	private Map<String, Object> mpParams;

	private UID entity;

	private Integer iMaxRowCount;

	private UID languageUID;

	/**
	 * for deserialization only
	 */
	protected DatasourceQueryExecutionParameters() {
		super();
	}

	public DatasourceQueryExecutionParameters(final Map<String, Object> mpParams, final UID entity, final Integer iMaxRowCount, final UID languageUID) {
		this.mpParams = mpParams;
		this.entity = entity;
		this.iMaxRowCount = iMaxRowCount;
		this.languageUID = languageUID;
	}

	public Map<String, Object> getParameterMap() {
		return mpParams;
	}

	public UID getEntity() {
		return entity;
	}

	public Integer getMaxRowCount() {
		return iMaxRowCount;
	}

	public Long getMaxRowCountAsLong() {
		if (iMaxRowCount != null) {
			return iMaxRowCount.longValue();
		}
		return null;
	}

	public UID getLanguageUID() {
		return languageUID;
	}
}
