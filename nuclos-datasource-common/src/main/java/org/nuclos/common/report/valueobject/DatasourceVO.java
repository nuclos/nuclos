//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.util.Date;
import java.util.Optional;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.StringUtils;

/**
 * Value object representing a datasource.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class DatasourceVO implements Comparable<DatasourceVO> {
	/**
	 * Constants for ORDER BY clause of column definitions.
	 */
	public enum OrderBy {
		NONE("keine"),
		ASCENDING("Aufsteigend"),
		DESCENDING("Absteigend");


		private final String sLabel;

		OrderBy(String sLabel) {
			this.sLabel = sLabel;
		}

		public String getLabel() {
			return this.sLabel;
		}

		@Override
		public String toString() {
			return this.getLabel();
		}

	}	// enum OrderBy

	/**
	 * Constants for GROUP BY clause of column definitions.
	 */
	public enum GroupBy {
		NONE("keine"),
		GROUP("Gruppe"),
		SUM("Summe"),
		MIN("Minimum"),
		MAX("Maximum"),
		COUNT("Anzahl"),
		AVERAGE("Mittelwert");

		private final String sLabel;

		GroupBy(String sLabel) {
			this.sLabel = sLabel;
		}

		public String getLabel() {
			return this.sLabel;
		}

		@Override
		public String toString() {
			return this.getLabel();
		}

	}	// enum GroupBy

	public enum Permission {
		PERMISSION_NONE,
		PERMISSION_READONLY,
		PERMISSION_READWRITE
	}

	private static final long serialVersionUID = -3138015304477033571L;

	protected EntityObjectVO<UID> eovo;

	public Date getCreatedAt() {
		return eovo.getCreatedAt();
	}

	public String getCreatedBy() {
		return eovo.getCreatedBy();
	}

	public Date getChangedAt() {
		return eovo.getChangedAt();
	}

	public String getChangedBy() {
		return eovo.getChangedBy();
	}

	public Integer getVersion() {
		return eovo.getVersion();
	}
	private String sMetaXML;
	private String sQuery;
	private String nuclet;
	private final Permission permission;
	private boolean bWithRuleClass;
	private String datasourceRule;

	protected DatasourceVO() {
		this.eovo = new EntityObjectVO<>(E.DATASOURCE.getUID());
		this.permission = Permission.PERMISSION_NONE;
	}

	protected DatasourceVO(final EntityMeta<UID> dsEntityMeta) {
		this.eovo = new EntityObjectVO<>(dsEntityMeta.getUID());
		this.permission = Permission.PERMISSION_NONE;
	}

	/**
	 * constructor used by server only
	 * @param eovo contains the common fields.
	 * @param sName
	 * @param sDescription
	 * @param sDatasourceXML
	 * @param permission permission of actual principal for datasource
	 */
	public DatasourceVO(EntityObjectVO<UID> eovo, String sName, String sDescription, Boolean bValid, String sDatasourceXML, UID nucletUID,
			Permission permission) {
		this.eovo = eovo;

		setName(sName);
		setDescription(sDescription);
		setValid(bValid);
		setSource(sDatasourceXML);
		setNucletUID(nucletUID);

		this.permission = permission;
	}

	/**
	 * constructor used by client only
	 *
	 * @param dsEntityMeta
	 * @param sName
	 * @param sDescription
	 * @param sDatasourceXML
	 */
	public DatasourceVO(final EntityMeta<UID> dsEntityMeta, String sName, String sDescription, String sDatasourceXML, Boolean bValid, UID nucletUID) {
		this(dsEntityMeta);

		setName(sName);
		setDescription(sDescription);
		setValid(bValid);
		setSource(sDatasourceXML);
		setNucletUID(nucletUID);
	}

	public EntityObjectVO<UID> getEntityObjectVO() {
		return eovo;
	}

	public UID getId() {
		return eovo.getPrimaryKey();
	}

	public void setId(UID primaryKey) {
		eovo.setPrimaryKey(primaryKey);
	}

	public UID getPrimaryKey() {
		return eovo.getPrimaryKey();
	}

	public void setPrimaryKey(UID primaryKey) {
		eovo.setPrimaryKey(primaryKey);
	}

	protected FieldMeta.Valueable<String> getNameField() {
		return E.DATASOURCE.name;
	}

	protected FieldMeta.Valueable<String> getDescriptionField() {
		return E.DATASOURCE.description;
	}

	protected FieldMeta.Valueable<Boolean> getValidField() {
		return E.DATASOURCE.valid;
	}

	protected FieldMeta.Valueable<String> getSourceField() {
		return E.DATASOURCE.source;
	}

	protected FieldMeta<UID> getNucletField() {
		return E.DATASOURCE.nuclet;
	}

	protected FieldMeta.Valueable<Boolean> getWithRuleClassField() {
		return E.DATASOURCE.withRuleClass;
	}

	protected FieldMeta.Valueable<String> getDatasourceRuleField() {
		return E.DATASOURCE.datasourceRule;
	}

	public String getName() {
		return eovo.getFieldValue(getNameField());
	}

	public void setName(String sName) {
		this.eovo.setFieldValue(getNameField(), sName);
	}

	public String getDescription() {
		return eovo.getFieldValue(getDescriptionField());
	}

	public void setDescription(String sDescription) {
		this.eovo.setFieldValue(getDescriptionField(), sDescription);
	}

	public void setValid(Boolean bValid) {
		this.eovo.setFieldValue(getValidField(), bValid);
	}

	public Boolean getValid() {
		return eovo.getFieldValue(getValidField());
	}

	public String getSource() {
		return eovo.getFieldValue(getSourceField());
	}

	public void setSource(String sDatasourceXML) {
		this.eovo.setFieldValue(getSourceField(), sDatasourceXML);
	}

	public final String getMeta() {
		return sMetaXML;
	}

	public final void setMeta(String sMetaXML) {
		this.sMetaXML = sMetaXML;
	}	

	public final String getQuery() {
		return sQuery;
	}

	public final void setQuery(String sQuery) {
		this.sQuery = sQuery;
	}

	public UID getNucletUID() {
		return eovo.getFieldUid(getNucletField());
	}

	public void setNucletUID(UID nucletUID) {
		this.eovo.setFieldUid(getNucletField(), nucletUID);
	}
	
	public String getNuclet() {
		return nuclet;
	}
	
	public void setNuclet(String nuclet){
		this.nuclet = nuclet;
	}
	
	public Permission getPermission() {
		return permission;
	}

	public void setWithRuleClass(boolean bWithRuleClass) {
		eovo.setFieldValue(getWithRuleClassField(), bWithRuleClass);
	}

	public boolean isWithRuleClass() {
		return Optional.ofNullable(eovo.getFieldValue(getWithRuleClassField())).orElse(Boolean.FALSE);
	}

	public String getDatasourceRule() {
		return eovo.getFieldValue(getDatasourceRuleField());
	}

	public void setDatasourceRule(String datasourceRule) {
		eovo.getFieldValue(getDatasourceRuleField());
	}

	/**
	 * validity checker
	 */
	public void validate() throws Exception {
		if (StringUtils.isNullOrEmpty(this.getName())) {
			throw new Exception("datasource.error.validation.value");
		}
		if (StringUtils.isNullOrEmpty(this.getDescription())) {
			throw new Exception("datasource.error.validation.description");
		}
		if (StringUtils.isNullOrEmpty(this.getSource())) {
			throw new Exception("datasource.error.validation.datasourcexml");
		}

	}

	@Override
	public String toString(){
		return getName();
	}

	@Override
	public int compareTo(final DatasourceVO o) {
		return StringUtils.compareIgnoreCase(toString(), o.toString());
	}
	
	// dummy fields.
	public void setCreatedAt(Date createdAt) {
	}

	public void setCreatedBy(String createdBy) {
	}

	public void setChangedAt(Date changedAt) {
	}

	public void setChangedBy(String changedBy) {
	}
	
	public void setNuclosImportVersion(Integer version) {
	}
	public Integer getNuclosImportVersion() {
		return null;
	}
	
	public void setNuclosOriginUID(UID nuclosOriginUID) {
	}
	public UID getNuclosOriginUID() {
		return null;
	}
}	// class DatasourceVO
