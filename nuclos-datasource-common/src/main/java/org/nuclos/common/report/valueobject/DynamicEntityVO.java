//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class DynamicEntityVO extends DatasourceVO {

	/**
	 *
	 */
	private static final long serialVersionUID = 7920433102837207682L;

	private String detailsEntity;

	/**
	 * for deserialization only
	 */
	protected DynamicEntityVO() {
		super(E.DYNAMICENTITY);
	}

	public DynamicEntityVO(EntityObjectVO<UID> eovo, String sName,
						   String sDescription, UID entity, Boolean bValid, String sDatasourceXML, final UID nucletUID) {
		super(eovo, sName, sDescription, bValid, sDatasourceXML, nucletUID, Permission.PERMISSION_NONE);
		setEntityUID(entity);
	}

	public DynamicEntityVO(String sName, String sDescription,
						   String sDatasourceXML, Boolean bValid, final UID nucletUID) {
		super(E.DYNAMICENTITY, sName, sDescription, sDatasourceXML, bValid, nucletUID);
	}

	/*
	 * Method is needed!
	 *  - @see handling as BeanPropertyCollectableField
	 * @deprecated
	 */
	public String getEntity() {
		return detailsEntity;
	}
	public void setEntity(final String entity) {
		this.detailsEntity = entity;
	}

	public UID getEntityUID() {
		return eovo.getFieldUid(E.DYNAMICENTITY.entity);
	}

	public void setEntityUID(final UID entityUID) {
		eovo.setFieldUid(E.DYNAMICENTITY.entity, entityUID);
	}

	protected FieldMeta.Valueable<String> getNameField() {
		return E.DYNAMICENTITY.name;
	}

	protected FieldMeta.Valueable<String> getDescriptionField() {
		return E.DYNAMICENTITY.description;
	}

	protected FieldMeta.Valueable<Boolean> getValidField() {
		return E.DYNAMICENTITY.valid;
	}

	protected FieldMeta.Valueable<String> getSourceField() {
		return E.DYNAMICENTITY.source;
	}

	protected FieldMeta<UID> getNucletField() {
		return E.DYNAMICENTITY.nuclet;
	}
}
