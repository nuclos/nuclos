//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class DynamicTasklistVO extends DatasourceVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3975884687160662635L;

	/**
	 * for deserialization only
	 */
	protected DynamicTasklistVO() {
		super(E.DYNAMICTASKLIST);
	}

	public DynamicTasklistVO(EntityObjectVO<UID> eovo, String sName,
							 String sDescription, Boolean bValid, String sDatasourceXML, final UID nucletUID) {
		super(eovo, sName, sDescription, bValid, sDatasourceXML, nucletUID, Permission.PERMISSION_NONE);
	}

	public DynamicTasklistVO(String sName, String sDescription,
		String sDatasourceXML, Boolean bValid, final UID nucletUID) {
		super(E.DYNAMICTASKLIST, sName, sDescription, sDatasourceXML, bValid, nucletUID);
	}

	protected FieldMeta.Valueable<String> getNameField() {
		return E.DYNAMICTASKLIST.name;
	}

	protected FieldMeta.Valueable<String> getDescriptionField() {
		return E.DYNAMICTASKLIST.description;
	}

	protected FieldMeta.Valueable<Boolean> getValidField() {
		return E.DYNAMICTASKLIST.valid;
	}

	protected FieldMeta.Valueable<String> getSourceField() {
		return E.DYNAMICTASKLIST.source;
	}

	protected FieldMeta<UID> getNucletField() {
		return E.DYNAMICTASKLIST.nuclet;
	}

	@Override
	protected FieldMeta.Valueable<String> getDatasourceRuleField() {
		return E.DYNAMICTASKLIST.datasourceRule;
	}
}
