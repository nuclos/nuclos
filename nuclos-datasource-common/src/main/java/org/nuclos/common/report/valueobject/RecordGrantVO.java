//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class RecordGrantVO extends DatasourceVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8168446072971177421L;
	private String entity;
	private UID entityUID;

	/**
	 * for deserialization only
	 */
	protected RecordGrantVO() {
		super(E.RECORDGRANT);
	}

	public RecordGrantVO(EntityObjectVO<UID> eovo, String sName,
						 String sDescription, final UID entityUID, Boolean bValid, String sDatasourceXML, UID nucletUID) {
		super(eovo, sName, sDescription, bValid, sDatasourceXML, nucletUID, Permission.PERMISSION_NONE);
		setEntityUID(entityUID);
	}

	public RecordGrantVO(String sName, String sDescription,
		String sDatasourceXML, final UID entityUID, Boolean bValid, UID nucletUID) {
		super(E.RECORDGRANT, sName, sDescription, sDatasourceXML, bValid, nucletUID);
		setEntityUID(entityUID);
	}
	
	/*
	 * Method is needed!
	 *  - @see handling as BeanPropertyCollectableField
	 * @deprecated
	 */
	public String getEntity() {
    	return entity;
    }
	public void setEntity(final String entity) {
    	this.entity = entity;
    }
	
	public UID getEntityUID() {
    	return eovo.getFieldUid(E.RECORDGRANT.entity);
    }
	public void setEntityUID(final UID entityUID) {
    	eovo.setFieldUid(E.RECORDGRANT.entity, entityUID);
    }

	@Override
	protected FieldMeta.Valueable<String> getNameField() {
		return E.RECORDGRANT.name;
	}

	@Override
	protected FieldMeta.Valueable<String> getDescriptionField() {
		return E.RECORDGRANT.description;
	}

	@Override
	protected FieldMeta.Valueable<Boolean> getValidField() {
		return E.RECORDGRANT.valid;
	}

	@Override
	protected FieldMeta.Valueable<String> getSourceField() {
		return E.RECORDGRANT.source;
	}

	@Override
	protected FieldMeta<UID> getNucletField() {
		return E.RECORDGRANT.nuclet;
	}	
}
