//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ILocaleProvider;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class ValuelistProviderVO extends DatasourceVO {
	/**
	 *
	 */
	private static final long serialVersionUID = -1506187964301279989L;
	public static final String DATASOURCE_IDFIELD = "id-fieldname";
	public static final String DATASOURCE_NAMEFIELD = "fieldname";
	public static final String DATASOURCE_DEFAULTMARKERFIELD = "default-fieldname";
	public static final String DATASOURCE_VALUELISTPROVIDER = "valuelistProvider";
	public static final String DATASOURCE_SEARCHMODE = "searchmode";

	private String detailsearchdescriptionResourceId;
	private String sDetailsearchdescriptionDe;
	private String sDetailsearchdescriptionEn;

	private ILocaleProvider localeProvider;

	public enum Type {

		DEFAULT("default"),DEPENDANT("dependant"),NAMED("named"), DS("ds");

		private final String type;

		private Type(String type) {
			this.type = type;
		}

		public static Type getByType(String sType) {
			return Arrays.stream(Type.values())
					.filter(type -> Objects.equals(type.getValue(), sType))
					.findFirst().orElse(null);
		}

		public String getValue() {
			return getType();
		}

		public String getType() {
			return type;
		}

		@Override
		public String toString() {
			return getType();
		}
	}

	/**
	 * for deserialization only
	 */
	protected ValuelistProviderVO() {
		super(E.VALUELISTPROVIDER);
	}

	public ValuelistProviderVO(final EntityObjectVO<UID> eovo, final String name,
							   final String sDescription, final Boolean bValid, final String sDatasourceXML, final UID nucletUID, final ILocaleProvider localeProvider) {
		super(eovo, name, sDescription, bValid, sDatasourceXML, nucletUID, Permission.PERMISSION_NONE);
	}

	public ValuelistProviderVO(final String name, final String sDescription,
							   final String sDatasourceXML, final Boolean bValid, final UID nucletUID) {
		super(E.VALUELISTPROVIDER, name, sDescription, sDatasourceXML, bValid, nucletUID);
	}

	public String getDetailsearchdescription(Locale locale) {
		if (Locale.GERMAN.equals(locale) || Locale.GERMANY.equals(locale)) {
			return sDetailsearchdescriptionDe;
		} else {
			return sDetailsearchdescriptionEn;
		}
	}

	public void setDetailsearchdescription(String sDetailssearchdescription, Locale locale) {
		if (Locale.GERMAN.equals(locale) || Locale.GERMANY.equals(locale)) {
			this.sDetailsearchdescriptionDe = sDetailssearchdescription;
		} else {
			this.sDetailsearchdescriptionEn = sDetailssearchdescription;
		}
	}

	public String getDetailsearchdescription() {
		return getDetailsearchdescription(localeProvider != null ? localeProvider.getLocale() : null);
	}

	public String getDetailsearchdescriptionResourceId() {
		return detailsearchdescriptionResourceId;
	}

	public void setDetailsearchdescriptionResourceId(final String detailsearchdescriptionResourceId) {
		this.detailsearchdescriptionResourceId = detailsearchdescriptionResourceId;
	}

	@Override
	protected FieldMeta.Valueable<String> getNameField() {
		return E.VALUELISTPROVIDER.name;
	}

	@Override
	protected FieldMeta.Valueable<String> getDescriptionField() {
		return E.VALUELISTPROVIDER.description;
	}

	@Override
	protected FieldMeta.Valueable<Boolean> getValidField() {
		return E.VALUELISTPROVIDER.valid;
	}

	@Override
	protected FieldMeta.Valueable<String> getSourceField() {
		return E.VALUELISTPROVIDER.source;
	}

	@Override
	protected FieldMeta<UID> getNucletField() {
		return E.VALUELISTPROVIDER.nuclet;
	}
}
