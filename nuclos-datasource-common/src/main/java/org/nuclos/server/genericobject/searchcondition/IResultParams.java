package org.nuclos.server.genericobject.searchcondition;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.nuclos.common.UID;

public interface IResultParams extends Serializable {
	Collection<UID> getFields();

	Long getOffset();

	Long getLimit();

	boolean isSortResult();

	boolean isIdOnlySelection();

	void setIdOnlySelection(boolean bIdOnlySelection);

	boolean loadThumbnailsOnly();

	void setLoadThumbnailsOnly(boolean bLoadThumbnailsOnly);

	IResultParams forceDistinct();

	boolean isForceDistinct();
}
