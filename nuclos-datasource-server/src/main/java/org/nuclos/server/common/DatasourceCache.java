//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.AbstractDisposableRegistrationBean;
import org.nuclos.common.E;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.server.dal.processor.DalChangeNotification;
import org.nuclos.server.dal.processor.nuclos.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.report.SchemaCache;
import org.nuclos.server.report.ejb3.DataSourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Cache for all Datasources.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 *
 * TODO: Re-check if all methods must be synchronized! (tp)
 */
@Component
@DependsOn("recordGrantUtils")
public class DatasourceCache extends AbstractDisposableRegistrationBean {

	private static final Logger LOG = LoggerFactory.getLogger(DatasourceCache.class);
	public static final List<UID> DS_ENTITIES = Arrays.asList(
			E.DATASOURCE.getUID(),
			E.VALUELISTPROVIDER.getUID(),
			E.RECORDGRANT.getUID(),
			E.DYNAMICENTITY.getUID(),
			E.DYNAMICTASKLIST.getUID(),
			E.CHART.getUID(),
			E.CALCATTRIBUTE.getUID()
	);

	//

	/** map which contains all datasources */
	private final Map<UID, DatasourceVO> mpDatasourcesByUID
		= new ConcurrentHashMap<>();

	/** map which contains the datasources where the user has at least read permission */
	private final Map<String, List<DatasourceVO>> mpDatasourcesByCreator
		= new ConcurrentHashMap<>();

	/** map which contains all datasources */
	private final Map<UID, ValuelistProviderVO> mpValuelistProviderById
		= new ConcurrentHashMap<>();

	/** map which contains all record grants */
	private final Map<UID, RecordGrantVO> mpRecordGrantById
		= new ConcurrentHashMap<>();

	/** map which contains all datasources */
	private final Map<UID, DynamicEntityVO> mpDynamicEntitiesById
		= new ConcurrentHashMap<>();

	private final Map<UID, DynamicTasklistVO> mpDynamicTasklistById
		= new ConcurrentHashMap<>();
	
	private final Map<UID, ChartVO> mpChartById
	= new ConcurrentHashMap<>();

	private final SQLCache sqlCache = new SQLCache();
	private final Map<UID, CalcAttributeVO> mpCalcAttributesById = new ConcurrentHashMap<>();
	private final Map<UID, IEntityObjectProcessor<UID>> procs = new HashMap<>();
	private final Map<UID, Map<UID, ? extends DatasourceVO>> mpCacheMaps = new HashMap<>();

	private DatasourceServerUtils datasourceServerUtils;
	private final SpringDataBaseHelper dataBaseHelper;
	private final ISecurityCache securityCache;
	private final NuclosDalProvider nuclosDalProvider;
	private final MetaProvider metaProv;

	private SchemaCache schemaCache;

	DatasourceCache(
			final DatasourceServerUtils datasourceServerUtils,
			final SpringDataBaseHelper dataBaseHelper,
			final ISecurityCache securityCache,
			final NuclosDalProvider nuclosDalProvider,
			final MetaProvider metaProv) {
		this.datasourceServerUtils = datasourceServerUtils;
		this.dataBaseHelper = dataBaseHelper;
		this.securityCache = securityCache;
		this.nuclosDalProvider = nuclosDalProvider;
		this.metaProv = metaProv;

		for (UID entity : DS_ENTITIES) {
			if (E.DATASOURCE.checkEntityUID(entity)) {
				this.mpCacheMaps.put(entity, this.mpDatasourcesByUID);
			} else if (E.VALUELISTPROVIDER.checkEntityUID(entity)) {
				this.mpCacheMaps.put(entity, mpValuelistProviderById);
			} else if (E.RECORDGRANT.checkEntityUID(entity)) {
				this.mpCacheMaps.put(entity, mpRecordGrantById);
			} else if (E.DYNAMICENTITY.checkEntityUID(entity)) {
				this.mpCacheMaps.put(entity, mpDynamicEntitiesById);
			} else if (E.DYNAMICTASKLIST.checkEntityUID(entity)) {
				this.mpCacheMaps.put(entity, mpDynamicTasklistById);
			} else if (E.CHART.checkEntityUID(entity)) {
				this.mpCacheMaps.put(entity, mpChartById);
			} else if (E.CALCATTRIBUTE.checkEntityUID(entity)) {
				this.mpCacheMaps.put(entity, mpCalcAttributesById);
			}
		}
	}

	@Autowired
	private void inject(@Lazy SchemaCache schemaCache) {
		this.schemaCache = schemaCache;
	}

	@PostConstruct
	public final void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		createSyncFlowableWithDropStrategyAndRegister(metaProv.observeEntityMetaRefreshsAfterCommit(), entityMetas -> invalidate(true));

		for (UID dsEntity : DS_ENTITIES) {
			final IEntityObjectProcessor<UID> proc = nuclosDalProvider.getEntityObjectProcessor(dsEntity);
			procs.put(dsEntity, proc);
			createAsyncFlowableWithoutStrategyAndRegister(proc.observeAllChangesImmediately(true), this::refreshDatasources);
		}

		refreshDatasources(null);
	}

	@SuppressWarnings("ImplicitSubclassInspection")
	@Transactional(propagation= Propagation.REQUIRED, noRollbackFor= {Exception.class})
	private void initializeCache(boolean initValueListProviderCache) {
		LOG.debug("Initializing DatasourceCache");

		for (final EntityObjectVO<UID> eoVO :
			nuclosDalProvider.getEntityObjectProcessor(E.DATASOURCE).getAll()) {
			mpDatasourcesByUID.put(eoVO.getPrimaryKey(), 
					DatasourceWrapper.getDatasourceVO(eoVO, "INITIAL", null, securityCache));
		}
		if (initValueListProviderCache) {
			for (final EntityObjectVO<UID> eoVO :
				nuclosDalProvider.getEntityObjectProcessor(E.VALUELISTPROVIDER).getAll()) {
				mpValuelistProviderById.put(eoVO.getPrimaryKey(),
						DatasourceWrapper.getValuelistProviderVO(eoVO, null));
			}
		}
		for (final EntityObjectVO<UID> eoVO :
			nuclosDalProvider.getEntityObjectProcessor(E.DYNAMICENTITY).getAll()) {
			mpDynamicEntitiesById.put(eoVO.getPrimaryKey(),
					DatasourceWrapper.getDynamicEntityVO(eoVO));
		}
		for (final EntityObjectVO<UID> eoVO :
			nuclosDalProvider.getEntityObjectProcessor(E.RECORDGRANT).getAll()) {
			mpRecordGrantById.put(eoVO.getPrimaryKey(),
					DatasourceWrapper.getRecordGrantVO(eoVO, metaProv));
		}
		for (final EntityObjectVO<UID> eoVO :
			nuclosDalProvider.getEntityObjectProcessor(E.DYNAMICTASKLIST).getAll()) {
			mpDynamicTasklistById.put(eoVO.getPrimaryKey(),
					DatasourceWrapper.getDynamicTasklistVO(eoVO));
		}
		for (final EntityObjectVO<UID> eoVO :
			nuclosDalProvider.getEntityObjectProcessor(E.CHART).getAll()) {
			mpChartById.put(eoVO.getPrimaryKey(),
					DatasourceWrapper.getChartVO(eoVO));
		}
		for (final EntityObjectVO<UID> eoVO :
			nuclosDalProvider.getEntityObjectProcessor(E.CALCATTRIBUTE).getAll()) {
			mpCalcAttributesById.put(eoVO.getPrimaryKey(),
					DatasourceWrapper.getCalcAttributeVO(eoVO));
		}
		LOG.debug("Finished initializing DatasourceCache.");
	}

	private void refreshValueListProviders() {
		refreshDatasources(DalChangeNotification.withUnidentifiedInserts(E.VALUELISTPROVIDER.getUID(), true));
	}

	private void refreshDatasources(final DalChangeNotification<EntityObjectVO<UID>, UID> notification) {
		synchronized (this) {
			List<UID> entitiesToRefresh = new ArrayList<>();
			if (notification == null) {
				entitiesToRefresh.addAll(DS_ENTITIES);
			} else {
				entitiesToRefresh.add(notification.getDalEntity());
			}
			for (final UID entity : entitiesToRefresh) {
				final IEntityObjectProcessor<UID> proc = procs.get(entity);
				final Map<UID, ? extends DatasourceVO> mpDatasourcesByUID = mpCacheMaps.get(entity);
				if (proc != null) {
					if (notification == null || notification.hasUnidentified() || notification.isRollbackOnly()) {
						// complete refresh
						final List<EntityObjectVO<UID>> allDatasources = proc.getAll();
						final Set<UID> pks = allDatasources.stream().map(HasPrimaryKey::getPrimaryKey).collect(Collectors.toSet());
						final Set<UID> cachedPks = new HashSet<>(mpDatasourcesByUID.keySet());
						cachedPks.stream().filter(pk -> !pks.contains(pk)).forEach(pk -> this.removeDatasource(pk, mpDatasourcesByUID));
						allDatasources.forEach(this::putDatasource);
					} else {
						if (notification.isSingleChangeOnly()) {
							putDatasource(notification.getSingleChange());
						} else {
							Optional.ofNullable(notification.getDeleted()).ifPresent(pks -> pks.forEach(pk -> this.removeDatasource(pk, mpDatasourcesByUID)));
							Optional.ofNullable(notification.getInsertedAndUpdated()).ifPresent(pks -> pks.forEach(pk -> this.putDatasource(pk, proc)));
						}
					}
				}
			}
		}
	}

	private void removeDatasource(UID pk, Map<UID, ? extends DatasourceVO> mpDatasourcesByUID) {
		mpDatasourcesByUID.remove(pk);
	}

	private void putDatasource(UID pk, IEntityObjectProcessor<UID> proc) {
		putDatasource(proc.getByPrimaryKey(pk));
	}

	private void putDatasource(EntityObjectVO<UID> eoDatasource) {
		final UID entity = eoDatasource.getDalEntity();
		final UID pk = eoDatasource.getPrimaryKey();

		if (E.DATASOURCE.checkEntityUID(entity)) {
			DatasourceVO datasourceVO = DatasourceWrapper.getDatasourceVO(eoDatasource, eoDatasource.getFieldValue(SF.CREATEDBY), null, securityCache);
			mpDatasourcesByUID.put(pk, datasourceVO);
		} else if (E.VALUELISTPROVIDER.checkEntityUID(entity)) {
			final ValuelistProviderVO valuelistProviderVO = DatasourceWrapper.getValuelistProviderVO(eoDatasource, null);
			mpValuelistProviderById.put(pk, valuelistProviderVO);
		} else if (E.RECORDGRANT.checkEntityUID(entity)) {
			final RecordGrantVO recordGrantVO = DatasourceWrapper.getRecordGrantVO(eoDatasource, metaProv);
			mpRecordGrantById.put(pk, recordGrantVO);
		} else if (E.DYNAMICENTITY.checkEntityUID(entity)) {
			final DynamicEntityVO dynamicEntityVO = DatasourceWrapper.getDynamicEntityVO(eoDatasource);
			mpDynamicEntitiesById.put(pk, dynamicEntityVO);
		} else if (E.DYNAMICTASKLIST.checkEntityUID(entity)) {
			final DynamicTasklistVO dynamicTasklistVO = DatasourceWrapper.getDynamicTasklistVO(eoDatasource);
			mpDynamicTasklistById.put(pk, dynamicTasklistVO);
		} else if (E.CHART.checkEntityUID(entity)) {
			final ChartVO chartVO = DatasourceWrapper.getChartVO(eoDatasource);
			mpChartById.put(pk, chartVO);
		} else if (E.CALCATTRIBUTE.checkEntityUID(entity)) {
			final CalcAttributeVO calcAttributeVO = DatasourceWrapper.getCalcAttributeVO(eoDatasource);
			mpCalcAttributesById.put(pk, calcAttributeVO);
		}
	}

	@SuppressWarnings("ImplicitSubclassInspection")
	@Transactional(propagation= Propagation.REQUIRED, noRollbackFor= {Exception.class})
	public void buildDatasourcesCachedSQL() {
		final Map[] cacheMaps = new Map[] {
				mpDatasourcesByUID, mpValuelistProviderById, mpDynamicEntitiesById,
				mpDynamicTasklistById, mpChartById, mpCalcAttributesById
		};

		for (int i = 0; i < cacheMaps.length; i++) {
			for (Iterator iterator = cacheMaps[i].values().iterator(); iterator.hasNext();) {
				DatasourceVO datasourceVO = (DatasourceVO) iterator.next();
				try {
					sqlCache.getSQL(datasourceVO);
				} catch (Throwable t) {
					// ignore. just try to cache.
				}
			}
		}
	}
	
	/**
	 * initialize the map of datasources by creator
	 * @param sCreator
	 */
	private void findDataSourcesByCreator(final String sCreator, UID mandator) {
		LOG.debug("Initializing DatasourceCacheByCreator");

		List<DatasourceVO> datasources = new ArrayList<DatasourceVO>();

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.DATASOURCE);
		query.select(t.baseColumn(SF.PK_UID));
		query.where(builder.equalValue(t.baseColumn(SF.CREATEDBY), sCreator));

		for (final UID datasourceUid : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
			final EntityObjectVO<UID> eovoDS = nuclosDalProvider.<UID>getEntityObjectProcessor(E.DATASOURCE.getUID()).getByPrimaryKey(datasourceUid);
			if (getPermission(datasourceUid, sCreator, mandator) != DatasourceVO.Permission.PERMISSION_NONE) {
				datasources.add(DatasourceWrapper.getDatasourceVO(eovoDS, sCreator, mandator, securityCache));
			}
		}
		mpDatasourcesByCreator.put(sCreator,datasources);

		LOG.debug("Finished initializing DatasourceCacheByCreator.");
	}

		
	/**
	 * Invalidate the cache
	 */
	@ManagedOperation
	@ManagedOperationParameters
	public void invalidate() {
		invalidate(true);
	}

	/**
	 * Invalidate the cache
	 */
	public void invalidate(boolean initCachedSQL) {
		LOG.debug("Invalidating DatasourceCache");

		refreshDatasources(null);
		sqlCache.invalidate();

		if (initCachedSQL) {
			buildDatasourcesCachedSQL();
		}
	}

	/**
	 * get a collection of all Datasources where the given user has at least read permission
	 * 
	 * @return Collection&lt;DatasourceVO&gt;
	 */
	public Collection<DatasourceVO> getAll() {
		List<DatasourceVO> result = new ArrayList<DatasourceVO>();
		result.addAll(getAllCharts());
		result.addAll(getAllDatasources());
		result.addAll(getAllDynamicEntities());
		result.addAll(getAllDynamicTasklists());
		result.addAll(getAllRecordGrant());
		result.addAll(getAllValuelistProvider(true));
		return result;
	}


	/**
	 * get a collection of all Datasources where the given user has at least read permission
	 * 
	 * @return Collection&lt;DatasourceVO&gt;
	 */
	public Collection<DatasourceVO> getAllDatasources(final String sUser, UID mandator) {
		List<DatasourceVO>result = new ArrayList<DatasourceVO>();

		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		for (DatasourceVO datasourceVO : mpDatasourcesByUID.values()) {
			if(getPermission(datasourceVO.getId(), sUser, mandator) != DatasourceVO.Permission.PERMISSION_NONE) {
				result.add(datasourceVO);
			}
		}
		return result;
	}

	/**
	 * get all Datasources without checking user permissions
	 * @return
	 */
	public Collection<DatasourceVO> getAllDatasources() {
		List<DatasourceVO>result = new ArrayList<DatasourceVO>();

		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		result.addAll(mpDatasourcesByUID.values());

		return result;
	}

	//TODO MULTINUCLET eliminate this method
	/**
	 * Get the base entity name of a dynamic entity.
	 *
	 * @param dynamicEntityUID The name of the dynamic entity.
	 * @return Returns the base entity name. Returns the original entity name if there is no dynamic entity with the given name.
	public String getBaseEntity(String dynamicEntityUID) {
		if (mapDynamicEntities.containsKey(dynamicEntityUID)) {
			return mapDynamicEntities.get(dynamicEntityUID).getEntity();
		}
		else {
			return dynamicEntityUID;
		}
	}
	 */

	/**
	 * get all valuelist provider
	 * @return
	 */
	public Collection<ValuelistProviderVO> getAllValuelistProvider(boolean useCache) {
		List<ValuelistProviderVO> result = new ArrayList<ValuelistProviderVO>();

		if (mpValuelistProviderById.isEmpty() && useCache)
			refreshValueListProviders();

		result.addAll(mpValuelistProviderById.values());

		return result;
	}

	/**
	 * get a ValuelistProvider
	 */
	public ValuelistProviderVO getValuelistProvider(final UID valuelistProviderUID, boolean useCache) {
		if (valuelistProviderUID == null) {
			return null;
		}

		if (mpValuelistProviderById.isEmpty() && useCache) {
			refreshValueListProviders();
		}

		return mpValuelistProviderById.get(valuelistProviderUID);
	}

	/**
	 * get all record grant
	 * @return
	 */
	public Collection<RecordGrantVO> getAllRecordGrant() {
		List<RecordGrantVO> result = new ArrayList<RecordGrantVO>();

		if (mpRecordGrantById.isEmpty())
			;//findDatasourcesById();

		result.addAll(mpRecordGrantById.values());

		return result;
	}

	/**
	 * get a record grant
	 * @param recordGrantUID record grant UID
	 * @return
	 */
	public RecordGrantVO getRecordGrant(final UID recordGrantUID) {
		if (mpRecordGrantById.isEmpty())
			;//findDatasourcesById();

		return mpRecordGrantById.get(recordGrantUID);
	}

	/**
	 * get all dynamic entities
	 * @return
	 */
	public Collection<DynamicEntityVO> getAllDynamicEntities() {
		List<DynamicEntityVO>result = new ArrayList<DynamicEntityVO>();

		if (mpDynamicEntitiesById.isEmpty())
			;//findDatasourcesById();

		result.addAll(mpDynamicEntitiesById.values());

		return result;
	}

	/**
	 * get all dynamic task lists
	 * @return
	 */
	public Collection<DynamicTasklistVO> getAllDynamicTasklists() {
		List<DynamicTasklistVO> result = new ArrayList<DynamicTasklistVO>();

		if (mpDynamicTasklistById.isEmpty())
			;//findDatasourcesById();

		result.addAll(mpDynamicTasklistById.values());

		return result;
	}

	/**
	 * get a  dynamic task lists
	 * @param dynamicTaskListUID
	 * @return
	 */
	public DynamicTasklistVO getDynamicTasklist(final UID dynamicTaskListUID) {
		if (mpDynamicTasklistById.isEmpty())
			;//findDatasourcesById();

		return mpDynamicTasklistById.get(dynamicTaskListUID);
	}

	/**
	 * get all charts
	 * @return
	 */
	public Collection<ChartVO> getAllCharts() {
		List<ChartVO>result = new ArrayList<ChartVO>();

		if (mpChartById.isEmpty())
			;//findDatasourcesById();

		result.addAll(mpChartById.values());

		return result;
	}
	
	
	/**
	 * get all calc attributes
	 * @return
	 */
	public Collection<CalcAttributeVO> getAllCalcAttributes() {
		List<CalcAttributeVO> result = new ArrayList<CalcAttributeVO>();

		result.addAll(mpCalcAttributesById.values());

		return result;
	}
	
	/**
	 * get a CalcAttribute
	 */
	public CalcAttributeVO getCalcAttribute(final UID calcAttributeUID) {
		if (calcAttributeUID == null) {
			return null;
		}
		return mpCalcAttributesById.get(calcAttributeUID);
	}


	/**
	 * get a Datasource by UID regardless of permisssions
	 */
	public DatasourceVO get(final UID dataSourceUID) {
		return mpDatasourcesByUID.get(dataSourceUID);
	}

	public DatasourceVO get(DataSourceType type, UID uid) {
		switch (type) {
			case CHART:
				return mpChartById.get(uid);
			case DATASOURCE:
				return mpDatasourcesByUID.get(uid);
			case RECORDGRANT:
				return mpRecordGrantById.get(uid);
			case CALCATTRIBUTE:
				return mpCalcAttributesById.get(uid);
			case DYNAMICENTITY:
				return mpDynamicEntitiesById.get(uid);
			case DYNAMICTASKLIST:
				return mpDynamicTasklistById.get(uid);
			case VALUELISTPROVIDER:
				return mpValuelistProviderById.get(uid);
			default:
				throw new NotImplementedException();
		}
	}

	/**
	 * get a Datasource by id where the given user has at least read permission
	 * @param dataSourceUID datasource UID
	 * @param sUserName username
	 * @return
	 */
	public DatasourceVO getDatasourcesByUser(final UID dataSourceUID, final String sUserName, UID mandator) {
		return mpDatasourcesByUID.get(dataSourceUID);
	}

	/**
	 * get datasources by creator
	 * @param sUserName
	 * @return
	 */
	public Collection<DatasourceVO> getDatasourcesByCreator(String sUserName, UID mandator) {
		if(mpDatasourcesByCreator.isEmpty() || mpDatasourcesByCreator.get(sUserName) == null) {
			findDataSourcesByCreator(sUserName, mandator);
		}
		return CollectionUtils.emptyIfNull(mpDatasourcesByCreator.get(sUserName));
	}

	/**
	 * get a datasource by name (used to check if different datasource exists with the same name).
	 * @param dataSourceUID
	 * @return DatasourceVO (may be null)
	 */
	public DatasourceVO getDatasource(final UID dataSourceUID) {
		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		DatasourceVO result = null;
		for(final DatasourceVO dsvo : mpDatasourcesByUID.values()) {
			if(dsvo.getPrimaryKey().equals(dataSourceUID)) {
				result = dsvo;
				break;
			}
		}

		return result;
	}

	/**
	 * get a valuelist provider by name.
	 * @param valuelistProviderUID
	 * @return ValuelistProviderVO (may be null)
	 */
	public ValuelistProviderVO getValuelistProviderByUID(final UID valuelistProviderUID) {
		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		ValuelistProviderVO result = null;
		for(ValuelistProviderVO vpvo : mpValuelistProviderById.values()) {
			if(vpvo.getPrimaryKey().equals(valuelistProviderUID)) {
				result = vpvo;
				break;
			}
		}

		return result;
	}

	/**
	 * get a record grant by name.
	 * @param recordGrantUID recordgrant UID
	 * @return RecordGrantVO (may be null)
	 */
	public RecordGrantVO getRecordGrantByName(UID recordGrantUID) {
		if (mpRecordGrantById.isEmpty())
			;//findDatasourcesById();

		RecordGrantVO result = null;
		for(RecordGrantVO vpvo : mpRecordGrantById.values()) {
			if(vpvo.getPrimaryKey().equals(recordGrantUID)) {
				result = vpvo;
				break;
			}
		}

		return result;
	}

	/**
	 * get a dynamic entity
	 * 
	 * @param entityUID	dynamic entity UID
	 * @return DynamicEntityVO (may be null)
	 */
	public DynamicEntityVO getDynamicEntity(final UID entityUID) {
		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		DynamicEntityVO result = null;
		for(final DynamicEntityVO devo : mpDynamicEntitiesById.values()) {
			if(ObjectUtils.equals(devo.getId(), entityUID)) {
				result = devo;
				break;
			}
		}

		return result;
	}

	/**
	 * get a chart
	 * 
	 * @param entityUID entity UID
	 * @return ChartVO (may be null)
	 */
	public ChartVO getChart(final UID entityUID) {
		if (mpChartById.isEmpty())
			;//findDatasourcesById();

		ChartVO result = null;
		for(ChartVO cvo : mpChartById.values()) {
			if(ObjectUtils.equals(cvo.getPrimaryKey(), entityUID)) {
				result = cvo;
				break;
			}
		}

		return result;
	}

	/**
	 * helper function to get the permission for a given user from the SecurityCache
	 * @param dataSourceUID datasource UID
	 * @param sUserName
	 * @return
	 */
	public DatasourceVO.Permission getPermission(final UID dataSourceUID, final String sUserName, UID mandator) {
		final DatasourceVO.Permission result;

		if (securityCache.getWritableDataSourceUids(sUserName, mandator).contains(dataSourceUID)) {
			result = DatasourceVO.Permission.PERMISSION_READWRITE;
		}
		else if (securityCache.getReadableDataSources(sUserName, mandator).contains(dataSourceUID)) {
			result = DatasourceVO.Permission.PERMISSION_READONLY;
		}
		else {
			result = DatasourceVO.Permission.PERMISSION_NONE;
		}
		return result;
		//return DatasourceVO.PERMISSION_READWRITE;
	}

	@ManagedAttribute(description="get the size (number of data sources) of mpDatasourcesByUID")
	public int getNumberOfDatasourcesInCache() {
		return mpDatasourcesByUID.size();
	}

	@ManagedAttribute(description="get the size (number of VLP) of mpValuelistProviderById")
	public int getNumberOfValuelistProviderInCache() {
		return mpValuelistProviderById.size();
	}

	@ManagedAttribute(description="get the size (number of data record grant) of mpRecordGrantById")
	public int getNumberOfRecordGrantInCache() {
		return mpRecordGrantById.size();
	}

	@ManagedAttribute(description="get the size (number of dynamic entities) of mpDynamicEntitiesById")
	public int getNumberOfDynamicEntitiesInCache() {
		return mpDynamicEntitiesById.size();
	}

	@ManagedAttribute(description="get the size (number of dynamic task lists) of mpDynamicTasklistById")
	public int getNumberOfDynamicTaskListsInCache() {
		return mpDynamicTasklistById.size();
	}

	@ManagedAttribute(description="get the size (number of charts) of mpChartById")
	public int getNumberOfChartsInCache() {
		return mpChartById.size();
	}
	
	@ManagedAttribute(description="get the size (number of calc attributes) of mpCalcAttributesById")
	public int getNumberOfCalcAttributesInCache() {
		return mpCalcAttributesById.size();
	}

	public String getSQL(final DatasourceVO datasourcevo) throws NuclosDatasourceException {
		return sqlCache.getSQL(datasourcevo);
	}

	public void setUtils(final DatasourceServerUtils datasourceServerUtils) {
		this.datasourceServerUtils = datasourceServerUtils;
	}

	/**
	 * SQL Cache
	 */
	private final class SQLCache {

		private final Map<String, String> mpDatasourcesSQLByXML = new ConcurrentHashMap<String, String>();
		private final ThreadLocal<HashSet<UID>> processing = ThreadLocal.withInitial(() -> new HashSet<>());

		private String getSQL(DatasourceVO datasourcevo) throws NuclosDatasourceException {
			if (!mpDatasourcesSQLByXML.containsKey(datasourcevo.getSource())) {
				// alreadyProcessed. to avoid circular calls. @see SelectQuery.appendFromClause().
				if (processing.get().contains(datasourcevo.getId())) {
					throw new StackOverflowError(String.format("alreadyProcessed %s (%s)", datasourcevo.getName(),
							datasourcevo.getId()));
				}
				processing.get().add(datasourcevo.getId());
				try {
					mpDatasourcesSQLByXML.put(datasourcevo.getSource(), datasourceServerUtils.createSQLForCaching(datasourcevo, schemaCache));
				} catch (Exception e) {
					throw new NuclosDatasourceException(e.getMessage(), e.getCause());
				} finally {
					processing.get().remove(datasourcevo.getId());
				}
			}

			return mpDatasourcesSQLByXML.get(datasourcevo.getSource());
		}

		public void invalidate() {
			mpDatasourcesSQLByXML.clear();
		}
	}

}
