//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosExceptionUtils;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.WhitelistedString;
import org.nuclos.common.collect.collectable.ValueListProviderType;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.database.query.SelectQuery;
import org.nuclos.common.database.query.definition.Column;
import org.nuclos.common.database.query.definition.Join;
import org.nuclos.common.database.query.definition.Join.JoinType;
import org.nuclos.common.database.query.definition.Schema;
import org.nuclos.common.database.query.definition.Table;
import org.nuclos.common.dblayer.IFieldRef;
import org.nuclos.common.querybuilder.DatasourceParameterParser;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.querybuilder.DatasourceXMLParser;
import org.nuclos.common.querybuilder.DatasourceXMLParser.XMLConnector;
import org.nuclos.common.querybuilder.DatasourceXMLParser.XMLTable;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceParameterValuelistproviderVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dal.processor.IDataLanguageLocator;
import org.nuclos.server.dal.processor.jdbc.ICalcAttributeBuilder;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbType;
import org.nuclos.server.dblayer.incubator.DbExecutor.LimitedResultSetRunner;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.report.SchemaCache;
import org.nuclos.server.report.WhereConditionParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class DatasourceServerUtils {
	
	private static final Logger LOG	= LoggerFactory.getLogger(DatasourceServerUtils.class);

	protected final IMandatorUtils mandatorUtils;
	protected final SessionUtils sessionUtils;
	protected final ISecurityCache securityCache;
	protected final SpringDataBaseHelper dataBaseHelper;

	protected final MetaProvider metaProv;

	public static final String AVOID_DOUBLE_DASHS = "(?!^.*--.*$)"; // <-- SQL Comments. Avoid it, if '-' is allowed as char in general
	public static final Pattern PATTERN_FOR_VALID_INTID_UID_VALUES = Pattern.compile(AVOID_DOUBLE_DASHS+"^[0-9a-z_-]+$", Pattern.CASE_INSENSITIVE);
	public static final Pattern PATTERN_FOR_VALID_LONG_VALUES = Pattern.compile("^-?[0-9]+$");
	public static final Pattern PATTERN_FOR_VALID_DOUBLE_VALUES = Pattern.compile("^-?[.0-9]+$");
	public static final Pattern PATTERN_FOR_VALID_BOOLEAN_VALUES = Pattern.compile("^(true|false|0|1)$", Pattern.CASE_INSENSITIVE);
	public static final Pattern PATTERN_FOR_VALID_DATE_VALUES = Pattern.compile(AVOID_DOUBLE_DASHS+"^[0-9-]+$");
	public static final Pattern PATTERN_FOR_VALID_LOCALE_VALUES = Pattern.compile(AVOID_DOUBLE_DASHS+"^[0-9a-z_-]+$", Pattern.CASE_INSENSITIVE);
	// support values like "c3po" or "c3po','r2d2"
	public static final Pattern PATTERN_FOR_VALID_MANDATOR_VALUES = Pattern.compile(AVOID_DOUBLE_DASHS+"^([0-9a-z_-]+)((',')([0-9a-z_-]+))*$", Pattern.CASE_INSENSITIVE);

	private static Map<String, Pattern> PATTERNS_PER_PARAMETER_NAME = new HashMap<>();
	private static Map<String, Pattern> PATTERNS_PER_PARAMETER_TYPE = new HashMap<>();

	static {
		PATTERNS_PER_PARAMETER_NAME.put("intid", PATTERN_FOR_VALID_INTID_UID_VALUES);
		PATTERNS_PER_PARAMETER_NAME.put("locale", PATTERN_FOR_VALID_LOCALE_VALUES);
		PATTERNS_PER_PARAMETER_NAME.put("mandator", PATTERN_FOR_VALID_MANDATOR_VALUES);
		PATTERNS_PER_PARAMETER_NAME = Collections.unmodifiableMap(PATTERNS_PER_PARAMETER_NAME);

		PATTERNS_PER_PARAMETER_TYPE.put("java.lang.Boolean", PATTERN_FOR_VALID_BOOLEAN_VALUES);
		PATTERNS_PER_PARAMETER_TYPE.put("java.lang.Long", PATTERN_FOR_VALID_LONG_VALUES);
		PATTERNS_PER_PARAMETER_TYPE.put("java.lang.Integer", PATTERN_FOR_VALID_LONG_VALUES);
		PATTERNS_PER_PARAMETER_TYPE.put("java.lang.Double", PATTERN_FOR_VALID_DOUBLE_VALUES);
		PATTERNS_PER_PARAMETER_TYPE.put("java.math.BigDecimal", PATTERN_FOR_VALID_DOUBLE_VALUES);
		PATTERNS_PER_PARAMETER_TYPE.put("java.util.Date", PATTERN_FOR_VALID_DATE_VALUES);
		PATTERNS_PER_PARAMETER_TYPE = Collections.unmodifiableMap(PATTERNS_PER_PARAMETER_TYPE);
	}

	public DatasourceServerUtils(
			final MetaProvider metaProv,
			final IMandatorUtils mandatorUtils,
			final SessionUtils sessionUtils,
			final ISecurityCache securityCache,
			final SpringDataBaseHelper dataBaseHelper) {
		this.metaProv = metaProv;
		this.mandatorUtils = mandatorUtils;
		this.sessionUtils = sessionUtils;
		this.securityCache = securityCache;
		this.dataBaseHelper = dataBaseHelper;
	}
	
//	public void invalidateCache() {
//		sqlCache.invalidate();
//	}

	/**
	 * get sql string for report execution.
	 * There is no authorization as the authorization is applied to the report.
	 *
	 * @param dataSourceUID    data source UID
	 * @param datasourceCache
	 * @param schemaCache
	 * @param languageSupplier
	 * @return string containing sql
	 */

	public String createSQLForReportExecution(
			final UID dataSourceUID,
			Map<String, Object> mpParams,
			final DatasourceCache datasourceCache,
			final SchemaCache schemaCache,
			Supplier<UID> languageSupplier
	) throws NuclosDatasourceException {
		DatasourceVO vo = datasourceCache.getDatasource(dataSourceUID);
		return createSQL(vo, mpParams, null, datasourceCache, schemaCache, languageSupplier, null, Collections::emptySet);
	}

	/**
	 * get sql string for datasource definition without parameter definition
	 *
	 * @param datasourcevo    datasource
	 * @param datasourceCache
	 * @return string containing sql
	 */

	public String createSQL(DatasourceVO datasourcevo, final DatasourceCache datasourceCache) throws NuclosDatasourceException {
		return datasourceCache.getSQL(datasourcevo);
	}

	public String getSqlWithIdForInClause(
			DatasourceVO datasourcevo,
			Map<String, Object> mpParams,
			final DatasourceCache datasourceCache,
			final SchemaCache schemaCache,
			Supplier<UID> languageSupplier,
			UID mandator,
			final Supplier<Set<UID>> accessibleMandatorsSupplier) throws NuclosDatasourceException {
		//@see NUCLOSINT-1347
		String sqlSelect = null;
		String sql = createSQL(datasourcevo, mpParams, null, datasourceCache, schemaCache, languageSupplier, mandator, accessibleMandatorsSupplier);
		String sIdField = (String)mpParams.get(ValuelistProviderVO.DATASOURCE_IDFIELD);
		if (StringUtils.isNullOrEmpty(sIdField)) {
			int idxIntid = sql.toLowerCase().indexOf("intid");
			if (idxIntid != -1) {
				sIdField = sql.substring(idxIntid, idxIntid+5);
			}
		}
		if (StringUtils.isNullOrEmpty(sIdField)) {
			throw new IllegalArgumentException("id-field or name-field is needed to construct the sql clause here.");
 		}
		sqlSelect = "(SELECT ds." + (!sql.contains("\"" + sIdField + "\"") ? sIdField : "\"" + sIdField + "\"") + " FROM (" + sql + ") ds)";
 		return sqlSelect;
	}


	public String getSqlQueryForId(
			DatasourceVO datasourcevo,
			Map<String, Object> mpParams,
			final DatasourceCache datasourceCache,
			final SchemaCache schemaCache, Object id,
			final Supplier<UID> languageSupplier,
			final Supplier<Set<UID>> accessibleMandatorsSupplier) throws NuclosDatasourceException {
		if (id == null) {
			throw new NullPointerException();
		}

		// Avoid SQL injection (tp)
		if (!(id instanceof Number) && !(id instanceof UID)) {
			throw new IllegalArgumentException("id is " + id + " of " + id.getClass().getName());
		}

		//@see NUCLOSINT-1347
		String sqlSelect = null;
		String sql = createSQL(datasourcevo, mpParams, null, datasourceCache, schemaCache, languageSupplier, null, accessibleMandatorsSupplier);

		String sIdField = (String)mpParams.get(ValuelistProviderVO.DATASOURCE_IDFIELD);
		if (StringUtils.isNullOrEmpty(sIdField)) {
			String idAlias = "intid";
			if (RecordGrantVO.class.isAssignableFrom(datasourcevo.getClass())) {
				RecordGrantVO recordGrantVO = (RecordGrantVO) datasourcevo;
				idAlias = E.isNuclosEntity(recordGrantVO.getEntityUID()) ? "struid" : "intid";
			}
			int idxIntid = sql.toLowerCase().indexOf(idAlias);
			if (idxIntid != -1) {
				sIdField = sql.substring(idxIntid, idxIntid + idAlias.length());
			}
 		}
		if (StringUtils.isNullOrEmpty(sIdField)) {
			throw new IllegalArgumentException("id-field or name-field is needed to construct the sql clause here.");
 		}
		sqlSelect = "SELECT * FROM (" + sql + ") ds WHERE ds."
				+ (!sql.contains("\"" + sIdField + "\"") ? sIdField : "\"" + sIdField + "\"") + "= " + (id instanceof UID ? "'" + ((UID)id).getString() + "'" : String.valueOf(id));
		return sqlSelect;
	}

	/**
	 * get sql string for datasource definition
	 *
	 * @param datasourcevo                contains xml of datasource
	 * @param from
	 * @param datasourceCache
	 * @param schemaCache
	 * @param languageSupplier
	 * @param accessibleMandatorsSupplier
	 * @return string containing sql
	 */

	public String createSQL(DatasourceVO datasourcevo,
							Map<String, Object> mpParams,
							final DbFrom<?> from, final DatasourceCache datasourceCache,
							final SchemaCache schemaCache,
							Supplier<UID> languageSupplier,
							UID dataMandator,
							final Supplier<Set<UID>> accessibleMandatorsSupplier
	) throws NuclosDatasourceException {
		String result = datasourceCache.getSQL(datasourcevo);
		fillParameters(mpParams, languageSupplier, dataMandator, accessibleMandatorsSupplier);

		if (mpParams.get("mandator") != null
				&& securityCache.isMandatorPresent()
				&& SystemFields.isMandatorColumn(dataMandator)) {
			result = result.replaceAll(Matcher.quoteReplacement("'$mandator'"), Matcher.quoteReplacement("$mandator"));
		}


		result = replaceParameters(result, mpParams, from, datasourceCache, schemaCache, getParameterDefinitionMap(datasourcevo), languageSupplier, accessibleMandatorsSupplier);
		LOG.debug(result);

		return result;
	}

	protected void fillParameters(Map<String, Object> mpParams, Supplier<UID> languageSupplier, final UID dataMandator, Supplier<Set<UID>> accessibleMandatorsSupplier) {
		if (mpParams.get("username") == null) {
			// username is already set. seems to be from preview of the datasource. @see NUCLOSINT-1560
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null && auth.getPrincipal() != null) {
				mpParams.put("username", auth.getPrincipal());
			}
		}

		UID language = languageSupplier.get();
		mpParams.put("locale", language != null ? language.toString() : null);

		if (mpParams.get("dataMandator") == null) {
			final StringBuffer sbMandator = new StringBuffer();
			if (securityCache.isMandatorPresent()) {
				if (SystemFields.isMandatorColumn(dataMandator)) {
					String alias = "nuclos_" + E.MANDATOR_ACCESSIBLE.getUID().getString();
					sbMandator.append("SELECT " + alias + "." + E.MANDATOR_ACCESSIBLE.accessible.getDbColumn());
					sbMandator.append(" FROM " + E.MANDATOR_ACCESSIBLE.getDbTable() + " " + alias);
					sbMandator.append(" WHERE " + alias + "." + E.MANDATOR_ACCESSIBLE.mandator.getDbColumn() + " = " + dataMandator.getString());

					mpParams.put("mandator", new WhitelistedString(sbMandator.toString()));

//					if (parameterDefinitionMap != null) {
//						parameterDefinitionMap.put("mandator", new DatasourceParameterVO(null, "mandator", "SQL", null, false));
//					}
				} else {
					final Set<UID> mandators = accessibleMandatorsSupplier.get();

					if (mandators != null) {
						final Iterator<UID> it = mandators.iterator();
						boolean isFirst = true;
						while (it.hasNext()) {
							if (isFirst) {
								isFirst = false;
							} else {
								sbMandator.append('\'');
							}
							sbMandator.append(it.next());
							if (it.hasNext()) {
								sbMandator.append('\'');
								sbMandator.append(',');
							}
						}
					}

					mpParams.put("mandator", sbMandator.toString());
				}
			} else {
				mpParams.put("mandator", new WhitelistedString(sbMandator.toString()));
			}
		}
	}

	/**
	 * @param datasourcevo    datasource
	 * @param datasourceCache
	 * @return sql with original parameter placeholders
	 * @throws NuclosDatasourceException
	 */

	public String createSQLOriginalParameter(DatasourceVO datasourcevo, final DatasourceCache datasourceCache) throws NuclosDatasourceException {
		return datasourceCache.getSQL(datasourcevo);
	}

	/**
	 * get sql string for datasource definition
	 *
	 * @param datasourcevo
	 * @param schemaCache
	 * @return string containing sql
	 */
	public String createSQLForCaching(DatasourceVO datasourcevo, final SchemaCache schemaCache) throws NuclosDatasourceException {
		final String sDatasourceXML = datasourcevo.getSource();
		final Object objLOCK = new Object();
		synchronized (objLOCK) {

			String result = null;
			final SelectQuery query = new SelectQuery();

			final DatasourceXMLParser.Result parseresult = parseDatasourceXML(sDatasourceXML);

			if(parseresult.isModelUsed()) {
				final Schema schema = schemaCache.getCurrentSchema();

				// create from clause
				final Map<String, Table> fromTables = new HashMap<String, Table>();
				for (XMLTable xmlTable : parseresult.getMapXMLTables().values()) {
					Table t = createTable(schema, xmlTable, datasourcevo, schemaCache);
					fromTables.put(t.getAlias(), t);
					query.addToFromClause(t);
				}

				for(XMLConnector xmlconnector : parseresult.getLstConnectors()) {
					if (fromTables.get(xmlconnector.getSrcTable()) == null) {
						continue;
					}
					final Table srcTable = (Table) (fromTables.get(xmlconnector.getSrcTable())).clone();
					final Column srcColumn = new Column(srcTable.getColumn(xmlconnector.getSrcColumn()));
					if (fromTables.get(xmlconnector.getDstTable()) == null) {
						continue;
					}
					final Table dstTable = (Table) (fromTables.get(xmlconnector.getDstTable())).clone();
					final Column dstColumn = new Column(dstTable.getColumn(xmlconnector.getDstColumn()));
					final JoinType joinType = xmlconnector.getJoinType() == null ? JoinType.INNER_JOIN : JoinType.getByType(xmlconnector.getJoinType());
					Join join = new Join(srcColumn, joinType, dstColumn);
					query.addJoin(join);
				}

				// create select, where, group by, order by claues
				final boolean bGroupBy = checkGroupBy(parseresult.getLstColumns());
				final List<String> lstColumnConditions = new ArrayList<>();

				for(DatasourceXMLParser.XMLColumn xmlcolumn : parseresult.getLstColumns()) {
					final Column column = createColumn(fromTables, xmlcolumn);
					if (column == null) {
						continue;
					}
					
					if(xmlcolumn.isVisible()) {
						query.addToSelectClause(column, xmlcolumn.getAlias());
					}
					if(bGroupBy) {
						if(xmlcolumn.getGroup() == null || xmlcolumn.getGroup().length() == 0) {
							xmlcolumn.setGroup(DatasourceVO.GroupBy.MAX.getLabel());
						}
						else if(xmlcolumn.getGroup().equals(DatasourceVO.GroupBy.GROUP.getLabel())) {
							query.addToGroupByClause(column, xmlcolumn.getGroup());
						}
						else {
							query.addToGroupByMap(column, xmlcolumn.getGroup());
						}
					}
					lstColumnConditions.clear();
					final WhereConditionParser whereConditionParser = new WhereConditionParser();
					for(DatasourceXMLParser.XMLCondition xmlcondition : xmlcolumn.getLstConditions()) {
						if(xmlcondition.getCondition() != null && xmlcondition.getCondition().length() > 0) {
							final String sCondition = whereConditionParser.parseCondition(column, xmlcondition.getCondition());
							lstColumnConditions.add(sCondition);
						}
					}

					if(lstColumnConditions.size() > 0) {
						query.addColumnWhereClauses(lstColumnConditions);
					}
					if(xmlcolumn.getSort() != null && xmlcolumn.getSort().length() > 0) {
						query.addToOrderByClause(column, xmlcolumn.getSort().equals(DatasourceVO.OrderBy.ASCENDING.getLabel()));
					}
				}
				result = query.getSelectStatement(parseresult.isEntityOptionDynamic(),
							SpringDataBaseHelper.getInstance().getDbAccess().getDbType().equals(DbType.MSSQL));
			}
			else {
				result = RigidUtils.cleanSql(parseresult.getQueryStringFromXml());
			}

			LOG.debug(result);
			return result;
		}
	}

	/**
	 * parses the datasource XML
	 *
	 * @param sDatasourceXML
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */

	public DatasourceXMLParser.Result parseDatasourceXML(
			String sDatasourceXML) throws NuclosFatalException,
	    NuclosDatasourceException {
		return DatasourceXMLParser.parse(sDatasourceXML);
	}

	/**
	 * return a copy of a Table in Schema because more than one instance is
	 * possible in the Datasource
	 *
	 * @param schema
	 * @param xmltable
	 * @param schemaCache
	 * @return a copy of the specified table from the schema.
	 */
	private Table createTable(Schema schema,
							  XMLTable xmltable,
							  DatasourceVO datasourcevo, final SchemaCache schemaCache) {
		final Table table = schema.getTable(xmltable.getEntity());
		if(table != null) {
			// cache the columns in schema
			schemaCache.getColumns(table);
			final Table resultTable = (Table) table.clone();
			resultTable.setAlias(xmltable.getId());	

			try {
				EntityMeta<?> eMeta = metaProv.getByTablename(xmltable.getEntity());

				if (!(datasourcevo instanceof DynamicEntityVO) && !(datasourcevo instanceof ChartVO)
						&& securityCache.isMandatorPresent()) {
					if (eMeta.isMandator()) {
						String tableLocaleReplacement = null;
						if (eMeta.IsLocalized()) {
							tableLocaleReplacement = createLocaleTableJoinSQL(eMeta, resultTable);
						}
						String tableNameReplacement = "(SELECT * FROM " + (tableLocaleReplacement != null ? tableLocaleReplacement : resultTable.getName()) + " WHERE " + SF.MANDATOR_UID.getDbColumn() + " in ('$mandator'))";
						resultTable.setRestriction(tableNameReplacement);
					} else {
						if (eMeta.IsLocalized()) {
							resultTable.setRestriction(
									createLocaleTableJoinSQL(eMeta, resultTable));
						}
					}
				} else {
					if (eMeta.IsLocalized()) {
						resultTable.setRestriction(
								createLocaleTableJoinSQL(eMeta, resultTable));
					}
				}
			} catch (CommonFatalException cfe) {
				// not an entity table; typically it's another data source.
			}
			return resultTable;
		}
		return new Table(schema.getTimestamp(), "");
	}

	protected String createLocaleTableJoinSQL(EntityMeta<?> eMeta,
			Table resultTable) {
		// localization is not supported at this point of execution
		return null;
	}

	private Column createColumn(Map<String, Table> mpTables,
	    DatasourceXMLParser.XMLColumn column) {
		final Table table = mpTables.get(column.getTable());
		if (table == null) {
			return null;
		}
		
		final Column result = new Column(table.getColumn(column.getColumn()));
		result.setTable(mpTables.get(column.getTable()));
		result.setAlias(column.getAlias());

		return result;
	}

	private boolean checkGroupBy(
	    List<DatasourceXMLParser.XMLColumn> lstColumns) {
		for(DatasourceXMLParser.XMLColumn xmlcolumn : lstColumns) {
			if((xmlcolumn != null && xmlcolumn.getGroup() != null)
			    || (xmlcolumn != null && xmlcolumn.getGroup() != null && xmlcolumn.getGroup().length() > 0)) {
				return true;
			}
		}
		return false;
	}

	private Map<String, DatasourceParameterVO> getParameterDefinitionMap(final DatasourceVO datasourceVO) throws NuclosDatasourceException {
		final Map<String, DatasourceParameterVO> result = new HashMap<>();
		final List<DatasourceParameterVO> parameters = getParameters(datasourceVO.getSource());
		for (DatasourceParameterVO parameterVO : parameters) {
			final String param = parameterVO.getParameter();
			result.put(param, parameterVO);
		}
		return result;
	}

	/**
	 * replace all parameters in sSql with the values from mpParams
	 *
	 * @param sSql
	 * @param mpParams
	 * @param from
	 * @param datasourceCache
	 * @param schemaCache
	 * @param parameterDefinitionMap
	 * @return sql string with parameters replaced.
	 */
	private String replaceParameters(String sSql, Map<String, ?> mpParams, final DbFrom<?> from, final DatasourceCache datasourceCache, final SchemaCache schemaCache, final Map<String, DatasourceParameterVO> parameterDefinitionMap, final Supplier<UID> languageSupplier, final Supplier<Set<UID>> accessibleMandatorsSupplier) throws NuclosDatasourceException {
		final StringBuilder result = new StringBuilder(sSql.length());
		final DatasourceParameterParser p = new DatasourceParameterParser(sSql);
		for (IFieldRef token: p) {
			final String content = token.getContent();
			if (token.isConstant()) {
				result.append(content);
			}
			else {
				final DatasourceParameterVO parameterVO = parameterDefinitionMap.get(content);
				result.append(getParamValueForReplacement("$" + content, parameterVO, mpParams, from, datasourceCache, schemaCache, languageSupplier, accessibleMandatorsSupplier));
			}
		}
		return result.toString();
	}

	private void checkParameterValueAgainstPattern(String sParameter, String sParameterValue, Pattern pattern) {
		if (!pattern.matcher((String)sParameterValue).matches()) {
			LOG.warn(String.format("Forbidden char in datasource parameter (%s) value \"%s\" found. For security purposes, this regex pattern must match: %s",
					sParameter, sParameterValue, pattern.pattern()));
			throw NuclosExceptionUtils.reduceStackTraceElements(new IllegalArgumentException("Forbidden character found."), 1);
		}
	}

	/**
	 * replace get the value for sParameter from mapParams
	 *
	 * @param sParameter
	 * @param parameterVO
	 * @param mapParams
	 * @param from
	 * @param datasourceCache
	 * @param schemaCache
	 * @return the value for sParameter from the map, or sParameter if no value
	 */
	private String getParamValueForReplacement(
			String sParameter,
			DatasourceParameterVO parameterVO,
			Map<String, ?> mapParams,
			final DbFrom<?> from, final DatasourceCache datasourceCache,
			final SchemaCache schemaCache,
			final Supplier<UID> languageSupplier,
			final Supplier<Set<UID>> accessibleMandatorsSupplier
	) throws NuclosDatasourceException {
		final String sDatatype = parameterVO==null?null:parameterVO.getDatatype();
		final String sKey = sParameter.replace('$', ' ').trim();
		if (!mapParams.containsKey(sKey)) {
			return sParameter;
		}

		Object param = mapParams.get(sKey);
		if (param instanceof String) {
			final String sValue = (String) param;
			if (!RigidUtils.looksEmpty(sValue) &&
					!"null".equalsIgnoreCase(sValue) &&
					!"0".equals(sValue)) {
				if (parameterVO == null) {
					// internal field like username, intid, dataMandator, locale...
					Pattern pattern = PATTERNS_PER_PARAMETER_NAME.get(sKey.toLowerCase());
					if (pattern != null) {
						checkParameterValueAgainstPattern(sParameter, sValue, pattern);
					}
				} else {
					String datatype = parameterVO.getDatatype();
					Pattern pattern = PATTERNS_PER_PARAMETER_TYPE.get(datatype);
					if (pattern != null) {
						checkParameterValueAgainstPattern(sParameter, sValue, pattern);
					}
				}
			}
		} else if (param instanceof WhitelistedString) {
			param = ((WhitelistedString) param).get();
		}

		boolean bEscapeResult = parameterVO != null; // ==null ? internal field -> do not escape
		if (param instanceof Boolean) {
			param = dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL((Boolean)param);
			bEscapeResult = false;
		} else if (param instanceof Date || ("java.util.Date".equals(sDatatype) && param instanceof Long)) {
			// ISO-format 'yyyy-mm-dd', matches JDBC escape syntax
			param = String.format("%tF", param);
			bEscapeResult = false;
		} else if (param instanceof String) {
			if ("java.util.List<String>".equals(sDatatype)) {
				// e.g. test string from query editor
				String sParam = (String) param;
				sParam = sParam.trim();
				if (sParam.startsWith("(") || sParam.startsWith("[")) {
					sParam = sParam.substring(1);
				}
				if (sParam.endsWith(")") || sParam.endsWith("]")) {
					sParam = sParam.substring(0, sParam.length()-1);
				}
				List<String> list = new ArrayList<>();
				for (String s : sParam.split(",")) {
					if (s.startsWith("'") || s.startsWith("\"")) {
						s = s.substring(1);
					}
					if (s.endsWith("'") || s.endsWith("\"")) {
						s = s.substring(0, s.length()-1);
					}
					list.add(s);
				}
				param = list;
			}
		}

		if (param instanceof List) {
			if ("java.util.List<String>".equals(sDatatype)) {
				StringBuilder s = new StringBuilder();
				for (Object o : (List)param) {
					if (o != null) {
						s.append(s.length()==0?'(':',').append('\'');
						try {
							s.append(dataBaseHelper.getDbAccess().escapeLiteral(o.toString()));
						} catch (SQLException e) {
							throw new NuclosDatasourceException(e);
						}
						s.append('\'');
					}
				}
				s.append(s.length()==0?"(null)":")");
				param = s;
				bEscapeResult = false;
			} else if ("java.util.List<Integer>".equals(sDatatype)) {
				StringBuilder s = new StringBuilder();
				for (Object o : (List)param) {
					if (o instanceof Number) {
						s.append(s.length()==0?'(':',').append(String.valueOf(o));
					}
				}
				s.append(s.length()==0?"(null)":")");
				param = s;
				bEscapeResult = false;
			} else {
				String s = param.toString();
				param = "(" + s.substring(1, s.length() - 1) + ")";
			}
		}
		if (param != null) {
			String result = param.toString();

			if ("SQL".equals(sDatatype)) {
				if (DatasourceUtils.getTestValueForParameter(parameterVO, dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL(false)).equals(result)) {
					// for datasource validation test... no whitelist check... we escape
				} else {
					final DatasourceParameterValuelistproviderVO parameterWhitelistVLP = parameterVO.getValueListProvider();
					if (parameterWhitelistVLP == null) {
						throw new NuclosDatasourceException("Valuelist provider of datasource parameter \"" + sParameter + "\" (SQL type) is null.");
					}
					final UID whitelistVLPUID = UID.parseUID(parameterWhitelistVLP.getValue());
					try {
						if (whitelistVLPUID != null) {
							final ValuelistProviderVO whitelistProviderVO = getValuelistProvider(whitelistVLPUID, datasourceCache);

							if (whitelistProviderVO == null) {
								if (ValueListProviderType.ATTRIBUTE_COLOR_SQL_WHITELIST.getType().equals(whitelistVLPUID.toString())) {
									final String struidfield = (String) mapParams.get("struidfield");
									final FieldMeta<?> baseField = metaProv.getEntityField(UID.parseUID(struidfield));
									if (!baseField.isCalculated()) {
										if (!result.endsWith("." + baseField.getDbColumn())) {
											throw new NuclosDatasourceException("Value \"" + result + "\" is not a valid whitelist entry of datasource parameter \"" + sParameter + "\" (SQL type)");
										}
									} else {
										if (from != null) {
											if (baseField.getCalcAttributeDS() != null) {
												final Object calcAttributeBuilder = FieldMeta.getCalcAttributeBuilder(baseField, metaProv);
												if (calcAttributeBuilder != null) {
													final String sSQL = ((ICalcAttributeBuilder) calcAttributeBuilder).buildCalcAttributeSQL(from);
													if (!StringUtils.equalsIgnoreCase("(" + sSQL + ")", result)) {
														throw new NuclosDatasourceException("Value \"" + result + "\" is not a valid whitelist entry of datasource parameter \"" + sParameter + "\" (SQL type)");
													}
												} else {
													throw new NuclosDatasourceException("Value \"" + result + "\" is not a valid whitelist entry of datasource parameter \"" + sParameter + "\" (SQL type)");
												}
											} else if (baseField.getCalcFunction() != null) {
												String localeAdd = baseField.isLocalized() ? " , '" + ((IDataLanguageLocator) FieldMeta.getDataLanguageLocator(baseField, metaProv)).getDataLanguageToUse() + "'" : "";

												final String sSQL = baseField.getCalcFunction() + "(" + from.basePk().getSqlColumnExpr() + localeAdd + ")";
												if (!StringUtils.equalsIgnoreCase("(" + sSQL + ")", result)) {
													throw new NuclosDatasourceException("Value \"" + result + "\" is not a valid whitelist entry of datasource parameter \"" + sParameter + "\" (SQL type)");
												}
											} else {
												throw new NuclosDatasourceException("Value \"" + result + "\" is not a valid whitelist entry of datasource parameter \"" + sParameter + "\" (SQL type)");
											}
										}
									}
									bEscapeResult = false;
								} else {
									throw new NuclosDatasourceException("Value \"" + result + "\" is not a valid whitelist entry of datasource parameter \"" + sParameter + "\" (SQL type)");
								}
							} else {

								final Map<String, DatasourceParameterVO> whitelistParameterDefinitionMap = getParameterDefinitionMap(whitelistProviderVO);
								VLPQuery q = new VLPQuery(whitelistVLPUID);

								final Map<String, Object> parsedValuesMap = DatasourceUtils.parseAllParameters(parameterWhitelistVLP.getParameters(), whitelistParameterDefinitionMap,
										dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL(true),
										dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL(false));
								q.setQueryParams(parsedValuesMap);
								q.setIdFieldName(parameterWhitelistVLP.getParameters().get(ValuelistProviderVO.DATASOURCE_IDFIELD));
								final String whitelistColumnName = parameterWhitelistVLP.getParameters().get(ValuelistProviderVO.DATASOURCE_NAMEFIELD);
								q.setNameField(whitelistColumnName);
								final ResultVO whitelistResultVO = executeQueryForVLP(datasourceCache, schemaCache, q, languageSupplier, accessibleMandatorsSupplier);
								final List<ResultColumnVO> columns = whitelistResultVO.getColumns();
								int whitelistColumnIndex = -1;
								for (int iIndex = 0; iIndex < columns.size(); iIndex++) {
									final ResultColumnVO rcvo = columns.get(iIndex);
									final String label = rcvo.getColumnLabel();
									if (label.equalsIgnoreCase(whitelistColumnName)) {
										whitelistColumnIndex = iIndex;
									}
								}
								if (whitelistColumnIndex == -1) {
									throw new NuclosDatasourceException("Column \"" + whitelistColumnName + "\" not found in whitelist result of datasource parameter \"" + sParameter + "\" (SQL type)");
								}
								boolean bValidWhitelistEntry = false;
								for (Object[] row : whitelistResultVO.getRows()) {
									if (result.equals(row[whitelistColumnIndex])) {
										bValidWhitelistEntry = true;
										break;
									}
								}
								if (!bValidWhitelistEntry) {
									throw new NuclosDatasourceException("Value \"" + result + "\" is not a valid whitelist entry of datasource parameter \"" + sParameter + "\" (SQL type)");
								}
								bEscapeResult = false;
							}
						}
					} catch (CommonBusinessException e) {
						throw new NuclosDatasourceException(e);
					}
				}
			}

			if (bEscapeResult) {
				try {
					result = dataBaseHelper.getDbAccess().escapeLiteral(result);
				} catch (SQLException e) {
					throw new NuclosDatasourceException(e);
				}
			}

			return result;
		}
		else {
			return "null";
		}
	}

	/**
	 * Get list of integers which test set (1, 2, 3) are set for any parameter
	 * @param sDatasourceXML
	 * @return
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */

	public List<String> getTestSetsToExecute(String sDatasourceXML) throws NuclosFatalException, NuclosDatasourceException {
		final DatasourceXMLParser.Result parserresult = parseDatasourceXML(sDatasourceXML);
		return parserresult.getTestSetsToExecute();
	}

	/**
	 * Return test sets for all parameters
	 * @param sDatasourceXML
	 * @return
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */

	public List<Map<String, String>> getTestSetsForParameters(String sDatasourceXML) throws NuclosFatalException, NuclosDatasourceException {
		final DatasourceXMLParser.Result parserresult = parseDatasourceXML(sDatasourceXML);
		return parserresult.getLstParameterTestValues();
	}

	/**
	 * Return test set value for given parameter
	 * @param sDatasourceXML
	 * @param parameterVOIndex
	 * @param testSet
	 * @return
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */

	public String getTestValueForParameter(String sDatasourceXML, int parameterVOIndex, String testSet) throws NuclosFatalException, NuclosDatasourceException {
		final DatasourceXMLParser.Result parserresult = parseDatasourceXML(sDatasourceXML);
		return parserresult.getTestValuesForParameter(parameterVOIndex).get(testSet);
	}

	/**
	    * Retrieve the parameters a datasource accepts.
	    * @param sDatasourceXML
	    * @return
	    * @throws NuclosFatalException
	    * @throws NuclosDatasourceException
	    */

	   public List<DatasourceParameterVO> getParameters(String sDatasourceXML) throws NuclosFatalException, NuclosDatasourceException {
			// @todo we need a getParameters(Integer iDatasourceId) method for the
			// remote interface to prevent that every user needs the right to
			// execute the get method
			final List<DatasourceParameterVO> result = new ArrayList<DatasourceParameterVO>();
			final DatasourceXMLParser.Result parseresult = parseDatasourceXML(sDatasourceXML);
			result.addAll(parseresult.getLstParameters());
			return result;
	   }

	   /**
		* Retrieve the parameters a datasource accepts.
		*
		* @param dataSourceUID
		* @param datasourceCache
		* @return
		* @throws NuclosFatalException
		* @throws NuclosDatasourceException
		*/

	   public List<DatasourceParameterVO> getParameters(final UID dataSourceUID, final DatasourceCache datasourceCache) throws NuclosFatalException, NuclosDatasourceException {
	 		final List<DatasourceParameterVO> result = new ArrayList<DatasourceParameterVO>();
	 		final DatasourceVO dsvo = datasourceCache.get(dataSourceUID);
	 		final DatasourceXMLParser.Result parseresult = parseDatasourceXML(dsvo.getSource());
	 		result.addAll(parseresult.getLstParameters());
	 		return result;
	   }
	   

	   public List<DatasourceParameterVO> getCalcAttributeParameters(final UID calcAttributeUID, final DatasourceCache datasourceCache) throws NuclosFatalException, NuclosDatasourceException {
	 		final List<DatasourceParameterVO> result = new ArrayList<DatasourceParameterVO>();
	 		final DatasourceVO dsvo = datasourceCache.getCalcAttribute(calcAttributeUID);
	 		final DatasourceXMLParser.Result parseresult = parseDatasourceXML(dsvo.getSource());
	 		result.addAll(parseresult.getLstParameters());
	 		return result;
	   }


	public ResultVO executeQueryForVLP(final DatasourceCache datasourceCache, final SchemaCache schemaCache, VLPQuery query, final Supplier<UID> languageSupplier, final Supplier<Set<UID>> accessibleMandatorsSupplier) throws CommonBusinessException {

		String sql = createSQLForVLP(query, datasourceCache, schemaCache, languageSupplier, accessibleMandatorsSupplier);

		if (query.getBaseEntityUID() != null) {
			final EntityMeta<?> eMeta = metaProv.getEntity(query.getBaseEntityUID());
			if (eMeta.isMandator()) {
				final String baseTable = eMeta.getDbSelect();
				final Set<UID> mandators = mandatorUtils.getAccessibleMandators(query.getMandatorUID());
				final StringBuffer sMandators = new StringBuffer();
				for (Iterator<UID> iterator = mandators.iterator(); iterator.hasNext(); ) {
					sMandators.append('\'');
					sMandators.append(iterator.next().getString());
					sMandators.append('\'');
					if (iterator.hasNext()) {
						sMandators.append(",");
					}

				}
				sql = "SELECT * FROM (" + sql + ") tmp345 WHERE tmp345.\"" + query.getIdFieldName() + "\" IN (SELECT INTID FROM " + baseTable + " WHERE STRUID_NUCLOSMANDATOR IN (" + sMandators.toString() + "))";
			}
		}

		if (org.apache.commons.lang.StringUtils.isNotBlank(query.getNameField()) &&
				org.apache.commons.lang.StringUtils.isNotBlank(query.getQuickSearchInput())
				) {
			String quickSearchSql = null;
			try {
				quickSearchSql = dataBaseHelper.getDbAccess().escapeLiteral(query.getQuickSearchInput().toUpperCase())
						.replace('*', '%')
						.replace('?', '_');
			} catch (SQLException e) {
				throw new NuclosDatasourceException(e);
			}
			sql = "SELECT * FROM (" + sql + ") tmp8329 WHERE UPPER(tmp8329.\"" + query.getNameField() + "\") LIKE '%" + quickSearchSql + "%'";
		}

		return executePlainQueryAsResultVO(sql, query.getMaxRowCount());
	}

	private String createSQLForVLP(VLPQuery query, final DatasourceCache datasourceCache, final SchemaCache schemaCache, final Supplier<UID> languageSupplier, final Supplier<Set<UID>> accessibleMandatorsSupplier) throws CommonBusinessException {
		ValuelistProviderVO vlpVO = getValuelistProvider(query.getValuelistProviderUid(), datasourceCache);
		if (vlpVO == null) {
			throw new NuclosDatasourceException("Could Not Find VLP with UID=" + query.getValuelistProviderUid());
		}

		//NUCLOS-5496
		for (DatasourceParameterVO param : getParameters(vlpVO.getSource())) {
			if (!query.getQueryParams().containsKey(param.getLabel())) {
				query.getQueryParams().put(param.getLabel(), null);
			}
		}

		return createSQL(vlpVO, query.getQueryParams(), null, datasourceCache, schemaCache, languageSupplier, null, accessibleMandatorsSupplier);
	}


	public ValuelistProviderVO getValuelistProvider(final UID valuelistProviderUID, final DatasourceCache datasourceCache) {
		return datasourceCache.getValuelistProvider(valuelistProviderUID, true);
	}


	public ResultVO executePlainQueryAsResultVO(String sql, Integer iMaxRowCount) {
		if (iMaxRowCount == null) {
			iMaxRowCount = sessionUtils.isCalledRemotely() ? LimitedResultSetRunner.MAXFETCHSIZE : -1;
		}
		return dataBaseHelper.getDbAccess().executePlainQueryAsResultVO(sql, iMaxRowCount);
	}

}
