package org.nuclos.server.common;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ILocaleProvider;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.server.report.ejb3.DataSourceType;

public class DatasourceWrapper {
	public static EntityObjectVO<UID> wrapDatasourceVO(DatasourceVO vo, UID en) {
		EntityObjectVO<UID> result = vo.getEntityObjectVO();
		result.setFieldValue(getDsValueableField(en, E.DATASOURCE.name), vo.getName());
		result.setFieldValue(getDsValueableField(en, E.DATASOURCE.description), vo.getDescription());
		result.setFieldValue(getDsValueableField(en, E.DATASOURCE.valid), vo.getValid());
		result.setFieldValue(getDsValueableField(en, E.DATASOURCE.source), vo.getSource());
		result.setFieldUid(getDsField(en, E.DATASOURCE.nuclet), vo.getNucletUID());
		result.setFieldValue(E.DATASOURCE.withRuleClass, vo.isWithRuleClass());

		if (vo instanceof RecordGrantVO) {
			result.setFieldUid(E.RECORDGRANT.entity, ((RecordGrantVO) vo).getEntityUID());
			result.setFieldValue(E.RECORDGRANT.meta, vo.getMeta());
		} else if (vo instanceof DynamicEntityVO) {
			result.setFieldUid(E.DYNAMICENTITY.entity, ((DynamicEntityVO) vo).getEntityUID());
			result.setFieldValue(E.DYNAMICENTITY.meta, vo.getMeta());
			result.setFieldValue(E.DYNAMICENTITY.query, vo.getQuery());
		} else if (vo instanceof ChartVO) {
			result.setFieldValue(E.CHART.meta, vo.getMeta());
			result.setFieldValue(E.CHART.query, vo.getQuery());
			result.setFieldUid(E.CHART.detailsEntity, ((ChartVO) vo).getDetailsEntityUID());
			result.setFieldUid(E.CHART.parentEntity, ((ChartVO) vo).getParentEntityUID());
		} else if (vo instanceof DynamicTasklistVO) {
			result.setFieldValue(E.DYNAMICTASKLIST.meta, vo.getMeta());
			result.setFieldValue(E.DYNAMICTASKLIST.query, vo.getQuery());
			result.setFieldValue(E.DYNAMICTASKLIST.datasourceRule, vo.getDatasourceRule());
			result.setFieldValue(E.DATASOURCE.withRuleClass, vo.isWithRuleClass());
		} else if (E.DATASOURCE.checkEntityUID(en)) {
			result.setFieldValue(E.DATASOURCE.datasourceRule, vo.getDatasourceRule());
			result.setFieldValue(E.DATASOURCE.withRuleClass, vo.isWithRuleClass());
		}

		return result;
	}

	public static <T> FieldMeta.Valueable<T> getDsValueableField(UID dsEntity, FieldMeta.Valueable<T> dsField) {
		return (FieldMeta.Valueable<T>) getDsField(dsEntity, dsField);
	}

	private static <T> FieldMeta<T> getDsField(UID dsEntity, FieldMeta<T> dsField) {
		if (E.DATASOURCE.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) {
				return (FieldMeta<T>) E.DATASOURCE.name;
			}
			if (E.DATASOURCE.description.equals(dsField)) {
				return (FieldMeta<T>) E.DATASOURCE.description;
			}
			if (E.DATASOURCE.valid.equals(dsField)) {
				return (FieldMeta<T>) E.DATASOURCE.valid;
			}
			if (E.DATASOURCE.source.equals(dsField)) {
				return (FieldMeta<T>) E.DATASOURCE.source;
			}
			if (E.DATASOURCE.nuclet.equals(dsField)) {
				return (FieldMeta<T>) E.DATASOURCE.nuclet;
			}
			if (E.DATASOURCE.datasourceRule.equals(dsField)) {
				return (FieldMeta<T>) E.DATASOURCE.datasourceRule;
			}
		} else if (E.DYNAMICENTITY.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICENTITY.name;
			}
			if (E.DATASOURCE.description.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICENTITY.description;
			}
			if (E.DATASOURCE.valid.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICENTITY.valid;
			}
			if (E.DATASOURCE.source.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICENTITY.source;
			}
			if (E.DATASOURCE.nuclet.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICENTITY.nuclet;
			}
		} else if (E.VALUELISTPROVIDER.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) {
				return (FieldMeta<T>) E.VALUELISTPROVIDER.name;
			}
			if (E.DATASOURCE.description.equals(dsField)) {
				return (FieldMeta<T>) E.VALUELISTPROVIDER.description;
			}
			if (E.VALUELISTPROVIDER.detailsearchdescription.equals(dsField)) {
				return (FieldMeta<T>) E.VALUELISTPROVIDER.detailsearchdescription;
			}
			if (E.DATASOURCE.valid.equals(dsField)) {
				return (FieldMeta<T>) E.VALUELISTPROVIDER.valid;
			}
			if (E.DATASOURCE.source.equals(dsField)) {
				return (FieldMeta<T>) E.VALUELISTPROVIDER.source;
			}
			if (E.DATASOURCE.nuclet.equals(dsField)) {
				return (FieldMeta<T>) E.VALUELISTPROVIDER.nuclet;
			}
		} else if (E.RECORDGRANT.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) {
				return (FieldMeta<T>) E.RECORDGRANT.name;
			}
			if (E.DATASOURCE.description.equals(dsField)) {
				return (FieldMeta<T>) E.RECORDGRANT.description;
			}
			if (E.DATASOURCE.valid.equals(dsField)) {
				return (FieldMeta<T>) E.RECORDGRANT.valid;
			}
			if (E.DATASOURCE.source.equals(dsField)) {
				return (FieldMeta<T>) E.RECORDGRANT.source;
			}
			if (E.DATASOURCE.nuclet.equals(dsField)) {
				return (FieldMeta<T>) E.RECORDGRANT.nuclet;
			}
		} else if (E.CHART.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) {
				return (FieldMeta<T>) E.CHART.name;
			}
			if (E.DATASOURCE.description.equals(dsField)) {
				return (FieldMeta<T>) E.CHART.description;
			}
			if (E.DATASOURCE.valid.equals(dsField)) {
				return (FieldMeta<T>) E.CHART.valid;
			}
			if (E.DATASOURCE.source.equals(dsField)) {
				return (FieldMeta<T>) E.CHART.source;
			}
			if (E.DATASOURCE.nuclet.equals(dsField)) {
				return (FieldMeta<T>) E.CHART.nuclet;
			}
		} else if (E.DYNAMICTASKLIST.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICTASKLIST.name;
			}
			if (E.DATASOURCE.description.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICTASKLIST.description;
			}
			if (E.DATASOURCE.valid.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICTASKLIST.valid;
			}
			if (E.DATASOURCE.source.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICTASKLIST.source;
			}
			if (E.DATASOURCE.nuclet.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICTASKLIST.nuclet;
			}
			if (E.DATASOURCE.datasourceRule.equals(dsField)) {
				return (FieldMeta<T>) E.DYNAMICTASKLIST.datasourceRule;
			}
		} else if (E.CALCATTRIBUTE.checkEntityUID(dsEntity)) {
			if (E.DATASOURCE.name.equals(dsField)) {
				return (FieldMeta<T>) E.CALCATTRIBUTE.name;
			}
			if (E.DATASOURCE.description.equals(dsField)) {
				return (FieldMeta<T>) E.CALCATTRIBUTE.description;
			}
			if (E.DATASOURCE.valid.equals(dsField)) {
				return (FieldMeta<T>) E.CALCATTRIBUTE.valid;
			}
			if (E.DATASOURCE.source.equals(dsField)) {
				return (FieldMeta<T>) E.CALCATTRIBUTE.source;
			}
			if (E.DATASOURCE.nuclet.equals(dsField)) {
				return (FieldMeta<T>) E.CALCATTRIBUTE.nuclet;
			}
		}
		throw new UnsupportedOperationException();
	}

	public static DatasourceVO getDatasourceVO(EntityObjectVO<UID> eovo, String currentUserName, UID mandator, final ISecurityCache securityCache) {

		DatasourceVO.Permission permission = DatasourceVO.Permission.PERMISSION_NONE;
		if ("INITIAL".equals(currentUserName)) {
			permission = DatasourceVO.Permission.PERMISSION_READONLY;
		} else if (securityCache.getWritableDataSourceUids(currentUserName, mandator).contains(eovo.getPrimaryKey()))
			permission = DatasourceVO.Permission.PERMISSION_READWRITE;
		else if (securityCache.getReadableDataSourceIds(currentUserName, mandator).contains(eovo.getPrimaryKey()))
			permission = DatasourceVO.Permission.PERMISSION_READONLY;

		UID en = eovo.getDalEntity();

		DatasourceVO vo = new DatasourceVO(
				eovo,
				eovo.getFieldValue(getDsValueableField(en, E.DATASOURCE.name)),
				eovo.getFieldValue(getDsValueableField(en, E.DATASOURCE.description)),
				eovo.getFieldValue(getDsValueableField(en, E.DATASOURCE.valid)),
				eovo.getFieldValue(getDsValueableField(en, E.DATASOURCE.source)),
				eovo.getFieldUid(getDsField(en, E.DATASOURCE.nuclet)),
				permission);

		vo.setNuclet(eovo.getFieldValue(getDsField(en, E.DATASOURCE.nuclet).getUID(), String.class));
		if (E.DATASOURCE.checkEntityUID(eovo.getDalEntity())) {
			vo.setWithRuleClass(Boolean.TRUE.equals(eovo.getFieldValue(E.DATASOURCE.withRuleClass)));
		}
		if (E.DATASOURCE.checkEntityUID(eovo.getDalEntity())) {
			vo.setDatasourceRule(eovo.getFieldValue(E.DATASOURCE.datasourceRule));
		}
		if (E.DYNAMICTASKLIST.checkEntityUID(eovo.getDalEntity())) {
			vo.setDatasourceRule(eovo.getFieldValue(E.DYNAMICTASKLIST.datasourceRule));
		}

		return vo;
	}

	public static ValuelistProviderVO getValuelistProviderVO(EntityObjectVO<UID> eovo, final ILocaleProvider localeProvider) {
		ValuelistProviderVO vo = new ValuelistProviderVO(
				eovo,
				eovo.getFieldValue(E.VALUELISTPROVIDER.name),
				eovo.getFieldValue(E.VALUELISTPROVIDER.description),
				eovo.getFieldValue(E.VALUELISTPROVIDER.valid),
				eovo.getFieldValue(E.VALUELISTPROVIDER.source),
				eovo.getFieldUid(E.VALUELISTPROVIDER.nuclet),
				localeProvider);

		vo.setNuclet(eovo.getFieldValue(E.VALUELISTPROVIDER.nuclet.getUID(), String.class));
		vo.setDetailsearchdescriptionResourceId(eovo.getFieldValue(E.VALUELISTPROVIDER.detailsearchdescription));

		return vo;
	}

	public static RecordGrantVO getRecordGrantVO(EntityObjectVO<UID> eovo, MetaProvider metaProvider) {
		UID entityUid = eovo.getFieldUid(E.RECORDGRANT.entity);
		if (metaProvider.checkEntity(entityUid)) {
			eovo.setFieldValue(E.RECORDGRANT.entity.getUID(), metaProvider.getEntity(entityUid).getEntityName());
		}
		RecordGrantVO vo = new RecordGrantVO(
				eovo,
				eovo.getFieldValue(E.RECORDGRANT.name),
				eovo.getFieldValue(E.RECORDGRANT.description),
				eovo.getFieldUid(E.RECORDGRANT.entity),
				eovo.getFieldValue(E.RECORDGRANT.valid),
				eovo.getFieldValue(E.RECORDGRANT.source),
				eovo.getFieldUid(E.RECORDGRANT.nuclet));

		vo.setEntity((String) eovo.getFieldValue(E.RECORDGRANT.entity.getUID()));
		vo.setNuclet(eovo.getFieldValue(E.RECORDGRANT.nuclet.getUID(), String.class));
		vo.setMeta(eovo.getFieldValue(E.RECORDGRANT.meta));

		return vo;
	}

	public static ChartVO getChartVO(EntityObjectVO<UID> eovo) {
		ChartVO vo = new ChartVO(
				eovo,
				eovo.getFieldValue(E.CHART.name),
				eovo.getFieldValue(E.CHART.description),
				eovo.getFieldUid(E.CHART.detailsEntity),
				eovo.getFieldUid(E.CHART.parentEntity),
				eovo.getFieldValue(E.CHART.valid),
				eovo.getFieldValue(E.CHART.source),
				eovo.getFieldUid(E.CHART.nuclet));

		vo.setDetailsEntity(eovo.getFieldValue(E.CHART.detailsEntity.getUID(), String.class));
		vo.setParentEntity(eovo.getFieldValue(E.CHART.parentEntity.getUID(), String.class));
		vo.setNuclet(eovo.getFieldValue(E.CHART.nuclet.getUID(), String.class));
		vo.setMeta(eovo.getFieldValue(E.CHART.meta));
		vo.setQuery(eovo.getFieldValue(E.CHART.query));

		return vo;
	}

	public static DynamicTasklistVO getDynamicTasklistVO(EntityObjectVO<UID> eovo) {
		DynamicTasklistVO vo = new DynamicTasklistVO(
				eovo,
				eovo.getFieldValue(E.DYNAMICTASKLIST.name),
				eovo.getFieldValue(E.DYNAMICTASKLIST.description),
				eovo.getFieldValue(E.DYNAMICTASKLIST.valid),
				eovo.getFieldValue(E.DYNAMICTASKLIST.source),
				eovo.getFieldUid(E.DYNAMICTASKLIST.nuclet));

		vo.setNuclet(eovo.getFieldValue(E.DYNAMICTASKLIST.nuclet.getUID(), String.class));
		vo.setMeta(eovo.getFieldValue(E.DYNAMICTASKLIST.meta));
		vo.setQuery(eovo.getFieldValue(E.DYNAMICTASKLIST.query));
		vo.setDatasourceRule(eovo.getFieldValue(E.DYNAMICTASKLIST.datasourceRule));

		return vo;
	}

	public static CalcAttributeVO getCalcAttributeVO(EntityObjectVO<UID> eovo) {
		CalcAttributeVO vo = new CalcAttributeVO(
				eovo,
				eovo.getFieldValue(E.CALCATTRIBUTE.name),
				eovo.getFieldValue(E.CALCATTRIBUTE.description),
				eovo.getFieldValue(E.CALCATTRIBUTE.valid),
				eovo.getFieldValue(E.CALCATTRIBUTE.source),
				eovo.getFieldUid(E.CALCATTRIBUTE.nuclet));

		vo.setNuclet(eovo.getFieldValue(E.CALCATTRIBUTE.nuclet.getUID(), String.class));

		return vo;
	}

	public static DynamicEntityVO getDynamicEntityVO(EntityObjectVO<UID> eovo) {
		DynamicEntityVO vo = new DynamicEntityVO(
				eovo,
				eovo.getFieldValue(E.DYNAMICENTITY.name),
				eovo.getFieldValue(E.DYNAMICENTITY.description),
				eovo.getFieldUid(E.DYNAMICENTITY.entity),
				eovo.getFieldValue(E.DYNAMICENTITY.valid),
				eovo.getFieldValue(E.DYNAMICENTITY.source),
				eovo.getFieldUid(E.DYNAMICENTITY.nuclet));

		vo.setEntity(eovo.getFieldValue(E.DYNAMICENTITY.entity.getUID(), String.class));
		vo.setNuclet(eovo.getFieldValue(E.DYNAMICENTITY.nuclet.getUID(), String.class));
		vo.setMeta(eovo.getFieldValue(E.DYNAMICENTITY.meta));
		vo.setQuery(eovo.getFieldValue(E.DYNAMICENTITY.query));

		return vo;
	}

	public static EntityObjectVO<UID> wrapValuelistProviderVO(ValuelistProviderVO vo) {
		EntityObjectVO<UID> result = vo.getEntityObjectVO();
		result.setFieldValue(E.VALUELISTPROVIDER.name, vo.getName());
		result.setFieldValue(E.VALUELISTPROVIDER.description, vo.getDescription());
		//result.setFieldValue(E.VALUELISTPROVIDER.detailsearchdescription, vo.getDetailsearchdescription());
		result.setFieldValue(E.VALUELISTPROVIDER.valid, vo.getValid());
		result.setFieldValue(E.VALUELISTPROVIDER.source, vo.getSource());
		result.setFieldUid(E.VALUELISTPROVIDER.nuclet, vo.getNucletUID());

		return result;
	}

	public static DatasourceVO wrap(
			EntityObjectVO<UID> eovo,
			final DataSourceType type,
			final String sUser,
			UID mandator,
			final ISecurityCache securityCache,
			final ILocaleProvider localeProvider,
			final MetaProvider metaProvider) {
		switch (type) {
			case DYNAMICENTITY:
				return getDynamicEntityVO(eovo);
			case VALUELISTPROVIDER:
				return getValuelistProviderVO(eovo, localeProvider);
			case RECORDGRANT:
				return getRecordGrantVO(eovo, metaProvider);
			case DYNAMICTASKLIST:
				return getDynamicTasklistVO(eovo);
			case CHART:
				return getChartVO(eovo);
			case CALCATTRIBUTE:
				return getCalcAttributeVO(eovo);
			default:
				return getDatasourceVO(eovo, sUser, mandator, securityCache);
		}
	}

	public static EntityObjectVO<UID> unwrap(final DatasourceVO dsVO, DataSourceType type, final UID entityUID) {
		if (dsVO instanceof ValuelistProviderVO) {
			return wrapDatasourceVO((ValuelistProviderVO) dsVO,
					entityUID);
		}
		return wrapDatasourceVO(dsVO, entityUID);
	}
}
