package org.nuclos.server.common;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.querybuilder.DatasourceXMLParser;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common2.exception.CommonBusinessException;

public interface IDatasourceServerUtils {

	String createSQLForReportExecution(UID dataSourceUID, Map<String, Object> mpParams, Supplier<UID> languageSupplier) throws NuclosDatasourceException;

	String createSQL(DatasourceVO datasourcevo) throws NuclosDatasourceException;

	String getSqlLikeWithIdForInClause(DatasourceVO datasourcevo, Map<String, Object> mpParams, String search,
									   Supplier<UID> languageSupplier, UID mandator, final Supplier<Set<UID>> accessibleMandatorsSupplier) throws NuclosDatasourceException;

	String getSqlWithIdForInClause(DatasourceVO datasourcevo, Map<String, Object> mpParams, Supplier<UID> languageSupplier, UID mandator, final Supplier<Set<UID>> accessibleMandatorsSupplier) throws NuclosDatasourceException;

	String getSqlQueryForId(DatasourceVO datasourcevo, Map<String, Object> mpParams, Object id, final Supplier<UID> languageSupplier, final Supplier<Set<UID>> accessibleMandatorsSupplier) throws NuclosDatasourceException;

	String createSQL(DatasourceVO datasourcevo, Map<String, Object> mpParams, Supplier<UID> languageSupplier, UID dataMandator, final Supplier<Set<UID>> accessibleMandatorSupplier) throws NuclosDatasourceException;

	String createSQLOriginalParameter(DatasourceVO datasourcevo) throws NuclosDatasourceException;

	DatasourceXMLParser.Result parseDatasourceXML(String sDatasourceXML) throws NuclosFatalException,
			NuclosDatasourceException;

	List<String> getTestSetsToExecute(String sDatasourceXML) throws NuclosFatalException, NuclosDatasourceException;

	List<Map<String, String>> getTestSetsForParameters(String sDatasourceXML) throws NuclosFatalException,
			NuclosDatasourceException;

	String getTestValueForParameter(String sDatasourceXML, int parameterVOIndex, String testSet) throws NuclosFatalException, NuclosDatasourceException;

	List<DatasourceParameterVO> getParameters(String sDatasourceXML) throws NuclosFatalException,
			NuclosDatasourceException;

	List<DatasourceParameterVO> getParameters(UID dataSourceUID) throws NuclosFatalException,
			NuclosDatasourceException;

	List<DatasourceParameterVO> getCalcAttributeParameters(UID calcAttributeUID) throws NuclosFatalException,
			NuclosDatasourceException;

	ResultVO executeQueryForVLP(VLPQuery query, final Supplier<UID> languageSupplier, final Supplier<Set<UID>> accessibleMandatorsSupplier) throws CommonBusinessException;

	ValuelistProviderVO getValuelistProvider(UID valuelistProviderUID);

	ResultVO executePlainQueryAsResultVO(String sql, Integer iMaxRowCount);
}
