package org.nuclos.server.common;

import java.util.Set;

import org.nuclos.common.UID;

public interface IMandatorUtils {
	Set<UID> getAccessibleMandators(UID mandatorFromData);
}
