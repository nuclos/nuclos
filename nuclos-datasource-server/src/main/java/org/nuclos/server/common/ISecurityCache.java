package org.nuclos.server.common;

import java.util.Collection;

import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;

public interface ISecurityCache {
	boolean isWriteAllowedForMasterData(String currentUserName, UID entity, UID currentMandatorUID);

	boolean isReadAllowedForMasterData(String currentUserName, UID dsEntity, UID currentMandatorUID);

	boolean isMandatorPresent();

	Collection<UID> getWritableDataSourceUids(String sUserName, UID mandator);

	Collection<UID> getReadableDataSources(String sUserName, UID mandator);

	Collection<UID> getReadableDataSourceIds(String currentUserName, UID mandator);

	void invalidate(boolean b);
}
