package org.nuclos.server.report.ejb3;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;

public enum DataSourceType {
	DYNAMICENTITY(E.DYNAMICENTITY, E.DYNAMICENTITYUSAGE, E.DYNAMICENTITYUSAGE.dynamicEntity.getUID(),
			E.DYNAMICENTITYUSAGE.dynamicEntityUsed.getUID()),
	VALUELISTPROVIDER(E.VALUELISTPROVIDER, E.VALUELISTPROVIDERUSAGE,
			E.VALUELISTPROVIDERUSAGE.valuelistProvider.getUID(),
			E.VALUELISTPROVIDERUSAGE.valuelistProviderUsed.getUID()),
	RECORDGRANT(E.RECORDGRANT, E.RECORDGRANTUSAGE, E.RECORDGRANTUSAGE.recordGrant.getUID(),
			E.RECORDGRANTUSAGE.recordGrantUsed.getUID()),
	DATASOURCE(E.DATASOURCE, E.DATASOURCEUSAGE, E.DATASOURCEUSAGE.datasource.getUID(),
			E.DATASOURCEUSAGE.datasourceUsed.getUID()),
	CHART(E.CHART, E.CHARTUSAGE, E.CHARTUSAGE.chart.getUID(), E.CHARTUSAGE.chartUsed.getUID()),
	DYNAMICTASKLIST(E.DYNAMICTASKLIST, E.DYNAMICTASKLISTUSAGE, E.DYNAMICTASKLISTUSAGE.dynamictasklist.getUID(),
			E.DYNAMICTASKLISTUSAGE.dynamictasklistUsed.getUID()),
	CALCATTRIBUTE(E.CALCATTRIBUTE, E.CALCATTRIBUTEUSAGE, E.CALCATTRIBUTEUSAGE.calcAttribute.getUID(),
			E.CALCATTRIBUTEUSAGE.calcAttributeUsed.getUID()),
	;

	final EntityMeta<UID> entity;
	final EntityMeta<UID> entityUsage;
	final UID fieldEntityUID;
	final UID fieldEntityUsedUID;

	DataSourceType(final EntityMeta<UID> entity, final EntityMeta<UID> entityUsage, final UID fieldEntityUID,
						   final UID fieldEntityUsedUID) {
		this.entity = entity;
		this.entityUsage = entityUsage;
		this.fieldEntityUID = fieldEntityUID;
		this.fieldEntityUsedUID = fieldEntityUsedUID;
	}

	public static DataSourceType getFromDatasourceVO(final DatasourceVO datasourceVO) {
		if (datasourceVO instanceof DynamicEntityVO) {
			return DataSourceType.DYNAMICENTITY;
		} else if (datasourceVO instanceof ValuelistProviderVO) {
			return DataSourceType.VALUELISTPROVIDER;
		} else if (datasourceVO instanceof RecordGrantVO) {
			return DataSourceType.RECORDGRANT;
		} else if (datasourceVO instanceof DynamicTasklistVO) {
			return DataSourceType.DYNAMICTASKLIST;
		} else if (datasourceVO instanceof ChartVO) {
			return DataSourceType.CHART;
		} else if (datasourceVO instanceof CalcAttributeVO) {
			return DataSourceType.CALCATTRIBUTE;
		} else {
			return DataSourceType.DATASOURCE;
		}
	}
}
