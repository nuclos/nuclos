//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report.ejb3;

import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.ISecurityCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.datasource.DatasourceMetaVO;
import org.nuclos.server.datasource.DatasourceMetaVO.ColumnMeta;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.ResultSetMetaDataTransformer;
import org.nuclos.server.report.SchemaCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * Datasource facade encapsulating datasource management. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor = { Exception.class })
public abstract class DatasourceFacadeBean implements DatasourceFacadeLocal {

	private static final Logger LOG = LoggerFactory.getLogger(DatasourceFacadeBean.class);

	protected final SpringDataBaseHelper dataBaseHelper;
	private final DatasourceServerUtils utils;
	protected final SessionUtils sessionUtils;
	protected final ISecurityCache securityCache;
	protected final DatasourceCache datasourceCache;
	protected final SchemaCache schemaCache;
	protected final MetaProvider metaProvider;

	// end of Spring injection

	public DatasourceFacadeBean(
			final SpringDataBaseHelper dataBaseHelper,
			final DatasourceServerUtils utils,
			final SessionUtils sessionUtils,
			final ISecurityCache securityCache, final DatasourceCache datasourceCache, final SchemaCache schemaCache, final MetaProvider metaProvider) {
		this.dataBaseHelper = dataBaseHelper;
		this.utils = utils;
		this.sessionUtils = sessionUtils;
		this.securityCache = securityCache;
		this.datasourceCache = datasourceCache;
		this.schemaCache = schemaCache;
		this.metaProvider = metaProvider;
	}


	/**
	 * get valuelist provider value object
	 *
	 * @param valuelistProviderUID
	 *            UID of valuelist provider
	 * @return valuelist provider value object
	 */
	@RolesAllowed("Login")
	public ValuelistProviderVO getValuelistProvider(final UID valuelistProviderUID) {
		ValuelistProviderVO vlpvo = utils.getValuelistProvider(valuelistProviderUID, datasourceCache);
		return vlpvo;
	}

	/**
	 * get dynamic entity value object
	 *
	 * @param dynamicEntityUID
	 *            UID of dynamic entity 
	 * @return dynamic entity value object
	 */
	@RolesAllowed("Login")
	public DynamicEntityVO getDynamicEntity(final UID dynamicEntityUID) {
		return datasourceCache.getDynamicEntity(dynamicEntityUID);
	}

	/**
	 * get chart value object
	 * 
	 * @param chartUID
	 *            UID of chart
	 * @return chart value object
	 */
	@RolesAllowed("Login")
	public ChartVO getChart(final UID chartUID) {
		return datasourceCache.getChart(chartUID);
	}

	/**
	 * get all charts
	 */
	@RolesAllowed("Login")
	public Collection<ChartVO> getCharts() {
		// this.checkReadAllowed(ENTITY_NAME_CHARTENTITY);
		return datasourceCache.getAllCharts();
	}

	@RolesAllowed("Login")
	public CalcAttributeVO getCalcAttribute(final UID calcAttributeUID) {
		return datasourceCache.getCalcAttribute(calcAttributeUID);
	}

	@RolesAllowed("Login")
	public Collection<CalcAttributeVO> getCalcAttributes() {
		// this.checkReadAllowed(ENTITY_NAME_CHARTENTITY);
		return datasourceCache.getAllCalcAttributes();
	}

	/**
	 * get a Datasource by UID regardless of permisssions
	 */
	@RolesAllowed("Login")
	public DatasourceVO getDatasource(final UID dataSourceUID) {
		return datasourceCache.get(dataSourceUID);
	}

	/**
	 * get a Datasource by name and nuclet regardless of permisssions
	 */
	@RolesAllowed("Login")
	public DatasourceVO getDatasource(final String datasourceName, UID nucletUid) {
		return datasourceCache.getAllDatasources().stream()
				.filter(datasourceVO -> datasourceVO.getName().equals(datasourceName) && datasourceVO.getNucletUID().equals(nucletUid))
				.findFirst().orElse(null);
	}

	/**
	 * Retrieve the parameters a datasource accepts.
	 *
	 * @param sDatasourceXML
	 * @return
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */
	@RolesAllowed("Login")
	public List<DatasourceParameterVO> getParametersFromXML(String sDatasourceXML) throws NuclosFatalException, NuclosDatasourceException {
		return utils.getParameters(sDatasourceXML);
	}

	/**
	 * Retrieve the parameters a datasource accepts.
	 *
	 * @param sDatasourceXML
	 * @return
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */
	@RolesAllowed("Login")
	public List<Map<String, String>> getParameterTestValuesFromXML(String sDatasourceXML) throws NuclosFatalException, NuclosDatasourceException {
		return utils.getTestSetsForParameters(sDatasourceXML);
	}

	/**
	 * Retrieve the parameters a datasource accepts.
	 *
	 * @param dataSourceUID
	 * @return
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */
	@Override
	@RolesAllowed("Login")
	public List<DatasourceParameterVO> getParameters(final UID dataSourceUID) throws NuclosFatalException, NuclosDatasourceException {
		return utils.getParameters(dataSourceUID, datasourceCache);
	}

	@RolesAllowed("Login")
	public List<DatasourceParameterVO> getCalcAttributeParameters(final UID calcAttributeUID) throws NuclosFatalException, NuclosDatasourceException {
		return utils.getCalcAttributeParameters(calcAttributeUID, datasourceCache);
	}

	/**
	 * validate the given DatasourceXML
	 *
	 * @param datasourcevo
	 */
	@RolesAllowed("Login")
	public void validateSqlFromXML(DatasourceVO datasourcevo) throws NuclosDatasourceException {
		List<String> testSetsToExecute = utils.getTestSetsToExecute(datasourcevo.getSource());
		if (testSetsToExecute.isEmpty()) {
			String sSql = this.createSQL(datasourcevo, this.getTestParameters(datasourcevo.getSource()));
			sSql = guardSqlForDynamicEntityAndChart(datasourcevo, sSql);
			this.validateSql(sSql, null);
		} else {
			for (String testSet : testSetsToExecute) {
				String sSql = this.createSQL(datasourcevo, this.getTestParametersForSet(datasourcevo.getSource(), testSet));
				sSql = guardSqlForDynamicEntityAndChart(datasourcevo, sSql);
				this.validateSql(sSql, null);
			}
		}
	}

	/**
	 * validate the given SQL
	 *
	 * @param sql
	 */
	public <T> T validateSql(String sql, final ResultSetMetaDataTransformer<T> transformer) throws NuclosDatasourceException {
		try {
			return dataBaseHelper.getDbAccess().checkSyntax(sql, transformer);
		}
		catch (DbException e) {
			throw new NuclosDatasourceException(StringUtils.getParameterizedExceptionMessage("datasource.error.invalid.statement", e.getMessage()), e);// "Die Abfrage ist ung\u00ef\u00bf\u00bdltig.\n"
		}
	}

	/**
	 * get sql string for datasource definition without parameter definition
	 *
	 * @param datasourcevo
	 *            xml of datasource
	 * @return string containing sql
	 */
	@RolesAllowed("Login")
	public String createSQL(final DatasourceVO datasourcevo) throws NuclosDatasourceException {
		return utils.createSQL(datasourcevo, datasourceCache);
	}

	@RolesAllowed("Login")
	public String createSQL(final DatasourceVO datasourcevo, final Map<String, Object> mpParams, Supplier<UID> languageSupplier, UID dataMandator, final Supplier<Set<UID>> accessibleMandatorsSupplier) throws NuclosDatasourceException {
		return utils.createSQL(datasourcevo, mpParams, null, datasourceCache, schemaCache, languageSupplier, dataMandator, accessibleMandatorsSupplier);
	}

	@RolesAllowed("Login")
	public String createSQL(final DatasourceVO datasourcevo, final Map<String, Object> mpParams) throws NuclosDatasourceException {
		return utils.createSQL(datasourcevo, mpParams, null, datasourceCache, schemaCache, () -> null, null, () -> null);
	}

	/**
	 * get sql string for report execution. check that this method is only
	 * called by the local interface as there is no authorization applied.
	 *
	 * @return string containing sql
	 */
	public String createSQLForReportExecution(final UID dataSourceUID, final Map<String, Object> mpParams, Supplier<UID> languageSupplier) throws NuclosDatasourceException {
		if (sessionUtils.isCalledRemotely()) {
			// just to ensure it won't be used in remote interface
			throw new NuclosFatalException("Invalid remote call");
		}

		return utils.createSQLForReportExecution(dataSourceUID, mpParams, datasourceCache, schemaCache, languageSupplier);
	}



	private String guardSqlForDynamicEntityAndChart(final DatasourceVO datasourcevo, String sSql) {
		if (datasourcevo instanceof DynamicEntityVO || datasourcevo instanceof ChartVO) {
			final String refCol;
			if (new DataSourceCaseSensivity(sSql).isIntidGenericObjectCaseInsensitive()) {
				refCol = DataSourceCaseSensivity.REF_ENTITY;
			} else {
				refCol = "\"" + DataSourceCaseSensivity.REF_ENTITY + "\"";
			}
			sSql = "SELECT * FROM ("+ sSql +") " + dataBaseHelper.getDbAccess().getTableAliasing() + " validateSqlFromXML WHERE validateSqlFromXML." + refCol + " = 123";
		}
		return sSql;
	}

	/**
	 * //TODO MULTINUCLET
	 *
	 * set test values for every parameter for sysntax check
	 *
	 * @param sDatasourceXML
	 * @return Map<String sName, String sValue>
	 */
	private Map<String, Object> getTestParameters(final String sDatasourceXML) {
		final Map<String, Object> result = new HashMap<String, Object>();

		final List<DatasourceParameterVO> lstParams;
		try {
			lstParams = this.getParametersFromXML(sDatasourceXML);
		}
		catch (NuclosDatasourceException e) {
			// No parameters defined?
			return result;
		}

		for (final DatasourceParameterVO paramvo : lstParams) {
			final String sValue = DatasourceUtils.getTestValueForParameter(paramvo, getBooleanRepresentationInSQL(Boolean.FALSE));
			result.put(paramvo.getParameter(), sValue);
			if (paramvo.getValueListProvider() != null) {
				result.put(paramvo.getParameter() + "Id", "123434");
			}
		}
		result.put("username", "test");

		return result;
	}

	private Map<String, Object> getTestParametersForSet(final String sDatasourceXML, String testSet) {
		final Map<String, Object> result = new HashMap<>();

		final List<DatasourceParameterVO> lstParams;
		try {
			lstParams = this.getParametersFromXML(sDatasourceXML);
		}
		catch (NuclosDatasourceException e) {
			// No parameters defined?
			return result;
		}

		for (final DatasourceParameterVO paramvo : lstParams) {
			try {
				String paramValue = utils.getTestValueForParameter(sDatasourceXML, lstParams.indexOf(paramvo), testSet);
				result.put(paramvo.getParameter(), paramValue);
				if (paramvo.getValueListProvider() != null) {
					result.put(paramvo.getParameter() + "Id", paramValue);
				}
			} catch (NuclosDatasourceException ignored) {}
		}
		result.put("username", "test");

		return result;
	}

	/**
	 * invalidate datasource cache
	 */
	@RolesAllowed("Login")
	public void invalidateCache() {
		datasourceCache.invalidate(true);
	}

	/**
	 * get all DynamicEntities
	 */
	@Override
	@RolesAllowed("Login")
	public Collection<DynamicEntityVO> getDynamicEntities() {
		// this.checkReadAllowed(ENTITY_NAME_DYNAMICENTITY);
		return datasourceCache.getAllDynamicEntities();
	}


	/**
	 * get all ValuelistProvider
	 *
	 * @return set of ValuelistProviderVO
	 */
	@RolesAllowed("Login")
	public Collection<ValuelistProviderVO> getValuelistProvider() {
		return datasourceCache.getAllValuelistProvider(true);
	}


	/**
	 * get all RecordGrant
	 *
	 * @return set of RecordGrantVO
	 */
	@RolesAllowed("Login")
	public Collection<RecordGrantVO> getRecordGrant() {
		return datasourceCache.getAllRecordGrant();
	}

	/**
	 * get RecordGrant value object
	 *
	 * @param recordGrantUID
	 *            primary key of RecordGrant
	 * @return RecordGrantVO
	 */
	@RolesAllowed("Login")
	public RecordGrantVO getRecordGrant(final UID recordGrantUID) {
		return datasourceCache.getRecordGrant(recordGrantUID);
	}

	protected ResultVO getDataFromRule(DatasourceVO datasource, Map<String, Object> mpParams, Integer iMaxRowCount, UID languageUID, UID mandatorUID) throws NuclosDatasourceException {
		throw new NuclosDatasourceException("Rulebased datasources are not supported.");
	}
	
	protected ResultVO executePlainQueryAsResultVO(String sql, Integer iMaxRowCount) {
		return utils.executePlainQueryAsResultVO(sql, iMaxRowCount);
	}

	@Override
	public String createSQLOriginalParameter(DatasourceVO datasourcevo) throws NuclosDatasourceException {
		return utils.createSQLOriginalParameter(datasourcevo, datasourceCache);
	}

	@RolesAllowed("Login")
	public DynamicTasklistVO getDynamicTasklist(final UID dynamicTaskListUID) {
		return datasourceCache.getDynamicTasklist(dynamicTaskListUID);
	}

	protected void invalidateCaches(final DataSourceType type) {
		datasourceCache.invalidate(true);
		schemaCache.invalidate(true);
		switch (type) {
		case DATASOURCE:
			securityCache.invalidate(true);
			break;
		case CHART:
		case DYNAMICENTITY:
			// already done by MasterDataFacadeHelper.invalidateCaches(UID, MasterDataVO<?>)
			// MetaProvider.getInstance().revalidate(true, false);
			break;
		}
	}

	public List<String> getColumnsFromVLP(UID valuelistProviderUid, Class clazz) throws CommonBusinessException {
		ValuelistProviderVO vlpVO = getValuelistProvider(valuelistProviderUid);
		if (vlpVO == null) {
			throw new NuclosDatasourceException("Could Not Find VLP with UID=" + valuelistProviderUid);
		}

		return getColumns(createSQL(vlpVO, getTestParameters(vlpVO.getSource())), clazz);
	}

	public List<String> getColumns(String sql) {
		return getColumns(sql, null);
	}
	
	public List<String> getColumns(String sql, Class clazz) {
		List<String> result = new ArrayList<String>();
		try {
			if (StringUtils.isNullOrEmpty(sql))
				return result;
			
			ResultVO resultVO = executePlainQueryAsResultVO(sql, 0);
			for (ResultColumnVO column : resultVO.getColumns()) {
				if (clazz == null || clazz.isAssignableFrom(column.getColumnClass()))
				result.add(column.getColumnLabel());
			}
			
			// fallback 
			// beware. we cannot respect the column clazz here.
			if (result.isEmpty() && clazz == null) {
				result.addAll(DatasourceUtils.getColumns(sql));
			}
			return result;
		} catch (Exception e) {
			// fallback
			// beware. we cannot respect the column clazz here.
			if (result.isEmpty() && clazz == null) {
				result.addAll(DatasourceUtils.getColumns(sql));
			}
		}
		return result;
	}

	public static class DatasourceMetaTransformer implements ResultSetMetaDataTransformer<DatasourceMetaVO> {
		
		private final DatasourceVO dsvo;
		
		private final DatasourceMetaVO current;
		
		private final EntityMeta<UID> datasourceMeta;

		public DatasourceMetaTransformer(DatasourceVO dsvo, DatasourceMetaVO current, EntityMeta<UID> datasourceMeta) {
			super();
			this.dsvo = dsvo;
			this.current = current;
			this.datasourceMeta = datasourceMeta;
		}

		public static boolean checkEntityUID(UID entityUID, EntityMeta<UID> datasourceMeta) {
			final String sTest = entityUID.getString();
			return sTest.startsWith(getEntityPrefix(datasourceMeta));
		}

		private static String getEntityPrefix(EntityMeta<UID> datasourceMeta) {
			return datasourceMeta.getUID().getString() + "-";
		}

		private static String getEntityFieldPrefix(EntityMeta<UID> datasourceMeta) {
			return datasourceMeta.getUID().getString() + "f-";
		}

		@Override
		public DatasourceMetaVO transform(ResultSetMetaData rsmeta) {
			if (rsmeta == null) {
				return null;
			}
			
			DatasourceMetaVO dsmeta = new DatasourceMetaVO();
			
			try {
				dsmeta.setEntityUID(new UID(getEntityPrefix(datasourceMeta)+dsvo.getId().getString()));
				
				List<ColumnMeta> columns = new ArrayList<>();
				for (int i = 1; i <= rsmeta.getColumnCount(); i++) {
					String columnname = rsmeta.getColumnLabel(i);
					
					boolean forceTimestampFormat = false;
					final String tsHint = "_as_timestamp";
					if (columnname.toLowerCase().endsWith(tsHint)) {
						forceTimestampFormat = true;
					}
					
					UID fieldUID = null;
					if (current != null && current.getColumns() != null) {
						for (ColumnMeta existingcol : current.getColumns()) {
							if (columnname.equals(existingcol.getColumnName())) {
								fieldUID = existingcol.getFieldUID();
							}
						}
					}
					if (fieldUID == null) {
						fieldUID = new UID(getEntityFieldPrefix(datasourceMeta)+new UID().getString());
					}
					
					Integer scale = rsmeta.getScale(i);
					Integer precision = rsmeta.getPrecision(i);
					String classname = rsmeta.getColumnClassName(i);
					Class<?> cls = Class.forName(classname);
					if (Number.class.isAssignableFrom(cls)) {
						if (RigidUtils.defaultIfNull(scale, new Integer(0)).intValue() > 0) {
							classname = Double.class.getName();
						} else {
							if (columnname.toUpperCase().startsWith("BLN") && RigidUtils.defaultIfNull(precision, new Integer(0)).intValue() == 1) {
								classname = Boolean.class.getName();
								scale = null;
							} else {
								classname = Integer.class.getName();
							}
						}
						if (precision == null) {
							precision = 9;
						}
					} else if (cls == java.sql.Date.class || Date.class.isAssignableFrom(Class.forName(classname))) {
						cls = Date.class;
						classname = cls.getName();
					}
					if (forceTimestampFormat && Date.class.isAssignableFrom(Class.forName(classname))) {
						classname = InternalTimestamp.class.getName();
					}
					Boolean readonly = rsmeta.isReadOnly(i);
					
					ColumnMeta colmeta = new ColumnMeta();
					colmeta.setFieldUID(fieldUID);
					colmeta.setColumnName(columnname);
					colmeta.setJavaType(classname);
					colmeta.setReadOnly(readonly);
					// switch scale <-> precision for nuclos
					colmeta.setPrecision(scale);
					colmeta.setScale(precision);
					columns.add(colmeta);
				}
				
				dsmeta.setColumns(columns);
			} catch (Exception ex) {
				LOG.error("Unable to generate meta data for datasource {} uid={}: {}",
				          dsvo.getName(), dsvo.getId(), ex.getMessage(), ex);
			}
			
			return dsmeta;
		}
	}

	@Override
	public String getBaseTable(UID baseEntityUID) {
		return metaProvider.getEntity(baseEntityUID).getDbSelect();
	}

	public Object getBooleanRepresentationInSQL(Boolean b) {
		return dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL(b);
	}



	public SpringDataBaseHelper getDataBaseHelper() {
		return dataBaseHelper;
	}

	public SessionUtils getSessionUtils() {
		return sessionUtils;
	}
}
