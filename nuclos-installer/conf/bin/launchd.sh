#!/bin/bash
#
# nuclos.${server.name}
# description: Startup / Shutdown Nuclos instance ${server.name}

# Attention: This could not be converted to sh as the line
# . "$CATALINA_HOME/bin/catalina.sh" start
# works in bash, but in the sh the parameter 'start' is ignored
# Thomas Pasch

shutdown() {
	date
	echo "Shutdown nuclos.${server.name}"
	"$CATALINA_HOME/bin/catalina.sh" stop
}

date
CATALINA_HOME="${server.tomcat.dir}"
export CATALINA_HOME
CATALINA_PID="/tmp/$$"
export CATALINA_PID
JRE_HOME="${server.java.home}"
export JRE_HOME
JAVA_OPTS="-Xmx${server.heap.size}m ${server.environment.jvm}"
export JAVA_OPTS

echo "Startup nuclos.${server.name}"

. "$CATALINA_HOME/bin/catalina.sh" start

# Allow any signal which would kill a process to stop nuclos
trap shutdown HUP INT QUIT ABRT KILL ALRM TERM TSTP

echo "Waiting for 'cat $CATALINA_PID'"
wait `cat $CATALINA_PID`

