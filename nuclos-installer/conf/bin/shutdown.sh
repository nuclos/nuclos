#!/bin/sh
# Nuclos Server Script

CATALINA_HOME="${server.tomcat.dir}"
export CATALINA_HOME
JRE_HOME="${server.java.home}"
export JRE_HOME

exec "$CATALINA_HOME/bin/shutdown.sh" "$@"

