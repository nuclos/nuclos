@echo off
title ${server.name}
rem Nuclos Server Script
if "%OS%" == "Windows_NT" setlocal
set CATALINA_HOME=${server.tomcat.dir}
set CATALINA_OPTS=-Djava.awt.headless=true -Dlog4j2.formatMsgNoLookups=true -Dorg.apache.catalina.connector.RECYCLE_FACADES=false
set JRE_HOME=${server.java.home}
set JAVA_OPTS=-Xmx${server.heap.size}m ${server.environment.jvm}
call "%CATALINA_HOME%\bin\catalina.bat" run

