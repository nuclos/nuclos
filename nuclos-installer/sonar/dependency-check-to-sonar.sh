#!/bin/bash
# dependency-check-to-sonar.sh
# include with:
#   mvn sonar:sonar -Dsonar.externalIssuesReportPaths="$(chmod +x nuclos-installer/sonar/*.sh && nuclos-installer/sonar/dependency-check-to-sonar.sh)"

if ! command -v jq >/dev/null 2>&1; then
  echo "error: jq is not installed." >&2
  exit 1
fi

BASEDIR=$(dirname $0)
INPUT_FILE="${1:-../../target/dependency-check-report.json}"
PARENT_POM="${2:-../../pom.xml}"
SOURCE_FILE="${3:-sonar/dependency-check-to-sonar.sh}"

cd "$BASEDIR"

if [ -f "$PARENT_POM" ]; then
  IGNORE_THRESHOLD=$(sed -n 's:.*<nuclos.dependency.check.ignoreThreshold>\(.*\)</nuclos.dependency.check.ignoreThreshold>.*:\1:p' "$PARENT_POM")
fi
if [ -z "$IGNORE_THRESHOLD" ]; then
  IGNORE_THRESHOLD=0
fi

if [ -f "$PARENT_POM" ]; then
  SEVERITY_BLOCKER=$(sed -n 's:.*<sonar.dependencyCheck.severity.blocker>\(.*\)</sonar.dependencyCheck.severity.blocker>.*:\1:p' "$PARENT_POM")
  SEVERITY_HIGH=$(sed -n 's:.*<sonar.dependencyCheck.severity.high>\(.*\)</sonar.dependencyCheck.severity.high>.*:\1:p' "$PARENT_POM")
  SEVERITY_MEDIUM=$(sed -n 's:.*<sonar.dependencyCheck.severity.medium>\(.*\)</sonar.dependencyCheck.severity.medium>.*:\1:p' "$PARENT_POM")
  SEVERITY_LOW=$(sed -n 's:.*<sonar.dependencyCheck.severity.low>\(.*\)</sonar.dependencyCheck.severity.low>.*:\1:p' "$PARENT_POM")
fi
if [ -z "$SEVERITY_BLOCKER" ]; then
  SEVERITY_BLOCKER=8.0
fi
if [ -z "$SEVERITY_HIGH" ]; then
  SEVERITY_HIGH=7.0
fi
if [ -z "$SEVERITY_MEDIUM" ]; then
  SEVERITY_MEDIUM=4.0
fi
if [ -z "$SEVERITY_LOW" ]; then
  SEVERITY_LOW=0.0
fi

mkdir -p "../target"
OUTPUT_FILE="../target/sonar-generic-issue-data.json"

jq --arg parentPom "$PARENT_POM" \
   --arg sourceFile "$SOURCE_FILE" \
   --argjson ignoreThreshold "$IGNORE_THRESHOLD" \
   --argjson severityBlocker "$SEVERITY_BLOCKER" \
   --argjson severityHigh "$SEVERITY_HIGH" \
   --argjson severityMedium "$SEVERITY_MEDIUM" \
   --argjson severityLow "$SEVERITY_LOW" '
  def mapBaseScoreSeverity:
    if (.cvssv3.baseScore // 0) >= $severityBlocker then "BLOCKER"
    elif (.cvssv3.baseScore // 0) >= $severityHigh then "HIGH"
    elif (.cvssv3.baseScore // 0) >= $severityMedium then "MEDIUM"
    elif (.cvssv3.baseScore // 0) >= $severityLow then "LOW"
    else "INFO" end;

  {
    rules: (
      [ .dependencies[]
        | select(.vulnerabilities != null)
        | .vulnerabilities[]
        | select((.cvssv3.baseScore // 0) >= $ignoreThreshold)
        | { severity: ( . | mapBaseScoreSeverity ) }
      ]
      | group_by(.severity)
      | map({
          id: .[0].severity,
          name: .[0].severity,
          description: ("Issues with severity " + .[0].severity),
          engineId: "dependency-check",
          cleanCodeAttribute: "COMPLETE",
          impacts: [
            { softwareQuality: "SECURITY", severity: .[0].severity }
          ]
        })
    ),
    issues: [
      .dependencies[]
      | select(.vulnerabilities != null)
      | . as $dep
      | .vulnerabilities[]
      | select((.cvssv3.baseScore // 0) >= $ignoreThreshold)
      | {
          engineId: "dependency-check",
          ruleId: ( . | mapBaseScoreSeverity ),
          primaryLocation: {
            message: ((.name) + " - File: " + (($dep.fileName // $dep.filePath) // "unknown") + " - " + (.description // "No description") + " (Base Score: " + ((.cvssv3.baseScore | tostring) // "0") + ")"),
            filePath: $sourceFile,
            textRange: { startLine: 1 }
          }
        }
    ]
  }
' "$INPUT_FILE" > "$OUTPUT_FILE"

if command -v realpath >/dev/null 2>&1; then
  ABS_PATH=$(realpath "$OUTPUT_FILE")
elif command -v readlink >/dev/null 2>&1; then
  ABS_PATH=$(readlink -f "$OUTPUT_FILE")
else
  ABS_PATH="$OUTPUT_FILE"
fi

echo "$ABS_PATH"
exit 0
