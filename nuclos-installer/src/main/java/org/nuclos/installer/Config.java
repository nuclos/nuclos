//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.installer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.nuclos.installer.database.DbType;
import org.nuclos.installer.database.PostgresService;
import org.nuclos.installer.unpack.Unpacker;
import org.nuclos.installer.util.ConfigFile;
import org.nuclos.installer.util.FileUtils;
import org.nuclos.installer.util.XmlUtils;
import org.xml.sax.SAXException;

public class Config extends Properties implements Constants {

    @Override
    public synchronized void loadFromXML(InputStream in) throws IOException {
        this.clear();
        ConfigFile config = new ConfigFile();
        try {
            config.loadXml(XmlUtils.readDocument(in));
            this.putAll(config.getProperties());
            if (containsKey(DATABASE_ADAPTER)) {
                DbType type = DbType.findType(getProperty("database.adapter"));
                if (type != null) {
                    type.parseJdbcConnectionString(config.getProperties().get("database.connection.url"), this);
                }
            }
            migrate();
           
        }
        catch(SAXException e) {
            throw new InvalidPropertiesFormatException(e);
        }
    }

    public File getFileProperty(String property) {
        return new File(getProperty(property));
    }

    public void setDefaults(Unpacker unpacker) {
        ensurePropertyDefault(this, NUCLOS_HOME, unpacker.getDefaultValue(NUCLOS_HOME));
        ensurePropertyDefault(this, JAVA_HOME, unpacker.getDefaultValue(JAVA_HOME));
        ensurePropertyDefault(this, NUCLOS_INSTANCE, unpacker.getDefaultValue(NUCLOS_INSTANCE));
        ensurePropertyDefault(this, DOCUMENT_PATH, unpacker.getDefaultValue(DOCUMENT_PATH));
        ensurePropertyDefault(this, INDEX_PATH, unpacker.getDefaultValue(INDEX_PATH));
        ensurePropertyDefault(this, WEBCLIENT_INSTANCE, unpacker.getDefaultValue(WEBCLIENT_INSTANCE));
        ensurePropertyDefault(this, CONNECTION_THREAD_POOL_SIZE, unpacker.getDefaultValue(CONNECTION_THREAD_POOL_SIZE));
        ensurePropertyDefault(this, HTTP_ENABLED, unpacker.getDefaultValue(HTTP_ENABLED));
        ensurePropertyDefault(this, HTTP_PORT, unpacker.getDefaultValue(HTTP_PORT));
        ensurePropertyDefault(this, AJP_ENABLED, unpacker.getDefaultValue(AJP_ENABLED));
        ensurePropertyDefault(this, AJP_PORT, unpacker.getDefaultValue(AJP_PORT));
        ensurePropertyDefault(this, HTTPS_ENABLED, unpacker.getDefaultValue(HTTPS_ENABLED));
        ensurePropertyDefault(this, HTTPS_PORT, unpacker.getDefaultValue(HTTPS_PORT));
        ensurePropertyDefault(this, SHUTDOWN_PORT, unpacker.getDefaultValue(SHUTDOWN_PORT));
        ensurePropertyDefault(this, HEAP_SIZE, unpacker.getDefaultValue(HEAP_SIZE));
        ensurePropertyDefault(this, CLIENT_SINGLEINSTANCE, unpacker.getDefaultValue(CLIENT_SINGLEINSTANCE));
        ensurePropertyDefault(this, CLUSTER_MODE, unpacker.getDefaultValue(CLUSTER_MODE));
        ensurePropertyDefault(this, CLUSTER_NODE_TYPE, unpacker.getDefaultValue(CLUSTER_NODE_TYPE));
        ensurePropertyDefault(this, CLUSTER_NODE_ID, unpacker.getDefaultValue(CLUSTER_NODE_ID));
        ensurePropertyDefault(this, CLUSTER_NODE_PROTOCOL, unpacker.getDefaultValue(CLUSTER_NODE_PROTOCOL));
        ensurePropertyDefault(this, CLUSTER_NODE_HOSTNAME, unpacker.getDefaultValue(CLUSTER_NODE_HOSTNAME));
        ensurePropertyDefault(this, CLUSTER_NODE_PORT, unpacker.getDefaultValue(CLUSTER_NODE_PORT));
        ensurePropertyDefault(this, CLUSTER_NODE_CONTEXT, unpacker.getDefaultValue(CLUSTER_NODE_CONTEXT));
        ensurePropertyDefault(this, CLUSTER_BALANCER_PROTOCOL, unpacker.getDefaultValue(CLUSTER_BALANCER_PROTOCOL));
        ensurePropertyDefault(this, CLUSTER_BALANCER_HOSTNAME, unpacker.getDefaultValue(CLUSTER_BALANCER_HOSTNAME));
        ensurePropertyDefault(this, CLUSTER_BALANCER_PORT, unpacker.getDefaultValue(CLUSTER_BALANCER_PORT));
        ensurePropertyDefault(this, CLUSTER_BALANCER_CONTEXT, unpacker.getDefaultValue(CLUSTER_BALANCER_CONTEXT));
        ensurePropertyDefault(this, CLIENT_RICHCLIENT, unpacker.getDefaultValue(CLIENT_RICHCLIENT));
        ensurePropertyDefault(this, CLIENT_WEBCLIENT, unpacker.getDefaultValue(CLIENT_WEBCLIENT));
        ensurePropertyDefault(this, CLIENT_LAUNCHER, unpacker.getDefaultValue(CLIENT_LAUNCHER));
        ensurePropertyDefault(this, CLIENT_SERVERHOST, unpacker.getDefaultValue(CLIENT_SERVERHOST));
        ensurePropertyDefault(this, CLIENT_JRE, unpacker.getDefaultValue(CLIENT_JRE));
        ensurePropertyDefault(this, FORCE_FILE_ENCODING_UTF8, unpacker.getDefaultValue(FORCE_FILE_ENCODING_UTF8));
        ensurePropertyDefault(this, PRODUCTION_ENABLED, unpacker.getDefaultValue(PRODUCTION_ENABLED));
		ensurePropertyDefault(this, ADDITIONAL_JVM_ARGUMENTS, unpacker.getDefaultValue(ADDITIONAL_JVM_ARGUMENTS));
		ensurePropertyDefault(this, ANTI_CLICKJACKING_ENABLED, unpacker.getDefaultValue(ANTI_CLICKJACKING_ENABLED));
		ensurePropertyDefault(this, ANTI_CLICKJACKING_OPTIONS, unpacker.getDefaultValue(ANTI_CLICKJACKING_OPTIONS));
		ensurePropertyDefault(this, ADDITIONAL_HTTP_HEADERS, unpacker.getDefaultValue(ADDITIONAL_HTTP_HEADERS));
    }

    public void setDbDefaults(Unpacker unpacker, String dboption) {
        ensurePropertyDefault(this, DATABASE_ADAPTER, unpacker.getDefaultValue(DATABASE_ADAPTER));
        ensurePropertyDefault(this, DATABASE_SERVER, unpacker.getDefaultValue(DATABASE_SERVER));
        ensurePropertyDefault(this, DATABASE_NAME, unpacker.getDefaultValue(DATABASE_NAME));
        ensurePropertyDefault(this, DATABASE_USERNAME, unpacker.getDefaultValue(DATABASE_USERNAME));
        ensurePropertyDefault(this, DATABASE_PASSWORD, unpacker.getDefaultValue(DATABASE_PASSWORD));
        ensurePropertyDefault(this, DATABASE_SCHEMA, unpacker.getDefaultValue(DATABASE_SCHEMA));
        ensurePropertyDefault(this, DATABASE_MSSQL_ISOLATION, unpacker.getDefaultValue(DATABASE_MSSQL_ISOLATION));
        ensurePropertyDefault(this, DATABASE_CONNECTION_POOL_SIZE, unpacker.getDefaultValue(DATABASE_CONNECTION_POOL_SIZE));
        ensurePropertyDefault(this, DATABASE_CONNECTION_POOL_WAIT_SECONDS, unpacker.getDefaultValue(DATABASE_CONNECTION_POOL_WAIT_SECONDS));
        if (!DBOPTION_EMBEDDED.equals(dboption)) {
            ensurePropertyDefault(this, DATABASE_PORT, unpacker.getDefaultValue(DATABASE_PORT));
        }

        if (DBOPTION_INSTALL.equals(dboption)) {
            ensurePropertyDefault(this, POSTGRES_PREFIX, unpacker.getDefaultValue(POSTGRES_PREFIX));
            ensurePropertyDefault(this, POSTGRES_DATADIR, unpacker.getDefaultValue(POSTGRES_DATADIR));
            ensurePropertyDefault(this, POSTGRES_TABLESPACEPATH, unpacker.getDefaultValue(POSTGRES_TABLESPACEPATH));
            ensurePropertyDefault(this, POSTGRES_SUPERUSER, unpacker.getDefaultValue(POSTGRES_SUPERUSER));

        }
        else {
            List<PostgresService> pgservices = unpacker.getPostgresServices();
            if (pgservices != null && pgservices.size() > 0) {
                PostgresService latest = pgservices.get(pgservices.size() - 1);
                ensurePropertyDefault(this, DATABASE_PORT, String.valueOf(latest.port));
                if (DBOPTION_SETUP.equals(dboption)) {
                    ensurePropertyDefault(this, POSTGRES_TABLESPACEPATH, latest.dataDirectory);
                    ensurePropertyDefault(this, POSTGRES_SUPERUSER, latest.superUser);
                }
            }
        }
    }

    private void migrate() {
        if (containsKey("nuclos.home") && !containsKey("server.home")) {
            put("server.home", get("nuclos.home"));
        }
        if (containsKey("java.home") && !containsKey("server.java.home")) {
            put("server.java.home", get("java.home"));
        }
        if (containsKey("tomcat.server.name") && !containsKey("server.name")) {
            put("server.name", get("tomcat.server.name"));
        }
        if (containsKey("tomcat.web.http.port") && !containsKey("server.http.port")) {
            put("server.http.port", get("tomcat.web.http.port"));
        }
        if (containsKey("webstart.singleinstance") && !containsKey("client.singleinstance")) {
            put("client.singleinstance", get("webstart.singleinstance"));
        }
        ensurePropertyDefault(this, Constants.CLUSTER_MODE, "false");
        ensurePropertyDefault(this, Constants.CLIENT_SERVERHOST, "localhost");
        ensurePropertyDefault(this, Constants.CLIENT_JRE, "");
        ensurePropertyDefault(this, Constants.PRODUCTION_ENABLED, "true");
        ensurePropertyDefault(this, Constants.DEVELOPMENT_ENABLED, "false");
        ensurePropertyDefault(this, Constants.DEBUG_PORT, "8000");
        ensurePropertyDefault(this, Constants.JMX_PORT, "30333");
        ensurePropertyDefault(this, Constants.FORCE_FILE_ENCODING_UTF8, "false");
    }

    /**
     * Verifies all parameters and ensures that the set is consistent.
     */
    public void verify() throws InstallException {
        // App-id (may differ for nuclets)
        ensurePropertyDefault(this, "server.name", "nuclos");

        // Database defaults
        DbType type = DbType.findType(getProperty("database.adapter"));
        if (type == null) {
            throw new InstallException("error.illegal.dbadapter", getProperty("database.adapter"));
        }
        ensurePropertyDefault(this, Constants.DATABASE_DRIVER_CLASS, type.getDriverClassName());
        ensurePropertyDefault(this, Constants.DATABASE_DRIVERJAR, "");
        ensurePropertyDefault(this, Constants.DATABASE_CONNECTION_URL, type.buildJdbcConnectionString(this));
        ensurePropertyDefault(this, Constants.DATABASE_TABLESPACE, "");
        ensurePropertyDefault(this, Constants.DATABASE_TABLESPACEINDEX, "");
        ensurePropertyDefault(this, Constants.DATABASE_CONNECTION_INIT, "");
        ensurePropertyDefault(this, Constants.CLIENT_SINGLEINSTANCE, "false");
        ensurePropertyDefault(this, Constants.CLIENT_RICHCLIENT, "true");
        ensurePropertyDefault(this, Constants.CLIENT_WEBCLIENT, "true");
        ensurePropertyDefault(this, Constants.CLIENT_SERVERHOST, "localhost");
        ensurePropertyDefault(this, Constants.CLIENT_JRE, "");
        
        // Quartz
        // - Start with the "defaults" (note that the empty values are interpreted as their defaults by Quartz)
        /*
        this.setProperty("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");
        this.setProperty("org.quartz.jobStore.selectWithLockSQL", "");
        this.setProperty("org.quartz.jobStore.txIsolationLevelSerializable", "");
        if (type == DbType.POSTGRESQL) {
            this.setProperty("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.PostgreSQLDelegate");
        } else  if (type == DbType.MSSQL || type == DbType.SYBASE) {
            this.setProperty("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.MSSQLDelegate");
            this.setProperty("org.quartz.jobStore.selectWithLockSQL", "SELECT * FROM {0}LOCKS UPDLOCK WHERE LOCK_NAME = ?");
            this.setProperty("org.quartz.jobStore.txIsolationLevelSerializable", "true");
        }
         */

        // Heap size
        ensurePropertyDefault(this, "server.heap.size", "1024");
        ensurePropertyDefault(this, "server.launch.on.startup", "false");

        // Protocol settings
        ensurePropertyDefault(this, "server.http.enabled", "true");
        ensurePropertyDefault(this, "server.http.port", "80");
        ensurePropertyDefault(this, "server.ajp.enabled", "true");
        ensurePropertyDefault(this, "server.ajp.port", "8009");
        ensurePropertyDefault(this, "server.https.enabled", "false");
        ensurePropertyDefault(this, "server.https.port", "443");
        ensurePropertyDefault(this, "server.https.keystore.file", "");
        ensurePropertyDefault(this, "server.https.keystore.password", "");
        
        // Environment
        ensurePropertyDefault(this, Constants.PRODUCTION_ENABLED, "true");
        ensurePropertyDefault(this, Constants.DEVELOPMENT_ENABLED, "false");
        ensurePropertyDefault(this, Constants.DEBUG_PORT, "8000");
        ensurePropertyDefault(this, Constants.JMX_PORT, "30333");

		ensurePropertyDefault(this, Constants.ADDITIONAL_JVM_ARGUMENTS, "");

        put("server.environment.jvm", generateEnvironmentJvm());

		ensureValidClusterParameters();

        File nuclosHome = new File(getProperty(NUCLOS_HOME)).getAbsoluteFile();
        File nuclosDocPath = new File(getProperty(DOCUMENT_PATH)).getAbsoluteFile();
        File nuclosIndexPath = "off".equals(getProperty(INDEX_PATH)) ? null : new File(getProperty(INDEX_PATH)).getAbsoluteFile();
        File nuclosWeb = new File(nuclosHome, DIR_NAME_WEBAPP);
        File nuclosClient = new File(nuclosHome, DIR_NAME_CLIENT);
        File nuclosConf = new File(nuclosHome, DIR_NAME_CONF);
        File nuclosData = new File(nuclosHome, DIR_NAME_DATA);
        File nuclosTomcat = new File(nuclosHome, DIR_NAME_TOMCAT);
        File nuclosCatalina = new File(nuclosTomcat, TOMCAT_VERSION);

        // Make some path properties absolute
        put(SERVER_HOME, nuclosHome.getAbsolutePath());
        put(SERVER_WEBAPP_DIR, nuclosWeb.getAbsolutePath());
        put(SERVER_CLIENT_DIR, nuclosClient.getAbsolutePath());
        put(SERVER_CONF_DIR, nuclosConf.getAbsolutePath());
        put(SERVER_DATA_DIR, nuclosData.getAbsolutePath());
        put(SERVER_TOMCAT_DIR, nuclosCatalina.getAbsolutePath());
        put(SERVER_DOCUMENT_PATH, nuclosDocPath.getAbsolutePath());
        put(SERVER_INDEX_PATH, nuclosIndexPath==null ? "off" : nuclosIndexPath.getAbsolutePath());
    }

	public String generateEnvironmentJvm() {
		final StringBuilder sEnvironmentJvm = new StringBuilder(
				" -XX:+IgnoreUnrecognizedVMOptions"
						+ " -XX:MaxPermSize=256m -XX:PermSize=128m"
						+ " -XX:+HeapDumpOnOutOfMemoryError"
						+ " -XX:+UseThreadPriorities"
						+ " -XX:+CMSClassUnloadingEnabled"
						+ " -server -Djava.awt.headless=true"
						+ " -Djdk.attach.allowAttachSelf=true"
						+ " --add-opens=java.base/java.io=ALL-UNNAMED"
						+ " --add-opens=java.base/java.lang=ALL-UNNAMED"
						+ " --add-opens=java.base/java.lang.reflect=ALL-UNNAMED"
						+ " --add-opens=java.base/java.net=ALL-UNNAMED"
						+ " --add-opens=java.desktop/java.awt=ALL-UNNAMED"
                        + " --add-opens=java.desktop/javax.swing.tree=ALL-UNNAMED"
						+ " --add-opens=java.rmi/sun.rmi.transport=ALL-UNNAMED"
						+ " --add-opens=java.naming/com.sun.jndi.ldap=ALL-UNNAMED");
		if ("true".equals(get(Constants.FORCE_FILE_ENCODING_UTF8))) {
			sEnvironmentJvm.append(" -Dfile.encoding=" + FileUtils.UTF8NAME);
		}
		if ("true".equals(get("server.development.enabled"))) {
			sEnvironmentJvm.append(" " + getDebugAgentJvmParameter());
			sEnvironmentJvm.append(" -Dcom.sun.management.jmxremote.port=" + get(Constants.JMX_PORT));
			sEnvironmentJvm.append(" -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false");
		}
		if (StringUtils.isNotEmpty((String) get(Constants.ADDITIONAL_JVM_ARGUMENTS))) {
			sEnvironmentJvm.append(" " + get(Constants.ADDITIONAL_JVM_ARGUMENTS));
		}
		return sEnvironmentJvm.toString();
	}

    public void setDerivedProperties() {
    	ensurePropertyDefault(this, "database.adapter", "postgresql");
        DbType type = DbType.findType(getProperty("database.adapter"));
        this.setProperty("database.driver", type.getDriverClassName());
        this.setProperty("database.connection.url", type.buildJdbcConnectionString(this));
    }

	private void ensureValidClusterParameters() {
    	// set cluster node configuration parameters for registration during startup
    	final Boolean https = "true".equals(this.getProperty(HTTPS_ENABLED));
		this.setProperty(CLUSTER_NODE_PROTOCOL, https ? "https" : "http");
		this.setProperty(CLUSTER_NODE_PORT, https ? this.getProperty(HTTPS_PORT) : this.getProperty(HTTP_PORT));
		String nodeId = this.getProperty(CLUSTER_NODE_ID);
		if (StringUtils.isBlank(nodeId)) {
			this.setProperty(CLUSTER_NODE_ID, org.nuclos.installer.util.StringUtils.generateNuclosUidString());
		}
		if ("true".equals(this.getProperty(CLUSTER_MODE))) {
			final Boolean master = "master".equals(this.getProperty(CLUSTER_NODE_TYPE));
			this.setProperty(DATABASE_AUTOSETUP, master.toString());
			// Disable lucene indexer:
			// The synchronization between the nodes is not easy. We need a high-performance solution.
			this.setProperty(INDEX_PATH, "off");
		} else {
			this.setProperty(DATABASE_AUTOSETUP, "true");
		}
	}

	public String getDebugAgentJvmParameter() {
		Object javaHome = get(Constants.JAVA_HOME);
		Object debugPort = get(Constants.DEBUG_PORT);
		if (javaHome != null) {
			try {
				String javaVersion = CheckJavaVersion.getJavaVersion(new File(javaHome.toString()));
				if (CheckJavaVersion.getVersionValue(javaVersion) < CheckJavaVersion.getVersionValue("9")) {
					// JDK 8 support
					return "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=" + debugPort;
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:" + debugPort;
	}

    private static void ensurePropertyDefault(Properties props, String key, String def) {
        if (isPropertyUnset(props, key) && def != null) {
            props.setProperty(key, def);
        }
    }

    public static boolean isPropertyUnset(Properties props, String key) {
        return isUnset(props.getProperty(key));
    }

    private static boolean isUnset(String s) {
        return s == null || s.trim().isEmpty();
    }
}
