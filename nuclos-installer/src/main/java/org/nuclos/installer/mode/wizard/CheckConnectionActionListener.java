package org.nuclos.installer.mode.wizard;

import org.pietschy.wizard.InvalidStateException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class CheckConnectionActionListener implements ActionListener {

    private AbstractWizardStep abstractWizardStep;

    CheckConnectionActionListener(AbstractWizardStep abstractWizardStep){
        this.abstractWizardStep = abstractWizardStep;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            abstractWizardStep.applyState();
        } catch (InvalidStateException e2) {
            abstractWizardStep.getModel().getCallback().info(e2.getMessage());
            return;
        }
        abstractWizardStep.getModel().getCallback().info("validation.valid.db.connection");
    }
}
