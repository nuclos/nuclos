package org.nuclos.installer.tomcat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.installer.Constants;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class IndexJspGenerator {
	private static final Logger log = Logger.getLogger(IndexJspGenerator.class);

	private final File indexJsp;
	private final String instanceName;

	public IndexJspGenerator(final File indexJsp, final String instanceName) {
		this.indexJsp = indexJsp;
		this.instanceName = instanceName;
	}

	public void run() throws IOException, TemplateException {
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
		cfg.setClassForTemplateLoading(getClass(), "/" + getClass().getPackage().getName().replaceAll("\\.", "/"));
		cfg.setDefaultEncoding(StandardCharsets.UTF_8.name());
		cfg.setOutputEncoding(StandardCharsets.UTF_8.name());

		Map<String, Object> templateParams = new HashMap<>();
		templateParams.put(Constants.NUCLOS_INSTANCE, instanceName);

		Template template = cfg.getTemplate("index.jsp.ftl");
		try (Writer fileWriter = new FileWriter(indexJsp)) {
			template.process(templateParams, fileWriter);
		}
	}
}
