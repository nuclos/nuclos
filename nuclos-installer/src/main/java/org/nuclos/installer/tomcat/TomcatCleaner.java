package org.nuclos.installer.tomcat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableSet;

import freemarker.template.TemplateException;

/**
 * Removes unnecessary Tomcat files and makes some adjustments.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class TomcatCleaner {
	private static final Logger log = Logger.getLogger(TomcatCleaner.class);

	private static final Set<String> UNSAFE_FILES = ImmutableSet.of(
			"RELEASE-NOTES",
			"RUNNING.txt",
			"webapps"
	);

	private final File tomcatHome;
	private final String instanceName;

	public TomcatCleaner(final File tomcatHome, final String instanceName) {
		this.tomcatHome = tomcatHome;
		this.instanceName = instanceName;
	}

	/**
	 * Cleans the Tomcat dir and updates the given Collection of files.
	 */
	public void run(List<String> files) {
		deleteUnsafeFiles();

		recreateRoot(files);

		addRedirect(files);

		removeVersionInfo();
	}

	private void removeVersionInfo() {
		final File catalinaJar = new File(tomcatHome, "lib/catalina.jar");

		if (!catalinaJar.exists()) {
			log.warn("Could not remove Tomcat version info: catalina.jar not found");
			return;
		}

		final URI uri = URI.create("jar:" + catalinaJar.toURI().toString());

		try (FileSystem zipfs = FileSystems.newFileSystem(uri, new HashMap<>())) {
			InputStream stream = getClass().getResourceAsStream("ServerInfo.properties");
			Path pathInZipfile = zipfs.getPath("/org/apache/catalina/util/ServerInfo.properties");

			Files.copy(
					stream,
					pathInZipfile,
					StandardCopyOption.REPLACE_EXISTING
			);
		} catch (IOException e) {
			log.warn("Could not remove Tomcat version info", e);
		}
	}

	private void addRedirect(final List<String> files) {
		final File indexJsp = new File(tomcatHome, "webapps/ROOT/index.jsp");

		try {
			new IndexJspGenerator(indexJsp, instanceName).run();
			files.add(indexJsp.getAbsolutePath());
		} catch (IOException | TemplateException e) {
			log.error("Could not write index.jsp", e);
		}
	}

	private void recreateRoot(final List<String> files) {
		final File appRoot = new File(tomcatHome, "webapps/ROOT");
		if (!appRoot.exists()) {
			if (appRoot.mkdirs()) {
				log.info("Recreated webapps/ROOT");
			}
			files.add(appRoot.getAbsolutePath());
		}
	}

	/**
	 * @return The Set of absolute paths of the deleted files.
	 */
	private Set<String> deleteUnsafeFiles() {
		Set<String> result = new HashSet<>();

		UNSAFE_FILES.forEach(path -> {
			final File file = new File(tomcatHome, path);
			if (file.exists()) {
				try {
					FileUtils.forceDelete(file);
				} catch (IOException e) {
					log.warn("Could not delete unsafe Tomcat file or directory: " + file.getAbsolutePath(), e);
				}
			}
		});


		return result;
	}


}
