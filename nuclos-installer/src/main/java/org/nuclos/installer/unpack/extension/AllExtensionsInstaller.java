package org.nuclos.installer.unpack.extension;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.installer.unpack.AbstractUnpacker;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class AllExtensionsInstaller {
	private static final Logger log = Logger.getLogger(AbstractUnpacker.class);

	final List<ExtensionInstaller> installers = new ArrayList<>();

	public AllExtensionsInstaller(
			final InstallationContext context
	) {

		ClientExtensionInstaller clientExtensionInstaller = new ClientExtensionInstaller(context);
		ServerExtensionInstaller serverExtensionInstaller = new ServerExtensionInstaller(context);
		CommonExtensionInstaller commonExtensionInstaller = new CommonExtensionInstaller(
				context,
				clientExtensionInstaller,
				serverExtensionInstaller
		);

		installers.add(commonExtensionInstaller);
		installers.add(clientExtensionInstaller);
		installers.add(serverExtensionInstaller);

		installers.add(new WebclientExtensionInstaller(context));
		installers.add(new TomcatExtensionInstaller(context));
		installers.add(new NuclosConfExtensionInstaller(context));
		installers.add(new NuclosBinExtensionInstaller(context));
		installers.add(new WebappExtensionInstaller(context));
	}

	public List<String> install() {
		final List<String> files = new ArrayList<>();
		installers.forEach(installer -> {
			try {
				files.addAll(installer.install());
			} catch (Exception e) {
				log.warn("Failed to install " + installer + ": " + e.getMessage(), e);
			}
		});
		return files;
	}

}
