package org.nuclos.installer.unpack.extension;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class CommonExtensionInstaller extends ExtensionInstaller {
	private final ClientExtensionInstaller clientExtensionInstaller;
	private final ServerExtensionInstaller serverExtensionInstaller;

	CommonExtensionInstaller(
			final InstallationContext context,
			final ClientExtensionInstaller clientExtensionInstaller,
			final ServerExtensionInstaller serverExtensionInstaller
	) {
		super(context);

		this.clientExtensionInstaller = clientExtensionInstaller;
		this.serverExtensionInstaller = serverExtensionInstaller;
	}

	@Override
	File getSourceDirectory() {
		return new File(context.getExtensionsDir(), DirectoryName.COMMON);
	}

	@Override
	List<File> getTargetDirectories() {
		final List<File> targetDirectories = new ArrayList<>();

		targetDirectories.addAll(clientExtensionInstaller.getTargetDirectories());
		targetDirectories.addAll(serverExtensionInstaller.getTargetDirectories());

		return targetDirectories;
	}
}
