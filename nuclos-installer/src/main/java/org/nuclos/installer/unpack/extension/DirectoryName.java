package org.nuclos.installer.unpack.extension;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class DirectoryName {
	private DirectoryName() {
	}

	public static final String BIN = "bin";
	public static final String CLIENT = "client";
	public static final String COMMON = "common";
	public static final String CONF = "conf";
	public static final String EXTENSIONS = "extensions";
	public static final String SERVER = "server";
	public static final String TOMCAT = "tomcat";
	public static final String WEBAPP = "webapp";
	public static final String WEBCLIENT = "webclient";
}
