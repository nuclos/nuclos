package org.nuclos.installer.unpack.extension;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class NuclosConfExtensionInstaller extends ExtensionInstaller {
	NuclosConfExtensionInstaller(
			final InstallationContext context
	) {
		super(context);
	}

	@Override
	File getSourceDirectory() {
		return new File(context.getExtensionsDir(), DirectoryName.CONF);
	}

	@Override
	List<File> getTargetDirectories() {
		return Arrays.asList(
				new File(context.getNuclosHome(), DirectoryName.CONF)
		);
	}
}
