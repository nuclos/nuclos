package org.nuclos.installer.util;

public class CertificateVerificationException extends Exception {
	CertificateVerificationException(final Exception e) {
		super(e);
	}

	CertificateVerificationException(final String message) {
		super(message);
	}

	public CertificateVerificationException(final String message, final Exception cause) {
		super(message, cause);
	}
}
