package org.nuclos.installer.util;

public class KeystoreVerificationException extends Exception {

	KeystoreVerificationException(final String message) {
		super(message);
	}

	KeystoreVerificationException(final Exception e) {
		super(e);
	}
}
