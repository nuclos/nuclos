package org.nuclos.installer.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.stream.IntStream;

import org.apache.commons.codec.binary.Base64;

public class StringUtils {

	private StringUtils() {
		// Never invoked.
	}
	
	public static String join(Object... args) {
		final StringBuilder result = new StringBuilder();
		join(result, args);
		return result.substring(0, result.length() - 1);
	}
	
	private static void join(StringBuilder result, Object[] args) {
		for (Object o: args) {
			join(result, o);
		}
	}
	
	private static void join(StringBuilder result, Collection<?> c) {
		for (Object o: c) {
			join(result, o);
		}
	}
	
	private static void join(StringBuilder result, Object o) {
		if (o == null) {
			throw new NullPointerException();
		}
		if (o instanceof Object[]) {
			final Object[] p = (Object[]) o;
			for (final Object aP: p) {
				join(result, aP);
			}
		}
		else if (o instanceof Collection) {
			join(result, (Collection<?>) o);
		}
		else {
			result.append(o.toString());
			result.append(" ");
		}
	}
	
	public static InputStream asInputStream(String value) {
		if (value == null) {
			throw new NullPointerException();
		}
		return new ByteArrayInputStream(value.getBytes(FileUtils.UTF8));
	}

	public static String generateNuclosUidString() {
		return generateRandomString(20);
	}

	public static String generateRandomString(int length) {
		SecureRandom random;
		try {
			random = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			random = new SecureRandom();
		}
		String result = "";
		while (result.length() < length) {
			byte[] binaryData = new byte[length];
			random.nextBytes(binaryData);
			String uidData = Base64.encodeBase64String(binaryData).replace("+", "").replace("/", "").replace("=", "");
			result += uidData;
		}
		if (result.length() > length) {
			result = result.substring(0, length);
		}
		return result;
	}

	public static void main(String[] args) {
		IntStream.rangeClosed(1,10).forEach(i ->
			System.out.println(generateNuclosUidString())
		);
	}
	
}
