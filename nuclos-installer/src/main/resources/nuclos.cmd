@echo off
rem Starting this script from a network drive there may be the error that UNC-Names are not supported
rem In this case add the value "DisableUNCCheck REG_DWORD 0x1" to the registry:
rem	HKEY_CURRENT_USER - Software - Microsoft - Command Processor
rem see also: https://support.microsoft.com/de-de/kb/156276

set HOST=${client.serverhost}
set HTTPS=${server.https.enabled}

if "%HTTPS%" == "true" (
	set SERVER=https://%HOST%:${server.https.port}/${server.name}
) ELSE (
	set SERVER=http://%HOST%:${server.http.port}/${server.name}
)

set MAXHEAP=-Xmx640m
set VMA0=-Dserver=%SERVER%
set VMA1=-Duser.home="%USERPROFILE%"
set VMA2=-Dfile.encoding=UTF-8
set MAINCLASS=org.nuclos.client.main.Nuclos

if exist jre if exist jre\bin if exist jre\bin\java.exe (
	set JAVAPATH=jre
	GOTO Unpacking
)

:FindJavaPath
SET KIT=JavaSoft\Java Runtime Environment
call:ReadRegValue VER "HKLM\Software\%KIT%" "CurrentVersion"
IF "%VER%" NEQ "" GOTO FoundJRE

SET KIT=Wow6432Node\JavaSoft\Java Runtime Environment
call:ReadRegValue VER "HKLM\Software\%KIT%" "CurrentVersion"
IF "%VER%" NEQ "" GOTO FoundJRE

SET KIT=JavaSoft\Java Development Kit
call:ReadRegValue VER "HKLM\Software\%KIT%" "CurrentVersion"
IF "%VER%" NEQ "" GOTO FoundJRE

SET KIT=Wow6432Node\JavaSoft\Java Development Kit
call:ReadRegValue VER "HKLM\Software\%KIT%" "CurrentVersion"
IF "%VER%" NEQ "" GOTO FoundJRE

SET KIT=JavaSoft\JRE
call:ReadRegValue VER "HKLM\Software\%KIT%" "CurrentVersion"
IF "%VER%" NEQ "" GOTO FoundJRE

ECHO Failed to find Java
GOTO :EOF

:FoundJRE
call:ReadRegValue JAVAPATH "HKLM\Software\%KIT%\%VER%" "JavaHome"
ECHO JavaPath: %JAVAPATH%

GOTO Unpacking

:ReadRegValue
SET key=%2%
SET name=%3%
SET "%~1="
SET reg=reg
IF DEFINED ProgramFiles(x86) (
  IF EXIST %WINDIR%\sysnative\reg.exe SET reg=%WINDIR%\sysnative\reg.exe
)
FOR /F "usebackq tokens=3* skip=1" %%A IN (`%reg% QUERY %key% /v %name% 2^>NUL`) DO SET "%~1=%%A %%B"
GOTO :EOF

:JavaVersionGeq9
SET "%~1="
SET path=%2%
SET _path=%path:~0,-1%
SET JAVAEXECINT=%_path%\bin\java"
FOR /F tokens^=2-5^ delims^=.-+_^" %%j in ('%JAVAEXECINT% -fullversion 2^>^&1') DO SET "jver=%%j"
IF %JVER% GEQ 9 (
	SET "%~1=1"
) ELSE (
	SET "%~1=0"
)
GOTO :EOF

:Unpacking
set JAVAEXEC=%JAVAPATH%/bin/javaw

call:JavaVersionGeq9 VERSION_GEQ_9 "%JAVAPATH%"
IF %VERSION_GEQ_9% EQU 1 (
	set JAVA9ARGFILES=@java-9-args @java-9-args-win
)

echo Server=%SERVER%
echo JavaExec=%JAVAEXEC%

set NUCLOSLIBS="lib/*;"

if exist extensions (
	set NUCLOSLIBS="lib/*;extensions/*"
)

@echo on

start "" "%JAVAEXEC%" %JAVA9ARGFILES% %MAXHEAP% %VMA0% %VMA1% %VMA2% -cp %NUCLOSLIBS% %MAINCLASS%
exit 0
