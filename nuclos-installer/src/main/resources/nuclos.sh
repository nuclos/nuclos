#!/bin/bash
cd $(dirname $0)

HOST=${client.serverhost}
HTTPS=${server.https.enabled}

if [ "$HTTPS" = "true" ];
then
	SERVER=https://$HOST:${server.https.port}/${server.name}
else
	SERVER=http://$HOST:${server.http.port}/${server.name}
fi

MAXHEAP=-Xmx640m
VMA0=-Dserver=$SERVER
VMA1=-Duser.home=$HOME
VMA2=-Dfile.encoding=UTF-8
MAINCLASS=org.nuclos.client.main.Nuclos

JAVAEXEC=java

if [ -d jre -a -d jre/bin -a -f jre/bin/java ];
then
	JAVAPATH=jre
	JAVAEXEC=$JAVAPATH/bin/java
	chmod +x $JAVAEXEC
fi

if [[ "$JAVAEXEC" ]]; then
  version=$("$JAVAEXEC" -fullversion 2>&1| awk -F '"' '{print $2}' | awk -F '.' '{print $1}')

  if [[ "$version" -ge 9 ]]; then
    JAVA9ARGS=@java-9-args
  fi
fi

echo Server=$SERVER
echo JavaExec=$JAVAEXEC

NUCLOSLIBS="lib/*"

if [ -d extensions ];
then
	NUCLOSLIBS="lib/*:extensions/*"
fi

$JAVAEXEC $JAVA9ARGS $MAXHEAP $VMA0 $VMA1 $VMA2 -cp "$NUCLOSLIBS" $MAINCLASS
