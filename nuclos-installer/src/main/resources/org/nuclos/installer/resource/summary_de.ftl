<html>
<head>
    <title>Zusammenfassung</title>
</head>
<body>
Nuclos wird installiert unter <b>${server\.home}</b><br/><br/>
<b>Datenbankeinstellungen:</b><br/>
<#if .data_model["database.setup"] == "use">
	Eine bereits vorhandene Datenbank wird verwendet.<br/>
	Datenbank Adapter: <b>${database\.adapter}</b><br/>
	Treiber Jar-Datei: <b>${database\.driverjar}</b><br/>
	Host: <b>${database\.server}</b><br/>
	Port: <b>${database\.port}</b><br/>
	Datenbankname: <b>${database\.name}</b><br/>
</#if>

<#if .data_model["database.setup"] == "install">
	PostgreSQL wird installiert und eine neue Datenbank wird erstellt.<br/>
	PostgreSQL Installationspfad: <b>${postgres\.prefix}</b><br/>
	PostgreSQL Datenpfad: <b>${postgres\.datadir}</b><br/>
	Port: <b>${database\.port}</b><br/>
	Datenbankname: <b>${database\.name}</b><br/>
</#if>

<#if .data_model["database.setup"] == "setup">
    Eine neue Datenbank wird aufgesetzt.<br/>
	Host: <b>${database\.server}</b><br/>
	Port: <b>${database\.port}</b><br/>
	Superuser: <b>${postgres\.superuser}</b><br/>
	Datenbankname: <b>${database\.name}</b><br/>
</#if>

<#if .data_model["database.setup"] == "embedded">
	Eine eingebettete H2-Datenbank wird benutzt (beta).<br/>
</#if>

Benutzername: <b>${database\.username}</b><br/>
Schema: <b>${database\.schema}</b><br/>

<#if .data_model["database.setup"] == "install" || .data_model["database.setup"] == "use" || .data_model["database.setup"] == "setup" >
	Tablespace: <b>${database\.tablespace}</b><br/>
	Tablespace Pfad: <b>${postgres\.tablespacepath!"(nicht gesetzt)"}</b><br/>
	Connection Pool-Größe: <b>${database\.connection\.pool\.size}</b><br/>
	Connection Pool Wartezeit in Sekunden: <b>${database\.connection\.pool\.wait\.seconds}</b><br/>
	Connection Initialisierung: <b>${database\.connection\.init}</b><br/>
</#if>

<#if .data_model["database.setup"] == "use">
	Index-Tablespace: <b>${database\.tablespace\.index}</b><br/>
	MSSQL Transaktionsisolation: <b>${database\.mssql\.isolation}</b><br/>
</#if>

<br/>
<b>Servereinstellungen:</b><br/>
Java Pfad: <b>${server\.java\.home}</b><br/>
Instanzname: <b>${server\.name}</b><br/>
Http port: <b>${server\.http\.port}</b><br/>
Https port: <b>${server\.https\.port}</b><br/>
AJP port: <b>${server\.ajp\.port}</b><br/>
Shutdown port: <b>${server\.shutdown\.port}</b><br/>
Connection Thread Pool-Größe: <b>${server\.connection\.thread\.pool\.size}</b><br/>
Heap-Gr&ouml;&szlig;e: <b>${server\.heap\.size}</b><br/>

<#if .data_model["database.setup"] == "embedded" || .data_model["database.setup"] == "use">
	Development environment: <b>${server\.development\.enabled}</b><br/>
	Debug port: <b>${server\.development\.debugport}</b><br/>
	JMX Port: <b>${server\.development\.jmxport}</b><br/>
</#if>

Beim Systemstart starten: <b>${server\.launch\.on\.startup}</b><br/><br/>

<#if (.data_model["server.cluster.mode"] == "true")>
	<b>Cluster settings:</b><br/>
	Node Id: <b>${server\.cluster\.node\.id}</b><br/>
	Node Typ: <b>${server\.cluster\.node\.type}</b><br/>
	Node Protokoll: <b>${server\.cluster\.node\.protocol}</b><br/>
	Node Hostname: <b>${server\.cluster\.node\.hostname}</b><br/>
	Node Port: <b>${server\.cluster\.node\.port}</b><br/>
	Load Balancer Protokoll: <b>${server\.cluster\.balancer\.protocol}</b><br/>
	Load Balancer Hostname: <b>${server\.cluster\.balancer\.hostname}</b><br/>
	Load Balancer Port: <b>${server\.cluster\.balancer\.port}</b><br/>
	Load Balancer Kontext-Pfad: <b>${server\.cluster\.balancer\.context}</b><br/><br/>
</#if>

<b>Clienteinstellungen:</b><br/>
Singleinstance: <b>${client\.singleinstance}</b><br/>
Webclient: <b>${client\.webclient}</b><br/>
Richclient: <b>${client\.richclient}</b><br/>
Launcher: <b>${client\.launcher}</b><br/>
Serverhost: <b>${client\.serverhost}</b><br/>
JRE: <b>${client\.jre}</b>
</body>
</html>
