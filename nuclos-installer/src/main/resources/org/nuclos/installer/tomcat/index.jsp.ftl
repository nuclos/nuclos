<%@ page session="false" %>
<%
	response.sendRedirect(
		request.getQueryString() != null
		? ("/${.data_model["server.name"]?url}/?" + request.getQueryString())
		: "/${.data_model["server.name"]?url}/"
	);
%>