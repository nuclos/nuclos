package org.nuclos.installer.tomcat;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import freemarker.template.TemplateException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class IndexJspGeneratorTest {

	private File testFile;

	@Before
	public void setup() throws IOException {
		Path temp = Files.createTempFile(getClass().getSimpleName(), "jsp");
		testFile = temp.toFile();
	}

	@Test
	public void generateIndexJsp() throws IOException, TemplateException {
		IndexJspGenerator generator = new IndexJspGenerator(testFile, "Test Instance Name");
		generator.run();

		final String fileContent = FileUtils.readFileToString(testFile);
		assert fileContent.contains("Test%20Instance%20Name") : "File content does not contain URL-encoded instance name:\n" + fileContent;
	}
}