package org.nuclos.installer.unpack.extension;

import java.io.IOException;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class AllExtensionsInstallerTest extends ExtensionInstallerTest {

	@Test
	public void installers() throws IOException {
		InstallationContext context = getInstallationContext();

		AllExtensionsInstaller extensionInstaller = new AllExtensionsInstaller(context);

		assert extensionInstaller.installers.size() == 8;

		// Should work without Exceptions
		extensionInstaller.install();
	}
}