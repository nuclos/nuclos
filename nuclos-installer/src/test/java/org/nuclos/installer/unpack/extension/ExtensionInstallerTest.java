package org.nuclos.installer.unpack.extension;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;
import org.nuclos.installer.mode.Installer;
import org.nuclos.installer.unpack.Unpacker;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
abstract class ExtensionInstallerTest {
	InstallationContext getInstallationContext() throws IOException {
		String tomcatVersion = "tomcat-x.x.x";
		File nuclosHome = Files.createTempDirectory("test-install").toFile();
		File tomcatDir = new File(nuclosHome, "tomcat/" + tomcatVersion);

		return new InstallationContext(
				tomcatVersion,
				tomcatDir,
				new File(tomcatDir, "webapps/ROOT/webclient"),
				nuclosHome,
				new TestInstaller(),
				new File(nuclosHome, "extensions"),
				new File(nuclosHome, "webapp")
		);
	}

	void assertFileWithContent(final File file, final String content) throws IOException {
		assert FileUtils.readFileToString(file).equals(content);
	}

	class TestInstaller implements Installer {

		@Override
		public void install(final Unpacker os) {

		}

		@Override
		public void uninstall(final Unpacker os) {

		}

		@Override
		public void info(final String resid, final Object... args) {

		}

		@Override
		public void warn(final String resid, final Object... args) {

		}

		@Override
		public void error(final String resid, final Object... args) {

		}

		@Override
		public void logException(final Throwable e) {

		}

		@Override
		public int askQuestion(final String text, final int questiontype, final int automatedAnswer, final Object... args) {
			return 0;
		}

		@Override
		public void close() {

		}

		@Override
		public void cancel() {

		}
	}
}