package org.nuclos.installer.util;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashSet;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class CertificateVerifierTest {

	@Test(expected = CertificateVerificationException.class)
	public void checkTrust() throws CertificateException, IOException, CertificateVerificationException {
		try(InputStream stream = getClass().getResourceAsStream("selfsigned.pem")) {
			CertificateFactory fact = CertificateFactory.getInstance("X.509");
			X509Certificate cert = (X509Certificate) fact.generateCertificate(stream);
			try {
				CertificateVerifier.verify(cert, new HashSet<>());
			} catch (CertificateVerificationException e) {
				e.printStackTrace();
				assert e.getMessage().contains("self-signed") || e.getMessage().contains("selbst-signiert");
				throw e;
			}
		}
	}
}