package org.nuclos.installer.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class FileUtilsTest {

	File nonEmptyDir;
	File emptyDir;

	@Before
	public void setup() throws IOException {
		nonEmptyDir = Files.createTempDirectory("non-empty").toFile();
		File file = new File(nonEmptyDir, "test.txt");
		file.createNewFile();

		emptyDir = Files.createTempDirectory("empty").toFile();
	}

	@Test(expected = IOException.class)
	public void deleteWithException() throws IOException {
		assert nonEmptyDir.exists() && nonEmptyDir.isDirectory();
		FileUtils.delete(nonEmptyDir, false);
	}

	@Test
	public void delete() throws IOException {
		assert emptyDir.exists() && emptyDir.isDirectory();
		FileUtils.delete(emptyDir, true);
		assert !emptyDir.exists();

		assert nonEmptyDir.exists() && nonEmptyDir.isDirectory();
		FileUtils.delete(nonEmptyDir, true);
		assert !nonEmptyDir.exists();
	}
}