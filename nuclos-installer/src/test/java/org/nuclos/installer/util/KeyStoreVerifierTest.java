package org.nuclos.installer.util;

import java.io.InputStream;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class KeyStoreVerifierTest {

	@Test(expected = KeystoreVerificationException.class)
	public void verifyKeystore() throws KeystoreVerificationException {
		InputStream stream = getClass().getResourceAsStream("keystore.jks");
		try {
			KeyStoreVerifier.verify(stream, "password");
		} catch (KeystoreVerificationException e) {
			assert e.getMessage().contains("self-signed") || e.getMessage().contains("selbst-signiert");
			throw e;
		}
	}

	@Test(expected = KeystoreVerificationException.class)
	public void verifyKeystoreWrongPassword() throws KeystoreVerificationException {
		InputStream stream = getClass().getResourceAsStream("keystore.jks");
		try {
			KeyStoreVerifier.verify(stream, "");
		} catch (Exception e) {
			assert e.toString().contains("password");
			throw e;
		}
	}
}