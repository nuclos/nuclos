package example.rest;

import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.statemodel.State;
import org.nuclos.common.UID;

@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.statemodel.StatemodelObjectBuilder")
public class ExampleorderSM {

public static State State_10 = 
	new State(UID.parseUID("wJHQIygTfOGTRcbZNvd4"), "Ein hinweis ohne html Tags", "Processing", 10, UID.parseUID("WNgPPsooAgC5fpUNWjVh"));
public static State State_80 = 
	new State(UID.parseUID("Ovf7rfxjXnUF19dDXHlj"), "<html>Alles <b>erledigt</b>!!!<br /><h2>Neue Zeile</h2></html>", "Finished", 80, UID.parseUID("WNgPPsooAgC5fpUNWjVh"));
public static State State_90 = 
	new State(UID.parseUID("hZm1rFHbbUvyEQDjELk6"), "Canceled", "Canceled", 90, UID.parseUID("WNgPPsooAgC5fpUNWjVh"));
public static State State_95 = 
	new State(UID.parseUID("ZmBGUrBJjpIfZFNO3QSo"), "Review", "Review", 95, UID.parseUID("WNgPPsooAgC5fpUNWjVh"));


  public static Integer getNumeral(org.nuclos.api.UID stateId) throws BusinessException {
    if (stateId == null) {
      throw new IllegalArgumentException("stateId must not be null");
    }

    if (stateId.equals(State_10.getId())) {
      return State_10.getNumeral();
    }
    if (stateId.equals(State_80.getId())) {
      return State_80.getNumeral();
    }
    if (stateId.equals(State_90.getId())) {
      return State_90.getNumeral();
    }
    if (stateId.equals(State_95.getId())) {
      return State_95.getNumeral();
    }
    throw new BusinessException("Status with id " + stateId + " does not exist in this status model");
  }

  public static Integer getNumeral(Stateful statefulBusinessObject) throws BusinessException {
    return getNumeral(statefulBusinessObject.getNuclosStateId());
  }

}
