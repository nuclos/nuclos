//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package example.rest;

import java.util.Date;
import java.util.List;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.Dependent;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.businessobject.Process;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.businessobject.facade.Lockable;
import org.nuclos.api.businessobject.facade.LogicalDeletable;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * BusinessObject: Order
 *<br>
 *<br>Nuclet: example.rest
 *<br>DB-Name: V594_ORDER
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: true
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class Order extends AbstractBusinessObject<Long> implements Lockable<Long>, LogicalDeletable<Long>, Modifiable<Long>, Stateful {

//public static final Dependent<example.rest.Invoice> _Invoice =
//	new Dependent<>("_Invoice", "null", "Invoice", "ZKzwnNnFSv3UJEfBXfwM", "order", "IOglB5aQmfBAKw1PVX7e", example.rest.Invoice.class);
//
//public static final Dependent<example.rest.OrderPosition> _OrderPosition =
//	new Dependent<>("_OrderPosition", "null", "OrderPosition", "WPYYgEC0psVEY9d7C78Z", "order", "6NnStbGHIp0xVoXK21KL", example.rest.OrderPosition.class);
//
//public static final Dependent<example.rest.PerspectiveOrderPosition> _PerspectiveOrderPosition =
//	new Dependent<>("_PerspectiveOrderPosition", "null", "PerspectiveOrderPosition", "ByaAaa7HcvvYtUb0xciA", "order", "SezJXyDDMCn1kL492S4l", example.rest.PerspectiveOrderPosition.class);


/**
 * Attribute: agent
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRagent
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> AgentId =
	new ForeignKeyAttribute<>("AgentId", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "UGuC6Ibf5S6i55nNB20K", UID.class);


/**
 * Attribute: approvedcreditlimit
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DBLapprovedcreditlimit
 *<br>Data type: java.lang.Double
 *<br>Localized: false
 *<br>Output format: #,##0.00
 *<br>Scale: 9
 *<br>Precision: 2
**/
public static final NumericAttribute<java.math.BigDecimal> Approvedcreditlimit = 
	new NumericAttribute<>("Approvedcreditlimit", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "FJaYvsscbnStZpX8v7ex", java.math.BigDecimal.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "A3wu3cgTYSt3iXx8mIeH3", Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "A3wu3cgTYSt3iXx8mIeH4", String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "A3wu3cgTYSt3iXx8mIeH1", Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "A3wu3cgTYSt3iXx8mIeH2", String.class);


/**
 * Attribute: customerAddress
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRcustomerAddress
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<Long> CustomerAddressId =
	new ForeignKeyAttribute<>("CustomerAddressId", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "91cxckSRNQyaKskdj0Zl", Long.class);


/**
 * Attribute: customer
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRcustomer
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<Long> CustomerId =
	new ForeignKeyAttribute<>("CustomerId", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "F8oMtVf266iYLHEF91LO", Long.class);


/**
 * Attribute: deliveryDate
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATdeliveryDate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> DeliveryDate =
	new NumericAttribute<>("DeliveryDate", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "3YiIdfSeqeqmmnb2ZOpW", Date.class);


/**
 * Attribute: discount
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DBLdiscount
 *<br>Data type: java.lang.Double
 *<br>Localized: false
 *<br>Output format: #,##0.00
 *<br>Scale: 9
 *<br>Precision: 2
**/
public static final NumericAttribute<java.math.BigDecimal> Discount = 
	new NumericAttribute<>("Discount", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "15f8oxrX6cSka7yq0jTS", java.math.BigDecimal.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<Long> Id =
	new PrimaryKeyAttribute<>("Id", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "A3wu3cgTYSt3iXx8mIeH0", Long.class);


/**
 * Attribute: note
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRnote
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<String> Note = new StringAttribute<>("Note", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "mkbW5XHJG69FyoxBA4uE", String.class);


/**
 * Attribute: notmodifiable
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRnotmodifiable
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Notmodifiable = new StringAttribute<>("Notmodifiable", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "BscGgRLzNldaiWhnxZkO", String.class);


/**
 * Attribute: nuclosDeleted
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: BLNNUCLOSDELETED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> NuclosLogicalDeleted =
	new Attribute<>("NuclosLogicalDeleted", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "A3wu3cgTYSt3iXx8mIeH12", Boolean.class);


/**
 * Attribute: nuclosProcess
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> NuclosProcessId =
	new ForeignKeyAttribute<>("NuclosProcessId", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "A3wu3cgTYSt3iXx8mIeH10", UID.class);


/**
 * Attribute: nuclosState
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_NUCLOSSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> NuclosStateId =
	new ForeignKeyAttribute<>("NuclosStateId", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "A3wu3cgTYSt3iXx8mIeH6", UID.class);


/**
 * Attribute: nuclosSystemId
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRNUCLOSSYSTEMID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> NuclosSystemIdentifier = new StringAttribute<>("NuclosSystemIdentifier", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "A3wu3cgTYSt3iXx8mIeH9", String.class);


/**
 * Attribute: orderDate
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATorderDate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> OrderDate =
	new NumericAttribute<>("OrderDate", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "sgVvrtGMgTS4B8l6MuOw", Date.class);


/**
 * Attribute: orderNumber
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: INTorderNumber
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<Integer> OrderNumber =
	new NumericAttribute<>("OrderNumber", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "DehvMLszOxRH2OHMcl5D", Integer.class);


/**
 * Attribute: rechnung
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRrechnung
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<Long> RechnungId =
	new ForeignKeyAttribute<>("RechnungId", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "uE63EntKfsZPwpTPSowc", Long.class);


/**
 * Attribute: sendDate
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATsendDate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> SendDate =
	new NumericAttribute<>("SendDate", "example.rest", "A3wu3cgTYSt3iXx8mIeH", "X8rGwHEZYlzAREmnaKun", Date.class);

public static final Process<Order> Priorityorder =
	new Process(new org.nuclos.common.UID("wyXdSnBkh0CbXoYW4lxU"),
		new org.nuclos.common.UID("A3wu3cgTYSt3iXx8mIeH"), Order.class);
public static final Process<Order> Standardorder =
	new Process(new org.nuclos.common.UID("kG5v0nsIZ8fmUDidwTuw"),
		new org.nuclos.common.UID("A3wu3cgTYSt3iXx8mIeH"), Order.class);

public Order() {
		super("A3wu3cgTYSt3iXx8mIeH");
		setNuclosLogicalDeleted(Boolean.FALSE);
}
/**
* Change Status of this BO. Use this instead of StateModelProvider
*/
public void changeStatus(org.nuclos.api.statemodel.State status) throws org.nuclos.api.exception.BusinessException {
		super.changeStatus(status);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(Order boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public Order copy() {
		return super.copy(Order.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("A3wu3cgTYSt3iXx8mIeH"), id);
}


/**
 * Remove-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_generalsearchdocument
 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteAttachment(org.nuclos.api.common.NuclosFile pNuclosFile) {
		super.deleteGenericObjectDocumentAttachment(pNuclosFile);
}


/**
 * Delete-Method for attribute: order
 *<br>
 *<br>Entity: Invoice
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: java.lang.String
 *<br>Reference entity: Order
 *<br>Reference field: orderNumber customer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public void deleteInvoice(example.rest.Invoice pInvoice) {
//		deleteDependent(_Invoice, pInvoice);
//}


/**
 * Delete-Method for attribute: order
 *<br>
 *<br>Entity: OrderPosition
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: java.lang.String
 *<br>Reference entity: Order
 *<br>Reference field: orderNumber
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public void deleteOrderPosition(example.rest.OrderPosition pOrderPosition) {
//		deleteDependent(_OrderPosition, pOrderPosition);
//}


/**
 * Delete-Method for attribute: order
 *<br>
 *<br>Entity: PerspectiveOrderPosition
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: java.lang.String
 *<br>Reference entity: Order
 *<br>Reference field: orderNumber customer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public void deletePerspectiveOrderPosition(example.rest.PerspectiveOrderPosition pPerspectiveOrderPosition) {
//		deleteDependent(_PerspectiveOrderPosition, pPerspectiveOrderPosition);
//}
/**
* Static Get by Id
*/
public static Order get(Long id) {
		return get(Order.class, id);
}


/**
 * Getter-Method for attribute: agent
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRagent
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_user
 *<br>Reference field: username
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getAgentId() {
		return getFieldUid("UGuC6Ibf5S6i55nNB20K");
}


/**
 * Getter-Method for attribute: approvedcreditlimit
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DBLapprovedcreditlimit
 *<br>Data type: java.lang.Double
 *<br>Localized: false
 *<br>Output format: #,##0.00
 *<br>Scale: 9
 *<br>Precision: 2
**/
public java.math.BigDecimal getApprovedcreditlimit() {
	java.math.BigDecimal retVal = null;
	if (getField("FJaYvsscbnStZpX8v7ex", Double.class) != null) {
		retVal = new java.math.BigDecimal(getField("FJaYvsscbnStZpX8v7ex", Double.class));
		retVal = retVal.setScale(2, java.math.BigDecimal.ROUND_HALF_UP);
	}
	return retVal;
}


/**
 * Getter-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_generalsearchdocument
 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.api.common.NuclosFile> getAttachments() {
		return super.getAttachments();
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("A3wu3cgTYSt3iXx8mIeH3", Date.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("A3wu3cgTYSt3iXx8mIeH4", String.class);
}

/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setChangedBy(String username) {
	setField("A3wu3cgTYSt3iXx8mIeH4", username);
}

/**
 * Getter-Method for attribute: confirmation
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRconfirmation
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.api.common.NuclosFile getConfirmation() {
		return getNuclosFile("ZGlXxHscatQLBoGWXmjm");
}


/**
 * Getter-Method for attribute: confirmation
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRconfirmation
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> T getConfirmation(Class<T> pConfirmation) {
		return super.getNuclosFile(pConfirmation,"ZGlXxHscatQLBoGWXmjm");
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("A3wu3cgTYSt3iXx8mIeH1", Date.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("A3wu3cgTYSt3iXx8mIeH2", String.class);
}

/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setCreatedBy(String username) {
	setField("A3wu3cgTYSt3iXx8mIeH2", username);
}

/**
 * Getter-Method for attribute: customerAddress
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRcustomerAddress
 *<br>Data type: java.lang.String
 *<br>Reference entity: CustomerAddress
 *<br>Reference field: city, zipCode, street
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends AbstractBusinessObject> T getCustomerAddressBO(Class<T> boClass) {
		return getReferencedBO(boClass, getFieldId("91cxckSRNQyaKskdj0Zl"), "91cxckSRNQyaKskdj0Zl", "iCkm9KpNyzMk7Zi47tYn");
}


/**
 * Getter-Method for attribute: customerAddress
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRcustomerAddress
 *<br>Data type: java.lang.String
 *<br>Reference entity: CustomerAddress
 *<br>Reference field: city, zipCode, street
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public example.rest.CustomerAddress getCustomerAddressBO() {
//		return getReferencedBO(example.rest.CustomerAddress.class, getFieldId("91cxckSRNQyaKskdj0Zl"), "91cxckSRNQyaKskdj0Zl", "iCkm9KpNyzMk7Zi47tYn");
//}


/**
 * Getter-Method for attribute: customerAddress
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRcustomerAddress
 *<br>Data type: java.lang.String
 *<br>Reference entity: CustomerAddress
 *<br>Reference field: city, zipCode, street
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public Long getCustomerAddressId() {
		return getFieldId("91cxckSRNQyaKskdj0Zl");
}


/**
 * Getter-Method for attribute: customer
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRcustomer
 *<br>Data type: java.lang.String
 *<br>Reference entity: Customer
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends AbstractBusinessObject> T getCustomerBO(Class<T> boClass) {
		return getReferencedBO(boClass, getFieldId("F8oMtVf266iYLHEF91LO"), "F8oMtVf266iYLHEF91LO", "sYFUgWuHvxtv8MDrpgV2");
}


/**
 * Getter-Method for attribute: customer
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRcustomer
 *<br>Data type: java.lang.String
 *<br>Reference entity: Customer
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public example.rest.Customer getCustomerBO() {
//		return getReferencedBO(example.rest.Customer.class, getFieldId("F8oMtVf266iYLHEF91LO"), "F8oMtVf266iYLHEF91LO", "sYFUgWuHvxtv8MDrpgV2");
//}


/**
 * Getter-Method for attribute: customer
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRcustomer
 *<br>Data type: java.lang.String
 *<br>Reference entity: Customer
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public Long getCustomerId() {
		return getFieldId("F8oMtVf266iYLHEF91LO");
}


/**
 * Getter-Method for attribute: deliveryDate
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATdeliveryDate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getDeliveryDate() {
		return getField("3YiIdfSeqeqmmnb2ZOpW", Date.class);
}


/**
 * Getter-Method for attribute: discount
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DBLdiscount
 *<br>Data type: java.lang.Double
 *<br>Localized: false
 *<br>Output format: #,##0.00
 *<br>Scale: 9
 *<br>Precision: 2
**/
public java.math.BigDecimal getDiscount() {
	java.math.BigDecimal retVal = null;
	if (getField("15f8oxrX6cSka7yq0jTS", Double.class) != null) {
		retVal = new java.math.BigDecimal(getField("15f8oxrX6cSka7yq0jTS", Double.class));
		retVal = retVal.setScale(2, java.math.BigDecimal.ROUND_HALF_UP);
	}
	return retVal;
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("A3wu3cgTYSt3iXx8mIeH");
}


/**
 * Getter-Method for attribute: order
 *<br>
 *<br>Entity: Invoice
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: java.lang.String
 *<br>Reference entity: Order
 *<br>Reference field: orderNumber customer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public List<example.rest.Invoice> getInvoice(Flag... flags) {
//		return getDependents(_Invoice, flags);
//}


/**
 * Getter-Method for attribute: note
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRnote
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public String getNote() {
		return getField("mkbW5XHJG69FyoxBA4uE", String.class);
}


/**
 * Getter-Method for attribute: notmodifiable
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRnotmodifiable
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getNotmodifiable() {
		return getField("BscGgRLzNldaiWhnxZkO", String.class);
}


/**
 * Getter-Method for attribute: nuclosDeleted
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: BLNNUCLOSDELETED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getNuclosLogicalDeleted() {
		return getField("A3wu3cgTYSt3iXx8mIeH12", Boolean.class);
}


/**
 * Getter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getNuclosProcessId() {
		return getFieldUid("A3wu3cgTYSt3iXx8mIeH10");
}


/**
 * Getter-Method for attribute: nuclosState
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_NUCLOSSTATE
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_state
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getNuclosStateId() {
		return getFieldUid("A3wu3cgTYSt3iXx8mIeH6");
}


/**
 * Getter-Method for attribute: nuclosSystemId
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRNUCLOSSYSTEMID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getNuclosSystemIdentifier() {
		return getField("A3wu3cgTYSt3iXx8mIeH9", String.class);
}


/**
 * Getter-Method for attribute: order
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.api.common.NuclosFile getOrder() {
		return getNuclosFile("8UBP3lSkRvpQN5161M7m");
}


/**
 * Getter-Method for attribute: order
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> T getOrder(Class<T> pOrder) {
		return super.getNuclosFile(pOrder,"8UBP3lSkRvpQN5161M7m");
}


/**
 * Getter-Method for attribute: orderDate
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATorderDate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getOrderDate() {
		return getField("sgVvrtGMgTS4B8l6MuOw", Date.class);
}


/**
 * Getter-Method for attribute: orderNumber
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: INTorderNumber
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public Integer getOrderNumber() {
		return getField("DehvMLszOxRH2OHMcl5D", Integer.class);
}


/**
 * Getter-Method for attribute: order
 *<br>
 *<br>Entity: OrderPosition
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: java.lang.String
 *<br>Reference entity: Order
 *<br>Reference field: orderNumber
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public List<example.rest.OrderPosition> getOrderPosition(Flag... flags) {
//		return getDependents(_OrderPosition, flags);
//}


/**
 * Getter-Method for attribute: order
 *<br>
 *<br>Entity: PerspectiveOrderPosition
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: java.lang.String
 *<br>Reference entity: Order
 *<br>Reference field: orderNumber customer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public List<example.rest.PerspectiveOrderPosition> getPerspectiveOrderPosition(Flag... flags) {
//		return getDependents(_PerspectiveOrderPosition, flags);
//}


/**
 * Getter-Method for attribute: rechnung
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRrechnung
 *<br>Data type: java.lang.String
 *<br>Reference entity: Rechnung
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends AbstractBusinessObject> T getRechnungBO(Class<T> boClass) {
		return getReferencedBO(boClass, getFieldId("uE63EntKfsZPwpTPSowc"), "uE63EntKfsZPwpTPSowc", "LSJBzTfQtnC8Sa7XGmjP");
}


/**
 * Getter-Method for attribute: rechnung
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRrechnung
 *<br>Data type: java.lang.String
 *<br>Reference entity: Rechnung
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public example.rest.Rechnung getRechnungBO() {
//		return getReferencedBO(example.rest.Rechnung.class, getFieldId("uE63EntKfsZPwpTPSowc"), "uE63EntKfsZPwpTPSowc", "LSJBzTfQtnC8Sa7XGmjP");
//}


/**
 * Getter-Method for attribute: rechnung
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRrechnung
 *<br>Data type: java.lang.String
 *<br>Reference entity: Rechnung
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public Long getRechnungId() {
		return getFieldId("uE63EntKfsZPwpTPSowc");
}


/**
 * Getter-Method for attribute: sendDate
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATsendDate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getSendDate() {
		return getField("X8rGwHEZYlzAREmnaKun", Date.class);
}


/**
 * Insert-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_generalsearchdocument
 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertAttachment(org.nuclos.api.common.NuclosFile pNuclosFile, String pComment) {
		insertGenericObjectDocumentAttachment(pNuclosFile,pComment);
}


/**
 * Insert-Method for attribute: order
 *<br>
 *<br>Entity: Invoice
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: java.lang.String
 *<br>Reference entity: Order
 *<br>Reference field: orderNumber customer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public void insertInvoice(example.rest.Invoice pInvoice) {
//		insertDependent(_Invoice, pInvoice);
//}


/**
 * Insert-Method for attribute: order
 *<br>
 *<br>Entity: OrderPosition
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: java.lang.String
 *<br>Reference entity: Order
 *<br>Reference field: orderNumber
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public void insertOrderPosition(example.rest.OrderPosition pOrderPosition) {
//		insertDependent(_OrderPosition, pOrderPosition);
//}


/**
 * Insert-Method for attribute: order
 *<br>
 *<br>Entity: PerspectiveOrderPosition
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: java.lang.String
 *<br>Reference entity: Order
 *<br>Reference field: orderNumber customer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
//public void insertPerspectiveOrderPosition(example.rest.PerspectiveOrderPosition pPerspectiveOrderPosition) {
//		insertDependent(_PerspectiveOrderPosition, pPerspectiveOrderPosition);
//}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
	super.save(saveFlags);
}


/**
 * Setter-Method for attribute: agent
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRagent
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_user
 *<br>Reference field: username
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setAgentId(UID pAgentId) {
		setFieldId("UGuC6Ibf5S6i55nNB20K", pAgentId); 
}


/**
 * Setter-Method for attribute: approvedcreditlimit
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DBLapprovedcreditlimit
 *<br>Data type: java.lang.Double
 *<br>Localized: false
 *<br>Output format: #,##0.00
 *<br>Scale: 9
 *<br>Precision: 2
**/
public void setApprovedcreditlimit(java.math.BigDecimal pApprovedcreditlimit) {
	if (pApprovedcreditlimit != null) {
		pApprovedcreditlimit = pApprovedcreditlimit.setScale(2, java.math.BigDecimal.ROUND_HALF_UP);
	}
	setField("FJaYvsscbnStZpX8v7ex", pApprovedcreditlimit != null ? pApprovedcreditlimit.doubleValue() : null); 
}


/**
 * Setter-Method for attribute: confirmation
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRconfirmation
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> void setConfirmation(T pConfirmation) {
		super.setNuclosFile(pConfirmation,"ZGlXxHscatQLBoGWXmjm");
}


/**
 * Setter-Method for attribute: customerAddress
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRcustomerAddress
 *<br>Data type: java.lang.String
 *<br>Reference entity: CustomerAddress
 *<br>Reference field: city, zipCode, street
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCustomerAddressId(Long pCustomerAddressId) {
		setFieldId("91cxckSRNQyaKskdj0Zl", pCustomerAddressId); 
}


/**
 * Setter-Method for attribute: customer
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRcustomer
 *<br>Data type: java.lang.String
 *<br>Reference entity: Customer
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCustomerId(Long pCustomerId) {
		setFieldId("F8oMtVf266iYLHEF91LO", pCustomerId); 
}


/**
 * Setter-Method for attribute: deliveryDate
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATdeliveryDate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setDeliveryDate(Date pDeliveryDate) {
		setField("3YiIdfSeqeqmmnb2ZOpW", pDeliveryDate); 
}


/**
 * Setter-Method for attribute: discount
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DBLdiscount
 *<br>Data type: java.lang.Double
 *<br>Localized: false
 *<br>Output format: #,##0.00
 *<br>Scale: 9
 *<br>Precision: 2
**/
public void setDiscount(java.math.BigDecimal pDiscount) {
	if (pDiscount != null) {
		pDiscount = pDiscount.setScale(2, java.math.BigDecimal.ROUND_HALF_UP);
	}
	setField("15f8oxrX6cSka7yq0jTS", pDiscount != null ? pDiscount.doubleValue() : null); 
}


/**
 * Setter-Method for attribute: note
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRnote
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setNote(String pNote) {
		setField("mkbW5XHJG69FyoxBA4uE", pNote); 
}


/**
 * Setter-Method for attribute: notmodifiable
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRnotmodifiable
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setNotmodifiable(String pNotmodifiable) {
		setField("BscGgRLzNldaiWhnxZkO", pNotmodifiable); 
}


/**
 * Setter-Method for attribute: nuclosDeleted
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: BLNNUCLOSDELETED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNuclosLogicalDeleted(Boolean pNuclosLogicalDeleted) {
		setField("A3wu3cgTYSt3iXx8mIeH12", pNuclosLogicalDeleted); 
}


/**
 * Getter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRUID_NUCLOSPROCESS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public void setNuclosProcess(Process<Order> pProcess) {
		setFieldId("A3wu3cgTYSt3iXx8mIeH10", pProcess.getId());
}


/**
 * Setter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setNuclosProcessId(UID pNuclosProcessId) {
		setFieldId("A3wu3cgTYSt3iXx8mIeH10", pNuclosProcessId); 
}


/**
 * Setter-Method for attribute: nuclosSystemId
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRNUCLOSSYSTEMID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setNuclosSystemIdentifier(String pNuclosSystemIdentifier) {
		setField("A3wu3cgTYSt3iXx8mIeH9", pNuclosSystemIdentifier); 
}


/**
 * Setter-Method for attribute: order
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRorder
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> void setOrder(T pOrder) {
		super.setNuclosFile(pOrder,"8UBP3lSkRvpQN5161M7m");
}


/**
 * Setter-Method for attribute: orderDate
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATorderDate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setOrderDate(Date pOrderDate) {
		setField("sgVvrtGMgTS4B8l6MuOw", pOrderDate); 
}


/**
 * Setter-Method for attribute: orderNumber
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: INTorderNumber
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setOrderNumber(Integer pOrderNumber) {
		setField("DehvMLszOxRH2OHMcl5D", pOrderNumber); 
}


/**
 * Setter-Method for attribute: rechnung
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: STRVALUE_STRrechnung
 *<br>Data type: java.lang.String
 *<br>Reference entity: Rechnung
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setRechnungId(Long pRechnungId) {
		setFieldId("uE63EntKfsZPwpTPSowc", pRechnungId); 
}


/**
 * Setter-Method for attribute: sendDate
 *<br>
 *<br>Entity: Order
 *<br>DB-Name: DATsendDate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setSendDate(Date pSendDate) {
		setField("X8rGwHEZYlzAREmnaKun", pSendDate); 
}
 }
