//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package nuclet.test.rules;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.nuclos.api.UID;
import org.nuclos.api.businessobject.Dependent;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.businessobject.Process;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.businessobject.facade.LogicalDeletable;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.businessobject.facade.Stateful;
import org.nuclos.server.nbo.AbstractBusinessObject;
import org.nuclos.system.*;

/**
 * BusinessObject: Test Rules
 *<br>
 *<br>Nuclet: nuclet.test.rules
 *<br>DB-Name: W81Y_TESTRULES
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: true
 **/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class TestRules extends AbstractBusinessObject<java.lang.Long> implements LogicalDeletable<java.lang.Long>, Modifiable<java.lang.Long>, Stateful {

	public static final Dependent<nuclet.test.rules.TestRules> _TestRules =
			new Dependent<>("_TestRules", "null", "TestRules", "vBKyjNyEkmyJW06u2Uqv", "referenz", "k1NH1zhsuuHoHcMY3387", nuclet.test.rules.TestRules.class);

	public static final Dependent<nuclet.test.rules.TestRulesSubformWithSM> _TestRulesSubformWithSM =
			new Dependent<>("_TestRulesSubformWithSM", "null", "TestRulesSubformWithSM", "spwkg6nrk1QO6V1CtdMr", "testrules", "In3rOSbdwyZYgCAmyePl", nuclet.test.rules.TestRulesSubformWithSM.class);


	/**
	 * Attribute: aktualisieren
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisieren
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Aktualisieren = new StringAttribute<>("Aktualisieren", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "7Ghnb4XFDLNbFcyMd19G", java.lang.String.class);


	/**
	 * Attribute: aktualisieren2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisieren2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Aktualisieren2 = new StringAttribute<>("Aktualisieren2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "OftmtriXCkv05icMoYVj", java.lang.String.class);


	/**
	 * Attribute: aktualisieren3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisieren3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Aktualisieren3 = new StringAttribute<>("Aktualisieren3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "lQXNtFoD6YNCvWl5t7Fp", java.lang.String.class);


	/**
	 * Attribute: aktualisierenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisierenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Aktualisierenimanschluss = new StringAttribute<>("Aktualisierenimanschluss", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "UVOvulyckYVW24unJi9E", java.lang.String.class);


	/**
	 * Attribute: aktualisierenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisierenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Aktualisierenimanschluss2 = new StringAttribute<>("Aktualisierenimanschluss2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "1rrpuO0alVhUjTG7BwKK", java.lang.String.class);


	/**
	 * Attribute: aktualisierenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisierenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Aktualisierenimanschluss3 = new StringAttribute<>("Aktualisierenimanschluss3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "zIlqEBiHgg7t6lCwF1ih", java.lang.String.class);


	/**
	 * Attribute: anlegen
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegen
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Anlegen = new StringAttribute<>("Anlegen", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "XUH8DRBktEaVely1zwx9", java.lang.String.class);


	/**
	 * Attribute: anlegen2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegen2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Anlegen2 = new StringAttribute<>("Anlegen2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "4YPYqDpsav0VVbBkN4y3", java.lang.String.class);


	/**
	 * Attribute: anlegen3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegen3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Anlegen3 = new StringAttribute<>("Anlegen3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "uVagkSP9H41FcB2CJgUd", java.lang.String.class);


	/**
	 * Attribute: anlegenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Anlegenimanschluss = new StringAttribute<>("Anlegenimanschluss", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "b2SuRMeU9XmXJBA7DsB3", java.lang.String.class);


	/**
	 * Attribute: anlegenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Anlegenimanschluss2 = new StringAttribute<>("Anlegenimanschluss2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "ippx8zo1YtCmjKyV1KBu", java.lang.String.class);


	/**
	 * Attribute: anlegenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Anlegenimanschluss3 = new StringAttribute<>("Anlegenimanschluss3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vn2xYqwAUEWoF8G5rN1R", java.lang.String.class);


	/**
	 * Attribute: benutzeraktion
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRbenutzeraktion
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Benutzeraktion = new StringAttribute<>("Benutzeraktion", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "3ndmrB5yZMtJOp0YvaKf", java.lang.String.class);


	/**
	 * Attribute: changedAt
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: DATCHANGED
	 *<br>Data type: org.nuclos.common2.InternalTimestamp
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vBKyjNyEkmyJW06u2Uqv3", java.util.Date.class);


	/**
	 * Attribute: changedBy
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRCHANGED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vBKyjNyEkmyJW06u2Uqv4", java.lang.String.class);


	/**
	 * Attribute: createdAt
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: DATCREATED
	 *<br>Data type: org.nuclos.common2.InternalTimestamp
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vBKyjNyEkmyJW06u2Uqv1", java.util.Date.class);


	/**
	 * Attribute: createdBy
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRCREATED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vBKyjNyEkmyJW06u2Uqv2", java.lang.String.class);


	/**
	 * Attribute: drucken
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdrucken
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Drucken = new StringAttribute<>("Drucken", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "coTYyY8F9NgFipwp0JtL", java.lang.String.class);


	/**
	 * Attribute: drucken2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdrucken2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Drucken2 = new StringAttribute<>("Drucken2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "Q0Ffrk8BxIrrSyd1zTEU", java.lang.String.class);


	/**
	 * Attribute: drucken3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdrucken3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Drucken3 = new StringAttribute<>("Drucken3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vMLhgLh4nSUsM7LQbwx3", java.lang.String.class);


	/**
	 * Attribute: druckenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdruckenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Druckenimanschluss = new StringAttribute<>("Druckenimanschluss", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "o5q9DbIBED3yaVvlo2GC", java.lang.String.class);


	/**
	 * Attribute: druckenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdruckenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Druckenimanschluss2 = new StringAttribute<>("Druckenimanschluss2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "zWq12CV7SyGUgxmzAGX5", java.lang.String.class);


	/**
	 * Attribute: druckenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdruckenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Druckenimanschluss3 = new StringAttribute<>("Druckenimanschluss3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "RVueuzQMiGCCSBr81mOd", java.lang.String.class);


	/**
	 * Attribute: primaryKey
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: INTID
	 *<br>Data type: java.lang.Long
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 20
	 *<br>Precision: null
	 **/
	public static final PrimaryKeyAttribute<java.lang.Long> Id =
			new PrimaryKeyAttribute<>("Id", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vBKyjNyEkmyJW06u2Uqv0", java.lang.Long.class);


	/**
	 * Attribute: inputrequireddelete
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequireddelete
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Inputrequireddelete = new StringAttribute<>("Inputrequireddelete", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "jAC5YiglVoFkIlm6Tdc3", java.lang.String.class);


	/**
	 * Attribute: inputrequireddeletefinal
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequireddeletefinal
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Inputrequireddeletefinal = new StringAttribute<>("Inputrequireddeletefinal", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "aTPBADRViCvV5otZhGOE", java.lang.String.class);


	/**
	 * Attribute: inputrequiredexceptions
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredexceptions
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Inputrequiredexceptions = new StringAttribute<>("Inputrequiredexceptions", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "RJv63kosFw172jAq9bik", java.lang.String.class);


	/**
	 * Attribute: inputrequiredgeneratefinal
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredgeneratefinal
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Inputrequiredgeneratefinal = new StringAttribute<>("Inputrequiredgeneratefinal", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "TItEiK5AO8cY1kMxS9MJ", java.lang.String.class);


	/**
	 * Attribute: inputrequiredgeneraterule
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredgeneraterule
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Inputrequiredgeneraterule = new StringAttribute<>("Inputrequiredgeneraterule", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "ovI6Jl5MyvrSJ7huh9bY", java.lang.String.class);


	/**
	 * Attribute: inputrequiredstatechange
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredstatechange
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Inputrequiredstatechange = new StringAttribute<>("Inputrequiredstatechange", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "sPJNRelNvH6T9ors7HjY", java.lang.String.class);


	/**
	 * Attribute: inputrequiredstatechangefin
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredstatechangefin
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Inputrequiredstatechangefin = new StringAttribute<>("Inputrequiredstatechangefin", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "DpHZzlsB1XV213K3deWy", java.lang.String.class);


	/**
	 * Attribute: java8
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRjava8
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Java8 = new StringAttribute<>("Java8", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "aHKbqhCRoGsPbVrFg1Ay", java.lang.String.class);


	/**
	 * Attribute: job
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRjob
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Job = new StringAttribute<>("Job", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "ei3YfMOsMasWUCh22rpx", java.lang.String.class);


	/**
	 * Attribute: loeschen
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschen
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Loeschen = new StringAttribute<>("Loeschen", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "s1OHzidd0UaHi23i2mjZ", java.lang.String.class);


	/**
	 * Attribute: loeschen2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschen2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Loeschen2 = new StringAttribute<>("Loeschen2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "XRrO1gY54tt5g2p4lFR8", java.lang.String.class);


	/**
	 * Attribute: loeschen3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschen3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Loeschen3 = new StringAttribute<>("Loeschen3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "UcHIbL4AfivzztO6xzJ8", java.lang.String.class);


	/**
	 * Attribute: loeschenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Loeschenimanschluss = new StringAttribute<>("Loeschenimanschluss", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "hVvGKbPkzj5zfPFOCKzi", java.lang.String.class);


	/**
	 * Attribute: loeschenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Loeschenimanschluss2 = new StringAttribute<>("Loeschenimanschluss2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "PXDfT3tbhLkXXCRqK11W", java.lang.String.class);


	/**
	 * Attribute: loeschenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Loeschenimanschluss3 = new StringAttribute<>("Loeschenimanschluss3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "xnM4ntoQbOzkV7pzeXqD", java.lang.String.class);


	/**
	 * Attribute: name
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRname
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "ILlTEs84xff3uKSMxPt4", java.lang.String.class);


	/**
	 * Attribute: nuclosDeleted
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: BLNNUCLOSDELETED
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final Attribute<java.lang.Boolean> NuclosLogicalDeleted =
			new Attribute<>("NuclosLogicalDeleted", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vBKyjNyEkmyJW06u2Uqv12", java.lang.Boolean.class);


	/**
	 * Attribute: nuclosProcess
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final ForeignKeyAttribute<org.nuclos.api.UID> NuclosProcessId =
			new ForeignKeyAttribute<>("NuclosProcessId", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vBKyjNyEkmyJW06u2Uqv10", org.nuclos.api.UID.class);


	/**
	 * Attribute: nuclosState
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_NUCLOSSTATE
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final ForeignKeyAttribute<org.nuclos.api.UID> NuclosStateId =
			new ForeignKeyAttribute<>("NuclosStateId", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vBKyjNyEkmyJW06u2Uqv6", org.nuclos.api.UID.class);


	/**
	 * Attribute: nuclosSystemId
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRNUCLOSSYSTEMID
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> NuclosSystemIdentifier = new StringAttribute<>("NuclosSystemIdentifier", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "vBKyjNyEkmyJW06u2Uqv9", java.lang.String.class);


	/**
	 * Attribute: objektgenerator
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgenerator
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Objektgenerator = new StringAttribute<>("Objektgenerator", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "DgaMV1Dg89wEm9lEtXaW", java.lang.String.class);


	/**
	 * Attribute: objektgenerator2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgenerator2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Objektgenerator2 = new StringAttribute<>("Objektgenerator2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "cDgwzXGKERHVqiVfQwwm", java.lang.String.class);


	/**
	 * Attribute: objektgenerator3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgenerator3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Objektgenerator3 = new StringAttribute<>("Objektgenerator3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "xgAYwak6uGDKz6DHWwbX", java.lang.String.class);


	/**
	 * Attribute: objektgeneratorimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgeneratorimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Objektgeneratorimanschluss = new StringAttribute<>("Objektgeneratorimanschluss", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "ZwHJMk9XVphVza2RPpqN", java.lang.String.class);


	/**
	 * Attribute: objektgeneratorimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgeneratorimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Objektgeneratorimanschluss2 = new StringAttribute<>("Objektgeneratorimanschluss2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "BGqXZuqNChreqHtHBSGN", java.lang.String.class);


	/**
	 * Attribute: objektgeneratorimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgeneratorimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Objektgeneratorimanschluss3 = new StringAttribute<>("Objektgeneratorimanschluss3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "prMKeDpXKvyGiNnblGZR", java.lang.String.class);


	/**
	 * Attribute: referenz
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_STRreferenz
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final ForeignKeyAttribute<java.lang.Long> ReferenzId =
			new ForeignKeyAttribute<>("ReferenzId", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "k1NH1zhsuuHoHcMY3387", java.lang.Long.class);


	/**
	 * Attribute: regelnausextension
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: BLNregelnausextension
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 1
	 *<br>Precision: null
	 **/
	public static final Attribute<java.lang.Boolean> Regelnausextension =
			new Attribute<>("Regelnausextension", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "Opgg263Y7D51Zl7egIHy", java.lang.Boolean.class);


	/**
	 * Attribute: regelndirektamuf
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRregelndirektamuf
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Regelndirektamuf = new StringAttribute<>("Regelndirektamuf", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "oTkTS69V3l6pGpSo21zS", java.lang.String.class);


	/**
	 * Attribute: statusabhaengigespflichtf
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatusabhaengigespflichtf
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Statusabhaengigespflichtf = new StringAttribute<>("Statusabhaengigespflichtf", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "pwXJrpD2tuByNoptAuOQ", java.lang.String.class);


	/**
	 * Attribute: statuswechsel
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechsel
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Statuswechsel = new StringAttribute<>("Statuswechsel", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "XSWkUV13DwFK1ocRwKWZ", java.lang.String.class);


	/**
	 * Attribute: statuswechsel2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechsel2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Statuswechsel2 = new StringAttribute<>("Statuswechsel2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "RsrmTSIC6jT4oFdjkbgO", java.lang.String.class);


	/**
	 * Attribute: statuswechsel3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechsel3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Statuswechsel3 = new StringAttribute<>("Statuswechsel3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "dEc3rV6oSpgY5pePbZjf", java.lang.String.class);


	/**
	 * Attribute: statuswechselautomatisch
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselautomatisch
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Statuswechselautomatisch = new StringAttribute<>("Statuswechselautomatisch", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "GGlydKmMXJ5q6lmtZGVK", java.lang.String.class);


	/**
	 * Attribute: statuswechselimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Statuswechselimanschluss = new StringAttribute<>("Statuswechselimanschluss", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "hhQKSa6x88a6yIromnD1", java.lang.String.class);


	/**
	 * Attribute: statuswechselimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Statuswechselimanschluss2 = new StringAttribute<>("Statuswechselimanschluss2", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "Zgg1yykQ9MA9H4HREsAa", java.lang.String.class);


	/**
	 * Attribute: statuswechselimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Statuswechselimanschluss3 = new StringAttribute<>("Statuswechselimanschluss3", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "ldSbfTBKkBrJ1wXNRfro", java.lang.String.class);


	/**
	 * Attribute: zahl
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: INTzahl
	 *<br>Data type: java.lang.Integer
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 9
	 *<br>Precision: null
	 **/
	public static final NumericAttribute<java.lang.Integer> Zahl =
			new NumericAttribute<>("Zahl", "nuclet.test.rules", "vBKyjNyEkmyJW06u2Uqv", "7tzyTfDWqtPOSVQ1U0bS", java.lang.Integer.class);


	public TestRules() {
		super("vBKyjNyEkmyJW06u2Uqv");
		setNuclosLogicalDeleted(java.lang.Boolean.FALSE);
		setRegelnausextension(java.lang.Boolean.FALSE);
	}
	/**
	 * Change Status of this BO. Use this instead of StateModelProvider
	 */
	public void changeStatus(org.nuclos.api.statemodel.State status) throws org.nuclos.api.exception.BusinessException {
		super.changeStatus(status);
	}
	/**
	 * This method compares the current BusinessObject with an other BusinessObject.
	 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
	 **/
	public boolean compare(TestRules boToCompareWith) {
		return super.compare(boToCompareWith);
	}
	/**
	 * This method creates a copy of the current BusinessObject
	 * and resets it by removing the primary key and setting the state flag to 'new'
	 **/
	public TestRules copy() {
		return super.copy(TestRules.class);
	}
	/**
	 * Delete this BO. Use this instead of BusinessObjectProvider
	 */
	public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
	}
	/**
	 * Static Delete for an Id
	 */
	public static void delete(java.lang.Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("vBKyjNyEkmyJW06u2Uqv"), id);
	}


	/**
	 * Remove-Method for attribute: documentfile
	 *<br>
	 *<br>Entity: nuclos_generalsearchdocument
	 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
	 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
	 *<br>Reference entity: nuclos_documentfile
	 *<br>Reference field: filename
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void deleteAttachment(org.nuclos.api.common.NuclosFile pNuclosFile) {
		super.deleteGenericObjectDocumentAttachment(pNuclosFile);
	}


	/**
	 * Delete-Method for attribute: referenz
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_STRreferenz
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void deleteTestRules(nuclet.test.rules.TestRules pTestRules) {
		deleteDependent(_TestRules, pTestRules);
	}


	/**
	 * Delete-Method for attribute: testrules
	 *<br>
	 *<br>Entity: Test Rules SubformWithSM
	 *<br>DB-Name: STRVALUE_STRtestrules
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void deleteTestRulesSubformWithSM(nuclet.test.rules.TestRulesSubformWithSM pTestRulesSubformWithSM) {
		deleteDependent(_TestRulesSubformWithSM, pTestRulesSubformWithSM);
	}
	/**
	 * Static Get by Id
	 */
	public static TestRules get(java.lang.Long id) {
		return get(TestRules.class, id);
	}


	/**
	 * Getter-Method for attribute: aktualisieren
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisieren
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAktualisieren() {
		return getField("7Ghnb4XFDLNbFcyMd19G", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: aktualisieren2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisieren2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAktualisieren2() {
		return getField("OftmtriXCkv05icMoYVj", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: aktualisieren3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisieren3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAktualisieren3() {
		return getField("lQXNtFoD6YNCvWl5t7Fp", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: aktualisierenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisierenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAktualisierenimanschluss() {
		return getField("UVOvulyckYVW24unJi9E", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: aktualisierenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisierenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAktualisierenimanschluss2() {
		return getField("1rrpuO0alVhUjTG7BwKK", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: aktualisierenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisierenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAktualisierenimanschluss3() {
		return getField("zIlqEBiHgg7t6lCwF1ih", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: anlegen
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegen
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAnlegen() {
		return getField("XUH8DRBktEaVely1zwx9", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: anlegen2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegen2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAnlegen2() {
		return getField("4YPYqDpsav0VVbBkN4y3", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: anlegen3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegen3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAnlegen3() {
		return getField("uVagkSP9H41FcB2CJgUd", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: anlegenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAnlegenimanschluss() {
		return getField("b2SuRMeU9XmXJBA7DsB3", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: anlegenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAnlegenimanschluss2() {
		return getField("ippx8zo1YtCmjKyV1KBu", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: anlegenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getAnlegenimanschluss3() {
		return getField("vn2xYqwAUEWoF8G5rN1R", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: documentfile
	 *<br>
	 *<br>Entity: nuclos_generalsearchdocument
	 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
	 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
	 *<br>Reference entity: nuclos_documentfile
	 *<br>Reference field: filename
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public List<org.nuclos.api.common.NuclosFile> getAttachments() {
		return super.getAttachments();
	}


	/**
	 * Getter-Method for attribute: benutzeraktion
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRbenutzeraktion
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getBenutzeraktion() {
		return getField("3ndmrB5yZMtJOp0YvaKf", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: changedAt
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: DATCHANGED
	 *<br>Data type: org.nuclos.common2.InternalTimestamp
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.util.Date getChangedAt() {
		return getField("vBKyjNyEkmyJW06u2Uqv3", java.util.Date.class);
	}


	/**
	 * Getter-Method for attribute: changedBy
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRCHANGED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getChangedBy() {
		return getField("vBKyjNyEkmyJW06u2Uqv4", java.lang.String.class);
	}

	/**
	 * Setter-Method for attribute: changedBy
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRCHANGED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setChangedBy(String username) {
		setField("vBKyjNyEkmyJW06u2Uqv4", username);
	}

	/**
	 * Getter-Method for attribute: createdAt
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: DATCREATED
	 *<br>Data type: org.nuclos.common2.InternalTimestamp
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.util.Date getCreatedAt() {
		return getField("vBKyjNyEkmyJW06u2Uqv1", java.util.Date.class);
	}


	/**
	 * Getter-Method for attribute: createdBy
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRCREATED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getCreatedBy() {
		return getField("vBKyjNyEkmyJW06u2Uqv2", java.lang.String.class);
	}

	/**
	 * Setter-Method for attribute: createdBy
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRCREATED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setCreatedBy(String username) {
		setField("vBKyjNyEkmyJW06u2Uqv2", username);
	}

	/**
	 * Getter-Method for attribute: drucken
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdrucken
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getDrucken() {
		return getField("coTYyY8F9NgFipwp0JtL", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: drucken2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdrucken2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getDrucken2() {
		return getField("Q0Ffrk8BxIrrSyd1zTEU", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: drucken3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdrucken3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getDrucken3() {
		return getField("vMLhgLh4nSUsM7LQbwx3", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: druckenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdruckenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getDruckenimanschluss() {
		return getField("o5q9DbIBED3yaVvlo2GC", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: druckenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdruckenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getDruckenimanschluss2() {
		return getField("zWq12CV7SyGUgxmzAGX5", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: druckenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdruckenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getDruckenimanschluss3() {
		return getField("RVueuzQMiGCCSBr81mOd", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: entity
	 *<br>
	 *<br>Entity: nuclos_entity
	 *<br>DB-Name: STRENTITY
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public org.nuclos.api.UID getEntityUid() {
		return new org.nuclos.common.UID("vBKyjNyEkmyJW06u2Uqv");
	}


	/**
	 * Getter-Method for attribute: inputrequireddelete
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequireddelete
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getInputrequireddelete() {
		return getField("jAC5YiglVoFkIlm6Tdc3", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: inputrequireddeletefinal
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequireddeletefinal
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getInputrequireddeletefinal() {
		return getField("aTPBADRViCvV5otZhGOE", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: inputrequiredexceptions
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredexceptions
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getInputrequiredexceptions() {
		return getField("RJv63kosFw172jAq9bik", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: inputrequiredgeneratefinal
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredgeneratefinal
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getInputrequiredgeneratefinal() {
		return getField("TItEiK5AO8cY1kMxS9MJ", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: inputrequiredgeneraterule
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredgeneraterule
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getInputrequiredgeneraterule() {
		return getField("ovI6Jl5MyvrSJ7huh9bY", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: inputrequiredstatechange
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredstatechange
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getInputrequiredstatechange() {
		return getField("sPJNRelNvH6T9ors7HjY", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: inputrequiredstatechangefin
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredstatechangefin
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getInputrequiredstatechangefin() {
		return getField("DpHZzlsB1XV213K3deWy", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: java8
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRjava8
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getJava8() {
		return getField("aHKbqhCRoGsPbVrFg1Ay", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: job
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRjob
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getJob() {
		return getField("ei3YfMOsMasWUCh22rpx", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: loeschen
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschen
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getLoeschen() {
		return getField("s1OHzidd0UaHi23i2mjZ", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: loeschen2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschen2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getLoeschen2() {
		return getField("XRrO1gY54tt5g2p4lFR8", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: loeschen3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschen3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getLoeschen3() {
		return getField("UcHIbL4AfivzztO6xzJ8", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: loeschenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getLoeschenimanschluss() {
		return getField("hVvGKbPkzj5zfPFOCKzi", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: loeschenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getLoeschenimanschluss2() {
		return getField("PXDfT3tbhLkXXCRqK11W", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: loeschenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getLoeschenimanschluss3() {
		return getField("xnM4ntoQbOzkV7pzeXqD", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: name
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRname
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getName() {
		return getField("ILlTEs84xff3uKSMxPt4", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: nuclosDeleted
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: BLNNUCLOSDELETED
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.lang.Boolean getNuclosLogicalDeleted() {
		return getField("vBKyjNyEkmyJW06u2Uqv12", java.lang.Boolean.class);
	}


	/**
	 * Getter-Method for attribute: nuclosProcess
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: nuclos_process
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getNuclosProcess() {
		return getField("vBKyjNyEkmyJW06u2Uqv10", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: nuclosProcess
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: nuclos_process
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public org.nuclos.api.UID getNuclosProcessId() {
		return getFieldUid("vBKyjNyEkmyJW06u2Uqv10");
	}


	/**
	 * Getter-Method for attribute: nuclosState
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_NUCLOSSTATE
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: nuclos_state
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getNuclosState() {
		return getField("vBKyjNyEkmyJW06u2Uqv6", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: nuclosState
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_NUCLOSSTATE
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: nuclos_state
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public org.nuclos.api.UID getNuclosStateId() {
		return getFieldUid("vBKyjNyEkmyJW06u2Uqv6");
	}


	/**
	 * Getter-Method for attribute: nuclosStateNumber
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: INTVALUE_NUCLOSSTATE
	 *<br>Data type: java.lang.Integer
	 *<br>Reference entity: nuclos_state
	 *<br>Reference field: numeral
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 3
	 *<br>Precision: null
	 **/
	public java.lang.Integer getNuclosStateNumber() {
		return getField("vBKyjNyEkmyJW06u2Uqv7", java.lang.Integer.class);
	}


	/**
	 * Getter-Method for attribute: nuclosSystemId
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRNUCLOSSYSTEMID
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getNuclosSystemIdentifier() {
		return getField("vBKyjNyEkmyJW06u2Uqv9", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: objektgenerator
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgenerator
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getObjektgenerator() {
		return getField("DgaMV1Dg89wEm9lEtXaW", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: objektgenerator2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgenerator2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getObjektgenerator2() {
		return getField("cDgwzXGKERHVqiVfQwwm", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: objektgenerator3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgenerator3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getObjektgenerator3() {
		return getField("xgAYwak6uGDKz6DHWwbX", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: objektgeneratorimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgeneratorimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getObjektgeneratorimanschluss() {
		return getField("ZwHJMk9XVphVza2RPpqN", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: objektgeneratorimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgeneratorimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getObjektgeneratorimanschluss2() {
		return getField("BGqXZuqNChreqHtHBSGN", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: objektgeneratorimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgeneratorimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getObjektgeneratorimanschluss3() {
		return getField("prMKeDpXKvyGiNnblGZR", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: referenz
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_STRreferenz
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getReferenz() {
		return getField("k1NH1zhsuuHoHcMY3387", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: referenz
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_STRreferenz
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public <T extends AbstractBusinessObject> T getReferenzBO(Class<T> boClass) {
		return getReferencedBO(boClass, getFieldId("k1NH1zhsuuHoHcMY3387"), "k1NH1zhsuuHoHcMY3387", "vBKyjNyEkmyJW06u2Uqv");
	}


	/**
	 * Getter-Method for attribute: referenz
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_STRreferenz
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public nuclet.test.rules.TestRules getReferenzBO() {
		return getReferencedBO(nuclet.test.rules.TestRules.class, getFieldId("k1NH1zhsuuHoHcMY3387"), "k1NH1zhsuuHoHcMY3387", "vBKyjNyEkmyJW06u2Uqv");
	}


	/**
	 * Getter-Method for attribute: referenz
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_STRreferenz
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.Long getReferenzId() {
		return getFieldId("k1NH1zhsuuHoHcMY3387");
	}


	/**
	 * Getter-Method for attribute: regelnausextension
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: BLNregelnausextension
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 1
	 *<br>Precision: null
	 **/
	public java.lang.Boolean getRegelnausextension() {
		return getField("Opgg263Y7D51Zl7egIHy", java.lang.Boolean.class);
	}


	/**
	 * Getter-Method for attribute: regelndirektamuf
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRregelndirektamuf
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getRegelndirektamuf() {
		return getField("oTkTS69V3l6pGpSo21zS", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: statusabhaengigespflichtf
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatusabhaengigespflichtf
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getStatusabhaengigespflichtf() {
		return getField("pwXJrpD2tuByNoptAuOQ", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: statuswechsel
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechsel
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getStatuswechsel() {
		return getField("XSWkUV13DwFK1ocRwKWZ", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: statuswechsel2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechsel2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getStatuswechsel2() {
		return getField("RsrmTSIC6jT4oFdjkbgO", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: statuswechsel3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechsel3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getStatuswechsel3() {
		return getField("dEc3rV6oSpgY5pePbZjf", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: statuswechselautomatisch
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselautomatisch
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getStatuswechselautomatisch() {
		return getField("GGlydKmMXJ5q6lmtZGVK", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: statuswechselimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getStatuswechselimanschluss() {
		return getField("hhQKSa6x88a6yIromnD1", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: statuswechselimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getStatuswechselimanschluss2() {
		return getField("Zgg1yykQ9MA9H4HREsAa", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: statuswechselimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getStatuswechselimanschluss3() {
		return getField("ldSbfTBKkBrJ1wXNRfro", java.lang.String.class);
	}


	/**
	 * Getter-Method for attribute: referenz
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_STRreferenz
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public List<nuclet.test.rules.TestRules> getTestRules(Flag... flags) {
		return getDependents(_TestRules, flags);
	}


	/**
	 * Getter-Method for attribute: testrules
	 *<br>
	 *<br>Entity: Test Rules SubformWithSM
	 *<br>DB-Name: STRVALUE_STRtestrules
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public List<nuclet.test.rules.TestRulesSubformWithSM> getTestRulesSubformWithSM(Flag... flags) {
		return getDependents(_TestRulesSubformWithSM, flags);
	}


	/**
	 * Getter-Method for attribute: zahl
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: INTzahl
	 *<br>Data type: java.lang.Integer
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 9
	 *<br>Precision: null
	 **/
	public java.lang.Integer getZahl() {
		return getField("7tzyTfDWqtPOSVQ1U0bS", java.lang.Integer.class);
	}


	/**
	 * Insert-Method for attribute: documentfile
	 *<br>
	 *<br>Entity: nuclos_generalsearchdocument
	 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
	 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
	 *<br>Reference entity: nuclos_documentfile
	 *<br>Reference field: filename
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void insertAttachment(org.nuclos.api.common.NuclosFile pNuclosFile, java.lang.String pComment) {
		insertGenericObjectDocumentAttachment(pNuclosFile,pComment);
	}


	/**
	 * Insert-Method for attribute: referenz
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_STRreferenz
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void insertTestRules(nuclet.test.rules.TestRules pTestRules) {
		insertDependent(_TestRules, pTestRules);
	}


	/**
	 * Insert-Method for attribute: testrules
	 *<br>
	 *<br>Entity: Test Rules SubformWithSM
	 *<br>DB-Name: STRVALUE_STRtestrules
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void insertTestRulesSubformWithSM(nuclet.test.rules.TestRulesSubformWithSM pTestRulesSubformWithSM) {
		insertDependent(_TestRulesSubformWithSM, pTestRulesSubformWithSM);
	}
	/**
	 * Refresh this BO with data from the db layer, interface or similar
	 */
	public void refresh() throws org.nuclos.api.exception.BusinessException {
		if (this.getId() == null) {
			throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
		}
		super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
	}
	/**
	 * Save this BO. Use this instead of BusinessObjectProvider
	 */
	public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
	}


	/**
	 * Setter-Method for attribute: aktualisieren
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisieren
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAktualisieren(java.lang.String pAktualisieren) {
		setField("7Ghnb4XFDLNbFcyMd19G", pAktualisieren);
	}


	/**
	 * Setter-Method for attribute: aktualisieren2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisieren2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAktualisieren2(java.lang.String pAktualisieren2) {
		setField("OftmtriXCkv05icMoYVj", pAktualisieren2);
	}


	/**
	 * Setter-Method for attribute: aktualisieren3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisieren3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAktualisieren3(java.lang.String pAktualisieren3) {
		setField("lQXNtFoD6YNCvWl5t7Fp", pAktualisieren3);
	}


	/**
	 * Setter-Method for attribute: aktualisierenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisierenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAktualisierenimanschluss(java.lang.String pAktualisierenimanschluss) {
		setField("UVOvulyckYVW24unJi9E", pAktualisierenimanschluss);
	}


	/**
	 * Setter-Method for attribute: aktualisierenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisierenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAktualisierenimanschluss2(java.lang.String pAktualisierenimanschluss2) {
		setField("1rrpuO0alVhUjTG7BwKK", pAktualisierenimanschluss2);
	}


	/**
	 * Setter-Method for attribute: aktualisierenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRaktualisierenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAktualisierenimanschluss3(java.lang.String pAktualisierenimanschluss3) {
		setField("zIlqEBiHgg7t6lCwF1ih", pAktualisierenimanschluss3);
	}


	/**
	 * Setter-Method for attribute: anlegen
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegen
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAnlegen(java.lang.String pAnlegen) {
		setField("XUH8DRBktEaVely1zwx9", pAnlegen);
	}


	/**
	 * Setter-Method for attribute: anlegen2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegen2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAnlegen2(java.lang.String pAnlegen2) {
		setField("4YPYqDpsav0VVbBkN4y3", pAnlegen2);
	}


	/**
	 * Setter-Method for attribute: anlegen3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegen3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAnlegen3(java.lang.String pAnlegen3) {
		setField("uVagkSP9H41FcB2CJgUd", pAnlegen3);
	}


	/**
	 * Setter-Method for attribute: anlegenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAnlegenimanschluss(java.lang.String pAnlegenimanschluss) {
		setField("b2SuRMeU9XmXJBA7DsB3", pAnlegenimanschluss);
	}


	/**
	 * Setter-Method for attribute: anlegenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAnlegenimanschluss2(java.lang.String pAnlegenimanschluss2) {
		setField("ippx8zo1YtCmjKyV1KBu", pAnlegenimanschluss2);
	}


	/**
	 * Setter-Method for attribute: anlegenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRanlegenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setAnlegenimanschluss3(java.lang.String pAnlegenimanschluss3) {
		setField("vn2xYqwAUEWoF8G5rN1R", pAnlegenimanschluss3);
	}


	/**
	 * Setter-Method for attribute: benutzeraktion
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRbenutzeraktion
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setBenutzeraktion(java.lang.String pBenutzeraktion) {
		setField("3ndmrB5yZMtJOp0YvaKf", pBenutzeraktion);
	}


	/**
	 * Setter-Method for attribute: drucken
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdrucken
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setDrucken(java.lang.String pDrucken) {
		setField("coTYyY8F9NgFipwp0JtL", pDrucken);
	}


	/**
	 * Setter-Method for attribute: drucken2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdrucken2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setDrucken2(java.lang.String pDrucken2) {
		setField("Q0Ffrk8BxIrrSyd1zTEU", pDrucken2);
	}


	/**
	 * Setter-Method for attribute: drucken3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdrucken3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setDrucken3(java.lang.String pDrucken3) {
		setField("vMLhgLh4nSUsM7LQbwx3", pDrucken3);
	}


	/**
	 * Setter-Method for attribute: druckenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdruckenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setDruckenimanschluss(java.lang.String pDruckenimanschluss) {
		setField("o5q9DbIBED3yaVvlo2GC", pDruckenimanschluss);
	}


	/**
	 * Setter-Method for attribute: druckenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdruckenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setDruckenimanschluss2(java.lang.String pDruckenimanschluss2) {
		setField("zWq12CV7SyGUgxmzAGX5", pDruckenimanschluss2);
	}


	/**
	 * Setter-Method for attribute: druckenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRdruckenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setDruckenimanschluss3(java.lang.String pDruckenimanschluss3) {
		setField("RVueuzQMiGCCSBr81mOd", pDruckenimanschluss3);
	}


	/**
	 * Setter-Method for attribute: inputrequireddelete
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequireddelete
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setInputrequireddelete(java.lang.String pInputrequireddelete) {
		setField("jAC5YiglVoFkIlm6Tdc3", pInputrequireddelete);
	}


	/**
	 * Setter-Method for attribute: inputrequireddeletefinal
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequireddeletefinal
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setInputrequireddeletefinal(java.lang.String pInputrequireddeletefinal) {
		setField("aTPBADRViCvV5otZhGOE", pInputrequireddeletefinal);
	}


	/**
	 * Setter-Method for attribute: inputrequiredexceptions
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredexceptions
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setInputrequiredexceptions(java.lang.String pInputrequiredexceptions) {
		setField("RJv63kosFw172jAq9bik", pInputrequiredexceptions);
	}


	/**
	 * Setter-Method for attribute: inputrequiredgeneratefinal
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredgeneratefinal
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setInputrequiredgeneratefinal(java.lang.String pInputrequiredgeneratefinal) {
		setField("TItEiK5AO8cY1kMxS9MJ", pInputrequiredgeneratefinal);
	}


	/**
	 * Setter-Method for attribute: inputrequiredgeneraterule
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredgeneraterule
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setInputrequiredgeneraterule(java.lang.String pInputrequiredgeneraterule) {
		setField("ovI6Jl5MyvrSJ7huh9bY", pInputrequiredgeneraterule);
	}


	/**
	 * Setter-Method for attribute: inputrequiredstatechange
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredstatechange
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setInputrequiredstatechange(java.lang.String pInputrequiredstatechange) {
		setField("sPJNRelNvH6T9ors7HjY", pInputrequiredstatechange);
	}


	/**
	 * Setter-Method for attribute: inputrequiredstatechangefin
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRinputrequiredstatechangefin
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setInputrequiredstatechangefin(java.lang.String pInputrequiredstatechangefin) {
		setField("DpHZzlsB1XV213K3deWy", pInputrequiredstatechangefin);
	}


	/**
	 * Setter-Method for attribute: java8
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRjava8
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setJava8(java.lang.String pJava8) {
		setField("aHKbqhCRoGsPbVrFg1Ay", pJava8);
	}


	/**
	 * Setter-Method for attribute: job
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRjob
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setJob(java.lang.String pJob) {
		setField("ei3YfMOsMasWUCh22rpx", pJob);
	}


	/**
	 * Setter-Method for attribute: loeschen
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschen
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setLoeschen(java.lang.String pLoeschen) {
		setField("s1OHzidd0UaHi23i2mjZ", pLoeschen);
	}


	/**
	 * Setter-Method for attribute: loeschen2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschen2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setLoeschen2(java.lang.String pLoeschen2) {
		setField("XRrO1gY54tt5g2p4lFR8", pLoeschen2);
	}


	/**
	 * Setter-Method for attribute: loeschen3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschen3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setLoeschen3(java.lang.String pLoeschen3) {
		setField("UcHIbL4AfivzztO6xzJ8", pLoeschen3);
	}


	/**
	 * Setter-Method for attribute: loeschenimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschenimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setLoeschenimanschluss(java.lang.String pLoeschenimanschluss) {
		setField("hVvGKbPkzj5zfPFOCKzi", pLoeschenimanschluss);
	}


	/**
	 * Setter-Method for attribute: loeschenimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschenimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setLoeschenimanschluss2(java.lang.String pLoeschenimanschluss2) {
		setField("PXDfT3tbhLkXXCRqK11W", pLoeschenimanschluss2);
	}


	/**
	 * Setter-Method for attribute: loeschenimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRloeschenimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setLoeschenimanschluss3(java.lang.String pLoeschenimanschluss3) {
		setField("xnM4ntoQbOzkV7pzeXqD", pLoeschenimanschluss3);
	}


	/**
	 * Setter-Method for attribute: name
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRname
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setName(java.lang.String pName) {
		setField("ILlTEs84xff3uKSMxPt4", pName);
	}


	/**
	 * Setter-Method for attribute: nuclosDeleted
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: BLNNUCLOSDELETED
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void setNuclosLogicalDeleted(java.lang.Boolean pNuclosLogicalDeleted) {
		setField("vBKyjNyEkmyJW06u2Uqv12", pNuclosLogicalDeleted);
	}


	/**
	 * Getter-Method for attribute: nuclosProcess
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRUID_NUCLOSPROCESS
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Reference entity: nuclos_process
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 128
	 *<br>Precision: null
	 **/
	public void setNuclosProcess(Process<nuclet.test.rules.TestRules> pProcess) {
		setFieldId("vBKyjNyEkmyJW06u2Uqv10", pProcess.getId());
	}


	/**
	 * Setter-Method for attribute: nuclosProcess
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: nuclos_process
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setNuclosProcessId(org.nuclos.api.UID pNuclosProcessId) {
		setFieldId("vBKyjNyEkmyJW06u2Uqv10", pNuclosProcessId);
	}


	/**
	 * Setter-Method for attribute: nuclosSystemId
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRNUCLOSSYSTEMID
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setNuclosSystemIdentifier(java.lang.String pNuclosSystemIdentifier) {
		setField("vBKyjNyEkmyJW06u2Uqv9", pNuclosSystemIdentifier);
	}


	/**
	 * Setter-Method for attribute: objektgenerator
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgenerator
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setObjektgenerator(java.lang.String pObjektgenerator) {
		setField("DgaMV1Dg89wEm9lEtXaW", pObjektgenerator);
	}


	/**
	 * Setter-Method for attribute: objektgenerator2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgenerator2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setObjektgenerator2(java.lang.String pObjektgenerator2) {
		setField("cDgwzXGKERHVqiVfQwwm", pObjektgenerator2);
	}


	/**
	 * Setter-Method for attribute: objektgenerator3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgenerator3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setObjektgenerator3(java.lang.String pObjektgenerator3) {
		setField("xgAYwak6uGDKz6DHWwbX", pObjektgenerator3);
	}


	/**
	 * Setter-Method for attribute: objektgeneratorimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgeneratorimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setObjektgeneratorimanschluss(java.lang.String pObjektgeneratorimanschluss) {
		setField("ZwHJMk9XVphVza2RPpqN", pObjektgeneratorimanschluss);
	}


	/**
	 * Setter-Method for attribute: objektgeneratorimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgeneratorimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setObjektgeneratorimanschluss2(java.lang.String pObjektgeneratorimanschluss2) {
		setField("BGqXZuqNChreqHtHBSGN", pObjektgeneratorimanschluss2);
	}


	/**
	 * Setter-Method for attribute: objektgeneratorimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRobjektgeneratorimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setObjektgeneratorimanschluss3(java.lang.String pObjektgeneratorimanschluss3) {
		setField("prMKeDpXKvyGiNnblGZR", pObjektgeneratorimanschluss3);
	}


	/**
	 * Setter-Method for attribute: referenz
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRVALUE_STRreferenz
	 *<br>Data type: java.lang.String
	 *<br>Reference entity: Test Rules
	 *<br>Reference field: name
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setReferenzId(java.lang.Long pReferenzId) {
		setFieldId("k1NH1zhsuuHoHcMY3387", pReferenzId);
	}


	/**
	 * Setter-Method for attribute: regelnausextension
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: BLNregelnausextension
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 1
	 *<br>Precision: null
	 **/
	public void setRegelnausextension(java.lang.Boolean pRegelnausextension) {
		setField("Opgg263Y7D51Zl7egIHy", pRegelnausextension);
	}


	/**
	 * Setter-Method for attribute: regelndirektamuf
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRregelndirektamuf
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setRegelndirektamuf(java.lang.String pRegelndirektamuf) {
		setField("oTkTS69V3l6pGpSo21zS", pRegelndirektamuf);
	}


	/**
	 * Setter-Method for attribute: statusabhaengigespflichtf
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatusabhaengigespflichtf
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setStatusabhaengigespflichtf(java.lang.String pStatusabhaengigespflichtf) {
		setField("pwXJrpD2tuByNoptAuOQ", pStatusabhaengigespflichtf);
	}


	/**
	 * Setter-Method for attribute: statuswechsel
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechsel
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setStatuswechsel(java.lang.String pStatuswechsel) {
		setField("XSWkUV13DwFK1ocRwKWZ", pStatuswechsel);
	}


	/**
	 * Setter-Method for attribute: statuswechsel2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechsel2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setStatuswechsel2(java.lang.String pStatuswechsel2) {
		setField("RsrmTSIC6jT4oFdjkbgO", pStatuswechsel2);
	}


	/**
	 * Setter-Method for attribute: statuswechsel3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechsel3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setStatuswechsel3(java.lang.String pStatuswechsel3) {
		setField("dEc3rV6oSpgY5pePbZjf", pStatuswechsel3);
	}


	/**
	 * Setter-Method for attribute: statuswechselautomatisch
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselautomatisch
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setStatuswechselautomatisch(java.lang.String pStatuswechselautomatisch) {
		setField("GGlydKmMXJ5q6lmtZGVK", pStatuswechselautomatisch);
	}


	/**
	 * Setter-Method for attribute: statuswechselimanschluss
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselimanschluss
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setStatuswechselimanschluss(java.lang.String pStatuswechselimanschluss) {
		setField("hhQKSa6x88a6yIromnD1", pStatuswechselimanschluss);
	}


	/**
	 * Setter-Method for attribute: statuswechselimanschluss2
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselimanschluss2
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setStatuswechselimanschluss2(java.lang.String pStatuswechselimanschluss2) {
		setField("Zgg1yykQ9MA9H4HREsAa", pStatuswechselimanschluss2);
	}


	/**
	 * Setter-Method for attribute: statuswechselimanschluss3
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: STRstatuswechselimanschluss3
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setStatuswechselimanschluss3(java.lang.String pStatuswechselimanschluss3) {
		setField("ldSbfTBKkBrJ1wXNRfro", pStatuswechselimanschluss3);
	}


	/**
	 * Setter-Method for attribute: zahl
	 *<br>
	 *<br>Entity: Test Rules
	 *<br>DB-Name: INTzahl
	 *<br>Data type: java.lang.Integer
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 9
	 *<br>Precision: null
	 **/
	public void setZahl(java.lang.Integer pZahl) {
		setField("7tzyTfDWqtPOSVQ1U0bS", pZahl);
	}
}
