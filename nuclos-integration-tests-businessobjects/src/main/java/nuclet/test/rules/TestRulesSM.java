package nuclet.test.rules;

import org.nuclos.common.UID;
import org.nuclos.api.statemodel.State;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.businessobject.facade.thin.Stateful;

@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.statemodel.StatemodelObjectBuilder")
public class TestRulesSM {

public static State State_10 = 
	new State(UID.parseUID("dIoAV30W439fKP9csozl"), "State A", "State A", 10, UID.parseUID("HQXZlQvUujmZDPu5SILF"));
public static State State_20 = 
	new State(UID.parseUID("655rVphGK5e88YeRAtzl"), "State B", "State B", 20, UID.parseUID("HQXZlQvUujmZDPu5SILF"));
public static State State_30 = 
	new State(UID.parseUID("Beij9tbb12Ueg1GLlG8C"), "State C", "State C", 30, UID.parseUID("HQXZlQvUujmZDPu5SILF"));
public static State State_40 = 
	new State(UID.parseUID("gU2GipStRvYSGOXQBgkD"), "Tests InputRequired exceptions on state change and immediately changes back to the previous state.", "State D", 40, UID.parseUID("HQXZlQvUujmZDPu5SILF"));
public static State State_50 = 
	new State(UID.parseUID("JSx5WRr0zq9nbdGjVCMx"), "StateChange not possibile due to missing rule", "Status E", 50, UID.parseUID("HQXZlQvUujmZDPu5SILF"));


  public static Integer getNumeral(org.nuclos.api.UID stateId) throws BusinessException {
    if (stateId == null) {
      throw new IllegalArgumentException("stateId must not be null");
    }

    if (stateId.equals(State_10.getId())) {
      return State_10.getNumeral();
    }
    if (stateId.equals(State_20.getId())) {
      return State_20.getNumeral();
    }
    if (stateId.equals(State_30.getId())) {
      return State_30.getNumeral();
    }
    if (stateId.equals(State_40.getId())) {
      return State_40.getNumeral();
    }
    if (stateId.equals(State_50.getId())) {
      return State_50.getNumeral();
    }
    throw new BusinessException("Status with id " + stateId + " does not exist in this status model");
  }

  public static Integer getNumeral(Stateful statefulBusinessObject) throws BusinessException {
    return getNumeral(statefulBusinessObject.getNuclosStateId());
  }

}