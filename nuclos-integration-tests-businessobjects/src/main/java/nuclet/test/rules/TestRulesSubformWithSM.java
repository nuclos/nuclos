//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package nuclet.test.rules;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.Process; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.LogicalDeletable; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.api.businessobject.facade.Stateful; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.system.*; 

/**
 * BusinessObject: Test Rules SubformWithSM
 *<br>
 *<br>Nuclet: nuclet.test.rules
 *<br>DB-Name: W81Y_TESTRULESSUBFORMWITHSM
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: true
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class TestRulesSubformWithSM extends AbstractBusinessObject<java.lang.Long> implements LogicalDeletable<java.lang.Long>, Modifiable<java.lang.Long>, Stateful {


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<java.lang.Long> Id = 
	new PrimaryKeyAttribute<>("Id", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "spwkg6nrk1QO6V1CtdMr0", java.lang.Long.class);


/**
 * Attribute: keinPflichtfeld
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRkeinPflichtfeld
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> KeinPflichtfeld = new StringAttribute<>("KeinPflichtfeld", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "cgfDnPuam8dysbiKsZMj", java.lang.String.class);


/**
 * Attribute: nuclosDeleted
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: BLNNUCLOSDELETED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> NuclosLogicalDeleted = 
	new Attribute<>("NuclosLogicalDeleted", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "spwkg6nrk1QO6V1CtdMr12", java.lang.Boolean.class);


/**
 * Attribute: testrules
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_STRtestrules
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<java.lang.Long> TestrulesId = 
	new ForeignKeyAttribute<>("TestrulesId", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "In3rOSbdwyZYgCAmyePl", java.lang.Long.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "spwkg6nrk1QO6V1CtdMr1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "spwkg6nrk1QO6V1CtdMr2", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "spwkg6nrk1QO6V1CtdMr3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "spwkg6nrk1QO6V1CtdMr4", java.lang.String.class);


/**
 * Attribute: nuclosState
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_NUCLOSSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.api.UID> NuclosStateId = 
	new ForeignKeyAttribute<>("NuclosStateId", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "spwkg6nrk1QO6V1CtdMr6", org.nuclos.api.UID.class);


/**
 * Attribute: pflichtfeld
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRpflichtfeld
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Pflichtfeld = new StringAttribute<>("Pflichtfeld", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "aHIGfvmYm9OmECDYoDMt", java.lang.String.class);


/**
 * Attribute: nuclosProcess
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.api.UID> NuclosProcessId = 
	new ForeignKeyAttribute<>("NuclosProcessId", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "spwkg6nrk1QO6V1CtdMr10", org.nuclos.api.UID.class);


/**
 * Attribute: nuclosSystemId
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRNUCLOSSYSTEMID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> NuclosSystemIdentifier = new StringAttribute<>("NuclosSystemIdentifier", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "spwkg6nrk1QO6V1CtdMr9", java.lang.String.class);


/**
 * Attribute: janeinfeld
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: BLNjaneinfeld
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Janeinfeld = 
	new Attribute<>("Janeinfeld", "nuclet.test.rules", "spwkg6nrk1QO6V1CtdMr", "JPzkr4qu5tGfIkUG4jQd", java.lang.Boolean.class);


public TestRulesSubformWithSM() {
		super("spwkg6nrk1QO6V1CtdMr");
		setNuclosLogicalDeleted(java.lang.Boolean.FALSE);
		setJaneinfeld(java.lang.Boolean.FALSE);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.api.UID getEntityUid() {
		return new org.nuclos.common.UID("spwkg6nrk1QO6V1CtdMr");
}


/**
 * Getter-Method for attribute: keinPflichtfeld
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRkeinPflichtfeld
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getKeinPflichtfeld() {
		return getField("cgfDnPuam8dysbiKsZMj", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: keinPflichtfeld
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRkeinPflichtfeld
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setKeinPflichtfeld(java.lang.String pKeinPflichtfeld) {
		setField("cgfDnPuam8dysbiKsZMj", pKeinPflichtfeld); 
}


/**
 * Getter-Method for attribute: nuclosDeleted
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: BLNNUCLOSDELETED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getNuclosLogicalDeleted() {
		return getField("spwkg6nrk1QO6V1CtdMr12", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: nuclosDeleted
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: BLNNUCLOSDELETED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNuclosLogicalDeleted(java.lang.Boolean pNuclosLogicalDeleted) {
		setField("spwkg6nrk1QO6V1CtdMr12", pNuclosLogicalDeleted); 
}


/**
 * Getter-Method for attribute: testrules
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_STRtestrules
 *<br>Data type: java.lang.String
 *<br>Reference entity: Test Rules
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getTestrules() {
		return getField("In3rOSbdwyZYgCAmyePl", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: testrules
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_STRtestrules
 *<br>Data type: java.lang.String
 *<br>Reference entity: Test Rules
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.Long getTestrulesId() {
		return getFieldId("In3rOSbdwyZYgCAmyePl");
}


/**
 * Setter-Method for attribute: testrules
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_STRtestrules
 *<br>Data type: java.lang.String
 *<br>Reference entity: Test Rules
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setTestrulesId(java.lang.Long pTestrulesId) {
		setFieldId("In3rOSbdwyZYgCAmyePl", pTestrulesId); 
}


/**
 * Getter-Method for attribute: testrules
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_STRtestrules
 *<br>Data type: java.lang.String
 *<br>Reference entity: Test Rules
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends AbstractBusinessObject> T getTestrulesBO(Class<T> boClass) {
		return getReferencedBO(boClass, getFieldId("In3rOSbdwyZYgCAmyePl"), "In3rOSbdwyZYgCAmyePl", "vBKyjNyEkmyJW06u2Uqv");
}


/**
 * Getter-Method for attribute: testrules
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_STRtestrules
 *<br>Data type: java.lang.String
 *<br>Reference entity: Test Rules
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public nuclet.test.rules.TestRules getTestrulesBO() {
		return getReferencedBO(nuclet.test.rules.TestRules.class, getFieldId("In3rOSbdwyZYgCAmyePl"), "In3rOSbdwyZYgCAmyePl", "vBKyjNyEkmyJW06u2Uqv");
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("spwkg6nrk1QO6V1CtdMr1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("spwkg6nrk1QO6V1CtdMr2", java.lang.String.class); 
}

/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setCreatedBy(String username) {
	setField("spwkg6nrk1QO6V1CtdMr2", username);
}

/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("spwkg6nrk1QO6V1CtdMr3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("spwkg6nrk1QO6V1CtdMr4", java.lang.String.class); 
}

/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setChangedBy(String username) {
	setField("spwkg6nrk1QO6V1CtdMr4", username);
}

/**
 * Getter-Method for attribute: nuclosState
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_NUCLOSSTATE
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_state
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getNuclosState() {
		return getField("spwkg6nrk1QO6V1CtdMr6", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclosState
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_NUCLOSSTATE
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_state
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.api.UID getNuclosStateId() {
		return getFieldUid("spwkg6nrk1QO6V1CtdMr6");
}


/**
 * Getter-Method for attribute: nuclosStateNumber
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: INTVALUE_NUCLOSSTATE
 *<br>Data type: java.lang.Integer
 *<br>Reference entity: nuclos_state
 *<br>Reference field: numeral
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 3
 *<br>Precision: null
**/
public java.lang.Integer getNuclosStateNumber() {
		return getField("spwkg6nrk1QO6V1CtdMr7", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: pflichtfeld
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRpflichtfeld
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getPflichtfeld() {
		return getField("aHIGfvmYm9OmECDYoDMt", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: pflichtfeld
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRpflichtfeld
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setPflichtfeld(java.lang.String pPflichtfeld) {
		setField("aHIGfvmYm9OmECDYoDMt", pPflichtfeld); 
}


/**
 * Getter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getNuclosProcess() {
		return getField("spwkg6nrk1QO6V1CtdMr10", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.api.UID getNuclosProcessId() {
		return getFieldUid("spwkg6nrk1QO6V1CtdMr10");
}


/**
 * Setter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setNuclosProcessId(org.nuclos.api.UID pNuclosProcessId) {
		setFieldId("spwkg6nrk1QO6V1CtdMr10", pNuclosProcessId); 
}


/**
 * Getter-Method for attribute: nuclosSystemId
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRNUCLOSSYSTEMID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getNuclosSystemIdentifier() {
		return getField("spwkg6nrk1QO6V1CtdMr9", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: nuclosSystemId
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRNUCLOSSYSTEMID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setNuclosSystemIdentifier(java.lang.String pNuclosSystemIdentifier) {
		setField("spwkg6nrk1QO6V1CtdMr9", pNuclosSystemIdentifier); 
}


/**
 * Getter-Method for attribute: janeinfeld
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: BLNjaneinfeld
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public java.lang.Boolean getJaneinfeld() {
		return getField("JPzkr4qu5tGfIkUG4jQd", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: janeinfeld
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: BLNjaneinfeld
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public void setJaneinfeld(java.lang.Boolean pJaneinfeld) {
		setField("JPzkr4qu5tGfIkUG4jQd", pJaneinfeld); 
}


/**
 * Insert-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_generalsearchdocument
 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertAttachment(org.nuclos.api.common.NuclosFile pNuclosFile, java.lang.String pComment) {
		insertGenericObjectDocumentAttachment(pNuclosFile,pComment);
}


/**
 * Getter-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_generalsearchdocument
 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.api.common.NuclosFile> getAttachments() {
		return super.getAttachments();
}


/**
 * Remove-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_generalsearchdocument
 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteAttachment(org.nuclos.api.common.NuclosFile pNuclosFile) {
		super.deleteGenericObjectDocumentAttachment(pNuclosFile);
}


/**
 * Getter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Test Rules SubformWithSM
 *<br>DB-Name: STRUID_NUCLOSPROCESS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public void setNuclosProcess(Process<nuclet.test.rules.TestRulesSubformWithSM> pProcess) {
		setFieldId("spwkg6nrk1QO6V1CtdMr10", pProcess.getId());
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(TestRulesSubformWithSM boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public TestRulesSubformWithSM copy() {
		return super.copy(TestRulesSubformWithSM.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
	super.save(saveFlags);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(java.lang.Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("spwkg6nrk1QO6V1CtdMr"), id);
}
/**
* Change Status of this BO. Use this instead of StateModelProvider
*/
public void changeStatus(org.nuclos.api.statemodel.State status) throws org.nuclos.api.exception.BusinessException {
		super.changeStatus(status);
}
/**
* Static Get by Id
*/
public static TestRulesSubformWithSM get(java.lang.Long id) {
		return get(TestRulesSubformWithSM.class, id);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
 }
