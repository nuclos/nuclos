//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package nuclet.test.subform;

import java.util.List;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.Dependent;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * BusinessObject: Subform
 *<br>
 *<br>Nuclet: nuclet.test.subform
 *<br>DB-Name: ONS6_SUBFORM
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class Subform extends AbstractBusinessObject<Long> implements Modifiable<Long> {

public static final Dependent<Subsubform> _Subsubform =
	new Dependent<>("_Subsubform", "null", "Subsubform", "Ey1mGEjJSqnTq9roJtMr", "subform", "ANJGoi68i7P644LqaFUk", Subsubform.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "nuclet.test.subform", "L2ed92mwtBb0n4hoZSpp", "L2ed92mwtBb0n4hoZSpp3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "nuclet.test.subform", "L2ed92mwtBb0n4hoZSpp", "L2ed92mwtBb0n4hoZSpp4", String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "nuclet.test.subform", "L2ed92mwtBb0n4hoZSpp", "L2ed92mwtBb0n4hoZSpp1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "nuclet.test.subform", "L2ed92mwtBb0n4hoZSpp", "L2ed92mwtBb0n4hoZSpp2", String.class);


/**
 * Attribute: date
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: DATdate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> Date = 
	new NumericAttribute<>("Date", "nuclet.test.subform", "L2ed92mwtBb0n4hoZSpp", "XlNizerFlhpH4ZvaGYEX", java.util.Date.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<Long> Id =
	new PrimaryKeyAttribute<>("Id", "nuclet.test.subform", "L2ed92mwtBb0n4hoZSpp", "L2ed92mwtBb0n4hoZSpp0", Long.class);


/**
 * Attribute: parent
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<Long> ParentId =
	new ForeignKeyAttribute<>("ParentId", "nuclet.test.subform", "L2ed92mwtBb0n4hoZSpp", "0f26kgAv7w5XETBxvEAL", Long.class);


/**
 * Attribute: text
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Text = new StringAttribute<>("Text", "nuclet.test.subform", "L2ed92mwtBb0n4hoZSpp", "rB1QaFI6axlJpgi11Ath", String.class);


public Subform() {
		super("L2ed92mwtBb0n4hoZSpp");
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(Subform boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public Subform copy() {
		return super.copy(Subform.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("L2ed92mwtBb0n4hoZSpp"), id);
}


/**
 * Delete-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void deleteSubsubform(Subsubform pSubsubform) {
		deleteDependent(_Subsubform, pSubsubform);
}
/**
* Static Get by Id
*/
public static Subform get(Long id) {
		return get(Subform.class, id);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("L2ed92mwtBb0n4hoZSpp3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("L2ed92mwtBb0n4hoZSpp4", String.class);
}

/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setChangedBy(String username) {
	setField("L2ed92mwtBb0n4hoZSpp4", username);
}

/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("L2ed92mwtBb0n4hoZSpp1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("L2ed92mwtBb0n4hoZSpp2", String.class);
}

/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setCreatedBy(String username) {
	setField("L2ed92mwtBb0n4hoZSpp2", username);
}

/**
 * Getter-Method for attribute: date
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: DATdate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getDate() {
		return getField("XlNizerFlhpH4ZvaGYEX", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("L2ed92mwtBb0n4hoZSpp");
}


/**
 * Getter-Method for attribute: parent
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Reference entity: Parent
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getParent() {
		return getField("0f26kgAv7w5XETBxvEAL", String.class);
}


/**
 * Getter-Method for attribute: parent
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Reference entity: Parent
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends AbstractBusinessObject> T getParentBO(Class<T> boClass) {
		return getReferencedBO(boClass, getFieldId("0f26kgAv7w5XETBxvEAL"), "0f26kgAv7w5XETBxvEAL", "eWrJem9beQc12i0fmdYx");
}


/**
 * Getter-Method for attribute: parent
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Reference entity: Parent
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public Parent getParentBO() {
		return getReferencedBO(Parent.class, getFieldId("0f26kgAv7w5XETBxvEAL"), "0f26kgAv7w5XETBxvEAL", "eWrJem9beQc12i0fmdYx");
}


/**
 * Getter-Method for attribute: parent
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Reference entity: Parent
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public Long getParentId() {
		return getFieldId("0f26kgAv7w5XETBxvEAL");
}


/**
 * Getter-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public List<Subsubform> getSubsubform(Flag... flags) {
		return getDependents(_Subsubform, flags); 
}


/**
 * Getter-Method for attribute: text
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getText() {
		return getField("rB1QaFI6axlJpgi11Ath", String.class);
}


/**
 * Insert-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void insertSubsubform(Subsubform pSubsubform) {
		insertDependent(_Subsubform, pSubsubform);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
	super.save(saveFlags);
}


/**
 * Setter-Method for attribute: date
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: DATdate
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setDate(java.util.Date pDate) {
		setField("XlNizerFlhpH4ZvaGYEX", pDate); 
}


/**
 * Setter-Method for attribute: parent
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Reference entity: Parent
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setParentId(Long pParentId) {
		setFieldId("0f26kgAv7w5XETBxvEAL", pParentId); 
}


/**
 * Setter-Method for attribute: text
 *<br>
 *<br>Entity: Subform
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setText(String pText) {
		setField("rB1QaFI6axlJpgi11Ath", pText); 
}
 }
