//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package nuclet.test.subform;

import java.util.Date;
import java.util.List;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.Dependent;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.businessobject.Process;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.businessobject.facade.LogicalDeletable;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.businessobject.facade.Stateful;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * BusinessObject: Subform Statusmodel
 *<br>
 *<br>Nuclet: nuclet.test.subform
 *<br>DB-Name: ONS6_SUBFORMSTATUSMODEL
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: true
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class SubformStatusmodel extends AbstractBusinessObject<Long> implements LogicalDeletable<Long>, Modifiable<Long>, Stateful {

public static final Dependent<SubsubformStateless> _SubsubformStateless =
	new Dependent<>("_SubsubformStateless", "null", "SubsubformStateless", "qp14vjTk28W7rG9a8Dmm", "subform", "V6kEWDetw2FzJMf1thwf", SubsubformStateless.class);

public static final Dependent<SubsubformStatusmodel> _SubsubformStatusmodel =
	new Dependent<>("_SubsubformStatusmodel", "null", "SubsubformStatusmodel", "22pPmoQwlD2jJZHbgWzt", "subform", "q6oeJmQsIIClKqNbRXPG", SubsubformStatusmodel.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "dgKEZYFU6r7LZWoJEyNl3", Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "dgKEZYFU6r7LZWoJEyNl4", String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "dgKEZYFU6r7LZWoJEyNl1", Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "dgKEZYFU6r7LZWoJEyNl2", String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<Long> Id =
	new PrimaryKeyAttribute<>("Id", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "dgKEZYFU6r7LZWoJEyNl0", Long.class);


/**
 * Attribute: nuclosDeleted
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: BLNNUCLOSDELETED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> NuclosLogicalDeleted =
	new Attribute<>("NuclosLogicalDeleted", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "dgKEZYFU6r7LZWoJEyNl12", Boolean.class);


/**
 * Attribute: nuclosProcess
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> NuclosProcessId =
	new ForeignKeyAttribute<>("NuclosProcessId", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "dgKEZYFU6r7LZWoJEyNl10", UID.class);


/**
 * Attribute: nuclosState
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_NUCLOSSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> NuclosStateId =
	new ForeignKeyAttribute<>("NuclosStateId", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "dgKEZYFU6r7LZWoJEyNl6", UID.class);


/**
 * Attribute: nuclosSystemId
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRNUCLOSSYSTEMID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> NuclosSystemIdentifier = new StringAttribute<>("NuclosSystemIdentifier", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "dgKEZYFU6r7LZWoJEyNl9", String.class);


/**
 * Attribute: parent
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<Long> ParentId =
	new ForeignKeyAttribute<>("ParentId", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "TLYuX0Nn72iAgarHJlcM", Long.class);


/**
 * Attribute: text
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Text = new StringAttribute<>("Text", "nuclet.test.subform", "dgKEZYFU6r7LZWoJEyNl", "SfZVaKyJDeZHxz24lmhO", String.class);


public SubformStatusmodel() {
		super("dgKEZYFU6r7LZWoJEyNl");
		setNuclosLogicalDeleted(Boolean.FALSE);
}
/**
* Change Status of this BO. Use this instead of StateModelProvider
*/
public void changeStatus(org.nuclos.api.statemodel.State status) throws org.nuclos.api.exception.BusinessException {
		super.changeStatus(status);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(SubformStatusmodel boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public SubformStatusmodel copy() {
		return super.copy(SubformStatusmodel.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("dgKEZYFU6r7LZWoJEyNl"), id);
}


/**
 * Remove-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_generalsearchdocument
 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteAttachment(org.nuclos.api.common.NuclosFile pNuclosFile) {
		super.deleteGenericObjectDocumentAttachment(pNuclosFile);
}


/**
 * Delete-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform Stateless
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform Statusmodel
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void deleteSubsubformStateless(SubsubformStateless pSubsubformStateless) {
		deleteDependent(_SubsubformStateless, pSubsubformStateless);
}


/**
 * Delete-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform Statusmodel
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform Statusmodel
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void deleteSubsubformStatusmodel(SubsubformStatusmodel pSubsubformStatusmodel) {
		deleteDependent(_SubsubformStatusmodel, pSubsubformStatusmodel);
}
/**
* Static Get by Id
*/
public static SubformStatusmodel get(Long id) {
		return get(SubformStatusmodel.class, id);
}


/**
 * Getter-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_generalsearchdocument
 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.api.common.NuclosFile> getAttachments() {
		return super.getAttachments();
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("dgKEZYFU6r7LZWoJEyNl3", Date.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("dgKEZYFU6r7LZWoJEyNl4", String.class);
}

/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setChangedBy(String username) {
	setField("dgKEZYFU6r7LZWoJEyNl4", username);
}

/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("dgKEZYFU6r7LZWoJEyNl1", Date.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("dgKEZYFU6r7LZWoJEyNl2", String.class);
}

/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setCreatedBy(String username) {
	setField("dgKEZYFU6r7LZWoJEyNl2", username);
}

/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("dgKEZYFU6r7LZWoJEyNl");
}


/**
 * Getter-Method for attribute: nuclosDeleted
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: BLNNUCLOSDELETED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getNuclosLogicalDeleted() {
		return getField("dgKEZYFU6r7LZWoJEyNl12", Boolean.class);
}


/**
 * Getter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getNuclosProcess() {
		return getField("dgKEZYFU6r7LZWoJEyNl10", String.class);
}


/**
 * Getter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getNuclosProcessId() {
		return getFieldUid("dgKEZYFU6r7LZWoJEyNl10");
}


/**
 * Getter-Method for attribute: nuclosState
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_NUCLOSSTATE
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_state
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getNuclosState() {
		return getField("dgKEZYFU6r7LZWoJEyNl6", String.class);
}


/**
 * Getter-Method for attribute: nuclosState
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_NUCLOSSTATE
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_state
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getNuclosStateId() {
		return getFieldUid("dgKEZYFU6r7LZWoJEyNl6");
}


/**
 * Getter-Method for attribute: nuclosStateNumber
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: INTVALUE_NUCLOSSTATE
 *<br>Data type: java.lang.Integer
 *<br>Reference entity: nuclos_state
 *<br>Reference field: numeral
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 3
 *<br>Precision: null
**/
public Integer getNuclosStateNumber() {
		return getField("dgKEZYFU6r7LZWoJEyNl7", Integer.class);
}


/**
 * Getter-Method for attribute: nuclosSystemId
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRNUCLOSSYSTEMID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getNuclosSystemIdentifier() {
		return getField("dgKEZYFU6r7LZWoJEyNl9", String.class);
}


/**
 * Getter-Method for attribute: parent
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Reference entity: Parent for Stateful Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getParent() {
		return getField("TLYuX0Nn72iAgarHJlcM", String.class);
}


/**
 * Getter-Method for attribute: parent
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Reference entity: Parent for Stateful Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends AbstractBusinessObject> T getParentBO(Class<T> boClass) {
		return getReferencedBO(boClass, getFieldId("TLYuX0Nn72iAgarHJlcM"), "TLYuX0Nn72iAgarHJlcM", "61sz9AOwt03irScnogJt");
}


/**
 * Getter-Method for attribute: parent
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Reference entity: Parent for Stateful Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public ParentforStatefulSubform getParentBO() {
		return getReferencedBO(ParentforStatefulSubform.class, getFieldId("TLYuX0Nn72iAgarHJlcM"), "TLYuX0Nn72iAgarHJlcM", "61sz9AOwt03irScnogJt");
}


/**
 * Getter-Method for attribute: parent
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Reference entity: Parent for Stateful Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public Long getParentId() {
		return getFieldId("TLYuX0Nn72iAgarHJlcM");
}


/**
 * Getter-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform Stateless
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform Statusmodel
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public List<SubsubformStateless> getSubsubformStateless(Flag... flags) {
		return getDependents(_SubsubformStateless, flags); 
}


/**
 * Getter-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform Statusmodel
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform Statusmodel
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public List<SubsubformStatusmodel> getSubsubformStatusmodel(Flag... flags) {
		return getDependents(_SubsubformStatusmodel, flags); 
}


/**
 * Getter-Method for attribute: text
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getText() {
		return getField("SfZVaKyJDeZHxz24lmhO", String.class);
}


/**
 * Insert-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_generalsearchdocument
 *<br>DB-Name: STRUID_T_UD_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertAttachment(org.nuclos.api.common.NuclosFile pNuclosFile, String pComment) {
		insertGenericObjectDocumentAttachment(pNuclosFile,pComment);
}


/**
 * Insert-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform Stateless
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform Statusmodel
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void insertSubsubformStateless(SubsubformStateless pSubsubformStateless) {
		insertDependent(_SubsubformStateless, pSubsubformStateless);
}


/**
 * Insert-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform Statusmodel
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform Statusmodel
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void insertSubsubformStatusmodel(SubsubformStatusmodel pSubsubformStatusmodel) {
		insertDependent(_SubsubformStatusmodel, pSubsubformStatusmodel);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
	super.save(saveFlags);
}


/**
 * Setter-Method for attribute: nuclosDeleted
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: BLNNUCLOSDELETED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNuclosLogicalDeleted(Boolean pNuclosLogicalDeleted) {
		setField("dgKEZYFU6r7LZWoJEyNl12", pNuclosLogicalDeleted); 
}


/**
 * Getter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRUID_NUCLOSPROCESS
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public void setNuclosProcess(Process<SubformStatusmodel> pProcess) {
		setFieldId("dgKEZYFU6r7LZWoJEyNl10", pProcess.getId());
}


/**
 * Setter-Method for attribute: nuclosProcess
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_NUCLOSPROCESS
 *<br>Data type: java.lang.String
 *<br>Reference entity: nuclos_process
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setNuclosProcessId(UID pNuclosProcessId) {
		setFieldId("dgKEZYFU6r7LZWoJEyNl10", pNuclosProcessId); 
}


/**
 * Setter-Method for attribute: nuclosSystemId
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRNUCLOSSYSTEMID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setNuclosSystemIdentifier(String pNuclosSystemIdentifier) {
		setField("dgKEZYFU6r7LZWoJEyNl9", pNuclosSystemIdentifier); 
}


/**
 * Setter-Method for attribute: parent
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRVALUE_STRparent
 *<br>Data type: java.lang.String
 *<br>Reference entity: Parent for Stateful Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setParentId(Long pParentId) {
		setFieldId("TLYuX0Nn72iAgarHJlcM", pParentId); 
}


/**
 * Setter-Method for attribute: text
 *<br>
 *<br>Entity: Subform Statusmodel
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setText(String pText) {
		setField("SfZVaKyJDeZHxz24lmhO", pText); 
}
 }
