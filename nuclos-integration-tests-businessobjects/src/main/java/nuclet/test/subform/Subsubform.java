//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package nuclet.test.subform;

import java.util.Date;
import java.util.List;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.Dependent;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * BusinessObject: Subsubform
 *<br>
 *<br>Nuclet: nuclet.test.subform
 *<br>DB-Name: ONS6_SUBSUBFORM
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class Subsubform extends AbstractBusinessObject<Long> implements Modifiable<Long> {

public static final Dependent<Subsubsubform> _Subsubsubform =
	new Dependent<>("_Subsubsubform", "null", "Subsubsubform", "3PzbIxYiAavqY0h8ZKZY", "subsubform", "hWXYuTwNmc8FH6w170RT", Subsubsubform.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "nuclet.test.subform", "Ey1mGEjJSqnTq9roJtMr", "Ey1mGEjJSqnTq9roJtMr3", Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "nuclet.test.subform", "Ey1mGEjJSqnTq9roJtMr", "Ey1mGEjJSqnTq9roJtMr4", String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "nuclet.test.subform", "Ey1mGEjJSqnTq9roJtMr", "Ey1mGEjJSqnTq9roJtMr1", Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "nuclet.test.subform", "Ey1mGEjJSqnTq9roJtMr", "Ey1mGEjJSqnTq9roJtMr2", String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<Long> Id =
	new PrimaryKeyAttribute<>("Id", "nuclet.test.subform", "Ey1mGEjJSqnTq9roJtMr", "Ey1mGEjJSqnTq9roJtMr0", Long.class);


/**
 * Attribute: subform
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<Long> SubformId =
	new ForeignKeyAttribute<>("SubformId", "nuclet.test.subform", "Ey1mGEjJSqnTq9roJtMr", "ANJGoi68i7P644LqaFUk", Long.class);


/**
 * Attribute: text
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Text = new StringAttribute<>("Text", "nuclet.test.subform", "Ey1mGEjJSqnTq9roJtMr", "NDvCXTKn16Iu13kLDdFr", String.class);


/**
 * Attribute: value
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: DBLvalue
 *<br>Data type: java.lang.Double
 *<br>Localized: false
 *<br>Output format: #,##0.00
 *<br>Scale: 9
 *<br>Precision: 2
**/
public static final NumericAttribute<java.math.BigDecimal> Value = 
	new NumericAttribute<>("Value", "nuclet.test.subform", "Ey1mGEjJSqnTq9roJtMr", "HOlbWRAd3xdX72yqJcpj", java.math.BigDecimal.class);


public Subsubform() {
		super("Ey1mGEjJSqnTq9roJtMr");
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(Subsubform boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public Subsubform copy() {
		return super.copy(Subsubform.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("Ey1mGEjJSqnTq9roJtMr"), id);
}


/**
 * Delete-Method for attribute: subsubform
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRVALUE_STRsubsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subsubform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void deleteSubsubsubform(Subsubsubform pSubsubsubform) {
		deleteDependent(_Subsubsubform, pSubsubsubform);
}
/**
* Static Get by Id
*/
public static Subsubform get(Long id) {
		return get(Subsubform.class, id);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("Ey1mGEjJSqnTq9roJtMr3", Date.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("Ey1mGEjJSqnTq9roJtMr4", String.class);
}

/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setChangedBy(String username) {
	setField("Ey1mGEjJSqnTq9roJtMr4", username);
}

/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("Ey1mGEjJSqnTq9roJtMr1", Date.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("Ey1mGEjJSqnTq9roJtMr2", String.class);
}

/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setCreatedBy(String username) {
	setField("Ey1mGEjJSqnTq9roJtMr2", username);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("Ey1mGEjJSqnTq9roJtMr");
}


/**
 * Getter-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getSubform() {
		return getField("ANJGoi68i7P644LqaFUk", String.class);
}


/**
 * Getter-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends AbstractBusinessObject> T getSubformBO(Class<T> boClass) {
		return getReferencedBO(boClass, getFieldId("ANJGoi68i7P644LqaFUk"), "ANJGoi68i7P644LqaFUk", "L2ed92mwtBb0n4hoZSpp");
}


/**
 * Getter-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public Subform getSubformBO() {
		return getReferencedBO(Subform.class, getFieldId("ANJGoi68i7P644LqaFUk"), "ANJGoi68i7P644LqaFUk", "L2ed92mwtBb0n4hoZSpp");
}


/**
 * Getter-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public Long getSubformId() {
		return getFieldId("ANJGoi68i7P644LqaFUk");
}


/**
 * Getter-Method for attribute: subsubform
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRVALUE_STRsubsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subsubform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public List<Subsubsubform> getSubsubsubform(Flag... flags) {
		return getDependents(_Subsubsubform, flags); 
}


/**
 * Getter-Method for attribute: text
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getText() {
		return getField("NDvCXTKn16Iu13kLDdFr", String.class);
}


/**
 * Getter-Method for attribute: value
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: DBLvalue
 *<br>Data type: java.lang.Double
 *<br>Localized: false
 *<br>Output format: #,##0.00
 *<br>Scale: 9
 *<br>Precision: 2
**/
public java.math.BigDecimal getValue() {
	java.math.BigDecimal retVal = null;
	if (getField("HOlbWRAd3xdX72yqJcpj", Double.class) != null) {
		retVal = new java.math.BigDecimal(getField("HOlbWRAd3xdX72yqJcpj", Double.class));
		retVal = retVal.setScale(2, java.math.BigDecimal.ROUND_HALF_UP);
	}
	return retVal;
}


/**
 * Insert-Method for attribute: subsubform
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRVALUE_STRsubsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subsubform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void insertSubsubsubform(Subsubsubform pSubsubsubform) {
		insertDependent(_Subsubsubform, pSubsubsubform);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
	super.save(saveFlags);
}


/**
 * Setter-Method for attribute: subform
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRVALUE_STRsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setSubformId(Long pSubformId) {
		setFieldId("ANJGoi68i7P644LqaFUk", pSubformId); 
}


/**
 * Setter-Method for attribute: text
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setText(String pText) {
		setField("NDvCXTKn16Iu13kLDdFr", pText); 
}


/**
 * Setter-Method for attribute: value
 *<br>
 *<br>Entity: Subsubform
 *<br>DB-Name: DBLvalue
 *<br>Data type: java.lang.Double
 *<br>Localized: false
 *<br>Output format: #,##0.00
 *<br>Scale: 9
 *<br>Precision: 2
**/
public void setValue(java.math.BigDecimal pValue) {
	if (pValue != null) {
		pValue = pValue.setScale(2, java.math.BigDecimal.ROUND_HALF_UP);
	}
	setField("HOlbWRAd3xdX72yqJcpj", pValue != null ? pValue.doubleValue() : null); 
}
 }
