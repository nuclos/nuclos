//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package nuclet.test.subform;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * BusinessObject: Subsubsubform
 *<br>
 *<br>Nuclet: nuclet.test.subform
 *<br>DB-Name: ONS6_SUBSUBSUBFORM
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class Subsubsubform extends AbstractBusinessObject<Long> implements Modifiable<Long> {


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "nuclet.test.subform", "3PzbIxYiAavqY0h8ZKZY", "3PzbIxYiAavqY0h8ZKZY3", Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "nuclet.test.subform", "3PzbIxYiAavqY0h8ZKZY", "3PzbIxYiAavqY0h8ZKZY4", String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "nuclet.test.subform", "3PzbIxYiAavqY0h8ZKZY", "3PzbIxYiAavqY0h8ZKZY1", Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "nuclet.test.subform", "3PzbIxYiAavqY0h8ZKZY", "3PzbIxYiAavqY0h8ZKZY2", String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<Long> Id =
	new PrimaryKeyAttribute<>("Id", "nuclet.test.subform", "3PzbIxYiAavqY0h8ZKZY", "3PzbIxYiAavqY0h8ZKZY0", Long.class);


/**
 * Attribute: subsubform
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRVALUE_STRsubsubform
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<Long> SubsubformId =
	new ForeignKeyAttribute<>("SubsubformId", "nuclet.test.subform", "3PzbIxYiAavqY0h8ZKZY", "hWXYuTwNmc8FH6w170RT", Long.class);


/**
 * Attribute: text
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Text = new StringAttribute<>("Text", "nuclet.test.subform", "3PzbIxYiAavqY0h8ZKZY", "oD9Mo1kgJV3xoNEIMC0A", String.class);


/**
 * Attribute: value
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: INTvalue
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<Integer> Value =
	new NumericAttribute<>("Value", "nuclet.test.subform", "3PzbIxYiAavqY0h8ZKZY", "3YmMY7qFXbqhKoynriZ6", Integer.class);


public Subsubsubform() {
		super("3PzbIxYiAavqY0h8ZKZY");
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(Subsubsubform boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public Subsubsubform copy() {
		return super.copy(Subsubsubform.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("3PzbIxYiAavqY0h8ZKZY"), id);
}
/**
* Static Get by Id
*/
public static Subsubsubform get(Long id) {
		return get(Subsubsubform.class, id);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("3PzbIxYiAavqY0h8ZKZY3", Date.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("3PzbIxYiAavqY0h8ZKZY4", String.class);
}

/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setChangedBy(String username) {
	setField("3PzbIxYiAavqY0h8ZKZY4", username);
}

/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("3PzbIxYiAavqY0h8ZKZY1", Date.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("3PzbIxYiAavqY0h8ZKZY2", String.class);
}

/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setCreatedBy(String username) {
	setField("3PzbIxYiAavqY0h8ZKZY2", username);
}

/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("3PzbIxYiAavqY0h8ZKZY");
}


/**
 * Getter-Method for attribute: subsubform
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRVALUE_STRsubsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subsubform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getSubsubform() {
		return getField("hWXYuTwNmc8FH6w170RT", String.class);
}


/**
 * Getter-Method for attribute: subsubform
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRVALUE_STRsubsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subsubform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public <T extends AbstractBusinessObject> T getSubsubformBO(Class<T> boClass) {
		return getReferencedBO(boClass, getFieldId("hWXYuTwNmc8FH6w170RT"), "hWXYuTwNmc8FH6w170RT", "Ey1mGEjJSqnTq9roJtMr");
}


/**
 * Getter-Method for attribute: subsubform
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRVALUE_STRsubsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subsubform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public Subsubform getSubsubformBO() {
		return getReferencedBO(Subsubform.class, getFieldId("hWXYuTwNmc8FH6w170RT"), "hWXYuTwNmc8FH6w170RT", "Ey1mGEjJSqnTq9roJtMr");
}


/**
 * Getter-Method for attribute: subsubform
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRVALUE_STRsubsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subsubform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public Long getSubsubformId() {
		return getFieldId("hWXYuTwNmc8FH6w170RT");
}


/**
 * Getter-Method for attribute: text
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getText() {
		return getField("oD9Mo1kgJV3xoNEIMC0A", String.class);
}


/**
 * Getter-Method for attribute: value
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: INTvalue
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public Integer getValue() {
		return getField("3YmMY7qFXbqhKoynriZ6", Integer.class);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
	super.save(saveFlags);
}


/**
 * Setter-Method for attribute: subsubform
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRVALUE_STRsubsubform
 *<br>Data type: java.lang.String
 *<br>Reference entity: Subsubform
 *<br>Reference field: text
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setSubsubformId(Long pSubsubformId) {
		setFieldId("hWXYuTwNmc8FH6w170RT", pSubsubformId); 
}


/**
 * Setter-Method for attribute: text
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setText(String pText) {
		setField("oD9Mo1kgJV3xoNEIMC0A", pText); 
}


/**
 * Setter-Method for attribute: value
 *<br>
 *<br>Entity: Subsubsubform
 *<br>DB-Name: INTvalue
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setValue(Integer pValue) {
		setField("3YmMY7qFXbqhKoynriZ6", pValue); 
}
 }
