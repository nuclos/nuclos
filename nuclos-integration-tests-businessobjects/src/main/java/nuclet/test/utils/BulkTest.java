//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package nuclet.test.utils;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * BusinessObject: BulkTest
 *<br>
 *<br>Nuclet: nuclet.test.utils
 *<br>DB-Name: VX57_BULKTEST
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class BulkTest extends AbstractBusinessObject<Long> implements Modifiable<Long> {


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "nuclet.test.utils", "gwWA91H6cceBH4syjs7v", "gwWA91H6cceBH4syjs7v3", Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "nuclet.test.utils", "gwWA91H6cceBH4syjs7v", "gwWA91H6cceBH4syjs7v4", String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "nuclet.test.utils", "gwWA91H6cceBH4syjs7v", "gwWA91H6cceBH4syjs7v1", Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "nuclet.test.utils", "gwWA91H6cceBH4syjs7v", "gwWA91H6cceBH4syjs7v2", String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<Long> Id =
	new PrimaryKeyAttribute<>("Id", "nuclet.test.utils", "gwWA91H6cceBH4syjs7v", "gwWA91H6cceBH4syjs7v0", Long.class);


/**
 * Attribute: text
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Text = new StringAttribute<>("Text", "nuclet.test.utils", "gwWA91H6cceBH4syjs7v", "6od4VUXSlHsYtWKwDm5h", String.class);


public BulkTest() {
		super("gwWA91H6cceBH4syjs7v");
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(BulkTest boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public BulkTest copy() {
		return super.copy(BulkTest.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("gwWA91H6cceBH4syjs7v"), id);
}
/**
* Static Get by Id
*/
public static BulkTest get(Long id) {
		return get(BulkTest.class, id);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("gwWA91H6cceBH4syjs7v3", Date.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("gwWA91H6cceBH4syjs7v4", String.class);
}

/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setChangedBy(String username) {
	setField("gwWA91H6cceBH4syjs7v4", username);
}

/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("gwWA91H6cceBH4syjs7v1", Date.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("gwWA91H6cceBH4syjs7v2", String.class);
}

/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setCreatedBy(String username) {
	setField("gwWA91H6cceBH4syjs7v2", username);
}

/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("gwWA91H6cceBH4syjs7v");
}


/**
 * Getter-Method for attribute: text
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getText() {
		return getField("6od4VUXSlHsYtWKwDm5h", String.class);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
	super.save(saveFlags);
}


/**
 * Setter-Method for attribute: text
 *<br>
 *<br>Entity: BulkTest
 *<br>DB-Name: STRtext
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setText(String pText) {
		setField("6od4VUXSlHsYtWKwDm5h", pText); 
}
 }
