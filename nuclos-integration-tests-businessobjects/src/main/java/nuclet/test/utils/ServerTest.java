//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package nuclet.test.utils;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * BusinessObject: Server Test
 *<br>
 *<br>Nuclet: nuclet.test.utils
 *<br>DB-Name: VX57_SERVERTEST
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class ServerTest extends AbstractBusinessObject<Long> implements Modifiable<Long> {


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "nuclet.test.utils", "pYTzLhLo9LKshuf6Pvf8", "pYTzLhLo9LKshuf6Pvf83", Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "nuclet.test.utils", "pYTzLhLo9LKshuf6Pvf8", "pYTzLhLo9LKshuf6Pvf84", String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "nuclet.test.utils", "pYTzLhLo9LKshuf6Pvf8", "pYTzLhLo9LKshuf6Pvf81", Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "nuclet.test.utils", "pYTzLhLo9LKshuf6Pvf8", "pYTzLhLo9LKshuf6Pvf82", String.class);


/**
 * Attribute: exception
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRexception
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<String> Exception = new StringAttribute<>("Exception", "nuclet.test.utils", "pYTzLhLo9LKshuf6Pvf8", "yz58ZbLAfDqRfijIGBTn", String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<Long> Id =
	new PrimaryKeyAttribute<>("Id", "nuclet.test.utils", "pYTzLhLo9LKshuf6Pvf8", "pYTzLhLo9LKshuf6Pvf80", Long.class);


/**
 * Attribute: ok
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: BLNok
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public static final Attribute<Boolean> Ok =
	new Attribute<>("Ok", "nuclet.test.utils", "pYTzLhLo9LKshuf6Pvf8", "kvtgBEmnChIbD0JxAtzr", Boolean.class);


/**
 * Attribute: testcase
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRtestcase
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Testcase = new StringAttribute<>("Testcase", "nuclet.test.utils", "pYTzLhLo9LKshuf6Pvf8", "aTWgUNWu4uJhFnP7h2yN", String.class);


public ServerTest() {
		super("pYTzLhLo9LKshuf6Pvf8");
		setOk(Boolean.FALSE);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(ServerTest boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public ServerTest copy() {
		return super.copy(ServerTest.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("pYTzLhLo9LKshuf6Pvf8"), id);
}
/**
* Static Get by Id
*/
public static ServerTest get(Long id) {
		return get(ServerTest.class, id);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("pYTzLhLo9LKshuf6Pvf83", Date.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("pYTzLhLo9LKshuf6Pvf84", String.class);
}

/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setChangedBy(String username) {
	setField("pYTzLhLo9LKshuf6Pvf84", username);
}

/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("pYTzLhLo9LKshuf6Pvf81", Date.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("pYTzLhLo9LKshuf6Pvf82", String.class);
}

/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
 **/
public void setCreatedBy(String username) {
	setField("pYTzLhLo9LKshuf6Pvf82", username);
}

/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("pYTzLhLo9LKshuf6Pvf8");
}


/**
 * Getter-Method for attribute: exception
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRexception
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public String getException() {
		return getField("yz58ZbLAfDqRfijIGBTn", String.class);
}


/**
 * Getter-Method for attribute: ok
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: BLNok
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public Boolean getOk() {
		return getField("kvtgBEmnChIbD0JxAtzr", Boolean.class);
}


/**
 * Getter-Method for attribute: testcase
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRtestcase
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getTestcase() {
		return getField("aTWgUNWu4uJhFnP7h2yN", String.class);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
	super.save(saveFlags);
}


/**
 * Setter-Method for attribute: exception
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRexception
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setException(String pException) {
		setField("yz58ZbLAfDqRfijIGBTn", pException); 
}


/**
 * Setter-Method for attribute: ok
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: BLNok
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public void setOk(Boolean pOk) {
		setField("kvtgBEmnChIbD0JxAtzr", pOk); 
}


/**
 * Setter-Method for attribute: testcase
 *<br>
 *<br>Entity: Server Test
 *<br>DB-Name: STRtestcase
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setTestcase(String pTestcase) {
		setField("aTWgUNWu4uJhFnP7h2yN", pTestcase); 
}
 }
