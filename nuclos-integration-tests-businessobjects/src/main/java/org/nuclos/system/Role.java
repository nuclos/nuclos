//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.system;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.common.NuclosRole;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * BusinessObject: nuclos_role
 *<br>
 *<br>Nuclet: org.nuclos.system
 *<br>DB-Name: T_MD_ROLE
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class Role extends AbstractBusinessObject<UID> implements NuclosRole {
private static final long serialVersionUID = 1L;



/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.system", "exNI", "exNI3", Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.system", "exNI", "exNI4", String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.system", "exNI", "exNI1", Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.system", "exNI", "exNI2", String.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<String> Description = new StringAttribute<>("Description", "org.nuclos.system", "exNI", "exNIb", String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<UID> Id =
	new PrimaryKeyAttribute<>("Id", "org.nuclos.system", "exNI", "exNI0", UID.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRROLE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Name = new StringAttribute<>("Name", "org.nuclos.system", "exNI", "exNIa", String.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> NucletId =
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.system", "exNI", "exNId", UID.class);


/**
 * Attribute: parentRole
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRUID_T_MD_ROLE
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> ParentRoleId =
	new ForeignKeyAttribute<>("ParentRoleId", "org.nuclos.system", "exNI", "exNIc", UID.class);


/**
 * Attribute: withRuleClass
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: BLNWITHRULECLASS
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> WithRuleClass =
	new Attribute<>("WithRuleClass", "org.nuclos.system", "exNI", "exNIe", Boolean.class);


public Role() {
		super("exNI");
}
/**
* Static Get by Id
*/
public static Role get(UID id) {
		return get(Role.class, id);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("exNI3", Date.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("exNI4", String.class);
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("exNI1", Date.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("exNI2", String.class);
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public String getDescription() {
		return getField("exNIb", String.class);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("exNI");
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRROLE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getName() {
		return getField("exNIa", String.class);
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("exNId");
}


/**
 * Getter-Method for attribute: parentRole
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: STRUID_T_MD_ROLE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_role
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public UID getParentRoleId() {
		return getFieldUid("exNIc");
}


/**
 * Getter-Method for attribute: withRuleClass
 *<br>
 *<br>Entity: nuclos_role
 *<br>DB-Name: BLNWITHRULECLASS
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getWithRuleClass() {
		return getField("exNIe", Boolean.class);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
 }
