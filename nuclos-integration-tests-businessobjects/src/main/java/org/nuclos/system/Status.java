//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.system;

import java.util.Date; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.system.*; 

/**
 * BusinessObject: nuclos_state
 *<br>
 *<br>Nuclet: org.nuclos.system
 *<br>DB-Name: T_MD_STATE
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class Status extends AbstractBusinessObject<org.nuclos.api.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.api.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.system", "U89N", "U89N0", org.nuclos.api.UID.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.system", "U89N", "U89Na", java.lang.String.class);


/**
 * Attribute: model
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID_T_MD_STATEMODEL
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.api.UID> ModelId = 
	new ForeignKeyAttribute<>("ModelId", "org.nuclos.system", "U89N", "U89Nb", org.nuclos.api.UID.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Description = new StringAttribute<>("Description", "org.nuclos.system", "U89N", "U89Nc", java.lang.String.class);


/**
 * Attribute: numeral
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: INTNUMERAL
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 3
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Numeral = 
	new NumericAttribute<>("Numeral", "org.nuclos.system", "U89N", "U89Nd", java.lang.Integer.class);


/**
 * Attribute: labelres
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_L
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> LabelResource = new StringAttribute<>("LabelResource", "org.nuclos.system", "U89N", "U89Ne", java.lang.String.class);


/**
 * Attribute: descriptionres
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_D
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> DescriptionResource = new StringAttribute<>("DescriptionResource", "org.nuclos.system", "U89N", "U89Nf", java.lang.String.class);


/**
 * Attribute: tab
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRTAB
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Tab = new StringAttribute<>("Tab", "org.nuclos.system", "U89N", "U89Ng", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.system", "U89N", "U89N1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.system", "U89N", "U89N2", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.system", "U89N", "U89N3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.system", "U89N", "U89N4", java.lang.String.class);


/**
 * Attribute: icon
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: OBJICON
 *<br>Data type: org.nuclos.common.NuclosImage
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final Attribute<org.nuclos.api.NuclosImage> Icon = 
	new Attribute<>("Icon", "org.nuclos.system", "U89N", "U89Nh", org.nuclos.api.NuclosImage.class);


/**
 * Attribute: buttonIcon
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID_BUTTONICON
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.api.UID> ButtonIconId = 
	new ForeignKeyAttribute<>("ButtonIconId", "org.nuclos.system", "U89N", "U89Ni", org.nuclos.api.UID.class);


/**
 * Attribute: color
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Color = new StringAttribute<>("Color", "org.nuclos.system", "U89N", "U89Nj", java.lang.String.class);


/**
 * Attribute: buttonRes
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_BUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ButtonResource = new StringAttribute<>("ButtonResource", "org.nuclos.system", "U89N", "U89Nk", java.lang.String.class);


public Status() {
		super("U89N");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.api.UID getEntityUid() {
		return new org.nuclos.common.UID("U89N");
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getName() {
		return getField("U89Na", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: model
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID_T_MD_STATEMODEL
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_statemodel
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.api.UID getModelId() {
		return getFieldUid("U89Nb");
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public java.lang.String getDescription() {
		return getField("U89Nc", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: numeral
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: INTNUMERAL
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 3
 *<br>Precision: null
**/
public java.lang.Integer getNumeral() {
		return getField("U89Nd", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: labelres
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_L
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getLabelResource() {
		return getField("U89Ne", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: descriptionres
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_D
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getDescriptionResource() {
		return getField("U89Nf", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: tab
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRTAB
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getTab() {
		return getField("U89Ng", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("U89N1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("U89N2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("U89N3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("U89N4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: icon
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: OBJICON
 *<br>Data type: org.nuclos.common.NuclosImage
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.api.NuclosImage getIcon() {
		return getField("U89Nh", org.nuclos.api.NuclosImage.class); 
}


/**
 * Getter-Method for attribute: buttonIcon
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRUID_BUTTONICON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_resource
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.api.UID getButtonIconId() {
		return getFieldUid("U89Ni");
}


/**
 * Getter-Method for attribute: color
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STRCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public java.lang.String getColor() {
		return getField("U89Nj", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: buttonRes
 *<br>
 *<br>Entity: nuclos_state
 *<br>DB-Name: STR_LOCALERESOURCE_BUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getButtonResource() {
		return getField("U89Nk", java.lang.String.class); 
}
/**
* Static Get by Id
*/
public static Status get(org.nuclos.api.UID id) {
		return get(Status.class, id);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
 }
