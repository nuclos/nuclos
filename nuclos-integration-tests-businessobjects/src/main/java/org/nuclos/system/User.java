//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.system;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.server.nbo.AbstractBusinessObject;

/**
 * BusinessObject: nuclos_user
 *<br>
 *<br>Nuclet: org.nuclos.system
 *<br>DB-Name: T_MD_USER
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class User extends AbstractBusinessObject<UID> implements NuclosUser {
private static final long serialVersionUID = 1L;



/**
 * Attribute: activationCode
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRACTIVATIONCODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ActivationCode = new StringAttribute<>("ActivationCode", "org.nuclos.system", "dRxj", "dRxjp", String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.system", "dRxj", "dRxj3", Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.system", "dRxj", "dRxj4", String.class);


/**
 * Attribute: communicationAccountPhone
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUID_COMPORT_PHONE
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<UID> CommunicationAccountPhoneId =
	new ForeignKeyAttribute<>("CommunicationAccountPhoneId", "org.nuclos.system", "dRxj", "dRxjo", UID.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.system", "dRxj", "dRxj1", Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.system", "dRxj", "dRxj2", String.class);


/**
 * Attribute: email
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STREMAIL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Email = new StringAttribute<>("Email", "org.nuclos.system", "dRxj", "dRxjb", String.class);


/**
 * Attribute: expirationDate
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATEXPIRATIONDATE
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> ExpirationDate =
	new NumericAttribute<>("ExpirationDate", "org.nuclos.system", "dRxj", "dRxjk", Date.class);


/**
 * Attribute: firstname
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRFIRSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Firstname = new StringAttribute<>("Firstname", "org.nuclos.system", "dRxj", "dRxjd", String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<UID> Id =
	new PrimaryKeyAttribute<>("Id", "org.nuclos.system", "dRxj", "dRxj0", UID.class);


/**
 * Attribute: lastLogin
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATLASTLOGIN
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> LastLogin =
	new NumericAttribute<>("LastLogin", "org.nuclos.system", "dRxj", "dRxjm", Date.class);


/**
 * Attribute: lastname
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRLASTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Lastname = new StringAttribute<>("Lastname", "org.nuclos.system", "dRxj", "dRxjc", String.class);


/**
 * Attribute: lastPasswordChange
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATPASSWORDCHANGED
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<Date> LastPasswordChange =
	new NumericAttribute<>("LastPasswordChange", "org.nuclos.system", "dRxj", "dRxjj", Date.class);


/**
 * Attribute: locked
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNLOCKED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> Locked =
	new Attribute<>("Locked", "org.nuclos.system", "dRxj", "dRxji", Boolean.class);


/**
 * Attribute: loginAttempts
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: INTLOGINATTEMPTS
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<Integer> LoginAttempts =
	new NumericAttribute<>("LoginAttempts", "org.nuclos.system", "dRxj", "dRxjn", Integer.class);


/**
 * Attribute: loginWithEmailAllowed
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNEMAILLOGIN
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> LoginWithEmailAllowed =
	new Attribute<>("LoginWithEmailAllowed", "org.nuclos.system", "dRxj", "dRxjs", Boolean.class);


/**
 * Attribute: password
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRPASSWORD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> Password = new StringAttribute<>("Password", "org.nuclos.system", "dRxj", "dRxjg", String.class);


/**
 * Attribute: passwordChangeRequired
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNREQUIREPASSWORDCHANGE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> PasswordChangeRequired =
	new Attribute<>("PasswordChangeRequired", "org.nuclos.system", "dRxj", "dRxjl", Boolean.class);


/**
 * Attribute: passwordResetCode
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRPASSWORDRESETCODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<String> PasswordResetCode = new StringAttribute<>("PasswordResetCode", "org.nuclos.system", "dRxj", "dRxjr", String.class);


/**
 * Attribute: privacyConsentAccepted
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNPRIVACYCONSENT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> PrivacyConsentAccepted =
	new Attribute<>("PrivacyConsentAccepted", "org.nuclos.system", "dRxj", "dRxjq", Boolean.class);


/**
 * Attribute: superuser
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNSUPERUSER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<Boolean> Superuser =
	new Attribute<>("Superuser", "org.nuclos.system", "dRxj", "dRxjh", Boolean.class);


/**
 * Attribute: username
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUSER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public static final StringAttribute<String> Username = new StringAttribute<>("Username", "org.nuclos.system", "dRxj", "dRxja", String.class);


public User() {
		super("dRxj");
		setSuperuser(Boolean.FALSE);
		setPrivacyConsentAccepted(Boolean.FALSE);
		setLoginWithEmailAllowed(Boolean.FALSE);
}
/**
* Static Get by Id
*/
public static User get(UID id) {
		return get(User.class, id);
}


/**
 * Getter-Method for attribute: activationCode
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRACTIVATIONCODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getActivationCode() {
		return getField("dRxjp", String.class);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getChangedAt() {
		return getField("dRxj3", Date.class);
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getChangedBy() {
		return getField("dRxj4", String.class);
}


/**
 * Getter-Method for attribute: communicationAccountPhone
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUID_COMPORT_PHONE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_userCommunicationAccount
 *<br>Reference field: communicationPort (account)
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public UID getCommunicationAccountPhoneId() {
		return getFieldUid("dRxjo");
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getCreatedAt() {
		return getField("dRxj1", Date.class);
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getCreatedBy() {
		return getField("dRxj2", String.class);
}


/**
 * Getter-Method for attribute: email
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STREMAIL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getEmail() {
		return getField("dRxjb", String.class);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public UID getEntityUid() {
		return new org.nuclos.common.UID("dRxj");
}


/**
 * Getter-Method for attribute: expirationDate
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATEXPIRATIONDATE
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getExpirationDate() {
		return getField("dRxjk", Date.class);
}


/**
 * Getter-Method for attribute: firstname
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRFIRSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getFirstname() {
		return getField("dRxjd", String.class);
}


/**
 * Getter-Method for attribute: lastLogin
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATLASTLOGIN
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getLastLogin() {
		return getField("dRxjm", Date.class);
}


/**
 * Getter-Method for attribute: lastname
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRLASTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getLastname() {
		return getField("dRxjc", String.class);
}


/**
 * Getter-Method for attribute: lastPasswordChange
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATPASSWORDCHANGED
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Date getLastPasswordChange() {
		return getField("dRxjj", Date.class);
}


/**
 * Getter-Method for attribute: locked
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNLOCKED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getLocked() {
		return getField("dRxji", Boolean.class);
}


/**
 * Getter-Method for attribute: loginAttempts
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: INTLOGINATTEMPTS
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public Integer getLoginAttempts() {
		return getField("dRxjn", Integer.class);
}


/**
 * Getter-Method for attribute: loginWithEmailAllowed
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNEMAILLOGIN
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getLoginWithEmailAllowed() {
		return getField("dRxjs", Boolean.class);
}


/**
 * Getter-Method for attribute: passwordChangeRequired
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNREQUIREPASSWORDCHANGE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getPasswordChangeRequired() {
		return getField("dRxjl", Boolean.class);
}


/**
 * Getter-Method for attribute: passwordResetCode
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRPASSWORDRESETCODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public String getPasswordResetCode() {
		return getField("dRxjr", String.class);
}


/**
 * Getter-Method for attribute: privacyConsentAccepted
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNPRIVACYCONSENT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getPrivacyConsentAccepted() {
		return getField("dRxjq", Boolean.class);
}


/**
 * Getter-Method for attribute: superuser
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNSUPERUSER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public Boolean getSuperuser() {
		return getField("dRxjh", Boolean.class);
}


/**
 * Getter-Method for attribute: username
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUSER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public String getUsername() {
		return getField("dRxja", String.class);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}


/**
 * Setter-Method for attribute: activationCode
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRACTIVATIONCODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setActivationCode(String pActivationCode) {
		setField("dRxjp", pActivationCode); 
}


/**
 * Setter-Method for attribute: email
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STREMAIL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setEmail(String pEmail) {
		setField("dRxjb", pEmail); 
}


/**
 * Setter-Method for attribute: expirationDate
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: DATEXPIRATIONDATE
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setExpirationDate(Date pExpirationDate) {
		setField("dRxjk", pExpirationDate); 
}


/**
 * Setter-Method for attribute: firstname
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRFIRSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setFirstname(String pFirstname) {
		setField("dRxjd", pFirstname); 
}


/**
 * Setter-Method for attribute: lastname
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRLASTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setLastname(String pLastname) {
		setField("dRxjc", pLastname); 
}


/**
 * Setter-Method for attribute: locked
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNLOCKED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setLocked(Boolean pLocked) {
		setField("dRxji", pLocked); 
}


/**
 * Setter-Method for attribute: loginWithEmailAllowed
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNEMAILLOGIN
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setLoginWithEmailAllowed(Boolean pLoginWithEmailAllowed) {
		setField("dRxjs", pLoginWithEmailAllowed); 
}


/**
 * Setter-Method for attribute: passwordChangeRequired
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNREQUIREPASSWORDCHANGE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setPasswordChangeRequired(Boolean pPasswordChangeRequired) {
		setField("dRxjl", pPasswordChangeRequired); 
}


/**
 * Setter-Method for attribute: passwordResetCode
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRPASSWORDRESETCODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setPasswordResetCode(String pPasswordResetCode) {
		setField("dRxjr", pPasswordResetCode); 
}


/**
 * Setter-Method for attribute: privacyConsentAccepted
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNPRIVACYCONSENT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setPrivacyConsentAccepted(Boolean pPrivacyConsentAccepted) {
		setField("dRxjq", pPrivacyConsentAccepted); 
}


/**
 * Setter-Method for attribute: superuser
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: BLNSUPERUSER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setSuperuser(Boolean pSuperuser) {
		setField("dRxjh", pSuperuser); 
}


/**
 * Setter-Method for attribute: username
 *<br>
 *<br>Entity: nuclos_user
 *<br>DB-Name: STRUSER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public void setUsername(String pUsername) {
		setField("dRxja", pUsername); 
}
 }
