package extension.example.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.PUT;

import org.nuclos.api.businessobject.Query;
import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.CustomRestRule;

import nuclet.test.utils.BulkTest;

public class BulkWritingRuleExtension implements CustomRestRule {

	@POST
	public String insertBulk(final CustomRestContext context) throws BusinessException {
       List<String> lstStrings = UserManagementRuleExtension.getStrings("words", context);
       List<BulkTest> lstTests = new ArrayList<>();

       for (String s : lstStrings) {
       		BulkTest bt = new BulkTest();
       		bt.setText(s);
       		lstTests.add(bt);
	   }

       BusinessObjectProvider.insertAll(lstTests);

       return lstStrings.size() + "";
	}

	@PUT
	public String updateBulk(final CustomRestContext context) throws BusinessException {
		String string = UserManagementRuleExtension.getString("words", context);

		Query<BulkTest> query = QueryProvider.create(BulkTest.class).where(BulkTest.Text.like(string));
		List<BulkTest> lstTests = QueryProvider.execute(query);

		for (BulkTest bt : lstTests) {
			bt.setText(bt.getText() + "C");
		}

		BusinessObjectProvider.updateAll(lstTests);

		return lstTests.size() + "";
	}

	@PUT
	public String deleteBulk(final CustomRestContext context) throws BusinessException {
		String string = UserManagementRuleExtension.getString("words", context); // ="TestWord";

		Query<BulkTest> query = QueryProvider.create(BulkTest.class).where(BulkTest.Text.like(string));
		List<BulkTest> lstTests = QueryProvider.execute(query);

		BusinessObjectProvider.deleteAll(lstTests);

		return lstTests.size() + "";
	}

}
