package extension.example.rest;

import javax.ws.rs.GET;

import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.CustomRestRule;

public class CustomRestInactiveRuleExtension implements CustomRestRule {

	@GET
	public String getTest(final CustomRestContext context) throws BusinessException {
		return "user-last-name:" + context.getUser().getLastname();
	}

}
