package extension.example.rest;

import javax.json.JsonObject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;

import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.CustomRestRule;
import org.nuclos.system.Status;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.Explode;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.enums.ParameterStyle;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.links.Link;
import io.swagger.v3.oas.annotations.links.LinkParameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;

public class CustomRestTestRuleExtension implements CustomRestRule {

	@Operation(
			deprecated = true,
			summary = "Test summary",
			tags = {"test"},
			description = "Test description",
			externalDocs = @ExternalDocumentation(
					description = "Test External Documentation",
					url = "http://www.nuclos.de"
			),
			parameters = {
					@Parameter(
							name = "Test Parameter 1",
							in = ParameterIn.QUERY,
							description = "Test Parameter 1 description",
							required = true,
							deprecated = true,
							allowEmptyValue = true,
							style = ParameterStyle.FORM,
							explode = Explode.TRUE,
							allowReserved = true,
							schema = @Schema(
								type = "array"
							),
							hidden = true,
							examples = {
									@ExampleObject(
											name = "Example 1",
											summary = "Test Parameter 1 Example 1",
											value = "Example Value 1",
											externalValue = "External Value 1",
											ref = "Example 1 ref"
									)
							},
							example = "Direct Example 1",
							ref = "Parameter 1 ref"
					)
			},
			requestBody = @RequestBody(
					required = true,
					description = "Request Body description",
					content = @Content(
							schema = @Schema(
									implementation = String.class,
									name = "Request Body Schema",
									type = "string"
							),
							mediaType = "application/json"

							// Fails validation:
//							encoding = @Encoding(
//									name = "Request Body Encoding",
//									style = "Request Body Encoding Style",
//									explode = true,
//									allowReserved = true,
//									contentType = "application/json"
//							)
					)
			),
			responses = @ApiResponse(
					description = "Test Response",
					responseCode = "123",
					headers = @Header(
							deprecated = true,
							description = "Test Response Header description",
							name = "Test Response Header name",
							required = true,
							schema = @Schema(
									type = "string"
							)
					),
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(
									implementation = String.class,
									type = "string"
							),
							examples = @ExampleObject(
									name = "Test Example Object",
									summary = "Test Example Object summary",
									value = "Test value",
									externalValue = "Test external value",
									ref = "Test ref"
							)
					),
					links = @Link(
							name = "Test Link",
							operationId = "extension.example.rest.CustomRestTestRuleExtension.getTest",
							parameters = @LinkParameter(
									name = "Test Parameter 1",
									expression = "Test expression"
							),
							description = "Test link description",
							requestBody = "Test link request body",
							server = @Server(
									url = "http://www.nuclos.de",
									description = "Nuclos Home"
							)
					)
			),
			security = @SecurityRequirement(
					name = "Test Security Requirement",
					scopes = {"Scope 1", "Scope 2"}
			),
			servers = @Server(
					url = "http://www.nuclos.de",
					description = "Nuclos Homepage",
					variables = @ServerVariable(
							name = "Test Server Variable",
							allowableValues = {"1", "2"},
							defaultValue = "1",
							description = "Test Server Variable description"
					)
			)
	)
	@GET
	public String getTest(final CustomRestContext context) throws BusinessException {
		Status testBusinessObjectClassFromCodeGen = new Status();
		context.log(testBusinessObjectClassFromCodeGen.toString());
		return "user-last-name:" + context.getUser().getLastname();
	}

	@GET
	public String getTestWithRequestParams(final CustomRestContext context) throws BusinessException {
		return context.getRequestParameter("param1") + context.getRequestParameter("param2");
	}

	@POST
	public JsonObject postTest(final CustomRestContext context) throws BusinessException {
		return context.getRequestData();
	}

	@PUT
	public JsonObject putTest(final CustomRestContext context) throws BusinessException {
		return context.getRequestData();
	}

	@DELETE
	public String deleteTest(final CustomRestContext context) throws BusinessException {
		return "ok";
	}
}
