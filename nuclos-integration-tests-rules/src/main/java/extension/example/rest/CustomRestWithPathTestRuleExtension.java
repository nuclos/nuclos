package extension.example.rest;
    
             
import javax.inject.Inject;
import javax.inject.Provider;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.nuclos.api.annotation.AnonymousAuthentication;
import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.rule.CustomRestRule;

import io.swagger.v3.oas.annotations.Operation;

@Path("restpathtest_extension")
public class CustomRestWithPathTestRuleExtension implements CustomRestRule {

	@Inject Provider<CustomRestContext> context;
  
	@Path("gettest")
	@Operation(
			summary = "Test summary",
			tags = {"test"},
			description = "Test description"
	)
	@AnonymousAuthentication
	@GET
	public String getTest() {
		return "user-last-name:" + context.get().getUser().getLastname();
	}

}
