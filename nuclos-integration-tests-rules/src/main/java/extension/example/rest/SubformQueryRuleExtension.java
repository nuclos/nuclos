package extension.example.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;

import org.nuclos.api.businessobject.Query;
import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.CustomRestRule;

import nuclet.test.subform.Parent;
import nuclet.test.subform.Subform;

public class SubformQueryRuleExtension implements CustomRestRule {

	@GET
	public String testSubformQuery(final CustomRestContext context) throws BusinessException {
		Query<Subform> subQuery = QueryProvider.create(Subform.class);
		subQuery.where(Subform.Text.eq("1").or(Subform.Text.eq("2")));

		Query<Parent> query = QueryProvider.create(Parent.class).orderBy(Parent.Text, true);
		query.exist(subQuery, null, Subform.ParentId);

		List<String> lst = QueryProvider.execute(query).stream().map(Parent::getText).collect(Collectors.toList());
        return lst.toString();
	}

	@GET
	public String testReferenceQuery(final CustomRestContext context) throws BusinessException {
		Query<Parent> subQuery = QueryProvider.create(Parent.class);
		subQuery.where(Parent.Value.Gte(2));

		Query<Subform> query = QueryProvider.create(Subform.class).orderBy(Subform.Text, true);
		query.exist(subQuery, Subform.ParentId);

		List<String> lst = QueryProvider.execute(query).stream().map(Subform::getText).collect(Collectors.toList());
		return lst.toString();
	}

}
