package extension.example.rest;

import java.util.ArrayList;
import java.util.List;

import javax.json.JsonArray;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;

import org.nuclos.api.businessobject.Query;
import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.provider.UserProvider;
import org.nuclos.api.rule.CustomRestRule;
import org.nuclos.system.Role;
import org.nuclos.system.User;

import example.rest.ExampleuserUR;

public class UserManagementRuleExtension implements CustomRestRule {

	@POST
	public String grantRoleToUser(final CustomRestContext context) throws BusinessException {
		return revokeOrGrantRoleFromUser(context, true);
	}

	@DELETE
	public String revokeRoleFromUser(final CustomRestContext context) throws BusinessException {
		return revokeOrGrantRoleFromUser(context, false);
	}

	private String revokeOrGrantRoleFromUser(final CustomRestContext context, boolean grant) throws BusinessException {
		List<String> userNames = getStrings("users", context);
		List<User> lstUsers = getMatchingUsers(userNames);

		// NUCLOS-7876
		Query<Role> qRole = QueryProvider.create(Role.class);
		qRole.where(Role.Name.eq("Example user"));
		Role role = QueryProvider.executeSingleResult(qRole);

		for (User user : lstUsers) {
			if (grant) {
				UserProvider.grantRole(role, user);
			} else {
				UserProvider.revokeRole(ExampleuserUR.class, user);
			}
		}

		return lstUsers.size() + "";
	}

	@POST
	public String createUsers(final CustomRestContext context) throws BusinessException {
		List<String> userNames = getStrings("users", context);

		List<User> toCreate = new ArrayList<>();

		for (String userName : userNames) {
			User user = new User();
			user.setUsername(userName);
			user.setFirstname(userName);
			user.setLastname(userName);
			user.setEmail(userName + "@test.de");

			toCreate.add(user);

			UserProvider.insert(user, userName, false);
		}

//		BusinessObjectProvider.insertAll(toCreate);

		return userNames.size() + "";
	}

	@DELETE
	public String deleteUsers(final CustomRestContext context) throws BusinessException {
		List<String> userNames = getStrings("users", context);
		List<User> toDelete = getMatchingUsers(userNames);

		BusinessObjectProvider.deleteAll(toDelete);

		return toDelete.size() + "";
	}

	@DELETE
	public String deleteRoles(final CustomRestContext context) throws BusinessException {
		String search = getString("search", context);

		Query<Role> query = QueryProvider.create(Role.class).where(Role.Name.like(search));
		List<Role> lstRoles = QueryProvider.execute(query);

		BusinessObjectProvider.deleteAll(lstRoles);

		return "" + lstRoles.size();
	}

	private static List<User> getMatchingUsers(List<String> userNames) throws BusinessException {
		List<User> result = new ArrayList<>();

		for (String userName : userNames) {
			Query<User> query = QueryProvider.create(User.class).where(User.Username.eq(userName));
			User user = QueryProvider.executeSingleResult(query);

			if (user != null) {
				result.add(user);
			}
		}

		return result;
	}

	public static String getString(String key, CustomRestContext context) {
		return context.getRequestData().getString(key);
	}

	public static List<String> getStrings(String key, CustomRestContext context) {
		JsonArray array = context.getRequestData().getJsonArray(key);
		List<String> ret = new ArrayList<>();
		for (int i = 0; i < array.size(); i++) {
			ret.add(array.getString(i));
		}

		return ret;
	}
}
