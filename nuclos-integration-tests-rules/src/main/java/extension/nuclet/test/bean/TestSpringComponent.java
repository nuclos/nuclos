package extension.nuclet.test.bean;

import org.springframework.stereotype.Component;

/**
 * With this little class we could test the Spring autowire functionalities in our nuclos rules
 * See Java8Rule(Extension)
 */
@Component
public class TestSpringComponent {

	public static final String TEST = "object created";

	private final String test;

	private TestSpringComponent() {
		this.test = TEST;
	}

	public String getTest() {
		return test;
	}

}
