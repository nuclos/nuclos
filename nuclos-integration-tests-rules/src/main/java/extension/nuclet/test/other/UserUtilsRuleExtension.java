package extension.nuclet.test.other;

import java.util.Optional;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;

import org.nuclos.api.UID;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.provider.UserProvider;
import org.nuclos.api.rule.CustomRestRule;
import org.nuclos.system.User;

public class UserUtilsRuleExtension implements CustomRestRule {

    @POST
	public JsonObject postNewEmailLoginSuperuser(final CustomRestContext context) throws BusinessException {
        JsonObject newUserData = context.getRequestData();
        User newUser = new User();
        newUser.setUsername(newUserData.getString("username"));
        newUser.setFirstname(newUserData.getString("firstname"));
        newUser.setLastname(newUserData.getString("lastname"));
        newUser.setEmail(newUserData.getString("email"));
        newUser.setLoginWithEmailAllowed(true);
        newUser.setSuperuser(true);
        UID userId = UserProvider.insert(newUser, newUserData.getString("password"), false);
        JsonObjectBuilder json = Json.createObjectBuilder();
        json.add("userId", userId.toString());
        json.add("status", "user-ok");
        return json.build();
	}

    @PUT
    public JsonObject putChangeEmailAndResetPassword(final CustomRestContext context) throws BusinessException {
        JsonObject modifiedUserData = context.getRequestData();
        String username = modifiedUserData.getString("username");
        Query<User> q = QueryProvider.create(User.class);
        q.where(User.Username.eq(username));
        NuclosUser user = QueryProvider.executeSingleResult(q);
        user.setEmail(modifiedUserData.getString("email"));
        // Resets updates the user also...
        UserProvider.resetPassword(user, modifiedUserData.getString("password"), false);
        JsonObjectBuilder json = Json.createObjectBuilder();
        json.add("userId", user.getId().toString());
        json.add("status", "user-ok");
        return json.build();
    }

    @POST
    public JsonObject verifyUser(final CustomRestContext context) {
        JsonObject userData = context.getRequestData();
        String username = userData.getString("username");
        String password = userData.getString("password");

        Optional<UID> userUid = UserProvider.verifyPasswordResolveUserId(username, password);
        JsonObjectBuilder json = Json.createObjectBuilder();
        if (userUid.isPresent()) {
            json.add("userId", userUid.get().toString());
            json.add("status", "user-ok");
        } else {
            json.add("status", "not-verified");
        }
        return json.build();
    }

    @GET
    public String getTestValueFromRule(final CustomRestContext context) throws BusinessException {
        return "1-2-3-4";
    }

}
