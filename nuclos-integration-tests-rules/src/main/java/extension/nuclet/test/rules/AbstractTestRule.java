package extension.nuclet.test.rules;

import nuclet.test.rules.TestRules;

public class AbstractTestRule {
	protected String ok() { return "ext-ok"; }
	protected boolean execute(TestRules bo) { return Boolean.TRUE.equals(bo.getRegelnausextension()); }
}
