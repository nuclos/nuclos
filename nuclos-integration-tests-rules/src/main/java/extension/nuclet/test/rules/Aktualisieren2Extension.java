package extension.nuclet.test.rules;

import org.nuclos.api.rule.UpdateRule; 
import org.nuclos.api.context.UpdateContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="Aktualisieren2Extension", description="Aktualisieren2Extension")
public class Aktualisieren2Extension extends AbstractTestRule implements UpdateRule {

	public void update(UpdateContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert bo.getAktualisieren() != null;
		if (bo.getAktualisieren2() == null) {
			assert bo.getAktualisieren3() == null;

			bo.setAktualisieren2(ok());
		}
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Aktualisieren2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=i6KFV5NhGwrs7JUSSs4O
// END NUCLOS CODEGENERATOR COMMENT
