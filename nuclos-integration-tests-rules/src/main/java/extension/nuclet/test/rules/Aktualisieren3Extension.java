package extension.nuclet.test.rules;

import org.nuclos.api.rule.UpdateRule; 
import org.nuclos.api.context.UpdateContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="Aktualisieren3Extension", description="Aktualisieren3Extension")
public class Aktualisieren3Extension extends AbstractTestRule implements UpdateRule {

	public void update(UpdateContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert bo.getAktualisieren() != null;
		assert bo.getAktualisieren2() != null;
		
		if (bo.getAktualisieren3() == null) {
			bo.setAktualisieren3(ok());
		}
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Aktualisieren3
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=wKsCTX3LBo6GcgY6HepZ
// END NUCLOS CODEGENERATOR COMMENT
