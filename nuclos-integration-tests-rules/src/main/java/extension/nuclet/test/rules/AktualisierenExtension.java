package extension.nuclet.test.rules;

import org.nuclos.api.rule.UpdateRule;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.exception.BusinessException;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "AktualisierenExtension", description = "AktualisierenExtension")
public class AktualisierenExtension extends AbstractTestRule implements UpdateRule {

	public void update(UpdateContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert invokeRecursive(bo, "getId") != null;

		assert Boolean.TRUE.equals(invokeRecursive(bo, "isUpdate"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isUnchanged"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isDelete"));

		if (bo.getAktualisieren() == null) {
			assert bo.getAktualisieren2() == null;
			assert bo.getAktualisieren3() == null;

			bo.setAktualisieren(ok());
		}

        Integer zahl = bo.getZahl();
        if (zahl != null && (zahl < 1 || zahl > 9)) {
            throw new BusinessException("Geben Sie als Zahl eine Ziffer zwischen 1 und 9 ein!");
        }
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Aktualisieren
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=D9jArHSJkiln40VR5LMv
// END NUCLOS CODEGENERATOR COMMENT
