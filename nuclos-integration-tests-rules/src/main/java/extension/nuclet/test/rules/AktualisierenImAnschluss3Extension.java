package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.UpdateFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="AktualisierenImAnschluss3Extension", description="AktualisierenImAnschluss3Extension")
public class AktualisierenImAnschluss3Extension extends AbstractTestRule implements UpdateFinalRule {

	public void updateFinal(UpdateContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		bo = QueryProvider.getById(TestRules.class, bo.getId());
		
		assert bo.getAktualisierenimanschluss() != null;
		assert bo.getAktualisierenimanschluss2() != null;

		if (bo.getAktualisierenimanschluss3() == null) {
			bo.setAktualisierenimanschluss3(ok());
			BusinessObjectProvider.update(bo);
		}
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.AktualisierenImAnschluss3
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=2XrT2y3Ofy83wRZcEbuC
// END NUCLOS CODEGENERATOR COMMENT
