package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.UpdateFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "AktualisierenImAnschlussExtension", description = "AktualisierenImAnschlussExtension")
public class AktualisierenImAnschlussExtension extends AbstractTestRule implements UpdateFinalRule {

	public void updateFinal(UpdateContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert invokeRecursive(bo, "getId") != null;

		assert Boolean.TRUE.equals(invokeRecursive(bo, "isUpdate"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isUnchanged"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isDelete"));

		bo = QueryProvider.getById(TestRules.class, bo.getId());

		if (bo.getAktualisierenimanschluss() == null) {
			assert bo.getAktualisierenimanschluss2() == null;
			assert bo.getAktualisierenimanschluss3() == null;

			bo.setAktualisierenimanschluss(ok());
			BusinessObjectProvider.update(bo);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.AktualisierenImAnschluss
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=G5uXORhJtB7Gh4nHlFfK
// END NUCLOS CODEGENERATOR COMMENT
