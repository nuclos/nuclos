package extension.nuclet.test.rules;

import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.context.InsertContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="Anlegen2Extension", description="Anlegen2Extension")
public class Anlegen2Extension extends AbstractTestRule implements InsertRule {

	public void insert(InsertContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		
		assert bo.getAnlegen() != null;
		assert bo.getAnlegen2() == null;
		assert bo.getAnlegen3() == null;

		bo.setAnlegen2(ok());
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Anlegen2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=yWRs9qcdashZehxfh2sE
// END NUCLOS CODEGENERATOR COMMENT
