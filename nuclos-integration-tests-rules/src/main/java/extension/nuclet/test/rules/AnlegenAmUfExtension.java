package extension.nuclet.test.rules;

import org.nuclos.api.rule.InsertRule; 
import org.nuclos.api.context.InsertContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;
import nuclet.test.rules.TestRulesSubformWithSM;

/** Siehe http://support.nuclos.de/browse/NUCLOS-7095
*/
@Rule(name="AnlegenAmUfExtension", description="Regel direkt am UF")
public class AnlegenAmUfExtension extends AbstractTestRule implements InsertRule {

	public void insert(InsertContext context) throws BusinessException {
		final TestRulesSubformWithSM testRulesSubformWithSM = context.getBusinessObject(TestRulesSubformWithSM.class);
		final TestRules bo = testRulesSubformWithSM.getTestrulesBO();
		if (!execute(bo)) {
			return;
		}
		// Pflichtfeld wird von dieser Regel gesetzt, die Validierung für dieses BO darf nicht vom Hauptobjekt vorgenommen werden (siehe genanntes Ticket oben)
		testRulesSubformWithSM.setPflichtfeld(getPflichtfeldText());
	}

	public String getPflichtfeldText() {
		return "Von Regel [" + AnlegenAmUfExtension.class.getCanonicalName() + "] gesetzt...";
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.AnlegenAmUf
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=1Xu2BHW6cjIe15WGx64x
// END NUCLOS CODEGENERATOR COMMENT
