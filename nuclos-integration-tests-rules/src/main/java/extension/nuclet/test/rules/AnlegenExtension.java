package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.rule.InsertRule; 
import org.nuclos.api.context.InsertContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="AnlegenExtension", description="AnlegenExtension")
public class AnlegenExtension extends AbstractTestRule implements InsertRule {

	public void insert(InsertContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert invokeRecursive(bo, "getId") == null;
		
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isUpdate"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isUnchanged"));
		assert Boolean.TRUE.equals(invokeRecursive(bo, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isDelete"));
		
		assert bo.getAnlegen() == null;
		assert bo.getAnlegen2() == null;
		assert bo.getAnlegen3() == null;

		bo.setAnlegen(ok());
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Anlegen
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=lG0NRlussvP6OwAah9Sq
// END NUCLOS CODEGENERATOR COMMENT
