package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.InsertFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "AnlegenImAnschluss2Extension", description = "AnlegenImAnschluss2Extension")
public class AnlegenImAnschluss2Extension extends AbstractTestRule implements InsertFinalRule {

	public void insertFinal(InsertContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		// BO has to be reloaded because the first InsertFinalRule updated the BO,
		// triggering UpdateFinalRules with updated the BO again -> stale version
		bo = QueryProvider.getById(TestRules.class, bo.getId());

		assert bo.getAnlegenimanschluss() != null;

		if (bo.getAnlegenimanschluss2() == null) {
			assert bo.getAnlegenimanschluss3() == null;

			bo.setAnlegenimanschluss2(ok());
			BusinessObjectProvider.update(bo);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.AnlegenImAnschluss2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=kMxdvvSxMqZsEVBtlDlG
// END NUCLOS CODEGENERATOR COMMENT
