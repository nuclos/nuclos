package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.InsertFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "AnlegenImAnschluss3Extension", description = "AnlegenImAnschluss3Extension")
public class AnlegenImAnschluss3Extension extends AbstractTestRule implements InsertFinalRule {

	public void insertFinal(InsertContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		// BO has to be reloaded because the first InsertFinalRule updated the BO,
		// triggering UpdateFinalRules with updated the BO again -> stale version
		bo = QueryProvider.getById(TestRules.class, bo.getId());

		assert bo.getAnlegenimanschluss() != null;
		assert bo.getAnlegenimanschluss2() != null;

		if (bo.getAnlegenimanschluss3() == null) {
			bo.setAnlegenimanschluss3(ok());
			BusinessObjectProvider.update(bo);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.AnlegenImAnschluss3
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=bduefD5hd8aFX4FyuZkl
// END NUCLOS CODEGENERATOR COMMENT
