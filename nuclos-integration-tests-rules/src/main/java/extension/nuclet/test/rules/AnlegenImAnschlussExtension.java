package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.rule.InsertFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "AnlegenImAnschlussExtension", description = "AnlegenImAnschlussExtension")
public class AnlegenImAnschlussExtension extends AbstractTestRule implements InsertFinalRule {

	public void insertFinal(InsertContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert invokeRecursive(bo, "getId") != null;

		assert Boolean.FALSE.equals(invokeRecursive(bo, "isUpdate"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isUnchanged"));
		assert Boolean.TRUE.equals(invokeRecursive(bo, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isDelete"));

		if (bo.getAnlegenimanschluss() == null) {
			assert bo.getAnlegenimanschluss2() == null;
			assert bo.getAnlegenimanschluss3() == null;

			bo.setAnlegenimanschluss(ok());
			BusinessObjectProvider.update(bo);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.AnlegenImAnschluss
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=5l9BNKRmajmDDIDlya6L
// END NUCLOS CODEGENERATOR COMMENT
