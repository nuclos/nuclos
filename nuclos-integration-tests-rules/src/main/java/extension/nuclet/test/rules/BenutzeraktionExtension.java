package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.CustomRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="BenutzeraktionExtension", description="BenutzeraktionExtension")
public class BenutzeraktionExtension extends AbstractTestRule implements CustomRule {

	public void custom(CustomContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		
        // TODO: isUpdate() is false when the CustomRule button is clicked in the Java client, but it is true if it the button is clicked in the web client
		// assert Boolean.FALSE.equals(invokeRecursive(bo, "isUpdate"));
		// assert Boolean.TRUE.equals(invokeRecursive(bo, "isUnchanged"));

		assert Boolean.FALSE.equals(invokeRecursive(bo, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isDelete"));
		
		bo.setBenutzeraktion(ok());

        Integer zahl = bo.getZahl();
        if (zahl != null && (zahl > 9 || zahl < 1)) {
            bo.setZahl(9);
        }
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Benutzeraktion
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=PyRKWybvA1Cps3tCCCnR
// END NUCLOS CODEGENERATOR COMMENT
