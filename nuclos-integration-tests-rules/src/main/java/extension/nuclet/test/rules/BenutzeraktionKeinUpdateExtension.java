package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.CustomRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="BenutzeraktionKeinUpdateExtension", description="BenutzeraktionKeinUpdateExtension")
public class BenutzeraktionKeinUpdateExtension extends AbstractTestRule implements CustomRule {

	public void custom(CustomContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		
        context.setUpdateAfterExecution(false);
        
        // TODO: isUpdate() is false when the CustomRule button is clicked in the Java client, but it is true if it the button is clicked in the web client
		// assert Boolean.FALSE.equals(invokeRecursive(bo, "isUpdate"));
		// assert Boolean.TRUE.equals(invokeRecursive(bo, "isUnchanged"));

		assert Boolean.FALSE.equals(invokeRecursive(bo, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isDelete"));
		
		bo.setBenutzeraktion(ok());
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.BenutzeraktionKeinUpdate
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=I8UOCbAF99LvYoDvkPvw
// END NUCLOS CODEGENERATOR COMMENT
