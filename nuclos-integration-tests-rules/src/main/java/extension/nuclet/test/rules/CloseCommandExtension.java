package extension.nuclet.test.rules;

import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.MessageContext;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.context.CustomContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.StateChangeRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="CloseCommandExtension", description="Sends an org.nuclos.api.command.CloseCommand to the client")
public class CloseCommandExtension extends AbstractTestRule implements CustomRule, InsertRule, StateChangeRule {

	public void custom(CustomContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		MessageContext.sendClose();
	}

	public void insert(final InsertContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		MessageContext.sendClose();
	}

	@Override
	public void changeState(final StateChangeContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		if ("CloseMeDuringStatechange".equals(bo.getName())) {
			MessageContext.sendClose();
		}
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.CloseCommand
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=Cis0xKaKhl9w3VSNt582
// END NUCLOS CODEGENERATOR COMMENT
