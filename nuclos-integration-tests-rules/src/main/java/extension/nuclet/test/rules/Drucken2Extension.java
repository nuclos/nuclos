package extension.nuclet.test.rules;

import org.nuclos.api.rule.PrintRule; 
import org.nuclos.api.context.PrintContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="Drucken2Extension", description="Drucken2Extension")
public class Drucken2Extension extends AbstractTestRule implements PrintRule {

	public void print(PrintContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		// nothing to do here? Where is the test?
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Drucken2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=trGVfAF1dNlJObOEpcFX
// END NUCLOS CODEGENERATOR COMMENT
