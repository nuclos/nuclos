package extension.nuclet.test.rules;

import org.nuclos.api.context.MessageContext;
import org.nuclos.api.rule.PrintFinalRule;
import org.nuclos.api.context.PrintFinalContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="DruckenImAnschluss2Extension", description="DruckenImAnschluss2Extension")
public class DruckenImAnschluss2Extension extends AbstractTestRule implements PrintFinalRule {

	public void printFinal(PrintFinalContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		// nothing to do here? Where is the test?

		MessageContext.sendMessage("DruckenImAnschluss2Extension!");
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.DruckenImAnschluss2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=zdyCjvLx4bzNcEtSQPKv
// END NUCLOS CODEGENERATOR COMMENT
