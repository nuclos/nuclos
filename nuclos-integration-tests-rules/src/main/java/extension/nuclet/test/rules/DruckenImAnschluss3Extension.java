package extension.nuclet.test.rules;

import org.nuclos.api.rule.PrintFinalRule; 
import org.nuclos.api.context.PrintFinalContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="DruckenImAnschluss3Extension", description="DruckenImAnschluss3Extension")
public class DruckenImAnschluss3Extension extends AbstractTestRule implements PrintFinalRule {

	public void printFinal(PrintFinalContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		// nothing to do here? Where is the test?
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.DruckenImAnschluss3
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=FyqtJHawImovGizIH19B
// END NUCLOS CODEGENERATOR COMMENT
