package extension.nuclet.test.rules;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import javax.ws.rs.GET;

import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.provider.StatemodelProvider;
import org.nuclos.api.rule.CustomRestRule;

import example.rest.ExampleorderSM;
import example.rest.Order;

public class GetByStateProcessExtension implements CustomRestRule {

	@GET
	public void deleteIfNecessary(CustomRestContext context) throws BusinessException {
		Collection<Order> col = QueryProvider.execute(
				QueryProvider.create(Order.class)
						.where(Order.NuclosLogicalDeleted.eq(true).or(Order.NuclosLogicalDeleted.eq(false)))
		);
		if (col.size() > 0) {
			BusinessObjectProvider.deleteAll(col);
		}
	}

    @GET
	public void getByState(CustomRestContext context) throws BusinessException {
		deleteIfNecessary(context);

		List<Order> ordersState10 = new ArrayList<>();
		List<Order> ordersState90 = new ArrayList<>();
		List<Order> ordersState10Deleted = new ArrayList<>();
		List<Order> ordersState90Deleted = new ArrayList<>();

		Random random = new Random();

		for (int i = 0; i < 10; i++) {
			Order order = new Order();
			order.setOrderNumber(i);
			BusinessObjectProvider.insert(order);

			if (random.nextBoolean()) {
				StatemodelProvider.changeState(order, ExampleorderSM.State_90);
				ordersState90.add(order);
			} else {
				ordersState10.add(order);
			}
		}

		List<Order> queriedOrdersState90 = QueryProvider.getByState(Order.class, ExampleorderSM.State_90);
		if (queriedOrdersState90.size() != ordersState90.size()) {
			throw new BusinessException("queried orders by state 90: " + queriedOrdersState90 + "\norders with state 90: " + ordersState90.size());
		}

		List<Order> queriedOrdersState10 = QueryProvider.getByState(Order.class, ExampleorderSM.State_10);
		if (queriedOrdersState10.size() != ordersState10.size()) {
			throw new BusinessException("queried orders by state 10: " + queriedOrdersState10 + "\norders with state 10: " + ordersState10.size());
		}

		int n = ordersState10.size();
		for (int i = 0; i < n; i++) {
			Order order = ordersState10.get(i);
			if (random.nextBoolean()) {
				BusinessObjectProvider.deleteLogical(order);
				ordersState10Deleted.add(order);
			}
		}

		List<Order> queriedOrdersState10NotDeleted = QueryProvider.getByState(Order.class, ExampleorderSM.State_10);
		if (queriedOrdersState10NotDeleted.size() != ordersState10.size() - ordersState10Deleted.size()) {
			throw new BusinessException("queried orders by state 10: " + queriedOrdersState10NotDeleted + "\norders with state 10 not deleted: " + (ordersState10.size() - ordersState10Deleted.size()));
		}

		n = ordersState90.size();
		for (int i = 0; i < n; i++) {
			Order order = ordersState90.get(i);
			if (random.nextBoolean()) {
				BusinessObjectProvider.deleteLogical(order);
				ordersState90Deleted.add(order);
			}
		}

		List<Order> queriedOrdersState90NotDeleted = QueryProvider.getByState(Order.class, ExampleorderSM.State_90);
		if (queriedOrdersState90NotDeleted.size() != ordersState90.size() - ordersState90Deleted.size()) {
			throw new BusinessException("queried orders by state 90: " + queriedOrdersState90NotDeleted + "\norders with state 90 not deleted: " + (ordersState90.size() - ordersState90Deleted.size()));
		}

	}

    @GET
	public void getByProcess(CustomRestContext context) throws BusinessException {
		deleteIfNecessary(context);

		List<Order> processPriority = new ArrayList<>();
		List<Order> processStandard = new ArrayList<>();
		List<Order> processPriorityDeleted = new ArrayList<>();
		List<Order> processStandardDeleted = new ArrayList<>();

		Random random = new Random();

		for (int i = 0; i < 10; i++) {
			Order order = new Order();
			order.setOrderNumber(i);

			if (random.nextBoolean()) {
				order.setNuclosProcess(Order.Priorityorder);
				processPriority.add(order);
			} else {
				order.setNuclosProcess(Order.Standardorder);
				processStandard.add(order);
			}

			BusinessObjectProvider.insert(order);
		}

		List<Order> queriedProcessPriority = QueryProvider.getByProcess(Order.Priorityorder);
		if (queriedProcessPriority.size() != processPriority.size()) {
			throw new BusinessException("queried orders by process 'Priorityorder': " + queriedProcessPriority + "\norders with process 'Priorityorder': " + processPriority.size());
		}

		List<Order> queriedProcessStandard = QueryProvider.getByProcess(Order.Standardorder);
		if (queriedProcessStandard.size() != processStandard.size()) {
			throw new BusinessException("queried orders by process 'Standardorder': " + queriedProcessStandard + "\norders with process 'Standardorder': " + processStandard.size());
		}

		int n = processPriority.size();
		for (int i = 0; i < n; i++) {
			Order order = processPriority.get(i);
			if (random.nextBoolean()) {
				BusinessObjectProvider.deleteLogical(order);
				processPriorityDeleted.add(order);
			}
		}

		List<Order> queriedOrdersProcessPriorityDeleted = QueryProvider.getByProcess(Order.Priorityorder);
		if (queriedOrdersProcessPriorityDeleted.size() != processPriority.size() - processPriorityDeleted.size()) {
			throw new BusinessException("queried orders by process 'Priorityorder': " + queriedOrdersProcessPriorityDeleted + "\norders with process 'Priorityorder' not deleted: " + (processPriority.size() - processPriorityDeleted.size()));
		}

		n = processStandard.size();
		for (int i = 0; i < n; i++) {
			Order order = processStandard.get(i);
			if (random.nextBoolean()) {
				BusinessObjectProvider.deleteLogical(order);
				processStandardDeleted.add(order);
			}
		}

		List<Order> queriedOrdersProcessStandardDeleted = QueryProvider.getByProcess(Order.Standardorder);
		if (queriedOrdersProcessStandardDeleted.size() != processStandard.size() - processStandardDeleted.size()) {
			throw new BusinessException("queried orders by process 'Standardorder': " + queriedOrdersProcessStandardDeleted + "\norders with process 'Standardorder' not deleted: " + (processStandard.size() - processStandardDeleted.size()));
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.GetByStateProcess
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=W5hrq8GisTYrZ4kRrKf1
// END NUCLOS CODEGENERATOR COMMENT
