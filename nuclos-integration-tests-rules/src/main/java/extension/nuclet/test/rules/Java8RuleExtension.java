package extension.nuclet.test.rules;

import java.util.ArrayList;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.InsertRule;
import org.springframework.beans.factory.annotation.Autowired;

import extension.nuclet.test.bean.TestSpringComponent;
import nuclet.test.rules.TestRules;

/**
 * @name
 * @description
 * @usage
 * @change
 */
@Rule(name = "Java8RuleExtension", description = "Java8RuleExtension")
public class Java8RuleExtension extends AbstractTestRule implements InsertRule {

	protected TestSpringComponent testSpringComponent;

	@Autowired
	protected void setTestSpringComponent(final TestSpringComponent testSpringComponent) {
		this.testSpringComponent = testSpringComponent;
	}

	protected void testSpringComponent() throws BusinessException {
		if (testSpringComponent == null
				|| testSpringComponent.getTest() == null
				|| !testSpringComponent.getTest().equals(TestSpringComponent.TEST)) {
			throw new BusinessException("Autowiring failed (Rule from extension)");
		}
	}

	public void insert(InsertContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		// Test the Spring autowiring feature here
		testSpringComponent();

		// Some Java 8 syntax, which caused Parser-Exceptions before
		new ArrayList<String>().forEach(context::log);

		Runnable r = () -> bo.setJava8(ok());
		r.run();
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Java8Rule
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=FmjLmnJxSbQnLiWFAXmA
// END NUCLOS CODEGENERATOR COMMENT
