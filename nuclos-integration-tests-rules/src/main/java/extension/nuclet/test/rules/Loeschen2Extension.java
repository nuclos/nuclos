package extension.nuclet.test.rules;

import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.DeleteRule; 
import org.nuclos.api.context.DeleteContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="Loeschen2Extension", description="Loeschen2Extension")
public class Loeschen2Extension extends AbstractTestRule implements DeleteRule {

	public void delete(DeleteContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		TestRules referenz = QueryProvider.getById(TestRules.class, bo.getReferenzId());

		if (referenz != null) {
			assert referenz.getLoeschen() != null;

			if (referenz.getLoeschen2() == null) {
				assert referenz.getLoeschen3() == null;

				referenz.setLoeschen2(ok());
				BusinessObjectProvider.update(referenz);
			}
		}
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Loeschen2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=1G0raSvPQRcNoDn3zDiF
// END NUCLOS CODEGENERATOR COMMENT
