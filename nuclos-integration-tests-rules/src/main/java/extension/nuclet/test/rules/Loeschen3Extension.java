package extension.nuclet.test.rules;

import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.DeleteRule; 
import org.nuclos.api.context.DeleteContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="Loeschen3Extension", description="Loeschen3Extension")
public class Loeschen3Extension extends AbstractTestRule implements DeleteRule {

	public void delete(DeleteContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		TestRules referenz = QueryProvider.getById(TestRules.class, bo.getReferenzId());

		if (referenz != null) {

			assert referenz.getLoeschen() != null;
			assert referenz.getLoeschen2() != null;

			if (referenz.getLoeschen3() == null) {
				referenz.setLoeschen3(ok());
				BusinessObjectProvider.update(referenz);
			}
		}
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Loeschen3
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=OsHGfrhYRMz1NSUEaDf1
// END NUCLOS CODEGENERATOR COMMENT
