package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.DeleteRule;
import org.nuclos.api.context.DeleteContext;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "LoeschenExtension", description = "LoeschenExtension")
public class LoeschenExtension extends AbstractTestRule implements DeleteRule {

	public void delete(DeleteContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert invokeRecursive(bo, "getId") != null;

		assert Boolean.FALSE.equals(invokeRecursive(bo, "isUpdate"));
		assert Boolean.TRUE.equals(invokeRecursive(bo, "isUnchanged"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isDelete"));

		TestRules referenz = QueryProvider.getById(TestRules.class, bo.getReferenzId());
		
		if (referenz != null && referenz.getLoeschen() == null) {
			assert referenz.getLoeschen2() == null;
			assert referenz.getLoeschen3() == null;

			referenz.setLoeschen(ok());
			BusinessObjectProvider.update(referenz);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Loeschen
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=fXxdCo3bSh5vUSLDzamz
// END NUCLOS CODEGENERATOR COMMENT
