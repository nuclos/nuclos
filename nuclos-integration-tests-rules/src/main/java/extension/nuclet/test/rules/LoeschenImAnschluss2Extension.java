package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.DeleteContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.DeleteFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "LoeschenImAnschluss2Extension", description = "LoeschenImAnschluss2Extension")
public class LoeschenImAnschluss2Extension extends AbstractTestRule implements DeleteFinalRule {

	public void deleteFinal(DeleteContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		TestRules referenz = QueryProvider.getById(TestRules.class, bo.getReferenzId());

		if (referenz != null) {
			assert referenz.getLoeschenimanschluss() != null;

			if (referenz.getLoeschenimanschluss2() == null) {
				assert referenz.getLoeschenimanschluss3() == null;

				referenz.setLoeschenimanschluss2(ok());
				BusinessObjectProvider.update(referenz);
			}
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.LoeschenImAnschluss2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=zPVtsoAHweR0AojeqaPE
// END NUCLOS CODEGENERATOR COMMENT
