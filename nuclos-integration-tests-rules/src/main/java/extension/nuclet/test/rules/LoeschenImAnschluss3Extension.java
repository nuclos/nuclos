package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.DeleteContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.DeleteFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "LoeschenImAnschluss3Extension", description = "LoeschenImAnschluss3Extension")
public class LoeschenImAnschluss3Extension extends AbstractTestRule implements DeleteFinalRule {

	public void deleteFinal(DeleteContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		TestRules referenz = QueryProvider.getById(TestRules.class, bo.getReferenzId());

		if (referenz != null) {
			assert referenz.getLoeschenimanschluss() != null;
			assert referenz.getLoeschenimanschluss2() != null;

			if (referenz.getLoeschenimanschluss3() == null) {
				referenz.setLoeschenimanschluss3(ok());
				BusinessObjectProvider.update(referenz);
			}
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.LoeschenImAnschluss3
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=DtIu9AtsvEr69WoIn91C
// END NUCLOS CODEGENERATOR COMMENT
