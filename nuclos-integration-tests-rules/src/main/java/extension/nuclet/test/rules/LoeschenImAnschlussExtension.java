package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.DeleteContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.DeleteFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "LoeschenImAnschlussExtension", description = "LoeschenImAnschlussExtension")
public class LoeschenImAnschlussExtension extends AbstractTestRule implements DeleteFinalRule {

	public void deleteFinal(DeleteContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert invokeRecursive(bo, "getId") != null;

		assert Boolean.FALSE.equals(invokeRecursive(bo, "isUpdate"));
		assert Boolean.TRUE.equals(invokeRecursive(bo, "isUnchanged"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isDelete"));

		TestRules referenz = QueryProvider.getById(TestRules.class, bo.getReferenzId());

		if (referenz != null && referenz.getLoeschenimanschluss() == null) {
			assert referenz.getLoeschenimanschluss2() == null;
			assert referenz.getLoeschenimanschluss3() == null;

			referenz.setLoeschenimanschluss(ok());
			BusinessObjectProvider.update(referenz);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.LoeschenImAnschluss
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=uIsr7rYnH4Ho7QNSjVS5
// END NUCLOS CODEGENERATOR COMMENT
