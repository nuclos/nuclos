package extension.nuclet.test.rules;

import org.nuclos.api.rule.CustomRule; 
import org.nuclos.api.context.CustomContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException; 
import org.nuclos.api.context.MessageContext;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="NavigateToBOExtension", description="NavigateToBOExtension")
public class NavigateToBOExtension extends AbstractTestRule implements CustomRule {

	public void custom(CustomContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
        MessageContext.navigate(new TestRules(), true);
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.NavigateToBO
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=d7Twn4H8FJrAnglcCQzq
// END NUCLOS CODEGENERATOR COMMENT
