package extension.nuclet.test.rules;

import java.net.MalformedURLException;
import java.net.URL;

import org.nuclos.api.UID;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.context.MessageContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.CustomRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="NavigateToUrlExtension", description="NavigateToUrlExtension")
public class NavigateToUrlExtension extends AbstractTestRule implements CustomRule {

	public void custom(CustomContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		try {
			MessageContext.navigate(new URL("https://www.nuclos.de/"), true);
		} catch (MalformedURLException e) {
			throw new BusinessException(e);
		}
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.NavigateToBO
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=d7Twn4H8FJrAnglcCQzq
// END NUCLOS CODEGENERATOR COMMENT
