package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.GenerateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.GenerateRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="Objektgenerator2Extension", description="Objektgenerator2Extension")
public class Objektgenerator2Extension extends AbstractTestRule implements GenerateRule {

	public void generate(GenerateContext context) throws BusinessException {
		TestRules source = context.getSourceObject(TestRules.class);
		if (!execute(source)) {
			return;
		}

		assert source.getObjektgenerator() != null;

		if (source.getObjektgenerator2() == null) {
			assert source.getObjektgenerator3() == null;

			source.setObjektgenerator2(ok());
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Objektgenerator2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=zKwbohzlzkZiVw8yfCMh
// END NUCLOS CODEGENERATOR COMMENT
