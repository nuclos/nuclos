package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.GenerateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.GenerateRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="Objektgenerator3Extension", description="Objektgenerator3Extension")
public class Objektgenerator3Extension extends AbstractTestRule implements GenerateRule {

	public void generate(GenerateContext context) throws BusinessException { 
		TestRules source = context.getSourceObject(TestRules.class);
		if (!execute(source)) {
			return;
		}

		assert source.getObjektgenerator() != null;
		assert source.getObjektgenerator2() != null;

		if (source.getObjektgenerator3() == null) {
			source.setObjektgenerator3(ok());
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Objektgenerator3
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=y5jpvoL1Qmkm3nMRjzeQ
// END NUCLOS CODEGENERATOR COMMENT
