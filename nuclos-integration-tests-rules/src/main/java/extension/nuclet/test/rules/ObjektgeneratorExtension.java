package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.GenerateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.GenerateRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "ObjektgeneratorExtension", description = "ObjektgeneratorExtension")
public class ObjektgeneratorExtension extends AbstractTestRule implements GenerateRule {

	public void generate(GenerateContext context) throws BusinessException {
		TestRules source = context.getSourceObject(TestRules.class);
		if (!execute(source)) {
			return;
		}
		TestRules target = context.getTargetObject(TestRules.class);

		assert invokeRecursive(source, "getId") != null;
		assert invokeRecursive(target, "getId") == null;

		assert Boolean.FALSE.equals(invokeRecursive(source, "isUpdate"));
		assert Boolean.TRUE.equals(invokeRecursive(source, "isUnchanged"));
		assert Boolean.FALSE.equals(invokeRecursive(source, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(source, "isDelete"));

		assert Boolean.FALSE.equals(invokeRecursive(target, "isUpdate"));
		assert Boolean.TRUE.equals(invokeRecursive(target, "isUnchanged"));
		assert Boolean.FALSE.equals(invokeRecursive(target, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(target, "isDelete"));

		target.setName(source.getName() + "_2");
		target.setReferenzId(source.getId());
		target.setRegelnausextension(source.getRegelnausextension());

		if (source.getObjektgenerator() == null) {
			assert source.getObjektgenerator2() == null;
			assert source.getObjektgenerator3() == null;

			source.setObjektgenerator(ok());
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Objektgenerator
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=mhED4m238twKzWk9z0tz
// END NUCLOS CODEGENERATOR COMMENT
