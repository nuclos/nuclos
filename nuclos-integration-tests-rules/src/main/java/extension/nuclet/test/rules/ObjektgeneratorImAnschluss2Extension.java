package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.GenerateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.rule.GenerateFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "ObjektgeneratorImAnschluss2Extension", description = "ObjektgeneratorImAnschluss2Extension")
public class ObjektgeneratorImAnschluss2Extension extends AbstractTestRule implements GenerateFinalRule {

	public void generateFinal(GenerateContext context) throws BusinessException {
		TestRules source = context.getSourceObject(TestRules.class);
		if (!execute(source)) {
			return;
		}

		assert source.getObjektgeneratorimanschluss() != null;

		if (source.getObjektgeneratorimanschluss2() == null) {
			assert source.getObjektgeneratorimanschluss3() == null;

			source.setObjektgeneratorimanschluss2(ok());
			BusinessObjectProvider.update(source);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.ObjektgeneratorImAnschluss2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=042P4MIEK76JuH0PRVwi
// END NUCLOS CODEGENERATOR COMMENT
