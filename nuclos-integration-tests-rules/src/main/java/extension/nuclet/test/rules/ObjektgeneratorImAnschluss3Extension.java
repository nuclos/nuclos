package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.GenerateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.rule.GenerateFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "ObjektgeneratorImAnschluss3Extension", description = "ObjektgeneratorImAnschluss3Extension")
public class ObjektgeneratorImAnschluss3Extension extends AbstractTestRule implements GenerateFinalRule {

	public void generateFinal(GenerateContext context) throws BusinessException {
		TestRules source = context.getSourceObject(TestRules.class);
		if (!execute(source)) {
			return;
		}

		assert source.getObjektgeneratorimanschluss() != null;
		assert source.getObjektgeneratorimanschluss2() != null;

		if (source.getObjektgeneratorimanschluss3() == null) {
			source.setObjektgeneratorimanschluss3(ok());
			BusinessObjectProvider.update(source);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.ObjektgeneratorImAnschluss3
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=kdecOZNyPNRw3qb2qz9F
// END NUCLOS CODEGENERATOR COMMENT
