package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.GenerateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.rule.GenerateFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "ObjektgeneratorImAnschlussExtension", description = "ObjektgeneratorImAnschlussExtension")
public class ObjektgeneratorImAnschlussExtension extends AbstractTestRule implements GenerateFinalRule {

	public void generateFinal(GenerateContext context) throws BusinessException {
		TestRules source = context.getSourceObject(TestRules.class);
		if (!execute(source)) {
			return;
		}
		TestRules target = context.getTargetObject(TestRules.class);

		assert invokeRecursive(source, "getId") != null;
		assert invokeRecursive(target, "getId") != null;

		assert Boolean.TRUE.equals(invokeRecursive(source, "isUpdate"));
		assert Boolean.FALSE.equals(invokeRecursive(source, "isUnchanged"));
		assert Boolean.FALSE.equals(invokeRecursive(source, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(source, "isDelete"));

		assert Boolean.FALSE.equals(invokeRecursive(target, "isUpdate"));
		assert Boolean.TRUE.equals(invokeRecursive(target, "isUnchanged"));
		assert Boolean.FALSE.equals(invokeRecursive(target, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(target, "isDelete"));

		if (source.getObjektgeneratorimanschluss() == null) {
			assert source.getObjektgeneratorimanschluss2() == null;
			assert source.getObjektgeneratorimanschluss3() == null;

			source.setObjektgeneratorimanschluss(ok());

			BusinessObjectProvider.update(source);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.ObjektgeneratorImAnschluss
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=h3K7BdVNfhcW6SYXuTCG
// END NUCLOS CODEGENERATOR COMMENT
