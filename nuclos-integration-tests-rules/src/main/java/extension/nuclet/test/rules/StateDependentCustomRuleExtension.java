package extension.nuclet.test.rules;

import org.nuclos.api.rule.CustomRule; 
import org.nuclos.api.context.CustomContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="StateDependentCustomRuleExtension", description="Should be available only in a certain state.")
public class StateDependentCustomRuleExtension extends AbstractTestRule implements CustomRule {

	public void custom(CustomContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.StateDependentCustomRule
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=GcjxxoRFCaFfURvCcVMf
// END NUCLOS CODEGENERATOR COMMENT
