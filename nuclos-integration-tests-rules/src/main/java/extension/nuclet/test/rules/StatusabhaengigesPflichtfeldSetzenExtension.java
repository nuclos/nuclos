package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.StateChangeRule;

import nuclet.test.rules.TestRules;

/** @name
  * @description
  * @usage
  * @change
*/
@Rule(name = "StatusabhaengigesPflichtfeldSetzenExtension", description = "StatusabhaengigesPflichtfeldSetzenExtension")
public class StatusabhaengigesPflichtfeldSetzenExtension extends AbstractTestRule implements StateChangeRule {

	public void changeState(StateChangeContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		bo.setStatusabhaengigespflichtf(ok());
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.StatusabhaengigesPflichtfeldSetzen
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=KH4JFBP9sL6oh3SyTLbj
// END NUCLOS CODEGENERATOR COMMENT
