package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.StateChangeRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="Statuswechsel2Extension", description="Statuswechsel2Extension")
public class Statuswechsel2Extension extends AbstractTestRule implements StateChangeRule {

	public void changeState(StateChangeContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert bo.getStatuswechsel() != null;

		if (bo.getStatuswechsel2() == null) {
			assert bo.getStatuswechsel3() == null;

			bo.setStatuswechsel2(ok());
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Statuswechsel2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=K8VizgZJ1GoxA1IW85NS
// END NUCLOS CODEGENERATOR COMMENT
