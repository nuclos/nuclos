package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.StateChangeRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="Statuswechsel3Extension", description="Statuswechsel3Extension")
public class Statuswechsel3Extension extends AbstractTestRule implements StateChangeRule {

	public void changeState(StateChangeContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert bo.getStatuswechsel() != null;
		assert bo.getStatuswechsel2() != null;

		if (bo.getStatuswechsel3() == null) {
			bo.setStatuswechsel3(ok());
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Statuswechsel3
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=cCWPb8X5RnoONwXjO74r
// END NUCLOS CODEGENERATOR COMMENT
