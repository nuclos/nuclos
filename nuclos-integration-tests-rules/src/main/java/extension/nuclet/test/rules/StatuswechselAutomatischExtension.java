package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.rule.StateChangeFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "StatuswechselAutomatischExtension", description = "StatuswechselAutomatischExtension")
public class StatuswechselAutomatischExtension extends AbstractTestRule implements StateChangeFinalRule {

	public void changeStateFinal(StateChangeContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		if (context.getSourceState().getNumeral() == 30 && context.getTargetState().getNumeral() == 10) {
			bo.setStatuswechselautomatisch(ok());
			BusinessObjectProvider.update(bo);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.StatuswechselAutomatisch
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=mSC4UzmKhX6DcaAuEFhP
// END NUCLOS CODEGENERATOR COMMENT
