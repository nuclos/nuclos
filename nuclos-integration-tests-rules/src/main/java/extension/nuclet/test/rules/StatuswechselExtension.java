package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.StateChangeRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="StatuswechselExtension", description="StatuswechselExtension")
public class StatuswechselExtension extends AbstractTestRule implements StateChangeRule {

	public void changeState(StateChangeContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		
		assert context.getSourceState() != null;
		assert context.getSourceStateId() != null;
		assert context.getSourceStateNumber() != null;

		
		assert context.getTargetState() != null;
		assert context.getTargetStateId() != null;
		assert context.getTargetStateNumber() != null;
		
		assert invokeRecursive(bo, "getId") != null;
		
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isUpdate"));
		assert Boolean.TRUE.equals(invokeRecursive(bo, "isUnchanged"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isDelete"));
		
		if (bo.getStatuswechsel() == null) {
			assert bo.getStatuswechsel2() == null;
			assert bo.getStatuswechsel3() == null;

			bo.setStatuswechsel(ok());
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.Statuswechsel
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=2R2ON61SGkvN291mCldK
// END NUCLOS CODEGENERATOR COMMENT
