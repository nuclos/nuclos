package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.StateChangeFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "StatuswechselImAnschluss2Extension", description = "StatuswechselImAnschluss2Extension")
public class StatuswechselImAnschluss2Extension extends AbstractTestRule implements StateChangeFinalRule {

	public void changeStateFinal(StateChangeContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		bo = QueryProvider.getById(TestRules.class, bo.getId());

		assert bo.getStatuswechselimanschluss() != null;

		if (bo.getStatuswechselimanschluss2() == null) {
			assert bo.getStatuswechselimanschluss3() == null;

			bo.setStatuswechselimanschluss2(ok());
			BusinessObjectProvider.update(bo);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.StatuswechselImAnschluss2
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=H0ZB3oapSm1IEtWGeQfQ
// END NUCLOS CODEGENERATOR COMMENT
