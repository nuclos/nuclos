package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.StateChangeFinalRule;

import nuclet.test.rules.TestRules;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="StatuswechselImAnschluss3Extension", description="StatuswechselImAnschluss3Extension")
public class StatuswechselImAnschluss3Extension extends AbstractTestRule implements StateChangeFinalRule {

	public void changeStateFinal(StateChangeContext context) throws BusinessException { 
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		bo = QueryProvider.getById(TestRules.class, bo.getId());

		assert bo.getStatuswechselimanschluss() != null;
		assert bo.getStatuswechselimanschluss2() != null;

		if (bo.getStatuswechselimanschluss3() == null) {
			bo.setStatuswechselimanschluss3(ok());
			BusinessObjectProvider.update(bo);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.StatuswechselImAnschluss3
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=2IAo00WYjVg5Uyj3iW61
// END NUCLOS CODEGENERATOR COMMENT
