package extension.nuclet.test.rules;

import static extension.nuclet.test.utils.ReflectionMethodsExtension.invokeRecursive;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.StatemodelProvider;
import org.nuclos.api.rule.StateChangeFinalRule;

import nuclet.test.rules.TestRules;
import nuclet.test.rules.TestRulesSM;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name = "StatuswechselImAnschlussExtension", description = "StatuswechselImAnschlussExtension")
public class StatuswechselImAnschlussExtension extends AbstractTestRule implements StateChangeFinalRule {

	public void changeStateFinal(StateChangeContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		assert context.getSourceState() != null;
		assert context.getSourceStateId() != null;
		assert context.getSourceStateNumber() != null;

		assert context.getTargetState() != null;
		assert context.getTargetStateId() != null;
		assert context.getTargetStateNumber() != null;

		assert invokeRecursive(bo, "getId") != null;

		assert Boolean.FALSE.equals(invokeRecursive(bo, "isUpdate"));
		assert Boolean.TRUE.equals(invokeRecursive(bo, "isUnchanged"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isInsert"));
		assert Boolean.FALSE.equals(invokeRecursive(bo, "isDelete"));

		if (bo.getStatuswechselimanschluss() == null) {
			assert bo.getStatuswechselimanschluss2() == null;
			assert bo.getStatuswechselimanschluss3() == null;

			bo.setStatuswechselimanschluss(ok());
			BusinessObjectProvider.update(bo);
		}

		if (bo.getNuclosStateNumber() == 30) {
			StatemodelProvider.changeState(bo, TestRulesSM.State_10);
		}
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.StatuswechselImAnschluss
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=OAkMOrbiqmA4mWbmAflJ
// END NUCLOS CODEGENERATOR COMMENT
