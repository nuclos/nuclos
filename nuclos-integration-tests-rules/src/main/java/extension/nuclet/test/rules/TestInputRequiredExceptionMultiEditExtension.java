package extension.nuclet.test.rules;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.context.GenerateContext;
import org.nuclos.api.context.InputContext;
import org.nuclos.api.context.InputRequiredException;
import org.nuclos.api.context.InputSpecification;
import org.nuclos.api.context.StateChangeContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.StatemodelProvider;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.GenerateFinalRule;
import org.nuclos.api.rule.GenerateRule;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;

import nuclet.test.rules.TestRules;
import nuclet.test.rules.TestRulesSM;

/**
 * @name
 * @description
 * @usage
 * @change
 */
@Rule(name = "TestInputRequiredExceptionMultiEditExtension", description = "TestInputRequiredExceptionMultiEditExtension")
public class TestInputRequiredExceptionMultiEditExtension extends AbstractTestRule implements CustomRule, StateChangeRule, StateChangeFinalRule, GenerateRule, GenerateFinalRule {

	public void custom(CustomContext context) throws BusinessException {
		assert InputContext.isSupported();

		boolean ok = doInputRequired();

		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		bo.setInputrequiredexceptions(ok ? ok() : null);
	}

	private boolean doInputRequired() {
		String[] options = {"Option 1", "Option 2", "Option 3"};
		String defaultOption = options[1];

		return doConfirmYes1()
				&& doConfirmNo1()
				&& doConfirmOk()
				&& doConfirmDefaultOption(options, defaultOption)
				&& doSelectOption3(options, defaultOption)
				&& doInputValue()
				&& doInputMemo();
	}

	public void generate(final GenerateContext context) throws BusinessException {
		TestRules bo = context.getSourceObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		boolean ok = doConfirmYes1() && doConfirmNo1();

		bo.setInputrequiredgeneraterule(ok ? ok() : "");
	}

	public void generateFinal(final GenerateContext context) throws BusinessException {
		TestRules bo = context.getSourceObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		boolean ok = doConfirmYes2() && doConfirmNo2();

		bo.setInputrequiredgeneratefinal(ok ? ok() : "");
		BusinessObjectProvider.update(bo);
	}

	public void changeState(final StateChangeContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		boolean ok = doConfirmYes1() && doConfirmNo1();

		bo.setInputrequiredstatechange(ok ? ok() : "");
	}

	public void changeStateFinal(final StateChangeContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}

		boolean ok = doConfirmYes2() && doConfirmNo2();

		bo.setInputrequiredstatechangefin(ok ? ok() : "");

		BusinessObjectProvider.update(bo);
		StatemodelProvider.changeState(bo, TestRulesSM.State_20);
	}

	private boolean doConfirmYes1() {
		return doConfirmYes("confirmYes1");
	}

	private boolean doConfirmYes2() {
		return doConfirmYes("confirmYes2");
	}

	private boolean doConfirmYes(final String context) {
		final Integer iValue = (Integer) InputContext.get(context);

		if (iValue == null) {
			InputSpecification spec = new InputSpecification(InputSpecification.CONFIRM_YES_NO, context, "Mehrzeilige InputRequired Exception\n\tMit Einrückung\n\nBitte \"Ja\" wählen");
			spec.setApplyForAllMultiEditObjects(true);
			throw new InputRequiredException(spec);
		}

		return iValue == InputSpecification.YES;
	}

	private boolean doConfirmNo1() {
		return doConfirmNo("confirmNo1");
	}

	private boolean doConfirmNo2() {
		return doConfirmNo("confirmNo2");
	}

	private boolean doConfirmNo(final String context) {
		final Integer iValue = (Integer) InputContext.get(context);

		if (iValue == null) {
			InputSpecification spec = new InputSpecification(InputSpecification.CONFIRM_YES_NO, context, "Bitte \"Nein\" wählen");
			spec.setApplyForAllMultiEditObjects(true);
			throw new InputRequiredException(spec);
		}
		return iValue == InputSpecification.NO;
	}

	private boolean doConfirmOk() {
		final Integer iValue = (Integer) InputContext.get("confirmOk");

		if (iValue == null) {
			InputSpecification spec = new InputSpecification(InputSpecification.CONFIRM_OK_CANCEL, "confirmOk", "Bitte \"OK\" wählen");
			spec.setApplyForAllMultiEditObjects(true);
			throw new InputRequiredException(spec);
		}

		return iValue == InputSpecification.OK;
	}

	private boolean doConfirmDefaultOption(final String[] options, final String defaultOption) {
		String strValue = (String) InputContext.get("inputDefaultOption");

		if (strValue == null) {
			InputSpecification spec = new InputSpecification(
					InputSpecification.INPUT_OPTION,
					"inputDefaultOption",
					"Bitte Default-Option bestätigen"
			);
			spec.setOptions(options);
			spec.setDefaultOption(defaultOption);
			spec.setApplyForAllMultiEditObjects(true);
			throw new InputRequiredException(spec);
		}

		return strValue.equals(defaultOption);
	}

	private boolean doSelectOption3(final String[] options, final String defaultOption) {
		final String strValue = (String) InputContext.get("inputOption3");

		if (strValue == null) {
			InputSpecification spec = new InputSpecification(InputSpecification.INPUT_OPTION, "inputOption3", "Bitte Option 3 wählen");
			spec.setOptions(options);
			spec.setDefaultOption(defaultOption);
			spec.setApplyForAllMultiEditObjects(true);
			throw new InputRequiredException(spec);
		}

		return strValue.equals(options[2]);
	}

	private boolean doInputValue() {
		final String strValue = (String) InputContext.get("inputValue");

		if (strValue == null) {
			InputSpecification spec = new InputSpecification(InputSpecification.INPUT_VALUE, "inputValue", "Bitte \"Test 123\" eingeben");
			spec.setApplyForAllMultiEditObjects(true);
			throw new InputRequiredException(spec);
		}

		return strValue.equals("Test 123");
	}

	private boolean doInputMemo() {
		final String strMemo = (String) InputContext.get("inputMemo");

		if (strMemo == null) {
			InputSpecification spec = new InputSpecification(InputSpecification.INPUT_MEMO, "inputMemo", "Bitte einen \"Test mit Zeilen-\numbruch\" eingeben");
			spec.setApplyForAllMultiEditObjects(true);
			throw new InputRequiredException(spec);
		}

		return strMemo.equals("Test mit Zeilen-\numbruch");
	}
}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.TestInputRequiredExceptionMultiEdit
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=fmINhVnsegrh7RqnMFI5
// END NUCLOS CODEGENERATOR COMMENT
