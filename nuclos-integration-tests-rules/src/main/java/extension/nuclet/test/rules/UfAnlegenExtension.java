package extension.nuclet.test.rules;

import java.util.List;

import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.context.InsertContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException;

import nuclet.test.rules.TestRules;
import nuclet.test.rules.TestRulesSubformWithSM;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="UfAnlegenExtension", description="Bearbeitet UF")
public class UfAnlegenExtension extends AbstractTestRule implements InsertRule, InsertFinalRule {

	public void insert(InsertContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		final TestRulesSubformWithSM testRulesSubformWithSM = new TestRulesSubformWithSM();
		bo.insertTestRulesSubformWithSM(testRulesSubformWithSM);
	}

	@Override
	public void insertFinal(final InsertContext context) throws BusinessException {
		TestRules bo = context.getBusinessObject(TestRules.class);
		if (!execute(bo)) {
			return;
		}
		final TestRules testRules = QueryProvider.getById(TestRules.class, bo.getId());
		final List<TestRulesSubformWithSM> testRulesSubformWithSM = testRules.getTestRulesSubformWithSM();
		final String pflichtfeldText = getPflichtfeldText();
		Boolean test1 = null;
		for (TestRulesSubformWithSM rulesSubformWithSM : testRulesSubformWithSM) {
			if (pflichtfeldText.equals(rulesSubformWithSM.getPflichtfeld())) {
				test1 = true;
			} else {
				test1 = false;
				break;
			}
		}
		testRules.setRegelndirektamuf(Boolean.TRUE.equals(test1) ? ok() : "" + test1);
		BusinessObjectProvider.update(testRules);
	}

	public String getPflichtfeldText() {
		return (new AnlegenAmUfExtension()).getPflichtfeldText();
	}

}


// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.rules.UfAnlegen
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=YKzgKUyMae7iiGBOeG3B
// END NUCLOS CODEGENERATOR COMMENT
