package extension.nuclet.test.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.annotation.Rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @name
 * @description
 * @usage
 * @change
 */
@Rule(name = "Reflection Methods", description = "Reflection Methods")
public class ReflectionMethodsExtension {

	private static final Logger LOG = LoggerFactory.getLogger(ReflectionMethodsExtension.class);

	public static boolean has(Object o, String attributeName) {
		try {
			Method method = o.getClass().getMethod("get" + StringUtils.capitalize(attributeName));
			return method != null;
		} catch (Exception e) {
		}
		return false;
	}

	public static Object get(Object o, String attributeName) {
		try {
			Method method = o.getClass().getMethod("get" + StringUtils.capitalize(attributeName));
			if (method != null) {
				return method.invoke(o);
			}
		} catch (Exception e) {
		}

		return null;
	}

	public static void set(Object o, String attributeName, Object attributeValue) {
		try {
			for (Method method : o.getClass().getMethods()) {
				if (method.getName().equals("set" + StringUtils.capitalize(attributeName))) {
					method.invoke(o, attributeValue);
					return;
				}
			}
		} catch (Exception e) {
		}
	}

	public static void setDefault(Object o, String attributeName, Object defaultValue) {
		if (has(o, attributeName) && get(o, attributeName) == null) {
			set(o, attributeName, defaultValue);
		}
	}
	
	public static Method getMethod(Object o, String methodName) {
		try {
			for (Method method : o.getClass().getMethods()) {
				if (method.getName().equals(methodName)) {
					return method;
				}
			}
		} catch (Exception e) {
		}
		return null;
	}
	
//	public static Object getAttribute(Object o, String attributeName) {
//		try {
//			Method method = o.getClass().getMethod("get" + StringUtils.capitalize(attributeName));
//			if (method != null) {
//				return method.invoke(o);
//			}
//		} catch (Exception e) {
//		}
//		
//		return null;
//	}
	
	public static Object getAttributeRecursive(Object o, String attributeName) {
		Field f = getFieldRecursive(o, attributeName, o.getClass());
		if (f != null) {
			f.setAccessible(true);
			try {
				return f.get(o);
			} catch (Exception e) {
			}
		}
		
		return null;
	}
	
	public static boolean setAttributeRecursive(Object o, String attributeName, Object value) {
		Field f = getFieldRecursive(o, attributeName, o.getClass());
		if (f != null) {
			f.setAccessible(true);
			try {
				f.set(o,  value);
				return true;
			} catch (Exception e) {
			}
		}
		
		return false;
	}
	
	public static Field getFieldRecursive(Object o, String attributeName, Class<?> c) {
		try {
			for (Field f: c.getDeclaredFields()) {
				if (f.getName().equals(attributeName)) {
					return f;
				}
			}
			if (c.getSuperclass() != null) {
				return getFieldRecursive(o, attributeName, c.getSuperclass());
			}
		}
		catch (Exception e) {
		}
		
		throw new IllegalArgumentException("Field " + attributeName + " not found in Object " + o);
	}
	
	public static Method getMethodRecursive(Object o, String methodName, Class<?> c, Object... args) {
		try {
			for (Method m: c.getDeclaredMethods()) {
				if (m.getName().equals(methodName) 
						&& m.getParameterTypes().length == args.length
						&& checkSignature(m, args)) {
					return m;
				}
			}
			if (c.getSuperclass() != null) {
				return getMethodRecursive(o, methodName, c.getSuperclass(), args);
			}
		}
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		throw new IllegalArgumentException("Method " + methodName + "(" + args + ") not found in Object " + o + " (Class " + c.getName() + ")");
	}
	
	public static Object invokeRecursive(Object o, String methodName, Object... args) {
		Method m= getMethodRecursive(o, methodName, o.getClass(), args);
		if (m != null) {
			m.setAccessible(true);
			try {
				return m.invoke(o, args);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		
		throw new IllegalArgumentException("Method " + methodName + "(" + args + ") not found in Object " + o);
	}
	
	private static boolean checkSignature(Method m, Object... args) {
		Class<?>[] types = m.getParameterTypes();
		for (int i=0; i<types.length; i++) {
			if (!types[i].isInstance(args[i])) {
				return false;
			}
		}
		
		return true;
	}
}

// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
// class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
// entity=nuclos_servercode
// name=nuclet.test.utils.ReflectionMethods
// type=org.nuclos.server.customcode.valueobject.CodeVO
// uid=eeNZVlDStiudVaLmbNqf
// END NUCLOS CODEGENERATOR COMMENT
