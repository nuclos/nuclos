package extension.nuclet.test.utils.servertest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.provider.FileProvider;

import nuclet.test.utils.ServerTest;

public class PoiTestRule {

	private static final Logger LOG = LogManager.getLogger(PoiTestRule.class);

	public static void runDocxToPdf(ServerTest test) {
		try {
			InputStream docxStream = PoiTestRule.class.getClassLoader().getResourceAsStream("NUCLOS-8829.docx");
			File docxFile = File.createTempFile("NUCLOS-8829-", ".docx");
			docxFile.deleteOnExit();
			try(OutputStream outputStream = new FileOutputStream(docxFile)) {
				IOUtils.copy(docxStream, outputStream);
			}
			NuclosFile docxNuclosFile = FileProvider.newFile(docxFile);
			NuclosFile pdfNuclosFile = FileProvider.toPdf(docxNuclosFile);
			LOG.info("PDF Filename = '{}'", pdfNuclosFile.getName());
			test.setOk(pdfNuclosFile != null && pdfNuclosFile.getName().endsWith(".pdf"));
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			test.setException(ex.getMessage());
		}
	}

}
