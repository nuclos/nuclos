package org.nuclet.authentication;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import javax.annotation.Priority;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.authentication.AuthenticationResult;
import org.nuclos.api.context.AuthenticationContext;
import org.nuclos.api.context.RefreshAuthenticationContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.AuthenticationRule;

@Priority(99)
public class NucletAuthenticationRule implements AuthenticationRule {

	@Override
	public AuthenticationResult authenticate(final AuthenticationContext context) throws BusinessException {
		String sAuthorization = context.getHttpHeaders().getHeaderString("Authorization");
		if (sAuthorization != null) {
			String checkUriInfo = context.getHttpHeaders().getHeaderString("checkUriInfo");
			if ("yes".equals(checkUriInfo)) {
				this.checkUriInfo(context.getUriInfo());
			}
			String checkRemoteAddress = context.getHttpHeaders().getHeaderString("checkRemoteAddress");
			if (StringUtils.isNotBlank(checkRemoteAddress)) {
				this.checkRemoteAddress(context, checkRemoteAddress);
			}
			String checkCookie = context.getHttpHeaders().getHeaderString("checkCookie");
			if (StringUtils.isNotBlank(checkCookie)) {
				this.checkCookie(context, checkCookie, context.getHttpHeaders().getHeaderString("checkCookieValue"));
			}
			String throwStatus = context.getHttpHeaders().getHeaderString("throwStatus");
			String throwMessage = context.getHttpHeaders().getHeaderString("throwMessage");
			if (StringUtils.isNotBlank(throwStatus)) {
				if (StringUtils.isNotBlank(throwMessage)) {
					Response.Status status = Response.Status.fromStatusCode(Integer.parseInt(throwStatus));
					throw new WebApplicationException(throwMessage, status);
				} else {
					throw new WebApplicationException(Integer.parseInt(throwStatus));
				}
			}
			if (sAuthorization.startsWith("i am ")) {
				String username = sAuthorization.substring(5);
				String refresh = context.getHttpHeaders().getHeaderString("myRefresh");
				String withLogin = context.getHttpHeaders().getHeaderString("withLogin");
				String withLifetimeInSeconds = context.getHttpHeaders().getHeaderString("withLifetimeInSeconds");
				AuthenticationResult.Builder builder = AuthenticationResult.builder()
						.withUsername(username)
						.withLoginRequired(Boolean.parseBoolean(withLogin))
						.withAttribute("myRefresh", refresh);
				if (StringUtils.isNotBlank(withLifetimeInSeconds)) {
					builder.withLifetime(Long.parseLong(withLifetimeInSeconds), TimeUnit.SECONDS);
				}
				return builder.build();
			} else {
				throw new BusinessException(String.format("Authorization not supported: \"%s\"", sAuthorization));
			}
		}
		return null;
	}

	@Override
	public boolean refreshAuthentication(final RefreshAuthenticationContext context) throws BusinessException {
		return "always".equals(context.getAuthenticationResult().getAttribute("myRefresh")) ||
			   "yes".equals(context.getHttpHeaders().getHeaderString("myRefresh"));
	}

	private void checkUriInfo(UriInfo uriInfo) throws BusinessException {
		MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();
		if (!"myValue".equals(queryParameters.getFirst("paramOnlyForTest"))) {
			throw new BusinessException("Query Parameter \"paramOnlyForTest\" is missing!");
		}
		String path = uriInfo.getPath();
		String expectedPath = "/maintenance/managementconsole/-showMaintenance";
		if (!expectedPath.equals(path)) {
			throw new BusinessException(String.format("Path \"%s\" does not match expected path \"%s\"!", path, expectedPath));
		}

		String[] expectedPathSegments = expectedPath.split("/");
		String[] pathSegments = uriInfo.getPathSegments().stream().map(pathSegment -> pathSegment.getPath()).toArray(String[]::new);
		if (!Arrays.equals(pathSegments, expectedPathSegments)) {
			throw new BusinessException(String.format("PathSegments %s does not match expected segments %s !", Arrays.toString(pathSegments), Arrays.toString(expectedPathSegments)));
		}
	}

	private void checkRemoteAddress(AuthenticationContext context, String remoteAddress) throws BusinessException {
        String clientIp = resolveClientIp(context.getHttpHeaders(), context.getRemoteAddr());
		if (!remoteAddress.contains(clientIp)) {
			throw new BusinessException(String.format("Remote address \"%s\" not found in expected address list \"%s\"!", remoteAddress, clientIp));
		}
	}

    /**
     * Resolves client IP address when application is behind a NGINX or other reverse proxy server
     */
    private String resolveClientIp(HttpHeaders headers, String remoteAddress) {
        String xRealIp = headers.getHeaderString("X-Real-IP"); // used by Nginx
        String xForwardedFor = headers.getHeaderString("X-Forwarded-For"); // used by the majority of load balancers

        // default fall back to remoteAddress
        // returns the first non null
        return Stream.of(xRealIp, xForwardedFor, remoteAddress)
                .filter(Objects::nonNull)
                .findFirst().orElse(null);
    }

	private void checkCookie(AuthenticationContext context, String cookieName, String cookieValue) throws BusinessException {
		Cookie cookie = context.getHttpHeaders().getCookies().get(cookieName);
		if (cookie == null) {
			throw new BusinessException(String.format("Cookie with name \"%s\" not found!", cookieName));
		}
		if (!StringUtils.equals(cookie.getValue(), cookieValue)) {
			throw new BusinessException(String.format("Cookie value \"%s\" does not match expected test value \"%s\"!", cookie.getValue(), cookieValue));
		}
	}

}
