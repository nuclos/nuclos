package org.nuclet.remoting;

import org.nuclos.api.remoting.TransportObject;

public class MyCustomRemotingTransport implements TransportObject {

	private final Object notUsed;

	/**
	 * for deserialization only
	 */
	protected MyCustomRemotingTransport() {
		super();
		this.notUsed = null;
	}

	public MyCustomRemotingTransport(final Object notUsed) {
		super();
		this.notUsed = notUsed;
	}
}
