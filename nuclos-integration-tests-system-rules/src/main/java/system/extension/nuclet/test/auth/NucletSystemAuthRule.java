package system.extension.nuclet.test.auth;

import org.nuclos.api.authentication.AuthenticationResult;
import org.nuclos.api.context.AuthenticationContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.AuthenticationRule;
import org.nuclos.businessentity.User;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.server.customcode.codegenerator.RuleClassLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SystemRuleUsage(boClass = User.class, order = 1)
public class NucletSystemAuthRule implements AuthenticationRule {

	private final Logger LOG = LoggerFactory.getLogger(NucletSystemAuthRule.class);

	@Override
	public AuthenticationResult authenticate(final AuthenticationContext context) throws BusinessException {
		if ("it-systemExtension".equals(context.getHttpHeaders().getHeaderString("Rule-System-Auth"))) {
			LOG.info("NucletSystemAuthRule is loaded from {}", NucletSystemAuthRule.class.getClassLoader());
			if (NucletSystemAuthRule.class.getClassLoader() instanceof RuleClassLoader) {
				throw new BusinessException("NucletSystemAuthRule is loaded from a RuleClassLoader");
			}
			if (NucletSystemAuthRule.class.getClassLoader() != Thread.currentThread().getContextClassLoader()) {
				throw new BusinessException("NucletSystemAuthRule is not loaded from Thread.currentThread().getContextClassLoader():\n" +
						"NucletSystemAuthRule.class.getClassLoader()=" + NucletSystemAuthRule.class.getClassLoader() + "\n" +
						"Thread.currentThread().getContextClassLoader()=" + Thread.currentThread().getContextClassLoader());
			}
			return AuthenticationResult.builder()
					.withUsername("nuclos")
					.build();
		}
		return null;
	}
}
