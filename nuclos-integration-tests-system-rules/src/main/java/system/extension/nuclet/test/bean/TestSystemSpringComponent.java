package system.extension.nuclet.test.bean;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.server.customcode.codegenerator.RuleClassLoader;
import org.springframework.stereotype.Component;

/**
 * We are testing here that this extension (with rules) is not covered by a RuleClassLoader.
 * The {@link org.nuclos.businessentity.rule.annotation.SystemRuleUsage} must ensure that it is loaded directly by the
 * system class loader.
 */
@Component
public class TestSystemSpringComponent {

	public static final String TEST = "\"SYSTEM\" object created";

	private final String test;

	private TestSystemSpringComponent() {
		this.test = TEST;
	}

	public String getTest() {
		return test;
	}

	public boolean checkSystemClassLoader() throws BusinessException {
		if (TestSystemSpringComponent.class.getClassLoader() instanceof RuleClassLoader) {
			throw new BusinessException("TestSystemSpringComponent is loaded from a RuleClassLoader");
		}
		if (TestSystemSpringComponent.class.getClassLoader() != Thread.currentThread().getContextClassLoader()) {
			throw new BusinessException("TestSystemSpringComponent is not loaded from Thread.currentThread().getContextClassLoader():\n" +
					"TestSystemSpringComponent.class.getClassLoader()=" + TestSystemSpringComponent.class.getClassLoader() + "\n" +
					"Thread.currentThread().getContextClassLoader()=" + Thread.currentThread().getContextClassLoader());
		}
		try {
			new example.rest.Order();
			return false;
		} catch (Throwable ex) {
			if (ex instanceof ClassNotFoundException || ex instanceof NoClassDefFoundError) {
				final String className = "example.rest.Order";
				return ex.getMessage().contains(className)
						|| ex.getMessage().contains(className.replace('.', '/'));
			}
			throw new BusinessException(String.format("Unexpected Exception caught: %s", ex.getMessage()), ex);
		}
	}

}
