package org.nuclos.test

import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit

import javax.ws.rs.core.Response

import org.apache.logging.log4j.Level
import org.awaitility.Awaitility
import org.json.JSONObject
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.experimental.categories.Category
import org.junit.rules.TestRule
import org.junit.runners.MethodSorters
import org.nuclos.test.log.CallTrace
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTException
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.ServerLogger
import org.nuclos.test.rest.SuperuserRESTClient
import org.nuclos.test.webclient.FailureHandler
import org.nuclos.test.webclient.util.Screenshot
import org.nuclos.test.webclient.utils.Utils
import org.openqa.selenium.TimeoutException

import groovy.transform.CompileStatic
import groovy.transform.stc.ClosureParams
import groovy.transform.stc.SimpleType

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
abstract class AbstractNuclosTest {
	static SuperuserRESTClient nuclosSession

	static NuclosTestContext context = NuclosTestContext.instance

	@Rule
	public final TestRule testRule = new NuclosTestRule()

	@BeforeClass
	static void setup() {
		Log.debug 'Setup'
		nuclosSession = null

		getNuclosSession().managementConsole('disableIndexer')
		getNuclosSession().stopMaintenance()

		setupSqlDefaultLogging()
		setupSystemParameters()

		truncateTablesAndCaches()

		// create default test user with DEFAULT password
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
	}

	static void setupSqlDefaultLogging() {
		Level sqlUpdateLoggerLevel = Level.OFF
		Level sqlLoggerLevel = Level.OFF
		Level sqlTimerLevel = Level.OFF
		switch (Log.getLogger().getLevel()) {
			case (Level.TRACE):
				sqlTimerLevel = Level.DEBUG
				break
			case Level.DEBUG:
				sqlLoggerLevel = Level.DEBUG
				break
			case Level.INFO:
				sqlUpdateLoggerLevel = Level.DEBUG
				break
			default:
				// warn
				Log.debug 'SQL Logging disabled'
		}
		nuclosSession.setServerLogLevel(ServerLogger.SQL_UPDATE, sqlUpdateLoggerLevel)
		nuclosSession.setServerLogLevel(ServerLogger.SQL_LOGGER, sqlLoggerLevel)
		nuclosSession.setServerLogLevel(ServerLogger.SQL_TIMER, sqlTimerLevel)
		nuclosSession.setDebugSql(null)
	}

	static void enableSqlCountLogging() {
		nuclosSession.setServerLogLevel(ServerLogger.SQL_LOGGER, Level.DEBUG)
	}

	static void enableSqlTimeLogging() {
		nuclosSession.setServerLogLevel(ServerLogger.SQL_TIMER, Level.DEBUG)
	}

	static SuperuserRESTClient getNuclosSession() {
		if (!nuclosSession) {
			CallTrace.trace('Login as "nuclos"') {
				nuclosSession = new SuperuserRESTClient('nuclos', '').login()
			}
		}

		return nuclosSession
	}

	static void truncateTablesAndCaches() {
		CallTrace.trace('Truncate tables and Caches') {
			Map truncateTable =
					[
							boMetaId  : 'nuclet_test_utils_TruncateTables',
							attributes: [
									'name': 'Truncate User Tables' + new Date().getTime()
							]
					]
			JSONObject bo = RESTHelper.createBo(truncateTable, getNuclosSession())
			bo.getJSONObject('attributes').put('nuclosState',  [id: 'nuclet_test_utils_TruncateTablesSM_State_20'])
			RESTHelper.updateBo(bo, getNuclosSession())

			Log.info getNuclosSession().invalidateServerCaches()
		}
	}

	/**
	 * Works only for requests done directly via RESTClient,
	 * does not capture Webclient requests.
	 */
	static void expectErrorStatus(
			Response.Status expectedStatus,
			Closure<?> c
	) {
		expectRestException(c) {
			def currentStatus = it.status
			assert currentStatus == expectedStatus
		}
	}

	static void failsafeAssert(Closure<Boolean> condition, String description) {
		try {
			assert condition()
		} catch (AssertionError e) {
			// take screenshot
			Screenshot.take("assertion")
			assert false: description + " -> " + e.getMessage()
		}
	}

	/**
	 * Catches any RestException that occurs while executing the given closure.
	 * Hands the RestException to the given exceptionHandler.
	 */
	static void expectRestException(
			Closure<?> c,

			@ClosureParams(value = SimpleType, options = ['org.nuclos.test.rest.RESTException'])
					Closure<?> exceptionHandler
	) {
		Exception e = null

		try {
			c()
		} catch (RESTException e2) {
			e = e2
		}

		if (e) {
			exceptionHandler(e)
		} else {
			throw new IllegalStateException("Expected exception did not occur")
		}
	}

	static void setupSystemParameters() {
		nuclosSession.setSystemParameters([:])
	}

	/**
	 * Use {@link org.nuclos.test.webclient.AbstractWebclientTest#waitFor(groovy.lang.Closure) or a custom WebDriverWait instead,
	 * if this is a Webclient test.
	 */
	static void sleep(final long millis) {
		long millisToSleep = millis
		try {
			long startTime = System.currentTimeMillis()
			long elapsedTime = 0L
			while (elapsedTime < millis) {
				if (elapsedTime > 0L) {
					String sLog = "System timer has not waited long enough, we need to fix it: ${elapsedTime} < ${millis}"
					Log.debug sLog
				}
				Thread.sleep(millisToSleep)
				elapsedTime = System.currentTimeMillis() - startTime
				millisToSleep = 100L
			}
		} catch (InterruptedException e) {
			Log.error(e.getMessage(), e)
		}
	}

	/**
	 * Waits for the given Closure to return not null.
	 * Fails the build if the condition is not met within the given timeout.
	 */
	static <T> T waitForNotNull(int timeoutInSeconds, Closure<T> condition, boolean bThrowFailures = true, Class<? extends Throwable>...tryAgainOn = []) {
		List<Throwable> catchedExceptions = []
		T result = null
		try {
			result = doWaitForNotNull(timeoutInSeconds, condition, catchedExceptions, tryAgainOn)
		} catch (Throwable t) {
			if (bThrowFailures) {
				String source = Utils.closureToString(condition)
				fail('Failed to wait for not null: ' + source, t)
			}
		}
		if (result == null && bThrowFailures) {
			String source = Utils.closureToString(condition)
			Throwable t
			if (catchedExceptions.isEmpty()) {
				t = new TimeoutException(source)
			} else {
				t = new TimeoutException(catchedExceptions.get(0))
			}
			fail('Failed to wait for not null: ' + source, t)
		}

		return result
	}

	/**
	 * Waits for the given Closure to return true.
	 * Fails the build if the condition is not met within the given timeout.
	 */
	static boolean waitFor(int timeoutInSeconds, Closure condition) {
		if (!doWaitFor(timeoutInSeconds, condition)) {
			String source = Utils.closureToString(condition)
			fail('Failed to wait for condition: ' + source, new Throwable(source))
		}

		return true
	}

	static boolean waitFor(Closure condition) {
		waitFor(NuclosTestContext.instance.DEFAULT_TIMEOUT, condition)
	}

	/**
	 * Waits for the given Closure to return not null.
	 *
	 * @param timeoutInSeconds
	 * @param condition
	 * @param catchedExceptions
	 * @return object of type T, if the condition was met within the timeout
	 */
	static <T> T doWaitForNotNull(int timeoutInSeconds, Closure<T> condition, List<Throwable> catchedExceptions = [], Class<? extends Throwable>...tryAgainOn) {
		def conditionCatched = {
			try {
				return condition()
			} catch (Throwable t) {
				catchedExceptions.add(t)
				for (Class<? extends Throwable> tryA : tryAgainOn) {
					if (tryA.isAssignableFrom(t.getClass())) {
						return null
					}
				}
				throw t
			}
		}
		final long joinTimeout = (timeoutInSeconds <= 0 ? context.DEFAULT_TIMEOUT : timeoutInSeconds) * 1000
		final List<T> results = new ArrayList<>(1)
		List<Throwable> unhandledExceptions = []
		try {
			Thread t = new Thread() {
				@Override
				void run() {
					try {
						def result = waitForCondition(conditionCatched, condition.toString(), timeoutInSeconds <= 0)
						if (result != null) {
							results.add((T)result)
						}
					} catch (Throwable t) {
						unhandledExceptions.add(t)
					}
				}
			}
			t.start()
			try {
				t.join(joinTimeout)
			} catch (InterruptedException ex) {
				Log.debug "Thread for condition ${condition.toString()} interrupted!"
			}

			if (t.alive) {
				t.interrupt()
				return null
			}
			if (results.isEmpty()) {
				if (unhandledExceptions.isEmpty()) {
					return null
				} else {
					throw unhandledExceptions.get(0)
				}
			} else {
				return results.get(0)
			}
		} catch (Exception ex) {
			Log.warn("Failed to wait for condition", ex)
			return null
		}
	}

	/**
	 * Waits for the given Closure to return true.
	 *
	 * @param timeoutInSeconds
	 * @param condition
	 * @return true , if the condition was met within the timeout
	 */
	static boolean doWaitFor(int timeoutInSeconds, Closure condition) {
		List<Throwable> unhandledExceptions = []
		try {
			Thread t = new Thread() {
				@Override
				void run() {
					try {
						waitForCondition(condition, condition.toString())
					} catch (Throwable t) {
						unhandledExceptions.add(t)
					}
				}
			}
			t.start()
			try {
				t.join(timeoutInSeconds * 1000)
			} catch (InterruptedException ex) {
				Log.debug "Thread for condition ${condition.toString()} interrupted!"
			}

			if (t.alive) {
				t.interrupt()
				return false
			}
			if (unhandledExceptions.isEmpty()) {
				return true
			} else {
				throw unhandledExceptions.get(0)
			}
		} catch (Exception ex) {
			Log.warn("Failed to wait for condition", ex)
			return false
		}
	}

	static <T> T waitForCondition(Closure<T> condition, String nameForLogging, boolean bOnlyOnce = false) {
		def result = null
		int iSleep = 0
		while (result == null && !Thread.currentThread().isInterrupted() && (iSleep == 0 || !bOnlyOnce)) {
			if (iSleep > 0) {
				Log.info "Waiting for condition $nameForLogging..."
				try {
					Thread.currentThread().sleep(iSleep)
				} catch (InterruptedException ex) {
					Log.debug "Thread for condition $nameForLogging interrupted!"
					break
				}
			}
			iSleep = 500
			result = condition()
		}
		return result as T
	}

	/**
	 * Forcefully fails the build immediately.
	 */
	static void fail(String failure, Throwable t = null) {
		FailureHandler.fail(failure, t)
	}

	static void waitUntilTrue(Callable<Boolean> conditionEvaluator, int secondsToWait = NuclosTestContext.DEFAULT_TIMEOUT) {
		Awaitility.await().atMost(secondsToWait, TimeUnit.SECONDS).until(conditionEvaluator);
	}
}
