package org.nuclos.test


import org.junit.Before
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters

import groovy.transform.CompileStatic

/**
 * Class to guarantee a clean test environment before EVERY single test(-method) will be executed.
 * (Truncate-tables, etc.... @Test void method1{})
 * (Truncate-tables, etc.... @Test void method2{})
 *
 * This ensures a smooth 'Rerun' from the Maven JUnit plugin
 * https://maven.apache.org/surefire/maven-surefire-plugin/examples/rerun-failing-tests.html
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
abstract class AbstractRerunnableNuclosTest extends AbstractNuclosTest {

	@BeforeClass
	static void setup() {
		// do nothing here
	}

	@Before
	void beforeEach() {
		AbstractNuclosTest.setup()
	}

	/*@After
	void afterEach() {

	}*/

}
