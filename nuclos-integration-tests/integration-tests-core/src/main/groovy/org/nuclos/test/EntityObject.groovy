package org.nuclos.test

import java.text.SimpleDateFormat

import org.apache.commons.lang3.tuple.Pair
import org.json.JSONObject
import org.nuclos.api.businessobject.Flag
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTClient

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.module.SimpleModule

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class EntityObject<PK> {

	private static final ObjectMapper mapper

	static {
		mapper = new ObjectMapper()

		SimpleModule module = new SimpleModule();
		module.addSerializer(JSONObject.class, new JSONObjectSerializer())

		Class<?> clazz = Class.forName('org.json.JSONObject$Null')
		module.addSerializer(clazz, new NullSerializer())

		mapper.registerModule(module)

		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
		mapper.configure(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME, true)
	}

	PK boId

	Boolean dirty = null
	Integer version = null
	String executeCustomRule

//	@JsonSerialize(using = EntityObjectAttributesSerializer.class)
	Map<String, Object> attributes = new HashMap<>()
	Map<String, JSONObject> attrImageLinks = new HashMap<>()
	Map<String, Pair<String, byte[]>> documents = new HashMap<>()

	@JsonIgnore
	Flag flag = Flag.INSERT

	@JsonIgnore
	final EntityClass<PK> entityClass

	@JsonProperty('subBos')
	@JsonSerialize(using = EntityObjectDependentsSerializer)
	Map<String, List<EntityObject<?>>> dependents = new HashMap<>()

	/**
	 * The RESTClient by which this EO was (re-)loaded.
	 */
	@JsonIgnore
	RESTClient client

	/**
	 * The parent EO, if this is a sub-EO.
	 */
	@JsonIgnore
	protected EntityObject parent

	Map<String, RestLink> links = new TreeMap<>()

	Map<String, Object> nextStates = new HashMap<>()

	static <PK> EntityObject<PK> fromJson(
			final EntityClass<PK> entityClass,
			final JSONObject json
	) {
		final EntityObject<PK> eo = new EntityObject<PK>(entityClass)
		eo.updateFromJson(json)
		return eo
	}

	EntityObject(EntityClass<PK> entityClass) {
		this.entityClass = entityClass
	}

	PK getId() {
		return boId
	}

	String toJson() {
		mapper.writeValueAsString(this)
	}

	void updateFromJson(JSONObject json) {
		def id = (PK) json['boId']

		// Convert Integer IDs to Long
		if (id instanceof Integer) {
			id = Long.valueOf(id as long)
		}

		boId = id as PK

		Map<String, Object> attributesMap = attributes
		attributesMap.clear()
		json.getJSONObject('attributes').with {
			for (String key : it.keySet()) {
				attributesMap.put(key, it.get(key))
			}
		}

		Map<String, JSONObject> attrImageLinksMap = attrImageLinks
		attrImageLinksMap.clear()
		if (json.has('attrImages')) {
			json.getJSONObject('attrImages').with {
				if (it.has('links')) {
					JSONObject links = it.getJSONObject('links')
					for (String key : links.keySet()) {
						attrImageLinksMap.put(key, links.getJSONObject(key))
					}
				}
			}
		}



		Map<String, RestLink> linksMap = links
		linksMap.clear()
		json.getJSONObject('links').with {
			for (String linkId : it.keySet()) {
				JSONObject link = it.getJSONObject(linkId)
				if (!link.has('href')) {
					Log.warn('Found link (id=' + linkId + ') without href: ' + link.toString())
					continue
				}
				linksMap.put(
						linkId,
						new RestLink(
								href: link.getString('href'),
								methods: link.getJSONArray('methods').collect { it.toString() }.toSet()
						)
				)
			}
		}

		nextStates.clear()
		if (json.has('nextStates')) {
			json.getJSONArray('nextStates').with {
				for (int i = 0; i < it.size(); i++) {
					JSONObject obj = (JSONObject)it.get(i)
					String name = obj.get("name")
					nextStates.put(name, obj)
				}
			}
		}

		version = json['version'] as Integer

		// Dependents must be reloaded after update
		dependents.clear()
	}

	@JsonIgnore
	boolean isNew() {
		return !id || id.equals(null)
	}

	@JsonIgnore
	boolean isUpdate() {
		return id && flag == Flag.UPDATE
	}

	@JsonIgnore
	boolean isDelete() {
		return id && flag == Flag.DELETE
	}

	Object getAttribute(String attributeName) {
		this.attributes.get(attributeName)
	}

	JSONObject getAttributeImageLink(String attributeName) {
		this.attrImageLinks.get(attributeName)
	}

	EntityReference<?> getReference(String attributeName) {
		JSONObject json = this.attributes.get(attributeName) as JSONObject

		return new EntityReference<?>(
				id: json.get('id'),
				name: json.getString('name')
		)
	}

	void setAttribute(String attributeName, Object value) {
		if (value instanceof Date) {
			value = new SimpleDateFormat('yyyy-MM-dd').format(value)
		} else if (value instanceof GString) {
			value = value.toString()
		}

		this.attributes.put(attributeName, value)

		flagUpdated()
	}

	void setAttributeNull(String attributeName) {
		setAttribute(attributeName, JSONObject.wrap(null))
	}

	void setDocument(String attributeName, String filename, byte[] content) {
		this.documents.put(attributeName, Pair.of(filename, content))
	}

	String getBoMetaId() {
		entityClass.fqn
	}

	@JsonIgnore
	public <SubPK> List<EntityObject<SubPK>> getDependents(
			EntityClass<SubPK> subEntityClass,
			String referenceAttributeName
	) {
		String referenceAttributeFqn = subEntityClass.fqn + '_' + referenceAttributeName

		if (!this.dependents.containsKey(referenceAttributeFqn)) {
			this.dependents.put(referenceAttributeFqn, [])
		}

		return this.dependents.get(referenceAttributeFqn) as List<EntityObject<SubPK>>
	}

	public <SubPK> List<EntityObject<SubPK>> loadDependents(
			EntityClass<SubPK> subEntityClass,
			String referenceAttributeName
	) {
		String referenceAttributeFqn = subEntityClass.fqn + '_' + referenceAttributeName

		List<EntityObject<SubPK>> dependents = client.loadDependents(
				this,
				subEntityClass,
				referenceAttributeFqn
		)

		dependents.each {
			it.parent = this
		}

		this.dependents.put(referenceAttributeFqn, dependents as List<EntityObject<?>>)

		return dependents
	}

	PK save() {
		checkClient()

		uploadDocuments(client)

		client.save(this) as PK
	}

	public void uploadDocuments(RESTClient client) {
		if (!this.documents.isEmpty()) {
			this.documents.entrySet().forEach {
				this.attributes.put(
						it.getKey(),
						[
								name       : it.getValue().getKey(),
								uploadToken: client.uploadDocument(it.getValue().getKey(), it.getValue().getValue())
						]
				)
			}
		}
	}

	EntityObject<PK> cloneEo(String layout = null) {
		checkClient()
		client.clone(this, layout) as EntityObject<PK>
	}

	private void checkClient() {
		if (!client) {
			throw new IllegalStateException('This EO is not associated with any REST client.')
		}
	}

	boolean delete() {
		checkClient()
		return client.delete(this)
	}

	void flagDeleted() {
		setFlag(Flag.DELETE)
	}

	void flagUpdated() {
		if (!this.new) {
			setFlag(Flag.UPDATE)
		}
	}

	void executeCustomRule(String ruleName) {
		checkClient()
		client.executeCustomRule(this, ruleName)
	}

	void changeState(int statusNumeral) {
		checkClient()
		client.changeState(this, statusNumeral)
	}

	void setFlag(Flag flag) {
		this.flag = flag

		if (parent) {
			parent.flagUpdated()
		}
	}

	public <PK2> EntityObject<PK2> generateObject(
			final String generatorFqn,
			final EntityClass<PK2> resultClass
	) {
		checkClient()
		client.generateObject(this, generatorFqn, resultClass)
	}
}
