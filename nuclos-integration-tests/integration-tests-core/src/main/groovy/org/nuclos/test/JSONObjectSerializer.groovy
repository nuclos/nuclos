package org.nuclos.test

import org.json.JSONObject

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class JSONObjectSerializer extends StdSerializer<JSONObject> {
	protected JSONObjectSerializer() {
		super(JSONObject.class)
	}

	@Override
	void serialize(
			final JSONObject value,
			final JsonGenerator gen,
			final SerializerProvider provider
	) throws IOException {
		String nonNullValue = value.keySet().find { !value.isNull(it) }

		// Do nothing, if this JSONObject has only null values
		if (!nonNullValue) {
			gen.writeNull()
			return
		}

		gen.writeStartObject()

		value.keySet().each {
			if (!value.isNull(it)) {
				gen.writeObjectField(it, value.get(it))
			}
		}

		gen.writeEndObject()
	}
}
