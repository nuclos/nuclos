package org.nuclos.test

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class NullSerializer extends StdSerializer<Object> {
	protected NullSerializer() {
		super(Object.class)
	}

	@Override
	void serialize(
			final Object value,
			final JsonGenerator gen,
			final SerializerProvider provider
	) throws IOException {
		gen.writeNull()
	}
}
