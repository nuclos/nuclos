package org.nuclos.test

import com.google.common.base.CaseFormat

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
enum SystemEntities implements EntityClass<String> {
	PARAMETER,
	USER,
	ROLE,
	ROLE_USER,
	NUCLET,
	NUCLET_PARAMETER,
	NUCLET_SQL_CONFIG,
	MANDATOR,
	NEWS,
	RESTAPIS,
	EMAIL_OUTGOING_SERVER,
	EMAIL_INCOMING_SERVER,
	JOBCONTROLLER

	@Override
	String getFqn() {
		String formattedName = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name())

		return 'org_nuclos_system_' + formattedName
	}
}
