package org.nuclos.test

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
enum TestEntities implements EntityClass<Long> {
	EXAMPLE_REST_AKTIONENSTANDARDPFADTEST('example_rest_AktionenStandardPfadTest'),
	EXAMPLE_REST_BERGSTEIGER('example_rest_Bergsteiger'),
	EXAMPLE_REST_CUSTOMER('example_rest_Customer'),
	EXAMPLE_REST_CUSTOMERADDRESS('example_rest_CustomerAddress'),
	EXAMPLE_REST_DYNTASKSORDERANDCUSTOMERDTL('example_rest_DynTasksOrderAndCustomerDTL'),
	EXAMPLE_REST_DYNTASKSACTIVEARTICLESDTL('example_rest_DynamicArticleTaskListDTL'),
	EXAMPLE_REST_ORDER('example_rest_Order'),
	EXAMPLE_REST_DISCOUNTCOUPON('example_rest_DiscountCoupon'),
	EXAMPLE_REST_ORDERPOSITION('example_rest_OrderPosition'),
	EXAMPLE_REST_ORDERPOSITIONDYN('example_rest_OrderPositionDYN'),
	EXAMPLE_REST_PERSPECTIVEORDERPOSITION('example_rest_PerspectiveOrderPosition'),
	EXAMPLE_REST_ARTICLE('example_rest_Article'),
	EXAMPLE_REST_TAG('example_rest_Tag'),
	EXAMPLE_REST_SHIPMENTTYPE('example_rest_ShipmentType'),
	EXAMPLE_REST_CATEGORY('example_rest_Category'),
	EXAMPLE_REST_MYORDERSANDCUSTOMERSDYN('example_rest_MyOrdersAndCustomersDYN'),
	EXAMPLE_REST_INVOICE('example_rest_Invoice'),
	EXAMPLE_REST_INVOICEPOSITION('example_rest_InvoicePosition'),
	EXAMPLE_REST_ADDRESSDIALOGSUBFORM('example_rest_AddressDialogSubform'),
	EXAMPLE_REST_CUSTOMERADRESSES('example_rest_CustomerAddresses'),

	EXAMPLE_REST_WORD('example_rest_Word'),
	EXAMPLE_REST_VIRTUALROWORD('example_rest_VirtualRoWord'),
	EXAMPLE_REST_VIRTUALRWWORD('example_rest_VirtualRwWord'),
	EXAMPLE_REST_WRITEPROXYDATA('example_rest_WriteProxyData'),
	EXAMPLE_REST_WRITEPROXYTEST('example_rest_WriteProxyTest'),

	EXAMPLE_REST_AUFTRAG('example_rest_Auftrag'),
	EXAMPLE_REST_AUFTRAGSPOSITION('example_rest_Auftragsposition'),
	EXAMPLE_REST_RECHNUNG('example_rest_Rechnung'),

	EXAMPLE_REST_WORLD('example_rest_World'),
	EXAMPLE_REST_COUNTRY('example_rest_Country'),
	EXAMPLE_REST_CITY('example_rest_City'),

	EXAMPLE_REST_MASCHINE('example_rest_Maschine'),
	EXAMPLE_REST_MASCHINENTEIL('example_rest_MaschinenTeil'),
	EXAMPLE_REST_HERSTELLER('example_rest_Hersteller'),

	NUCLET_TEST_OTHER_ATTRIBUGTENAMETEST('nuclet_test_other_AttributeNameTest'),
	NUCLET_TEST_OTHER_CALCULATEDATTRIBUTE('nuclet_test_other_CalculatedAttribute'),
	NUCLET_TEST_OTHER_RESERVEDSQLKEYWORDINALIASDTL('nuclet_test_other_ReservedSQLKeywordInAliasDTL'),
	NUCLET_TEST_OTHER_TESTPROXYCONTAINER('nuclet_test_other_TestProxyContainer'),
	NUCLET_TEST_OTHER_TESTVALIDATION('nuclet_test_other_TestValidation'),
	NUCLET_TEST_OTHER_TESTSUBFORMBUTTONS('nuclet_test_other_TestSubformButtons'),
	NUCLET_TEST_OTHER_TESTDROPDOWNS('nuclet_test_other_TestDropdowns'),
	NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS('nuclet_test_other_TestLayoutComponents'),
	NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSMEMO('nuclet_test_other_TestLayoutComponentsMemo'),
	NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSSUBFORM('nuclet_test_other_TestLayoutComponentsSubform'),
	NUCLET_TEST_OTHER_SUBFORMDOCUMENTS('nuclet_test_other_SubformDocuments'),
	NUCLET_TEST_OTHER_SUBFORMNOTOOLBAR('nuclet_test_other_SubformNoToolbar'),
	NUCLET_TEST_OTHER_TESTSUBFORMIMAGES('nuclet_test_other_TestSubformImages'),
	NUCLET_TEST_OTHER_TESTIMAGETAB('nuclet_test_other_TestImageTab'),
	NUCLET_TEST_OTHER_TESTLAYOUTRULES('nuclet_test_other_TestLayoutRules'),
	NUCLET_TEST_OTHER_TESTLAYOUTRULESSUBFORM('nuclet_test_other_TestLayoutRulesSubform'),
	NUCLET_TEST_OTHER_TESTLAYOUTRULESSUBSUBFORM('nuclet_test_other_TestLayoutRulesSubSubform'),
	NUCLET_TEST_OTHER_TESTLAYOUTUSAGE('nuclet_test_other_TestLayoutUsage'),
	NUCLET_TEST_OTHER_TESTLOADINGINDICATOR('nuclet_test_other_TestLoadingIndicator'),
	NUCLET_TEST_OTHER_TESTSTATECHANGE('nuclet_test_other_TestStateChange'),
	NUCLET_TEST_OTHER_TESTHYPERLINK('nuclet_test_other_TestHyperlink'),
	NUCLET_TEST_OTHER_TESTOBJEKTGENERATORSOURCEWITHSYSTEMENTITY('nuclet_test_other_GenerationSourceWithSystemEntityRef'),
	NUCLET_TEST_OTHER_TESTOBJEKTGENERATORTARGETWITHSYSTEMENTITY('nuclet_test_other_GenerationTargetWithSystemEntityRef'),
	NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR('nuclet_test_other_TestObjektgenerator'),
	NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT('nuclet_test_other_TestObjektgeneratorDependent'),
	NUCLET_TEST_OTHER_TESTVERSION('nuclet_test_other_TestVersion'),
	NUCLET_TEST_OTHER_TESTVLP('nuclet_test_other_TestVLP'),
	NUCLET_TEST_OTHER_TESTVLPSUBFORM('nuclet_test_other_TestVLPSubform'),
	NUCLET_TEST_OTHER_LAGERBUCHUNG('nuclet_test_other_Lagerbuchung'),
	NUCLET_TEST_OTHER_TESTAUTONUMBER('nuclet_test_other_TestAutonumber'),
	NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORM('nuclet_test_other_TestAutonumberSubform'),
	NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORMRO('nuclet_test_other_TestAutonumberSubformRO'),
	NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORMUNIQUE('nuclet_test_other_TestAutonumberSubformUnique'),
	NUCLET_TEST_OTHER_SQLINJECTIONTEST('nuclet_test_other_SQLInjectionTest'),
	NUCLET_TEST_OTHER_SQLSNIPPET('nuclet_test_other_SQLSnippet'),
	NUCLET_TEST_OTHER_TESTRULELOG('nuclet_test_other_TestRuleLog'),
	NUCLET_TEST_OTHER_SIDEBARTEST('nuclet_test_other_SidebarTest'),
	NUCLET_TEST_OTHER_STATECHANGEINPUTREQUIRED('nuclet_test_other_StateChangeInputRequired'),
	NUCLET_TEST_OTHER_TESTTABFOCUS('nuclet_test_other_TestTabfocus'),
	NUCLET_TEST_OTHER_TESTHTMLEDITOR('nuclet_test_other_TestHTMLEditor'),
	NUCLET_TEST_OTHER_TEXTMODULE('nuclet_test_other_Textmodule'),
	NUCLET_TEST_OTHER_TESTBOPROXY('nuclet_test_other_TestBOProxy'),
	NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING('nuclet_test_other_PlanningTableBooking'),
	NUCLET_TEST_OTHER_PLANNING_TABLE_RESOURCE('nuclet_test_other_PlanningTableResource'),
	NUCLET_TEST_OTHER_PLANNING_TABLE_MILESTONE('nuclet_test_other_PlanningTableMilestone'),
	NUCLET_TEST_OTHER_PLANNING_TABLE_RELATION('nuclet_test_other_PlanningTableRelation'),
	NUCLET_TEST_OTHER_PLANNING_TABLE_HOLIDAY('nuclet_test_other_PlanningTableHoliday'),
	NUCLET_TEST_OTHER_TESTPLANNINGTABLELAYOUTCOMPONENT('nuclet_test_other_TestPlanningTableLayoutComponent'),
	NUCLET_TEST_OTHER_TESTPLANNINGTABLEINACTIVE('nuclet_test_other_TestPlanningTableInactive'),
	NUCLET_TEST_OTHER_TESTINFINITENODEA('nuclet_test_other_TestInfiniteNodeA'),
	NUCLET_TEST_OTHER_TESTINFINITENODEB('nuclet_test_other_TestInfiniteNodeB'),
	NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE('nuclet_test_other_TestInfiniteRecursiveTree'),
	NUCLET_TEST_OTHER_TREEWITHDYNAMICNODE('nuclet_test_other_TreeWithDynamicNode'),
	NUCLET_TEST_OTHER_PROCESSTEST('nuclet_test_other_ProcessTest'),
	NUCLET_TEST_OTHER_PROCESSTESTSUB('nuclet_test_other_ProcessTestSub'),
	NUCLET_TEST_OTHER_PROCESSTESTSUB2('nuclet_test_other_ProcessTestSub2'),
	NUCLET_TEST_OTHER_TESTWEBCLIENTCLONEDEPENDENTS('nuclet_test_other_TestWebclientCloneDependents'),
	NUCLET_TEST_OTHER_TESTWEBCLIENTCLONESUBFORM('nuclet_test_other_TestWebclientCloneSubform'),
	NUCLET_TEST_OTHER_RESTAPISTEST('nuclet_test_other_RestAPIsTest'),
	NUCLET_TEST_OTHER_NAVIGATIONTEST('nuclet_test_other_NavigationTest'),
	NUCLET_TEST_OTHER_NAVIGATIONTESTGEN('nuclet_test_other_NavigationTestGen'),
	NUCLET_TEST_OTHER_NAVIGATIONTESTGENSUB('nuclet_test_other_NavigationTestGenSub'),
	NUCLET_TEST_OTHER_TESTCLONE('nuclet_test_other_TestClone'),
	NUCLET_TEST_OTHER_TESTCLONEDEP('nuclet_test_other_TestCloneDep'),
	NUCLET_TEST_OTHER_CUSTOMKEYLAYOUTTEST('nuclet_test_other_CustomKeyLayoutTest'),
	NUCLET_TEST_OTHER_TESTCALCULATEDATTRIBUTE('nuclet_test_other_TestCalculatedAttribute'),
	NUCLET_TEST_OTHER_TESTCALCULATEDATTRIBUTEREF('nuclet_test_other_TestCalculatedAttributeRef'),
	NUCLET_TEST_OTHER_TESTCALCULATEDATTRIBUTEREF2('nuclet_test_other_TestCalculatedAttributeRef2'),

	NUCLET_TEST_UTILS_SERVERTEST('nuclet_test_utils_ServerTest'),
	NUCLET_TEST_RULES_TESTAPI('nuclet_test_rules_TestAPI'),
	NUCLET_TEST_RULES_TESTMULTIREF('nuclet_test_rules_TestMultiref'),
	NUCLET_TEST_RULES_TESTMULTIREFDEPENDENT('nuclet_test_rules_TestMultirefDependent'),
	NUCLET_TEST_RULES_TESTQUERYPROVIDER('nuclet_test_rules_TestQueryProvider'),
	NUCLET_TEST_RULES_TESTRULES('nuclet_test_rules_TestRules'),
	NUCLET_TEST_RULES_TESTGENERATORWITHPROCESS('nuclet_test_rules_TestGeneratorWithProcess'),
	NUCLET_TEST_RULES_BOWITHINVALIDREFERENCE('nuclet_test_rules_BOWithInvalidReference'),
	NUCLET_TEST_RULES_TESTWEBSERVICE('nuclet_test_rules_TestWebservice'),
	NUCLET_TEST_RULES_TESTINPUTREQUIREDUPDATECUSTOM('nuclet_test_rules_TestInputRequiredUpdateCustom'),

	NUCLET_TEST_TABINDEX_TESTTABINDEX('nuclet_test_tabindex_TestTabindex'),

	NUCLET_TEST_MATRIX_MATRIX('nuclet_test_matrix_Matrix'),
	NUCLET_TEST_MATRIX_MATRIXZUORDNUNG('nuclet_test_matrix_MatrixZuordnung'),

	NUCLET_TEST_I18N_ARTIKEL('nuclet_test_i18n_Artikel'),
	NUCLET_TEST_I18N_ARTIKELTYP('nuclet_test_i18n_Artikeltyp'),
	NUCLET_TEST_I18N_AUFTRAGSTYP('nuclet_test_i18n_Auftragstyp'),
	NUCLET_TEST_I18N_DATALANGAUFTRAG('nuclet_test_i18n_Datalangauftrag'),
	NUCLET_TEST_I18N_DATALANGPOS('nuclet_test_i18n_Datalangpos'),

	NUCLET_TEST_SUBFORM_PARENT('nuclet_test_subform_Parent'),
	NUCLET_TEST_SUBFORM_SUBFORM('nuclet_test_subform_Subform'),
	NUCLET_TEST_SUBFORM_SUBSUBFORM('nuclet_test_subform_Subsubform'),
	NUCLET_TEST_SUBFORM_SUBSUBSUBFORM('nuclet_test_subform_Subsubsubform'),

	NUCLET_TEST_SUBFORM_PARENTFORSTATEFULSUBFORM('nuclet_test_subform_ParentforStatefulSubform'),
	NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL('nuclet_test_subform_SubformStatusmodel'),
	NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL('nuclet_test_subform_SubsubformStatusmodel'),
	NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS('nuclet_test_subform_SubsubformStateless'),

	NUCLET_TEST_CHARTS_COMPANY('nuclet_test_charts_Company'),
	NUCLET_TEST_CHARTS_FINANCIALFIGURES('nuclet_test_charts_FinancialFigures'),

	NUCLET_TEST_UTILS_BULKTEST('nuclet_test_utils_BulkTest'),

	ORG_NUCLOS_SYSTEM_GENERALSEARCHDOCUMENT('org_nuclos_system_Generalsearchdocument'),

	CONFIGURATION_TEST_SAMPLESAMPLECONFIGURATIONENTITY('configuration_test_SampleConfigurationEntity'),

	NUCLOS_BUSINESSTEST('org_nuclos_system_Businesstest'),
	NUCLOS_ROLES('org_nuclos_system_Role'),
	NUCLOS_USERROLES('org_nuclos_system_RoleUser'),
	NUCLOS_USER('org_nuclos_system_User'),
	NUCLOS_PREFERENCE('95mD'),
	NUCLOS_JOBNOTIFICATION('LGYb'),
	NUCLOS_RELATIONTYPE('XFra'),
	NUCLOS_GENERICOBJECTRELATION('QhPT'),
	NUCLOS_LAF_ENTITYPARAMETER('zzbG')


	final String fqn

	private TestEntities(String fqn) {
		this.fqn = fqn
	}

	String toString() {
		fqn
	}
}
