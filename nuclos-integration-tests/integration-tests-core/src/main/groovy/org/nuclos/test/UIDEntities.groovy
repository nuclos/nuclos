package org.nuclos.test

import org.nuclos.common.UID

import groovy.transform.CompileStatic

@CompileStatic
enum UIDEntities implements EntityClass<UID> {
	NUCLOS_USER('org_nuclos_system_User')

	final String fqn

	private UIDEntities(String fqn) {
		this.fqn = fqn
	}

	@Override
	String toString() {
		fqn
	}
}