package org.nuclos.test.rest

import org.apache.http.HttpException
import org.apache.http.HttpHost
import org.apache.http.HttpRequest
import org.apache.http.HttpRequestInterceptor
import org.apache.http.client.CookieStore
import org.apache.http.client.config.CookieSpecs
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.conn.routing.HttpRoute
import org.apache.http.conn.routing.HttpRoutePlanner
import org.apache.http.impl.client.BasicCookieStore
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.conn.DefaultProxyRoutePlanner
import org.apache.http.impl.conn.DefaultRoutePlanner
import org.apache.http.impl.conn.DefaultSchemePortResolver
import org.apache.http.protocol.HttpContext
import org.json.JSONObject
import org.nuclos.common.Mutable
import org.nuclos.common.UID
import org.nuclos.schema.rest.News
import org.nuclos.server.rest.services.helper.RecursiveDependency
import org.nuclos.test.EntityClass
import org.nuclos.test.EntityObject
import org.nuclos.test.NuclosTestProxy
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.LoginParams

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.Har
import net.lightbody.bmp.proxy.CaptureType

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RESTClient {

	final ThreadLocal<Map<String, String>> additionalHeaders = new ThreadLocal<>()

	final LoginParams loginParams

	CloseableHttpClient httpClient
	CookieStore cookieStore
	String sessionId

	/**
	 * The IP address of this client, as seen by the server.
	 * If there is a proxy in between, might be the address of the proxy.
	 */
	String ipAddress

	/**
	 * Datalanguage set during login
	 */
	String initialDatalanguage

	CloseableHttpResponse getUrl(final String url) {
		def request = new HttpGet(url)
		httpClient.execute(request)
	}

	InputStream getInputStream(final String url) {
		RESTHelper.requestInputStream(url, org.springframework.http.HttpMethod.GET, sessionId)
	}

	File executeReport(UID reportUid) {
		RESTHelper.executeReport(reportUid, this)
	}

	File executeReport(UID reportUid, UID outputUid) {
		RESTHelper.executeReport(reportUid, outputUid, this)
	}

	class DynamicProxyRoutePlanner implements HttpRoutePlanner {

		private HttpRoutePlanner defaultRoutePlanner = new DefaultRoutePlanner(DefaultSchemePortResolver.INSTANCE)
		private HttpRoutePlanner proxyRoutePlanner

		DynamicProxyRoutePlanner(HttpHost host) {
			if (host) {
				proxyRoutePlanner = new DefaultProxyRoutePlanner(host)
			}
		}

		void setProxy(HttpHost host) {
			if (host) {
				proxyRoutePlanner = new DefaultProxyRoutePlanner(host)
			} else {
				proxyRoutePlanner = null
			}
		}

		HttpRoute determineRoute(
				HttpHost target,
				HttpRequest request,
				HttpContext context
		) {
			if (proxyRoutePlanner) {
				proxyRoutePlanner.determineRoute(target, request, context)
			} else {
				defaultRoutePlanner.determineRoute(target, request, context)
			}
		}
	}

	DynamicProxyRoutePlanner proxyRoutePlanner = new DynamicProxyRoutePlanner(null)

	RESTClient(final String username, final String password) {
		this(
				new LoginParams(
						username: username,
						password: password
				)
		)
	}

	RESTClient(final LoginParams loginParams) {
		this (
				RequestConfig.copy(RequestConfig.DEFAULT)
					.setCookieSpec(CookieSpecs.STANDARD)
					.build()
		)
		this.loginParams = loginParams
	}

	RESTClient(final boolean ignoreCookies) {
		this(
				RequestConfig.copy(RequestConfig.DEFAULT)
						.setCookieSpec(ignoreCookies ? CookieSpecs.IGNORE_COOKIES : CookieSpecs.STANDARD)
						.build()
		)
	}

	RESTClient(final RequestConfig requestConfig) {
		this.cookieStore = new BasicCookieStore()
		this.httpClient = HttpClients.custom()
				.setDefaultRequestConfig(requestConfig)
				.setDefaultCookieStore(cookieStore)
				.addInterceptorFirst(new ResponseSqlInterceptor())
				.addInterceptorFirst(new HttpRequestInterceptor() {
					@Override
					void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
						additionalHeaders.get()?.each { key, value ->
							request.addHeader(key, value)
						}
					}
				}).setRoutePlanner(proxyRoutePlanner)
				.build()
	}

	void changePassword(EntityObject<UID> user) {
		changePassword(user.getAttribute('username').toString(), user.getAttribute('oldPassword').toString(), user.getAttribute('newPassword').toString())
	}

	void changePassword(String username, String pwOld, String pwNew) {
		RESTHelper.changePassword(username, pwOld, pwNew, this)
	}

	RESTClient login() {
		def loginResult = RESTHelper.login(this)

		sessionId = loginResult.get('sessionId')
		ipAddress = loginResult.get('clientIp')
		initialDatalanguage = loginResult.get('datalanguage')

		return this
	}

	void logout() {
		RESTHelper.logout(this)
	}

	void setDataLanguage() {
		RESTHelper.setDataLanguage(this);
	}

	String getDataLanguage() {
		RESTHelper.getDataLanguage(this)
	}

	public <PK> PK save(EntityObject<PK> eo) {
		eo.uploadDocuments(this)
		RESTHelper.save(eo, this)
	}

	public <PK> EntityObject<PK> clone(EntityObject<PK> eo, String layout) {
		RESTHelper.clone(eo, this, layout)
	}

	public <PK> boolean delete(final String entityClassId, final PK id) {
		RESTHelper.delete(entityClassId, id, this)
	}

	public <PK> boolean delete(EntityObject<PK> eo) {
		RESTHelper.delete(eo, this)
	}

	public <PK> void deleteAll(Collection<EntityObject<PK>> eos) {
		RESTHelper.deleteAll(eos, this)
	}

	String uploadDocument(String filename, byte[] content) {
		RESTHelper.uploadDocument(filename, content, this)
	}

	/**
	 * TODO: Proper typing instead of JSONObject.
	 */
	JSONObject getEntityMeta(final EntityClass entityClass) {
		RESTHelper.getEntityMeta(entityClass, this)
	}

	public <PK> EntityObject<PK> getEntityObject(
			EntityClass<PK> entityClass,
			PK id
	) {
		RESTHelper.getEntityObject(entityClass, id, this)
	}

	public <PK> List<EntityObject<PK>> getEntityObjects(
			EntityClass<PK> entityClass,
			QueryOptions options = null
	) {
		RESTHelper.getEntityObjects(entityClass, this, options)
	}

	public <PK, SubPK> List<EntityObject<SubPK>> loadDependents(
			EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			String referenceAttributeId
	) {
		RESTHelper.loadDependents(eo, subEoClass, referenceAttributeId, this)
	}

	public <PK, SubPK> List<EntityObject<SubPK>> loadDependentsRecursively(
			EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			RecursiveDependency dependency,
			QueryOptions queryOptions = null,
			Mutable<Long> countTotalResult = null
	) {
		RESTHelper.loadDependentsRecursively(this, eo, subEoClass, dependency, queryOptions, countTotalResult)
	}

	public <PK> void executeCustomRule(
			EntityObject<PK> eo,
			String ruleName
	) {
		RESTHelper.executeCustomRule(eo, ruleName, this)
	}

	JSONObject getSessionData() {
		RESTHelper.getSessionData(this)
	}

	void saveAll(final List<EntityObject> entityObjects) {
		RESTHelper.saveAll(entityObjects, this)
	}

	public <PK> void changeState(EntityObject<PK> eo, int statusNumeral) {
		RESTHelper.changeState(eo, statusNumeral, this)
	}

	public <PK, PK2> EntityObject<PK2> generateObject(
			final EntityObject<PK> eo,
			final String generator,
			final EntityClass<PK2> resultClass
	) {
		return RESTHelper.generateObject(eo, generator, resultClass, this)
	}

	File exportResultList(
			final TestEntities entity,
			final String outputFormat
	) {
		return RESTHelper.exportResultList(entity, outputFormat, this)
	}

	File exportSubBoList(
			final TestEntities entity,
			String boId,
			final String refAttribute,
			final String outputFormat,
			String searchCondition = null
	) {
		return RESTHelper.exportSubBoList(entity, boId, refAttribute, outputFormat, searchCondition, this)
	}

	byte[] exportNuclet(String nucletName) {
		return RESTHelper.exportNuclet(nucletName, this)
	}

	JSONObject getMenuStructure() {
		RESTHelper.getMenuStructure(this)
	}

	JSONObject getPreferenceContent(String preferenceId) {
		def preference = RESTHelper.getPreference(preferenceId, this)
		if (preference) {
			return preference.getJSONObject('content')
		}
		return null
	}

	/**
	 * Adds the given HTTP headers to all direct REST requests that are made from the given Closure via this RESTClient.
	 */
	public <T> T withHeaders(Map<String, String> headers, Closure<T> c) {
		def previousHeaders = additionalHeaders.get()

		additionalHeaders.set(headers)

		try {
			return c()
		} finally {
			additionalHeaders.set(previousHeaders)
		}
	}

	/**
	 * Captures a HAR containing the requests / responses made by this RESTClient
	 * within the given Closure.
	 */
	Har getHar(Closure c) {
		def proxy = new NuclosTestProxy()
		proxy.start()

		proxyRoutePlanner.setProxy(proxy.httpHost)

		try {
			return proxy.getHar(
					[
							CaptureType.RESPONSE_HEADERS,
							CaptureType.RESPONSE_COOKIES
					],
					c
			)
		} finally {
			proxyRoutePlanner.setProxy(null)
			proxy.stop()
		}
	}

	String getDescriptionToStatus(String statusId) {
		return RESTHelper.getDescriptionToStatus(statusId, this)
	}

	List<News> getUnconfirmedNews() {
		RESTHelper.getUnconfirmedNews(this)
	}

	List<News> getCurrentNews() {
		RESTHelper.getCurrentNews(this)
	}
}
