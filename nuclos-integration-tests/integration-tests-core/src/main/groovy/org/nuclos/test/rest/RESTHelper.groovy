package org.nuclos.test.rest

import static org.nuclos.test.log.CallTrace.trace
import static org.nuclos.test.rest.RESTHelper.requestJson

import java.nio.file.Files

import org.apache.commons.io.Charsets
import org.apache.http.HttpHeaders
import org.apache.http.HttpResponse
import org.apache.http.client.methods.*
import org.apache.http.entity.ContentType
import org.apache.http.entity.StringEntity
import org.apache.http.entity.mime.HttpMultipartMode
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.util.EntityUtils
import org.apache.logging.log4j.Level
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.nuclos.common.Mutable
import org.nuclos.common.UID
import org.nuclos.schema.rest.News
import org.nuclos.server.rest.services.helper.RecursiveDependency
import org.nuclos.test.*
import org.nuclos.test.log.Log
import org.springframework.http.HttpMethod
import org.springframework.util.StreamUtils
import org.springframework.web.client.RestClientException

import com.fasterxml.jackson.databind.ObjectMapper

import groovy.json.JsonOutput
import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * Performs REST requests directly, instead of using the Webclient GUI.
 *
 */
@CompileStatic
class RESTHelper {

	static NuclosTestContext context = NuclosTestContext.instance
	static String BASE_URL = context.nuclosServerProtocol + '://' +
			context.nuclosServerHost + ':' +
			context.nuclosServerPort +
			(context.nuclosServerContext.length() > 0 ? '/' + context.nuclosServerContext : '');
	static String REST_BASE_URL = BASE_URL + '/rest'

	static JSONObject request(String url, HttpMethod httpMethod, RESTClient client, String json = null, failOnEmptyResponse = true) {
		String response = execRequest(url, httpMethod, client, json)
		if ("" == response && !failOnEmptyResponse) {
			return null
		}
		parseJsonObject(response)
	}

	static JSONArray requestArray(String url, HttpMethod httpMethod, RESTClient client, String json = null) {
		String response = execRequest(url, httpMethod, client, json)
		parseJsonArray(response)
	}

	@PackageScope
	static String execRequest(
			String urlOrPath,
			HttpMethod httpMethod,
			RESTClient client,
			String json = null
	) {
		String url = fixRestUrl(urlOrPath)
		HttpUriRequest request
		switch (httpMethod) {
			case HttpMethod.GET:
				if (json != null) {
					throw new UnsupportedOperationException('GET-Method with JSON not supported.')
				}
				request = new HttpGet(url)
				break
			case HttpMethod.POST:
				HttpPost post = new HttpPost(url)
				post.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON))
				request = post
				break
			case HttpMethod.PUT:
				HttpPut put = new HttpPut(url)
				put.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON))
				request = put
				break
			default:
				throw new UnsupportedOperationException('HTTP-Method ' + httpMethod + ' not supported.')
		}
		try (CloseableHttpResponse response = client.httpClient.execute(request)) {
			def statusLine = response.getStatusLine()
			int statusCode = statusLine.getStatusCode()
			if (statusCode >= 200 && statusCode <= 299) {
				def entity = response.getEntity()
				if (entity) {
					return EntityUtils.toString(entity)
				} else {
					return ''
				}
			} else {
				def entity = response.getEntity()
				def errorText = 'Reason=' + statusLine.getReasonPhrase() + ', Entity=' + (entity != null ? EntityUtils.toString(entity) : '')
				throw new RESTException(statusCode, errorText)
			}
		} catch (Exception ex) {
			if (ex instanceof RESTException) {
				throw ex;
			}
			def errorText = "Failed to execute request [url=$url, method=$httpMethod, json=$json]: $ex.message"
			Log.error (errorText, ex)
			throw new RESTException(-1, errorText, ex)
		}
	}

	static JSONObject requestJson(String url, HttpMethod httpMethod, String sessionId = null, String json = null, failOnEmptyResponse = true) {
		String response = requestString(url, httpMethod, sessionId, json)
		if ("" == response && !failOnEmptyResponse) {
			return null
		}
		parseJsonObject(response)
	}

	static JSONArray requestJsonArray(String url, HttpMethod httpMethod, String sessionId = null, String json = null) {
		String response = requestString(url, httpMethod, sessionId, json)
		parseJsonArray(response)
	}

	static String requestString(
			String urlOrPath,
			HttpMethod httpMethod,
			String sessionId = null,
			String json = null
	) {
		return getStreamText(requestInputStream(urlOrPath, httpMethod, sessionId, json))
	}

	static String fixRestUrl(String urlOrPath) {
		// Protocol-relative URL
		if (urlOrPath.startsWith('//')) {
			urlOrPath = 'http:' + urlOrPath
		}

		if (!urlOrPath.startsWith('http://') && !urlOrPath.startsWith('https://')) {
			if (!urlOrPath.startsWith('/')) {
				urlOrPath = '/' + urlOrPath
			}
			urlOrPath = REST_BASE_URL + urlOrPath
		}
		return urlOrPath;
	}

	/**
	 * @deprecated Get rid of HttpURLConnection, use only Apache HttpClient from RESTClient!
	 */
	@Deprecated
	static InputStream requestInputStream(
			String urlOrPath,
			HttpMethod httpMethod,
			String sessionId = null,
			String json = null
	) {
		urlOrPath = fixRestUrl(urlOrPath)

		HttpURLConnection urlConnection = (HttpURLConnection) new URL(urlOrPath).openConnection();
		urlConnection.setRequestMethod(httpMethod.name())
		urlConnection.setRequestProperty("Accept", "application/json, text/plain, */*")
		urlConnection.setRequestProperty("Content-Type", "application/json")

		if (sessionId != null) {
			urlConnection.setRequestProperty("Cookie", "JSESSIONID=" + sessionId)
		}

		urlConnection.doOutput = true

		if (json != null) {
			def writer = new OutputStreamWriter(urlConnection.outputStream)
			writer.write(json.trim())
			writer.flush()
			writer.close()
		}
		urlConnection.connect()

		try {
			urlConnection.getInputStream()
		} catch (Exception e) {
			int statusCode = urlConnection.getResponseCode()
			String errorText = getStreamText(urlConnection.getErrorStream())

			throw new RESTException(statusCode, errorText, e)
		}
	}

	private static String getStreamText(InputStream is) {
		StreamUtils.copyToString(
				is,
				Charsets.UTF_8
		)
	}

	static JSONObject login(final RESTClient client) {
		def params = client.loginParams

		JSONObject postData = new JSONObject()
				.put('username', params.username)
				.put('password', params.password)
				.put('datalanguage', params.datalanguage)
				.put('autologin', params.autologin)
				.put('locale', params.locale)
				.put('mandator', params.mandator)

		HttpPost post = new HttpPost(REST_BASE_URL)
		post.setEntity(new StringEntity(postData.toString()))
		post.setHeader(HttpHeaders.CONTENT_TYPE, 'application/json')

		JSONObject loginResult = requestJsonObject(client, post)
		client.sessionId = loginResult.get('sessionId')
		return loginResult
	}

	static void logout(final RESTClient client) {
		HttpDelete request = new HttpDelete(REST_BASE_URL)

		requestString(client, request)
	}

	static String getDataLanguage(RESTClient client) {
		def result = requestJson(REST_BASE_URL + '/datalanguage', HttpMethod.GET, client.sessionId, null)
		return result.get('datalanguage')
	}

	static void setDataLanguage(RESTClient client) {
		def params = client.loginParams

		JSONObject putData = new JSONObject()
				.put('datalanguage', params.datalanguage)

		HttpPut put = new HttpPut(REST_BASE_URL + '/datalanguage')
		put.setEntity(new StringEntity(putData.toString()))
		put.setHeader(HttpHeaders.CONTENT_TYPE, 'application/json')

		requestJsonObject(client, put)
	}

	@PackageScope
	static JSONObject getSessionData(RESTClient client) {
		def result = requestJson(REST_BASE_URL, HttpMethod.GET, client.sessionId, null)
		return result
	}

	@PackageScope
	static JSONObject getMenuStructure(RESTClient client) {
		requestJson(REST_BASE_URL + '/meta/menustructure', HttpMethod.GET, client.sessionId)
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static JSONObject createBo(Map bo, RESTClient client) {
		String entityClassId = bo['boMetaId']
		trace("Create EO ${entityClassId}") {
			def url = REST_BASE_URL + '/bos/' + entityClassId
			JSONObject result = requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(bo))

			return result
		} as JSONObject
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static Object createBo(
			String boMetaId,
			Map<String, Serializable> attributes,
			RESTClient client
	) {
		trace('Creat EO: ' + boMetaId) {
			def bo = [
					boMetaId  : boMetaId,
					attributes: [:]
			]

			for (Map.Entry<String, Serializable> attribute : attributes.entrySet()) {
				bo['attributes'][attribute.key] = attribute.value
			}

			def url = REST_BASE_URL + '/bos/' + boMetaId
			return requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(bo))
		}
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static void updateBo(Map bo, RESTClient client) {
		String entityClassId = bo['boMetaId']
		trace("Update EO ${entityClassId}") {
			def url = REST_BASE_URL + '/bos/' + entityClassId + '/' + bo['boId']
			requestJson(url, HttpMethod.PUT, client.sessionId, JsonOutput.toJson(bo))
		}
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static void updateBo(JSONObject bo, RESTClient client) {
		String entityClassId = bo['boMetaId']
		trace("Update EO ${entityClassId}") {
			def url = REST_BASE_URL + '/bos/' + entityClassId + '/' + bo['boId']
			requestJson(url, HttpMethod.PUT, client.sessionId, bo.toString())
		}
	}

	/**
	 * Use {@link #getEntityObjects(org.nuclos.test.EntityClass, RESTClient)}.
	 */
	@Deprecated
	static JSONObject getBos(String boMetaId, RESTClient client, String searchFilterId = null) {
		def url = REST_BASE_URL + '/bos/' + boMetaId
		if (searchFilterId != null) {
			url += '?searchFilter=' + searchFilterId
		}
		return requestJson(url, HttpMethod.GET, client.sessionId, null)
	}

	/**
	 * Use {@link #getEntityObjects(org.nuclos.test.EntityClass, RESTClient)}.
	 */
	@Deprecated
	static JSONObject getBosByQueryContext(String boMetaId, Map queryContext, RESTClient client) {
		def url = REST_BASE_URL + '/bos/' + boMetaId + '/query';
		return requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(queryContext));
	}

	static byte[] exportNuclet(String nucletName, RESTClient client) {
		def url = REST_BASE_URL + '/maintenance/nucletexport/' + nucletName
		InputStream is = requestInputStream(url, HttpMethod.POST, client.getSessionId());
		return StreamUtils.copyToByteArray(is)
	}

	static RestResponse importNuclet(String nuclet, RESTClient client) {
		def request = new HttpPost(REST_BASE_URL + '/maintenance/nucletimport')
		def nucletDir = System.getProperty("basedir")
		nucletDir = nucletDir.substring(0, nucletDir.lastIndexOf("nuclos-integration-tests/"))
		def data = MultipartEntityBuilder.create()
				.setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
				.addBinaryBody(
						"file",
						new File(nucletDir + "/nuclos-integration-tests/testnuclets/" + nuclet),
						ContentType.DEFAULT_BINARY,
						nuclet
				)
				.build()
		request.setEntity(data)
		request.setHeader("JSESSIONID", client.getSessionId())

		requestResponse(client, request)
	}

	/**
	 *
	 * @param name
	 * @param password
	 * @param roles
	 * @param client
	 * @return
	 */
	static EntityObject<String> createUser(
			String name,
			String password,
			List roles,
			RESTClient client,
			String email = name + '@nuclos.de',
			boolean loginWithEmailAllowed = false,
			boolean createAsSuperUser = false,
			String loginRestriction = null
	) {
		EntityObject user = findUserByUsername(name, client)
		if (user != null) {
			// delete already existing user
			client.delete(user)
		}

		EntityObject<String> eoUser = new EntityObject(SystemEntities.USER)
		eoUser.setAttribute('username', name)
		eoUser.setAttribute('firstname', name)
		eoUser.setAttribute('lastname', name)
		eoUser.setAttribute('email', email)
		eoUser.setAttribute('newPassword', password)
		eoUser.setAttribute('newPasswordConfirm', password)
		eoUser.setAttribute('setPassword', true)
		eoUser.setAttribute('superuser', createAsSuperUser)
		eoUser.setAttribute('loginWithEmailAllowed', loginWithEmailAllowed)
		if (loginRestriction != null) {
			eoUser.setAttribute('loginRestriction', loginRestriction)
		}
		client.save(eoUser)

		createUserRoles(eoUser as EntityObject<String>, roles, client)

		return eoUser as EntityObject<String>
	}

	static void changePassword(String username, String pwold, String pwnew, RESTClient client) {
		String url = REST_BASE_URL + "/user/" + username + "/password"
		requestString(url, HttpMethod.PUT, client.sessionId, JsonOutput.toJson(["oldPassword":pwold, "newPassword":pwnew]))
	}

	static EntityObject<String> findUserByUsername(String username,
	                                       RESTClient client) {
		return client.getEntityObjects(SystemEntities.USER,
				new QueryOptions(where: "org_nuclos_system_User_username='$username'")).stream().findFirst().orElse(null)
	}

	@PackageScope
	static boolean deleteUserByEmail(
			String email,
			SuperuserRESTClient client
	) {
		QueryOptions options = new QueryOptions(where: "org_nuclos_system_User_email='$email'")
		List<EntityObject<String>> results = getEntityObjects(SystemEntities.USER, client, options)

		if (!results.empty) {
			delete(results[0], client)
			return true
		}

		return false
	}

	static boolean deleteUserByUsername(
			String username,
			SuperuserRESTClient client
	) {
		QueryOptions options = new QueryOptions(where: "org_nuclos_system_User_username='$username'")
		List<EntityObject<String>> results = getEntityObjects(SystemEntities.USER, client, options)

		if (!results.empty) {
			delete(results[0], client)
			return true
		}

		return false
	}

	static String maintenanceModeUpdate(String mode, final RESTClient client) {
		requestString(REST_BASE_URL + '/maintenance/mode', HttpMethod.PUT, client.sessionId, mode)
	}

	static String maintenanceModeStatus(final RESTClient client) {
		requestString(REST_BASE_URL + '/maintenance/mode', HttpMethod.GET, client.sessionId)
	}

	@PackageScope
	static void executeCustomRule(
			final EntityObject eo,
			final String ruleName,
			final RESTClient client
	) {
		eo.executeCustomRule = ruleName
		save(eo, client)
	}

	@PackageScope
	static String managementConsole(
			String command,
			String arguments,
			RESTClient client
	) {

		def url = REST_BASE_URL + '/maintenance/managementconsole/' + command + '/'
		if (arguments) {
			String urlEncodedArguments = URLEncoder.encode(arguments, "UTF-8")
			url += urlEncodedArguments + '/'
		}

		requestString(url, HttpMethod.POST, client.sessionId)
	}

	@PackageScope
	static String startJob(
			String jobFullQualifiedName,
			RESTClient client
	) {
		def url = REST_BASE_URL + '/maintenance/jobs/' + jobFullQualifiedName + '/start'
		requestJson(url, HttpMethod.POST, client.sessionId).get('status')
	}

	@PackageScope
	static String invalidateServerCaches(SuperuserRESTClient client) {
		managementConsole('invalidateAllCaches', null, client)
	}

	@PackageScope
	static <PK> List<EntityObject<PK>> getEntityObjects(
			EntityClass<PK> entityClass,
			RESTClient client,
			QueryOptions options = null
	) {
		String url = REST_BASE_URL + '/bos/' + entityClass.fqn

		if (options) {
			// TODO: Make sure that url does not already contain a query
			url += '/query?' + options.toQuery()
		}

		JSONObject json = requestJson(url, HttpMethod.GET, client.sessionId, null)
		if (json == null) {
			return null
		}

		json.getJSONArray('bos').collect { eoJson ->
			eoFromJson(entityClass, (JSONObject)eoJson, client)
		}
	}

	@PackageScope
	static <PK, SubPK> List<EntityObject<SubPK>> loadDependents(
			EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			String referenceAttributeId,
			RESTClient client
	) {
		String url = REST_BASE_URL +
				'/bos/' + eo.entityClass.fqn +
				'/' + eo.id +
				'/subBos/' + referenceAttributeId

		return requestSubEoList(url, client, subEoClass)
	}

	@PackageScope
	static <PK, SubPK> List<EntityObject<SubPK>> loadDependentsRecursively(
			final RESTClient client,
			final EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			final RecursiveDependency dependency,
			final QueryOptions queryOptions = null,
			final Mutable<Long> countTotalResult = null
	) {
		String url = REST_BASE_URL +
				'/bos/' + eo.entityClass.fqn +
				'/' + eo.id +
				'/subBos/recursive' +
				dependency.toUrlPath()

		return requestSubEoList(url, client, subEoClass, queryOptions, countTotalResult)
	}

	private static <SubPK> List<EntityObject<SubPK>> requestSubEoList(
			String url,
			RESTClient client,
			EntityClass<SubPK> subEoClass,
			final QueryOptions queryOptions = null,
			final Mutable<Long> countTotalResult = null
	) {

		String jsonBody = null
		HttpMethod method =  HttpMethod.GET

		if (queryOptions) {
			if (queryOptions.method == HttpMethod.POST) {
				// TODO: Make sure that url does not already contain a query
				url += '/query'
				jsonBody = queryOptions.toJson()
			} else {
				// TODO: Make sure that url does not already contain a query
				url += '?' + queryOptions.toQuery()
			}
		}


		JSONObject json = requestJson(url, method, client.sessionId, jsonBody)
		if (json == null) {
			return null
		}

		if (queryOptions && countTotalResult != null && json.has('total')) {
			countTotalResult.value = json.getLong('total');
		}

		json.getJSONArray('bos').collect { eoJson ->
			eoFromJson(subEoClass, (JSONObject)eoJson, client)
		}
	}

	/**
	 * TODO: Proper typing instead of JSONObject.
	 */
	@PackageScope
	static JSONObject getEntityMeta(
			final EntityClass entityClass,
			final RESTClient client
	) {
		String url = REST_BASE_URL + '/boMetas/' + entityClass.fqn

		requestJson(url, HttpMethod.GET, client.sessionId, null)
	}

	@PackageScope
	static <PK> EntityObject<PK> getEntityObject(EntityClass<PK> entityClass, PK id, RESTClient client) {
		String url = REST_BASE_URL + '/bos/' + entityClass.fqn + '/' + id

		JSONObject json = requestJson(url, HttpMethod.GET, client.sessionId, null)

		if (json == null) {
			return null
		}
		eoFromJson(entityClass, json, client)
	}

	static <PK> void saveAll(List<EntityObject<PK>> eos, RESTClient client) {
		// TODO: Save all at once (see NUCLOS-5650)
		eos.each {
			save(it, client)
		}
	}

	static <PK> PK save(EntityObject<PK> eo, RESTClient client) {
		JSONObject json
		String url = REST_BASE_URL + '/bos/' + eo.getEntityClass().fqn

		if (eo.new) {
			json = postEntityObject(url, eo, client)
		} else {
			url += '/' + eo.id
			json = putEntityObject(url, eo, client)
		}

		if (json != null) {
			eo.updateFromJson(json)
		}

		eo.client = client

		return eo.getId()
	}

	@PackageScope
	static <PK> EntityObject<PK> clone(EntityObject<PK> eo, RESTClient client, String layout = null) {
		String url = REST_BASE_URL + '/bos/' + eo.getEntityClass().fqn + '/' + eo.id + '/clone' +
				(layout != null ? ('?layoutid=' + layout) : '')

		EntityObject<PK> clonedEo = new EntityObject<PK>(eo.entityClass);
		clonedEo.client = client;
		JSONObject clonedJson = requestJson(url, HttpMethod.GET, client.sessionId)
		clonedEo.updateFromJson(clonedJson)

		return clonedEo
	}

	@PackageScope
	static <PK> boolean delete(EntityObject<PK> eo, RESTClient client) {
		boolean result = delete(
				eo.entityClass.fqn,
				eo.id,
				client
		)
		if (result) {
			eo.setBoId(null)
		}
		return result
	}

	@PackageScope
	static <PK> boolean delete(
			final String entityClassId,
			final PK id,
			RESTClient client
	) {
		String url = REST_BASE_URL + '/bos/' + entityClassId + '/' + id
		String response = requestString(url, HttpMethod.DELETE, client.sessionId)
		if (response == null) {
			return false
		}
		return true
	}

	@PackageScope
	static <PK> void changeState(EntityObject<PK> eo, int statusNumeral, RESTClient client) {
		String url = REST_BASE_URL + '/boStateChanges/' + eo.getEntityClass().fqn + '/' + eo.getId() + '/' + statusNumeral

		requestString(url, HttpMethod.PUT, client.sessionId, '{}')
	}



	@PackageScope
	static <PK> String uploadDocument(String filename, byte[] content, RESTClient client) {
		String url = REST_BASE_URL + '/boDocuments/upload'

		def entity = MultipartEntityBuilder
				.create()
				.addBinaryBody("file", content, ContentType.APPLICATION_OCTET_STREAM, filename)
				.build()

		HttpUriRequest request = new HttpPost(url)
		request.setEntity(entity)
		requestString(client, request)
	}

	private static <PK> JSONObject postEntityObject(String url, EntityObject<PK> eo, RESTClient client) {
		postJson(url, eo.toJson(), client)
	}

	static JSONObject postJson(final String url, final String json, final RESTClient client) {
		request(url, HttpMethod.POST, client, json)
	}

	static JSONObject putJson(final String url, final String json, final RESTClient client) {
		request(url, HttpMethod.PUT, client, json)
	}

	private static <PK> JSONObject putEntityObject(String url, EntityObject<PK> eo, RESTClient client) {
		request(url, HttpMethod.PUT, client, eo.toJson(),
				false // failOnEmptyResponse: Could be empty (e.g. org.nuclos.test.server.LogicalDeletableTest._02_physicalDeletion)
		)
	}

	@PackageScope
	static Map<String, String> getSimpleSystemParameters(
			SuperuserRESTClient client
	) {
		getSystemParameters(client).collectEntries {
			[(it.getAttribute('name')): it.getAttribute('value')]
		}
	}

	@PackageScope
	static List<EntityObject<String>> getSystemParameters(
			SuperuserRESTClient client
	) {
		getEntityObjects(SystemEntities.PARAMETER, client)
	}

	@PackageScope
	static List<EntityObject<String>> setSystemParameters(
			Map<String, String> newParams,
			SuperuserRESTClient client
	) {
		List<EntityObject<String>> params = getSystemParameters(client)

		params.each {
			delete(it, client)
		}

		patchSystemParameters(newParams, client)
	}

	static EntityObject<Object> addBOLAFParameter(String key, String value, String entityUID, SuperuserRESTClient client) {
		EntityObject<Object> eoLAF = new EntityObject(TestEntities.NUCLOS_LAF_ENTITYPARAMETER)
		eoLAF.setAttribute('parameter', key)
		eoLAF.setAttribute('value', value)
		eoLAF.setAttribute('entity', [id: entityUID])
		client.save(eoLAF)
		return eoLAF
	}

	static void deleteBOLAFParameter(EntityObject<Object> entityToDelete, SuperuserRESTClient client) {
		client.delete(entityToDelete)
	}

	@PackageScope
	static List<EntityObject<String>> patchSystemParameters(
			Map<String, String> newParams,
			SuperuserRESTClient client
	) {
		List<EntityObject<String>> params = getSystemParameters(client)

		newParams.each { String key, String value ->
			EntityObject<String> param = params.find { it.getAttribute('name') == key }

			if (!param) {
				param = new EntityObject(SystemEntities.PARAMETER)

				param.setAttribute('name', key)
				param.setAttribute('description', key)

				params << param
			}

			param.setAttribute('value', value)
			save(param, client)
		}

		return params
	}

	private static <PK> EntityObject<PK> eoFromJson(
			EntityClass<PK> entityClass,
			JSONObject json,
			RESTClient client
	) {
		EntityObject<PK> eo = new EntityObject(entityClass)

		eo.updateFromJson(json)
		eo.client = client

		return eo
	}

	static JSONArray getReferenceList(
			String referenceAttributeId,
			RESTClient client) {
		String url = REST_BASE_URL +
				'/data/referencelist/' + referenceAttributeId
		JSONArray jsonArray = requestJsonArray(url, HttpMethod.GET, client.sessionId, null)
		return jsonArray
	}

	static JSONArray getVlpData(String referenceAttributeId,
								String vlpType, String vlpUID, String idColumn, String nameColumn,
								String intid, RESTClient client) {
		String url = REST_BASE_URL +
				'/data/vlpdata' +
				'?reffield=' + referenceAttributeId +
				'&vlptype=' + vlpType +
				'&vlpvalue=' + vlpUID +
				'&id-fieldname=' + idColumn +
				'&fieldname=' + nameColumn +
				'&intid=' + intid
		JSONArray jsonArray = requestJsonArray(url, HttpMethod.GET, client.sessionId, null)
		return jsonArray
	}

	private static JSONObject requestJsonObject(
			final RESTClient client,
			final HttpRequestBase request
	) {
		request.setHeader(HttpHeaders.ACCEPT, 'application/json')
		String response = requestString(client, request)
		parseJsonObject(response)
	}

	private static JSONObject parseJsonObject(String response) {
		JSONObject result = null
		if (response != null) {
			try {
				result = new JSONObject(response)
			} catch (JSONException e) {
				Log.error('Parsing response \"' + response + '\" to JSON failed: ',e)
				throw e
			}
		}
		result
	}

	private static JSONArray parseJsonArray(String response) {
		JSONArray result = null
		if (response != null) {
			try {
				result = new JSONArray(response)
			} catch (JSONException e) {
				Log.error(e.getMessage(), e)
			}
		}
		result
	}

	static String requestString(
			final RESTClient client,
			final HttpUriRequest request
	) {
		HttpResponse response = client.httpClient.execute(request)
		String responseText = null

		if (response.entity) {
			responseText = getStreamText(response.entity.content)
		}

		if ((response.statusLine.statusCode / 100).toInteger() != 2) {
			throw new RESTException(
					response.statusLine.statusCode,
					responseText
			)
		}

		return responseText
	}

	static RestResponse requestResponse(
			final RESTClient client,
			final HttpRequestBase request
	) {
		HttpResponse response = client.httpClient.execute(request)

		new RestResponse(response)
	}

	@PackageScope
	static void putServerLogLevel(
			final SuperuserRESTClient client,
			final ServerLogger serverLogger,
			final Level level) {
		String url = "$REST_BASE_URL/maintenance/logging/$serverLogger/level"

		JSONObject data = new JSONObject()
		data.put("level", level.toString())

		HttpPut put = new HttpPut(url)
		addJson(put, data)

		requestString(client, put)
	}

	@PackageScope
	static void putSqlDebug(
			final SuperuserRESTClient client,
			final String debugSqlString,
			final int minExecTime = 0) {

		String url = "$REST_BASE_URL/maintenance/logging/debugSQL"

		JSONObject data = new JSONObject()
		data.put("debugSQL", debugSqlString)
		data.put("minExecTime", minExecTime)

		HttpPut put = new HttpPut(url)
		addJson(put, data)

		requestString(client, put)
	}

	private static void addJson(
			HttpEntityEnclosingRequestBase request,
			JSONObject data
	) {
		StringEntity entity = new StringEntity(data.toString())
		request.setEntity(entity)
		request.setHeader(HttpHeaders.CONTENT_TYPE, 'application/json')
	}

	static <PK, PK2> EntityObject<PK2> generateObject(
			final EntityObject<PK> eo,
			final String generatorFqn,
			final EntityClass<PK2> resultClass,
			final RESTClient client
	) {
		final String url = REST_BASE_URL + '/boGenerations' +
				'/' + eo.getEntityClass().fqn +
				'/' + eo.getId() +
				'/' + eo.getVersion() +
				'/generate/' + generatorFqn

		final JSONObject json = postJson(url, new JSONObject().toString(), client)

		if (json.has('businessError')) {
			throw new RestClientException("Business Error: " + json.get('businessError'))
		}

		EntityObject.fromJson(resultClass, json.getJSONObject('bo'))
	}

	static <PK> void deleteAll(
			final Collection<EntityObject<PK>> entityObjects,
			final RESTClient restClient
	) {
		// TODO: Delete all at once (see NUCLOS-5650)
		entityObjects.each {
			delete(it, restClient)
		}
	}

	static File exportResultList(
			final EntityClass entity,
			final String outputFormat,
			final RESTClient client
	) {
		final String url = REST_BASE_URL + '/bos/' + entity.fqn +
				'/boListExport' +
				'/' + outputFormat +
				'/false' +
				'/false'
		JSONObject json = requestJson(
				url,
				HttpMethod.POST,
				client.getSessionId(),
				new JSONObject().toString()
		)
		String downloadUrl = json.getJSONObject('links').getJSONObject('export').getString('href')

		// URL might be protocol-relative
		if (downloadUrl.startsWith('//')) {
			downloadUrl = 'http:' + downloadUrl
		}

		// replace :8080 from nginx proxy with actual port
		downloadUrl = downloadUrl.replace(":8080", ":" + context.nuclosServerPort)

		downloadFile(downloadUrl.toURL(), client)
	}

	static File exportSubBoList(EntityClass entity, String boId, String refAttribute, String outputFormat, String searchCondition, RESTClient client) {
		final String url = REST_BASE_URL +
				'/bos/' + entity.fqn +
				'/' + boId +
				'/subBos/' + refAttribute +
				'/export/' + outputFormat +
				'/false' +
				'/false' + (
					searchCondition != null ? ("?searchCondition=$searchCondition") : ''
				)
		JSONObject json = requestJson(
				url,
				HttpMethod.POST,
				client.getSessionId(),
				new JSONObject().toString()
		)
		String downloadUrl = json.getJSONObject('links').getJSONObject('export').getString('href')

		// URL might be protocol-relative
		if (downloadUrl.startsWith('//')) {
			downloadUrl = 'http:' + downloadUrl
		}

		// replace :8080 from nginx proxy with actual port
		downloadUrl = downloadUrl.replace(":8080", ":" + context.nuclosServerPort)
		downloadFile(downloadUrl.toURL(), client)
	}

	static File downloadFile(final URL url, final RESTClient client) {
		HttpGet request = new HttpGet(url.toURI())
		request.setHeader("JSESSIONID", client.getSessionId())
		RestResponse response = requestResponse(client, request)
		InputStream responseStream = response.getHttpResponse().getEntity().getContent()

		File file = Files.createTempFile('nuclos_rest_download', null).toFile()

		BufferedOutputStream stream = file.newOutputStream()
		stream << responseStream
		stream.close()

		return file
	}

	static String getDescriptionToStatus(
			String statusId,
			RESTClient client) {
		String url = REST_BASE_URL +
				'/data/statusinfo/' + statusId
		JSONObject json = requestJson(
				url,
				HttpMethod.GET,
				client.getSessionId()
		)
		return json.get('description')
	}

	static List<News> getCurrentNews(final RESTClient client) {
		getNews(
				REST_BASE_URL + '/news',
				client
		)
	}

	static List<News> getUnconfirmedNews(final RESTClient client) {
		getNews(
				REST_BASE_URL + '/news/unconfirmed',
				client
		)
	}

	static List<News> getNews(final String url, final RESTClient client) {
		final String json = requestString(
				url,
				HttpMethod.GET,
				client.getSessionId()
		)

		new ObjectMapper().readValue(json, News[].class).toList()
	}

	static String callCustomRestRuleWithSqlCountCheck(RESTClient client, HttpRequestBase base,
	                                                  int maxSqlCount, int maxSqlWriteCount) {
		base.setHeader(HttpHeaders.CONTENT_TYPE, 'application/json')

		RestResponse response = RESTHelper.requestResponse(client, base)

		assert response.getSqlCount() <= maxSqlCount
		assert response.getSqlInsertUpdateDeleteCount() <= maxSqlWriteCount

		InputStream is = response.getHttpResponse().getEntity().getContent()
		String s = StreamUtils.copyToString(is, Charsets.UTF_8)

		return s
	}

	static void createUserRoles(EntityObject<String> user, List roles, RESTClient restClient) {
		if (!roles.isEmpty()) {
			roles.forEach {
				roleName ->
					def roleObject = restClient.getEntityObjects(SystemEntities.ROLE,
							new QueryOptions(where: "org_nuclos_system_Role_name='$roleName'")).stream().findFirst().orElse(null)

					EntityObject<String> eoUserRole = new EntityObject(SystemEntities.ROLE_USER)
					eoUserRole.setAttribute('role', [id: roleObject.id])
					eoUserRole.setAttribute('user', [id: user.id])
					restClient.save(eoUserRole)
			}
		}
	}

	static boolean isDevMode(RESTClient restClient) {
		return requestJson(REST_BASE_URL + '/meta/systemparameters', HttpMethod.GET, restClient.getSessionId()).get('ENVIRONMENT_DEVELOPMENT')
	}

	static JSONObject getPreference(String preferenceId, RESTClient client) {
		requestJson(REST_BASE_URL + '/preferences/' + preferenceId, HttpMethod.GET, client.getSessionId())
	}

	static File executeReport(UID reportUid, RESTClient client) {
		return executeReport(reportUid, new UID('mRj3hiVf5Ln3qt69aWKt'), client)
	}

	static File executeReport(UID reportUid, UID outputFormatUid, RESTClient client) {
		final String url = REST_BASE_URL + '/reports/' + reportUid +
				'/execute'



		def data = JsonOutput.toJson([printoutId: reportUid.getString(), outputFormats: [[parameters: [], outputFormatId: outputFormatUid.getString(), selected: true]]])

		JSONObject json = requestJson(
				url,
				HttpMethod.POST,
				client.getSessionId(),
				data
		)
		String downloadUrl = json.getJSONArray('outputFormats').getJSONObject(0).getJSONObject('links').getJSONObject('file').getString('href')

		// URL might be protocol-relative
		if (downloadUrl.startsWith('//')) {
			downloadUrl = 'http:' + downloadUrl
		}

		// replace :8080 from nginx proxy with actual port
		downloadUrl = downloadUrl.replace(":8080", ":"+ context.nuclosServerPort)
		downloadFile(downloadUrl.toURL(), client)
	}
}
