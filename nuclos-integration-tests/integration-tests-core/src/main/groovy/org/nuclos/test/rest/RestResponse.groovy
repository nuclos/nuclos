package org.nuclos.test.rest

import org.apache.commons.io.Charsets
import org.apache.http.Header
import org.apache.http.HttpResponse
import org.springframework.util.StreamUtils

import groovy.transform.CompileStatic
import io.netty.handler.codec.http.HttpHeaders

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RestResponse {

	public static final String NUCLOS_SQL_COUNT = 'nuclos-sql-count'
	public static final String NUCLOS_SQL_INSERT_UPDATE_DELETE_COUNT = 'nuclos-sql-insert-update-delete-count'
	public static final String NUCLOS_SQL_TIME = 'nuclos-sql-time'

	private final HttpResponse httpResponse
	private final Integer sqlCount
	private final Integer sqlInsertUpdateDeleteCount
	private final Integer sqlTime

	private String responseText

	RestResponse(final HttpResponse httpResponse) {
		this.httpResponse = httpResponse

		sqlCount = getIntValueFromHeader(httpResponse, NUCLOS_SQL_COUNT)
		sqlInsertUpdateDeleteCount = getIntValueFromHeader(httpResponse, NUCLOS_SQL_INSERT_UPDATE_DELETE_COUNT)
		sqlTime = getIntValueFromHeader(httpResponse, NUCLOS_SQL_TIME)
	}

	RestResponse(final io.netty.handler.codec.http.HttpResponse httpResponse) {
		HttpHeaders headers = httpResponse.headers()
		sqlCount = headers.get(NUCLOS_SQL_COUNT) as Integer
		sqlInsertUpdateDeleteCount = headers.get(NUCLOS_SQL_INSERT_UPDATE_DELETE_COUNT) as Integer
		sqlTime = headers.get(NUCLOS_SQL_TIME) as Integer
	}

	private Integer getIntValueFromHeader(HttpResponse httpResponse, String headerName) {
		Header header = httpResponse.getFirstHeader(headerName)

		if (header) {
			return Integer.parseInt(header.value)
		}

		null
	}

	HttpResponse getHttpResponse() {
		return httpResponse
	}

	Integer getSqlCount() {
		return sqlCount
	}

	Integer getSqlInsertUpdateDeleteCount() {
		return sqlInsertUpdateDeleteCount
	}

	Integer getSqlTime() {
		return sqlTime
	}

	String getResponseText() {
		if (!responseText) {
			responseText = StreamUtils.copyToString(
					httpResponse.entity.content,
					Charsets.UTF_8
			)
		}

		return responseText
	}
}
