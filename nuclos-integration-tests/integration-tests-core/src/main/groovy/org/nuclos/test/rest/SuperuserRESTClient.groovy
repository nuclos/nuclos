package org.nuclos.test.rest

import org.apache.logging.log4j.Level
import org.nuclos.test.EntityObject
import org.nuclos.test.SystemEntities
import org.nuclos.test.rest.service.BusinessTestService
import org.nuclos.test.webclient.LoginParams

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class SuperuserRESTClient extends RESTClient {

	SuperuserRESTClient(final String user, final String password, final Locale locale) {
		super(new LoginParams(
				username: user,
				password: password,
				locale: locale
		))
	}

	SuperuserRESTClient(final String user, final String password) {
		super(user, password)
	}

	@Override
	SuperuserRESTClient login() {
		return (SuperuserRESTClient) super.login()
	}

	Map<String, String> getSimpleSystemParameters() {
		RESTHelper.getSimpleSystemParameters(this)
	}

	List<EntityObject<String>> getSystemParameters(
	) {
		RESTHelper.getEntityObjects(SystemEntities.PARAMETER, this)
	}

	List<EntityObject<String>> setSystemParameters(
			Map<String, String> newParams
	) {
		RESTHelper.setSystemParameters(newParams, this)
	}

	List<EntityObject<String>> patchSystemParameters(
			Map<String, String> newParams
	) {
		RESTHelper.patchSystemParameters(newParams, this)
	}

	EntityObject<Object> addBOLAFParameter(String key, String value, String entityUID) {
		RESTHelper.addBOLAFParameter(key, value, entityUID, this)
	}

	void removeBOLAFParameter(EntityObject<Object> entity) {
		RESTHelper.deleteBOLAFParameter(entity, this)
	}

	boolean deleteUserByEmail(
			String email
	) {
		RESTHelper.deleteUserByEmail(email, this)
	}

	boolean deleteUserByUsername(
			String username
	) {
		RESTHelper.deleteUserByUsername(username, this)
	}

	String invalidateServerCaches() {
		RESTHelper.invalidateServerCaches(this)
	}

	String managementConsole(
			String command,
			String arguments = null
	) {
		RESTHelper.managementConsole(command, arguments, this)
	}

	String startJob(
			String jobFullQualifiedName
	) {
		RESTHelper.startJob(jobFullQualifiedName, this)
	}

	void setServerLogLevel(ServerLogger logger, Level level) {
		RESTHelper.putServerLogLevel(this, logger, level)
	}

	void setDebugSql(String debugSqlString, int minExecTime = 0) {
		RESTHelper.putSqlDebug(this, debugSqlString, minExecTime)
	}

	BusinessTestService getBusinesstestService() {
		new BusinessTestService(this)
	}

	void startMaintenance() {
		RESTHelper.managementConsole('startMaintenance', null, this)
	}

	void stopMaintenance() {
		RESTHelper.managementConsole('stopMaintenance', null, this)
	}

	boolean isMaintenance() {
		RESTHelper.managementConsole('showMaintenance', null, this).startsWith('on')
	}

	boolean isDevMode() {
		return RESTHelper.isDevMode(this);
	}
}
