package org.nuclos.test.rest

import org.nuclos.test.TestEntities

/**
 * TODO: Javadoc
 * TODO: @CompileStatic
 */
class TestDataHelper {

	static File nuclosPngFile =  new File(TestDataHelper.getClassLoader().getResource("org/nuclos/test/webclient/documents/nuclos.png").toURI())
	static File nuclosDocxFile =  new File(TestDataHelper.getClassLoader().getResource("org/nuclos/test/webclient/documents/nuclos.docx").toURI())

	static InputStream getExampleText() {
		return TestDataHelper.getClassLoader().getResourceAsStream("org/nuclos/test/webclient/documents/exampletext.txt")
	}


	private static def deepcopy(orig) {
		def bos = new ByteArrayOutputStream()
		def oos = new ObjectOutputStream(bos)
		oos.writeObject(orig); oos.flush()
		def bin = new ByteArrayInputStream(bos.toByteArray())
		def ois = new ObjectInputStream(bin)
		return ois.readObject()
	}
	
	private static def updateData(object, variation) {
		if (variation != null) {
			object.each {
				it ->
					def variationValue = variation[it.key]
					if (variationValue instanceof String || variationValue instanceof Integer || variationValue instanceof Date || variationValue instanceof Boolean) {
						if (variationValue != null) {
							object[it.key] = variationValue
						}
					} else {
						if (variationValue != null) {
							updateData(object[it.key], variationValue)
						}
					}
			}
		}
	}

	private static def insert(template, variation, sessionId) {
		def copy = deepcopy(template)
		
		// update template data with variation data
		updateData(copy, variation)
		return RESTHelper.createBo(copy, sessionId)
	}

	static void insertTestCategories(RESTClient sessionId) {
		def categories = [
				[ "name": "man" ], [ "name": "team" ], [ "name": "jar" ], [ "name": "activity" ], [ "name": "cows" ], [ "name": "cough" ],
				[ "name": "hole" ], [ "name": "badge" ], [ "name": "channel" ], [ "name": "vegetable" ], [ "name": "look" ], [ "name": "rock" ],
				[ "name": "crate" ], [ "name": "bike" ], [ "name": "purpose" ], [ "name": "creature" ], [ "name": "stop" ], [ "name": "mountain" ],
				[ "name": "operation" ], [ "name": "scissors" ], [ "name": "limit" ], [ "name": "sticks" ], [ "name": "selection" ], [ "name": "shape" ],
				[ "name": "crib" ], [ "name": "edge" ], [ "name": "sheet" ], [ "name": "wash" ], [ "name": "guide" ], [ "name": "screw" ],
				[ "name": "board" ], [ "name": "basket" ], [ "name": "hose" ], [ "name": "minister" ], [ "name": "twist" ], [ "name": "daughter" ],
				[ "name": "crush" ], [ "name": "actor" ], [ "name": "cars" ], [ "name": "library" ], [ "name": "dinner" ], [ "name": "rain" ],
				[ "name": "wrench" ], [ "name": "science" ], [ "name": "advice" ], [ "name": "winter" ], [ "name": "zebra" ], [ "name": "quiet" ],
				[ "name": "island" ], [ "name": "hair" ], [ "name": "balls" ], [ "name": "country" ], [ "name": "weight" ], [ "name": "account" ],
				[ "name": "chess" ], [ "name": "dirt" ], [ "name": "list" ], [ "name": "temper" ], [ "name": "goat" ], [ "name": "space" ],
				[ "name": "underwear" ], [ "name": "ice" ], [ "name": "earthquake" ], [ "name": "story" ], [ "name": "lift" ],
				[ "name": "spot" ], [ "name": "house" ], [ "name": "laborer" ], [ "name": "oil" ], [ "name": "drain" ],
				[ "name": "teeth" ], [ "name": "wind" ], [ "name": "company" ], [ "name": "example" ], [ "name": "toe" ],
				[ "name": "slip" ], [ "name": "love" ], [ "name": "quilt" ], [ "name": "mass" ], [ "name": "base" ], [ "name": "slope" ],
				[ "name": "religion" ], [ "name": "hook" ], [ "name": "stream" ], [ "name": "celery" ], [ "name": "bone" ],
				[ "name": "cracker" ], [ "name": "porter" ], [ "name": "mine" ], [ "name": "fruit" ], [ "name": "ornament" ],
				[ "name": "belief" ], [ "name": "eggnog" ], [ "name": "patch" ], [ "name": "jellyfish" ], [ "name": "toothpaste" ],
				["name": "pear" ], [ "name": "stew" ], [ "name": "error" ], [ "name": "condition" ], [ "name": "growth" ], [ "name": "hammer" ],
				[ "name": "rub" ], [ "name": "balloon" ], [ "name": "flower" ], [ "name": "muscle" ], [ "name": "brain" ], [ "name": "bird" ],
				[ "name": "road" ], [ "name": "payment" ], [ "name": "veil" ], [ "name": "drug" ], [ "name": "middle" ], [ "name": "monkey" ],
				[ "name": "rest" ], [ "name": "flock" ], [ "name": "tin" ], [ "name": "summer" ], [ "name": "wilderness" ],
				[ "name": "chalk" ], [ "name": "recess" ], [ "name": "cable" ], [ "name": "spade" ], [ "name": "bite" ], [ "name": "tongue" ],
				[ "name": "earth" ], [ "name": "art" ], [ "name": "poison" ], [ "name": "cook" ], [ "name": "stem" ], [ "name": "beginner" ],
				[ "name": "land" ], [ "name": "join" ], [ "name": "design" ], [ "name": "snail" ], [ "name": "book" ], [ "name": "beggar" ],
				[ "name": "cart" ], [ "name": "cushion" ], [ "name": "lead" ], [ "name": "coat" ], [ "name": "business" ], [ "name": "room" ],
				[ "name": "cow" ], [ "name": "name" ], [ "name": "twig" ], [ "name": "time" ], [ "name": "bun" ], [ "name": "alarm" ],
				[ "name": "spark" ], [ "name": "star" ], [ "name": "tank" ], [ "name": "zinc" ], [ "name": "crayon" ], [ "name": "range" ],
				[ "name": "knot" ], [ "name": "cheese" ], [ "name": "holiday" ], [ "name": "connection" ], [ "name": "pail" ],
				[ "name": "locket" ], [ "name": "clam" ], [ "name": "shelf" ], [ "name": "spoon" ], [ "name": "nation" ],
				[ "name": "beast" ], [ "name": "dust" ], [ "name": "glass" ], [ "name": "bucket" ], [ "name": "mom" ],
				[ "name": "string" ], [ "name": "tiger" ], [ "name": "rhythm" ], [ "name": "plot" ], [ "name": "passenger" ],
				[ "name": "drawer" ], [ "name": "cloth" ], [ "name": "soup" ], [ "name": "pump" ], [ "name": "ink" ], [ "name": "act" ],
				[ "name": "ocean" ], [ "name": "carpenter" ], [ "name": "polish" ], [ "name": "pancake" ]
		]

		def category =
				[
						boMetaId: 'example_rest_Category',
						attributes: [
								'name': ''
						]
				]
		categories.forEach(
			{ LinkedHashMap<String, String> cat -> insert(category, [attributes: cat], sessionId) }
		)
	}

	/**
	 * TODO: Javadoc
	 */
	static void insertTestData(RESTClient sessionId) {
		def customer =
		[
			boMetaId: 'example_rest_Customer',
			attributes: [
				name: 'Test-Customer',
				customerNumber: 2001,
				discount: 12.5,
				active: true
			],
			'subBos': [
					insert: [
							example_rest_CustomerAddress_customer: [
									[
											_flag     : 'insert',
											boId      : null,
											attributes:
													[
															street : 'Test 1',
															city   : 'Test 1',
															zipCode: '12345',
															default: true
													]
									],
									[
											_flag     : 'insert',
											boId      : null,
											attributes:
													[
															street : 'Test 2',
															city   : 'Test 2',
															zipCode: '54321'
													]
									],
							]
					]
			]
		]
		customer = insert(customer, null, sessionId)

		def customer2 =
		[
			boMetaId: 'example_rest_Customer',
			attributes: [
				name: 'Test-Customer 2',
				customerNumber: 22222,
				discount: 5,
				active: true
			],
			'subBos': [
					insert: [
							example_rest_CustomerAddress_customer: [
									[
											_flag     : 'insert',
											boId      : null,
											attributes:
													[
															street : 'Street 4',
															city   : 'City 4',
															zipCode: '44444',
															default: true
													]
									]
							]
					]
			]
		]
		insert(customer2, null, sessionId)

		def inactiveCustomer =
		[
			boMetaId: 'example_rest_Customer',
			attributes: [
				name: 'Test-Customer inactive',
				customerNumber: 9001,
				discount: 10.0,
				active: false
			],
			'subBos': [
					insert: [
							example_rest_CustomerAddress_customer: [
									[
											_flag     : 'insert',
											boId      : null,
											attributes:
													[
															street : 'Test 3',
															city   : 'Test 3',
															zipCode: '33333'
													]
									],
							]
					]
			]
		]
		inactiveCustomer = insert(inactiveCustomer, null, sessionId)
		

		def category =
		[
			boMetaId: 'example_rest_Category',
			attributes: [
				'name': ''
			]
		]
		def categoryHardware = insert(category, [attributes: ['name': 'Hardware']], sessionId)
		def categorySoftware = insert(category, [attributes: ['name': 'Software']], sessionId)
		def categoryNuclos = insert(category, [attributes: ['name': 'Nuclos']], sessionId)
		
		
		def article =
		[
			boMetaId: 'example_rest_Article',
			attributes: [
				'articleNumber': 1001,
				'name': 'Nuclos',
				'category': ['id': null],
				'price': '0',
				'active': true,
			]
		]
		
		def articleNuclos = insert(article, [attributes: [articleNumber: 1001, name: 'Nuclos', price: '0', category: [id: categorySoftware.boId] ]], sessionId)
		def articleTextProcessor = insert(article, [attributes: [articleNumber: 1002, name: 'Text processor', price: '200', category: [id: categorySoftware.boId] ]], sessionId)
		def articleNotebookPro = insert(article, [attributes: [articleNumber: 1003, name: 'Notebook Pro', price: '3000', category: [id: categoryHardware.boId] ]], sessionId)
		def articleMouse = insert(article, [attributes: [articleNumber: 1004, name: 'Mouse', price: '30', category: [id: categoryHardware.boId] ]], sessionId)
		def articleInactive = insert(article, [attributes: [articleNumber: 1005, name: 'Mouse', price: '30', 'active': false, category: [id: categoryHardware.boId] ]], sessionId)

		
		def order =
		[
			boMetaId: 'example_rest_Order',
			attributes: [
				'orderNumber': 100,
				'customer': [id: customer.boId],
				'customernoclose': [id: customer.boId],
				'orderDate': new Date(),// TODO
				'note': '',
				'referencewithoutpopup': [id: customer.boId],
			],
			'subBos': [
				insert: [
					example_rest_OrderPosition_order: []
				]
			]
		]
		def subBo = [
			_flag: 'insert',
			boId: null,
			boMetaId: 'example_rest_OrderPosition',
			attributes: [
				article: [id: 0],
				quantity: 1,
				price:''
			]
		]
		
		
		for (int year = 2014; year <= 2015; year++) {
			for (int month = 0; month < 12; month += 4) {
				def orderCopy = deepcopy(order)
				Calendar cal = Calendar.instance
				cal.set(Calendar.YEAR, year)
				cal.set(Calendar.MONTH, month)
				cal.set(Calendar.DATE, 1)
				cal.set(Calendar.HOUR_OF_DAY, 12)
				updateData(orderCopy, [attributes: [
					orderNumber: '100' + year + month, 
					orderDate: cal.getTime(),
					note: (month < 7 ? 'B' : 'A')
					]
				])
				
				def quantity = 0
				def
				subBoTemp = deepcopy(subBo); quantity = year%2000; updateData(subBoTemp, [attributes: [article: [id: articleNuclos.boId], quantity: '' + quantity, price: ''+(quantity * articleNuclos.attributes.price)]]); orderCopy.subBos.insert.example_rest_OrderPosition_order.add(subBoTemp)
				subBoTemp = deepcopy(subBo); quantity = year%2000+10000; updateData(subBoTemp, [attributes: [article: [id: articleTextProcessor.boId], quantity: '' + quantity, price: ''+(quantity * articleTextProcessor.attributes.price)]]); orderCopy.subBos.insert.example_rest_OrderPosition_order.add(subBoTemp)
				subBoTemp = deepcopy(subBo); quantity = year%2000; updateData(subBoTemp, [attributes: [article: [id: articleNotebookPro.boId], quantity: '' + quantity, price: ''+(quantity * articleNotebookPro.attributes.price)]]); orderCopy.subBos.insert.example_rest_OrderPosition_order.add(subBoTemp)
				subBoTemp = deepcopy(subBo); quantity = (year%2000)*(20-month); updateData(subBoTemp, [attributes: [article: [id: articleMouse.boId], quantity: '' + quantity, price: ''+(quantity * articleMouse.attributes.price)]]); orderCopy.subBos.insert.example_rest_OrderPosition_order.add(subBoTemp)
				subBoTemp = deepcopy(subBo); quantity = (year%2000)*(20-month); updateData(subBoTemp, [attributes: [article: [id: articleInactive.boId], quantity: '' + quantity, price: ''+(quantity * articleMouse.attributes.price)]]); orderCopy.subBos.insert.example_rest_OrderPosition_order.add(subBoTemp)
				insert(orderCopy, null, sessionId)
			}
		}
	}

	static void insertTextModules(RESTClient client) {
		def textModule1 = [
			boMetaId: 'nuclet_test_other_Textmodule',
			attributes:
			[
					name          : 'Name 1',
					text          : 'Text 1',
					order         : 1
			]
		]
		insert(textModule1, null, client)

		def textModule2 = [
				boMetaId: 'nuclet_test_other_Textmodule',
				attributes:
						[
								name          : 'Name 2',
								text          : 'Text 2',
								order         : 3
						]
		]
		insert(textModule2, null, client)

		def textModule3 = [
				boMetaId: 'nuclet_test_other_Textmodule',
				attributes:
						[
								name          : 'Name 3',
								text          : 'Text 3',
								order         : 2
						]
		]
		insert(textModule3, null, client)
	}

	static String catName = 'Essen'
	static String catReName = 'Lebensmittel'
	static String cat2Name = 'Werkzeug'
	static String cat3Name = 'Flugzeuge Schiffe'
	static String cat4Name = '5-Tonnen-Maschine'
	static String cat5Name = '6-T-Maschine'
	static String cat6Name = 'Maschinen'

	static Map cat1 = [name: catName]
	static Map cat2 = [name: cat2Name]
	static Map cat3 = [name: cat3Name]
	static Map cat4 = [name: cat4Name]
	static Map cat5 = [name: cat5Name]
	static Map cat6 = [name: cat6Name]

	static Map word1 = [text: 'Fleisch', times: '1', agent: 'test', kategorie: cat1]
	static Map word2 = [text: 'Kartoffel', times: '3', agent: 'test', kategorie: cat1]
	static Map word3 = [text: catName, times: '7', agent: 'test', kategorie: cat1]

	static Map word4 = [text: 'Hammer', times: '4', agent: 'nuclos', kategorie: cat2]
	static Map word5 = [text: 'Schraubenzieher', times: '10', agent: 'nuclos', kategorie: cat2]

	static Map word6 = [text: 'Zange', times: '-1', agent: 'test', kategorie: cat2]

	static Map auftrag = [name: '1', bemerkung: 'remark']

	static void createLuceneTestData(SuperuserRESTClient nuclosSession) {
		cat1['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_CATEGORY.fqn,
				attributes: cat1
		], nuclosSession).boId

		cat2['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_CATEGORY.fqn,
				attributes: cat2
		], nuclosSession).boId

		cat3['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_CATEGORY.fqn,
				attributes: cat3
		], nuclosSession).boId

		cat4['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_CATEGORY.fqn,
				attributes: cat4
		], nuclosSession).boId

		cat5['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_CATEGORY.fqn,
				attributes: cat5
		], nuclosSession).boId

		cat6['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_CATEGORY.fqn,
				attributes: cat6
		], nuclosSession).boId

		word1['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_WORD.fqn,
				attributes: word1
		], nuclosSession).boId

		word2['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_WORD.fqn,
				attributes: word2
		], nuclosSession).boId

		word3['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_WORD.fqn,
				attributes: word3
		], nuclosSession).boId

		word4['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_WORD.fqn,
				attributes: word4
		], nuclosSession).boId

		word5['id'] = RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_WORD.fqn,
				attributes: word5
		], nuclosSession).boId

		auftrag['id'] = RESTHelper.createBo([
				boMetaId: TestEntities.EXAMPLE_REST_AUFTRAG.fqn,
				attributes: auftrag
		], nuclosSession).boId
	}

	static void insertNuclosProcessTestData(RESTClient sessionId) {
		def orders = [
				[
						boMetaId  : 'example_rest_Order',
						attributes: [
								orderNumber : 1,
								note: 'order'
						]
				],
				[
						boMetaId  : 'example_rest_Order',
						attributes: [
								orderNumber: 2,
								note: 'Priority order',
								nuclosProcess: [id: 'example_rest_Order_Priorityorder', name: 'Priority order']
						]
				],
				[
						boMetaId  : 'example_rest_Order',
						attributes: [
								orderNumber: 3,
								note: 'Standing order',
								nuclosProcess: [id: 'example_rest_Order_Standingorder', name: 'Standing order']
						]
				],
				[
						boMetaId  : 'example_rest_Order',
						attributes: [
								orderNumber: 4,
								note: 'Standard order',
								nuclosProcess: [id: 'example_rest_Order_Standardorder', name: 'Standard order']
						]
				]
			]

		orders.forEach({ order -> insert(order, null, sessionId) })
	}
}
