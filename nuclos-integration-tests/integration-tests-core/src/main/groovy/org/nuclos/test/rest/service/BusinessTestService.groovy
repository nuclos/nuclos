package org.nuclos.test.rest.service

import org.json.JSONArray
import org.json.JSONObject
import org.nuclos.common.UID
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.SuperuserRESTClient
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class BusinessTestService {
	private final SuperuserRESTClient client

	BusinessTestService(final SuperuserRESTClient client) {
		this.client = client
	}

	JSONArray getAllTests() {
		RESTHelper.requestJsonArray('/businesstests', HttpMethod.GET, client.sessionId)
	}

	JSONObject getStatus() {
		RESTHelper.requestJson('/businesstests/status', HttpMethod.GET, client.sessionId)
	}

	String getStatusSimple() {
		RESTHelper.requestString('/businesstests/status/simple', HttpMethod.GET, client.sessionId)
	}

	JSONArray runAllTests() {
		RESTHelper.requestJsonArray('/businesstests/run', HttpMethod.POST, client.sessionId)
	}

	void runTest(final UID uid) {
		RESTHelper.requestJson('/businesstests/run/' + uid, HttpMethod.POST, client.sessionId)
	}
}
