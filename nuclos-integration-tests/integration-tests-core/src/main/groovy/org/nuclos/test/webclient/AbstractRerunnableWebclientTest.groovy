package org.nuclos.test.webclient


import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest

import groovy.transform.CompileStatic

/**
 * Class to guarantee a clean test environment before EVERY single test(-method) will be executed.
 * (Truncate-tables, new Browser instance, etc.... @Test void method1{}... shutdown)
 * (Truncate-tables, new Browser instance, etc.... @Test void method2{}... shutdown)
 *
 * This ensures a smooth 'Rerun' from the Maven JUnit plugin
 * https://maven.apache.org/surefire/maven-surefire-plugin/examples/rerun-failing-tests.html
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
abstract class AbstractRerunnableWebclientTest extends AbstractWebclientTest {

	@BeforeClass
	@Override
	static void setup() {
		Runtime.addShutdownHook {
			shutdown()
		}
	}

	@AfterClass
	@Override
	static void teardown() {
		// do nothing here
	}

	@Before
	void beforeEach() {
		AbstractWebclientTest.setup(true, false)
	}

	@After
	void afterEach() {
		AbstractWebclientTest.teardown(true)
	}

}
