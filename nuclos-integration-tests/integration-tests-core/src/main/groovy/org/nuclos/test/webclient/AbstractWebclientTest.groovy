package org.nuclos.test.webclient

import static org.nuclos.test.NuclosTestContext.initFromSystemProperty

import java.awt.Toolkit
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.StringSelection
import java.nio.file.Files
import java.nio.file.Paths
import java.util.stream.Collectors

import org.apache.commons.lang3.mutable.Mutable
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.experimental.categories.Category
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.NuclosTestContext
import org.nuclos.test.log.Log
import org.nuclos.test.rest.request.RequestCounts
import org.nuclos.test.webclient.pageobjects.AuthenticationComponent
import org.nuclos.test.webclient.pageobjects.DataLanguageModal
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.MessageModal
import org.nuclos.test.webclient.util.Screenshot
import org.nuclos.test.webclient.util.ValueFormatter
import org.openqa.selenium.*
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.logging.LogEntries
import org.openqa.selenium.logging.LogEntry
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.WebDriverWait
import org.testcontainers.containers.BrowserWebDriverContainer
import org.testcontainers.lifecycle.TestDescription

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.Har
import net.lightbody.bmp.core.har.HarEntry
import net.lightbody.bmp.proxy.CaptureType

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
abstract class AbstractWebclientTest extends AbstractNuclosTest {

	static RemoteWebDriver driver

	static WebDriverWait waitDriver
	static WebDriverWait waitDriverForScriptReady

	static List<String> localStorageKeys
	static String localStorageExec = 'return Object.keys(localStorage).filter((k) => k !== "selectEntryInOtherWindow" && k !== "preferencechange" && k !== "SidebarLayoutType");'

	/**
	 * If a Docker container is started for this test, we should stop it again after each test class.
	 * It would also be automatically stopped when the JVM shuts down, but there could be lots of
	 * unnecessarily running containers in the meantime, if we run a lot of tests.
	 */
	static BrowserWebDriverContainer browserContainer

	/**
	 * Cause everything is static in this world, we need a way to tell browserContainer
	 * when test ended via @Rule Watcher. So it can retain test videos on failure.
	 */
	@Rule
	public TestWatcher testWatcher = new TestWatcher() {

		private TestDescription toTestDescription(Description description) {
			return new TestDescription() {
				String getTestId() {
					return description.getDisplayName()
				}
				String getFilesystemFriendlyName() {
					return description.getClassName() + "-" + description.getMethodName()
				}
			};
		}

		protected void failed(Throwable e, Description description) {
			browserContainer?.afterTest(toTestDescription(description), Optional.of(e))
		};

		protected void succeeded(Description description) {
			browserContainer?.afterTest(toTestDescription(description), Optional.empty())
		};
	}

	static int getDefaultWaitDriverTimeout() {
		return Integer.valueOf(initFromSystemProperty('waitDriver.timeOutInSeconds', '5'))
	}

	@BeforeClass
	static void setup() {
		setup(true, true)
	}

	/**
	 * @param callbackBeforeWebclientStart
	 * 	Will be called after "truncate tables" and before the Webclient ist started.
	 * 	Can be used to prepare test data (via REST) that must be available for the initial Webclient startup.
	 */
	static void setup(boolean withTestUserLogin, boolean registerShutdownHook, Closure<?> callbackBeforeWebclientStart = null) {
		Log.debug 'Setup'

		if (registerShutdownHook) {
			Runtime.addShutdownHook {
				shutdown()
			}
		}

		final Mutable<Throwable> testSetupThrowable = new Mutable<Throwable>() {
			Throwable t;
			@Override
			Throwable getValue() {
				return t
			}
			@Override
			void setValue(final Throwable value) {
				this.t = value
			}
		}
		final Thread testSetupThread = new Thread("AbstractNuclosTest.setup()") {
			@Override
			void run() {
				try {
					AbstractNuclosTest.setup()
				} catch (Throwable t) {
					testSetupThrowable.setValue(t)
				}
			}
		}
		testSetupThread.start()

		try {
			driver = WebclientTestContext.instance.initDriver()
			waitDriver = new WebDriverWait(driver, getDefaultWaitDriverTimeout())
			// for long-running server transactions:
			waitDriverForScriptReady = new WebDriverWait(driver, Long.valueOf(initFromSystemProperty('waitDriverForScriptReady.timeOutInSeconds', '300')))

			resizeBrowserWindow()

			testSetupThread.join()
			if (testSetupThrowable.getValue() != null) {
				throw testSetupThrowable.getValue()
			}
			if (callbackBeforeWebclientStart) {
				callbackBeforeWebclientStart()
			}

			openStartpage()
			waitForAngularRequestsToFinish()

			// clear localStorage - needed for phantomjs
//			executeScript('localStorage.clear();')

			nuclosSession.managementConsole('resetCompleteMandatorSetup')

			if (withTestUserLogin) {
				login('test', 'test')
			}

			localStorageKeys = executeScript(localStorageExec) as List<String>

		}

		catch (Throwable ex) {
			fail(ex.message, ex)
		}

		Log.debug 'Setup done'
	}

	@AfterClass
	static void teardown() {
		// It is to late to check for page error and throw an AssertionError
		teardown(false)
	}

	static void teardown(boolean bCheckForPageError) {
		try {
			if (bCheckForPageError) {
				checkForPageError()
			}

			// nyc coverage analysis
			String coverage = executeScript("return JSON.stringify(window.__coverage__);")
			if (!WebclientTestContext.instance.skipCoverage()) {
				Files.createDirectories(Paths.get("target/coverage-output"))
				def myFile = new File('target/coverage-output/coverage_' + System.currentTimeMillis())
				myFile.write(coverage)
			}

			checkLocalStorageOverflow()
		} finally {
			shutdown()
		}
	}

	static void checkLocalStorageOverflow() {
		def currentStorageKeys = executeScript(localStorageExec) as List<String>

		if (currentStorageKeys.size() != localStorageKeys.size() && containsAnyNonPermittedTemporaryKeys(currentStorageKeys)) {
			// print diff as error
			def newlyAddedDuringTest = currentStorageKeys.stream().filter(
					(entry) -> !localStorageKeys.contains(entry) && 'SUBFORM_CLIPBOARD' != entry
			).collect(Collectors.toList()).join(", ")
			throw new AssertionError((Object)"There are uncleaned localStorage keys: $newlyAddedDuringTest")
		}
	}

	static boolean containsAnyNonPermittedTemporaryKeys(List<String> currentStorageKeys) {
		return currentStorageKeys.stream().filter(
				(entry) -> !localStorageKeys.contains(entry) && 'SUBFORM_CLIPBOARD' != entry
		).any()
	}

	static void checkForPageError() {
		// There should be no unhandled script errors
		def error = pageError
		if (error) {
			printBrowserDebugInfos()
			throw new AssertionError((Object)"Found unhandled error: $error")
		}
	}

	/**
	 * Extract possible (JavaScript) errors that occured on the page.
	 */
	static String getPageError() {
		if (driver) {
			try {
				$('body').getAttribute('error')
			} catch (Exception e) {
				Log.warn("Page error not accessible: " + e.getMessage())
			}
		}
	}

	static NuclosWebElement $xXx(String selector) {
		Log.info("Wait for clickability: \"$selector\"")
		waitForAngularRequestsToFinish()
		return new NuclosWebElement(waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector(selector))), selector)
	}

	static NuclosWebElement $xXx(NuclosWebElement element) {
		Log.info("Wait for clickability: \"$element.debugSelector\"")
		waitForAngularRequestsToFinish()
		return new NuclosWebElement(waitDriver.until(ExpectedConditions.elementToBeClickable(element.element)), element.debugSelector)
	}

	static NuclosWebElement $vVv(String selector) {
		Log.info("Wait for steadiness: \"$selector\"")
		waitForAngularRequestsToFinish()
		WebElement element = waitDriver.until(waitForSteadiness(null, selector))
		element == null ? null : new NuclosWebElement(element, selector)
	}

	static NuclosWebElement $zZz(String selector) {
		Log.info("Wait for visibility: \"$selector\"")
		waitForAngularRequestsToFinish()
		List<WebElement> elements = waitDriver.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(selector)))
		elements.empty ? null : new NuclosWebElement(elements.get(0), selector)
	}

	private static String debugSelector(WebElement e, String selector) {
		final String result
		if (e != null) {
			if (e instanceof NuclosWebElement) {
				result = ((NuclosWebElement)e).debugSelector + ' -> ' + selector
			} else {
				result = e.toString() + ' -> ' + selector
			}
		} else {
			result = selector
		}
		return result
	}

	static NuclosWebElement $vVv(WebElement e, String selector) {
		WebElement element = waitDriver.until(waitForSteadiness(e, selector))
		element == null ? null : new NuclosWebElement(element, selector)
	}

	static NuclosWebElement $zZz(WebElement e, String selector) {
		List<WebElement> elements = waitDriver.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(e, By.cssSelector(selector)))
		elements.empty ? null : new NuclosWebElement(elements.get(0), debugSelector(e, selector))
	}

	static NuclosWebElement $(String selector) {
		Log.info("Selecting: \"$selector\"")
		waitForAngularRequestsToFinish()
		return $(null, selector)
	}

	static NuclosWebElement $(WebElement e, String selector) {
		int attempts = 0;
		while( attempts < 5) {
			try {
				WebElement element
				By cssLocator = By.cssSelector(selector)
				if (e == null ) {
					element = driver?.findElement(cssLocator)
				} else {
					element = e.findElement(cssLocator)
				}
				if (element == null) {
					return null
				}
				return new NuclosWebElement(element, debugSelector(e, selector))
			} catch (StaleElementReferenceException ignored) {
				sleep(100)
				waitForAngularRequestsToFinish()
			} catch (WebDriverException ignored) {
				return null
			}
			attempts++;
		}

		return null
	}

	static List<NuclosWebElement> $$xXx(String selector) {
		Log.info("Wait for clickability: \"$selector\"")
		waitForAngularRequestsToFinish()
		driver.findElements(By.cssSelector(selector)).stream()
				.map(it -> waitDriver.until(ExpectedConditions.elementToBeClickable(it)))
				.map(it -> new NuclosWebElement(it, selector))
				.collect(Collectors.toList())
	}

	static List<NuclosWebElement> $$zZz(String selector) {
		Log.info("Wait for visibility: \"$selector\"")
		waitForAngularRequestsToFinish()
		waitDriver.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(selector))).collect { new NuclosWebElement(it, selector) }
	}

	static List<NuclosWebElement> $$zZz(WebElement e, String selector) {
		waitDriver.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(e, By.cssSelector(selector))).collect { new NuclosWebElement(it, selector) }
	}

	static List<NuclosWebElement> $$(String selector) {
		Log.info("Selecting: \"$selector\"")
		waitForAngularRequestsToFinish()
		driver.findElements(By.cssSelector(selector)).collect { new NuclosWebElement(it, selector) }
	}

	static List<NuclosWebElement> $$(WebElement e, String selector) {
		e.findElements(By.cssSelector(selector)).collect { new NuclosWebElement(it, selector) }
	}

	static void openStartpage() {
		Log.info("Open startpage: \"$WebclientTestContext.instance.nuclosWebclientBaseURL\"")
		getUrl(WebclientTestContext.instance.nuclosWebclientBaseURL)
	}

	/**
	 * Counts all browser requests.
	 * Does not count direct REST requests via RESTClient / RESTHelper.
	 */
	static RequestCounts countBrowserRequests(Closure c) {
		waitForAngularRequestsToFinish()
		WebclientTestContext.instance.browserProxy.countRequests(c)
	}

	/**
	 * Gets the HAR using default capture settings.
	 * Does not capture response content.
	 * Does not capture direct REST request via RESTClient / RESTHelper.
	 */
	static Har getBrowserHar(Closure c) {
		WebclientTestContext.instance.browserProxy.getHar([], c)
	}

	/**
	 * Gets the HAR using the given capture settings.
	 * Here you can capture resopnse content via {@link net.lightbody.bmp.proxy.CaptureType#RESPONSE_CONTENT}
	 */
	static Har getBrowserHar(List<CaptureType> captureTypes, Closure c) {
		WebclientTestContext.instance.browserProxy.getHar(captureTypes, c)
	}

	static void logBrowserRequestsWithClosure(Closure c, logBrowserDebug = false) {
		Throwable safedException = null
		getBrowserHar([CaptureType.REQUEST_HEADERS, CaptureType.RESPONSE_HEADERS, CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT], {
			try {
				c()
			} catch (Throwable t) {
				safedException = t
			}
		}).with {
			it.log.entries.forEach({ HarEntry entry ->
				Log.error "[" + entry.request.method + "] -> " + entry.request.url
				Log.error "[STATUS] -> " + entry.response.status.toString()
				Log.error "[REQUEST] -> " + entry.request.postData?.text
				Log.error "[RESPONSE] -> " + entry.response.content.text
			})
			if (logBrowserDebug) {
				printBrowserDebugInfos()
			}
		}

		if (safedException != null) {
			throw safedException
		}
	}

	static boolean login(String username = 'nuclos', String password = '', boolean autologin = false) {
		return login(new LoginParams(
				username: username,
				password: password,
				autologin: autologin
		))
	}

	/**
	 * Performs a Login with the given credentials and the current locale, if not logged in already.
	 */
	static boolean login(LoginParams params) {
        waitForAngularRequestsToFinish()
		if (isLoggedIn(params.username)) {
			return false
		}

		loginUnsafe(params)

		waitForNotNull(60, {return $('#user-menu')}, true, WebDriverException.class)
		return true
	}

	/**
	 * Performs a Login with the given credentials and the current locale.
	 * Without any checks for login success etc.
	 */
	static void loginUnsafe(LoginParams params) {
		// sometimes browser window is to small (800x600) and therefore the locale chooser is not visible
		resizeBrowserWindow()

		if (params.locale) {
			LocaleChooser.locale = params.locale
		} else {
			LocaleChooser.locale = AbstractNuclosTest.context.locale
		}

		AuthenticationComponent.setLogin(params)
		AuthenticationComponent.submit()

		if (params.mandator) {
			if (params.mandator == 'null') {
				// superuser login without any mandator selection
				$('#mandatorless-session').click()
			} else {
				WebElement chooseMandator = $('#choose-mandator')
				Select droplist = new Select(chooseMandator)
				droplist.selectByVisibleText(params.mandator)
				$('#choose-mandator-button').click()
			}
		}
	}

	/**
	 * Does not fail the build, if "waitForAngular" did not succeed.
	 */
	static void tryToWaitForAngular(int timeout) {
		Log.debug "Try to wait for Angular requests to finish"
		Closure waitClosure = {
			waitForAllReadyChecks(false, false)
			return true
		}
		if (!doWaitFor(timeout, waitClosure)) {
			// only 'tryToWait'... should be a debug message not a warning:
			Log.debug("Failed to wait for angular requests to finish")
		}
	}

	static void dragElementByOffset(NuclosWebElement element, int x, int y) {
		new Actions(driver)
				.clickAndHold(element.element)
				.moveToElement(element.element, x, y)
				.release()
				.build()
				.perform()
	}

	static void waitForAngularRequestsToFinish() {
		waitForAngularRequestsToFinish(NuclosTestContext.instance.DEFAULT_TIMEOUT)
	}

	static void waitForAngularRequestsToFinish(int timeoutInSeconds) {
		Log.debug "Waiting for Angular requests to finish (Timeout=$timeoutInSeconds)"
		waitFor(timeoutInSeconds) {
			waitForAllReadyChecks(timeoutInSeconds > 1)
			return true
		}
	}

	private static final String waitForJsReadyScript = 'return document.readyState === \'complete\''
	private static final String waitForJQueryScript = 'return window.jQuery != undefined && jQuery.active === 0'
	private static final String waitForAngularReadyScript = 'return typeof window.getAllAngularTestabilities === "function" && window.getAllAngularTestabilities().findIndex(x=>!x.isStable()) === -1'

	private static void waitForAllReadyChecks(boolean bUseWaitDriver, boolean bThrowFailures = true) {
		if (isWindowClosed()) {
			return
		}

		waitForReadyScript(waitForJsReadyScript, bUseWaitDriver, bThrowFailures)
		waitForReadyScript(waitForJQueryScript, bUseWaitDriver, bThrowFailures)
		waitForReadyScript(waitForAngularReadyScript, bUseWaitDriver, bThrowFailures)
		// check JsReady after angular again
		waitForReadyScript(waitForJsReadyScript, bUseWaitDriver, bThrowFailures)
	}

	private static void waitForJsReady() {
		waitForReadyScript(waitForJsReadyScript, true, true)
	}

	private static void waitForReadyScript(String waitForScript, boolean bUseWaitDriver = true, boolean bThrowFailures = true) {
		try {
			boolean scriptReady
			while (!scriptReady) {
				if (bUseWaitDriver) {
					scriptReady = waitDriverForScriptReady.until({
						driver -> (boolean)((JavascriptExecutor) driver).executeScript(waitForScript)
					})
				} else {
					while (!scriptReady) {
						scriptReady = executeScript(waitForScript)
					}
				}
			}
		} catch (e) {
			if (bThrowFailures) {
				fail("Failed to wait for script '$waitForScript': $e.message", e)
			} else {
				Log.debug "Failed to wait for script '$waitForScript': $e.message"
			}
		}
	}

	private static ExpectedCondition<WebElement> waitForSteadiness(final WebElement parent, final String selector) {
		return new SteadinessCondition(parent, selector)
	}

	static String getCurrentUrl() {
		try {
			return driver.getCurrentUrl()
		}
		catch (Throwable ex) {
			Log.error 'Could not get current URL', ex
		}

		return null
	}

	static String getTitle() {
		return driver.title
	}

	/**
	 * Takes a screenshot and saves it under the given name,
	 * if screenshots are enabled or "force" is true.
	 */
	static void screenshot(String name, boolean force = false) {
		if (!WebclientTestContext.instance.takeScreenshots && !force) {
			Log.debug("Skipping screenshot '$name'")
			return
		}
		Screenshot.take(name)
	}

	static boolean logout() {
		if (!isPresent('#logout')) {
			return false
		}

		logoutUnsafe()
        assertLoggedOut()

		return true
	}

	static void logoutUnsafe(withWaitForAngular = true) {
		if (withWaitForAngular) {
			waitForAngularRequestsToFinish()
		}

		$('#user-menu .dropdown').click()
		$('#logout').click()

		if (withWaitForAngular) {
			waitForAngularRequestsToFinish()
		}
	}

	static boolean isPresent(NuclosWebElement parent, String selector) {
		try {
			def element = null
			if (parent != null) {
				element = parent.$(selector)
			} else {
				element = $(selector)
			}
			return !!element
		}
		catch (Exception ignore) {
			return false
		}
	}

	static boolean isPresent(String selector) {
		isPresent(null, selector)
	}

	/**
	 * Reloads the current page in the browser.
	 */
	static void refresh() {
		Log.info "Refreshing: $currentUrl"

		// getDriver().navigate().refresh() does not always work with PhantomJS
		if (WebclientTestContext.instance.getBrowser() != Browser.PHANTOMJS) {
			driver.navigate().refresh()
		} else {
			getUrl(currentUrl)
		}

		// give the browser some time to load the page before waitingForAngular etc.
		sleep(500)
		waitForAngularRequestsToFinish()
	}

	static void getUrl(String url, boolean bWaitForPageLoad = true) {
		Log.debug "Get (waitForPageLoad=$bWaitForPageLoad): $url"
		driver.get(url)
		if (bWaitForPageLoad) {
			waitForJsReady()
		} else {
			// Give the browser and app some time to start loading/redirecting
			sleep(1000)
		}
	}

	/**
	 * @param hash The URL hash without leading '#'
	 */
	static void getUrlHash(String hash) {
        waitForAngularRequestsToFinish()
		Log.debug "Get hash: $hash"
		try {
			driver.navigate().to(currentUrlHost + '#' + hash)
			// executeScript("window.location.hash='$hash'")
			waitForAngularRequestsToFinish()
		}
		catch (Exception ex) {
			// try again one time
			try {
				driver.navigate().to(currentUrlHost + '#' + hash)
				// executeScript("window.location.hash='$hash'")
				waitForAngularRequestsToFinish()
			}
			catch (Exception ex2) {
				Log.warn "Could not set URL hash $hash; first attempt: ", ex2
				Log.warn "Could not set URL hash $hash; second attempt: ", ex2
			}
		}
	}

	static String getCurrentUrlHost() {
		String url = driver.getCurrentUrl()
		String host = url.substring(0, url.findIndexOf { c -> c == '#' })
		return host
	}

	static boolean isLoggedIn(String username) {
		return $('#user-menu')?.$('.nav-link')?.text == username
	}

    static void assertLoggedIn(String username) {
	    // // This may help against "Expected condition failed: waiting for visibility of all elements located by By.cssSelector: #user-menu (tried for 30 second(s)"
	    waitForNotNull(90, {
		    return $zZz('#user-menu').$('.nav-link').text == username
	    }, true, WebDriverException.class, NullPointerException.class)
    }

    static void assertLoggedOut() {
        assert $zZz('#login-form')
        assert currentUrl.contains('login')
    }

	static void keydownShortCut(boolean altKeyPressed, boolean shiftKeyPressed, boolean ctrlKeyPressed, CharSequence... keys) {
		def keyhitLogline = 'Hitting '

		def action = new Actions(driver)

		if (altKeyPressed) {
			action = action.keyDown(Keys.ALT)
			keyhitLogline += ' ALT '
		}

		if (shiftKeyPressed) {
			action = action.keyDown(Keys.SHIFT)
			keyhitLogline += ' SHIFT '
		}

		if (ctrlKeyPressed) {
			action = action.keyDown(Keys.CONTROL)
			keyhitLogline += ' CONTROL '
		}

		action.sendKeys(keys).pause(500)
		keyhitLogline += keys.toString()

		if (altKeyPressed) {
			action = action.keyUp(Keys.ALT)
		}

		if (shiftKeyPressed) {
			action = action.keyUp(Keys.SHIFT)
		}

		if (ctrlKeyPressed) {
			action = action.keyUp(Keys.CONTROL)
		}

		Log.debug keyhitLogline
		action
				.build()
				.perform()
	}

	static void assertNotFound() {
		assertError('404')
	}

	static void assertAccessDenied() {
		assert driver.currentUrl.contains('/error/403')
		assertError('403')
	}

	static void assertError(String s = '') {
		String error = errorMessage

		assert error
		assert !error.empty
		assert error.contains(s)
	}

	static String getErrorMessage() {
		return $('nuc-error .card-header')?.text
	}

	/**
	 * Returns a currently displayed modal message dialog.
	 */
	static MessageModal getMessageModal(String selector = 'nuc-dialog', boolean allowCurlyBraceContent = false) {
		waitForAngularRequestsToFinish()
		// def element = waitForNotNull(10, {$(selector)}, false)
		def element = waitForNotNull(getDefaultWaitDriverTimeout()-1, {$vVv(selector)}, false)
		if (!element) {
			//sendKeys(Keys.ESCAPE);
			return null
		}

		NuclosWebElement topDialog = $$("${selector} > div")?.findAll { it.getCssValue('z-index') != 'auto' }?.max { it.getCssValue('z-index') }?.parent

		// fallback if there is no z-index set
		if (topDialog == null) {
			topDialog = waitForNotNull(1, {$("${selector}")})
		}
		String title = waitForNotNull(1, {topDialog?.$('.modal-title')?.text?.trim()}, false)
		String message = waitForNotNull(1, {topDialog?.$('.modal-body')?.text?.trim()}, false)

		// we had trouble reading dialog so discard it
		if (selector == "nuc-dialog" && (title == null || message == null)) {
			return null
		}

		if (!allowCurlyBraceContent) {
			assert !(title ==~ /.*\{\d*}.*/), 'Message parameters were not resolved'
			assert !(message ==~ /.*\{\d*}.*/), 'Message parameters were not resolved'
		}

		new MessageModal(
				element: topDialog,
				title: title,
				message: message
		)
	}

	static void assertNoMessageModel() {
		MessageModal messageModal = getMessageModal()

		if (messageModal) {
			// show message in test result
			assert !messageModal.message
		}

		waitFor {
			$('nuc-dialog') == null
		}
	}

	static void assertMessageModalAndConfirm(String title, String partOfMessage, Closure closureToOpenModal = null) {
        waitForAngularRequestsToFinish()
        if (closureToOpenModal != null) {
            closureToOpenModal()
        }
		MessageModal messageModal = getMessageModal()
        if (messageModal == null && closureToOpenModal != null) {
            // retry click to open
            closureToOpenModal()
            messageModal = getMessageModal()
        }
		assert messageModal

		assert messageModal.title == title

		if (partOfMessage != null) {
			assert messageModal.message.contains(partOfMessage)
		}

		messageModal.confirm()

		waitFor {
			!messageModal.element.isDisplayed()
		}
        waitForAngularRequestsToFinish()
	}

	static DataLanguageModal openDataLanguageModal(Closure closureToOpenModal = null, String selector = '.dialog') {
		if (closureToOpenModal != null) {
			closureToOpenModal()
		}
		waitForAngularRequestsToFinish()
		def element = waitForNotNull(getDefaultWaitDriverTimeout()-1, {$vVv(selector)}, false)
		if (!element) {
			return null
		}

		String title = waitForNotNull(1, {$('.modal-title')?.text?.trim() }, false)

		// we had trouble reading dialog so discard it
		if (selector == "nuc-dialog" && (title == null)) {
			return null
		}

		assert !(title ==~ /.*\{\d*}.*/), 'Message parameters were not resolved'

		new DataLanguageModal(
				element: $(selector),
				title: title,
		)
	}

	/**
	 * Returns a currently displayed modal message dialog.
	 */
	static DataLanguageModal getDataLanguageModal(String selector = '.dialog') {
		waitForAngularRequestsToFinish()
		def element = waitForNotNull(getDefaultWaitDriverTimeout()-1, {$vVv(selector)}, false)
		if (!element) {
			return null
		}

		String title = waitForNotNull(1, {$('.modal-title')?.text?.trim() }, false)

		// we had trouble reading dialog so discard it
		if (selector == "nuc-dialog" && (title == null)) {
			return null
		}

		assert !(title ==~ /.*\{\d*}.*/), 'Message parameters were not resolved'

		new DataLanguageModal(
				element: $(selector),
				title: title,
		)
	}

	/**
	 * @return The handle of the newly opened window.
	 */
	static String duplicateCurrentWindow() {
		Set<String> handles = driver.windowHandles

		String url = currentUrl
		executeScript("window.open('$url', '_blank')")

		Set<String> newHandles = driver.windowHandles
		assert newHandles.size() == handles.size() + 1

		String newWindowHandle = (newHandles - handles).first()
		driver.switchTo().window(newWindowHandle)
		getUrl(url)

		return newWindowHandle
	}

	static boolean isWindowClosed() {
		try {
			!driver.windowHandles.contains(driver.windowHandle)
		} catch (NullPointerException | NoSuchWindowException ignore) {
			return true
		}
	}

	static List<NuclosWebElement> luceneSearch(String s) {

		WebElement fullTextSearch = $('nuc-full-text-search nuc-dropdown')
		assert fullTextSearch != null

		def lov = ListOfValues.fromElement(
				"nuc-full-text-search-dropdown",
				fullTextSearch
		)

		lov.search(s)

		screenshot('afterLuceneSearch')

		return lov.getChoiceElements()
	}

	static void luceneTest(String s, int expected) {
		waitForAngularRequestsToFinish()

		try {
			// wait for double the timeout, because there is already a wait operation inside the luceneSearch method, which waits for the whole timeout
			waitFor(2 * getDefaultWaitDriverTimeout(), {
				luceneSearch(s).size() == expected
			})
		} catch (Exception ignore) {
			def size = luceneSearch(s).size()
			screenshot('beforeExceptionInLuceneTest')
			assert size == expected
		}

		screenshot('afterLuceneSearchAndWait')
	}

	static void sendKeysAndWaitForAngular(CharSequence... keys) {
		sendKeys(keys)
		waitForAngularRequestsToFinish()
	}

	static void sendNumber(Number number) {
		sendKeys(ValueFormatter.formatNumber(number))
	}

	/**
	 * Sends the keys to the active element, how a real user would do it.
	 */
	static void sendKeys(CharSequence... keys) {
		sendKeysWithDelay(100, keys)
	}

	static void sendKeysWithDelay(int delayInMiliSeconds, CharSequence... keys) {
		for (int i = 0; i < keys.length; i++) {
			new Actions(driver)
					.sendKeys(keys[i])
					.build()
					.perform()
			Thread.sleep(delayInMiliSeconds)
		}
	}

	static void sendShiftTab() {
		new Actions(driver)
				.keyDown(Keys.SHIFT)
				.sendKeys(Keys.TAB)
				.keyUp(Keys.SHIFT)
				.build()
				.perform()
	}

	static void clearInput() {
		new Actions(driver)
				.sendKeys(Keys.HOME)
				.keyDown(Keys.SHIFT)
				.sendKeys(Keys.END)
				.keyUp(Keys.SHIFT)
				.sendKeys(Keys.BACK_SPACE)
				.build()
				.perform()
	}

	/**
	 * Performs a general click at the location of the given element,
	 * like a real user would do it.
	 */
	static NuclosWebElement click(NuclosWebElement e) {
		e = $xXx(e)
		new Actions(driver)
				.click(e.element)
				.build()
				.perform()
		return e
	}

	static void clickWithKey(List<NuclosWebElement> e, Keys k) {
		new Actions(driver).keyDown(k).build().perform()
		e.forEach({NuclosWebElement it ->
			it.click()
		})
		new Actions(driver).keyUp(k).build().perform()
		waitForAngularRequestsToFinish()
	}

	static void shutdown() {
		try {
			driver?.quit()
		}
		catch (Throwable t) {
			Log.warn "Could not quit selenium driver", t
		}
		driver = null

		AbstractNuclosTest.nuclosSession = null

		try {
			WebclientTestContext.instance.browserstackLocal?.stop()
		} catch (Exception e) {
			Log.warn 'Failed to stop BrowserStack Local', e
		}

		try {
			// We often have to deal with "bad gateway" errors. This could help
			WebclientTestContext.instance.shutdownBrowserProxy()
		} catch (Exception e) {
			Log.warn 'Failed to shutdown BrowserProxy', e
		}
	}

	static void resizeBrowserWindow() {
		if (driver.manage().window().getSize() != WebclientTestContext.instance.preferredWindowSize) {
			waitUntilTrue({
				driver.manage().window().setSize(WebclientTestContext.instance.preferredWindowSize)
				return driver.manage().window().getSize() == WebclientTestContext.instance.preferredWindowSize
			})
		}
	}

	static void resizeBrowserWindow(int width, int height) {
		waitUntilTrue({
			def dimension = new Dimension(width, height)
			driver.manage().window().setPosition(new Point(0, 0))
			driver.manage().window().setSize(dimension)
			return driver.manage().window().getSize() == dimension
		})
	}

	static void printBrowserDebugInfos() {
		if (!driver) {
			Log.warn 'Could not print browser debug infos - browser not started'
			return
		}

		printBrowserLog()
		printCookies()

		Log.error "Last URL: $currentUrl"
	}

	private static void printBrowserLog() {
		try {
			LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER)
			for (LogEntry entry : logEntries) {
				Log.error "[$WebclientTestContext.instance.browser] " + new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage()
			}
		}
		catch (Exception ex) {
			Log.error "Failed to get browser logs for $WebclientTestContext.instance.browser: $ex.message"
		}
	}

	private static void printCookies() {
		try {
			Log.info "Browser Cookies:"
			driver.manage().cookies.each {
				Log.warn it.toString()
			}
		}
		catch (Exception ex) {
			Log.warn "Failed to get browser cookies for $WebclientTestContext.instance.browser: $ex.message"
		}
	}

	protected static void deleteCookies() {
        try {
            Log.debug "Delete all browser cookies"
            driver.manage().deleteAllCookies()
        }
        catch (Exception ex) {
            Log.warn "Failed to delete all browser cookies for $WebclientTestContext.instance.browser: $ex.message"
        }
    }

	/**
	 * Switches to another browser window, if more than 1 window exists.
	 */
	static String switchToOtherWindow(boolean bWaitForAngular = true) {
		def currentWindow = driver.windowHandle
		def otherWindow = driver.windowHandles.find { it != currentWindow }

		if (otherWindow) {
			driver.switchTo().window(otherWindow)
//			resizeBrowserWindow()
			if (bWaitForAngular) {
				waitForAngularRequestsToFinish()
			}
			return otherWindow
		}

		return null
	}

	static void closeOtherWindows() {
		def currentWindow = driver.windowHandle

		driver.windowHandles.each {
			if (it != currentWindow) {
				driver.switchTo().window(it)
				driver.close()
			}
		}

		driver.switchTo().window(currentWindow)
	}

	static int getWindowsCount() {
		return driver.windowHandles.size()
	}

	static void blur() {
		executeScript('document.activeElement.blur();')
	}

	static class SteadinessCondition implements ExpectedCondition<WebElement> {

		final WebElement parent
		final String selector

		private WebElement element = null
		private Point location = null

		SteadinessCondition(final WebElement parent, final String selector) {
			this.parent = parent
			this.selector = selector
		}

		@Override
		WebElement apply(WebDriver driver) {
			if (element == null) {
				try {
					element = parent != null ?
							parent.findElement(By.cssSelector(selector)) :
							driver.findElement(By.cssSelector(selector))
				} catch (NoSuchSessionException | NoSuchElementException ex) {
					return null
				}
			}
			try {
				if (element.isDisplayed()) {
					Point currentLocation = element.getLocation()
					if (currentLocation.equals(location)) {
						return element
					}
					location = currentLocation
				}
			} catch (StaleElementReferenceException ex) {
				element = null
			}
			return null
		}

		@Override
		String toString() {
			return "SteadinessCondition for: " + debugSelector(parent, selector)
		}
	}

	static Object executeScript(String script, Object... args) {
		((JavascriptExecutor) driver).executeScript(script, args)
	}

	static Object executeAsyncScript(String script, Object... args) {
		((JavascriptExecutor) driver).executeAsyncScript(script, args)
	}

	/**
	 * Downloads a file via AJAX in the browser (as the current user).
	 * Use only for small files!
	 */
	static byte[] downloadFile(String url) {
		executeAsyncScript("""
var callback = arguments[arguments.length - 1];
var oReq = new XMLHttpRequest();
oReq.open("GET", "$url", true);
oReq.responseType = "arraybuffer";
oReq.withCredentials = true;

oReq.onload = oEvent => {
	var arrayBuffer = oReq.response; // Note: not oReq.responseText
	if (arrayBuffer) {
		var byteArray = new Uint8Array(arrayBuffer);
		callback(byteArray);
	} else {
		callback(arrayBuffer)
	}
};
oReq.onerror = error => {
	callback(error);
};

oReq.send(null);
""") as byte[]
	}

	static boolean isHorizontalScrollbarPresent() {
		executeScript('return document.documentElement.scrollWidth > document.documentElement.clientWidth;')
	}

	static boolean isVerticalScrollbarPresent() {
		executeScript('return document.documentElement.scrollHeight > document.documentElement.clientHeight;')
	}

	static highlight(WebElement element) {
		executeScript('arguments[0].style.border="3px solid red"', element)
	}

	static RemoteWebDriver getDriver() {
		driver
	}

	static Locale getLocale() {
		AbstractNuclosTest.context.locale
	}

	static void setClipboardContents(String text) {
		StringSelection stringSelection = new StringSelection( text )
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard()
		clipboard.setContents(stringSelection, null)
	}

	protected boolean checkCurrentActiveElement(String tagName, Map<String, String> elementAttributes) {
		def elementToCheck = new NuclosWebElement(AbstractWebclientTest.driver.switchTo().activeElement(), 'activeElement')
		def attributesMatch = false
		elementAttributes.forEach({ String key, String value ->
			attributesMatch = elementToCheck.getAttribute(key) == value
		})
		return elementToCheck.getTagName() == tagName && attributesMatch
	}
}
