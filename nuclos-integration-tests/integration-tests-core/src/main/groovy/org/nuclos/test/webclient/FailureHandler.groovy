package org.nuclos.test.webclient

import org.nuclos.test.log.Log
import org.nuclos.test.webclient.util.Screenshot

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class FailureHandler {

	private static boolean bEnabled

	/**
	 * Handles any failure (e.g. assertion errors) and throws them.
	 *
	 * Tries to gather some useful information (browser logs, screenshot, last URL) before throwing.
	 *
	 * @param failure
	 * @param t
	 */
	static void fail(String failure, Throwable t) {
		if (!t) {
			t = new Throwable(failure)
		}

		logErrorAndEnvironmentInfos(failure, t)

		throw t
	}

	static void logErrorAndEnvironmentInfos(String failure, Throwable t) {
		if (!bEnabled) {
			return
		}

		AbstractWebclientTest.printBrowserDebugInfos()

		Log.error "FAILURE: " + failure, t
        if (AbstractWebclientTest.driver) {
            Screenshot.take("failure")
        }
	}

	static void setLoggingEnabled(final boolean b) {
		this.bEnabled = b
	}
}
