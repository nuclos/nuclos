package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.waitForNotNull
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import java.text.NumberFormat

import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.Browser
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.WebclientTestContext
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@CompileStatic
class AbstractPageObject {
	static List<NuclosWebElement> getErrorMessages() {
		waitForNotNull(5, {$vVv('.error-dialog')}, false)
		$$('.error-dialog div[ng-bind-html="dialog.messageTitle"]')
	}

	static List<NuclosWebElement> getErrorStacktraces() {
		waitForNotNull(5, {$vVv('.error-dialog')}, false)
		return $$('.error-dialog div[ng-bind-html="dialog.stacktrace"]')
	}

	/**
	 * Returns the trimmed text of an open modal alert dialog.
	 *
	 * @return
	 */
	static String getAlertText(boolean expectEmpty = false) {
		boolean throwFailures = !expectEmpty
		waitForNotNull(5, {$vVv('nuc-dialog div.modal-body')?.text?.trim()}, throwFailures)
	}

	static void clickButtonClose() {
		try {
			$('div.modal-header #button-close').click()
			waitForAngularRequestsToFinish()
			sleep(250)
		} catch (NullPointerException npe) {
			screenshot("no-close-button");
			throw npe;
		}
	}

	static void clickFooterButtonClose(Closure closureToOpenModal = null) {
		clickButton('button-close', closureToOpenModal)
	}

	static void clickButtonOk(Closure closureToOpenModal = null) {
		clickButton('button-ok', closureToOpenModal)
	}

	static void clickButtonCancel(Closure closureToOpenModal = null) {
		clickButton('button-cancel', closureToOpenModal)
	}

	static void clickButtonYes(Closure closureToOpenModal = null) {
		clickButton('button-yes', closureToOpenModal)
	}

	static void clickButtonNo(Closure closureToOpenModal = null) {
		clickButton('button-no', closureToOpenModal)
	}

	private static void clickButton(final String buttonId, final Closure closureToOpenModal = null) {
		waitForAngularRequestsToFinish()
		if (closureToOpenModal != null) {
			closureToOpenModal()
		}
		NuclosWebElement button = waitForNotNull(5, {$vVv('div.modal-footer #' + buttonId)}, false)
		if (button == null) {
			waitForAngularRequestsToFinish()
			if (closureToOpenModal != null) {
				closureToOpenModal()
			}
			button = $('div.modal-footer #' + buttonId)
		}
		try {
			button.click()
			waitForAngularRequestsToFinish()
			sleep(250)
		} catch (NullPointerException npe) {
			screenshot('no-' + buttonId)
			throw npe
		}
	}

	static void refresh() {
		Log.info "Refreshing: $currentUrl"

		// getDriver().navigate().refresh() does not always work with PhantomJS
		if (WebclientTestContext.instance.browser != Browser.PHANTOMJS) {
			//sometimes just hangs up, then retry
			waitUntilTrue({
				return waitForNotNull(10, {
					AbstractWebclientTest.getDriver().navigate().refresh()
					return true
				})
			})
		}
		else {
			getUrl(currentUrl)
		}

		waitForAngularRequestsToFinish()
	}

	static String formatValue(value) {
		if (value instanceof Date) {
			return AbstractWebclientTest.context.dateFormat.format(value)
		}
		else if (value instanceof BigDecimal ) {
			return NumberFormat.getNumberInstance(AbstractWebclientTest.context.locale).format(value)
		}

		return "$value"
	}
	
	static WebElement findElementContainingText(String cssSelector, String text) {
		return $$(cssSelector).find { it.text == text }
	}

	static def executeScript(String script, Object ...args) {
		((org.openqa.selenium.JavascriptExecutor) AbstractWebclientTest.getDriver()).executeScript(script, args)
	}
}
