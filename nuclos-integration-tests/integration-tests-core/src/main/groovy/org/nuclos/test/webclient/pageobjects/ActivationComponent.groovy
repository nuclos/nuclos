package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import groovy.transform.CompileStatic

/**
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class ActivationComponent extends AbstractPageObject {
	static String containerSelector = '#activation-container'

	static String getMessage() {
		$("$containerSelector .alert").text?.trim()
	}

	static String getErrorMessage() {
		$("$containerSelector .alert.alert-danger").text?.trim()
	}

	static String getSuccessMessage() {
		$("$containerSelector .alert.alert-success").text?.trim()
	}
}
