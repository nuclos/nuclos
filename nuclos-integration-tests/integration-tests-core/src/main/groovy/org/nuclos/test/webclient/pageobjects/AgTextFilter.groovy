package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.Keys
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@CompileStatic
class AgTextFilter {
	NuclosWebElement filterBox;
	NuclosWebElement filterOption;
	NuclosWebElement filterText;

	AgTextFilter() {
		initFilter()
	}

	void initFilter() {
		filterBox = $('.ag-filter')
		if (filterBox != null) {
			filterOption = filterBox.$('div.ag-picker-field-display')
			filterText = filterBox.$('.ag-input-field-input')
		}
	}

	String filterOptionString() {
		initFilter()
		filterOption?.text
	}

	String filteredText() {
		initFilter()
		filterText?.value
	}

	void enterFilterText(String textToFilter) {
		initFilter()
		if (filterText) {
			filterText.doubleClick()
			sendKeys(textToFilter)
			sendKeys(Keys.ENTER)
			sendKeys(Keys.ESCAPE)
			waitForAngularRequestsToFinish()
		}
	}

	void clearFilter() {
		initFilter()
		if (filterText) {
			filterText.doubleClick()
			new Actions(driver).keyDown(Keys.CONTROL).perform()
			sendKeys('a')
			new Actions(driver).keyUp(Keys.CONTROL).perform()
			sendKeys(Keys.DELETE)
			sendKeys(Keys.ENTER)
			sendKeys(Keys.ESCAPE)
			waitForAngularRequestsToFinish()
		}
	}

	void selectFilterOption(String option) {
		initFilter()
		if (filterOption) {
			filterOption.click()
			NuclosWebElement optionEl = $$('.ag-popup .ag-list .ag-list-item').find({ NuclosWebElement item ->
				try {
					if (item?.text == option) {
						return item
					}
				} catch (StaleElementReferenceException ignored) {
					return null
				}
			})
			optionEl?.click()
		}
		sendKeys(Keys.ESCAPE)
	}
}
