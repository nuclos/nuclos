package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.LoginParams
import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

/**
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class AuthenticationComponent extends AbstractPageObject {
	static String getErrorMessage() {
		$('div.alert')?.text
	}

	static void setLogin(LoginParams params) {
		$zZz('#username').focus()
		clearInput()
		sendKeys(params.username)
		sendKeys(Keys.TAB)
		clearInput()
		sendKeys(params.password)
		if (params.autologin) {
			$('#autologin').click()
		}
		if (params.datalanguage) {
			clickDataLanguageOption($zZz('#datalanguage')?.$$('option'), params.datalanguage)
		}
	}

	static void clickDataLanguageOption(List<NuclosWebElement> dataLanguageOptions, String dataLanguageLabel) {
		def dataLanguageOption = dataLanguageOptions?.find { it.text == dataLanguageLabel }
		if (dataLanguageOption) {
			dataLanguageOption.click()
		} else {
			String[] optionsArray = null
			if (dataLanguageOptions) {
				optionsArray = dataLanguageOptions.stream().map { dlo -> dlo.text }.toArray{size -> new String[size]}
			}
			throw new IllegalArgumentException("Data language option \"" + dataLanguageLabel + "\" not found. Values are: " + String.join(', ', optionsArray))
		}
	}

	static void submit() {
		getSubmitButton().click()
	}

	static NuclosWebElement getSubmitButton() {
		return $('#submit')
	}

	static NuclosWebElement getUsernameInput() {
		return $('input[id=username]')
	}

	static NuclosWebElement getPasswordInput() {
		return $('input[id=password]')
	}

	/**
	 * Returns the trimmed text of the username label.
	 *
	 * @return
	 */
	static String getUsernameLabel() {
		$('label[for="username"]').text.trim()
	}

	/**
	 * Returns the trimmed text of the username label.
	 *
	 * @return
	 */
	static String getPasswordLabel() {
		$('label[for="password"]').text.trim()
	}

	static void clickSelfRegistration() {
		$('a[href$="/register"]').click()
	}

	static NuclosWebElement getForgotLoginDetailsLink() {
		$('#forgot-login-details-link')
	}
}
