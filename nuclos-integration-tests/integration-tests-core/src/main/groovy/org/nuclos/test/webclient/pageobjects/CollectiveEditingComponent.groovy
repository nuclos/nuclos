package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.isPresent

import groovy.transform.CompileStatic

@CompileStatic
class CollectiveEditingComponent extends AbstractPageObject {

	static boolean isCollectiveEditing() {
		isPresent("nuc-collective-editing")
	}

	static void startEditing() {
		$("#startCollectiveEditingBtn").click()
	}

	static void stopEditing() {
		$("#stopCollectiveEditingBtn").click()
	}

	static boolean isStartEditingEnabled() {
		$("#startCollectiveEditingBtn").isEnabled()
	}

	static ListOfValues getAttributeSelection() {
		ListOfValues.fromElement(
				'nuc-field-select-dropdown',
				$('nuc-dropdown[for-id=nuc-field-select-dropdown]')
		)
	}

	static TableComponent getAttributeTable() {
		TableComponent.within($("nuc-collective-editing"))
	}
}
