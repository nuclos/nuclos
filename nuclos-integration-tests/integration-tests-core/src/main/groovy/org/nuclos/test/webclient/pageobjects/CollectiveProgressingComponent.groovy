package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.waitUntilTrue
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.By
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.TimeoutException

import groovy.transform.CompileStatic

/**
 * Represents the collective progressing component.
 */
@CompileStatic
class CollectiveProgressingComponent extends AbstractPageObject {

	static boolean isCollectiveProcessing() {
		def elCollectiveProcessing = AbstractWebclientTest.$("nuc-collective-processing")
		elCollectiveProcessing?.isDisplayed()
	}

	static void showDetails() {
		$xXx("#backToDetails").click()
	}

	static void showCollectiveProcessing() {
		$("#backToCollectiveProcessing").click()
	}

	static void cancelCollectiveProcessing() {
		$xXx("#cancelCollectiveProcessing").click()
	}

	static List<NuclosWebElement> getStateChangeActions() {
		if (isPresent('.collective-processing-state-change-action')) {
			return $$('.collective-processing-state-change-action')
		} else {
			return new ArrayList<NuclosWebElement>()
		}
	}

	static List<NuclosWebElement> getObjectActions() {
		if (isPresent('.collective-processing-object-action')) {
			return $$('.collective-processing-object-action')
		} else {
			return new ArrayList<NuclosWebElement>()
		}
	}

	static List<NuclosWebElement> getFunctionActions() {
		if (isPresent('.collective-processing-function-action')) {
			return $$('.collective-processing-function-action')
		} else {
			return new ArrayList<NuclosWebElement>()
		}
	}

	static void doStateChange(String stateName) {
		getStateChangeActions().find{
			it.text?.startsWith(stateName)
		}.$('a').click()
	}

	static void doAction(String actionName) {
		getObjectActions().find{
			it.text?.startsWith(actionName)
		}.$('a').click()
	}

	static void doFunction(String funcName, boolean withWait = true) {
		Log.debug "doFunction $funcName"
		NuclosWebElement function = waitForNotNull(10, {
			getFunctionActions().find {
				it.text?.startsWith(funcName)
			}
		}, false)
		assert function != null: "Function with name '$funcName' not found"

		withWait ? function.$('a').click() : function.$('a').clickWithoutWait()
	}

	static List<String>  getStateChangeNames() {
		def result = []
		getStateChangeActions().each {
			result.add(it.$('a').text)
		}
		return result
	}

	static List<String>  getActionNames() {
		def result = []
		getObjectActions().each {
			result.add(it.$('a').text)
		}
		return result
	}

	static List<String>  getFuncNames() {
		def result = []
		getFunctionActions().each {
			result.add(it.$('a').text)
		}
		return result
	}

	static void assertProgress() {
		assert $zZz('nuc-collective-processing-progress') != null
	}

	static String getProgressHeader() {
		driver?.findElement(By.cssSelector('div.collective-processing-header > p'))?.text
	}

	static boolean progressGridOverlayVisible() {
		!driver?.findElement(By.cssSelector('nuc-collective-processing-progress > div.collective-processing-progress-container > ag-grid-angular > div.ag-root-wrapper > div.ag-root-wrapper-body > div.ag-root > div.ag-overlay'))
		?.getAttribute('class')?.contains('ag-hidden')
	}

	static void assertProgressSuccess(final int elements) {
		waitUntilTrue({checkProgressSuccess(elements, elements)})
	}

    static void assertProgressSuccess(final int success, final int sum) {
		waitUntilTrue({checkProgressSuccess(success, sum)})
	}

	static boolean checkProgressSuccess(final int success, final int sum) {
        int countSuccess = 0
		for (int i = 0; i < sum; i++) {
			boolean bResult = (getProgressSuccess(i)
					&& getProgressInfoRowNumber(i) == i+1 + ''
					&& getProgressName(i).length() > 0
					&& getProgressNote(i).length() == 0)
			if (bResult) {
				countSuccess++
			}
		}
		return countSuccess == success
	}

	static List<String> getProgressGenObjectNames(int elements) {
		def result = []
		for (int i = 0; i < elements; i++) {
			result.add(getProgressGenObjName(i))
		}
		return result
	}

	static String getProgressInfoRowNumber(int rowIndex) {
		getProgressValue(rowIndex, 'infoRowNumber')
	}

	static String getProgressName(int rowIndex) {
		getProgressValue(rowIndex, 'name')
	}

	static String getProgressGenObjName(int rowIndex) {
		getProgressValue(rowIndex, 'boGenerationResult.bo.title')
	}

	static String getProgressNote(int rowIndex) {
		getProgressValue(rowIndex, 'note')
	}

	static boolean getProgressSuccess(int rowIndex) {
		getProgressValue(rowIndex, 'success')
	}

	private static getProgressValue(int rowIndex, String col) {
		List result = waitForNotNull(120, {
			NuclosWebElement cell = $('nuc-collective-processing-progress ag-grid-angular [row-index="' + rowIndex + '"] [col-id="' + col + '"]')
			if (col == 'success') {
				if (cell.$('.fa-check-square-o') != null) {
					return [Boolean.TRUE]
				} else if (cell.$('.fa-square-o') != null) {
					return [Boolean.FALSE]
				} else {
					return null
				}
			} else {
				[cell.text]
			}
		}, true, StaleElementReferenceException.class, TimeoutException.class, NullPointerException.class)
		if (result) {
			return result.first()
		} else {
			throw new IllegalStateException('Failed to read progress value for row ' + rowIndex + ' and col ' + col)
		}
	}

	static void closeProgress() {
		$xXx("#closeProgress").click()
	}

	static void scrollBottom() {
		$("nuc-collective-processing").scrollBottom()
	}
}
