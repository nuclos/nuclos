package org.nuclos.test.webclient.pageobjects

import org.nuclos.test.webclient.NuclosWebElement

class DataLanguageModal {

	NuclosWebElement element
	String title

	void enterTranslation(String languageId, String translation) {
		element.$vVv('tr#' + languageId +" .p-inputtext").setValue(translation)
	}

	String getTranslation(String languageId) {
		element.$vVv('tr#' + languageId +" .p-inputtext").getValue()
	}

	void clickEmptyLanguageButton(String languageId) {
		element.$vVv('tr#' + languageId + " button").click()
	}

	void clickButtonOk() {
		element.$vVv('#button-ok').click()
	}

	void clickButtonCancel() {
		element.$vVv('#button-cancel').click()
	}

}
