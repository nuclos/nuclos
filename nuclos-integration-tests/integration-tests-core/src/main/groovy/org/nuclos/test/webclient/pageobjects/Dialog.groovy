package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import groovy.transform.CompileStatic

@CompileStatic
class Dialog {

	static close() {
		def button = $('nuc-dialog #button-close')

		button?.click()
		// wait until modal is closed correctly
		waitFor {
			$('nuc-dialog') == null
		}

		return button
	}

	static confirmClose() {
		$("nuc-dialog #button-ok").click()
	}

}
