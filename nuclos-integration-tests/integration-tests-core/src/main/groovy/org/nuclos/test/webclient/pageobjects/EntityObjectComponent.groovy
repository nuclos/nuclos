package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.*
import static org.nuclos.test.log.CallTrace.trace
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import java.text.SimpleDateFormat
import java.util.NoSuchElementException

import org.codehaus.groovy.runtime.typehandling.GroovyCastException
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebInputComponent
import org.nuclos.test.EntityClass
import org.nuclos.test.EntityObject
import org.nuclos.test.log.Log
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.planningtable.PlanningTable
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.util.ValueParser
import org.nuclos.test.webclient.validation.ValidationStatus
import org.openqa.selenium.*
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

/**
 * Represents the entity-object component.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class EntityObjectComponent extends AbstractPageObject {

	static <PK> EntityObjectComponent open(EntityObject<PK> eo) {
		open(eo.entityClass, eo.id)
	}

	static <PK> EntityObjectComponent open(EntityClass<PK> entityClass, PK entityObjectId = null, boolean openSidebarTree = false) {
		String url = "/view/$entityClass.fqn"
		if (entityObjectId) {
			url += "/$entityObjectId"
			if (openSidebarTree) {
				url += "?explorerTreeMetaId=$entityClass.fqn&explorerTreeId=$entityObjectId"
			}
		}
		getUrlHash(url)

		return forDetail()
	}

	static EntityObjectComponent forDetail() {
		EntityObjectComponent result = new EntityObjectComponent()
		assert result != null
		return result
	}

	static EntityObjectModal forModal(final Closure closureToOpenModal = null) {
		waitForAngularRequestsToFinish()
		if (closureToOpenModal != null) {
			closureToOpenModal()
		}
		EntityObjectModal result = new EntityObjectModal()
		assert result != null
		NuclosWebElement modal = waitForNotNull(5, {$vVv(result.getSelectorPrefix())}, false)
		if (modal == null) {
			waitForAngularRequestsToFinish()
			if (closureToOpenModal != null) {
				closureToOpenModal()
			}
			modal = $(result.getSelectorPrefix())
		}
		assert modal
		return result
	}

	static EntityObjectDialog forDialog() {
		EntityObjectDialog result = new EntityObjectDialog()
		assert result != null
		return result
	}

	String getSelectorPrefix() {
		return 'nuc-detail '
	}

	/**
	 * Clicks the "new" Button to create a new entity object.
	 * Should only be available if the current EO is not new or dirty.
	 */
	EntityObjectComponent addNew() {
		trace("Add new EO") {
			newButtonZzz.click()
			return this
		}
	}

	/**
	 * Clicks the "save" Button to save the current entity object.
	 * Should only be available if the current EO is new or dirty.
	 */
	EntityObjectComponent save() {
		trace("Save EO") {
			saveButtonZzz.click()
			waitForAngularRequestsToFinish()
			return this
		}
	}

	void saveNoWait() {
		trace("Save EO") {
			saveButtonZzz.clickWithoutWait()
		}
	}

	/**
	 * Clicks the "cancel" Button to cancel modifying the current entity object.
	 * Should only be available if the current EO is marked as new or dirty.
	 */
	EntityObjectComponent cancel() {
		clickButtonOk({
			cancelButtonZzz.click()
			waitForAngularRequestsToFinish()
		})
		return this
	}

	/**
	 * Clicks the "refresh" Button to refresh the current entity object.
	 */
	EntityObjectComponent reload() {
		refreshButton.click()
		waitForAngularRequestsToFinish()
		return this
	}

	/**
	 * Clicks the "delete" Button to delete the current entity object.
	 * Should only be available if the current EO is not new and not dirty.
	 */
	EntityObjectComponent delete() {
		clickButtonOk({
			getDeleteButton().click()
			waitForAngularRequestsToFinish()
		})
		return this
	}

	/**
	 * Clicks the "clone" Button to save the current entity object.
	 * Should only be available if the current EO is new or dirty.
	 */
	EntityObjectComponent clone() {
		trace("Clone EO") {
			cloneButton.click()
			return this
		}
	}

	/**
	 * Closes this window and selects the current EO in the opener window.
	 * This should be only available if this window was opened via the search function
	 * of a reference field.
	 */
	static void selectInOtherWindow() {
		$('#selectEntryInOtherWindow').clickWithoutWait()
	}

	NuclosWebElement getAttributeElementWaitFor(
			String attributeName,
			Class<? extends WebComponent> componentClass = null,
			int waitForSeconds = 10
	) {
		return waitForNotNull(waitForSeconds, {getAttributeElement(attributeName, componentClass)})
	}


	/**
	 * Finds the corresponding WebElement for the given attribute name.
	 *
	 * Warning: Theoretically there could be multiple input nonCloneableFields for the same attribute.
	 */
	NuclosWebElement getAttributeElement(
			String attributeName,
			Class<? extends WebComponent> componentClass = null,
			boolean bWaitFor = false
	) {
		waitForAngularRequestsToFinish()
		return waitForNotNull(bWaitFor?10:0, {
			return getAttributeElementUnsafe(attributeName, componentClass, bWaitFor)}, bWaitFor, StaleElementReferenceException.class)
	}

	private NuclosWebElement getAttributeElementUnsafe(
			String attributeName,
			Class<? extends WebComponent> componentClass = null,
			boolean bWaitFor = false
	) {
		String selector = "[name=\"attribute-$attributeName\"]"

		if (componentClass) {
			selector = 'nuc-' + dashSeparated(componentClass.simpleName) + ' ' + selector
		}

		selector = selectorPrefix + selector

		if (bWaitFor) {
			$zZz(selector)
		} else {
			$(selector)
		}
	}

	static String dashSeparated(String s) {
		return s.replaceAll(/\B[A-Z]/, '-$0').toLowerCase()
	}

	/**
	 * Checks if the corresponding input field for the given attribute name is displayed.
	 *
	 * @param attributeName
	 * @return
	 */
	boolean isFieldVisible(String attributeName) {
		getAttributeElement(attributeName)?.isDisplayed()
	}

	boolean isAttributeReadonly(String attributeName) {
		def element = getAttributeElement(attributeName);
		String inputType = element.getAttribute('type')
		boolean useCssClass = false
		if (inputType == 'url') {
			useCssClass = true
		} else if (inputType == 'email') {
			useCssClass = true
		}
		if (useCssClass) {
			return element.getAttribute('class').indexOf('nuc-readonly') != -1
		} else {
			def input = element.$('input')
			if (input) {
				return input.getAttribute('readonly') as boolean
			} else {
				return element.getAttribute('readonly') as boolean
			}
		}
	}

	/**
	 * Fills the corresponding input element for attributeName with the given value.
	 *
	 * @param attributeName
	 * @param value
	 */
	void setAttribute(String attributeName, Object value,
	                  Class<? extends WebInputComponent> componentClass = null) {
		try {
			getAttributeElement(attributeName, componentClass,true)

			if (value instanceof Date) {
				value = new SimpleDateFormat('yyyy-MM-dd').format(value)
			}

			activateInputViaMouseOverButton(attributeName)

			NuclosWebElement element = getAttributeElement(attributeName, componentClass)

			if (value instanceof File) {
				if (value == TestDataHelper.nuclosPngFile) {
					setImageAttribute(attributeName, value as File)
				} else {
					setFileAttribute(attributeName, value as File)
				}
				return
			}

			if (element.tagName == 'p-editor') {
				enterTextForEditor(element, value)
				return
			}

			if (element.$('.p-autocomplete')) {
				ListOfValues lov = ListOfValues.fromElement(attributeName, element)
				boolean hasEntry = lov.hasEntry("$value")
				if (!hasEntry && value['id'] && value['name']) {
					value = value['name']
					hasEntry = lov.hasEntry("$value")
				}
				if (hasEntry) {
					lov.selectEntry("$value")
				} else if (value != null) {
					throw new NoSuchElementException("LOV option not found: " + attributeName + "=" + value)
				}
				return
			}

			String inputType = element.getAttribute('type')
			if (inputType == 'checkbox') {
				String checked = element.getAttribute('checked')
				if (checked as boolean != value as boolean) {
					element.click()
				}
			} else {
				// HyperlinkTest / hyperlink field, exception from Selenium: "invalid element state: Element must be user-editable in order to clear it."
				// But input is possible? ..... Selenium :(
				boolean selectOldTextInsteadOfClear = (inputType == 'url' || inputType == 'email' || 'date')
				enterText(attributeName, "$value", element, selectOldTextInsteadOfClear)
			}

			blur()

			boolean elementNoLongerExists = false;
			if (inputType == 'url' || inputType == 'email') {
				// if focus is lost, the element is hidden
				elementNoLongerExists = true;
			}

			if (!elementNoLongerExists) {
				if (element.getAttribute('ngbdatepicker') != null) {
					// Makes sure that the datepicker panel is closed
					sendKeys(Keys.ESCAPE)
				}
			}
		} catch (StaleElementReferenceException e) {
			this.setAttribute(attributeName, value, componentClass);
		}

		waitForAngularRequestsToFinish()
	}

	private void enterTextForEditor(
			NuclosWebElement element,
			Object value
	) {
		element.$(".ql-editor")?.click()
		new Actions(driver)
				.keyDown(Keys.CONTROL)
				.sendKeys('a')
				.keyUp(Keys.CONTROL)
				.build()
				.perform()
		sendKeys("$value")
	}

	void setFileAttribute(String attributeName, File file) {
		FileComponent component = getFileComponent(attributeName)
		component.setFile(file)
	}

	void setImageAttribute(String attributeName, File file) {
		ImageComponent component = getImageComponent(attributeName)
		component.dropFile(file)
	}

	void enterText(String attributeName, String value, NuclosWebElement element = null, selectOldTextInsteadOfClear = false) {
		if (!element) {
			element = getAttributeElement(attributeName)
		}

		element.click()
		if (selectOldTextInsteadOfClear) {
			new Actions(driver)
					.keyDown(Keys.CONTROL)
					.sendKeys('a')
					.keyUp(Keys.CONTROL)
					.build()
					.perform()
		} else {
			element.clear()
		}
		sendKeys(value)

		// clear()/sendKeys('') will not mark eo dirty
		if (value == '') {
			sendKeys(Keys.SPACE)
			sendKeys(Keys.BACK_SPACE)
		}
	}

	/**
	 * Gets the value for the given attribute from the corresponding input field.
	 *
	 * The optional parameter "componentClass" can be used to distinguish between the input components
	 * if there are multiple different components for the same attribute on the layout.
	 */
	def <T> T getAttribute(
			String attributeName,
			Class<? extends WebInputComponent> componentClass = null,
			Class<T> valueClass = String.class
	) {
		trace("getAttribute('$attributeName')") {
			T result

			NuclosWebElement field = getAttributeElement(attributeName, componentClass)

			if (field == null) {
				return null
			}

			// checkbox
			if (field.getAttribute('type') == 'checkbox') {
				return field.isSelected()
			}

			// lov
			if (isLovOrComoboboxElement(field)) {
				field = field.$('p-autocomplete input')
			}

			//textarea
			if (isTextareaElement(field)) {
				field = field.$('div > textarea')
			}

			String value
			if (field.tagName == 'a' || isHTMLStaticField(field)) {
				value = field.text?.trim()
			} else if (isHTMLEditorElement(field)) {
				value = field.$('div.p-editor-content').text?.trim()
			} else {
				value = field.getAttribute('value')
			}

			if (valueClass == String.class) {
				if (value?.contains('\n')) {
					value = value?.substring(0, value?.indexOf('\n'))
				}
				result = value as T
			} else if (valueClass == Date.class) {
				result = ValueParser.parseDate(value) as T
			} else if (valueClass == Integer.class) {
				result = value != null ? Integer.parseInt(value) as T : null
			} else if (valueClass == BigDecimal.class) {
				result = ValueParser.parseBigDecimal(value) as T
			} else {
				throw new IllegalArgumentException('Can not get attribute value for class ' + valueClass)
			}

			return result
		} as T
	}

	static Date getCreatedAt() {
		String createdAtTitle = $('nuc-detail-statusbar div').getTooltip()
		String c = createdAtTitle.findAll(/\[.*\]/)[0]
		String createdAtString = c.subSequence(1, c.length() - 1)
		createdAtString ? ValueParser.parseDate(createdAtString) : null
	}
	static Date getChangedAt() {
		def changedAtTitle = $('nuc-detail-statusbar div').getText()
		String c = changedAtTitle.findAll(/\[.*\]/)[0]
		String changedAtString = c.subSequence(1, c.length() - 1)
		changedAtString ? ValueParser.parseDate(changedAtString) : null
	}
	static String getChangedBy() {
		$('nuc-detail-statusbar div > div').text
	}

	static String getLabelText(String attributeName) {
		getLabel(attributeName)?.text
	}

	static NuclosWebElement getLabel(String attributeName) {
		$('nuc-web-label label[data-name="' + attributeName + '"]')
	}

	static NuclosWebElement getStaticLabel(String label) {
		$('nuc-web-label-static label[name="attribute-' + label + '"]')
	}


	String getText(
			String attributeName,
			Class<? extends WebInputComponent> componentClass = null
	) {
		getAttribute(attributeName, componentClass, String.class)
	}

	/**
	 * Gets the LoV page object for the given attribute name.
	 *
	 * TODO: The same attribute could be present in the layout multiple times and in different input components.
	 *
	 * @param attributeName
	 * @param value
	 */
	ListOfValues getLOV(String attributeName) {
		ListOfValues.findByAttribute(attributeName)
	}

	FileComponent getFileComponent(String attributeName) {
		NuclosWebElement element = getAttributeElement(attributeName)

		if (!element) {
			return null
		}

		new FileComponent(element)
	}

	ImageComponent getImageComponent(String attributeName) {
		NuclosWebElement element = getAttributeElement(attributeName)

		if (!element) {
			return null;
		}

		new ImageComponent(element, attributeName)
	}

	Datepicker getDatepicker(String attributeName) {
		NuclosWebElement element = getAttributeElement(attributeName).parent
		new Datepicker(element)
	}

	/**
	 * Determines if the currently selected EO is marked as dirty.
	 *
	 * @return
	 */
	static boolean isDirty() {
		$('nuc-detail-buttons [name="isDirty"]').getAttribute('value') == 'true'
	}

	static Long getId() {
		$('nuc-detail-buttons [name="id"]').getAttribute('value')?.toLong()
	}

	static PlanningTable getPlanningTable(String planningComponentName) {
		final String fqn = 'Plantafel' + '_' + planningComponentName

		NuclosWebElement planningTableElement = $('[id="' + fqn + '"]')

		if (!planningTableElement) {
			return null
		}

		return new PlanningTable(planningTableElement)
	}

	static Subform getSubform(EntityClass entityClass, String referenceAttributeName) {
		final String fqn = entityClass.fqn + '_' + referenceAttributeName
		return getSubform(fqn)
	}

	/**
	 * @deprecated Use {@link #getSubform(EntityClass, String)}
	 */
	@Deprecated()
	static Subform getSubform(String refAttrFqn) {
		NuclosWebElement subformElement = $('[ref-attr-id="' + refAttrFqn + '"]')

		if (!subformElement) {
			return null
		}

		new Subform(refAttrFqn: refAttrFqn, subformElement: subformElement)
	}

	static void clickButton(final String text) {
		trace("Click Button \"$text\"") {
			waitForNotNull(30) {getButton(text)}.click()
		}
	}

	static void clickButtonWithoutWait(final String text) {
		trace("Click Button (no wait) \"$text\"") {
			getButton(text).clickWithoutWait()
		}
	}

	static NuclosWebElement getButton(String buttonText) {
		$$('button').find { it.text.trim() == buttonText.trim() }
	}

	static WebElement checkButton(String buttonText, boolean exists, boolean enabled) {
		if (!exists) {
			// only to ensure that it is not shown with a time delay
			sleep(100)
		}
		WebElement we = getButton(buttonText)

		if (!exists) {
			assert we == null
			return null
		}

		assert we != null
		waitUntilTrue {
			we.getAttribute('disabled') as boolean == !enabled
		}

		return we
	}

	NuclosWebElement checkField(String attributeName, boolean exists, boolean enabled) {
		if (!exists) {
			// only to ensure that it is not shown with a time delay
			sleep(100)
		}
		NuclosWebElement we = getAttributeElement(attributeName, null, exists)

		if (!exists) {
			assert we == null
			return null
		}

		assert we != null

		try {
			checkFieldUnsafe(attributeName, we, enabled)
		} catch (StaleElementReferenceException e) {
			we = getAttributeElement(attributeName, null, exists)
			checkFieldUnsafe(attributeName, we, enabled)
		}

		return we
	}

	private void checkFieldUnsafe(String attributeName, NuclosWebElement we, boolean enabled) {
		if (isLovOrComoboboxElement(we)) {
			// Dropdown / LOV
			boolean disabled = we.$$('input')[0].getAttribute('readonly') as boolean
			assert disabled == !enabled
		} else if ('div'.equals(we.getTagName())) {
			boolean disabled = we.getAttribute('class').contains('disabled') as boolean
			assert disabled == !enabled
		} else {
			assert isAttributeReadonly(attributeName) == !enabled
		}
	}

	static Set<String> getLabels() {
		return waitForNotNull(30, {return getLabelsUnsafe()}, true, StaleElementReferenceException.class)
	}

	private static Set<String> getLabelsUnsafe() {
		waitForAngularRequestsToFinish()
		Set<String> ret = new HashSet<String>()

		List<NuclosWebElement> labels = $$('label')
		Log.info("labels size: " + labels.size())
		for (WebElement webElement : labels) {
			String text = webElement.getText()
			ret.add(text)
		}

		return ret
	}

	static NuclosWebElement getTab(String tabName, boolean strict = false) {
		WebElement tabTitle = $$('.p-tabview-nav li').find {
			doesTabMatch(it, tabName, strict)
		}
		if (tabTitle != null) {
			return new NuclosWebElement(
					tabTitle,
					'tab with title "' + tabName + '"'
			)
		}
		return null
	}

	private static boolean doesTabMatch(NuclosWebElement it, String tabName, boolean strict = false) {
		strict && it.getText() == tabName || it.getText() =~ /$tabName(\s\([0-9]+\))?/
	}

	static boolean hasTab(String tabName, boolean strict = false) {
		getTab(tabName, strict) != null
	}

	static void selectTab(String tabName) {
		getTab(tabName).click()
	}

	/**
	 * Creates a new EO of the given entity class, fills in the given data, and saves.
	 */
	static <PK> EntityObjectComponent createEO(final EntityClass<PK> entityClass, final Map<String, ?> eoData, String mandator = null, boolean save = true) {
		EntityObjectComponent eo = open(entityClass)
		eo.addNew()

		if (mandator) {
			chooseMandator(mandator)
		}

		eoData.each { key, value ->
			eo.setAttribute(key, value)
		}

		if (save) {
			eo.save()
		}
	}

	static NuclosWebElement getNewButton() {
		$('#newEO')
	}

	static NuclosWebElement getNewButtonZzz() {
		waitForNotNull(30, {
			return $('#newEO')
		}, true, WebDriverException.class)
	}

	static NuclosWebElement getSaveButton() {
		$('#saveEO')
	}

	static NuclosWebElement getSaveButtonZzz() {
		waitForNotNull(30, {
			return $('#saveEO')
		}, true, WebDriverException.class)
	}

	static NuclosWebElement getDeleteButton() {
		def deleteButtonList = $$('#deleteEO')
		if (deleteButtonList && !deleteButtonList.isEmpty()) {
			return deleteButtonList.last()
		}
	}

	static NuclosWebElement getCancelButtonZzz() {
		$zZz('#cancelEO')
	}

	static NuclosWebElement getCancelButton() {
		$('#cancelEO')
	}

	static NuclosWebElement getCloneButton() {
		$('#cloneEO')
	}

	static NuclosWebElement getRefreshButton() {
		$('#refreshEO')
	}

	private static boolean isLovOrComoboboxElement(NuclosWebElement element) {
		return element.getAttribute('class').indexOf('lov-container') != -1
	}

	private static boolean isLovElement(NuclosWebElement element) {
		return isLovOrComoboboxElement(element) && element.parent.getTagName() == 'nuc-web-listofvalues'
	}

	private static boolean isComoboboxElement(NuclosWebElement element) {
		return isLovOrComoboboxElement(element) && element.parent.getTagName() == 'nuc-web-combobox'
	}

	private static boolean isTextareaElement(NuclosWebElement element) {
		return element.$('div > textarea') != null
	}

	private static boolean isHTMLEditorElement(NuclosWebElement element) {
		return element.getTagName() == 'p-editor'
	}

	private static boolean isHTMLStaticField(NuclosWebElement element) {
		return element.getTagName() == 'nuc-web-html-field' || element.getParent().getTagName() == 'nuc-web-html-field'
	}

	private static void chooseMandator(String mandator) {
		List<WebElement> options = $('#mandators').findElements(By.xpath("//*[text()='" + mandator + "']"))
		if (options.size() == 0) {
			throw new NoSuchElementException("not found")
		}
		options.get(0).click()
	}

	/**
	 * select the process if a process dropdown is defined in layout
	 */
	void selectProcess(String processName) {
		setAttribute('nuclosProcess', processName)
	}

	static int getListEntryCount() {
		$$('nuc-sidebar ag-grid-angular .ag-center-cols-container [row-index]').size()
	}

	static boolean hasNextStateButton() {
		return $('.nuclos-next-state') != null
	}

	static boolean datePickerIsOpen() {
		return $('ngb-datepicker') != null
	}

	void activateInputViaMouseOverButton(String attributeName) {
		NuclosWebElement element = getAttributeElement(attributeName)

		if (!element) {
			throw new IllegalArgumentException("Could not find element for attribute '$attributeName'")
		}

		String inputType = element.getAttribute('type')
		String mouseOverCssClass = null
		if (inputType == 'url') {
			mouseOverCssClass = '.edit-hyperlink'
		} else if (inputType == 'email') {
			mouseOverCssClass = '.edit-email'
		}

		if (mouseOverCssClass == null) {
			return
		}

		element.mouseover()
		NuclosWebElement edit = element.getParent().$(mouseOverCssClass)
		edit?.click()
	}

	ValidationStatus getValidationStatus(String attributeName) {
		waitForAngularRequestsToFinish()
		NuclosWebElement element = getAttributeElement(attributeName, null, true)
		String cssClasses = element.getAttribute('class')

		ValidationStatus.fromCssClasses(cssClasses)
	}

	static void openCharts() {
		$xXx('#openCharts').click()
	}

	static <PK> PK getPrimaryKey() {
		def url = getCurrentUrl()
		def pk = url.substring(url.lastIndexOf('/') + 1)

		try {
			return pk.toLong() as PK
		} catch (GroovyCastException e) {
			return pk as PK
		}
	}
}
