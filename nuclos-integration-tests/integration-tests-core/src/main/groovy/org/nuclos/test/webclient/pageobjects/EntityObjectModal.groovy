package org.nuclos.test.webclient.pageobjects

import groovy.transform.CompileStatic

/**
 * Represents the (sub-)entity-object component when opened in a detail modal.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class EntityObjectModal extends EntityObjectComponent {
	@Override
	String getSelectorPrefix() {
		'nuc-detail-modal '
	}

	@Override
	ListOfValues getLOV(final String attributeName) {
		ListOfValues.findByAttributeInModal(attributeName)
	}
}
