package org.nuclos.test.webclient.pageobjects

import org.nuclos.test.webclient.NuclosWebElement
import static org.nuclos.test.webclient.AbstractWebclientTest.sendKeys

import org.openqa.selenium.Keys

class ExplorerTreeComponent {

	final NuclosWebElement element

	ExplorerTreeComponent(NuclosWebElement element) {
		this.element = element
	}

	void assertNodeSelected(String node) {
		NuclosWebElement nodeElement = element.$zZz('div.p-treenode-content[aria-label="' + node + '"]')
		assert nodeElement.$('.node-text-selected') != null: node + 'should be visible and expanded'
	}

	void assertNodeExpanded(String node) {
		NuclosWebElement nodeElement = element.$zZz('div.p-treenode-content[aria-label="' + node + '"][aria-expanded="true"]')
		assert nodeElement: node + 'should be visible and expanded'
	}

	void assertNodeCollapsed(String node) {
		NuclosWebElement nodeElement = element.$zZz('div.p-treenode-content[aria-label="' + node + '"]')
		assert !nodeElement.hasProperty('aria-expanded')
		assert nodeElement: node + 'should be visible and collapsed'
	}

	/** Selects the node with the specified title **/
	void selectNode(String title) {
		NuclosWebElement nodeElement = element.$zZz('div.p-treenode-content[aria-label="' + title + '"]')
		nodeElement.click();
	}

	/** Opens the node of this tree that is currently selected **/
	void clickOpenButtonOnSelectedNode() {
		element.$('.node-text-selected').click()
	}

	void openNodeByClick(String nodeTitle) {
		NuclosWebElement nodeElement = element.$zZz('div.p-treenode-content[aria-label="' + nodeTitle + '"]')
		nodeElement.$('.custom-node').click()
	}

	void openSelectedNodeByKey() {
		sendKeys(Keys.ENTER)
	}

	void keyToggleExpansionOnSelectedNode() {
		sendKeys(Keys.SPACE)
	}

	String getNodeImageHref(String nodeTitle) {
		element.$zZz('div.p-treenode-content[aria-label="' + nodeTitle + '"] img').getAttribute("src")
	}

	String[] getAllNodeLabels() {
		List<NuclosWebElement> nodeList = element.$$zZz('div.p-treenode-content')
		Set<String> ariaLabelSet = nodeList.collect { it.getAttribute('aria-label') }.findAll { it != null }.toSet()
		return ariaLabelSet.toArray(new String[0])
	}

	int getNodeCount() {
		List<NuclosWebElement> nodeList = element.$$zZz('div.p-treenode-content')
		return nodeList.size()
	}

	NuclosWebElement getNodeByTitle(String title) {
		NuclosWebElement nodeElement = element.$zZz('div.p-treenode-content[aria-label="' + title + '"]')
		return nodeElement;
	}

	/** Expands the node with the specified title **/
	void clickToggleExpansion(String title) {
		NuclosWebElement nodeElement = element.$zZz('div.p-treenode-content[aria-label="' + title + '"]')
		nodeElement.$('button').click()
	}

	/** Clicks the expand all button of the TreeExplorerComponent **/
	void expandAll() {
		element.$('#expandAll').click()
	}

	/** Clicks the collapse all button of the TreeExplorerComponent **/
	void collapseAll() {
		element.$('#collapseAll').click()
	}

	/** Clicks the refresh button of the ExplorerTreeComponent **/
	void refresh() {
		element.$('#refreshTree').click()
	}

}
