package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import java.util.regex.Matcher

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class FileComponent extends AbstractPageObject {
	final NuclosWebElement element

	FileComponent(NuclosWebElement element) {
		this.element = element
	}

	void setFile(File file) {
		NuclosWebElement input = element.$('input')
		if (file) {
			input.sendKeys(file.absolutePath)
		}
	}

	void clearFile() {
		element.mouseover()

		AbstractWebclientTest.waitFor {
			NuclosWebElement reset = element.getParent().$('.reset-document')
			reset?.click()
			return reset
		}
	}

	String getImageUrl() {
		NuclosWebElement imageElement = getImageElement()
		if (imageElement != null) {
			return imageElement.getAttribute('src')
		}
		return ''
	}

	String getText() {
		def end = element.text.indexOf('\n')
		element.text.substring(0, end != -1 ? end : element.text.length())
	}

	boolean hasImage() {
		getImageElement() != null
	}

	NuclosWebElement getImageElement() {
		element.$('img.image')
	}

	NuclosWebElement getFileuploadContainer() {
		element
	}

	String getHref() {
		element.$('a').getAttribute('href')
	}

	void downloadFile() {
		element.mouseover()
		$('.icons .fa-download').click()
	}


	void deleteFile() {
		element.mouseover()
		$('.icons .fa-trash').click()
	}
}
