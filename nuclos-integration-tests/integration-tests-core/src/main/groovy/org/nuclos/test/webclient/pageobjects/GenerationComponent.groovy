package org.nuclos.test.webclient.pageobjects


import static org.nuclos.test.AbstractNuclosTest.waitForNotNull
import static org.nuclos.test.log.CallTrace.trace
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.util.Screenshot

import groovy.transform.CompileStatic

/**
 * Represents the 'Generator' component which is typically shown in the toolbar above an EO.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class GenerationComponent extends AbstractPageObject {

	/**
	 * Executes the object generator with the given name.
	 *
	 * @param name
	 */
	static int generateObjectAndConfirm(final String name, confirm = true, String dialogSelector = 'nuc-dialog') {
		int generatorCount = 0

		Closure generateClosure = {
			NuclosWebElement container = $vVv('nuc-generation')
			NuclosWebElement dropdownBtn = container.$zZz('.dropdown-toggle')

			if (dropdownBtn.getAttribute('aria-expanded') == 'false') {
				dropdownBtn.click()
			}

			List<NuclosWebElement> lstWe = container.$$('.dropdown-item')
			generatorCount = lstWe.size()

			waitForAngularRequestsToFinish()

			// The many random errors indicate that the following click was not fulfilled, and no error is thrown. This is just a trial ...
			sleep(1000)

			Screenshot.take("before click objgen dropdown \\\"$name\\\"")

			trace("Click objgen dropdown \"$name\"") {
				lstWe.find { it.text?.trim() == name }.click()
			}

			waitForAngularRequestsToFinish()

			Screenshot.take("after click objgen dropdown \\\"$name\\\"")
		}
		generateClosure()

		def modal = waitForNotNull(30, {getMessageModal(dialogSelector)}, false)
		if (modal == null && confirm) {
			// click not successfully, try again
			generateClosure()
			modal = getMessageModal(dialogSelector)
		}
		if (confirm) {
			modal.confirm()
		}
		return generatorCount
	}

	/**
	 * If there is a result dialog, clicks on the result link.
	 */
	static void openResult() {
		$('#generation-result-dialog a')?.click()
	}

	static List<NuclosWebElement> getGenerationItems() {
		NuclosWebElement container = $('nuc-generation')
		container.$$('.dropdown-item')
	}

	static int countParameterObjects() {
		parameterObjects.size()
	}

	private static List<NuclosWebElement> getParameterObjects() {
		$$('nuc-parameter-object ag-grid-angular .ag-body-viewport .ag-center-cols-viewport [row-index]').sort({
			first, second -> Integer.parseInt(first.getAttribute('row-index')) < Integer.parseInt(second.getAttribute('row-index')) ? -1 : 1
		})
	}

	static void selectParameterObject(int idx) {
		parameterObjects[idx].click()
		waitForNotNull(30) {
			$('nuc-modal-dialog')
		}.$('div.modal-footer #button-confirm').click()
	}

	static void cancelParameterObjectSelection() {
		waitForNotNull(30) {
			$('nuc-modal-dialog')
		}.$('div.modal-footer #button-cancel').click()
	}

	static void closeParameterObjectSelection() {
		Modal.close()
	}

	static String generationListingsName() {
		NuclosWebElement container = $('nuc-generation')
		return container.$('button').getText().trim()
	}
}
