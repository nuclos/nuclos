package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import org.nuclos.test.webclient.NuclosWebElement

class ImageComponent extends AbstractPageObject {
	final NuclosWebElement element
	final String attributeName

	ImageComponent(NuclosWebElement element, String attributeName) {
		this.element = element
		this.attributeName = attributeName
	}

	void dropFile(File file) {
		$("#attribute-" + attributeName + "-testInputFile").sendKeys(file.path)
	}

	NuclosWebElement openContextMenu() {
		element.click()
		return $('.p-contextmenu')
	}

	List<String> contextActions() {
		def contextMenu = openContextMenu()
		contextMenu.$$('ul li')*.text
	}

	void executeContextAction(String actionName) {
		def contextMenu = openContextMenu().$$('ul li').find({
			it -> it.text == actionName
		})
		contextMenu?.click()
	}

	boolean emptyImage() {
		return element.$("img") == null
	}

	boolean isReadOnly() {
		return element.$("div") == null
	}
}
