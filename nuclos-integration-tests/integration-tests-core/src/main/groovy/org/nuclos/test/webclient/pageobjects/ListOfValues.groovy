package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.sleep
import static org.nuclos.test.AbstractNuclosTest.waitForNotNull
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import javax.annotation.Nonnull
import javax.annotation.Nullable

import org.nuclos.schema.layout.web.WebCombobox
import org.nuclos.schema.layout.web.WebListofvalues
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriverException
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@CompileStatic
class ListOfValues {

	@Nullable String attributeName
	NuclosWebElement lov

	static ListOfValues findByAttribute(String attributeName) {
		NuclosWebElement element = EntityObjectComponent.forDetail().getAttributeElement(attributeName, WebListofvalues)
		if (!element) {
			element = EntityObjectComponent.forDetail().getAttributeElement(attributeName, WebCombobox)
		}
		fromElement(
				attributeName,
				element
		)
	}

	static ListOfValues findByAttributeInModal(final String attributeName
	) {
		// somehow static type checker could not resolve correctly
		// putting default params for this call fixed it
		NuclosWebElement element = EntityObjectComponent.forModal().getAttributeElement(attributeName, null, false)
		fromElement(
				attributeName,
				element
		)
	}

	static ListOfValues fromElement(
			@Nullable String attributeName,
			@Nonnull NuclosWebElement element
	) {
		if (!element) {
			return null
		}

		new ListOfValues(
				attributeName: attributeName,
				lov: element
		)
	}

	ListOfValues() {}

	void open() {
		if (!open) {
			waitForNotNull(90, {
				NuclosWebElement button = dropdownButton
				if (button) {
					button.click()
				} else {
					lov.click()
				}
				return lovPanelZzz
			}, true, WebDriverException.class, NullPointerException.class)
		}
	}

	void clickDropdownButton() {
		dropdownButton.click()
		waitForAngularRequestsToFinish()
	}

	void close() {
		if (open) {
			lovWidget.$('input').click()
		}
	}

	void closeViaEscape() {
		if (open) {
			sendKeys(Keys.ESCAPE)
		}
	}

	void selectEntry(String text) {
		if (!open) {
			open()
		}

		def choices = getChoices()
		for (int i = 0; i < choices.size(); i++) {
			if (choices[i] == text) {
				NuclosWebElement element = choiceElements[i];
				NuclosWebElement lovPanel = getLovPanel();

				while (!element.isDisplayed() && lovPanel.canScrollBottom()) {
					lovPanel.scrollBottom()
					sleep(100)
				}

				if (element.isDisplayed()) {
					element.click();
					try {
						// Sometimes a fragment remains ... close it
						if (open) {
							close()
						}
					} catch(Exception e) {
					}
				} else {
					// It is possible that the component is not completely displayed in the window
					// and extends beyond the bottom edge.
					// TODO find a solution
					throw new NoSuchElementException("No such visible option in dropdown: $text $choices")
				}
				return
			}
		}

		throw new NoSuchElementException("No such option in dropdown: $text $choices")
	}

	boolean hasEntry(String text) {
		open()
		def choices = getChoices()
		for (int i = 0; i < choices.size(); i++) {
			if (choices[i] == text) {
				return true
			}
		}
		return false
	}

	/**
	 * Returns the text of the currently highlighted entry (if the dropdown is visible).
	 *
	 * @return
	 */
	String getHighlightedEntry() {
		trimText(lovPanel.$('li.p-highlight')?.text)
	}

	String getHighlightedEntryZzz() {
		trimText(lovPanelZzz.$zZz('li.p-highlight')?.text)
	}

	String getSelectedEntryFromDropdown() {
		trimText(lovPanel.$('li > div.selected')?.text)
	}

	String getSelectedEntryFromDropdownZzz() {
		trimText(lovPanelZzz.$zZz('li > div.selected')?.text)
	}

	/**
	 * Returns the current textual value of this LOV.
	 *
	 * @return
	 */
	String getTextValue() {
		trimText(inputElement.value)
	}

	/**
	 * Enters the given search text into the input field.
	 */
	void search(String text) {
		inputElement.click()
		inputElement.sendKeys(Keys.CONTROL, 'a')
		waitForAngularRequestsToFinish()
		sendKeys(text)
		waitForAngularRequestsToFinish()
	}

	NuclosWebElement getInputElement() {
		lovContainer.$('input')
	}

	List<NuclosWebElement> getChoiceElements() {
		try {
			open()
			NuclosWebElement ul = lovPanelZzz.$$zZz('ul').find { it.displayed }

			if (!ul) {
				return []
			}

			// TODO: This is not very performant. Unify the dropdown component (NUCLOS-6571) and use a class selector here.
			// The reason for not using simply a "li" selector is that there can be an additional li element for
			// the empty/loading message, which is just plain text.
			ul.$$('.dd-item')*.parent
		} catch (Exception ignored) {
			return [];
		}
	}

	List<String> getChoices() {
		List<String> result = []
		List<NuclosWebElement> choiceElements = getChoiceElements()
		for (int i = 0; i < choiceElements.size(); i++) {
			NuclosWebElement element = choiceElements[i]
			String s = element.getAttribute('innerText')
			s = s.replace('\u200B','')
			s = s.trim()
			result.add(s)
		}
		result
	}

	boolean isOpen() {
		lovPanel?.displayed
	}

	NuclosWebElement getLovContainer() {
		lov
	}

	String getComponentId() {
		lovContainer?.getAttribute('for-id')
	}

	NuclosWebElement getLovPanel() {
		return _getLovPanel(false)
	}

	NuclosWebElement getLovPanelZzz() {
		return _getLovPanel(true)
	}

	private NuclosWebElement _getLovPanel(boolean bWaitFor) {
		String sel = '.p-autocomplete-panel[for-id="' + componentId + '"]'
		if (bWaitFor) {
			$zZz(sel)
		} else {
			$(sel)
		}
	}

	NuclosWebElement getLovWidget() {
		lovContainer?.$('span.p-autocomplete')
	}

	NuclosWebElement getDropdownButton() {
		lovContainer?.$('.p-autocomplete-dropdown')
	}

	boolean isRefreshVisible() {
		getSearchElement(false)
	}

	NuclosWebElement getRefreshElement(boolean bWaitFor) {
		if (bWaitFor) {
			return lov.$zZz(".refresh-reference")
		} else {
			return lov.$(".refresh-reference")
		}
	}

	boolean isSearchVisible() {
		getSearchElement(false)
	}

	NuclosWebElement getSearchElement(boolean bWaitFor) {
		if (bWaitFor) {
			return lov.$zZz(".search-reference")
		} else {
			return lov.$(".search-reference")
		}
	}

	boolean isAddVisible() {
		getAddElement(false)
	}

	NuclosWebElement getAddElement(boolean bWaitFor) {
		if (bWaitFor) {
			return lov.$(".add-reference")
		} else {
			return lov.$(".add-reference")
		}
	}

	boolean isOpenVisible() {
		getOpenElement(false)
	}

	NuclosWebElement getOpenElement(boolean bWaitFor) {
		if (bWaitFor) {
			return lov.$(".edit-reference")
		} else {
			return lov.$(".edit-reference")
		}
	}

	private <T> T retryOnHoveredIsNotClickable(Closure<T> c) {
		try {
			return c()
		} catch (WebDriverException e) {
			// retry the hovering
			new Actions(driver).moveToElement(lov.$('input').element, 0, -50).build().perform()
			return c()
		}
	}

	void refreshReference() {
		retryOnHoveredIsNotClickable {
			hoverLovOverlay(false)
			getRefreshElement(true).clickWithoutWait()
		}
	}

	void searchReference() {
		retryOnHoveredIsNotClickable {
			hoverLovOverlay(false)
			getSearchElement(true).clickWithoutWait()
		}
	}

	void addReference() {
		retryOnHoveredIsNotClickable {
			hoverLovOverlay(false)
			getAddElement(true).clickWithoutWait()
		}
	}

	void openReference() {
		retryOnHoveredIsNotClickable {
			hoverLovOverlay(false)
			getOpenElement(true).clickWithoutWait()
		}
	}

	void hoverLovOverlay(boolean bAndWait) {
		new Actions(driver).moveToElement(lov.$('input').element).build().perform()

		if (bAndWait) {
			sleep(1000)
		}
	}

	void blur() {
		inputElement.blur()
	}

	private String trimText(String text) {
		return text?.replace('\u200B', '')?.trim()
	}

	void sendKeys(final CharSequence... keysToSend) {
		AbstractWebclientTest.sendKeys(keysToSend)
		waitForAngularRequestsToFinish()
	}

	void clearMultiSelection() {
		open()
		lovContainer?.$$('.p-autocomplete-token-icon')?.stream()?.forEach({ icon -> icon.click() })
	}
}
