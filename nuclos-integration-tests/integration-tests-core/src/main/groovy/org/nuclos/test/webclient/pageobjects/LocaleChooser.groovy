package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.sleep
import static org.nuclos.test.AbstractNuclosTest.waitForNotNull
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.WebDriverException

import groovy.transform.CompileStatic

/**
 * Represents the locale chooser component, which should be available on all Webclient pages.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LocaleChooser extends AbstractPageObject {
	/**
	 * Returns the currently via the locale chooser component selected locale, e.g. "de".
	 * @return
	 */
	static String getLocale() {
		getElement(false)?.$('div.selected-locale').getAttribute('innerHTML')
	}

	/**
	 * Sets the given locale for the tests and selects it in the Webclient
	 * via the locale chooser component.
	 *
	 * TODO: Do nothing if the correct locale is already selected
	 *
	 * @param locale
	 */
	static void setLocale(Locale locale) {
		if ($("div[class=locale-chooser] a:not([class~=dropdown-item]) > img[src=assets\\/flags\\/$locale.language\\.png]")) {
			// locale already active
			return
		}

		//getElement(true).click()
		def clickedChooser = click($xXx('nuc-locale .locale-chooser'))
		waitForAngularRequestsToFinish()

		waitForNotNull(60, {
			return click($(clickedChooser, "a[class~=locale-$locale.language]"))
		}, true,
				NullPointerException.class,
				WebDriverException.class // element not interactable
		)

		// give the browser some time to load the page before waitingForAngular etc.
		sleep(500)
		waitForAngularRequestsToFinish()
	}

	static NuclosWebElement getElement(boolean bWaitFor) {
		waitForNotNull(bWaitFor?30:0, {
			return $('nuc-locale .locale-chooser')
		}, true, WebDriverException.class)
	}
}
