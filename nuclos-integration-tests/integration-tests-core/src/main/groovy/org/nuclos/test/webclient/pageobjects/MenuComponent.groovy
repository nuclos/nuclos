package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.waitFor
import static org.nuclos.test.AbstractNuclosTest.waitForNotNull
import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$zZz
import static org.nuclos.test.webclient.AbstractWebclientTest.$zZz
import static org.nuclos.test.webclient.AbstractWebclientTest.getLocale
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 * Represents the menu component, should always be visible.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class MenuComponent extends AbstractPageObject {
	/**
	 * Returns the trimmed text of preference menu item.
	 * The user menu must be toggled before the link text can be read.
	 *
	 * @return
	 */
	static String getPreferenceLinkText() {
		$('a[href="#/preferences"]').text.trim()
	}

	/**
	 * Clicks on the user menu dropdown to toggle it.
	 */
	static void toggleUserMenu() {
		waitForNotNull(10, {$('#user-menu a.dropdown-toggle')}).click()
	}

	static void toggleDevUtilsMenu() {
		try {
			waitForNotNull(10, { $('#Dev-Utils-menu a.dropdown-toggle') }).click()
		} catch (Throwable t) {
			waitForNotNull(10, { $('#Dev-utils-menu a.dropdown-toggle') }).click()
		}
	}

	static void clickSwaggerUI() {
		waitForNotNull(10, { $('#navbarNavDropdown a[href="#/swagger-ui"]') }).click()
		waitForAngularRequestsToFinish()
	}

	/**
	 * Clicks on the user menu dropdown to toggle it.
	 */
	static void toggleAdminMenu() {
		waitForNotNull(10, {$('#Administration-menu a.dropdown-toggle')}).click()
	}

	static void assertConfigurationAvailable() {
		if (getLocale() == Locale.GERMANY) {
			waitForNotNull(10, {$('#Konfiguration-menu a.dropdown-toggle')})
		} else {
			waitForNotNull(10, {$('#Configuration-menu a.dropdown-toggle')})
		}
	}

	static void assertConfigurationNotAvailable() {
		if (getLocale() == Locale.GERMANY) {
			waitFor(10, {$('#Konfiguration-menu a.dropdown-toggle') == null})
		} else {
			waitFor(10, {$('#Configuration-menu a.dropdown-toggle') == null})
		}
	}

	static void assertDevUtilsAvailable() {
		if (getLocale() == Locale.GERMANY) {
			waitForNotNull(10, { $('#Dev-Utils-menu a.dropdown-toggle') })
		} else {
			waitForNotNull(10, { $('#Dev-utils-menu a.dropdown-toggle') })
		}
	}

	static void assertDevUtilsNotAvailable() {
		if (getLocale() == Locale.GERMANY) {
			waitFor(10, { $('#Dev-Utils-menu a.dropdown-toggle') == null})
		} else {
			waitFor(10, { $('#Dev-utils-menu a.dropdown-toggle') == null})
		}
	}

	static void assertServerInfoAvailable() {
		toggleAdminMenu()
		waitForNotNull(10, {
			$$('#Administration-menu nuc-menu-item div').find {
				def itemName = getLocale() == Locale.GERMANY ? 'Server-Info' : 'Server info'
				itemName == it.text
			}
		})
		toggleAdminMenu()
	}

	static void assertServerInfoNotAvailable() {
		if ($('#Administration-menu')) {
			toggleAdminMenu()
			waitFor(10, {
				$$('#Administration-menu nuc-menu-item div').find {
					def itemName = getLocale() == Locale.GERMANY ? 'Server-Info' : 'Server info'
					itemName == it.text
				} == null
			})
		}
	}

	static void assertMaintenanceModeAvailable() {
		toggleAdminMenu()
		waitForNotNull(10, {
			$$('#Administration-menu nuc-menu-item div').find {
				def itemName = getLocale() == Locale.GERMANY ? 'Wartungsmodus' : 'Maintenance mode'
				itemName == it.text
			}
		})
		toggleAdminMenu()
	}

	static void assertMaintenanceModeNotAvailable() {
		if ($('#Administration-menu')) {
			toggleAdminMenu()
			waitFor(10, {
				$$('#Administration-menu nuc-menu-item div').find {
					def itemName = getLocale() == Locale.GERMANY ? 'Wartungsmodus' : 'Maintenance mode'
					itemName == it.text
				} == null
			})
		}
	}

	static void assertAdministrationMenuItemAvailable(String itemDE, String itemEN) {
		toggleAdminMenu()
		waitForNotNull(10, {
			$$('#Administration-menu nuc-menu-item div').find {
				def itemName = getLocale() == Locale.GERMANY ? itemDE : itemEN
				itemName == it.text
			}
		})
		toggleAdminMenu()
	}

	static void assertAdministrationMenuItemNotAvailable(String itemDE, String itemEN) {
		if ($('#Administration-menu')) {
			toggleAdminMenu()
			waitFor(10, {
				$$('#Administration-menu nuc-menu-item div').find {
					def itemName = getLocale() == Locale.GERMANY ? itemDE : itemEN
					itemName == it.text
				} == null
			})
		}
	}

	static void clickChangePasswordLink() {
		waitForNotNull(10, {$('a[href="#/account/changePassword"]')}).click()
	}

	static void clickOpenPreferencesResetModal() {
		waitForNotNull(10, {$('#open-preferences-reset-modal')}).click()
	}

	static void clickOpenExecuteReports() {
		waitForNotNull(10, {$('#open-reports-manage')}).click()
	}

	static String getSelectedDataLanguage() {
		return $zZz('a[class~="datalanguage-item"][class~="selected"]')?.text
	}

	static void clickDataLanguage(String dataLanguageLabel) {
		AuthenticationComponent.clickDataLanguageOption($$zZz('a[class~="datalanguage-item"]'), dataLanguageLabel)
	}

	static void clickOpenPreferencesManage() {
		getOpenPreferencesManage().click()
	}

	static NuclosWebElement getOpenPreferencesManage() {
		$('#open-preferences-manage')
	}

	static void openMenu(String[] menuPath) {
		def menuElement = $zZz('nuc-menu')
		for (int i = 0; i < menuPath.length; i++) {
			String menuName = menuPath[i]
			boolean isFirst = i == 0;
			boolean isLast = i == menuPath.length - 1;
			if (isFirst) {
				menuElement
						.$$('.nav-link')
						.findAll{menuName.equals(it.text)}.first()
						.click()
			} else {
				def elem = menuElement
						.$$('.dropdown-item')
						.find { menuName.equals(it.text.trim()) }
				isLast ? elem.click() : elem.mouseover()
			}
		}
	}
}
