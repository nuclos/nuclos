package org.nuclos.test.webclient.pageobjects

import org.nuclos.test.webclient.NuclosWebElement

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class MessageModal {
	NuclosWebElement element

	String title
	String message

	void confirm() {
		element.$vVv('div.modal-footer #button-ok').click()
	}

	NuclosWebElement decline() {
		def button = element.$vVv('div.modal-header #button-close')
		button.click()
		return button
	}

	void yes() {
		element.$vVv('div.modal-footer #button-yes').click()
	}

	void no() {
		element.$vVv('div.modal-footer #button-no').click()
	}

	void cancel() {
		element.$vVv('div.modal-footer #button-cancel').click()
	}

	void copyToClipboard() {
		if (hasClipboardBtn()) {
			element.$('div.modal-footer #button-copy').click()
		}
	}

	boolean hasClipboardBtn() {
		element.$vVv('div.modal-footer #button-copy') != null
	}
}
