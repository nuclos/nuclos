package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Modal {
	public static String modalSelector = 'nuc-modal-dialog'

	static Modal forSelector(String modalSelector) {
		def modal = new Modal()
		modal.modalSelector = modalSelector
		return modal
	}

	static String getTitle() {
		$(modalSelector + ' .modal-title')?.text
	}

	static String getBody() {
		$(modalSelector + ' .modal-body')?.text
	}

	static void close() {
		$(modalSelector + ' #button-close')?.click()
	}

	static boolean isVisible() {
		$(modalSelector)?.displayed
	}
}
