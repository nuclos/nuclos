package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.sleep
import static org.nuclos.test.AbstractNuclosTest.waitForNotNull
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import javax.annotation.Nonnull
import javax.annotation.Nullable

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriverException
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@CompileStatic
class MultiselectionCombobox {

	@Nullable String referenceAttributeId
	NuclosWebElement nuclosWebElement

	static MultiselectionCombobox findByAttribute(String refAttrFqn) {
		NuclosWebElement element = $('[ref-attr-id="' + refAttrFqn + '"]')
		fromElement(
				refAttrFqn,
				element
		)
	}

	static MultiselectionCombobox findByAttributeInModal(final String referenceAttrFqn
	) {
		// somehow static type checker could not resolve correctly
		// putting default params for this call fixed it
		NuclosWebElement element = EntityObjectComponent.forModal().getAttributeElement(referenceAttrFqn, null, false)
		fromElement(
				referenceAttrFqn,
				element
		)
	}

	static MultiselectionCombobox fromElement(
			@Nullable String referenceAttributeId,
			@Nonnull NuclosWebElement element
	) {
		if (!element) {
			return null
		}

		new MultiselectionCombobox(
				referenceAttributeId: referenceAttributeId,
				nuclosWebElement: element
		)
	}

	MultiselectionCombobox() {}

	void open() {
		if (!open) {
			waitForNotNull(90, {
				NuclosWebElement button = dropdownButton
				if (button) {
					button.click()
				} else {
					nuclosWebElement.click()
				}
				return lovPanelZzz
			}, true, WebDriverException.class, NullPointerException.class)
		}
	}

	int getChipCount() {
		return nuclosWebElement.$$('.p-multiselect-token').size()
	}

	void clickDropdownButton() {
		dropdownButton.click()
		waitForAngularRequestsToFinish()
	}

	void close() {
		if (open) {
			lovWidget.$('input').click()
		}
	}

	void closeViaEscape() {
		if (open) {
			sendKeys(Keys.ESCAPE)
		}
	}

	void toggleSelectAll() {
		if (!open) {
			open()
		}
		NuclosWebElement checkBoxBox = lovPanel.$('.p-multiselect-header .p-checkbox-box')
		checkBoxBox.click()
	}

	void selectEntry(String text) {
		if (!open) {
			open()
		}

		def choices = getChoices()
		for (int i = 0; i < choices.size(); i++) {
			if (choices[i] == text) {
				NuclosWebElement element = choiceElements[i];
				NuclosWebElement lovPanel = getLovPanel();

				while (!element.isDisplayed() && lovPanel.canScrollBottom()) {
					lovPanel.scrollBottom()
					sleep(100)
				}

				if (element.isDisplayed()) {
					element.click();
					try {
						// Sometimes a fragment remains ... close it
						if (open) {
							close()
						}
					} catch(Exception e) {
					}
				} else {
					// It is possible that the component is not completely displayed in the window
					// and extends beyond the bottom edge.
					// TODO find a solution
					throw new NoSuchElementException("No such visible option in dropdown: $text $choices")
				}
				return
			}
		}

		throw new NoSuchElementException("No such option in dropdown: $text $choices")
	}

	boolean hasEntry(String text) {
		open()
		def choices = getChoices()
		for (int i = 0; i < choices.size(); i++) {
			if (choices[i] == text) {
				return true
			}
		}
		return false
	}

	/**
	 * Returns the text of the currently highlighted entry (if the dropdown is visible).
	 *
	 * @return
	 */
	String getHighlightedEntry() {
		trimText(lovPanel.$('li.p-highlight')?.text)
	}

	String getHighlightedEntryZzz() {
		trimText(lovPanelZzz.$zZz('li.p-highlight')?.text)
	}

	String getSelectedEntryFromDropdown() {
		trimText(lovPanel.$('li > div.selected')?.text)
	}

	String getSelectedEntryFromDropdownZzz() {
		trimText(lovPanelZzz.$zZz('li > div.selected')?.text)
	}

	/**
	 * Returns the current textual value of this LOV.
	 *
	 * @return
	 */
	String getTextValue() {
		trimText(inputElement.text)
	}

	/**
	 * Enters the given search text into the input field.
	 */
	void search(String text) {
		if (!isOpen()) {
			open()
		}
		getLovPanel().$('.p-overlay-content input[type=\'text\'').click()
		new Actions(driver).keyDown(Keys.CONTROL).perform()
		sendKeys('a')
		new Actions(driver).keyUp(Keys.CONTROL).perform()
		sendKeys(Keys.BACK_SPACE)
		waitForAngularRequestsToFinish()
		sendKeys(text)
		waitForAngularRequestsToFinish()
	}

	NuclosWebElement getInputElement() {
		nuclosWebElement.$('.p-multiselect-token-label')
	}

	List<NuclosWebElement> getChoiceElements() {
		try {
			open()
			NuclosWebElement ul = lovPanelZzz.$$zZz('ul').find { it.displayed }

			if (!ul) {
				return []
			}

			// TODO: This is not very performant. Unify the dropdown component (NUCLOS-6571) and use a class selector here.
			// The reason for not using simply a "li" selector is that there can be an additional li element for
			// the empty/loading message, which is just plain text.
			ul.$$('.p-multiselect-item')/**.parent*/
		} catch (Exception ignored) {
			return [];
		}
	}

	List<String> getChoices() {
		List<String> result = []
		List<NuclosWebElement> choiceElements = getChoiceElements()
		for (int i = 0; i < choiceElements.size(); i++) {
			NuclosWebElement element = choiceElements[i]
			String s = element.getAttribute('innerText')
			s = s.replace('\u200B','')
			s = s.trim()
			result.add(s)
		}
		result
	}

	boolean isOpen() {
		lovPanel?.displayed
	}

	NuclosWebElement getLovContainer() {
		nuclosWebElement
	}

	String getComponentId() {
		lovContainer?.getAttribute('for-id')
	}

	NuclosWebElement getLovPanel() {
		return _getLovPanel(false)
	}

	NuclosWebElement getLovPanelZzz() {
		return _getLovPanel(true)
	}

	private NuclosWebElement _getLovPanel(boolean bWaitFor) {
		String sel = '.p-overlay[ref-attr-id="' + referenceAttributeId + '"]'
		if (bWaitFor) {
			$zZz(sel)
		} else {
			$(sel)
		}
	}

	NuclosWebElement getLovWidget() {
		lovContainer?.$('div.p-multiselect')
	}

	NuclosWebElement getDropdownButton() {
		lovContainer?.$('.p-multiselect-trigger')
	}

	boolean isAddVisible() {
		return Integer.parseInt(getAddElement(true).parent.getCssValue('z-index')) != -1
	}

	NuclosWebElement getAddElement(boolean bWaitFor) {
		if (bWaitFor) {
			return nuclosWebElement.$(".add-reference")
		} else {
			return nuclosWebElement.$(".add-reference")
		}
	}

	boolean isOpenVisible() {
		getOpenElement(false)
	}

	NuclosWebElement getOpenElement(boolean bWaitFor) {
		if (bWaitFor) {
			return nuclosWebElement.$(".edit-reference")
		} else {
			return nuclosWebElement.$(".edit-reference")
		}
	}

	private <T> T retryOnHoveredIsNotClickable(Closure<T> c) {
		try {
			return c()
		} catch (WebDriverException e) {
			// retry the hovering
			new Actions(driver).moveToElement(nuclosWebElement.$('input').element, 0, -50).build().perform()
			return c()
		}
	}

	void addReference() {
		retryOnHoveredIsNotClickable {
			hoverLovOverlay(false)
			getAddElement(true).clickWithoutWait()
		}
	}

	void openReference() {
		retryOnHoveredIsNotClickable {
			hoverLovOverlay(false)
			getOpenElement(true).clickWithoutWait()
		}
	}

	void hoverLovOverlay(boolean bAndWait) {
		new Actions(driver).moveToElement(nuclosWebElement.$('p-multiSelect').element).build().perform()

		if (bAndWait) {
			sleep(1000)
		}
	}

	void blur() {
		inputElement.blur()
	}

	private String trimText(String text) {
		return text?.replace('\u200B', '')?.trim()
	}

	void sendKeys(final CharSequence... keysToSend) {
		AbstractWebclientTest.sendKeys(keysToSend)
		waitForAngularRequestsToFinish()
	}

	void clearMultiSelection() {
		open()
		lovContainer?.$$('.p-multiselect-token-icon')?.stream()?.forEach({ icon -> icon.click() })
	}
}
