package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.sendKeys

import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.Keys
import org.openqa.selenium.StaleElementReferenceException

import groovy.transform.CompileStatic

@CompileStatic
class NuclosBooleanFilter {
	NuclosWebElement filterBox
	NuclosWebElement selectionOption

	NuclosBooleanFilter() {
		initFilter()
	}

	void initFilter() {
		filterBox = $('.ag-filter')
		if (filterBox) {
			selectionOption = filterBox.$('nuc-filter-boolean select')
		}
	}

	String getCurrentSelection() {
		initFilter()
		selectionOption?.value
	}

	void selectOption(String option) {
		initFilter()
		if (selectionOption) {
			NuclosWebElement optionEl = selectionOption.$$('option').find({ NuclosWebElement item ->
				try {
					if (item?.value == option) {
						return item
					}
				} catch (StaleElementReferenceException ignored) {
					return null
				}
			})
			optionEl?.click()
		}
		sendKeys(Keys.ESCAPE)
	}

	void clearFilter() {
		initFilter()
		selectOption('all')
	}
}
