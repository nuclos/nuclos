package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.$xXx
import static org.nuclos.test.webclient.AbstractWebclientTest.getMessageModal
import static org.nuclos.test.webclient.AbstractWebclientTest.screenshot

import org.apache.commons.lang3.tuple.Pair
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.By

import groovy.transform.CompileStatic

/**
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class PrintoutComponent extends AbstractPageObject {
	static String containerSelector = '#activation-container'

	static void open() {
		$xXx('nuc-printout button').click()
	}

	static String getPrintoutName(int index) {
		getMessageModal()
		return $$('.output-format-label')[index].getText()
	}

	static void selectPrintout(int index) {
		getMessageModal()
		$$('.output-format-checkbox')[index].click()
	}

	static void execute() {
		$('#execute-printout-button').click()
	}

	static NuclosWebElement getDownloadLink() {
		List<NuclosWebElement> links = getDownloadLinks()
		assert links.size() == 1

		links.first()
	}

	static List<NuclosWebElement> getDownloadLinks() {
		List<NuclosWebElement> links = $$('.printout-downoload-link')
		return links
	}

	static void closeModal() {
		// TODO: Modal component should be used here (currently not possible because of inconsistent modals in the webclient)
		try {
			$('div.modal-header button.btn-close').click()
		} catch (NullPointerException npe) {
			screenshot("no-close-button");
			throw npe;
		}
	}

	static Map<String, Pair<String, String>> getOutputFormatParameters() {
		Map<String, Pair<String, String>> result = new HashMap<>();
		$$('div.output-format-parameter').forEach({ NuclosWebElement it ->
			String forLabel = it.$('label').getAttribute('for')
			String nameLabel = it.$('label').getText()

			def input = it.$('#' + forLabel.replaceAll(':', '\\\\:'))
			def value = input.value == 'on' ? input.isSelected().toString() : input.value
			result.put(forLabel, Pair.of(nameLabel, value))
		})
		return result
	}

	static void setParameterValue(String parameter, String value) {
		def element = AbstractWebclientTest.$("div.output-format-parameter #${parameter.replaceAll(':', '\\\\:')}")
		if (element.getTagName() == 'input' && element.getAttribute('type') == 'checkbox') {
			if (Boolean.parseBoolean(value) && !element.isSelected()) {
				element.click()
			} else if (!Boolean.parseBoolean(value) && element.isSelected()) {
				element.click()
			}
		} else if (element.getTagName() == 'input' && element.hasClass('p-autocomplete-input')) {
			element.findElement(By.xpath("./..//button[contains(@class, 'p-autocomplete-dropdown')]")).click()
			$$('.modal .p-autocomplete-items li div').find { it.text == value }.click()
		} else {
			element.setValue(value, element.getTagName() == 'input' && element.findElements(By.xpath('./../../../nuc-datechooser')).size() > 0)
		}
	}
}
