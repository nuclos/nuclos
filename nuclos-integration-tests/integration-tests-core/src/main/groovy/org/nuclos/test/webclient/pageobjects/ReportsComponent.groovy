package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$$;

import groovy.transform.CompileStatic;

@CompileStatic
class ReportsComponent extends AbstractPageObject{
	static int getReportCount() {
		$$('#reports .ag-body-viewport .ag-center-cols-container .ag-row').size()
	}
}
