package org.nuclos.test.webclient.pageobjects


import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.getDriver
import static org.nuclos.test.webclient.AbstractWebclientTest.sendKeys
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.apache.commons.lang.NotImplementedException
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

/**
 * TODO: Remove this completely.
 */
@CompileStatic
@Deprecated
class Searchtemplate extends AbstractPageObject {

	static WebElement getSearchfilterValueTextInput(String attributeFqn) { $('#search-value-string-' + attributeFqn) }

	static WebElement getSearchfilterValueDatepickerInput() {
		def elems = $$('.searchfilter-popover-content [ngbdatepicker]')
		return elems.size() > 0 ? elems.get(0) : null
	}

	static class SearchTemplateItem {
		String name
		String operator
		String value
		String value2
	}

	static void setSearchCondition(String attributeFqn, SearchTemplateItem searchTemplateItem) {
		// select operator
		if (searchTemplateItem.operator == null) { // boolean
			throw new NotImplementedException("boolean operator is not implemented");
			// TODO implement boolean operator
		} else {
			def searchOp = driver.findElement(By.id('search-operator-' + attributeFqn))
			searchOp.findElement(By.id('operator-' + searchTemplateItem.operator)).click()

			if (searchTemplateItem.value) {
				// default string input
				WebElement input
				try {
					input = driver.findElement(By.id('search-value-string-' + attributeFqn));
					input.clear()
					input.sendKeys('' + searchTemplateItem.value)
				} catch (Exception ex) {
					// continue search...
				}
				try {
					boolean overlayVisible = isLovOverlayIsOpen(searchTemplateItem)
					input = driver.findElement(By.cssSelector('#search-value-lov-' + attributeFqn + ' .p-multiselect-trigger'))
					if (!overlayVisible) {
						input.click()
					}
					waitForAngularRequestsToFinish()
					boolean notSelected = !isLovItemSelected(searchTemplateItem)
					if (notSelected) {
						driver.findElement(By.cssSelector('li[aria-label="' + searchTemplateItem.value + '"]')).click()
					} else {
						sendKeys(Keys.ESCAPE)
					}
					waitForAngularRequestsToFinish()
				} catch (Exception ex) {
					// continue search...
				}
				if (input == null) {
					throw new NotImplementedException("datepicker values are not implemented")
				}
				if (searchTemplateItem.value2) {
					// 2-value-comparison...
					try {
						input = driver.findElement(By.id('search-value2-string-' + attributeFqn));
						input.clear()
						input.sendKeys('' + searchTemplateItem.value2)
					} catch (Exception ex) {
						// continue search...
					}
				}
			}


			// TODO implement dropdown and datepicker
//			List<NuclosWebElement> dropdowns = $$('.searchfilter-attribute-popover .dropdown')
//			if (dropdowns.size() > 0) {
//				// reference
//
//				ListOfValues lov = new ListOfValues()
//				lov.lov = dropdowns[0]
//				if (!lov.open) {
//					lov.open()
//				}
//				lov.selectEntry(searchTemplateItem.value)
//			} else {
//				def datepickerInput = getSearchfilterValueDatepickerInput()
//				if (datepickerInput != null) {
//					// datepicker
//					// TODO sendKeys will not update the model ????
//					datepickerInput.clear()
//					datepickerInput.sendKeys('' + searchTemplateItem.value + Keys.TAB)
//				}
//			}
		}
		waitForAngularRequestsToFinish()
	}

	static boolean isLovOverlayIsOpen(SearchTemplateItem searchTemplateItem) {
		boolean overlayVisible = false;
		try {
			driver.findElement(By.cssSelector('li[aria-label="' + searchTemplateItem.value + '"]'))
			overlayVisible = true
		} catch (Exception e) { }
		return overlayVisible
	}

	static boolean isLovItemSelected(SearchTemplateItem searchTemplateItem) {
		boolean itemSelected = false;
		try {
			driver.findElement(By.cssSelector('li[aria-label="' + searchTemplateItem.value + '"].p-highlight'))
			itemSelected = true
		} catch (Exception e) { }
		return itemSelected
	}
}
