package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.*
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import java.util.stream.Collectors

import org.nuclos.test.EntityClass
import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.Keys
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.WebDriverException
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

/**
 * Represents the sidebar component.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Sidebar extends AbstractPageObject {

	static List<String> getListEntries() {
		entryElements*.text
	}

	static void assertListCount(int iCount) {
		waitUntilTrue({listEntries.size() == iCount})
	}

	/**
	 * Gets the count of entries (EntityObjects) that are currently shown in the Sidebar.
	 */
	static int getListEntryCount() {
		waitForAngularRequestsToFinish()
		entryElements.size()
	}

	static void selectEntry(int index) {
		$('nuc-sidebar ag-grid-angular .ag-body-viewport .ag-center-cols-viewport div[row-index="' + index + '"]')?.click()
	}

	static void selectRange(int startIndex, int endIndex, boolean shiftSelect) {
		if (shiftSelect) {
			entryElements[startIndex].click()
			AbstractWebclientTest.clickWithKey([entryElements[endIndex]], Keys.SHIFT)
		} else {
			AbstractWebclientTest.clickWithKey([entryElements[startIndex], entryElements[endIndex]], Keys.CONTROL)
		}
	}

	static void selectEntryByText(String text) {
		waitForNotNull(60, {
			findEntryByText(text)
		}, true, WebDriverException.class).click()
	}

	static NuclosWebElement findEntryByText(String text) {
		entryElements.find {
			it.$$('.ag-cell').find {
				it.text?.trim() == text
			}
		}
	}

	static int getSelectedIndex() {
		entryElements.findIndexOf { NuclosWebElement it ->
			it.hasClass('ag-row-selected')
		}
	}

	static int countMultiSelected() {
		int count = 0
		$$('nuc-sidebar ag-grid-angular .ag-body-viewport [row-index] div[col-id="selected"] .fa-check-square-o').forEach({ NuclosWebElement it ->
			count++
		})
		return count
	}

	static void assertValue(int rowIndex, int colIndex, def value) {
		waitUntilTrue({
			def valueFromCell = getValue(rowIndex, colIndex)
			Log.debug "assertValue '$valueFromCell' == '$value'"
			return valueFromCell == value
		})
	}

	private static String getValue(int rowIndex, int colIndex) {
		return waitForNotNull(10, {
			return $$('nuc-sidebar ag-grid-angular .ag-body-viewport [row-index="' + rowIndex + '"] .ag-cell')[colIndex].text
		}, true, StaleElementReferenceException.class, NullPointerException.class)
	}

	static String getValueByColumnLabel(int rowIndex, String colName) {
		def colIndex = $$('nuc-sidebar ag-grid-angular .ag-header-container .ag-header-cell-text').findIndexOf {
			WebElement option -> option.text?.trim() == colName
		}

		// if so we need to increment this by one cause of the selection box
		if (isMultiSelecting()) {
			colIndex = colIndex + 1
		}

		getValue(rowIndex, colIndex)
	}

	/*static String getValue(int rowIndex, String attr) {
		$$('nuc-sidebar ag-grid-angular [row-index=' + rowIndex + '] [col-id="' + attr + '"]').text
	}*/

	static List<NuclosWebElement> getEntryElements() {
		// AgGrid garantiert keine sortierte row-index Reihenfolge im DOM mehr
		// Also sortieren wir hier selbst die elemente aus dem DOM nach row-index, die View im Client hingegehen ist korrekt
		$$('nuc-sidebar ag-grid-angular .ag-body-viewport .ag-center-cols-viewport [row-index]').sort({
			first, second -> Integer.parseInt(first.getAttribute('row-index')) < Integer.parseInt(second.getAttribute('row-index')) ? -1 : 1
		})
	}

	static boolean isImageVisible(int rowIndex, String attr) {
		executeScript(
			"return arguments[0].complete && typeof arguments[" + rowIndex + "].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0",
			$('nuc-sidebar ag-grid-angular [row-index="' + rowIndex + '"] [col-id="' + attr + '"] nuc-image-renderer img').getElement()
		)
	}


	static void addColumn(String boMetaId, String name) {
		SideviewConfiguration.open()

		String attributeFqn = boMetaId + '_' + name
		SideviewConfiguration.selectColumn(attributeFqn)

		SideviewConfiguration.close()
	}

	static void hideColumn(String boMetaId, String name) {
		SideviewConfiguration.open()

		String attributeFqn = boMetaId + '_' + name
		SideviewConfiguration.deselectColumn(attributeFqn)

		SideviewConfiguration.close()
	}

	static void moveColumnUp(String boMetaId, String name) {
		moveColumn(boMetaId, name, false)
	}

	static void moveColumnDown(String boMetaId, String name) {
		moveColumn(boMetaId, name, true)
	}

	private static void moveColumn(String boMetaId, String name, boolean down) {
		SideviewConfiguration.open()

		String attributeFqn = boMetaId + '_' + name
		SideviewConfiguration.moveColumn(attributeFqn, down)

		SideviewConfiguration.close()
	}

	/**
	 * WARNING returns only visible columns
	 */
	static List<String> selectedColumns() {
		def result = []
		$$('nuc-sidebar ag-grid-angular .ag-header-row .ag-header-cell-text').each {
			result.add(it.text)
		}
		return result
	}

	static List<String> getColumnHeaders() {
		$$('nuc-sidebar ag-grid-angular .ag-header-row .ag-header-cell-text')*.text*.trim().findAll { it }
	}

	static NuclosWebElement getColumnHeaderElementOrNull(String name) {
		$('nuc-sidebar ag-grid-angular .ag-header-container .ag-header-cell[col-id$="' + name + '"]')
	}

	static List<String> getColumnHeaderTooltips() {
		$$('nuc-sidebar ag-grid-angular .ag-header-row .ag-cell-label-container')*.getTooltip().findAll { it }
	}

	static List<String> getPinnedAttributeColumns(EntityClass entity) {
		$$('nuc-sidebar .ag-header .ag-pinned-left-header .ag-header-row [col-id]').stream()
			.filter {
				it.getAttribute('col-id').startsWith(entity.fqn)
			}
			.map {
				return it.getAttribute('col-id')
			}
			.collect(Collectors.toList())
	}

	static void resizeSidebarComponent(int newWidth, boolean horizontal = false) {
		NuclosWebElement e = getResizeBar()
		if (!horizontal) {
			int x = e.getParent().getLocation().getX()

			new Actions(AbstractWebclientTest.getDriver())
					.dragAndDropBy(e.element, newWidth - x - e.getParent().getSize().getWidth() - 1, 0) // 1 px for rounding errors
					.build()
					.perform()

			AbstractWebclientTest.waitFor {
				getResizeBar().getParent().getLocation().getX() == newWidth
			}
		} else {
			int y = e.getParent().getLocation().getY()

			new Actions(AbstractWebclientTest.getDriver())
					.dragAndDropBy( e.element, 0, newWidth - y - e.getParent().getSize().getHeight() - 1) // 1 px for rounding errors
					.build()
					.perform()

			AbstractWebclientTest.waitFor {
				getResizeBar().getParent().getLocation().getY() == newWidth
			}
		}

		// wait for ag-grid in sidebar to get ready
		sleep(500)
	}

	static void toggleSidebarMode() {
		$('#toolbar > div > button:nth-child(1)')?.click()
	}

	static boolean isHorizontalResizeBar() {
		$('nuc-sidebar nuc-resizehandledivider .fa-ellipsis-h') != null
	}

	static NuclosWebElement getResizeBar() {
		$('#sidebar > div.d-flex.flex-column.justify-content-center > nuc-resizehandledivider > div > div:nth-child(2)')
	}

	static int getSizebarWidth() {
		$('#sidebar').getCssValue('width').replace('px', '').toInteger()
	}

	static int getSizebarHeight() {
		$('#sidebar').getCssValue('height').replace('px', '').toInteger()
	}

	static void resizeLeftMax() {
		$('#sidebar > div.d-flex.flex-column.justify-content-center > nuc-resizehandledivider > div > div:nth-child(1) > button')?.click()
		waitForAngularRequestsToFinish()
	}

	static void resizeRightMax() {
		$('#sidebar > div.d-flex.flex-column.justify-content-center > nuc-resizehandledivider > div > div:nth-child(3) > button')?.click()
		waitForAngularRequestsToFinish()
	}

	static List<NuclosWebElement> getTreeRows() {
		WebElement we = $('#sidetree')
		assert we != null

		WebElement we2 = $(we, '.ag-body-container')
		assert we2 != null

		List<NuclosWebElement> lstWe = $$(we2, '.ag-row')
		return lstWe
	}

	static boolean isEntryMarkedDirty(int index) {
		// check if background is yellow
		entryElements[index].element.getCssValue('background-color').toString().equals('rgba(255, 255, 170, 1)')
	}

	static boolean hasEntryCSSClass(int index, String className) {
		entryElements[index].element.getAttribute("class").contains(className)
	}

	static void toggleMultiSelection() {
		$(".multi-selection-statusbar-button a").click()
	}

	static void toggleMultiSelectionAll() {
		$("nuc-multi-selection-header").click()
	}

	static boolean isMultiSelecting() {
		WebElement we = $("nuc-multi-selection-header")
		if (we == null) {
			return false;
		}
		return we.isDisplayed()
	}

	static NuclosWebElement getStateIconImage(int rowIndex) {
		$('nuc-sidebar ag-grid-angular .ag-body-viewport [row-index="' + rowIndex + '"] .ag-cell[col-id$="_nuclosStateIcon"] img')
	}
}
