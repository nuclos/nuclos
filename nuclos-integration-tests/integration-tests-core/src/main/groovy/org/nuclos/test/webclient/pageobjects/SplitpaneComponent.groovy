package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.AbstractNuclosTest.sleep
import static org.nuclos.test.AbstractNuclosTest.waitUntilTrue
import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@CompileStatic
class SplitpaneComponent extends AbstractPageObject {
	NuclosWebElement paneElement;

	static SplitpaneComponent getByName(String name) {
		new SplitpaneComponent(paneElement: $('nuc-web-component[id^="' + name + '"] > nuc-web-splitpane'))
	}

	int getPaneSize() {
		isHorizontal() ? paneElement?.size?.width : paneElement?.size?.height
	}

	int getGutterSize() {
		isHorizontal() ? getResizeBar()?.size?.width : getResizeBar()?.size?.height
	}

	boolean isHorizontal() {
		paneElement?.$('prime-custom-splitter')?.getAttribute('layout') == 'horizontal'
	}

	void collapsePanel(int index) {
		if (index == 0) {
			getResizeBar()?.$$('button')?.get(0)?.click()
			waitForAngularRequestsToFinish()
		} else {
			getResizeBar()?.$$('button')?.get(1)?.click()
			waitForAngularRequestsToFinish()
		}
	}

	void expandPanel(int index) {
		if (index == 0) {
			getResizeBar()?.$$('button')?.get(1)?.click()
			waitForAngularRequestsToFinish()
		} else {
			getResizeBar()?.$$('button')?.get(0)?.click()
			waitForAngularRequestsToFinish()
		}
	}

	// relative in percent
	int getPanelSize(int index) {
		// we need to subtract 2 for the border size
		Math.round((isHorizontal() ? panels.get(index)?.size?.width / (paneSize - gutterSize - 2) * 100 :
				panels.get(index)?.size?.height / (paneSize - gutterSize - 2) * 100)
		.toDouble())
	}

	List<NuclosWebElement> getPanels() {
		paneElement?.$$('prime-custom-splitter > div.p-splitter > div.p-splitter-panel')
	}

	NuclosWebElement getResizeBar() {
		paneElement?.$('prime-custom-splitter > div.p-splitter > div.p-splitter-gutter')
	}

	void resizePanels(int index, int newSize) {
		def calcNewPaneSize = calculateRelativeToAbsoluteSize(newSize) - calculateRelativeToAbsoluteSize(
				getPanelSize(index)
		)
		NuclosWebElement e = getResizeBar()
		if (!isHorizontal()) {
			new Actions(AbstractWebclientTest.getDriver())
					.dragAndDropBy(e.element, 0, calcNewPaneSize)
					.build()
					.perform()
		} else {
			new Actions(AbstractWebclientTest.getDriver())
					.dragAndDropBy( e.element, calcNewPaneSize, 0)
					.build()
					.perform()
		}

		waitUntilTrue({
			getPanelSize(index) == newSize
		})

		// wait for ag-grid in sidebar to get ready
		sleep(500)
	}

	private int calculateRelativeToAbsoluteSize(int relativeSize) {
		Math.round(((paneSize - gutterSize - 2) * ( relativeSize / 100)).toDouble())
	}
}
