package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 * Represents the state component,
 * should be visible on the detail page of an entity object with state model.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class StateComponent extends AbstractPageObject {

	/**
	 * Returns the trimmed text of the 'current state' element.
	 *
	 * @return
	 */
	static String getCurrentState() {
		currentStateElement?.text?.trim()
	}

	/**
	 * Returns the number of the 'current state' element.
	 *
	 * @return
	 */
	static Integer getCurrentStateNumber() {
		currentStateElement?.getAttribute('state-number')?.toInteger()
	}

	/**
	 * Returns the trimmed texts of all 'next state' elements.
	 *
	 * TODO: If a stat button label is configured, the label is returned here instead of the state name.
	 * It would be probably better to always return the state name here.
	 *
	 * @return
	 */
	static List<String> getNextStates(boolean expectEmpty = false) {
		getNextStateElements(expectEmpty)*.getAttribute("state-name")
	}

	/**
	 * Returns the state numbers of all 'next state' elements.
	 *
	 * @return
	 */
	static List<Integer> getNextStateNumbers() {
		getNextStateElements().collect { it.getAttribute('state-number').toInteger() }
	}

	/**
	 * Tries to click a 'state change' element that contains the given text.
	 *
	 * Warning: Label might be i18nized!
	 *
	 * @param label
	 */
	static void changeState(String label) {
		//Click the currentState Button to open the dropdown -> before selecting the next state
		$("#nuclosCurrentState").click()
		getNextStateElements().find { it.getAttribute("state-name").contains(label) }.click()
		waitForAngularRequestsToFinish()
	}

	/**
	 * Returns all 'next state' elements,
	 * i.e. the state change buttons in the nuc-state component.
	 *
	 * @return
	 */
	private static List<NuclosWebElement> getNextStateElements(boolean expectEmpty = false) {
		if (expectEmpty) {
			return waitForNotNull(5, {$$('nuc-state .nuclos-next-state')}, false)
		}
		return $$('nuc-state .nuclos-next-state')
	}

	/**
	 * Returns all state change buttons of the layout.
	 *
	 * TODO: Those buttons are not exactly part of the state component, so this
	 * method should not be part of the state component.
	 */
	static List<StateButton> getStateButtons() {
		$$zZz('nuc-web-button-change-state button').collect {
			new StateButton(
					element: it,
					label: it.text?.trim(),
					enabled: it.enabled
			)
		}
	}

	/**
	 * FIXME Doesn't work yet
	 *
	 * @return
	 */
	static List<StateButton> getActiveChangeStateButtons() {
		List<StateButton> ret = new ArrayList<>()
		for (StateButton sb : getStateButtons()) {
			if (sb.enabled) {
				ret.add(sb);
			}
		}
		return ret;
	}

	/**
	 * Clicks the 'OK' button of the state change confirmation dialog.
	 */
	static void confirmStateChange(final Closure closureToOpenModal = null) {
		innerStateChange(false, closureToOpenModal)
	}

	/**
	 * Clicks the 'Cancel' button of the state change confirmation dialog.
	 */
	static void cancelStateChange(final Closure closureToOpenModal = null) {
		innerStateChange(true, closureToOpenModal)
	}

	private static void innerStateChange(boolean cancel, final Closure closureToOpenModal = null) {
		waitForAngularRequestsToFinish()
		if (closureToOpenModal != null) {
			closureToOpenModal()
		}
		final String selector = cancel ? '#button-cancel' : '#button-ok'
		NuclosWebElement button = waitForNotNull(5, { $vVv(selector) }, false)
		if (button == null) {
			waitForAngularRequestsToFinish()
			if (closureToOpenModal != null) {
				closureToOpenModal()
			}
			button = $(selector)
		}
		button.click()
		waitForAngularRequestsToFinish()
	}

	static void changeStateByNumeral(final int stateNumber) {
		//Click the currentState Button to open the dropdown -> before selecting the next state
		$("#nuclosCurrentState").click()
		getNextStateElements().find { it.getAttribute('state-number').toInteger() == stateNumber }.click()
		waitForAngularRequestsToFinish()
	}

	static NuclosWebElement getConfirmStateChangeElement() {
		$('nuc-confirm-state-change')
	}

	static class StateButton {
		NuclosWebElement element
		String label
		boolean enabled

		@Override
		String toString() {
			label
		}

		void click() {
			element.click()
		}
	}

	private static NuclosWebElement getCurrentStateElement() {
		$('#nuclosCurrentState')
	}
}
