package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.isPresent

import org.nuclos.test.webclient.NuclosWebElement

import com.sun.istack.Nullable

import groovy.transform.CompileStatic

@CompileStatic
class TableComponent {

	@Nullable
	NuclosWebElement parent

	TableComponent() {}

	static TableComponent within(NuclosWebElement parent) {
		new TableComponent(parent: parent)
	}

	boolean isShowing() {
		isPresent(parent, "table")
	}

	List<NuclosWebElement> getRowColumnsAt(int index) {
		if (rows.size() >= index) {
			getColumns(rows.get(index))
		} else {
			[]
		}

	}

	List<NuclosWebElement> getRows() {
		if (parent == null) {
			$$("table tr")
		} else {
			parent.$$("table tr")
		}
	}

	List<NuclosWebElement> getColumns(NuclosWebElement row) {
		row.$$("td")
	}

	List<String> getHeaderColumns() {
		List<String> result = []
		if (rows.size() > 0) {
			List<NuclosWebElement> columns = rows.get(0).$$("th")
			for (int i = 0; i < columns.size(); i++) {
				NuclosWebElement element = columns[i]
				String s = element.getAttribute('innerText')
				s = s.replace('\u200B', '')
				s = s.trim()
				result.add(s)
			}
		}

		result
	}
}
