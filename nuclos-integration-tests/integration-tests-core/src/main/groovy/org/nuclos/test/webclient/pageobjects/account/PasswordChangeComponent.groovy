package org.nuclos.test.webclient.pageobjects.account

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic

@CompileStatic
class PasswordChangeComponent extends AbstractPageObject {

	static String getUsername() {
		$('#username').text
	}

	static void setOldPassword(String password) {
		NuclosWebElement element = $('#oldPassword')
		element.clear()
		element.sendKeys(password)
	}

	static void setNewPassword(String password) {
		NuclosWebElement element = $('#newPassword')
		element.clear()
		element.sendKeys(password)
	}

	static void setConfirmNewPassword(String password) {
		NuclosWebElement element = $('#confirmNewPassword')
		element.clear()
		element.sendKeys(password)
	}

	static void submit() {
		$('button[type="submit"]').click()
	}

	static boolean isErrorMessage() {
		!!$('div.alert.alert-danger')
	}

	static boolean isSuccessMessage() {
		!!$('div.alert.alert-success')
	}

	static void clickLoginLink() {
		$('a[href$="/login"]').click()
	}
}
