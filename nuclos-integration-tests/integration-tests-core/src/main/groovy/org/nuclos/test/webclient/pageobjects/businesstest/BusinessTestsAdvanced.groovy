package org.nuclos.test.webclient.pageobjects.businesstest

import static org.nuclos.test.AbstractNuclosTest.waitFor
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import java.util.regex.Matcher

import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic

/**
 * Represents the business test overview page for advanced users ("Expertenmodus").
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class BusinessTestsAdvanced extends AbstractPageObject {
	static void open() {
		getUrlHash('/businesstests/advanced')
	}

	static boolean isStarted() {
		!$('#generateAllTests').enabled
	}

	static void generateAndRun() {
		$('#generateAndRun').click()
		waitForAngularRequestsToFinish(120)
		waitFor(120) {
			!BusinessTestsAdvanced.started
		}
	}

	static void generateAllTests() {
		$('#generateAllTests').click()
		waitForAngularRequestsToFinish(120)
		waitFor(120) {
			!BusinessTestsAdvanced.started
		}
	}

	static void executeAllTests() {
		$('#executeAllTests').click()
		waitForAngularRequestsToFinish(240)
		waitFor(240) {
			!BusinessTestsAdvanced.started
		}
	}

	static void deleteAllTests() {
		$('#deleteAllTests').click()
		waitForAngularRequestsToFinish()
		clickButtonOk()
		waitForAngularRequestsToFinish()
	}

	static void deleteTest(int index) {
		def id = getIdFromTest(getTestsMatcher()[index])
		$('#del_' + id).click()
		waitForAngularRequestsToFinish()
	}

	// static List<NuclosWebElement> getTests() {
	//		$$('tr[id^=test_]').findAll { it.displayed }
	// Regex is muuuuuuuuuuuuch faster than with selectors
	static Matcher getTestsMatcher() {
		waitForAngularRequestsToFinish()
		def html = driver.pageSource
		return (html =~ /(?s)<tr[^>]*id[\s]*=[\s]*"test_[a-zA-Z\d]*".*?<\/tr>/)

	}

	static String getIdFromTest(def test) {
		def idMatcher = test =~ /(?s)<tr[^>]*id[\s]*=[\s]*"(test_[a-zA-Z\d]*)"/
		assert idMatcher.find()
		assert idMatcher.groupCount() > 0
		return idMatcher.group(1)
	}

	static int assertExecutedTests(String log) {
		def testsMatcher = BusinessTestsAdvanced.testsMatcher
		int countTestRows = 0
		List<String> lstTestnames = []
		while (testsMatcher.find()) {
			countTestRows++
			def test = testsMatcher.group()
			def cellsMatcher = test =~ /(?s)<td[^>]*>(.*?)<\/td>/
			int iCell = -1
			while (cellsMatcher.find()) {
				iCell++
				def cell = cellsMatcher.group()
				assert cellsMatcher.groupCount() > 0
				def value = cellsMatcher.group(1)
				// Assert there are no empty table cells now
				assert value
				if (iCell == 0) {
					def valueMatcher = value =~ /(?s)<a[^>]*>(.*?)<\/a>/
					assert valueMatcher.find()
					assert valueMatcher.groupCount() > 0
					String testname = valueMatcher.group(1)
					assert log.contains(testname)
					lstTestnames.add(testname)
				}
			}
			assert iCell > 0
		}
		assert lstTestnames == lstTestnames.sort()
		return countTestRows
	}

	static String getLog() {
		$('#output').text.trim()
	}

	static int getTestsTotal() {
		$('#overview_testsTotal').text.toInteger()
	}

	static String getState() {
		$('.overview-state').text
	}

	static int getTestsGreen() {
		$('#overview_testsGreen').text.toInteger()
	}

	static int getTestsYellow() {
		$('#overview_testsYellow').text.toInteger()
	}

	static int getTestsRed() {
		$('#overview_testsRed').text.toInteger()
	}

	static int getDuration() {
		$('#overview_duration').text.replace(' ms', '').toInteger()
	}
}
