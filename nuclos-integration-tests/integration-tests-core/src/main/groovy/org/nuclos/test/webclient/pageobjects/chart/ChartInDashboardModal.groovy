package org.nuclos.test.webclient.pageobjects.chart

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.Modal
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@CompileStatic
class ChartInDashboardModal extends Modal {
	private static String modalSelector = 'ngb-modal-window'

	static boolean isConfigurationValid2() {
		!$(modalSelector + ' .alert.alert-danger')
	}

	static NuclosWebElement getChartEventRects2() {
		$(modalSelector + ' .c3-event-rects');
	}

	static NuclosWebElement getReferenceElement() {
		$(modalSelector + ' #referenceAttributeId')
	}

	static String getCategoryAttributeId2() {
		new Select($(modalSelector + ' #categoryAttributeId')).firstSelectedOption.text?.trim()
	}

	static void setCategoryAttributeId2(String attributeId) {
		new Select($(modalSelector + ' #categoryAttributeId')).selectByValue(attributeId)
	}

	static String getSeriesAttributeId2() {
		new Select($(modalSelector + ' #seriesAttributeId')).firstSelectedOption.text?.trim()
	}

	static void setSeriesAttributeId2(String attributeId) {
		new Select($(modalSelector + ' #seriesAttributeId')).selectByValue(attributeId)
	}

	static int getSeriesOptionsSize2() {
		new Select($(modalSelector + ' #seriesAttributeId')).getOptions().size()
	}

	static String getChartType2() {
		$(modalSelector + ' .chart-type.active')?.id
	}

	static void setChartType2(final String chartType) {
		$("$modalSelector .chart-type[id=\"$chartType\"]").click()
	}

	static void setChartName2(String name) {
		$(modalSelector + ' #chartName').with {
			it.clear()
			it.sendKeys(name)
		}
	}

	static String getChartTitle() {
		$(modalSelector + ' .fa.fa-chart-simple').parent.text
	}

	static NuclosWebElement getSaveButton() {
		$(modalSelector + ' #dashboardChartConfigSave')
	}

	static NuclosWebElement getCancelButton() {
		$(modalSelector + ' #dashboardChartConfigCancel')
	}

}
