package org.nuclos.test.webclient.pageobjects.chart

import static org.nuclos.test.AbstractNuclosTest.waitFor
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.nuclos.test.webclient.pageobjects.Modal
import org.openqa.selenium.Keys
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class ChartModal extends Modal {
	public static String modalSelector = 'nuc-chart-modal'

	static List<NuclosWebElement> getTabs() {
		$$(modalSelector + ' .chart-tab-title')
	}

	static List<String> getTabTitles() {
		tabs*.text
	}

	static int getChartCount() {
		tabs.size()
	}

	static void newChart() {
		$vVv(modalSelector + ' #newChart').click()
	}

	static int getSelectedChartIndex() {
		$(modalSelector + ' .p-tabview-nav-link')?.getAttribute('tabindex')?.toInteger()
	}

	private static NuclosWebElement getConfigButton() {
		$(modalSelector + ' #configureChart')
	}

	static void toggleConfigure() {
		configButton.click()
	}

	private static NuclosWebElement getSaveButton() {
		$(modalSelector + ' #saveChart')
	}

	static void saveChart() {
		saveButton.click()
	}

	static boolean canSave() {
		saveButton?.displayed
	}

	static void deleteChart() {
		$(modalSelector + ' #deleteChart').click()
		AbstractPageObject.clickButtonOk()
	}

	static void setChartName(String name) {
		clickTab('Optionen')
		$(modalSelector + ' #chartName').with {
			it.clear()
			it.sendKeys(name)
		}
	}

	static String getChartName() {
		clickTab('Optionen')
		$(modalSelector + ' #chartName').getAttribute('value')
	}

	static void setChartOptions(String chartOptions) {
		clickTab('Erweitert')
		$(modalSelector + ' #chartOptions').value = chartOptions
	}

	static NuclosWebElement getChartWebElement() {
		// For displaying multiple charts a dynamic id is necessary
		// $(Modal.modalSelector + ' #chart svg')
		return getChartEventRects()
	}

	static void waitForChartVisible() {
		// A chart element might be visible, but invalid configuration prevents the chart from rendering properly.
		waitFor(30, {
			try {
				getChartWebElement()?.isDisplayed()
			} catch (StaleElementReferenceException e) {
				return false
			}
		})
	}

	static void waitForChartNotVisible() {
		waitFor(3, {
			try {
				def chart = getChartWebElement()
				if (!chart) {
					return true
				} else {
					return !chart.isDisplayed()
				}
			} catch (StaleElementReferenceException e) {
				return false
			}
		})
	}

	private static void clickTab(String tabName) {
		$$(".p-tabview-title").find({ e -> e.text == tabName })?.click()
	}

	static boolean isMultibarChartVisible() {
		$(modalSelector + ' svg .nv-multiBarWithLegend')
	}

	static boolean isConfigurationValid() {
		!$(modalSelector + ' .alert.alert-danger')
	}

	static String getReferenceAttributeId() {
		clickTab('Optionen')
		new Select($(modalSelector + ' #referenceAttributeId')).firstSelectedOption.text?.trim()
	}

	static void setReferenceAttributeId(String attributeId) {
		clickTab('Optionen')
		new Select($(modalSelector + ' #referenceAttributeId')).selectByValue(attributeId)
	}

	static String getCategoryAttributeId() {
		clickTab('Optionen')
		new Select($(modalSelector + ' #categoryAttributeId')).firstSelectedOption.text?.trim()
	}

	static void setCategoryAttributeId(String attributeId) {
		clickTab('DOptionen')
		new Select($(modalSelector + ' #categoryAttributeId')).selectByValue(attributeId)
	}

	static String getChartType() {
		$(modalSelector + ' .chart-type.active')?.id
	}

	static void setChartType(final String chartType) {
		clickTab('Optionen')
		$("$modalSelector .chart-type[id=\"$chartType\"]").click()
	}

	static void newSeries() {
		$(modalSelector + ' #newSeries').click()
	}

	static int getChartSeriesCount() {
		$(modalSelector + ' #chartSeriesCount').value?.toInteger()
	}

	static void removeSeries() {
		$(modalSelector + ' #removeSeries').click()
	}

	static String getSeriesAttributeId() {
		new Select($(modalSelector + ' #seriesAttributeId')).firstSelectedOption.text?.trim()
	}

	static void setSeriesAttributeId(String attributeId) {
		new Select($(modalSelector + ' #seriesAttributeId')).selectByValue(attributeId)
	}

	static int getSeriesOptionsSize() {
		new Select($(modalSelector + ' #seriesAttributeId')).getOptions().size()
	}

	static NuclosWebElement getChartEventRects() {
		$(modalSelector + ' .c3-event-rects');
	}

 	static int getChartDataCount() {
		$$(modalSelector + ' .c3-shape.c3-bar,.c3-circle').size()
	}

	static clickDataEntry(int index) {
		// click needs to be called on c3-event-rect element
		def barElement = $$zZz(' .c3-shape.c3-bar,.c3-circle').get(index).element
		def c3EventRectElement = $zZz(' .c3-event-rect').element

		new org.openqa.selenium.interactions.Actions(AbstractWebclientTest.driver)
				.moveToElement(c3EventRectElement)
				.moveToElement(barElement, 1, 1)
				.click().perform()
	}

	static void setOpenReferenceOnClick(final boolean value) {
		def element = $zZz(modalSelector + " #openReferenceOnClickCbx")
		String checked = element.getAttribute('checked')
		if (checked as boolean != value as boolean) {
			element.click()
		}
	}

	static void setFilter(final int index, final String value) {
		$$(modalSelector + " .chart-filter-item input").get(index).with {
			it.click()
			clearInput()
		}
		sendKeys(value)
		sendKeys(Keys.ENTER)
	}

	static void applyFilter() {
		$(modalSelector + ' #buttonChartSearch').click()
	}

	static boolean isExportAvailable() {
		$(modalSelector + ' #exportChart')?.displayed
	}

	static void exportChart() {
		$(modalSelector + ' #exportChart').click()
	}
}
