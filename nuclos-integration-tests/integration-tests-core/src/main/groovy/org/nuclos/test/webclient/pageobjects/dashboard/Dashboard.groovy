package org.nuclos.test.webclient.pageobjects.dashboard

import static org.nuclos.test.AbstractNuclosTest.failsafeAssert
import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.getDriver

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.nuclos.test.webclient.pageobjects.ListOfValues

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Dashboard extends AbstractPageObject {

	static void open() {
		$('nuc-dashboard-notification-count').click()
	}

	static boolean isActive(String dashboardTitle) {
		int indexOfTitle = tabTitles.findLastIndexOf {
			return (it as String) == dashboardTitle
		}
		if (indexOfTitle < 0) {
			return false
		} else {
			int indexOfActivesLinks = $$('.p-tabview-nav li').findLastIndexOf {
				NuclosWebElement element = it as NuclosWebElement
				return element.getAttribute('class').contains('p-highlight')
			}

			return indexOfTitle == indexOfActivesLinks
		}
	}

	static void select(String dashboardTitle) {

	}

	static void addNew() {
		newButton.click()
	}

	static NuclosWebElement getNewButton() {
		$('#newDashboard')
	}

	static NuclosWebElement getToggleConfig() {
		$('a[name=toggle-config]')
	}

	static NuclosWebElement getStartupDefault() {
		$('#startupDefault')
	}

	static NuclosWebElement getDeleteButton() {
		$('a[name=delete-dashboard]')
	}

	static NuclosWebElement getConfigButton() {
		$('#configButton')
	}

	static boolean isShowing() {
		try {
			getDriver().findElementByTagName('nuc-dashboard')
		} catch (org.openqa.selenium.NoSuchElementException e) {
			return false
		}
		return true
	}

	static boolean isConfigMode() {
		configComponent as boolean
	}

	static NuclosWebElement getConfigComponent() {
		$('nuc-dashboard-config')
	}

	static String getName() {
		nameInput.value
	}

	static void setName(String name) {
		nameInput.value = name
	}

	static NuclosWebElement getNameInput() {
		def nameInput = $('#dashboardName')
		failsafeAssert({ nameInput != null }, "Name Input should be accessible")
		return nameInput
	}

	static List<NuclosWebElement> getTabs() {
		$$('.dashboard-tab-title')
	}

	static List<String> getTabTitles() {
		tabs*.text*.trim()
	}

	static void addDynamicTaskListItem(final String taskListName) {
		dynamicTaskListSelect.selectEntry(taskListName)
	}

	static ListOfValues getDynamicTaskListSelect() {
		ListOfValues.fromElement(null, $('#dynamicTaskLists'))
	}

	static void addSearchfilterTaskListItem(final String taskListName) {
		searchfilterTaskListSelect.selectEntry(taskListName)
	}

	static ListOfValues getSearchfilterTaskListSelect() {
		ListOfValues.fromElement(null, $('#searchfilterTaskLists'))
	}

	static ListOfValues getChartListSelect() {
		ListOfValues.fromElement(null, $('#dynamicChartLists'))
	}

	static void addChartListItem(final String taskListName) {
		chartListSelect.selectEntry(taskListName)
	}

	static ListOfValues getSearchfilterChartListSelect() {
		ListOfValues.fromElement(null, $('#searchfilterChartLists'))
	}

	static void addSearchfilterChartListItem(final String taskListName) {
		searchfilterChartListSelect.selectEntry(taskListName)
	}

	static void addAddonItem(final String addonName) {
		addonSelect.selectEntry(addonName)
	}

	static ListOfValues getAddonSelect() {
		ListOfValues.fromElement(null, $('#addons'))
	}

	static List<DashboardItem> getItems() {
		$$('gridster-item').collect {
			new DashboardItem(it)
		}
	}

	static void configure() {
		if (!isConfigMode()) {
			getToggleConfig().click()
		}
	}

	static void closeConfig() {
		if (isConfigMode()) {
			getToggleConfig().click()
		}
	}

	static List<String> getChartTitles() {
		$$('.fa.fa-chart-simple').parent.text
	}
}
