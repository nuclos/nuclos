package org.nuclos.test.webclient.pageobjects.dashboard

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.openqa.selenium.StaleElementReferenceException


class DashboardAddonConfig extends AbstractPageObject {

	private static String selector = 'nuc-dashboard-addon-config'
	private NuclosWebElement element

	static DashboardAddonConfig get() {
		return new DashboardAddonConfig()
	}

	DashboardAddonConfig() {
		this.element = AbstractWebclientTest.$(selector)
	}

	DashboardAddonConfig(NuclosWebElement element) {
		this.element = element
	}

	void changeProperty(String type, String name, String value) {
		switch(type) {
			case "property-boolean":
				String inputType = element?.$("#${type}-${name}")?.getAttribute('type')
				if (inputType == 'checkbox') {
					String checked = element.getAttribute('checked')
					if (checked as boolean != Boolean.parseBoolean(value)) {
						element?.$("#${type}-${name}")?.click()
					}
				}
				break;
			case "property-number":
			case "property-string":
				element?.$("#${type}-${name}")?.click()
				AbstractWebclientTest.waitForAngularRequestsToFinish()
				AbstractWebclientTest.sendKeys(value)
				break;
			default:
				break;
		}
	}

	boolean isVisible() {
		try {
			element?.displayed
		} catch (StaleElementReferenceException e) {
			false
		}
	}

	void save() {
		element.$('#perspectiveSave').click()
	}

	void cancel() {
		element.$('#perspectiveCancel').click()
	}
}
