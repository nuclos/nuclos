package org.nuclos.test.webclient.pageobjects.dashboard

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class DashboardItem extends AbstractPageObject {

	final NuclosWebElement item
	final String type
	final int x
	final int y
	final Integer rows
	final Integer cols

	DashboardItem(NuclosWebElement item) {
		this.item = item
		this.type = item.getAttribute('item-type')
		this.x = item.getAttribute('item-x') as Integer
		this.y = item.getAttribute('item-y') as Integer
		this.rows = item.getAttribute('item-rows') as Integer
		this.cols = item.getAttribute('item-cols') as Integer
	}

	NuclosWebElement row(int rowIndex) {
		return item.$('nuc-entity-object-grid ag-grid-angular .ag-center-cols-viewport  [row-index="' + rowIndex + '"]')
	}

	int countRows() {
		return item.$$('nuc-entity-object-grid ag-grid-angular .ag-center-cols-viewport [role="row"]').size()
	}

	void clickRow(int rowIndex) {
		row(rowIndex).click()
	}

	void configure() {
		item.$('button[name="show-config"]').click()
	}

	void delete() {
		item.$('a[name="delete-dashboard-item"]').click()
	}

	void deleteWithSlightMovement() {
		def btnDelete = item.$('a[name="delete-dashboard-item"]')

		new Actions(AbstractWebclientTest.driver)
			.clickAndHold(btnDelete.element)
			.moveByOffset(-2, -2)
			.release()
			.perform()
	}

	NuclosWebElement chartEventRect() {
		item.$('nuc-chart-view .c3-event-rects')
	}

	NuclosWebElement chartBars() {
		item.$('nuc-chart-view .c3-chart-bars')
	}

	NuclosWebElement chartLines() {
		item.$('nuc-chart-view .c3-chart-lines')
	}

	NuclosWebElement chartAreas() {
		item.$('nuc-chart-view .c3-chart-lines .c3-areas')
	}

}
