package org.nuclos.test.webclient.pageobjects.dashboard

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement;
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.openqa.selenium.StaleElementReferenceException;

/**
 * Created by Sebastian Debring on 12/1/2020.
 */
public class DashboardTasklistConfig extends AbstractPageObject {

	private static String selector = 'nuc-dashboard-tasklist-config'
	private NuclosWebElement element

	static DashboardTasklistConfig get() {
		return new DashboardTasklistConfig()
	}

	DashboardTasklistConfig() {
		this.element = AbstractWebclientTest.$(selector)
	}

	DashboardTasklistConfig(NuclosWebElement element) {
		this.element = element
	}

	void toggleShowDetailsNewTab() {
		element.$('#property-show-details-new-tab').click()
	}

	boolean isShowDetailsNewTab() {
		element.$('#property-show-details-new-tab:checked:enabled')
	}

	boolean isVisible() {
		try {
			element?.displayed
		} catch (StaleElementReferenceException e) {
			false
		}
	}

	void save() {
		element.$('#dashboardTasklistConfigSave').click()
	}

	void cancel() {
		element.$('#dashboardTasklistConfigCancel').click()
	}
}
