package org.nuclos.test.webclient.pageobjects.perspective

import static org.nuclos.test.AbstractNuclosTest.waitFor
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.openqa.selenium.StaleElementReferenceException

import groovy.transform.CompileStatic
import groovy.transform.Immutable

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Immutable(knownImmutableClasses = [NuclosWebElement])
@CompileStatic
class Perspective extends AbstractPageObject {
	private NuclosWebElement element

	/**
	 * The preference ID for this perspective.
	 */
	String id

	String name

	boolean isSelected() {
		element.hasClass('btn-info')
	}

	/**
	 * Edit this perspective.
	 * Performs a mouse move to the perspective button (to toggle the edit/delete overlay)
	 * and clicks the edit button (which opens the edit modal).
	 *
	 * @return
	 */
	PerspectiveModal edit() {
		$xXx("#editPerspective").click()
		waitForAngularRequestsToFinish()
		waitFor {
			try {
				$vVv('nuc-perspective-edit')?.isDisplayed()
			} catch (StaleElementReferenceException ignored) {}
		}

		new PerspectiveModal()
	}

	void delete() {
		$("#deletePerspective").click()
	}

	void toggle() {
		$("#perspective_$id").click()
	}
}
