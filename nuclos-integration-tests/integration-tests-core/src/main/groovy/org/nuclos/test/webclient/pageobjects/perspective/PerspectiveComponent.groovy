package org.nuclos.test.webclient.pageobjects.perspective

import static org.nuclos.test.AbstractNuclosTest.waitFor
import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.$vVv
import static org.nuclos.test.webclient.AbstractWebclientTest.$xXx
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.openqa.selenium.StaleElementReferenceException

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class PerspectiveComponent {

	static PerspectiveModal newPerspective() {
		$xXx('#newPerspective').click()
		waitForAngularRequestsToFinish()
		waitFor {
			try {
				$vVv('nuc-perspective-edit')?.isDisplayed()
			} catch (StaleElementReferenceException ignored) {}
		}
		return new PerspectiveModal()
	}

	/**
	 * Returns all currently visible perspectives.
	 *
	 * @return
	 */
	static List<Perspective> getPerspectives() {
		$$('nuc-perspective-button button').collect {
			String uid = it.getAttribute('uid')
			new Perspective(
					element: it,
					id: uid,
					name: it.text
			)
		}
	}

	/**
	 * Searches for a perspective with the given name in the current page.
	 *
	 * @param name
	 * @return
	 */
	static Perspective getPerspective(String name) {
		perspectives.find {
			it.name == name
		}
	}
}
