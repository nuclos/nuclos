package org.nuclos.test.webclient.pageobjects.perspective

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class PerspectiveModal extends AbstractPageObject {

	void setName(final String name) {
		$('#perspectiveName').clear()
		$('#perspectiveName').sendKeys(name)
	}

	String getName() {
		$('#perspectiveName').getAttribute('value')
	}

	void selectLayout(String layoutName) {
		getLayoutSelect().selectByVisibleText(layoutName)
	}

	String getLayout() {
		getLayoutSelect().firstSelectedOption.text?.trim()
	}

	void setLayoutForNew(boolean forNew) {
		WebElement checkbox = layoutForNewCheckbox
		if (checkbox.selected != forNew) {
			checkbox.click()
		}
	}

	boolean isLayoutForNew() {
		layoutForNewCheckbox.selected
	}

	void selectSearchtemplate(String searchtemplateName) {
		selectOptionByText(searchtemplateSelect, searchtemplateName)
	}

	String getSearchtemplate() {
		getSearchtemplateSelect().firstSelectedOption.text?.trim()
	}

	void selectSideviewMenu(String sideviewMenuName) {
		selectOptionByText(sideviewMenuSelect, sideviewMenuName)
	}

	void selectSubformTablePreference(String subformEntityFqn, String preferenceName) {
		selectOptionByText(
				getSubformTableSelect(subformEntityFqn),
				preferenceName
		)
	}

	private void selectOptionByText(Select select, String text) {
		int index = select.options.findIndexOf {
			WebElement option -> option.text?.contains(text)
		}
		select.selectByIndex(index)
	}

	String getSideviewMenu() {
		getSideviewMenuSelect().firstSelectedOption.text?.trim()
	}

	void save() {
		$('#perspectiveSave').click()
	}

	/**
	 * Closes this modal dialog without saving.
	 */
	void cancel() {
		$('#perspectiveCancel').click()
	}

	private Select getLayoutSelect() {
		new Select($('#perspectiveLayout'))
	}

	private Select getSearchtemplateSelect() {
		new Select($('#perspectiveSearchtemplate'))
	}

	private Select getSideviewMenuSelect() {
		new Select($('#perspectiveSideviewMenu'))
	}

	private Select getSubformTableSelect(String subformEntityFqn) {
		new Select($('#subformTable_' + subformEntityFqn))
	}

	private WebElement getLayoutForNewCheckbox() {
		$('#perspectiveLayoutForNew')
	}
}