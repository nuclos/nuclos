package org.nuclos.test.webclient.pageobjects.planningtable

import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.nuclos.test.webclient.pageobjects.Datepicker

class PlanningTable extends AbstractPageObject {

	final NuclosWebElement element
	Datepicker fromPicker
	Datepicker untilPicker

	PlanningTable(NuclosWebElement element) {
		this.element = element;
		fromPicker = new Datepicker(this.element.$('#datechooser-From'))
		untilPicker = new Datepicker(this.element.$('#datechooser-Until'))
	}

	/**
	 * Clicks the switch alignment button of the planning-table.component
	 */
	void switchAlignment() {
		this.element.$('.fa-align-justify').click()
	}

	/**
	 * Clicks the refresh button of the planning-table.component
	 */
	void refresh() {
		this.element.$('.fa-sync').click()
	}

	/**
	 * Clicks the fit content button of the planning-table.component
	 */
	void fitContentToView() {
		this.element.$('.fa-arrows-alt-h').click()
	}

	/**
	 * Clicks the export button of the planning-table.component
	 */
	void exportPlanningTable() {
		this.element.$('.fa-print').click()
	}

	/**
	 * Selects the specified timeMode from the time mode dropdown.
	 * @param timeModeName
	 */
	void switchToTimeMode(String timeModeName) {
		waitForAngularRequestsToFinish()
		this.element.$('#nuclosCurrentTimeMode').click()
		waitForAngularRequestsToFinish()
		NuclosWebElement item = this.element.$$('.time-item').find(x -> (x.id == timeModeName))
		item.mouseover()
		item.click()
		waitForAngularRequestsToFinish()
	}

	/**
	 * Clicks the 'Create relation from here' button in the dropdown.
	 */
	void setCreateRelationFrom() {
		this.element.$('.fa-plus').click()
		this.element.$('#set_relation_from_booking').click()
	}

	/**
	 * Opens the dropdown and clicks the correct button, to create the specified relation.
	 * @param relationName - name of the eo which represents the relation.
	 */
	void createRelation(String relationName) {
		this.element.$('.fa-plus').click()
		this.element.$('#selection_create_relation_' + relationName).click()
	}

	/**
	 * Opens the date chooser for the fromDate and selects the specified date relative to the current date.
	 * @param monthsFromCurrent
	 * @param dayOfCurrentMonth
	 */
	void setDateFrom(int monthsFromCurrent, int dayOfCurrentMonth) {
		fromPicker.open()
		for (i in 0..<monthsFromCurrent) {
			if (monthsFromCurrent < 0) {
				fromPicker.previousMonth()
			} else {
				fromPicker.nextMonth()
			}
		}
		fromPicker.clickDayOfCurrentMonth(dayOfCurrentMonth)
	}

	/**
	 * Opens the date chooser for the untilDate and selects the specified date relative to the current date.
	 * @param monthsFromCurrent
	 * @param dayOfCurrentMonth
	 */
	void setDateUntil(int monthsFromCurrent, int dayOfCurrentMonth) {
		untilPicker.open()
		for (i in 0..<monthsFromCurrent) {
			if (monthsFromCurrent < 0) {
				untilPicker.previousMonth()
			} else {
				untilPicker.nextMonth()
			}
		}
		untilPicker.clickDayOfCurrentMonth(dayOfCurrentMonth)
	}

	/**
	 * Selects the filter with the specified name.
	 * @param filterName
	 */
	void setFilter(String filterName) {
		this.element.$('#nuclosCurrentFilter').click()
		List<NuclosWebElement> filterElements = this.element.$$('.dropdown-item')
		for (NuclosWebElement filterElement : filterElements) {
			if (filterName == filterElement.$('span').text) {
				filterElement.click()
				break
			}
		}
	}

	/**
	 * Clicks the 'show own bookings only' checkbox.
	 */
	void toggleShowOwnBookingsOnly() {
		this.element.$('.p-checkbox-box').click()
	}

	/**
	 * Checks if the 'show own bookings only' checkbox is checked.
	 * @return
	 */
	boolean isShowOwnBookingsOnly() {
		return this.element.$('.pi-check') != null
	}

	/**
	 * Checks if the 'show own bookings only' checkbox is visible.
	 * The show own bookings only checkbox should only be visible, if there are bookings from multiple users.
	 * @return true, if the checkbox is visible.
	 */
	boolean isShowOwnBookingsCheckboxVisible() {
		return this.element.$('.p-checkbox').isDisplayed()
	}

}
