package org.nuclos.test.webclient.pageobjects.planningtable

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.getUrlHash

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic

@CompileStatic
class PlanningTableComponent extends AbstractPageObject {

	static <PK> PlanningTableComponent open(PK entityObjectId) {
		String url = "/planningtable/$entityObjectId"
		getUrlHash(url)

		return forDetail()
	}

	static PlanningTableComponent forDetail() {
		PlanningTableComponent result = new PlanningTableComponent()
		assert result != null
		return result
	}

	PlanningTable getPlanningTable() {
		NuclosWebElement planningTableElement = $('.main-container')

		if (!planningTableElement) {
			return null
		}

		return new PlanningTable(planningTableElement)
	}

}
