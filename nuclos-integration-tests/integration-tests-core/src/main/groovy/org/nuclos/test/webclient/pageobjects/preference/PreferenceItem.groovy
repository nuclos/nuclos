package org.nuclos.test.webclient.pageobjects.preference

import static org.nuclos.test.AbstractNuclosTest.waitFor
import static org.nuclos.test.AbstractNuclosTest.waitForNotNull
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.AbstractWebclientTest
import org.openqa.selenium.By
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic
import groovy.transform.Immutable

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Immutable(knownImmutableClasses = [WebElement])
@CompileStatic
class PreferenceItem {
	private WebElement element
	String name
	PreferenceType type
	boolean shared
	boolean customized
	String businessObject
	int rowIndex

	/**
	 * Selects this {@link PreferenceItem}.
	 * This must happen before it can be shared with users groups.
	 */
	void select() {
		element.click()
		waitForAngularRequestsToFinish()
	}

	/**
	 * Searches for a PreferenceGroup with the given name and shares
	 * this {@link PreferenceItem} with it.
	 * This only works correctly if the item is selected.
	 *
	 * @param groupName
	 */
	void shareWith(String groupName) {
		PreferenceGroup group = groups.find { it.name == groupName }
		waitFor {
			group = groups.find { it.name == groupName }
			return group != null
		}
		if (!group.selected) {
			group.toggle()
			waitForAngularRequestsToFinish()
		}
	}

	void unshareFrom(String groupName) {
		PreferenceGroup group = groups.find { it.name == groupName }
		waitFor {
			group = groups.find { it.name == groupName }
			return group != null
		}
		if (group.selected) {
			group.toggle()
			waitForAngularRequestsToFinish()
		}
	}

	void publishChanges() {
		getShareIcon(rowIndex).click()
		waitForAngularRequestsToFinish()
	}

	void discardChanges() {
		getDiscardIcon(rowIndex).click()
		waitForAngularRequestsToFinish()
		AbstractWebclientTest.getMessageModal().confirm()
	}

	void resetLayoutAssignment() {
		//$('#reset-layout-assignment').click()
		waitForNotNull(10, {$('#reset-layout-assignment')}).click()
		waitForAngularRequestsToFinish()
	}

	void delete() {
		$('#collapseDelete button').click()
		waitForAngularRequestsToFinish()
		AbstractWebclientTest.getMessageModal().confirm()
	}

	void doNotdelete() {
		//$('#collapseDelete button').click()
		waitForNotNull(10, {$('#collapseDelete button')}).click()
		waitForAngularRequestsToFinish()
		AbstractWebclientTest.getMessageModal().decline()
	}

	static WebElement getShareIcon(rowIndex) {
		def items = $('.ag-pinned-right-cols-container .ag-row:nth-child(' + rowIndex + ')')
				.findElements(By.cssSelector('.update-preference-share'))
		items.size() == 1 ? items[0] : null
	}

	static WebElement getDiscardIcon(rowIndex) {
		def items = $('.ag-pinned-right-cols-container .ag-row:nth-child(' + rowIndex + ')')
				.findElements(By.cssSelector('.delete-customized-preference-item'))
		items.size() == 1 ? items[0] : null
	}


	/**
	 * Fetches all available user groups with which this item can be shared.
	 * This only works after the item is selected.
	 *
	 * @return
	 */
	List<PreferenceGroup> getGroups() {
		retryOnStaleElement {
			$$('.list-group .list-group-item').collect {
				String name = it.text.trim()
				boolean selected = !!it.findElement(By.cssSelector('input')).getAttribute('checked')

				new PreferenceGroup(
						element: it,
						name: name,
						selected: selected
				)
			}
		}
	}

	/**
	 * Tries to re-fetch the row element once, in case it became stale.
	 * Happens when subform rows are re-rendered.
	 *
	 * @param c
	 * @return
	 */
	private <T> T retryOnStaleElement(Closure<T> c) {
		try {
			return c()
		} catch (StaleElementReferenceException e) {
			getGroups()
		}

		return c()
	}
}
