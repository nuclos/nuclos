package org.nuclos.test.webclient.pageobjects.search

import static org.nuclos.test.webclient.AbstractWebclientTest.*
import static org.nuclos.test.webclient.AbstractWebclientTest.driver

import java.util.stream.Collectors

import org.nuclos.test.EntityClass
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.*
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Searchbar {

	static void clear() {
		boolean searchEditorWasClosedBefore = false
		NuclosWebElement clearSearchFilter = getClearSearchFilter()
		if (clearSearchFilter == null) {
			searchEditorWasClosedBefore = true
			openSearchEditor()
			clearSearchFilter = getClearSearchFilter()
		}
		if (clearSearchFilter != null) {
			clearSearchFilter.click()
			if (searchEditorWasClosedBefore) {
				closeSearchEditor()
			}
		}
	}

	private static NuclosWebElement getClearSearchFilter() {
		return $('#searchfilter-clear');
	}

	static void saveAsSearchfilter() {
		$('#meta-edit').click()
		$('#meta-editor-make-temp-persistent').click()
	}

	static void editName(String name) {
		$('#meta-edit').click()
		$('#meta-editor-name-input').value = name
	}

	static void setFavorite(String icon, Integer position) {
		$('#meta-edit').click()
		$('#meta-editor-show-as-favorite').click()
		if (position) {
			$('#meta-editor-position-input').click()
			sendKeys(position + '')
		}
		if (icon) {
			$vVv('.icon-picker').click()
			$('#icon_' + icon).scrollIntoView()
			$('#icon_' + icon).click()
			waitFor { !$('nuc-icon-picker-modal')?.isDisplayed() }
			waitForAngularRequestsToFinish()
		}
	}

	static void selectFavorite(String name) {
		$('.searchfilter-favorite-button-' +
		name.toLowerCase().replaceAll('[^a-z0-9-_]', '-')).click();
	}

	static void done() {
		$('#done-button').click()
	}

	static void edit() {
		$('#meta-edit').click()
	}

	static void save() {
		$('#meta-editor-save-changes').click()
	}

	static void undo() {
		$('#meta-editor-undo-changes').click()
	}

	static void resetCustomization() {
		$('#meta-editor-reset-customization').click()
	}

	static void delete() {
		$('#meta-edit').click()
		$('#meta-editor-delete-searchfilter').click()
		EntityObjectComponent.clickButtonOk()
	}

	static void refresh() {
		$('#search-execution').click()
	}

	static void expertMode() {
		$('#meta-edit').click()
		$('#meta-editor-expert-mode').click()
	}

	static void expertModeReset() {
		$('#meta-edit').click()
		$('#meta-editor-expert-reset').click()
		EntityObjectComponent.clickButtonOk()
	}

	static String getExpertWhereCause() {
		return $('#search-expert-where-cause').value
	}

	static void setExpertWhereCause(String cause) {
		$('#search-expert-where-cause').value = cause
	}

	static boolean isDeleteAllowed() {
		NuclosWebElement deleteButton = $('#meta-editor-delete-searchfilter')
		return deleteButton && deleteButton.isDisplayed() && deleteButton.isEnabled()
	}

	static String getSelectedFilter() {
		searchfilterSelector.selectedEntry
	}

	static void selectSearchfilter(String name, boolean dirty = false) {
		searchfilterSelector.selectEntry(name, dirty)
		// Searcheditor is closed after selection, reopen it
		openSearchEditor()
	}

	static void selectAttribute(String attribute) {
		attributeLov.selectEntry(attribute)
	}

	static List<String> getFilters() {
		return searchfilterSelector.entries;
	}

	static List<String> getFavorites() {
		return $$('.searchfilter-favorite-button').stream().map { item ->
			def icon = ((NuclosWebElement)item).findElement(By.className('material-icons'))
			def iconName = icon ? icon.getText() : ''
			return ((NuclosWebElement)item).getText().replace(iconName, '').trim()
		}.collect(Collectors.toList())
	}

	/**
	 * TODO: Select the corresponding automatically when setting a search condition.
	 */
	static def setCondition(final EntityClass<?> entityClass, final Searchtemplate.SearchTemplateItem item) {
		Searchtemplate.setSearchCondition(entityClass.fqn + '_' + item.name, item)
	}

	static toggleItemSelection(final EntityClass<?> entityClass, final String itemName) {
		def fqn = entityClass.fqn + '_' + itemName
		$zZz('#attribute-label-' + fqn).mouseover()
		$xXx('#search-item-checkbox-' + fqn).click()
		// driver.findElement(By.id('search-item-checkbox-' + entityClass.fqn + '_' + itemName)).click()
		waitForAngularRequestsToFinish()
	}

	static SearchfilterDropdown getSearchfilterSelector() {
		openSearchEditor()
		new SearchfilterDropdown()
	}

	static openSearchEditor() {
		if ($('#search-editor') == null) {
			$('.search-editor-button').click()
		}
	}

	static NuclosWebElement searchEditorContainer() {
		$('#search-editor-container')
	}

	static closeSearchEditor() {
		if ($('#search-editor') != null) {
			$('.search-editor-button').click()
		}
	}

	static boolean isPopOverSearchEditor() {
		$('#search-editor-popover')?.findElements(By.xpath('*'))?.size() > 0
	}

	static toggleSearchEditorPinned() {
		$('#search-editor-fixed-button')?.click()
	}

	static void resizeSearchEditor(int newHeight) {
		NuclosWebElement e = $('#search-editor-resize-handle')
		int y = e.getParent().getLocation().getY()

		new Actions(AbstractWebclientTest.getDriver())
				.dragAndDropBy( e.element, 0, newHeight - y - e.getParent().getSize().getHeight()) // 1 px for rounding errors
				.build()
				.perform()

		AbstractWebclientTest.waitFor {
			$('#search-editor-resize-handle').getParent().getLocation().getY() == newHeight
		}

		// wait for ag-grid in sidebar to get ready
		sleep(500)
	}

	static ListOfValues getAttributeLov() {
		ListOfValues.fromElement(
				null,
				$xXx('#nuc-search-attribute-selector-dropdown[field="name"]')
		)
	}

	static ListOfValues getSubformLov() {
		ListOfValues.fromElement(
				null,
				$xXx('#nuc-search-attribute-selector-subform-dropdown[field="name"]')
		)
	}

	static void removeAttribute(final String fqn) {
		openSearchEditor()
		$('#attribute-label-' + fqn).mouseover()
		$('#search-remove-' + fqn).click()
	}

	static WebElement getSearchfilter() {
		$('#text-search-input')
	}

	static void search(String searchString) {
		clearTextSearchfilter()
		searchfilter.sendKeys(searchString)
		searchfilter.sendKeys(Keys.ENTER)

		waitForAngularRequestsToFinish()
	}

	static void clearTextSearchfilter() {
		if ($('.clear-searchfilter') != null) {
			$('.clear-searchfilter').click()
		}
	}

	static ListOfValues getValueLov(final EntityClass<?> entityClass, final String attributeName) {
		def attributeFqn = entityClass.fqn + '_' + attributeName
		//NuclosWebElement element = new NuclosWebElement(driver.findElement(By.id('search-value-lov-' + attributeFqn)))
		ListOfValues.fromElement(attributeFqn, $("#search-value-lov-$attributeFqn"))
	}

	static MultiselectionCombobox getValueMultiselect(final EntityClass<?> entityClass, final String attributeName) {
		def attributeFqn = entityClass.fqn + '_' + attributeName
		MultiselectionCombobox.fromElement(attributeFqn, $("#search-value-lov-$attributeFqn p-multiselect"))
	}

	static Datepicker getValueDatepicker(final EntityClass<?> entityClass, final String attributeName) {
		def attributeFqn = entityClass.fqn + '_' + attributeName
		//NuclosWebElement element = new NuclosWebElement(driver.findElement(By.id('search-value-datepicker-' + attributeFqn)))
		new Datepicker($("#search-value-datepicker-$attributeFqn"))
	}

	static BooleanSearchComponent getValueBoolean(final EntityClass<?> entityClass, final String attributeName) {
		def attributeFqn = entityClass.fqn + '_' + attributeName
		//NuclosWebElement element = new NuclosWebElement(driver.findElement(By.id('search-value-boolean-' + attributeFqn)))
		new BooleanSearchComponent($("#search-value-boolean-$attributeFqn"))
	}

	static StringSearchComponent getValueString(final EntityClass<?> entityClass, final String attributeName) {
		def attributeFqn = entityClass.fqn + '_' + attributeName
		//NuclosWebElement element = new NuclosWebElement(driver.findElement(By.id('search-value-string-' + attributeFqn)))
		new StringSearchComponent($("#search-value-string-$attributeFqn"))
	}

	static void enableSubformSearch() {
		//NuclosWebElement element = new NuclosWebElement(driver.findElement(By.id('nuc-search-enable-subform-button')))
		//element.click()
		$xXx("#nuc-search-enable-subform-button").click()
	}

	static String getActiveTaskList() {
		def label = $('#task-list-label')
		if (label) {
			return label.text.substring(label.text.indexOf(':') + 2)
		}
		return null
	}

	static NuclosWebElement getActiveTaskListRemoveButton() {
		waitForNotNull(10, {
			$('#task-list-label').mouseover()
			return $('#remove-task-list-filter')
		})
	}
}
