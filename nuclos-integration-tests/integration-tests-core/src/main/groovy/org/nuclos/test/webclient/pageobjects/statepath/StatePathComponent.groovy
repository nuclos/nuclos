package org.nuclos.test.webclient.pageobjects.statepath

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.OutputType

import groovy.transform.CompileStatic

@CompileStatic
class StatePathComponent {
	private NuclosWebElement graphElement

	StatePathComponent(String selector = 'nuc-graph') {
		graphElement = $(selector)
	}

	int getElementsToShow() {
		Integer.parseInt(graphElement.getAttribute('elements-to-show'))
	}

	List<StatePathStepComponent> getStepElements() {
		graphElement.$$('div.nuc-progress-step').collect { new StatePathStepComponent(it)}
	}

	List<Integer> getStepNumerals() {
		graphElement.$$('div.numeral_text').collect{Integer.parseInt(it.getText())}
	}

	float getCurrentProgressValue() {
		Float.parseFloat(graphElement.$('p-progressbar > div').getAttribute('aria-valuenow'))
	}

	Boolean isVisible() {
		graphElement == null ? false : graphElement.isDisplayed()
	}

	byte[] screenShot() {
		graphElement.getScreenshotAs(OutputType.BYTES)
	}
}
