package org.nuclos.test.webclient.pageobjects.statepath

import static org.nuclos.test.AbstractNuclosTest.waitForNotNull
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.Dimension
import org.openqa.selenium.JavascriptExecutor

import groovy.transform.CompileStatic

@CompileStatic
class StatePathStepComponent {
	private NuclosWebElement stepElement;

	StatePathStepComponent(NuclosWebElement element) {
		stepElement = element
	}

	String getStateName() {
		stepElement.getText()
	}

	String getStateId() {
		stepElement.getId()
	}

	Boolean isEnabled() {
		stepElement.hasClass('is-enabled')
	}

	Boolean isComplete() {
		stepElement.hasClass('is-complete')
	}

	Boolean isActive() {
		stepElement.hasClass('is-active')
	}

	Boolean hasOutsidePath() {
		outsidePathButton() != null
	}

	String getBackgroundColor() {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		String script = "return window.getComputedStyle(document.querySelector('#" + getStateId() + "'),':before')['background-color']";
		(String) js.executeScript(script);
	}

	String getFontColor() {
		stepElement.$('div.state-name').getCssValue('color')
	}

	Dimension getSize() {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		String script = "return window.getComputedStyle(document.querySelector('#" + getStateId() + "'),':before').width";
		String widthStr = (String) js.executeScript(script);
		script = "return window.getComputedStyle(document.querySelector('#" + getStateId() + "'),':before').height";
		String heightStr = (String) js.executeScript(script);

		new Dimension(
				Integer.parseInt(widthStr.replace("px", "")),
				Integer.parseInt(heightStr.replace("px", ""))
		)
	}

	NuclosWebElement outsidePathButton() {
		stepElement.$('button.outsidepathBtn')
	}

	List<NuclosWebElement> outsideStates() {
		outsidePathButton()?.click()
		waitForNotNull(10, {
			$('div.p-overlaypanel')
		})
		$$('div.nextStatesButtonsList > nuc-next-state')
	}

	boolean hasOutsidePathElement(String stateName) {
		return outsideStates().find(it -> it.$('button[state-name$="' + stateName + '"]')) != null
	}

	void clickOutSidePathElement(String stateName) {
		outsideStates().find(it -> it.$('button[state-name$="' + stateName + '"]')).click()
	}

	void hover() {
		stepElement.getParent().$('#' + getStateId() + 'Numeral').mouseover()
		waitForAngularRequestsToFinish()
	}

	void click() {
		stepElement.getParent().$('#' + getStateId() + 'Numeral').click()
		waitForAngularRequestsToFinish()
	}
}
