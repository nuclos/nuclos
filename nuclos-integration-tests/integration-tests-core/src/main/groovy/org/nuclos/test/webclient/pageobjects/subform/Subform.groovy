package org.nuclos.test.webclient.pageobjects.subform

import static org.nuclos.test.AbstractNuclosTest.*
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import java.util.stream.Collectors

import org.nuclos.test.EntityClass
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@CompileStatic
class Subform extends AbstractPageObject {
	String refAttrFqn
	NuclosWebElement subformElement

	protected final static String CHECKBOX_CHECKED_SELECTOR = '.fa-check-square-o'

	SubformRow newRow() {
		List<String> oldIdList = subformElement.$$('.ag-body-viewport .ag-pinned-left-cols-container .ag-row')
				.collect {x -> x.getAttribute('row-id')}
		subformElement.$('.new-subbo').click()
		/* NUCLOS-9449 it isn't guaranteed anymore that new entries are inserted on top (at index = 0). */
		List<String> currentIdList = subformElement.$$('.ag-body-viewport .ag-pinned-left-cols-container .ag-row')
				.collect {x -> x.getAttribute('row-id')}
		/* The only way to find the new row index is to compare the list before and after the insertion
		   in order to find the id, that was not present before. */
		String rowId = currentIdList.find {x -> !oldIdList.contains(x) }
		if (rowId == -1) {
			rowId = 0;
		}
		int rowIndex = subformElement.$('.ag-row[row-id="' + rowId + '"]').getAttribute('row-index') as Integer
		new SubformRow(this, rowIndex)
	}

	boolean isNewVisible() {
		subformElement?.$('.new-subbo')?.displayed
	}

	boolean isNewEnabled() {
		subformElement?.$('.new-subbo')?.enabled
	}

	boolean isDeleteVisible() {
		subformElement?.$('.delete-selected-subbos')?.displayed
	}

	boolean isDeleteEnabled() {
		subformElement?.$('.delete-selected-subbos')?.enabled
	}

	void deleteSelectedRows() {
		subformElement.$('.delete-selected-subbos').click()
		waitForAngularRequestsToFinish()
	}

	void toggleMoreActionsPopup() {
		subformElement?.$('.open-more-actions-popup')?.click()
	}

	boolean isMoreActionsButtonVisible() {
		subformElement?.$('.open-more-actions-popup')?.displayed
	}

	boolean isMoreActionsPopupVisible() {
		subformElement?.$('.more-actions-popup')?.displayed
	}

	boolean isCopyVisible() {
		subformElement?.$('.copy-selected-subbos')?.displayed
	}

	boolean isCopyEnabled() {
		subformElement?.$('.copy-selected-subbos')?.enabled
	}

	boolean isPasteVisible() {
		subformElement?.$('.paste-selected-subbos')?.displayed
	}

	boolean isPasteEnabled() {
		subformElement?.$('.paste-selected-subbos')?.enabled
	}

	boolean isCloneVisible() {
		subformElement?.$('.clone-selected-subbos')?.displayed
	}

	boolean isCloneEnabled() {
		subformElement?.$('.clone-selected-subbos')?.enabled
	}

	void cloneSelectedRows() {
		$(subformElement, '.clone-selected-subbos').click()
	}

	void copySelectedRows() {
		$(subformElement, '.copy-selected-subbos').click()
	}

	void pasteSelectedRows() {
		$(subformElement, '.paste-selected-subbos').click()
	}

	boolean isViewPreferencesVisible() {
		subformElement?.$('nuc-view-preferences-config')?.displayed
	}

	NuclosWebElement getSubformButtonsElement() {
		subformElement?.$('nuc-subform-buttons')
	}

	NuclosWebElement getGrid() {
		subformElement?.$('ag-grid-angular')
	}

	void toggleFilterForHeader(String header) {
		def headerElement = $('.ag-header-container .ag-header-row [col-id$="' + header + '"] .ag-header-cell-menu-button')
		headerElement.scrollIntoView()
		headerElement.click()
	}

	void toggleFilter() {
		subformElement.$('.filter-subbos').click()
	}

	SubformFavoriteFiltersHeader getFavoriteFiltersHeader() {
		NuclosWebElement favoriteFilterHeaderElement = subformElement.$('nuc-filter-favorite-header-group')
		if (favoriteFilterHeaderElement) {
			new SubformFavoriteFiltersHeader(favoriteFilterHeaderElement)
		}
	}

	SubformFilterEditor toggleFilterEditor() {
		subformElement.$('.fa-search').click()
		NuclosWebElement filterEditorElement = subformElement.$('nuc-filter-editor-overlay')
		if (filterEditorElement) {
			new SubformFilterEditor(filterEditorElement)
		}
	}

	/**
	 * Counts only rendered rows!
	 * There might be more (virtual) rows that are not currently renderered by the grid component
	 * because they would not be visible.
	 */
	int getRowCount() {
		subformElement.$$('.ag-body-viewport .ag-pinned-left-cols-container .ag-row').size()
	}

	int getDeletedRowCount() {
		subformElement.$$('.ag-body-viewport .ag-pinned-left-cols-container .ag-row .row-deleted[col-id="rowSelection"]').size()
	}

	int getNewRowCount() {
		subformElement.$$('.ag-body-viewport .ag-pinned-left-cols-container .ag-row .row-new[col-id="rowSelection"]').size()
	}

	int getSelectedRowCount() {
		subformElement.$$('.ag-body-viewport .ag-pinned-left-cols-container .ag-row.ag-row-selected').size()
	}

	SubformRow getRow(int index, boolean bThrowFailures = true) {
		new SubformRow(this, index, bThrowFailures)
	}

	void scrollToRow(int rowIndex) {
		executeScript("arguments[0].scrollIntoView();", $$(subformElement, '.ui-grid-row')[rowIndex])
	}

	void scrollToColumn(String column) {
		subformElement.$('.ag-header-container .ag-header-cell[col-id$="' + column + '"]').scrollIntoView()
	}

	void toggleSelection(int index) {
		// the HTML elements are reorder in DOM depending on sorting position of the row - so use row attribute
		subformElement.$$('.ag-pinned-left-cols-container [row-index="' + index + '"] [col-id]')
				.stream()
				.min({ webElement1, webElement2 -> webElement1.getAttribute('col-id').compareTo(webElement1.getAttribute('col-id')) })
				.get()
				.click()
	}

	NuclosWebElement getEditInModalLink(int index) {
		getEditCell(index).$('a.edit-in-modal-link')
	}

	NuclosWebElement getReferenceLink(int index) {
		$('.ag-row[row-index="' + index + '"] nuc-subform-reference a.edit-reference')
	}

	NuclosWebElement getEditCell(int index) {
		subformElement.$('.ag-row[row-index="' + index + '"] nuc-subform-edit-row-renderer')
	}

	void editInModal(int index) {
		getEditInModalLink(index).click()
	}

	NuclosWebElement getEditInWindowLink(int index) {
		getEditCell(index).$('a.edit-in-window-link')
	}

	String getEditColumnPosition() {
		subformElement.$$('.ag-header .ag-pinned-left-header .ag-header-row [col-id]').size() == 2
				? 'left'
				: subformElement.$('.ag-header .ag-pinned-right-header .ag-header-row [col-id]') ? 'right' : null
	}

	List<String> getPinnedAttributeColumns(EntityClass entity) {
		subformElement.$$('.ag-header .ag-pinned-left-header .ag-header-row [col-id]').stream()
			.filter {
				it.getAttribute('col-id').startsWith(entity.fqn)
			}
			.map {
				return it.getAttribute('col-id')
			}.collect(Collectors.toList())
	}

	void editInWindow(int index) {
		getEditInWindowLink(index).click()
	}

	void selectAllRows() {
		NuclosWebElement selectAll = selectAllElement

		selectAll.click()
		waitForAngularRequestsToFinish()

		if (selectedRowCount < 1) {
			selectAll.click()
			waitForAngularRequestsToFinish()
		}
	}

	void unselectAllRows() {
		NuclosWebElement selectAll = selectAllElement

		selectAll.click()
		waitForAngularRequestsToFinish()

		if (selectedRowCount > 0) {
			selectAll.click()
			waitForAngularRequestsToFinish()
		}
	}

	NuclosWebElement getSelectAllElement() {
		subformElement.$('.ag-header-select-all')
	}

	NuclosWebElement getHeaderElementByAttributeName(String name) {
		getHeaderElementOrScrollHorizontally(name)
	}

	private List<NuclosWebElement> getHeaderElements() {
		subformElement.$$('.ag-header-container .ag-header-cell')
	}

	private NuclosWebElement getHeaderElementOrNull(String name) {
		subformElement.$('.ag-header-container .ag-header-cell[col-id$="' + name + '"]')
	}

	private NuclosWebElement getHeaderElementOrScrollHorizontally(String name) {
		NuclosWebElement element = getHeaderElementOrNull(name)
		if (element && element.isDisplayed()) {
			return element
		}

		NuclosWebElement view = viewPort

		if (view.canScrollLeft()) {
			view.scrollToLeftEnd()
			element = getHeaderElementOrNull(name)
		}

		while (!element && view.canScrollRight()) {
			view.scrollRight()
			sleep(100)
			element = getHeaderElementOrNull(name)
		}

		return element
	}

	NuclosWebElement getViewPort() {
		subformElement.$('.ag-center-cols-viewport')
	}

	void openViewConfiguration() {
		subformElement.$zZz('.view-preferences-button').click()
		if (waitForNotNull(10, {$vVv('#headingColumnConfig')}, false) == null) {
			// not opened -> selenium bug?
			subformElement.$zZz('.view-preferences-button').click()
			$vVv('#headingColumnConfig')
		}
	}

	/**
	 * get visible column headers
	 * TODO scroll to get all column headers
	 */
	List<String> getColumnHeaders() {
		headerElements*.text*.trim()
	}

	List<String> getColumnHeaderTooltips() {
		headerElements*.getTooltip()
	}

	void clickColumnHeader(String attributeName) {
		getHeaderElementByAttributeName(attributeName)
				.$('.ag-header-cell-text')
				.click()
	}

	void resizeColumn(String attributeName, int width) {
		NuclosWebElement resizeHandle = getColumnResizeHandle(attributeName)

		new Actions(getDriver())
				.dragAndDropBy(resizeHandle.element, width - resizeHandle.getParent().getSize().getWidth(), 0)
				.build()
				.perform()

		waitFor {
			getColumnResizeHandle(attributeName).getParent().getSize().getWidth() == width
		}
	}

	private NuclosWebElement getColumnResizeHandle(String attributeName) {
		return getHeaderElementByAttributeName(attributeName).$('div[ref="eResize"]')
	}

	List<String> getColumnValues(String attributeName) {
		getColumnValues(attributeName, String.class)
	}

	def <T> List<T> getColumnValues(String attributeName, Class<T> clazz, boolean bThrowFailures = true) {
		return waitForNotNull(30, {
			return getColumnValuesUnsafe(attributeName, clazz, bThrowFailures)
		}, bThrowFailures,
				IllegalArgumentException.class // Could not find subform row or column for attribute ...
		)
	}

	static <T> List<T> getColumnValuesRetryOnStaleElement(EntityObjectComponent eo,
	                                                      EntityClass<?> subformEntityClass,
	                                                      String subformReferenceAttributeName,
	                                                      String attributeName,
	                                                      Class<T> clazz,
	                                                      int timeout = 30) {
		return waitForNotNull(timeout, { ->
			Subform subform = eo.getSubform(subformEntityClass, subformReferenceAttributeName)
			return subform.getColumnValues(attributeName, clazz, false)
		}, true,
				NullPointerException.class,
				StaleElementReferenceException.class
		)
	}

	private def <T> List<T> getColumnValuesUnsafe(String attributeName, Class<T> clazz, boolean bThrowFailures = true) {
		(0..rowCount - 1).collect {
			def row = getRow(it, bThrowFailures)
			if (row) {
				row.getValue(attributeName, clazz)
			} else {
				throw new IllegalStateException('Row ' + it + ' not accessible!')
			}
		}
	}

	NuclosWebElement getPopupEditor(NuclosWebElement cell) {
		String compId = cell.getAttribute('comp-id')
		subformElement.$(
				'.ag-popup-editor [for-cell="' + compId + '"] input,' +
						'.ag-popup-editor  [for-cell="' + compId + '"] textarea'
		)
	}

	List getSortModel() {
		List<Map> sortModel = []

		headerElements.each { header ->
			NuclosWebElement sortASCIndicator = header.$('.ag-sort-ascending-icon')
			NuclosWebElement sortDESCIndicator = header.$('.ag-sort-descending-icon')
			String sort = ''
			int prio = 0
			String colId = header.getAttribute('col-id')

			if (sortASCIndicator?.displayed) {
				sort = 'asc'
			} else if (sortDESCIndicator?.displayed) {
				sort = 'desc'
			}

			NuclosWebElement indicator = header.$('.ag-sort-order')
			if (indicator?.displayed) {
				prio = indicator.text.trim().toInteger()
			}

			if (sort != '') {
				sortModel << [
						colId   : colId,
						sort    : sort,
						priority: prio
				]
			}
		}

		sortModel.sort { it.priority }

		return sortModel
	}

	String getOverlayText() {
		subformElement.$('.ag-overlay-panel')?.text
	}

	boolean isMoveUpEnabled() {
		moveUpButton?.enabled
	}

	NuclosWebElement getMoveUpButton() {
		waitForAngularRequestsToFinish()
		subformElement.$('.autonumber-move-up')
	}

	void moveSelectedRowsUp() {
		moveUpButton.click()
	}

	boolean isMoveDownEnabled() {
		moveDownButton?.enabled
	}

	NuclosWebElement getMoveDownButton() {
		waitForAngularRequestsToFinish()
		subformElement.$('.autonumber-move-down')
	}

	void moveSelectedRowsDown() {
		moveDownButton.click()
	}

}
