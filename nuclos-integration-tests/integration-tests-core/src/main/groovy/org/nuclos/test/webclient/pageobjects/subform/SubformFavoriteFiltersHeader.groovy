package org.nuclos.test.webclient.pageobjects.subform

import org.nuclos.test.webclient.NuclosWebElement

class SubformFavoriteFiltersHeader {

	NuclosWebElement favoriteFilterRow

	SubformFavoriteFiltersHeader(NuclosWebElement favoriteFilterRow) {
		this.favoriteFilterRow = favoriteFilterRow
	}

	void selectFilter(String favoriteFilterName) {
		favoriteFilterRow.$$(".searchfilter-favorite-button")
				.find(webElement -> webElement.text.replace("\nListe filtern", "").trim() == favoriteFilterName)
				.click()
	}

	List<String> getAllFavoriteFilters() {
		favoriteFilterRow.$$(".searchfilter-favorite-button")
				.collect {it.text.replace("\nListe filtern", "")}
	}

	boolean containsFilter(String filtername) {
		getAllFavoriteFilters().contains(filtername)
	}

}
