package org.nuclos.test.webclient.pageobjects.subform

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.waitFor
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish
import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.WebElement

class SubformFilterEditor {

	NuclosWebElement _editorElement

	SubformFilterEditor(NuclosWebElement editorOverlay) {
		_editorElement = editorOverlay
	}

	void toggleFilterAsFavorite() {
		_editorElement.$('#meta-editor-show-as-favorite').click()
	}

	void changeFavoriteIcon(String icon) {
		if (icon) {
			_editorElement.$vVv('.icon-picker').click()
			$('#icon_' + icon).scrollIntoView()
			$('#icon_' + icon).click()
			waitFor { !$('nuc-icon-picker-modal')?.isDisplayed() }
			waitForAngularRequestsToFinish()
		}
	}

	boolean isFavorite() {
		return _editorElement.$('#meta-editor-show-as-favorite').isSelected()
	}

	String getFilterIcon() {
		if (_editorElement.$('.icon-picker')) {
			return _editorElement.$('.icon-picker').text
		}
		return null
	}

	String getFilterPosition() {
		NuclosWebElement nameInput = _editorElement.$('#meta-editor-position-input')
		return nameInput.text
	}

	void changeFavoritePosition(int position) {
		NuclosWebElement nameInput = _editorElement.$('#meta-editor-position-input')
		WebElement inputElement = nameInput.getElement()
		inputElement.click()
		inputElement.clear()
		inputElement.sendKeys("" + position)
	}

	void enterFilterName(String filtername) {
		NuclosWebElement nameInput = _editorElement.$('#meta-editor-name-input')
		WebElement inputElement = nameInput.getElement()
		inputElement.click()
		inputElement.clear()
		inputElement.sendKeys(filtername)
	}

	void undoFilterChanges() {
		_editorElement.$('#meta-editor-undo-changes').click()
	}

	String getSelectedFilterName() {
		return _editorElement.$('#searchfilter-selector-dropdown-link').text
	}

	List<String> getAllFilters() {
		_editorElement.$('#searchfilter-selector-dropdown-link').click()
		List<NuclosWebElement> dropdownItems = _editorElement.$$('.dropdown-item')
		_editorElement.$('#searchfilter-selector-dropdown-link').click()
		return dropdownItems.collect {it.text}
	}

	boolean isFilterAvailable(String filtername) {
		return getAllFilters().contains(filtername)
	}

	void selectFilter(String filterName) {
		_editorElement.$('#searchfilter-selector-dropdown-link').click()
		List<NuclosWebElement> dropdownItems = $$('#searchfilter-selector-dropdown-menu .dropdown-item')
		NuclosWebElement itemToSelect = dropdownItems.find(element -> element.text.trim() == filterName)
		itemToSelect.click()
	}

	void makeTempPersistent() {
		_editorElement.$('#meta-editor-make-temp-persistent').click()
	}

	void saveFilterChanges() {
		_editorElement.$('#meta-editor-save-changes').click()
	}

	void deleteSelectedFilter() {
		_editorElement.$('#meta-editor-delete-searchfilter').click()
	}

}
