package org.nuclos.test.webclient.pageobjects.subform

import static org.nuclos.test.AbstractNuclosTest.sleep
import static org.nuclos.test.AbstractNuclosTest.waitForNotNull
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.util.ValueParser
import org.nuclos.test.webclient.validation.ValidationStatus
import org.openqa.selenium.Keys
import org.openqa.selenium.StaleElementReferenceException

import groovy.transform.CompileStatic

@CompileStatic
class SubformRow {
	private final String subformRefAttrFqn
	private final boolean bThrowRowLoadingFailures
	final int index

	NuclosWebElement _bodyRow

	SubformRow(Subform subform, int index, boolean bThrowRowLoadingFailures = true) {
		this.subformRefAttrFqn = subform.refAttrFqn
		this.bThrowRowLoadingFailures = bThrowRowLoadingFailures
		this.index = index

		_bodyRow = this.fetchRowElement()
	}

	NuclosWebElement getBodyRow() {
		if (_bodyRow != null) {
			return _bodyRow
		}
		_bodyRow = waitForNotNull(10, {this.fetchRowElement()}, bThrowRowLoadingFailures)
		return _bodyRow
	}

	private Subform getSubform() {
		new Subform(refAttrFqn: subformRefAttrFqn, subformElement: $('[ref-attr-id="' + subformRefAttrFqn + '"]'))
	}

	private NuclosWebElement fetchRowElement() {
		// TODO why is 'index'-attribute of grid row not available after deleting a subform entry
		NuclosWebElement element = subform.subformElement.$('.ag-body-viewport .ag-center-cols-viewport .ag-row[index="' + index + '"]')
		if (element != null)
			return element
		else
			return subform.subformElement.$('.ag-body-viewport .ag-center-cols-viewport .ag-row[row-index="' + index + '"]')
	}

	ListOfValues getLOV(String attributeName) {
		NuclosWebElement lovElement = getFieldElement(attributeName)
		return lovElement ? new SubformListOfValues(
				attributeName: attributeName,
				lov: lovElement,
				subform: subform
		) : null
	}

	FileComponent getFileComponent(String attributeName) {
		NuclosWebElement element = getFieldElement(attributeName)
		element ? new SubformFileComponent(element) : null
	}

	ImageComponent getImageComponent(String attributeName) {
		NuclosWebElement element = getFieldElement(attributeName)
		element ? new ImageComponent(element, attributeName) : null
	}

	/**
	 * Gets the field element whose "col-id" ends with the given attribute name.
	 *
	 * @param attributeName
	 * @return
	 */
	NuclosWebElement getFieldElement(String attributeName) {
		waitForAngularRequestsToFinish()

		NuclosWebElement element = getFieldElementOrScrollHorizontally(attributeName)

		if (!element) {
			throw new IllegalArgumentException("Could not find subform column for attribute '$attributeName'")
		}

		return element
	}

	void clickCell(String attributeName) {
		getFieldElement(attributeName).click()
	}

	private NuclosWebElement getFieldElementOrScrollHorizontally(String attributeName) {
		NuclosWebElement element = getFieldElementOrNull(attributeName)
		if (element && element.isDisplayed()) {
			return element
		}

		NuclosWebElement view = subform.viewPort

		if (view.canScrollLeft()) {
			view.scrollToLeftEnd()
			element = getFieldElementOrNull(attributeName)
		}

		while (!element && view.canScrollRight()) {
			view.scrollRight()
			sleep(100)
			element = getFieldElementOrNull(attributeName)
		}

		return element
	}

	private NuclosWebElement getFieldElementOrNull(String attributeName) {
		retryOnStaleElement {
			def row = getBodyRow()
			if (row != null) {
				NuclosWebElement result = row.$('.ag-cell[col-id$="' + attributeName + '"]')
				if (result) {
					// test for StaleElementReferenceException: stale element reference: element is not attached to the page document
					result.isDisplayed()
				}
				return result
			}
			return null
		}
	}

	/**
	 * Checks or unchecks a boolean attribute.
	 */
	void setChecked(final String attributeName, final boolean check) {
		NuclosWebElement cell = getFieldElement(attributeName)

		boolean checked = cell.$(Subform.CHECKBOX_CHECKED_SELECTOR) as boolean
		if (checked != check) {
			cell.click()
		}

		// the first click may have only selected the row,
		// check and click again...
		checked = cell.$(Subform.CHECKBOX_CHECKED_SELECTOR) as boolean
		if (checked != check) {
			cell.click()
		}

		checked = cell.$(Subform.CHECKBOX_CHECKED_SELECTOR) as boolean
		assert check == checked
	}

	NuclosWebElement openEditor(NuclosWebElement cell) {
		NuclosWebElement input = getEditor(cell)

		// press enter to edit cell
		if (!input) {
			// Sometimes the cell is hidden by another big cell editor (date editor)
			// Close it first by sending ESC
			sendKeys(Keys.ESCAPE)

			cell.click()
			input = getEditor(cell)
		}

		return input
	}

	NuclosWebElement getEditor(NuclosWebElement cell) {
		subform.getPopupEditor(cell) ?: cell.$('input')
	}

	int enterValue(String attributeName, String value, boolean sendEscape = true) {
		int nrChoices = 0
		NuclosWebElement cell = getFieldElement(attributeName)

		NuclosWebElement subformReference = cell.$('nuc-subform-reference')
		if (subformReference) {
			ListOfValues lov = getLOV(attributeName)
			lov.open()
			nrChoices = lov.getChoices().size()
			lov.selectEntry(value)
		} else {
			NuclosWebElement input = openEditor(cell)

			if (!input) {
				// TODO: Should it ever be necessary to send text to the cell itself?
				input = cell
			}

			// Textarea must be cleared first, because the text is not pre-selected like in other components
			if (input.tagName == 'textarea') {
				input.clear()
			}

			sendKeys(value)
			Thread.sleep(200)
			sendKeys(Keys.TAB)
			if (sendEscape) {
				sendKeys(Keys.ESCAPE)
			}
		}

		waitForAngularRequestsToFinish()
		return nrChoices
	}

	void enterValues(Map<String, String> data) {
		data.each { String fieldName, String value ->
			enterValue(fieldName, value)
		}
	}

	def <T> T getValue(String attributeName, Class<T> clazz = String.class, boolean multiline = false) {
		T result = null

		NuclosWebElement element = getFieldElement(attributeName)
		String text = element?.text
		if (clazz == String.class) {
			if (text.contains('\n') && !multiline) {
				text = element?.text?.substring(0, element?.text?.indexOf('\n'))
			}
			result = text as T
		} else if (clazz == Date.class) {
			result = ValueParser.parseDate(text) as T
		} else if (clazz == Boolean.class) {
			result = !!element.$(Subform.CHECKBOX_CHECKED_SELECTOR) as T
		} else if (clazz == Integer.class) {
			if (text.isNumber()) {
				result = text.toInteger() as T
			}
		} else if (clazz == BigDecimal.class) {
			result = ValueParser.parseBigDecimal(text) as T
		} else {
			throw new IllegalArgumentException('Can not get subform values for class ' + clazz)
		}

		return result
	}

	ValidationStatus getValidationStatus(String attributeName) {
		NuclosWebElement element = getFieldElement(attributeName)
		String cssClasses = element.getAttribute('class')

		ValidationStatus.fromCssClasses(cssClasses)
	}

	boolean isDirty() {
		hasCssClass('row-modified')
	}

	boolean isNew() {
		hasCssClass('row-new')
	}

	boolean isDeleted() {
		hasCssClass('row-deleted')
	}

	boolean isSelected() {
		subform.subformElement.$$('.ag-pinned-left-cols-container [row-index="' + index + '"]')
				.get(0)
				.getAttribute('class')
				.contains('ag-row-selected')
	}

	private boolean hasCssClass(String className) {
		String result = retryOnStaleElement {
			def row = getBodyRow()
			if (row != null) {
				return (row.getAttribute('class').contains(className) ||
						row.$('.ag-cell')?.getAttribute('class').contains(className))
				? 'classFound' : 'classNotFound'
			}
			return null
		}
		if (result != null) {
			return result.equals('classFound')
		} else {
			throw new IllegalStateException('Subform row/cell CSS classes could not be determined')
		}
	}

	void setSelected(boolean selected) {
		// TODO: Workaround for selection bug with autonumbers...
		while (this.selected != selected) {
			subform.toggleSelection(index)
		}
	}

	/**
	 * Tries to re-fetch the row element once, in case it became stale.
	 * Happens when subform rows are re-rendered.
	 *
	 * @param c
	 * @return
	 */
	private <T> T retryOnStaleElement(Closure<T> c) {
		try {
			return c()
		} catch (StaleElementReferenceException e) {
			return waitForNotNull(10, {
				try {
					return c()
				} catch (StaleElementReferenceException e2) {
					_bodyRow = waitForNotNull(10, {this.fetchRowElement()}, false)
					return null
				}
			}) as T
		}
	}

	boolean canEditInModal() {
		subform.getEditInModalLink(index)
	}

	void editInModal() {
		subform.editInModal(index)
	}

	boolean canEditInWindow() {
		subform.getEditInWindowLink(index)
	}

	void editInWindow() {
		subform.editInWindow(index)
	}

	Datepicker getDatepicker(final String attributeName) {
		NuclosWebElement element = getFieldElement(attributeName)
		new Datepicker(element) {
			/**
			 * Subform datepicker is not a child of the cell (containerElement), but is opened in the popup
			 * editor area of the subform.
			 * Datepicker is attached to the body and needs the right component ID to find the datepicker panel.
			 */
			@Override
			protected String getComponentId() {
				return inputElement?.id
			}

			@Override
			NuclosWebElement getInputElement() {
				open()
				return containerElement.$('input')
			}

			@Override
			boolean isOpen() {
				return containerElement.$('input') != null
			}

			@Override
			void open() {
				if (!open) {
					containerElement.click()
				}
			}

			@Override
			void commit() {
				if (open) {
					// Blur does not work here - send enter to close the subform editor
					inputElement.focus()
					sendKeys(Keys.ENTER)
				}
			}
		}
	}

	int getHeight() {
		getBodyRow().size.height
	}
}
