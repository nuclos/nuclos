package org.nuclos.test.webclient.pageobjects.viewconfiguration

import static org.nuclos.test.AbstractNuclosTest.waitForNotNull
import static org.nuclos.test.AbstractNuclosTest.waitUntilTrue
import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.click
import static org.nuclos.test.webclient.AbstractWebclientTest.driver
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.By
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class SideviewConfiguration extends ViewConfigurationModal {

	static String colorMarked = "rgba(208, 208, 208, 1)"
	static String colorUnmarked = "rgba(255, 255, 255, 1)"

	static void selectMarkedColumns() {
		$('#sideviewmenu-columns-config #btn-selected-marked').click()
	}

	static void deselectMarkedColumns() {
		$('#sideviewmenu-columns-config #btn-deselected-marked').click()
	}

	static void selectColumn(String attributeFqn) {
		findUnselectedColumnOption(attributeFqn).click()
		selectMarkedColumns()
	}

	static void deselectColumn(String attributeFqn) {
		findSelectedColumnOption(attributeFqn).click()
		deselectMarkedColumns()
	}

	static void selectAll() {
		$('#sideviewmenu-columns-config #btn-select-all').click()
	}

	static void deselectAll() {
		$('#sideviewmenu-columns-config #btn-deselect-all').click()
	}

	static List<NuclosWebElement> getAllSelected() {
		$$('#selected-items [attr-fqn]')
	}

	static List<NuclosWebElement> getMarkedSelected() {
		getAllSelected().findAll { it.getCssValue("background-color") == colorMarked }
	}

	static List<NuclosWebElement> getAllUnselected() {
		$$('#available-items [attr-fqn]')
	}

	static List<NuclosWebElement> getMarkedUnselected() {
		getAllUnselected().findAll { it.getCssValue("background-color") == colorMarked }
	}

	static List<NuclosWebElement> getMarked(boolean selected) {
		if(selected) {
			getMarkedSelected()
		} else {
			getMarkedUnselected()
		}
	}

	static NuclosWebElement findSelectedColumnOption(String attributeFqn) {
		$('#sideviewmenu-columns-config .select-box-container-selected [attr-fqn="' + attributeFqn + '"]')
	}

	static NuclosWebElement findUnselectedColumnOption(String attributeFqn) {
		$('#sideviewmenu-columns-config [attr-fqn="' + attributeFqn + '"]')
	}

	static void pinAll() {
		$$('#selected-items [attr-fqn]').forEach {
			def unpinnedElement = it.$('div.pin-column i.item-unpinned')
			if (unpinnedElement) {
				unpinnedElement.getParent().click()
			}
		}
	}

	static void unpinAll() {
		$$('#selected-items [attr-fqn]').forEach {
			def pinnedElement = it.$('div.pin-column i.item-pinned')
			if (pinnedElement) {
				pinnedElement.getParent().click()
			}
		}
	}

	static void pin(String attributeFqn) {
		NuclosWebElement option = findSelectedColumnOption(attributeFqn)

		def unpinnedElement = option.$('div.pin-column i.item-unpinned')
		if (unpinnedElement) {
			unpinnedElement.getParent().click()
		}
	}

	static void unpin(String attributeFqn) {
		NuclosWebElement option = findSelectedColumnOption(attributeFqn)

		def pinnedElement = option.$('div.pin-column i.item-pinned')
		if (pinnedElement) {
			pinnedElement.getParent().click()
		}
	}

	static void moveColumn(String attributeFqn, boolean down) {
		NuclosWebElement option = findSelectedColumnOption(attributeFqn)
		option.click()

		if (down) {
			$('#btn-move-down').click()
		} else {
			$('#btn-move-up').click()
		}
	}

	static void newSideviewConfiguration(final String name = null) {
		open()
		if (isSideviewConfigurationModified()) {
			saveSideviewConfiguration()
		}
		$('#new-sideviewmenu').click()
		if (name) {
			$('#sideviewmenu-name-input').sendKeys(name)
		}
	}

	static void saveSideviewConfiguration() {
		$('#save-sideviewmenu').click()
	}

	static void selectSideviewConfiguration(final String name) {
		openColumnConfigurationPanel()
		selectSideviewConfigurationInModal(name)
		clickButtonOk()
	}

	static void selectSideviewConfigurationInModal(final String name) {
		$$('#sideviewmenu-selector option').find{ it.text?.trim().indexOf(name) != -1}.click()
	}

	static void openColumnConfigurationPanel() {
		open()
		clickIfCollapsed('#column-preferences-header')
	}

	static String getSelectedConfigurationName() {
		new Select($('#sideviewmenu-selector')).getFirstSelectedOption()?.text?.trim()
	}

	static void discardChanges() {
		// ein einfacher Klick auf dem Button geht im Jenkins häufig nicht, also so lange probieren bis dieser Button nicht mehr angezeigt wird...
		waitUntilTrue({
			def discardButton = waitForNotNull(5, {$('#discard-sideviewmenu')}, false)
			if (discardButton) {
				click(discardButton)
				waitForAngularRequestsToFinish()
				return false
			} else {
				return true
			}
		})
	}

	static void resetSorting() {
		if ($('#reset-sorting')) {
			$('#reset-sorting').click()
		}
	}

	static boolean hasSideviewConfiguration(String name) {
		def retVal = AbstractWebclientTest.$$('#sideviewmenu-selector option').find { it.text?.trim().indexOf(name) != -1 } != null
		close()
		return retVal
	}

	static boolean isSideviewConfigurationModified() {
		return $('#save-sideviewmenu') != null
	}
}
