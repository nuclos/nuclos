package org.nuclos.test.webclient.util

import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.utils.Utils
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot

import groovy.transform.CompileStatic

@CompileStatic
class Screenshot {
	static void take(String name) {
        try {
            // don't call waitForAngularRequestsToFinish if called from waitForAngularRequestsToFinish - prevent StackOverflow
            if (!Utils.isCalledFrom('waitForAngularRequestsToFinish')) {
                AbstractWebclientTest.waitForAngularRequestsToFinish()
            }

            String clazzNameRunning = ""
            String testNameRunning = ""

            StackTraceElement stackTraceEl = Utils.getCurrentTestClassFromStackTrace()
            if (stackTraceEl != null) {
                clazzNameRunning = stackTraceEl.className
                testNameRunning = stackTraceEl.methodName
            }

            // remove org.nuclos.test from clazzName and replace . with / so it would be a path
            clazzNameRunning = clazzNameRunning.replace("org.nuclos.test", "")
            clazzNameRunning = clazzNameRunning.replace(".", "/")

            File scrFile = ((TakesScreenshot) AbstractWebclientTest.getDriver()).getScreenshotAs(OutputType.FILE)
            String fileName = testNameRunning + " - ${name}.png"

            String targetPath = 'target/screenshots/' + clazzNameRunning
            if (System.getProperty('basedir')) {
                targetPath = System.getProperty('basedir') + '/' + targetPath
            }

            File targetBrowserDir = new File(targetPath)
            Utils.mkdirP(targetBrowserDir)
            File targetFile = new File(targetBrowserDir, fileName)

            scrFile.withDataInputStream { targetFile << it }
        } catch (Throwable t) {
            Log.warn "Failed to take screenshot: $t.message"
        }
	}
}
