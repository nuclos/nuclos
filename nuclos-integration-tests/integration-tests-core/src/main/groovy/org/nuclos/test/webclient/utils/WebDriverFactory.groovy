package org.nuclos.test.webclient.utils

import java.util.logging.Level

import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.WebclientTestContext
import org.openqa.selenium.MutableCapabilities
import org.openqa.selenium.Proxy
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.logging.LoggingPreferences
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import org.testcontainers.containers.BrowserWebDriverContainer
import org.testcontainers.containers.DefaultRecordingFileFactory

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class WebDriverFactory {

	private static final String SELENIUM_CAPABILITIES_PREFIX = 'selenium.capabilities.'

	static RemoteWebDriver remoteWebDriver(
			final MutableCapabilities caps,
			final WebclientTestContext context,
			final String dockerImage = null
	) {
		final Proxy proxy = context.browserProxy.getSeleniumProxy()

		// Enable browser console logging
		LoggingPreferences logPrefs = new LoggingPreferences()
		logPrefs.enable(LogType.BROWSER, Level.ALL)
		caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs)
		// Google renamed loggingPrefs for their Browser - this will be fixed in Selenium 4.0.0 / 3.150.0
		caps.setCapability("goog:loggingPrefs", logPrefs)
		caps.setCapability(CapabilityType.PROXY, proxy)
		caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true)
		caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		caps.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

		setCapabilitiesFromSystemProperties(caps)

		// If no selenium server is defined via system properties, but we know a Docker image,
		// try to instantiate a selenium server via Docker.
		if (!context.seleniumServer) {
			try {
				File dirPath = new File(context.nuclosVidFilePath)
				dirPath.mkdir()
				// Stop any previously started containers
				if (AbstractWebclientTest.browserContainer != null) {
					AbstractWebclientTest.browserContainer.stop()
				}

				AbstractWebclientTest.browserContainer = new BrowserWebDriverContainer<>()
				AbstractWebclientTest.browserContainer.withCapabilities(caps)
				AbstractWebclientTest.browserContainer.withRecordingMode(
						BrowserWebDriverContainer.VncRecordingMode.RECORD_FAILING,
						dirPath
				)
				AbstractWebclientTest.browserContainer.withRecordingFileFactory(new DefaultRecordingFileFactory())

				AbstractWebclientTest.browserContainer.start()
				return AbstractWebclientTest.browserContainer.webDriver
			} catch (Exception e) {
				Log.error 'Failed to start selenium server via Docker', e
			}
		}

		return new RemoteWebDriver(new URL(context.seleniumServer), caps)
	}

	private static void setCapabilitiesFromSystemProperties(MutableCapabilities caps) {
		System.properties.keySet()
				.collect { "$it" }
				.findAll { it.startsWith(SELENIUM_CAPABILITIES_PREFIX) }
				.each
				{
					String capability = it.substring(SELENIUM_CAPABILITIES_PREFIX.size())
					caps.setCapability(capability, System.getProperty(it))
				}
	}

	static Closure remoteFirefox = { WebclientTestContext context ->
		FirefoxOptions options = new FirefoxOptions()
		options.addPreference("network.proxy.allow_hijacking_localhost", true)
		remoteWebDriver(options, context, 'selenium/standalone-firefox:latest')
	}

	static Closure remoteChrome = { WebclientTestContext context ->
		ChromeOptions options = new ChromeOptions()
		options.addArguments("--enable-automation")
		options.addArguments("--disable-extensions")
		//options.addArguments("--disable-dev-shm-usage") // overcome limited resource problems
		options.addArguments("--shm-size=\"4g\"") // overcome limited resource problems
		options.addArguments("--no-sandbox") // Bypass OS security model
		Map<String, Object> prefs = new HashMap<>()
		prefs.put("intl.accept_languages", "de") // de -> the most used in our testing
		options.setExperimentalOption("prefs", prefs)
		remoteWebDriver(options, context, 'selenium/standalone-chrome:latest')
	}

	static Closure remotePhantomJS = { WebclientTestContext context ->
		DesiredCapabilities caps = DesiredCapabilities.phantomjs()
		remoteWebDriver(caps, context)
	}

	static Closure remoteIE = { WebclientTestContext context ->
		DesiredCapabilities caps = DesiredCapabilities.internetExplorer()
		remoteWebDriver(caps, context)
	}

	static Closure remoteSafari = { WebclientTestContext context ->
		DesiredCapabilities caps = DesiredCapabilities.safari()
		remoteWebDriver(caps, context)
	}
}
