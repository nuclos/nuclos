package org.nuclos.test

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.FailureHandler
import org.openqa.selenium.TimeoutException

import groovy.transform.CompileStatic

@CompileStatic
class WaitForNotNullTest {

	@BeforeClass
	static void disableFailureHandlerLogging() {
		FailureHandler.loggingEnabled = false
	}

	@AfterClass
	static void enableFailureHandlerLogging() {
		FailureHandler.loggingEnabled = true
	}

	@Test
	void testWaitForNotNull() {
		final String tMessage = 'x'
		final String rMessage = 'r'

		testThrowingNoRetryOnException: {
			def c = {throw new Throwable(tMessage)}
			Throwable catched
			int timeoutInSeconds = 30
			long startTime = System.currentTimeSeconds()
			try {
				AbstractWebclientTest.waitForNotNull(timeoutInSeconds, c)
			} catch (Throwable t) {
				catched = t
			}
			long finishTime = System.currentTimeSeconds()
			assert finishTime-startTime < timeoutInSeconds
			assert catched instanceof Throwable
			assert catched.getMessage() == tMessage
		}

		testThrowingWithRetryOnException: {
			def c = {throw new ArrayIndexOutOfBoundsException(tMessage)}
			Throwable catched
			int timeoutInSeconds = 5
			long startTime = System.currentTimeSeconds()
			try {
				AbstractWebclientTest.waitForNotNull(timeoutInSeconds, c, true, IndexOutOfBoundsException.class)
			} catch (Throwable t) {
				catched = t
			}
			long finishTime = System.currentTimeSeconds()
			assert finishTime-startTime >= timeoutInSeconds
			assert catched instanceof TimeoutException
			assert catched.getMessage().startsWith('java.lang.ArrayIndexOutOfBoundsException: ' + tMessage)
		}

		testNoThrowing: {
			def c = {throw new Throwable(tMessage)}
			def result
			Throwable catched
			try {
				result = AbstractWebclientTest.waitForNotNull(5, c, false)
			} catch (Throwable t) {
				catched = t
			}
			assert !result
			assert !catched
		}

		testNotNull: {
			def c = {return rMessage}
			def result
			Throwable catched
			try {
				result = AbstractWebclientTest.waitForNotNull(10, c)
			} catch (Throwable t) {
				catched = t
			}
			assert result == rMessage
			assert !catched
		}

		testNull: {
			def c = {return null}
			def result
			Throwable catched
			try {
				result = AbstractWebclientTest.waitForNotNull(5, c)
			} catch (Throwable t) {
				catched = t
			}
			assert !result
			assert catched instanceof TimeoutException
			assert catched.getMessage().startsWith(getClass().getCanonicalName() + '\$')
		}

		testNullNoTimeout: {
			// no timeout? try only once!
			def c = {return null}
			def result
			Throwable catched
			long startTime = System.currentTimeSeconds()
			try {
				result = AbstractWebclientTest.waitForNotNull(0, c)
			} catch (Throwable t) {
				catched = t
			}
			long finishTime = System.currentTimeSeconds()
			assert finishTime-startTime <= NuclosTestContext.instance.DEFAULT_TIMEOUT
			assert !result
			assert catched instanceof TimeoutException
			assert catched.getMessage().startsWith(getClass().getCanonicalName() + '\$')
		}

		testEmptyString: {
			def c = {return ""}
			def result
			Throwable catched
			int timeoutInSeconds = 30
			long startTime = System.currentTimeSeconds()
			try {
				result = AbstractWebclientTest.waitForNotNull(timeoutInSeconds, c)
			} catch (Throwable t) {
				catched = t
			}
			long finishTime = System.currentTimeSeconds()
			assert finishTime-startTime <= timeoutInSeconds
			assert result == ""
			assert !catched
		}

	}

}
