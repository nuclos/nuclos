package org.nuclos.test.architecture

import com.tngtech.archunit.core.importer.ImportOption
import com.tngtech.archunit.core.importer.Location

import groovy.transform.CompileStatic

/**
 * Inspect only nuclos classes
 */
@CompileStatic
class OnlyLookUpNuclos implements ImportOption {
	@Override
	boolean includes(final Location location) {
		return location.contains("org/nuclos")
	}
}
