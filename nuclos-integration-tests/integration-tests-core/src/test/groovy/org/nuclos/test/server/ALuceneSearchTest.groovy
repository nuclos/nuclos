package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.awaitility.core.ConditionTimeoutException
import org.json.JSONArray
import org.junit.After
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.*
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 * The "A" at the beginning is for this test to start the series of ServerTests
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ALuceneSearchTest extends AbstractRerunnableNuclosTest {

	@After
	void disableIndexer() {
		nuclosSession.managementConsole('disableIndexer')
	}

@Test
void runTest() {
	RESTClient suclient = new RESTClient('nuclos', '')
	RESTClient client


	_00_setup: {
		nuclosSession.managementConsole('enableIndexerSynchronously')
		nuclosSession.managementConsole('rebuildLuceneIndex')
	}

	_01_createTestUser: {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
	}

	_02createTestData: {
		TestDataHelper.createLuceneTestData(getNuclosSession())
	}

	_04_login: {
		client.login()
		assert client.sessionId
	}

	_06luceneTest: {
		// After data change give lucene some time to cleanly close the indexing, here 20s

		expectSearchResults(client, TestDataHelper.catName, 4, 20) //Note: The three words + the category itself.

		expectSearchResults(client, TestDataHelper.catReName, 0) //Note: No hit until now.
	}

	_08renameCategory: {

		EntityObject<Long> eo = client.getEntityObject(TestEntities.EXAMPLE_REST_CATEGORY, Long.valueOf((String)TestDataHelper.cat1['id']))

		eo.setAttribute('name', TestDataHelper.catReName)

		eo.save()

	}

	_10testReference: {

		expectSearchResults(client, TestDataHelper.catName, 1, 200) //Note: catName is still the originalName of word3

		expectSearchResults(client, TestDataHelper.catReName, 4) //Note: The three words + the category itself.

	}

	_12testRecordGrant: {

		//word4 and word5 must not appear for user test, but still the category for which there are not record grants

		expectSearchResults(client, TestDataHelper.cat2Name, 1)

		suclient.login()
		assert suclient.sessionId
		// while the super user see them all
		expectSearchResults(suclient, TestDataHelper.cat2Name, 3)

	}

	_14testRollback: {

		EntityObject<Long> newWord = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		newWord.setClient(client)
		newWord.setAttributes(new HashMap<>(TestDataHelper.word6))

		// An error from the final rule must have been
		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			newWord.save()
		}

		expectSearchResults(client, TestDataHelper.word6.text as String, 0, 200) // It must not be indexed (rollback)

		newWord.setAttribute('times', 37)

		newWord.save()

		assert newWord.id // Saved!

		expectSearchResults(client, TestDataHelper.word6.text as String, 1, 200) // Now it must be indexed

	}

	_16testComplexSearches: {
		expectSearchResults(client, 'Fl', 2) // Fleisch and Flugzeuge

		expectSearchResults(client, 'Fle', 1) // Only Fleisch

		expectSearchResults(client, 'fl%20sc', 1) // "Flugzeuge Schiffe"

		expectSearchResults(client, 'Sch', 1) // Schiffe

		expectSearchResults(suclient, 'Sch', 2) // Schiffe and Schraubenzieher

		expectSearchResults(suclient, '5-', 1) // 5-Tonnen-Maschine

		expectSearchResults(suclient, 'Tonnen', 1) // 5-Tonnen-Maschine

		expectSearchResults(suclient, 'Masch', 3) // 5-Tonnen-Maschine and 6-T-Maschine and Maschinen

		expectSearchResults(suclient, '%22Maschine%22', 2) // 5-Tonnen-Maschine and 6-T-Maschine
	}


	_18testDeleteRecord: {

		EntityObject<Long> eo = client.getEntityObject(TestEntities.EXAMPLE_REST_CATEGORY, Long.valueOf((String)TestDataHelper.cat1['id']))

		// Should not be able to delete (existing dependent data)
		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			assert !eo.delete()
		}

		assert eo.id

		expectSearchResults(client, TestDataHelper.catReName, 4, 200) //Note: Still three words + the category itself.

		eo = client.getEntityObject(TestEntities.EXAMPLE_REST_WORD, (Long) TestDataHelper.word1['id'])
		assert eo.delete()
		assert !eo.id // Delete successfull

		expectSearchResults(client, TestDataHelper.catReName, 3, 200) //Note: One delete, one hit less

		expectSearchResults(client, 'Fl', 1) // Only Flugzeuge now
	}

	//@Ignore('DocumentFileStore does not work with lucene yet')
	/*_20_testDocumentFile: {
		client.expectSearchResults('Blind', 0)

		ServerIntegrationTest.runServerTest("documentFileStoreFileInsertMD")

		client.expectSearchResults('Blind', 1) // Multiple hits of "Blind" in the document, but one document

	}*/

}

	private void expectSearchResults(RESTClient client, String search, int hits, int waitInSeconds = 30) {
		Closure c = {AbstractNuclosTest.waitUntilTrue({getSearchResult(client, search).length() == hits}, waitInSeconds)}
		try {
			c()
		} catch (ConditionTimeoutException e) {
			// Lucene is not working properly:
			// org.apache.lucene.index.IndexNotFoundException:
			// no segments* file found in MMapDirectory@/home/maik/Temp/integration-tests-run.sh/data/index lockFactory=org.apache.lucene.store.NativeFSLockFactory
			// try to rebuild index and try again
			nuclosSession.managementConsole('rebuildLuceneIndex')
			c()
		}
	}

	private JSONArray getSearchResult(RESTClient client, String search) {
		String url = RESTHelper.REST_BASE_URL + '/data/search/' + search
		return RESTHelper.requestJsonArray(url, HttpMethod.GET, client.sessionId, null)
	}

}
