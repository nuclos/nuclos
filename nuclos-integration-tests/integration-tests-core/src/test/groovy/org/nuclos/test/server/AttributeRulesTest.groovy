package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class AttributeRulesTest extends AbstractNuclosTest {
	static RESTClient client
	static Long auftrag1Id;
	static Long rechnung1Id;
	static Long article1Id;
	static EntityObject<Long> auftrag

	@Test
	void _02_login() {
		client = new RESTClient('nuclos', '')
		client.login()
		assert client.sessionId
	}

	@Test
	void _10_createTestData() {
		Map rechnung = [name: 'Rechnung 1'] as Map<String, Serializable>
		rechnung1Id = (Long) RESTHelper.createBo(TestEntities.EXAMPLE_REST_RECHNUNG.fqn, rechnung, nuclosSession)['boId']

		assert rechnung1Id > 0

		Map auftrag = [name: 'Auftrag 1', bemerkung: 'Bemerkung', nogroup: 'NoGroup1', rechnung: [id: rechnung1Id], anzahl: 13] as Map<String, Serializable>
		auftrag1Id = (Long) RESTHelper.createBo(TestEntities.EXAMPLE_REST_AUFTRAG.fqn, auftrag, nuclosSession)['boId']

		assert auftrag1Id > 0

		Map article = [name: 'Artikel 1', price: '2.50', articleNumber: '13'] as Map<String, Serializable>
		article1Id = (Long) RESTHelper.createBo(TestEntities.EXAMPLE_REST_ARTICLE.fqn, article, nuclosSession)['boId']

		assert article1Id > 0

	}

	@Test
	void _16_readEo() {
		auftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)

		assert auftrag && auftrag.id == auftrag1Id

		assert auftrag.getAttribute('name') == 'Auftrag 1'
		assert auftrag.getAttribute('rechnung')['name'] == 'Rechnung 1'
		assert auftrag.getAttribute('anzahl') == 13
	}

	@Test
	void _26_changeStateTo_20() {
		auftrag.changeState(20)

		auftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)

		assert auftrag.getAttribute('nuclosState')['name'] == 'Bearbeitung'
	}

	@Test
	void _30_readEoState20() {
		assert auftrag.getAttribute('name') == 'Auftrag 1'
		assert auftrag.getAttribute('bemerkung') == 'Bemerkung'
		assert auftrag.getAttribute('rechnung')['name'] == 'Rechnung 1'
		assert auftrag.getAttribute('anzahl') == 13
	}

	@Test
	void _40_checkCustomRule2() {

		List<EntityObject<Long>> lst = client.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG, null)
		assert lst
		assert lst.size() == 1

		EntityObject<Long> auftrag = lst.get(0)
		assert auftrag.id == auftrag1Id

		// For anzahl == 13 the custom rule tries to read attributes that are not loaded.
		expectRestException({
			client.executeCustomRule(auftrag, 'example.rest.RegisterAuftrag2')
		}) {
			assert it.status == Response.Status.PRECONDITION_FAILED

			// TODO: Use an error code instead of locale-dependent text fragments
			assert it.errorText.contains("NuclosAttributeNotInRuleException")
			assert it.errorText.contains("Cannot access attribute 2Mh0z6qm0Z5oeMO46KSa. It was not in the query")
		}

		auftrag.setAttribute('anzahl', 14)

		// For anzahl == 14 the custom rule tries to write attributes that are not loaded.
		expectRestException({
			client.executeCustomRule(auftrag, 'example.rest.RegisterAuftrag2')
		}) {
			assert it.status == Response.Status.PRECONDITION_FAILED

			// TODO: Use an error code instead of locale-dependent text fragments
			assert it.errorText.contains("NuclosAttributeNotInRuleException")
			assert it.errorText.contains("Cannot write attribute 3QHJvTOV1Z9JwIYYLDl2. It was not in the query")
		}

		// For anzahl == 555 the custom rule queries normally, reads and writes attributes, then saves.
		auftrag.setAttribute('anzahl', 555)

		client.executeCustomRule(auftrag, 'example.rest.RegisterAuftrag2')

		lst = client.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG, null)
		assert lst && lst.size() == 1
		assert lst.get(0).id == auftrag1Id

		EntityObject<?> mipsAuftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, lst.get(0).id);

		assert mipsAuftrag.getAttribute('bemerkung') == 'Eine Bemerkung 555'
		assert mipsAuftrag.getAttribute('notiz') == 'Rechnung: Rechnung 1 555'

		// For anzahl == 4711 the custom rule reads and writes loaded attributes, then saves.
		mipsAuftrag.setAttribute('anzahl', 4711)

		expectRestException({
			client.executeCustomRule(mipsAuftrag, 'example.rest.RegisterAuftrag2')
		}) {
			assert it.status == Response.Status.PRECONDITION_FAILED

			// TODO: Use an error code instead of locale-dependent text fragments
			assert it.errorText.contains("Object cannot be saved without SaveFlags, because of selected attributes.")
		}

		// For anzahl == 4712 the custom rule reads and writes loaded attributes, then saves with DIRECTLY_SET.
		mipsAuftrag.setAttribute('anzahl', 4712)
		client.executeCustomRule(mipsAuftrag, 'example.rest.RegisterAuftrag2')

		lst = client.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG, null)
		assert lst && lst.size() == 1
		assert lst.get(0).id == auftrag1Id

		mipsAuftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, lst.get(0).id);

		assert mipsAuftrag.getAttribute('bemerkung') == 'Eine Bemerkung 555'
		// This is the most important part: Notiz has been changed (but not bemerkung, because it was not loaded)
		assert mipsAuftrag.getAttribute('notiz') == 'Rechnung: Rechnung 1 4712'

	}

	@Test
	void _50_checkCustomRule3() {

		List<EntityObject<Long>> lst = client.getEntityObjects(TestEntities.EXAMPLE_REST_ARTICLE, null)
		assert lst
		assert lst.size() == 1

		EntityObject<Long> article = lst.get(0)
		assert article.id == article1Id

		EntityObject<?> auftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)
		auftrag.setAttribute('anzahl', -13)
		// For anzahl == 13 the custom rule tries to read attributes that are not loaded.
		expectRestException({
			client.executeCustomRule(auftrag, 'example.rest.RegisterAuftrag2')
		}) {
			assert it.status == Response.Status.PRECONDITION_FAILED

			// TODO: Use an error code instead of locale-dependent text fragments
			assert it.errorText.contains("NuclosAttributeNotInRuleException")
			assert it.errorText.contains("Cannot access attribute ZAVTJqVqe3ToyhsvtZnA. It was not in the query")
		}

		auftrag.setAttribute('anzahl', -14)

		// For anzahl == 14 the custom rule tries to write attributes that are not loaded.
		expectRestException({
			client.executeCustomRule(auftrag, 'example.rest.RegisterAuftrag2')
		}) {
			assert it.status == Response.Status.PRECONDITION_FAILED

			// TODO: Use an error code instead of locale-dependent text fragments
			assert it.errorText.contains("NuclosAttributeNotInRuleException")
			assert it.errorText.contains("Cannot write attribute 4iZHuVCLA4ZjdJJgGXDe. It was not in the query")
		}

		// For anzahl == 555 the custom rule queries normally, reads and writes attributes, then saves.
		auftrag.setAttribute('anzahl', -555)

		client.executeCustomRule(auftrag, 'example.rest.RegisterAuftrag2')

		lst = client.getEntityObjects(TestEntities.EXAMPLE_REST_ARTICLE, null)
		assert lst && lst.size() == 1
		assert lst.get(0).id == article1Id

		article = lst.get(0)

		assert article.getAttribute('description') == 'Eine Beschreibung 555'
		assert article.getAttribute('price') == 557.50

		// For anzahl == 4711 the custom rule reads and writes loaded attributes, then saves.
		auftrag.setAttribute('anzahl', -4711)

		expectRestException({
			client.executeCustomRule(auftrag, 'example.rest.RegisterAuftrag2')
		}) {
			assert it.status == Response.Status.PRECONDITION_FAILED

			// TODO: Use an error code instead of locale-dependent text fragments
			assert it.errorText.contains("Object cannot be saved without SaveFlags, because of selected attributes.")
		}

		// For anzahl == 4712 the custom rule reads and writes loaded attributes, then saves with DIRECTLY_SET.
		auftrag.setAttribute('anzahl', -4712)
		client.executeCustomRule(auftrag, 'example.rest.RegisterAuftrag2')

		lst = client.getEntityObjects(TestEntities.EXAMPLE_REST_ARTICLE, null)
		assert lst && lst.size() == 1
		assert lst.get(0).id == article1Id

		article = client.getEntityObject(TestEntities.EXAMPLE_REST_ARTICLE, lst.get(0).id);

		assert article.getAttribute('description') == 'Eine Beschreibung 555'
		// This is the most important part: Price has been changed (but not description, because it was not loaded)
		assert article.getAttribute('price') == 5269.50

	}
}
