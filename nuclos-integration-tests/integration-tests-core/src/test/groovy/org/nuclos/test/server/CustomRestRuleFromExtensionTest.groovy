package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class CustomRestRuleFromExtensionTest extends CustomRestRuleTest {

    String getOldBasePath()             { return '/execute/extension.example.rest.CustomRestTestRuleExtension/' }
    String getOldBasePathInactive()     { return '/execute/extension.example.rest.CustomRestInactiveRuleExtension/' }
    String getNewBasePathCustomized()   { return '/custom/restpathtest_extension/' }

}
