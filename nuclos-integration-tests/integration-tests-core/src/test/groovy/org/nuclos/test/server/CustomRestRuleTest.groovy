package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.rules.ExpectedException
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTException
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class CustomRestRuleTest extends AbstractNuclosTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none()

	static RESTClient client

    String getOldBasePath()             { return '/execute/example.rest.CustomRestTestRule/' }
    String getOldBasePathInactive()     { return '/execute/example.rest.CustomRestInactiveRule/' }
    String getNewBasePathCustomized()   { return '/custom/restpathtest/' }

	@Test
	void _00_setupAccounts() {
		RESTHelper.createUser('testct', 'testct', ['Example controlling', 'Example readonly'], nuclosSession)
	}

	@Test
	void _01_noPermission() {
		client = new RESTClient('testct', 'testct').login()
		exception.expect(RESTException)
		RESTHelper.requestString(getOldBasePath() + 'getTest', HttpMethod.GET, client.sessionId)
	}

	@Test
	void _02_getTestSuperuser() {
		client = new RESTClient('nuclos', '').login()

		String response = RESTHelper.requestString(getOldBasePath() + 'getTest', HttpMethod.GET, client.sessionId)
		assert response == 'user-last-name:Nuclos'
	}

	@Test
	void _03_getTest() {
		client = new RESTClient('test', 'test').login()

		String response = RESTHelper.requestString(getOldBasePath() + 'getTest', HttpMethod.GET, client.sessionId)
		assert response == 'user-last-name:test'
	}

	@Test
	void _04_getTestWithRequestParams() {
		String response = RESTHelper.requestString(getOldBasePath() + 'getTestWithRequestParams?param1=ABC&param2=XYZ', HttpMethod.GET, client.sessionId)
		assert response == 'ABCXYZ'
	}

	@Test
	void _05_postTest() {
		String input = '{"a":1}'
		String response = RESTHelper.postJson(getOldBasePath() + 'postTest', input, client)
		assert response == input
	}

	@Test
	void _06_putTest() {
		String input = '{"a":1}'
		String response = RESTHelper.putJson(getOldBasePath() + 'putTest', input, client)
		assert response == input
	}

	@Test
	void _07_deleteTest() {
		String input = '{"a":1}'
		String response = RESTHelper.requestString(getOldBasePath() + 'deleteTest', HttpMethod.DELETE, client.sessionId, input)
		assert response == 'ok'
	}

	@Test
	void _08_methodNotAllowedTest() {
		// try a GET request on a method which only accepts POST
		exception.expect(RESTException)
		RESTHelper.requestString(getOldBasePath() + 'postTest', HttpMethod.GET, client.sessionId)
	}

	@Test
	void _10_inactiveRule() {
		exception.expect(RESTException)
		RESTHelper.requestString(getOldBasePathInactive() + 'getTest', HttpMethod.GET, client.sessionId)
	}

	void assertUserLastNameForSession(String sessionId, String expectedLastName) {
		String response = null
		try {
			waitFor {
				response = RESTHelper.requestString(getNewBasePathCustomized() + 'gettest', HttpMethod.GET, sessionId)
			}
		} catch (Exception e) {
			Log.warn('Could not read custom rest response: ' + e.message)
		}

		assert response == 'user-last-name:' + expectedLastName
	}

	@Test
	void _15_getTestWithPathAnnotation() {
		client = new RESTClient('test', 'test').login()

		assertUserLastNameForSession(client.sessionId, 'test')
	}

	@Test
	void _16_getTestAnonymousWithoutDbUser() {
		// the default anonymous user taken from org.nuclos.server.rest.services.helper.CustomRestContextFactory.ANONYMOUS_USER
		assertUserLastNameForSession(null, 'Nym')
	}

	@Test
	void _17_getTestAnonymousWithDbUser() {
		RESTHelper.createUser('anonymous', 'nuclos', ['Example user'], nuclosSession)
		client = new RESTClient('anonymous', 'nuclos').login()

		assertUserLastNameForSession(client.sessionId, 'anonymous')
	}

	@Test
	void _18_getTestAnonymousWithoutDbUserButDbUserExists() {
		// User is created from _17 and must be used
		// run _16 again with expected name 'anonymous'
		assertUserLastNameForSession(null, 'anonymous')
	}

}
