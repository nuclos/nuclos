package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.rules.ExpectedException
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTException
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class CustomRuleTest extends AbstractNuclosTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none()

	static RESTClient client

	@Test
	void _00_setupUpAccounts() {
		RESTHelper.createUser('testct', 'testct', ['Example controlling', 'Example readonly'], nuclosSession)
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
	}

	@Test
	void _11_notAssignedRule() {
		client = new RESTClient('nuclos', '').login()

		EntityObject<Long> auftrag = new EntityObject<>(TestEntities.EXAMPLE_REST_AUFTRAG)
		auftrag.setAttribute('name', 'test')

		exception.expect(RESTException)
		client.executeCustomRule(auftrag, 'example.rest.CustomRuleNotAssignedTest')
	}

	@Test
	void _12_assignedRule() {
		EntityObject<Long> auftrag = new EntityObject<>(TestEntities.EXAMPLE_REST_AUFTRAG)
		auftrag.setAttribute('name', 'test 2')

		client.executeCustomRule(auftrag, 'example.rest.CustomRuleAssignedTest')
	}

	@Test
	void _13_executeRuleOnNewBoWithUpdate() {
		EntityObject<Long> auftrag = new EntityObject<>(TestEntities.EXAMPLE_REST_AUFTRAG)
		auftrag.setAttribute('name', 'test 3')

		client.executeCustomRule(auftrag, 'example.rest.CustomRuleForNewBusinessObjectWithUpdate')
		QueryOptions query = new QueryOptions(
				orderBy: (Set) [TestEntities.EXAMPLE_REST_AUFTRAG.fqn + '_primaryKey desc']
		)
		EntityObject<Long> auftragDb = client.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG, query).first()
		assert auftragDb != null && auftragDb.getAttribute('name') == 'test 3'
		assert auftragDb.getAttribute('anzahl') == 123456
	}

	@Test
	void _14_executeRuleOnNewBoWithoutUpdate() {
		EntityObject<Long> auftrag = new EntityObject<>(TestEntities.EXAMPLE_REST_AUFTRAG)
		auftrag.setAttribute('name', 'test 4')

		client.executeCustomRule(auftrag, 'example.rest.CustomRuleForNewBusinessObjectWithoutUpdate')
		QueryOptions query = new QueryOptions(
				orderBy: (Set) [TestEntities.EXAMPLE_REST_AUFTRAG.fqn + '_primaryKey desc']
		)
		EntityObject<Long> auftragDb = client.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG, query).first()
		assert auftragDb == null || auftragDb.getAttribute('name') == 'test 3'
	}

}
