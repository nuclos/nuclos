package org.nuclos.test.server

import org.json.JSONArray
import org.json.JSONObject
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.LoginParams
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DataLanguageTest extends AbstractNuclosTest {

	static RESTClient clientDe
	static RESTClient clientEn

	static Long auftragstypSchnellId
	static Long artikeltypSpielzeugId
	static Long artikelTeddybaerId
	static Long artikelMatchboxAutoId
	static Long auftragNachbarschaftId
	static Long posNachbarschaft1Id
	static Long maschinenId
	static Long maschinenTeilId

	static void setupDataLanguageTestData() {
		clientDe = new RESTClient(
				new LoginParams(username: 'nuclos', password: '', datalanguage: 'de_DE')
		)
		assert clientDe.login().initialDatalanguage == 'de_DE'
		clientEn = new RESTClient(
				new LoginParams(username: 'nuclos', password: '', datalanguage: 'en_GB')
		)
		assert clientEn.login().initialDatalanguage == 'en_GB'

		EntityObject<Long> auftragstyp = new EntityObject(TestEntities.NUCLET_TEST_I18N_AUFTRAGSTYP)
		auftragstyp.setAttribute('auftragstyp', 'Schnell')
		clientDe.save(auftragstyp);
		auftragstypSchnellId = auftragstyp.getId()
		auftragstyp = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_AUFTRAGSTYP, auftragstypSchnellId);
		auftragstyp.setAttribute('auftragstyp', 'Fast')
		clientEn.save(auftragstyp);

		EntityObject<Long> artikeltyp = new EntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKELTYP)
		artikeltyp.setAttribute('artikeltyp', 'Spielzeug')
		clientDe.save(artikeltyp);
		artikeltypSpielzeugId = artikeltyp.getId()
		artikeltyp = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKELTYP, artikeltypSpielzeugId)
		artikeltyp.setAttribute('artikeltyp', 'Toys')
		clientEn.save(artikeltyp);

		EntityObject<Long> artikelTeddybaer = new EntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		artikelTeddybaer.setAttribute('artikeltyp', [id: artikeltypSpielzeugId])
		artikelTeddybaer.setAttribute('artikelname', 'Teddybär')
		clientDe.save(artikelTeddybaer);
		artikelTeddybaerId = artikelTeddybaer.getId()
		artikelTeddybaer = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKEL, artikelTeddybaerId)
		artikelTeddybaer.setAttribute('artikelname', 'Teddy bear')
		clientEn.save(artikelTeddybaer);

		EntityObject<Long> artikelMatchboxAuto = new EntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		artikelMatchboxAuto.setAttribute('artikeltyp', [id: artikeltypSpielzeugId])
		artikelMatchboxAuto.setAttribute('artikelname', 'Matchbox Auto')
		clientDe.save(artikelMatchboxAuto);
		artikelMatchboxAutoId = artikelMatchboxAuto.getId()
		artikelMatchboxAuto = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKEL, artikelMatchboxAutoId)
		artikelMatchboxAuto.setAttribute('artikelname', 'Matchbox car')
		clientEn.save(artikelMatchboxAuto);

		EntityObject<Long> auftragNachbarschaft = new EntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG)
		auftragNachbarschaft.setAttribute('auftragstyp', [id: auftragstypSchnellId])
		auftragNachbarschaft.setAttribute('hauptartikel', [id: artikelMatchboxAutoId])
		auftragNachbarschaft.setAttribute('auftragsbezeichnung', 'Nachbarschaft')
		clientDe.save(auftragNachbarschaft);
		auftragNachbarschaftId = auftragNachbarschaft.getId()
		auftragNachbarschaft = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG, auftragNachbarschaftId)
		auftragNachbarschaft.setAttribute('auftragsbezeichnung', 'Neighbourhood')
		clientEn.save(auftragNachbarschaft);


		EntityObject<Long> posNachbarschaft1 = new EntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGPOS)
		posNachbarschaft1.setAttribute('datalangauftrag', [id: auftragNachbarschaftId])
		posNachbarschaft1.setAttribute('artikel', [id: artikelMatchboxAutoId])
		posNachbarschaft1.setAttribute('menge', 1)
		clientDe.save(posNachbarschaft1);
		posNachbarschaft1Id = posNachbarschaft1.getId()

		EntityObject<Long> maschine = new EntityObject(TestEntities.EXAMPLE_REST_MASCHINE)
		maschine.setAttribute('maschinenname', 'Motorsäge')
		clientDe.save(maschine);
		maschinenId = maschine.getId()
		maschine = clientDe.getEntityObject(TestEntities.EXAMPLE_REST_MASCHINE, maschinenId);
		maschine.setAttribute('maschinenname', 'Chainsaw')
		clientEn.save(maschine);

		EntityObject<Long> maschine2 = new EntityObject(TestEntities.EXAMPLE_REST_MASCHINE)
		maschine2.setAttribute('maschinenname', 'Motor M1')
		clientDe.save(maschine2);
		maschine2 = clientDe.getEntityObject(TestEntities.EXAMPLE_REST_MASCHINE, maschine2.getId());
		maschine2.setAttribute('maschinenname', 'Engine M1')
		clientEn.save(maschine2);

		EntityObject<Long> maschinenteil = new EntityObject(TestEntities.EXAMPLE_REST_MASCHINENTEIL)
		maschinenteil.setAttribute('bauteilname', 'Motor M1')
//		maschinenteil.setAttribute('bauteilnummer', '271828')
		maschinenteil.setAttribute('parent', [id: maschinenId])
		maschinenteil.setAttribute('maschine', [id: maschine2.getId()])
		clientDe.save(maschinenteil)
		maschinenTeilId = maschinenteil.getId()
		maschinenteil = clientDe.getEntityObject(TestEntities.EXAMPLE_REST_MASCHINENTEIL, maschinenTeilId);
		maschinenteil.setAttribute('bauteilname', 'Engine M1')
		clientEn.save(maschinenteil)
	}

	@Test
	void _00_setup() {
		setupDataLanguageTestData()
	}

	@Test
	void _01_testResultlist_de() {
		List<EntityObject<Long>> datalangAuftragList = clientDe.getEntityObjects(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG)
		assert datalangAuftragList.size() == 1
		assert datalangAuftragList.getAt(0).getAttribute("auftragsbezeichnung") == 'Nachbarschaft'
		assert datalangAuftragList.getAt(0).getAttribute("auftragstyp").getAt("name") == 'Schnell'
		assert datalangAuftragList.getAt(0).getAttribute("hauptartikel").getAt("name") == 'Spielzeug: Matchbox Auto'
	}

	@Test
	void _02_testResultlist_en() {
		List<EntityObject<Long>> datalangAuftragList = clientEn.getEntityObjects(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG)
		assert datalangAuftragList.size() == 1
		assert datalangAuftragList.getAt(0).getAttribute("auftragsbezeichnung") == 'Neighbourhood'
		assert datalangAuftragList.getAt(0).getAttribute("auftragstyp").getAt("name") == 'Fast'
		assert datalangAuftragList.getAt(0).getAttribute("hauptartikel").getAt("name") == 'Toys: Matchbox car'
	}

	@Test
	void _03_testDetails_de() {
		EntityObject<Long> datalangAuftrag = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG, auftragNachbarschaftId)
		assert datalangAuftrag.getAttribute("auftragsbezeichnung") == 'Nachbarschaft'
		assert datalangAuftrag.getAttribute("auftragstyp").getAt("name") == 'Schnell'
		assert datalangAuftrag.getAttribute("hauptartikel").getAt("name") == 'Spielzeug: Matchbox Auto'
	}

	@Test
	void _04_testDetails_de() {
		EntityObject<Long> datalangAuftrag = clientEn.getEntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG, auftragNachbarschaftId)
		assert datalangAuftrag.getAttribute("auftragsbezeichnung") == 'Neighbourhood'
		assert datalangAuftrag.getAttribute("auftragstyp").getAt("name") == 'Fast'
		assert datalangAuftrag.getAttribute("hauptartikel").getAt("name") == 'Toys: Matchbox car'
	}

	@Test
	void _05_testReferenceList_de() {
		JSONArray reflist = RESTHelper.getReferenceList(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG.fqn + '_hauptartikel', clientDe);
		assert reflist.size() == 2
		assert reflist.getAt(0).getAt("name") == 'Spielzeug: Matchbox Auto'
		assert reflist.getAt(1).getAt("name") == 'Spielzeug: Teddybär'
	}

	@Test
	void _06_testReferenceList_en() {
		JSONArray reflist = RESTHelper.getReferenceList(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG.fqn + '_hauptartikel', clientEn);
		assert reflist.size() == 2
		assert reflist.getAt(0).getAt("name") == 'Toys: Matchbox car'
		assert reflist.getAt(1).getAt("name") == 'Toys: Teddy bear'
	}

	@Test
	void _07_testVlpData_de() {
		JSONArray vlpdata = RESTHelper.getVlpData(TestEntities.NUCLET_TEST_I18N_DATALANGPOS.fqn + '_artikel', '', 'D7ChzSO8q0pw9abMRIRd', 'intid', 'vlpconcat', auftragNachbarschaftId.toString(), clientDe)
		assert vlpdata.size() == 2
		// Sortierung in Datenquelle ist DESC (ACHTUNG: funktioniert nur im Restservice! Der Desktop wrapped das VLP Statement noch mit einer Standardlogik NUCLOS-5545)
		assert vlpdata.getAt(0).getAt("name") == 'Spielzeug: Teddybär'
		assert vlpdata.getAt(1).getAt("name") == 'Spielzeug: Matchbox Auto'
	}

	@Test
	void _08_testVlpData_en() {
		JSONArray vlpdata = RESTHelper.getVlpData(TestEntities.NUCLET_TEST_I18N_DATALANGPOS.fqn + '_artikel', '', 'D7ChzSO8q0pw9abMRIRd', 'intid', 'vlpconcat', auftragNachbarschaftId.toString(), clientEn)
		assert vlpdata.size() == 2
		// Sortierung in Datenquelle ist DESC (ACHTUNG: funktioniert nur im Restservice! Der Desktop wrapped das VLP Statement noch mit einer Standardlogik NUCLOS-5545)
		assert vlpdata.getAt(0).getAt("name") == 'Toys: Teddy bear'
		assert vlpdata.getAt(1).getAt("name") == 'Toys: Matchbox car'
	}

	@Test
	void _09_setDataLanguage() {
		List<EntityObject<Long>> datalangAuftragList = clientDe.getEntityObjects(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG)
		assert datalangAuftragList.size() == 1
		assert datalangAuftragList.getAt(0).getAttribute("auftragsbezeichnung") == 'Nachbarschaft'
		assert datalangAuftragList.getAt(0).getAttribute("auftragstyp").getAt("name") == 'Schnell'
		assert datalangAuftragList.getAt(0).getAttribute("hauptartikel").getAt("name") == 'Spielzeug: Matchbox Auto'

		clientDe.loginParams.datalanguage = 'en_GB'
		clientDe.setDataLanguage()
		assert clientDe.getDataLanguage() == 'en_GB'

		datalangAuftragList = clientDe.getEntityObjects(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG)
		assert datalangAuftragList.size() == 1
		assert datalangAuftragList.getAt(0).getAttribute("auftragsbezeichnung") == 'Neighbourhood'
		assert datalangAuftragList.getAt(0).getAttribute("auftragstyp").getAt("name") == 'Fast'
		assert datalangAuftragList.getAt(0).getAttribute("hauptartikel").getAt("name") == 'Toys: Matchbox car'

		clientDe.loginParams.datalanguage = 'tbd'
		clientDe.setDataLanguage()
		assert clientDe.getDataLanguage() == 'de_DE'
	}

	@Test
	void _10_getDataLanguagesTest() {
		JSONArray dataLanguagesDe = RESTHelper.requestJsonArray(RESTHelper.REST_BASE_URL + '/datalanguages/?language=de', HttpMethod.GET, null, null)
		JSONArray dataLanguagesEn = RESTHelper.requestJsonArray(RESTHelper.REST_BASE_URL + '/datalanguages/?language=en', HttpMethod.GET, null, null)

		assert dataLanguagesDe.length() >= 3: "Missing Data languages (must be at least 3)"
		assert dataLanguagesEn.length() >= 3: "Missing Data languages (must be at least 3)"

		assert ((JSONObject)dataLanguagesDe[0]).get('datalanguage') == 'de_DE': "The first data language must be de_DE"
		assert ((JSONObject)dataLanguagesDe[1]).get('datalanguage') == 'sv_SE': "The second data language must be sv_SE"
		assert ((JSONObject)dataLanguagesDe[2]).get('datalanguage') == 'en_GB': "The third data language must be en_GB"

		assert ((JSONObject)dataLanguagesDe[0]).get('datalanguageLabel') == 'Deutsch (DE)': "The first data language label must be \"Deutsch (DE)\" for language request \"de\""
		assert ((JSONObject)dataLanguagesDe[1]).get('datalanguageLabel') == 'Schwedisch (SE)': "The second data language label must be \"Schwedisch (SE)\" for language request \"de\""
		assert ((JSONObject)dataLanguagesDe[2]).get('datalanguageLabel') == 'Englisch (GB)': "The third data language label must be \"Englisch (GB)\" for language request \"de\""

		assert ((JSONObject)dataLanguagesEn[0]).get('datalanguageLabel') == 'German (DE)': "The first data language label must be \"German (DE)\" for language request \"en\""
		assert ((JSONObject)dataLanguagesEn[1]).get('datalanguageLabel') == 'Swedish (SE)': "The second data language label must be \"Swedish (SE)\" for language request \"en\""
		assert ((JSONObject)dataLanguagesEn[2]).get('datalanguageLabel') == 'English (GB)': "The third data language label must be \"English (GB)\" for language request \"en\""
	}

	@Test
	void _11_testCollectiveProcessingActions() {
		String payload = '{"where":{"aliases":{},"clause":""},"multiSelectionCondition":{"allSelected":true}}'

		JSONObject response = RESTHelper.requestJson(RESTHelper.REST_BASE_URL
				+ '/collectiveProcessing/nuclet_test_i18n_Datalangauftrag?search=nach&attributes=nuclosStateIcon,auftragsbezeichnung,hauptartikel,auftragstyp',
				HttpMethod.POST, clientDe.sessionId, payload)

		assert ((JSONObject) response.getJSONArray('actions')[0]).get('name') == 'delete'
	}

	@Test
	void _12_testSubformData() {
		clientDe.loginParams.datalanguage = 'en_GB'
		clientDe.setDataLanguage()

		EntityObject<Long> datalangAuftrag = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG, auftragNachbarschaftId)
		List<EntityObject<Long>> pos = datalangAuftrag.loadDependents(TestEntities.NUCLET_TEST_I18N_DATALANGPOS, 'datalangauftrag')
		assert pos[0].getAttribute('artikel')['name'] == 'Toys: Matchbox car'

		clientDe.loginParams.datalanguage = 'de_DE'
		clientDe.setDataLanguage()

		datalangAuftrag = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG, auftragNachbarschaftId)
		pos = datalangAuftrag.loadDependents(TestEntities.NUCLET_TEST_I18N_DATALANGPOS, 'datalangauftrag')
		assert pos[0].getAttribute('artikel')['name'] == 'Spielzeug: Matchbox Auto'
	}

	@Test
	void _13_testUpdate() {
		EntityObject<Long> datalangAuftrag = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG, auftragNachbarschaftId)
		assert datalangAuftrag.getAttribute('hauptartikel')['name'] == 'Spielzeug: Matchbox Auto'

		datalangAuftrag.setAttribute('auftragsbezeichnung', 'Nachbarschaft 2')
		datalangAuftrag.save()
		assert datalangAuftrag.getAttribute('hauptartikel')['name'] == 'Spielzeug: Matchbox Auto'

		datalangAuftrag = clientEn.getEntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG, auftragNachbarschaftId)
		assert datalangAuftrag.getAttribute('hauptartikel')['name'] == 'Toys: Matchbox car'

		datalangAuftrag.setAttribute('auftragsbezeichnung', 'Neighborhood 2')
		datalangAuftrag.save()
		assert datalangAuftrag.getAttribute('hauptartikel')['name'] == 'Toys: Matchbox car'
	}

	@Test
	void _14_testNUCLOS_10841() {
		EntityObject<Long> maschine = clientDe.getEntityObject(TestEntities.EXAMPLE_REST_MASCHINE, maschinenId)
		List<EntityObject<Long>> teile = maschine.loadDependents(TestEntities.EXAMPLE_REST_MASCHINENTEIL, 'parent')
		Map<String, Object> attributes = teile[0].getAttributes()
		assert !attributes['parent']['name'].toString().contains("Motor M1"): 'Translation should not get mixed up'
		assert !attributes['maschine']['name'].toString().contains("Motorsäge"): 'Translation should not get mixed up'
	}

}
