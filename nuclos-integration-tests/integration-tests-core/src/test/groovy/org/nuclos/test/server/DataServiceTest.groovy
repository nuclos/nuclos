package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.apache.commons.io.IOUtils
import org.junit.Test
import org.nuclos.common.UID
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

public class DataServiceTest extends AbstractNuclosTest {

	public UID existingResourceUid = new UID("ncN9Ud5sC8gdWvuADcuY")
	public UID nonExistentResourceUid = new UID("nonExistent")

	@Test
	void _01_testGetResource() {
		RESTClient restClient = new RESTClient('nuclos', '')
		restClient.login()

		testResourcesWithNameLessThan3CharactersAvailable: {
			// Tests fix NUCLOS-9758
			File output = getResource(existingResourceUid, restClient)
			assert ['Test Content'] == output.readLines(): 'NUCLOS-9758 fix not working anymore'
		}
		testResourceNotFoundIs404InsteadOfNullPointerCausingInternalError: {
			String nonExistentResourcePath = getResourcePath(nonExistentResourceUid)
			restClient.getUrl(nonExistentResourcePath).with {
				assert it.statusLine.statusCode == Response.Status.NOT_FOUND.statusCode
				assert IOUtils.toString(it.getEntity().content).contains('Not Found')
			}
		}
	}

	String getResourcePath(UID resourceUid) {
		RESTHelper.REST_BASE_URL + "/data/resource/" + resourceUid
	}

	File getResource(UID resourceUid, RESTClient restClient) {
		String url = getResourcePath(resourceUid)
		RESTHelper.downloadFile(url.toURL(), restClient)
	}

}
