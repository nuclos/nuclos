package org.nuclos.test.server


import java.security.Security
import java.util.regex.Pattern

import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.nuclos.common.PortAllocator
import org.nuclos.common.mail.NuclosMail
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.SystemEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTHelper

import com.icegreen.greenmail.user.GreenMailUser
import com.icegreen.greenmail.util.DummySSLSocketFactory
import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.ServerSetup
import com.icegreen.greenmail.util.ServerSetupTest

class EmailTest extends AbstractNuclosTest {
	private static Set<ServerSetup> serverSetups = new HashSet<>()
	private static GreenMail greenMail
	private static GreenMailUser userA
	private static GreenMailUser userB
	private static GreenMailUser userC
	private static NuclosMail exampleMail

	private static Pattern pReceivedEmails = ~/Received (\d+) mail\(s\)\./

	@BeforeClass
	static void setup() {
		AbstractNuclosTest.setup()

		exampleMail = new org.nuclos.common.mail.NuclosMail()
		exampleMail.addRecipients(Arrays.asList("user.a@nuclos.de"))
		exampleMail.addRecipientCC("user.b@nuclos.de;user.cc2@nuclos.de")
		exampleMail.setSubject("Example Subject")
		exampleMail.setMessage("Example Mail Message")
		exampleMail.setReplyTo("example.mail@nuclos.de")

		serverSetups.clear()
		// Copy the default GreenMail setups and make some adjustments
		int order = 1
		for (ServerSetup setup : ServerSetupTest.SMTP_POP3_IMAP) {
			final int port
			try {
				port = PortAllocator.allocate()
			} catch (IOException e) {
				throw new RuntimeException(e)
			}

			final ServerSetup newSetup = new ServerSetup(port, context.nuclosServerHost, setup.getProtocol())
			newSetup.setServerStartupTimeout(10000)

			serverSetups.add(newSetup)

			final SystemEntities entity
			if (setup.getProtocol().startsWith("smtp")) {
				entity = SystemEntities.EMAIL_OUTGOING_SERVER
			} else {
				entity = SystemEntities.EMAIL_INCOMING_SERVER
			}

			EntityObject<String> server = new EntityObject<>(entity)
			server.setAttribute("name", setup.getProtocol())
			server.setAttribute("order", order++)
			server.setAttribute("active", true)
			server.setAttribute("serverName", context.nuclosServerHost)
			server.setAttribute("port", port)
			server.setAttribute("connectionSecurity", "None")
			server.setAttribute("authenticationMethod", "Password")

			if (setup.getProtocol().startsWith("smtp")) {
				server.setAttribute("userName", "a")
				server.setAttribute("password", "a")
				server.setAttribute("sender", "example.mail@nuclos.de")
				server.setAttribute("testRecipient", "user.b@nuclos.de")
			} else {
				if (setup.getProtocol().startsWith("imap")) {
					server.setAttribute("userName", "b")
					server.setAttribute("password", "b")
					server.setAttribute("type", "IMAP")
					server.setAttribute("folderFrom", "INBOX")
					server.setAttribute("folderTo", "Trash")
				} else {
					server.setAttribute("userName", "c")
					server.setAttribute("password", "c")
					server.setAttribute("type", "POP3")
				}
			}

			getNuclosSession().save(server)
		}

		final Map<String, String> sysParams = new HashMap<>()
		sysParams.put("SMTP Server", "127.0.0.1")
		sysParams.put("SMTP Port", "3025")
		sysParams.put("SMTP Username", "c")
		sysParams.put("SMTP Password", "c")
		sysParams.put("SMTP Sender", "user.c@nuclos.de")
		sysParams.put("SMTP Authentication", "Y")

		sysParams.put("IMAP Server", "127.0.0.1")
		sysParams.put("IMAP Port", "3143")
		sysParams.put("IMAP Username", "c")
		sysParams.put("IMAP Password", "c")
		sysParams.put("IMAP Protocol", "Y")

		getNuclosSession().setSystemParameters(sysParams)

		System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2")
		Security.setProperty("ssl.SocketFactory.provider", DummySSLSocketFactory.class.getName())
		greenMail = new GreenMail(serverSetups.toArray(new ServerSetup[]{}))

		//Start all email servers using non-default ports.
		greenMail.start()
		userA = greenMail.setUser("user.a@nuclos.de", "a", "a")
		userB = greenMail.setUser("user.b@nuclos.de", "b", "b")
		userC = greenMail.setUser("user.b@nuclos.de", "c", "c")
	}

	@AfterClass
	static void teardown() {
		greenMail.stop()
	}

	@Test
	void testSendAndReceiveEmail() {
		final List<EntityObject<String>> oServers = getNuclosSession().getEntityObjects(SystemEntities.EMAIL_OUTGOING_SERVER, new QueryOptions(where: 'org_nuclos_system_EmailOutgoingServer_active = 1'))

		for (EntityObject<String> oServer : oServers) {
			int receivedEmails = receiveEmail(1, userB.getLogin(), false)
			getNuclosSession().executeCustomRule(oServer, "org.nuclos.businessentity.rule.EmailOutgoingServerValidationRule")
			assert oServer.getAttribute("testResult") == "Email sent to " + userB.getEmail()
			receiveEmail(receivedEmails + 1, userB.getLogin())
		}
	}

	@Test
	void testEmailNotificationForJob() {
		EntityObject<String> userTest = RESTHelper.findUserByUsername('test', nuclosSession)
		userTest.setAttribute('email', userB.getEmail())
		userTest.save()

		EntityObject<String> job = nuclosSession.getEntityObject(SystemEntities.JOBCONTROLLER, 'nuclet_test_other_TestEmailNotificationJOB')
		job.setAttribute('user', [id: userTest.getId()])
		job.setAttribute('level', 'bei Info')
		job.save()

		nuclosSession.executeCustomRule(job, 'org.nuclos.businessentity.rule.JobControllerExecuteRule')
		// job execution needs some time
		sleep(1000)
		receiveEmail(1, userB.getLogin())

		deactivateAllOutgoingEmailServers()
		nuclosSession.executeCustomRule(job, 'org.nuclos.businessentity.rule.JobControllerExecuteRule')
		// job execution needs some time
		sleep(1000)
		receiveEmail(1, userB.getLogin())
		receiveEmail(1, userC.getLogin())
	}

	private int receiveEmail(int emailCount, String user, boolean bAssert = true) {
		final List<EntityObject<String>> iServers = getNuclosSession().getEntityObjects(SystemEntities.EMAIL_INCOMING_SERVER, new QueryOptions(where: "org_nuclos_system_EmailIncomingServer_active = 1 AND org_nuclos_system_EmailIncomingServer_userName = '${user}'"))

		assert !iServers.empty

		int received = 0
		for (EntityObject<String> iServer : iServers) {
			getNuclosSession().executeCustomRule(iServer, "org.nuclos.businessentity.rule.EmailIncomingServerValidationRule")
			def matcher = pReceivedEmails.matcher(((String) iServer.getAttribute("testResult")))
			assert matcher.matches()
			received = matcher.group(1) as int

			if (bAssert) {
				assert received == emailCount
			}
		}

		return received
	}

	private void deactivateAllOutgoingEmailServers() {
		final List<EntityObject<String>> oServers = getNuclosSession().getEntityObjects(SystemEntities.EMAIL_OUTGOING_SERVER, new QueryOptions(where: 'org_nuclos_system_EmailOutgoingServer_active = 1'))

		oServers.forEach {
			it.setAttribute('active', false)
			it.save()
		}
	}

	private void activateAllOutgoingEmailServers() {
		final List<EntityObject<String>> oServers = getNuclosSession().getEntityObjects(SystemEntities.EMAIL_OUTGOING_SERVER, new QueryOptions(where: 'org_nuclos_system_EmailOutgoingServer_active = 0'))

		oServers.forEach {
			it.setAttribute('active', true)
			it.save()
		}
	}
}
