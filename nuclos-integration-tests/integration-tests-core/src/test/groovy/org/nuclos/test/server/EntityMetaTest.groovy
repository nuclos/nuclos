package org.nuclos.test.server

import org.json.JSONObject
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.SuperuserRESTClient

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class EntityMetaTest extends AbstractNuclosTest {

	@Test
	void _testRenamingOfNuclosProcessAttribute() {
		SuperuserRESTClient suDeSession = new SuperuserRESTClient('nuclos', '', Locale.GERMAN).login()
		SuperuserRESTClient suEnSession = new SuperuserRESTClient('nuclos', '', Locale.ENGLISH).login()
		JSONObject metaDe = suDeSession.getEntityMeta(TestEntities.EXAMPLE_REST_RECHNUNG)
		JSONObject metaEn = suEnSession.getEntityMeta(TestEntities.EXAMPLE_REST_RECHNUNG)

		JSONObject attributesDe = metaDe.getJSONObject('attributes')
		def processAttributeDe = attributesDe.getJSONObject('nuclosProcess')
		assert processAttributeDe.getString('name') == 'Rechnungstyp'
		assert processAttributeDe.getString('description') == '"Aktion" ist nicht für eine Rechnung geeignet'

		JSONObject attributesEn = metaEn.getJSONObject('attributes')
		def processAttributeEn = attributesEn.getJSONObject('nuclosProcess')
		assert processAttributeEn.getString('name') == 'Order type'
		assert processAttributeEn.getString('description') == '"Process" is not suitabel for an invoice'
	}

}
