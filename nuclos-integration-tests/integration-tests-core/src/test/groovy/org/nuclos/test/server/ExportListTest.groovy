package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.BeforeClass
import org.junit.Test
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper

import com.google.common.collect.Iterators
import com.opencsv.CSVIterator
import com.xlson.groovycsv.CsvIterator
import com.xlson.groovycsv.CsvParser;

public class ExportListTest extends AbstractNuclosTest {

	@BeforeClass
	static void initData() {
		RESTHelper.createUser('Test_w-o_export', 'test', ['Example user w/o Export'], nuclosSession)
		def testClient = new RESTClient('test', 'test')
		testClient.login()
		TestDataHelper.insertTestData(testClient)
		testClient.logout()
	}

	@Test
	void exportListNotAllowed() {
		def client = new RESTClient('Test_w-o_export', 'test')
		client.login()

		expectErrorStatus(Response.Status.FORBIDDEN, {
			client.exportResultList(TestEntities.EXAMPLE_REST_ARTICLE, "csv")
		})

		def order10020140 = client.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER, new QueryOptions(where: "${TestEntities.EXAMPLE_REST_ORDER.fqn}_orderNumber=10020140"))
				.stream().findFirst().orElse(null)
		assert order10020140 != null : 'Order 100 not found.'
		expectErrorStatus(Response.Status.FORBIDDEN, {
			client.exportSubBoList(TestEntities.EXAMPLE_REST_ORDER, String.valueOf(order10020140.id), "${TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn}_order", "csv")
		})
	}

	@Test
	void exportList() {
		def client = new RESTClient('test', 'test')
		client.login()

		final File csvArticle = client.exportResultList(TestEntities.EXAMPLE_REST_ARTICLE, "csv")
		CsvIterator iter = CsvParser.parseCsv(new FileReader(csvArticle), separator: ';')
		int size = Iterators.size(iter)
		assert size == 5 : 'There should be 5 \'Articles\' in exported result list'

		def order10020140 = client.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER, new QueryOptions(where: "${TestEntities.EXAMPLE_REST_ORDER.fqn}_orderNumber=10020140"))
				.stream().findFirst().orElse(null)
		assert order10020140 != null : 'Order 10020140 not found.'
		final File csvOrderposition = client.exportSubBoList(TestEntities.EXAMPLE_REST_ORDER,
				String.valueOf(order10020140.id),
				"${TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn}_order",
				"csv")
		iter = CsvParser.parseCsv(new FileReader(csvOrderposition), separator: ';')
		size = Iterators.size(iter)
		assert size == 5 : 'There should be 5 \'Orderpositions\' in exported subBo list'

		final File csvOrderpositionFiltered = client.exportSubBoList(
				TestEntities.EXAMPLE_REST_ORDER,
				String.valueOf(order10020140.id),
				"${TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn}_order",
				"csv", "Comparison:GREATER:price:8400")
		iter = CsvParser.parseCsv(new FileReader(csvOrderpositionFiltered), separator: ';')
		size = Iterators.size(iter)
		assert size == 2 : 'There should be 2 \'Orderpositions\' with \'price > 8400\''
		iter.forEachRemaining({it -> assert Integer.valueOf(it[1]) > 8400 : '\'Orderpositions\' should have \'price > 8400\''})
	}

	//NUCLOS-8764
	@Test
	void exportListForCertainBoNotAllowed() {
		def client = new RESTClient('test', 'test')
		client.login()

		expectErrorStatus(Response.Status.FORBIDDEN, {
			client.exportResultList(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, "csv")
		})

		def customer22222 = client.getEntityObjects(TestEntities.EXAMPLE_REST_CUSTOMER, new QueryOptions(where: "${TestEntities.EXAMPLE_REST_CUSTOMER.fqn}_customerNumber=22222"))
				.stream().findFirst().orElse(null)
		assert customer22222 != null : 'Customer 22222 not found.'
		expectErrorStatus(Response.Status.FORBIDDEN, {
			client.exportSubBoList(TestEntities.EXAMPLE_REST_CUSTOMER, String.valueOf(customer22222.id), "${TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn}_customer", "csv")
		})
	}
}
