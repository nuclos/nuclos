package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class GdprDsgvoTest extends AbstractNuclosTest {

	static RESTClient client

	@Test
	void _09_testRuleExecution() {
		createAndAssertGdprTestApiObject('DefaultMemo', 'defaultMemo', 'test', 'test')
	}

	@Test
	void _10_testOverrideCreatedAndChangedByDuringInsert() {
		createAndAssertGdprTestApiObject('OverrideCreatedAndChangedBy', 'defaultMemo', '*****', '*****')
	}

	@Test
	void _20_testOverrideCreatedAndChangedByDuringUpdate() {
		def bo = createAndAssertGdprTestApiObject('DefaultMemo', 'defaultMemo', 'test', 'test')
		def id = bo.getId()
		assert bo.getVersion() == 1
		setTest(bo, 'OverrideCreatedAndChangedBy')
		bo = getTestApiBusinessObject(bo.save())
		assert id == bo.getId()
		assert bo.getVersion() == 1
		assertGdprTestApiObject(bo, 'defaultMemo', '*****', '*****')
	}

	@Test
	void _30_testNoRuleExecution() {
		def bo = createAndAssertGdprTestApiObject('NoRuleExecution', 'defaultMemo', 'test', 'test')
		assert bo.getAttribute('123') != null
		def boNoRuleExecution = getTestApiBusinessObject(Long.valueOf((String) bo.getAttribute('123')))
		assertGdprTestApiObject(boNoRuleExecution, null, 'test', 'test')
	}

	@Test
	void _40_testOverrideCreatedAndChangedByDuringUpdate_WithinJob() {
		def bo = createAndAssertGdprTestApiObject('WithinJob_40', 'defaultMemo', 'test', 'test')
		long boId = bo.getId()
		assert nuclosSession.startJob('nuclet_test_rules_TestJobJOB') == 'ok'
		EntityObject<Long> updatedBo
		waitUntilTrue({
			updatedBo = getTestApiBusinessObject(boId)
			updatedBo.getAttribute('createdBy') == '*****'
		})
		assertGdprTestApiObject(updatedBo, 'gdprDsgvoWithinJob_40Updated', '*****', '*****')
		assert updatedBo != null && updatedBo.getVersion() == 1
	}

	@Test
	void _49_testRuleExecution_WithinJob() {
		def bo = createAndAssertGdprTestApiObject('WithinJob_49', 'defaultMemo', 'test', 'test')
		long boId = bo.getId()
		assert nuclosSession.startJob('nuclet_test_rules_TestJobJOB') == 'ok'
		EntityObject<Long> updatedBo
		waitUntilTrue({
			updatedBo = getTestApiBusinessObject(boId)
			updatedBo.getAttribute('test') == 'gdprDsgvoValidateRuleExecution'
		})
		assertGdprTestApiObject(updatedBo, 'gdprDsgvoValidateRuleExecutionUpdated', 'test', 'nuclos')
		assert updatedBo != null && updatedBo.getVersion() == 2
	}

	@Test
	void _50_testNoRuleExecution_WithinJob() {
		def bo = createAndAssertGdprTestApiObject('WithinJob_50', 'defaultMemo', 'test', 'test')
		long boId = bo.getId()
		assert nuclosSession.startJob('nuclet_test_rules_TestJobJOB') == 'ok'
		EntityObject<Long> updatedBo
		waitUntilTrue({
			updatedBo = getTestApiBusinessObject(boId)
			updatedBo.getAttribute('memo') == 'MemoFromJobNotOverridden'
		})
		assertGdprTestApiObject(updatedBo, 'MemoFromJobNotOverridden', 'test', 'nuclos')
		assert updatedBo != null && updatedBo.getVersion() == 2
	}

	@Test
	void _60_testAllBoClasses() {
		def bo = getTestApiBusinessObject(createGdprTestApiObject('AllBoClasses'))
		def allBoClassesFromApi = ((String)bo.getAttribute('memo')).split(', ')
		assert allBoClassesFromApi.contains('nuclet.test.rules.TestAPI'), 'Normales Businessobjekt fehlt in der Liste normaler BoClasses'
		assert allBoClassesFromApi.contains('example.rest.WriteProxyData'), 'Normales Businessobjekt fehlt in der Liste normaler BoClasses'
		assert !allBoClassesFromApi.contains('example.rest.WriteProxyTest'), 'VirtualEntity ist in der Liste normaler BoClasses'
		assert !allBoClassesFromApi.contains('nuclet.test.other.LagerArtikel'), 'Integrationspunkt ist in der Liste normaler BoClasses'
		assert !allBoClassesFromApi.contains('nuclet.test.other.TestProxyProxy'), 'Proxy Businessobjekt ist in der Liste normaler BoClasses'
		assert !allBoClassesFromApi.contains('nuc.a.GenerischerVorgang'), 'GenericBusinessObject ist in der Liste normaler BoClasses'
		assert !allBoClassesFromApi.contains('example.rest.MyOrdersAndCustomersDYN'), 'DynamicEntity ist in der Liste normaler BoClasses'
		assert !allBoClassesFromApi.contains('example.rest.CategorySalesCustomerCRT'), 'Chart ist in der Liste normaler BoClasses'
		assert !allBoClassesFromApi.contains('example.rest.DynTasksOrderAndCustomerDTL'), 'DynamicTaskList ist in der Liste normaler BoClasses'
		assert !allBoClassesFromApi.contains('org.nuclos.system'), 'Nuclos System Businessobjekt ist in der Liste normaler BoClasses'
		assert !allBoClassesFromApi.contains('org.nuclet.businessentity.ArtikelLANG'), 'Nuclos interne Datensprache Entity ist in der Liste normaler BoClasses'
	}

	@Test
	void _70_testChunkAndOffset() {
		def bo = getTestApiBusinessObject(createGdprTestApiObject('WithinJob_70'))
		long boId = bo.getId()
		assert nuclosSession.startJob('nuclet_test_rules_TestJobJOB') == 'ok'
		EntityObject<Long> updatedBo
		waitUntilTrue({
			updatedBo = getTestApiBusinessObject(boId)
			updatedBo.getAttribute('123') == '200'
		})
		// 200 Einträge wurden vom Job erzeugt
		bo = getTestApiBusinessObject(createGdprTestApiObject('ChunkOffset_50_50'))
		String memo = bo.getAttribute('memo')
		String[] memoParts = memo.split('; ')
		int first = -1
		int last = -1
		int size = -1
		String list = null
		for (int i = 0; i < memoParts.size(); i++) {
			def partKeyValue = memoParts[i].split('=')
			if (partKeyValue.size() == 2) {
				String key = partKeyValue[0]
				String value = partKeyValue[1]
				if ('First'.equals(key)) {
					first = Integer.valueOf(value)
				} else if ('Last'.equals(key)) {
					last = Integer.valueOf(value)
				} else if ('Size'.equals(key)) {
					size = Integer.valueOf(value)
				} else if ('List'.equals(key)) {
					list = value
				}
			}
		}
		assert first == 50, 'Offset ist nicht korrekt, erwartet ' + 50 + ' erhalten ' + first + '. List=' + list
		assert size == 50, 'ChunkSize ist nicht korrekt, erwartet ' + 50 + ' erhalten ' + size + '. List=' + list
		assert last == 99, 'Anscheinend gibt es Lücken im Result, Letzter Erwarteter ist ' + 99 + ' und gemeldet wurde ' + last + '. List=' + list
	}

	@Test
	void _80_testDeleteSessionEntriesTask() {
		nuclosSession.setSystemParameters(['PRIVACY_DELETE_SESSIONLOG_AFTER_PERIOD':'0'])
		def bo = getTestApiBusinessObject(createGdprTestApiObject('DeleteSessionEntriesTask'))
		String _123Value = bo.getAttribute('123')
		assert Integer.valueOf(_123Value) < 0, 'Es wurden keine Session Einträge gelöscht! Anzahl gelöscht vom Server: ' + _123Value
	}

	@Test
	void _90_testHistoryAnonymizerTask() {
		// memo History Einträge erzeugen
		def boTestData = createAndAssertGdprTestApiObject('HistoryAnonymizerTaskData', 'defaultMemo', 'test', 'test')
		boTestData.setAttribute('123', '007')
		boTestData = getTestApiBusinessObject(boTestData.save())
		assert boTestData.getAttribute('memo') == 'gdprDsgvoHistoryAnonymizerTaskDataUpdated'
		def bo = getTestApiBusinessObject(createGdprTestApiObject('HistoryAnonymizerTask'))
		String _123Value = bo.getAttribute('123')
		assert Integer.valueOf(_123Value) > 0, 'Es wurden keine History Einträge anonymisiert! Anzahl anonymisiert vom Server: ' + _123Value
	}

	@Test
	void _100_testDeleteUser() {
		def deleteUserTestUser = RESTHelper.createUser('DeleteMe', 'DeleteMe', ['Example user'], nuclosSession)
		RESTClient clientDeleteUserTest = new RESTClient('DeleteMe', 'DeleteMe').login()
		Long boId = createGdprTestApiObject("DeleteUserTest")
		// noch ist der User vorhanden und ein lesen des BO möglich
		def bo = clientDeleteUserTest.getEntityObject(TestEntities.NUCLET_TEST_RULES_TESTAPI, boId)
		// jetzt wird gelöscht
		bo.setAttribute('refuser', [id: deleteUserTestUser.getId()])
		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			// "Der aktuell angemeldete Nutzer kann nicht gelöscht werden."
			bo.save()
		}
		bo = getTestApiBusinessObject(bo.getId())
		bo.setAttribute('refuser', [id: deleteUserTestUser.getId()])
		bo.save()
		assert bo.getAttribute('memo') == String.format('User %s deleted.', deleteUserTestUser.getAttribute('username'))
		expectErrorStatus(Response.Status.UNAUTHORIZED) {
			clientDeleteUserTest.getEntityObject(TestEntities.NUCLET_TEST_RULES_TESTAPI, boId)
		}
	}

	static EntityObject<Long> createAndAssertGdprTestApiObject(String test, String assertMemo, String assertCreatedBy, String assertChangedBy) {
		long boId = createGdprTestApiObject(test)
		def bo = getTestApiBusinessObject(boId)
		assertGdprTestApiObject(bo, assertMemo, assertCreatedBy, assertChangedBy)
		return bo;
	}

	static assertGdprTestApiObject(EntityObject<Long> bo, String memo, String createdBy, String changedBy) {
		assert bo.getAttribute('memo') == memo
		assert bo.getAttribute('createdBy') == createdBy
		assert bo.getAttribute('changedBy') == changedBy
	}

	static EntityObject<Long> getTestApiBusinessObject(long boId) {
		client.getEntityObject(TestEntities.NUCLET_TEST_RULES_TESTAPI, boId)
	}

	static long createGdprTestApiObject(String test) {
		client = new RESTClient('test', 'test').login()

		EntityObject<Long> boTestAPI = new EntityObject<>(TestEntities.NUCLET_TEST_RULES_TESTAPI)
		setTest(boTestAPI, test)

		client.save(boTestAPI)
	}

	static void setTest(EntityObject<Long> boTestAPI, String test) {
		boTestAPI.setAttribute('test', 'gdprDsgvo' + test)
	}

}
