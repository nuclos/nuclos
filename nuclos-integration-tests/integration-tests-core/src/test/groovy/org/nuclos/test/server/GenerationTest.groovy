package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.EntityReference
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.springframework.web.client.RestClientException

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class GenerationTest extends AbstractNuclosTest {

	@Test
	void _05_exceptionForWrongProcess() {
		EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_RULES_TESTGENERATORWITHPROCESS)
		eo.setAttribute("name", "generated")
		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			nuclosSession.save(eo)
		}
	}

	@Test
	void _10_insertAndGenerate() {
		EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_RULES_TESTGENERATORWITHPROCESS)
		eo.setAttribute("name", "test")
		nuclosSession.save(eo)

		EntityObject<Long> resultEo = nuclosSession.generateObject(
				eo,
				'nuclet_test_rules_GeneratorWithProcessGEN',
				TestEntities.NUCLET_TEST_RULES_TESTGENERATORWITHPROCESS
		)

		EntityReference<?> reference = resultEo.getReference('nuclosProcess')
		assert reference.id == 'nuclet_test_rules_TestGeneratorWithProcess_GeneratorProcess2'
		assert reference.name == 'Generator Process 2'
	}

	@Test
	void _11_insertAndGenerateWithCompleteDependents() {
		// Create the main EO
		EntityObject eo = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)
		eo.setAttribute('name', 'Complete Dependent Test')
		eo.setAttribute('mandatorytext', 'Test')

		List<EntityObject> dependents = [];
		2.times {
			EntityObject dep = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT)
			dep.setAttribute('name', 'Dependent ' + it)
			dep.setAttribute('mandatorytext', it)
			dependents.add(dep)
		}
		eo.setDependents([(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT.toString() + '_' + 'testobjektgen'): dependents] as Map<String, List<EntityObject<?>>>)

		nuclosSession.save(eo)

		EntityObject generatedEo = eo.generateObject('nuclet_test_other_TestObjektgeneratorGEN',
				TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR
		)

		List<EntityObject<Long>> testObjektGeneratorDependents = nuclosSession.loadDependents(generatedEo, TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT.toString() + '_' + 'testobjektgen')

		assert testObjektGeneratorDependents.size() == 2

		testObjektGeneratorDependents.sort{eo1, eo2 ->
			(eo1.getAttribute('name') as String).compareTo(eo2.getAttribute('name') as String)
		}

		testObjektGeneratorDependents.eachWithIndex { dep, i ->
			assert dep.getAttribute('name') ==  'Dependent ' + i
			assert dep.getAttribute('mandatorytext') == i as String
		}
	}

	@Test
	void _15_generateIncompleteDependent() {
		// Create the main EO
		EntityObject eo = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)
		eo.setAttribute('name', 'Incomplete Dependent Test')
		eo.setAttribute('mandatorytext', 'Test')

		nuclosSession.save(eo)

		RestClientException expectedException = null
		try {
			eo.generateObject('nuclet_test_other_TestObjektgeneratorDependentWithValidationErrorGEN',
					TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT
			)
		} catch (RestClientException e) {
			expectedException = e
		}

		// There should be a validation error ...
		assert expectedException.message.contains('Valid')

		// ... for the empty "Mandatory Text" field ...
		assert expectedException.message.contains('Mandatory Text')

		// ... without mentioning the subform entity ...
		assert !expectedException.message.contains('Test Objektgenerator Dependent')

		// ... and without mentioning subforms, see NUCLOS-7806
		assert !expectedException.message.contains('Subform')
		assert !expectedException.message.contains('Unterformular')
	}
	
	@Test
	void _20_generateWithoutSaving() {
		EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)
		eo.setAttribute("name", "test without save")
		eo.setAttribute("mandatorytext", 'test')
		nuclosSession.save(eo)

		EntityObject<Long> resultEo = nuclosSession.generateObject(
				eo,
				'nuclet_test_other_TestobjectgeneratorwithoutsavingaftergenerationGEN',
				TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR
		)

		assert resultEo.isNew()
		assert resultEo.getAttribute('valuefromgenrulebefore') == 'before'
		assert resultEo.getAttribute('valuefromgenruleafter').equals(null)
	}
}
