package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.Test
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

class JMSBrokerTest extends AbstractNuclosTest {

	@Test
	void testGET() {
		expectErrorStatus(Response.Status.BAD_REQUEST) {
			RESTHelper.requestString(RESTHelper.BASE_URL + '/jmsbroker', HttpMethod.GET, nuclosSession.sessionId)
		}
	}

	@Test
	void testPOST() {
		expectErrorStatus(Response.Status.BAD_REQUEST) {
			RESTHelper.requestJson(RESTHelper.BASE_URL + '/jmsbroker', HttpMethod.POST, nuclosSession.sessionId, "null")
		}
	}
}
