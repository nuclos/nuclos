package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.rules.ExpectedException
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTException
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LogicalDeletableTest extends AbstractNuclosTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none()

	static RESTClient client

	static Long orderId

	@Test
	void _00_setupUpAccounts() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
	}

	@Test
	void _01_logicalDeletion() {
		client = new RESTClient('test', 'test').login()

		EntityObject<Long> order = new EntityObject<>(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', '123')
		orderId = client.save(order)

		client.executeCustomRule(order, 'example.rest.CustomRuleDelete')

	}

	@Test
	void _02_physicalDeletion() {
		EntityObject<Long> order = client.getEntityObject(TestEntities.EXAMPLE_REST_ORDER, orderId);

		client.executeCustomRule(order, 'example.rest.CustomRuleDelete')
	}
}
