package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.schema.rest.News
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class NewsTest extends AbstractNuclosTest {

	private RESTClient test = new RESTClient('test', 'test')

	@Test
	void _05_createNews() {

		test.login()

		assert test.unconfirmedNews.empty

		2.times {
			createPrivacyPolicy(it + 1)
		}

		assert !privacyConsentForTest

		List<News> news = test.currentNews
		assert news.size() == 2
		assert news*.title.toSet() == ['Test 1', 'Test 2'].toSet()
	}

	@Test
	void _15_deleteNews() {
		List<News> news = test.currentNews

		nuclosSession.delete(SystemEntities.NEWS.fqn, news.first().id)

		assert !privacyConsentForTest

		nuclosSession.delete(SystemEntities.NEWS.fqn, news.get(1).id)

		// All News deleted -> Flag is true now
		assert privacyConsentForTest

		createPrivacyPolicy(3)

		assert !privacyConsentForTest
	}

	private void createPrivacyPolicy(int number) {
		EntityObject eo = new EntityObject(SystemEntities.NEWS)

		eo.setAttribute('name', "Privacy Policy $number")
		eo.setAttribute('title', "Test $number")
		eo.setAttribute('content', "Test $number")
		eo.setAttribute('active', true)
		eo.setAttribute('showAtStartup', false)
		eo.setAttribute('confirmationRequired', true)
		eo.setAttribute('privacyPolicy', true)

		nuclosSession.save(eo)
	}

	static boolean getPrivacyConsentForTest() {
		nuclosSession.getEntityObjects(SystemEntities.USER, new QueryOptions(
				where: 'org_nuclos_system_User_username = \'test\''
		)).first().getAttribute('privacyConsentAccepted')
	}
}
