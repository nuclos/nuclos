package org.nuclos.test.server

import java.util.zip.ZipInputStream

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.*
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.RestResponse
import org.nuclos.test.rest.SuperuserRESTClient

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class NucletExportImportTest extends AbstractNuclosTest {

	static SuperuserRESTClient client

	static String nucletName = 'Rest%20example'

	@Before
	void _00_setup() {
		client = new SuperuserRESTClient('nuclos', '')
		client.login()

	}

	@Test
	void _01_writeNucletParams() {
		QueryOptions opt = new QueryOptions(
				search: 'nucb'
		)
		List<EntityObject<String>> eos = client.getEntityObjects(SystemEntities.NUCLET, opt);

		assert eos.size() == 1

		EntityObject<String> eo = eos.get(0)

		List<EntityObject<String>> lstDep = eo.loadDependents(SystemEntities.NUCLET_PARAMETER, 'nuclet')

		setNucletParamAttributes(lstDep, '01', true)
		setNucletParamAttributes(lstDep, '02', false)

		eo.save()

	}

	static void setNucletParamAttributes(List<EntityObject<String>> lstNp, String nr, Boolean export) {
		String param = 'ParamX' + nr
		EntityObject<String> nucletParam = null
		for (EntityObject<String> np : lstNp) {
			if (np.getAttribute('name') == param) {
				nucletParam = np;
				break;
			}
		}
		if (nucletParam == null) {
			nucletParam = new EntityObject<String>(SystemEntities.NUCLET_PARAMETER)
			lstNp.add(nucletParam)
		}
		nucletParam.setAttribute('name', param)
		nucletParam.setAttribute('description', 'Desc' + nr)
		nucletParam.setAttribute('value', 'ValueY' + nr)
		nucletParam.setAttribute('exportvalue', export)
	}

	@Test
	void _02_exportNuclet() {
		byte[] nucletContent = client.exportNuclet(nucletName);

		assert nucletContent

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(nucletContent));

		while ((zis.getNextEntry()) != null) {
			int size;
			byte[] buffer = new byte[2048];

			while ((size = zis.read(buffer, 0, buffer.length)) != -1) {
				baos.write(buffer, 0, size);
			}
			baos.flush();
		}
		zis.close();
		baos.close();

		String s = baos.toString();

		assert s;
		assert s.contains('ParamX01')
		assert s.contains('ValueY01')
		assert s.contains('ParamX02')
		// This is the very test: It may not contain the value with exportvalue == false
		assert !s.contains('ValueY02')
	}

	@Test
	void _03_importNuclet() {
		// boMetaId org_nuclos_system_Nuclet
		// subBo sql Configs org_nuclos_system_NucletSqlConfig_nuclet
		// boId nuclet_name (configuration_test)
		RestResponse response = RESTHelper.importNuclet("ConfigurationTest-v1.nuclet", client)
		EntityObject<String> nucletEO = client.getEntityObject(SystemEntities.NUCLET, "configuration_test")
		assert response.httpResponse.statusLine.statusCode == 204
		assert nucletEO.getAttribute("nucletVersion") == 1

		List<EntityObject<String>> sqlConfigs = client.loadDependents(
				nucletEO,
				SystemEntities.NUCLET_SQL_CONFIG,
				SystemEntities.NUCLET_SQL_CONFIG.fqn + '_nuclet'
		)

		for (EntityObject<String> eo : sqlConfigs) {
			if (eo.getAttribute("nucletVersion") == 1 &&
					(eo.getAttribute("tags") == null || eo.getAttribute("tags") == "H2")) {
				assert eo.getAttribute("lastRun") != null: String.format("%s should be executed", eo.getAttribute("name"))
				assert eo.getAttribute("lastRunCkSum") != "":  String.format("%s should have checksum", eo.getAttribute("name"))
				if (eo.getAttribute("name") == "Error-SQL") {
					// this should have an error
					assert eo.getAttribute("isLastRunError") == Boolean.TRUE:  String.format("%s should be error prune", eo.getAttribute("name"))
					assert !((String) eo.getAttribute("lastRunResult"))?.isEmpty(): String.format("%s should have error message", eo.getAttribute("name"))
				} else {
					assert eo.getAttribute("isLastRunError") == Boolean.FALSE: String.format("%s should be without error", eo.getAttribute("name"))
				}
			}
		}

		sleep(1000) // the typical H2 transaction handling :-(
		List<String> sampleConfigs = client.getEntityObjects(TestEntities.CONFIGURATION_TEST_SAMPLESAMPLECONFIGURATIONENTITY)
				.collect { o -> ((String) o.getAttribute("bezeichner")) }

		assert sampleConfigs == ["User", "Administrator", "H2"]

		client.patchSystemParameters(
				[
				        "SQL_CONFIG_FILTER": "<include>Special</include>"
				]
		)

		RESTHelper.importNuclet("ConfigurationTest-v1.nuclet", client)

		assert client.loadDependents(
				nucletEO,
				SystemEntities.NUCLET_SQL_CONFIG,
				SystemEntities.NUCLET_SQL_CONFIG.fqn + '_nuclet'
		).find {it -> ("Special" == it.getAttribute("tags")) }?.getAttribute("lastRun") != null: "Tagged Config should be executed now"
		sampleConfigs = client.getEntityObjects(TestEntities.CONFIGURATION_TEST_SAMPLESAMPLECONFIGURATIONENTITY)
				.collect { o -> ((String) o.getAttribute("bezeichner")) }

		assert sampleConfigs == ["Tag", "User", "Administrator", "H2"]

		// importing should fail
		response = RESTHelper.importNuclet("ConfigurationTest-v3.nuclet", client)
		nucletEO = client.getEntityObject(SystemEntities.NUCLET, "configuration_test")
		assert response.httpResponse.statusLine.statusCode == 500
		assert nucletEO.getAttribute("nucletVersion") == 1

		response = RESTHelper.importNuclet("ConfigurationTest-v2.nuclet", client)
		nucletEO = client.getEntityObject(SystemEntities.NUCLET, "configuration_test")
		assert response.httpResponse.statusLine.statusCode == 204
		assert nucletEO.getAttribute("nucletVersion") == 2

		sampleConfigs = client.getEntityObjects(TestEntities.CONFIGURATION_TEST_SAMPLESAMPLECONFIGURATIONENTITY)
				.collect { o -> ((String) o.getAttribute("bezeichner")) }

		assert sampleConfigs == ["Version", "Tag", "User", "Administrator", "H2"]

		EntityObject<Long> admin = client.getEntityObjects(TestEntities.CONFIGURATION_TEST_SAMPLESAMPLECONFIGURATIONENTITY)
				.find { it -> it.getAttribute("nummer") == "01" }

		assert admin.getAttribute("active")

		// importing should go now
		response = RESTHelper.importNuclet("ConfigurationTest-v3.nuclet", client)
		nucletEO = client.getEntityObject(SystemEntities.NUCLET, "configuration_test")
		assert response.httpResponse.statusLine.statusCode == 204
		assert nucletEO.getAttribute("nucletVersion") == 3

		sampleConfigs = client.getEntityObjects(TestEntities.CONFIGURATION_TEST_SAMPLESAMPLECONFIGURATIONENTITY)
				.collect { o -> ((String) o.getAttribute("bezeichner")) }

		assert sampleConfigs == ["Version (aktiv)", "Tag", "User", "Administrator (aktiv)", "H2"]
	}

	@Test
	void _04_deleteNuclet() {
		RestResponse response = RESTHelper.importNuclet("ConfigurationTest-Empty.nuclet", client)
		EntityObject<String> nucletEO = client.getEntityObject(SystemEntities.NUCLET, "configuration_test")
		assert response.httpResponse.statusLine.statusCode == 204
		assert nucletEO.getAttribute("nucletVersion") != 0

		assert client.delete(nucletEO)
	}
}
