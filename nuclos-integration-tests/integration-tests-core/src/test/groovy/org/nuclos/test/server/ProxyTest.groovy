package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTException
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ProxyTest extends AbstractNuclosTest {

	/*
	 * 
	 */

	@Test
	void _00_setup() {
		RESTHelper.createUser('ProxyTest', 'ProxyTest', ['Example user'], nuclosSession)
	}


	@Test
	void _01_testResultForBasicBO() {
		RESTClient client = new RESTClient('ProxyTest', 'ProxyTest').login()
		def result = RESTHelper.getBos('nuclet_test_other_TestProxyContainer', client) as Map
		def bos = result.get('bos') as List
		assert bos.size() == 2
	}

	@Test
	void _02_getTestBoProxy() {
		RESTClient client = new RESTClient('ProxyTest', 'ProxyTest').login()

		assert client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY).size() == 40

		Set<String> orderBy = ["${TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY.fqn}_primaryKey asc" as String] as Set
		def proxyObjects = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY, new QueryOptions(chunkSize: 250, offset: 500, orderBy: orderBy))
		assert proxyObjects.size() == 250
		assert proxyObjects.get(0).getBoId() == 501

		proxyObjects = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY, new QueryOptions(chunkSize: 500, offset: 4900, orderBy: orderBy))
		assert proxyObjects.size() == 100
		assert proxyObjects.get(proxyObjects.size() - 1).getBoId() == 5000
	}

	@Test
	void _03_createTestBoProxy() {
		RESTClient client = new RESTClient('ProxyTest', 'ProxyTest').login()

		def now = new Date()

		def proxy = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY)
		proxy.setAttribute('name', 'Test5001')
		proxy.setAttribute('number', 5001)
		proxy.setAttribute('date', now)
		client.save(proxy)

		Set<String> orderBy = ["${TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY.fqn}_primaryKey asc" as String] as Set
		def objects = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY, new QueryOptions(chunkSize: 10000, orderBy: orderBy))
		assert objects.size() == 5001
		assert objects[5000].getBoId() == 5001
		assert objects[5000].getAttribute('name') == 'Test5001'
		assert objects[5000].getAttribute('number') == 5001
	}

	@Test
	void _04_updateTestBoProxy() {
		RESTClient client = new RESTClient('ProxyTest', 'ProxyTest').login()

		def proxy = client.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY, 5001L)
		assert proxy.getBoId() == 5001
		assert proxy.getAttribute('name') == 'Test5001'
		assert proxy.getAttribute('number') == 5001

		proxy.setAttribute('name', 'Modified')
		client.save(proxy)

		proxy = client.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY, 5001L)
		assert proxy.getAttribute('name') == 'Modified'
	}

	@Test
	void _05_deleteTestBoProxy() {
		RESTClient client = new RESTClient('ProxyTest', 'ProxyTest').login()

		def proxy = client.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY, 5001L)
		assert proxy.getBoId() == 5001

		client.delete(proxy)

		try {
			proxy = client.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTBOPROXY, 5001L)
			assert proxy == null
		} catch(RESTException e) {
		}
	}
}
