package org.nuclos.test.server


import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RemotingTransportObjectTest extends AbstractNuclosTest {

	static RESTClient client

	@Test
	void _testRegisteredTransportInterface() {
		client = new RESTClient('nuclos', '').login()

		String url = RESTHelper.REST_BASE_URL + '/remotingclasslists'
		def classlists = RESTHelper.requestJson(url, HttpMethod.GET, client.sessionId, null)

		assert classlists.getJSONArray('regularClasses').contains('org.nuclet.remoting.MyCustomRemotingTransport')
	}

}
