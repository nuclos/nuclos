package org.nuclos.test.server

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.Test
import org.nuclos.common.UID;
import org.nuclos.test.AbstractNuclosTest;
import org.nuclos.test.rest.RESTClient;

/**
 * Created by Sebastian Debring on 04.04.2022.
 */
public class ReportTest extends AbstractNuclosTest {

	@Test
	void _01_testRewindableDataSource() {
		RESTClient restClient = new RESTClient('nuclos', '')
		restClient.login()

		File output = restClient.executeReport(UID.parseUID('ykZOBJGnVzP1LbOj5eqg')) //Test JRRewindableDatasource

	}

	@Test
	void _02_testReportTextParameter() {
		RESTClient restClient = new RESTClient('nuclos', '')
		restClient.login()

		File output = restClient.executeReport(UID.parseUID('UIIBtwrYOwcQpalZiWho'), UID.parseUID('0met1if6MMKKmqF2rdU6')) // Test Report Text Parameter

		assert output != null: 'Resulting report should not be null'

		String text = getPdfTextContent(output)

		assert text.contains("Hallo Welt"): 'Text Parameter not found in resulting report'
	}

	@Test
	void _03_testSubreportFromResource() {
		RESTClient restClient = new RESTClient('nuclos', '')
		restClient.login()

		File output = restClient.executeReport(UID.parseUID('UIIBtwrYOwcQpalZiWho'), UID.parseUID('diLMA49pNXSbJ97fXAfF')) // Test Report Text Parameter

		assert output != null: 'Resulting report should not be null'

		String text = getPdfTextContent(output)

		assert text.contains("Subreport from resource"): 'Subreport from resource seems to be missing'
	}

	@Test
	void _04_testSubreportFromFile() {
		RESTClient restClient = new RESTClient('nuclos', '')
		restClient.login()

		File output = restClient.executeReport(UID.parseUID('UIIBtwrYOwcQpalZiWho'), UID.parseUID('kdZSLkAmflnfmTgNXobh')) // Test Report Text Parameter

		assert output != null: 'Resulting report should not be null'

		String text = getPdfTextContent(output)

		assert text.contains("Subreport from file"): 'Subreport from file seems to be missing'
	}

	@Test
	void _05_testReportFromResource() {
		RESTClient restClient = new RESTClient('nuclos', '')
		restClient.login()

		File output = restClient.executeReport(UID.parseUID('UIIBtwrYOwcQpalZiWho'), UID.parseUID('2GwE49wAFNABHvcvXr8A')) // Test Report Text Parameter

		assert output != null: 'Resulting report should not be null'

		String text = getPdfTextContent(output)

		assert text.contains("Report from resource"): 'Different report content than expected'
	}

	@Test
	void _06_testReportFromFile() {
		RESTClient restClient = new RESTClient('nuclos', '')
		restClient.login()

		File output = restClient.executeReport(UID.parseUID('UIIBtwrYOwcQpalZiWho'), UID.parseUID('RkZLSXNA7NIKP8ek7gxY')) // Test Report Text Parameter

		assert output != null: 'Resulting report should not be null'

		String text = getPdfTextContent(output)

		assert text.contains("Report from file"): 'Different report content than expected'
	}

	private String getPdfTextContent(File pdfFile) {
		PDDocument doc = PDDocument.load(pdfFile)
		PDFTextStripper pdfStripper = new PDFTextStripper()
		String text = pdfStripper.getText(doc)
		return text
	}

}
