package org.nuclos.test.server

import org.apache.commons.io.IOUtils
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.NuclosTestContext
import org.nuclos.test.SystemEntities
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RestAPIsTest extends AbstractNuclosTest {

	static RESTClient client

	@Test
	void testRestAdapterClass() {
		client = new RESTClient('nuclos', '').login()
		NuclosTestContext context = NuclosTestContext.instance

		def nucletParameter = client.getEntityObject(SystemEntities.NUCLET_PARAMETER, 'xTzK6JXqAsIgnfbwKhy6')
		nucletParameter.setAttribute('value', context.nuclosServer)
		nucletParameter.save()

		EntityObject<Long> boRestAPIsTest = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_RESTAPISTEST)
		boRestAPIsTest.setAttribute('name', 'testRestAdapterClass')

		long boId = client.save(boRestAPIsTest)
		boRestAPIsTest = client.getEntityObject(TestEntities.NUCLET_TEST_OTHER_RESTAPISTEST, boId)

		assert  boRestAPIsTest.getAttribute('result') == '{"ready":true}\n' +
				'{"debugSQL":"false","minExecTime":null}\n' +
				'{"debugSQL":"true","minExecTime":null}\n'
	}

	@Test
	void restAPIsCRUD() {
		client = new RESTClient('nuclos', '').login()

		EntityObject<String> boRestAPIs = new EntityObject<>(SystemEntities.RESTAPIS)
		boRestAPIs.setAttribute('name', 'Nuclos2')
		boRestAPIs.setAttribute('description', 'Nuclos2')
		boRestAPIs.setAttribute('invokerpackage', 'de.nuclos2.rest')
		boRestAPIs.setAttribute('apipackage', 'de.nuclos2.rest.api')
		boRestAPIs.setAttribute('modelpackage', 'de.nuclos2.rest.model')

		try (InputStream is = RestAPIsTest.class.getResourceAsStream('NuclosDE2.json')) {
			boRestAPIs.setDocument('apifile', 'NuclosDE2.json', IOUtils.toByteArray(is))
		}

		def id = client.save(boRestAPIs)

		Thread.sleep(20000)
		ServerIntegrationTest.runServerTest("restAPIs")

		boRestAPIs = client.getEntityObject(SystemEntities.RESTAPIS, id)
		boRestAPIs.setAttribute('name', 'Nuclos3')
		boRestAPIs.setAttribute('invokerpackage', 'de.nuclos3.rest')
		boRestAPIs.setAttribute('apipackage', 'de.nuclos3.rest.api')
		boRestAPIs.setAttribute('modelpackage', 'de.nuclos3.rest.model')
		client.save(boRestAPIs)

		Thread.sleep(20000)
		ServerIntegrationTest.runServerTest("restAPIs")


		client.delete(boRestAPIs)

		Thread.sleep(20000)
		ServerIntegrationTest.runServerTest("restAPIs")
	}

}
