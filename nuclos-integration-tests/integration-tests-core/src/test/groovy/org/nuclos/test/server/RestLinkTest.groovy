package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RestLinkTest extends AbstractNuclosTest {
	@Test
	void _05_entityObject() {
		EntityObject eo = new EntityObject<>(TestEntities.EXAMPLE_REST_ORDER)

		eo.setAttribute('orderNumber', '123')
		nuclosSession.save(eo)

		checkLinks(eo)
	}

	@Test
	void _10_eoList() {
		List<EntityObject<Long>> eos = nuclosSession.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER)
		assert eos.size() == 1
		eos.each {
			checkLinks(it)
		}
	}

	private static void checkLinks(EntityObject<?> eo) {
		assert eo.links.size() > 0

		eo.links.values().each {
			assert it.href.startsWith('//')
			assert !it.methods.empty
		}
	}
}
