package org.nuclos.test.server


import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.rules.ExpectedException
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RuleAnnotationTest extends AbstractNuclosTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none()

	static RESTClient client;

	@Test
	void _01_createTestUser() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
	}



	@Test
	void _05_RuleAnnotation() {
		client.login()

		EntityObject<Long> testRules = new EntityObject<>(TestEntities.NUCLET_TEST_RULES_TESTRULES);
		testRules.setAttribute('name', 'TestRuleAnnotation')

		client.executeCustomRule(testRules, 'nuclet.test.rules.CustomRuleAnnotationTest')
	}
}
