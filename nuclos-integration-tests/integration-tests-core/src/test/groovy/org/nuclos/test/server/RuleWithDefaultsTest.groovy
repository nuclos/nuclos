package org.nuclos.test.server

import java.text.SimpleDateFormat

import org.json.JSONObject
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.rules.ExpectedException
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RuleWithDefaultsTest extends AbstractNuclosTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none()

	static RESTClient client

	String getOldBasePath()             { return '/execute/nuclet.test.other.CustomRestWithDefaultsTestRule/' }

	@Test
	void _01_testWithDefaultsMethod() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
		client.login()

		JSONObject response = RESTHelper.requestJson(getOldBasePath() + 'getTest', HttpMethod.GET, client.sessionId)
		def format = new SimpleDateFormat("dd.MM.yyyy")
		Date date = format.parse("01.08.2024")
		Date date2 = format.parse("01.02.2000")
		Date now = new Date()
		Calendar calendar = Calendar.getInstance()
		calendar.setTime(now)
		calendar.set(Calendar.HOUR_OF_DAY, 0)
		calendar.set(Calendar.MINUTE, 0)
		calendar.set(Calendar.SECOND, 0)
		calendar.set(Calendar.MILLISECOND, 0)

		assert response.get("memo") == 'Memo Default Text Wert'
		assert response.get("integer") == 7
		assert response.get("double94") == new BigDecimal("7.75")
		assert response.get("double92") == new BigDecimal("3.25")
		assert response.get("date") == date.getTime()
		assert response.get("dateformat") == date2.getTime()
		assert response.get("datetoday") == calendar.time.getTime()
	}

}
