package org.nuclos.test.server


import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.rules.ExpectedException
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RuleWithInvalidReferenceTest extends AbstractNuclosTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none()

	static RESTClient client;
	final static String PATH_DROP_CONSTRAINTS = '/maintenance/managementconsole/dropConstraints/'
	final static String CONSTRAINTS_TO_DROP = 'XR__ATMWUBPOSVVE6D7PF5T3S4WCXJ'

	@Test
	void _01_setup() {
		RESTClient suClient = new RESTClient('nuclos', '')
		suClient.login()

		//drop fk constraint first
		Log.info RESTHelper.requestString(PATH_DROP_CONSTRAINTS + CONSTRAINTS_TO_DROP, HttpMethod.POST, suClient.sessionId)
		suClient.logout()

		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
	}



	@Test
	void _02_getInvalidReferencedBO() {
		client.login()

		EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_RULES_BOWITHINVALIDREFERENCE)
		eo.setAttribute('name', 'test')
		Long id = client.save(eo)

		eo = client.getEntityObject(TestEntities.NUCLET_TEST_RULES_BOWITHINVALIDREFERENCE, id)
		eo.setAttribute('name', eo.getAttribute('name'))
		eo.save()
	}
}
