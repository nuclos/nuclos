package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.apache.commons.io.IOUtils
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SQLInjectionTest extends AbstractNuclosTest {

	private String getVlpUrl(String intid, String mandatorId, String categoryId, String fieldName, String idFieldNameExtension) {
		if (intid == null) {
			intid = ''
		}
		if (mandatorId == null) {
			mandatorId = ''
		}
		if (categoryId == null) {
			categoryId = 'null'
		}
		if (fieldName == null) {
			fieldName = ''
		}
		if (idFieldNameExtension == null) {
			idFieldNameExtension = ''
		}
		return RESTHelper.REST_BASE_URL + '/data/vlpdata?reffield=example_rest_OrderPosition_article' +
				'&vlptype=ds' +
				'&vlpvalue=PNTDWlP02tq7zjsj5gYi' +
				'&intid=' + intid +
				'&mandatorId=' + mandatorId +
				'&categoryId=' + categoryId +
				'&fieldname=' + fieldName +
				'&id-fieldname=%3CprimaryKey%3E' + idFieldNameExtension +
				'&valuelistProvider=PNTDWlP02tq7zjsj5gYi' +
				'&searchmode=false' +
				'&chunkSize=100'
	}

	@Test
	void _01_testForbiddenCharsInIntidParameterValue() {
		nuclosSession.getUrl(getVlpUrl('0+AND+24%3d(SELECT+24+FROM+PG_SLEEP(10))', null, null, null, null)).with {
			assert it.statusLine.statusCode == Response.Status.PRECONDITION_FAILED.statusCode
			assert IOUtils.toString(it.getEntity().content).contains('Forbidden character found.')
		}
	}

	//@Test <-- TODO: needs enabled mandator setup
	void _02_testForbiddenCharsInMandatorIdParameterValue() {
		nuclosSession.getUrl(getVlpUrl(null, '0+AND+24%3d(SELECT+24+FROM+PG_SLEEP(10))', null, null, null)).with {
			assert it.statusLine.statusCode == Response.Status.EXPECTATION_FAILED.statusCode
			assert IOUtils.toString(it.getEntity().content).contains('Forbidden character found.')
		}
	}

	@Test
	void _03_testForbiddenCharsInCustomIntegerParameterValue() {
		nuclosSession.getUrl(getVlpUrl(null, null, '0+AND+24%3d(SELECT+24+FROM+PG_SLEEP(10))', null, null)).with {
			assert it.statusLine.statusCode == Response.Status.PRECONDITION_FAILED.statusCode
			assert IOUtils.toString(it.getEntity().content).contains('Forbidden character found.')
		}
	}

	@Test
	void _04_testForbiddenCharsInFieldNameAlias() {
		nuclosSession.getUrl(getVlpUrl(null, null, null, '(SELECT+24+FROM+PG_SLEEP(10))', null)).with {
			assert it.statusLine.statusCode == Response.Status.PRECONDITION_FAILED.statusCode
			assert IOUtils.toString(it.getEntity().content).contains('Forbidden character found.')
		}
	}

	@Test
	void _05_testForbiddenCharsInIdFieldNameAlias() {
		nuclosSession.getUrl(getVlpUrl(null, null, null, null, ',(SELECT+24+FROM+PG_SLEEP(10))')).with {
			assert it.statusLine.statusCode == Response.Status.PRECONDITION_FAILED.statusCode
			assert IOUtils.toString(it.getEntity().content).contains('Forbidden character found.')
		}
	}



}
