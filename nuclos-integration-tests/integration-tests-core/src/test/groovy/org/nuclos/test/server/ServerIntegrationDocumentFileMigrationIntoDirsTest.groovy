package org.nuclos.test.server

import static org.nuclos.test.server.ServerIntegrationTest.runServerTest

import org.junit.AfterClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
public class ServerIntegrationDocumentFileMigrationIntoDirsTest extends AbstractNuclosTest {

	/*
	 * documentFile_ - Tests
	 * Siehe auch NUCLOS-5233 "Refactoring Dokumentenanhänge"
	 */
	@Test
	void documentFile_00disableIndexer() {
		nuclosSession.managementConsole('disableIndexer')
	}

	@Test
	void documentFile_01checkDocumentAttachmentsDeleteUnused() {
		nuclosSession.managementConsole('checkDocumentAttachments', '-deleteUnusedFiles')
	}

	@Test
	void documentFile_30_CheckNoDirs() {
		runServerTest("documentFileCheckNoDirs")
	}

	@Test
	void documentFile_31_EnableDirs() {
		nuclosSession.setSystemParameters(['DOCUMENTS_GUIDELINE_FOR_TOTAL_NUMBER_OF_FILES_PER_DIR':'10',
		                                   'DOCUMENTS_SUBDIR_STRUCTURE_DATE_FORMAT':'yyyy/MM'])
	}

	@Test
	void documentFile_32_RunMigration() {
		nuclosSession.managementConsole('migrateDocumentAttachmentsIntoSubdirectories', '-ignoreExtension wsdl')
	}

	@Test
	void documentFile_33_CheckMigration() {
		runServerTest("documentFileCheckMigrationIntoSubDirs")
	}

	@AfterClass
	static void cleanupSystemparameter() {
		nuclosSession.setSystemParameters([:])
	}

}
