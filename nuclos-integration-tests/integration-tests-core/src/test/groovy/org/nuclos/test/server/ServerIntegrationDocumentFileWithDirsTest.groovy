package org.nuclos.test.server

import static org.nuclos.test.server.ServerIntegrationTest.runServerTest;

import org.junit.AfterClass
import org.junit.FixMethodOrder;
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest

import groovy.transform.CompileStatic;

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
public class ServerIntegrationDocumentFileWithDirsTest extends ServerIntegrationDocumentFileTest {

	@Override
	protected DocTestClass docTestClass() {
		return DocTestClass.WithDirs;
	}

	@Test
	void documentFile_00enableDirs() {
		nuclosSession.setSystemParameters(['DOCUMENTS_GUIDELINE_FOR_TOTAL_NUMBER_OF_FILES_PER_DIR':'10'])
	}

	@AfterClass
	static void cleanupSystemparameter() {
		nuclosSession.setSystemParameters([:])
	}

}
