package org.nuclos.test.server


import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ServerIntegrationTest extends AbstractNuclosTest {

	@Test
	void genericBusinessObject_01_withDependents() { runServerTest("genericBusinessObjectWithDependents") }

	@Test
	void genericBusinessObject_02_withAttachments() { runServerTest("genericBusinessObjectWithAttachments") }

	@Test
	void tablePreferencesManager_01_columnOrder() { runServerTest("tablePreferencesManagerColumnOrder") }

	@Test
	void importObjectWithSMTest() {
		runServerTest("importObjectWithSMTest")
	}

	@Test
	void importObjectNoSMTest() {
		runServerTest("importObjectNoSMTest")
	}
	
	@Test
	void importObjectTest01() {
		runServerTest("importObjectTest01")
	}

	@Test
	void importObjectTest02() {
		runServerTest("importObjectTest02")
	}

	@Test
	void poiDocxToPdf() {
		runServerTest("poiDocxToPdf")
	}

	@Test
	void eoProcessor_AddJoinsForSortingOrder_RefOverRefWithIdSelectionOnly() {
		runServerTest("eoProcessor_AddJoinsForSortingOrder_RefOverRefWithIdSelectionOnly")
	}

	@Test
	void systemExtension() {
		RESTClient clientWithoutLogin = new RESTClient(true) // AutoLogin via system.extension.nuclet.test.auth.NucletSystemAuthRule
		Map testHeaders = ["Rule-System-Auth": "it-systemExtension"];
		clientWithoutLogin.withHeaders(testHeaders, () -> {
			runServerTest("systemExtension", clientWithoutLogin)
		})
	}

	static void runServerTest(String sTestcase) {
		runServerTest(sTestcase, nuclosSession)
	}

	static void runServerTest(String sTestcase, RESTClient client) {
		EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_UTILS_SERVERTEST)
		eo.setAttribute('testcase', sTestcase)
		RESTHelper.save(eo, client)

		def ex = eo.getAttribute('exception')
		if (ex) {
			throw new Exception(ex.toString())
		}
		assert eo.getAttribute('ok')
	}

}
