package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class StateChangeAndUpdateRulesTest extends AbstractNuclosTest {

	static int auftragCounter = 0
	static boolean useSaveWithAttributeChange = false;
	static boolean checkForMethodNotAllowed = false;

	@Test
	void _10_stateChangeDirectCallCheckWithoutUpdateRules() {
		useSaveWithAttributeChange = false
		checkForMethodNotAllowed = false
		execute()
	}

	@Test
	void _11_stateChangeViaAttributeCheckWithoutUpdateRules() {
		useSaveWithAttributeChange = true
		checkForMethodNotAllowed = false
		execute()
	}

	@Test
	void _12_stateChangeViaAttributeCheckWithUpdateRules() {
		useSaveWithAttributeChange = true
		checkForMethodNotAllowed = true
		execute(50)
	}

	private void execute(int expectedFinalState = 10) {
		EntityObject auftrag = new EntityObject(TestEntities.EXAMPLE_REST_AUFTRAG)
		auftrag.setAttribute('name', 'Auftrag ' + (auftragCounter++))
		nuclosSession.save(auftrag)

		changeState(auftrag, 20) // Bearbeitung
		changeState(auftrag, 50) // Fertig
		// Im Status 50 wird die UpdateFinalRule 'AenderungR' ausgeführt, welche im Kontext des Statuswechsel einen Fehler wirft (NUCLOS-9246).
		// Diese Regel darf also bei einem einfachen Statuswechsel, weg von 50, nicht ausgeführt werden, außer das Dirty Flag wurde gesetzt
		changeState(auftrag, 10) // Entwurf

		assert auftrag.getAttribute('nuclosStateNumber') == expectedFinalState
	}

	private void changeState(EntityObject auftrag, int stateNum) {
		if (useSaveWithAttributeChange) {
			auftrag.setAttribute('nuclosState', [id: 'example_rest_AuftragSM_State_' + stateNum]) // Bearbeitung
			if (checkForMethodNotAllowed && stateNum == 10) {
				// Durch das Dirty Flag wird ein changeStateAndModifyByUser verwendet, wobei dieser Fehler aber erwartet wird:
				auftrag.dirty = true
				expectErrorStatus(Response.Status.METHOD_NOT_ALLOWED) {
					auftrag.save()
				}
			} else {
				auftrag.save()
			}
		} else {
			auftrag.changeState(stateNum)
		}
	}

}
