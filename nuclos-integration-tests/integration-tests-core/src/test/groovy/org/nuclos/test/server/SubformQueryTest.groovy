package org.nuclos.test.server

import org.apache.http.client.methods.HttpGet
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common.Mutable
import org.nuclos.server.rest.services.helper.RecursiveDependency
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformQueryTest extends AbstractNuclosTest {

	private static EntityObject<Long> parent
	private static List<EntityObject<Long>> subformData

	private static RecursiveDependency dependency

	static RESTClient testUser = new RESTClient('test', 'test')

	@Test
	void _00_setup() {
		parent = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		parent.setAttribute('text', 'parent')
		parent.setAttribute('value', 1)

		List<EntityObject<Long>> subform = parent.getDependents(
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
				'parent'
		)

		3.times { subCount ->
			EntityObject subEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM)
			subEo.setAttribute('text', subCount.toString())
			subform << subEo

			List<EntityObject<Long>> subsubform = subEo.getDependents(
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					'subform'
			)
			3.times { subsubCount ->
				EntityObject subsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM)
				subsubEo.setAttribute('text', "$subCount-$subsubCount".toString())
				subsubform << subsubEo

				List<EntityObject<Long>> subsubsubform = subsubEo.getDependents(
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM,
						'subsubform'
				)
				3.times { subsubsubCount ->
					EntityObject subsubsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM)
					subsubsubEo.setAttribute('text', "$subCount-$subsubCount-$subsubsubCount".toString())
					subsubsubform << subsubsubEo
				}
			}
		}

		nuclosSession.save(parent)

		subformData = nuclosSession.loadDependents(
				parent,
				parent.entityClass,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent'
		)

		dependency = RecursiveDependency.forRoot(parent.id as String)
				.dependency(
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent',
				subformData.first().id as String
		).dependency(
				TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM.fqn + '_subform'
		)
	}

	@Test
	void _10_filterViaWhere() {
		testUser.login()

		[HttpMethod.GET, HttpMethod.POST].each { method ->
			Log.info "$method..."
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					dependency,
					new QueryOptions(
							method: method
					)
			).with {
				assert it.size() == 3
				assert it.find { it.getAttribute('text') == '2-0' }
				assert it.find { it.getAttribute('text') == '2-1' }
				assert it.find { it.getAttribute('text') == '2-2' }
			}
		}

		[HttpMethod.GET, HttpMethod.POST].each { method ->
			Log.info "$method..."
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					dependency,
					new QueryOptions(
							method: method,
							where: TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM.fqn + '_text = \'2-2\''
					)
			).with {
				assert it.size() == 1
				assert it.first().getAttribute('text') == '2-2'
			}
		}
	}

	@Test
	void _15_chunkSize() {
		[HttpMethod.GET, HttpMethod.POST].each { method ->
			Log.info "$method..."
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					dependency,
					new QueryOptions(
							method: method,
							chunkSize: 1l
					)
			).with {
				assert it.size() == 1
				assert it.first().getAttribute('text') == '2-2'
			}
		}
	}

	@Test
	void _20_offset() {
		[HttpMethod.GET, HttpMethod.POST].each { method ->
			Log.info "$method..."
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					dependency,
					new QueryOptions(
							method: method,
							offset: 2l
					)
			).with {
				assert it.size() == 1
				assert it.first().getAttribute('text') == '2-0'
			}
		}
	}

	@Test
	void _22_countTotal() {
		// Order by text ASC
		[HttpMethod.GET, HttpMethod.POST].each { method ->
			Log.info "$method..."
			Mutable<Long> countTotalResult = new Mutable<Long>();
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					dependency,
					new QueryOptions(
							method: method,
							chunkSize: 1l,
							countTotal: true
					),
					countTotalResult
			).with {
				assert it.size() == 1
				assert it.first().getAttribute('text') == '2-2'
				assert countTotalResult.value == 3
			}
		}
	}

	@Test
	void _23_noCountTotal() {
		// Order by text ASC
		[HttpMethod.GET, HttpMethod.POST].each { method ->
			Log.info "$method..."
			Mutable<Long> countTotalResult = new Mutable<Long>();
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					dependency,
					new QueryOptions(
							method: method,
							chunkSize: 1l,
							countTotal: false
					),
					countTotalResult
			).with {
				assert it.size() == 1
				assert it.first().getAttribute('text') == '2-2'
				assert countTotalResult.value == null
			}
		}
	}

	@Test
	void _25_attributes() {
		[HttpMethod.GET].each { method ->
			Log.info "$method..."
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					dependency,
					new QueryOptions(
							method: method,
							attributes: ['primaryKey'].toSet()
					)
			).with {
				assert it.size() == 3
				assert it.first().getAttribute('text') == null
			}
		}
	}

	@Test
	void _30_orderBy() {
		// Order by text ASC
		[HttpMethod.GET, HttpMethod.POST].each { method ->
			Log.info "$method..."
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					dependency,
					new QueryOptions(
							method: method,
							orderBy: [
									TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM.fqn + '_text asc'
							].toSet()
					)
			).with {
				assert it.size() == 3
				assert it.first().getAttribute('text') == '2-0'
			}
		}

		// Order by text DESC
		[HttpMethod.GET, HttpMethod.POST].each { method ->
			Log.info "$method..."
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					dependency,
					new QueryOptions(
							method: method,
							orderBy: [
									TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM.fqn + '_text desc'
							].toSet()
					)
			).with {
				assert it.size() == 3
				assert it.first().getAttribute('text') == '2-2'
			}
		}
	}

	@Test
	void _35_search() {
		// Order by text ASC
		[HttpMethod.GET, HttpMethod.POST].each { method ->
			Log.info "$method..."
			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					dependency,
					new QueryOptions(
							method: method,
							search: '2-1'
					)
			).with {
				assert it.size() == 1
				assert it.first().getAttribute('text') == '2-1'
			}
		}
	}

	// the extension is required for a test with Glassfish
	final static String BASE_PATH = '/execute/extension.example.rest.SubformQueryRuleExtension/'

	@Test
	void _50_moreRecordsWithSubformData() {
		createRecordWithSubormData(2)
		createRecordWithSubormData(5)

		HttpGet get = new HttpGet(RESTHelper.REST_BASE_URL + BASE_PATH + 'testSubformQuery')

		int maxSqlCount = 5
		// 1x Servercode (only if empty Db / single test)
		// 1x User (only if empty Db / single test)
		// 1x Security Cache
		// 1x Subform
		// 1x Parent
		String s = RESTHelper.callCustomRestRuleWithSqlCountCheck(nuclosSession, get, maxSqlCount, 0)
		assert s == '[parent, parent2]'

		HttpGet get2 = new HttpGet(RESTHelper.REST_BASE_URL + BASE_PATH + 'testReferenceQuery')

		maxSqlCount = 2
		// 1x Subform
		// 1x Parent
		String s2 = RESTHelper.callCustomRestRuleWithSqlCountCheck(nuclosSession, get2, maxSqlCount, 0)
		assert s2 == '[2, 3, 4, 5, 5, 6, 6, 7, 8, 9]'

	}

	void createRecordWithSubormData(int offset) {
		EntityObject<Long> parent2 = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		parent2.setAttribute('text', 'parent' + offset)
		parent2.setAttribute('value', offset)

		List<EntityObject<Long>> subform = parent2.getDependents(
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
				'parent'
		)

		5.times { subCount ->
			EntityObject subEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM)
			subEo.setAttribute('text', (subCount + offset).toString())
			subform << subEo
		}

		nuclosSession.save(parent2)
	}

}


