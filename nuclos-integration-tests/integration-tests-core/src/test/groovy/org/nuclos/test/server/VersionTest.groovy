package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class VersionTest extends AbstractNuclosTest {

	@Test
	void _05_insertWithStatusModel() {
		def eo = new EntityObject<>(TestEntities.EXAMPLE_REST_ORDER)
		eo.setAttribute('orderNumber', 123)

		checkVersions(eo)
	}

	@Test
	void _10_insertWithoutStatusModel() {
		def eo = new EntityObject<>(TestEntities.EXAMPLE_REST_CUSTOMER)
		eo.setAttribute('customerNumber', 123)
		eo.setAttribute('name', 'Customer 123')

		checkVersions(eo)
	}

	@Test
	void _15_insertSubforms() {
		def eo = new EntityObject<>(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		eo.setAttribute('text', 'Test')

		def subform = eo.getDependents(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		def subformEntry = new EntityObject<>(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM)
		subformEntry.setAttribute('text', 'subform 1')
		subform << subformEntry

		def subsubform = subformEntry.getDependents(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform')
		def subsubformEntry1 = new EntityObject<>(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM)
		subsubformEntry1.setAttribute('text', 'subsubform 1')
		subsubform << subsubformEntry1
		def subsubformEntry2 = new EntityObject<>(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM)
		subsubformEntry2.setAttribute('text', 'subsubform 2')
		subsubform << subsubformEntry2

		def subsubsubform = subsubformEntry1.getDependents(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM, 'subsubform')
		def subsubsubformEntry = new EntityObject<>(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM)
		subsubsubformEntry.setAttribute('text', 'subsubsubform 1')
		subsubsubform << subsubsubformEntry

		nuclosSession.save(eo)
		assert eo.getVersion() == 1

		nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM).with {
			assert it.size() == 1
			it.each {
				assert it.version == 1
			}
		}
		nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM).with {
			assert it.size() == 2
			it.each {
				assert it.version == 1
			}
		}
		nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM).with {
			assert it.size() == 1
			it.each {
				assert it.version == 1
			}
		}
	}

	@Test
	void _20_editSubforms() {
		def eo = nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_SUBFORM_PARENT).first()

		def subform = eo.loadDependents(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		assert subform.size() == 1

		def subformEntry = subform.first()
		assert subformEntry.version == 1

		def subsubform = subformEntry.loadDependents(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform')
		assert subsubform.size() == 2

		def subsubformEntry = subsubform.find { it.getAttribute('text') == 'subsubform 1' }
		assert subsubformEntry
		assert subsubformEntry.version == 1

		def subsubsubform = subsubformEntry.loadDependents(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM, 'subsubform')
		assert subsubsubform.size() == 1

		def subsubsubformEntry = subsubsubform.first()
		assert subsubsubformEntry.version == 1

		subsubsubformEntry.setAttribute('text', 'subsubsubform edited')
		assert eo.update
		assert subformEntry.update
		assert subsubformEntry.update
		assert subsubsubformEntry.update

		eo.save()

		nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM).with {
			assert it.size() == 1
			it.each {
				assert it.version == 2
			}
		}
		nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM).with {
			assert it.size() == 2

			// Only the version of one subsubform entry should be incremented (the one whose
			// subsubsubform entry we edited).
			assert it.find { it.version == 2 }

			// The version of the other one should still be 1.
			assert it.find { it.version == 1 }
		}
		nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM).with {
			assert it.size() == 1
			it.each {
				assert it.version == 2
			}
		}
	}

	@Test
	void _25_insertStatefulSubforms() {
		def eo = new EntityObject<>(TestEntities.NUCLET_TEST_SUBFORM_PARENTFORSTATEFULSUBFORM)
		eo.setAttribute('text', 'Test')

		def subform = eo.getDependents(TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL, 'parent')
		def subformEntry = new EntityObject<>(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM)
		subformEntry.setAttribute('text', 'subform 1')
		subform << subformEntry

		// FIXME: Insert of stateful subsubform does not work
		def statefulSubsubform = subformEntry.getDependents(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL, 'subform')
		def statefulSubsubformEntry = new EntityObject<>(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL)
		statefulSubsubformEntry.setAttribute('text', 'stateful subsubform 1')
		statefulSubsubform << statefulSubsubformEntry

		def statelessSubsubform = subformEntry.getDependents(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS, 'subform')
		def statelessSubsubformEntry = new EntityObject<>(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS)
		statelessSubsubformEntry.setAttribute('text', 'stateless subsubform 1')
		statelessSubsubform << statelessSubsubformEntry

		nuclosSession.save(eo)
		assert eo.getVersion() == 1

		nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL).with {
			assert it.size() == 1
			it.each {
				assert it.version == 1
			}
		}
		// FIXME: Insert of stateful subsubform does not work
//		nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL).with {
//			assert it.size() == 1
//			it.each {
//				assert it.version == 1
//			}
//		}
		nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATELESS).with {
			assert it.size() == 1
			it.each {
				assert it.version == 1
			}
		}
	}

	@Test
	void _30_insertViaRule() {
		EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTVERSION)

		eo.setAttribute('text', 'Test')

		nuclosSession.save(eo)
		eo.executeCustomRule('nuclet.test.other.CreateNewRecordViaRule')

		// Rule should have inserted one new entry with text "example"
		def example = nuclosSession.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_TESTVERSION).find {
			it.getAttribute('text').toString().startsWith('From Rule ')
		}

		assert example.version == 1
	}

	private void checkVersions(EntityObject eo) {
		assert eo.new

		// Check initial version
		nuclosSession.save(eo)
		assert eo.version == 1

		eo.save()
		assert eo.version == 2

		// No version given - no version check, but normal increment
		eo.version = null
		eo.save()
		assert eo.version == 3

		// All other version numbers must lead to conflicts
		eo.version = -1
		expectErrorStatus(Response.Status.CONFLICT) {
			eo.save()
		}
		eo.version = 2
		expectErrorStatus(Response.Status.CONFLICT) {
			eo.save()
		}
		eo.version = 4
		expectErrorStatus(Response.Status.CONFLICT) {
			eo.save()
		}
	}

}