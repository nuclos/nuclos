package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class WebclientProviderTest extends AbstractNuclosTest {

	static RESTClient client

	@Test
	void _10_testWebclientUrlBuilder_NoConfiguredBaseUrl() {
		nuclosSession.setSystemParameters([:])
		createAndAssertWebclientUrl('Final_10',
				AbstractNuclosTest.context.nuclosServerProtocol + '://' + AbstractNuclosTest.context.nuclosServerHost + ':' +	AbstractNuclosTest.context.nuclosServerPort
			+ '/webclient/#/view/nuclet_test_rules_TestAPI/{boId}?searchFilterId=JxJKhtvgk12msybuDCez'
		)
		createAndAssertWebclientUrl('ClassOnly_10',
				AbstractNuclosTest.context.nuclosServerProtocol + '://' + AbstractNuclosTest.context.nuclosServerHost + ':' +	AbstractNuclosTest.context.nuclosServerPort
						+ '/webclient/#/view/nuclet_test_rules_TestAPI?searchFilterId=JxJKhtvgk12msybuDCez'
		)
		createAndAssertWebclientUrl('_10', 'bo id must not be null')
	}

	@Test
	void _15_testWebclientUrlBuilder_WithRelativeBaseUrl() {
		nuclosSession.setSystemParameters(['WEBCLIENT_BASEURL':'/#'])
		createAndAssertWebclientUrl('Final_15',
				AbstractNuclosTest.context.nuclosServerProtocol + '://' + AbstractNuclosTest.context.nuclosServerHost + ':' +	AbstractNuclosTest.context.nuclosServerPort
			+ '/#/view/nuclet_test_rules_TestAPI/{boId}?searchFilterId=JxJKhtvgk12msybuDCez'
		)
		createAndAssertWebclientUrl('ClassOnly_15',
				AbstractNuclosTest.context.nuclosServerProtocol + '://' + AbstractNuclosTest.context.nuclosServerHost + ':' +	AbstractNuclosTest.context.nuclosServerPort
						+ '/#/view/nuclet_test_rules_TestAPI?searchFilterId=JxJKhtvgk12msybuDCez'
		)
		createAndAssertWebclientUrl('_15', 'bo id must not be null')
	}

	@Test
	void _20_testWebclientUrlBuilder_WithAbsolutBaseUrl() {
		nuclosSession.setSystemParameters(['WEBCLIENT_BASEURL':'https://nuclosserver.local/mywebclient/#'])
		createAndAssertWebclientUrl('Final_20',
			'https://nuclosserver.local/mywebclient/#/view/nuclet_test_rules_TestAPI/{boId}?searchFilterId=JxJKhtvgk12msybuDCez'
		)
		createAndAssertWebclientUrl('ClassOnly_20',
				'https://nuclosserver.local/mywebclient/#/view/nuclet_test_rules_TestAPI?searchFilterId=JxJKhtvgk12msybuDCez'
		)
		createAndAssertWebclientUrl('_20', 'bo id must not be null')
	}

	@Test
	void _25_testWebclientUrlBuilder_WithinJob_NoConfiguredBaseUrl() {
		nuclosSession.setSystemParameters([:])
		long boId = createWebclientUrl('WithinJob_25')
		assert getTestApiBusinessObject(boId).getAttribute('weblink') == null
		assert nuclosSession.startJob('nuclet_test_rules_TestJobJOB') == 'ok'
		waitUntilTrue({
			getTestApiBusinessObject(boId).getAttribute('weblink') ==
				'Webclient URL could not be determined. Set an absolut value in the system parameter WEBCLIENT_BASEURL.'
		})
	}

	@Test
	void _30_testWebclientUrlBuilder_WithinJob_WithAbsolutBaseUrl() {
		nuclosSession.setSystemParameters(['WEBCLIENT_BASEURL':'https://nuclosserver.local/mywebclient/#'])
		long boId = createWebclientUrl('WithinJob_30')
		assert getTestApiBusinessObject(boId).getAttribute('weblink') == null
		assert nuclosSession.startJob('nuclet_test_rules_TestJobJOB') == 'ok'
		waitUntilTrue({
			getTestApiBusinessObject(boId).getAttribute('weblink') ==
				'https://nuclosserver.local/mywebclient/#/view/nuclet_test_rules_TestAPI/' + boId + '?searchFilterId=JxJKhtvgk12msybuDCez'
		})
	}

	static void createAndAssertWebclientUrl(String test, String webclientUrl) {
		long boId = createWebclientUrl(test)
		webclientUrl = webclientUrl.replace('{boId}', Long.toString(boId))
		assert ((String)getTestApiBusinessObject(boId).getAttribute('weblink')).replace(":8080", ":" + context.nuclosServerPort) == webclientUrl
	}

	static EntityObject<Long> getTestApiBusinessObject(long boId) {
		client.getEntityObject(TestEntities.NUCLET_TEST_RULES_TESTAPI, boId)
	}

	static long createWebclientUrl(String test) {
		client = new RESTClient('test', 'test').login()

		EntityObject<Long> boTestAPI = new EntityObject<>(TestEntities.NUCLET_TEST_RULES_TESTAPI)
		boTestAPI.setAttribute('test', 'buildWebclientUrl' + test)

		client.save(boTestAPI)
	}

}
