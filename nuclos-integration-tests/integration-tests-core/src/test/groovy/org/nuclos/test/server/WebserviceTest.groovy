package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class WebserviceTest extends AbstractNuclosTest {

	static RESTClient client

	@Test
	void _10_testPostAndReadFromHelloWebservice() {
		client = new RESTClient('nuclos', '').login()

		EntityObject<Long> boWebserviceTest = new EntityObject<>(TestEntities.NUCLET_TEST_RULES_TESTWEBSERVICE)
		boWebserviceTest.setAttribute('firstName', 'Jone')

		long boId = client.save(boWebserviceTest)
		boWebserviceTest = client.getEntityObject(TestEntities.NUCLET_TEST_RULES_TESTWEBSERVICE, boId)

		assert  boWebserviceTest.getAttribute('greeting') == 'hello from castle mock'
	}

}
