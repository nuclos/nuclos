package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class WriteProxyTest extends AbstractNuclosTest {

	/*
	 *
	 */

	@Test
	void _00_setup() {
		def proxData1 =
				[
						boMetaId  : TestEntities.EXAMPLE_REST_WRITEPROXYDATA.fqn,
						attributes: [
								name   : 'Write Proxy 1',
								nummer : 33,
								nummer2: 3.3
						]
				]
		proxData1 = RESTHelper.createBo(proxData1, nuclosSession)



		def proxData2 =
				[
						boMetaId  : TestEntities.EXAMPLE_REST_WRITEPROXYDATA.fqn,
						attributes: [
								name   : 'Write Proxy 2',
								nummer : 44,
								nummer2: 4.4
						]
				]
		proxData2 = RESTHelper.createBo(proxData2, nuclosSession)



		def proxData3 =
				[
						boMetaId  : TestEntities.EXAMPLE_REST_WRITEPROXYDATA.fqn,
						attributes: [
								name   : 'Write Proxy 3',
								nummer : 55,
								nummer2: 5.5
						]
				]
		proxData3 = RESTHelper.createBo(proxData3, nuclosSession)

		RESTHelper.createUser('WriteProxyTest', 'WriteProxyTest', ['Example user'], nuclosSession)
	}


	@Test
	void _01_testResultForBasicBO() {
		RESTClient client = new RESTClient('WriteProxyTest', 'WriteProxyTest').login()
		def result = RESTHelper.getBos(TestEntities.EXAMPLE_REST_WRITEPROXYDATA.fqn, client) as Map
		def bos = result.get('bos') as List
		assert bos.size() == 3
	}


	@Test
	void _02_ReadTestForWriteProxyBO() {
		RESTClient client = new RESTClient('WriteProxyTest', 'WriteProxyTest').login()
		def result = RESTHelper.getBos(TestEntities.EXAMPLE_REST_WRITEPROXYTEST.fqn, client) as Map
		def bos = result.get('bos') as List

		assert bos.size() == 3
	}

	@Test
	void _03_WriteTestForWriteProxyBO() {
		def proxData1 =
				[
						boMetaId  : TestEntities.EXAMPLE_REST_WRITEPROXYTEST.fqn,
						attributes: [
								strname   : 'Should be overitten by Write-Proxy',
								intnummer : 33,
								intnummer2: 3.3
						]
				]
		proxData1 = RESTHelper.createBo(proxData1, nuclosSession)

		assert proxData1.get("attributes").getAt("strname") == "Created by Write-Proxy"
		assert proxData1.get("attributes").getAt("intnummer") == 9 /*9 from Write-Proxy*/
	}


	@Test
	void _04_ReadAllInclWriteProxyBO() {
		RESTClient client = new RESTClient('WriteProxyTest', 'WriteProxyTest').login()
		def result = RESTHelper.getBos(TestEntities.EXAMPLE_REST_WRITEPROXYTEST.fqn, client) as Map
		def bos = result.get('bos') as List

		assert bos.size() == 4
	}
}