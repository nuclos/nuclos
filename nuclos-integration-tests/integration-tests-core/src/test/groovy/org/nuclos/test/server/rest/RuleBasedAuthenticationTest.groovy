package org.nuclos.test.server.rest

import org.apache.commons.lang.StringEscapeUtils
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.cookie.BasicClientCookie
import org.glassfish.web.util.HtmlEntityEncoder
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common2.StringUtils
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RuleBasedAuthenticationTest extends AbstractNuclosTest {

	@Test
	void _10_withLogin() {
		RESTClient client = new RESTClient(false)

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'withLogin': 'true'
		]) {
			assertResponse(client)
			failsafeAssert({
				client
					.getCookieStore()
					.getCookies()
					.find({ ("JSESSIONID" == it.getName()) }) != null
			}, 'Missing JSESSIONID cookie!')
		}
		assertResponse(client)
	}

	@Test
	void _11_withLoginAndLifetime_butWithoutRefresh() {
		RESTClient client = new RESTClient(false)

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'withLogin': 'true',
				'withLifetimeInSeconds': '2',
				'myRefresh': 'No'
		]) {
			assertResponse(client)
		}
		assertResponse(client)
		sleep(2500)
		expectError(client)
	}

	@Test
	void _12_withLoginAndLifetime_andAlwaysRefresh() {
		RESTClient client = new RESTClient(false)

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'withLogin': 'true',
				'withLifetimeInSeconds': '2',
				'myRefresh': 'always'
		]) {
			assertResponse(client)
		}
		assertResponse(client)
		sleep(2500)
		assertResponse(client)
	}

	@Test
	void _13_withLoginAndLifetime_andRefreshFromHeader() {
		RESTClient client = new RESTClient(false)

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'withLogin': 'true',
				'withLifetimeInSeconds': '2'
		]) {
			assertResponse(client)
		}
		assertResponse(client)
		sleep(2500)
		client.withHeaders([
		        'myRefresh': 'yes'
		]) {
			assertResponse(client)
		}
	}

	@Test
	void _20_withoutLogin() {
		RESTClient client = new RESTClient(false)

		client.withHeaders([
				'Authorization': 'i am nuclos',
		]) {
			assertResponse(client)
			failsafeAssert({
				client.getCookieStore().cookies.isEmpty()
			}, 'No cookie may be set if it is a ThrowAwaySession!')
		}

		expectError(client)
	}

	@Test
	void _30_checkUriInfo() {
		RESTClient client = new RESTClient(false)

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'checkUriInfo': 'yes'
		]) {
			assertResponse(client, true)
		}
	}

	@Test
	void _31_checkRemoteAddress() {
		RESTClient client = new RESTClient(false)

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'checkRemoteAddress': getMyIpAddresses()
		]) {
			assertResponse(client)
		}
	}

	@Test
	void _32_checkCookie() {
		RESTClient client = new RESTClient(false)

		String domain = AbstractNuclosTest.context.nuclosServerHost
		String testCookie = 'myCookie'
		String testCookieValue = 'testValue'
		BasicClientCookie cookie = new BasicClientCookie(testCookie, testCookieValue)
		cookie.setDomain(domain)
		client.cookieStore.addCookie(cookie)

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'checkCookie': testCookie,
				'checkCookieValue': testCookieValue
		]) {
			assertResponse(client)
		}
	}

	@Test
	void _60_businessExceptionInAuthRule() {
		RESTClient client = new RESTClient(false)

		client.withHeaders([
				'Authorization': 'ich bin nuclos',
				'withLogin': 'true'
		]) {
			expectError(client, 412, 'Authorization not supported: "ich bin nuclos"')
		}

		client.withHeaders([
				'Authorization': 'i am nuclos',
		]) {
			assertResponse(client)
		}
	}

	@Test
	void _61_runtimeExceptionInAuthRule() {
		RESTClient client = new RESTClient(false)

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'withLifetimeInSeconds': 'This is not a long value'
		]) {
			expectError(client, 500)
		}
	}

	@Test
	void _62_webExceptionInAuthRule() {
		RESTClient client = new RESTClient(true)

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'throwStatus': '403'
		]) {
			expectError(client, 403)
		}

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'throwStatus': '403',
				'throwMessage': 'My custom error message ;)'
		]) {
			expectError(client, 403, 'My custom error message ;)')
		}
	}

	@Test
	void _70_validCookieMustHaveHigherPriority_defaultLogin() {
		RESTClient client = new RESTClient('nuclos', '')
		client.login()
		assertResponse(client)

		client.withHeaders([
				'Authorization': 'ich bin nuclos', // businessException test case, ignored here
		]) {
			assertResponse(client)
		}
	}

	@Test
	void _71_validCookieMustHaveHigherPriority_ruleBasedAuth() {
		RESTClient client = new RESTClient(false)

		client.withHeaders([
				'Authorization': 'i am nuclos',
				'withLogin': 'true'
		]) {
			assertResponse(client)
		}

		client.withHeaders([
				'Authorization': 'ich bin nuclos', // businessException test case, ignored here
		]) {
			assertResponse(client)
		}
	}

	static void assertResponse(RESTClient client, boolean addCheckParameter = false) {
		HttpPost post = new HttpPost(RESTHelper.REST_BASE_URL + '/maintenance/managementconsole/-showMaintenance' + (addCheckParameter ? '?paramOnlyForTest=myValue' : ''))
		assert RESTHelper.requestString(client, post).startsWith('off\nNuclosConsole finished in')
	}

	static void expectError(RESTClient client, int status = 401, String errMessage = null) {
		expectRestException({
			assertResponse(client)
		}) {
			assert it.statusCode == status
			if (errMessage != null) {
				final errorText = it.errorText
				failsafeAssert ({
					// tomcat:
					errorText.contains(StringEscapeUtils.escapeHtml(errMessage)) ||
					// glassfish (may be a bug or https://github.com/javaee/grizzly/issues/1718 ?):
					errorText.contains(HtmlEntityEncoder.encodeXSS(HtmlEntityEncoder.encodeXSS(errMessage)))
				}, "Error text does not contain expected message \"" + errMessage + "\": " + errorText)
			}
		}
	}

	static String getMyIpAddresses() {
		Set<String> resultSet = new HashSet<>()
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces()
			while (interfaces.hasMoreElements()) {
				NetworkInterface iface = interfaces.nextElement()
				if (!iface.isUp())
					continue

				Enumeration<InetAddress> addresses = iface.getInetAddresses()
				while(addresses.hasMoreElements()) {
					InetAddress addr = addresses.nextElement()
					resultSet.add(addr.getHostAddress())
				}
			}
		} catch (SocketException e) {
			throw new RuntimeException(e)
		}
		return StringUtils.join(',', resultSet)
	}

}
