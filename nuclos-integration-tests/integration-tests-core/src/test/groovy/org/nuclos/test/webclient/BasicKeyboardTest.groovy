package org.nuclos.test.webclient

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.experimental.categories.Category
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Modal
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.account.PasswordChangeComponent
import org.nuclos.test.webclient.pageobjects.dashboard.Dashboard
import org.nuclos.test.webclient.util.Screenshot
import org.openqa.selenium.Keys
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@CompileStatic
class BasicKeyboardTest extends AbstractRerunnableWebclientTest {
	private EntityObject<String> testUser
	/**
	 * j) durch Suchfilter des Dashboards wechseln, Datensatz in einem Suchfilter des Dashboards öffnen
	 */

	private expected_field_changes = [
			['name': 'customerNumber', 'value': '454545'],
			['name': 'turnover', 'value': '1,00'],
			['name': 'name', 'value': 'Test-Customer changed by keys'],
			['name': 'birthday', 'value': '10.08.2006'],
			['name': 'active', 'value': 'true'],
			['name': 'discount', 'value': '15,00']
	]

	@Before
	void setup() {
		logout()
		TestDataHelper.insertTestData(nuclosSession)
		this.testUser = RESTHelper.createUser('keyboardTest', 'test', ['Example user'], nuclosSession)
	}

	@After
	void teardown() {
		if (this.testUser != null && this.testUser.id) {
			nuclosSession.delete(this.testUser)
		}
	}

	@Test
	void testBasicUsageWithCatch() {
		try {
			testBasicUsage()
		} catch (Throwable t) {
			// catch everything to make screenshot
			Screenshot.take('huge-failure')
			throw t
		}
	}

	@Test
	void testSubformSavingEnteredText() {
		login('test', 'test')
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		keydownShortCut(true, false, false, '0')
		for (def i = 0; i < 19; i++) {
			sendKeys(Keys.TAB)
		}

		keydownShortCut(true, true, false, '0')
		sendKeys(Keys.TAB)
		sendKeys(Keys.ENTER)
		sendKeys('12')

		keydownShortCut(true, false, false, 's')
		waitForAngularRequestsToFinish()

		assert !EntityObjectComponent.isDirty()
		assert EntityObjectComponent
				.forDetail()
				.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
				.getRow(0)
				.getValue("price") == "12,00"
	}

	private void testBasicUsage() {
		keydownShortCut(true, false, false, 'l')

		failsafeAssert({ return checkCurrentActiveElement("input", ["name": "username"]); }, "User Input should be focused")
		keydownShortCut(false, false, true, 'a')
		sendKeys('keyboardTest')
		sendKeys(Keys.TAB)

		failsafeAssert({ return checkCurrentActiveElement("input", ["name": "password"]); }, "Password Input should be focused")
		sendKeys('test')
		sendKeys(Keys.TAB)

		failsafeAssert({ return checkCurrentActiveElement("input", ["name": "autologin"]); }, "AutoLogin Input should be focused")
		sendKeys(Keys.TAB)


		failsafeAssert({ return checkCurrentActiveElement("button", ["id": "submit"]); }, "Login Button should be focused")
		sendKeys(Keys.ENTER)

		waitForAngularRequestsToFinish()
		failsafeAssert({ return getDriver().getCurrentUrl().contains('dashboard') }, "After login we should routed to dashboard view")

		keydownShortCut(true, false, false, 'c')
		waitForAngularRequestsToFinish()

		/**
		 * Search
		 */
		keydownShortCut(true, false, false, 'f')
		waitForAngularRequestsToFinish()
		failsafeAssert({ return checkCurrentActiveElement("input", ['id': "text-search-input"]); }, "BO quick search should be active and has focus")
		sendKeys('inactive')
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return Sidebar.getListEntryCount() == 1 }, "Quick search should be working and filtered for one customer only")
		keydownShortCut(true, false, false, 'f')
		keydownShortCut(false, false, true, 'a')
		sendKeys(Keys.DELETE)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return Sidebar.getListEntryCount() > 1 }, "Quick search is empty and all entries should be visible")

		// create search filter
		keydownShortCut(true, true, false, 'f')
		failsafeAssert({ return $('#search-editor-container').isDisplayed(); }, "Search Container should be expanded")

		keydownShortCut(true, true, false, 'g')
		failsafeAssert({ return checkCurrentActiveElement("input", ["id": "nuc-search-attribute-selector-dropdown"]); }, "Search attribute selector should have focus")

		sendKeys('customer number')
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		sendKeys(Keys.DOWN)
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return checkCurrentActiveElement("input", ["id": "search-value-string-example_rest_Customer_customerNumber"]); }, "Customer Number field should be added and have focus")
		sendKeys('10000')
		keydownShortCut(false, true, false, Keys.TAB)
		failsafeAssert({ return checkCurrentActiveElement("select", ["id": "search-operator-example_rest_Customer_customerNumber"]); }, "Customer Number operator should have focus")
		sendKeys(Keys.DOWN)
		sendKeys(Keys.DOWN)
		sendKeys(Keys.DOWN)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return Sidebar.getListEntryCount() == 2 }, "There should be filtered only 2 entries")

		keydownShortCut(true, true, false, 'o')
		waitForAngularRequestsToFinish()
		failsafeAssert({ return $('#searchfilter-selector-dropdown-link').text == "Suchfilter 1"; }, "Search filter should be saved")
		keydownShortCut(true, true, false, 't')
		failsafeAssert({ return $('#searchfilter-selector-dropdown-link').text == "[Meine Suche]"; }, "Search filter should have changed to my search")

		keydownShortCut(true, true, false, 'g')
		failsafeAssert({ return checkCurrentActiveElement("input", ["id": "nuc-search-attribute-selector-dropdown"]); }, "Search attribute selector should have focus")

		sendKeys('customer number')
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		sendKeys(Keys.DOWN)
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return checkCurrentActiveElement("input", ["id": "search-value-string-example_rest_Customer_customerNumber"]); }, "Customer Number field should be added and have focus")
		sendKeys('10000')
		keydownShortCut(false, true, false, Keys.TAB)
		failsafeAssert({ return checkCurrentActiveElement("select", ["id": "search-operator-example_rest_Customer_customerNumber"]); }, "Customer Number operator should have focus")
		sendKeys(Keys.DOWN)
		sendKeys(Keys.DOWN)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return Sidebar.getListEntryCount() == 1 }, "There should be filtered only one entry")

		keydownShortCut(true, true, false, 'o')
		waitForAngularRequestsToFinish()
		failsafeAssert({ return $('#searchfilter-selector-dropdown-link').text == "Suchfilter 2"; }, "Search filter should be saved")

		keydownShortCut(true, true, false, 't')
		failsafeAssert({ return $('#searchfilter-selector-dropdown-link').text == "[Meine Suche]"; }, "Search filter should have changed to my search")

		keydownShortCut(true, true, false, 'f')
		failsafeAssert({ return $('#search-editor-container') == null; }, "Search Container should not be visible anymore")

		keydownShortCut(true, false, true, Keys.HOME)
		failsafeAssert({ return EntityObjectComponent.forDetail().getAttribute('customerNumber') == "22222"; }, "First element in list should be selected")
		keydownShortCut(true, false, false, 'x')
		waitForAngularRequestsToFinish()
		def deleteModal = Modal.forSelector("nuc-dialog")
		waitFor {
			return deleteModal.isVisible()
		}
		failsafeAssert({ return deleteModal.getTitle() == "Löschen"; }, "Delete modal should be opened")
		failsafeAssert({ return checkCurrentActiveElement("button", ["id": "button-ok"]); }, "Acceptance button should be focused")
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return EntityObjectComponent.forDetail().getAttribute('customerNumber') == "9001"; }, "First element in list should be selected")

		/**
		 * Checking tab focus and change content
		 */
		keydownShortCut(true, false, false, '0')
		expected_field_changes.each {
			Map<String, String> elementToCheck = it as Map<String, String>
			failsafeAssert({ return checkCurrentActiveElement('input', ['name': 'attribute-' + elementToCheck.get('name')]); }, "${elementToCheck.get('name')} Input should have focus")
			if (elementToCheck.get('value') == 'true') {
				sendKeys(Keys.SPACE)
			} else {
				keydownShortCut(false, false, true, 'a')
				sendKeys(elementToCheck.get('value'))
			}
			sendKeys(Keys.TAB)
		}
		keydownShortCut(true, false, false, 's')
		waitForAngularRequestsToFinish()
		refresh()
		waitForAngularRequestsToFinish()

		expected_field_changes.each {
			Map<String, String> elementToCheck = it as Map<String, String>
			if (elementToCheck.get('value') == 'true') {
				failsafeAssert({ return EntityObjectComponent.forDetail().getAttribute(elementToCheck.get('name')) != null }, "${elementToCheck.get('name')} Input should have changed value after save")
			} else {
				failsafeAssert({ return EntityObjectComponent.forDetail().getAttribute(elementToCheck.get('name')) == elementToCheck.get('value') }, "${elementToCheck.get('name')} Input should have changed value after save")
			}
		}

		// delete old subform entry and add new one
		// first navigate to subform focus
		keydownShortCut(true, false, false, '0')
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		keydownShortCut(true, true, false, '0')
		sendKeys(Keys.SPACE)
		keydownShortCut(true, true, false, 'x')
		Screenshot.take('subform-editing-delete')
		failsafeAssert({ return EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer').getDeletedRowCount() == 1; }, "There should be one deleted entry by now")
		keydownShortCut(true, true, false, 'n')
		Screenshot.take('subform-editing-add')
		waitForAngularRequestsToFinish()
		keydownShortCut(true, true, false, '0')
		Screenshot.take('subform-editing-first-row')
		waitForAngularRequestsToFinish()
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		sendKeys('TestKeys')
		waitForAngularRequestsToFinish()
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		sendKeys(Keys.TAB)
		waitForAngularRequestsToFinish()
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		sendKeys('123456')
		waitForAngularRequestsToFinish()
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		sendKeys(Keys.TAB)
		waitForAngularRequestsToFinish()
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		sendKeys('Something')
		waitForAngularRequestsToFinish()
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer').getNewRowCount() == 1; }, "There should be one new entry by now")
		Screenshot.take('subform-editing-entries')
		keydownShortCut(true, false, false, 's')
		Screenshot.take('subform-editing-save')
		waitForAngularRequestsToFinish()
		failsafeAssert({ return !EntityObjectComponent.isDirty() }, 'EO should be saved correctly')
		failsafeAssert({ return $('nuc-alert-modal-component') == null }, "There should be no validation error")
		failsafeAssert({ return EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer').getRowCount() == 1; }, "There should be one entry at all by now")
		Screenshot.take('subform-editing-end')

		failsafeAssert({
			return EntityObjectComponent.
					forDetail().
					getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer').
					getRow(0).
					getValue('city') == 'TestKeys'
		}, "First Row City should be correctly set")
		failsafeAssert({
			return EntityObjectComponent.
					forDetail().
					getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer').
					getRow(0).
					getValue('zipCode') == '123456'
		}, "First Row zipCode should be correctly set")
		failsafeAssert({
			return EntityObjectComponent.
					forDetail().
					getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer').
					getRow(0).
					getValue('street') == 'Something'
		}, "First Row street should be correctly set")

		//create new bo
		keydownShortCut(true, false, false, 'n')
		waitForAngularRequestsToFinish()
		keydownShortCut(true, false, false, '0')
		sendKeys('1234')
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		sendKeys('TestKeysNewEntered')
		keydownShortCut(true, false, false, 's')
		waitForAngularRequestsToFinish()
		failsafeAssert({ return EntityObjectComponent.forDetail().getAttribute('customerNumber') == '1234'; }, 'Newly added bo should be saved and created')
		failsafeAssert({ return !EntityObjectComponent.isDirty() }, 'EO should be saved correctly')
		failsafeAssert({ return $('nuc-alert-modal-component') == null }, "There should be no validation error")
		Screenshot.take('BO Creation')

		// switch to dashboard view
		keydownShortCut(false, true, true, 'd')
		waitForAngularRequestsToFinish()
		failsafeAssert({ return driver.currentUrl.contains('dashboard'); }, "We should have navigated to the dashboard with CTRL+SHIFT+D")

		createDashboards:
		{
			Dashboard.addNew()
			waitForAngularRequestsToFinish()
			Dashboard.name = 'KeyboardTest-1'
			waitForAngularRequestsToFinish()
			waitForAngularRequestsToFinish()
			Dashboard.closeConfig()
			waitForAngularRequestsToFinish()

			Dashboard.addNew()
			waitForAngularRequestsToFinish()
			Dashboard.name = 'KeyboardTest-2'
			waitForAngularRequestsToFinish()
			Dashboard.addSearchfilterTaskListItem('[Customer] Suchfilter 1')
			waitForAngularRequestsToFinish()
			Dashboard.addSearchfilterTaskListItem('[Customer] Suchfilter 2')
			waitForAngularRequestsToFinish()
			Dashboard.closeConfig()
			waitForAngularRequestsToFinish()
		}
		failsafeAssert({ return Dashboard.isActive('KeyboardTest-2'); }, "Last created dashboard should have focus by now")
		keydownShortCut(true, true, true, 't')
		failsafeAssert({ return Dashboard.isActive('KeyboardTest-1'); }, "After CTRL+ALT+SHIFT+T we should have selected first dashboard view")
		keydownShortCut(true, false, true, 't')
		failsafeAssert({ return Dashboard.isActive('KeyboardTest-2'); }, "After CTRL+ALT+T we should have selected second dashboard view")


		keydownShortCut(true, false, false, '0')
		failsafeAssert({ return $('gridster-item[item-x="0"] div.ag-row[row-index="0"]').getAttribute('class').contains('ag-row-selected'); }, "First row of first panel should be selected")
		keydownShortCut(true, false, false, Keys.ARROW_DOWN)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return $('gridster-item[item-x="0"] div.ag-row[row-index="1"]').getAttribute('class').contains('ag-row-selected'); }, "Second row of first panel should be selected")
		keydownShortCut(true, false, false, Keys.ARROW_UP)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return $('gridster-item[item-x="0"] div.ag-row[row-index="0"]').getAttribute('class').contains('ag-row-selected'); }, "First row of first panel should be selected")
		keydownShortCut(true, false, false, Keys.ENTER)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return driver.currentUrl.contains('example_rest_Customer'); }, "We should have navigated to customer")

		keydownShortCut(false, true, true, 'd')
		waitForAngularRequestsToFinish()
		failsafeAssert({ return getDriver().getCurrentUrl().contains('dashboard'); }, "We should have navigated to the dashboard with CTRL+SHIFT+D")

		screenshot('Dashboard view')
		keydownShortCut(true, false, false, '1')
		screenshot('ALT+1 hit')
		waitForAngularRequestsToFinish()
		failsafeAssert({ return !$('gridster-item[item-x="0"] div.ag-row[row-index="0"]')?.getAttribute('class')?.contains('ag-row-selected'); }, "First row of first panel should be deselected")
		failsafeAssert({ return $('gridster-item[item-x="1"] div.ag-row[row-index="0"]')?.getAttribute('class')?.contains('ag-row-selected'); }, "First row of second panel should be selected")
		keydownShortCut(true, false, false, Keys.ENTER)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return getDriver().getCurrentUrl().contains('example_rest_Customer'); }, "We should have navigated to customer")

		// change password
		keydownShortCut(true, true, false, 'p')
		waitForAngularRequestsToFinish()
		failsafeAssert({ return checkCurrentActiveElement('input', ['id': 'oldPassword']); }, "Opening password change, old password field should have focus")
		sendKeys('test')
		sendKeys(Keys.TAB)
		failsafeAssert({ return checkCurrentActiveElement('input', ['id': 'newPassword']); }, "After tabbing new password field should be focused")
		sendKeys('testChanged')
		sendKeys(Keys.TAB)
		failsafeAssert({ return checkCurrentActiveElement('input', ['id': 'confirmNewPassword']); }, "After tab new password confirm field should be focused")
		sendKeys('testChanged')
		sendKeys(Keys.TAB)
		failsafeAssert({ return checkCurrentActiveElement('button', ['type': 'submit']); }, "Last tab should be submit button")
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return PasswordChangeComponent.successMessage; }, "Password should be changed correctly")


		// Logout
		keydownShortCut(true, true, false, 'l')
		// After logout shortcut we should on the login site
		assertLoggedOut()
	}
}
