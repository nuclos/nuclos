package org.nuclos.test.webclient


import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.MenuComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.search.Searchbar

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DataLanguageTest extends AbstractRerunnableWebclientTest {

	@Before
	@Override
	void beforeEach() {
		AbstractWebclientTest.setup(false, false)
		org.nuclos.test.server.DataLanguageTest.setupDataLanguageTestData()
	}

	@Test
	void testLoginWithDataLanguage() {
		loginUnsafe(new LoginParams(username: 'nuclos', locale: Locale.GERMAN, datalanguage: 'Deutsch (DE)')) // Primary language
		assertLoggedIn('nuclos')

		EntityObjectComponent eoc = EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		Searchbar.search('Teddy')
		assert Sidebar.listEntries.size() == 1

		Sidebar.selectEntry(0)
		assert eoc.getAttribute('artikelname') == 'Teddybär'

		logout()
		assertLoggedOut()

		loginUnsafe(new LoginParams(username: 'nuclos', locale: Locale.GERMAN, datalanguage: 'Schwedisch (SE)'))
		assertLoggedIn('nuclos')

		eoc = EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		Searchbar.search('Teddy')
		assert Sidebar.listEntries.size() == 1

		Sidebar.selectEntry(0)
		assert eoc.getAttribute('artikelname') == 'Teddybär' // not english!

		eoc.setAttribute('artikelname', 'Nallebjörn')
		eoc.save()
		eoc.reload()
		assert eoc.getAttribute('artikelname') == 'Nallebjörn' // now swedish

		logout()
		assertLoggedOut()

		loginUnsafe(new LoginParams(username: 'nuclos', locale: Locale.GERMAN, datalanguage: 'Deutsch (DE)'))
		assertLoggedIn('nuclos')

		eoc = EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		Searchbar.search('Teddy')
		assert Sidebar.listEntries.size() == 1

		Sidebar.selectEntry(0)
		assert eoc.getAttribute('artikelname') == 'Teddybär' // not swedish!
	}

	@Test
	void testSwitchingDataLanguage() {
		loginUnsafe(new LoginParams(username: 'nuclos', locale: Locale.ENGLISH, datalanguage: 'English (GB)')) // Primary language
		assertLoggedIn('nuclos')

		MenuComponent.toggleUserMenu()
		assert MenuComponent.getSelectedDataLanguage() == 'English (GB)'

		EntityObjectComponent eoc = EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		Searchbar.search('Teddy')
		assert Sidebar.listEntries.size() == 1

		Sidebar.selectEntry(0)
		assert eoc.getAttribute('artikelname') == 'Teddy bear'

		MenuComponent.toggleUserMenu()
		MenuComponent.clickDataLanguage('German (DE)')

		eoc = EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		Searchbar.search('Teddy')
		assert Sidebar.listEntries.size() == 1

		Sidebar.selectEntry(0)
		assert eoc.getAttribute('artikelname') == 'Teddybär'

		MenuComponent.toggleUserMenu()
		assert MenuComponent.getSelectedDataLanguage() == 'German (DE)'
	}

}
