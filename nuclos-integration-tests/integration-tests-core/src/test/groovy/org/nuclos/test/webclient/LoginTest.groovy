package org.nuclos.test.webclient

import org.json.JSONObject
import org.junit.After
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.account.PasswordChangeComponent
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.support.ui.ExpectedConditions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LoginTest extends AbstractRerunnableWebclientTest {

	@After
	void cleanup() {
		nuclosSession.deleteUserByUsername("foobar")
		RESTHelper.maintenanceModeUpdate('off', nuclosSession)
	}

	@Before
	@Override
	void beforeEach() {
		nuclosSession.deleteUserByUsername("foobar")
		AbstractWebclientTest.setup(false, false)
	}

	@Test
	void testSsoMaintenanceLogin() {
		RESTHelper.createUser(
				'foobar',
				'test',
				['Example user'],
				nuclosSession,
				'foo@bar.com',
				true,
				true
		)

		RESTHelper.maintenanceModeUpdate('on', nuclosSession)
		waitFor {
			RESTHelper.maintenanceModeStatus(nuclosSession) == 'on'
		}

		startHydraLogin('foo@bar.com', 'foobar', false)
		assertLoggedIn('foobar')
	}

	@Test
	void testAlreadyLoggedInRedirection() {
		RESTHelper.createUser('foobar', 'test', ['Example user'], nuclosSession, 'foo@bar.com', true)
		login('foobar', 'test')

		assertLoggedIn('foobar')

		getUrlHash('/login')
		failsafeAssert({ return driver.currentUrl.contains('dashboard'); }, "We should have navigated to the dashboard with valid login context")

		logout()
		getUrlHash('/login')
		failsafeAssert({ return driver.currentUrl.contains('login'); }, "Without login context we should still be on login page")
	}

	@Test
	void testLoginRestrictionPassword() {
		RESTHelper.createUser('foobar', 'test', ['Example user'], nuclosSession, 'foo@bar.com', true, false, 'PASSWORD')

		login('foobar', 'test')
		assertLoggedIn('foobar')

		logout()
		assertLoggedOut()

		startHydraLogin('foo@bar.com', 'foobar', false)
		assertLoggedOut()
	}

	@Test
	void testLoginRestrictionSSO() {
		RESTHelper.createUser('foobar', 'test', ['Example user'], nuclosSession, 'foo@bar.com', true, false, 'SSO')

		login('foobar', 'test')
		assertLoggedOut()


		startHydraLogin('foo@bar.com', 'foobar', false)
		assertLoggedIn('foobar')
		logout()
		assertLoggedOut()
	}

	@Test
	void testSsoLogin() {

		_10_noUser:
		{
			startHydraLogin('foo@bar.com', 'foobar')
			assertLoggedOut()
			String errorMessage = AuthenticationComponent.errorMessage
			assert errorMessage.contains('A user account has not yet been created. Please contact your administrator for further information!') ||
					errorMessage.contains('Es wurde noch kein Benutzerkonto für Sie erstellt.')
		}

		EntityObject user = RESTHelper.createUser('foobar', 'test', ['Example user'], nuclosSession,
				'foo@bar.com', true)

		_20_successfulLoginWithUserinfo:
		{
			startHydraLogin('foo@bar.com', 'foobar', false)
			assertLoggedIn('foobar')
			// testLastLoginSet:
			user = RESTHelper.findUserByUsername('foobar', nuclosSession)
			assert user.getAttribute('lastLogin') != null
		}

		logout()
		assertLoggedOut()

		_21_successfulLoginWithIdToken:
		{
			startHydraLogin('foo@bar.com', 'foobar', true)
			assertLoggedIn('foobar')
		}

		logout()
		assertLoggedOut()

		_22_successfulLoginWithRememberMe:
		{
			startHydraLogin('foo@bar.com', 'foobar', true, true)
			assertLoggedIn('foobar')
		}

		logoutUnsafe(false)
		finishHydraLogout()
		assertLoggedOut()

		_26_refreshTokenTest:
		{
			startHydraLogin('foo@bar.com', 'foobar')
			assertLoggedIn('foobar')
			// AccessToken lifetime is set to 10 seconds (hydra/Dockerfile: ENV TTL_ACCESS_TOKEN=10s)
			long sleeptime = 11 * 1000l;
			sleep(sleeptime)
			refresh()

			if (!isLoggedIn('foobar')) {
				// first time after starting the hydra container refresh fails with:
				// "The provided authorization grant (e.g., authorization code, resource owner credentials)
				// or refresh token is invalid, expired, revoked, does not match the redirection URI used in the authorization request,
				// or was issued to another client (400 invalid_grant) during token request against "http://...."
				startHydraLogin('foo@bar.com', 'foobar')
				sleep(sleeptime)
				refresh()
			}

			assertLoggedIn('foobar')
		}

		logout()
		assertLoggedOut()

		_30_deniedLogin:
		{
			AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('.sso-button'))).click()
			AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('#reject'))).click()

			assertLoggedOut()
			String errorMessage = AuthenticationComponent.errorMessage
			assert errorMessage.contains('SSO authentication could not be completed successfully.') ||
					errorMessage.contains('SSO Authentifizierung konnte nicht erfolgreich abgeschlossen werden.')
		}

		logout()
		assertLoggedOut()

		_40_checkLocked:
		{
			user.setAttribute('locked', true)
			user.save()

			startHydraLogin('foo@bar.com', 'foobar')

			assertLoggedOut()
			String errorMessage = AuthenticationComponent.errorMessage
			assert errorMessage.contains('Account is locked. Please contact your administrator for further information!') ||
					errorMessage.contains('Das Benutzerkonto ist gesperrt. Bitte wenden Sie sich an Ihren Administrator!') ||
					errorMessage.contains('Your account is not activated.') ||
					errorMessage.contains('Der Zugang wurde noch nicht aktiviert.')
		}

		user.setAttribute('locked', false)
		user.save()

		_45_checkExpired:
		{
			user.setAttribute('expirationDate', '2020-01-01')
			user.save()

			startHydraLogin('foo@bar.com', 'foobar')

			assertLoggedOut()
			String errorMessage = AuthenticationComponent.errorMessage
			assert errorMessage.contains('Your account has expired.') ||
					errorMessage.contains('Ihr Zugang ist abgelaufen.')
		}

		user.setAttributeNull('expirationDate')
		user.save()

		_60_passwordBasedLoginForbidden:
		{
			openStartpage()
			assert AuthenticationComponent.usernameInput != null
			assert AuthenticationComponent.passwordInput != null
			assert AuthenticationComponent.submitButton != null

			try {
				Map<String, String> params = [
						'SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED': 'false'
				]
				getNuclosSession().patchSystemParameters(params)
				openStartpage() // <-- should now redirect to sso login directly
				waitFor(10, {
					getCurrentUrl().startsWith(WebclientTestContext.instance.hydraConsentServer)
				})

				hydraLogin('foo@bar.com', 'foobar')
				assertLoggedIn('foobar')
				logoutUnsafe() // <-- should stay on login page / no redirect after logouts
				assert AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('.sso-button'))).isDisplayed()
				assert AuthenticationComponent.usernameInput == null
				assert AuthenticationComponent.passwordInput == null
				assert AuthenticationComponent.submitButton == null

			} finally {
				Map<String, String> params = [
						'SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED': 'true'
				]
				getNuclosSession().patchSystemParameters(params)
			}

		}
	}

	@Test
	void testAnonymousLogin() {
		nuclosSession.setSystemParameters(['ANONYMOUS_USER_ACCESS_ENABLED':'true'])
		RESTHelper.createUser('anonymous', 'anonymous', ['Example readonly'], nuclosSession)
		refresh()

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		assert getCurrentUrl().endsWith("/view/$TestEntities.EXAMPLE_REST_ORDER.fqn")
		assertLoggedIn(' Login ')

		click($zZz('#user-menu').$('.nav-link'))
		assertLoggedOut()

		login('test','test')
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		logout()
		assertLoggedOut()
	}

	private finishHydraLogout() {
		AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('#accept'))).click()
	}

	private startHydraLogin(String email, String password, boolean withOpenId = false, boolean withRememberMe = false) {
		AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('.sso-button'))).click()
		hydraLogin(email, password, withOpenId, withRememberMe)
	}

	private hydraLogin(String email, String password, boolean withOpenId = false, boolean withRememberMe = false) {
		AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('#email'))).click()

		sendKeys(email)
		sendKeys(Keys.TAB)
		sendKeys(password)
		if (withRememberMe) {
			AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('#remember'))).click()
		}
		AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('#accept'))).click()

		if (withOpenId) {
			AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('#openid'))).click()
		}
		AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('#offline'))).click()
		AbstractWebclientTest.waitDriver.until(ExpectedConditions.elementToBeClickable(By.cssSelector('#accept'))).click()
		waitForAngularRequestsToFinish()
	}

	@Test
	void testDefaultLogin() {
		String sessionId

		_05_normalLogin:
		{
			logout()
			login('nuclos', '')

			Log.info 'Login test...'

			assertLoggedIn('nuclos')

			getUrlHash('/businesstests')
			assert currentUrl.endsWith('/businesstests')

			AbstractWebclientTest.refresh()
			assert currentUrl.endsWith('/businesstests')

			assertLoggedIn('nuclos')
			logout()
			assertLoggedOut()

			openStartpage()
			assertLoggedOut()
			assert !AuthenticationComponent.errorMessage

			// TODO: Restart the browser and assert that we are still logged out (i.e. no autologin happened).
			// Unfortunately if we restart the Selenium session, all cookies are deleted, so we
			// cannot relyably test this.
		}

		_10_autoLogin:
		{
			assertLoggedOut()

			login('nuclos', '', true)
			assertLoggedIn('nuclos')

			logout()
			assertLoggedOut()

			// This may help against "Timed out receiving message from renderer" during openStartpage()
			sleep(1000)

			waitForAngularRequestsToFinish()
			getBrowserHar {
				openStartpage()
				waitForAngularRequestsToFinish()
			}.with {
				// There should be no HTTP error codes, except 401 because we are logged out
				Set<Integer> responseCodes = it.log.entries*.response*.status*.toInteger().unique().toSet()
				assert [0, 200, 304, 401].containsAll(responseCodes)
			}
			assertLoggedOut()

			// TODO: Login with autologin checked, restart the browser and assert that we are automatically
			// logged in again (i.e. autologin happened).
			// Unfortunately if we restart the Selenium session, all cookies are deleted, so we cannot test this.
		}

		/**
		 * Tests if login is still possible after a failed login attempt.
		 */
		_20_testFailedLogin:
		{
			assertLoggedOut()

			loginUnsafe(new LoginParams(username: '123'))

			assertLoggedOut()

			String errorMessage = AuthenticationComponent.errorMessage
			assert errorMessage.contains('Wrong username/password') || errorMessage.contains('Falscher Benutzername/Passwort')

			login('nuclos', '')

		}

		/**
		 * Tests if we are automatically redirected to the Login page, if we have no valid session.
		 */
		_25_testUnauthorizedRedirect:
		{
			logout()
			assertLoggedOut()

			getUrlHash('/businesstests')
			assertLoggedOut()
			assert currentUrl.endsWith('/login')

			login('nuclos', '')

			assert currentUrl.endsWith('/businesstests')
		}

		/**
		 * Tests if we are automatically redirected to the last remembered location.
		 */
		_30_testRedirectToLastLocation:
		{
			getUrlHash('/businesstests/advanced')

			logout()
			login('nuclos', '')

			assert currentUrl.endsWith('/businesstests/advanced')
		}

		/**
		 * Tests if locale changes are applied and remembered.
		 */
		_35_testLocaleChange:
		{
			logout()

			LocaleChooser.locale = Locale.ENGLISH
			assert LocaleChooser.locale == 'en'

			assert AuthenticationComponent.usernameLabel == 'Username'
			assert AuthenticationComponent.passwordLabel == 'Password'

			LocaleChooser.locale = Locale.GERMANY
			assert LocaleChooser.locale == 'de'

			assert AuthenticationComponent.usernameLabel == 'Benutzername'
			assert AuthenticationComponent.passwordLabel == 'Passwort'

			LocaleChooser.locale = Locale.ENGLISH
			assert LocaleChooser.locale == 'en'

			login(new LoginParams(username: 'nuclos', locale: Locale.ENGLISH))

			SessionInfo.open()
			sessionId = SessionInfo.getSessionId()
			assertServerLocale(Locale.ENGLISH, sessionId)

			getUrlHash('/dashboard')

			// Check if a menu item and a text in the dashboard are in english now
			MenuComponent.toggleUserMenu()

			assert MenuComponent.preferenceLinkText.contains('Manage preferences')

			refresh()

			assert LocaleChooser.locale == 'en'
			assertServerLocale(Locale.ENGLISH, sessionId)

			// Check if a menu item and a text in the dashboard are in english now
			MenuComponent.toggleUserMenu()

			assert MenuComponent.preferenceLinkText.contains('Manage preferences')

			LocaleChooser.locale = Locale.GERMANY
			assertServerLocale(Locale.GERMANY, sessionId)
			// TODO: The menu component is not updated yet after the locale is changed
			//assert MenuComponent.preferenceLinkText.contains('Manage preferences')
		}

		_40_testOpenPasswordChange:
		{
			logout()
			login('test', 'test')

			MenuComponent.toggleUserMenu()
			MenuComponent.clickChangePasswordLink()

			assert currentUrl.contains('/account/changePassword')

			assert !PasswordChangeComponent.errorMessage
			assert !PasswordChangeComponent.successMessage

		}

		_42_testChangePassword:
		{
			testEmptyOldPassword:
			{
				PasswordChangeComponent.submit()
				assert PasswordChangeComponent.errorMessage
			}

			PasswordChangeComponent.oldPassword = 'test'

			testEmptyNewPassword:
			{
				PasswordChangeComponent.submit()
				assert PasswordChangeComponent.errorMessage
			}

			PasswordChangeComponent.newPassword = 'test2'

			testEmptyConfirmNewPassword:
			{
				PasswordChangeComponent.submit()
				assert PasswordChangeComponent.errorMessage
			}

			PasswordChangeComponent.confirmNewPassword = 'test2'

			testSuccessfulChange:
			{
				PasswordChangeComponent.submit()
				assert PasswordChangeComponent.successMessage
			}
		}

		_45_testLoginWithChangedPassword:
		{
			logout()
			login('test', 'test2')
		}

		_50_testChangePasswordBack:
		{
			MenuComponent.toggleUserMenu()
			MenuComponent.clickChangePasswordLink()
			assert !PasswordChangeComponent.errorMessage
			assert !PasswordChangeComponent.successMessage

			PasswordChangeComponent.oldPassword = 'test2'
			PasswordChangeComponent.newPassword = 'test'
			PasswordChangeComponent.confirmNewPassword = 'test'

			PasswordChangeComponent.submit()
			assert PasswordChangeComponent.successMessage
		}

		_55_testRequestsWhenLoggedIn:
		{
			waitForAngularRequestsToFinish()
			getBrowserHar {
				openStartpage()
				waitForAngularRequestsToFinish()
			}.with {
				// There should be no HTTP error codes
				Set<Integer> responseCodes = it.log.entries*.response*.status*.toInteger().unique().toSet()
				assert [200, 204, 304].containsAll(responseCodes)
			}
		}

		_60_loginWithExpiredCredentials:
		{
			// Set the "passwordChangeRequired" flag of the User test, this should cause a logout
			EntityObject<String> testUser = RESTHelper.findUserByUsername('test', nuclosSession)
			assert testUser != null
			testUser.setAttribute('passwordChangeRequired', true)
			testUser.save()

			// Should cause a redirect to the login page, since the session was invalidated
			EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
			assertLoggedOut()

			// Should redirect to the password change page
			loginUnsafe(new LoginParams(username: 'test', password: 'test'))
			assertPassordChange()
		}

		_62_setNewPassword:
		{
			PasswordChangeComponent.oldPassword = 'test'
			PasswordChangeComponent.newPassword = 'test3'
			PasswordChangeComponent.confirmNewPassword = 'test3'
			PasswordChangeComponent.submit()

			assert PasswordChangeComponent.successMessage

			PasswordChangeComponent.clickLoginLink()
		}

		_64_loginWithNewPassword:
		{
			assert login('test', 'test3')
		}
	}

	private assertServerLocale(Locale locale, String sessionId) {
		RESTClient client = new RESTClient('test', 'test')
		client.sessionId = sessionId

		JSONObject session = client.getSessionData()
		assert session.get('locale') == locale.getLanguage()
	}

	private void assertPassordChange() {
		assert $zZz('#password-change-container')
		assert currentUrl.contains('changePassword')
	}

}
