package org.nuclos.test.webclient


import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.pageobjects.AuthenticationComponent
import org.nuclos.test.webclient.pageobjects.DashboardComponent
import org.nuclos.test.webclient.pageobjects.MaintenanceModeAdminComponent
import org.nuclos.test.webclient.pageobjects.MenuComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class MaintenanceModeTest extends AbstractRerunnableWebclientTest {

	@Before
	void beforeEach() {
		AbstractWebclientTest.setup(false, false)
	}

@Test
void runTest() {
	_00_setup: {
		login('nuclos')
	}

	_01_activateMaintenanceMode: {
		MaintenanceModeAdminComponent.open()
		assert MaintenanceModeAdminComponent.status() == "off"
		MaintenanceModeAdminComponent.activateMaintenaceMode()
		assert MaintenanceModeAdminComponent.status() == "on"
	}

	_02_loginWithOtherUserExpectNoSuccess: {
		logout()
		loginUnsafe(new LoginParams(username: 'test', password: 'test'))
		waitForAngularRequestsToFinish()
		assert AuthenticationComponent.getErrorMessage().contains("Wartungsmodus") ||
				AuthenticationComponent.getErrorMessage().contains("maintenance")
	}


	_03_deactivateMaintenanceMode: {
		login('nuclos', '')
        assertLoggedIn('nuclos')
		MaintenanceModeAdminComponent.open()
		MaintenanceModeAdminComponent.deactivateMaintenaceMode()
	}

	_04_loginWithOtherUserExpectSuccess: {
		logout()
		login('test', 'test')
        assertLoggedIn('test')
	}

	_05_activeMaintenanceModeWithoutWebclient: {
		def nuclosSession = new RESTClient('nuclos', '');
		nuclosSession.login()
		RESTHelper.maintenanceModeUpdate('on', nuclosSession)


		MenuComponent.openMenu('Example', 'Article')
		$('#logo').click()

		assertLoggedOut()
	}

	_06_loginWithOtherUserExpectNoSuccessAgain: {
		logout()
		loginUnsafe(new LoginParams(username: 'test', password: 'test'))
		waitForAngularRequestsToFinish()
		assert AuthenticationComponent.getErrorMessage().contains("Wartungsmodus") ||
				AuthenticationComponent.getErrorMessage().contains("maintenance")
	}
}
}
