package org.nuclos.test.webclient

import org.junit.After
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ParameterTests extends AbstractRerunnableWebclientTest {

	@After
	void cleanupSystemparameter() {
		nuclosSession.setSystemParameters([:])
	}

	@Test
	void testAppNameParamIsSetCorrectly() {
		nuclosSession.setSystemParameters(['APP_NAME': 'Test Nuclet'])

		refresh()

		var documentTitle = getDriver().getTitle()
		assert documentTitle.startsWith('Test Nuclet'): 'SystemParameter APP_NAME should be showed correctly (was: ' + documentTitle + ')'
	}
}
