package org.nuclos.test.webclient


import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

import org.apache.commons.mail.util.MimeMessageParser
import org.jsoup.Jsoup
import org.junit.After
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common.PortAllocator
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.entityobject.DisclaimerTest
import org.nuclos.test.webclient.pageobjects.ActivationComponent
import org.nuclos.test.webclient.pageobjects.AuthenticationComponent
import org.nuclos.test.webclient.pageobjects.Busy
import org.nuclos.test.webclient.pageobjects.RegistrationComponent
import org.nuclos.test.webclient.pageobjects.account.ForgotLoginDetailsComponent
import org.nuclos.test.webclient.pageobjects.account.PasswordResetComponent

import com.icegreen.greenmail.user.GreenMailUser
import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.ServerSetup

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RegistrationTest extends AbstractRerunnableWebclientTest {
	private final String passwordStrengthDescription = 'Test Password Strength Description'
	private final String usernameDescription = 'Test Username Description'
	private final String usernameRegexp = 'test.+'\

	private GreenMail greenMail

	private ServerSetup serverSetup
	private GreenMailUser mailUser

	private Map<String, String> oldSystemParameters

	private String registrationEmail = 'testregistration@nuclos.de'

	private String activationUrl

	@Before()
	@Override
	void beforeEach() {
		AbstractWebclientTest.setup(true, false) {
			DisclaimerTest.createPrivacyPolicyViaRest()
		}

		setupRegistration()
	}

	private void setupRegistration() {
		setupMailServer:
		{
			// Copy the default GreenMail setups and make some adjustments
			final int port = PortAllocator.allocate()
			serverSetup = new ServerSetup(port, WebclientTestContext.instance.greenmailHost, WebclientTestContext.instance.greenmailPort)
			serverSetup.serverStartupTimeout = 10000
			greenMail = new GreenMail(serverSetup)

			//Start all email servers using non-default ports.
			greenMail.start()
			mailUser = greenMail.setUser("test-user@nuclos.de", "test", "test")
		}

		Map<String, String> params = [
				'SMTP Server'                   : greenMail.smtp.bindTo,
				'SMTP Port'                     : greenMail.smtp.port as String,
				'SMTP Username'                 : mailUser.email,
				'SMTP Password'                 : mailUser.password,
				'SMTP Authentication'           : 'Y',
				'SMTP Sender'                   : 'registration@nuclos.de',

				// Anonymous login and self registration
//				'ANONYMOUS_USER_ACCESS_ENABLED' : 'Y',
				'ROLE_FOR_SELF_REGISTERED_USERS': 'Example user',
				'ACTIVATION_EMAIL_SUBJECT'      : 'Nuclos account registration',
				'ACTIVATION_EMAIL_MESSAGE'      : activationMessage,
				'EMAIL_SIGNATURE'               : '<b>TODO: signature</b>',
				'INITIAL_ENTRY'                 : 'anonymous:' + TestEntities.EXAMPLE_REST_ORDER.fqn
		]

		getNuclosSession().setSystemParameters(params)

		nuclosSession.deleteUserByEmail(registrationEmail)
		nuclosSession.deleteUserByEmail('2' + registrationEmail)
		nuclosSession.deleteUserByEmail(registrationEmail + '2')
		RESTHelper.createUser('anonymous', 'anonymous', ['Example user'], nuclosSession)
	}

	private static void setupEmailSystemParameters() {
		getNuclosSession().patchSystemParameters([
				'USERNAME_EMAIL_SUBJECT': 'Forgot username',
				'USERNAME_EMAIL_MESSAGE': '''Hello {0} {1},
<br/><br/>
you are registered with the following detalis:
<br/><br/>
Username: {2}<br/>
First name: {0}<br/>
Last name: {1}<br/>
<br/><br/>
		Bye
''',
				'RESET_PW_EMAIL_SUBJECT': 'Forgot username',
				'RESET_PW_EMAIL_MESSAGE': """Hello {0} {1},
<br/><br/>
you are registered with the following details:
<br/><br/>
Username: {2}<br/>
First name: {0}<br/>
Last name: {1}<br/>
Password reset code: {3}<br/><br/>

to reset your password <a href="$WebclientTestContext.instance.nuclosWebclientBaseURL#/account/resetPassword/{2}/{3}">click here</a>.
<br/><br/>
		Bye
""".toString()
		])

		refresh()
	}

	@After
	@Override
	void afterEach() {
		if (greenMail) {
			greenMail.stop()
		}
		getNuclosSession().deleteUserByEmail('test@nuclos.de')
		AbstractWebclientTest.teardown()
	}

	@Test
	void runTest() {
		_00_loginAsAnonymous: {
			Map<String, String> params = [
					'ANONYMOUS_USER_ACCESS_ENABLED' : 'Y'
			]
			getNuclosSession().patchSystemParameters(params)

			logout()
			refresh()

			// Should be logged in as anonymous and redirected to the "initial entry" Order
			assert currentUrl.contains('example_rest_Order')
		}

		_01_loginAsAnonymousNotEnabledFailing: {
			Map<String, String> params = [
					'ANONYMOUS_USER_ACCESS_ENABLED' : 'N'
			]
			getNuclosSession().patchSystemParameters(params)

			logout()
			refresh()

			getUrlHash('login')

			loginUnsafe(new LoginParams(username: 'anonymous', password: ''))
			// TODO: Check message content
			assert AuthenticationComponent.errorMessage
		}

		_02_loginAsAnonymousNotEnabledWithPasswordSuccess: {
			Map<String, String> params = [
					'ANONYMOUS_USER_ACCESS_ENABLED' : 'N'
			]
			getNuclosSession().patchSystemParameters(params)

			logout()
			refresh()

			getUrlHash('login')

			loginUnsafe(new LoginParams(username: 'anonymous', password: 'anonymous'))

			assertLoggedIn('anonymous')
		}

		_05_register: {
			Map<String, String> params = [
					'ANONYMOUS_USER_ACCESS_ENABLED' : 'N'
			]
			getNuclosSession().patchSystemParameters(params)

			getUrlHash('/login')

			AuthenticationComponent.clickSelfRegistration()

			RegistrationComponent.submitAndAssertError(['Datenschutzerklärung', 'privacy consent'])
			RegistrationComponent.togglePrivacyConsent()

			RegistrationComponent.submitAndAssertError(['Benutzername', 'Username'])

			RegistrationComponent.username = 'test-registration'
			RegistrationComponent.submitAndAssertError(['Passwort', 'Password'])

			RegistrationComponent.password = 'test'
			RegistrationComponent.submitAndAssertError(['Unterschiedlich', 'Differing'])

			RegistrationComponent.confirmPassword = 'test2'
			RegistrationComponent.submitAndAssertError(['Unterschiedlich', 'Differing'])

			RegistrationComponent.confirmPassword = 'test'
			RegistrationComponent.submitAndAssertError(['Vorname', 'First name'])

			RegistrationComponent.firstname = 'Firstname'
			RegistrationComponent.submitAndAssertError(['Nachname', 'Last name'])

			RegistrationComponent.lastname = 'Lastname'
			RegistrationComponent.submitAndAssertError(['E-Mail', 'Email'])

			// Not a valid email format
			RegistrationComponent.email = 'test'
			// TODO: Check if the message really equals 'webclient.account.email.invalid'
			RegistrationComponent.submitAndAssertError(['E-Mail', 'Email'])


			RegistrationComponent.email = registrationEmail
			// TODO: Check if the message really equals 'webclient.account.email.confirmation.unequal'
			RegistrationComponent.submitAndAssertError(['E-Mail', 'Email'])

			// Unequal email and confirmation
			RegistrationComponent.email2 = registrationEmail + '2'
			// TODO: Check if the message really equals 'webclient.account.email.confirmation.unequal'
			RegistrationComponent.submitAndAssertError(['E-Mail', 'Email'])

			RegistrationComponent.email2 = registrationEmail

			assert !Busy.busy

			RegistrationComponent.submit()
			assert RegistrationComponent.successMessage
		}

		_10_loginBeforeActivation: {
			getUrlHash('/login')

			loginUnsafe(new LoginParams(username: 'test-registration', password: 'test'))

			// TODO: Check message content
			assert AuthenticationComponent.errorMessage
		}

		_15_activationEmail: {
			greenMail.waitForIncomingEmail(1)
			MimeMessage activationMail = greenMail.receivedMessages.last()
			greenMail.purgeEmailFromAllMailboxes()

			assert activationMail.from.first() == new InternetAddress('registration@nuclos.de')

			MimeMessageParser parser = new MimeMessageParser(activationMail)
			parser.parse()
			String htmlContent = parser.getHtmlContent()
			String plainContent = parser.getPlainContent()

			assert !plainContent
			assert htmlContent

			activationUrl = Jsoup.parse(htmlContent).select('a[href]').first().attr('href')
			assert activationUrl
		}

		_20_activateViaLink: {
			getUrl(activationUrl + '_wrong')
			assert ActivationComponent.errorMessage

			getUrl(activationUrl)
			assert ActivationComponent.successMessage

			getUrl(activationUrl)
			assert ActivationComponent.errorMessage.contains('Aktivierungscode ist ungültig.')
		}

		_25_loginAfterActivation: {
			getUrlHash('/login')
			assert login('test-registration', 'test')
		}

		_30_uniqueValues: {
			logout()
			AuthenticationComponent.clickSelfRegistration()

			RegistrationComponent.togglePrivacyConsent()
			RegistrationComponent.username = 'test-registration'
			RegistrationComponent.password = 'test'
			RegistrationComponent.confirmPassword = 'test'
			RegistrationComponent.firstname = 'Firstname'
			RegistrationComponent.lastname = 'Lastname'
			RegistrationComponent.email = registrationEmail
			RegistrationComponent.email2 = registrationEmail

			username: {
				RegistrationComponent.submitAndAssertError(['Benutzername', 'Username'])
				RegistrationComponent.username = 'testregistration2'
			}

			email: {
				RegistrationComponent.submitAndAssertError(['E-Mail', 'Email'])
				RegistrationComponent.email = '2' + registrationEmail
				RegistrationComponent.email2 = '2' + registrationEmail
			}

			RegistrationComponent.submit()
			assert RegistrationComponent.successMessage
		}

		_35_forgotLoginDetails: {
			if (!logout()) {
				getUrlHash('/login')
			}

			// Link should only be visible after all necessary system parameters are configured
			assert !AuthenticationComponent.forgotLoginDetailsLink

			setupEmailSystemParameters()
		}

		_40_forgotUsername: {
			if (!logout()) {
				getUrlHash('/login')
			}

			AuthenticationComponent.forgotLoginDetailsLink.click()

			assert ForgotLoginDetailsComponent.type == ForgotLoginDetailsComponent.TYPE.FORGOT_USERNAME
			assert !ForgotLoginDetailsComponent.submitButton.enabled
			assert !ForgotLoginDetailsComponent.usernameField?.displayed
			assert ForgotLoginDetailsComponent.emailField.displayed

			ForgotLoginDetailsComponent.email = 'non_existent_email@nuclos.de'

			ForgotLoginDetailsComponent.submitAndAssertError(['exist', 'There is no'])

			ForgotLoginDetailsComponent.email = 'test@nuclos.de'
			ForgotLoginDetailsComponent.submit()
			assert ForgotLoginDetailsComponent.successMessage

			greenMail.waitForIncomingEmail(1)
			MimeMessage activationMail = greenMail.receivedMessages.last()
			greenMail.purgeEmailFromAllMailboxes()

			assert activationMail.from.first() == new InternetAddress('registration@nuclos.de')

			MimeMessageParser parser = new MimeMessageParser(activationMail)
			parser.parse()
			String htmlContent = parser.getHtmlContent()

			assert htmlContent.contains('Username: test')
			assert htmlContent.contains('First name: test')
			assert htmlContent.contains('Last name: test')

			ForgotLoginDetailsComponent.clickLoginLink()
		}

		_45_forgotPassword: {
			AuthenticationComponent.forgotLoginDetailsLink.click()
			ForgotLoginDetailsComponent.type = ForgotLoginDetailsComponent.TYPE.FORGOT_PASSWORD

			assert ForgotLoginDetailsComponent.type == ForgotLoginDetailsComponent.TYPE.FORGOT_PASSWORD
			assert !ForgotLoginDetailsComponent.submitButton.enabled
			assert ForgotLoginDetailsComponent.usernameField.displayed
			assert !ForgotLoginDetailsComponent.emailField?.displayed

			ForgotLoginDetailsComponent.username = 'non_existent_user'

			ForgotLoginDetailsComponent.submitAndAssertError(['exist', 'There is no'])

			ForgotLoginDetailsComponent.username = 'test'
			ForgotLoginDetailsComponent.submit()
			assert ForgotLoginDetailsComponent.successMessage

			greenMail.waitForIncomingEmail(1)
			MimeMessage activationMail = greenMail.receivedMessages.last()
			greenMail.purgeEmailFromAllMailboxes()

			assert activationMail.from.first() == new InternetAddress('registration@nuclos.de')

			MimeMessageParser parser = new MimeMessageParser(activationMail)
			parser.parse()
			String htmlContent = parser.getHtmlContent()

			assert htmlContent.contains('Username: test')
			assert htmlContent.contains('First name: test')
			assert htmlContent.contains('Last name: test')
			assert htmlContent.contains('Password reset code: ')

			activationUrl = Jsoup.parse(htmlContent).select('a[href]').first().attr('href')
			assert activationUrl
		}

		_49_testWrongActivationLink:
		{
			getUrl(activationUrl + '_wrong')
			waitForAngularRequestsToFinish()

			assert PasswordResetComponent.errorMessage.contains('Dieser Aktivierungslink ist nicht mehr gültig.'):
					'Defect link should correctly notice'
			assert $("form[name='register']") == null: 'Reset form should not be displayed'
		}

		_50_resetPassword:
		{
			getUrl(activationUrl)
			waitForAngularRequestsToFinish()

			assert !PasswordResetComponent.errorMessage
			assert !PasswordResetComponent.successMessage
			assert $("form[name='register']") != null: 'Reset form should be displayed'

			PasswordResetComponent.submitAndAssertError(['Passwort', 'Password'])

			PasswordResetComponent.newPassword = 'test'
			PasswordResetComponent.submitAndAssertError(['Passwörter', 'Passwords'])

			PasswordResetComponent.confirmNewPassword = 'foo'
			PasswordResetComponent.submitAndAssertError(['Passwörter', 'Passwords'])

			// Password in the DB is already "test" - expect an error when trying to set it to "test" again
			PasswordResetComponent.confirmNewPassword = 'test'
			PasswordResetComponent.submitAndAssertError(['unterscheidet', 'differ'])

			PasswordResetComponent.newPassword = 'foo'
			PasswordResetComponent.confirmNewPassword = 'foo'

			PasswordResetComponent.submit()
			assert PasswordResetComponent.successMessage

			getUrl(activationUrl)
			waitForAngularRequestsToFinish()

			// check that the previously correct resetCode does not work anymore
			assert PasswordResetComponent.errorMessage.contains("Dieser Aktivierungslink ist nicht mehr gültig.")
		}

		_55_loginWithNewPassword: {
			getUrlHash('/login')
			assert login('test', 'foo')
		}
	}

	@Test
	void testPasswordStrengthDescription() {
		Map<String, String> params = [
				'SECURITY_PASSWORD_STRENGTH_DESCRIPTION' : passwordStrengthDescription,
				'ROLE_FOR_SELF_REGISTERED_USERS': 'Example user'
		]
		getNuclosSession().patchSystemParameters(params)
		logout()

		AuthenticationComponent.clickSelfRegistration()
		def elPasswortStrengthDescription = $('#passwordStrengthDescription')
		assert elPasswortStrengthDescription != null
		assert elPasswortStrengthDescription.getAttribute('title') == passwordStrengthDescription

		getUrlHash('/login')
		login('nuclos', '')
		getNuclosSession().setSystemParameters(['ROLE_FOR_SELF_REGISTERED_USERS': 'Example user'])
		logout()

		AuthenticationComponent.clickSelfRegistration()
		elPasswortStrengthDescription = $('#passwordStrengthDescription')
		assert elPasswortStrengthDescription == null
	}

	/**
	 * See NUCLOS-8299
	 * Currently, it is not planned to support registering the same e-mail address with multiple accounts for the webclient.
	 * This test verifies that a sensible error message is displayed, when reusing an e-mail address that already has been registered,
	 * instead of the previous behavior, which displayed the internal stacktrace.
	 */
	@Test
	void testUniqueMailAccountRequiredForRegistration() {
		Map<String, String> params = [
				'USERNAME_DESCRIPTION' : usernameDescription,
				'USERNAME_REGEXP' : usernameRegexp
		]
		getNuclosSession().patchSystemParameters(params)
		logout()

		AuthenticationComponent.clickSelfRegistration()
		def elPasswortStrengthDescription = $('#usernameDescription')
		assert elPasswortStrengthDescription != null
		assert elPasswortStrengthDescription.getAttribute('title') == usernameDescription

		testFirstMailUseValid:
		{
			RegistrationComponent.togglePrivacyConsent()
			RegistrationComponent.username = 'test registration'
			RegistrationComponent.password = 'test'
			RegistrationComponent.confirmPassword = 'test'
			RegistrationComponent.firstname = 'Firstname'
			RegistrationComponent.lastname = 'Lastname'
			RegistrationComponent.email = registrationEmail
			RegistrationComponent.email2 = registrationEmail
			RegistrationComponent.submit()
			assert RegistrationComponent.successMessage
		}

		testSecondMailUseInvalid:
		{
			getUrlHash('/login')
			AuthenticationComponent.clickSelfRegistration()

			RegistrationComponent.togglePrivacyConsent()
			RegistrationComponent.username = 'test registration 2'
			RegistrationComponent.password = 'test'
			RegistrationComponent.confirmPassword = 'test'
			RegistrationComponent.firstname = 'Firstname'
			RegistrationComponent.lastname = 'Lastname'
			RegistrationComponent.email = registrationEmail
			RegistrationComponent.email2 = registrationEmail
			RegistrationComponent.submitAndAssertError(['E-Mail wird bereits verwendet.'])
		}
	}

	/**
	 *
	 */
	@Test
	void testAllowPasswordResetOnlyAfterActivation() {
		logout()

		Map<String, String> params = [
				'ANONYMOUS_USER_ACCESS_ENABLED' : 'N'
		]
		getNuclosSession().patchSystemParameters(params)

		refresh()

		getUrlHash('/login')

		AuthenticationComponent.clickSelfRegistration()

		RegistrationComponent.togglePrivacyConsent()
		RegistrationComponent.username = 'test-registration'
		RegistrationComponent.password = 'test'
		RegistrationComponent.confirmPassword = 'test'
		RegistrationComponent.firstname = 'Firstname'
		RegistrationComponent.lastname = 'Lastname'
		RegistrationComponent.email = registrationEmail
		RegistrationComponent.email2 = registrationEmail

		assert !Busy.busy

		RegistrationComponent.submit()
		assert RegistrationComponent.successMessage

		setupEmailSystemParameters()

		if (!logout()) {
			getUrlHash('/login')
		}

		AuthenticationComponent.forgotLoginDetailsLink.click()

		ForgotLoginDetailsComponent.type = ForgotLoginDetailsComponent.TYPE.FORGOT_PASSWORD

		ForgotLoginDetailsComponent.username = 'test-registration'
		ForgotLoginDetailsComponent.submit()
		assert ForgotLoginDetailsComponent.errorMessage.contains('Der Zugang wurde noch nicht aktiviert.')
	}

	@Test
	void testUsernameValidation() {
		Map<String, String> params = [
				'USERNAME_DESCRIPTION' : usernameDescription,
				'USERNAME_REGEXP' : usernameRegexp
		]
		getNuclosSession().patchSystemParameters(params)
		logout()

		AuthenticationComponent.clickSelfRegistration()
		def elPasswortStrengthDescription = $('#usernameDescription')
		assert elPasswortStrengthDescription != null
		assert elPasswortStrengthDescription.getAttribute('title') == usernameDescription

		validUser:
		{
			RegistrationComponent.togglePrivacyConsent()
			RegistrationComponent.username = 'test registration'
			RegistrationComponent.password = 'test'
			RegistrationComponent.confirmPassword = 'test'
			RegistrationComponent.firstname = 'Firstname'
			RegistrationComponent.lastname = 'Lastname'
			RegistrationComponent.email = registrationEmail
			RegistrationComponent.email2 = registrationEmail
			RegistrationComponent.submit()
			assert RegistrationComponent.successMessage
		}

		invalidUser:
		{
			getUrlHash('/login')
			AuthenticationComponent.clickSelfRegistration()

			RegistrationComponent.togglePrivacyConsent()
			RegistrationComponent.username = 'Test registration 2'
			RegistrationComponent.password = 'test'
			RegistrationComponent.confirmPassword = 'test'
			RegistrationComponent.firstname = 'Firstname'
			RegistrationComponent.lastname = 'Lastname'
			RegistrationComponent.email = 2 + registrationEmail
			RegistrationComponent.email2 = 2 + registrationEmail
			RegistrationComponent.submitAndAssertError([usernameDescription])
		}

		getUrlHash('/login')
		login('nuclos', '')
		getNuclosSession().setSystemParameters(['ROLE_FOR_SELF_REGISTERED_USERS': 'Example user'])
		logout()

		AuthenticationComponent.clickSelfRegistration()
		elPasswortStrengthDescription = $('#passwordStrengthDescription')
		assert elPasswortStrengthDescription == null
	}

	String getActivationMessage() {
		"""Hello {0} {1},
<br/><br/>
to activate your account <a href="$WebclientTestContext.instance.nuclosWebclientBaseURL#/account/activate/{2}/{3}">click here</a>.
<br/><br/>
		Bye
"""
	}
}
