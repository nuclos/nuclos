package org.nuclos.test.webclient

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.pageobjects.MenuComponent
import org.nuclos.test.webclient.pageobjects.PrintoutComponent
import org.nuclos.test.webclient.pageobjects.ReportsComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ReportTest extends AbstractRerunnableWebclientTest {

	@Test
	void testReportExecution() {
		MenuComponent.toggleUserMenu()
		MenuComponent.clickOpenExecuteReports()

		assert ReportsComponent.getReportCount() == 6

		ReportsComponent.findElementContainingText('div.ag-cell[col-id="name"]', 'Empty Report').click()
		assert getCurrentUrl().endsWith('/AJtWSbJ2RrB9OSkLSG5r')

		$('#execution button').click()
		waitForAngularRequestsToFinish()
		assert $('#nuclet_test_other_EmptyReportPO_Test_Empty_Report:empty').value == 'on': 'Parameter checkboxes should be \'on\' per default'
		assert $('#nuclet_test_other_EmptyReportPO_Test_Empty_Report\\:paramWithDefault').value == '3 three': 'Parameter Combobox should have been initialized with VLP default value'

		PrintoutComponent.execute()

		$('#execution button').click()
		PrintoutComponent.setParameterValue('nuclet_test_other_EmptyReportPO_Test_Empty_Report:empty', 'true')
		PrintoutComponent.execute()
		assertMessageModalAndConfirm('Fehler', 'Die Datenquelle hat keine Daten zurückgegeben.')
		PrintoutComponent.closeModal()

		ReportsComponent.findElementContainingText('div.ag-cell[col-id="name"]', 'test Excel').click()
		assert !$('#execution button').enabled: 'Button must be disabled, because you cannot execute "Excel collective" reports'

		ReportsComponent.findElementContainingText('div.ag-cell[col-id="name"]', 'Test Collective Report').click()
		$('#execution button').click()
		def links = PrintoutComponent.downloadLinks
		assert links.size() == 2
	}

	@Test
	void testSortOrder() {
		MenuComponent.toggleUserMenu()
		MenuComponent.clickOpenExecuteReports()

		String[] reportsSorted = new String[ReportsComponent.getReportCount()]
		ArrayList<String> reports = new ArrayList<>(ReportsComponent.getReportCount())

		$$('.ag-center-cols-container div.ag-row').forEach {
			def text = it.$('div.ag-cell[col-id="name"]').text
			reportsSorted[Integer.valueOf(it.getAttribute('row-index'))] = text
			reports.add(text)
		}

		assert reports.sort().join(", ") == reportsSorted.join(", ")

		ReportsComponent.findElementContainingText('.ag-header-cell-text','Name').click()

		reportsSorted = new String[ReportsComponent.getReportCount()]

		$$('.ag-center-cols-container div.ag-row').forEach {
			def text = it.$('div.ag-cell[col-id="name"]').text
			reportsSorted[Integer.valueOf(it.getAttribute('row-index'))] = text
		}

		assert reports.reverse().join(", ") == reportsSorted.join(", ")
	}
}
