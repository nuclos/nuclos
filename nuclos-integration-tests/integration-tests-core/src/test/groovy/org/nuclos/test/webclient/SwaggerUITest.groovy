package org.nuclos.test.webclient


import org.junit.After
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.pageobjects.MenuComponent
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SwaggerUITest extends AbstractRerunnableWebclientTest {

	private Collection<String> operationsExcludedFromDefaultContentType = [
			'Nuclos_Documents-boDocumentUpload',
			'Nuclos_Maintenance-dbimport',
			'Nuclos_Maintenance-nucletimport_1',
			'Nuclos_Maintenance-setMode'
	];

	@After
	void cleanupSystemparameter() {
		nuclosSession.setSystemParameters([:])
	}

@Test
void runTest() {
	_05_openSwaggerUI: {
		openSwaggerUI()

		// open Authentication panel
		$('#operations-tag-Nuclos_Authentication')?.click()

		// Check for the "login" operation
		waitFor {
			$('#operations-Nuclos_Authentication-login')
		}
	}

	/**
	 * We should be directly able to execute requests that require a valid login.
	 */
	_10_executeRequestWithAuthorization:
	{
		// The GET /rest/login operation
		NuclosWebElement operation = $('[id*="operations-Nuclos_Authentication-login"].opblock-get')

		operation.scrollIntoView()

		// Requires a login first (POST /rest/login) - check for authorization button
		assert operation.$('.authorization__btn.unlocked')

		// Expand the operation panel
		operation.click()

		// Click the "Try it out" button
		operation.$('.try-out__btn').click()
		// ...and the "Execute" button
		operation.$('.execute').click()

		NuclosWebElement response = operation.$('.response')
		assert response.$('.response-col_status').text.trim() == '200'
		response.$('.response-col_description').text.trim().with {
			assert it.contains('"sessionId":')
			assert it.contains('"username":')
		}
	}

	_15_disableSwagger: {
		nuclosSession.setSystemParameters(['SWAGGER_ACTIVE': 'false'])

		getUrlHash('/swagger-ui')
		refresh()

		waitFor {
			$('.swagger-ui')?.text?.contains('No API definition provided.')
		}
	}
}

	@Test
	void testDefaultContentTypeJson() {
		openSwaggerUI()

		$$('.opblock-tag').forEach {
			it.scrollIntoView()
			// open tag panel
			it?.click()

			$$('.opblock-post, .opblock-put').forEach {
				if (!operationsExcludedFromDefaultContentType.contains(it.getId().substring("operations-".length()))) {

					it.scrollIntoView()
					// open operation panel
					it.click()

					def type = it.$('.body-param-content-type')
					if (type) {
						assert type?.text == 'application/json' : "${it.getId()} should have 'Content-Type: application/json'"
					}

					// close operation panel
					it.click()
				}
			}

			// close tag panel
			it?.click()
		}
	}

	@Test
	void executeReport() {
		openSwaggerUI()

		// open Reports panel
		$('#operations-tag-Nuclos_Reports')?.click()

		// The POST /rest/reports/{reportId}/execute operation
		NuclosWebElement operation = $('[id*="operations-Nuclos_Reports-report_execution"].opblock-post')

		operation.scrollIntoView()

		// Expand the operation panel
		operation.click()

		// Click the "Try it out" button
		operation.$('.try-out__btn').click()

		// set reportId
		operation.$('input[placeholder="reportId"]').setValue('AJtWSbJ2RrB9OSkLSG5r')

		// make sure 'Content-Type' is set to "application/json"
		assert operation.$('.body-param-content-type').text == 'application/json'

		// this part does not work with our Selenium version
		/**
		// set proper request body
		def sBody =
				'{' +
						'"printoutId":"nuclet_test_other_EmptyReportPO",' +
						'"name":"Empty Report",' +
						'"outputFormats":[' +
						'{' +
						'"name":"Test Empty Report",' +
						'"outputFormatId":"nuclet_test_other_EmptyReportPO_Test_Empty_Report",' +
						'"parameters":[' +
						'{' +
						'"name":"empty",' +
						'"nullable":false,' +
						'"parameter":"empty",' +
						'"type":"Boolean",' +
						'"value":false' +
						'}' +
						'],' +
						'"selected":true' +
						'}' +
						']' +
						'}'

		def body = operation.$('div.body-param textarea')

		String js = "document.querySelector('div.body-param textarea').value = '" + body + "';";
		JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].setAttribute('value', '" + sBody +"')", body.element);


		// ...and the "Execute" button
		operation.$('.execute').click()

		NuclosWebElement response = operation.$('.response')
		assert response.$('.response-col_status').text.trim() == '200'
		response.$('.response-col_description').text.trim().with {
			assert it.contains('"printoutId":')
			assert it.contains('"fileName":')
		}**/
	}

	private static void openSwaggerUI() {
		MenuComponent.toggleDevUtilsMenu()
		MenuComponent.clickSwaggerUI()
	}
}
