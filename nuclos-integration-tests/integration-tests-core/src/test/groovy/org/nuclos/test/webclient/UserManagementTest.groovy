package org.nuclos.test.webclient;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject;
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar;

import groovy.transform.CompileStatic;

/**
 * Created by Sebastian Debring on 4/7/2020.
 */

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class UserManagementTest extends AbstractWebclientTest {

	@Test
	void _01_openUserManagement() {
		logout()
		login('nuclos', '')

		assert $('#Administration-menu > li > a')
		assert $('a[href="#/view/org_nuclos_system_User"]')

		$('#Administration-menu > li > a').click()
		$('a[href="#/view/org_nuclos_system_User"]').click()
	}

	@Test
	void _02_changeUserAttribute() {
		Sidebar.selectEntryByText('test')
		EntityObjectComponent user = EntityObjectComponent.forDetail()
		def attribute = user.getAttribute('loginWithEmailAllowed')
		user.setAttribute('loginWithEmailAllowed', !attribute)
		user.save()

		assert $('a[href="#/view/org_nuclos_system_User"]')
		assert user.getAttribute('loginWithEmailAllowed') == !attribute
	}

	@Test
	void _03_setPassword() {
		EntityObjectComponent user = EntityObjectComponent.forDetail()

		user = EntityObjectComponent.forDetail()
		user.setAttribute('setPassword', true)
		user.setAttribute('newPassword', '1234')
		user.setAttribute('newPasswordConfirm', '12345')
		user.save()
		assertMessageModalAndConfirm('Fehler', 'Die Passwörter stimmen nicht überein.')

		user.setAttribute('setPassword', true)
		user.setAttribute('newPassword', '1234')
		user.setAttribute('newPasswordConfirm', '1234')
		user.save()

		user.setAttribute('setPassword', true)
		user.setAttribute('newPassword', 'test')
		user.setAttribute('newPasswordConfirm', 'test')
		user.save()
	}

	@Test
	void _04_setSuperUser() {
		RESTHelper.createUser('usermanager', 'test', ['Example user', 'User management'], nuclosSession)
		logout()
		login('usermanager', 'test')

		getUrlHash('/view/org_nuclos_system_User')
		Sidebar.selectEntryByText('test')
		EntityObjectComponent testUser = EntityObjectComponent.forDetail()
		testUser.setAttribute('superuser', true)
		testUser.save()
		assertMessageModalAndConfirm('Fehler', 'Sie können keine Super-User verändern!')
	}

}
