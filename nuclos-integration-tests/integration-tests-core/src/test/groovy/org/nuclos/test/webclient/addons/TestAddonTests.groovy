package org.nuclos.test.webclient.addons

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.dashboard.Dashboard
import org.nuclos.test.webclient.pageobjects.dashboard.DashboardAddonConfig

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class TestAddonTests extends AbstractRerunnableWebclientTest {

	@Ignore // Disabled due to npm connection problems on test server
	@Test
	void testDashboardAddonWorks() {
		Dashboard.addNew()
		assert Dashboard.configMode
		Dashboard.name = 'TestAddon Test'

		// Add TestAddon
		Dashboard.addAddonItem('TestAddon')

		assert $('nuc-dashboard-addon') != null: 'TestAddon should be added to view'

		assert Dashboard.getItems()?.get(0)?.item?.getText()?.contains('Addon TestAddon works.'): 'TestAddon should work'

		Dashboard.getItems()?.get(0)?.configure()

		DashboardAddonConfig.get().changeProperty('property-boolean', 'myBoolean', 'true')
		waitForAngularRequestsToFinish()
		DashboardAddonConfig.get().changeProperty('property-number', 'myInteger', '1200')
		waitForAngularRequestsToFinish()
		DashboardAddonConfig.get().changeProperty('property-string', 'myString', 'Hello there, General Kenobi! :)')
		waitForAngularRequestsToFinish()
		DashboardAddonConfig.get().save()

		waitForAngularRequestsToFinish()

		Dashboard.closeConfig()

		assert Dashboard.getItems()?.get(0)?.item?.getText()?.contains('myBoolean property: true'): 'TestAddon should be configured (boolean)'
		assert Dashboard.getItems()?.get(0)?.item?.getText()?.contains('myInteger property: 1200'): 'TestAddon should be configured (integer)'
		assert Dashboard.getItems()?.get(0)?.item?.getText()?.contains('myString property: Hello there, General Kenobi! :)'): 'TestAddon should be configured (string)'
	}
}
