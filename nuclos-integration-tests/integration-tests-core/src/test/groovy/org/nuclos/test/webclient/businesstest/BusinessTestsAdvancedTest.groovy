package org.nuclos.test.webclient.businesstest

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.businesstest.BusinessTestsAdvanced

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class BusinessTestsAdvancedTest extends AbstractRerunnableWebclientTest {
@Test
void runTest() {
	_00_setup: {
		logout()
		login('nuclos')
	}

	_01_deleteTests: {
		BusinessTestsAdvanced.open()
		BusinessTestsAdvanced.deleteAllTests()
	}

	_02_generateAllTests: {
		BusinessTestsAdvanced.generateAllTests()

		String log = BusinessTestsAdvanced.log
		assert log.startsWith('Generating all business tests...')
		assert !log.contains('[ERROR]')
		assert log.endsWith('Generating all business tests: Done')
	}

	_03_executeAllTests: {
		BusinessTestsAdvanced.executeAllTests()

		String log = BusinessTestsAdvanced.log
		assert log.startsWith('Running all business tests...')
		assert !log.contains('[ERROR]')
		assert log.endsWith('Running all business tests: Done')

		def countExecutedTestRows = BusinessTestsAdvanced.assertExecutedTests(log)
		assert countExecutedTestRows > 0

		assert BusinessTestsAdvanced.testsTotal > 0
		assert BusinessTestsAdvanced.testsTotal == countExecutedTestRows
		assert ['OK', 'WARNING', 'ERROR'].contains(BusinessTestsAdvanced.state)

		assert BusinessTestsAdvanced.testsGreen + BusinessTestsAdvanced.testsYellow + BusinessTestsAdvanced.testsRed == BusinessTestsAdvanced.testsTotal
		assert BusinessTestsAdvanced.duration > 0
	}

	_04_deleteSingleTests: {
		long countBefore = BusinessTestsAdvanced.testsMatcher.size()

		BusinessTestsAdvanced.deleteTest(0)
		BusinessTestsAdvanced.deleteTest(-1)

		assert BusinessTestsAdvanced.testsMatcher.size() == countBefore - 2
	}

	_05_deleteAllTests: {
		BusinessTestsAdvanced.deleteAllTests()

		assert BusinessTestsAdvanced.testsMatcher.size() == 0
	}
}
}
