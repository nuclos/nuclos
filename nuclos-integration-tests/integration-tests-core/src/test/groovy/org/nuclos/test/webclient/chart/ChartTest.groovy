package org.nuclos.test.webclient.chart

import java.text.SimpleDateFormat

import org.apache.commons.io.IOUtils
import org.junit.After
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Modal
import org.nuclos.test.webclient.pageobjects.chart.ChartModal

import groovy.transform.CompileStatic

/**
 * testing charts by using subform data
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ChartTest extends AbstractRerunnableWebclientTest {

	@After
	@Override
	void afterEach() {
		// We currently have problems with the Chart component, probably because it is rendered beyond the page.
		// This raises errors in the browser.
		// Fix the settings to fit on page and not to force the user to scroll down.
		AbstractWebclientTest.teardown(false)
	}

@Test
void runTest() {
	Modal.forSelector("nuc-chart-modal")
	EntityObject company
	String chartOptionsString = IOUtils.toString(
			ChartTest.class.getResourceAsStream('chart-config.json')
	)
	String referenceAttributeId = TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES.fqn + '_company'
	String categoryAttributeId = TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES.fqn + '_umsatzjahr'
	String series1AttributeId = TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES.fqn + '_umsatz'
	String series2AttributeId = TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES.fqn + '_gewinn'

	List<String> chartTypes = [
			'multiBar',
			'multiBarHorizontal',
			'line',
			'area',
			'scatter',
			'pie',
			'linePlusBar'
	]

	_00_createTestData: {
		company = new EntityObject(TestEntities.NUCLET_TEST_CHARTS_COMPANY)
		company.setAttribute('name', 'Company 123')

		List<LinkedHashMap<String, Integer>> testData = [
				[umsatzjahr: 2006, umsatz: 10000, gewinn: 5000],
				[umsatzjahr: 2007, umsatz: 12000, gewinn: 7000],
				[umsatzjahr: 2008, umsatz: 15000, gewinn: 7000],
				[umsatzjahr: 2009, umsatz: 17000, gewinn: 9000],
				[umsatzjahr: 2010, umsatz: 18000, gewinn: 10000],
				[umsatzjahr: 2011, umsatz: 20000, gewinn: 11000],
				[umsatzjahr: 2012, umsatz: 21000, gewinn: 10000],
				[umsatzjahr: 2013, umsatz: 18000, gewinn: 16000],
				[umsatzjahr: 2014, umsatz: 22000, gewinn: 17000],
				[umsatzjahr: 2015, umsatz: 24000, gewinn: 15000],
		]

		List<EntityObject<Long>> dependents = company.getDependents(
				TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES,
				'company'
		)

		testData.each {
			EntityObject subEo = new EntityObject<>(TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES)
			subEo.setAttribute('umsatzjahr', it['umsatzjahr'])
			subEo.setAttribute('umsatz', it['umsatz'])
			subEo.setAttribute('gewinn', it['gewinn'])
			dependents << subEo
		}

		nuclosSession.save(company)
	}
	_05_openChartModal:
	{
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_CHARTS_COMPANY)

		eo.openCharts()
		waitForAngularRequestsToFinish()
		waitFor {
			return ChartModal.isVisible()
		}

		assert ChartModal.title == 'Charts' || ChartModal.title == 'Diagramme'
		assert ChartModal.body

		ChartModal.close()
		waitFor {
			return !ChartModal.visible
		}
	}
	_10_createChart:
	{
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_CHARTS_COMPANY)

		eo.openCharts()
		waitForAngularRequestsToFinish()
		waitFor {
			return ChartModal.isVisible()
		}

		assert ChartModal.chartCount == 1

		// ChartModal.newChart()

		assert !ChartModal.configurationValid
		assert ChartModal.chartEventRects == null
		assert ChartModal.referenceAttributeId == 'FinancialFigures'
		assert ChartModal.getSeriesOptionsSize() == 4 // Three options plus ''
		assert !ChartModal.exportAvailable
		assert ChartModal.canSave()
		assert ChartModal.chartCount == 1
		assert ChartModal.tabTitles == ['New'] || ChartModal.tabTitles == ['Neu']

	}
	_12_checkToggleConfigure:
	{
		assert ChartModal.categoryAttributeId == ''
		ChartModal.categoryAttributeId = categoryAttributeId
		assert ChartModal.categoryAttributeId == 'Umsatzjahr'
		assert ChartModal.seriesAttributeId == ''
		ChartModal.seriesAttributeId = series2AttributeId
		assert ChartModal.seriesAttributeId == 'Gewinn'
		assert ChartModal.configurationValid
		assert ChartModal.chartEventRects != null

		ChartModal.toggleConfigure()
		waitForAngularRequestsToFinish()

		assert ChartModal.configurationValid
		assert ChartModal.chartEventRects != null
		assert $$(".p-tabview-title") == []
		assert $('.chart-types') == null
		assert $('#categoryAttributeId') == null
		assert $('#seriesAttributeId') == null

		ChartModal.toggleConfigure()
		waitForAngularRequestsToFinish()

		assert ChartModal.chartName == 'New' || ChartModal.chartName == 'Neu'
		assert $('.chart-types') != null
		assert ChartModal.referenceAttributeId == 'FinancialFigures'
		assert ChartModal.categoryAttributeId == 'Umsatzjahr'
		assert ChartModal.seriesAttributeId == 'Gewinn'
		assert ChartModal.configurationValid
		assert ChartModal.chartEventRects != null

		assert ChartModal.canSave()
		ChartModal.close()
	}
	_15_openAndAddChart:
	{
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_CHARTS_COMPANY)

		eo.openCharts()
		waitForAngularRequestsToFinish()
		waitFor {
			return ChartModal.isVisible()
		}

		assert ChartModal.tabTitles == ['New'] || ChartModal.tabTitles == ['Neu']
		assert ChartModal.canSave()
		// ChartModal.newChart()
		assert ChartModal.selectedChartIndex == 0
		assert !ChartModal.exportAvailable
	}
	_20_saveChart: {
		ChartModal.newChart()

		countBrowserRequests {
			ChartModal.saveChart()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_CREATE) == 1
			assert it.getRequestCount(RequestType.PREFERENCE_UPDATE) == 0

			assert it.requestCount == 1
		}

		assert !ChartModal.canSave()

		ChartModal.setChartName('Test-Chart 2')
		assert ChartModal.categoryAttributeId == ''
		assert ChartModal.seriesAttributeId == ''
		ChartModal.seriesAttributeId = series1AttributeId
		assert ChartModal.seriesAttributeId == 'Umsatz'
		assert !ChartModal.configurationValid
		assert ChartModal.chartEventRects == null

		assert ChartModal.canSave()

		countBrowserRequests {
			ChartModal.saveChart()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_CREATE) == 0
			assert it.getRequestCount(RequestType.PREFERENCE_UPDATE) == 1

			assert it.requestCount == 1
		}
		ChartModal.close()
	}
	_22_checkSavedChart: {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_CHARTS_COMPANY)
		eo.openCharts()

		waitForAngularRequestsToFinish()
		waitFor {
			return ChartModal.isVisible()
		}

		assert ChartModal.tabTitles == ['Test-Chart 2']
		assert ChartModal.getSeriesOptionsSize() == 4 // Three options plus ''
		assert ChartModal.referenceAttributeId == 'FinancialFigures'
		assert ChartModal.categoryAttributeId == ''
		assert ChartModal.seriesAttributeId == 'Umsatz'
		assert !ChartModal.configurationValid
		assert ChartModal.chartEventRects == null
		assert !ChartModal.canSave()

		ChartModal.categoryAttributeId = categoryAttributeId
		assert ChartModal.categoryAttributeId == 'Umsatzjahr'
		waitForAngularRequestsToFinish()

		assert ChartModal.configurationValid
		assert ChartModal.chartEventRects != null
		assert ChartModal.canSave()

		assert ChartModal.chartCount == 1
		ChartModal.newChart()
		assert ChartModal.tabs.size() == 2
		ChartModal.tabs[0].click()
		waitForAngularRequestsToFinish()
	}
	_25_deleteChart: {
		assert ChartModal.chartCount == 2

		countBrowserRequests {
			ChartModal.deleteChart()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_DELETE) == 1
			assert it.requestCount == 1
		}

		assert ChartModal.chartCount == 1
	}
	_30_configureChart: {
		ChartModal.chartName = 'Test-Chart'

		ChartModal.saveChart()

		assert ChartModal.chartName == 'Test-Chart'

		ChartModal.waitForChartNotVisible()

		String optionString = chartOptionsString

		Log.debug optionString

		ChartModal.chartOptions = optionString

		assert ChartModal.configurationValid
		assert ChartModal.referenceAttributeId == 'FinancialFigures'
		ChartModal.waitForChartVisible()
		waitUntilTrue({ChartModal.chartDataCount == 20})
	}
	_35_openDataEntryInNewTab: {
		ChartModal.openReferenceOnClick = true
		ChartModal.clickDataEntry(3)
		switchToOtherWindow()

		waitUntilTrue({ -> EntityObjectComponent.forDetail().getAttribute('umsatzjahr') == '2009' }, 90)

		switchToOtherWindow()
		closeOtherWindows()
	}
	_40_chartTypes: {
		assert ChartModal.chartType == chartTypes[0]

		chartTypes.each {
			ChartModal.chartType = it
			assert ChartModal.chartType == it
			ChartModal.waitForChartVisible()
		}
	}
	_45_filterChartData: {
		waitUntilTrue({ChartModal.chartDataCount == 20})
		assert ChartModal.exportAvailable

		ChartModal.setFilter(0, '1970-1-1')
		ChartModal.applyFilter()
		waitUntilTrue({ChartModal.chartDataCount == 0})
		assert !ChartModal.exportAvailable

		String today = new SimpleDateFormat('yyyy-MM-dd').format(new Date())
		ChartModal.setFilter(0, today)
		ChartModal.applyFilter()
		waitUntilTrue({ChartModal.chartDataCount == 20})

		ChartModal.setFilter(1, '2008')
		ChartModal.applyFilter()
		waitUntilTrue({ChartModal.chartDataCount == 16})

		ChartModal.setFilter(2, '2012')
		ChartModal.applyFilter()
		waitUntilTrue({ChartModal.chartDataCount == 10})
	}
	_47_exportChartAsCsv: {
		ChartModal.exportChart()

		// TODO: Check content of the downloaded file
	}
	_50_createChartViaGuiOnly: {
		ChartModal.newChart()

		waitUntilTrue({ChartModal.chartSeriesCount == 1})

		ChartModal.chartName = 'Configured via GUI'
		ChartModal.referenceAttributeId = referenceAttributeId
		ChartModal.categoryAttributeId = categoryAttributeId
		// ChartModal.newSeries()
		assert ChartModal.chartSeriesCount == 1

		ChartModal.waitForChartNotVisible()
		ChartModal.seriesAttributeId = series1AttributeId

		ChartModal.waitForChartVisible()

		ChartModal.newSeries()
		waitUntilTrue({ChartModal.chartSeriesCount == 2})
		ChartModal.waitForChartVisible()
		ChartModal.seriesAttributeId = series2AttributeId
		ChartModal.waitForChartVisible()

		ChartModal.newSeries()
		waitUntilTrue({ChartModal.chartSeriesCount == 3})
		ChartModal.removeSeries()
		waitUntilTrue({ChartModal.chartSeriesCount == 2})

		ChartModal.close()
	}
}

}
