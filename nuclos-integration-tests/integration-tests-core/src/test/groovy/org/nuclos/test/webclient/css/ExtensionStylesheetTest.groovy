package org.nuclos.test.webclient.css

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ExtensionStylesheetTest extends AbstractRerunnableWebclientTest {

	private static final String COLOR_ALICE_BLUE = 'rgb(240, 248, 255)'
	private static final String COLOR_YELLOW = 'rgb(255, 255, 0)'

	private static final String NUCLET_A_CSS = '''
div[name="attribute-HTML_UEvS"] {
	background: yellow;
}
'''

	private static final String NUCLET_B_CSS = '''
input[name="attribute-name"][next-focus-field="attribute-category"] {
	background: aliceblue;
}
'''

	@Before
	void beforeEach() {
		AbstractWebclientTest.setup(true, false)
	}

	@Test
	void runTest() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)

		eo.addNew()

		waitForAngularRequestsToFinish()

		assert $$('head > style').find {
			it.getAttribute('innerHTML')?.trim() == NUCLET_A_CSS.trim()
		}

		assert $$('head > style').find {
			it.getAttribute('innerHTML')?.trim() == NUCLET_B_CSS.trim()
		}

		assertBackgroundEquals('input[name="attribute-name"][next-focus-field="attribute-category"]', COLOR_ALICE_BLUE)
		assertBackgroundEquals('div[name="attribute-HTML_UEvS"]', COLOR_YELLOW)

		eo.cancel()
	}

	private String assertBackgroundEquals(String selector, String color) {
		assert $(selector).getCssValue('background').contains(color)
	}

}
