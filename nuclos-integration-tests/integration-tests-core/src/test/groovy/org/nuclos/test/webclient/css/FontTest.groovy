package org.nuclos.test.webclient.css

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class FontTest extends AbstractRerunnableWebclientTest {

	// TODO: Adjust for new Webclient!
	final String WEBCLIENT_CSS = '''
#detailblock #sideview-statusbar-content:before {
	opacity: 0.3;
	content: 'statusbar with timestamp disabled for screenshot diffs';
}
'''

	final String FONT_FAMILY_DEFAULT = 'sans-serif'

	final String FONT_SIZE_DEFAULT = '11px'
	final String FONT_SIZE_SIDEBAR = '11px'
	final String FONT_SIZE_BUTTON = '11px'
	final String FONT_SIZE_MENU = '16px'
	final String FONT_SIZE_STATUSBAR = '12px'
	final String FONT_SIZE_SEARCH_INPUT = '12px'

	@Before
	void beforeEach() {
		AbstractWebclientTest.setup(true, false) {
			nuclosSession.setSystemParameters([WEBCLIENT_CSS: WEBCLIENT_CSS])
		}
	}

@Test
void runTest() {

	_00_checkCssViaSystemparameter: {
		assert $$('head > style').find {
			it.getAttribute('innerHTML')?.trim() == WEBCLIENT_CSS.trim()
		}
	}

	_01setupBos: {
        assertLoggedIn('test')

		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
	}

	_02detail: {

		$$('nuc-web-component input, nuc-web-component textarea, nuc-web-component span:not(.fa):not(.sr-only), nuc-web-component label').each {
			assert it.getCssValue('font-size') == FONT_SIZE_DEFAULT:
					"Element ${it.element.tagName} (${it.element.getAttribute('class')}) has not default font-size"
			assertFontFamily(it)
		}

		$$('nuc-web-component button').each {
			def cssClass = it.getAttribute("class")
			if (it.text.length() > 0) {
				if (cssClass.indexOf("p-autocomplete-dropdown") != -1) {
					// lov/combobox button
				} else {
					assert it.getCssValue('font-size') == FONT_SIZE_BUTTON:
							"Element ${it.element.tagName} (${it.element.getAttribute('class')}) has not default font-size"
					assertFontFamily(it)
				}
			}
		}
	}

	_03sidebar: {
		String orderNumberInDetailBlock = EntityObjectComponent.forDetail().getAttribute('orderNumber')
		def orderNumberInSidebar = Sidebar.findElementContainingText('.ag-cell', orderNumberInDetailBlock)
		assert orderNumberInSidebar != null
		assert orderNumberInSidebar.getCssValue('font-size') == FONT_SIZE_SIDEBAR:
				"Element ${orderNumberInSidebar.tagName} (${orderNumberInSidebar.getAttribute('class')}) has not default font-size"
		assertFontFamily(orderNumberInSidebar)
	}

	_04subform: {

		// subform headers and subform cells
		$$('nuc-web-subform .ag-cell, nuc-web-subform .ag-header-cell-text').each {
			it.findElements(By.cssSelector('*')).each {
				assert it.getCssValue('font-size') == FONT_SIZE_DEFAULT:
						"Element ${it.tagName} (${it.getAttribute('class')}) has not default font-size"
				assertFontFamily(it)
			}
		}
	}

	_05searchInput: {
		$$('.search-input input').each {
			assert it.getCssValue('font-size') == FONT_SIZE_DEFAULT || FONT_SIZE_SEARCH_INPUT:
					"Element ${it.element.tagName} (${it.element.getAttribute('class')}) has not default font-size"
			assertFontFamily(it)
		}
	}

	_06statusbar: {
		$$('nuc-statusbar sideview-statusbar-content *').each {
			assert it.getCssValue('font-size') == FONT_SIZE_STATUSBAR:
					"Element ${it.element.tagName} (${it.element.getAttribute('class')}) has not default font-size"
			assertFontFamily(it)
		}
	}

	_07menu: {
		$$('nuc-menu .nav-link').each {
			assert it.getCssValue('font-size') == FONT_SIZE_MENU
			assertFontFamily(it)
		}
	}
}

	private void assertFontFamily(WebElement element) {
		def fontFamily = element.getCssValue('font-family')
		if (fontFamily.length() > 0
				&& fontFamily != 'agGridBalham'
				&& element.getAttribute('class').indexOf('fa') == -1
				&& element.getAttribute('class').indexOf('sr-only') == -1
				&& element.getAttribute('class').indexOf('pi') == -1) {
			assert fontFamily.indexOf(FONT_FAMILY_DEFAULT) != -1:
					"Element ${element.tagName} (${fontFamily}) has wrong family"
		}
	}
}
