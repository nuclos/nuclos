package org.nuclos.test.webclient.dashboard

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.chart.ChartInDashboardModal
import org.nuclos.test.webclient.pageobjects.dashboard.Dashboard
import org.nuclos.test.webclient.pageobjects.dashboard.DashboardItem
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ChartInDashboardTest extends AbstractRerunnableWebclientTest {
@Test
void runTest() {
	String series1AttributeId = 'example_rest_OrderDynTasksDTL_Ordersum'
	String category2AttributeId = 'example_rest_DynamicArticleTaskListDTL_name'
	String series2AttributeId = 'example_rest_DynamicArticleTaskListDTL_number'

	_00_setup: {
		LocaleChooser.locale = Locale.ENGLISH
		TestDataHelper.insertTestData(nuclosSession)
	}

	_05_newDashboard: {
		assert !Dashboard.configMode
		assert Dashboard.getNewButton() != null
		Dashboard.addNew()
		Dashboard.name = 'WithCharts'
		assert Dashboard.tabTitles == ['WithCharts']

		assert Dashboard.configMode
		Dashboard.dynamicTaskListSelect.open()
		assert Dashboard.dynamicTaskListSelect.choices.contains('Order-Dyn-Tasks')
		Dashboard.addDynamicTaskListItem('Order-Dyn-Tasks')

		Dashboard.closeConfig()
		assert !Dashboard.configMode
		assert Dashboard.getToggleConfig() != null
		assert Dashboard.items.size() == 1
	}

	_10_addChart: {
		assert Dashboard.getToggleConfig()
		Dashboard.getToggleConfig().click()
		assert Dashboard.configMode

		Dashboard.chartListSelect.open()
		assert Dashboard.chartListSelect.choices.containsAll([
				'Order-Dyn-Tasks',
				'Without Menu Entry',
		])

		Dashboard.addChartListItem('Order-Dyn-Tasks')
		waitForAngularRequestsToFinish()

		assert Dashboard.chartTitles == ['Order-Dyn-Tasks']

		Dashboard.items.with {
			assert it.size() == 2
			it.get(1).with {
				assert it.type == 'chart'
				assert it.x == 1
				assert it.y == 0
				// rows/cols initially undefined
				assert it.rows == null
				assert it.cols == null
			}
		}
	}

	_12_checkUnconfiguredChart: {
		DashboardItem itemChart = Dashboard.items.get(1)

		assert itemChart.chartEventRect() == null
	}

	_14_configureChart: {
		DashboardItem itemChart = Dashboard.items.get(1)
		itemChart.configure()

		waitForAngularRequestsToFinish()
		waitFor {
			return ChartInDashboardModal.isVisible()
		}
		assert itemChart.chartEventRect() == null

		assert !ChartInDashboardModal.configurationValid2
		assert ChartInDashboardModal.chartTitle == 'Order-Dyn-Tasks'
		ChartInDashboardModal.chartName2 = 'Bar Order'
		assert ChartInDashboardModal.chartType2 == 'multiBar'
		assert ChartInDashboardModal.chartEventRects2 == null
		assert ChartInDashboardModal.referenceElement == null

		assert ChartInDashboardModal.categoryAttributeId2 == 'Ordernumber'

		assert ChartInDashboardModal.seriesAttributeId2 == ''
		assert ChartInDashboardModal.getSeriesOptionsSize2() == 3 // Two options plus ''
		ChartInDashboardModal.seriesAttributeId2 = series1AttributeId
		assert ChartInDashboardModal.seriesAttributeId2 == 'Ordersum'

		assert ChartInDashboardModal.chartEventRects2 != null

		// Working on a configuration clone, so titel not changing immediately
		// assert ChartInDashboardModal.chartTitle == 'Bar Order'
		assert ChartInDashboardModal.chartTitle == 'Order-Dyn-Tasks'

		assert itemChart.chartEventRect() == null
		assert ChartInDashboardModal.saveButton
		ChartInDashboardModal.saveButton.click()

		assert ChartInDashboardModal.chartEventRects2 == null
		assert !ChartInDashboardModal.isVisible()
		assert Dashboard.chartTitles == ['Bar Order']

		assert itemChart.chartEventRect() != null

		Dashboard.closeConfig()
	}

	_16_checkChartAfterClosingConfig: {
		assert Dashboard.items.size() == 2

		DashboardItem itemChart = Dashboard.items.get(1)
		assert itemChart.chartEventRect() != null
		assert itemChart.chartBars().isDisplayed()
		assert !itemChart.chartLines().isDisplayed()
	}

	_20_addSecondChart: {
		Dashboard.configure()
		assert Dashboard.configMode

		Dashboard.chartListSelect.open()
		assert Dashboard.chartListSelect.choices.contains('Order-Dyn-Tasks')
		assert Dashboard.chartListSelect.choices.contains('Active articles')

		Dashboard.addChartListItem('Active articles')
		waitForAngularRequestsToFinish()

		Dashboard.items.with {
			assert it.size() == 3
			it.get(2).with {
				assert it.type == 'chart'
				assert it.x == 0
				assert it.y == 1
			}
		}
	}

	_22_checkUnconfiguredChart: {
		DashboardItem itemChart = Dashboard.items.get(1)
		assert itemChart.chartEventRect() != null

		DashboardItem itemChart2 = Dashboard.items.get(2)
		assert itemChart2.chartEventRect() == null
	}

	_24_configureChart: {
		DashboardItem itemChart2 = Dashboard.items.get(2)
		itemChart2.configure()

		waitForAngularRequestsToFinish()
		waitFor {
			return ChartInDashboardModal.isVisible()
		}
		assert itemChart2.chartEventRect() == null

		assert !ChartInDashboardModal.configurationValid2
		assert ChartInDashboardModal.chartEventRects2 == null
		assert ChartInDashboardModal.referenceElement == null

		assert ChartInDashboardModal.categoryAttributeId2 == ''
		ChartInDashboardModal.categoryAttributeId2 = category2AttributeId
		assert ChartInDashboardModal.categoryAttributeId2 == 'name'

		assert ChartInDashboardModal.getSeriesOptionsSize2() == 2 // Two options plus ''

		// FIXME As there is only one option it should be selected automatically
		if (ChartInDashboardModal.seriesAttributeId2 == '') {
			ChartInDashboardModal.seriesAttributeId2 = series2AttributeId
		}
		assert ChartInDashboardModal.seriesAttributeId2 == 'number'

		assert ChartInDashboardModal.chartEventRects2 != null

		assert itemChart2.chartEventRect() == null
		assert ChartInDashboardModal.chartType2 == 'multiBar'
		assert ChartInDashboardModal.saveButton
		ChartInDashboardModal.saveButton.click()

		assert ChartInDashboardModal.chartEventRects2 == null
		assert !ChartInDashboardModal.isVisible()

		assert itemChart2.chartEventRect() != null
		assert Dashboard.items.get(1).chartEventRect() != null

	}

	_25_addThirdChart: {
		Dashboard.configure()
		assert Dashboard.configMode

		Dashboard.chartListSelect.open()
		assert Dashboard.chartListSelect.choices.contains('Order-Dyn-Tasks')
		assert Dashboard.chartListSelect.choices.contains('Active articles')

		Dashboard.addChartListItem('Order-Dyn-Tasks')
		waitForAngularRequestsToFinish()

		Dashboard.items.with {
			assert it.size() == 4
			it.get(3).with {
				assert it.type == 'chart'
				assert it.x == 1
				assert it.y == 1
			}
		}
	}

	_26_configureChart: {
		DashboardItem itemChart = Dashboard.items.get(3)
		itemChart.configure()

		waitForAngularRequestsToFinish()
		waitFor {
			return ChartInDashboardModal.isVisible()
		}
		assert itemChart.chartEventRect() == null

		assert !ChartInDashboardModal.configurationValid2
		assert ChartInDashboardModal.chartType2 == 'multiBar'
		assert ChartInDashboardModal.chartEventRects2 == null
		assert ChartInDashboardModal.referenceElement == null

		assert ChartInDashboardModal.categoryAttributeId2 == 'Ordernumber'

		assert ChartInDashboardModal.seriesAttributeId2 == ''
		assert ChartInDashboardModal.getSeriesOptionsSize2() == 3 // Two options plus ''
		ChartInDashboardModal.seriesAttributeId2 = series1AttributeId
		assert ChartInDashboardModal.seriesAttributeId2 == 'Ordersum'

		assert ChartInDashboardModal.chartEventRects2 != null

		assert itemChart.chartEventRect() == null
		assert ChartInDashboardModal.saveButton
		ChartInDashboardModal.saveButton.click()

		assert ChartInDashboardModal.chartEventRects2 == null
		assert !ChartInDashboardModal.isVisible()

		assert itemChart.chartEventRect() != null
		assert itemChart.chartBars().isDisplayed()
		assert !itemChart.chartLines().isDisplayed()
		assert Dashboard.chartTitles == ['Bar Order', 'Active articles', 'Order-Dyn-Tasks']
	}

	_27_checkCancelingConfiguration: {
		DashboardItem itemChart = Dashboard.items.get(3)

		// Cancel Button
		itemChart.configure()
		waitForAngularRequestsToFinish()
		waitFor {
			return ChartInDashboardModal.isVisible()
		}
		assert itemChart.chartEventRect() == null
		ChartInDashboardModal.chartName2 = 'Line Order'
		assert ChartInDashboardModal.chartType2 == 'multiBar'
		ChartInDashboardModal.chartType2 = 'line'
		assert ChartInDashboardModal.chartType2 == 'line'

		assert ChartInDashboardModal.cancelButton
		ChartInDashboardModal.cancelButton.click()

		assert ChartInDashboardModal.chartEventRects2 == null
		assert !ChartInDashboardModal.isVisible()

		assert itemChart.chartEventRect() != null
		assert itemChart.chartBars().isDisplayed()
		assert !itemChart.chartLines().isDisplayed()
		assert Dashboard.chartTitles == ['Bar Order', 'Active articles', 'Order-Dyn-Tasks']

		// Save Button
		itemChart.configure()
		waitForAngularRequestsToFinish()
		waitFor {
			return ChartInDashboardModal.isVisible()
		}
		assert itemChart.chartEventRect() == null
		ChartInDashboardModal.chartName2 = 'Line Order'
		assert ChartInDashboardModal.chartType2 == 'multiBar'
		ChartInDashboardModal.chartType2 = 'line'
		assert ChartInDashboardModal.chartType2 == 'line'

		assert ChartInDashboardModal.saveButton
		ChartInDashboardModal.saveButton.click()

		assert ChartInDashboardModal.chartEventRects2 == null
		assert !ChartInDashboardModal.isVisible()

		assert itemChart.chartEventRect() != null
		assert !itemChart.chartBars().isDisplayed()
		assert itemChart.chartLines().isDisplayed()
		assert Dashboard.chartTitles == ['Bar Order', 'Active articles', 'Line Order']

		// Escape Keys
		itemChart.configure()
		waitForAngularRequestsToFinish()
		waitFor {
			return ChartInDashboardModal.isVisible()
		}
		assert itemChart.chartEventRect() == null
		ChartInDashboardModal.chartName2 = 'Again MultiBar'
		assert ChartInDashboardModal.chartType2 == 'line'
		ChartInDashboardModal.chartType2 = 'multiBar'
		assert ChartInDashboardModal.chartType2 == 'multiBar'

		sendKeys(Keys.ESCAPE)

		assert ChartInDashboardModal.chartEventRects2 == null
		assert !ChartInDashboardModal.isVisible()

		assert itemChart.chartEventRect() != null
		assert !itemChart.chartBars().isDisplayed()
		assert itemChart.chartLines().isDisplayed()
		assert Dashboard.chartTitles == ['Bar Order', 'Active articles', 'Line Order']

		Dashboard.closeConfig()
	}

	_28_checkChartAfterClosingConfig: {

		assert Dashboard.items.size() == 4

		DashboardItem itemChart2 = Dashboard.items.get(2)
		assert itemChart2.chartEventRect() != null
		assert itemChart2.chartBars().isDisplayed()
		assert !itemChart2.chartLines().isDisplayed()

		DashboardItem itemChart3 = Dashboard.items.get(3)
		assert itemChart3.chartEventRect() != null
		assert !itemChart3.chartBars().isDisplayed()
		assert itemChart3.chartLines().isDisplayed()
		assert !itemChart3.chartAreas().isDisplayed()

		assert Dashboard.chartTitles == ['Bar Order', 'Active articles', 'Line Order']
	}

	_30_deleteChart: {
		assert Dashboard.items.size() == 4
		Dashboard.configure()

		DashboardItem itemChart1 = Dashboard.items.get(1)
		assert itemChart1.chartEventRect() != null

		DashboardItem itemChart2 = Dashboard.items.get(2)
		assert itemChart2.chartEventRect() != null

		itemChart1.delete()
		assert itemChart1.chartEventRect() == null
		assert itemChart2.chartEventRect() != null
		assert Dashboard.items.size() == 3

		assert itemChart2.x == Dashboard.items.get(1).x && itemChart2.y == Dashboard.items.get(1).y
		Dashboard.closeConfig()
	}

	_32_addForthChart: {
		Dashboard.configure()
		assert Dashboard.configMode

		Dashboard.searchfilterChartListSelect.open()
		assert Dashboard.searchfilterChartListSelect.choices.contains('[Order] Order position')
		assert Dashboard.searchfilterChartListSelect.choices.contains('[Test Layout Rules] User-LOV2-Test')

		Dashboard.addSearchfilterChartListItem('[Order] Order position')
		waitForAngularRequestsToFinish()

		Dashboard.items.with {
			assert it.size() == 4
			it.get(3).with {
				assert it.type == 'chart'
				assert it.x == 1
				assert it.y == 0
			}
		}
	}

	_34_configureChart: {
		DashboardItem itemChart = Dashboard.items.get(3)
		itemChart.configure()

		waitForAngularRequestsToFinish()
		waitFor {
			return ChartInDashboardModal.isVisible()
		}
		assert itemChart.chartEventRect() == null

		assert !ChartInDashboardModal.configurationValid2
		ChartInDashboardModal.chartName2 = 'Order by Date'
		ChartInDashboardModal.chartType2 = 'area'
		ChartInDashboardModal.categoryAttributeId2 = 'example_rest_Order_orderDate'
		assert ChartInDashboardModal.categoryAttributeId2 == 'Order date'

		ChartInDashboardModal.seriesAttributeId2 = 'example_rest_Order_orderNumber'
		assert ChartInDashboardModal.seriesAttributeId2 == 'Order number'

		assert ChartInDashboardModal.configurationValid2
		assert ChartInDashboardModal.chartEventRects2 != null

		assert itemChart.chartEventRect() == null
		assert ChartInDashboardModal.saveButton
		ChartInDashboardModal.saveButton.click()

		assert ChartInDashboardModal.chartEventRects2 == null
		assert !ChartInDashboardModal.isVisible()

		assert itemChart.chartEventRect() != null
		assert !itemChart.chartBars().isDisplayed()
		assert itemChart.chartLines().isDisplayed()
		assert itemChart.chartAreas().isDisplayed()
		assert Dashboard.chartTitles == ['Active articles', 'Line Order', 'Order by Date']

		Dashboard.closeConfig()
	}

	_38_reloadDashboard: {
		Dashboard.open()
		assert Dashboard.tabTitles == ['WithCharts']
		assert Dashboard.items.size() == 4
		assert Dashboard.chartTitles == ['Active articles', 'Line Order', 'Order by Date']
		assert Dashboard.getToggleConfig() != null

		logout()
	}

	_40_checkImplicitPermissionForCharts: {
		RESTHelper.createUser('control', 'control', ['Example controlling', 'Example readonly', 'recordGrantSFTest'], nuclosSession)
		login('control', 'control')

		assert Dashboard.getNewButton() != null
		Dashboard.addNew()
		Dashboard.name = 'NoChartsConfig'
		assert Dashboard.tabTitles == ['NoChartsConfig']

		Dashboard.chartListSelect.open()
		assert Dashboard.chartListSelect.choices.contains('Order-Dyn-Tasks')

		Dashboard.addChartListItem('Order-Dyn-Tasks')
		waitForAngularRequestsToFinish()

		assert Dashboard.items.size() == 1

		DashboardItem itemChart = Dashboard.items.get(0)
		assert itemChart.item.$('button[name="show-config"]') != null

		Dashboard.closeConfig()

		logout()
	}
}

}
