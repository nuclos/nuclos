package org.nuclos.test.webclient.dashboard

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.dashboard.Dashboard
import org.nuclos.test.webclient.pageobjects.dashboard.DashboardItem
import org.nuclos.test.webclient.pageobjects.dashboard.DashboardTasklistConfig
import org.nuclos.test.webclient.pageobjects.preference.PreferenceType
import org.nuclos.test.webclient.pageobjects.preference.Preferences
import org.nuclos.test.webclient.pageobjects.search.Searchbar

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DashboardTest extends AbstractRerunnableWebclientTest {
@Test
void runTest() {
	_00_setup: {
		LocaleChooser.locale = Locale.ENGLISH
		TestDataHelper.insertTestData(nuclosSession)
	}

	_05_newDashboard: {
		assert !Dashboard.configMode

		assert Dashboard.getToggleConfig() == null
		Dashboard.addNew()

		assert Dashboard.configMode
		assert Dashboard.getToggleConfig() != null
		assert Dashboard.getDeleteButton() != null

		assert Dashboard.name == 'New dashboard'
		Dashboard.name = 'Dashboard 1'
		assert Dashboard.name == 'Dashboard 1'
		assert Dashboard.tabTitles == ['Dashboard 1']

		assert Dashboard.items.empty
	}

	_10_addDynamicTasklist: {
		Dashboard.dynamicTaskListSelect.open()
		assert Dashboard.dynamicTaskListSelect.choices.containsAll([
				'Order-Dyn-Tasks',
				'Without Menu Entry',
		])

		Dashboard.addDynamicTaskListItem('Order-Dyn-Tasks')

		Dashboard.items.with {
			assert it.size() == 1
			it.first().with {
				assert it.type == 'tasklist'
				assert it.x == 0
				assert it.y == 0
				// rows/cols initially undefined
				assert it.rows == null
				assert it.cols == null
			}
		}

		// TODO: Check column sorting (should already have been loaded from sidebar preferences and applied)

		Dashboard.closeConfig()
		assert Dashboard.getToggleConfig() != null
	}

	_11_addSearchfilterTasklist: {
		Dashboard.configure()
		Dashboard.searchfilterTaskListSelect.open()
		assert Dashboard.searchfilterTaskListSelect.choices.containsAll([])

		//TODO
//		Dashboard.addDynamicTaskListItem('Order-Dyn-Tasks')
//
//		Dashboard.items.with {
//			assert it.size() == 1
//			it.first().with {
//				assert it.type == 'tasklist'
//				assert it.x == 0
//				assert it.y == 0
//				// rows/cols initially undefined
//				assert it.rows == null
//				assert it.cols == null
//			}
//		}

		// TODO: Check column sorting (should already have been loaded from sidebar preferences and applied)

		Dashboard.closeConfig()
	}

	_12_addRuleBasedTasklist: {
		Dashboard.configure()
		Dashboard.dynamicTaskListSelect.open()
		Dashboard.addDynamicTaskListItem('Active articles')

		Dashboard.closeConfig()

		def activeArticleTaskList = Dashboard.items[1]
		assert activeArticleTaskList.countRows() == 4

		activeArticleTaskList.clickRow(2)
		assert Sidebar.listEntryCount == 4

		Dashboard.open()
	}

	_13_testTaskList: {
		def orderDynTaskList = Dashboard.items.first()
		assert orderDynTaskList.countRows() == 4

		// click task list to open sidebar (here configured to jump into Order BO)
		orderDynTaskList.clickRow(2)
		waitUntilTrue({ Searchbar.getActiveTaskList() == 'Order-Dyn-Tasks' })
		assert Sidebar.listEntryCount == 4

		// remove task list filter from bo view
		Searchbar.activeTaskListRemoveButton.click()
		waitUntilTrue({ Sidebar.listEntryCount == 6 })
	}

	_14_openDetailsInNewTab: {
		setupSystemParameters()
		refresh()

		Dashboard.open()

		//DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB not set -> prefill property with false
		checkPropertyShowDetailsNewTab(false)

		//DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB == false -> prefill property with false
		nuclosSession.setSystemParameters([
		        'DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB':'false'
		])
		refresh()
		checkPropertyShowDetailsNewTab(false)

		//DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB == true -> prefill property with true
		nuclosSession.setSystemParameters([
				'DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB':'true'
		])
		refresh()
		checkPropertyShowDetailsNewTab(true)

		//DashboardTasklistConfigComponent
		setupSystemParameters()
		refresh()
		setupDashboardWithDynamicTasklist()
		DashboardItem item = Dashboard.items.get(0)
		item.configure()
		DashboardTasklistConfig config = DashboardTasklistConfig.get()

		config.toggleShowDetailsNewTab()
		assert config.showDetailsNewTab

		config.cancel()
		assert !config.visible

		item.configure()
		config = DashboardTasklistConfig.get()
		assert !config.showDetailsNewTab

		config.toggleShowDetailsNewTab()
		config.save()
		assert !config.visible

		item.configure()
		config = DashboardTasklistConfig.get()
		assert config.showDetailsNewTab

		config.cancel()
		item.configure()
		config = DashboardTasklistConfig.get()
		assert config.showDetailsNewTab

		config.cancel()

		//showDetailsNewTab == true -> open details in new tab
		Dashboard.closeConfig()
		item = Dashboard.items.get(0)
		item.clickRow(0)
		switchToOtherWindow(false)
		waitUntilTrue({ Searchbar.getActiveTaskList() == 'Order-Dyn-Tasks' })
		assert Sidebar.listEntryCount == 4

		//showDetailsNewTab == false -> open details in same tab
		switchToOtherWindow(false)
		closeOtherWindows()
		Dashboard.configure()
		item = Dashboard.items.get(0)
		item.configure()
		config = DashboardTasklistConfig.get()
		config.toggleShowDetailsNewTab()
		config.save()

		Dashboard.closeConfig()
		item = Dashboard.items.get(0)
		item.clickRow(0)
		waitUntilTrue({ Searchbar.getActiveTaskList() == 'Order-Dyn-Tasks' })
		assert Sidebar.listEntryCount == 4
	}

	_15_reloadDashboard: {
		Dashboard.open()
		assert Dashboard.tabTitles == ['Dashboard 1']
		assert Dashboard.items.size() == 1
		assert Dashboard.getToggleConfig() != null
		logout()
	}

	_20_shareDashboard: {
		login('test', 'test')
		assert Dashboard.tabTitles == ['Dashboard 1']
		Dashboard.configure()
		assert Dashboard.getDeleteButton() != null
		Dashboard.closeConfig()

		Preferences.open()
		Preferences.shareItem(PreferenceType.DASHBOARD, 'Dashboard 1', 'Example user')

		// In order to test "show At Start"
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)
		logout()
	}

	_22_checkSharedDashboard: {
		login('nuclos', '')
		assert Dashboard.getNewButton() != null
		assert Dashboard.getToggleConfig() != null

		assert Dashboard.tabTitles == ['Dashboard 1']
		Dashboard.configure()

		assert Dashboard.getDeleteButton() == null
	}

	_25_customizeDashboard: {
		Dashboard.name = 'Dashboard 2'
		assert Dashboard.tabTitles == ['Dashboard 2']

		assert !Dashboard.startupDefault.isSelected()
		Dashboard.startupDefault.click()
		assert Dashboard.startupDefault.isSelected()

		Dashboard.closeConfig()
		assert Dashboard.tabTitles == ['Dashboard 2']
		logout()
	}

	_30_checkUnpublishedCustomization: {
		login('test', 'test')
		assert !Dashboard.isShowing()

		Dashboard.open()
		assert Dashboard.tabTitles == ['Dashboard 1']

		assert Dashboard.getNewButton() != null
		assert Dashboard.getToggleConfig() != null

		Dashboard.configure()
		assert Dashboard.getDeleteButton() == null
		assert !Dashboard.startupDefault.isSelected()
		Dashboard.closeConfig()

		// In order to test "show At Start"
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)

		logout()
	}

	_35_publishCustomization: {
		login('nuclos', '')
		assert Dashboard.tabTitles == ['Dashboard 2']

		Preferences.open()
		Preferences.filter('name', 'Dashboard 2')
		def pref = Preferences.preferenceItems.first()
		pref.select()
		pref = Preferences.preferenceItems.first()
		assert pref.customized
		pref.publishChanges()

		logout()
	}

	_37_checkPublishedCustomization: {
		login('test', 'test')
		assert Dashboard.isShowing()
		assert Dashboard.getToggleConfig() != null
		assert Dashboard.tabTitles == ['Dashboard 2']

		Dashboard.configure()
		assert Dashboard.getDeleteButton() == null
		assert Dashboard.startupDefault.isSelected()
		Dashboard.startupDefault.click()

		Dashboard.closeConfig()
		logout()
	}

	_40_checkReadonly:
	{
		login('nuclos', '')

		Dashboard.configure()
		Dashboard.startupDefault.click()
		Dashboard.closeConfig()

		Dashboard.addNew()
		Dashboard.name = 'DashView'
		Dashboard.closeConfig()

		Preferences.open()
		Preferences.shareItem(PreferenceType.DASHBOARD, 'DashView', 'Example readonly')
		logout()

		RESTHelper.createUser('readonly', 'readonly', ['Example readonly'], nuclosSession)
		login('readonly', 'readonly')

		assert !Dashboard.configMode
		assert Dashboard.getNewButton() == null
		assert Dashboard.getToggleConfig() == null

		assert Dashboard.tabTitles == ['DashView']

		logout()
	}

	_50_addSecondDashboard: {
		login('nuclos', '')
		Dashboard.open()
		Dashboard.addNew()
		Dashboard.name = 'StartView'
		Dashboard.startupDefault.click()
		assert Dashboard.startupDefault.isSelected()
		Dashboard.closeConfig()

		assert Dashboard.tabTitles == ['Dashboard 2', 'DashView', 'StartView']

		Preferences.open()
		Preferences.shareItem(PreferenceType.DASHBOARD, 'StartView', 'Example readonly')
		logout()
	}

	_52_checkStartView: {
		login('readonly', 'readonly')
		assert Dashboard.tabTitles == ['DashView', 'StartView']
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)
		logout()

		login('readonly', 'readonly')
		assert Dashboard.isShowing()
		def startIndex = Dashboard.tabTitles.indexOf('StartView')
		def atabs = $$('.p-tabview-nav-link')

		assert atabs && atabs.size() == 2
		assert atabs[1-startIndex].getAttribute('aria-selected') == 'false'
		assert atabs[startIndex].getAttribute('aria-selected') == 'true'
		logout()
	}

	_53_resetSearchTermOnDashboardOpen_NUCLOS_10466: {
		login('nuclos', '')
		assert Dashboard.tabTitles == ['Dashboard 2', 'DashView', 'StartView']


		// In order to test that the searchfilter gets cleared from the SearchService, when switching to the dashboard
		Dashboard.open()
		Dashboard.configure()
		Dashboard.searchfilterTaskListSelect.open()
		Dashboard.addSearchfilterTaskListItem('[Order] Customer')

		Dashboard.closeConfig()

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		// set a search filter and check that it does not affect the results on the dashboard
		Searchbar.search("NO_RESULTS_!")

		Dashboard.open()
		waitForAngularRequestsToFinish()

		assert Dashboard.getItems()
				.get(0)
				.row(0)
				.$('nuc-state-icon-renderer img'): 'State icon should be present in dashboard lists'
		assert Dashboard.tabTitles == ['Dashboard 2', 'DashView', 'StartView']

		def activeArticleTaskList = Dashboard.items[0]
		// if the search filter didn't get removed, we will get no results
		assert activeArticleTaskList.countRows() != 0
		assert activeArticleTaskList.countRows() == 6

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		assert Searchbar.searchfilter.getAttribute('value') == 'NO_RESULTS_!'
		logout()
	}

	_55_deleteDashboard: {
		// TODO: Delete the newly created dashboard
		// TODO: Check that the shared dashboard is not deletable
	}

}

	@Test
	void testItemRemoval() {
		Dashboard.addNew()
		setupDashboardWithDynamicTasklist()

		DashboardItem item = Dashboard.items.get(0)
		item.deleteWithSlightMovement()

		Dashboard.closeConfig()
		refresh()

		assert Dashboard.items.empty
	}

	private checkPropertyShowDetailsNewTab(boolean defaultShowDetailsNewTab) {
		setupDashboardWithDynamicTasklist()
		DashboardItem item = Dashboard.items.get(0)
		item.configure()
		DashboardTasklistConfig config = DashboardTasklistConfig.get()
		assert config.visible
		assert config.showDetailsNewTab == defaultShowDetailsNewTab
	}

	private setupDashboardWithDynamicTasklist() {
		Dashboard.configure()
		Dashboard.items.each{
			it.delete()
		}
		Dashboard.dynamicTaskListSelect.open()
		Dashboard.addDynamicTaskListItem('Order-Dyn-Tasks')
	}
}
