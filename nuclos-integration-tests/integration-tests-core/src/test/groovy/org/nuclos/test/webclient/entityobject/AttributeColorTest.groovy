package org.nuclos.test.webclient.entityobject

import org.apache.commons.lang.StringUtils
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class AttributeColorTest extends AbstractRerunnableWebclientTest {
	private Long idCategory1
	private Long[] idArticles = new Long[2]

	@Before
	void setup() {
		EntityObject eoCategory1 = new EntityObject(TestEntities.EXAMPLE_REST_CATEGORY)
		eoCategory1.setAttribute("name", '1')
		idCategory1 = nuclosSession.save(eoCategory1) as Long

		2.times {
			EntityObject eoArticle = new EntityObject(TestEntities.EXAMPLE_REST_ARTICLE)
			eoArticle.setAttribute("articleNumber", it)
			eoArticle.setAttribute("name", "Article $it")
			eoArticle.setAttribute("category", [id: idCategory1])
			eoArticle.setAttribute("price", it == 0 ? 123.0 : 5.0)
			def idArticle = nuclosSession.save(eoArticle)
			idArticles[it] = idArticle as Long
		}

		EntityObject eoOrder = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		eoOrder.setAttribute('orderNumber', 1)
		eoOrder.setAttribute('note', '123456')
		nuclosSession.save(eoOrder)
	}

	@Test
	void testAttributeColor() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE, idArticles[0])
		assert StringUtils.lowerCase(eo.getAttributeElement('description').getCssValue('background-color')) == 'rgba(255, 255, 0, 1)'
		assert StringUtils.lowerCase(eo.getAttributeElement('price').getCssValue('background-color')) == 'rgba(0, 255, 0, 1)'

		eo.setAttribute('description', 'test')
		eo.setAttribute('price', 99)
		eo.save()
		assert StringUtils.lowerCase(eo.getAttributeElement('description').getCssValue('background-color')) == 'rgba(0, 255, 255, 1)'
		assert StringUtils.lowerCase(eo.getAttributeElement('price').getCssValue('background-color')) == 'rgba(255, 0, 0, 1)'

		eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE, idArticles[1])
		assert StringUtils.lowerCase(eo.getAttributeElement('description').getCssValue('background-color')) == 'rgba(255, 255, 0, 1)'
		assert StringUtils.lowerCase(eo.getAttributeElement('price').getCssValue('background-color')) == 'rgba(255, 0, 0, 1)'

		eo.setAttribute('description', 'tes')
		eo.save()
		assert eo.getAttributeElement('description').getCssValue('background-color') == 'rgba(255, 0, 0, 1)'

		Sidebar.getEntryElements().forEach {
			assert it.getCssValue('background-color') == 'rgba(171, 205, 239, 1)'
		}

		Sidebar.getEntryElements().forEach {
			def fqnPrice = TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_price'
			assert it.$("div[col-id='$fqnPrice']").getCssValue('background-color') == 'rgba(255, 0, 0, 1)'
		}
	}

	@Test
	void testAttributeColorSubform() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY, idCategory1)

		def sfArticle = eo.getSubform(TestEntities.EXAMPLE_REST_ARTICLE, 'category')
		assert sfArticle.getRowCount() == 2

		2.times {
			assert sfArticle.getRow(it).getBodyRow().getCssValue('background-color') == 'rgba(171, 205, 239, 1)'
			assert sfArticle.getRow(it).getFieldElement('price').getCssValue('background-color') == (it == 1 ? 'rgba(0, 255, 0, 1)' : 'rgba(255, 0, 0, 1)')

			sfArticle.getRow(it).enterValue('price', '123')
		}

		eo.save()

		2.times {
			assert sfArticle.getRow(it).getBodyRow().getCssValue('background-color') == 'rgba(171, 205, 239, 1)'
			assert sfArticle.getRow(it).getFieldElement('price').getCssValue('background-color') == 'rgba(0, 255, 0, 1)'

			sfArticle.getRow(it).enterValue('price', '123')
		}
	}

	@Test
	void testAttributeColorCalculatedField() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		assert eo.getAttributeElement('calculated').getCssValue('background-color') == 'rgba(0, 255, 0, 1)'

		eo.setAttribute('note', '12345')
		eo.save()
		assert eo.getAttributeElement('calculated').getCssValue('background-color') == 'rgba(255, 0, 0, 1)'
	}
}
