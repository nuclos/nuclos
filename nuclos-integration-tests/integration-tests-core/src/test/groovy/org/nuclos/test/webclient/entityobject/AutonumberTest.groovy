package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class AutonumberTest extends AbstractRerunnableWebclientTest {

	@Test
	void runTest() {
		doIt()
	}

	@Test
	void runTestReadOnly() {
		doIt(true)
	}

	@Test
	void testAutonumberInsertLocationParameter() {
		/* First Case: Test Insert On Top - This is the default behaviour. */
		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_SubformInsertBehaviour': 'nuclos_LAF_Subform_Insert_top'])
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBER)
		eo.addNew()
		eo.setAttribute('name', 'Autonumber insert test')

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORM, 'parent')
		subform.clickColumnHeader('autonumber')
		assert subform

		/* This row will be saved, to make sure the sorting works for both saved and unsaved entries. */
		SubformRow saveRow = subform.newRow()
		saveRow.enterValue('text', "test")

		eo.save()
		subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORM, 'parent')

		4.times {
			SubformRow row = subform.newRow()
			row.enterValue('text', "${97 + it as char}")
		}

		assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]

		subform.selectAllRows()
		subform.cloneSelectedRows()

		assert subform.getColumnValues('autonumber', Integer.class) == [6, 7, 8, 9, 10, 5, 4, 3, 2, 1]
	}

	@Test
	void testAutonumberInsertLocationParameterBottom() {
		/* Second Case: Test insert on bottom. */
		nuclosSession.invalidateServerCaches();
		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_SubformInsertBehaviour': 'nuclos_LAF_Subform_Insert_bottom'])
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBER)
		eo.addNew()
		eo.setAttribute('name', 'Autonumber insert test')

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORM, 'parent')
		subform.clickColumnHeader('autonumber')
		assert subform

		/* This row will be saved, to make sure the sorting works for both saved and unsaved entries. */
		SubformRow saveRow = subform.newRow()
		saveRow.enterValue('text', "test")

		eo.save()
		subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORM, 'parent')

		4.times {
			SubformRow row = subform.newRow()
			row.enterValue('text', "${97 + it as char}")
		}

		assert subform.getColumnValues('autonumber', Integer.class) == [1, 2, 3, 4, 5]

		subform.selectAllRows()
		subform.cloneSelectedRows()

		assert subform.getColumnValues('autonumber', Integer.class) == [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	}

	@Test
	void nonMovableEntriesInSubformWithUniqueAutonumber() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBER)
		eo.addNew()
		eo.setAttribute('name', 'Autonumber unique test')

		eo.selectTab('Autonumber Subform Unique')
		Subform subformUnique = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORMUNIQUE, 'parent')

		createEntries:
		{
			5.times {
				SubformRow row = subformUnique.newRow()
				assert row.getValue('autonumber', Integer.class) == it + 1
			}

			assert subformUnique.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
		}
		eo.save()

		eo.selectTab('Autonumber Subform Unique')
		subformUnique = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORMUNIQUE, 'parent')
		assert !subformUnique.moveUpEnabled
		assert !subformUnique.moveDownEnabled

		subformUnique.toggleSelection(0)
		assert !subformUnique.moveUpEnabled
		assert !subformUnique.moveDownEnabled

		subformUnique.toggleSelection(1)
		assert !subformUnique.moveUpEnabled
		assert !subformUnique.moveDownEnabled
	}

	private void doIt(boolean isFieldReadOnly = false) {
		def subformEntity = !isFieldReadOnly ? TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORM : TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORMRO

		_05_autonumberInSubform:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBER)
			eo.addNew()
			eo.setAttribute('name', 'Autonumber test')

			if (isFieldReadOnly) {
				eo.selectTab('Autonumber Subform RO')
			}
			Subform subform = eo.getSubform(subformEntity, 'parent')

			createEntries:
			{
				5.times {
					SubformRow row = subform.newRow()
					assert row.getValue('autonumber', Integer.class) == it + 1
				}

				assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
			}

			deleteNewEntry:
			{
				subform.getRow(2).setSelected(true)
				subform.deleteSelectedRows()

				assert subform.getColumnValues('autonumber', Integer.class) == [4, 3, 2, 1]
			}

			save:
			{
				eo.save()
				if (isFieldReadOnly) {
					eo.selectTab('Autonumber Subform RO')
				}
				subform = eo.getSubform(subformEntity, 'parent')
				assert subform.getColumnValues('autonumber', Integer.class) == [4, 3, 2, 1]
			}

			deleteSavedEntry:
			{
				subform.getRow(0).setSelected(true)
				subform.getRow(2).setSelected(true)
				subform.deleteSelectedRows()

				assert subform.getColumnValues('autonumber', Integer.class) == [2, 1, null, null]

				eo.save()
				assert subform.getColumnValues('autonumber', Integer.class) == [2, 1]
			}

			addAndClone:
			{
				if (isFieldReadOnly) {
					eo.selectTab('Autonumber Subform RO')
				}
				subform.newRow()

				assert subform.getColumnValues('autonumber', Integer.class) == [3, 2, 1]
				assert subform.getRow(0).dirty
				assert !subform.getRow(1).dirty
				assert !subform.getRow(2).dirty

				subform.getRow(1).setSelected(true)
				subform.getRow(2).setSelected(true)

				subform.cloneSelectedRows()

				assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
				assert subform.getRow(0).dirty
				assert subform.getRow(1).dirty
				assert subform.getRow(2).dirty
				assert !subform.getRow(3).dirty
				assert !subform.getRow(4).dirty

				eo.save()
			}

			if (!isFieldReadOnly) {
				editManually:
				{
					subform.getRow(2).enterValue('autonumber', '8')

					assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
					assert subform.getRow(0).dirty
					assert subform.getRow(1).dirty
					assert subform.getRow(2).dirty
					assert !subform.getRow(3).dirty
					assert !subform.getRow(4).dirty
				}

				eo.cancel()
			}

		}

		_10_moveViaArrows:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()
			eo.setAttribute('name', 'Test 2')

			if (isFieldReadOnly) {
				eo.selectTab('Autonumber Subform RO')
			}
			Subform subform = eo.getSubform(subformEntity, 'parent')
			assert subform


			createEntries:
			{
				5.times {
					SubformRow row = subform.newRow()
					assert row.getValue('autonumber', Integer.class) == it + 1
					row.enterValue('text', "${97 + it as char}")
				}

				assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
				assert subform.getColumnValues('text', String.class) == ['e', 'd', 'c', 'b', 'a']
			}

			eo.save()
			if (isFieldReadOnly) {
				eo.selectTab('Autonumber Subform RO')
			}

			assert !subform.moveUpEnabled
			assert !subform.moveDownEnabled

			subform.toggleSelection(0)

			assert !subform.moveUpEnabled
			assert subform.moveDownEnabled

			assert !eo.dirty
			subform.moveSelectedRowsDown()
			assert eo.dirty

			// Moving down the first row should update only first and second row
			assert subform.getRow(0).dirty
			assert subform.getRow(1).dirty
			assert !subform.getRow(2).dirty

			assert subform.getColumnValues('text', String.class) == ['d', 'e', 'c', 'b', 'a']

			assert subform.moveUpEnabled
			assert subform.moveDownEnabled

			4.times {
				subform.moveSelectedRowsDown()
			}

			assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
			assert subform.getColumnValues('text', String.class) == ['d', 'c', 'b', 'a', 'e']

			assert subform.moveUpEnabled
			assert !subform.moveDownEnabled

			subform.toggleSelection(0)

			// First and last row toggled now -> can't move
			assert !subform.moveUpEnabled
			assert !subform.moveDownEnabled

			subform.toggleSelection(4)

			assert !subform.moveUpEnabled
			assert subform.moveDownEnabled

			subform.toggleSelection(0)

			// No rows toggled now -> can't move
			assert !subform.moveUpEnabled
			assert !subform.moveDownEnabled

			eo.save()
			if (isFieldReadOnly) {
				eo.selectTab('Autonumber Subform RO')
			}

			assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
			assert subform.getColumnValues('text', String.class) == ['d', 'c', 'b', 'a', 'e']

			// Move multiple non-consecutive rows at once
			subform.toggleSelection(1)
			subform.toggleSelection(3)

			assert subform.moveUpEnabled
			assert subform.moveDownEnabled

			subform.moveSelectedRowsDown()
			assert subform.moveUpEnabled
			assert !subform.moveDownEnabled
			assert subform.getColumnValues('text', String.class) == ['d', 'b', 'c', 'e', 'a']

			subform.moveSelectedRowsUp()
			assert subform.moveUpEnabled
			assert subform.moveDownEnabled
			assert subform.getColumnValues('text', String.class) == ['d', 'c', 'b', 'a', 'e']

			subform.moveSelectedRowsUp()
			assert !subform.moveUpEnabled
			assert subform.moveDownEnabled
			assert subform.getColumnValues('text', String.class) == ['c', 'd', 'a', 'b', 'e']

			// Move multiple consecutive rows at once
			subform.toggleSelection(1)

			subform.moveSelectedRowsDown()
			assert subform.moveUpEnabled
			assert subform.moveDownEnabled
			assert subform.getColumnValues('text', String.class) == ['b', 'c', 'd', 'a', 'e']
			eo.save()
		}
	}
}
