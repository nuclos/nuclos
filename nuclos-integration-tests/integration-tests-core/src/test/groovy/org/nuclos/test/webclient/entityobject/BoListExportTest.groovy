package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.PrintoutComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration

import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.HarEntry
import net.lightbody.bmp.proxy.CaptureType

@Ignore
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class BoListExportTest extends AbstractRerunnableWebclientTest {

	public static final String USERWOEXPORT_PASSWORD = 'test'
	private final String USERWOEXPORT = 'Test.wo.export'

	@Before
	void initializeData() {
		TestDataHelper.insertTestData(nuclosSession)
		logout()
		login('nuclos')
		RESTHelper.createUser(USERWOEXPORT, USERWOEXPORT_PASSWORD, ['Example user w/o Export'], nuclosSession)
	}

	@Ignore
	@Test
	void exportNotAllowed() {

		generalPermissionNotGranted: {
			logout()
			login(USERWOEXPORT, USERWOEXPORT_PASSWORD)

			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
			assert $('#export-bo-list') == null

			eo.selectTab('Position')
			$('.open-more-actions-popup').click()
			assert $('#export-subbo-list') == null

		}

		exportForBoNotGranted: {
			logout()
			login('test', 'test')

			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS)
			assert $('#export-bo-list') == null

			eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
			eo.selectTab('Address')
			$('.open-more-actions-popup').click()
			assert $('#export-subbo-list') == null
		}
	}

	@Ignore
	@Test
	void exportBoList() {
		RESTClient client = new RESTClient('test', 'test').login()
		String sessionId = client.sessionId

		openPrintDialog: {
			EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)

			SideviewConfiguration.newSideviewConfiguration('Sideviewconfig1')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_name')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_active')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_price')
			SideviewConfiguration.saveSideviewConfiguration()
			SideviewConfiguration.close()
			Sidebar.resizeSidebarComponent(800)

			assert Sidebar.selectedColumns().size() == 3

			// open export dialog
			$('#export-bo-list').click()
			waitForAngularRequestsToFinish()
		}

		executePrintoutPDF: {
			testExecuteDownload('pdf', sessionId)
		}

		executePrintoutCSV: {
			// assert that export contains selcted columns
			testExecuteDownload('csv', sessionId, '"Name";"Active";"Price"\n"Mouse"')
		}

		executePrintoutXLSX: {
			testExecuteDownload('xlsx', sessionId)
		}

		executePrintoutXLS: {
			testExecuteDownload('xls', sessionId)
		}
	}

	@Ignore
	@Test
	void exportSubBoList() {
		RESTClient client = new RESTClient('test', 'test').login()
		String sessionId = client.sessionId

		prepareSubformExport: {
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
			subform.openViewConfiguration()
			SideviewConfiguration.newSideviewConfiguration('SubformColumnConfig1')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_quantity')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_name')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_price')
			SideviewConfiguration.clickButtonOk()

			openExportModal()
		}

		executeSubformPrintoutPDF: {
			testExecuteDownload('pdf', sessionId)
		}

		executeSubformPrintoutCSV: {
			// assert that export contains selcted columns
			testExecuteDownload('csv', sessionId, '"Quantity";"Name";"Price"')
		}

		executeSubformPrintoutXLSX: {
			testExecuteDownload('xlsx', sessionId)
		}

		executeSubformPrintoutXLS: {
			testExecuteDownload('xls', sessionId)
		}
	}

	@Ignore
	@Test
	void exportDynamicBoList() {
		RESTClient client = new RESTClient('test', 'test').login()
		String sessionId = client.sessionId

		prepareExport: {
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
			eo.selectTab('Dynamic Entity')

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_MYORDERSANDCUSTOMERSDYN.fqn + '_INTIDTUDGENERICOBJECT')
			subform.openViewConfiguration()
			SideviewConfiguration.newSideviewConfiguration('DynBoColumnConfig1')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_MYORDERSANDCUSTOMERSDYN.fqn + '_ENTITYUID')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_MYORDERSANDCUSTOMERSDYN.fqn + '_Number')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_MYORDERSANDCUSTOMERSDYN.fqn + '_Type')
			SideviewConfiguration.clickButtonOk()

			openExportModal()
		}

		executePrintoutCSV: {
			// assert that export contains selcted columns
			testExecuteDownload('csv', sessionId, '"ENTITY_UID";"Number";"Type"\n"sYFUgWuHvxtv8MDrpgV2"')
		}
	}

	@Ignore
	@Test
	void exportSubBoListWithID() {
		RESTClient client = new RESTClient('test', 'test').login()
		String sessionId = client.sessionId

		prepareExportWithoutID: {
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
			eo.addNew()
			eo.setAttribute('text', 'exportSubBoListWithID')

			Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
			subform.openViewConfiguration()
			SideviewConfiguration.newSideviewConfiguration('exportWithIdTest1')
			SideviewConfiguration.selectColumn(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_text')
			SideviewConfiguration.selectColumn(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_date')
			SideviewConfiguration.clickButtonOk()
			def row = subform.newRow()
			row.enterValue('text', 'SF 1')
			eo.save()

			openExportModal()
		}

		executePrintoutCSVWithoutID: {
			// assert that export contains selected columns
			testExecuteDownload('csv', sessionId, '"Text";"Date"\n"SF 1";""\n')
			$('button.btn-close.close').click()
		}

		def subId
		prepareExportWithID: {
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
			subform.openViewConfiguration()
			SideviewConfiguration.newSideviewConfiguration('exportWithIdTest2')
			SideviewConfiguration.selectColumn(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_text')
			SideviewConfiguration.selectColumn(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_date')
			SideviewConfiguration.selectColumn(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_primaryKey')
			SideviewConfiguration.clickButtonOk()

			def row = subform.getRow(0)
			subId = row.getValue('primaryKey')

			openExportModal()
		}

		executePrintoutCSVWithID: {
			// assert that export contains selected columns
			testExecuteDownload('csv', sessionId, "\"Text\";\"Date\";\"ID\"\n\"SF 1\";\"\";\"$subId\"\n");
			$('button.btn-close.close').click()
		}
	}

	private static void testExecuteDownload(String outputType, String sessionId, String containsText = null) {
		// select outputType
		//$('#list-export-modal input[value="' + outputType + '"]').click()
		$vVv('#list-export-modal input[value="' + outputType + '"]').click()

		// execute & download export
		def har = getBrowserHar([CaptureType.RESPONSE_CONTENT], {
			$('#list-export-modal #execute-listexport-button').click()
		})
		def entry = har.log.entries.find {
			HarEntry entry -> entry.request.method == 'POST' && entry.request.url.toLowerCase().contains('export')
		}
		def jsonSlurper = new JsonSlurper();
		def jsonMap = jsonSlurper.parseText(entry.response.content.text)
		def url = (((jsonMap as Map).links as Map).export as Map).href

		checkExportResponse(url as String, sessionId, containsText)
		screenshot('export-' + outputType + '-done')
	}

	static void checkExportResponse(String url, String sessionId, String containsText) {
		HttpURLConnection connection = (HttpURLConnection)new URL("http:$url").openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Cookie","JSESSIONID=" + sessionId);
		connection.connect();

		InputStreamReader isr = new InputStreamReader((InputStream) connection.getContent());
		BufferedReader buff = new BufferedReader(isr);
		String line;
		String text;
		while (true) {
			line = buff.readLine();
			text += line + '\n';
			if (line == null) {
				break
			}
		}

		assert connection.getResponseCode() == 200 // HTTP status ok

		assert text.size() > 0

		if (containsText != null) {
			assert text.indexOf(containsText) != -1
		}
	}

	static void openExportModal() {
		$('.open-more-actions-popup').click()
		$xXx('#export-subbo-list').click()
		if (waitForNotNull(10, {$vVv('#list-export-modal')}, false) == null) {
			// not opened -> selenium bug?
			$xXx('#export-subbo-list').click()
			$vVv('#list-export-modal')
		}
	}

}
