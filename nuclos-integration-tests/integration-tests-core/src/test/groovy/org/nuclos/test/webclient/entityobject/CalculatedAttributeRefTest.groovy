package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.Test
import org.nuclos.test.EntityObject
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

class CalculatedAttributeRefTest  extends AbstractRerunnableWebclientTest {
	private EntityObject ref2
	private EntityObject ref1
	private EntityObject testCalcAttribute

	@Before
	void setup() {
		this.ref2 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTCALCULATEDATTRIBUTEREF2)
		this.ref2.setAttribute("name", 'ref2')
		nuclosSession.save(this.ref2)

		this.ref1 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTCALCULATEDATTRIBUTEREF)
		this.ref1.setAttribute("name", 'ref1')
		this.ref1.setAttribute("ref2", [id: ref2.id])
		nuclosSession.save(this.ref1)

		this.testCalcAttribute = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTCALCULATEDATTRIBUTE)
		this.testCalcAttribute.setAttribute("name", 'testCalcAttribute')
		this.testCalcAttribute.setAttribute("ref", [id: ref1.id])
		nuclosSession.save(this.testCalcAttribute)
	}

	@Test
	void testCalcAttributeRef() {
		EntityObjectComponent eoRef2 = EntityObjectComponent.open(ref2)
		assert eoRef2.getAttribute('calcds') == "${ref2.id}"
		assert eoRef2.getAttribute('calcfunc') == "${ref2.id}"

		EntityObjectComponent eoRef1 = EntityObjectComponent.open(ref1)
		assert eoRef1.getAttribute('calcds') == "${ref1.id}"
		assert eoRef1.getAttribute('calcfunc') == "${ref1.id}"
		assert eoRef1.getAttribute('ref2') == "${ref2.getAttribute('name')} - ${ref2.id}: ${ref2.id}"

		EntityObjectComponent eoTestCalcAttribute = EntityObjectComponent.open(testCalcAttribute)
		assert eoTestCalcAttribute.getAttribute('ref') == "${ref1.getAttribute('name')} - ${ref2.getAttribute('name')} - ${ref2.id}: ${ref2.id}: ${ref1.id} / ${ref1.id}"
	}
}
