package org.nuclos.test.webclient.entityobject

import java.nio.file.Files

import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.CloseableHttpResponse;
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.statepath.StatePathComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class CloneTest extends AbstractRerunnableWebclientTest {

	private EntityObject<Long> parent;

@Test
void runTest() {
	_00_setup: {
		RESTHelper.createUser('tset', 'test', ['Example user'], nuclosSession)

		parent = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		parent.setAttribute('text', 'parent')
		parent.setAttribute('value', 1)

		List<EntityObject<Long>> subform = parent.getDependents(
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
				'parent'
		)

		3.times { subCount ->
			EntityObject subEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM)
			subEo.setAttribute('text', subCount.toString())
			subEo.setAttribute('date', new Date())
			subform << subEo

			List<EntityObject<Long>> subsubform = subEo.getDependents(
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					'subform'
			)
			3.times { subsubCount ->
				EntityObject subsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM)
				subsubEo.setAttribute('text', "$subCount-$subsubCount".toString())
				subsubEo.setAttribute('value', 123.45)
				subsubform << subsubEo

				List<EntityObject<Long>> subsubsubform = subsubEo.getDependents(
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM,
						'subsubform'
				)
				3.times { subsubsubCount ->
					EntityObject subsubsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM)
					subsubsubEo.setAttribute('text', "$subCount-$subsubCount-$subsubsubCount".toString())
					subsubsubEo.setAttribute('value', 12345)
					subsubsubform << subsubsubEo
				}
			}
		}

		nuclosSession.save(parent)

		screenshot('before clone')

		EntityObjectComponent eo = EntityObjectComponent.open(parent);
		eo.clone()

		screenshot('after clone')

		assert eo.isDirty()

		assert eo.getAttribute('text') == 'parent'
		eo.setAttribute('text', 'parent 2')
		assert eo.getAttribute('value') == ''

		Subform subformComponent = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		assert subformComponent.getRowCount() == 3

		for (int i = 0; i < subformComponent.getRowCount(); i++) {
			SubformRow sfRow = subformComponent.getRow(i)
			assert sfRow.getValue('text', String.class) == i + ''
			assert sfRow.getValue('date', Date.class) != null

			sfRow.setSelected(true)

			Subform subsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform')

			for (int j = 0; j < subsubform.getRowCount(); j++) {
				assert subsubform.getRowCount() == 3

				SubformRow ssfRow = subsubform.getRow(j)
				assert ssfRow.getValue('text', String.class) == i + '-' + j
				assert ssfRow.getValue('value', String.class) == ''

				ssfRow.setSelected(true)

				Subform subsubsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM, 'subsubform')
				assert subsubsubform.getRowCount() == 0

				ssfRow.setSelected(false)
			}

			sfRow.setSelected(false)
		}

		eo.save()

		eo = EntityObjectComponent.forDetail()
		assert eo.getAttribute('text') == 'parent 2'
	}
}

	@Test
	void testCloneCorrectFileLink_NUCLOS_10451() {
		RESTClient testRestClient = new RESTClient('test', 'test')
		testRestClient.login()
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()
		eo.setAttribute('text', 'testCloneSubformWithEmptyDocumentField')
		eo.selectTab('Subform Documents')
		def subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_SUBFORMDOCUMENTS, 'parent')
		def row = subform.newRow()
		row.getFileComponent('document1').setFile(TestDataHelper.nuclosDocxFile)
		eo.save()
		eo.clone()
		eo.selectTab('Subform Documents')
		subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_SUBFORMDOCUMENTS, 'parent')
		row = subform.getRow(0)

		String fileUrl = row.getFileComponent('document1').getHref()

		eo.cancel()

		CloseableHttpResponse response = nuclosSession.getUrl(fileUrl)
		assert response.getStatusLine().getStatusCode() == 403: 'Only the test-user, who created the temporary object, should be able to access it'
		CloseableHttpResponse response2 = testRestClient.getUrl(fileUrl)
		assert response2.getStatusLine().getStatusCode() == 200: 'Test-user should be able to access the temporary file'
		assert Files.readAllBytes(TestDataHelper.nuclosDocxFile.toPath()) == IOUtils.toByteArray(response2.getEntity().getContent()): 'The file url should return the correct file contents'

		assert !fileUrl.contains('null'): 'The file url should contain the temporary id, when an entry has been cloned and not saved'
	}

	/**
	 * This test verifies that following problem (see NUCLOS-10318) has been fixed:
	 * Original and WRONG behaviour: When cloning an entry, the subforms defined in the initial layout of an entity object are being cloned
	 * Correct behaviour: When cloning an entry, the subforms defined in the layout associated with the current state should be cloned
	 */
	@Test
	void _testCloneCurrentLayoutDependents_NUCLOS_10318() {

		_00_setup:
		{
			parent = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTWEBCLIENTCLONEDEPENDENTS)
			parent.setAttribute('name', 'parent')

			List<EntityObject<Long>> subform = parent.getDependents(
					TestEntities.NUCLET_TEST_OTHER_TESTWEBCLIENTCLONESUBFORM,
					'parent'
			)
			3.times { subCount ->
				EntityObject subEo = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTWEBCLIENTCLONESUBFORM)
				subEo.setAttribute('text', subCount.toString())
				subform << subEo
			}

			nuclosSession.save(parent)

			EntityObjectComponent eoc = EntityObjectComponent.open(parent)

			StatePathComponent defaultPath = new StatePathComponent()
			StateComponent.confirmStateChange {
				defaultPath.stepElements[1].click()
			}
			eoc.clone()
			eoc.setAttribute("name", "test123")

			eoc.save()

			defaultPath = new StatePathComponent()
			StateComponent.confirmStateChange {
				defaultPath.stepElements[1].click()
			}

			eoc = EntityObjectComponent.forDetail()

			Subform subformComponent = eoc.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTWEBCLIENTCLONESUBFORM, 'parent')

			assert subformComponent.getRowCount() == 3: 'Subform rows should have been cloned'

			for (int j = 0; j < subformComponent.getRowCount(); j++) {
				SubformRow ssfRow = subformComponent.getRow(j)
				assert ssfRow.getValue('text', String.class) == '' + (2 - j)
			}
		}
	}

	@Test
	void testCloneWithDependantsAndException() {
		TestDataHelper.insertTestData(nuclosSession)

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		def subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'article')
		def count = subform.rowCount
//		subform.newRow()
//		def modal = EntityObjectComponent.forModal()
//		modal.getLOV('order').sendKeys(Keys.TAB, Keys.ENTER)
//		modal.setAttribute('price', 123)
//		EntityObjectComponent.clickButtonOk()
//		eo.save()

		eo.clone()
		eo.save()
		assertMessageModalAndConfirm('Fehler', 'Eindeutigkeitsverletzung')

		eo.save()
		assertMessageModalAndConfirm('Fehler', 'Eindeutigkeitsverletzung')

		eo.setAttribute('articleNumber', 123)
		eo.setAttribute('name', 'test NUCLOS-10651')
		eo.save()
		assertMessageModalAndConfirm('Fehler', 'No!')

		eo.setAttribute('name', 'test NUCLOS-10651 OK')
		eo.save()
		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'article')
		def cloneCount = subform.rowCount
		assert cloneCount == count: 'There must be as many subform rows after cloning as before.'
	}

	@Test
	void testCloneSubformWithEmptyDocumentField() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()
		eo.setAttribute('text', 'testCloneSubformWithEmptyDocumentField')
		eo.selectTab('Subform Documents')
		def subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_SUBFORMDOCUMENTS, 'parent')
		def row = subform.newRow()
		row.getFileComponent('document1').setFile(TestDataHelper.nuclosDocxFile)
		eo.save()

		eo.selectTab('Subform Documents')
		subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_SUBFORMDOCUMENTS, 'parent')
		subform.getRow(0).setSelected(true)
		subform.cloneSelectedRows()

		eo.save()
		assertNoMessageModel()

		eo.selectTab('Subform Documents')
		subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_SUBFORMDOCUMENTS, 'parent')
		[0,1].forEach {
			row = subform.getRow(it)
			assert row.getFileComponent('document1').getText() == TestDataHelper.nuclosDocxFile.name
			assert row.getFileComponent('document2').getText().startsWith('Datei zum Hochladen auswählen')
		}
	}

	@Test
	void testRuleWithExceptionAfterClone() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTCLONE)
		eo.addNew()
		eo.setAttribute('name', 'testRuleWithExceptionAfterClone')
		def subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTCLONEDEP, 'parent')
		5.times {
			def row = subform.newRow()
			row.enterValue('name', "test_dep_$it")
		}
		eo.save()

		eo.clone()
		eo.setAttribute('name', 'testRuleWithExceptionAfterClone_clone')
		eo.setAttribute('cloned', true)
		eo.save()
		assertMessageModalAndConfirm('Fehler', 'Failed!')

		eo.save()
		assertMessageModalAndConfirm('Fehler', 'Failed!')

		eo.setAttribute('cloned', false)
		eo.save()
		assertNoMessageModel()
	}
}
