package org.nuclos.test.webclient.entityobject


import org.junit.FixMethodOrder
import org.junit.experimental.categories.Category
import org.junit.jupiter.api.Test
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class CustomKeyLayoutTest extends AbstractRerunnableWebclientTest {

	@org.junit.Test
	void testLayoutCustomKey() {
		setupSystemParameters()
		refresh()

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_CUSTOMKEYLAYOUTTEST)
		eo.addNew()
		assert eo.getStaticLabel('Label_2nNY').getText() == 'Weblayout'

		nuclosSession.setSystemParameters(['LAYOUT_CUSTOM_KEY':'custom'])
		refresh()

		assert eo.getStaticLabel('Label_2nNY').getText() == 'Custom'
	}
}
