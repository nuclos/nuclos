package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.MenuComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.statepath.StatePathComponent
import org.openqa.selenium.Dimension

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DefaultPathTest extends AbstractRerunnableWebclientTest {

	@Before
	void setup() {
		TestDataHelper.insertTestData(nuclosSession)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()

		eo.setAttribute('name', 'Stüker')
		eo.setAttribute('vorname', 'Maik')
		eo.setAttribute('alter', '35')
		eo.setAttribute('kondition', 'ganz gut')

		eo.save()

		eo = EntityObjectComponent.forDetail().addNew()

		eo.setAttribute('name', 'Zilm')
		eo.setAttribute('vorname', 'Falk')
		eo.setAttribute('alter', '31')
		eo.setAttribute('kondition', 'joa')

		eo.save()
	}

	@Test
	void assertGraphNotVisibleWhenNoDefaultPath() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		assert new StatePathComponent().isVisible() == Boolean.FALSE: 'There should be no graph element on Order'
	}

	@Test
	void testDefaultPathVisible() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		Sidebar.selectEntry(1)
		StatePathComponent defaultPath = new StatePathComponent()

		assert defaultPath.isVisible(): 'There should be a default graph'
		assert defaultPath.currentProgressValue == 12.5f: 'Initially no progress'
		assert defaultPath.stepNumerals == [100, 2, 33, 444]: 'Numerals should be visible'
		assert defaultPath.stepElements.collect {it.getStateName()} == ['Grund', 'Erster Aufstieg', 'Zweiter Aufstieg', 'Bergspitze']: 'State names should be visible'
		assert defaultPath.stepElements.first().isActive(): 'First element should be active'
		assert defaultPath.stepElements.first().getSize() == new Dimension(40, 40): 'First bubble should be bigger'
		assert defaultPath.stepElements[1].isEnabled(): 'Second element should be enabled'
		assert defaultPath.stepElements[1].getSize() == new Dimension(30, 30): 'Second bubble should be normal size'
		assert defaultPath.stepElements[1].getFontColor() == 'rgba(17, 144, 254, 1)': 'Enabled state path should be color blue'

		defaultPath.stepElements[1].hover()
		assert defaultPath.currentProgressValue > 12.5f: 'Path Progress should be moved'
		assert !defaultPath.stepElements.first().isActive(): 'First element should be switched visible'
		assert defaultPath.stepElements.first().isComplete(): 'First element should be marked as complete'
		assert defaultPath.stepElements.first().getBackgroundColor() == 'rgb(153, 204, 0)': 'Complete Background should be applied correctly'
		assert defaultPath.stepElements[1].getFontColor() == 'rgba(17, 144, 254, 1)': 'Active state should be still color blue'
		assert defaultPath.stepElements[1].isActive(): 'Second element should be active'
		assert defaultPath.stepElements[1].getSize() == new Dimension(40, 40): 'Second bubble should be bigger'

		StateComponent.confirmStateChange {
			defaultPath.stepElements[1].click()
		}

		waitForAngularRequestsToFinish()
		assert defaultPath.currentProgressValue > 12.5f: 'Path Progress should be moved'
		assert !defaultPath.stepElements.first().isActive(): 'First element should be switched visible'
		assert defaultPath.stepElements.first().isComplete(): 'First element should be marked as complete'
		assert defaultPath.stepElements.first().getBackgroundColor() == 'rgb(153, 204, 0)': 'Complete Background should be applied correctly'
		assert defaultPath.stepElements[1].getFontColor() == 'rgba(33, 37, 41, 1)': 'Active state should be font color black'
		assert defaultPath.stepElements[1].isActive(): 'Second element should be active'
		assert defaultPath.stepElements[1].getSize() == new Dimension(40, 40): 'Second bubble should be bigger'
	}

	@Test
	void testOutsidePathHandling() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		Sidebar.selectEntry(0)
		StatePathComponent defaultPath = new StatePathComponent()

		assert defaultPath.isVisible(): 'There should be a default graph'

		StateComponent.confirmStateChange {
			defaultPath.stepElements.last()?.click()
		}
		waitForAngularRequestsToFinish()

		// we should be now on last step bergspitze
		assert defaultPath.stepElements.last().isActive(): 'Bergspitze should be active'
		assert defaultPath.stepElements.last().hasOutsidePath(): 'There should be outsidePath Button'

		StateComponent.confirmStateChange {
			defaultPath.stepElements.last().clickOutSidePathElement("Zurücksetzen")
		}
		waitForAngularRequestsToFinish()

		// we now left default path
		assert defaultPath.stepNumerals == [100, 2, 33, 444, 900]: 'Numerals should be correctly present'
		assert defaultPath.stepElements.findAll {it -> !it.isEnabled()}.size() == 5: 'No state should be enabled/clickable'
		assert defaultPath.stepElements.last().isActive(): 'Last element is active'
		assert defaultPath.stepElements.last().hasOutsidePath(): 'Button to get ongoing is visible'

		// go back to default path
		StateComponent.confirmStateChange {
			defaultPath.stepElements.last().clickOutSidePathElement("Grund")
		}
		waitForAngularRequestsToFinish()

		assert defaultPath.stepNumerals == [100, 2, 33, 444]: 'Numerals should be visible'
		assert defaultPath.stepElements.collect {it.getStateName()} == ['Grund', 'Erster Aufstieg', 'Zweiter Aufstieg', 'Bergspitze']: 'State names should be visible'
		assert defaultPath.stepElements.first().isActive(): 'First element should be active'
		assert defaultPath.stepElements.first().getSize() == new Dimension(40, 40): 'First bubble should be bigger'
		assert defaultPath.stepElements[1].isEnabled(): 'Second element should be enabled'
		assert defaultPath.stepElements[1].getSize() == new Dimension(30, 30): 'Second bubble should be normal size'
		assert defaultPath.stepElements[1].getFontColor() == 'rgba(17, 144, 254, 1)': 'Enabled state path should be color blue'
	}

	@Test
	void testNonDefaultTransitionBetweenDefaultPathStates() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		Sidebar.selectEntry(0)
		StatePathComponent defaultPath = new StatePathComponent()

		assert defaultPath.isVisible(): 'There should be a default graph'

		StateComponent.confirmStateChange {
			defaultPath.stepElements[1]?.click()
		}
		waitForAngularRequestsToFinish()

		assert !defaultPath.stepElements[1]?.hasOutsidePathElement('Grund'): 'A default backwards transition should not be listed as an outside path element'
		defaultPath.stepElements[1]?.outsidePathButton().click()
		// we should be now on the second step "Erster Aufstieg"
		assert defaultPath.stepElements[1]?.isActive(): 'Erster Aufstieg should be active'
		assert defaultPath.stepElements[1]?.hasOutsidePath(): 'There should be outsidePath Button leading to the last state inside the default path'

		StateComponent.confirmStateChange {
			defaultPath.stepElements[1]?.clickOutSidePathElement("Abkürzung")
		}
		waitForAngularRequestsToFinish()

		// we now left default path
		assert defaultPath.stepNumerals == [100, 2, 33, 444]: 'Numerals should be correctly present'
		assert defaultPath.stepElements.findAll {it -> !it.isEnabled()}.size() == 4: 'No state should be enabled/clickable'
		assert defaultPath.stepElements.last().isActive(): 'Last element is active'
		assert defaultPath.stepElements.last().hasOutsidePath(): 'Button to get ongoing is visible'
	}

	@Test
	void testTraversingBackOnDefaultPathViaNonDefaultTransition() {
		//it should be possible to traverse back on default path via a non-default transition
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		Sidebar.selectEntry(0)
		StatePathComponent defaultPath = new StatePathComponent()

		assert defaultPath.isVisible(): 'There should be a default graph'

		StateComponent.confirmStateChange {
			defaultPath.stepElements[1]?.click()
		}
		waitForAngularRequestsToFinish()

		// we should be now on the second step "Erster Aufstieg"
		assert defaultPath.stepElements[1]?.isActive(): 'Erster Aufstieg should be active'
		assert defaultPath.stepElements[1]?.hasOutsidePath(): 'There should be outsidePath Button leading to the last state inside the default path'
		assert defaultPath.stepElements[0]?.enabled: 'Grund should be enabled, because we want to be able to go backwards on the default path'

		StateComponent.confirmStateChange {
			defaultPath.stepElements[0]?.click()
		}

		assert defaultPath.stepNumerals == [100, 2, 33, 444]: 'Numerals should be correctly present'
		assert defaultPath.stepElements.findAll {it -> it.isEnabled()}.size() == 3: 'The complete default path should be ahead of us and therefore clickable'
		assert defaultPath.stepElements[0].isActive(): 'First element should be active'

		StateComponent.confirmStateChange {
			defaultPath.stepElements[3]?.click()
		}
		assert defaultPath.stepElements[3]?.isActive(): 'Bergspitze should be active'
		assert defaultPath.stepElements[3]?.hasOutsidePath(): 'There should be outsidePath Button leading to the last state inside the default path'
		assert defaultPath.stepElements[3]?.hasOutsidePathElement('2 Erster Aufstieg')
		defaultPath.stepElements[3]?.outsidePathButton().click()
		assert !defaultPath.stepElements[1]?.enabled: 'Erster Aufstieg should be disabled, because there is no way backwards on the default path'

		StateComponent.confirmStateChange {
			defaultPath.stepElements[3]?.clickOutSidePathElement("Erster Aufstieg")
		}
		assert defaultPath.stepElements[1].isActive(): 'Erster Aufstieg should be active'
	}

	@Test
	void updatingGraphWhenSwitchingBOs() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		Sidebar.selectEntry(0)
		StatePathComponent defaultPath = new StatePathComponent()

		assert defaultPath.isVisible(): 'There should be a default graph'
		StateComponent.confirmStateChange {
			defaultPath.stepElements.last()?.click()
		}
		waitForAngularRequestsToFinish()

		// we should be now on last step bergspitze
		assert defaultPath.stepElements.last().isActive(): 'Bergspitze should be active'
		assert defaultPath.stepElements.last().hasOutsidePath(): 'There should be outsidePath Button'


		Sidebar.selectEntry(1)
		defaultPath = new StatePathComponent()
		assert defaultPath.stepElements.first().isActive(): 'First element should be active'
		assert defaultPath.stepElements.first().getSize() == new Dimension(40, 40): 'First bubble should be bigger'
		assert defaultPath.stepElements[1].isEnabled(): 'Second element should be enabled'
		assert defaultPath.stepElements[1].getSize() == new Dimension(30, 30): 'Second bubble should be normal size'
		assert defaultPath.stepElements[1].getFontColor() == 'rgba(17, 144, 254, 1)': 'Enabled state path should be color blue'
	}

	@Test
	void readOnlyPath() {
		RESTHelper.createUser('test_read', 'test_read', ['Example readonly'], nuclosSession)

		// change to path state Erster Aufstieg
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		Sidebar.selectEntry(0)
		StatePathComponent defaultPath = new StatePathComponent()

		assert defaultPath.isVisible(): 'There should be a default graph'
		StateComponent.confirmStateChange {
			defaultPath.stepElements[1].click()
		}
		waitForAngularRequestsToFinish()


		assert defaultPath.stepElements[1].getFontColor() == 'rgba(33, 37, 41, 1)': 'Active state should be font color black'
		assert defaultPath.stepElements[1].isActive(): 'Second element should be active'
		assert defaultPath.stepElements[1].getSize() == new Dimension(40, 40): 'Second bubble should be bigger'
		assert defaultPath.stepElements[1].hasOutsidePath(): 'OutsidePath should be visible'
		assert defaultPath.stepElements.first().isEnabled(): 'First step is enabled'
		assert defaultPath.stepElements[2].isEnabled(): 'Third path step is enabled'

		logout()
		login('test_read', 'test_read')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		Sidebar.selectEntry(0)
		defaultPath = new StatePathComponent()

		assert defaultPath.isVisible(): 'There should be a default graph'
		assert defaultPath.stepElements[1].isActive(): 'Second element should be active'
		assert defaultPath.stepElements[1].getSize() == new Dimension(40, 40): 'Second bubble should be bigger'
		assert !defaultPath.stepElements[1].hasOutsidePath(): 'No OutsidePath should be visible'
		assert defaultPath.stepElements.findAll(it -> it.isEnabled()).size() == 0: 'There should be no step enabled'
	}

	@Test
	void testResizingPathOnViewPortChange() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		Sidebar.selectEntry(0)

		StatePathComponent defaultPath = new StatePathComponent()
		assert defaultPath.isVisible(): 'There should be a default graph'
		StateComponent.confirmStateChange {
			defaultPath.stepElements.last()?.click()
		}
		waitForAngularRequestsToFinish()

		// we should be now on last step bergspitze
		assert defaultPath.stepElements.last().isActive(): 'Bergspitze should be active'
		assert defaultPath.stepElements.last().hasOutsidePath(): 'There should be outsidePath Button'

		waitForAngularRequestsToFinish()
		Thread.sleep(1000) // let animation finish

		def graphImageSidebarHorizontal = defaultPath.screenShot()
		$('#toolbar > div > button:nth-child(1)')?.click()
		waitForAngularRequestsToFinish()
		Thread.sleep(1000) // let animation finish
		def graphImageSidebarVertical = defaultPath.screenShot()

		refresh()
		$('#toolbar > div > button:nth-child(1)')?.click()
		waitForAngularRequestsToFinish()
		Thread.sleep(1000) // let animation finish

		screenshot('refreshed_graph_horizontal')
		waitFor {
			graphImageSidebarHorizontal == new StatePathComponent().screenShot()
		}

		$('#toolbar > div > button:nth-child(1)')?.click()
		waitForAngularRequestsToFinish()
		Thread.sleep(1000) // let animation finish

		screenshot('refreshed_graph_vertical')
		waitFor {
			graphImageSidebarVertical == new StatePathComponent().screenShot()
		}

	}

	@Test
	void testChangeDefaultPathOnNuclosProcessChange() {
		logout()
		login('nuclos', '')
		MenuComponent.openMenu('Test Utils', 'Aktionen Standardpfad', 'Neu')
		def eo = EntityObjectComponent.forDetail()
		waitForAngularRequestsToFinish()

		StatePathComponent defaultPath = new StatePathComponent()
		assert defaultPath.isVisible(): 'There should be a default graph'
		assert defaultPath.stepElements.size() == 2: 'The default graph should have 2 states'
		// Check that the default path for Aktionenstandardpfad with no process is visible

		eo.setAttribute('nuclosProcess', 'Beta')
		waitForAngularRequestsToFinish()

		StatePathComponent defaultPath2 = new StatePathComponent()
		assert defaultPath2.isVisible(): 'There should be a default graph'
		assert defaultPath2.stepElements.size() == 3: 'The default graph for beta action should have 3 states'
	}

	/**
	 * See NUCLOS-10253
	 * When the user was not allowed to the UserAction.WorkspaceCustomizeEntityAndSubFormColumn,
	 * then the default path did not recalculate its layout because it did not receive the onSideviewPrefChanged() callback.
	 * This led to the no_more_bar div remaining at its old position.
	 */
	@Test
	void testResizeWhenMissingModifyPrefPermission() {
		RESTHelper.createUser('readonly', 'readonly', ['Example readonly'], nuclosSession)

		logout()
		login('readonly', 'readonly')

		// change to path state Erster Aufstieg
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		Sidebar.selectEntry(0)

		Sidebar.toggleSidebarMode()

		NuclosWebElement containerFluid = $('.nuc-progress');
		NuclosWebElement noMoreBar2 = $$('.no_more_bar')[1]

		// test for interval, where the trailing no_more_bar should generally be located in, when the graph component receives the callback and recalculates its layout
		assert Math.abs(noMoreBar2.element.location.x - (containerFluid.size.width - noMoreBar2.element.size.width)) <= 16: 'no_more_bar should have adjusted to sidebar mode toggle'
	}

}
