package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.Test
import org.junit.experimental.categories.Category
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.GenerationComponent
import org.nuclos.test.webclient.pageobjects.Modal
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.openqa.selenium.Keys
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@CompileStatic
class DetailAccessibilityTest extends AbstractRerunnableWebclientTest {

	@Before
	void setup() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void testAutoCompleteFieldPresent() {
		// we can just look up fields in DOM if autocomplete is present further tests are not really doable
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		waitForAngularRequestsToFinish()

		EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()
		assert eo.getAttributeElement('name').getAttribute('autocomplete') == 'family-name'
		assert eo.getAttributeElement('vorname').getAttribute('autocomplete') == 'given-name'
		assert eo.getAttributeElement('alter').getAttribute('autocomplete') == ''
	}

	@Test
	void focusFirstComponentInDetailView() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		new Actions(driver).keyDown(Keys.ALT).sendKeys('0').keyUp(Keys.ALT).perform()
		waitForAngularRequestsToFinish()

		assert checkCurrentActiveElement("input", [name: "attribute-orderNumber"]): "OrderNumber Attribute should be focused"
	}

	@Test
	void focusFirstThenUnfocus() {
		focusFirstComponentInDetailView()

		new Actions(driver).keyDown(Keys.ALT).sendKeys(Keys.ESCAPE).keyUp(Keys.ALT).perform()
		assert !checkCurrentActiveElement("input", ["name": "attribute-orderNumber"]): "OrderNumber Attribute should not be focused anymore"
	}

	@Test
	void focusNextStateElementComboBox() {
		/* Ensure that the State Display uses a ComboBox to display its states. */
		nuclosSession.setSystemParameters(['nuclos_LAF_Show_State_Combobox_Threshold': '0'])

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		new Actions(driver).keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys('1').keyUp(Keys.SHIFT).keyUp(Keys.ALT).perform()
		assert checkCurrentActiveElement("a", ["state-number": "80"]): "State button abschließen should be active"
	}

	@Test
	void focusNextStateElementButton() {
		/* Set the Threshold high enough, so the next states get displayed as individual buttons, instead of inside a Combobox. */
		nuclosSession.setSystemParameters(['nuclos_LAF_Show_State_Combobox_Threshold': '-1'])

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		new Actions(driver).keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys('1').keyUp(Keys.SHIFT).keyUp(Keys.ALT).perform()
		assert checkCurrentActiveElement("button", ["state-number": "80"]): "State button abschließen should be active"
	}

	@Test
	void testStateDisplayShowComboBoxLafParameter() {
		/* Set threshold so that the state display should use a combobox. */
		nuclosSession.setSystemParameters(['nuclos_LAF_Show_State_Combobox_Threshold': '0'])

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		/* Ensure that the State Display uses a ComboBox to display its states. */
		assert $("nuc-state-dropdown") != null
		$("nuc-state-dropdown").click()
		assert $$zZz(".nuclos-next-state").size() == 2

		/* Set the Threshold high enough, so the next states get displayed as individual buttons, instead of inside a Combobox. */
		nuclosSession.setSystemParameters(['nuclos_LAF_Show_State_Combobox_Threshold': '5'])
		EntityObjectComponent.refresh()

		/* We expect 3 Buttons: 1 button for the current state and in this case 2 next state buttons. */
		assert $("nuc-current-state") != null
		assert $$zZz("nuc-next-state").size() == 2
	}

	@Test
	void testStateShowIconPropertyPresence() {
		//For this test we assume, that the Test Rules StateModel has the showIcon Property set to true.
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		new Actions(driver).keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys('1').keyUp(Keys.SHIFT).keyUp(Keys.ALT).perform()

		assert $('#nuclosCurrentState .state-icon').displayed == true
		assert $$('nuclos-next-state').every {it -> it.$('state-icon').displayed == true }
	}

	@Test
	void testStateShowIconPropertyAbsence() {
		//For this test we assume, that the Test Rules StateModel has the showIcon Property set to false.
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_RULES_TESTRULES)
		waitForAngularRequestsToFinish()

		eo.addNew()

		eo.setAttribute('name', 'test')
		eo.save()

		new Actions(driver).keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys('1').keyUp(Keys.SHIFT).keyUp(Keys.ALT).perform()

		assert $('#nuclosCurrentState .state-icon').displayed == false
		assert $$('nuclos-next-state').every {it -> it.$('state-icon').displayed == false }
	}

	@Test
	void focusObjectGenDropdownAndOpen() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		new Actions(driver).keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys('2').keyUp(Keys.SHIFT).keyUp(Keys.ALT).perform()
		assert checkCurrentActiveElement("button", ["data-toggle": "dropdown", "aria-expanded": "true"]): "ObjectGen Dropdown should be selected and expanded"

		GenerationComponent.getGenerationItems().forEach({ NuclosWebElement element ->
			assert element.isDisplayed(): "Generation Element " + element.text + " should be display as dropdown was opened"
		})
	}

	@Test
	void navigationShortCutsTest() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys(Keys.END).keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform()
		assert EntityObjectComponent.forDetail().getAttribute('orderNumber') == "10020140": "Last element in list should be selected"

		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys(Keys.HOME).keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform()
		assert EntityObjectComponent.forDetail().getAttribute('orderNumber') == "10020158": "First element in list should be selected"

		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys(Keys.DOWN).keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform()
		assert EntityObjectComponent.forDetail().getAttribute('orderNumber') == "10020154": "Second element in list should be selected"

		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys(Keys.UP).keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform()
		assert EntityObjectComponent.forDetail().getAttribute('orderNumber') == "10020158": "First element in list should be selected"
	}

	@Test
	void tabNavigationTestInDetail() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		assert EntityObjectComponent.getTab('Position').getAttribute('class').contains('p-highlight'): "Current active Tab is Position"

		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys('t').keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform()
		assert !EntityObjectComponent.getTab('Position').getAttribute('class').contains('p-highlight'): "Previous active Tab was Position"
		assert EntityObjectComponent.getTab('Documents').getAttribute('class').contains('p-highlight'): "Current active Tab is Documents"

		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys('t').keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform()
		assert !EntityObjectComponent.getTab('Documents').getAttribute('class').contains('p-highlight'): "Previous active Tab was Documents"
		assert EntityObjectComponent.getTab('Rechnungen').getAttribute('class').contains('p-highlight'): "Current active Tab is Rechnungen"

		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys('t').keyUp(Keys.ALT).keyUp(Keys.SHIFT).keyUp(Keys.CONTROL).perform()
		assert !EntityObjectComponent.getTab('Rechnungen').getAttribute('class').contains('p-highlight'): "Previous active Tab was Rechnungen"
		assert EntityObjectComponent.getTab('Documents').getAttribute('class').contains('p-highlight'): "Current active Tab is Documents"
	}

	@Test
	void toolbarButtonsDetail() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		new Actions(driver).keyDown(Keys.ALT).sendKeys('n').keyUp(Keys.ALT).perform()
		waitForAngularRequestsToFinish()
		assert EntityObjectComponent.forDetail().getAttribute('orderNumber') == "": "OrderNumber should be empty as we are creating a new element"
		assert EntityObjectComponent.cancelButton != null: "While not saved yet there should be a cancel button"

		countBrowserRequests {
			new Actions(driver).keyDown(Keys.ALT).sendKeys('n').keyUp(Keys.ALT).perform()
		}.with {
			assert it.requestCount == 0: "We are already creating a new element, therefore the shortcut to create a new record must be ignored."
		}

		EntityObjectComponent.forDetail().enterText("orderNumber", "123456")
		new Actions(driver).keyDown(Keys.ALT).sendKeys('s').keyUp(Keys.ALT).perform()
		waitForAngularRequestsToFinish()
		assert EntityObjectComponent.cancelButton == null: "Cancel button should not be visible anymore as we saved"

		new Actions(driver).keyDown(Keys.ALT).sendKeys('d').keyUp(Keys.ALT).perform()
		assert EntityObjectComponent.cancelButton != null: "Cancel button should be visible"
		assert EntityObjectComponent.saveButton != null: "Save button should be visible"
		assert EntityObjectComponent.forDetail().getAttribute('orderNumber') == "123456": "Duplicate action should copy data"

		new Actions(driver).keyDown(Keys.ALT).sendKeys('u').keyUp(Keys.ALT).perform()
		waitForAngularRequestsToFinish()
		EntityObjectComponent.clickButtonOk()
		assert EntityObjectComponent.cancelButton == null: "Cancel button should be not visible"
		assert EntityObjectComponent.saveButton == null: "Save button should be not visible"
		Sidebar.selectEntry(0)
		waitForAngularRequestsToFinish()

		new Actions(driver).keyDown(Keys.ALT).sendKeys('u').keyUp(Keys.ALT).perform()
		assert !Modal.forSelector('nuc-dialog').isVisible(): "Shortcut to cancel editing should be ignored because there are no unsaved changes."

		new Actions(driver).keyDown(Keys.ALT).sendKeys('p').keyUp(Keys.ALT).perform()
		def printoutModal = Modal.forSelector("nuc-printout-dialog")
		waitForAngularRequestsToFinish()
		waitFor {
			return printoutModal.isVisible()
		}
		assert printoutModal.getTitle() == "Formularausdruck": "Print form should be visible"
		printoutModal.close()

		// update our data with REST to test refresh shortcut
		def boId = driver.currentUrl.substring(driver.currentUrl.lastIndexOf('/') + 1, driver.currentUrl.length())
		RESTClient client = new RESTClient('nuclos', '').login()
		RESTHelper.updateBo([
				"boMetaId"  : TestEntities.EXAMPLE_REST_ORDER.fqn,
				"boId"      : boId.toInteger(),
				"attributes": [
						"note": "testing from rest"
				]
		], client)
		assert EntityObjectComponent.forDetail().getAttribute('note') == "": "Note should be empty first"
		new Actions(driver).keyDown(Keys.ALT).sendKeys('r').keyUp(Keys.ALT).perform()
		waitForAngularRequestsToFinish()
		assert EntityObjectComponent.forDetail().getAttribute('note') == "testing from rest": "Note should be now filled"

		new Actions(driver).keyDown(Keys.ALT).sendKeys('x').keyUp(Keys.ALT).perform()
		waitForAngularRequestsToFinish()
		def deleteModal = Modal.forSelector("nuc-dialog")
		waitFor {
			return deleteModal.isVisible()
		}
		assert deleteModal.getTitle() == "Löschen": "Delete modal should be opened"
		deleteModal.close()
	}

	@Test
	void testFocusOnFirstSubformRow() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		keydownShortCut(true, false, false, '0')
		for (def i = 0; i < 19; i++) {
			sendKeys(Keys.TAB)
		}

		keydownShortCut(true, true, false, '0')
		assert checkCurrentActiveElement("div", ["col-id": "example_rest_OrderPosition_article"]): "Article should have focus in order position subform"
	}

	@Test
	void testSubformEditingActions() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		keydownShortCut(true, false, false, '0')
		for (def i = 0; i < 19; i++) {
			sendKeys(Keys.TAB)
		}

		new Actions(driver).keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys('n').keyUp(Keys.SHIFT).keyUp(Keys.ALT).perform()
		Subform subform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert subform.getNewRowCount() == 1: "There should be a row added newly to the subform"
		EntityObjectComponent.forDetail().cancel()

		// focus first row
		keydownShortCut(true, false, false, '0')
		for (def i = 0; i < 19; i++) {
			sendKeys(Keys.TAB)
		}

		keydownShortCut(true, true, false, '0')
		new Actions(driver).sendKeys(Keys.SPACE).perform()
		new Actions(driver).keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys('x').keyUp(Keys.SHIFT).keyUp(Keys.ALT).perform()
		subform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert subform.getDeletedRowCount() == 1: "There should be a row deleted in the subform"
		EntityObjectComponent.forDetail().cancel()

		// focus first row
		keydownShortCut(true, false, false, '0')
		for (def i = 0; i < 19; i++) {
			sendKeys(Keys.TAB)
		}

		keydownShortCut(true, true, false, '0')
		new Actions(driver).sendKeys(Keys.SPACE).perform()
		new Actions(driver).keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys('d').keyUp(Keys.SHIFT).keyUp(Keys.ALT).perform()
		subform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert subform.getNewRowCount() == 1: "There should be a row added in the subform"
		EntityObjectComponent.forDetail().cancel()
	}

	@Test
	void testResizeHandlerMovingViaKeyboard() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		def normalSizeBarWidth = Sidebar.getSizebarWidth()

		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys(Keys.LEFT).keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform()
		assert Sidebar.getSizebarWidth() < normalSizeBarWidth: "Sidebar should be collapsed on the left side"

		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys(Keys.RIGHT).keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform()
		assert Sidebar.getSizebarWidth() == normalSizeBarWidth: "Sidebar should be same width as it was on page open"

		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys(Keys.RIGHT).keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform()
		assert Sidebar.getSizebarWidth() > normalSizeBarWidth: "Sidebar should be collapsed on the right side"
	}

	@Test
	void canOpenSubFormElementInNewTabAfterSelect() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		waitForAngularRequestsToFinish()

		// Prerequisites for testing
		// Select Test-Customer and Orders Tab
		Sidebar.selectEntry(2)
		waitForAngularRequestsToFinish()
		EntityObjectComponent.selectTab('Orders')


		keydownShortCut(true, false, false, '0')
		for (def i = 0; i < 8; i++) {
			sendKeys(Keys.TAB)
		}

		keydownShortCut(true, true, false, '0')
		assert checkCurrentActiveElement("div", ["col-id": "example_rest_Order_nuclosStateIcon"]): "First customer should have focus"

		sendKeys(Keys.SPACE)
		def subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDER, 'customer')
		assert subform.getRow(0).isSelected()

		// short cut to open in new tab
		new Actions(driver).keyDown(Keys.ALT).keyDown(Keys.SHIFT).sendKeys('w').keyUp(Keys.SHIFT).keyUp(Keys.ALT).perform()
		waitForAngularRequestsToFinish()

		assert driver.windowHandles.size() > 1: 'ALT + SHIFT + W should open a new tab with selected entry'

		driver.switchTo().window(driver.windowHandles[1])
		waitForAngularRequestsToFinish()

		assert driver.title == 'Nuclos - Order | 10020158 Test-Customer': 'Second handle should be selected Test-Customer 10020158 in new tab'

		driver.close()
		driver.switchTo().window(driver.windowHandles[0])
		waitForAngularRequestsToFinish()

		sendKeys(Keys.SPACE)
		assert !subform.getRow(0).isSelected()
	}

	@Test
	void testAlternativTextForStateIcon() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()
		assert eo.getAttribute('nuclosState') == (getLocale() == Locale.GERMANY ? 'Aktive' : 'Processing'): 'State should be "Processing/Aktive" after initial data setup'

		def stateIcon = Sidebar.getStateIconImage(0)
		assert stateIcon.getAttribute('alt') == (getLocale() == Locale.GERMANY ? 'Status' : 'State') + ': ' + (getLocale() == Locale.GERMANY ? 'Aktive' : 'Processing')

		StateComponent.changeStateByNumeral(80)
		StateComponent.confirmStateChange()
		stateIcon = Sidebar.getStateIconImage(0)
		assert stateIcon.getAttribute('alt') == (getLocale() == Locale.GERMANY ? 'Status' : 'State') + ': ' + (getLocale() == Locale.GERMANY ? 'Abgeschlossen' : 'Finished')
	}

	@Test
	void testTabFocus() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTTABFOCUS)
		eo.addNew()
		assert checkCurrentActiveElement('input', ['name': 'attribute-name'])

		sendKeys(Keys.TAB)
		assert checkCurrentActiveElement('input', ['name': 'attribute-attribut1'])

		sendKeys(Keys.TAB)
		assert checkCurrentActiveElement('input', ['name': 'attribute-attribut2'])

		sendKeys(Keys.TAB)
		assert checkCurrentActiveElement('input', ['name': 'attribute-attributintab1'])

		sendKeys(Keys.TAB)
		assert checkCurrentActiveElement('input', ['name': 'attribute-attributintab2'])

		sendKeys(Keys.TAB)
		assert checkCurrentActiveElement('input', ['name': 'attribute-attributintab3'])

		sendKeys(Keys.TAB)
		assert checkCurrentActiveElement('input', ['name': 'attribute-name'])
	}
}
