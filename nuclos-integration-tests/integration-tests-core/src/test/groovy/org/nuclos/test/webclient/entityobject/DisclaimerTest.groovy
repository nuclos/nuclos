package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.DetailButtonsComponent
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DisclaimerTest extends AbstractRerunnableWebclientTest {

	@Before
	@Override
	void beforeEach() {
		AbstractWebclientTest.setup(true, false) {
			createPrivacyPolicyViaRest()
		}
	}

	static String title = 'Privacy Policy'

	static createPrivacyPolicyViaRest() {
		EntityObject eo = new EntityObject(SystemEntities.NEWS)

		eo.setAttribute('name', 'privacy policy name')
		eo.setAttribute('title', title)
		eo.setAttribute('content', '''Content... <b>Bold</b> <br/> New line....''')
		eo.setAttribute('active', true)
//		eo.setAttribute('confirmationRequired', true)
		eo.setAttribute('privacyPolicy', true)

		nuclosSession.save(eo)
	}

	@Test
	void runTest() {

		setup:
		{
			assert $('#showprivacycontent')
		}

		_05_cookieWarning:
		{
			WebElement we = $('#showprivacycontent')
			assert we

			// we.click()
			assertMessageModalAndConfirm(title, null, {we.click()})

			we = $('#acceptcookies')
			assert we

			we.click()

			assert !$('#showprivacycontent')
			assert !$('#acceptcookies')

			assert logout()
			EntityObjectComponent.refresh()

			assert $('#showprivacycontent') == null
		}

		_10_legalDisclaimersMenu:
		{
			NuclosWebElement we = $('#news')

			assert we
			we.click()

			NuclosWebElement di = $zZz(we, '.dropdown-item')

			assert di
			String text = di.getText()

			assert text.trim() == title
			// di.click()
			assertMessageModalAndConfirm(title, null, {di.click()})

            waitForAngularRequestsToFinish()
			assert login('test', 'test')

			assert $('#news')
			assert !$('#showprivacycontent')
		}

		_15_routes:
		{
            assertLoggedIn('test')
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
			eo.addNew()
			eo.setAttribute('note', 'Note')
			assert eo.dirty

            waitForAngularRequestsToFinish()
            sleep(1000) // for stabilization
			// Both #/news/... and #/disclaimer/... routes should work - #/disclaimer/... only for backward compatibility
			['disclaimer', 'news'].each {
				getUrlHash("/$it/$title")
				assertMessageModalAndConfirm(title, null)
			}

			assert currentUrl.contains('/view/' + TestEntities.EXAMPLE_REST_ORDER)
			assert DetailButtonsComponent.popoverTitle
			assert DetailButtonsComponent.popoverText

			eo.cancel()
		}

		_20_routeViaHyperlinkButton:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

			// Both #/news/... and #/disclaimer/... routes should work - #/disclaimer/... only for backward compatibility
			['disclaimer', 'news'].each {
				eo.addNew()
				eo.setAttribute('text', "Disclaimer Button Test: $it")

				eo.setAttribute('hyperlink', "#/$it/$title")
				eo.save()

				assertMessageModalAndConfirm(title, null, {eo.clickButton('Hyperlink button')})
			}

			assert currentUrl.contains('/view/nuclet_test_other_TestLayoutComponents')
			assert !DetailButtonsComponent.popoverTitle
			assert !DetailButtonsComponent.popoverText
		}
	}

	@Test
	void disclaimerShownAfterConfirmedAndFlagRemoved() {
		logout()
		login('nuclos', '')

		RESTHelper.createUser('testingUser', 'test', ['Example user', 'User management'], nuclosSession)
		EntityObjectComponent.open(SystemEntities.NEWS)
		waitForAngularRequestsToFinish()
		EntityObjectComponent.forDetail().setAttribute('confirmationRequired', true)
		EntityObjectComponent.forDetail().save()

		logout()
		login('testingUser', 'test')

		assertMessageModalAndConfirm(title, null)
		assert isLoggedIn('testingUser'): 'User should accept privacy consent and successfully loggedIn afterwards'

		logout()
		login('nuclos', '')
		assertMessageModalAndConfirm(title, null)

		EntityObjectComponent.open(TestEntities.NUCLOS_USER)
		waitForAngularRequestsToFinish()

		Sidebar.selectEntryByText('testingUser')
		waitForAngularRequestsToFinish()

		EntityObjectComponent.forDetail().setAttribute('privacyConsentAccepted', false)
		EntityObjectComponent.forDetail().save()
		waitForAngularRequestsToFinish()

		logout()
		login('testingUser', 'test')
		assertMessageModalAndConfirm(title, null)

		assert isLoggedIn('testingUser'): 'User should accept privacy consent and successfully loggedIn afterwards'


		logout()
		login('nuclos', '')

		EntityObjectComponent.open(TestEntities.NUCLOS_USER)
		waitForAngularRequestsToFinish()

		Sidebar.selectEntryByText('testingUser')
		waitForAngularRequestsToFinish()


		assert EntityObjectComponent.forDetail().getAttribute('privacyConsentAccepted'): 'Confirming News again should set flag'
	}
}
