package org.nuclos.test.webclient.entityobject

import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.HttpPost
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.RestResponse
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DropdownPerformanceTest extends AbstractRerunnableWebclientTest {

	private final static List<String> choicesForZwa = ['Zwar', 'abgezwackt', 'vierundzwanzig']
	private final static List<String> choicesForKzert = ['Konzert']
	private final static String kzert = 'k*zert'

	@Test
	void runTest() {

		_00_setup:
		{
			InputStream stream = TestDataHelper.exampleText
			String exampleText = IOUtils.toString(stream, 'UTF-8')

			EntityObject word = new EntityObject(TestEntities.EXAMPLE_REST_WORD)

			word.setAttribute('text', 'jweoriwejori')
			word.setAttribute('times', "30000")
			word.setAttribute('exampletext', exampleText)

			nuclosSession.save(word)
			word.executeCustomRule('example.rest.RegisterWords')

			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS)

			eo.addNew()
		}

		_01_searchForDate:
		{
			HttpPost post = new HttpPost(RESTHelper.REST_BASE_URL +
					'/bos/example_rest_Word/query?' +
					'offset=0&' +
					'gettotal=true&' +
					'chunksize=100&' +
					'countTotal=true&' +
					'search=1972&' +
					'withTitleAndInfo=false&' +
					'attributes=text,times,agent,kategorie,parent,date,countletters&' +
					'sort=example_rest_Word_date+asc&' +
					'skipStatesAndGenerations=true'
			)
			enableSqlTimeLogging()
			RestResponse response = RESTHelper.requestResponse(nuclosSession, post)
			setupSqlDefaultLogging()
			// TODO: Normally this should be exactly 1 query
			assert response.sqlCount > 0 && response.sqlCount <= 5

			assert response.sqlTime > 0 && response.sqlTime < 1000
		}

		_05_testLov:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			ListOfValues lov = eo.getLOV('lov')

			lov.inputElement.click()
			inputZ(lov)
			inputAddWa(lov)
			inputKzert(lov)
			lov.close()
		}

		_10_testLovWithVlp:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			ListOfValues lov = eo.getLOV('lovwithvlp')

			lov.inputElement.click()
			inputZ(lov)
			inputAddWa(lov)
			inputKzert(lov)
			lov.close()
		}

		_15_testCombobox:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			ListOfValues lov = eo.getLOV('combobox')

			lov.inputElement.click()
			inputZ(lov)
			inputAddWa(lov)
			inputKzert(lov)
			lov.close()
		}

		_20_testComboboxWithVlp:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			ListOfValues lov = eo.getLOV('comboboxwithvlp')

			lov.inputElement.click()
			inputZ(lov)
			inputAddWa(lov)
			inputKzert(lov)
			lov.close()
		}

		_25_testTextfieldWithVlp:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			ListOfValues lov = eo.getLOV('textwithvlp')
			lov.inputElement.click()
			assert lov.open
			assert lov.choiceElements.size() == 5
			lov.selectEntry('2 two')
			eo.getAttribute('textwithvlp') == '2 two'

			Subform testSubform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS, 'reference')
			SubformRow row = testSubform.newRow()
			testSubform.scrollToColumn('textwithvlp')

			ListOfValues sfLov = row.getLOV('textwithvlp')
			sfLov.open()
			assert sfLov.getChoices().size() == 5
			sfLov.selectEntry('3 three')
			row.getValue('textwithvlp') == '3 three'
		}
	}

	@Test
	void noHoverElementsWhenLayoutIsMissing() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS)

		eo.addNew()

		ListOfValues lov = eo.getLOV('nucbenutzer')
		lov.selectEntry('nuclos')

		lov.hoverLovOverlay(false)
		assert lov.getOpenElement(false) == null
		assert lov.getAddElement(false) == null
		assert lov.getSearchElement(false) == null

		lov = eo.getLOV('lov')

		lov.hoverLovOverlay(false)
		assert lov.getOpenElement(false) == null
		assert lov.getAddElement(false) != null
		assert lov.getSearchElement(false) != null
	}

	private void inputZ(ListOfValues lov) {
		sendKeys('z')
		assert lov.open
		assert lov.choiceElements.size() == 100
	}

	private static void inputAddWa(ListOfValues lov) {
		sendKeys('wa')
		assert lov.choices == choicesForZwa
	}

	private static void inputKzert(ListOfValues lov) {
		lov.inputElement.clear()
		sendKeys(kzert)
		assert lov.choices == choicesForKzert
	}
}
