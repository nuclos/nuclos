package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class FocusTest extends AbstractRerunnableWebclientTest {

	@Before
	void setup() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void defaultInitialFocusOnAddNew() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		eo.addNew()
		def activeElement = driver.switchTo().activeElement()
		assert eo.getAttributeElement('orderNumber').getElement() == activeElement: 'First attribute component (Order number) should get the initial focus'

		def elNote = eo.getAttributeElement('note')
		elNote.focus()
		activeElement = driver.switchTo().activeElement()
		assert eo.getAttributeElement('note').getElement() == activeElement: 'Focus must be changeable after initial focus has been set'
		eo.cancel()
	}

	@Test
	void defaultInitialFocusAfterSave() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		eo.addNew()
		eo.setAttribute('orderNumber', 1)
		eo.setAttribute('note', 'Note')
		eo.save()
		def activeElement = driver.switchTo().activeElement()
		assert eo.getAttributeElement('orderNumber').getElement() == activeElement: 'First attribute component (Order number) should get the initial focus after saving.'
	}

	@Test
	void defaultInitialFocusOnEOSelection() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		failsafeAssert({ return checkCurrentActiveElement("input", ['id': "text-search-input"]); }, "BO quick search should be active and has focus")

		Sidebar.selectEntry(1)
		waitForAngularRequestsToFinish()
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		def activeElement = driver.switchTo().activeElement()
		assert eo.getAttributeElement('orderNumber').getElement() == activeElement: 'First attribute component (Order number) should get the initial focus'

		Sidebar.selectEntry(2)
		waitForAngularRequestsToFinish()
		eo = EntityObjectComponent.forDetail()
		activeElement = driver.switchTo().activeElement()
		assert eo.getAttributeElement('orderNumber').getElement() == activeElement: 'First attribute component (Order number) should get the initial focus'

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		waitForAngularRequestsToFinish()
		failsafeAssert({ return checkCurrentActiveElement("input", ['id': "text-search-input"]); }, "BO quick search should be active and has focus")
	}

	@Test
	void initialFocusSetInPopup() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()


		EntityObjectComponent
				.forDetail()
				.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
				.getReferenceLink(0)
				.click()

		waitForAngularRequestsToFinish()

		assert driver.windowHandles.size() == 2: 'PopUp should have opened'
		switchToOtherWindow(true)

		def activeElement = driver.switchTo().activeElement()
		assert EntityObjectComponent
				.forDetail()
				.getAttributeElement('articleNumber')
				.getElement() == activeElement: 'First attribute component (Article number) should get the initial focus'

		switchToOtherWindow(true)
		closeOtherWindows()
	}

	@Test
	void defaultInitialFocusAfterErrorDialog() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		eo.addNew()
		eo.getAttributeElement('note').focus()
		eo.save()
		assertMessageModalAndConfirm(getLocale() == Locale.GERMANY ? 'Fehler' : 'Error', null)
		def activeElement = driver.switchTo().activeElement()
		assert eo.getAttributeElement('orderNumber').getElement() == activeElement: 'First attribute component (Order number) should get the initial focus after closing error dialog'
		eo.cancel()
	}

	@Test
	void initialFocusOnAddNew() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		eo.addNew()
		eo.setAttribute('nuclosProcess', 'Priority order')

		def activeElement = driver.switchTo().activeElement()
		assert eo.getAttributeElement('discount').getElement() == activeElement: 'Initial focus should be set on "discount"'

		def elNote = eo.getAttributeElement('note')
		elNote.focus()
		activeElement = driver.switchTo().activeElement()
		assert eo.getAttributeElement('note').getElement() == activeElement: 'Focus must be changeable after initial focus has been set'
		eo.cancel()
	}

	@Test
	void initialFocusAfterSave() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		eo.addNew()
		eo.setAttribute('nuclosProcess', 'Priority order')
		eo.setAttribute('orderNumber', 2)
		eo.setAttribute('note', 'Note')
		eo.save()
		def activeElement = driver.switchTo().activeElement()
		assert eo.getAttributeElement('discount').getElement() == activeElement: 'Initial focus should be set on "discount" after saving.'
	}

	@Test
	void subformNewEntryFocusOrder() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		eo.addNew()

		Subform subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		SubformRow newRow = subform.newRow()

		waitForAngularRequestsToFinish()

		def activeElement = driver.switchTo().activeElement()
		assert newRow.getEditor(newRow.getFieldElement("article")).getElement() == activeElement: 'Focus should be in the first column \'article\''

		sendKeysAndWaitForAngular(Keys.TAB)

		activeElement = driver.switchTo().activeElement()
		assert newRow.getEditor(newRow.getFieldElement("price")).getElement() == activeElement: 'Focus should have traversed to the next column \'price\''

		sendShiftTab()
		activeElement = driver.switchTo().activeElement()
		assert newRow.getEditor(newRow.getFieldElement("article")).getElement() == activeElement: 'Focus should be back in the first column \'article\''
	}

}
