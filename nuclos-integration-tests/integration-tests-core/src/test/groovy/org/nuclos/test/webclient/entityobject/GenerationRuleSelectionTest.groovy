package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.GenerationComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class GenerationRuleSelectionTest extends AbstractRerunnableWebclientTest {

	private static final String TEXT = 'text'

	@Test
	void _01_generationRuleSelectionTest() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		eo.addNew()
		eo.setAttribute('text', 'GenerationRuleSelectionTest')
		// select subform entries
		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		subform.newRow().enterValue(TEXT, 'subentry1')
		subform.newRow().enterValue(TEXT, 'subentry2')
		subform.newRow().enterValue(TEXT, 'subentry3')
		subform.toggleSelection(0)

		Subform subsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform')
		subsubform.newRow().enterValue(TEXT, 'subsubentry1')
		subsubform.newRow().enterValue(TEXT, 'subsubentry2')
		subsubform.newRow().enterValue(TEXT, 'subsubentry3')
		subsubform.toggleSelection(1)

		Subform subsubsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM, 'subsubform')
		subsubsubform.newRow().enterValue(TEXT, 'subsubsubentry1')
		subsubsubform.newRow().enterValue(TEXT, 'subsubsubentry2')
		subsubsubform.newRow().enterValue(TEXT, 'subsubsubentry3')
		subsubsubform.toggleSelection(2)

		eo.save()

		subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		subsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform')
		subsubsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM, 'subsubform')

		subform.unselectAllRows()
		subsubform.unselectAllRows()
		subsubsubform.unselectAllRows()

		subform.toggleSelection(2)
		subsubform.toggleSelection(1)
		subsubsubform.toggleSelection(0)

		// NUCLOS-10470
		assert GenerationComponent.generateObjectAndConfirm('GenerationRuleSelectionTest', false, "nuc-detail") == 1
		assert driver.getWindowHandles().size() == 2
		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()
		assert oldWindow != newWindow

		subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		assert subform.rowCount == 1
		assert subform.getColumnValues(TEXT)[0] == 'subentry3'
		subform.toggleSelection(0)
		subsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform')
		assert subsubform.rowCount == 1
		assert subsubform.getColumnValues(TEXT)[0] == 'subsubentry2'
		subsubform.toggleSelection(0)
		subsubsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM, 'subsubform')
		assert subsubsubform.rowCount == 1
		assert subsubsubform.getColumnValues(TEXT)[0] == 'subsubsubentry1'
		subsubsubform.toggleSelection(0)

		// NUCLOS-10533
		assert GenerationComponent.generationListingsName() == "el General"
	}

}
