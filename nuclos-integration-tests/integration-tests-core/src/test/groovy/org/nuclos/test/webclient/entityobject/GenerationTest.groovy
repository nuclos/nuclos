package org.nuclos.test.webclient.entityobject

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.HarEntry
import net.lightbody.bmp.proxy.CaptureType

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class GenerationTest extends AbstractRerunnableWebclientTest {

	final long COUNT_GENERATORS = 12

	Map<String, ?> eoData = [
			name              : 'test',
			mandatorytext     : 'test',
			exceptionbeiinsert: false,
			document          : TestDataHelper.nuclosDocxFile,
			image             : TestDataHelper.nuclosPngFile,
	]

	Map<String, ?> eoData2 = [
			name              : 'test2',
			mandatorytext     : 'test2',
			exceptionbeiinsert: false
	]

	Map<String, ?> eoData3 = [
			name              : 'block Popup',
			mandatorytext     : 'block Popup',
			exceptionbeiinsert: false
	]

	Map<String, ?> eoData4 = [
			name              : 'block Popup2',
			mandatorytext     : 'block Popup2',
			exceptionbeiinsert: false
	]

	@Test
	void runTest_01_generateEO() {
		LocaleChooser.locale = Locale.ENGLISH
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData)

		String oldWindow

		generateEo:
		{
			screenshot('before-calling-generator')
			countBrowserRequests {
				int generatorCount = GenerationComponent.generateObjectAndConfirm('Test Objektgenerator')
				assert generatorCount == COUNT_GENERATORS

				GenerationComponent.openResult()

				assert driver.getWindowHandles().size() == 2

				oldWindow = driver.windowHandle
				String newWindow = switchToOtherWindow()

				assert oldWindow != newWindow
				// TODO: Download files, check content

				driver.close()
				driver.switchTo().window(oldWindow)
				refresh()
			}.with {
				// There should be no update, because the EO is not dirty
				assert it.getRequestCount(RequestType.EO_UPDATE) == 0

				assert it.getRequestCount(RequestType.EO_GENERATION) == 1

				assert it.getRequestCount(RequestType.EO_READ) == 2
				assert it.getRequestCount(RequestType.EO_READ_LIST_EMPTY) == 2
			}
			screenshot('after-calling-generator')

		}

		checkGeneratedObject:
		{
			Sidebar.selectEntryByText("test test test")
			eo = EntityObjectComponent.forDetail()

			assert eo.getAttribute('name') == 'test test test'
			assert eo.getAttribute("valuefrominsertbefore") == 'before'
			assert eo.getAttribute("valuefrominsertafter") == 'after'
			assert eo.getAttribute("valuefromgenrulebefore") == 'before'
			assert eo.getAttribute("valuefromgenruleafter") == 'after'
			assert eo.getFileComponent('document').text == TestDataHelper.nuclosDocxFile.name
			assert !eo.getImageComponent('image').emptyImage()
		}

		deleteFileInSourceEo:
		{
			Sidebar.selectEntryByText("test test")

			eo = EntityObjectComponent.forDetail()
			eo.getFileComponent('document').clearFile()

			eo.getImageComponent('image').executeContextAction('Remove image')

			eo.save()
		}

		/**
		 * Files should still be available in the generated EO.
		 */
		checkFileInGeneratedEo:
		{
			Sidebar.selectEntryByText("test test test")

			eo = EntityObjectComponent.forDetail()

			eo.refresh()
			assert !eo.dirty
			assert Sidebar.listEntryCount == 2

			assert eo.getFileComponent('document').text == TestDataHelper.nuclosDocxFile.name
			assert !eo.getImageComponent('image').emptyImage()
		}

		screenshot('generate-1-window-1')
	}

	@Test
	void runTest_07_staleVersion() {
		LocaleChooser.locale = Locale.ENGLISH

		EntityObjectComponent eoComponent = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData)
		Long eoId = eoComponent.id

		EntityObject<Long> eo = nuclosSession.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoId)
		EntityObject<Long> eo2 = nuclosSession.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoId)

		eo.setAttribute('name', eo.getAttribute('name').toString() + ' 2')
		eo.save()

		checkRestStatusCode:
		{
			eo2.setAttribute('name', '...')
			expectErrorStatus(Response.Status.CONFLICT) {
				eo2.save()
			}
		}

		checkConflictInWebclient:
		{
			eoComponent = EntityObjectComponent.forDetail()

			// The many random errors indicate that the following click was not fulfilled, and no error is thrown. This is just a trial ...
			sleep(1000)

			eoComponent.clickButton('Objekt generieren')
			def modal = waitForNotNull(30, { messageModal }, false)
			if (modal == null) {
				// click not successfully, try again
				eoComponent.clickButton('Objekt generieren')
				modal = waitForNotNull(30, { messageModal })
			}
			modal.confirm()

			assert waitForNotNull(30, {
				getMessageModal('nuc-dialog', true)
			}).message.toLowerCase().contains('version')
		}
	}

	@Test
	void runTest_10_generateEOWithException() {

		LocaleChooser.locale = Locale.ENGLISH
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData)

		screenshot('with-exception-before-update')
		eo.setAttribute('exceptionbeiinsert', true)
		eo.save()

		screenshot('with-exception-before-calling-generator-2')
		int generatorCount = GenerationComponent.generateObjectAndConfirm('Test Objektgenerator')

		assert generatorCount == COUNT_GENERATORS
		screenshot('with-exception-after-calling-generator-2')

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		// give the browser some time to load the page before waitingForAngular etc.
		sleep(1000)
		assert eo.alertText == 'Test Exception'

		// close alert dialog
		eo.clickButtonOk()
		sendKeys(Keys.ESCAPE)


		driver.close()
		driver.switchTo().window(oldWindow)
		eo.setAttribute('exceptionbeiinsert', false)
		eo.save()

		// Workaround für einen noch nicht gelösten Bug.
		// Der modale Dialog zum Bestätigen des Objektgenerators entfernt das BO aus der URL.
		// Was den Webclient in Folge dessen beim "refresh" auf die letzte Redirect URL springen lässt, welche auf das temporäre Objekt des OG zeigt.
		eo.addNew()
		eo.cancel()

		refresh()
		assert Sidebar.listEntryCount == 1

		screenshot('with-exception-generate-1-window-2-after-save')
	}

	@Test
	void runTest_15_generateEOViaButton() {
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData2)

		eo.checkButton('Intern generieren', true, true)

		// The many random errors indicate that the following click was not fulfilled, and no error is thrown. This is just a trial ...
		sleep(1000)

		eo.clickButton('Objekt generieren')
		waitForNotNull(30, {
			messageModal
		}).confirm()

		GenerationComponent.openResult()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert !eo.dirty

		driver.close()
		driver.switchTo().window(oldWindow)
	}

	@Test
	void runTest_17_generateEOViaButtonWithSave() {
		generateEo:
		{
			EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData2)
			waitForAngularRequestsToFinish()

			eo.setAttribute('name', 'test 2')

			assert eo.dirty

			countBrowserRequests {

				// The many random errors indicate that the following click was not fulfilled, and no error is thrown. This is just a trial ...
				sleep(1000)

				eo.clickButton('Objekt generieren')
				waitForNotNull(30, {
					messageModal
				}).confirm()
				GenerationComponent.openResult()

				assert driver.getWindowHandles().size() == 2

				String oldWindow = driver.windowHandle
				String newWindow = switchToOtherWindow()

				assert oldWindow != newWindow

				assert !eo.dirty

				driver.close()
				driver.switchTo().window(oldWindow)
			}.with {
				// 1 request to save the dirty EO
				assert it.getRequestCount(RequestType.EO_UPDATE) == 1

				assert it.getRequestCount(RequestType.EO_GENERATION) == 1

				// 1 request to read the generated EO
				// + 1 for an empty list to set the right "canCreateBo" flag
				assert it.getRequestCount(RequestType.EO_READ) == 1
				assert it.getRequestCount(RequestType.EO_READ_LIST_EMPTY) == 1
			}

			assert !eo.dirty
		}
	}

	@Test
	void runTest_20_25_30() {
		LocaleChooser.locale = Locale.ENGLISH

		_20_generateDependent:
		{
			EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData2)

			Subform subform = waitForNotNull(30, {eo.getSubform('nuclet_test_other_TestObjektgeneratorDependent_testobjektgen')})
			assert subform.rowCount == 0
			eo.hasTab('Dependents (0)', true)

			// The many random errors indicate that the following click was not fulfilled, and no error is thrown. This is just a trial ...
			sleep(1000)

			assertMessageModalAndConfirm('generate Test Objektgenerator Dependent', '', {eo.clickButton('Dependent generieren')})

			subform = waitForNotNull(30, {eo.getSubform('nuclet_test_other_TestObjektgeneratorDependent_testobjektgen')})
			assert subform.rowCount == 1
			eo.hasTab('Dependents (1)', true)
		}

		_25_closeOnException:
		{
			sleep(1000)
			assertMessageModalAndConfirm('Error', 'Exception from GenerateRule', {
				GenerationComponent.generateObjectAndConfirm('Generate with close on exception')
				waitForAngularRequestsToFinish()
			})
		}

		_30_generateWithDependentException:
		{
			assertMessageModalAndConfirm('Error', 'Mandatory Text', {
				GenerationComponent.generateObjectAndConfirm('Test Objektgenerator With Incomplete Dependents')
				waitForAngularRequestsToFinish()
			})
		}
	}

	@Test
	void runTest_35_generateEOWithMandatoryNull() {
		LocaleChooser.locale = Locale.ENGLISH

		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData2)

		eo.setAttribute('clearmandatorytextduringgen', true)
		eo.save()

		int generatorCount = GenerationComponent.generateObjectAndConfirm('Test Objektgenerator')

		assert generatorCount == COUNT_GENERATORS

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert eo.dirty
		waitUntilTrue {
			eo.alertText == 'Validation errors occurred.\nThe field "Mandatory Text" must not be empty'
		}

		eo.clickButtonOk()

		assert eo.getAttribute("valuefrominsertbefore") == 'before'
		assert eo.getAttribute("valuefrominsertafter") == ''
		assert eo.getAttribute("valuefromgenrulebefore") == 'before'
		assert eo.getAttribute("valuefromgenruleafter") == ''

		eo.setAttribute("mandatorytext", "not empty")
		eo.save()

		eo.refresh()

		waitUntilTrue({Sidebar.listEntryCount == 2}, 90)
		assert !eo.dirty

		driver.close()
		driver.switchTo().window(oldWindow)
	}

	@Test
	void runTest_40_generateWithoutSaving() {
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData2)

		eo.addNew()
		eo.setAttribute('name', 'test 40')
		eo.setAttribute('mandatorytext', 'mandatory text')
		eo.save()

		int generatorCount = GenerationComponent.generateObjectAndConfirm('Test objectgenerator without saving after generation')

		assert generatorCount == COUNT_GENERATORS

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert eo.dirty
		waitUntilTrue {
			eo.getAlertText(true) == null
		}

		assert eo.getAttribute("valuefrominsertbefore") == ''
		assert eo.getAttribute("valuefrominsertafter") == ''
		assert eo.getAttribute("valuefromgenrulebefore") == 'before'
		assert eo.getAttribute("valuefromgenruleafter") == ''

		assert eo.getAttribute("mandatorytext") == 'mandatory text'
		assert eo.getLOV("werteliste").getChoices().contains("mandatory text")

		eo.save()

		assert eo.dirty
		def alertText = waitForNotNull(10, { eo.alertText }, false)
		if (alertText == null) {
			// click not successfully, try again
			eo.save()
		}
		waitUntilTrue {
			eo.alertText == 'Test Exception'
		}
		eo.clickButtonOk()

		driver.close()
		driver.switchTo().window(oldWindow)
	}

	@Test
	void runTest_45_generateInOverlay() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		eo.addNew()
		eo.setAttribute('name', 'test 45')
		eo.setAttribute('mandatorytext', 'mandatory text 45')
		eo.save()

		GenerationComponent.generateObjectAndConfirm('Generate dependent and show in Overlay')

		assert driver.getWindowHandles().size() == 1

		editGeneratedObjectInModal:
		{
			EntityObjectModal eoModal = EntityObjectComponent.forModal()
			assert eoModal.getAttribute('mandatorytext') == 'mandatory text 45'
			eoModal.setAttribute('name', 'changed-in-modal')
			eoModal.save()
			eoModal.clickButtonClose()
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 1
		assert subform.getRow(0).getValue('name') == 'changed-in-modal'
	}

	@Test
	void runTest_46_generateInOverlayAndDelete() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		eo.addNew()
		eo.setAttribute('name', 'test 46')
		eo.setAttribute('mandatorytext', 'mandatory text 46')
		eo.save()

		editGeneratedObjectInModal:
		{
			EntityObjectModal eoModal = EntityObjectComponent.forModal({
				GenerationComponent.generateObjectAndConfirm('Generate dependent and show in Overlay')
				assert driver.getWindowHandles().size() == 1
			})
			assert eoModal.getAttribute('mandatorytext') == 'mandatory text 46'
			eoModal.delete()
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 0
	}

	@Test
	void runTest_47_generateInOverlayCloseDirtyAndDelete() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		eo.addNew()
		eo.setAttribute('name', 'test 46')
		eo.setAttribute('mandatorytext', 'mandatory text 46')
		eo.save()

		editGeneratedObjectInModal:
		{
			EntityObjectModal eoModal = EntityObjectComponent.forModal({
				GenerationComponent.generateObjectAndConfirm('Generate dependent and show in Overlay')
				assert driver.getWindowHandles().size() == 1
			})
			assert eoModal.getAttribute('mandatorytext') == 'mandatory text 46'
			eoModal.setAttribute('mandatorytext', 'mandatory text edit')

			EntityObjectModal.clickButtonClose()

			// check that clicking close while dirty opens the popover
			NuclosWebElement popover = $('.popover')
			assert popover != null: 'Popover should exist'
			assert popover.isDisplayed(): 'Popover should be displayed'
			// check that the modal hasn't been closed already
			assert eoModal.isFieldVisible('mandatorytext')

			eoModal.delete()

			// check that clicking delete also closed the popover and the modal
			assert !eoModal.isFieldVisible('mandatorytext')
			assert !popover.isDisplayed(): 'Popover should no longer be displayed'
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 0
	}

	@Test
	void runTest_48_generateInOverlayCloseDirtyAndSave() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		eo.addNew()
		eo.setAttribute('name', 'test 46')
		eo.setAttribute('mandatorytext', 'mandatory text 46')
		eo.save()

		editGeneratedObjectInModal:
		{
			EntityObjectModal eoModal = EntityObjectComponent.forModal({
				GenerationComponent.generateObjectAndConfirm('Generate dependent and show in Overlay')
				assert driver.getWindowHandles().size() == 1
			})
			assert eoModal.getAttribute('mandatorytext') == 'mandatory text 46'
			eoModal.setAttribute('mandatorytext', 'mandatory text edit')

			EntityObjectModal.clickButtonClose()

			// check that clicking close while dirty opens the popover
			NuclosWebElement popover = $('.popover')
			assert popover != null: 'Popover should exist'
			assert popover.isDisplayed(): 'Popover should be displayed'
			// check that the modal hasn't been closed already
			assert eoModal.isFieldVisible('mandatorytext')

			eoModal.save()

			// check that clicking delete also closed the popover and the modal
			assert !eoModal.isFieldVisible('mandatorytext')
			assert !popover.isDisplayed(): 'Popover should no longer be displayed'
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 1
	}

	@Test
	void runTest_49_generateInOverlayCloseDirtyAndDiscardChanges() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		eo.addNew()
		eo.setAttribute('name', 'test 46')
		eo.setAttribute('mandatorytext', 'mandatory text 46')
		eo.save()

		editGeneratedObjectInModal:
		{
			EntityObjectModal eoModal = EntityObjectComponent.forModal({
				GenerationComponent.generateObjectAndConfirm('Generate dependent and show in Overlay')
				assert driver.getWindowHandles().size() == 1
			})
			assert eoModal.getAttribute('mandatorytext') == 'mandatory text 46'
			eoModal.setAttribute('mandatorytext', 'mandatory text edit')

			EntityObjectModal.clickButtonClose()

			// check that clicking close while dirty opens the popover
			NuclosWebElement popover = $('.popover')
			assert popover != null: 'Popover should exist'
			assert popover.isDisplayed(): 'Popover should be displayed'
			// check that the modal hasn't been closed already
			assert eoModal.isFieldVisible('mandatorytext')

			eoModal.cancel()

			// check that clicking delete also closed the popover and the modal
			assert !eoModal.isFieldVisible('mandatorytext')
			assert !popover.isDisplayed(): 'Popover should no longer be displayed'
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 1
	}

	@Test
	void runTest_50_generateInOverlayAndCompleteInput() {
		LocaleChooser.locale = Locale.ENGLISH
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		eo.addNew()
		eo.setAttribute('name', 'test 50')
		eo.setAttribute('mandatorytext', 'mandatory text 50')
		eo.save()

		GenerationComponent.generateObjectAndConfirm('Generate incomplete dependent and show in Overlay')

		assert driver.getWindowHandles().size() == 1

		editGeneratedObjectInModal:
		{
			EntityObjectModal eoModal = EntityObjectComponent.forModal()
			assert eoModal.alertText == 'Validation errors occurred.\n' +
					'The field "Mandatory Text" must not be empty'
			assert !eoModal.dirty
			eoModal.clickButtonOk()

			eoModal.setAttribute('mandatorytext', 'input-in-modal')
			eoModal.save()
			eoModal.clickButtonClose()
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 1
		assert subform.getRow(0).getValue('mandatorytext') == 'input-in-modal'
	}

	@Test
	void runTest_55_generateInDialog() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		eo.addNew()
		eo.setAttribute('name', 'test 55')
		eo.setAttribute('mandatorytext', 'mandatory text 55')
		eo.save()

		GenerationComponent.generateObjectAndConfirm('Generate dependent and show in Dialog', false)

		assert driver.getWindowHandles().size() == 1

		editGeneratedObjectInDialog:
		{
			EntityObjectDialog eoDialog = EntityObjectComponent.forDialog()
			waitUntilTrue {
				eoDialog.getAttribute('mandatorytext') == 'mandatory text 55'
			}
			eoDialog.setAttribute('name', 'changed-in-dialog')
			eoDialog.clickButtonOk()
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 1
		assert subform.getRow(0).getValue('name') == 'changed-in-dialog'
	}

	/**
	 * NUCLOS-8452
	 */
	@Test
	void runTest_56_generateInDialogAndDoNotClose() {
		LocaleChooser.locale = Locale.ENGLISH

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		eo.addNew()
		eo.setAttribute('name', 'test 56')
		eo.setAttribute('mandatorytext', 'mandatory text 56')
		eo.save()

		GenerationComponent.generateObjectAndConfirm('Generate dependent and show in Dialog with SAVE', false)

		assert driver.getWindowHandles().size() == 1
		waitUntilTrue {
			EntityObjectComponent.forDialog().getButton('Custom-Rule no BEX') != null
		}
		EntityObjectDialog eoDialog = EntityObjectComponent.forDialog()

		testDoNotCloseDialog:
		{
			eoDialog.clickButton('Custom-Rule no BEX')
			assert eoDialog.getAttribute('mandatorytext') == 'mandatory text 56, rule-rulez'
			eoDialog.clickButton('Custom-Rule with BEX')
			assert eoDialog.alertText == 'BEX from Rule'
			eoDialog.setAttribute('mandatorytext', '')
			eoDialog.clickButtonOk()
		}

		testCloseOnlyOnOk:
		{
			assert eoDialog.alertText == 'Validation errors occurred.\n' +
					'The field "Mandatory Text" must not be empty'
			eoDialog.setAttribute('mandatorytext', 'Value after first OK')
			// hier testen wir, dass nach einem Schließ Versuchs mittels OK, auch weiterhin eine Custom-Rule den Dialog nicht schließt:
			eoDialog.clickButton('Custom-Rule no BEX')
			eoDialog.clickButtonOk()
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 1
		assert subform.getRow(0).getValue('mandatorytext') == 'Value after first OK, rule-rulez'
	}

	@Test
	void runTest_60_generateInDialogAndCompleteInput() {
		LocaleChooser.locale = Locale.ENGLISH

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		eo.addNew()
		eo.setAttribute('name', 'test 60')
		eo.setAttribute('mandatorytext', 'mandatory text 60')
		eo.save()

		GenerationComponent.generateObjectAndConfirm('Generate incomplete dependent and show in Dialog', false)

		assert driver.getWindowHandles().size() == 1

		editGeneratedObjectInDialog:
		{
			EntityObjectDialog eoDialog = EntityObjectComponent.forDialog()
			assert eoDialog.alertText == 'Validation errors occurred.\n' +
					'The field "Mandatory Text" must not be empty'

			String nameBeforeClear = eoDialog.getAttribute('name')
			eoDialog.setAttribute('name', '')
			eoDialog.setAttribute('mandatorytext', 'input-in-dialog')
			eoDialog.clickButtonOk()
			assert eoDialog.alertText == 'Validation errors occurred.\n' +
					'The field "Name" must not be empty'

			eoDialog.setAttribute('name', nameBeforeClear)
			eoDialog.clickButtonOk()
		}

		assert !eo.dirty

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT, 'testobjektgen')
		assert subform.rowCount == 1
		assert subform.getRow(0).getValue('mandatorytext') == 'input-in-dialog'
	}

	@Test
	void runTest_70_cloneWithGeneratorWithInputRequiredException() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		eo.addNew()
		eo.setAttribute('name', 'test 70')
		eo.setAttribute('mandatorytext', 'mandatory text 60')
		eo.save()
		eo.clone()

		waitForAngularRequestsToFinish()
		assert $('#input-required-dialog #specification-message').text == 'Really generate?': 'Input Required text should be displayed'
		$('#input-required-dialog #button-yes').click()

		waitForAngularRequestsToFinish()
		eo.setAttribute('name', 'test 70 clone')
		eo.save()
		assert !eo.dirty
	}

		@Test
	void runNuclosProcessAction_NUCLOS_9067() {
		TestDataHelper.insertTestData(nuclosSession)

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		eo.setAttribute('nuclosProcess', 'Priority order')
		GenerationComponent.generateObjectAndConfirm('Order Clone')
		waitForAngularRequestsToFinish()

		assert driver.getWindowHandles().size() == 2
		switchToOtherWindow()

		assert EntityObjectComponent.forDetail().getAttribute('orderNumber') == '10020159'
		assert EntityObjectComponent.forDetail().getAttribute('nuclosProcess') == 'Priority order'
	}

	@Test
	void runNuclosModalRulesTest_NUCLOS_9307() {
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData)

		eo.clickButton('Objektgenerator With Client Rules')
		waitForNotNull(30, {
			getMessageModal('nuc-detail-modal')
		})

		EntityObjectComponent eoModal = EntityObjectComponent.forModal()
		assert eoModal.getAttribute('textvaluechanged') == '': 'textvaluechanged should be empty'
		assert eoModal.getAttribute('clear1') == 'Clear me': 'clear field should be filled initial'

		eoModal.setAttribute('textvaluechanged', 'test')
		assert eoModal.getAttribute('clear1') == '': 'Rules should have been applied and field cleared'

		assert eoModal.getAttribute('clear2') == 'Clear me': 'clear2 field should be filled initial'
		assert eoModal.getAttribute('transfervalue2') == '': 'transfervalue2 should be empty'
		eoModal.getLOV('refvaluechanged').selectEntry('nuclos')
		waitForAngularRequestsToFinish()

		assert eoModal.getAttribute('clear2') == '': 'clear2 field should be empty'
		assert eoModal.getAttribute('transfervalue2') == 'nuclos': 'transfervalue2 should be selected ref'

		getMessageModal('nuc-detail-modal').decline()

		DetailButtonsComponent.checkPopover(true)
		eoModal.cancel()

		waitFor(getDefaultWaitDriverTimeout() + 10, {
			!getMessageModal('nuc-detail-modal')
		})
	}

	@Test
	void runGenerationWithParameterObject() {
		TestDataHelper.insertTestData(nuclosSession);

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		GenerationComponent.generateObjectAndConfirm("Test Generator With Parameter Entity")
		waitUntilTrue({GenerationComponent.countParameterObjects() == 2})

		assertMessageModalAndConfirm('Error', 'Es sind Validierungsfehler aufgetreten.', {
			GenerationComponent.selectParameterObject(1)

			waitForAngularRequestsToFinish()

			assert driver.getWindowHandles().size() == 2
			switchToOtherWindow()
		})
		assert EntityObjectComponent.forDetail().getAttribute('referencewithoutpopup') == 'Test-Customer'
	}

	@Test
	void runGenerationWithParameterObjectWithOutVLP() {
		TestDataHelper.insertTestData(nuclosSession);

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		GenerationComponent.generateObjectAndConfirm("Test Generator With Parameter Entity Wo VLP")
		waitUntilTrue({GenerationComponent.countParameterObjects() == 3})

		assertMessageModalAndConfirm('Error', 'Es sind Validierungsfehler aufgetreten.', {
			GenerationComponent.selectParameterObject(1)

			waitForAngularRequestsToFinish()

			assert driver.getWindowHandles().size() == 2
			switchToOtherWindow()
		})
		assert EntityObjectComponent.forDetail().getAttribute('referencewithoutpopup') == 'Test-Customer inactive'
	}

	@Test
	void runGenerationWithParameterObjectCancel() {
		TestDataHelper.insertTestData(nuclosSession);

		cancel:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
			GenerationComponent.generateObjectAndConfirm("Test Generator With Parameter Entity")
			waitUntilTrue({GenerationComponent.countParameterObjects() == 2})
			GenerationComponent.cancelParameterObjectSelection()

			waitForAngularRequestsToFinish()

			assert driver.getWindowHandles().size() == 1
		}

		close:
		{
			GenerationComponent.generateObjectAndConfirm("Test Generator With Parameter Entity Wo VLP")
			waitUntilTrue({GenerationComponent.countParameterObjects() == 3})
			GenerationComponent.closeParameterObjectSelection()

			waitForAngularRequestsToFinish()

			assert driver.getWindowHandles().size() == 1
		}
	}

	@Test
	void generationWorksWithVirtualBO() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORLD)
		waitForAngularRequestsToFinish()

		def button = $('button[name="attribute-Button_YQtc"]')

		assert button != null: 'Button should be visible'
		assert button.isEnabled(): 'Button should be clickable'

		button.click()
		waitForAngularRequestsToFinish()

		def messageModal = getMessageModal('nuc-detail-dialog')
		if (messageModal == null) {
			// click not successful, try again
			button.click()
			messageModal = getMessageModal('nuc-detail-dialog')
		}
		assert messageModal != null: 'Object Generator should open'
		messageModal.cancel()
	}

	@Test
	void generateAddressTextCheckImage() {
		TestDataHelper.insertTestData(nuclosSession);
		logout()
		login('nuclos')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		waitForAngularRequestsToFinish()

		getBrowserHar([CaptureType.REQUEST_HEADERS, CaptureType.RESPONSE_HEADERS, CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT], {
			GenerationComponent.generateObjectAndConfirm("Add AddressText", false, "nuc-detail-dialog")
			waitForAngularRequestsToFinish()

			//dialog opens check pencil is visible
			assert $("nuc-web-image > div > img") != null: 'Image should be visible'
		}).with {
			it.log.entries.forEach({ HarEntry entry ->
				if (entry.request.method == "GET" &&
						entry.request.url
								.matches('.*rest/boImages/example_rest_AddressDialog/(.*)/images/example_rest_AddressDialog_warnicon/(.*)')){
				    assert entry.response.status == 200: 'Picture loaded with error [ ' + entry.response.status + ' ]: ' + entry.response.content.text
				}
			})
		}
		sendKeys(Keys.ESCAPE)
	}

	@Test
	void generateMessageContextWorks() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		waitForAngularRequestsToFinish()
		EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()
		eo.setAttribute('articleNumber', 35434)
		eo.setAttribute('name', 'Testing something')
		eo.setAttribute('price', 12)
		eo.save()

		GenerationComponent.generateObjectAndConfirm("Creates order and navigates to it", false)
		waitForAngularRequestsToFinish()

		assert driver.windowHandles.size() == 2: 'There should be a new tab'
		switchToOtherWindow()

		assert driver.currentUrl.contains('example_rest_Order'): 'Order page should be open'
		def orderNumber = EntityObjectComponent.forDetail().getAttribute('orderNumber')
		assert orderNumber == '1200': 'Correct order number should be opened: ' + orderNumber
		closeOtherWindows()
	}

	@Test
	void runTest_18_generateEOViaButtonWithActivePopupBlockerAndCloseWithoutOpening() {
		generateEo:
		{
			EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData3)
			waitForAngularRequestsToFinish()

			eo.setAttribute('name', 'blocked Popup')

			assert eo.dirty

			// simulate a popup blocker by overriding window.open with an empty function
			driver.executeScript("window.open = function() { };")

			countBrowserRequests {

				// The many random errors indicate that the following click was not fulfilled, and no error is thrown. This is just a trial ...
				sleep(1000)

				eo.clickButton('Objekt generieren')
				waitForNotNull(30, {
					messageModal
				}).confirm()

				assert $('.modal-body a') != null: 'There should be an hyperlink inside the popup blocked notification dialog'
				assert $('.modal-body a').text == 'Hier klicken um Test Objektgenerator zu öffnen'

				GenerationComponent.clickFooterButtonClose()

				assert driver.getWindowHandles().size() == 1

			}.with {
				// 1 request to save the dirty EO
				assert it.getRequestCount(RequestType.EO_UPDATE) == 1
				assert it.getRequestCount(RequestType.EO_GENERATION) == 1
			}

			assert !eo.dirty
		}
	}

	@Test
	void runTest_19_generateEOViaButtonWithActivePopupBlockerAndOpen() {
		generateEo:
		{
			EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData4)
			waitForAngularRequestsToFinish()

			eo.setAttribute('name', 'blocked Popup2')

			assert eo.dirty

			// simulate a popup blocker by overriding window.open with an empty function
			driver.executeScript("window.open = function() { };")

			countBrowserRequests {

				// The many random errors indicate that the following click was not fulfilled, and no error is thrown. This is just a trial ...
				sleep(1000)

				eo.clickButton('Objekt generieren')
				waitForNotNull(30, {
					messageModal
				}).confirm()

				assert $('.modal-body a') != null: 'There should be an hyperlink inside the popup blocked notification dialog'
				assert $('.modal-body a').text == 'Hier klicken um Test Objektgenerator zu öffnen'

				$('.modal-body a').click()

				assert driver.getWindowHandles().size() == 2

				String oldWindow = driver.windowHandle
				String newWindow = switchToOtherWindow()

				assert oldWindow != newWindow

				assert !eo.dirty

				driver.close()
				driver.switchTo().window(oldWindow)
			}.with {
				// 1 request to save the dirty EO
				assert it.getRequestCount(RequestType.EO_UPDATE) == 1

				assert it.getRequestCount(RequestType.EO_GENERATION) == 1

				// 1 request to read the generated EO
				// + 1 for an empty list to set the right "canCreateBo" flag
				assert it.getRequestCount(RequestType.EO_READ) == 1
				assert it.getRequestCount(RequestType.EO_READ_LIST_EMPTY) == 1
			}

			assert !eo.dirty
		}
	}

	@Test
	void generateWithoutSaveTemporaryElementsInLocalStorageCleanup() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()
		eo.setAttribute('name', 'TestingGen_Temp')
		eo.setAttribute('mandatorytext', 'Some mandatory text')
		eo.save()

		GenerationComponent.generateObjectAndConfirm('Test objectgenerator without saving after generation')
		assert driver.windowHandles.size() == 2: 'Another tab with generated object should be opened'
		driver.switchTo().window(driver.windowHandles[0]) // make sure we are still in first tab
		GenerationComponent.generateObjectAndConfirm('Test objectgenerator without saving after generation')
		assert driver.windowHandles.size() == 3: 'Another tab with generated object should be opened'

		driver.switchTo().window(driver.windowHandles[1]) // go to first tab

		EntityObjectComponent tempId = EntityObjectComponent.forDetail()
		assert tempId.getAttribute('name') != '': 'There should be generated entity'
		tempId.cancel()

		driver.switchTo().window(driver.windowHandles[2]) // go to second tab

		EntityObjectComponent tempId2 = EntityObjectComponent.forDetail()
		assert tempId2.getAttribute('name') != '': 'There should be generated entity'
		tempId2.setAttribute('exceptionbeiinsert', false)
		tempId2.save()

		driver.switchTo().window(driver.windowHandles[0]) // go to second tab
		closeOtherWindows()
	}

	@Test
	void generateSaveRuleShouldUpdateSidebar() {
		TestDataHelper.insertTestData(nuclosSession)
		RESTHelper.createUser('testCollectiveProcessing', 'test', ['Example user', 'Example user Collective Processing'], nuclosSession)

		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		$('#toolbar > div > button:nth-child(1)')?.click()

		Sidebar.addColumn(TestEntities.EXAMPLE_REST_ORDER.fqn, 'note')
		waitForAngularRequestsToFinish()

		GenerationComponent.generateObjectAndConfirm('Updates note information', true, 'nuc-detail-dialog')
		waitForAngularRequestsToFinish()

		assert EntityObjectComponent.forDetail().getAttribute('note') == "6 Positions on order": "Note should be updated in EO view"
		assert Sidebar.getValueByColumnLabel(0, 'Note') == "6 Positions on order": "Note should be updated in Sidebar"

		Sidebar.selectEntry(1)
		Sidebar.toggleMultiSelection()
		waitForAngularRequestsToFinish()
		Sidebar.selectEntry(1)

		sendKeysAndWaitForAngular(Keys.SPACE) //Space hit
		assert CollectiveProgressingComponent.isCollectiveProcessing()

		CollectiveProgressingComponent.doFunction('Updates note information')
		waitForAngularRequestsToFinish()

		getMessageModal('nuc-detail-dialog').confirm()
		waitForAngularRequestsToFinish()
		assert Sidebar.getValueByColumnLabel(1, 'Note') == "6 Positions on order": "Note should be updated in Sidebar"

		CollectiveProgressingComponent.closeProgress()
		CollectiveProgressingComponent.showCollectiveProcessing()
		CollectiveProgressingComponent.cancelCollectiveProcessing()
		Sidebar.toggleMultiSelection()
	}

	@Test
	void testGenerateDependentInPopupWithStateChange() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)

		EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()
		eo.setAttribute('name', 'TestingGen_Temp')
		eo.setAttribute('mandatorytext', 'Some mandatory text')
		eo.save()

		eo.clickButton('Dependent generieren in Popup')

		switchToOtherWindow(true)
		EntityObjectComponent eoDep = EntityObjectComponent.forDetail()
		eoDep.setAttribute('name', 'testGenerateDependentInPopupWithStateChange')
		eoDep.save()

		switchToOtherWindow(true)
		eo = EntityObjectComponent.forDetail()
		Subform sfDep = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT.fqn + '_testobjektgen')

		SubformRow row0 = sfDep.getRow(0)
		assert row0.getValue('name') == 'testGenerateDependentInPopupWithStateChange'
		assert row0.getValue('nuclosState') == 'Status 10'

		switchToOtherWindow(true)
		eoDep = EntityObjectComponent.forDetail()

		StateComponent.confirmStateChange {StateComponent.changeState('Status 20')}

		switchToOtherWindow(true)
		eo = EntityObjectComponent.forDetail()
		sfDep = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORDEPENDENT.fqn + '_testobjektgen')

		row0 = sfDep.getRow(0)
		assert row0.getValue('nuclosState') == 'Status 20'
	}
}
