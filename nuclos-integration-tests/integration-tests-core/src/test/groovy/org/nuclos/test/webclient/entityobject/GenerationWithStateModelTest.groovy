package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.GenerationComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.StateComponent

import groovy.transform.CompileStatic;

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
public class GenerationWithStateModelTest extends AbstractRerunnableWebclientTest {

	@Test
	void runTest() {
		_00_setup:
		{
			assertLoggedIn('test')
			TestDataHelper.insertTestData(nuclosSession)
		}

		_10_testUnsavedObjectState:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
			int orderNumber = Integer.parseInt(eo.getAttribute('orderNumber') as String)
            int countBeforeGeneration = Sidebar.listEntryCount

            // ordered by intid desc
            assert orderNumber == 10020158

			Sidebar.selectEntry(0)
			StateComponent.changeState('abschließen')
			StateComponent.confirmStateChange()
			assert StateComponent.currentState == '80 Abgeschlossen'

			GenerationComponent.generateObjectAndConfirm('Order Clone') == 1 // only this generation is allowed
			GenerationComponent.openResult()

			assert driver.getWindowHandles().size() == 2

			String oldWindow = driver.windowHandle
			String newWindow = switchToOtherWindow()

			assert oldWindow != newWindow

			assert !StateComponent.currentState
			assert !StateComponent.getNextStates(true)

			eo = EntityObjectComponent.forDetail()
            assert EntityObjectComponent.isDirty()

			// Test input, would not be possible in the state 'Abgeschlossen' for the test user
            String noteInput = 'Note input with the webclient'
			eo.setAttribute('note', noteInput)
			eo.save()

			int orderNumberGenerated = Integer.parseInt(eo.getAttribute('orderNumber') as String)
			assert orderNumber + 1 == orderNumberGenerated
            assert !EntityObjectComponent.isDirty()

            assert eo.getAttribute('note') == noteInput

            Sidebar.refresh()
            assert countBeforeGeneration + 1 == Sidebar.listEntryCount
		}
	}

}
