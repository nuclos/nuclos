package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class HyperlinkTest extends AbstractRerunnableWebclientTest {

	private final String hyperlink = 'hyperlink'
	private final String disabledHyperlink = 'disabledhyperlink'

	private final String testUrl = 'http://127.0.0.1'
	private final String testTitle = 'Test Title'
	private final String testAnchorTag = '<a href="' + testUrl + '">' + testTitle + '</a>'
	private final String dataURL = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABGdBTUEAALGPC/' +
			'xhBQAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9YGARc5KB0XV+IAAAAddEVYdENvbW1lbnQAQ3JlYX' +
			'RlZCB3aXRoIFRoZSBHSU1Q72QlbgAAAF1JREFUGNO9zL0NglAAxPEfdLTs4BZM4DIO4C7OwQg2JoQ9LE1exd' +
			'lYvBBeZ7jqch9//q1uH4TLzw4d6+ErXMMcXuHWxId3KOETnnXXV6MJpcq2MLaI97CER3N0vr4MkhoXe0rZig' +
			'AAAABJRU5ErkJggg=='

	@Test
	void runTest() {
		_00_setup:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTHYPERLINK)
			eo.addNew()
			eo.save()
		}

		_05_testEmptyLink:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			assert !eo.getAttribute(hyperlink)
			assert !eo.getAttribute(disabledHyperlink)

			NuclosWebElement link = eo.getAttributeElement(hyperlink)
			// to understand following assertions, see: NUCLOS-10786 & NUCLOS-10105
			assert $$('[name=attribute-hyperlink]').size() == 1: 'The div which is for disabled hyperlink fields without content, should not be visible at this moment'
			link.click()
			assert getDriver().getWindowHandles().size() == 1, 'Clicking an empty link should not open a new window'
		}

		_10_testInputLink:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.setAttribute(hyperlink, testUrl)
			NuclosWebElement link = eo.getAttributeElement(hyperlink)
			link.click()

			checkNewWindowAndCloseIt() {
				getDriver().currentUrl == testUrl
			}
		}

		_15_testEditLink:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.setAttribute(hyperlink, dataURL)
			eo.clickButton('Hyperlink Button')

			checkNewWindowAndCloseIt() {
				List<WebElement> images = driver.findElements(By.cssSelector('img'))
				assert images.size() == 1
				assert images[0].getAttribute('src') == dataURL
			}
		}

		_20_testDisabledLink:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			eo.save()
			eo.addNew()

			// Use about:blank as URL because data URLs can't be opened anymore in newer browser versions
			eo.setAttribute(hyperlink, testUrl)

			eo.save()
			assert eo.getText(disabledHyperlink) == testUrl

			NuclosWebElement link = eo.getAttributeElement(disabledHyperlink)
			assert link.tagName == 'a', 'A disabled hyperlink component should just be displayed as a standard HTML link'
			link.click()

			checkNewWindowAndCloseIt() {
				getDriver().currentUrl == testUrl
			}
		}

		_25_testAnchorTagLink:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.setAttribute(hyperlink, testAnchorTag)
			NuclosWebElement link = eo.getAttributeElement(hyperlink)

			assert link.getText() == testTitle
			link.click()

			checkNewWindowAndCloseIt() {
				getDriver().currentUrl == testUrl
			}
		}
	}

	@Test
	void testDisabledHyperLinkVisibleBackground() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTHYPERLINK)
		eo.addNew()
		eo.save()
		assert eo.getAttributeElement(disabledHyperlink).getCssValue('background-color') == 'rgba(233, 236, 239, 1)'
	}

	private void checkNewWindowAndCloseIt(Closure check) {
		Set<String> handles = getDriver().getWindowHandles()

		assert handles.size() == 2

		// no angular page is opened here...
		boolean bWaitForAngular = false
		switchToOtherWindow(bWaitForAngular)

		check()

		switchToOtherWindow()
		closeOtherWindows()
	}
}
