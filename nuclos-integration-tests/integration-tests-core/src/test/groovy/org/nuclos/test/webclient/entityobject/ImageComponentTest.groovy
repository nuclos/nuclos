package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ImageComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ImageComponentTest extends AbstractRerunnableWebclientTest {

	@Before()
	void setup() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test()
	void testImageComponent() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		waitForAngularRequestsToFinish()

		ImageComponent grafikComp = eo.getImageComponent('grafik')

		assert grafikComp != null : 'Graphic component should be available'
		assert grafikComp.emptyImage(): 'Test-Data do not have any data associated'
		assert grafikComp.element.text == 'Zum Hochladen Bild hier fallen lassen.': 'Correct description on empty image is visible'

		grafikComp.dropFile(TestDataHelper.nuclosPngFile)
		waitForAngularRequestsToFinish()

		assert !grafikComp.emptyImage(): 'Image is uploaded'

		eo.save()
		eo.reload()

		assert !grafikComp.emptyImage(): 'Image still present'

		assert grafikComp.openContextMenu() != null: 'Context Menu should open with click on element'
		assert grafikComp.contextActions().containsAll([
		        'Bild ersetzen',
		        'Bild anzeigen',
		        'Bild entfernen'
		]): 'All actions should be visible'

		grafikComp.executeContextAction('Bild entfernen')
		waitForAngularRequestsToFinish()

		assert grafikComp.emptyImage(): 'Image should be removed'

		eo.cancel()

		assert !grafikComp.emptyImage(): 'Image should be restored'

		grafikComp.executeContextAction('Bild anzeigen')
		waitForAngularRequestsToFinish()

		waitUntilTrue({ -> driver.getWindowHandles().size() == 2 })
	}

	/**
	 * Test to verify that NUCLOS-10130 has been fixed.
	 */
	@Test
	void testImageLayouting() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTIMAGETAB)
		eo.addNew()
		waitForAngularRequestsToFinish()
		testHorizontalStretchVariations: {
			NuclosWebElement stretchPanel = findPanel('#PanelVerticalStretch-3')
			assert getImage(stretchPanel, 0).size.height == stretchPanel.size.height
			assert getImage(stretchPanel, 0).size.width != stretchPanel.size.width
			assert getImage(stretchPanel, 1).size.height == stretchPanel.size.height
			assert getImage(stretchPanel, 1).size.width != stretchPanel.size.width
			assert getImage(stretchPanel, 2).size.height == stretchPanel.size.height
			assert getImage(stretchPanel, 2).size.width != stretchPanel.size.width

			assertHorizontalStart(getImage(stretchPanel, 0))
			assertHorizontalCenter(getImage(stretchPanel, 1))
			assertHorizontalEnd(getImage(stretchPanel, 2))
		}
		testVerticalStretchVariations: {
			NuclosWebElement stretchPanel = findPanel('#PanelHorizontalStretch-2')
			assert getImage(stretchPanel, 0).size.width == stretchPanel.size.width
			assert getImage(stretchPanel, 0).size.height != stretchPanel.size.height
			assert getImage(stretchPanel, 1).size.width == stretchPanel.size.width
			assert getImage(stretchPanel, 1).size.height != stretchPanel.size.height
			assert getImage(stretchPanel, 2).size.width == stretchPanel.size.width
			assert getImage(stretchPanel, 2).size.height != stretchPanel.size.height
			assertVerticalStart(getImage(stretchPanel, 0))
			assertVerticalCenter(getImage(stretchPanel, 1))
			assertVerticalEnd(getImage(stretchPanel, 2))
		}
		testPackedAlignment: {
			NuclosWebElement stretchPanel = findPanel('#PanelAlignment-0')
			for (int i = 0; i < 9; i++) {
				assert getImage(stretchPanel, i).size.width != stretchPanel.size.width
				assert getImage(stretchPanel, i).size.height != stretchPanel.size.height
			}
			// 1. row
			for (int i = 0; i < 3; i++) {
				assertVerticalStart(getImage(stretchPanel, 0 + 3 * i))
			}
			// 2. row
			for (int i = 0; i < 3; i++) {
				assertVerticalCenter(getImage(stretchPanel, 1 + 3 * i))
			}
			// 3. row
			for (int i = 0; i < 3; i++) {
				assertVerticalEnd(getImage(stretchPanel, 2 + 3 * i))
			}
			// 1. column
			for (int i = 0; i < 3; i++) {
				assertHorizontalStart(getImage(stretchPanel, i))
			}
			// 2. column
			for (int i = 0; i < 3; i++) {
				assertHorizontalCenter(getImage(stretchPanel, 3 + i))
			}
			// 3. column
			for (int i = 0; i < 3; i++) {
				assertHorizontalEnd(getImage(stretchPanel, 6 + i))
			}
		}
		testFullStretch: {
			eo.selectTab('Full stretch tab')
			waitForAngularRequestsToFinish()
			NuclosWebElement fullTabPanel = $('#tab-2 .web-grid-container .web-grid-container').$$('div')[1]
			NuclosWebElement imageComponent = getImage(fullTabPanel, 0)
			assert imageComponent.size.width == fullTabPanel.size.width
			assert imageComponent.size.height == fullTabPanel.size.height
			assert Math.abs(imageComponent.size.width - imageComponent.$('div').size.width) <= 2
			assert Math.abs(imageComponent.size.height - imageComponent.$('div').size.height) <= 2
			assert (imageComponent.location.x - imageComponent.$('div').location.x) == 0
			assert (imageComponent.location.y - imageComponent.$('div').location.y) == 0
		}
	}

//	private NuclosWebElement findPanel(int id) {
//		return $zZz('#undefined-' + id + ' #undefined-0 .web-grid-container')
//	}
	private NuclosWebElement findPanel(String panelId) {
		return $zZz(panelId + ' #undefined-0 .web-grid-container')
	}


	private NuclosWebElement getImage(NuclosWebElement panel, int id) {
		return panel.$('#image-' + id)
	}

	private assertHorizontalStart(NuclosWebElement image) {
		assert image.$('div').location.x == image.location.x
	}

	private assertHorizontalCenter(NuclosWebElement image) {
		NuclosWebElement insideDiv = image.$('div');
		assert Math.abs((int) (insideDiv.location.x + insideDiv.size.width / 2 - (image.location.x + image.size.width / 2))) <= 2
	}

	private assertHorizontalEnd(NuclosWebElement image) {
		assert Math.abs((int) (image.$('div').location.x + image.$('div').size.width - (image.location.x + image.size.width))) <= 2
	}

	private assertVerticalStart(NuclosWebElement image) {
		assert image.$('div').location.y == image.location.y
	}

	private assertVerticalCenter(NuclosWebElement image) {
		NuclosWebElement insideDiv = image.$('div');
		assert Math.abs((int) (insideDiv.location.y + insideDiv.size.height / 2 - (image.location.y + image.size.height / 2))) <= 2
	}

	private assertVerticalEnd(NuclosWebElement image) {
		assert Math.abs((int) (image.$('div').location.y + image.$('div').size.height - (image.location.y + image.size.height))) <= 2
	}

}
