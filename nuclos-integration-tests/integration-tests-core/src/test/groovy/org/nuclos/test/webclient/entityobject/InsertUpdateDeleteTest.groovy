package org.nuclos.test.webclient.entityobject

import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class InsertUpdateDeleteTest extends AbstractRerunnableWebclientTest {

	@Test
	void runTest() {
		String testEntryName = null

		_05_layoutUsage:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTUSAGE)

			eo.addNew()

			def isLayoutForNew = {
				$$('label').find { it.text.contains('Layout for "new"') }
			}
			assert isLayoutForNew()

			eo.setAttribute('name', 'Test')
			eo.save()

			assert !isLayoutForNew()
		}

		_10_openEntity:
		{
			EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
			assertLoggedIn('test')
		}

		_20_createNewEntry:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Log.debug 'createNewEntry'
			int entryCountBefore = Sidebar.listEntryCount

			screenshot('createnewentry-a')
			eo.addNew()
			screenshot('createnewentry-b')

			// TODO fix sidebar view update after first
			// assert Sidebar.listEntryCount == entryCountBefore + 1

			// testEntryName = 'testentry '+formatDate(new Date());
			testEntryName = '42'

			eo.setAttribute('customerNumber', testEntryName)
			eo.setAttribute('name', testEntryName)

			assert eo.dirty

//		Sideview.fillRequiredFields(testEntryName)

			screenshot('createnewentry-c')
			eo.save()
			screenshot('createnewentry-d')

			assert !eo.dirty

			/* TODO fix handling of english dateformat
			// seconds are not shown in detail statusbar - so check if timestamp is max 1min before now
			def nowMillis = new Date().getTime()
			def changedAtMillis = eo.getChangedAt().getTime()
			assert changedAtMillis < nowMillis
			assert changedAtMillis > nowMillis - 60000

			def createdAtMillis = eo.getCreatedAt().getTime()
			assert createdAtMillis < nowMillis
			assert createdAtMillis > nowMillis - 60000
			*/

			assert eo.getChangedBy().contains('test')

			def entryCountAfter = Sidebar.listEntryCount
			assert entryCountAfter > entryCountBefore
		}

		_25_checkDynamicEntityColumns:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.selectTab('Dynamic Entity');
			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_MYORDERSANDCUSTOMERSDYN.fqn + '_INTIDTUDGENERICOBJECT')

			// Column is defined as "Changed By" - with a space
			assert subform.columnHeaders.contains('Changed By')

			assert subform.rowCount > 0

			// The attribute name is "ChangedBy" - without space
			assert subform.getRow(0).getValue('ChangedBy') == 'test'
		}

//	@Test
//	void _4findNewEntry() {
//		Log.debug 'findNewEntry'
//		screenshot('search-new-entry-start')
//		Searchtemplate.search(testEntryName)
//		screenshot('search-new-entry-end')
//
//		def foundEntries = $$('.sideview-list-entry')
//		assert foundEntries.size() == 1
//	}
//
//	@Test
//	void _5openSideviewAndFindNewEntry() {
//
//		Sideview.openSideview('example_rest_Customer')
//
//		screenshot('before-search')
//		Searchtemplate.search(testEntryName)
//
//		screenshot('search-entry-again')
//
//		def foundEntries = $$('.sideview-list-entry')
//		assert foundEntries.size() == 1
//
//		// open found entry again
//		$('.sideview-list-entry').click()
//	}

		_30_updateNewEntry:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Log.debug 'updateNewEntry'
			testEntryName = '43'

			assert !eo.dirty

			def inputs = $$xXx('nuc-detail input[type="text"]:not([readonly])')
			inputs.forEach(it -> {
				it.clear()
				waitForAngularRequestsToFinish()
			})

			eo.setAttribute('customerNumber', testEntryName)
			eo.setAttribute('name', testEntryName)

			assert eo.dirty
			assert Sidebar.isEntryMarkedDirty(0)

//		Sideview.fillRequiredFields(testEntryName)
			// TODO: Assert the EO is marked as dirty now and we cannot navigate away from it before we save or cancel

			screenshot('updatenewentry-beforesave')
			eo.save()
			screenshot('updatenewentry-aftersave')

			assert !eo.dirty

			assert eo.getAttribute('customerNumber') == testEntryName
			assert eo.getAttribute('name') == testEntryName

			// TODO: Assert the EO is saved and it is possible to navigate to another EO or to create a new one
		}

		_35_cancelUpdate:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			def oldEntryName = '43'
			testEntryName = '44'

			assert eo.getAttribute('customerNumber') == oldEntryName
			assert eo.getAttribute('name') == oldEntryName

			def inputs = $$xXx('nuc-detail input[type="text"]:not([readonly])')
			inputs.forEach(it -> {
				it.clear()
				waitForAngularRequestsToFinish()
			})

			eo.setAttribute('customerNumber', testEntryName)
			eo.setAttribute('name', testEntryName)

			// EO should be dirty anymore and the field values should be set to the new value
			assert eo.dirty
			assert eo.getAttribute('customerNumber') == testEntryName
			assert eo.getAttribute('name') == testEntryName

			eo.cancel()

			// EO should not be dirty anymore and the field values should be reset to the old value
			assert !eo.dirty
			assert eo.getAttribute('customerNumber') == oldEntryName
			assert eo.getAttribute('name') == oldEntryName
		}

		_37_cancelNew:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			def previousEntryCount = Sidebar.listEntryCount

			def previousEntryName = '43'

			eo.addNew()

			assert Sidebar.listEntryCount == previousEntryCount + 1

			assert eo.getAttribute('customerNumber') == ''
			assert eo.getAttribute('name') == ''

			eo.cancel()

			assert Sidebar.listEntryCount == previousEntryCount

			assert !eo.dirty

			assert eo.getAttribute('customerNumber') == previousEntryName
			assert eo.getAttribute('name') == previousEntryName
		}

		_38_preventNavigationFromDirty:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			2.times {
				eo.addNew()
				eo.setAttribute('customerNumber', 100 + it)
				eo.setAttribute('name', 100 + it)
				eo.save()
			}

			eo.addNew()
			eo.setAttribute('customerNumber', testEntryName)

			assert eo.dirty
			assert Sidebar.listEntryCount == 4
			// FIXME assert Sidebar.selectedIndex == 0

			Sidebar.selectEntry(1)
			checkPopover(true)

			MenuComponent.toggleUserMenu()
			MenuComponent.clickChangePasswordLink()
			MenuComponent.clickOpenPreferencesResetModal()
			LocaleChooser.setLocale(Locale.GERMAN)
			MenuComponent.openMenu('Example', 'Article')
			checkPopover(true)

			Sidebar.selectEntry(1)
			checkPopover(true)

			eo.cancel()
			assert Sidebar.selectedIndex == 0
			assert !DetailButtonsComponent.popoverTitle
			assert !DetailButtonsComponent.popoverText

			// NUCLOS-7434 Popover should not re-appear after new record was cancelled
			Sidebar.selectEntry(2)
			waitForAngularRequestsToFinish()
			checkPopover(false)
			assert EntityObjectComponent.forDetail().getAttribute('customerNumber') == '43': 'Second Entry should be selected'
		}

//	@Test
//	void _7findUpdatedEntry() {
//		Log.debug 'findUpdatedEntry'
//		screenshot('search-updated-entry-start')
//		Searchtemplate.search(testEntryName)
//		screenshot('search-updated-entry-end')
//
//		assert $$('.sideview-list-entry').size() == 1
//	}

		_40_staleVersionError:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			assert !StateComponent.currentState, 'This test must be run on an entity without state model'

			EntityObject entityObject = nuclosSession.getEntityObject(
					TestEntities.EXAMPLE_REST_CUSTOMER,
					eo.id
			)
			nuclosSession.save(entityObject)

			eo.setAttribute('customerNumber', 50)
			eo.save()

			assertMessageModalAndConfirm('Fehler', 'Versions-Konflikt', {eo.save()})
		}

		_50_deleteNewentry:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Log.debug 'deleteNewentry'

			screenshot('delete-eo-start')
			eo.delete()

			screenshot('delete-eo-end')

			assert Sidebar.listEntryCount == 2
		}

		_60_addNewWithSubformAndExistingRecords:
		{
			addRecord:
			{
				EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES)
				eo.setAttribute('name', 'Pre-existing record')
				nuclosSession.save(eo)
			}

			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES)
			eo.addNew()
			eo.enterText('name', 'New record')
			Subform subform = eo.getSubform('nuclet_test_other_TestLayoutRulesSubform_testlayoutrules')
			subform.newRow()

			EntityObjectModal modal = EntityObjectComponent.forModal()
			modal.enterText('textvaluechanged', 'Test')
			EntityObjectComponent.clickButtonOk()

			eo.save()

			assert !eo.dirty
			assert !LoadingIndicatorComponent.loading
			assert Sidebar.listEntryCount == 2
		}
	}

	@Test
	void testMaxLengthOfInput() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()

		eo.setAttribute('orderNumber', 56456465)
		eo.setAttribute('begrenzt', 'dassindjetztnur25zeichen.')

		eo.save()
		waitForAngularRequestsToFinish()
		assert !eo.dirty: 'Begrenzt with exactly 25 chars should save'

		eo.setAttribute('begrenzt', 'dassindjetztmehrals25zeichen.')

		assert eo.getAttribute('begrenzt') == 'dassindjetztmehrals25zeic': 'MaxLength should only accept a few letters'
		assert eo.dirty
		eo.cancel()
	}

	@Test
	void testTabCountInSubforms() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		waitForAngularRequestsToFinish()

		def tabList = $$('.p-tabview-nav li')

		assert tabList[0]?.text?.trim() == 'Address (1)': 'Address should have count of 1'
		assert tabList[1]?.text?.trim() == 'Orders (0)': 'Orders should have count of 0'
		assert tabList[2]?.text?.trim() == 'Dynamic Entity (0)': 'Dynamic Entity should have count of 0'
		assert tabList[3]?.text?.trim() == 'Matrix-Zuordnung (0)': 'Matrix-Zuordnung should have count of 0'
		assert tabList[4]?.text?.trim() == 'Addresses (0)': 'Addresses should have count of 0'

		eo.addNew()

		tabList = $$('.p-tabview-nav li')

		assert tabList[0]?.text?.trim() == 'Address (0)': 'Address should have count of 0'
		assert tabList[1]?.text?.trim() == 'Orders (0)': 'Orders should have count of 0'
		assert tabList[2]?.text?.trim() == 'Dynamic Entity (0)': 'Dynamic Entity should have count of 0'
		assert tabList[3]?.text?.trim() == 'Matrix-Zuordnung (0)': 'Matrix-Zuordnung should have count of 0'
		assert tabList[4]?.text?.trim() == 'Addresses (0)': 'Addresses should have count of 0'

		eo.setAttribute('customerNumber', '123')
		eo.setAttribute('name', 'Test')
		eo.save()

		waitForAngularRequestsToFinish()

		tabList = $$('.p-tabview-nav li')

		assert tabList[0]?.text?.trim() == 'Address (0)': 'Address should have count of 0'
		assert tabList[1]?.text?.trim() == 'Orders (0)': 'Orders should have count of 0'
		assert tabList[2]?.text?.trim() == 'Dynamic Entity (1)': 'Dynamic Entity should have count of 1'
		assert tabList[3]?.text?.trim() == 'Matrix-Zuordnung (0)': 'Matrix-Zuordnung should have count of 0'
		assert tabList[4]?.text?.trim() == 'Addresses (0)': 'Addresses should have count of 0'
	}

	@Test
	void testStatusBarUpdate() {
		String timezone = EntityObjectComponent.executeScript('return Intl.DateTimeFormat().resolvedOptions().timeZone')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute('orderNumber', 1)
		eo.save()

		long primaryKey = eo.<Long> getPrimaryKey()
		EntityObject<Long> eoData = nuclosSession.getEntityObject(TestEntities.EXAMPLE_REST_ORDER, primaryKey)
		String sChangedAt = eoData.getAttribute("changedAt")
		ZonedDateTime changedAt = ZonedDateTime.parse(sChangedAt.substring(0, sChangedAt.length())).truncatedTo(ChronoUnit.SECONDS)

		assert changedAt.isEqual(ZonedDateTime.of(eo.getChangedAt().toLocalDateTime(), ZoneId.of(timezone)))

		duplicateCurrentWindow()
		switchToOtherWindow()

		eo = EntityObjectComponent.forDetail()
		eo.setAttribute('orderNumber', 2)
		eo.save()
		switchToOtherWindow()
		closeOtherWindows()

		eo = EntityObjectComponent.forDetail()
		eo.reload()
		eoData = nuclosSession.getEntityObject(TestEntities.EXAMPLE_REST_ORDER, primaryKey)
		sChangedAt = eoData.getAttribute("changedAt")
		changedAt = ZonedDateTime.parse(sChangedAt.substring(0, sChangedAt.length())).truncatedTo(ChronoUnit.SECONDS)

		assert changedAt.isEqual(ZonedDateTime.of(eo.getChangedAt().toLocalDateTime(), ZoneId.of(timezone)))
	}

	private void checkPopover(boolean shouldBeVisible) {
		DetailButtonsComponent.checkPopover(shouldBeVisible)
	}
}
