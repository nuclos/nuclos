package org.nuclos.test.webclient.entityobject

import java.text.SimpleDateFormat

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.schema.layout.web.WebButtonExecuteRule
import org.nuclos.schema.layout.web.WebCombobox
import org.nuclos.schema.layout.web.WebDatechooser
import org.nuclos.schema.layout.web.WebHtmlField
import org.nuclos.schema.layout.web.WebTextfield
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.Dimension
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LayoutComponentTest extends AbstractRerunnableWebclientTest {

	def colorSelect = "rgba(204, 255, 204, 1)"
	def colorInput = "rgba(255, 255, 204, 1)"
	def gradientTabHover = "linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2))"
	def gradientTabSelected = "linear-gradient(0deg, rgba(0, 0, 0, 0.125), rgba(0, 0, 0, 0.125))"
	def colorDate_File = "rgba(255, 204, 204, 1)"
	def color_Border = "rgb(255, 0, 51)"
	def size_Border = "2px"

	@Before
	void setup() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void testHoverTooltip() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()
		eo.setAttribute('text', 'test hover tooltip')
		eo.save()

		NuclosWebElement dateLabel = $('label[data-name=\'date\'')
		dateLabel.mouseover()
		waitUntilTrue {
			def tooltip = $('div.p-tooltip')
			tooltip && tooltip.text == 'Date'
		}

		def tooltip = $('div.p-tooltip')
		assert tooltip
		assert tooltip.text == 'Date'

		eo.getAttributeElement('date').mouseover()
		waitUntilTrue {
			!$('div.p-tooltip')
		}
		assert !$('div.p-tooltip')
	}

	@Test
	void runTest() {
		_00_textareaInSubform:
		{
			final int standardRowHeight = 27
			final String multilineText = "Multi\nline\ntext"

			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
			eo.addNew()

			Subform subform = getMemoSubform()

			multilineViaModal:
			{
				SubformRow row = subform.newRow()
				waitForAngularRequestsToFinish()
				EntityObjectModal modal = EntityObjectComponent.forModal()
				modal.setAttribute('memo', multilineText)

				// Close the modal
				EntityObjectComponent.clickButtonOk()

				row = subform.getRow(0);
				assert row.height > standardRowHeight
				assert row.getValue('memo', String.class, true) == multilineText
			}

			multilineInlineEditing:
			{
				SubformRow row = subform.newRow()

				// Close the modal
				EntityObjectComponent.clickButtonOk()
				row = subform.getRow(0)

				row.enterValue('memo', "Single line text")
				assert row.height == standardRowHeight

				row.enterValue('memo', multilineText)
				assert row.height > standardRowHeight
				assert row.getValue('memo', String.class, true) == multilineText
			}

			eo.cancel()
		}

		_05_comboboxOnTextfield:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
			eo.addNew()

			Object test = eo.getAttribute('radio', WebTextfield)
			assert test == 3 || test == '3' // Default Value für OptionGroup "RadioTest" is 3

			assert !eo.getAttribute('text', WebTextfield)

			ListOfValues lov = eo.getLOV('text')
			assert !lov.textValue
			lov.open()
			List<String> choices = lov.choices
			assert choices.containsAll(['nuclos', 'test'])
			assert !choices.contains('')

			changeValueViaTextfield:
			{
				String nonexistentuser = 'nonexistentuser'
				lov.sendKeys(nonexistentuser)

				assert eo.getAttribute('text', WebTextfield) == nonexistentuser
				choices = lov.choices
				assert choices.containsAll([])
				assert lov.textValue == nonexistentuser
			}

			changeValueViaLov:
			{
				lov.inputElement.clear()
				lov.selectEntry('test')
				assert eo.getAttribute('text', WebTextfield) == 'test'
				assert lov.textValue == 'test'
			}

			checkValuesAfterSave:
			{
				eo.save()
				assert eo.getAttribute('text', WebTextfield) == 'test'
				assert eo.getLOV('text').textValue == 'test'
			}

			checkValuesAfterRest:
			{
				eo.setAttribute('text', 'asdf')
				eo.cancel()
				assert eo.getAttribute('text', WebTextfield) == 'test'
				assert eo.getLOV('text').textValue == 'test'
			}
		}

		_06_colorChooser:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			assert !eo.getAttribute('color')
			NuclosWebElement element = eo.getAttributeElement('color')

			assert element.getCssValue('background').startsWith('rgb(255, 255, 255)')

			eo.setAttribute('color', '#f00')

			assert element.getCssValue('background').startsWith('rgb(255, 0, 0)')
		}

		_07_testSeparators:
		{
			NuclosWebElement nbw = $('.titled-separator')
			assert nbw
			assert nbw.isDisplayed()
			assert nbw.getText() == 'Separator with Title'
			assert nbw.getCssValue('border-bottom').startsWith('1px solid')

			nbw = $('.separator-horizontal')
			assert nbw
			// This Layout is completely broken and not really reliable on all resolutions, thus
			// turn off this check as it gets overflown by other elements
			// assert nbw.isDisplayed()
			assert nbw.getCssValue('border-bottom').startsWith('1px solid')
			assert nbw.getCssValue('border-left').startsWith('0px none')

			nbw = $('.separator-vertical')
			assert nbw
			assert nbw.isDisplayed()
			assert nbw.getCssValue('border-bottom').startsWith('0px none')
			assert nbw.getCssValue('border-left').startsWith('1px solid')

		}

		_10_selectDateByDatepickerDropdown:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Date date = new Date(117, 0, 1)   // 2017-01-01

			eo.setAttribute('date', date)

			Datepicker datepicker = eo.getDatepicker('date')

			datepicker.open()
			assert datepicker.getMonth() == 'Jan'
			assert datepicker.getYear() == 2017

			// NUCLOS-7475 Datepicker should be attached to body
			assert datepicker.datepicker.parent.tagName == 'body'

			assert datepicker.datepicker.$('.ngb-dp-week-number') != null

			changeDate:
			{
				datepicker.clickDayOfCurrentMonth(31)

				Date expectedDate = new Date(117, 0, 31)    // 2017-01-31
				assert eo.getAttribute('date', null, Date.class) == expectedDate

				assert eo.dirty
				eo.save()
			}
		}

		_11_selectInvalidDate:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.setAttribute('date', 'ABC')
			sendKeys(Keys.TAB)

			assert eo.getAttribute('date') == ''

			eo.cancel()
		}

		_12_selectDateByDatepickerDropdownInSubform:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.selectTab('Subform Date')
			Subform subform = eo.getSubform('nuclet_test_other_TestLayoutComponentsSubform_parent')

			subform.newRow()
			SubformRow row = subform.newRow()
			Datepicker datepicker = row.getDatepicker('subdate')

			// NUCLOS-7475 Datepicker should be attached to body
			assert datepicker.datepicker.parent.tagName == 'body'

			row.enterValue('subdate', '2017-01-01')

			datepicker.open()
			assert datepicker.getMonth() == 'Jan'
			assert datepicker.getYear() == 2017

			changeDate:
			{
				datepicker.clickDayOfCurrentMonth(31)
				row.getFieldElement('subdate').click()
				sendKeys(Keys.ESCAPE)

				// NUCLOS-6498 r)
				SubformRow rowEmpty = subform.getRow(1)
				NuclosWebElement column = row.getFieldElement('subdate')
				NuclosWebElement columnEmpty = rowEmpty.getFieldElement('subdate')

				assert column.hasClass('ag-cell-focus')
				assert !columnEmpty.hasClass('ag-cell-focus')
				column.sendKeys(Keys.ARROW_DOWN)

				assert !column.hasClass('ag-cell-focus')
				assert columnEmpty.hasClass('ag-cell-focus')

				rowEmpty.setSelected(true)
				subform.deleteSelectedRows()

				Date expectedDate = new Date(117, 0, 31)    // 2017-01-31
				assert subform.getRow(0).getValue('subdate', Date.class) == expectedDate

				assert eo.dirty
				eo.save()
				assert !eo.dirty

				assert subform.getRow(0).getValue('subdate', Date.class) == expectedDate
			}
		}

		_13_deleteDate:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Date expectedDate = new Date(117, 0, 31)    // 2017-01-31
			assert eo.getAttribute('date', null, Date.class) == expectedDate

			eo.setAttribute('date', '')
			eo.save()

			// Expect no error message
			assert !messageModal

			assert !eo.getAttribute('date', null, Date.class)
		}

		_16_enterInvalidDateInSubform:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.selectTab('Subform Date')
			Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSSUBFORM, 'parent')

			subform.newRow()
			SubformRow row = subform.newRow()

			row.enterValue('subdate', 'ABC')
			assert row.getValue('subdate') == ''

			NuclosWebElement cell = row.getFieldElement('subdate')
			cell.click()
			sendKeys('XXX')
			sendKeys(Keys.ESCAPE)

			assert row.getValue('subdate') == ''

			// NUCLOS-7059 Browser freeze occurred when trying to edit this cell again:
			cell.click()

			sendKeys(Keys.ESCAPE)

			eo.cancel()
		}

		_17_enterDateManually:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Datepicker datepicker = eo.getDatepicker('date')
			datepicker.focus()

			assert datepicker.open

			enterMay1999(datepicker)

			Date expectedDate = new Date(99, 4, 1)    // 1999-05-01
			assert eo.getAttribute('date', WebDatechooser.class, Date.class) == expectedDate
			assert eo.getAttribute('date', WebTextfield.class, String.class) == new SimpleDateFormat('dd.MM.yyyy').format(expectedDate)

			eo.cancel()

			datepicker.input = 98

			// TODO: Pressing ENTER selects the complete date in normal Chrome, but does not work via Selenium
//		sendKeys(Keys.ENTER)
//		assert eo.getAttribute('date', null, Date.class) == new Date(98, 0, 1)    // 1998-01-01

			sendKeys(Keys.TAB)
			assert eo.getAttribute('date', null, Date.class) == new Date(98, 0, 1)    // 1998-01-01

			eo.cancel()
		}

		_18_enterDateManuallyInSubform:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
			eo.addNew()

			eo.selectTab('Subform Date')
			Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSSUBFORM, 'parent')

			SubformRow row = subform.newRow()
			def datepicker = row.getDatepicker('subdate')

			datepicker.open()
			enterMay1999(datepicker)

			assert row.getValue('subdate', Date.class) == new Date(99, 4, 1)    // 1999-05-01

			eo.cancel()
		}


		_20_tooltips:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

			Subform subform = getMemoSubform()
			assert subform.getColumnHeaderTooltips()[0] == '<b>This</b> is a <br> <u>formatted</u> Text'
			assert Sidebar.getColumnHeaderTooltips()[1] == '<b>This</b> is a <br> <u>formatted</u> Text'

			assert eo.getLabel('text').getTooltip() == 'Label-Tooltip<br>' + 'Mehrzeilig?'
			assert eo.getAttributeElement('text').getTooltip() == 'Textfield-Tooltip'
			assert eo.getLOV('text').lovContainer.getTooltip() == 'Combobox-Tooltip'
			assert eo.getAttributeElement('email').getTooltip() == 'Email-Tooltip'
			assert eo.getAttributeElement('hyperlink').getTooltip() == 'Hyperlink-Tooltip'
			assert eo.getLOV('reference').lovContainer.getTooltip() == 'Reference-Tooltip'
			assert eo.getAttributeElement('encryptedtext').getTooltip() == 'Encryptedtext-Tooltip'
			assert eo.getAttributeElement('radio').getTooltip() == 'Radio-Tooltip'
			assert eo.getButton('Rule button').getTooltip() == 'Button-Tooltip'
		}

		_30_nonInsertableDropdown:
		{
			logout()
			login('nuclos')

			EntityObjectComponent eo = EntityObjectComponent.open(SystemEntities.PARAMETER)
			eo.addNew()
			// THUMBNAIL_SIZE is not shown, only the first 100 are visible
//			eo.setAttribute('name', 'CODEGENERATOR_ENABLED')
//			eo.setAttribute('description', 'test')
			sendKeysAndWaitForAngular('CODEGENERATOR_ENABLED')
			sendKeysAndWaitForAngular(Keys.TAB)
			sendKeysAndWaitForAngular('test')
			eo.save()
			screenshot('after-system-parameter-save') // error-prone point
			assert !eo.dirty

			assert eo.getAttribute('name') == 'CODEGENERATOR_ENABLED'

			ListOfValues lov = eo.getLOV('name')
			lov.open()

			// Only after an existing dropdown entry is selected should the record be dirty
			lov.inputElement.clear()
			sendKeysAndWaitForAngular('CLIENT_READ_TIMEOUT')
			sendKeysAndWaitForAngular(Keys.TAB)
			assert eo.dirty
			assert eo.getAttribute('name') == 'CLIENT_READ_TIMEOUT'

			eo.save()
			assert !eo.dirty
			assert eo.getAttribute('name') == 'CLIENT_READ_TIMEOUT'
		}

		_40_noReadPermissions:
		{

			logout()
			login('test', 'test')

			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
			eo.addNew()
			eo.setAttribute('text', '1')
			eo.save()

			assert eo.getAttributeElement('text') != null
			assert eo.getAttributeElement('date') != null
			assert eo.getAttributeElement('email') != null
			assert eo.getAttributeElement('integer') != null
			assert eo.getAttributeElement('hyperlink') != null
			assert eo.getAttributeElement('checkbox') != null
			assert eo.getAttributeElement('double9_2') != null
			assert eo.getAttributeElement('double9_4') != null
			assert eo.getAttributeElement('memo') != null
			assert eo.getAttributeElement('phonenumber') != null
			assert eo.getAttributeElement('reference') != null
			assert eo.getAttributeElement('textlargeobject') != null
			assert eo.getAttributeElement('encryptedtext') != null
			assert eo.getAttributeElement('radio') != null

			StateComponent.changeState('Nicht sichtbar')
			StateComponent.confirmStateChange()

			eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
			assert eo.getAttributeElement('text') == null
			assert eo.getAttributeElement('date') == null
			assert eo.getAttributeElement('email') == null
			assert eo.getAttributeElement('integer') == null
			assert eo.getAttributeElement('hyperlink') == null
			assert eo.getAttributeElement('checkbox') == null
			assert eo.getAttributeElement('double9_2') == null
			assert eo.getAttributeElement('double9_4') == null
			assert eo.getAttributeElement('memo') == null
			assert eo.getAttributeElement('phonenumber') == null
			assert eo.getAttributeElement('reference') == null
			assert eo.getAttributeElement('textlargeobject') == null
			assert eo.getAttributeElement('encryptedtext') == null
			assert eo.getAttributeElement('radio') == null

			eo.delete()
		}

		// FIXME option group tests broken, but work manual
		_45_testOptionGroup:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			assert !eo.dirty
			def webElements = $$('p-radiobutton[name="attribute-radio"]')

			assert webElements.size() == 3: 'There should be 3 radio button options'
			WebElement we1 = $('#Test-1')
			WebElement we3 = $('#Test-3')
			WebElement we59 = $('#Test-59')

			assert we1 != null
			assert we3 != null
			assert we59 != null

			assert !we1.isSelected()
			assert we3.isSelected()
			assert !we59.isSelected()

			webElements[0].click()

			Object test = eo.getAttribute('radio', WebTextfield)
			assert test == 1 || test == '1' // Now value für OptionGroup "RadioTest" is 1

			assert eo.dirty

			assert we1.isSelected()
			assert !we3.isSelected()

			eo.setAttribute('radio', 20, WebTextfield)

			assert !we1.isSelected()
			assert !we59.isSelected()

			eo.setAttribute('radio', 59, WebTextfield)

			assert we59.isSelected()

			test = eo.getAttribute('radio', WebTextfield)
			assert test == 59 || test == '59' // Now value für OptionGroup "RadioTest" is 59

			eo.cancel()

			assert !eo.dirty

			test = eo.getAttribute('radio', WebTextfield)
			assert test == 3 || test == '3' // Now value für OptionGroup "RadioTest" is 3 again

			assert !we1.isSelected()
			assert we3.isSelected()
			assert !we59.isSelected()

		}

		_46_testRoOptionGroup:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			assert !eo.dirty
			def webElements = $$('p-radiobutton[name="attribute-radioro"]')

			assert webElements.size() == 3: 'There should be 3 radio button options'

			WebElement we1 = $('#Test-RO-1')
			WebElement we3 = $('#Test-RO-3')
			WebElement we59 = $('#Test-RO-59')

			assert we1 != null
			assert we3 != null
			assert we59 != null

			assert we1.isSelected()
			assert !we3.isSelected()
			assert !we59.isSelected()

			webElements[2].click()

			Object test = eo.getAttribute('radioro', WebTextfield)
			assert test == 1 || test == '1'

			assert !eo.dirty
		}

		_50_generationInPopup:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.clickButton('Generation button (popup)')

			messageModal.confirm()

			assert driver.windowHandles.size() == 2
			switchToOtherWindow()

			Dimension size = driver.manage().window().size
			assert size.width == 400

			// Window is a bit higher than the configured 300 pixels
			// because of the address bar and title of the browser.
			assert size.height >= 300 && size.height <= 350

			switchToOtherWindow()
			closeOtherWindows()
		}

		_55_readonlyInputComponents:
		{
			StateComponent.changeStateByNumeral(30)

			List<NuclosWebElement> inputs = $('nuc-layout').$$('input')
			assert inputs.size() > 10
			inputs.each {
				// Input components should be set to "readonly" if not writable,
				// but not completely disabled, so the user can still copy text.
				// See NUCLOS-4648
				if (!it.getAttribute('class').contains('ag-checkbox-input') && it.getAttribute('class') != '') {
					if (it.getAttribute('type') != 'checkbox' && it.getAttribute('type') != 'radio') {
						assert it.enabled: 'Component ' + it.getId() + ' is not enabled'
						assert it.getAttribute('readonly') == 'true': 'Component ' + it.getId() + ' has no correct readonly flag'
					} else {
						assert !it.enabled: 'Component ' + it.getId() + ' is not disabled'
					}
				}
			}
		}

		_060_starOnMandatoryFieldLabel:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
			eo.addNew()
			assert eo.getLabelText('text') == 'Text'

			nuclosSession.setSystemParameters([
					'ADD_STAR_TO_MANDATORY_FIELD_LABEL': 'true'
			])
			refresh()

			assert eo.getLabelText('text') == 'Text *'

			nuclosSession.setSystemParameters([
					'ADD_STAR_TO_MANDATORY_FIELD_LABEL': 'false'
			])
			refresh()

			assert eo.getLabelText('text') == 'Text'
		}
		_064_subformBorder: {
			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			eo.selectTab('Documents')
			Subform subform = eo.getSubform(TestEntities.ORG_NUCLOS_SYSTEM_GENERALSEARCHDOCUMENT, 'genericObject')

			assert subform.subformElement.getCssValue('border-style') == 'solid': 'Subform should have a border with style \'solid\''
			assert subform.subformElement.getCssValue('border-color') == 'rgb(0, 153, 255)': 'Subform border color does not match expected color'
			assert subform.subformElement.getCssValue('border-width') == '4px': 'Subform border size does not match expected size'
		}
		_065_panelBackground: {
			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			NuclosWebElement colorPanel = $('nuc-web-component[id^="ColorPanel"] div div')
			assert colorPanel.getCssValue('background-color') == colorSelect: 'Panel background color incorrect'
		}
	}

	@Test
	void testMultiselectionComponent() {
		RESTHelper.createUser('testctrl', 'testctrl', ['Example controlling', 'Example readonly'], nuclosSession)
		createTestTags: {
			EntityObject coupon = new EntityObject(TestEntities.EXAMPLE_REST_DISCOUNTCOUPON)
			coupon.setAttribute('code', '20Y')
			coupon.setAttribute('active', true)
			coupon.setAttribute('discount', 0.9)
			coupon.setAttribute('description', '20 years of nuclos discount code')
			nuclosSession.save(coupon)

			EntityObject coupon2 = new EntityObject(TestEntities.EXAMPLE_REST_DISCOUNTCOUPON)
			coupon2.setAttribute('code', 'SPRING')
			coupon2.setAttribute('active', true)
			coupon2.setAttribute('discount', 0.8)
			coupon2.setAttribute('description', 'Frühjahrsrabatt')
			nuclosSession.save(coupon2)

			EntityObject tagErp = new EntityObject(TestEntities.EXAMPLE_REST_TAG)
			tagErp.setAttribute('name', 'ERP')
			nuclosSession.save(tagErp)

			EntityObject tagAddon = new EntityObject(TestEntities.EXAMPLE_REST_TAG)
			tagAddon.setAttribute('name', 'Addon')
			nuclosSession.save(tagAddon)

			EntityObject shipmentDownload = new EntityObject(TestEntities.EXAMPLE_REST_SHIPMENTTYPE)
			shipmentDownload.setAttribute('name', 'Download')
			shipmentDownload.setAttribute('active', true)
			shipmentDownload.setAttribute('digital', true)
			nuclosSession.save(shipmentDownload)

			EntityObject shipmentHardware = new EntityObject(TestEntities.EXAMPLE_REST_SHIPMENTTYPE)
			shipmentHardware.setAttribute('name', 'Versand')
			shipmentHardware.setAttribute('active', true)
			shipmentHardware.setAttribute('digital', false)
			nuclosSession.save(shipmentHardware)
		}

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		eo.addNew()

		eo.setAttribute('articleNumber', 31415)
		eo.setAttribute('name', 'Nuclos Web Addon')
		eo.setAttribute('price', 12)

		waitForAngularRequestsToFinish()

		MultiselectionCombobox multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleTag_article')
		testMultiselectPageObject: {
			assert multiselectionCombobox != null
			assert !multiselectionCombobox.isAddVisible(): 'When not hovered the add button should not be visible'

			multiselectionCombobox.hoverLovOverlay(true)
			assert multiselectionCombobox.isAddVisible(): 'When hovered, the add button should be visible'
		}

		testCreateNewReferenceValue: {
			multiselectionCombobox.getAddElement(true).click()

			waitUntilTrue({ -> driver.getWindowHandles().size() == 2 })

			String oldWindow = driver.windowHandle
			String newWindow = switchToOtherWindow()

			waitUntilTrue({ -> oldWindow != newWindow })

			EntityObjectComponent newTagEo = EntityObjectComponent.forDetail()

			// create new tag entry
			waitUntilTrue({newTagEo.getAttributeElement('name') != null})
			newTagEo.setAttribute('name', 'Software')

			// save and close window
			newTagEo.save()

			driver.switchTo().window(oldWindow)

			waitForAngularRequestsToFinish()
		}

		// click and assert second item has got selected
		testSelectEntry: {
			multiselectionCombobox.selectEntry('ERP')
			eo.save()
			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleTag_article')
			multiselectionCombobox.selectEntry('Software')
			eo.save()
		}

		// click and assert item has got removed
		testDeselectEntry: {
			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleTag_article')
			multiselectionCombobox.selectEntry('ERP')
		}

		testChoiceOrder: {
			assert multiselectionCombobox.getChoices() == ['Addon', 'ERP', 'Software']: 'Choices should be sorted alphabetically'
		}

		testFilter: {
			multiselectionCombobox.search('E')

			assert multiselectionCombobox.getChoices().size() == 2: 'Only the 2 entries with the letter \'e\' should be visible'

			multiselectionCombobox.search('Add')

			assert multiselectionCombobox.getChoices().size() == 1: 'Only one choice should be displayed, with this filter'
			assert multiselectionCombobox.getChoices().get(0) == 'Addon'
			multiselectionCombobox.search('')
		}

		testSelectAll: {
			multiselectionCombobox.toggleSelectAll()

			assert multiselectionCombobox.getChipCount() == 3: 'All 3 entries should be selected and displayed as a chip.'
			eo.save()
		}

		testDeselectAll: {
			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleTag_article')
			multiselectionCombobox.toggleSelectAll()

			assert multiselectionCombobox.getChipCount() == 0: 'After deselecting there should be no chip element.'
			eo.cancel()
		}

		testBetweenEntries: {
			assert multiselectionCombobox.getChipCount() == 3: 'All 3 entries should be selected and displayed as a chip.'

			Sidebar.selectEntry(3)
			assert multiselectionCombobox.getChipCount() == 0: 'No entries should be selected.'

			Sidebar.selectEntry(0)
			assert multiselectionCombobox.getChipCount() == 3: 'All 3 entries should be displayed again after switching to the entry'
		}

		testDeselectAllSave: {
			multiselectionCombobox.toggleSelectAll()
			eo.save()

			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleTag_article')
			assert multiselectionCombobox.getChipCount() == 0: 'After deselecting and saving there should be no chip element.'
		}

		testMultiselectionComponentFocusManagement: {
			eo.getAttributeElement('price').focus()

			sendKeys(Keys.TAB, Keys.TAB)
			sendKeys(Keys.SPACE)

			assert multiselectionCombobox.isOpen(): 'Typing space should open the multiselection combobox overlay'

			sendKeys(Keys.ESCAPE)

			assert !multiselectionCombobox.isOpen(): 'Typing escape should close the opened multiselection combobox overlay'

			sendKeys(Keys.TAB)
		}

		testDynamicToolTip: {
			sendKeys(Keys.SPACE)
			assert multiselectionCombobox.nuclosWebElement.getTooltip() == 'Nuclos Web Addon'
			sendKeys(Keys.SPACE)
		}

		testValuelistProvider: {
			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleShipmentType_article')
			waitUntilTrue {
				sendKeys(Keys.ESCAPE)
				multiselectionCombobox.getChoices() == ['VLPText: Download']
			}
			assert multiselectionCombobox.getChoices() == ['VLPText: Download']: '\'VLPText: Download\' item expected'

			sendKeys(Keys.ESCAPE)
			eo.setAttribute('category', 'Hardware')

			waitForAngularRequestsToFinish()

			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleShipmentType_article')
			waitUntilTrue {
				sendKeys(Keys.ESCAPE)
				multiselectionCombobox.getChoices() == ['VLPText: Versand']
			}
			assert multiselectionCombobox.getChoices() == ['VLPText: Versand']: '\'VLPText: Versand\' item expected'

			eo.cancel()
		}

		EntityObjectComponent layoutComponentsEo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		layoutComponentsEo.addNew()
		multiselectionCombobox = MultiselectionCombobox.findByAttribute('org_nuclos_system_Statehistory_genericObject')

		testDisabledMultiselect: {
			assert multiselectionCombobox == null: 'Disabled Multiselect component should be hidden'
		}
		testBackgroundColor: {
//			assert multiselectionCombobox.nuclosWebElement.$("p-multiSelect").getCssValue('background-color') == colorInput:
//					"Multi selection Combobox should have correct background-color"
			layoutComponentsEo.cancel()
		}
		testInSubformOverlay: {
			EntityObjectComponent eoWithSubformOverlay = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)

			EntityObjectModal eoModal = EntityObjectComponent.forModal({
				eo.getSubform(TestEntities.EXAMPLE_REST_ARTICLE, 'category').newRow()
			})
			eoModal.setAttribute('articleNumber', 12341324)
			eoModal.setAttribute('name', 'Test Software')
			eoModal.setAttribute('price', 12);
			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleTag_article')
			multiselectionCombobox.selectEntry('ERP')
			multiselectionCombobox.selectEntry('Addon')

			EntityObjectModal.clickButtonOk()
			eoWithSubformOverlay.save()

			eoWithSubformOverlay = EntityObjectComponent.forDetail()
			EntityObjectComponent.forModal({
				eo.getSubform(TestEntities.EXAMPLE_REST_ARTICLE, 'category').getRow(0).editInModal()
			})
			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleTag_article')
			assert multiselectionCombobox.getChipCount() == 2: 'There should be 2 chips in the modal multiselection component'

			EntityObjectModal.clickButtonCancel()

			EntityObjectComponent.forModal({
				eo.getSubform(TestEntities.EXAMPLE_REST_ARTICLE, 'category').newRow()
			})

			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleTag_article')
			multiselectionCombobox.selectEntry('ERP')
			multiselectionCombobox.selectEntry('Software')
			EntityObjectModal.clickButtonCancel()

			EntityObjectComponent.forModal({
				eo.getSubform(TestEntities.EXAMPLE_REST_ARTICLE, 'category').getRow(0).editInModal()
			})

			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_ArticleTag_article')
			assert multiselectionCombobox.getChipCount() == 0: 'When cancelling the modal, it should reset the newly selected entries, when opening again.'

			eoWithSubformOverlay.cancel()
			eo.cancel()
		}
		testMultiselectInStatefulEntity: {
			logout()
			login('testctrl', 'testctrl')
			EntityObjectComponent eoWithSubformOverlay = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

			eo.selectTab('Rechnungen')
			waitForAngularRequestsToFinish()

			Subform subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_INVOICE, 'order')
			// opens the first modal
			EntityObjectComponent.forModal({subform.newRow()})

			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_OrderCoupon_order')

			EntityObjectModal.clickButtonCancel()
			eo.cancel()

			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_OrderCoupon_order')
			multiselectionCombobox.selectEntry('20Y')

			eoWithSubformOverlay.save()
			StateComponent.changeStateByNumeral(80)
			StateComponent.confirmStateChange()

			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_OrderCoupon_order')
			assert multiselectionCombobox.nuclosWebElement.$('p-multiSelect div.p-disabled')

			multiselectionCombobox = MultiselectionCombobox.findByAttribute('example_rest_OrderCoupon_order')
		}
		testMultiselectInStatefulEntityOverlay: {
			logout()
			login('test', 'test')
			EntityObjectComponent eoWithSubformOverlay = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)

			eo.selectTab('Auftrag')
			Subform subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_AUFTRAG, 'kategorie')
			// opens the first modal
			EntityObjectComponent.forModal({subform.newRow()})
			assert $('.no-multiselect-in-overlay')

			EntityObjectModal.clickButtonCancel()
			eo.cancel()
		}
	}

	@Test
	void testHideComponentsWorks() {
		EntityObject testLayoutComponentsVisible = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		testLayoutComponentsVisible.setAttribute('text', 'visible')
		testLayoutComponentsVisible.setAttribute('checkbox', true)
		testLayoutComponentsVisible.setAttribute('radioro', 1)
		nuclosSession.save(testLayoutComponentsVisible)

		EntityObject testLayoutComponentsInvisible = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		testLayoutComponentsInvisible.setAttribute('text', 'invisible')
		nuclosSession.save(testLayoutComponentsInvisible)
		testLayoutComponentsInvisible.changeState(20)

		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS, (long) testLayoutComponentsVisible.getId())
		Sidebar.selectEntry(0)

		assert $('web-optiongroup fieldset') == null: 'Option Group fieldset element should be hidden, when component is hidden'
		assert $('web-multiselection-combobox div') == null: 'Multiselection Combobox div should be hidden, when component is hidden'
		assert $('web-hyperlink div') == null: 'Hyperlink div should be hidden, when component is hidden'
		assert $('web-checkbox div') == null: 'Checkbox div should be hidden, when component is hidden'
		assert $('web-colorchooser .input-group') == null: 'Color Chooser .input-group Element should be hidden, when component is hidden'
	}

	@Test
	void testBackgroundColorInInputs() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		waitForAngularRequestsToFinish()

		assert $("div[name='attribute-text'] input").getCssValue('background-color') == colorSelect:
				"VLP Selection should have correct background-color"

		assert $("input[name='attribute-date']").getCssValue('background-color') == colorDate_File:
				"Date input should have correct background-color"

		assert $("div[name='attribute-documentfile']").getCssValue('background-color') == colorDate_File:
				"File input should have correct background-color"

		assert $("input[name='attribute-email']").getCssValue('background-color') == colorInput:
				"Email input should have correct background-color"

		assert $("input[name='attribute-integer']").getCssValue('background-color') == colorInput:
				"Integer input should have correct background-color"

		assert $("input[name='attribute-hyperlink']").getCssValue('background-color') == colorInput:
				"Hyperlink input should have correct background-color"

		assert $("input[name='attribute-checkbox']").getParent().getCssValue('background-color') == colorInput:
				"Checkbox input should have correct background-color"

		assert $("input[name='attribute-double9_2']").getCssValue('background-color') == colorInput:
				"Double(9, 2) input should have correct background-color"

		assert $("textarea[name='attribute-memo']").getCssValue('background-color') == colorInput:
				"Memo textarea should have correct background-color"

		assert $("input[name='attribute-phonenumber']").getCssValue('background-color') == colorInput:
				"Phonenumber input should have correct background-color"

		assert $("div[name='attribute-reference'] input").getCssValue('background-color') == colorSelect:
				"Reference Selection should have correct background-color"

		assert $("button[name='attribute-static-button']").getCssValue('background-color') == colorSelect:
				"Reference Selection should have correct background-color"

		assert $("input[name='attribute-textcolor']").getCssValue('background-color') == colorInput:
				"Reference Selection should have correct color"

		assert $("label[name='attribute-static-label']").getCssValue('background-color') == colorSelect:
				"Static Label should have correct background-color"

		eo.cancel()
	}

	@Test
	void testTextColors() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		waitForAngularRequestsToFinish()

		assert $("button[name='attribute-static-button']").getCssValue('color') == colorInput:
				"Reference Selection should have correct color"

		assert $("input[name='attribute-textcolor']").getCssValue('color') == colorSelect:
				"Reference Selection should have correct color"

		assert $("label[name='attribute-static-label']").getCssValue('color') == colorInput:
				"Static Label should have correct color"

		eo.cancel()
	}

	@Test
	void testBorders() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		waitForAngularRequestsToFinish()

		assert $("label[data-name='date']").getCssValue('border-color') == color_Border:
				"Date should have correct border color"

		assert $("label[data-name='date']").getCssValue('border-width') == size_Border:
				"Date should have correct border size"

		assert $("label[name='attribute-Label_NTcD']").getCssValue('border-color') == "rgb(255, 0, 0)"
		assert $("label[name='attribute-Label_NTcD']").getCssValue('border-width') == "5px"

		eo.cancel()
	}

	@Test
	void testLoV_NUCLOS9202() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		ListOfValues customerLoV = ListOfValues.findByAttribute('customer')

		customerLoV.search('VLPText: Test-Customer 2')
		customerLoV.selectEntry('VLPText: Test-Customer 2')

		assert EntityObjectComponent.dirty: 'LoV should have changed and marked EO dirty'
		assert EntityObjectComponent.forDetail().getAttribute('customer') == 'VLPText: Test-Customer 2': 'LoV should have updated'

		customerLoV.selectEntry('VLPText: Test-Customer')

		assert EntityObjectComponent.forDetail().getAttribute('customer') == 'VLPText: Test-Customer': 'Changing value via dropdown after search is possible'
	}

	@Test
	void testLoV_NUCLOS9213() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		ListOfValues customerLoV = ListOfValues.findByAttribute('customer')
		customerLoV.search('VLPText: Test-Customer 2')
		sendKeys(Keys.ENTER)
		sendKeys(Keys.TAB)

		assert EntityObjectComponent.dirty: 'LoV should have changed and marked EO dirty'
		assert EntityObjectComponent.forDetail().getAttribute('customer') == 'VLPText: Test-Customer 2': 'LoV value should have been saved'

		EntityObjectComponent.forDetail().save()
		assert !EntityObjectComponent.dirty: 'EO should have been saved'
		assert EntityObjectComponent.forDetail().getAttribute('customer') == 'Test-Customer 2': 'LoV value should have been saved'
	}

	@Test
	void testImageReadOnly_NUCLOS9440() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

		eo.addNew()

		assert eo.getImageComponent("image").isReadOnly(): "Image should be readonly and not present div text"
		assert eo.getImageComponent("image").openContextMenu() == null: "ContextMenu should also not open when readonly"

		eo.cancel()
	}

	/**
	 * See NUCLOS-10343 for more information about this issue.
	 * This test verifies that an inactive image column has a transparent background and thus does not occlude the grids column color.
	 */
	@Test
	void testSubformDisabledImageColumnNoBackground() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()
		waitForAngularRequestsToFinish()
		eo.selectTab('Subform Images')

		Subform imageSubform = EntityObjectComponent.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTSUBFORMIMAGES, 'parent')
		SubformRow subformRow = imageSubform.newRow()
		assert subformRow.getFieldElement('inactiveimage').$('.fileupload-container.disabled').getCssValue('background').contains('none'): 'Subform Image Component should have no background'
		assert subformRow.getFieldElement('inactiveimage').$('.fileupload-container.disabled').getCssValue('border-style') == 'none': 'Border Style \'none\' should be set'
	}

	@Test
	void testButtonStates() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

		eo.addNew()
		assert eo.getButton('Rule button').enabled
		assert eo.getButton('Dummy Button 2').enabled
		assert eo.getButton('Colored Static Button').enabled
		assert !eo.getButton('Hyperlink button').enabled
		assert !eo.getButton('Dummy button').enabled
		assert !eo.getButton('Generation button').enabled
		assert !eo.getButton('State change button').enabled
		assert !eo.getButton('Generation button (popup)').enabled

		eo.setAttribute('text', 'testButtonStates')
		assert !eo.getButton('Hyperlink button').enabled

		eo.setAttribute('hyperlink', 'www.nuclos.de')
		assert eo.getButton('Hyperlink button').enabled

		eo.save()
		assert eo.getButton('Rule button').enabled
		assert eo.getButton('Dummy Button 2').enabled
		assert eo.getButton('Colored Static Button').enabled
		assert eo.getButton('Dummy button').enabled
		assert eo.getButton('Generation button (popup)').enabled
		assert eo.getButton('Generation button').enabled
		assert eo.getButton('Hyperlink button').enabled
		assert !eo.getButton('State change button').enabled

		eo.setAttribute('hyperlink', '')
		assert !eo.getButton('Dummy button').enabled
		assert !eo.getButton('Hyperlink button').enabled

		eo.cancel()
	}

	/**
	 * Verifies that the label of the button is centered vertically.
	 * @see <a href="https://support.nuclos.de/browse/NUCLOS-10408">NUCLOS-10408</a>
	 */
	@Test
	void testButtonCenteredText() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

		eo.addNew()

		NuclosWebElement buttonElement = eo.getButton('Generation button')
		NuclosWebElement outerElement = buttonElement.$('.button-container')
		NuclosWebElement innerElement = buttonElement.$('.label')

		assert Math.abs(outerElement.location.y - innerElement.location.y) <= 1 : "Web-Button label div is not vertically centered"
	}

	@Test
	void testSplitpane() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)

		SplitpaneComponent splitpaneComponent = SplitpaneComponent.getByName('SplitPane_W4Mv')

		assert splitpaneComponent != null: 'Splitpane should be found'
		assert splitpaneComponent.gutterSize == 15: 'Splitter Gutter should have 15px size'
		assert splitpaneComponent.getPanelSize(0) == 50: 'First pane should exactly use half size on initial load'
		assert splitpaneComponent.getPanelSize(1) == 50: 'Second pane should exactly use half size on initial load'

		splitpaneComponent.resizePanels(0, 20)
		assert splitpaneComponent.getPanelSize(0) == 20: 'First pane should now only be 20 percent'
		assert splitpaneComponent.getPanelSize(1) == 80: 'Second pane should be 80 percent'

		splitpaneComponent.resizePanels(0, 80)
		assert splitpaneComponent.getPanelSize(0) == 80: 'First pane should now only be 80 percent'
		assert splitpaneComponent.getPanelSize(1) == 20: 'Second pane should be 20 percent'

		waitForAngularRequestsToFinish()
		refresh()
		waitForAngularRequestsToFinish()

		splitpaneComponent = SplitpaneComponent.getByName('SplitPane_W4Mv')
		assert splitpaneComponent.getPanelSize(0) == 80: 'First pane should now only be 80 percent'
		assert splitpaneComponent.getPanelSize(1) == 20: 'Secound pane should be 20 percent'

		splitpaneComponent.collapsePanel(0)
		assert splitpaneComponent.getPanelSize(0) == 0: 'First pane should be collapsed'
		assert splitpaneComponent.getPanelSize(1) == 100: 'Second pane should be expanded'

		splitpaneComponent.expandPanel(0)
		assert splitpaneComponent.getPanelSize(0) == 100: 'First pane should be expanded'
		assert splitpaneComponent.getPanelSize(1) == 0: 'Second pane should be collapsed'
	}

	@Test
	void testTabbedPane() {
		// test the new coloring feature for single tab headers
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		waitForAngularRequestsToFinish()

		testTabColorStatic: {
			assert eo.getTab('Subform Images').$('.tab-background').getCssValue('background-color') == colorInput:
					"Subform Images tab should have correct color"

			assert eo.getTab('Subform Date').$('.tab-background').getCssValue('background-color') == colorSelect:
					"Subform Date tab should have correct color"

			// Hover the tab with the mouse
			eo.getTab('Subform Images').$('.tab-background').mouseover()
			assert eo.getTab('Subform Images').$('.tab-background').getCssValue('background-image') == gradientTabHover:
					"Hovered Subform Images tab should have correct linear gradient applied."

			// Click & select the tab
			eo.selectTab('Subform Images')
			// for better visibility focus now also has the hover gradientTab
			// for this test to work we need to click some html outside so the according selected gradient is applied
			$("body").click()
			assert eo.getTab('Subform Images').$('.tab-background').getCssValue('background-image') == gradientTabSelected:
					"Selected Subform Images tab should have correct linear gradient applied."

		}
		testTabLabelFromAttributeNoSubform: {
			eo.setAttribute('text', 'dynamic title')
			assert eo.hasTab('dynamic title'): 'Tab Title not matching with attribute value'

			eo.save()
			eo.addNew()

			assert eo.hasTab(''): 'Tab Title not matching with empty attribute value'
			eo.setAttribute('text', 'custom title')
			eo.cancel()

			assert eo.hasTab('dynamic title'): 'Tab Title not matching with attribute value after switching entry'
		}
		testTabLabelFromAttributeSubform: {
			eo.addNew()
			eo.setAttribute('text', 'subform tab title test')
			eo.setAttribute('memo', 'custom title')
			assert eo.hasTab('custom title'): 'Tab Title not matching with attribute value'

			eo.save()
			eo.addNew()

			assert eo.hasTab(''): 'Tab Title not matching with empty attribute value'
			eo.setAttribute('memo', 'test title')
			eo.cancel()

			assert eo.hasTab('custom title'): 'Tab Title not matching with attribute value after switching entry'
		}
	}

	/**
	 * <p>
	 * Verify that it is not possible to save values not present in a valuelist, when insertable = false.
	 * </p>
	 * Following scenario should now be fixed:
	 * <br>
	 * It should not be possible to save arbitrary values for a Combobox on a string attribute, if a valuelist has been set and insertable = false.
	 * @see <a href="https://support.nuclos.de/browse/NUCLOS-10689">NUCLOS-10689</a>
	 */
	@Test
	void testComboboxComponent() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_PROCESSTEST)
		EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()
		eo.setAttribute('name', 'TestCombobox')
		eo.setAttribute('nuclosProcess', 'Testaktion 1')
		assert eo.getAttribute('defaultvlp') == '3 three'

		NuclosWebElement combobox = eo.getAttributeElement('defaultvlp')
		combobox.click()
		sendKeys('ABC')
		eo.save()
		assert eo.getAttribute('defaultvlp') == '3 three': 'Because \'ABC\' is not in the valuelist the value should have been reset to \'3 three\''

		eo.addNew()
		eo.setAttribute('name', 'Combobox Test 2')
		eo.setAttribute('nuclosProcess', 'Testaktion 1')
		eo.save()

		combobox = eo.getAttributeElement('defaultvlp')
		combobox.click()
		// clearing will select the EMPTY entry
		combobox.clear()
		sendKeys('ABC')
		eo.getAttributeElement('defaultvlp2').click()

		assert eo.getAttribute('defaultvlp') == ''
	}

	@Test
	void testTextfelderCombobox() {
		// add bunch of test categories to have a long list
		TestDataHelper.insertTestCategories(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)

		EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()
		eo.setAttribute('articleNumber', 200)
		eo.setAttribute('name', 'TestTextfelderCombobox')
		eo.setAttribute('category', 'Hardware')
		eo.enterText('categorytextual', 'NotInList')
		eo.setAttribute('price', 10)
		eo.save()

		assert eo.getAttribute('categorytextual') == 'NotInList'

		// test for search
		eo.enterText('categorytextual', 'ocean')
		eo.getLOV('categorytextual').selectEntry('ocean')
		eo.save()

		assert eo.getAttribute('categorytextual') == 'ocean'

		eo.enterText('categorytextual', '')
		eo.save()

		assert eo.getAttribute('categorytextual') == ''

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Subform orderPositionSubform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		orderPositionSubform.getRow(0).enterValue('categorytextual', 'NotInList')
		EntityObjectComponent.forDetail().save()
		orderPositionSubform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert orderPositionSubform.getRow(0).getValue('categorytextual') == 'NotInList'

		orderPositionSubform.getRow(0).enterValue('categorytextual', 'ocean')
		EntityObjectComponent.forDetail().save()
		orderPositionSubform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert orderPositionSubform.getRow(0).getValue('categorytextual') == 'ocean'

		orderPositionSubform.getRow(0).openEditor(orderPositionSubform.getRow(0).getFieldElement('categorytextual')).clear()
		EntityObjectComponent.forDetail().save()
		orderPositionSubform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert orderPositionSubform.getRow(0).getValue('categorytextual') == ''
	}

	@Test
	void testRealTooltipPopup() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

		eo.addNew()
		eo.setAttribute('text', 'testingTooltip')
		eo.save()
		waitForAngularRequestsToFinish()

		// hover over date input to trigger tooltip popup and check for its content, too!
		eo.getAttributeElement('date').click()
		waitForAngularRequestsToFinish()
		new Actions(driver)
				.keyDown(Keys.ALT)
				.sendKeys(Keys.ESCAPE)
				.keyUp(Keys.ALT)
				.perform()
		waitForAngularRequestsToFinish()

		waitForNotNull(30, {
			new Actions(driver)
					.moveToElement(
							$('div.fileupload-container[name$="documentfile"]').element
					)
					.perform()
			waitForAngularRequestsToFinish()

			return $('div.p-tooltip')
		})
		def tooltipText = $('div.p-tooltip').text
		assert tooltipText.contains('Document-Tooltip'): 'Wrong tooltipText (' + tooltipText + ' != Document-Tooltip)'
	}

	@Ignore
	@Test
	void testDatechooserFocus() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

		eo.addNew()
		eo.getAttributeElement('text', WebCombobox.class).focus()
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)

		assert checkCurrentActiveElement("input", ["name": "attribute-date"])
		assert !eo.getDatepicker('date').isOpen()

		sendKeys(Keys.ENTER)
		assert eo.getDatepicker('date').isOpen()
	}

	@Test
	void testStaticHTML() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)

		eo.addNew()
		eo.setAttribute('articleNumber', 1)
		eo.setAttribute('name', 'Test 1')
		eo.setAttribute('price', 12)
		def staticHTML = eo.getAttributeElement('HTML_UEvS')
		def value = staticHTML?.getAttribute('innerHTML')
		assert  value == '<div>\n     <h1>Test</h1>\n     <p>\n        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\n     </p>\n     <br>\n     <a href="/#dashboard">Zum Dashboard</a>\n</div>'
		assert staticHTML.getCssValue('color') == 'rgba(0, 102, 153, 1)': 'Color should be set from layout editor'

		eo.setAttribute('description', '<h3>Test2</h3><br><p>Testing test</p>')

		value = eo.getAttributeElement('description', WebHtmlField).getAttribute('innerHTML')
		assert  value == '<h3>Test2</h3><br><p>Testing test</p>': 'HTML should be correctly transported (was: ' + value + ')'

		eo.save()

		eo = eo.addNew()
		value = eo.getAttributeElement('HTML_UEvS')?.getAttribute('innerHTML')
		assert value == '<div>\n     <h1>Test</h1>\n     <p>\n        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\n     </p>\n     <br>\n     <a href="/#dashboard">Zum Dashboard</a>\n</div>'
		value = eo.getAttributeElement('description', WebHtmlField).getAttribute('innerHTML')
		assert value == '': 'No HTML should be present (was: ' + value + ')'
		eo.cancel()
	}

	@Test
	void testButtonDynamicLabelAttribute() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

		eo.addNew()

		// make sure, buttons without a label attribute still use their static label
		assert $("button[name='attribute-static-button']").text == 'Colored Static Button': 'Different text expected for static button'
		// for a new eo the attribute has no value so we expect an empty text (even, if there is a static text defined)
		assert $("button[name='attribute-Button_DynamicLabel']").text.isEmpty()

		eo.setAttribute('text', 'eo1')
		eo.setAttribute('textlargeobject', 'Label aus Attribut')

		assert $("button[name='attribute-Button_DynamicLabel']").text == 'Label aus Attribut': 'Button label should match the new attribute value'

		eo.save()

		eo.addNew()
		eo.setAttribute('textlargeobject', 'TestLabelChange')
		eo.cancel()

		assert $("button[name='attribute-Button_DynamicLabel']").text == 'Label aus Attribut': 'Button label does not match the current attribute value'
	}

	@Test
	void testTabVisibilityByAttribute() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()
		eo.setAttribute('text', 'test tab visibility by attribute')

		assert eo.hasTab('Tab Visibility')

		selectWhileNew: {
			eo.getAttributeElement('checkbox').click()
			assert eo.hasTab('Tab Visibility'): 'While the entry is new, the visibility is not determined by the checkbox value'
		}
		unselectWhileNew: {
			eo.getAttributeElement('checkbox').click()
			assert eo.hasTab('Tab Visibility'): 'While the entry is new, the visibility is not determined by the checkbox value'
		}
		saveUnselected: {
			eo.save()
			eo = EntityObjectComponent.forDetail()
			assert !eo.hasTab('Tab Visibility'): 'Tab should be hidden, after saving with deselected checkbox.'
		}
		selectAndSave: {
			eo.getAttributeElement('checkbox').click()
			eo.save()
			eo = EntityObjectComponent.forDetail()
			assert eo.hasTab('Tab Visibility'): 'Tab should be visible, after saving with selected checkbox.'
		}
		selectTabAndHideItBySaving: {
			eo.selectTab('Tab Visibility')
			eo.getAttributeElement('checkbox').click()
			eo.save()
			assert !eo.hasTab('Tab Visibility'): 'Tab should be hidden and the test should correctly teardown without any exception'
		}
		testVisibilityWhenSwitching: {
			eo = EntityObjectComponent.forDetail()
			eo.addNew()
			eo.setAttribute('text', 'another visibilty entry')
			eo.getAttributeElement('checkbox').click()
			eo.save()
			assert eo.hasTab('Tab Visibility'): 'Tab should be visible for this entry.'
			Sidebar.selectEntry(1)
			assert !eo.hasTab('Tab Visibility'): 'Tab should be hidden for this entry.'
		}
	}

	@Test
	void testHtmlEditorPasteClipboard() {
		// this test makes sure, that the fix for NUCLOS-10117 works
		// when pasting text, the current selection should be replaced by the clipboard content
		// when pasting text without anything selected inside the html-editor, it should not clear the editors text
		EntityObjectComponent eo
		setup: {
			eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

			eo.addNew()
			eo.setAttribute('text', 'testHtmlEditorPasteClipboard')

			eo.selectTab('Subform Memo')

			Subform memoSubform = EntityObjectComponent.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSMEMO, 'parent')

			memoSubform.openViewConfiguration()
			SideviewConfiguration.selectColumn(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSMEMO.fqn + '_htmlmemo')
			SideviewConfiguration.clickButtonOk()

			SubformRow newRow = memoSubform.newRow()
			newRow.clickCell('htmlmemo')

			NuclosWebElement editor = $(".p-editor-content")
			editor.click()
			sendKeys("TextInput")
		}
		copyToClipboard: {
			// uses keyboard shortcuts to select the text inside the editor and copy it to the clipboard
			new Actions(driver)
					.keyDown(Keys.LEFT_CONTROL).sendKeys(Keys.ARROW_LEFT)
					.keyDown(Keys.SHIFT).sendKeys(Keys.ARROW_RIGHT)
					.keyUp(Keys.SHIFT).sendKeys("c")
					.keyUp(Keys.LEFT_CONTROL).build().perform()
		}
		pasteFromClipboard: {
			// sending Ctrl + V twice -> the first time it should replace the current text, because it is selected
			// the second time it should concatenate the editor text with the clipboard text
			new Actions(driver)
					.keyDown(Keys.LEFT_CONTROL)
					.sendKeys("vv")
					.keyUp(Keys.LEFT_CONTROL)
					.build()
					.perform()
		}
		assert $(".ql-editor").text == "TextInputTextInput": 'Pasting should have concatenated the existing text with the clipboard text'

		new Actions(driver).sendKeys(Keys.ESCAPE).build().perform()

		eo.save()
		eo.selectTab('Subform Memo')
		Subform memoSubform = EntityObjectComponent.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSMEMO, 'parent')
		assert memoSubform.getRow(0).getValue('htmlmemo') == "TextInputTextInput"
	}

	@Test
	void testLabelTranslation() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		LocaleChooser.setLocale(Locale.GERMAN)
		waitForAngularRequestsToFinish()

		assert $('label[data-name="orderNumber"]').text.trim() == 'Auftragsnummer'
		assert EntityObjectComponent.forDetail().getAttributeElement('Button_Send', WebButtonExecuteRule).text.trim() == 'Auftrag übermitteln'
		assert EntityObjectComponent.forDetail().getAttributeElement('Button_Lock', WebButtonExecuteRule).text.trim() == 'Sperren'
		assert $('nuc-tab-bar[name="attribute-Tabbedpane_1"] ul li:nth-child(3)').text.trim() == 'Rechnungen'

		LocaleChooser.setLocale(Locale.ENGLISH)
		waitForAngularRequestsToFinish()

		assert $('label[data-name="orderNumber"]').text.trim() == 'Order number'
		assert EntityObjectComponent.forDetail().getAttributeElement('Button_Send', WebButtonExecuteRule).text.trim() == 'Send order'
		assert EntityObjectComponent.forDetail().getAttributeElement('Button_Lock', WebButtonExecuteRule).text.trim() == 'Lock'
		assert $('nuc-tab-bar[name="attribute-Tabbedpane_1"] ul li:nth-child(3)').text.trim() == 'Invoices'
	}

	@Test
	void testBooleanTextfield() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

		LocaleChooser.setLocale(Locale.GERMAN)
		waitForAngularRequestsToFinish()

		def eo = EntityObjectComponent.forDetail()
		eo.addNew()
		eo.setAttribute('text', 'testBooleanTextfield')

		eo.setAttribute('checkbox', true)
		assert eo.getAttribute('checkbox', WebTextfield.class) == 'ja'

		eo.setAttribute('checkbox', false)
		assert eo.getAttribute('checkbox', WebTextfield.class) == 'nein'

		eo.save()

		LocaleChooser.setLocale(Locale.ENGLISH)
		waitForAngularRequestsToFinish()

		eo.setAttribute('checkbox', true)
		assert eo.getAttribute('checkbox', WebTextfield.class) == 'yes'

		eo.setAttribute('checkbox', false)
		assert eo.getAttribute('checkbox', WebTextfield.class) == 'no'
	}

	@Test
	void testSelectTab() {
		String url = "/view/${TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS.fqn}/new?selectTab=Tab 2&selectTab=Comments"
		getUrlHash(url)
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		assert eo.getTab('Tab_2').hasClass('p-highlight')
		assert eo.getTab('Comments').hasClass('p-highlight')

		eo.setAttribute('text', 'test')
		eo.selectTab('Tab 1')
		eo.selectTab('Documents')
		eo.save()
		assert eo.getTab('Tab 1').hasClass('p-highlight')
		assert eo.getTab('Documents').hasClass('p-highlight')

		refresh()
		assert eo.getTab('Tab_2').hasClass('p-highlight')
		assert eo.getTab('Comments').hasClass('p-highlight')
	}

	private Subform getMemoSubform() {
		def eo = EntityObjectComponent.forDetail()

		// TODO
		// tab needs to be scrolled
		// this should be done by the framework automatically
		$('button.p-tabview-nav-next').click()
		eo.selectTab('Subform Memo')

		return eo.getSubform('nuclet_test_other_TestLayoutComponentsMemo_parent')
	}

	private void enterMay1999(Datepicker datepicker) {
		assert datepicker.getYear() == Calendar.getInstance().get(Calendar.YEAR)

		datepicker.input = '99'
		assert datepicker.year == 1999
		assert datepicker.month == 'Jan'

		datepicker.previousMonth()
		assert datepicker.year == 1998
		assert ['Dec', 'Dez'].contains(datepicker.month)

		datepicker.input = '99-5'
		assert datepicker.year == 1999
		assert ['May', 'Mai'].contains(datepicker.month)

		datepicker.nextMonth()
		assert datepicker.month == 'Jun'

		datepicker.commit()
	}
}
