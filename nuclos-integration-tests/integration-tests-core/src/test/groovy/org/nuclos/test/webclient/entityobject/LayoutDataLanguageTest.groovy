package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.DataLanguageModal
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LayoutDataLanguageTest extends AbstractRerunnableWebclientTest {

	private static EntityObject articleType

	@Before
	void setup() {
		articleType = new EntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKELTYP)

		articleType.setAttribute('artikeltyp', 'Digital Product')
		nuclosSession.save(articleType)
	}

	@Test
	void testButtonAndInputAreBothVisible() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		eo.addNew()

		// subtract toggle tip button
		def combinedSize = eo.getAttributeElement('artikelname').getSize().width + $('#artikelname-4 a').getSize().width - 20
		def groupWidth = eo.getAttributeElement('artikelname').parent.getSize().width
		assert combinedSize < groupWidth: 'Both elements should fit within web-textfield element'

		eo.cancel()
	}

	@Test
	void _01_testModifyLocalizableAttributeTextField() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		eo.addNew()
		eo.setAttribute('artikeltyp', 'Digital Product')
		eo.setAttribute('artikelname', 'Online Wörterbuch')
		eo.setAttribute('artikelbeschreibung', 'Test Beschreibung')

		DataLanguageModal languageModal = openDataLanguageModal({
			$('#artikelname-4 a').click()
		})
		assert 'Online Wörterbuch' == languageModal.getTranslation('de_DE')
		assert 'Online Wörterbuch' == languageModal.getTranslation('en_GB')

		languageModal.enterTranslation('de_DE', 'Online Wörterbuch Band 1')
		languageModal.enterTranslation('en_GB', 'Online dictionary volume 1')
		languageModal.clickButtonOk()

		// assert that the attribute value change to the new translation of the current dl 'Online Wörterbuch Band 1'
		assert 'Online Wörterbuch Band 1' == eo.getAttribute('artikelname'): 'artikelname value should have changed to \'Online Wörterbuch Band 1\''

		eo.save()
		eo.reload()
		waitForAngularRequestsToFinish()

		languageModal = openDataLanguageModal({
			$('#artikelname-4 a').click()
		})
		assert 'Online Wörterbuch Band 1' == languageModal.getTranslation('de_DE')
		assert 'Online dictionary volume 1' == languageModal.getTranslation('en_GB')

		languageModal.clickButtonCancel()

	}

	@Test
	void _02_testModifyLocalizableAttributeTextArea() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		eo.addNew()
		eo.setAttribute('artikeltyp', 'Digital Product')
		eo.setAttribute('artikelname', 'Online Wörterbuch')
		eo.setAttribute('artikelbeschreibung', 'Default Artikelbeschreibung')

		DataLanguageModal languageModal = openDataLanguageModal({
			$('#artikelbeschreibung-5 a').click()
		})
		assert 'Default Artikelbeschreibung' == languageModal.getTranslation('de_DE'): 'Modal should contain the initial article description for the german translation'
		assert 'Default Artikelbeschreibung' == languageModal.getTranslation('en_GB'): 'Modal should contain the initial article description for the english translation'

		languageModal.enterTranslation('de_DE', 'Deutschsprachige Beschreibung')
		languageModal.enterTranslation('en_GB', 'Englischsprachige Beschreibung')
		languageModal.clickButtonOk()

		// assert that the attribute value change to the new translation of the current dl 'Online Wörterbuch Band 1'
		assert 'Deutschsprachige Beschreibung' == eo.getAttribute('artikelbeschreibung'): 'artikelbeschreibung value should have changed to \'Deutschsprachige Beschreibung\''

		eo.save()
		eo.reload()

		languageModal = openDataLanguageModal({
			$('#artikelbeschreibung-5 a').click()
		})
		assert 'Deutschsprachige Beschreibung' == languageModal.getTranslation('de_DE'): 'Modal should contain the updated german translation'
		assert 'Englischsprachige Beschreibung' == languageModal.getTranslation('en_GB'): 'Modal should contain the updated english translation'
	}

	@Test
	void _03_testCancelLocalizationModal() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		eo.addNew()
		eo.setAttribute('artikeltyp', 'Digital Product')
		eo.setAttribute('artikelname', 'Online Wörterbuch')
		eo.setAttribute('artikelbeschreibung', 'Test Beschreibung')

		DataLanguageModal languageModal = openDataLanguageModal({
			$('#artikelname-4 a').click()
		})

		assert 'Online Wörterbuch' == languageModal.getTranslation('de_DE'): 'Modal translation should match the original attribute value'
		assert 'Online Wörterbuch' == languageModal.getTranslation('en_GB'): 'Modal translation should match the original attribute value'

		languageModal.enterTranslation('de_DE', 'Online Wörterbuch Band 1')
		languageModal.enterTranslation('en_GB', 'Online dictionary volume 1')
		languageModal.clickButtonCancel()

		assert 'Online Wörterbuch' == eo.getAttribute('artikelname'): 'Attribute value should not have changed'

		languageModal = openDataLanguageModal({
			$('#artikelname-4 a').click()
		})

		assert 'Online Wörterbuch' == languageModal.getTranslation('de_DE'): 'Modal should have reset to the current attribute value'
		assert 'Online Wörterbuch' == languageModal.getTranslation('en_GB'): 'Modal should have reset to the current attribute value'

		eo.cancel()

	}

	@Test
	void _04_testCloneLocalizedEntry() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		eo.addNew()
		eo.setAttribute('artikeltyp', 'Digital Product')
		eo.setAttribute('artikelname', 'Online Wörterbuch')
		eo.setAttribute('artikelbeschreibung', 'Test Beschreibung')

		DataLanguageModal languageModal = openDataLanguageModal({
			$('#artikelname-4 a').click()
		})

		assert 'Online Wörterbuch' == languageModal.getTranslation('de_DE')
		assert 'Online Wörterbuch' == languageModal.getTranslation('en_GB')

		languageModal.enterTranslation('de_DE', 'Online Wörterbuch Band 1')
		languageModal.enterTranslation('en_GB', 'Online dictionary volume 1')
		languageModal.clickButtonOk()

		// assert that the attribute value change to the new translation of the current dl 'Online Wörterbuch Band 1'
		assert 'Online Wörterbuch Band 1' == eo.getAttribute('artikelname'): 'artikelname value should have changed to \'Online Wörterbuch Band 1\''

		eo.save()
		eo.clone()

		waitForAngularRequestsToFinish()

		eo.setAttribute('artikeltyp', 'Digital Product')

		languageModal = openDataLanguageModal({
			$('#artikelname-4 a').click()
		})
		languageModal.enterTranslation('de_DE', 'Berechnungsprogramm')
		languageModal.enterTranslation('en_GB', 'Calculation Software')
		languageModal.clickButtonOk()
		eo.save()

		languageModal = openDataLanguageModal({
			$('#artikelname-4 a').click()
		})
		assert 'Berechnungsprogramm' == languageModal.getTranslation('de_DE')
		assert 'Calculation Software' == languageModal.getTranslation('en_GB')
		languageModal.clickButtonCancel()

		// select previous entry, to verify its translations have not been accidentally modified
		Sidebar.selectEntry(1)

		waitForAngularRequestsToFinish()

		languageModal = openDataLanguageModal({
			$('#artikelname-4 a').click()
		})
		// there was a bug overriding the translations of the source eo, when cloning. So we have to assert the previous translations again
		assert 'Online Wörterbuch Band 1' == languageModal.getTranslation('de_DE')
		assert 'Online dictionary volume 1' == languageModal.getTranslation('en_GB')
		languageModal.clickButtonCancel()
	}

}
