package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.WebclientTestContext
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.HarEntry
import net.lightbody.bmp.proxy.CaptureType

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class MatrixTest extends AbstractRerunnableWebclientTest {

	@Before
	void setup() {
		TestDataHelper.insertTestData(nuclosSession)

		// create new "Matrix" entry
		def matrix = RESTHelper.createBo(
				[
						boMetaId  : 'nuclet_test_matrix_Matrix',
						attributes: [name: 'Test-Matrix']
				],
				nuclosSession
		)

		// create x-axis entries
		def n = ['c', 'a', 'b'];
		def g = ['1', '1', '2'];
		for (int i = 1; i < 4; i++) {
			RESTHelper.createBo(
					[
							boMetaId  : 'nuclet_test_matrix_XAchse',
							attributes: [
									name: 'x-axis-value-' + n[i - 1],
									gruppe: 'gruppe-' + g[i - 1]
							]
					],
					nuclosSession
			)
		}

		// create new y-axis entries and asign to matrix
		for (int i = 0; i < 30; i++) {
			RESTHelper.createBo(
					[
							boMetaId  : 'nuclet_test_matrix_YAchse',
							attributes: [
									name  : 'y-axis-value-' + i,
									matrix:
											[id: matrix.get('boId'), name: ''],
									nuclosrowcolor: i == 1 ? '#8888cc' : null
							]
					],
					nuclosSession
			)

		}
	}

	@Test
	void runTest() {
		List<Coordinate> testCoordinates = []

		(1..2).each { row ->
			(1..2).each { col ->
				testCoordinates << new Coordinate(row as Integer, col as Integer)
			}
		}

	_03_selectTripleStateIcons: {
		logout()
		login('nuclos', '')

		EntityObjectComponent eo
		countBrowserRequests {
			eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_MATRIX_MATRIX)
		}.with {
			assert it.getRequestCount(RequestType.EO_READ) == 1
		}

		assert !eo.dirty

		Sidebar.selectEntry(0)

		// state 2 = ( )
		assert isMatrixInState(1, 1, 2)

		// click all matrix cells to State (+)
		testCoordinates.each {
			assert matrixClick(it.row, it.col)
			assert isMatrixInState(it.row, it.col, 3)
		}
		screenshot('matrix-cells-clicked.to.plus')

		// Next State (-)
		testCoordinates.each {
			assert matrixClick(it.row, it.col)
			assert isMatrixInState(it.row, it.col, 4)
		}
		screenshot('matrix-cells-clicked.to.minus')

		assert eo.dirty

		// NUCLOS-9231 Don't forfeit when changing tabs
		NuclosWebElement tab1 = EntityObjectComponent.getTab('Matrix')
		assert tab1 != null

		NuclosWebElement tab2 = EntityObjectComponent.getTab('Subform')
		assert tab2 != null

		tab2.click()

		assert matrixHeader(1) == null

		tab1.click()

		assert matrixHeader(1) != null
		assert eo.dirty
		// End NUCLOS-9231

		countBrowserRequests {
			eo.save()
		}.with {
			assert it.getRequestCount(RequestType.EO_UPDATE) == 1
		}
		waitForAngularRequestsToFinish()
		assert !eo.dirty

		// Still all should be in state (-)
		testCoordinates.each {
			assert isMatrixInState(it.row, it.col, 4)
		}

		// Next State ( )
		testCoordinates.each {
			assert matrixClick(it.row, it.col)
			assert isMatrixInState(it.row, it.col, 2)
		}

		assert eo.dirty
		screenshot('matrix-cells-clicked-again')

		eo.save()
		waitForAngularRequestsToFinish()
		assert !eo.dirty

		// Still all should be unchecked
		testCoordinates.each {
			assert isMatrixInState(it.row, it.col, 2)
		}

		NuclosWebElement row0 = $('nuc-web-matrix .ag-center-cols-container .ag-row[row-index="0"]')
		assert row0.getCssValue("background-color") != 'rgba(136, 136, 204, 1)'
		assert row0.getCssValue("color") == 'rgba(0, 0, 0, 1)': 'Black text color expected'

		NuclosWebElement row1 = $('nuc-web-matrix .ag-center-cols-container .ag-row[row-index="1"]')
		assert row1.getCssValue("background-color") == 'rgba(136, 136, 204, 1)'
		assert row1.getCssValue("color") == 'rgba(255, 255, 255, 1)': 'White text color expected'
	}

	_04_checkNewAndMultiColumn: {
		// NUCLOS-9049 add rows
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert getLeftColText(0) == 'y-axis-value-0'

		NuclosWebElement newButton = getNewRowButton()
		assert newButton != null && newButton.isEnabled()

		NuclosWebElement deleteButton = getDeleteRowButton()
		assert deleteButton != null && !deleteButton.isEnabled()

		newButton.click()
		sendKeys('data')
		sendKeys(Keys.ENTER)
		assert eo.dirty

		assert getLeftColText(0) == 'data'

		eo.save()

		assert !eo.dirty

		assert getLeftColText(0) == 'y-axis-value-0'

		// delete row 5
		NuclosWebElement fifth = matrixLeftCol(5)
		assert fifth != null
		assert fifth.getText() == 'y-axis-value-5'

		NuclosWebElement firstColFifthRow = $(fifth, '.ag-cell:nth-child(1)')
		assert firstColFifthRow != null

		firstColFifthRow.click()
		assert deleteButton.isEnabled()
		deleteButton.click()
		assert eo.dirty

		eo.save()

		fifth = matrixLeftCol(5)
		assert fifth != null
		assert fifth.getText() == 'y-axis-value-6'

		// NUCLOS-9001 MultiColumn Y-Axis
		NuclosWebElement leftCol = matrixLeftCol(0)
		NuclosWebElement yAchseCell = $zZz(leftCol, 'div.ag-cell[aria-colindex="3"]')

		yAchseCell.click()
		sendKeys('7')
		sendKeys(Keys.ENTER)

		eo.save()

		assert getLeftColText(0) == 'y-axis-value-0\n7'

	}

	_05_checkGrouping: {
		// NUCLOS-9037 X-Axis grouping
		String header1 = getHeaderText(1)
		String header2 = getHeaderText(2)

		assert header1 == 'x-axis-value-c'
		assert header2 == 'x-axis-value-b'

		WebElement cell = matrixHeader(3)
		assert cell == null

		cell = matrixGroupHeader(1)
		assert cell != null

		WebElement text = $zZz(cell, '.ag-header-group-cell-label .ag-header-group-text')
		assert text != null
		assert text.getText() == 'gruppe-1'

		cell.click()

		header1 = getHeaderText(1)
		header2 = getHeaderText(2)
		String header3 = getHeaderText(3)

		// The columns are c, a, b after expanding, but the div sort is c, b, a
		assert header1 == 'x-axis-value-c'
		assert header2 == 'x-axis-value-b'
		assert header3 == 'x-axis-value-a'
	}

	_06_selectCheckicons: {

		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		eo.selectProcess('Matrix-Checkicon-Aktion')

		assert !isMatrixChecked(1, 1)

		// click all matrix cells
		testCoordinates.each {
			assert matrixClick(it.row, it.col)
			assert isMatrixChecked(it.row, it.col)
		}
		assert eo.dirty
		screenshot('matrix-cells-clicked')

		countBrowserRequests {
			eo.save()
		}.with {
			assert it.getRequestCount(RequestType.EO_UPDATE) == 1
		}

		assert !eo.dirty

		// Still all should be checked
		testCoordinates.each {
			assert isMatrixChecked(it.row, it.col)
		}

		// Reload the matrix because the events are not always processed correctly
		refresh()
		// Uncheck all
		testCoordinates.each {
			assert matrixClick(it.row, it.col)
			assert !isMatrixChecked(it.row, it.col)
		}
		assert eo.dirty
		eo.save()
		assert !eo.dirty

		// Still all should be unchecked
		testCoordinates.each {
			assert !isMatrixChecked(it.row, it.col)
		}
	}

	_08_testAxisSorting: {
		// NUCLOS-6621
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		screenshot('select-process-text')
		eo.selectProcess('Matrix-Text-Aktion')
		screenshot('process-text-selected')

		assert eo.dirty

		String header1 = getHeaderText(1)
		String header2 = getHeaderText(2)
		String header3 = getHeaderText(3)

		assert header1 == 'x-axis-value-a'
		assert header2 == 'x-axis-value-b'
		assert header3 == 'x-axis-value-c'

		getLeftColText(1)
		String rowheader1 = getLeftColText(0)
		String rowheader2 = getLeftColText(1)
		String rowheader3 = getLeftColText(2)

		// Note: This is pure alpabetical sorting, not natural, which would yield 30, 29, 28
		assert rowheader1 == 'y-axis-value-9'
		assert rowheader2 == 'y-axis-value-8'
		assert rowheader3 == 'y-axis-value-7'

	}

	_10_changeMatrixTextValues: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		testCoordinates.each {
			new Actions(driver).moveToElement(matrixCell(it.row, it.col).element, 20, 20).click().perform()
			sendKeys("${it.row}, ${it.col}")
			sendKeys(Keys.ENTER)
		}

		eo.save()
		assert !eo.dirty

		//TODO: Test saved data

		screenshot('process-text-input')
	}

	_11_changeMatrixTextValues_NUCLOS_6199: {
		// TEST NUCLOS-6199
		EntityObjectComponent.refresh()
		assert matrixClick(3, 1)

		sendKeys('3, 1')
		screenshot('process-text-input-matrix-cell')
		assert matrixClick(3, 2)
		screenshot('process-matrix-cell-click-after-input')

		String textIn31 = matrixCell(3, 1).text
		assert textIn31

		if (textIn31 == '2, 1') {
			//This is some unexplained behavior: It is in wrong row (one too high)
			screenshot('process-text-input-matrix-wrongvalue')
			textIn31 = matrixCell(4, 1).text
		}

		assert textIn31 == '3, 1'
	}

		_12_testComboboxFeature:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			eo.selectProcess('Matrix-Combobox-Aktion')

			assert matrixClick(3, 1)
			NuclosWebElement cell = matrixCell(3, 1)

			def lov = ListOfValues.fromElement('undefined', $("div[name='attribute-undefined']"))
			waitUntilTrue({
				lov.isOpen()
			})
			failsafeAssert({ return lov.choiceElements.size() > 0 }, "Lov should be visible with elements")
			failsafeAssert({ return lov.choiceElements.text.containsAll(["Test-Customer", "Test-Customer 2", "Test-Customer inactive"]) }, "LoV should contain: Test-Customer, Test-Customer 2, Test-Customer inactive")
			lov.selectEntry('Test-Customer')

			failsafeAssert({ return cell.text == 'Test-Customer' }, "LoV Element should be saved into matrix")

			eo.save()
			assert !eo.dirty
		}
	}

	static class Coordinate {
		int row
		int col

		Coordinate(final int row, final int col) {
			this.row = row
			this.col = col
		}
	}

	boolean matrixClick(int row, int column) {
		$xXx('nuc-web-matrix .ag-center-cols-container .ag-row[row-index="' + row + '"] .ag-cell:nth-child(' + column + ')').clickWithWait()
	}

	// TODO: Old
	NuclosWebElement matrixCell(int row, int column) {
		$('nuc-web-matrix .ag-center-cols-container .ag-row[row-index="' + row + '"] .ag-cell:nth-child(' + column + ')')
	}

	boolean isMatrixInState(int row, int column, int state) {
		WebElement cell = matrixCell(row, column);
		switch (state) {
			case 1: return $(cell, '.fa-check-square-o') != null
			case 2: return $(cell, '.fa-square-o') != null
			case 3: return $(cell, '.fa-plus') != null
			case 4: return $(cell, '.fa-minus') != null
		}
		return $(cell, '.fa-square-o') != null
	}

	// TODO: Old
	boolean isMatrixChecked(int row, int column) {
		return isMatrixInState(row, column, 1);
	}

	NuclosWebElement matrixLeftCol(int row) {
		$('nuc-web-matrix .ag-pinned-left-cols-container div.ag-row[row-index="' + row + '"]')
	}

	String getLeftColText(int row) {
		WebElement cell = matrixLeftCol(row)
		return cell.getText()
	}

	NuclosWebElement matrixGroupHeader(int col) {
		$('nuc-web-matrix .ag-header-row .ag-header-group-cell-with-group:nth-child(' + col + ')')
	}

	NuclosWebElement matrixHeader(int col) {
		$('nuc-web-matrix .ag-header-row .ag-header-cell-grouped:nth-child(' + col + ')')
	}

	String getHeaderText(int col) {
		WebElement cell = matrixHeader(col)
		WebElement text = $(cell, '.ag-header-cell-label .ag-header-cell-text')
		return text.getText()
	}

	NuclosWebElement getNewRowButton() {
		$('nuc-matrix-buttons .new-subbo')
	}

	NuclosWebElement getDeleteRowButton() {
		$('nuc-matrix-buttons .delete-selected-subbos')
	}

	@Test
	void testSelectionBoxConfiguration() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_MATRIX_MATRIX)
		waitForAngularRequestsToFinish()

		assert $('nuc-web-matrix .ag-header-select-all').isDisplayed(): 'Per default matrix selection should be available'
		getNuclosSession().setSystemParameters(['nuclos_LAF_Webclient_Matrix_Show_Selection_Column': 'false'])

		refresh()
		assert !$('nuc-web-matrix .ag-header-select-all').isDisplayed(): 'Disabling it should hide it correctly'
	}

	@Test
	void testTabChangeShouldNotThrowAwayMatrixData() {
		logout()
		login('nuclos', '')

		EntityObjectComponent.open(TestEntities.NUCLET_TEST_MATRIX_MATRIX)
		waitForAngularRequestsToFinish()


		matrixClick(1, 1)

		assert EntityObjectComponent.isDirty(): 'Data change occurred, eo should be modified'
		assert isMatrixInState(1, 1, 3): 'Changed state is 3'

		EntityObjectComponent.selectTab('Subform')
		assert EntityObjectComponent.isDirty(): 'Data still dirty'

		EntityObjectComponent.selectTab('Matrix')
		assert isMatrixInState(1, 1, 3): 'Changes should be still visible'

		EntityObjectComponent.forDetail().cancel()
		assert isMatrixInState(1, 1, 2): 'Changes should be discarded correctly'
		assert !EntityObjectComponent.isDirty(): 'EO should not be marked as dirty anymore'
	}

	@Test
	void testZMatrixReadonly() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_MATRIX_MATRIX)

		assert !eo.dirty
		Sidebar.selectEntry(0)

		assert eo.getCancelButton() == null
		assert eo.getDeleteButton() == null

		// state 2 = ( )
		assert isMatrixInState(1, 1, 2)

		matrixClick(1, 1)
		assert isMatrixInState(1, 1, 2)

		assert !eo.dirty
		assert getNewRowButton() == null
		assert getDeleteRowButton() == null

	}
}
