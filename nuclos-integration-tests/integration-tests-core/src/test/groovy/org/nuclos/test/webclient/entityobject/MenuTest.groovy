package org.nuclos.test.webclient.entityobject

import java.awt.Menu
import java.util.concurrent.TimeoutException

import org.apache.commons.lang.StringUtils
import org.json.JSONArray
import org.json.JSONObject
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.MenuComponent
import org.openqa.selenium.Dimension

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class MenuTest extends AbstractRerunnableWebclientTest {

	@Test
	void runTest() {
		_00_openMenuEntry: {
			MenuComponent.openMenu('Example', 'Article')
			assert getCurrentUrl().endsWith('Article')
		}

		_05_openSubMenuEntry: {
			MenuComponent.openMenu('Matrix', 'Unterobjekte', 'X-Achse')
			assert getCurrentUrl().endsWith('XAchse')
		}

		_10_hiddenDynamicTasklist: {
			JSONObject menu = nuclosSession.menuStructure

			checkMenuForNull(menu)
		}
	}

	@Test
	void testClearMenuAfterFormerSuperuserLogin() {
		logout()
		login('nuclos', '')

		MenuComponent.assertConfigurationAvailable()

		logout()
		login('test','test')
		try {
			MenuComponent.assertConfigurationNotAvailable()
		} catch (AssertionError | TimeoutException error) {
			//do nothing
		}
	}

	@Test
	void superUserMenuItems() {
		//logged in as 'test'
		MenuComponent.assertMaintenanceModeNotAvailable()
		MenuComponent.assertServerInfoNotAvailable()
		if (nuclosSession.isDevMode()) {
			MenuComponent.assertDevUtilsAvailable()
		}

		logout()
		login('nuclos', '')
		MenuComponent.assertMaintenanceModeAvailable()
		MenuComponent.assertServerInfoAvailable()
		if (nuclosSession.isDevMode()) {
			MenuComponent.assertDevUtilsAvailable()
		}
	}

	@Test
	void testNucletMenuItemInAdministration() {
		//logged in as 'test'
		MenuComponent.assertAdministrationMenuItemAvailable('MenuTest', 'MenuTest')
		MenuComponent.assertAdministrationMenuItemNotAvailable('MenuTest2', 'MenuTest2')

		logout()
		login('nuclos', '')
		MenuComponent.assertAdministrationMenuItemAvailable('MenuTest', 'MenuTest')
		MenuComponent.assertAdministrationMenuItemAvailable('MenuTest2', 'MenuTest2')
	}

	@Test
	void testOverLongMenu() {
		resizeBrowserWindow(1000, 700)

		MenuComponent.openMenu('zzZZVeryVeryVeryVeryVeryLongMenuEntry', 'TestMenuOverLong')
		assert getCurrentUrl().endsWith('TestMenuOverLong')

		MenuComponent.toggleUserMenu()
	}

	@Test
	void testMenuLexicalOrder() {
		waitForNotNull(10, {$('#Dropdown-MenüTest')}).click()
		assert $$('#Dropdown-MenüTest ~ ul.dropdown-menu nuc-menu-item li').collect {it.$('a.dropdown-item').getText()} == ['Bla', 'Blä', 'Blo']
	}

	void checkMenuForNull(JSONObject menu) {
		String label = menu.getString('name')
		assert label != 'null'
		assert StringUtils.isNotBlank(label)

		if (menu.has('entries')){
			JSONArray entries = menu.getJSONArray('entries')
			(0..entries.length()-1).each {
				checkMenuForNull(entries.getJSONObject(it))
			}
		}
	}
}
