package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ModifiableTest extends AbstractRerunnableWebclientTest {

@Test
void runTest() {
	Long customerId

	_10_insertCustomer: {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		eo.addNew()
		eo.setAttribute('customerNumber', 123)
		eo.setAttribute('name', 123)
		eo.save()

		customerId = eo.id
	}

	_20_checkIsModifiableBO: {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Log.debug 'checkIsModifiableBO'

		eo.addNew()
		screenshot('checkIsModifiableBO-a')
		eo.checkField('notmodifiable', true, false)
		eo.checkField('nuclosState', true, false)
		eo.checkField('nuclosProcess', true, true)

		eo.setAttribute('orderNumber', 42)
		eo.getLOV('customer').selectEntry('VLPText: 123')

		eo.save()
		screenshot('checkIsModifiableBO-b')
		eo.checkField('notmodifiable', true, false)
	}

	_25_checkIsModifiableSubBO: {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER, customerId)

		eo.selectTab('Orders')
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER, 'customer')

		assert subform.getRow(0).getFieldElement('notmodifiable').getCssValue('background-color')?.contains('rgba(230, 230, 230, 1)')
	}
}
}
