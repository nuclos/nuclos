package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.WebclientTestContext
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.EntityObjectDialog
import org.nuclos.test.webclient.pageobjects.GenerationComponent
import org.nuclos.test.webclient.pageobjects.Searchtemplate
import org.nuclos.test.webclient.pageobjects.search.Searchbar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class NavigationTest extends AbstractRerunnableWebclientTest {

	@Before
	void setup() {
		assertLoggedIn('test')
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void navigateToEmptyBoList() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		assert eo.getDeleteButton() != null
		assert eo.getNewButton() != null

		// navigate to an entity with no data
		eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CITY)

		assert eo.getDeleteButton() == null // no item in list -> no first item to select -> no delete button
		assert eo.getNewButton() != null
	}

	@Test
	void navigateToMissingBo() {
		/**
		 * This will test that if we navigate to a missing BO that there is a error message.
		 * And if we re-navigate to top eo meta view the list is correctly loaded
		 */
		EntityObject<Long> notFoundEO = new EntityObject<>(TestEntities.EXAMPLE_REST_ORDER)
		notFoundEO.boId = 1200L

		EntityObjectComponent.open(notFoundEO)

		assert $('#error-container .card-header')?.text == 'Fehler'
		assert $('#error-container .card-body')?.text == 'Das angeforderte Objekt wurde nicht gefunden.'

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		assert $('#error-container') == null
	}

	@Test
	void navigatePerLinkToEntryWithSearchFilterEmpty() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		def currentEOUrl = driver.getCurrentUrl()
		def currentEONumber = eo.getAttribute('orderNumber')

		// Set SearchFilter
		Searchbar.openSearchEditor()
		Searchbar.clear()

		Searchbar.selectAttribute('Order number')
		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'orderNumber',
						operator: '=',
						value: '0'
				)
		)

		waitForAngularRequestsToFinish()

		driver.navigate().to(currentEOUrl)
		waitForAngularRequestsToFinish()

		def currentEO = EntityObjectComponent.forDetail()

		assert currentEO != null: 'There should be an entity result detail'
		assert currentEO.getAttribute('orderNumber') == currentEONumber: 'Navigating to this URL with active filter and no result should work'
	}

	@Test
	void navigateWithinTabAfterGenerationInDialog() {
		RESTClient client = new RESTClient('nuclos', '').login()
		WebclientTestContext context = WebclientTestContext.instance

		def nucletParameter = client.getEntityObject(SystemEntities.NUCLET_PARAMETER, '7p5r4bYdVt7v5xefYM5U')
		nucletParameter.setAttribute('value', context.nuclosWebclientServer)
		nucletParameter.save()
		client.logout()

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_NAVIGATIONTEST)
		eo.addNew()
		eo.setAttribute('name', 'navigateWithinTabAfterGenerationInDialog1')
		eo.save()
		Long eo1Id = eo.getId()

		eo = eo.addNew()
		eo.setAttribute('name', 'navigateWithinTabAfterGenerationInDialog2')
		eo.save()

		GenerationComponent.generateObjectAndConfirm('NavigationTestGen', false)

		EntityObjectDialog eoDialog = EntityObjectComponent.forDialog()
		eoDialog.setAttribute('name', 'NavigationTestGen1')
		Subform genSubForm = eoDialog.getSubform(TestEntities.NUCLET_TEST_OTHER_NAVIGATIONTESTGENSUB, 'navigationtestgen')
		SubformRow genSub = genSubForm.newRow()
		genSub.enterValue('name', 'NavigationTestGenSub1.1', false)
		assert !eo.getCancelButton(): 'Parent EO must not be dirty when EO in dialog is edited.'

		eoDialog.clickButtonOk()
		eo = EntityObjectComponent.forDetail()
		assert eo.getId() == eo1Id: 'The webclient should navigate to the first created EO after confirming (and therefore saving) the generated object.'
	}
}
