package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.MenuComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class NewsTest extends AbstractRerunnableWebclientTest {

	@Before
	@Override
	void beforeEach() {
		AbstractWebclientTest.setup(false, false)
	}

	@Test
	void runTest() {
		String title = 'Privacy Policy Title'
		String messageSource = '''Content

More text...'''
		String message = '''Content

More text...'''

		_05_openNews:
		{
			login('nuclos')

			MenuComponent.toggleAdminMenu()

			$('a[href*=org_nuclos_system_News]').click()
		}

		_10_createNews:
		{
			def eo = EntityObjectComponent.forDetail()

			eo.addNew()

			eo.setAttribute('name', 'Privacy Policy')
			eo.setAttribute('title', title)
			eo.setAttribute('content', messageSource)
			eo.setAttribute('active', true)
			eo.setAttribute('showAtStartup', false)
			eo.setAttribute('confirmationRequired', true)
			eo.setAttribute('privacyPolicy', true)

			// "confirmationRequired" implies "showAtStartup" - checked by server rule
			assert !eo.getAttribute('showAtStartup', null, Boolean.class)
			eo.save()
			assert eo.getAttribute('showAtStartup', null, Boolean.class)

			(1..2).each {
				eo.addNew()

				eo.setAttribute('name', "News after Login $it")
				eo.setAttribute('title', "News $it")
				eo.setAttribute('content', "News $it ...")
				eo.setAttribute('active', true)
				eo.setAttribute('showAtStartup', true)
				eo.setAttribute('confirmationRequired', false)

				eo.save()
			}
		}

		_15_loginAndDecline:
		{
			logout()
			login('test', 'test')

			def modal = getMessageModal()
			assert modal.title == title
			assert modal.message == message

			modal.decline()
			assertLoggedOut()

			assert !org.nuclos.test.server.NewsTest.privacyConsentForTest
		}

		_20_loginAndConfirm:
		{
			assertMessageModalAndConfirm(title, message, { login('test', 'test') })
			assertLoggedIn('test')

			assert org.nuclos.test.server.NewsTest.privacyConsentForTest
		}

		_25_expectNews:
		{
			(1..2).each {
				def modal = getMessageModal()
				assert modal.title == "News $it"
				assert modal.message == "News $it ..."

				modal.confirm()
				waitForAngularRequestsToFinish()
				// maybe angular is too fast now
				sleep(250)
			}
			assertLoggedIn('test')

			assert !messageModal
		}

		_testNewsPositioningDropdown:
		{
			logout()
			waitForAngularRequestsToFinish()
			assert $('#news > li > div').hasClass("dropdown-menu-start"): 'When not logged in news menu dropdown should start at the beginning of toggle'

			login('test', 'test')
			waitForAngularRequestsToFinish()
			assert $('#news > li > div').hasClass("dropdown-menu-end"): 'When logged in news menu dropdown should start at the end of toggle'
		}
	}
}
