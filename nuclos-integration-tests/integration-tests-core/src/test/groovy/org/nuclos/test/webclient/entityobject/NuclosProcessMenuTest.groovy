package org.nuclos.test.webclient.entityobject


import java.util.concurrent.TimeoutException

import org.apache.commons.lang.StringUtils
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.MenuComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.search.Searchbar

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class NuclosProcessMenuTest extends AbstractRerunnableWebclientTest {

	@Before
	void initTestData() {
		TestDataHelper.insertNuclosProcessTestData(nuclosSession)
	}

	@Test
	void testMenuNew() {
		MenuComponent.openMenu('Example', 'New Order')
		assert driver.getCurrentUrl().endsWith('example_rest_Order/new')
		def eo = EntityObjectComponent.forDetail()
		assert eo.getCancelButton()
		assert eo.getAttribute('nuclosProcess') == ''
	}

	@Test
	void testMenuNewWithAction() {
		MenuComponent.openMenu('Example', 'New Standing Order')
		assert driver.getCurrentUrl().endsWith('example_rest_Order/new?processMetaId=example_rest_Order_Standingorder')
		def eo = EntityObjectComponent.forDetail()
		assert eo.getCancelButton()
		assert eo.getAttribute('nuclosProcess') == 'Standing order'

		Searchbar.openSearchEditor()
		assert Searchbar.getValueMultiselect(TestEntities.EXAMPLE_REST_ORDER, 'nuclosProcess').getTextValue() == 'Standing order'
	}

	@Test
	void testMenuWithAction() {
		MenuComponent.openMenu('Example', 'Priority Order')
		assert driver.getCurrentUrl().endsWith('processMetaId=example_rest_Order_Priorityorder')
		def eo = EntityObjectComponent.forDetail()
		assert eo.getAttribute('nuclosProcess') == 'Priority order'
		assert eo.getAttribute('orderNumber') == '2'

		Searchbar.openSearchEditor()
		assert Searchbar.getValueMultiselect(TestEntities.EXAMPLE_REST_ORDER, 'nuclosProcess').getTextValue() == 'Priority order'

		MenuComponent.openMenu('Example', 'Standing Order')
		assert driver.getCurrentUrl().endsWith('processMetaId=example_rest_Order_Standingorder')
		eo = EntityObjectComponent.forDetail()
		Sidebar.selectEntry(0);
		assert eo.getAttribute('nuclosProcess') == 'Standing order'
		assert eo.getAttribute('orderNumber') == '3'

		Searchbar.openSearchEditor()
		assert Searchbar.getValueMultiselect(TestEntities.EXAMPLE_REST_ORDER, 'nuclosProcess').getTextValue() == 'Standing order'
	}

	@Test
	void testMenuWithoutAction() {
		MenuComponent.openMenu('Example', 'Priority Order')
		assert driver.getCurrentUrl().endsWith('processMetaId=example_rest_Order_Priorityorder')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		assert !driver.getCurrentUrl().endsWith('processMetaId=example_rest_Order_Priorityorder')
		assert Sidebar.listEntryCount == 4

		Searchbar.openSearchEditor()
		assert Searchbar.getValueMultiselect(TestEntities.EXAMPLE_REST_ORDER, 'nuclosProcess') == null
	}
}
