package org.nuclos.test.webclient.entityobject

import java.text.SimpleDateFormat

import org.apache.commons.lang3.tuple.Pair
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common2.IOUtils
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.PrintoutComponent
import org.openqa.selenium.WebElement

import com.google.common.collect.Iterators
import com.xlson.groovycsv.CsvIterator
import com.xlson.groovycsv.CsvParser
import com.xlson.groovycsv.PropertyMapper

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.HarEntry
import net.lightbody.bmp.core.har.HarRequest

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class PrintoutTest extends AbstractRerunnableWebclientTest {

	@Before
	void setup() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
	}

	@Test
	void _01_outputFormatOrder() {
		PrintoutComponent.open()
		assert PrintoutComponent.getPrintoutName(2) == '1'
		assert PrintoutComponent.getPrintoutName(3) == '2'
		assert PrintoutComponent.getPrintoutName(4) == '3'
		assert PrintoutComponent.getPrintoutName(5) == 'A'
		assert PrintoutComponent.getPrintoutName(6) == 'C'
		assert PrintoutComponent.getPrintoutName(7) == 'b'
		PrintoutComponent.closeModal()
	}

	@Test
	void _10_printPDF() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		PrintoutComponent.open()

		PrintoutComponent.selectPrintout(0)

		def downloadUrl = execute()

		// Download file and check content
		byte[] data = downloadFile(downloadUrl)
		PDDocument document = PDDocument.load(data)
		PDFTextStripper pdfStripper = new PDFTextStripper()
		String text = pdfStripper.getText(document)
		try {
			assert text.contains('Order')
			assert text.contains(eo.getAttribute('orderNumber') as String)
			assert text.contains('Created')
			assert text.contains(new SimpleDateFormat('yyyy-MM-dd').format(new Date()))
		} catch (Throwable t) {
			final File failure = new File('failure.pdf')
			IOUtils.writeToBinaryFile(failure, data)
			throw t
		}

		//modal already closed?
		waitUntilTrue({$('#button-ok') == null}, 5)
	}

	@Test
	void _15_printXLSX() {
		PrintoutComponent.open()
		PrintoutComponent.selectPrintout(1)
		PrintoutComponent.execute()

		PrintoutComponent.open()
		PrintoutComponent.selectPrintout(1)
		selectDropdownValue('Software')

		countBrowserRequests {
			PrintoutComponent.execute()
		}.with {
			assert it.getRequestCount(RequestType.EO_PRINTOUT_RESULT) == 1
		}

		//modal already closed?
		waitUntilTrue({$('nuc-printout-dialog') == null}, 5)
	}

	@Test
	void testOutputFormatParameters() {
		PrintoutComponent.open()

		PrintoutComponent.selectPrintout(8)
		Map<String, List<String>> mpDefaultParameters = [
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:articleCategory' : ['Article category with default value', 'Software'],
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:articleCategory2' : ['Article category with another default value', 'Hardware'],
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:articleCategory3' : ['Article category without default value', ''],
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:boolean' : ['boolean', 'true'],
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:date' : ['date', ''],
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:double' : ['double', ''],
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:long' : ['long', ''],
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:text' : ['text', '']
		]

		def parameters = PrintoutComponent.outputFormatParameters
		for (Map.Entry<String, List<String>> parameter : mpDefaultParameters) {
			assert parameters.containsKey(parameter.key)
			assert parameters.get(parameter.key).left == parameter.value[0]
			assert parameters.get(parameter.key).right == parameter.value[1]
		}

		Map<String, String> mpParameters = [
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:articleCategory' : 'Software',
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:articleCategory2' :  'Hardware',
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:articleCategory3' : 'Nuclos',
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:boolean' : 'true',
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:date' : '31.12.2021',
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:double' : '123.45',
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:long' : '12345',
				'example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:text' : 'abcdef'
		]

		mpParameters.each { Map.Entry<String, String> parameter ->
			PrintoutComponent.setParameterValue(parameter.key, parameter.value)
		}

		def downloadUrl = execute()
		byte[] data = downloadFile(downloadUrl)

		CsvIterator iter = CsvParser.parseCsv(new InputStreamReader(new ByteArrayInputStream(data)), separator: ';') as CsvIterator

		PropertyMapper mapper = iter.next() as PropertyMapper;

		Map<String, String> expectedFormatting = mpParameters;
		expectedFormatting['example_rest_PrintoutOutputFormatParameterTestPO_Parameter_Test:double'] = '123,45'

		for (parameter in expectedFormatting) {
			def key = parameter.key.substring(parameter.key.indexOf(':') + 1)
			assert mapper[key] == (key != 'boolean' ? parameter.value : 'ja')
		}
	}

	private static String execute() {
		getBrowserHar {
			PrintoutComponent.execute()
		}.with {
			return it.log.entries.find { HarEntry entry -> entry.request.method == 'GET' && entry.request.url.contains('boPrintouts') }.request.url
		}
	}

	private WebElement findElementContainingText(String cssSelector, String text) {
		return $$(cssSelector).find { it.text == text }
	}

	private void selectDropdownValue(String value) {
		waitFor {
			$('.modal .p-autocomplete-dropdown').click()
			$('.modal .p-autocomplete-panel')?.isDisplayed()
		}
		findElementContainingText('.modal .p-autocomplete-items li div', value).click()
	}
}
