package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.MenuComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ProcessTest extends AbstractRerunnableWebclientTest {
	@Test
	void changeProcess() {
		withNewEntity: {
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_PROCESSTEST)
			eo.addNew()
			assert eo.getAttribute('nuclosProcess') == ''
			assert eo.getAttribute('defaultvlp2') == '3 three'
			eo.getLOV('defaultvlp2').selectEntry('4 four')
			assert !eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PROCESSTESTSUB, 'processtestref')
			assert !eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PROCESSTESTSUB2, 'processtestref')

			eo.setAttribute('name', 'ProcessTest')

			eo.setAttribute('nuclosProcess', 'Testaktion 1')
			assert eo.getAttribute('defaultvalue') == 'Default'
			assert eo.getAttribute('defaultvlp2') == '4 four'
			assert eo.getAttribute('defaultvlp') == '3 three'
			assert eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PROCESSTESTSUB, 'processtestref')
			assert !eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PROCESSTESTSUB2, 'processtestref')
			eo.setAttribute('defaultvalue', '')

			eo.setAttribute('nuclosProcess', 'Testaktion 2')
			assert eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PROCESSTESTSUB2, 'processtestref')
			assert !eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PROCESSTESTSUB, 'processtestref')

			eo.save()

			eo.addNew()
			eo.setAttribute('nuclosProcess', 'Testaktion 1')
			assert eo.getAttribute('refvlpdefault') == 'ProcessTest'
			eo.cancel()
		}

		withExistingEntity: {
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.setAttribute('nuclosProcess', 'Testaktion 1')
			assert eo.getAttribute('defaultvalue') == ''
			assert eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PROCESSTESTSUB, 'processtestref')
			assert !eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PROCESSTESTSUB2, 'processtestref')

			eo.setAttribute('nuclosProcess', 'Testaktion 2')
			assert eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PROCESSTESTSUB2, 'processtestref')
			assert !eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PROCESSTESTSUB, 'processtestref')
		}
	}
}
