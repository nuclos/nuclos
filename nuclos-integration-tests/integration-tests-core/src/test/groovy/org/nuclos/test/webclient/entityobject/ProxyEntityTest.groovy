package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ProxyEntityTest extends AbstractRerunnableWebclientTest {

@Test
void runTest() {
	_00_openProxyContainer: {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPROXYCONTAINER)

		assert Sidebar.listEntryCount == 2 : 'Dynamic entity "Test Proxy Container" should have 2 records'
	}

	_05_checkProxyContainer1: {
		Sidebar.selectEntryByText('Test')

		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		waitUntilTrue({ -> eo.getAttribute('name') == 'Test'})

		Subform subform = eo.getSubform('nuclet_test_other_TestProxy_proxycontainer')
		waitUntilTrue({-> subform.rowCount == 2})
		assert !subform.newVisible
		assert !subform.cloneVisible
		assert !subform.deleteVisible

		assert subform.getRow(0).getValue('name') == 'Test Proxy 2'
		assert subform.getRow(1).getValue('name') == 'Test Proxy 1'
	}

	_10_checkProxyContainer2: {
		Sidebar.selectEntryByText('Test 2')

		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		waitUntilTrue({ -> eo.getAttribute('name') == 'Test 2'})

		Subform subform = eo.getSubform('nuclet_test_other_TestProxy_proxycontainer')
		assert subform.rowCount == 1
		assert !subform.newVisible
		assert !subform.cloneVisible
		assert !subform.deleteVisible

		assert subform.getRow(0).getValue('name') == 'Test Proxy 3'
	}
}
}
