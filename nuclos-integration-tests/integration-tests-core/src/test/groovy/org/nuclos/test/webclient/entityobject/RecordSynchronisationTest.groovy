package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common.E
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RecordSynchronisationTest extends AbstractRerunnableWebclientTest {

	@Before
	void setupData() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void _01_editAttributes() {
		EntityObjectComponent eo1 = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.open()
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.toString() + '_calculated')
		SideviewConfiguration.clickButtonOk()

		duplicateCurrentWindow()
		EntityObjectComponent eo2 = EntityObjectComponent.forDetail()

		def noteValue = 'Test _01_editAttributes()'
		eo2.setAttribute('note', noteValue)
		eo2.save()

		assert eo2.getAttribute('calculated') == noteValue

		switchToOtherWindow()
		eo1 = EntityObjectComponent.forDetail()
		assert eo1.getAttribute('calculated') == noteValue

		// ignore for now because of NUCLOS-9539
		// assert Sidebar.getValueByColumnLabel(0, 'Calculated') == noteValue
	}

	@Test
	void _02_lock() {
		EntityObjectComponent eo1 = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		duplicateCurrentWindow()
		EntityObjectComponent eo2 = EntityObjectComponent.forDetail()

		lock:
		{
			eo2.getButton('Sperren').click()
			assert $('#lock-info')

			switchToOtherWindow()
			assert $('#lock-info')
		}

		unlock:
		{
			$('#unlock-button').click()
			waitForAngularRequestsToFinish()
			waitFor(5, { $('#unlock-button') == null })

			switchToOtherWindow()
			waitFor(5, { $('#unlock-button') == null })
		}
	}

	@Test
	void _03_stateChange() {
		EntityObjectComponent eo1 = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		duplicateCurrentWindow()
		EntityObjectComponent eo2 = EntityObjectComponent.forDetail()

		StateComponent.changeStateByNumeral(90)
		StateComponent.confirmStateChange()
		switchToOtherWindow()

		assert StateComponent.currentStateNumber == 90

		StateComponent.changeStateByNumeral(95)
		StateComponent.confirmStateChange()
		switchToOtherWindow()

		assert StateComponent.currentStateNumber == 95
	}

	@Test
	void _04_stateRelatedRestrictions() {
		EntityObjectComponent eo1 = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		eo1.addNew()
		eo1.setAttribute('text', '_04_stateRelatedRestrictions')
		eo1.save()

		duplicateCurrentWindow()
		EntityObjectComponent eo2 = EntityObjectComponent.forDetail()
		StateComponent.changeStateByNumeral(20)
		assert eo2.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform') == null
		assert !eo2.getButton('Rule Subform read-only').enabled

		switchToOtherWindow()
		eo1 = EntityObjectComponent.forDetail()
		assert eo1.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform') == null
		assert !eo1.getButton('Rule Subform read-only').enabled

		StateComponent.changeStateByNumeral(30)
		StateComponent.confirmStateChange()
		assert eo1.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent') == null
		assert eo1.getAttributeElement('text') == null

		switchToOtherWindow()
		eo2 = EntityObjectComponent.forDetail()
		assert eo2.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent') == null
		assert eo2.getAttributeElement('text') == null

		StateComponent.changeStateByNumeral(10)
		StateComponent.confirmStateChange()
		StateComponent.changeStateByNumeral(40)
		StateComponent.confirmStateChange()
		assert !eo2.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent').isNewVisible()

		switchToOtherWindow()
		eo1 = EntityObjectComponent.forDetail()
		assert !eo1.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent').isNewVisible()
	}

	@Test
	void _05_editSubformEntryInWindow() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		Subform subform = eo.getSubform('example_rest_CustomerAddress_customer')

		SubformRow row = subform.getRow(0)
		String oldStreet = row.getValue('street')

		assert oldStreet

		String newStreet = 'Another Street'
		assert oldStreet != newStreet

		/**
		 * Edit the row in a new Window.
		 * After saving in the other window, the changes should be synchronized back to this subform.
		 */
		editStreetInOtherWindow:
		{
			row.editInWindow()

			switchToOtherWindow()

			EntityObjectComponent detail = EntityObjectComponent.forDetail()
			detail.setAttribute('street', 'Another Street')
			detail.save()

			switchToOtherWindow()
		}

		assert row.getValue('street') == newStreet

		/**
		 * Edit again directly in subform to check that there are no version conflicts.
		 */
		editSubformEntryInSubform:
		{
			String newStreet2 = 'Another Street 2'
			row.enterValue('street', newStreet2)
			assert row.dirty

			eo.save()
			row = subform.getRow(0)

			assert !eo.dirty
			assert eo.getErrorMessages().empty

			assert row.getValue('street') == newStreet2
		}
	}
}
