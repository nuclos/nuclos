package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestCounts
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ResultRequestTest extends AbstractRerunnableWebclientTest {

@Test
void runTest() {
	_00_setup: {
		TestDataHelper.insertTestData(nuclosSession)
	}

	_10_testOpeningEntityClass: {
		RequestCounts requests = countBrowserRequests {
			EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		}

		// There should be exactly 1 request for the result list
		assert requests.getRequestCount(RequestType.EO_READ_LIST) == 1

		// There should be exactly 1 request when the first result is auto-selected
		assert requests.getRequestCount(RequestType.EO_READ) == 1
	}

	_20_testSwitchingEntityClass: {
		countBrowserRequests {
			EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		}.with {
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			assert it.getRequestCount(RequestType.EO_READ) == 1
		}
		countBrowserRequests {
			EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		}.with {
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			assert it.getRequestCount(RequestType.EO_READ) == 1
		}
	}
}
}
