package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.search.Searchbar

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RulesFromExtensionTest extends RulesTest {

    String ok() { return "ext-ok" }

    boolean regelnAusExtensionVerwenden() { return true }

    String adjustCustomRuleButtonLabel(String sLabel) { return sLabel+"Ext" }

	boolean fromExtension() {
		true
	}
}
