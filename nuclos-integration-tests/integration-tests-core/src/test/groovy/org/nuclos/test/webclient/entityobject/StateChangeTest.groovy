package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.StateComponent

import groovy.transform.CompileStatic

/**
 * Tests state changes via the state-component.
 *
 * TODO: Also test numerals
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class StateChangeTest extends AbstractRerunnableWebclientTest {


@Test
void runTest() {
	_01_setup: {
		nuclosSession.setSystemParameters(['nuclos_LAF_Show_State_Combobox_Threshold': '0'])
		LocaleChooser.locale = Locale.GERMANY

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE)
		eo.addNew()

		assert !StateComponent.getNextStates(true)

		// Assert all state change buttons in the layout are disabled
		def buttons = StateComponent.stateButtons
		assert buttons*.label.toSet() == ['Status A', 'Status B', 'Status C', 'Status ☠', 'Status D', 'Status E'].toSet()
		assert !buttons.find { it.enabled }

		eo.setAttribute('name', 'test')
		eo.save()
		waitForAngularRequestsToFinish()
	}

	_02_stateComponentReadOnly: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		def elemState = eo.getAttributeElement('nuclosState')
		def attrReadonly = elemState.$('input').getAttribute('readonly')
		assert attrReadonly == 'true'
	}

	_05_assertStateA: {
		// Should be in state A initially
		assertStateA()
	}

	_07_changeToStateX: {
		StateComponent.confirmStateChange({StateComponent.changeState('Status ☠')})

		assertStateX()
	}

	_08_changeToStateA: {
		StateComponent.confirmStateChange({StateComponent.changeState('Auf Initialstatus setzen')})

		assertStateA()
	}

	_10_changeToStateB: {
		StateComponent.confirmStateChange({StateComponent.changeState('10 -> 20')})

		assertStateB()
	}

	_15_changeToStateC: {
		StateComponent.confirmStateChange({StateComponent.changeState('Status C')})

		// State is automatically changed from 30 to 40 (by a server rule)
		assertStateD()
	}

	_25_changeToStateA: {
		// Perform the state change to state 10 (this is a "nonstop" state change)
		StateComponent.changeState('Auf Initialstatus setzen')

		assertStateA()
	}

	_30_changeToStateB: {
		StateComponent.changeState('10 -> 20')
		StateComponent.confirmStateChange()

		assertStateB()
	}

	_35_changeToStateD: {
		StateComponent.confirmStateChange({StateComponent.changeState('D Button de')})

		assertStateD()
	}

	/**
	 * Does another cycle of state changes only via layout buttons.
	 * State changes via layout button do not show confirmation dialogs.
	 */
	_40_changeToStateViaButtons: {
		StateComponent.stateButtons.find { it.label == 'Status A' }.click()

		assertStateA()

		StateComponent.stateButtons.find { it.label == 'Status B' }.click()

		assertStateB()

		StateComponent.stateButtons.find { it.label == 'Status C' }.click()

		assertStateD()
	}

	_45_cancelStateChange: {
		StateComponent.changeState('Status E')
		StateComponent.cancelStateChange()

		assertStateD()
	}

	_99_changeToStateE: {
		StateComponent.confirmStateChange({StateComponent.changeState('Status E')})

		assertStateE()
	}
}

	@Test
	void stateChangeWithBusinessException() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE)
		eo.addNew()

		eo.setAttribute('name', 'test2')
		eo.save()
		eo.setAttribute('name', 'test3')
		StateComponent.changeStateByNumeral(60)
		assert StateComponent.getConfirmStateChangeElement().$('.nuc-current-state-label').text == getLocale() == Locale.GERMANY ? 'Status A' : 'State A'
		assert StateComponent.getConfirmStateChangeElement().$('.nuc-target-state-label').text == getLocale() == Locale.GERMANY ? 'Fehler' : 'Failure'
		StateComponent.confirmStateChange()
		messageModal.confirm()

		StateComponent.changeStateByNumeral(60)
		assert StateComponent.getConfirmStateChangeElement().$('.nuc-current-state-label').text == getLocale() == Locale.GERMANY ? 'Status A' : 'State A'
		assert StateComponent.getConfirmStateChangeElement().$('.nuc-target-state-label').text == getLocale() == Locale.GERMANY ? 'Fehler' : 'Failure'

		//cleanup
		StateComponent.cancelStateChange()
		eo.cancel()
	}

	@Test
	void stateChangeTriggeringStateChangeWithInputRequired() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE)
		eo.addNew()
		eo.setAttribute('name', 'stateChangeTriggeringStateChangeWithInputRequired')
		eo.save()

		StateComponent.confirmStateChange({StateComponent.changeStateByNumeral(70)})
		getMessageModal('nuc-input-required-dialog').no()
		assertMessageModalAndConfirm(locale == Locale.GERMANY ? 'Fehler' : 'Error','State not changed!')

		StateComponent.confirmStateChange({StateComponent.changeStateByNumeral(70)})
		getMessageModal('nuc-input-required-dialog').yes()

		eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_STATECHANGEINPUTREQUIRED)
		assert StateComponent.currentState == 'Input Required'
	}

	@Test
	void stateChangeFromButtons() {
		nuclosSession.setSystemParameters(['nuclos_LAF_Show_State_Combobox_Threshold': '-1'])

		_01_setup: {
			LocaleChooser.locale = Locale.GERMANY

			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE)
			eo.addNew()

			assert !StateComponent.getNextStates(true)

			// Assert all state change buttons in the layout are disabled
			def buttons = StateComponent.stateButtons
			assert buttons*.label.toSet() == ['Status A', 'Status B', 'Status C', 'Status ☠', 'Status D', 'Status E'].toSet()
			assert !buttons.find { it.enabled }

			eo.setAttribute('name', 'test')
			eo.save()
			waitForAngularRequestsToFinish()
		}

		_02_stateComponentReadOnly: {
			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			def elemState = eo.getAttributeElement('nuclosState')
			def attrReadonly = elemState.$('input').getAttribute('readonly')
			assert attrReadonly == 'true'
		}

		_05_assertStateA: {
			// Should be in state A initially
			assertStateA()
		}

		_07_changeToStateX: {
			StateComponent.confirmStateChange({StateComponent.changeState('Status ☠')})

			assertStateX()
		}
	}

	// Some helper methods which contain only assertions:

	private void assertStateA() {
		assert StateComponent.currentState == '10 Status A'
		assert StateComponent.nextStates == ['0 Status ☠', '10 -> 20', '60 Fehler', '70 TriggerAnotherStateChange']    // Sorted by numeral

		// Assert the state change buttons correctly enabled/disabled
		def buttons = StateComponent.stateButtons
		assert buttons.findAll { it.enabled }*.label.toSet() == ['Status B', 'Status ☠'].toSet()
		assert buttons.findAll { !it.enabled }*.label.toSet() == ['Status A', 'Status C', 'Status D', 'Status E'].toSet()
		assert getActiveTabName() == 'Reiter A'
		assert getActiveTabContent() == 'Pane A'
	}

	private void assertStateB() {
		assert StateComponent.currentState == '20 Status B'
		assert StateComponent.nextStates == ['30 Status C', 'D Button de']

		// Assert the state change buttons correctly enabled/disabled
		def buttons = StateComponent.stateButtons
		assert buttons.findAll { it.enabled }*.label.toSet() == ['Status C', 'Status D'].toSet()
		assert buttons.findAll { !it.enabled }*.label.toSet() == ['Status A', 'Status B', 'Status ☠', 'Status E'].toSet()
		assert getActiveTabName() == 'Reiter B'
		assert getActiveTabContent() == 'Pane B'
	}

	private void assertStateD() {
		assert StateComponent.currentState == '40 Status D'
		assert StateComponent.nextStates == ['Auf Initialstatus setzen', '99 Status E'] // Zurücksetzen is the label of the transition
		// Assert the state change buttons correctly enabled/disabled
		def buttons = StateComponent.stateButtons
		assert buttons.findAll { it.enabled }*.label.toSet() == ['Status A', 'Status E'].toSet()
		assert buttons.findAll { !it.enabled }*.label.toSet() == ['Status B', 'Status C', 'Status ☠', 'Status D'].toSet()
		assert getActiveTabName() == 'Reiter D'
		assert getActiveTabContent() == 'Pane D'
	}

	private void assertStateE() {
		assert StateComponent.currentState == '99 Status E'
		assert !StateComponent.getNextStates(true)

		// Assert the state change buttons correctly enabled/disabled
		def buttons = StateComponent.stateButtons
		assert !buttons.find { it.enabled }
		assert buttons.findAll { !it.enabled }*.label.toSet() == ['Status A', 'Status B', 'Status C', 'Status ☠', 'Status D', 'Status E'].toSet()
		// status e needs to be scrolled

		def navNext = $('button.p-tabview-nav-next')
		if (navNext != null) {
			navNext.click()
		}
		assert getActiveTabName() == 'Reiter E (umbenannt)'
		assert getActiveTabContent() == 'Pane E'
	}

	private void assertStateX() {
		assert StateComponent.currentState == '0 Status ☠'
		assert StateComponent.nextStates == ['Auf Initialstatus setzen']

		// Assert the state change buttons correctly enabled/disabled
		def buttons = StateComponent.stateButtons
		assert buttons.findAll { it.enabled }*.label == ['Status A']
		assert buttons.findAll { !it.enabled }*.label.toSet() == ['Status B', 'Status C', 'Status ☠', 'Status D', 'Status E'].toSet()
	}

	private String getActiveTabContent() {
		$('.p-tabview-panel nuc-web-label-static')?.text
	}

	private String getActiveTabName() {
		$('.p-tabview-nav li.p-highlight .tab-title')?.text
	}
}
