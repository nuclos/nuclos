package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.FileComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.MessageModal
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformFavoriteFiltersHeader
import org.nuclos.test.webclient.pageobjects.subform.SubformFilterEditor
import org.nuclos.test.webclient.pageobjects.subform.SubformRow
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformFilterTest extends AbstractRerunnableWebclientTest {

	@Test
	void runTest() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		eo.addNew()
		eo.setAttribute("text", "Test Subform Filters")

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		SubformRow subformRow = subform.newRow()
		subformRow.enterValue("text", "filter1_text")
		subformRow.enterValue("date", "25012024")

		SubformRow subformRow2 = subform.newRow()
		subformRow2.enterValue("text", "renamed_text")
		subformRow2.enterValue("date", "26012024")

		SubformRow subformRow3 = subform.newRow()
		subformRow3.enterValue("text", "renamed_text")
		subformRow3.enterValue("date", "26012024")
		eo.save()

		subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		SubformFilterEditor filterEditor

		_00_initialAssertions: {
			assert subform.getFavoriteFiltersHeader() == null: 'The favorite filter display should be hidden, when there are no favorite filters.'
		}
		_01_openEditorOverlay: {
			subform.toggleFilter()
			filterEditor = subform.toggleFilterEditor()
			assert filterEditor != null: 'The overlay for configuring subform search filters should be visible'
			assert filterEditor.getSelectedFilterName() == '[Meine Suche]': 'Temporary filter should be selected without being dirty'
		}
		_02_closeEditorOverlay: {
			filterEditor = subform.toggleFilterEditor()
			assert filterEditor == null: 'The overlay for configuring subform search filters should be hidden'

			subform.subformElement.$('input[type="text"]').click()
			sendKeys("filter1_text")
			subform.subformElement.$('nuc-datechooser[type="date"]').click()
			sendKeys("25012024")
		}
		_03_saveTempFilter: {
			filterEditor = subform.toggleFilterEditor()
			filterEditor.makeTempPersistent()

			assert filterEditor.getSelectedFilterName() == 'Suchfilter 1': 'The name for the first subform search filter should be \'Suchfilter 1\''

			filterEditor.selectFilter("[Meine Suche]")

			NuclosWebElement textFilterField = subform.subformElement.$('input[type="text"]')
			textFilterField.click()
			clearInput()
			sendKeys("renamed_text")
			subform.subformElement.$('nuc-datechooser[type="date"]').click()
			clearInput()
			sendKeys("26012024")
			sendKeys(Keys.ENTER)

			filterEditor = subform.toggleFilterEditor()
			filterEditor.makeTempPersistent()
			assert filterEditor.getSelectedFilterName() == 'Suchfilter 2': 'The number suffix should have increased for the second subform search filter.'
		}
		_04_renameFilterAndSave: {
			filterEditor.enterFilterName("Filter Renamed")
			filterEditor.saveFilterChanges()

			assert filterEditor.getSelectedFilterName() == 'Filter Renamed': 'The selected searchfilter should have been renamed to \'Filter Renamed\''
		}
		_05_undoCurrentChanges: {
			filterEditor.enterFilterName("TMP Rename")
			filterEditor.toggleFilterAsFavorite()
			filterEditor.changeFavoritePosition(8)
			filterEditor.changeFavoriteIcon('people_outline')

			assert filterEditor.getSelectedFilterName() == 'Filter Renamed*'

			filterEditor.undoFilterChanges()

			assert !filterEditor.isFavorite(): 'Favorite Checkbox should have been reset'
			assert filterEditor.getFilterPosition() == '': 'Filter position should have been reset'
			assert filterEditor.getFilterIcon() == '': 'Filter icon should have been reset'
			assert filterEditor.getSelectedFilterName() == 'Filter Renamed': 'Filter name should have been reset to \'Filter Renamed\''
		}
		_06_rememberSelectedFilter: {
			refresh()
			subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
			subform.toggleFilter()
			filterEditor = subform.toggleFilterEditor()

			assert filterEditor.getSelectedFilterName().trim() == 'Filter Renamed': 'The filter \'Filter Renamed\' should be selected even after refreshing the page'
		}
		_07_rememberSwitchSelectedFilter: {
			// this test case seems to be identical with the previous one, but during testing it occurred, that switching between non-temp filters
			// won't always work
			filterEditor.selectFilter("Suchfilter 1")
			refresh()

			subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
			subform.toggleFilter()
			filterEditor = subform.toggleFilterEditor()
			assert filterEditor.getSelectedFilterName().trim() == 'Suchfilter 1': 'The filter \'Suchfilter 1\' should be selected even after refreshing the page'
		}
		_08_makeFavorite: {
			filterEditor.toggleFilterAsFavorite()
			filterEditor.saveFilterChanges()

			assert subform.getFavoriteFiltersHeader(): 'The favorite filter header should be visible, once there is at least on favorite filter'
		}
		_09_removeFavorite: {
			filterEditor.toggleFilterAsFavorite()
			filterEditor.saveFilterChanges()

			assert subform.getFavoriteFiltersHeader() == null: 'The favorite filter header should be hidden again, when there are no more favorite filters'
		}
		_10_multipleFavoritesOrder: {
			filterEditor.toggleFilterAsFavorite()
			filterEditor.saveFilterChanges()

			assert subform.getFavoriteFiltersHeader(): 'The favorite filter header should be visible, once there is at least on favorite filter'

			filterEditor.selectFilter('Filter Renamed')
			filterEditor = subform.toggleFilterEditor()
			filterEditor.toggleFilterAsFavorite()
			filterEditor.saveFilterChanges()

			SubformFavoriteFiltersHeader favoriteFiltersHeader = subform.getFavoriteFiltersHeader()
			assert subform.getFavoriteFiltersHeader(): 'The favorite filter header should be visible, once there is at least on favorite filter'

			assert favoriteFiltersHeader.getAllFavoriteFilters() == ['Filter Renamed', 'Suchfilter 1']: 'Original filter positioning not met'

			// move Filter Renamed to position 2
			filterEditor.changeFavoritePosition(2)
			filterEditor.saveFilterChanges()
			// select Filter 1 and move it to position 1
			filterEditor.selectFilter('Suchfilter 1')
			filterEditor = subform.toggleFilterEditor()
			filterEditor.changeFavoritePosition(1)
			filterEditor.saveFilterChanges()
			subform.toggleFilterEditor()

			waitForAngularRequestsToFinish()

			favoriteFiltersHeader = subform.getFavoriteFiltersHeader()
			assert favoriteFiltersHeader.getAllFavoriteFilters() == ['Suchfilter 1', 'Filter Renamed']: 'Favorite filters should have switched positions'
		}
		_11_switchBetweenFiltersViaFavoriteHeader: {
			filterEditor = subform.toggleFilterEditor()

			SubformFavoriteFiltersHeader favoriteFiltersHeader = subform.getFavoriteFiltersHeader()
			favoriteFiltersHeader.selectFilter('Filter Renamed')
			favoriteFiltersHeader.selectFilter('Suchfilter 1')
		}
		_12_editFavoriteIcon: {
			filterEditor.changeFavoriteIcon('people_outline')

			SubformFavoriteFiltersHeader favoriteFiltersHeader = subform.getFavoriteFiltersHeader()
			filterEditor.saveFilterChanges()
			subform.toggleFilterEditor()

			assert favoriteFiltersHeader.favoriteFilterRow.$('i.material-icons').text == 'people_outline': 'The chosen icon should be visible in the favorite filter header.'
		}
		_12_II_deselectFavoriteFilter: {
			SubformFavoriteFiltersHeader favoriteFiltersHeader = subform.getFavoriteFiltersHeader()
			favoriteFiltersHeader.selectFilter('people_outline\nSuchfilter 1')
			assert subform.getRowCount() == 3: 'Suchfilter 1 should have been deselected and all 3 rows should be on screen right now'
		}
		_13_filterChangeWorks: {
			SubformFavoriteFiltersHeader favoriteFiltersHeader = subform.getFavoriteFiltersHeader()
			// select filter 1 assert
			favoriteFiltersHeader.selectFilter('people_outline\nSuchfilter 1')
			assert subform.getRowCount() == 1

			SubformRow firstRow = subform.getRow(0)
			assert firstRow.getValue("text") == 'filter1_text'
			assert firstRow.getValue("date") == '25.01.2024'

			assert subform.subformElement.$('input[type="text"]').value == 'filter1_text'
			assert subform.subformElement.$('nuc-datechooser[type="date"] input').value == '25.01.2024'

			// select filter renamed assert
			favoriteFiltersHeader.selectFilter('Filter Renamed')
			assert subform.getRowCount() == 2

			firstRow = subform.getRow(0)
			SubformRow secondRow = subform.getRow(1)

			assert firstRow.getValue("text") == 'renamed_text'
			assert firstRow.getValue("date") == '26.01.2024'
			assert secondRow.getValue("text") == 'renamed_text'
			assert secondRow.getValue("date") == '26.01.2024'

			assert subform.subformElement.$('input[type="text"]').value == 'renamed_text'
			assert subform.subformElement.$('nuc-datechooser[type="date"] input').value == '26.01.2024'
		}
		_14_filterChangeWorksWithFilterColumnHidden: {
			subform.toggleFilter()

			SubformFavoriteFiltersHeader favoriteFiltersHeader = subform.getFavoriteFiltersHeader()
			// select filter 1 assert
			favoriteFiltersHeader.selectFilter('people_outline\nSuchfilter 1')
			assert subform.getRowCount() == 1

			SubformRow firstRow = subform.getRow(0)
			assert firstRow.getValue("text") == 'filter1_text'
			assert firstRow.getValue("date") == '25.01.2024'

			assert subform.subformElement.$('input[type="text"]') == null: 'The filter input components should be hidden'
			assert subform.subformElement.$('nuc-datechooser[type="date"]') == null: 'The filter input components should be hidden'

			// select filter renamed assert
			favoriteFiltersHeader.selectFilter('Filter Renamed')
			assert subform.getRowCount() == 2

			firstRow = subform.getRow(0)
			SubformRow secondRow = subform.getRow(1)

			assert firstRow.getValue("text") == 'renamed_text'
			assert firstRow.getValue("date") == '26.01.2024'
			assert secondRow.getValue("text") == 'renamed_text'
			assert secondRow.getValue("date") == '26.01.2024'

			assert subform.subformElement.$('input[type="text"]') == null: 'The filter input components should be hidden'
			assert subform.subformElement.$('nuc-datechooser[type="date"]') == null: 'The filter input components should be hidden'

			subform.toggleFilter()
		}
		_15_removeSubformSearchfilter: {
			filterEditor = subform.toggleFilterEditor()
			filterEditor.deleteSelectedFilter()

			MessageModal msgBox = getMessageModal()
			assert msgBox != null: 'There should be a Dialog to confirm the removal of a search filter'
			msgBox.confirm()

			filterEditor = subform.toggleFilterEditor()

			assert !filterEditor.isFilterAvailable("Filter Renamed"): 'The deleted filter should not be available anymore'
			assert filterEditor.getSelectedFilterName() == "[Meine Suche]": 'Removing a subform search filter should automatically select the temporary filter \'[Meine Suche]\''
		}

		Subform subsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform')
		subsubform.toggleFilter()
		SubformFilterEditor subSubFilterEditor = subsubform.toggleFilterEditor()
		_16_saveTempFilterForSubsubform: {
			subSubFilterEditor.makeTempPersistent()

			assert subSubFilterEditor.getSelectedFilterName() == 'Suchfilter 1': 'The name for the first subform search filter should be \'Suchfilter 1\''
		}
		_17_removeFilterForSubsubform: {
			subSubFilterEditor.deleteSelectedFilter()
		}
	}

	@Test
	void testSubformVlpFilter() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subform.toggleFilter()

		NuclosWebElement articleDropdown = subform.subformElement.$('#example_rest_OrderPosition_article')

		comboboxSelectFilter: {
			assert subform.getRowCount() == 5

			articleDropdown.click()

			sendKeys(Keys.DOWN, Keys.DOWN, Keys.DOWN)
			sendKeys(Keys.ENTER)

			waitForAngularRequestsToFinish()
			assert subform.getRowCount() == 1: 'Only 1 row should be visible, after selecting an entry.'
		}
		comboboxDeselectFilter: {
			articleDropdown.click()

			sendKeys(Keys.UP, Keys.UP, Keys.UP)
			sendKeys(Keys.ENTER)

			waitForAngularRequestsToFinish()
			assert subform.getRowCount() == 5: 'All rows should be visible again, after deselecting the filter combobox'
		}
		refreshValuelistTransferParameter: {
			NuclosWebElement categoryDropdown = subform.subformElement.$('#example_rest_OrderPosition_category')
			categoryDropdown.click()

			sendKeys(Keys.DOWN, Keys.DOWN, Keys.ENTER)

			articleDropdown.click()

			NuclosWebElement autoCompletePanel = $('.p-autocomplete-panel')
			assert autoCompletePanel.$$('li').size() == 3

			categoryDropdown.click()
			sendKeys(Keys.UP, Keys.UP, Keys.ENTER)

			articleDropdown.click()
			articleDropdown.click()

			autoCompletePanel = $('.p-autocomplete-panel')
			assert autoCompletePanel.$$('li').size() == 5
		}
	}

	@Test
	void testFilterComponentsDisplayValueAfterReload() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subform.toggleFilter()
		waitForAngularRequestsToFinish()

		NuclosWebElement articleDropdown = subform.subformElement.$('#example_rest_OrderPosition_article')
		NuclosWebElement categoryDropdown = subform.subformElement.$('#example_rest_OrderPosition_category')
		NuclosWebElement categoryTextualDropdown = subform.subformElement.$('#example_rest_OrderPosition_categorytextual')
		NuclosWebElement priceInput = subform.subformElement.$('#example_rest_OrderPosition_price')
		NuclosWebElement quantityInput = subform.subformElement.$('#example_rest_OrderPosition_quantity')
		NuclosWebElement nameInput = subform.subformElement.$('#example_rest_OrderPosition_name')
		NuclosWebElement versendetDropdown = subform.subformElement.$('#example_rest_OrderPosition_shipped')
		NuclosWebElement calenderElement = subform.subformElement.$('#example_rest_OrderPosition_createdAt')

		articleDropdown.click()
		sendKeys(Keys.DOWN, Keys.DOWN, Keys.ENTER)
		categoryDropdown.click()
		sendKeys(Keys.DOWN, Keys.DOWN, Keys.ENTER)
		categoryTextualDropdown.click()
		sendKeys(Keys.DOWN, Keys.DOWN, Keys.ENTER)
		priceInput.click()
		sendKeys('12,01')
		quantityInput.click()
		sendKeys('4')
		nameInput.click()
		sendKeys('custom name')
		versendetDropdown.click()
		sendKeys(Keys.DOWN, Keys.DOWN, Keys.DOWN, Keys.ENTER)
		calenderElement.scrollIntoView()
		calenderElement.click()
		sendKeys('01012024')
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		refresh()

		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subform.toggleFilter()
		waitForAngularRequestsToFinish()
		waitForAngularRequestsToFinish()

		articleDropdown = subform.subformElement.$('#example_rest_OrderPosition_article')
		categoryDropdown = subform.subformElement.$('#example_rest_OrderPosition_category')
		categoryTextualDropdown = subform.subformElement.$('#example_rest_OrderPosition_categorytextual')
		priceInput = subform.subformElement.$('#example_rest_OrderPosition_price')
		quantityInput = subform.subformElement.$('#example_rest_OrderPosition_quantity')
		nameInput = subform.subformElement.$('#example_rest_OrderPosition_name')
		versendetDropdown = subform.subformElement.$('#example_rest_OrderPosition_shipped')
		calenderElement = subform.subformElement.$('#example_rest_OrderPosition_createdAt')

		assert articleDropdown.value == '1001 Nuclos'
		assert categoryDropdown.value == 'Hardware'
		assert categoryTextualDropdown.value == 'Hardware'
		assert priceInput.value == '12,01'
		assert quantityInput.value == '4'
		assert nameInput.value == 'custom name'
		assert versendetDropdown.value == 'Nicht aktiv'
		assert calenderElement.value == '01.01.2024'

		//
		LocaleChooser.setLocale(Locale.ENGLISH)
		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subform.toggleFilter()

		articleDropdown = subform.subformElement.$('#example_rest_OrderPosition_article')
		categoryDropdown = subform.subformElement.$('#example_rest_OrderPosition_category')
		categoryTextualDropdown = subform.subformElement.$('#example_rest_OrderPosition_categorytextual')
		priceInput = subform.subformElement.$('#example_rest_OrderPosition_price')
		quantityInput = subform.subformElement.$('#example_rest_OrderPosition_quantity')
		nameInput = subform.subformElement.$('#example_rest_OrderPosition_name')
		versendetDropdown = subform.subformElement.$('#example_rest_OrderPosition_shipped')
		calenderElement = subform.subformElement.$('#example_rest_OrderPosition_createdAt')

		assert articleDropdown.value == '1001 Nuclos'
		assert categoryDropdown.value == 'Hardware'
		assert categoryTextualDropdown.value == 'Hardware'
		assert priceInput.value == '12.01'
		assert quantityInput.value == '4'
		assert nameInput.value == 'custom name'
		assert versendetDropdown.value == 'not active'
		assert calenderElement.value == '01/01/2024'
	}

	@Test
	void testSubformFileFilter() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		eo.setAttribute('text', 'Test Document in Subform')

		eo.selectTab('Document')
		insertFilesAndSave(eo)
		toggleSubformFilterAndVerifyFileFiltering(eo)

		logout()
		login('nuclos')

		EntityObjectComponent orderEo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		orderEo.addNew()
		orderEo.setAttribute("orderNumber", 123)

		orderEo.selectTab('Documents')
		waitForAngularRequestsToFinish()

		Subform subform = orderEo.getSubform(TestEntities.ORG_NUCLOS_SYSTEM_GENERALSEARCHDOCUMENT.fqn + '_genericObject')
		subform.openViewConfiguration()
		SideviewConfiguration.selectColumn(TestEntities.ORG_NUCLOS_SYSTEM_GENERALSEARCHDOCUMENT.fqn + '_documentfile')
		SideviewConfiguration.clickButtonOk()

		insertFilesAndSave(orderEo)
		orderEo.selectTab('Documents')
		toggleSubformFilterAndVerifyFileFiltering(eo)
	}

	@Test
	void testFilterWithDirtyRows() {
		TestDataHelper.insertTestData(nuclosSession)

		EntityObjectComponent orderEo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		orderEo.selectTab('Position')
		waitForAngularRequestsToFinish()

		Subform subform = orderEo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION,'order')
		subform.toggleFilter()
		NuclosWebElement articleDropdown = subform.subformElement.$("#${TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_article'}")
		NuclosWebElement priceFilter = subform.subformElement.$("#${TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_price'}")
		NuclosWebElement nameFilter = subform.subformElement.$("#${TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_name'}")
		NuclosWebElement shippedFilter = subform.subformElement.$("#${TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_shipped'}")
		NuclosWebElement createdAtFilter = subform.subformElement.$("#${TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_createdAt'}")

		articleDropdown.click()
		sendKeys(Keys.DOWN, Keys.DOWN, Keys.DOWN)
		sendKeys(Keys.ENTER)
		priceFilter.click()
		sendKeys('1234')
		sendKeys(Keys.ENTER)
		nameFilter.click()
		sendKeys('test')
		sendKeys(Keys.ENTER)
		shippedFilter.click()
		sendKeys(Keys.DOWN, Keys.DOWN)
		sendKeys(Keys.ENTER)
		createdAtFilter.click()
		sendKeys('01012024')
		sendKeys(Keys.ENTER)

		assert subform.rowCount == 0

		subform.newRow()
		assert subform.rowCount == 1 : 'new row must always be visible even if filter is set'

		articleDropdown.click()
		sendKeys(Keys.UP, Keys.UP, Keys.UP)
		sendKeys(Keys.ENTER)
		assert subform.rowCount == 1 : 'new row must always be visible even if filter is set'

		priceFilter.click()
		clearInput()
		assert subform.rowCount == 1 : 'new row must always be visible even if filter is set'

		nameFilter.click()
		clearInput()
		assert subform.rowCount == 1 : 'new row must always be visible even if filter is set'

		shippedFilter.click()
		sendKeys(Keys.DOWN)
		sendKeys(Keys.ENTER)
		assert subform.rowCount == 1 : 'new row must always be visible even if filter is set'

		createdAtFilter.scrollIntoView()
		createdAtFilter.click()
		clearInput()
		sendKeys(Keys.ENTER)
		assert subform.rowCount > 1 : 'new row must always be visible even if filter is set'
	}

	private void insertFilesAndSave(EntityObjectComponent eo) {
		Subform subform = eo.getSubform(TestEntities.ORG_NUCLOS_SYSTEM_GENERALSEARCHDOCUMENT.fqn + '_genericObject')

		subform.newRow()

		FileComponent fileComponent = subform.getRow(0).getFileComponent('file')
		fileComponent.setFile(TestDataHelper.nuclosDocxFile)

		subform.newRow()

		fileComponent = subform.getRow(0).getFileComponent('file')
		fileComponent.setFile(TestDataHelper.nuclosPngFile)

		eo.save()
	}

	private void toggleSubformFilterAndVerifyFileFiltering(EntityObjectComponent eo) {
		Subform subform = eo.getSubform(TestEntities.ORG_NUCLOS_SYSTEM_GENERALSEARCHDOCUMENT.fqn + '_genericObject')
		subform.toggleFilter()

		subform.subformElement.$('input[type="text"]').click()
		sendKeys(".png")
		sendKeys(Keys.ENTER)

		waitForAngularRequestsToFinish()
		assert subform.getRowCount() == 1

		clearInput()
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		assert subform.getRowCount() == 2
	}

}
