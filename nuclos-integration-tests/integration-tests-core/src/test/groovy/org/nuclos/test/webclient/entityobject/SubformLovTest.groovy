package org.nuclos.test.webclient.entityobject


import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow
import org.openqa.selenium.Keys
import org.openqa.selenium.interactions.Actions

import com.sun.xml.bind.v2.TODO

import groovy.transform.CompileStatic

/**
 * TODO: Don't use the Selenium driver directly, use the integration test API instead
 * TODO: Don't ever use generated IDs
 * TODO: Don't use "sleep(...)"
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformLovTest extends AbstractRerunnableWebclientTest {

@Test
void runTest() {
	String cat1Name = 'cat1'
	String cat2Name = 'cat2'
	String cat3Name = 'cat3'
	String art1Label = '1 One'
	String art2Label = '2 Two'

	_00_setup: {
		Long cat1Id = (Long)RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_CATEGORY.fqn,
				attributes: [name: cat1Name]
		], getNuclosSession())['boId']
		Long cat2Id = (Long)RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_CATEGORY.fqn,
				attributes: [name: cat2Name]
		], getNuclosSession())['boId']
		RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_ARTICLE.fqn,
				attributes: [category: [id: cat1Id],
				             articleNumber: 1,
							 name: 'One',
							 price: 1]
		], getNuclosSession())
		RESTHelper.createBo([
				boMetaId  : TestEntities.EXAMPLE_REST_ARTICLE.fqn,
				attributes: [category: [id: cat2Id],
				             articleNumber: 2,
				             name: 'Two',
				             price: 2]
		], getNuclosSession())
	}

	_01_testLovAddReferenceWithOpenDropdown:
	{
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute("orderNumber", 1)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		subform.newRow()

		subform.getRow(0).getLOV('article').lov.mouseover()
		$zZz('a.add-reference').click()

		//lov.addReference()

		waitFor { driver.getWindowHandles().size() == 2 }

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		// create a new article in new window
		EntityObjectComponent article = EntityObjectComponent.forDetail()
		article.setAttribute('articleNumber', 2000)
		article.setAttribute('price', 2000)
		article.setAttribute('name', 'Test-Article')
		// save and close window
		article.saveNoWait()

		driver.switchTo().window(oldWindow)
		resizeBrowserWindow()
		waitForAngularRequestsToFinish()

		eo = EntityObjectComponent.forDetail()
		eo.save()

		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert subform.getRow(0).getValue('article') == '2000 Test-Article'
	}

	_02_testLovMagnifyingWithOpenDropdown:
	{
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute("orderNumber", 2)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		SubformRow subformRow = subform.newRow()

		subformRow.getLOV('article').lov.mouseover()
		$zZz('a.search-reference').click()

		waitFor { driver.getWindowHandles().size() == 2 }

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		waitUntilTrue({ -> Sidebar.listEntryCount == 3 }, 60)

		switchToOtherWindow()
		closeOtherWindows()

		subformRow.enterValue('category', cat1Name)
		subformRow.clickCell('article')

		subformRow.getLOV('article').lov.mouseover()
		$zZz('a.search-reference').click()

		assert driver.getWindowHandles().size() == 2

		oldWindow = driver.windowHandle
		newWindow = switchToOtherWindow()

		assert oldWindow != newWindow
		assert Sidebar.listEntryCount == 1

		closeOtherWindows()

	}

	_03_testLovAfterCloning: {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		// TODO fix already existing error which causes NUCLOS-7599 to fail
		refresh()

		eo.addNew()
		eo.setAttribute("orderNumber", 3)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		SubformRow subformRow = subform.newRow()
		subformRow.enterValue('category', cat1Name)
		subformRow.clickCell('article')
		ListOfValues lov = subformRow.getLOV('article')
		assert lov.open
		assert lov.choices == [art1Label]
		lov.selectEntry(art1Label)

		subformRow = subform.newRow()
		subformRow.enterValue('category', cat2Name)
		subformRow.clickCell('article')
		lov = subformRow.getLOV('article')
		assert lov.open
		assert lov.choices == [art2Label]
		lov.selectEntry(art2Label)

		subform.toggleSelection(1)
		subform.cloneSelectedRows()

		subformRow = subform.getRow(0)
		subformRow.clickCell('article')
		lov = subformRow.getLOV('article')
		assert lov.open
		assert lov.choices == [art1Label]

		eo.save()

		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert subform.getRowCount() == 3
	}

	_04_testLovAddReferenceShortcut:
	{
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute("orderNumber", 4)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		subform.newRow()

		subform.getRow(0).getLOV('article').lov.focus()
		new Actions(driver)
				.keyDown(Keys.SHIFT)
				.keyDown(Keys.ALT)
				.sendKeys('a')
				.keyUp(Keys.ALT)
				.keyUp(Keys.SHIFT)
				.build()
				.perform()

		//lov.addReference()

		waitFor { driver.getWindowHandles().size() == 2 }

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		EntityObjectComponent article = EntityObjectComponent.forDetail()
		article.setAttribute('articleNumber', 3000)
		article.setAttribute('price', 3000)
		article.setAttribute('name', 'Test-Article')
		article.saveNoWait()

		driver.switchTo().window(oldWindow)
		resizeBrowserWindow()
		waitForAngularRequestsToFinish()

		eo = EntityObjectComponent.forDetail()
		eo.save()

		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert subform.getRow(0).getValue('article') == '3000 Test-Article'
	}

	_05_testLovSearchReferenceShortcut:
	{
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute("orderNumber", 5)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		subform.newRow()

		subform.getRow(0).getLOV('article').lov.focus()
		new Actions(driver)
				.keyDown(Keys.SHIFT)
				.keyDown(Keys.ALT)
				.sendKeys('q')
				.keyUp(Keys.ALT)
				.keyUp(Keys.SHIFT)
				.build()
				.perform()

		//lov.addReference()

		waitFor { driver.getWindowHandles().size() == 2 }

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		Sidebar.selectEntryByText('One')

		EntityObjectComponent article = EntityObjectComponent.forDetail()
		article.selectInOtherWindow()

		driver.switchTo().window(oldWindow)
		resizeBrowserWindow()
		waitForAngularRequestsToFinish()

		eo = EntityObjectComponent.forDetail()
		eo.save()

		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert subform.getRow(0).getValue('article') == '1 One'
	}

	_06_testLovEditReferenceShortcut:
	{
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute("orderNumber", 6)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		subform.newRow()

		subform.getRow(0).getLOV('article').selectEntry('1 One')
		subform.getRow(0).getLOV('article').lov.focus()

		new Actions(driver)
				.keyDown(Keys.SHIFT)
				.keyDown(Keys.ALT)
				.sendKeys('e')
				.keyUp(Keys.ALT)
				.keyUp(Keys.SHIFT)
				.build()
				.perform()


		//lov.addReference()

		waitFor { driver.getWindowHandles().size() == 2 }

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		EntityObjectComponent article = EntityObjectComponent.forDetail()
		article.setAttribute('name', 'One Test')
		article.saveNoWait()

		driver.switchTo().window(oldWindow)
		resizeBrowserWindow()
		waitForAngularRequestsToFinish()

		eo = EntityObjectComponent.forDetail()
		eo.save()

		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert subform.getRow(0).getValue('article') == '1 One Test'

		subform.getRow(0).getLOV('article').lov.focus()
		sendKeys(Keys.ESCAPE)
		new Actions(driver)
				.keyDown(Keys.SHIFT)
				.keyDown(Keys.ALT)
				.sendKeys('e')
				.keyUp(Keys.ALT)
				.keyUp(Keys.SHIFT)
				.build()
				.perform()

		waitFor { driver.getWindowHandles().size() == 2 }

		oldWindow = driver.windowHandle
		newWindow = switchToOtherWindow()

		assert oldWindow != newWindow
		article = EntityObjectComponent.forDetail()
		article.setAttribute('name', 'One')
		article.saveNoWait()

		driver.switchTo().window(oldWindow)
		resizeBrowserWindow()
		waitForAngularRequestsToFinish()

		eo = EntityObjectComponent.forDetail()

		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert subform.getRow(0).getValue('article') == 'One'
	}

	_07_testLovRefreshReferenceShortcutAndButton: {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute("orderNumber", 7)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		subform.newRow()

		subform.getRow(0).getLOV('article').selectEntry('1 One')
		subform.getRow(0).getLOV('article').lov.focus()
		assert !subform.getRow(0).getLOV('article').hasEntry('3 Three')

		EntityObject cat3= new EntityObject(TestEntities.EXAMPLE_REST_CATEGORY)
		cat3.setAttributes((Map) [name: cat3Name])
		nuclosSession.save(cat3)

		EntityObject art3 = new EntityObject(TestEntities.EXAMPLE_REST_ARTICLE)
		art3.setAttributes((Map) [category: [id: cat3.id],
		                          articleNumber: 3,
		                          name: 'Three',
		                          price: 2])
		nuclosSession.save(art3)

		new Actions(driver)
				.keyDown(Keys.SHIFT)
				.keyDown(Keys.ALT)
				.sendKeys('r')
				.keyUp(Keys.ALT)
				.keyUp(Keys.SHIFT)
				.build()
				.perform()


		assert subform.getRow(0).getLOV('article').hasEntry('3 Three')

		subform.getRow(0).getLOV('article').selectEntry('3 Three')

		nuclosSession.delete(art3)
		nuclosSession.delete(cat3)

		$zZz('a.refresh-reference').click()

		assert !subform.getRow(0).getLOV('article').hasEntry('3 Three')
		assert !subform.getRow(0).getLOV('article').getSelectedEntryFromDropdown() != '3 Three'

		resizeBrowserWindow()
		waitForAngularRequestsToFinish()

		eo.cancel()
	}
}
}
