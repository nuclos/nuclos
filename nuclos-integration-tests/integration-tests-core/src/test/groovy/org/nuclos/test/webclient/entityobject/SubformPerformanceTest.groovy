package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformPerformanceTest extends AbstractRerunnableWebclientTest {

@Test
void runTest() {
	_00_setup: {
		TestDataHelper.insertTestData(nuclosSession)

	}

	_05_insertSubformEntries: {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)

		eo.addNew()
		eo.setAttribute('text', 'Test')

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')

		def deltas = []
		10.times {
			countBrowserRequests {
				long startRow = System.currentTimeMillis()
				subform.newRow()
				sendKeys(Keys.ESCAPE)
				long deltaRow = System.currentTimeMillis() - startRow
				deltas << deltaRow
			}.with {
				assert it.getRequestCount(RequestType.EO_GENERATION_WITH_DEFAULTS) == 1

				// There might be an additional request for Meta-Data on first insert
				assert it.getRequestCount(RequestType.ALL) <= 2
			}
		}
		Log.info "Row deltas:"
		deltas.each {
			Log.info "$it ms"
		}
		Log.info "Total time: ${deltas.sum()} ms"

		def average = (BigDecimal) deltas.sum() / deltas.size()
		Log.info "Average: $average"

		// Adding a new row should on average not take more than 1 second locally / 3 seconds on Jenkins
		assert average < 3000

		eo.save()

		subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		assert subform.rowCount == 10
	}

	_10_cloneSubformEntries: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')

		subform.selectAllRows()

		def deltas = []
		9.times {
			countBrowserRequests {
				long startRow = System.currentTimeMillis()
				subform.cloneSelectedRows()
				long deltaRow = System.currentTimeMillis() - startRow
				deltas << deltaRow
			}.with {
				assert it.getRequestCount(RequestType.EO_GENERATION_WITH_DEFAULTS) == 1
				assert it.getRequestCount(RequestType.ALL) == 1
			}
		}
		Log.info "Row deltas:"
		deltas.each {
			Log.info "$it ms"
		}
		Log.info "Total time: ${deltas.sum()} ms"

		def average = (BigDecimal) deltas.sum() / deltas.size()
		Log.info "Average: $average"

		// Cloning 10 rows should on average not take more than 2 seconds locally / 4 seconds on Jenkins
		assert average < 4000

		eo.save()

		subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		assert subform.rowCount > 10
	}

}
}
