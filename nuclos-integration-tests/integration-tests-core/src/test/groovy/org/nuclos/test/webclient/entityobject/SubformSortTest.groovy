package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformSortTest extends AbstractRerunnableWebclientTest {

	@Test
	void runTest() {
		List<String> orderNumbers = ['100', '1', '11', '0', '9']
		List<String> orderNotes = ['123', 'a', '', 'A', 'äöü']
		List<Date> orderDates = [
				new Date(117, 3, 1),
				new Date(116, 0, 1),
				new Date(117, 8, 3),
				new Date(117, 0, 1),
				new Date(117, 0, 5)
		]
		EntityObject<Long> multiSortCustomer
		EntityObject<Long> customerWithOrders

		_00_setup:
		{
			TestDataHelper.insertTestData(nuclosSession)

			EntityObject<Long> customer = nuclosSession.getEntityObjects(
					TestEntities.EXAMPLE_REST_CUSTOMER,
					new QueryOptions(
							where: TestEntities.EXAMPLE_REST_CUSTOMER.fqn + '_customerNumber = 22222'
					)
			).first()

			List<EntityObject> orders = []
			5.times {
				EntityObject order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
				order.setAttribute('customer', [id: customer.id])

				orders << order
			}

			orders.eachWithIndex { EntityObject entry, int i ->
				// User the numbers in reverse order, so it appears in correct order in the subform
				def orderNumber = orderNumbers[orderNumbers.size() - i - 1]
				def note = orderNotes[orderNotes.size() - i - 1]
				def orderDate = orderDates[orderDates.size() - i - 1]

				entry.setAttribute('orderNumber', orderNumber)
				entry.setAttribute('note', note)
				entry.setAttribute('orderDate', orderDate)
			}

			customerWithOrders = customer

			nuclosSession.saveAll(orders)

			addCustomerForMultiSort:
			{
				multiSortCustomer = new EntityObject(TestEntities.EXAMPLE_REST_CUSTOMER)
				multiSortCustomer.setAttribute('customerNumber', 289382)
				multiSortCustomer.setAttribute('name', 'Multi-Sort')

				// TODO: Insert directly as dependents of multiSortCustomer
				(0..1).each { i ->
					(1..0).each { j ->
						EntityObject address = new EntityObject(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS)
						address.setAttribute('customer', [id: multiSortCustomer.id])
						address.setAttribute('street', "$j$i".toString())
						address.setAttribute('city', "$i".toString())
						address.setAttribute('zipCode', "$i".toString())

						multiSortCustomer.getDependents(
								TestEntities.EXAMPLE_REST_CUSTOMERADDRESS,
								'customer'
						) << address
					}
				}

				nuclosSession.save(multiSortCustomer)
			}
		}

		_05_sortByReference:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

			// This is how we prevent many NPEs :(
			sleep(1000)
			Sidebar.selectEntryByText('10020158')

			// first wait for a valid and accessible subform
			List<String> articles = Subform.getColumnValuesRetryOnStaleElement(eo,
					TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order', 'article', String.class)
			assert articles != articles.toSorted()

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')

			subform.clickColumnHeader('article')
			articles = subform.getColumnValues('article')
			assert articles == articles.toSorted()

			subform.clickColumnHeader('article')
			articles = subform.getColumnValues('article')
			assert articles == articles.toSorted().reverse()

			subform.clickColumnHeader('article')
			articles = subform.getColumnValues('article')
			assert articles != articles.toSorted()
		}

		_06_sortByReferenceWithNew:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			// first wait for a valid and accessible subform
			List<String> articles = Subform.getColumnValuesRetryOnStaleElement(eo,
					TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order', 'article', String.class)
			assert articles != articles.toSorted()

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')

			// Sort before adding new row
			subform.clickColumnHeader('article')

			// Adding a new row must not reset the sorting
			subform.newRow()

			sortAscending:
			{
				articles = subform.getColumnValues('article')

				// New entry must stay on top
				assert articles.first() == ''

				assert articles == articles.toSorted()
			}

			sortDescending:
			{
				subform.clickColumnHeader('article')
				articles = subform.getColumnValues('article')

				// New entry must stay on top
				assert articles.first() == ''

				assert articles - '' == articles.toSorted().reverse() - ''
			}

			noSorting:
			{
				subform.clickColumnHeader('article')
				articles = subform.getColumnValues('article')

				// New entry must stay on top
				assert articles.first() == ''

				assert articles != articles.toSorted()
			}

			eo.cancel()
		}

		_10_sortByTextWithNew:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
			Sidebar.selectEntryByText('Test-Customer 2')
			eo.selectTab('Orders')

			// first wait for a valid and accessible subform
			Subform.getColumnValuesRetryOnStaleElement(eo,
					TestEntities.EXAMPLE_REST_ORDER, 'customer', 'orderNumber', String.class)
			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')
			subform.openViewConfiguration()
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_note')
			SideviewConfiguration.clickButtonOk()

			// TODO subform.getColumnHeaders() only contains visible header
			assert subform.getRow(0).getFieldElement('note') != null

			subform.newRow()
			AbstractPageObject.clickButtonOk()

			List<String> values = subform.getColumnValues('note')

			assert values == [''] + orderNotes

			sortAscending:
			{
				subform.clickColumnHeader('note')
				values = subform.getColumnValues('note')

				def sortedValues = [''] + orderNotes.toSorted()

				assert values == sortedValues
			}

			sortDescending:
			{
				subform.clickColumnHeader('note')
				values = subform.getColumnValues('note')

				def sortedValues = [''] + orderNotes.toSorted().reverse()

				assert values == sortedValues
			}

			// Reset sorting
			subform.clickColumnHeader('note')
		}

		_15_sortByDateWithNew:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			// first wait for a valid and accessible subform
			List<Date> values = Subform.getColumnValuesRetryOnStaleElement(eo,
					TestEntities.EXAMPLE_REST_ORDER, 'customer', 'orderDate', Date.class)
			assert values == [null] + orderDates

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')
			sortAscending:
			{
				subform.clickColumnHeader('orderDate')
				values = subform.getColumnValues('orderDate', Date.class)

				def sortedValues = [null] + orderDates.toSorted()

				assert values == sortedValues
			}

			sortDescending:
			{
				subform.clickColumnHeader('orderDate')
				values = subform.getColumnValues('orderDate', Date.class)

				def sortedValues = [null] + orderDates.toSorted().reverse()

				assert values == sortedValues
			}

			// Reset sorting
			subform.clickColumnHeader('orderDate')
		}

		_20_sortByNumberWithNew:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			// first wait for a valid and accessible subform
			List<String> values = Subform.getColumnValuesRetryOnStaleElement(eo,
					TestEntities.EXAMPLE_REST_ORDER, 'customer', 'orderNumber', String.class)
			assert values == [''] + orderNumbers

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')
			sortAscending:
			{
				subform.clickColumnHeader('orderNumber')
				values = subform.getColumnValues('orderNumber')

				def arraylistOfStrings = orderNumbers.collect { it.toInteger() }.toSorted()*.toString()
				def sortedValues = [''] + arraylistOfStrings

				assert values == sortedValues
			}

			sortDescending:
			{
				subform.clickColumnHeader('orderNumber')
				values = subform.getColumnValues('orderNumber')

				def arraylistOfStrings = orderNumbers.collect { it.toInteger() }.toSorted().reverse()*.toString()
				def sortedValues = [''] + arraylistOfStrings

				assert values == sortedValues
			}

			// Reset sorting
			subform.clickColumnHeader('orderNumber')

			eo.cancel()
		}

		_25_sortByBooleanWithNew:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
			Sidebar.refresh()

			Sidebar.selectEntryByText('10020158')

			// first wait for a valid and accessible subform
			List<Boolean> values = Subform.getColumnValuesRetryOnStaleElement(eo,
					TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order', 'shipped', Boolean.class)

			assert values == [false, false, false, false, false]

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
			subform.getRow(0).setChecked('shipped', true)
			subform.getRow(3).setChecked('shipped', true)

			values = subform.getColumnValues('shipped', Boolean.class)
			assert values == [true, false, false, true, false]

			subform.newRow()

			values = subform.getColumnValues('shipped', Boolean.class)
			assert values == [false, true, false, false, true, false]

			sortAscending:
			{
				subform.clickColumnHeader('shipped')
				values = subform.getColumnValues('shipped', Boolean.class)
				assert values == [false, false, false, false, true, true]
			}

			sortDescending:
			{
				subform.clickColumnHeader('shipped')
				values = subform.getColumnValues('shipped', Boolean.class)
				assert values == [false, true, true, false, false, false]
			}

			eo.cancel()
		}

		_35_multiSort:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(multiSortCustomer)

			// first wait for a valid and accessible subform
			List<String> streetValues = Subform.getColumnValuesRetryOnStaleElement(eo,
					TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer', 'street', String.class)
			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer')
			List<String> cityValues = subform.getColumnValues('city')

			assert streetValues == ['01', '11', '00', '10']
			assert cityValues == ['1', '1', '0', '0']

			sortCityAsc:
			{
				subform.clickColumnHeader('city')
				assert subform.getColumnValues('city') == ['0', '0', '1', '1']
				assert subform.sortModel == [
						[colId: TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_city', sort: 'asc', priority: 0]
				]
			}

			sortCityAscStreetAsc:
			{
				subform.clickColumnHeader('street')
				assert subform.getColumnValues('city') == ['0', '0', '1', '1']
				assert subform.getColumnValues('street') == ['00', '10', '01', '11']
				assert subform.sortModel == [
						[colId: TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_city', sort: 'asc', priority: 1],
						[colId: TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_street', sort: 'asc', priority: 2],
				]
			}

			sortCityDescStreetAsc:
			{
				subform.clickColumnHeader('city')
				assert subform.getColumnValues('city') == ['1', '1', '0', '0']
				assert subform.getColumnValues('street') == ['01', '11', '00', '10']
				assert subform.sortModel == [
						[colId: TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_city', sort: 'desc', priority: 1],
						[colId: TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_street', sort: 'asc', priority: 2],
				]
			}

			sortCityDescStreetAscZipAsc:
			{
				subform.clickColumnHeader('zipCode')
				assert subform.getColumnValues('city') == ['1', '1', '0', '0']
				assert subform.getColumnValues('street') == ['01', '11', '00', '10']
				assert subform.sortModel == [
						[colId: TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_city', sort: 'desc', priority: 1],
						[colId: TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_street', sort: 'asc', priority: 2],
						[colId: TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_zipCode', sort: 'asc', priority: 3],
				]
			}

			sortStreetAscZipCodeAsc:
			{
				subform.clickColumnHeader('city')
				assert subform.getColumnValues('city') == ['0', '1', '0', '1']
				assert subform.getColumnValues('street') == ['00', '01', '10', '11']
				assert subform.sortModel == [
						[colId: TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_street', sort: 'asc', priority: 1],
						[colId: TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_zipCode', sort: 'asc', priority: 2],
				]
			}
		}

		_40_sortByCalculatedAttributeDisabled:
		{
			getNuclosSession().setSystemParameters(['SORT_CALCULATED_ATTRIBUTES': 'false'])
			refresh()
			EntityObjectComponent eo = EntityObjectComponent.open(customerWithOrders)
			eo.selectTab('Orders')

			// first wait for a valid and accessible subform
			Subform.getColumnValuesRetryOnStaleElement(eo,
					TestEntities.EXAMPLE_REST_ORDER, 'customer', 'orderNumber', String.class)
			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')

			subform.openViewConfiguration()
			SideviewConfiguration.resetSorting()
			SideviewConfiguration.deselectAll()
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_orderNumber')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_note')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_calculated')
			SideviewConfiguration.clickButtonOk()

			List<String> orderNumbersFromSubform = subform.getColumnValues('orderNumber')
			List<String> notes = subform.getColumnValues('note')

			assert orderNumbersFromSubform == ['100', '1', '11', '0', '9']
			assert notes == ['123', 'a', '', 'A', 'äöü']

			sortCalculatedAsc:
			{
				subform.clickColumnHeader('calculated')
				assert subform.getColumnValues('calculated') == ['123', 'a', '', 'A', 'äöü']
				assert subform.sortModel == []
			}

			sortOrderNumber:
			{
				subform.clickColumnHeader('orderNumber')
				assert subform.getColumnValues('orderNumber') == ['0', '1', '9', '11', '100']
				assert subform.sortModel == [
						[colId: TestEntities.EXAMPLE_REST_ORDER.fqn + '_orderNumber', sort: 'asc', priority: 0]
				]
			}
		}

		_41_sortByCalculatedAttributeEnabled:
		{
			getNuclosSession().setSystemParameters(['SORT_CALCULATED_ATTRIBUTES': 'true'])
			refresh()
			EntityObjectComponent eo = EntityObjectComponent.open(customerWithOrders)
			eo.selectTab('Orders')

			// first wait for a valid and accessible subform
			Subform.getColumnValuesRetryOnStaleElement(eo,
					TestEntities.EXAMPLE_REST_ORDER, 'customer', 'orderNumber', String.class)
			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')

			subform.openViewConfiguration()
			SideviewConfiguration.resetSorting()
			SideviewConfiguration.deselectAll()
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_orderNumber')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_note')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_calculated')
			SideviewConfiguration.clickButtonOk()

			List<String> orderNumbersFromSubform = subform.getColumnValues('orderNumber')
			List<String> notes = subform.getColumnValues('note')

			assert orderNumbersFromSubform == ['100', '1', '11', '0', '9']
			assert notes == ['123', 'a', '', 'A', 'äöü']

			sortCalculatedAsc:
			{
				subform.clickColumnHeader('calculated')
				assert subform.getColumnValues('calculated') == ['', '123', 'A', 'a', 'äöü']
				assert subform.sortModel == [
						[colId: TestEntities.EXAMPLE_REST_ORDER.fqn + '_calculated', sort: 'asc', priority: 0]
				]
			}

			sortCalculatedAscOrderNumberAsc:
			{
				subform.clickColumnHeader('orderNumber')
				assert subform.getColumnValues('orderNumber') == ['11', '100', '0', '1', '9']
				assert subform.sortModel == [
						[colId: TestEntities.EXAMPLE_REST_ORDER.fqn + '_calculated', sort: 'asc', priority: 1],
						[colId: TestEntities.EXAMPLE_REST_ORDER.fqn + '_orderNumber', sort: 'asc', priority: 2]
				]
			}
		}
	}

	@Test
	void testEmptySubformSorting() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, "order")

		subform.getRow(1).enterValue("categorytextual", "aaa")
		subform.getRow(3).enterValue("categorytextual", "bbb");
		subform.clickColumnHeader('categorytextual')

		assert subform.getColumnValues("categorytextual") == ['', '', '', 'aaa', 'bbb']

		subform.clickColumnHeader('categorytextual')

		assert subform.getColumnValues("categorytextual") == ['bbb', 'aaa', '', '', '']

		subform.clickColumnHeader('categorytextual')

		assert subform.getColumnValues("categorytextual") == ['', 'aaa', '', 'bbb', '']

		subform.clickColumnHeader('categorytextual')
		subform.newRow()
		subform.newRow()

		assert subform.getColumnValues("categorytextual") == ['Software', 'Software', '', '', '', 'aaa', 'bbb']
		eo.cancel()
	}
}
