package org.nuclos.test.webclient.entityobject

import java.text.SimpleDateFormat

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.nuclos.test.webclient.pageobjects.viewconfiguration.ViewConfigurationModal
import org.openqa.selenium.Dimension
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformTest extends AbstractRerunnableWebclientTest {

	@Before
	void setup() {
		RESTHelper.createUser('readonly', 'readonly', ['Example readonly'], nuclosSession)
		RESTHelper.createUser('useradmin', 'useradmin', ['User admin'], nuclosSession)

		EntityObject article = new EntityObject(TestEntities.EXAMPLE_REST_ARTICLE)
		article.setAttribute('articleNumber', 2000)
		article.setAttribute('price', 2000)
		article.setAttribute('name', 'Test-Article')
		nuclosSession.save(article)

		EntityObject resource = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_RESOURCE)
		resource.setAttribute('name', 'Res w Subform')
		resource.setAttribute('label', 'Res w Subform')
		resource.setAttribute('description', 'Res with Subform to test NuclosRowColor in Subforms')
		nuclosSession.save(resource)

		EntityObject booking = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
		booking.setAttribute('name', 'Booking')
		booking.setAttribute('label', 'Booking')
		booking.setAttribute('fromdate', new Date())
		booking.setAttribute('untildate', new Date())
		booking.setAttribute('nuclosrowcolor', '#0000FF')
		booking.setAttribute('planningtableresource', resource)
		nuclosSession.save(booking)

		EntityObject bookingNoRowColor = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
		bookingNoRowColor.setAttribute('name', 'Booking')
		bookingNoRowColor.setAttribute('label', 'Booking')
		bookingNoRowColor.setAttribute('fromdate', new Date())
		bookingNoRowColor.setAttribute('untildate', new Date())
		bookingNoRowColor.setAttribute('planningtableresource', resource)
		nuclosSession.save(bookingNoRowColor)
	}

	@Test
	void runTest() {

		String tabAuftragsPosition = 'tab_' + TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION.fqn + '_auftrag'
		String auftragNo1stTransition = TestEntities.EXAMPLE_REST_AUFTRAG.fqn
		String customerBoMetaId = TestEntities.EXAMPLE_REST_CUSTOMER.fqn

		Map customer = [customerNumber: 123, name: 'Customer 123']
		String subBoMetaId = TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer'
		String orderSubBoMetaId = TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer'

		_00_createEo:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

			countBrowserRequests {
				eo.addNew()

				screenshot('createnewentry-b')

				def testEntryName = '42'

				eo.setAttribute('customerNumber', testEntryName)
				eo.setAttribute('name', testEntryName)

				screenshot('createnewentry-c')
				eo.save()
				screenshot('createnewentry-d')
			}.with {
				assert it.getRequestCount(RequestType.EO_CREATE) == 1
				assert it.getRequestCount(RequestType.EO_SUBFORM) == 17
				assert it.getSqlCount(RequestType.EO_CREATE) < 5

				assert it.sqlCount < 10
				assert it.sqlTime < 100
			}
		}

		_01_editSmallSubform:
		{

			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
			eo.addNew()

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

			SubformRow subformRow = subform.newRow()
			subformRow.enterValue('article', '2000 Test-Article')
			assert subformRow.getValue('article') == '2000 Test-Article'

			// resize window
			driver.manage().window().setSize(new Dimension(1024, 680))

			subformRow = subform.newRow()
			subformRow.enterValue('article', '2000 Test-Article')
			assert subformRow.getValue('article') == '2000 Test-Article'

			screenshot('edit-small-subform')

			eo.cancel()

			resizeBrowserWindow()
		}

		_02_overlayStatusText:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			eo.addNew()

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assertOverlayTextForEmpty(subform)

			subform.newRow()
			assert !subform.overlayText

			subform.selectAllRows()
			subform.deleteSelectedRows()

			assertOverlayTextForEmpty(subform)

			eo.cancel()

		}

		_03_checkboxTest:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
			eo.addNew()
			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

			SubformRow firstRow = subform.newRow()


			// toggle checkbox via click -> true
			firstRow.getFieldElement('shipped').click()
			assert firstRow.getValue('shipped', Boolean.class)

			// toggle checkbox via click -> false
			firstRow.getFieldElement('shipped').click()
			assert !firstRow.getValue('shipped', Boolean.class)

			assert !firstRow.getValue('shipped', Boolean.class)

			// toggle checkbox -> true
			sendKeys(Keys.SPACE)
			assert firstRow.getValue('shipped', Boolean.class)

			// toggle checkbox -> false
			sendKeys(Keys.SPACE)
			assert !firstRow.getValue('shipped', Boolean.class)

			eo.cancel()
		}

		_05_insertSubformEntryInModal:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
			eo.getTab('Orders').click()

			EntityObjectModal eoModal = EntityObjectComponent.forModal({eo.getSubform(orderSubBoMetaId).newRow()})
			parentRefFieldShouldBeDisabled:
			{
				assert eoModal.getAttribute('customer') == '42'
				assert eoModal.isAttributeReadonly('customer')
			}

			eoModal.setAttribute('orderNumber', 111)
			eoModal.clickButtonOk()

			eo.save()

			eo.getTab('Orders').click()
			Subform subform = eo.getSubform(orderSubBoMetaId)
			assert subform.rowCount == 1

		}

		_06_customRuleButtonInModal:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
			eo.getTab('Orders').click()

			EntityObjectModal eoModal = EntityObjectComponent.forModal({
				eo.getSubform(TestEntities.EXAMPLE_REST_ORDER, 'customer').getRow(0).editInModal()
			})

			assert eoModal.getAttribute('note') == ''

			assertMessageModalAndConfirm('Customer Name', null, {eoModal.clickButton('Auftrag übermitteln')})
			assert eoModal.getAttribute('note') == 'Mail send...'

			// reset fields for subsequent tests
			eoModal.setAttribute('note', '')
			eoModal.setAttribute('discount', '')
			assert eoModal.getAttribute('note') == ''

			eoModal.clickButtonOk()
			eo.save()
		}

		Map<String, Boolean> fieldsInSubOrder = [
				'orderDate'          : true,
				'deliveryDate'       : true,
				'discount'           : true,
				'approvedcreditlimit': true,
				'confirmation'       : false,
				'order'              : false,

				// Textarea
				'note'               : true,

				'sendDate'           : true,

				// LOVs
				'rechnung'           : true,
				'nuclosState'        : false,
		]

		_07_testEnteringAndTabbing:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			eo.getTab('Orders').click()
			Subform subform = eo.getSubform(orderSubBoMetaId)

			SubformRow row = subform.getRow(0)

			boolean first = true

			// NUCLOS-6489 q) Test ENTER and ARROW_RIGHT
			fieldsInSubOrder.each { String field, boolean editable ->
				NuclosWebElement cell = row.getFieldElement(field)

				if (first) {
					// Get focus once
					cell.click()
					sendKeys(Keys.ESCAPE)
					first = false
				}

				assert cell.hasClass('ag-cell-focus'): "$field does not have focus class"

				if (editable) {
					def value = row.getValue(field)

					sendKeys(Keys.ENTER) // Edit

					NuclosWebElement editor = row.getEditor(cell)
					assert editor: "No editor for $field"

					// Unedit
					if (editor.tagName == 'input' && !editor.hasClass('ui-autocomplete-input')) {
						sendKeys(Keys.ENTER)
					} else {
						// Textarea is not closed by Enter
						sendKeys(Keys.ESCAPE)
					}

					cell = row.getFieldElement(field)

					editor = row.getEditor(cell)

					assert !editor: "$field still has an editor"

					assert cell.hasClass('ag-cell-focus'): "$field does not have focus class"

					//field value should not change when editing is started only without "real" editing
					assert value == row.getValue(field)
				}

				sendKeys(Keys.ARROW_RIGHT)
				if (!field.toString().equals("nuclosState")) { // except last column (nuclosState)
					assert !cell.hasClass('ag-cell-focus'): "$field still has the focus class"
					// Focus lost => Navigation works.
				}
			}

			first = true
			// NUCLOS-6489 l) Test TAB
			fieldsInSubOrder.findAll { it.value }.each { String field, boolean editable ->
				NuclosWebElement cell = row.getFieldElement(field)
				if (first) {
					cell.click()
					first = false
				}

				NuclosWebElement editor = row.getEditor(cell)
				assert editor: "No editor for $field"

				// TODO: Also do some editing

				sendKeys(Keys.TAB)
			}
		}

		_08_saveTextarea:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Subform subform = eo.getSubform(orderSubBoMetaId)
			SubformRow row = subform.getRow(0)
			assert !row.getValue('note')
			row.getFieldElement('note').click()
			sendKeys('Test')

			eo.save()
			row = subform.getRow(0)
			assert row.getValue('note') == 'Test'
		}

		_10_checkDirty:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			Subform subform = eo.getSubform(orderSubBoMetaId)

			SubformRow row = subform.getRow(0)
			assert waitForNotNull(10, { return row.getValue('orderNumber') }, true,
					IllegalArgumentException.class // Could not find subform column for attribute 'orderNumber'
			) == '111'

			// TODO: Row becomes already dirty now, when the first '1' is entered
//		row.enterValue('orderNumber', '111')
			assert !row.isDirty()

			row.enterValue('orderNumber', '112')
			assert row.isDirty()

			eo.cancel()

			row = subform.getRow(0)
			assert !row.getValue('discount')
			row.enterValue('discount', '')

			assert !row.isDirty()

			assert !row.getValue('orderDate')
			row.enterValue('orderDate', '')

			assert !row.isDirty()

			row.enterValue('discount', '3')
			assert row.isDirty()

			eo.cancel()
		}

		_12_editSubformEntryInModal:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Subform subform = eo.getSubform(orderSubBoMetaId)
			countBrowserRequests {
				subform.getRow(0).editInModal()
			}.with {
				assert it.getRequestCount(RequestType.EO_READ) == 0

				// 1 request for the details of the subform record
				// + 1 request for the subform in the modal
				assert it.getRequestCount(RequestType.EO_SUBFORM) == 3
			}

			EntityObjectModal eoModal = EntityObjectComponent.forModal()
			parentRefFieldShouldBeDisabled:
			{
				assert eoModal.getAttribute('customer') == '42'
				assert eoModal.isAttributeReadonly('customer')
			}

			assert eoModal.getAttribute('orderNumber') == '111'
			eoModal.setAttribute('orderNumber', 222)

			eoModal.clickButtonOk()

			eo.save()

			assert subform.rowCount == 1

			SubformRow row = subform.getRow(0)
			assert row.getValue('orderNumber') == '222'
		}

		_15_testDynamicButtonActivation:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTSUBFORMBUTTONS)
			eo.addNew()

			Subform subform = eo.getSubform('nuclet_test_other_TestSubformButtonsDependent_reference')
			checkEmptySubform:
			{
				assert subform.newEnabled
				assert !subform.deleteEnabled
				assert !subform.cloneEnabled
			}

			SubformRow row = subform.newRow()
			assert !row.selected

			checkSubformWithoutSelection:
			{
				assert subform.newEnabled
				assert !subform.deleteEnabled
				assert !subform.cloneEnabled
			}

			row.selected = true
			assert row.selected

			checkSubformWithSelection:
			{
				assert subform.newEnabled
				assert subform.deleteEnabled
				assert subform.cloneEnabled
			}

			eo.cancel()
		}

		_30_createBos:
		{
			EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_CUSTOMER, customer)
		}

		_32_testEditLinksForIgnoredSublayout:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Subform subform = eo.getSubform(subBoMetaId)
			SubformRow subformRow = subform.newRow()
			assert subformRow.new
			assert !subformRow.canEditInModal()
			assert !subformRow.canEditInWindow()

			def data = [city: 'Test', zipCode: '123', street: 'Test']
			subformRow.enterValues(data)

			eo.save()
			subformRow = subform.getRow(0)
			assert !subformRow.canEditInModal()
			assert subformRow.canEditInWindow()

			subformRow.enterValue('city', 'Test 2')
			assert !subformRow.canEditInModal()
			assert !subformRow.canEditInWindow()

			eo.cancel()
			subformRow = subform.getRow(0)
			assert !subformRow.canEditInModal()
			assert subformRow.canEditInWindow()

			subformRow.setSelected(true)
			subform.deleteSelectedRows()
			assert !subformRow.canEditInModal()
			assert !subformRow.canEditInWindow()

			eo.save()
		}

		_34_testEditLinksForEnabledSublayout:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			eo.selectTab('Orders')

			Subform subform = eo.getSubform(orderSubBoMetaId)
			SubformRow subformRow = subform.newRow()
			assert subformRow.new
			assert subformRow.canEditInModal()
			assert !subformRow.canEditInWindow()

			EntityObjectModal eoModal = EntityObjectComponent.forModal()
			eoModal.setAttribute('orderNumber', '123')
			eoModal.clickButtonOk()
			assert subformRow.canEditInModal()
			assert !subformRow.canEditInWindow()

			eo.save()
			subformRow = subform.getRow(0)
			assert subformRow.canEditInModal()
			assert !subformRow.canEditInWindow()

			subformRow.enterValue('orderNumber', '1234')
			assert !subformRow.canEditInModal()
			assert !subformRow.canEditInWindow()

			eo.cancel()
			subformRow = subform.getRow(0)
			assert subformRow.canEditInModal()
			assert !subformRow.canEditInWindow()

			subformRow.editInModal()
			eoModal = EntityObjectComponent.forModal()
			eoModal.setAttribute('orderNumber', '1234')
			eoModal.clickButtonOk()
			assert subformRow.canEditInModal()
			assert !subformRow.canEditInWindow()

			eo.save()
			subformRow = subform.getRow(0)
			assert subformRow.getValue('orderNumber') == '1234'

			subformRow.setSelected(true)
			subform.deleteSelectedRows()
			assert !subformRow.canEditInModal()
			assert !subformRow.canEditInWindow()

			eo.save()
		}

		_35_createEntry:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.selectTab('Address')

			Subform subform = eo.getSubform(subBoMetaId)
			SubformRow subformRow = subform.newRow()
			assert subformRow.new
			// Editing should start automatically for first cell of new row - check if there is a cell editor
			assert subformRow.getEditor(subformRow.getFieldElement('city'))

			// Expect validation error for empty subform row
			eo.save()

			// FIXME: Create proper error dialog
//		assert EntityObject.getErrorMessages().size() == 1
//		assert EntityObject.getErrorStacktraces().first().getAttribute('innerHTML').contains('common.exception.novabitvalidationexception')
			eo.clickButtonOk()

			assert EntityObjectComponent.getErrorMessages().size() == 0

			// Enter valid data
			def data = [city: 'Sauerlach', zipCode: '82054', street: 'Mühlweg 2']
			subformRow.enterValues(data)

			assert subformRow.getValue('city') == data.city
			assert subformRow.getValue('zipCode') == data.zipCode
			assert subformRow.getValue('street') == data.street

			eo.save()
			subformRow = subform.getRow(0)

			assert subformRow.getValue('city') == data.city
			assert subformRow.getValue('zipCode') == data.zipCode
			assert subformRow.getValue('street') == data.street

			subformRow = subform.newRow()
			data = [city: 'Sauerlach_2', zipCode: '82054_2', street: 'Mühlweg 2_A']
			subformRow.enterValues(data)

			subformRow = subform.newRow()
			data = [city: 'Sauerlach_3', zipCode: '82054_3', street: 'Mühlweg 2_B']
			subformRow.enterValues(data)

			subformRow = subform.newRow()
			data = [city: 'Sauerlach_4', zipCode: '82054_4', street: 'Mühlweg 2_C']
			subformRow.enterValues(data)

			assert subform.getRowCount() == 4

			// FIXME
//		assert EntityObject.getTab('tab_' + subBoMetaId + '_customer').getText() == 'Address (4)'

			assert subformRow.new

			countBrowserRequests {
				eo.save()
			}.with {
				assert it.getRequestCount(RequestType.EO_UPDATE) == 1
				assert it.getSqlCount(RequestType.EO_UPDATE) < 5

				// All initialized subforms are reloaded (Address, Orders)
				assert it.getRequestCount(RequestType.EO_SUBFORM) == 13

				assert it.requestCount < 16
				assert it.sqlCount < 10
				assert it.sqlTime < 100
			}
			subformRow = subform.getRow(0)

			assert !subformRow.new
			assert !subformRow.dirty
			assert !subformRow.deleted

			assert subform.getRowCount() == 4
		}

		_37_editInWindow:
		{

			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer')
			SubformRow row = subform.getRow(0)

			assert row.canEditInWindow()
			subform.getEditInWindowLink(0).click()

			editInOtherWindow:
			{
				switchToOtherWindow()

				EntityObjectComponent eo2 = EntityObjectComponent.forDetail()
				eo2.setAttribute('street', 'a')
				eo2.save()

				switchToOtherWindow()
				closeOtherWindows()
			}

			assert row.getValue('street') == 'a'
			assert row.canEditInWindow()
		}

		/**
		 * The "edit in new window" link should not be visible,
		 * if the user has no rights on the target entity.
		 */
		_38_editInWindowWithoutRights:
		{
			logout()
			login('readonly', 'readonly')

			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer')
			SubformRow row = subform.getRow(0)

			assert !row.canEditInWindow()

			logout()
			login('test', 'test')
		}

		_39_testHiddenSelectionCheckboxViaLafParameter:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

			Subform subformAddresses = eo.getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer')
			assert subformAddresses.getSelectAllElement().isDisplayed()

			// Look and feel parameter hides the default checkbox selection column
			eo.selectTab('Matrix-Zuordnung')
			Subform subformMatrixZuordnungen = eo.getSubform(TestEntities.NUCLET_TEST_MATRIX_MATRIXZUORDNUNG, 'customer')
			assert !subformMatrixZuordnungen.getSelectAllElement().isDisplayed()

			eo.selectTab('Address')
		}

		_40_updateEntry:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Subform subform = eo.getSubform(subBoMetaId)
			SubformRow subformRow = subform.getRow(1)
			def data = [city: '', zipCode: '', street: '']

			// TODO: Uncomment the following code block when NUCLOS-4220 is fixed
			/*
	subformRow.enterValues(data);


	// Expect validation error for empty subform row
	EntityObject.save();
	expect(Sideview.getErrorMessages().count()).toBe(1);
	expect(Sideview.getErrorStacktraces().first().getInnerHtml()).toContain('common.exception.novabitvalidationexception');
	Sideview.clickOkButton();
	expect(Sideview.getErrorMessages().count()).toBe(0);
	*/
			data = [city: 'Sauerlach_5', zipCode: '82054_5', street: 'Mühlweg 2_5']

			assert !subformRow.dirty
			assert !eo.dirty
			subformRow.enterValues(data)
			assert subformRow.dirty
			assert eo.dirty

			eo.save()
			// Neu Selektieren damit die Maske g'scheit ausschaut (NUCLOS-5479)
			Sidebar.selectEntry(0)
			subform = eo.getSubform(subBoMetaId)

			for (int i = 0; i < subform.rowCount; i++) {
				if (subform.getRow(i).getValue('city') == data.city) {
					Log.debug 'Matching row: ' + i
					subformRow = subform.getRow(i)

					assert subformRow.getValue('city') == data.city
					assert subformRow.getValue('zipCode') == data.zipCode
					assert subformRow.getValue('street') == data.street
				}
			}
		}

		_42_checkInputIsNotLost:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Subform subform = eo.getSubform(subBoMetaId)
			SubformRow subformRow = subform.getRow(0)

			assert !subformRow.dirty
			assert !eo.dirty
			String oldStreet = subformRow.getValue('street')
			String newStreet = "$oldStreet 2"

			enterValueWithoutLeavingEditor:
			{
				NuclosWebElement cell = subformRow.getFieldElement('street')
				cell.click()
				sendKeys(newStreet)
				sendKeys(Keys.ENTER)
			}

			assert eo.dirty
			assert subformRow.dirty

			eo.save()
			subformRow = subform.getRow(0)

			assert subformRow.getValue('street') == newStreet

			subformRow.enterValue('street', oldStreet)
			eo.save()
		}

		_45_testCloning:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Subform subform = eo.getSubform(subBoMetaId)
			subform.selectAllRows()
			assert subform.rowCount == 4

			assert !eo.dirty
			subform.cloneSelectedRows()
			assert subform.getRow(0).new
			assert eo.dirty

			eo.save()

			// uncheck "select all rows" checkbox
			subform.unselectAllRows()
			assert subform.rowCount == 8

			// FIXME
//		assert EntityObject.getTab('tab_' + subBoMetaId + '_customer').getText() == 'Address (8)'

		}

		_52_testDirtyState:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.getTab('Orders').click()

			Subform orderSubform = eo.getSubform(orderSubBoMetaId)
			SubformRow row = orderSubform.newRow()

			// Close the edit modal
			EntityObjectComponent.clickButtonOk()

			row.enterValues([orderNumber: '123'])

			eo.save()

			assert orderSubform.rowCount == 1

			rowShouldBecomeDirtyWhileEditing:
			{
				assert !row.new
				assert !row.dirty
				assert !row.deleted

				NuclosWebElement cell = row.getFieldElement('orderNumber')
				cell.click()
				assert !row.dirty
				sendKeys('1234')
				sendKeys(Keys.ENTER)
				assert row.dirty

				eo.save()
			}

			// Reference field
			assertDirtyStateAndReset {
				orderSubform.getRow(0).enterValue('customerAddress', 'Sauerlach, 82054, Mühlweg 2')
			}

			// Date chooser
			assertDirtyStateAndReset {
				orderSubform.getRow(0).enterValue('sendDate', '2017-12-31')
			}
		}

		_55_deleteEntry:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.getTab('Address').click()
			Subform subform = eo.getSubform(subBoMetaId)

			subform.unselectAllRows()

			int rowCount = subform.rowCount

			subform.getRow(1).selected = true
			subform.getRow(3).selected = true

			screenshot('before-deletion')
			assert !eo.dirty
			assert !subform.getRow(1).deleted
			subform.deleteSelectedRows()
			assert subform.getRow(1).deleted
			assert eo.dirty
			screenshot('after-deletion')

			assert subform.rowCount == rowCount
			assert subform.deletedRowCount == 2

			eo.save()

			assert subform.getRowCount() == rowCount - 2

			subform.selectAllRows()
			for (int i = 0; i < subform.rowCount; i++) {
				assert subform.getRow(i).selected
			}

			subform.deleteSelectedRows()

			assert subform.deletedRowCount == subform.rowCount

			eo.save()

			assert subform.rowCount == 0
		}

		_88_openDynamicSubformEntity:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPROXYCONTAINER)
			screenshot('After Open Dynamic Subform Entity')

			assert eo.getAttribute('name') == 'Test 2'

			eo.selectTab('Dynamic Proxy')
			Subform subform = eo.getSubform('nuclet_test_other_DynamicProxyContainerDYN_INTIDTUDGENERICOBJECT')

			subform.getRow(0).editInWindow()

			checkOtherWindow:
			{
				switchToOtherWindow()

				EntityObjectComponent eo2 = EntityObjectComponent.forDetail()
				assert eo2.id == eo.id
				assert currentUrl.contains('/' + TestEntities.NUCLET_TEST_OTHER_TESTPROXYCONTAINER.fqn + '/')

				switchToOtherWindow()
				closeOtherWindows()
			}
		}

		_90_tabKeyTest:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
			Sidebar.refresh()
			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')


			SubformRow firstRow = subform.newRow()

			NuclosWebElement priceWe = firstRow.getFieldElement('price')

			priceWe.click()
			sendKeys('33')

			// Tab to quantity
			sendKeys(Keys.TAB)
			sendKeys('222')
			sendKeys(Keys.ENTER)

			def price = firstRow.getValue('price')
			assert price == '33.00' || price == '33,00'
			def quantity = firstRow.getValue('quantity')
			assert quantity == '222.00' || quantity == '222,00'

			// NUCLOS-6489 j) Don't leave editing cell by left-right arrow keys

			priceWe.click()
			sendKeys(Keys.ARROW_RIGHT)
			sendKeys(Keys.ARROW_LEFT)
			sendKeys(Keys.BACK_SPACE)
			sendKeys('6')
			sendKeys(Keys.TAB)

			price = firstRow.getValue('price')
			assert price == '33.60' || price == '33,60'

			// Test escape key
			priceWe.click()
			sendKeys('75')
			sendKeys(Keys.ESCAPE)

			price = firstRow.getValue('price')
			assert price == '33.60' || price == '33,60'

			// NUCLOS-6489 i) Send directly without pressing Enter

			priceWe.sendKeys('1')
			sendKeys('01')
			sendKeys(Keys.TAB)

			price = firstRow.getValue('price')
			assert price == '101.00' || price == '101,00'

			eo.cancel()
		}

		_100_SubformEditColumnPositionTest:
		{
			logout()
			nuclosSession.loginParams.username = 'nuclos'
			nuclosSession.loginParams.password = ''
			login(nuclosSession.loginParams)

			setupSystemParameters()
			refresh()
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
			eo.addNew()
			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.getEditColumnPosition() == 'left'

			nuclosSession.setSystemParameters([
					'nuclos_LAF_Webclient_Subform_Position_Edit_Column': 'nuclos_LAF_Position_right'
			])
			refresh()
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.getEditColumnPosition() == 'right'

			nuclosSession.setSystemParameters([
					'nuclos_LAF_Webclient_Subform_Position_Edit_Column': 'nuclos_LAF_Position_left'
			])
			refresh()
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.getEditColumnPosition() == 'left'
			eo.cancel()
		}

		_101_SubformDisplaysNuclosRowColor: {
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_RESOURCE)
			Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING, 'planningtableresource')
			SubformRow uncoloredRow = subform.getRow(0)
			SubformRow nuclosRowColoredRow = subform.getRow(1)

			assert nuclosRowColoredRow.bodyRow.getAttribute('nuclos-row-color') == '#0000FF': 'nuclos-row-color attribute should be present at row element'
			assert nuclosRowColoredRow.bodyRow.getCssValue('background-color') == 'rgba(0, 0, 255, 1)': 'nuclosRowColor should affect the row element'
			assert nuclosRowColoredRow.bodyRow.getCssValue('color') == 'rgba(255, 255, 255, 1)': 'The background of this row should force the text color to be white'
			assert uncoloredRow.bodyRow.getCssValue('background-color') == 'rgba(248, 248, 248, 1)': 'row without color should have no background'
			assert uncoloredRow.bodyRow.getCssValue('color') == 'rgba(0, 0, 0, 1)': 'Rows without background should have black text color'
		}
	}

	@Test
	void testSubformCopyAndPaste() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		eo.addNew()
		eo.setAttribute('name', 'irrelevant')
		eo.setAttribute('customerNumber', '777')
		eo.selectTab('Orders')
		Subform orderSubform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER, 'customer')
		SubformRow row1 = orderSubform.newRow()
		row1.enterValue('orderNumber', '123');
		SubformRow row2 = orderSubform.newRow()
		row2.enterValue('orderNumber', '456');
		eo.save()

		testCopyAndPasteRows: {
			eo.selectTab('Orders')

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER, 'customer')
			subform.toggleMoreActionsPopup()
			assert !subform.copyEnabled: 'The subform copy button should be disabled because the selection is currently empty'
			assert !subform.pasteEnabled: 'The paste button should be disabled because the clipboard is empty'

			// select 2 rows
			subform.toggleSelection(0)
			subform.toggleSelection(1)
			subform.toggleMoreActionsPopup()
			assert subform.copyEnabled: 'The subform copy button should be enabled because the selection is now not empty and the subform bo qualifies for cloning'
			assert !subform.pasteEnabled: 'The paste button should be disabled because the clipboard is empty'

			// copy the selected rows to the clipboard
			subform.copySelectedRows()
			subform.toggleMoreActionsPopup()
			assert subform.copyEnabled: 'Copying the selected rows should not disable the copy button, because the selection will not be cleared when copying'
			assert subform.pasteEnabled: 'The paste button should now be enabled because the clipboard contains copied rows for this subform bo'

			// click paste button
			subform.pasteSelectedRows()

			SubformRow clonedRow1 = subform.getRow(0)
			SubformRow clonedRow2 = subform.getRow(1)

			// assert 2 new rows present
			assert clonedRow1.isNew()
			assert clonedRow2.isNew()

			// assert that the unique fields were not set
			assert !clonedRow1.getValue('orderNumber')
			assert !clonedRow2.getValue('orderNumber')

			clonedRow1.enterValue('orderNumber', '246');
			clonedRow2.enterValue('orderNumber', '255');

			// assert that saving is possible without error
			eo.save()
		}
		testPasteDisabledForNonMatchingSubBo: {
			// switch to another subform
			eo.selectTab('Address')
			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer')

			// assert that the paste button is disabled
			assert !subform.copyEnabled
			assert !subform.pasteEnabled: 'Pasting the current clipboard content should not be possible for this subform'
		}
		testCopyPasteAcrossDifferentMainBos: {
			eo.selectTab('Orders')

			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER, 'customer')
			subform.toggleSelection(2)
			Subform subsubform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			//  article, price
			SubformRow sfRow1
			EntityObjectModal eoModal = EntityObjectComponent.forModal({
				 sfRow1 = subsubform.newRow()
			})
			eoModal.setAttribute('price', '21')
			eoModal.setAttribute('article', '2000 Test-Article')
			eoModal.clickButtonOk()
			// quantity 1,00     versandt  categoryTextual

			SubformRow sfRow2
			eoModal = EntityObjectComponent.forModal({
				sfRow2 = subsubform.newRow()
			})
			eoModal.setAttribute('price', '54')
			eoModal.setAttribute('article', '2000 Test-Article')
			eoModal.clickButtonOk()

			eo.save()

			eo.selectTab('Orders')
			subsubform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			subsubform.toggleSelection(0)
			subsubform.toggleSelection(1)
			subsubform.toggleMoreActionsPopup()
			subsubform.copySelectedRows()

			EntityObjectComponent articleEo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
			Subform orderPositionSubform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'article')
			orderPositionSubform.toggleMoreActionsPopup()
			orderPositionSubform.pasteSelectedRows()

			SubformRow clonedRow1 = orderPositionSubform.getRow(0)
			SubformRow clonedRow2 = orderPositionSubform.getRow(1)

			// assert 2 new rows present
			assert clonedRow1.isNew()
			assert clonedRow2.isNew()

			assert clonedRow1.getValue('order') == '456'
			assert clonedRow1.getValue('price') == '21,00'
			assert clonedRow2.getValue('order') == '456'
			assert clonedRow2.getValue('price') == '54,00'
			articleEo.cancel()
		}
		testCopyPasteNewEntries: {
			EntityObjectComponent customerEo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
			Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer')
			SubformRow sfRow = subform.newRow()
			sfRow.enterValue('city', 'Test City')
			sfRow.enterValue('zipCode', '11111')
			sfRow.enterValue('street', 'Test Street')
			subform.toggleSelection(0)
			subform.toggleMoreActionsPopup()
			subform.copySelectedRows()
			subform.toggleMoreActionsPopup()
			subform.pasteSelectedRows()
			subform.getRow(0).getValue('city') ==  'Test City'
			subform.getRow(0).getValue('zipCode') ==  '11111'
			subform.getRow(0).getValue('street') ==  'Test Street'
			subform.getRow(1).getValue('city') ==  'Test City'
			subform.getRow(1).getValue('zipCode') ==  '11111'
			subform.getRow(1).getValue('street') ==  'Test Street'
			customerEo.cancel()
		}
	}

	@Test
	void _60_testSubFormFiltering() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute('orderNumber', '9999')
		waitForAngularRequestsToFinish()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')


		SubformRow firstRow = subform.newRow()

		firstRow.enterValue('article', '2000 Test-Article')

		eo.save()

		assert $('.fa-filter').getCssValue('color') == 'rgba(33, 37, 41, 1)'

		/*testTextFilter:
		{
			subform.toggleFilterForHeader('name')
			AgTextFilter textFilter = new AgTextFilter()
			assert 'Enthält' == textFilter.filterOptionString()
			assert '' == textFilter.filteredText()

			textFilter.enterFilterText('Test')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount > 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('name', 'Test', 'Enthält')

			assert $('.fa-filter').getCssValue('color') == 'rgba(51, 122, 183, 1)'

			subform.toggleFilter()
			refresh()
			assertTextualRepresentationFilterPrefs('name', 'Test', 'Enthält')
			assert $('.fa-filter').getCssValue('color') == 'rgba(51, 122, 183, 1)'

			subform.toggleFilter()

			subform.toggleFilterForHeader('name')
			textFilter.selectFilterOption('Enthält nicht')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('name', 'Test', 'Enthält nicht')

			subform.toggleFilterForHeader('name')
			textFilter.enterFilterText('Test-Article')
			subform.toggleFilterForHeader('name')
			textFilter.selectFilterOption('Gleich')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount > 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('name', 'Test-Article', 'Gleich')

			subform.toggleFilterForHeader('name')
			textFilter.selectFilterOption('Ungleich')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('name', 'Test-Article', 'Ungleich')

			//reset filter
			subform.toggleFilterForHeader('name')
			textFilter.clearFilter()

			assert $('.fa-filter').getCssValue('color') == 'rgba(33, 37, 41, 1)'
		}

		testReferenceFilter:
		{
			subform.toggleFilterForHeader('article')
			AgTextFilter textFilter = new AgTextFilter()
			assert 'Enthält' == textFilter.filterOptionString()
			assert '' == textFilter.filteredText()

			textFilter.enterFilterText('Test')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount > 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('article', 'Test', 'Enthält')

			subform.toggleFilterForHeader('article')
			textFilter.selectFilterOption('Enthält nicht')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('article', 'Test', 'Enthält nicht')

			subform.toggleFilterForHeader('article')
			textFilter.enterFilterText('2000 Test-Article')
			subform.toggleFilterForHeader('article')
			textFilter.selectFilterOption('Gleich')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount > 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('article', '2000 Test-Article', 'Gleich')

			subform.toggleFilterForHeader('article')
			textFilter.selectFilterOption('Ungleich')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('article', '2000 Test-Article', 'Ungleich')

			//reset filter
			subform.toggleFilterForHeader('article')
			textFilter.clearFilter()
		}

		testNumberFilter:
		{
			subform.toggleFilterForHeader('quantity')
			AgTextFilter numberFilter = new AgTextFilter()
			assert 'Gleich' == numberFilter.filterOptionString()
			assert '' == numberFilter.filteredText()

			numberFilter.enterFilterText('1')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount > 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('quantity', '1', 'Gleich')


			subform.toggleFilterForHeader('quantity')
			numberFilter.selectFilterOption('Ungleich')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('quantity', '1', 'Ungleich')

			subform.toggleFilterForHeader('quantity')
			numberFilter.selectFilterOption('Kleiner')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('quantity', '1', 'Kleiner')

			subform.toggleFilterForHeader('quantity')
			numberFilter.selectFilterOption('Kleiner oder Gleich')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount > 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('quantity', '1', 'Kleiner oder Gleich')

			subform.toggleFilterForHeader('quantity')
			numberFilter.selectFilterOption('Größer')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('quantity', '1', 'Größer')

			subform.toggleFilterForHeader('quantity')
			numberFilter.selectFilterOption('Größer oder Gleich')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount > 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('quantity', '1', 'Größer oder Gleich')

			//reset filter
			subform.toggleFilterForHeader('quantity')
			numberFilter.clearFilter()
		}

		testBooleanFilter:
		{
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			subform.scrollToColumn('shipped')
			subform.toggleFilterForHeader('shipped')
			NuclosBooleanFilter booleanFilter = new NuclosBooleanFilter()
			assert 'all' == booleanFilter.getCurrentSelection()

			booleanFilter.selectOption('true')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertBooleanFilterPrefs('true')

			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			subform.scrollToColumn('shipped')
			subform.toggleFilterForHeader('shipped')
			booleanFilter.selectOption('false')
			assert subform.rowCount > 0
			// ensure preferences are correctly saved
			assertBooleanFilterPrefs('false')

			//reset filter
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			subform.scrollToColumn('shipped')
			subform.toggleFilterForHeader('shipped')
			booleanFilter.clearFilter()
		}*/

		testDateFilter:
		{
			subform.toggleFilterForHeader('createdAt')
			AgTextFilter dateFilter = new AgTextFilter()
			assert 'Gleich' == dateFilter.filterOptionString()
			assert '' == dateFilter.filteredText()

			Date dateNow = new Date();
			String sDateNow = new SimpleDateFormat('yyyy-MM-dd').format(dateNow)
			String sDateInput = new SimpleDateFormat('MMddyyyy').format(dateNow)

			dateFilter.filterText.click()
			sendKeys(Keys.TAB)
			sendKeys(Keys.TAB)
			sendKeys(sDateInput)
			sendKeys(Keys.ENTER)
			sendKeys(Keys.ESCAPE)
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount > 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('createdAt', sDateNow, 'Gleich')


			subform.toggleFilterForHeader('createdAt')
			dateFilter.selectFilterOption('Ungleich')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('createdAt', sDateNow, 'Ungleich')

			subform.toggleFilterForHeader('createdAt')
			dateFilter.selectFilterOption('Kleiner')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('createdAt', sDateNow, 'Kleiner')

			subform.toggleFilterForHeader('createdAt')
			dateFilter.selectFilterOption('Größer')
			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
			assert subform.rowCount == 0
			// ensure preferences are correctly saved
			assertTextualRepresentationFilterPrefs('createdAt', sDateNow, 'Größer')

			// TODO inRange
//			subform.toggleFilterForHeader('createdAt')
//			numberFilter.selectFilterOption('Im Bereich')
//			subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
//			assert subform.rowCount > 0
//			// ensure preferences are correctly saved
//			assertTextualRepresentationFilterPrefs('createdAt', '1', 'Im Bereich')

			//reset filter
			subform.toggleFilterForHeader('createdAt')
			dateFilter.initFilter()
			dateFilter.filterText.clear()
		}
	}

	@Test
	void testNUCLOS_10273() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute('orderNumber', '9999')
		waitForAngularRequestsToFinish()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')


		SubformRow firstRow = subform.newRow()

		firstRow.enterValue('article', '2000 Test-Article')

		eo.save()

		assert $('.fa-filter').getCssValue('color') == 'rgba(33, 37, 41, 1)'

		subform.toggleFilterForHeader('createdAt')
		AgTextFilter dateFilter = new AgTextFilter()

		Date dateNow = new Date();
		String sDateNow = new SimpleDateFormat('yyyy-MM-dd').format(dateNow)
		String sDateInput = new SimpleDateFormat('MMddyyyy').format(dateNow)

		dateFilter.filterText.click()
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		sendKeys(sDateInput)
		sendKeys(Keys.ENTER)
		sendKeys(Keys.ESCAPE)

		subform.toggleFilterForHeader('createdAt')
		dateFilter.selectFilterOption('Ungleich')
		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert subform.rowCount == 0
		// ensure preferences are correctly saved
		assertTextualRepresentationFilterPrefs('createdAt', sDateNow, 'Ungleich')

		eo.addNew()

		// Before the fix for NUCLOS-10273 this refresh in combination with the assertion afterwards would have failed
		refresh()

		assert $('.fa-filter').getCssValue('color') != 'rgba(33, 37, 41, 1)'
	}

	@Test
	void NUCLOS_8901_VLP_Test() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subform.newRow()
		subform.newRow()

		// setting both categories to 'Hardware' should show same article VLPs
		subform.getRow(1).enterValue('category', 'Hardware')
		waitUntilTrue {
			subform.getRow(1).getLOV('article').choiceElements.text.containsAll(
					['1003 Notebook Pro', '1004 Mouse']
			)
		}

		subform.getRow(0).enterValue('category', 'Hardware')
		waitUntilTrue {
			subform.getRow(0).getLOV('article').choiceElements.text.containsAll(
					['1003 Notebook Pro', '1004 Mouse']
			)
		}

		// changing first added row category to 'Software', last row added VLP should still show initial article VLP
		subform.getRow(1).enterValue('category', 'Software')
		waitUntilTrue {
			subform.getRow(1).getLOV('article').choiceElements.text.containsAll(
					['1001 Nuclos', '1002 Text processor']
			)
		}
		sendKeys(Keys.ESCAPE) // closing LOV
		waitUntilTrue {
			subform.getRow(0).getLOV('article').choiceElements.text.containsAll(
					['1003 Notebook Pro', '1004 Mouse']
			)
		}

		// changing last added row category to 'Software' should update its article VLP correctly
		subform.getRow(0).enterValue('category', 'Software')
		waitUntilTrue {
			subform.getRow(0).getLOV('article').choiceElements.text.containsAll(
					['1001 Nuclos', '1002 Text processor']
			)
		}

		// changing first added row category to 'Hardware' should update its article VLP, but remain for last added row
		subform.getRow(1).enterValue('category', 'Hardware')
		waitUntilTrue {
			subform.getRow(1).getLOV('article').choiceElements.text.containsAll(
					['1003 Notebook Pro', '1004 Mouse']
			)
		}
		sendKeys(Keys.ESCAPE) // closing LOV
		waitUntilTrue {
			subform.getRow(0).getLOV('article').choiceElements.text.containsAll(
					['1001 Nuclos', '1002 Text processor']
			)
		}

		eo.cancel()
	}

	@Test
	void subformHyperlinkFieldTest() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		/* Add the hyperlinkTest Column to the subform view first. */
		subform.openViewConfiguration()
		ViewConfigurationModal.findElementContainingText(".select-item", "hyperlinkTest").click()
		$('#btn-selected-marked').click()
		ViewConfigurationModal.close()

		/* Enter an anchor tag hyperlink with title = "Test Title" and destination = "127.0.0.1". */
		subform.scrollToColumn("createdAt") // we need to first scroll to here as agGrid seems to remove headers from DOM if there is not enough space
		subform.scrollToColumn("hyperlinktest")
		SubformRow firstRow = subform.newRow()
		firstRow.clickCell('hyperlinktest')
		firstRow.enterValue('hyperlinktest', "<a href=\"127.0.0.1\">Test Title</a>")

		/* Assure that only the title of the anchor tag gets displayed but clicking opens the destination. */
		assert firstRow.getValue('hyperlinktest', String.class) == 'Test Title'
		firstRow.clickCell('hyperlinktest')

		checkNewWindowAndCloseIt() {
			getDriver().currentUrl == '127.0.0.1'
		}

		eo.cancel()
	}

	@Test
	void newEntryStateRelatedRestrictions() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute('orderNumber', '0123456')


		assert eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order') != null

		eo.selectTab('Documents')
		assert eo.getSubform(TestEntities.ORG_NUCLOS_SYSTEM_GENERALSEARCHDOCUMENT, 'genericObject') == null

		eo.selectTab('Rechnungen')
		assert eo.getSubform(TestEntities.EXAMPLE_REST_INVOICE, 'order') == null
	}

	/**
	 * Requirement: Hitting the ENTER key on a focus subform should transfer the focus to the next element inside the subform.
	 * Old/wrong behaviour: No element was focussed, when no active button or input element was visible
	 * Correct behaviour: If there is no active toolbar button and no checkbox column, the first column header should receive the focus.
	 * The exact div, that should receive the focus is identified by its tabindex attribute.
	 *
	 * @see <a href="https://support.nuclos.de/browse/NUCLOS-10372">NUCLOS-10372</a>
	 */
	@Test
	void testSubformFocusEnterSkipInactiveButtons() {
		/** Hide all active subform toolbar buttons */
		final String WEBCLIENT_CSS =
				'''
					#export-subbo-list, #filter-bo-list, .view-preferences-button, .view-preferences-button-mainbar
					.export-subbos,.autonumber-move-up,.autonumber-move-down,.new-subbo,.open-more-actions-popup {
						display: none !important;
					}
				'''
		getNuclosSession().setSystemParameters(
				[
						'WEBCLIENT_CSS': WEBCLIENT_CSS,
		                'nuclos_LAF_Tool_Bar_Detail_Subform': '',
		                'nuclos_LAF_Webclient_Subform_Show_Selection_Column': 'false'
				]
		)

		TestDataHelper.insertTestData(nuclosSession)

		// is required for the webclient_css parameter to take effect
		refresh()

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		// focus the subform toolbar, without selecting a subform row
		subform.subformElement.$("nuc-subform-buttons").focus()
		sendKeys(Keys.ENTER)

		assert driver.switchTo().activeElement() == subform.subformElement.$("[tabindex]").getElement()
	}

	@Test
	void columnSelectionNavigableTest() {
		//NUCLOS-8964 make "selection" navigable

		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		SubformRow row = subform.newRow()
		NuclosWebElement elArticle = row.getFieldElement('article')
		elArticle.focus()
		sendKeys(Keys.ESCAPE)
		sendShiftTab()
		sendShiftTab()

		WebElement elActiveFocus = driver.switchTo().activeElement()
		assert $('.ag-row[row-index="0"] .ag-cell[col-id="rowSelection"]').getElement().equals(elActiveFocus)
	}

	@Test
	void subformEditableTest() {
		//NUCLOS-9006

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		waitForAngularRequestsToFinish()

		eo.addNew()
		eo.setAttribute('text', 'parent')
		eo.save()

		assert StateComponent.getCurrentStateNumber() == 10: 'initial state of BO "parent" must be "10 (All allowed)"'

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		3.times {
			SubformRow row = subform.newRow()
			row.enterValue('text', 'SF ' + it)
		}
		eo.save()

		subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		3.times {
			SubformRow row = subform.getRow(it)
			row.enterValue('text', 'SF_' + it)
		}
		eo.save()

		StateComponent.changeStateByNumeral(40)
		StateComponent.confirmStateChange()
		subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		3.times {
			SubformRow row = subform.getRow(it)
			assert row.getValue('text') == 'SF_' + it
			row.enterValue('text', 'SF ' + it)
		}
		assert !eo.isDirty()

		eo.clickButton('Rule All allowed')
		assert StateComponent.getCurrentStateNumber() == 10: 'state must be "10 (All allowed)" after button "Rule All allowed" has been clicked'
		subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		3.times {
			SubformRow row = subform.getRow(it)
			row.enterValue('text', 'SF ' + it)
		}
		eo.save()

		eo.clickButton('Rule Subform read-only')
		assert StateComponent.getCurrentStateNumber() == 40: 'state must be "40 (Subform read-only)" after button "Rule Subform read-only" has been clicked'
		subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		3.times {
			SubformRow row = subform.getRow(it)
			assert row.getValue('text') == 'SF ' + it
			row.enterValue('text', 'SF_' + it)
		}
		assert !eo.isDirty()
	}

	@Ignore("Not working on Jenkins")
	@Test()
	void columnRowEditIconKeyboardTest() {
		//NUCLOS-8964 make "selection" navigable

		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		$('.ag-row[row-index="0"] .ag-cell[col-id="rowSelection"]').focus()
		sendKeys(Keys.TAB)
		sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		assert getWindowsCount() == 2

		driver.switchTo().window(driver.windowHandles[1])
		waitForAngularRequestsToFinish()
		assert driver.title == 'Nuclos - Order position | 1004 Mouse'
	}

	@Test()
	void sendMessageContextHasClipboardFeature() {

		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		String customerName = eo.getAttribute('customer')
		eo.clickButton('Auftrag übermitteln')

		MessageModal msgBox = getMessageModal()
		assert msgBox.title == 'Customer Name': 'Correct message box should be found'
		assert msgBox.hasClipboardBtn(): 'Should have clipboard button'
		msgBox.copyToClipboard()
		msgBox.confirm()
		waitForAngularRequestsToFinish()

		// workaround to test if clipboard feature works by
		// pasting via CTRL+V into a field
		eo.getAttributeElement('note').focus()

		clearInput()

		Closure paste = {
			new Actions(driver)
				.keyDown(Keys.CONTROL)
				.sendKeys('v')
				.keyUp(Keys.CONTROL)
				.build()
				.perform()
		}

		paste()
		if (!waitFor(5, {eo.getAttribute('note') == customerName})) {
			// try again
			paste()
		}
		assert waitFor(5, {eo.getAttribute('note') == customerName}): 'Copy to clipboard has worked'
	}

	@Test
	void testSubformRespectsLayoutColumnDefinitions() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		Subform subform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		assert subform.columnHeaders.contains("Versendet"): "Label Versendet should be used instead of versandt"

		subform.openViewConfiguration()
		assert SideviewConfiguration.allUnselected.stream().noneMatch {it.getAttribute("attr-fqn") == "${TestEntities.EXAMPLE_REST_ORDERPOSITION}_primaryKey"}
		assert SideviewConfiguration.allSelected.stream().noneMatch {it.getAttribute("attr-fqn") == "${TestEntities.EXAMPLE_REST_ORDERPOSITION}_primaryKey"}
		SideviewConfiguration.clickButtonOk()

		waitForAngularRequestsToFinish()

		assert !subform.columnHeaders.contains("ID"): "Primary Key Column should not be visible due to layout setting"
	}

	@Test
	void dragColumnOutDoesChangeColumnDefinition() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()


		Subform subform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		def headerElement = subform.getHeaderElementByAttributeName("shipped")

		// drag header out of subform
		dragElementByOffset(headerElement, 0, -50)

		waitForAngularRequestsToFinish()
		assert !subform.getColumnHeaders().contains("Versendet"): "Header element should be removed from subform"

		refresh()
		subform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert !subform.getColumnHeaders().contains("Versendet"): "Header element should still be removed from subform"

		// test for sidebar again
		Sidebar.resizeRightMax()
		headerElement = Sidebar.getColumnHeaderElementOrNull("orderNumber")
		dragElementByOffset(headerElement, 0, -50)

		waitForAngularRequestsToFinish()

		assert !Sidebar.getColumnHeaders().contains("Order number"): "Order number should be removed from sidebar header"
		refresh()
		assert !Sidebar.getColumnHeaders().contains("Order number"): "Order number should still be removed from sidebar header"
	}

	@Test
	void testSubformSelection() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		eo.addNew()
		eo.setAttribute('text', 'testSubformSelection')
		eo.save()

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		Subform subsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform')
		Subform subsubsubform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM, 'subsubform')

		subform.openViewConfiguration()
		SideviewConfiguration.deselectAll()
		SideviewConfiguration.selectColumn(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.toString() + '_text')
		SideviewConfiguration.clickButtonOk()

		subform.clickColumnHeader('text')
		subsubform.clickColumnHeader('text')
		subsubsubform.clickColumnHeader('text')

		SubformRow row2 = subform.newRow()
		row2.enterValue('text', 'SF 2')
		SubformRow row1 = subform.newRow()
		row1.enterValue('text', 'SF 1')

		row1.selected = true
		SubformRow row1_3 = subsubform.newRow()
		row1_3.enterValue('text', 'SF 1.3')
		SubformRow row1_2 = subsubform.newRow()
		row1_2.enterValue('text', 'SF 1.2')
		SubformRow row1_1 = subsubform.newRow()
		row1_1.enterValue('text', 'SF 1.1')

		row1_2.selected = true
		SubformRow row1_2_2 = subsubsubform.newRow()
		row1_2_2.enterValue('text',  'SF 1.2.2')
		SubformRow row1_2_1 = subsubsubform.newRow()
		row1_2_1.enterValue('text',  'SF 1.2.1')

		row1_2_1.selected = true
		row1_2_2.selected = true

		eo.save()

		assert row1.selected
		assert row1_2.selected
		assert row1_2_1.selected
		assert row1_2_2.selected
	}

	@Test
	void testSubformInOverlayContext() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)

		eo.addNew()
		eo.setAttribute('name', 'Category "testSubformInOverlayContext"')
		eo.save()

		def sfArticle = eo.getSubform(TestEntities.EXAMPLE_REST_ARTICLE, 'category')
		sfArticle.newRow()
		EntityObjectModal modal = EntityObjectComponent.forModal()
		def sfOrderpos = modal.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'article')
		assert sfOrderpos != null
		EntityObjectComponent.clickButtonCancel()
		sfArticle.selectAllRows()
		sfArticle.deleteSelectedRows()

		eo.selectTab('Auftrag')
		def sfAuftrag = eo.getSubform(TestEntities.EXAMPLE_REST_AUFTRAG, 'kategorie')
		sfAuftrag.newRow()
		EntityObjectModal modalAuftrag = EntityObjectComponent.forModal()
		modalAuftrag.setAttribute('name', 'Auftrag "testSubformInOverlayContext"')
		def sfPos = modal.getSubform(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION, 'auftrag')
		assert sfPos == null

		assert $('nuc-web-subform div.no-subform-in-overlay') != null

		EntityObjectComponent.clickButtonOk()
		eo.save()

		eo.selectTab('Auftrag')
		sfAuftrag = eo.getSubform(TestEntities.EXAMPLE_REST_AUFTRAG, 'kategorie')
		sfAuftrag.editInModal(0)
		modalAuftrag = EntityObjectComponent.forModal()
		sfPos = modal.getSubform(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION, 'auftrag')
		assert sfPos == null

		assert $('nuc-web-subform div.no-subform-in-overlay a') != null
		$('nuc-web-subform div.no-subform-in-overlay a').click()

		switchToOtherWindow(true)
		EntityObjectComponent eoAuftrag = EntityObjectComponent.forDetail()
		sfPos = eoAuftrag.getSubform(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION, 'auftrag')
		assert sfPos != null
	}

	@Test
	void testXSSJavascriptInSubform() {
		def XSS_STRING = "<a href=\"javascript:document.getElementById('sidebar-container').innerHTML='<div><span>EMPTY</span></div>'\" onmouseover=\"document.getElementById('sidebar-container').innerHTML='<div><span>EMPTY</span></div>'\">This link contains javascript</a>";
		TestDataHelper.insertTestData(nuclosSession)

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		eo.setAttribute('note', XSS_STRING)
		eo.save()

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		Sidebar.selectEntryByText('Test-Customer')
		waitForAngularRequestsToFinish()

		EntityObjectComponent.forDetail().selectTab('Orders')
		waitForAngularRequestsToFinish()
		def subform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_ORDER, 'customer')
		// this block is needed to bring note column into screen
		subform.getHeaderElementByAttributeName('note')

		subform.getRow(0).getFieldElement('note').mouseover()
		waitForAngularRequestsToFinish()

		assert $('#sidebar-container').text != 'EMPTY'
	}

	@Test
	void NUCLOS_10208_testSubformPermissions() {
		logout()
		login('useradmin', 'useradmin')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLOS_USER)
		Sidebar.selectEntryByText('useradmin')
		def roleSubform = EntityObjectComponent.forDetail().getSubform(TestEntities.NUCLOS_USERROLES, 'user')
		assert roleSubform.getRow(0).getLOV('role').hasEntry('Example user')
	}

	/**
	 * Test verifying that hitting ESC closes the modal dialogs in the reverse order they appeared on screen.
	 * NUCLOS-10509: There was a bug, that closed the modal behind closing before the ViewConfiguration modal.
	 */
	@Test
	void NUCLOS_10509_testShortcutModalCloseOrder() {
		logout()
		login('nuclos')
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.selectTab('Rechnungen')

		Subform subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_INVOICE, 'order')
		// opens the first modal
		EntityObjectComponent.forModal({subform.newRow()})

		// opens the second modal
		Subform invoicePosSubform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_INVOICEPOSITION, 'invoice')
		invoicePosSubform.openViewConfiguration()

		assert $$('.modal-title').last().text.contains('InvoicePosition'): 'The View Configuration modal should have opened'

		// should close the second modal first
		sendKeys(Keys.ESCAPE)
		assert !$('.modal-title')?.text?.contains('InvoicePosition'): 'Hitting Esc should have closed the view configuration first'

		// should close the first modal
		sendKeys(Keys.ESCAPE)
		assert $('.modal-title') == null: 'All modals should have been closed'
	}

	/**
	 * If a Subform is based on a non-editable BO, it should not be possible to create new entries.
	 * @see <a href="https://support.nuclos.de/browse/NUCLOS-9484">NUCLOS-9484</a>
	 */
	@Test
	void testSubformDisabledForNonEditableSubformBO() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENTFORSTATEFULSUBFORM)
		eo.addNew()

		Subform subform = EntityObjectComponent.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORMSTATUSMODEL, 'parent')
		assert subform.subformElement.$('.new-subbo') != null: 'The add button should be visible for this subform'

		Subform subsubform = EntityObjectComponent.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORMSTATUSMODEL, 'subform')
		assert subsubform.subformElement.$('.new-subbo') == null: 'The stateful subsubform is not editable, thus the add button should not be present.'
	}

	@Test
	void testPinnedColumns() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()

		Subform subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subform.openViewConfiguration()
		SideviewConfiguration.deselectAll()
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_article')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_name')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_category')
		SideviewConfiguration.pinAll()
		assertMessageModalAndConfirm('Fehler', 'Es dürfen nicht alle Spalten fixiert werden.') {
			SideviewConfiguration.clickButtonOk {}
		}

		subform.openViewConfiguration()
		SideviewConfiguration.unpinAll()
		SideviewConfiguration.pin(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_article')
		SideviewConfiguration.clickButtonOk {}
		assert subform.getPinnedAttributeColumns(TestEntities.EXAMPLE_REST_ORDERPOSITION) == [TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_article']

		subform.openViewConfiguration()
		SideviewConfiguration.unpinAll()
		SideviewConfiguration.clickButtonOk {}
		assert subform.getPinnedAttributeColumns(TestEntities.EXAMPLE_REST_ORDERPOSITION) == []
	}

	// NUCLOS-10602
	@Test
	void testDynamicEntityModification() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute('orderNumber', '123')

		Subform subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		def row = subform.newRow()
		row.getLOV('article').selectEntry('2000 Test-Article')

		eo.save()

		eo.selectTab('Dynamic Position')
		Subform dynamicSubform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITIONDYN, 'INTIDTUDGENERICOBJECT')
		row = dynamicSubform.getRow(0)
		assert row.getValue('name') == 'Test-Article'
		assert row.canEditInWindow()
		row.editInWindow()
		switchToOtherWindow()

		EntityObjectComponent posEo = EntityObjectComponent.forDetail()
		posEo.setAttribute('name', 'test')
		posEo.save()

		switchToOtherWindow()
		dynamicSubform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITIONDYN, 'INTIDTUDGENERICOBJECT')
		row = dynamicSubform.getRow(0)
		assert row.getValue('name') == 'test'
	}

	@Test
	void testHiddenSubformColumn() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute('orderNumber', '123')
		eo.save()

		Subform subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subform.openViewConfiguration()
		assert SideviewConfiguration.allUnselected.stream().noneMatch {it.getAttribute("attr-fqn") == "${TestEntities.EXAMPLE_REST_ORDERPOSITION}_hidden"}
		assert SideviewConfiguration.allSelected.stream().noneMatch {it.getAttribute("attr-fqn") == "${TestEntities.EXAMPLE_REST_ORDERPOSITION}_hidden"}
		SideviewConfiguration.clickButtonOk()

		eo.reload()
		subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subform.openViewConfiguration()
		assert SideviewConfiguration.allUnselected.stream().noneMatch {it.getAttribute("attr-fqn") == "${TestEntities.EXAMPLE_REST_ORDERPOSITION}_hidden"}
		assert SideviewConfiguration.allSelected.stream().noneMatch {it.getAttribute("attr-fqn") == "${TestEntities.EXAMPLE_REST_ORDERPOSITION}_hidden"}
		SideviewConfiguration.clickButtonOk()
	}

	@Test
	void testSubformCounts() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		assert eo.hasTab('Position (5)', true)
		assert eo.hasTab('Documents', true)
		assert eo.hasTab('Rechnungen', true)

		eo.setAttribute('note', 'B')
		assert eo.hasTab('Position (5)', true)
		assert eo.hasTab('Documents', true)
		assert eo.hasTab('Rechnungen', true)

		Subform sfPos = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		sfPos.selectAllRows()
		sfPos.cloneSelectedRows()
		eo.save()

		assert eo.hasTab('Position (10)', true)
		assert eo.hasTab('Documents', true)
		assert eo.hasTab('Rechnungen', true)
	}

	private void assertTextFilterPrefs(String text, String option) {
		EntityObjectComponent.getTab('Documents').click()
		waitForAngularRequestsToFinish()
		EntityObjectComponent.getTab('Position').click()
		waitForAngularRequestsToFinish()
		EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
				.toggleFilterForHeader('name')
		AgTextFilter textFilter = new AgTextFilter()
		assert option == textFilter.filterOptionString()
		assert text == textFilter.filteredText()
	}

	private void assertTextualRepresentationFilterPrefs(String headerName, String text, String option) {
		EntityObjectComponent.getTab('Documents').click()
		waitForAngularRequestsToFinish()
		EntityObjectComponent.getTab('Position').click()
		waitForAngularRequestsToFinish()
		EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
				.toggleFilterForHeader(headerName)
		AgTextFilter textFilter = new AgTextFilter()
		assert option == textFilter.filterOptionString()
		assert text == textFilter.filteredText()
	}

	private void assertBooleanFilterPrefs(String option) {
		EntityObjectComponent.getTab('Documents').click()
		waitForAngularRequestsToFinish()
		EntityObjectComponent.getTab('Position').click()
		waitForAngularRequestsToFinish()
		Subform subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		subform.scrollToColumn('shipped')
		subform.toggleFilterForHeader('shipped')
		NuclosBooleanFilter booleanFilter = new NuclosBooleanFilter()
		assert option == booleanFilter.getCurrentSelection()
		sendKeys(Keys.ESCAPE)
	}

	private void assertOverlayTextForEmpty(Subform subform) {
		assert ['Keine Daten zur Anzeige verfügbar.', 'No Rows to show.'].contains(subform.overlayText)
	}

	private void assertOverlayTextForLoading(Subform subform) {
		assert ['Lade...', 'Loading...'].contains(subform.overlayText)
	}

	private void assertDirtyStateAndReset(Closure c) {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert !eo.dirty
		c()
		assert eo.dirty
		eo.cancel()
	}

	private void checkNewWindowAndCloseIt(Closure check) {
		Set<String> handles = getDriver().getWindowHandles()

		assert handles.size() == 2

		// no angular page is opened here...
		boolean bWaitForAngular = false
		switchToOtherWindow(bWaitForAngular)

		check()

		switchToOtherWindow()
		closeOtherWindows()
	}

}
