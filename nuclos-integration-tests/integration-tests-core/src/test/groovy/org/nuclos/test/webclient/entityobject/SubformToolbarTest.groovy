package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformToolbarTest extends AbstractRerunnableWebclientTest {

	@Test
	void testToolbarHidden() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		eo.selectTab('Subform w/o Toolbar')
		def subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_SUBFORMNOTOOLBAR, 'reference')

		assert !subform.newVisible : '"Create a new entry" should not be visible.'
		assert !subform.deleteVisible : '"Delete selected entries" should not be visible.'
		assert !subform.cloneVisible : '"Clone selected entries" should not be visible.'
		assert !subform.moreActionsButtonVisible : '"Clone selected entries" should not be visible.'
		assert !subform.isViewPreferencesVisible() : '"View preferences config" should not be visible.'
	}

	@Test
	void testToolbarHorizontal() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		eo.selectTab('Subform Date')
		def subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSSUBFORM, 'parent')

		assert subform.newVisible : '"Create a new entry" should be visible.'
		assert subform.deleteVisible : '"Delete selected entries" should be visible.'
		assert subform.cloneVisible : '"Clone selected entries" should be visible.'
		assert subform.moreActionsButtonVisible : '"Clone selected entries" should be visible.'
		assert subform.isViewPreferencesVisible() : '"View preferences config" should be visible.'
		subform.toggleMoreActionsPopup()
		assert subform.copyVisible : '"Copy selected entries" should be visible.'
		assert subform.pasteVisible : '"Paste selected entries" should be visible.'

		assert subform.getSubformButtonsElement().getLocation().getY() < subform.getGrid().getLocation().getY()
		assert subform.getSubformButtonsElement().getLocation().getX() == subform.getGrid().getLocation().getX()
	}

	@Test
	void testToolbarVertical() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		eo.selectTab('Subform Memo')
		def subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSMEMO, 'parent')

		subform.toggleMoreActionsPopup()
		assert subform.newVisible : '"Create a new entry" should be visible.'
		assert subform.deleteVisible : '"Delete selected entries" should be visible.'
		assert subform.moreActionsButtonVisible : '"Clone selected entries" should be visible.'
		assert subform.copyVisible : '"Copy selected entries" should be visible.'
		assert subform.pasteVisible : '"Paste selected entries" should be visible.'
		assert subform.isViewPreferencesVisible() : '"View preferences config" should be visible.'

		assert subform.getSubformButtonsElement().getLocation().getY() == subform.getGrid().getLocation().getY()
		assert subform.getSubformButtonsElement().getLocation().getX() < subform.getGrid().getLocation().getX()
	}

}
