package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.openqa.selenium.Keys
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class TabindexTest extends AbstractRerunnableWebclientTest {
@Test
void runTest() {
	_05_tabAndEnter: {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_TABINDEX_TESTTABINDEX)

		eo.addNew()

		// TODO: Initial focus
		$('[name="attribute-a"]').focus()

		sendKeys('input 1')	// write text in first input field
		sendKeys(Keys.TAB)    // tab to next input field

		sendKeys('input 2') // write text in 2. input field
		sendKeys(Keys.TAB)    // tab to next input field

		sendKeys('input 3') // write text in 3. input field
		sendKeys(Keys.TAB)    // tab to next input field

		sendKeys('input 4') // write text in 4. input field
		sendKeys(Keys.TAB)    // tab to next input field

		sendKeys('input 5') // write text in 5. input field
		sendKeys(Keys.TAB)    // tab to next input field
		sendKeys(Keys.TAB)    // tab over toggletip

		sendKeys('input 6')
		sendKeys(Keys.TAB)

		sendKeys('input 7')
		sendKeys(Keys.TAB)

		// User LOV - must enter an existing user
		sendKeys('test')
		sendKeys(Keys.TAB)
		waitForAngularRequestsToFinish()

		sendKeys('input 9')
		sendKeys(Keys.TAB)

		sendKeys(' ')	// This is a checkbox - toggle it with space
		sendKeys(Keys.TAB)

		sendKeys('input 11')
		sendKeys(Keys.TAB)

		sendKeys('2018-09-25')
		sendKeys(Keys.TAB)

		sendNumber(38293)	// Integer input
		sendKeys(Keys.TAB)

		sendNumber(8372.38)
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB) // readonly must be tab focusable for accessibility

		sendKeys(' + again')	// We are back to the first input now
		sendKeys(Keys.TAB)

		assert eo.getAttribute('a') == 'input 1 + again'
		assert eo.getAttribute('b') == 'input 5'
		assert eo.getAttribute('c') == 'input 2'
		assert eo.getAttribute('d') == 'input 4'
		assert eo.getAttribute('e') == 'input 3'
		assert eo.getAttribute('encryptedtext') == 'input 6'
		assert eo.getAttribute('phone') == 'input 7'

		// FIXME: Works locally, but not on Jenkins
//		assert eo.getAttribute('reference') == 'test'

		assert eo.getAttribute('hyperlink') == 'input 9'
		assert eo.getAttribute('boolean', null, Boolean.class)
		assert eo.getAttribute('email') == 'input 11'
		assert eo.getAttribute('date', null, Date.class) == new Date(118, 8, 25)	// 25.09.2018
		assert eo.getAttribute('integer', null, Integer.class) == 38293
		assert eo.getAttribute('decimal', null, BigDecimal.class) == 8372.38

		eo.cancel()
	}

	/**
	 * Same like before, but tabbing in reverse order using SHIFT+TAB
	 */
	_10_tabAndEnterReverse: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()

		// TODO: Initial focus
		$('[name="attribute-a"]').focus()

		sendKeys('input 1')
		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendNumber(8372.38)

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendNumber(38293)	// Integer input

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendKeys('2018-09-25')

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendKeys('input 11')

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendKeys(' ')	// This is a checkbox - toggle it with space

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendKeys('input 9')

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		// User LOV - must enter an existing user
		sendKeys('test')

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()
		waitForAngularRequestsToFinish()

		sendKeys('input 7')

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendKeys('input 6')

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendKeys('input 5')

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendKeys('input 4')

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendKeys('input 3')

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendKeys('input 2')

		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()

		sendKeys(' + again')	// We are back to the first input now
		sendKeys(Keys.TAB)

		assert eo.getAttribute('a') == 'input 1 + again'
		assert eo.getAttribute('b') == 'input 5'
		assert eo.getAttribute('c') == 'input 2'
		assert eo.getAttribute('d') == 'input 4'
		assert eo.getAttribute('e') == 'input 3'
		assert eo.getAttribute('encryptedtext') == 'input 6'
		assert eo.getAttribute('phone') == 'input 7'

		// FIXME: Works locally, but not on Jenkins
//		assert eo.getAttribute('reference') == 'test'

		assert eo.getAttribute('hyperlink') == 'input 9'
		assert eo.getAttribute('boolean', null, Boolean.class)
		assert eo.getAttribute('email') == 'input 11'
		assert eo.getAttribute('date', null, Date.class) == new Date(118, 8, 25)    // 25.09.2018
		assert eo.getAttribute('integer', null, Integer.class) == 38293
		assert eo.getAttribute('decimal', null, BigDecimal.class) == 8372.38
	}
}

	@Test
	void correctPaneTabbingSetOnDefault() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_TABINDEX_TESTTABINDEX)
		eo.addNew()

		$('[name="attribute-memo"]').focus()
		sendKeys(Keys.TAB)

		assert checkCurrentActiveElement("input", ["name": "attribute-ort"]): "Tabbing from Memo should focus ort from first pane"
		sendKeys(Keys.TAB)
		assert checkCurrentActiveElement("input", ["name": "attribute-name"]): "Tabbing from Ort should focus name from first pane"
		sendKeys(Keys.TAB)
		assert checkCurrentActiveElement("input", ["name": "attribute-ort2"]): "Tabbing from name should focus ort2 from second pane"
		sendKeys(Keys.TAB)
		assert checkCurrentActiveElement("input", ["name": "attribute-name2"]): "Tabbing from ort2 should focus name2 from second pane"
		sendKeys(Keys.TAB)
		assert checkCurrentActiveElement("input", ["name": "attribute-unternehmen"]): "Tabbing from ort2 should focus unternehmen"

		// way back
		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()
		assert checkCurrentActiveElement("input", ["name": "attribute-name2"]): "Tabbing back from unternehmen should focus name2 from second pane"
		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()
		assert checkCurrentActiveElement("input", ["name": "attribute-ort2"]): "Tabbing back from name2 should focus ort2 from second pane"
		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()
		assert checkCurrentActiveElement("input", ["name": "attribute-name"]): "Tabbing back from ort2 should focus name from first pane"
		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()
		assert checkCurrentActiveElement("input", ["name": "attribute-ort"]): "Tabbing back from name should focus ort from first pane"
		new Actions(driver).keyDown(Keys.SHIFT).build().perform()
		sendKeys(Keys.TAB)
		new Actions(driver).keyUp(Keys.SHIFT).build().perform()
		assert checkCurrentActiveElement("textarea", ["name": "attribute-memo"]): "Tabbing back from name should focus memo"

		eo.cancel()
	}
}
