package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.schema.layout.web.WebHtmlEditor
import org.nuclos.schema.layout.web.WebInputComponent
import org.nuclos.schema.layout.web.WebTextarea
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class TextModuleTest extends AbstractRerunnableWebclientTest {

	@Test
	void runTest() {
		logout()
		login('nuclos', '')
		TestDataHelper.insertTextModules(nuclosSession)
		logout()
		login('test', 'test')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTHTMLEDITOR)

		eo.addNew()
		eo.setAttribute('subject', 'TextModuleTest')

		htmlEditor:
		{
			def htmlEditor = eo.getAttributeElement('content', WebHtmlEditor.class)
			assertTextModuleAndSelectItem(eo, htmlEditor, WebHtmlEditor.class, [['Name 1', 'Text 1'], ['Name 3', 'Text 3'], ['Name 2', 'Text 2']], 1, -1)
		}

		eo.save()

		textarea: {
			def textarea = eo.getAttributeElement('content', WebTextarea.class);

			assertTextModuleAndSelectItem(eo, textarea, WebTextarea.class, [['Name 1', 'Text 1'], ['Name 2', 'Text 2'], ['Name 3', 'Text 3']], 2, -1)
		}

	}

	private void assertTextModuleAndSelectItem(EntityObjectComponent eo, NuclosWebElement element, Class<? extends WebInputComponent> webElementClass, List<List<String>> menuItemsExpected, int selectIdx, int focusIdx) {
		element.focus()
		if (focusIdx == -1) {
			sendKeys(Keys.END)
		}

		WebElement textModuleComponent
		if (element.getTagName() == 'textarea') {
			textModuleComponent = element.findElement(By.xpath("./..//nuc-text-module"))
		} else {
			textModuleComponent = element.$('nuc-text-module').getElement()
		}

		//hover over text module component
		Actions actions = new Actions(driver)
		actions.moveToElement(textModuleComponent)
		actions.build().perform()

		assert textModuleComponent
		textModuleComponent.findElement(By.cssSelector('a')).click()

		def contextMenus = $$('.p-contextmenu').findAll {
			it.getCssValue('display') != 'none'
		}
		assert contextMenus.size() == 1
		def contextMenu = contextMenus.first()
		def menuItems = contextMenu.$$('p-contextmenusub li')
		assert menuItems.size() == menuItemsExpected.size()
		menuItems.eachWithIndex{ NuclosWebElement item, int idx ->
			assert item.text == menuItemsExpected[idx][0]
		}

		def oldValue = (String) eo.getAttribute('content', webElementClass)

		menuItems[selectIdx].click()
		waitForAngularRequestsToFinish()
		assert eo.getAttribute('content', webElementClass) == oldValue + menuItemsExpected[selectIdx][1]
		def activeElement = driver.switchTo().activeElement()
		if (element.getTagName() == 'textarea') {
			// TODO fix focus
//			assert element.getElement() == activeElement: 'Corresponding input component should get focus after selection of text module'
		} else {
			assert element.getElement().findElements(By.tagName(activeElement.getTagName())).contains(activeElement): 'Corresponding input component should get focus after selection of text module'
		}
	}
}
