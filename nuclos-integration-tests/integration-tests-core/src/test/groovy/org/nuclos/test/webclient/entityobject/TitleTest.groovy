package org.nuclos.test.webclient.entityobject


import org.json.JSONObject
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.businesstest.BusinessTestsAdvanced
import org.nuclos.test.webclient.pageobjects.businesstest.BusinessTestsIndex
import org.nuclos.test.webclient.pageobjects.preference.Preferences

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class TitleTest extends AbstractRerunnableWebclientTest {

	@Test
	void _01_testEntityObjectTitle() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)

		JSONObject metaJson = nuclosSession.getEntityMeta(TestEntities.EXAMPLE_REST_CATEGORY)
		assert getTitle().equals('Nuclos - ' + metaJson.get('name'))

		eo.addNew()
		assert getTitle().equals('Nuclos - ' + metaJson.get('name'))

		def sName1 = 'test1'
		eo.setAttribute('name', sName1)
		eo.save()
		assert getTitle().equals('Nuclos - ' + metaJson.get('name') + ' | ' + sName1)

		eo.addNew()
		def sName2 = 'test2'
		eo.setAttribute('name', sName2)
		eo.save()
		assert getTitle().equals('Nuclos - ' + metaJson.get('name') + ' | ' + sName2)

		Sidebar.selectEntryByText(sName1)
		assert getTitle().equals('Nuclos - ' + metaJson.get('name') + ' | ' + sName1)

		eo.delete()
		assert getTitle().equals('Nuclos - ' + metaJson.get('name') + ' | ' + sName2)

		eo.delete()
		assert getTitle().equals('Nuclos - ' + metaJson.get('name'))
	}

	@Test
	void _02_testPreferenceTitle() {
		Preferences.open()

		assert getTitle().equals('Nuclos - ' + (Locale.ENGLISH.equals(getLocale()) ? 'Manage preferences' : 'Einstellungen verwalten'))
	}

	@Test
	void _03_testBusinessTestsTitle() {
		logout()
		login('nuclos','')

		BusinessTestsIndex.open()
		assert getTitle().equals('Nuclos - ' + (Locale.ENGLISH.equals(getLocale()) ? 'Business tests' : 'Business-Tests'))

		BusinessTestsAdvanced.open()
		assert getTitle().equals('Nuclos - ' + (Locale.ENGLISH.equals(getLocale()) ? 'Business tests | Advanced view' : 'Business-Tests | Expertenmodus'))
	}

	@Test
	void testDynamicTooltip() {
		TestDataHelper.insertTestData(nuclosSession)

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		eo.setAttribute('note', 'Das ist ein dynamischer Tooltip\n\n<b>Fetter Text</b>')
		eo.save()

		eo.getAttributeElement('Button_Send').toggleTooltip()
		assert eo.getAttributeElement('Button_Send').getActiveTooltip() == 'Das ist ein dynamischer Tooltip<br><br><b>Fetter Text</b>'

		Sidebar.selectEntry(1)

		eo = EntityObjectComponent.forDetail()

		eo.getAttributeElement('Button_Send').toggleTooltip()
		assert eo.getAttributeElement('Button_Send').getActiveTooltip() == 'B'
	}

	@Test
	void titleWithoutAppName() {
		nuclosSession.setSystemParameters(['APP_NAME':'<none>'])
		refresh()

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		var documentTitle = getDriver().getTitle()
		assert documentTitle == 'Order': 'Application name should not be displayed if APP_NAME is set to "<none>"'
	}

	@Test
	void titleFromTemplate() {
		nuclosSession.setSystemParameters(['WEBCLIENT_TITLE_TEMPLATE':'{{title}} ({{bo}})'])
		refresh()

		logout()
		assert getDriver().getTitle() == 'Login ()'

		login('test', 'test')
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		assert getDriver().getTitle() == '(Order)'

		eo.addNew()
		eo.setAttribute('orderNumber', 123)
		eo.save()
		assert getDriver().getTitle() == '123 (Order)'

		nuclosSession.setSystemParameters(['WEBCLIENT_TITLE_TEMPLATE':'{{application}} - {{title}}'])
		refresh()

		assert getDriver().getTitle() == 'Nuclos - 123'

		nuclosSession.setSystemParameters(['WEBCLIENT_TITLE_TEMPLATE':'{{application}} - {{title}}', 'APP_NAME':'MyApp'])
		refresh()
		assert getDriver().getTitle() == 'MyApp - 123'

		nuclosSession.setSystemParameters(['WEBCLIENT_TITLE_TEMPLATE':'{{application}} - {{title}}', 'APP_NAME':'<none>'])
		refresh()
		assert getDriver().getTitle() == '- 123'
	}
}
