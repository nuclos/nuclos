package org.nuclos.test.webclient.entityobject

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class VLPTest extends AbstractRerunnableWebclientTest {

	private static String name = 'VLP Test'
	private static String name2 = 'VLP Test 2'
	private static String name3 = 'VLP Test 3'

	@Before
	void setup() {
		 TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void runTest() {
		_00_setup:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTVLP)

			eo.addNew()
			eo.setAttribute('name', name)
			eo.setAttribute('default', false)
			eo.save()

			eo.addNew()
			eo.setAttribute('name', name2)
			eo.setAttribute('default', true)
			eo.save()

			eo.addNew()
			eo.setAttribute('name', name3)
			eo.setAttribute('default', true)
			eo.save()
		}

		_05_vlpForMainRecord:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			ListOfValues lov = eo.getLOV('vlpreference')
			lov.open()

			assert lov.choices == ['', name3]
		}

		_10_vlpForSubform:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTVLPSUBFORM, 'parent')

			SubformRow row = subform.newRow()

			ListOfValues lov = row.getLOV('vlpreference')
			assert lov.open

			assert lov.choices == ['', name3]

			eo.cancel()
		}

		_15_vlpWithDefaultNewEO:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.addNew()
			assert eo.getAttribute('vlpdefaultnew') == name2

			ListOfValues lov = eo.getLOV('vlpdefaultnew')
			lov.open()

			assert lov.choices == ['', name, name2, name3]

			eo.cancel()
		}

		_20_vlpWithDefault:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			assert eo.getAttribute('vlpdefault') == ''

			ListOfValues lov = eo.getLOV('vlpreference')
			lov.selectEntry(name3)

			// checks also translated FQNs here (NUCLOS-9658)
			String PROCESS_nuclet_test_other_TestVLP_VLPProcessParameter = 'LNhsXPDK23rCWIRChKkR'
			String STATUS_nuclet_test_other_VLPTestSM_State_100 = 'UaWUgPqv0FMDDKC5pwJW'

			def expected = String.format('%s&processId=%s&statusId=%s', // temporary name from VLP here
					name2,
					'',
					STATUS_nuclet_test_other_VLPTestSM_State_100)


			def attribute = eo.getAttribute('vlpdefault')
			assert attribute == expected

			ListOfValues lovProcess = eo.getLOV('nuclosProcess')
			lovProcess.selectEntry('VLP Process Parameter')
			// reset to enable defaults again
			eo.setAttribute('vlpdefault', {id: null})
			eo.setAttribute('vlpreference', {id: null})
			eo.save()
			lov.selectEntry(name3)

			assert eo.getAttribute('vlpdefault') == String.format('%s&processId=%s&statusId=%s', // temporary name from VLP here
					name2,
					PROCESS_nuclet_test_other_TestVLP_VLPProcessParameter,
					STATUS_nuclet_test_other_VLPTestSM_State_100)
		}
	}

	@Test
	void testVLPDefault_NUCLOS9100() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		assert Sidebar.listEntryCount == 6: 'There should be 6 orders after test start'
		assert !EntityObjectComponent.forDetail().dirty: 'Intial load should not dirty - no vlp change should be triggered'

		Sidebar.selectEntry(2)
		waitForAngularRequestsToFinish()
		assert !EntityObjectComponent.forDetail().dirty: 'Change should not trigger vlp change'

		EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()
		waitForAngularRequestsToFinish()

		assert eo.dirty: 'New eo should be added'
		assert eo.getAttribute('customer') == '': 'No customer selected'
		assert eo.getAttribute('customerAddress') == '': 'No customer address selected'

		eo.getLOV('customer').selectEntry('VLPText: Test-Customer')
		eo.setAttribute('orderNumber', 9000)
		assert eo.getAttribute('customer') != '': 'Customer selected'
		assert eo.getAttribute('customerAddress') != '': 'Customer address should be default selected'

		def oldValue = eo.getAttribute('customerAddress')
		eo.getLOV('customer').selectEntry('VLPText: Test-Customer 2')
		assert eo.getAttribute('customerAddress') != oldValue: 'Customer address should have changed accordingly'

		eo.save()

		Sidebar.selectEntry(1)
		oldValue = EntityObjectComponent.forDetail().getAttribute('customerAddress')
		EntityObjectComponent.forDetail().getLOV('customer').selectEntry('VLPText: Test-Customer 2')
		assert EntityObjectComponent.forDetail().dirty: 'EO should have changed'
		assert oldValue != EntityObjectComponent.forDetail().getAttribute('customerAddress'): 'Selecting customer should change to default vlp'
		EntityObjectComponent.forDetail().save()
		waitForAngularRequestsToFinish()
	}

	@Test
	void testCloneContextForEntry_NUCLOS_10594() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		eo.clone()
		ListOfValues lov = eo.getLOV("customerAddress")

		assert lov.choices.size() == 3: 'When the bug is fixed, there should be 3 choices visible, instead of just the empty choice.'

		eo.cancel()
	}

	@Test
	void vlpReferenceShouldBeEditableWithoutBeingClosed() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		EntityObjectComponent.forDetail().getLOV("customernoclose").openReference()
		waitForAngularRequestsToFinish()

		assert driver.windowHandles.size() == 2: 'Reference window should have opened'

		switchToOtherWindow(true)
		EntityObjectComponent.forDetail().setAttribute("name", "Test-Customer Name-Changed")
		EntityObjectComponent.forDetail().save()

		assert driver.windowHandles.size() == 2: 'Reference window still opened'
		switchToOtherWindow(true)

		assert EntityObjectComponent.forDetail().getAttribute("customernoclose") == "Test-Customer Name-Changed": 'Change should be applied'
		assert !EntityObjectComponent.isDirty(): 'Order should not be dirty'
	}

	@Test
	void vlpSearchReferenceTarget() {
		//setup
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTVLP)

		eo.addNew()
		eo.setAttribute('name', name)
		eo.setAttribute('default', false)
		eo.save()

		eo.addNew()
		eo.setAttribute('name', name2)
		eo.setAttribute('default', true)
		eo.save()

		eo.addNew()
		eo.setAttribute('name', name3)
		eo.setAttribute('default', true)
		eo.save()

		//combobox
		def vlpreference = eo.getLOV('vlpreference')
		assert vlpreference.hasEntry(name3)
		assert vlpreference.getChoices().size() == 2

		vlpreference.searchReference()
		switchToOtherWindow()
		assert Sidebar.listEntryCount == 1
		switchToOtherWindow()
		closeOtherWindows()
		vlpreference.close()

		//lov
		def vlpdefault = eo.getLOV('vlpdefault')
		assert vlpdefault.getChoices().size() == 1
		vlpdefault.searchReference()
		switchToOtherWindow()
		assert Sidebar.listEntryCount == 0
		switchToOtherWindow()
		closeOtherWindows()
		vlpdefault.close()
	}

	@Test
	void vlpNoRefreshAfterCancel() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTVLP)

		eo.addNew()
		eo.setAttribute('name', name)
		eo.setAttribute('default', true)
		eo.save()

		def vlpReference = eo.getLOV('vlpreference')
		vlpReference.selectEntry(name)
		vlpReference.selectEntry('')
		eo.save()

		vlpReference = eo.getLOV('vlpreference')
		vlpReference.selectEntry(name)
		assert eo.getAttribute('vlpdefault')
		eo.save()

		eo.addNew()
		eo.cancel()

		eo = EntityObjectComponent.forDetail()
		assert eo.getAttribute('vlpdefault')
	}

	@Test
	void vlpSubformSearchReferenceTarget() {
		//setup
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTVLP)

		eo.addNew()
		eo.setAttribute('name', name2)
		eo.setAttribute('default', false)
		eo.save()

		eo.addNew()
		eo.setAttribute('name', name)
		eo.setAttribute('default', false)
		eo.save()

		def subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTVLPSUBFORM, 'parent')
		def row = subform.newRow()

		row.getLOV('vlpreference').lov.mouseover()
		$zZz('a.search-reference').click()

		switchToOtherWindow()
		assert Sidebar.listEntryCount == 1
		switchToOtherWindow()
		closeOtherWindows()
	}

	@Test void vlpDefaultWithBooleanParameter() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTVLP)

		eo.addNew()
		eo.setAttribute('name', name2)
		eo.setAttribute('default', true)
		eo.save()

		eo.addNew()
		eo.setAttribute('name', name)
		eo.setAttribute('default', false)
		eo.save()

		eo.addNew()
		assert eo.getAttribute('default') == true : 'attribute \'default\' must have default value set to \'true\''
		assert eo.getAttribute('vlpdefaultnew2') == name2

		refresh()
		eo = EntityObjectComponent.forDetail()
		assert eo.getAttribute('default') == true : 'attribute \'default\' must have default value set to \'true\''
		assert eo.getAttribute('vlpdefaultnew2') == name2
	}

	@Test void textVLP() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTVLP)

		eo.addNew()
		eo.setAttribute('name', name)

		def textVLP = eo.getLOV('textvlp')
		textVLP.open()
		assert textVLP.getSelectedEntryFromDropdown() == ''

		eo.save()
		textVLP = eo.getLOV('textvlp')
		textVLP.open()
		assert textVLP.getSelectedEntryFromDropdown() == ''

		textVLP.selectEntry('3 three')
		textVLP.open()
		assert textVLP.getSelectedEntryFromDropdown() == '3 three'

		eo.save()
		textVLP = eo.getLOV('textvlp')
		textVLP.open()
		assert textVLP.getSelectedEntryFromDropdown() == '3 three'

		textVLP.selectEntry('')
		textVLP.open()
		assert textVLP.getSelectedEntryFromDropdown() == ''
	}

	@Test
	void testVLPWithoutFieldName() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_INVOICE)
		eo.addNew()
		def lovOrder = eo.getLOV('order')
		assert lovOrder.getChoices().size() > 0
	}
}
