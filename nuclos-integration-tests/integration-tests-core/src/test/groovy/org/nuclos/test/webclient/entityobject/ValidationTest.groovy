package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.Dialog
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow
import org.nuclos.test.webclient.validation.ValidationStatus
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ValidationTest extends AbstractRerunnableWebclientTest {

@Test
void runTest() {
	_00_setup: {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTVALIDATION)
		eo.addNew()
	}

	_04_testInitiallyValid: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		// Booleans are initially set to false (not null)
		waitUntilTrue({eo.getValidationStatus('requiredboolean') == ValidationStatus.VALID})
	}

	_05_testInitiallyMissing: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		waitUntilTrue({eo.getValidationStatus('requiredtext') == ValidationStatus.MISSING})
		waitUntilTrue({eo.getValidationStatus('requireddate') == ValidationStatus.MISSING})
		waitUntilTrue({eo.getValidationStatus('requiredemail') == ValidationStatus.MISSING})
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.MISSING})
		waitUntilTrue({eo.getValidationStatus('requireddouble92') == ValidationStatus.MISSING})
		waitUntilTrue({eo.getValidationStatus('requiredmemo') == ValidationStatus.MISSING})
		waitUntilTrue({eo.getValidationStatus('requiredreference') == ValidationStatus.MISSING})
		waitUntilTrue({eo.getValidationStatus('123aeoeuess') == ValidationStatus.MISSING})
	}

	_06_testInitiallyValid: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.getValidationStatus('integerminmax') == ValidationStatus.VALID
		eo.getValidationStatus('dateminmax') == ValidationStatus.VALID
	}

	_07_trySave: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('integerminmax', '0')
		assertMessageModalAndConfirm('Fehler', 'Die Eingabe im Feld "Integer Min/Max" des Businessobjektes "Test Validation" liegt nicht im zulässigen Wertebereich von "5" bis "2000000000".', {eo.save()})
	}

	_08_testSaveInvalidInput: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('integerminmax', '2') // Min: 5, Max: 2000000000
		eo.setAttribute('doubleminmax', '0,99') // Min: 10, Max: 300
		eo.setAttribute('dateminmax', '2000-01-01') // Min: 2017-01-01, Max: 2099-12-31

		assertMessageModalAndConfirm('Fehler', 'Die Eingabe im Feld "Date Min/Max" des Businessobjektes "Test Validation" liegt nicht im zulässigen Wertebereich von "01.01.2017" bis "31.12.2099".', {eo.save()})

		waitUntilTrue({eo.getValidationStatus('integerminmax') == ValidationStatus.INVALID})
		waitUntilTrue({eo.getValidationStatus('doubleminmax') == ValidationStatus.INVALID})
		waitUntilTrue({eo.getValidationStatus('dateminmax') == ValidationStatus.INVALID})
	}

	_09_testSaveValidInput: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('integerminmax', '5') // Min: 5, Max: 2000000000
		eo.setAttribute('doubleminmax', '12,12') // Min: 10, Max: 300
		eo.setAttribute('dateminmax', '2017-01-01') // Min: 2017-01-01, Max: 2099-12-31

		assertMessageModalAndConfirm('Fehler', '', {eo.save()})

		waitUntilTrue({eo.getValidationStatus('integerminmax') == ValidationStatus.VALID})
		waitUntilTrue({eo.getValidationStatus('dateminmax') == ValidationStatus.VALID})

	}

	_10_fillRequiredFields: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requiredtext', 'text')
		eo.setAttribute('requireddate', new Date())
		eo.setAttribute('requiredemail', 'test@example.com')
		eo.setAttribute('requiredinteger', 123)
		eo.setAttribute('requireddouble92', 1.23)
		eo.setAttribute('requiredboolean', true)
		eo.setAttribute('requiredmemo', 'text')
		eo.setAttribute('requiredreference', 'nuclos')
		eo.setAttribute('123aeoeuess', 'text')
	}

	_15_testAllValid: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		waitUntilTrue({eo.getValidationStatus('requiredtext') == ValidationStatus.VALID})
		waitUntilTrue({eo.getValidationStatus('requireddate') == ValidationStatus.VALID})
		waitUntilTrue({eo.getValidationStatus('requiredemail') == ValidationStatus.VALID})
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID})
		waitUntilTrue({eo.getValidationStatus('requireddouble92') == ValidationStatus.VALID})
		waitUntilTrue({eo.getValidationStatus('requiredboolean') == ValidationStatus.VALID})
		waitUntilTrue({eo.getValidationStatus('requiredmemo') == ValidationStatus.VALID})
		waitUntilTrue({eo.getValidationStatus('requiredreference') == ValidationStatus.VALID})
		waitUntilTrue({eo.getValidationStatus('123aeoeuess') == ValidationStatus.VALID})
	}

	_20_testInvalidEmail: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requiredemail', 'a')
		waitUntilTrue({eo.getValidationStatus('requiredemail') == ValidationStatus.INVALID})

		eo.setAttribute('requiredemail', 'a@a.a')
		waitUntilTrue({eo.getValidationStatus('requiredemail') == ValidationStatus.INVALID})

		eo.setAttribute('requiredemail', 'test@nuclos.de@')
		waitUntilTrue({eo.getValidationStatus('requiredemail') == ValidationStatus.INVALID})
	}

	_22_testValidEmail: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requiredemail', 'test@nuclos.de')
		waitUntilTrue({eo.getValidationStatus('requiredemail') == ValidationStatus.VALID})
	}

	_24_testInvalidInteger: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requiredinteger', 'a')
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.INVALID})

		eo.setAttribute('requiredinteger', 'a1')
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.INVALID})
	}

	_26_testValidInteger: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requiredinteger', '0')
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID})
		waitUntilTrue({eo.getAttribute('requiredinteger') == '0'})

		eo.setAttribute('requiredinteger', '1')
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID})
		waitUntilTrue({eo.getAttribute('requiredinteger') == '1'})


		eo.setAttribute('requiredinteger', AbstractWebclientTest.context.locale == Locale.ENGLISH ? '.5' : ',5')
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID})
		waitUntilTrue({eo.getAttribute('requiredinteger') == '1'})

		eo.setAttribute('requiredinteger', '-1')
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID})
		waitUntilTrue({eo.getAttribute('requiredinteger') == '-1'})

		eo.setAttribute('requiredinteger', '1test')
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID})
		waitUntilTrue({eo.getAttribute('requiredinteger') == '1'})

		eo.setAttribute('requiredinteger', '1e10')
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID})
		waitUntilTrue({eo.getAttribute('requiredinteger') == '10000000000'})

		eo.setAttribute('requiredinteger', AbstractWebclientTest.context.locale == Locale.ENGLISH ? '1.2345' : '1,2345')
		waitUntilTrue({eo.getValidationStatus('requiredinteger') == ValidationStatus.VALID})
		waitUntilTrue({eo.getAttribute('requiredinteger') == '1'})
	}

	_28_testInvalidDate: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.enterText('requireddate', 'a')
		waitUntilTrue({eo.getValidationStatus('requireddate') == ValidationStatus.INVALID})

		eo.enterText('requireddate', '')
		waitUntilTrue({eo.getValidationStatus('requireddate') == ValidationStatus.MISSING})
	}

	_30_testValidDate: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('requireddate', '2017-03-04')
		waitUntilTrue({eo.getValidationStatus('requireddate') == ValidationStatus.VALID})
	}

	_50_testSave: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.save()

		assert !eo.dirty
	}

	_60_testRequiredFieldForStateChange: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		Sidebar.refresh()

		eo.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		eo.setAttribute('orderNumber', 123)
		eo.save()

		StateComponent.changeStateByNumeral(80)
		StateComponent.confirmStateChange()
		eo.clickButtonOk()
		waitUntilTrue({eo.getValidationStatus('customer') == ValidationStatus.MISSING})
	}

	_65_testSubformValidation: {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		SubformRow row = subform.newRow()

		// Stop editing
		sendKeys(Keys.ESCAPE)

		waitUntilTrue({row.getValidationStatus('article') == ValidationStatus.MISSING})
		waitUntilTrue({row.getValidationStatus('price') == ValidationStatus.MISSING})
		waitUntilTrue({[ValidationStatus.VALID, null].contains(row.getValidationStatus('quantity'))})

		// row.enterValue('price', 'asdf' + Keys.TAB) // css class is added after blur - tab out of cell
		def priceField = row.getFieldElement('price')
		priceField.click()
		sendKeys('asdf' + Keys.TAB)

		waitUntilTrue({row.getValidationStatus('price') == ValidationStatus.INVALID})

		row.enterValue('price', '123')
		waitUntilTrue({[ValidationStatus.VALID, null].contains(row.getValidationStatus('price'))})

	}
}
}
