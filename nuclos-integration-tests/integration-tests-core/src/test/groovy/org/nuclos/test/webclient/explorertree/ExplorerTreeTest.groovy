package org.nuclos.test.webclient.explorertree

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ExplorerTreeComponent
import org.openqa.selenium.By

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ExplorerTreeTest extends AbstractRerunnableWebclientTest {

	EntityObject country1
	EntityObject country2
	EntityObject country3

	@Before
	void setup() {
		country1 = new EntityObject(TestEntities.EXAMPLE_REST_COUNTRY)
		country1.setAttributes((Map) [name: 'Australien', world: [id: 4711]])
		country2 = new EntityObject(TestEntities.EXAMPLE_REST_COUNTRY)
		country2.setAttributes((Map) [name: 'Deutschland', world: [id: 4711]])
		country3 = new EntityObject(TestEntities.EXAMPLE_REST_COUNTRY)
		country3.setAttributes((Map) [name: 'Schweden', world: [id: 4711]])

		nuclosSession.save(country1)
		nuclosSession.save(country2)
		nuclosSession.save(country3)

		EntityObject city11 = new EntityObject(TestEntities.EXAMPLE_REST_CITY)
		city11.setAttributes((Map) [name: 'Sidney', einwohner: 5005000, land: [id: country1.id]])
		EntityObject city12 = new EntityObject(TestEntities.EXAMPLE_REST_CITY)
		city12.setAttributes((Map) [name: 'Perth', einwohner: 2066000, land: [id: country1.id]])


		EntityObject city21 = new EntityObject(TestEntities.EXAMPLE_REST_CITY)
		city21.setAttributes((Map) [name: 'Berlin', einwohner: 3671000, land: [id: country2.id]])
		EntityObject city22 = new EntityObject(TestEntities.EXAMPLE_REST_CITY)
		city22.setAttributes((Map) [name: 'München', einwohner: 1450000, land: [id: country2.id]])
		EntityObject city23 = new EntityObject(TestEntities.EXAMPLE_REST_CITY)
		city23.setAttributes((Map) [name: 'Frankfurt', einwohner: 733000, land: [id: country2.id]])

		nuclosSession.save(city11)
		nuclosSession.save(city12)

		nuclosSession.save(city21)
		nuclosSession.save(city22)
		nuclosSession.save(city23)
	}

	@Test
	void _01_testButtonVisibleInToolbar() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORLD)

		assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'
		assert $('#openTreeViewTab') != null: 'The open in tree view button could not be selected via its id'
	}

	@Test
	void _02_testOpenExplorerTree() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORLD)

		NuclosWebElement openInTreeButton = $('.fa-sitemap')

		assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

		openInTreeButton.click()

		switchToOtherWindow(true)

		assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
	}

	@Test
	void _03_testExplorerTreeShowChildren() {
		ExplorerTreeComponent explorerTree
		_setup: {
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORLD)

			NuclosWebElement openInTreeButton = $('.fa-sitemap')

			assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

			openInTreeButton.click()

			switchToOtherWindow(true)

			assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
			explorerTree = new ExplorerTreeComponent(new NuclosWebElement(driver.findElement(By.cssSelector('#explorer-tree')), '#explorer-tree'))
		}

		_testExpandByClick: {
			explorerTree.clickToggleExpansion('World')
			explorerTree.assertNodeExpanded('World')
		}

		_testWhichNodesAreLeaves: {
			String t1 = explorerTree.getNodeByTitle('Australien').parent.getAttribute('class')
			assert t1 == 'p-treenode ng-star-inserted'

			String t2= explorerTree.getNodeByTitle('Deutschland').parent.getAttribute('class')
			assert t2 == 'p-treenode ng-star-inserted'

			String t3 = explorerTree.getNodeByTitle('Schweden').parent.getAttribute('class')
			assert t3 == 'p-treenode p-treenode-leaf ng-star-inserted'
		}

	}

	@Test
	void _04_testExplorerTreeExpandAndCollapseAll() {
		_setup: {
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORLD)

			NuclosWebElement openInTreeButton = $('.fa-sitemap')

			assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

			openInTreeButton.click()

			switchToOtherWindow(true)

			assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
		}

		_testExpandAll: {
			assert driver.findElement(By.cssSelector('#explorer-tree .fa-expand')): 'The should be a collapse all button'
			NuclosWebElement expandButton = $('.fa-expand')
			assert expandButton: 'Expand all button should be visible.'
			expandButton.click()

			NuclosWebElement[] visibleNodes = $$('.p-treenode-content')
			assert visibleNodes.size() == 9
			// assert the visibility of all our test data entries
		}

		_testCollapseAll: {
			NuclosWebElement collapseButton = $('.fa-compress')
			assert collapseButton: 'Collapse all button should be visible.'
			collapseButton.click()

			NuclosWebElement[] visibleNodes = $$('.p-treenode-content')
			assert visibleNodes.size() == 1
			// assert that only the root node is visible and the rest of the test data is hidden
		}

	}

	@Test
	void _05_testExplorerTreeSingleNodeExpandAndCollapse() {
		ExplorerTreeComponent explorerTree
		_setup: {
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORLD)

			NuclosWebElement openInTreeButton = $('.fa-sitemap')

			assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

			openInTreeButton.click()

			switchToOtherWindow(true)

			assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
			explorerTree = new ExplorerTreeComponent(new NuclosWebElement(driver.findElement(By.cssSelector('#explorer-tree')), '#explorer-tree'))
		}

		_testExpandByClick: {
			explorerTree.clickToggleExpansion('World')
			explorerTree.assertNodeExpanded('World')
		}
		_testCollapseByClick: {
			explorerTree.clickToggleExpansion('World')
			explorerTree.assertNodeCollapsed('World')
		}
		_testExpandByKey: {
			explorerTree.keyToggleExpansionOnSelectedNode()
			explorerTree.assertNodeExpanded('World')
		}
		_testCollapseByKey: {
			explorerTree.keyToggleExpansionOnSelectedNode()
			explorerTree.assertNodeCollapsed('World')
		}
	}

	@Test
	void _06_testExplorerTreeRefresh() {
		ExplorerTreeComponent explorerTree
		_setup: {
			EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORLD)

			NuclosWebElement openInTreeButton = $('.fa-sitemap')

			assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

			openInTreeButton.click()

			switchToOtherWindow(true)

			assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
			explorerTree = new ExplorerTreeComponent($('#explorer-tree'))
			explorerTree.expandAll()
		}
		_setupRemoveExistingNode: {
			nuclosSession.delete(country3)
		}
		_setupInsertNewNods: {
			EntityObject country4 = new EntityObject(TestEntities.EXAMPLE_REST_COUNTRY)
			country4.setAttributes((Map) [name: 'Italien', world: [id: 4711]])

			nuclosSession.save(country4)

			EntityObject city24 = new EntityObject(TestEntities.EXAMPLE_REST_CITY)
			city24.setAttributes((Map) [name: 'Hamburg', einwohner: 1841000, land: [id: country2.id]])

			nuclosSession.save(city24)
		}
		_testRefresh: {
			waitForAngularRequestsToFinish()
			explorerTree.refresh()
			NuclosWebElement nodeElement = explorerTree.element.$('div.p-treenode-content[aria-label="' + 'Schweden' + '"]')
			assert nodeElement == null: 'Tree node \'Schweden\' should have been removed'
			explorerTree.assertNodeExpanded('Deutschland')
			explorerTree.assertNodeExpanded('Australien')
			explorerTree.assertNodeCollapsed('Italien')
			explorerTree.assertNodeCollapsed('Hamburg')
		}

	}

	@Test
	void _07_testOpenTreeNodeInMainView() {
		ExplorerTreeComponent explorerTree
		_setup: {
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORLD)

			NuclosWebElement openInTreeButton = $('.fa-sitemap')

			assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

			openInTreeButton.click()

			switchToOtherWindow(true)

			assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
			explorerTree = new ExplorerTreeComponent(new NuclosWebElement(driver.findElement(By.cssSelector('#explorer-tree')), '#explorer-tree'))
			explorerTree.expandAll()
		}
		testOpenByClick: {
			explorerTree.openNodeByClick('Deutschland')

			explorerTree.assertNodeSelected('Deutschland')
			assert getCurrentUrl().contains(country2.boMetaId + '/' + country2.id);
		}
		testOpenByKey: {
			explorerTree.selectNode('Schweden')
			explorerTree.openSelectedNodeByKey()

			explorerTree.assertNodeSelected('Schweden')
			assert getCurrentUrl().contains(country3.boMetaId + '/' + country3.id);
		}
	}

	@Test
	void _08_testSidebarHideListViewElements() {
		_setup: {
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORLD)

			NuclosWebElement openInTreeButton = $('.fa-sitemap')

			assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

			openInTreeButton.click()

			switchToOtherWindow(true)

			assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
		}
		_testSearchbarAndViewPreferencesHidden: {
			assert $('#searchbar') == null: 'Searchbar should not be visible, because it has no use, when the sidebar is showing the explorer tree'
			assert $('.view-preferences-button') == null: 'View preferences button should not be visible, because it has no use, when the sidebar is showing the explorer tree'
			assert $('.view-preferences-button-mainbar') == null: 'View preferences button mainbar should not be visible, because it has no use, when the sidebar is showing the explorer tree'
		}
	}

	@Test
	void _09_testPredecessorRelationNode() {
		_relation_setup: {
			EntityObject infiniteNodeA = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEA)
			infiniteNodeA.setAttribute('name', 'TestA')

			nuclosSession.save(infiniteNodeA)

			EntityObject<String> infiniteNodeB = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEB)
			infiniteNodeB.setAttribute('name', 'TestB')

			nuclosSession.save(infiniteNodeB)

			EntityObject predecessorRelation = new EntityObject(TestEntities.NUCLOS_GENERICOBJECTRELATION)
			predecessorRelation.setAttributes((Map) [source: infiniteNodeA, destination: infiniteNodeB, relationType: 'PredecessorOf'])

			nuclosSession.save(predecessorRelation)
		}
		_open_tree: {
			EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEA)

			NuclosWebElement openInTreeButton = $('.fa-sitemap')

			assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

			openInTreeButton.click()

			switchToOtherWindow(true)

			assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
		}
		_expand_node: {
			ExplorerTreeComponent explorerTree = new ExplorerTreeComponent(new NuclosWebElement(driver.findElement(By.cssSelector('#explorer-tree')), '#explorer-tree'))
			explorerTree.clickToggleExpansion('TestA')

			assert $('.fa-turn-up') != null

			explorerTree.clickToggleExpansion('TestB')

			assert $('.fa-turn-down') != null
		}
	}


	@Test
	void _10_testPartOfRelationNode() {
		_relation_setup: {
			EntityObject infiniteNodeA = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEA)
			infiniteNodeA.setAttribute('name', 'TestA')

			nuclosSession.save(infiniteNodeA)

			EntityObject<String> infiniteNodeB = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEB)
			infiniteNodeB.setAttribute('name', 'TestB')

			nuclosSession.save(infiniteNodeB)

			EntityObject predecessorRelation = new EntityObject(TestEntities.NUCLOS_GENERICOBJECTRELATION)
			predecessorRelation.setAttributes((Map) [source: infiniteNodeA, destination: infiniteNodeB, relationType: 'PartOf'])

			nuclosSession.save(predecessorRelation)
		}
		_open_tree: {
			EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEA)

			NuclosWebElement openInTreeButton = $('.fa-sitemap')

			assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

			openInTreeButton.click()

			switchToOtherWindow(true)

			assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
		}
		_expand_node: {
			ExplorerTreeComponent explorerTree = new ExplorerTreeComponent(new NuclosWebElement(driver.findElement(By.cssSelector('#explorer-tree')), '#explorer-tree'))
			explorerTree.clickToggleExpansion('TestA')

			assert $('.fa-object-ungroup') != null

			explorerTree.clickToggleExpansion('TestB')

			assert $('.fa-object-group') != null
		}
	}

	@Test
	void _11_testCustomRelationNode() {
		// Nuclos differentiates between system relations and user defined relations
		_relation_setup: {
			EntityObject infiniteNodeA = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEA)
			infiniteNodeA.setAttribute('name', 'TestA')

			nuclosSession.save(infiniteNodeA)

			EntityObject<String> infiniteNodeB = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEB)
			infiniteNodeB.setAttribute('name', 'TestB')

			nuclosSession.save(infiniteNodeB)

			EntityObject predecessorRelation = new EntityObject(TestEntities.NUCLOS_GENERICOBJECTRELATION)
			predecessorRelation.setAttributes((Map) [source: infiniteNodeA, destination: infiniteNodeB, relationType: 'CustomRelation'])

			nuclosSession.save(predecessorRelation)
		}
		_open_tree: {
			EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEA)

			NuclosWebElement openInTreeButton = $('.fa-sitemap')

			assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

			openInTreeButton.click()

			switchToOtherWindow(true)

			assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
		}
		_expand_node: {
			ExplorerTreeComponent explorerTree = new ExplorerTreeComponent(new NuclosWebElement(driver.findElement(By.cssSelector('#explorer-tree')), '#explorer-tree'))
			explorerTree.clickToggleExpansion('TestA')
			explorerTree.clickToggleExpansion('CustomRelation')

			assert $('.fa-turn-up') != null

			explorerTree.clickToggleExpansion('TestB')

			NuclosWebElement nodeElement = $$zZz('div.p-treenode-content[aria-label="' + 'CustomRelation' + '"]')[1]
			nodeElement.$('button').click()

			assert $('.fa-turn-down') != null
		}
	}

	@Test
	void _12_testButtonHiddenForInactiveTree() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)

		assert $('#openTreeViewTab') == null: 'The tree view button should be hidden'
	}

	@Test
	void _13_testExpandAllOnOpen() {
		EntityObject node1 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE)
		node1.setAttribute('name', 'Root')

		EntityObject node2 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE)
		node2.setAttribute('name', 'Layer 1')
		node2.setAttribute('reference', node1)

		EntityObject node3 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE)
		node3.setAttribute('name', 'Layer 2')
		node3.setAttribute('reference', node2)

		nuclosSession.save(node1)
		nuclosSession.save(node2)
		nuclosSession.save(node3)

		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE, (long) node2.id, true)

		ExplorerTreeComponent explorerTree = new ExplorerTreeComponent(new NuclosWebElement(driver.findElement(By.cssSelector('#explorer-tree')), '#explorer-tree'))

		assert explorerTree.getNodeByTitle('Root')
		assert explorerTree.getNodeByTitle('Layer 1')
		assert explorerTree.getNodeByTitle('Layer 2')

		node1.setAttribute('reference', node3)
		nuclosSession.save(node1)

		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE, (long) node2.id, true)

		explorerTree = new ExplorerTreeComponent(new NuclosWebElement(driver.findElement(By.cssSelector('#explorer-tree')), '#explorer-tree'))

		assert explorerTree.getNodeCount() == 4: 'It should expand exactly 4 nodes until it recognizes the loop'
		assert explorerTree.getAllNodeLabels() == ['Root', 'Layer 1', 'Layer 2']: '4 nodes are visible, but their name set does not match the set [\'Root\', \'Layer 1\', \'Layer 2\']'
	}

	@Test
	void _14_testDynamicNodesClickableAndIconPresent() {
		EntityObject infiniteNodeA
		_node_setup: {
			infiniteNodeA = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEA)
			infiniteNodeA.setAttribute('name', 'Test A_1')

			nuclosSession.save(infiniteNodeA)

			EntityObject<String> infiniteNodeB = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITENODEA)
			infiniteNodeB.setAttribute('name', 'Test A_2')

			nuclosSession.save(infiniteNodeA)

			EntityObject<String> dynamicNodeEntry = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TREEWITHDYNAMICNODE)
			dynamicNodeEntry.setAttribute('name', 'Test Tree with Dynamic BusinessObject Subnodes')

			nuclosSession.save(dynamicNodeEntry)
		}
		_open_tree: {
			EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TREEWITHDYNAMICNODE)

			NuclosWebElement openInTreeButton = $('.fa-sitemap')

			assert $('.fa-sitemap') != null: 'There should be a button to open the eo inside the treeview'

			openInTreeButton.click()

			waitForAngularRequestsToFinish()
			switchToOtherWindow(false)

			assert $('#explorer-tree') != null: 'There should have opened a new tab with a tree view.'
		}
		_expand_node: {
			ExplorerTreeComponent explorerTree = new ExplorerTreeComponent(new NuclosWebElement(driver.findElement(By.cssSelector('#explorer-tree')), '#explorer-tree'))
			explorerTree.clickToggleExpansion('Test Tree with Dynamic BusinessObject Subnodes')

			assert explorerTree.getNodeImageHref('Test A_1').endsWith("loopback.png")

			explorerTree.openNodeByClick('Test A_1')

			explorerTree.assertNodeSelected('Test A_1')

			assert getCurrentUrl().contains(infiniteNodeA.boMetaId + '/' + infiniteNodeA.id)
		}
	}
}
