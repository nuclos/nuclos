package org.nuclos.test.webclient.menu

import org.junit.Test
import org.junit.experimental.categories.Category
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Modal
import org.openqa.selenium.Keys
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@CompileStatic
class MenuAccessibilityTest extends AbstractRerunnableWebclientTest {

	@Test
	void navigateViaShortcutToExampleCustomer() {
		new Actions(driver).keyDown(Keys.ALT).sendKeys('c').keyUp(Keys.ALT).perform()

		assert driver.currentUrl.endsWith('example_rest_Customer'): "Webclient should have been navigated to Example/Customer"
		assert EntityObjectComponent.forDetail() != null: "EntityObject Detail for Customer should be available"
	}

	@Test
	void openHotkeysHelpModal() {
		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys('H').keyUp(Keys.CONTROL)
				.keyUp(Keys.SHIFT).perform()

		def helpModal = Modal.forSelector('nuc-nucloshotkey-help')
		waitFor {
			return helpModal.isVisible()
		}
		assert helpModal.title == 'Nuclos Tastaturkürzel Hilfe'
		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys('H').keyUp(Keys.CONTROL)
				.keyUp(Keys.SHIFT).perform()

		waitFor {
			return !helpModal.isVisible()
		}
	}

	@Test
	void navigateViaShortcutToExampleWord() {
		new Actions(driver).keyDown(Keys.CONTROL).sendKeys('w').keyUp(Keys.CONTROL).perform()

		assert driver.currentUrl.endsWith('example_rest_Word'): "Webclient should have been navigated to Example/Word"
		assert EntityObjectComponent.forDetail() != null: "EntityObject Detail for Customer should be available"
	}

	@Test
	void navigateFromOneShortcutToAnother() {
		navigateViaShortcutToExampleCustomer()
		navigateViaShortcutToExampleWord()
	}

	@Test
	void openFirstDropdownMenu() {
		new Actions(driver).keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys('1').keyUp(Keys.CONTROL)
				.keyUp(Keys.SHIFT).perform()

		def customerEntryLink = $vVv('#navbarNavDropdown > div.offcanvas-body > ul > li:nth-child(1) > ul > nuc-menu-item > li:nth-child(4) > a > div:nth-child(2)')
		def dropdownMenu = $vVv('#navbarNavDropdown > div.offcanvas-body > ul > li:nth-child(1) > ul')
		def dropdownMenuLink = $vVv('#Dropdown-Example')

		assert dropdownMenu != null: "DropdownMenu should be opened"
		assert dropdownMenuLink.getCssValue('color') == 'rgba(170, 170, 170, 1)': 'First Dropdown Link should have changed color on activation'
		assert customerEntryLink.isDisplayed(): "Customer Link should be visible"
		assert customerEntryLink.text == 'Customer': "Customer Link should be named Customer"
	}

	@Test
	void assureKeyboardShortCutIsVisible() {
		openFirstDropdownMenu()
		def shortCutEntry = $vVv('#navbarNavDropdown > div.offcanvas-body > ul > li:nth-child(1) > ul > nuc-menu-item > li:nth-child(4) > a > div.ml-auto.p-2 > span > kbd')
		assert shortCutEntry.text == 'ALT + C'
	}
}
