package org.nuclos.test.webclient.planningtable

import java.text.SimpleDateFormat
import java.util.stream.IntStream

import org.apache.commons.lang.time.DateUtils
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.Datepicker
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.EntityObjectModal
import org.nuclos.test.webclient.pageobjects.Searchtemplate
import org.nuclos.test.webclient.pageobjects.planningtable.PlanningTable
import org.nuclos.test.webclient.pageobjects.planningtable.PlanningTableComponent
import org.nuclos.test.webclient.pageobjects.search.Searchbar
import org.openqa.selenium.Keys
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class PlanningTableTest extends AbstractRerunnableWebclientTest {

	private static final String COLOR_CELL_BASE = 'rgba(0, 0, 0, 0)'
	private static final String COLOR_CELL_HOVER = 'rgba(208, 208, 208, 1)'
	private static final String COLOR_CELL_TODAY = 'rgba(177, 245, 177, 1)'
	private static final String COLOR_CELL_TODAY_HOVER = 'rgba(132, 199, 131, 1)'
	private static final String COLOR_CELL_WEEKEND = 'rgba(255, 187, 187, 1)'
	private static final String COLOR_CELL_WEEKEND_HOVER = 'rgba(221, 160, 160, 1)'
	private static final String COLOR_CELL_HOLIDAY = 'rgba(255, 187, 187, 1)'
	private static final double RESIZE_SENSITIVITY = 0.5
	private static final double DEFAULT_BASE_CELL_SIZE = 50
	private static final double DEFAULT_RESOURCE_CELL_SIZE = 64

	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy")
	private static SimpleDateFormat TIME_ALLOCATION_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
	private static SimpleDateFormat HOUR_MINUTE_FORMAT = new SimpleDateFormat("HH:mm")
	private static Date DATE_TODAY = new Date()
	private static Date DATE_HOLIDAY
	private static String TODAY = DATE_FORMAT.format(new Date())
	private static String YESTERDAY
	private static String TOMORROW
	private static String NEXT_WEEK
	private static String NEXT_MONTH
	private static String NEXT_QUARTER
	private static String NEXT_YEAR

	private static String FORMATTED_KW
	private static String FORMATTED_DATE = new SimpleDateFormat("dd.MM").format(new Date())
	private static String FORMATTED_YEAR = new SimpleDateFormat("yyyy").format(new Date())

	private EntityObject<String> testUser
	private EntityObject<String> nuclosUser
	private EntityObject resource1
	private EntityObject resource2
	private EntityObject resource3
	private EntityObject booking1
	private EntityObject tomorrowBooking

	@Before
	void setup() {
		EntityObject<String> readonlyUser = RESTHelper.createUser('readonly', 'readonly', ['Example readonly'], nuclosSession)
		testUser = RESTHelper.findUserByUsername('test', nuclosSession)
		nuclosUser = RESTHelper.findUserByUsername('nuclos', nuclosSession)

		Date tmp = new Date()
		Calendar c = Calendar.getInstance(Locale.GERMANY)

		c.setTime(tmp)
		FORMATTED_KW = 'KW ' + c.get(Calendar.WEEK_OF_YEAR);
		c.add(Calendar.DATE, 1)
		TOMORROW = DATE_FORMAT.format(c.getTime())

		c.setTime(tmp)
		c.add(Calendar.DATE, -1)
		YESTERDAY = DATE_FORMAT.format(c.getTime())

		c.setTime(tmp)
		c.add(Calendar.WEEK_OF_YEAR, 1)
		NEXT_WEEK = DATE_FORMAT.format(c.getTime())

		c.setTime(tmp)
		c.add(Calendar.MONTH, 1)
		NEXT_MONTH = DATE_FORMAT.format(c.getTime())

		c.setTime(tmp)
		c.add(Calendar.MONTH, 3)
		NEXT_QUARTER = DATE_FORMAT.format(c.getTime())

		c.setTime(tmp)
		c.add(Calendar.DAY_OF_MONTH, (7 + Calendar.FRIDAY - c.get(Calendar.DAY_OF_WEEK)))
		DATE_HOLIDAY = c.getTime()

		EntityObject testInactivePlanningTableEntry = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTPLANNINGTABLEINACTIVE)
		testInactivePlanningTableEntry.setAttribute('name', 'name123')
		nuclosSession.save(testInactivePlanningTableEntry)

		resource1 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_RESOURCE)
		resource1.setAttribute('name', 'Resource X name')
		resource1.setAttribute('label', 'Resource X')
		resource1.setAttribute('description', 'Resource Description')
		nuclosSession.save(resource1)

		resource2 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_RESOURCE)
		resource2.setAttribute('name', 'Resource Y name')
		resource2.setAttribute('label', 'Resource Y')
		resource2.setAttribute('description', 'Resource Description')
		resource2.setAttribute('inactiveref', testInactivePlanningTableEntry)
		nuclosSession.save(resource2)

		resource3 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_RESOURCE)
		resource3.setAttribute('name', 'Resource Z name')
		resource3.setAttribute('label', 'Resource Z')
		resource3.setAttribute('description', 'Resource Description')
		nuclosSession.save(resource3)

		booking1 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
		booking1.setAttribute('name', 'Test Booking')
		booking1.setAttribute('label', 'Booking1')
		booking1.setAttribute('memo', 'Booking Tooltip')
		booking1.setAttribute('fromdate', TODAY)
		booking1.setAttribute('untildate', TODAY)
		booking1.setAttribute('fromtime', '03:00')
		booking1.setAttribute('untiltime', '07:00')
		booking1.setAttribute('planningtableresource', resource2)
		booking1.setAttribute('user', testUser)
		nuclosSession.save(booking1)

		EntityObject milestone1 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_MILESTONE)
		milestone1.setAttribute('name', 'MilestoneOne')
		milestone1.setAttribute('datefrom', TODAY)
		milestone1.setAttribute('resource', resource1)
		nuclosSession.save(milestone1)

		EntityObject testHoliday = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_HOLIDAY)
		testHoliday.setAttribute('name', 'Test Holiday')
		testHoliday.setAttribute('date', DATE_HOLIDAY)
		nuclosSession.save(testHoliday)

		EntityObject yesterdayBooking = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
		yesterdayBooking.setAttribute('name', 'Yesterday Booking')
		yesterdayBooking.setAttribute('memo', 'Yesterday Booking Tooltip')
		yesterdayBooking.setAttribute('fromdate', YESTERDAY)
		yesterdayBooking.setAttribute('untildate', YESTERDAY)
		yesterdayBooking.setAttribute('fromtime', '16:00')
		yesterdayBooking.setAttribute('untiltime', '17:00')
		yesterdayBooking.setAttribute('planningtableresource', resource2)
		yesterdayBooking.setAttribute('user', testUser)
		nuclosSession.save(yesterdayBooking)

		EntityObject yesterdayMilestone = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_MILESTONE)
		yesterdayMilestone.setAttribute('name', 'Yesterday Milestone')
		yesterdayMilestone.setAttribute('datefrom', YESTERDAY)
		yesterdayMilestone.setAttribute('resource', resource2)
		nuclosSession.save(yesterdayMilestone)

		tomorrowBooking = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
		tomorrowBooking.setAttribute('name', 'Tomorrow Booking')
		tomorrowBooking.setAttribute('memo', 'Tomorrow Booking Tooltip')
		tomorrowBooking.setAttribute('fromdate', TOMORROW)
		tomorrowBooking.setAttribute('untildate', TOMORROW)
		tomorrowBooking.setAttribute('fromtime', '16:00')
		tomorrowBooking.setAttribute('untiltime', '17:00')
		tomorrowBooking.setAttribute('planningtableresource', resource3)
		tomorrowBooking.setAttribute('user', testUser)
		nuclosSession.save(tomorrowBooking)

		EntityObject tomorrowMilestone = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_MILESTONE)
		tomorrowMilestone.setAttribute('name', 'Tomorrow Milestone')
		tomorrowMilestone.setAttribute('datefrom', TOMORROW)
		tomorrowMilestone.setAttribute('resource', resource2)
		nuclosSession.save(tomorrowMilestone)

		EntityObject nextMonthBooking = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
		nextMonthBooking.setAttribute('name', 'Month Booking')
		nextMonthBooking.setAttribute('memo', 'Month Booking Tooltip')
		nextMonthBooking.setAttribute('fromdate', NEXT_MONTH)
		nextMonthBooking.setAttribute('untildate', NEXT_MONTH)
		nextMonthBooking.setAttribute('fromtime', '00:00')
		nextMonthBooking.setAttribute('untiltime', '24:00')
		nextMonthBooking.setAttribute('planningtableresource', resource3)
		nextMonthBooking.setAttribute('user', testUser)
		nuclosSession.save(nextMonthBooking)

		EntityObject nextQuarterBooking = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
		nextQuarterBooking.setAttribute('name', 'Quarter Booking')
		nextQuarterBooking.setAttribute('memo', 'Quarter Booking Tooltip')
		nextQuarterBooking.setAttribute('fromdate', NEXT_QUARTER)
		nextQuarterBooking.setAttribute('untildate', NEXT_QUARTER)
		nextQuarterBooking.setAttribute('fromtime', '00:00')
		nextQuarterBooking.setAttribute('untiltime', '24:00')
		nextQuarterBooking.setAttribute('planningtableresource', resource2)
		nextQuarterBooking.setAttribute('user', testUser)
		nuclosSession.save(nextQuarterBooking)
	}

	@Test
	void testResizeHorizontalHeaderContainer() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		// Drag and Drop Resource Header and check the new size
		NuclosWebElement horizontalHeaderContainer = $zZz('.horizontal-header-container')
		NuclosWebElement source = horizontalHeaderContainer.$zZz('.resize-handle-y')
		int oldHeight = horizontalHeaderContainer.size.height
		int oldWidth = horizontalHeaderContainer.size.width
		double newHeight = oldHeight + (50)
		Actions act = new Actions(driver)
		act.dragAndDropBy(source.element, 50, 50).build().perform()

		// allow some minimal pixel error
		assert Math.abs(horizontalHeaderContainer.size.height - newHeight) <= 5: 'The new height does not match the expected value'
		// make sure the width is unaffected
		assert horizontalHeaderContainer.size.width == oldWidth: 'Width should be unaffected, when resizing the horizontal header container'

	}

	@Test
	void testResizeVerticalHeaderContainer() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		// Drag and Drop Resource Header and check the new size
		NuclosWebElement verticalHeader = $zZz('.vertical-header-container')
		NuclosWebElement source = verticalHeader.$zZz('.resize-handle-x')
		int oldHeight = verticalHeader.size.height
		int oldWidth = verticalHeader.size.width
		double newWidth = oldWidth + (50)
		Actions act = new Actions(driver)
		act.dragAndDropBy(source.element, 50, 50).build().perform()

		// allow some minimal pixel error
		assert Math.abs(verticalHeader.size.width - newWidth) <= 5: 'The new width does not match the expected value'
		// make sure the width is unaffected
		assert verticalHeader.size.height == oldHeight: 'Height should be unaffected, when resizing the vertical header container'

	}

	@Test
	void testResizeResourceHeaderDefaultAlignment() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		// Drag and Drop Resource Header and check the new size
		NuclosWebElement resourceHeader = $zZz('.planning-resource-header')
		NuclosWebElement source = resourceHeader.$zZz('.resize-handle-y')
		int oldHeight = resourceHeader.size.height
		int oldWidth = resourceHeader.size.width
		double newHeight = oldHeight + (50 * RESIZE_SENSITIVITY)
		Actions act = new Actions(driver)
		act.dragAndDropBy(source.element, 50, 50).build().perform()

		// allow some minimal pixel error
		assert Math.abs(resourceHeader.size.height - newHeight) <= 5
		// make sure the width is unaffected
		assert resourceHeader.size.width == oldWidth

	}

	@Test
	void testResizeResourceHeaderSwitchedAlignment() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		planningTable.switchAlignment()

		// Drag and Drop Resource Header and check the new size
		NuclosWebElement resourceHeader = $zZz('.planning-resource-header')
		NuclosWebElement source = resourceHeader.$zZz('.resize-handle-x')
		int oldHeight = resourceHeader.size.height
		int oldWidth = resourceHeader.size.width
		double newWidth = oldWidth + (50 * RESIZE_SENSITIVITY)
		Actions act = new Actions(driver)
		act.dragAndDropBy(source.element, 50, 50).build().perform()

		// allow some minimal pixel error
		assert Math.abs(resourceHeader.size.width - newWidth) <= 5
		// make sure the width is unaffected
		assert resourceHeader.size.height == oldHeight

	}

	@Test
	void testResizeTimeHeaderDefaultAlignment() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPLANNINGTABLELAYOUTCOMPONENT)
		eo.addNew()

		PlanningTable planningTable = eo.getPlanningTable('nGYk-2')
		assert planningTable != null

		planningTable.switchToTimeMode('month')

		// Drag and Drop Resource Header and check the new size
		NuclosWebElement timeHeader = $zZz('.month').$zZz('.planning-time-header')
		NuclosWebElement source = timeHeader.$zZz('.resize-handle-x')
		int oldHeight = timeHeader.size.height
		int oldWidth = timeHeader.size.width
		double newWidth = oldWidth + (50 * RESIZE_SENSITIVITY)
		Actions act = new Actions(driver)
		act.dragAndDropBy(source.element, 50, 50).build().perform()

		// allow some minimal pixel error
		assert Math.abs(timeHeader.size.width - newWidth) <= 5
		// make sure the width is unaffected
		assert timeHeader.size.height == oldHeight

		eo.cancel()
	}

	@Test
	void testResizeTimeHeaderSwitchedAlignment() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPLANNINGTABLELAYOUTCOMPONENT)
		eo.addNew()

		PlanningTable planningTable = eo.getPlanningTable('nGYk-2')
		assert planningTable != null

		planningTable.switchAlignment()
		planningTable.switchToTimeMode('month')

		// Drag and Drop Resource Header and check the new size
		NuclosWebElement timeHeader = $zZz('.month').$zZz('.planning-time-header')
		NuclosWebElement source = timeHeader.$zZz('.resize-handle-y')
		int oldHeight = timeHeader.size.height
		int oldWidth = timeHeader.size.width
		double newHeight = oldHeight + (50 * RESIZE_SENSITIVITY)
		Actions act = new Actions(driver)
		act.dragAndDropBy(source.element, 50, 50).build().perform()

		// allow some minimal pixel error
		assert Math.abs(timeHeader.size.height - newHeight) <= 5
		// make sure the width is unaffected
		assert timeHeader.size.width == oldWidth

		eo.cancel()
	}

	@Test
	void testChangeDateFrom() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		planningTable.refresh()
		planningTable.switchToTimeMode('week')
		planningTable.setDateFrom(-1, 28)

		List<NuclosWebElement> bookings = $$zZz('.booking-base')
		List<NuclosWebElement> milestones = $$zZz('.milestone-inside')

		assert bookings.find({ x -> (x.text == 'Yesterday Booking') }) != null: 'Yesterday booking should be visible in the new date range'
		assert milestones.size() >= 2: 'At least 2 milestones should be visible in the new date range'
	}

	@Test
	void testChangeDateUntil() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		planningTable.refresh()
		planningTable.switchToTimeMode('day')
		planningTable.setDateUntil(1, 1)

		List<NuclosWebElement> bookings = $$zZz('.booking-base')
		List<NuclosWebElement> milestones = $$zZz('.milestone-inside')

		assert bookings.find({ x -> (x.text == 'Tomorrow Booking') }) != null: 'Tomorrow Booking should be visible in the new date range'
		assert milestones.size() >= 2: 'At least 2 milestones should be visible in the new date range'
	}

	/**
	 * Test to verify, that choosing a negative date interval will preserve the current time span.
	 * For more details see: NUCLOS-10538
	 */
	@Test
	void testChangeDateNegativInterval() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		planningTable.fromPicker.setInput('01012024')
		planningTable.untilPicker.setInput('07012024')

		planningTable.fromPicker.setInput('08012024')
		sendKeys(Keys.ENTER)

		assert planningTable.untilPicker.getInput() == '14.01.2024'

		planningTable.untilPicker.setInput('01012024')
		sendKeys(Keys.ENTER)

		assert planningTable.fromPicker.getInput() == '01.01.2024'
		assert planningTable.untilPicker.getInput() == '07.01.2024'
	}

	@Test
	void testToggleShowOwnBookingsOnly() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		EntityObject differentUserBooking = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
		differentUserBooking.setAttribute('name', 'Nuclos Booking')
		differentUserBooking.setAttribute('label', 'Nuclos Booking label')
		differentUserBooking.setAttribute('description', 'Nuclos Booking Tooltip')
		differentUserBooking.setAttribute('fromdate', TODAY)
		differentUserBooking.setAttribute('untildate', TODAY)
		differentUserBooking.setAttribute('fromtime', '16:00')
		differentUserBooking.setAttribute('untiltime', '18:00')
		differentUserBooking.setAttribute('planningtableresource', resource3)
		differentUserBooking.setAttribute('user', nuclosUser)
		nuclosSession.save(differentUserBooking)

		planningTable.refresh()

		assert $$zZz('.booking-base').find(x -> (x.text == 'Nuclos Booking')) != null: 'Booking from user nuclos should be displayed'

		planningTable.toggleShowOwnBookingsOnly()

		assert $$zZz('.booking-base').find(x -> (x.text == 'Nuclos Booking')) == null: 'Booking from user nuclos should be hidden'
	}

	@Test
	void testCheckboxVisibility() {
		// check that the checkbox is only present, when there are bookings of multiple users
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		// assert that the checkbox is not showing
		assert !planningTable.isShowOwnBookingsCheckboxVisible(): 'Checkbox should only be visible, if there are bookings from multiple users'

		// add a booking from another user
		EntityObject differentUserBooking = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
		differentUserBooking.setAttribute('name', 'Nuclos Booking')
		differentUserBooking.setAttribute('label', 'Nuclos Booking label')
		differentUserBooking.setAttribute('description', 'Nuclos Booking Tooltip')
		differentUserBooking.setAttribute('fromdate', TODAY)
		differentUserBooking.setAttribute('untildate', TODAY)
		differentUserBooking.setAttribute('fromtime', '16:00')
		differentUserBooking.setAttribute('untiltime', '18:00')
		differentUserBooking.setAttribute('planningtableresource', resource3)
		differentUserBooking.setAttribute('user', nuclosUser)
		nuclosSession.save(differentUserBooking)

		planningTable.refresh()

		// assert that the checkbox is now showing up
		assert planningTable.isShowOwnBookingsCheckboxVisible(): 'Checkbox should be visible because the planning table has bookings from multiple users'
	}

	@Test
	void testToggleFilter() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_RESOURCE)
		Searchbar.openSearchEditor()
		Searchbar.selectAttribute('Name')
		Searchbar.setCondition(
				TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_RESOURCE,
				new Searchtemplate.SearchTemplateItem(
						name: 'name',
						operator: 'like',
						value: 'Resource X*'
				)
		)
		Searchbar.saveAsSearchfilter()
		Searchbar.editName('custom test filter')
		Searchbar.save()

		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')

		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		planningTable.setFilter('Kein Filter')
		// Store all resources for comparing later
		List<NuclosWebElement> unfilteredResources = planningTable.element.$$('.planning-resource-header')

		// Check if the filter condition works
		planningTable.setFilter('custom test filter')
		List<NuclosWebElement> filteredResources = planningTable.element.$$('.planning-resource-header')
		filteredResources.each {
			assert it.text.startsWith('Resource X'): 'Visible resources should be filtered and start with \'Resource X\''
		}

		// Check if removing the filter brings back the resources from before
		planningTable.setFilter('Kein Filter')
		List<NuclosWebElement> unfilteredResources2 = planningTable.element.$$('.planning-resource-header')
		unfilteredResources.each { assert unfilteredResources2.contains(it) }
		unfilteredResources2.each { assert unfilteredResources.contains(it) }

	}

	@Test
	void testChangeTimeMode() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		String[] hours = new String[]{'00-01', '01-02'}
		String[] months = new String[]{'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August',
				'September', 'Oktober', 'November', 'Dezember'}
		String[] abbreviatedMonths = months.collect { it.substring(0, 3) }
		String[] formattedMonths = months.collect { it.substring(0, 3) + ' ' + FORMATTED_YEAR.substring(2, 4) }
		String[] quarters = new String[]{'Q1', 'Q2', 'Q3', 'Q4'}


		planningTable.switchToTimeMode('hour')
		waitForAngularRequestsToFinish()
		List<NuclosWebElement> unitLabels = $$zZz('.time-unit-label')
		assert unitLabels.find({ x -> (hours.contains(x.text)) })
		assert unitLabels.find({ x -> (FORMATTED_KW.trim() == x.text.trim()) })
		assert unitLabels.find({ x -> (FORMATTED_DATE == x.text) })

		planningTable.switchToTimeMode('day')
		waitForAngularRequestsToFinish()

		unitLabels = $$zZz('.time-unit-label')
		assert unitLabels.find({ x -> (formattedMonths.contains(x.text)) })
		assert unitLabels.find({ x -> (FORMATTED_KW.trim() == x.text.trim()) })
		assert unitLabels.find({ x -> (FORMATTED_DATE == x.text) })

		planningTable.switchToTimeMode('week')
		waitForAngularRequestsToFinish()

		unitLabels = $$zZz('.time-unit-label')
		assert unitLabels.find({ x -> (formattedMonths.contains(x.text)) })
		assert unitLabels.find({ x -> (FORMATTED_KW.trim() == x.text.trim()) })

		planningTable.setDateFrom(-1, 1)
		planningTable.setDateUntil(3, 1)

		planningTable.switchToTimeMode('month')
		waitForAngularRequestsToFinish()

		List<NuclosWebElement> bookings = $$zZz('.booking-base')
		unitLabels = $$zZz('.time-unit-label')

		assert bookings.find({ x -> (x.text == 'Month Booking') })
		assert unitLabels.find({ x -> (abbreviatedMonths.contains(x.text)) })

		planningTable.switchToTimeMode('quarter')
		waitForAngularRequestsToFinish()

		planningTable.setDateFrom(-1, 1)
		planningTable.setDateUntil(6, 1)
		unitLabels = $$zZz('.time-unit-label')

		assert unitLabels.find({ x -> (quarters.contains(x.text)) })
		assert unitLabels.find({ x -> (FORMATTED_YEAR == x.text) })
	}

	@Test
	void testRefreshPlanningTable() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		int bookingCountStart = $$zZz('.booking-base').size()

		/* Create a new resource, booking, milestone and check if they are present after refreshing. */
		EntityObject newResource = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_RESOURCE)
		newResource.setAttribute('name', 'Resource A name')
		newResource.setAttribute('label', 'Resource A')
		newResource.setAttribute('description', 'Resource Description')
		nuclosSession.save(newResource)

		EntityObject newBooking = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
		newBooking.setAttribute('name', 'New Booking')
		newBooking.setAttribute('label', 'New Booking label')
		newBooking.setAttribute('description', 'New Booking Tooltip')
		newBooking.setAttribute('fromdate', TODAY)
		newBooking.setAttribute('untildate', TODAY)
		newBooking.setAttribute('fromtime', '16:00')
		newBooking.setAttribute('untiltime', '17:00')
		newBooking.setAttribute('planningtableresource', this.resource1)
		newBooking.setAttribute('user', this.testUser)
		nuclosSession.save(newBooking)

		EntityObject newMilestone = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_MILESTONE)
		newMilestone.setAttribute('name', 'New Milestone')
		newMilestone.setAttribute('datefrom', TODAY)
		newMilestone.setAttribute('resource', newResource)
		nuclosSession.save(newMilestone)

		planningTable.refresh()

		int bookingCountEnd = $$zZz('.booking-base').size()

		assert bookingCountEnd == bookingCountStart + 1: 'Refreshing should have increased the number of bookings by 1'
	}

	@Test
	void testFitPlanningTableToView() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		planningTable.fitContentToView()

		int viewportWidth = Integer.parseInt($zZz('.viewport').getAttribute("clientWidth"))
		int contentWidth = Integer.parseInt($zZz('.grid').getAttribute("scrollWidth"))

		// the component has some error threshold
		assert (viewportWidth - contentWidth >= 0 && viewportWidth - contentWidth <= 3): 'The time header should fit the viewport width'
	}

	@Test
	void testChangeTableAlignment() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPLANNINGTABLELAYOUTCOMPONENT)
		eo.addNew()

		PlanningTable planningTable = eo.getPlanningTable('nGYk-2')
		assert planningTable != null

		NuclosWebElement horizontalHeader = $zZz('.horizontal-header')
		NuclosWebElement verticalHeader = $zZz('.vertical-header')

		assert horizontalHeader.$zZz('.timeHeader') != null: 'The horizontal header should contain the time header'
		assert horizontalHeader.$('.resourceHeader') == null: 'The horizontal header should NOT contain the resource header'

		assert verticalHeader.$zZz('.resourceHeader') != null: 'The vertical header should contain the resource header'
		assert verticalHeader.$('.timeHeader') == null: 'The vertical header should NOT contain the time header'

		// switch planning table axis and thus the headers
		planningTable.switchAlignment()

		assert horizontalHeader.$zZz('.resourceHeader') != null: 'The horizontal header should contain the resource header'
		assert horizontalHeader.$('.timeHeader') == null: 'The horizontal header should NOT contain the time header'

		assert verticalHeader.$zZz('.timeHeader') != null: 'The vertical header should contain the time header'
		assert verticalHeader.$('.resourceHeader') == null: 'The vertical header should NOT contain the resource header'

		eo.cancel()
	}

	@Test
	void testBookingPresence() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		// check if the test booking is present
		assert $zZz('.booking-base') != null: 'There should be a booking on screen. Bookings should be identifiable by querying for the booking-base class'
		assert $zZz('.booking-label b').text == 'Test Booking': 'Expected the text of the booking label div to be \'Test Booking\''
	}

	@Test
	void testMilestonePresence() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		// check if the test milestone is present
		assert $zZz('.milestone-base') != null: 'There should be a milestone on screen. Milestones should be identifiable by querying for the milestone-base class'
		assert $zZz('.milestone-label').text == TODAY: 'The milestone label format should match \'dd.mm.yyyy\' for today'
	}

	@Test
	void testBookingColor() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		testStaticColoringIfNoNuclosRowColorSet: {
			assert $zZz('.booking-base').getCssValue('background-color') == 'rgba(188, 34, 52, 1)': 'Booking does not have the expected static background color'
		}

		testNuclosRowColorBeatsStaticColor: {
			booking1.setAttribute('nuclosrowcolor', '#0000FF')
			nuclosSession.save(booking1)

			planningTable.refresh()

			// check if the Test Booking is blue
			assert $zZz('.booking-base').getCssValue('background-color') == 'rgba(0, 0, 255, 1)': 'Booking does not have the expected background color'
		}
	}

	@Test
	void testLegendPresence() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		assert $zZz('.legend-container').$zZz('.p-element').text == 'Label ${name} - ${name}': 'The legend element should contain the text without template substitution \'Label ${name} - ${name}\''
	}

	@Test
	void testLegendLayoutEoTextTemplating() {
		// Check if the Planning Table Component is present in the layout
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPLANNINGTABLELAYOUTCOMPONENT)
		eo.addNew()
		eo.setAttribute('name', 'Test123')

		PlanningTable planningTable = eo.getPlanningTable('nGYk-2')

		assert planningTable != null: 'There should be a WebPlanningTable element inside the opened layout'

		assert $zZz('.legend-container').$zZz('.p-element').text == 'Label Test123 - Test123': 'The legend element should contain the text with substituted template values \'Label Test123 - Test123\''

		eo.cancel()
	}

	@Test
	void testPlanningViewport() {
		// test if only the elements inside the view are visible
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		planningTable.setDateUntil(1, 1)
		planningTable.switchToTimeMode('day')
		planningTable.switchToTimeMode('hour')

		NuclosWebElement firstVisibleCell = $zZz('.grid-cell')
		int cellCount = $$zZz('.grid-cell').size()

		NuclosWebElement viewport = planningTable.element.$('.viewport')
		viewport = $zZz('.viewport')
		viewport.scrollRight()
		viewport.scrollRight()

		NuclosWebElement newFirstVisibleCell = $zZz('.grid-cell')
		int newCellCount = $$zZz('.grid-cell').size()
		for (NuclosWebElement cell : $$zZz('.grid-cell')) {
			assertCellInsideView(cell, viewport)
		}

		// make sure the first cell from before is no more the first visible cell
		assert firstVisibleCell.element != newFirstVisibleCell.element: 'The first cell should not be present after scrolling to the right'

		// make sure the new cell count has not increased significantly
		// one column of + 3 resources might
		assert newCellCount <= cellCount + 3: 'The amount of visible cells should not increase by more than 3 cells (= 1 column)'

		// verify that all cells are inside the view rectangle
		for (NuclosWebElement cell : $$zZz('.grid-cell')) {
			assertCellInsideView(cell, viewport)
		}
	}

	@Test
	void testPlanningComponentTitle() {
		// test if only the elements inside the view are visible
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		assert driver.title == 'Nuclos - Plantafel | Test Planning Table': 'The title for the browser tab does not match the typical title for the planning table component'
	}

	/**
	 * NUCLOS-10226 verify that the browser title is not changed by the planning table component, when the planning table is embedded inside a layout.
	 */
	@Test
	void testLayoutElementChangeDateNotOverridingBrowserTitle() {
		// Check if the Planning Table Component is present in the layout
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPLANNINGTABLELAYOUTCOMPONENT)
		eo.addNew()

		assert driver.title == 'Nuclos - Test Planning Table Layout Component': 'The title of the browser tab does not match the typical title of the eo component'

		PlanningTable planningTable = eo.getPlanningTable('nGYk-2')
		planningTable.setDateUntil(1, 0)

		assert planningTable != null: 'There should be a WebPlanningTable element inside the opened layout'
		assert driver.title == 'Nuclos - Test Planning Table Layout Component': 'The title for the browser tab should not have changed'

		eo.cancel()
	}

	/**
	 * test that verifies that the planning table is available via the route /plantable/<plantableid>
	 */
	@Test
	void testPlanningTableComponent() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null: 'The planning table should have opened'

		planningTable.switchToTimeMode('hour')
		planningTable.switchAlignment()
	}

	/**
	 * test that verifies that the planning table is present, when embedded in a layout
	 */
	@Test
	void testLayoutElement() {
		// Check if the Planning Table Component is present in the layout
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPLANNINGTABLELAYOUTCOMPONENT)
		eo.addNew()

		PlanningTable planningTable = eo.getPlanningTable('nGYk-2')

		assert planningTable != null: 'There should be a WebPlanningTable element inside the opened layout'

		eo.cancel()
	}

	/**
	 * Currently the coloring of cells should work as follows:
	 * If the current day lies within a cells date range or vice versa, then the cell will be colored green in all cases.
	 * If the date of a holiday or weekend day completely contains/matches the range of the cell it will be colored red.
	 * This means, red cells will only occur, when a time unit <= {@code TimeUnit.DAY} is set.
	 */
	@Test
	void testCellBackgroundColoredByDate() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		planningTable.setDateUntil(-1, 27)
		planningTable.setDateUntil(1, 12)
		planningTable.switchToTimeMode('hour')
		planningTable.fitContentToView()
		assertCorrectColoring(true)
		planningTable.switchToTimeMode('day')
		planningTable.fitContentToView()
		assertCorrectColoring(true)
		planningTable.switchToTimeMode('week')
		planningTable.fitContentToView()
		// when a cell fills a whole week, the weekend coloring should no more happen
		assertCorrectColoring(false)
		assertNoWeekendOrHolidaysCells()
		planningTable.switchToTimeMode('month')
		assertCorrectColoring(false)
		assertNoWeekendOrHolidaysCells()
	}

	@Test
	void testModifyResource() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		EntityObjectModal eoModal = EntityObjectComponent.forModal({
			$zZz('.planning-resource-header').click()
		})

		eoModal.setAttribute('label', 'Resource Renamed')
		eoModal.clickButtonOk()

		assert null != $$zZz('.planning-resource-header').find(x -> x.text == 'Resource Renamed'): 'The new resource name should be on screen'
	}

	@Test
	void testCellSelection() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		NuclosWebElement[] cells = $$zZz('.cell-base')
		/** test select a cell and make sure, all other cells are deselected **/
		cells[0].click()
		cells.each {
			(item, index) -> {
				assert (it.hasClass('cell-selected') && index == 0) || (index != 0)
			}
		}

		/** select a different cell, to assure the previous cell gets deselected properly **/
		cells[2].click()
		cells.each {
			(item, index) -> {
				assert (it.hasClass('cell-selected') && index == 2) || (index != 2)
			}
		}
	}

	@Test
	void testPlanElementSelectionSingle() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		planningTable.setDateUntil(1, 1)
		/** note: selenium fails to correctly click the cells, when they are too small. Timeunit should not be longer than a day. **/
		planningTable.switchToTimeMode('hour')

		NuclosWebElement[] allBookings = $$zZz('.booking-base')
		NuclosWebElement[] allMilestones = $$zZz('.milestone-base')

		/** test single click selection **/
		allBookings[0].click()
		allBookings.eachWithIndex { NuclosWebElement item, index ->
			assert (item.hasClass('booking-selected') && index == 0) || (index != 0)
		}
		allMilestones.eachWithIndex { NuclosWebElement item, index ->
			assert (!item.hasClass('milestone-selected'))
		}

		allMilestones[0].click()
		allBookings.eachWithIndex { NuclosWebElement item, index ->
			assert (!item.hasClass('booking-selected'))
		}
		allMilestones.eachWithIndex { NuclosWebElement item, index ->
			assert (item.hasClass('milestone-selected') && index == 0) || (index != 0)
		}

	}

	@Test
	void testPlanElementSelectionMulti() {
		/** test multi selection by holding ctrl-key **/
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		planningTable.setDateUntil(1, 1)
		/** note: selenium fails to correctly click the cells, when they are too small. Timeunit should not be longer than a day. **/
		planningTable.switchToTimeMode('day')

		NuclosWebElement[] allBookings = $$zZz('.booking-base')
		NuclosWebElement[] allMilestones = $$zZz('.milestone-base')

		NuclosWebElement timeHeader = $$('.planning-time-header')[2]
		NuclosWebElement source = timeHeader.$('.resize-handle-x')
		Actions act = new Actions(driver)
		act.dragAndDropBy(source.element, 200, 50).build().perform()

		allBookings[1].click()
		clickWithKey([allMilestones[1]], Keys.CONTROL)

		/** assert that both - the booking and the milestone - are selected **/
		allBookings.eachWithIndex { NuclosWebElement item, index ->
			assert (item.hasClass('booking-selected') && index == 1) || (index != 1)
		}
		allMilestones.eachWithIndex { NuclosWebElement item, index ->
			assert (item.hasClass('milestone-selected') && index == 1) || (index != 1)
		}

		/** check that milestone[1] got toggle-removed from the selection **/
		clickWithKey([allMilestones[1]], Keys.CONTROL)
		allBookings.eachWithIndex { NuclosWebElement item, index ->
			assert (item.hasClass('booking-selected') && index == 1) || (index != 1)
		}
		allMilestones.eachWithIndex { NuclosWebElement item, index ->
			assert !item.hasClass('milestone-selected')
		}
	}

	@Test
	void cellSelectionCreateBooking() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		int previousBookingCount = $$zZz('.booking-base').size()
		NuclosWebElement selectedCell = $$zZz('.cell-base')[1]
		selectedCell.click()
		waitForAngularRequestsToFinish()

		$zZz('#createPlanElement').click()
		NuclosWebElement detailsButton = $zZz('#selection_create_planning_table_booking')
		EntityObjectModal eoModal = EntityObjectComponent.forModal({
			detailsButton.click()
		})
		eoModal.setAttribute('name', 'Create Booking')
		eoModal.clickButtonOk()
		waitForAngularRequestsToFinish()

		NuclosWebElement newBooking = $$zZz('.booking-base').find(element -> element.text == 'Create Booking')

		// check that 1 new booking is present
		assert $$zZz('.booking-base').size() == previousBookingCount + 1: 'A new booking should have been created and increased the booking count by 1'

		// check that the positioning at the x-axis aligns
		assert selectedCell.location.x == newBooking.location.x: 'The new booking should start at the beginning of the selected cell'
		assert Math.abs(selectedCell.size.width - newBooking.size.width) <= 1: 'The width of the booking should match the selected cell'

		// because there is already another booking and they stack, we just check if it is inside the resources bounds
		assert newBooking.location.y >= selectedCell.location.y && newBooking.location.y < selectedCell.location.y + selectedCell.size.height: 'The new booking should be in the same resource row as the selected cell'
	}

	@Test
	void cellSelectionCreateMilestone() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		int previousMilestoneCount = $$zZz('.milestone-base').size()

		NuclosWebElement selectedCell = $$zZz('.cell-base')[1]
		selectedCell.click()
		waitForAngularRequestsToFinish()

		$zZz('#createPlanElement').click()

		NuclosWebElement detailsButton = $zZz('#selection_create_planning_table_milestone')
		EntityObjectModal eoModal = EntityObjectComponent.forModal({
			detailsButton.click()
		})
		eoModal.setAttribute('name', 'Create Milestone')
		eoModal.clickButtonOk()
		waitForAngularRequestsToFinish()

		NuclosWebElement newMilestone = $$zZz('.milestone-base')[1]

		assert $$zZz('.milestone-base').size() == previousMilestoneCount + 1: 'A new milestone should have been created and increased the milestone count by 1'

		assert ((selectedCell.location.x + selectedCell.size.width / 2) - (newMilestone.location.x + newMilestone.size.width / 2)).abs() <= 1: 'The x-/time-location of the new milestone should be in the middle of the selected cell'
		// because there is already another booking and they stack, we just check if it is inside the resources bounds
		assert newMilestone.location.y >= selectedCell.location.y && newMilestone.location.y < selectedCell.location.y + selectedCell.size.height: 'The new milestone should be in the same resource row as the selected cell'
	}

	@Test
	void planElementSelectionCreateBooking() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		int previousBookingCount = $$zZz('.booking-base').size()
		int gridLocationX = $zZz('.grid').location.x
		int gridLocationY = $zZz('.grid').location.y

		NuclosWebElement selectedBooking = $$zZz('.booking-base')[0]
		selectedBooking.click()
		waitForAngularRequestsToFinish()

		$zZz('#createPlanElement').click()

		NuclosWebElement detailsButton = $zZz('#selection_create_planning_table_booking')
		EntityObjectModal eoModal = EntityObjectComponent.forModal({
			detailsButton.click()
		})
		eoModal.setAttribute('name', 'Create Booking')
		eoModal.clickButtonOk()
		waitForAngularRequestsToFinish()

		NuclosWebElement newBooking = $$zZz('.booking-base').find(element -> element.text == 'Create Booking')
		selectedBooking = $$zZz('.booking-base')[0]

		// check that 1 new booking is present
		assert $$zZz('.booking-base').size() == previousBookingCount + 1: 'A new booking should have been created and increased the booking count by 1'

		// check that the positioning at the x-axis aligns
		assert (newBooking.location.x.toInteger() - gridLocationX) / DEFAULT_BASE_CELL_SIZE == 0: 'The new booking should start at the beginning of the cell in which the selected booking is located in'
		assert Math.abs(newBooking.size.width - DEFAULT_BASE_CELL_SIZE) <= 1: 'The new booking should fill the time intervall of one grid cell'

		// because there is already another booking and they stack, we just check if it is inside the resources bounds
		assert newBooking.location.y >= selectedBooking.location.y && newBooking.location.y == selectedBooking.location.y + selectedBooking.size.height: 'The new booking should be in the same resource row as the selected booking'
	}

	@Test
	void planElementSelectionCreateMilestone() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		int previousMilestoneCount = $$zZz('.milestone-base').size()
		int gridLocationX = $zZz('.grid').location.x
		int gridLocationY = $zZz('.grid').location.y

		NuclosWebElement selectedBooking = $$zZz('.booking-base')[0]
		selectedBooking.click()
		waitForAngularRequestsToFinish()

		$zZz('#createPlanElement').click()

		NuclosWebElement detailsButton = $zZz('#selection_create_planning_table_milestone')
		EntityObjectModal eoModal = EntityObjectComponent.forModal({
			detailsButton.click()
		})
		eoModal.setAttribute('name', 'Create Milestone')
		eoModal.clickButtonOk()
		waitForAngularRequestsToFinish()

		NuclosWebElement newMilestone = $$zZz('.milestone-base')[1]
		selectedBooking = $$zZz('.booking-base')[0]

		assert $$zZz('.milestone-base').size() == previousMilestoneCount + 1

		assert ((newMilestone.location.x + newMilestone.size.width / 2 - gridLocationX).toDouble() - (DEFAULT_BASE_CELL_SIZE / 2)).abs() <= 1
		// because there is already another booking and they stack, we just check if it is inside the resources bounds
		assert newMilestone.location.y >= selectedBooking.location.y && newMilestone.location.y < selectedBooking.location.y + selectedBooking.size.height
	}

	/** Test might be unstable. Succeed, directly after failing without any changes. **/
	@Test
	void testRemoveSelectedPlanElements() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		planningTable.setDateUntil(1, 1)
		planningTable.switchToTimeMode('hour')

		NuclosWebElement[] bookings = $$zZz('.booking-base')
		NuclosWebElement[] milestones = $$zZz('.milestone-base')
		bookings[0].click()
		waitForAngularRequestsToFinish()

		NuclosWebElement removeButton = $zZz('#removeSelectedElements')
		removeButton.click()
		waitForAngularRequestsToFinish()

		NuclosWebElement[] remainingBookings = $$('.booking-base')
		NuclosWebElement[] remainingMilestones = $$('.milestone-base')

		assert bookings[0].isDisplayed() == false
		assert bookings.size() == remainingBookings.size() + 1
		assert milestones.size() == remainingMilestones.size()
	}

	@Test
	void testShowDetailsForSelectedPlanElement() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null
		planningTable.switchToTimeMode('hour')

		$zZz('.booking-base').click()

		waitForAngularRequestsToFinish()
		NuclosWebElement detailsButton = $zZz('#showElementDetails')
		EntityObjectModal eoModal = EntityObjectComponent.forModal({
			detailsButton.click()
		})

		assert eoModal.getAttribute('name') == 'Test Booking': 'The modal should contain the name of the selected booking'
		assert eoModal.getAttribute('memo') == 'Booking Tooltip': 'The modal should contain the tooltip of the selected booking'
		assert eoModal.getAttribute('fromdate') == TODAY: 'The modal should contain the from date of the selected booking'
		assert eoModal.getAttribute('untildate') == TODAY: 'The modal should contain the until date of the selected booking'
		assert eoModal.getAttribute('fromtime') == '03:00': 'The modal should contain the from time of the selected booking'
		assert eoModal.getAttribute('untiltime') == '07:00': 'The modal should contain the until time of the selected booking'
		assert eoModal.getAttribute('planningtableresource') == resource2.getAttribute('label'): 'The modal should contain the resource of the selected booking'
		assert eoModal.getAttribute('user') == testUser.getAttribute('username'): 'The modal should contain the user of the selected booking'

	}

	/** Test might be unstable. Succeed, directly after failing without any changes. **/
	@Test
	void testMoveSelection() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		int gridLocationX = $zZz('.grid').location.x
		int gridLocationY = $zZz('.grid').location.y

		planningTable.setDateUntil(1, 12)
		planningTable.switchToTimeMode('hour')

		NuclosWebElement moveElement = $zZz('.booking-base')
		assert moveElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4)
		assert moveElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 3)
		assert moveElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1)

		// add the booking to the selection
		moveElement.click()

		dragElementByOffset(moveElement, (int) DEFAULT_BASE_CELL_SIZE, 50)
		waitForAngularRequestsToFinish()

		moveElement = $zZz('.booking-base')
		assert moveElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4): 'Width should not change, when moving an element'
		assert moveElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 4): 'Element should have moved 1 column to the right.'
		assert moveElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 2): 'Element should have moved 1 row below.'

	}

	@Test
	void testScaleSelectionEnd() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		int gridLocationX = $zZz('.grid').location.x
		int gridLocationY = $zZz('.grid').location.y

		planningTable.setDateUntil(1, 12)
		planningTable.switchToTimeMode('hour')

		NuclosWebElement scaleElement = $zZz('.booking-base')
		assert scaleElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4)
		assert scaleElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 3)
		assert scaleElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1)

		// add the booking to the selection
		scaleElement.click()

		// test positive scaling: make the booking start one cell earlier
		dragElementByOffset(scaleElement.$zZz('.resize-handle-right'), (int) DEFAULT_BASE_CELL_SIZE, 50)
		waitForAngularRequestsToFinish()

		scaleElement = $zZz('.booking-base')
		assert scaleElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 5): 'Booking width should have changed by +1 cell size'
		assert scaleElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 3): 'x-location should not change when resizing the end of a booking'
		assert scaleElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1): 'y-location should not change when resizing the end of a booking'

		// test positive scaling: make the booking start one cell earlier
		dragElementByOffset(scaleElement.$zZz('.resize-handle-right'), 8 * (int) -DEFAULT_BASE_CELL_SIZE, 50)
		waitForAngularRequestsToFinish()

		scaleElement = $zZz('.booking-base')
		assert scaleElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 2): 'Booking width should have shrunk to the size of 2 cells'
		assert scaleElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 1): 'x-location should not change, when resizing the end of a booking'
		assert scaleElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1): 'y-location should not change, when resizing the end of a booking'
	}

	@Test
	void testScaleSelectionStart() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		planningTable.setDateUntil(1, 12)
		planningTable.switchToTimeMode('hour')

		int gridLocationX = $zZz('.grid').location.x
		int gridLocationY = $zZz('.grid').location.y

		NuclosWebElement scaleElement = $zZz('.booking-base')
		assert scaleElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4): 'Before scaling the booking should have a width of 4 cells'
		assert scaleElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 3): 'Before scaling the booking x-location should be at column 4'
		assert scaleElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1): 'Before scaling the booking y-location should be at row 2'

		// add the booking to the selection
		scaleElement.click()

		// test positive scaling: make the booking start one cell earlier
		dragElementByOffset(scaleElement.$zZz('.resize-handle-left'), (int) -DEFAULT_BASE_CELL_SIZE, 50)
		waitForAngularRequestsToFinish()

		scaleElement = $zZz('.booking-base')
		assert scaleElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 5): 'Booking width should have changed by +1 cell'
		assert scaleElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 2): 'x-location of the booking should have changed by -1 cell'
		assert scaleElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1): 'y-/resource-location should not change when resizing a booking from start'

		// test negative scaling: drag the start position behind the end position
		dragElementByOffset(scaleElement.$zZz('.resize-handle-left'), 8 * (int) DEFAULT_BASE_CELL_SIZE, 50)
		waitForAngularRequestsToFinish()

		scaleElement = $zZz('.booking-base')

		assert scaleElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 3)
		assert scaleElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 7)
		assert scaleElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1)
	}

	@Test
	void testCancelMoveSelection() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		int gridLocationX = $zZz('.grid').location.x
		int gridLocationY = $zZz('.grid').location.y

		planningTable.setDateUntil(1, 12)
		planningTable.switchToTimeMode('hour')

		NuclosWebElement moveElement = $zZz('.booking-base')
		assert moveElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4): 'Original width should equal the width of 4 cells'
		assert moveElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 3): 'Original start column should be 4'
		assert moveElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1): 'Original start row should be 2'

		// add the booking to the selection
		moveElement.click()

		// drag the selection 1 cell down and 1 cell to the right but then click the right mouse button
		new Actions(driver)
				.clickAndHold(moveElement.element)
				.moveByOffset((int)DEFAULT_BASE_CELL_SIZE, 50)
				.contextClick()
				.build()
				.perform()

		moveElement = $zZz('.booking-base')
		assert moveElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4): 'Booking size should not have changed'
		assert moveElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 3): 'Booking time location should not have changed'
		assert moveElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1): 'Booking resource location should not have changed'

	}

	@Test
	void testCancelScaleSelectionEnd() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()

		assert planningTable != null

		int gridLocationX = $zZz('.grid').location.x
		int gridLocationY = $zZz('.grid').location.y

		planningTable.setDateUntil(1, 12)
		planningTable.switchToTimeMode('hour')

		NuclosWebElement scaleElement = $zZz('.booking-base')
		assert scaleElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4): 'Original booking width should equal the width of 4 cells'
		assert scaleElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 3): 'Original start column should be 4'
		assert scaleElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1): 'Booking resource location should not have changed'

		// add the booking to the selection
		scaleElement.click()

		// drag the end and drag it 1 cell to the right but then right click to cancel the action
		new Actions(driver)
				.clickAndHold(scaleElement.$zZz('.resize-handle-right').element)
				.moveByOffset((int)DEFAULT_BASE_CELL_SIZE, 50)
				.contextClick()
				.build()
				.perform()

		scaleElement = $zZz('.booking-base')
		assert scaleElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4): 'Booking size should not have changed'
		assert scaleElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 3): 'Booking location x should not have changed'
		assert scaleElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1): 'Booking location y should not have changed'
	}

	@Test
	void testCancelScaleSelectionStart() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null: 'The PlanningTable Component should be visible for this test'

		planningTable.setDateUntil(1, 12)
		planningTable.switchToTimeMode('hour')

		int gridLocationX = $zZz('.grid').location.x
		int gridLocationY = $zZz('.grid').location.y

		NuclosWebElement scaleElement = $zZz('.booking-base')
		assert scaleElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4): 'Original booking width should equal the width of 4 cells'
		assert scaleElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 3): 'Original start column should be 4'
		assert scaleElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1): 'Booking resource location should not have changed'

		// add the booking to the selection
		scaleElement.click()

		// drag the end and drag it 1 cell to the right but then right click to cancel the action
		new Actions(driver)
				.clickAndHold(scaleElement.$zZz('.resize-handle-left').element)
				.moveByOffset((int)DEFAULT_BASE_CELL_SIZE, 50)
				.contextClick()
				.build()
				.perform()

		scaleElement = $zZz('.booking-base')
		assert scaleElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4): 'Booking size should not have changed'
		assert scaleElement.location.x - gridLocationX == (int) (DEFAULT_BASE_CELL_SIZE * 3): 'Booking location x should not have changed'
		assert scaleElement.location.y - gridLocationY == (int) (DEFAULT_RESOURCE_CELL_SIZE * 1): 'Booking location y should not have changed'
	}

	@Test
	void testSelectRelation() {
		EntityObject relation1 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_RELATION)
		relation1.setAttribute('name', 'Relation one')
		relation1.setAttribute('bookingfrom', booking1)
		relation1.setAttribute('bookingto', tomorrowBooking)
		nuclosSession.save(relation1)

		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null
		planningTable.setDateUntil(1, 1)

		assert $zZz('.relation-base') != null: 'There should be a relation visible on screen'

		// click the relation
		new Actions(driver).click($zZz('.dot').element).build().perform()

		assert $zZz('.parent-selected') != null: 'There should be a selected relation on screen'

	}

	@Test
	void testCreateRelation() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null
		planningTable.setDateUntil(1, 1)

		NuclosWebElement [] bookings = $$zZz('.booking-base')
		bookings[0].click()
		planningTable.setCreateRelationFrom()

		bookings[1].click()
		EntityObjectModal eoModal = EntityObjectComponent.forModal({
			planningTable.createRelation("planning_table_relation")
		})

		assert eoModal != null: 'The entity object modal should be visible to specify the attributes of the new relation.'

		eoModal.setAttribute('name', 'Test Relation 123')
		eoModal.clickButtonOk()

		assert $zZz('.relation-base') != null: 'The new relation should be visible.'
	}

	@Test
	void testExportSvg() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		planningTable.exportPlanningTable()

	}

	@Test
	void testPlanningTableInactiveInLayout() {
		// Check if the Planning Table Component is present in the layout
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPLANNINGTABLEINACTIVE)

		waitForAngularRequestsToFinish()

		PlanningTable planningTable = eo.getPlanningTable('nYCg')

		assert planningTable != null: 'There should be a WebPlanningTable element inside the opened layout'

		testResourceEditingDeactivated: {
			// assert that a resource can not be opened by clicking
			$zZz('.planning-resource-header').click()
			assert $('.dialog') == null: 'The resource dialog should not open, when the layout component has been deactivated'
		}

		testToolbarButtonsDeactivated: {
			$zZz('.booking-base').click()
			assert $zZz('.booking-selected') != null: 'Failed to select a booking: this is a prerequisite for the following asserts'
			// assert that the toolbar buttons for editing bookings, milestones & relations are hidden
			assert $('.toolbar-editing') == null: 'The toolbar div should be hidden, when the layout component has been deactivated'
			assert $('#createPlanElement') == null: 'The createPlanElement Button should be hidden, when the layout component has been deactivated'
			assert $('#showElementDetails') == null: 'The showElementDetails Button should be hidden, when the layout component has been deactivated'
			assert $('#removeSelectedElements') == null: 'The removeSelectedElements Button should be hidden, when the layout component has been deactivated'
		}

		testSelectionMovementDeactivated: {
			planningTable.switchToTimeMode('hour')

			int gridLocationX = $zZz('.grid').location.x
			int gridLocationY = $zZz('.grid').location.y

			int originalX = (int) (DEFAULT_BASE_CELL_SIZE * 3)
			int originalY = (int) (DEFAULT_RESOURCE_CELL_SIZE * 0)

			NuclosWebElement moveElement = $zZz('.booking-base')
			assert moveElement.size.width == (int) (DEFAULT_BASE_CELL_SIZE * 4)
			assert moveElement.location.x - gridLocationX == originalX: 'Booking is not at the expected x-location'
			assert moveElement.location.y - gridLocationY == originalY: 'Booking is not at the expected y-location'

			// add the booking to the selection
			moveElement.click()

			// attempt moving
			dragElementByOffset(moveElement, (int) DEFAULT_BASE_CELL_SIZE, 50)

			// attempt resizing via right handle
			dragElementByOffset(moveElement.$zZz('.resize-handle-right'), (int) DEFAULT_BASE_CELL_SIZE, 50)

			// attempt resizing via left handle
			dragElementByOffset(moveElement.$zZz('.resize-handle-left'), (int) DEFAULT_BASE_CELL_SIZE, 50)

			waitForAngularRequestsToFinish()

			moveElement = $zZz('.booking-base')
			// assert that drag & drop movement & scaling is deactivated for the selection
			assert moveElement.location.x - gridLocationX == originalX: 'Booking should still be at the original x-location'
			assert moveElement.location.y - gridLocationY == originalY: 'Booking should still be at the original y-location'
		}
	}

	/**
	 * See NUCLOS-10335
	 * This test makes sure, the time unit week has been fixed for the exact problematic date range specified in the ticket.
	 * The problem arose because the first day of the year, was actually located in calendar week 52 of the previous year.
	 */
	@Test
	void testTimeUnitCalendarWeek() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		planningTable.switchToTimeMode('week')
		waitForAngularRequestsToFinish()

		testScenario_from_01_01_2023_to_30_09_2023: {
			// this is a scenario, where the 01.01.2023 is not calendar week 1 but rather belongs to calendar week 52 of the previous year, which caused problems
			setDateInPicker(planningTable.getFromPicker(), newDate(2023, 1, 1))
			waitForAngularRequestsToFinish()
			setDateInPicker(planningTable.getUntilPicker(), newDate(2023, 9, 30))
			waitForAngularRequestsToFinish()

			planningTable.fitContentToView()

			List<NuclosWebElement> timeHeaders = $$zZz('.time-container')
			List<NuclosWebElement> unitLabels = $$zZz('.time-unit-label')

			assert timeHeaders[1].$$zZz('.time-unit-label')[0].text.trim() == 'KW 52': 'First day of 2023 lies within the last calendar week of the previous year'
			for (int i = 1; i < 39; i++) {
				assert unitLabels.find({ x -> ('KW ' + i == x.text.trim()) })
			}
			for (int i = 40; i < 51; i++) {
				assert !unitLabels.find({ x -> ('KW ' + i == x.text.trim()) })
			}
		}

		testScenario_from_28_12_2024_to_15_01_2025: {
			// In this scenario, the code problem was, that calendar week 1 was already beginning in december 2024, which caused problems
			setDateInPicker(planningTable.getFromPicker(), newDate(2024, 12, 28))
			waitForAngularRequestsToFinish()
			setDateInPicker(planningTable.getUntilPicker(), newDate(2025, 1, 15))
			waitForAngularRequestsToFinish()

			planningTable.fitContentToView()

			List<NuclosWebElement> timeHeaders = $$zZz('.time-container')
			List<NuclosWebElement> unitLabels = $$zZz('.time-unit-label')

			assert timeHeaders[1].$$zZz('.time-unit-label')[0].text.trim() == 'KW 52': 'First day of 2023 lies within the last calendar week of the previous year'
			for (int i = 1; i < 52; i++) {
				// assert CW 52, 1, 2, 3 should be present, no other weeks should be present in this scenario
				if (i <= 3 || i == 52 ) {
					assert unitLabels.find({ x -> ('KW ' + i == x.text.trim()) })
				} else {
					assert !unitLabels.find({ x -> ('KW ' + i == x.text.trim()) })
				}
			}
		}

		testScenario_from_28_12_2026_to_02_01_2027: {
			// Another Scenario: here some calendar weeks were missing
			setDateInPicker(planningTable.getFromPicker(), newDate(2026, 12, 28))
			waitForAngularRequestsToFinish()
			setDateInPicker(planningTable.getUntilPicker(), newDate(2027, 1, 2))
			waitForAngularRequestsToFinish()

			planningTable.fitContentToView()

			List<NuclosWebElement> timeHeaders = $$zZz('.time-container')
			List<NuclosWebElement> unitLabels = $$zZz('.time-unit-label')

			assert timeHeaders[0].$$zZz('.time-unit-label')[0].text.trim() == 'Dez 26': 'Dez 26 expected as first month cell'
			assert timeHeaders[0].$$zZz('.time-unit-label')[1].text.trim() == 'Jan 27': 'Jan 27 expected as second month cell'
			assert timeHeaders[1].$$zZz('.time-unit-label')[0].text.trim() == 'KW 53': 'Calendar Week 53 expected'
			for (int i = 1; i < 53; i++) {
				// assert CW 53 is present, no other weeks should be present in this scenario
				if (i == 53) {
					assert unitLabels.find({ x -> ('KW ' + i == x.text.trim()) })
				} else {
					assert !unitLabels.find({ x -> ('KW ' + i == x.text.trim()) })
				}
			}
		}
	}

	/**
	 * See NUCLOS-10335
	 * Test to verify that the interval [03.05.2023, 12.09.2024] works correctly.
	 */
	@Test
	void testMultipleYearsAsCalendarWeeks() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		String[] months = new String[]{'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August',
				'September', 'Oktober', 'November', 'Dezember'}
		String[] formattedMonths23 = months.collect { it.substring(0, 3) + ' 23' }
		String[] formattedMonths24 = months.collect { it.substring(0, 3) + ' 24' }
		String[] formattedMonths = formattedMonths23 + formattedMonths24
		String[] formattedKW = IntStream.range(1, 53).mapToObj(i -> "KW " + i).toArray(String[]::new)

		planningTable.switchToTimeMode('week')
		waitForAngularRequestsToFinish()

		// we want to test a specific date
		testScenario_from_03_05_2023_to_13_09_2024: {
			// test scenario, where the problem was an intervall spanning over a year
			planningTable.getFromPicker().open()
			setDateInPicker(planningTable.getFromPicker(), newDate(2023, 5, 3))
			waitForAngularRequestsToFinish()
			planningTable.getUntilPicker().open()
			setDateInPicker(planningTable.getUntilPicker(), newDate(2024, 9, 13))
			waitForAngularRequestsToFinish()

			planningTable.fitContentToView()

			List<NuclosWebElement> timeHeaders = $$zZz('.time-container')
			List<NuclosWebElement> monthCells = timeHeaders[0].$$zZz('.time-unit-label')
			List<NuclosWebElement> weekCells = timeHeaders[1].$$zZz('.time-unit-label')

			int monthHeaders = monthCells.size()
			int weekHeaders = weekCells.size()

			// the upper limit would be the correct one, if the selenium screen space was wide enough to display all elements
			// the lower limit is set accordingly to the current screen space available in selenium
			assert monthHeaders >= 11 && monthHeaders <= 17
			assert weekHeaders >= 49 && weekHeaders <= 73

			// check that the elements follow the correct order
			for (int i = 0; i < monthHeaders; i++) {
				if (monthCells[i].isDisplayed()) {
					assert formattedMonths.findIndexOf({month -> month == monthCells[i].text.trim()}) == 4 + i
				}
			}
			for (int i = 0; i < weekHeaders; i++) {
				if (weekCells[i].isDisplayed()) {
					assert formattedKW.findIndexOf({week -> week == weekCells[i].text.trim()}) == ((17 + i) % 52)
				}
			}
		}

		testScenario_from_29_12_2025_to_01_01_2027: {
			// test scenario, where a problem was with this interval spanning over multiple years
			planningTable.getFromPicker().open()
			setDateInPicker(planningTable.getFromPicker(), newDate(2025, 12, 29))
			waitForAngularRequestsToFinish()
			planningTable.getUntilPicker().open()
			setDateInPicker(planningTable.getUntilPicker(), newDate(2027, 1, 1))
			waitForAngularRequestsToFinish()

			planningTable.fitContentToView()

			List<NuclosWebElement> timeHeaders = $$zZz('.time-container')
			List<NuclosWebElement> monthCells = timeHeaders[0].$$zZz('.planning-time-header')
			List<NuclosWebElement> weekCells = timeHeaders[1].$$zZz('.planning-time-header')

			int weekHeaders = weekCells.size()

			assert weekHeaders > 5: 'at least 5 weeks should be visible'
			weekCells.forEach(c -> {
				assert weekCells[0].size.width == c.size.width
			})
		}

		testScenario_from_09_01_2025_to_01_01_2026: {
			// 09.01.2025 to 01.01.2026
			planningTable.getFromPicker().open()
			setDateInPicker(planningTable.getFromPicker(), newDate(2025, 1, 9))
			waitForAngularRequestsToFinish()
			planningTable.getUntilPicker().open()
			setDateInPicker(planningTable.getUntilPicker(), newDate(2026, 1, 1))
			waitForAngularRequestsToFinish()

			planningTable.fitContentToView()

			List<NuclosWebElement> timeHeaders = $$zZz('.time-container')
			List<NuclosWebElement> monthCells = timeHeaders[0].$$zZz('.time-unit-label')
			List<NuclosWebElement> weekCells = timeHeaders[1].$$zZz('.time-unit-label')

			int weekHeaders = weekCells.size()

			for (int i = 0; i < weekHeaders; i++) {
				if (weekCells[i].isDisplayed()) {
					assert weekCells[i].text.trim() == formattedKW[(i + 1) % 53]
				}
			}
		}
	}

	@Test
	void testTimeHeaderScenario() {
		PlanningTableComponent planningComponent = PlanningTableComponent.open('PKWdUYGS0BcCMTrTFj5L')
		PlanningTable planningTable = planningComponent.getPlanningTable()
		assert planningTable != null

		String[] months = new String[]{'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August',
				'September', 'Oktober', 'November', 'Dezember'}
		String[] formattedMonths = months.collect { it.substring(0, 3) + ' 23' }

		planningTable.switchToTimeMode('week')
		waitForAngularRequestsToFinish()

		// we want to test a specific date
		planningTable.getFromPicker().open()
		setDateInPicker(planningTable.getFromPicker(), newDate(2023, 9, 15))
		waitForAngularRequestsToFinish()
		planningTable.getUntilPicker().open()
		setDateInPicker(planningTable.getUntilPicker(), newDate(2023, 9, 30))
		waitForAngularRequestsToFinish()

		planningTable.fitContentToView()

		List<NuclosWebElement> unitLabels = $$zZz('.time-unit-label')

		for (int i = 0; i < 12; i++) {
			if (i >= 8 && i <= 9) {
				assert unitLabels.find({ x -> (formattedMonths[i] == x.text.trim()) })
			} else {
				assert !unitLabels.find({ x -> (formattedMonths[i] == x.text.trim()) })
			}
		}

		for (int i = 1; i < 52; i++) {
			if (i >= 37 && i <= 39) {
				assert unitLabels.find({ x -> ('KW ' + i == x.text.trim()) })
			} else {
				assert !unitLabels.find({ x -> ('KW ' + i == x.text.trim()) })
			}
		}
	}

	@Ignore("Not a real test, because no assertions. Creates a lot of random plan elements for local performance testing.")
	@Test
	void testPerformance() {
		EntityObject[] resourceEOs = new EntityObject[2000]
		for (int i = 0; i < 2000; i++) {
			resourceEOs[i] = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_RESOURCE)
			resourceEOs[i].setAttribute('name', 'Resource ' + i + ' name')
			resourceEOs[i].setAttribute('label', 'Resource ' + i)
			resourceEOs[i].setAttribute('description', 'Resource ' + i + ' Description')
			nuclosSession.save(resourceEOs[i])
		}
		for (int i = 0; i < 1000; i++) {
			// concentrate all bookings on the first 12 resources
			int resourceIndex = (int) (Math.random() * 38)
			Date fromDate = generateRandomDateBetween2Months()
			Date untilDate = generatePlausibleBookingEnd(fromDate)

			EntityObject booking1 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_BOOKING)
			booking1.setAttribute('name', 'Test Booking ' + i)
			booking1.setAttribute('label', 'Booking ' + i)
			booking1.setAttribute('memo', 'Booking Tooltip ' + i)
			booking1.setAttribute('fromdate', fromDate)
			booking1.setAttribute('untildate', untilDate)
			booking1.setAttribute('fromtime', HOUR_MINUTE_FORMAT.format(fromDate))
			booking1.setAttribute('untiltime', HOUR_MINUTE_FORMAT.format(untilDate))
			booking1.setAttribute('planningtableresource', resourceEOs[resourceIndex])
			booking1.setAttribute('nuclosrowcolor', generateRandomColor())
			booking1.setAttribute('user', testUser)
			nuclosSession.save(booking1)
		}
		for (int i = 0; i < 1000; i++) {
			// concentrate all bookings on the first 12 milestones
			int resourceIndex = (int) (Math.random() * 38)
			EntityObject milestone1 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_PLANNING_TABLE_MILESTONE)
			milestone1.setAttribute('name', 'Milestone ' + i)
			milestone1.setAttribute('datefrom', generateRandomDateBetween2Months())
			milestone1.setAttribute('resource', resourceEOs[resourceIndex])
			nuclosSession.save(milestone1)
		}
	}

	/**
	 * Generates a random color as a hex-string.
	 * @return
	 */
	String generateRandomColor() {
		int rgb = new Random().nextInt(1 << 24) // A random 24-bit integer
		return '#' + Integer.toString(rgb, 16).padLeft(6, '0')
	}

	private void setDateInPicker(Datepicker datepicker, Date date) {
		datepicker.setInput(AbstractNuclosTest.context.dateFormat.format(date))
		sendKeys(Keys.ESCAPE)
	}

	private Date newDate(int year, int month, int day) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.clear()
		cal.set(Calendar.YEAR, year)
		cal.set(Calendar.MONTH, month - 1)
		cal.set(Calendar.DAY_OF_MONTH, day)
		cal.getTime()
	}

	static Date generateRandomDateBetween2Months() {
		Calendar c = Calendar.getInstance()

		c.setTime(new Date())
		c.add(Calendar.DATE, (int) (Math.random() * 70))
		c.add(Calendar.HOUR, (int) (Math.random() * 24))
		c.add(Calendar.MINUTE, (int) (Math.random() * 60))

		return c.getTime()
	}

	static Date generatePlausibleBookingEnd(Date start) {
		Calendar c = Calendar.getInstance()

		c.setTime(start)
		c.add(Calendar.DATE, (int) (Math.random() * 2))
		c.add(Calendar.HOUR, (int) (Math.random() * 24))
		c.add(Calendar.MINUTE, (int) (Math.random() * 60))

		return c.getTime()
	}

	static assertCellInsideView(NuclosWebElement cell, NuclosWebElement viewport) {
		assert cell.location.x >= viewport.location.x || cell.location.x <= viewport.location.x + viewport.size.width
		assert cell.location.y >= viewport.location.y || cell.location.y <= viewport.location.y + viewport.size.height
	}

	static assertCorrectColoring(boolean weekendShouldColor) {
		$$zZz('.cell-base').forEach(item ->
				assertColorMatchesDate(item, weekendShouldColor)
		)
	}

	static assertColorMatchesDate(NuclosWebElement cell, boolean weekendShouldColor) {
		String timeAllocation = cell.getAttribute("data-time-allocation")
		timeAllocation = timeAllocation.replace("[", "")
		timeAllocation = timeAllocation.replace("]", "")
		String[] split = timeAllocation.split(" / ")
		Date start = TIME_ALLOCATION_FORMAT.parse(split[0])
		Date end = TIME_ALLOCATION_FORMAT.parse(split[1])
		Calendar c = Calendar.getInstance()
		c.setTime(start)
		Calendar startCalendar = Calendar.getInstance()
		startCalendar.setTime(start)
		Calendar endCalendar = Calendar.getInstance()
		endCalendar.setTime(end)
		boolean nonWeekendOrMoreBiggerTimeUnit = !isSameDay(startCalendar, endCalendar) || !isWeekend(startCalendar)
		if (DATE_TODAY.compareTo(start) >= 0 && DATE_TODAY.compareTo(end) < 0 && nonWeekendOrMoreBiggerTimeUnit) {
			// Color green
			assert (cell.getCssValue('background-color') == COLOR_CELL_TODAY || cell.getCssValue('background-color') == COLOR_CELL_TODAY_HOVER)
		} else if (weekendShouldColor
				&& (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
			// Color red
			assert (cell.getCssValue('background-color') == COLOR_CELL_WEEKEND || cell.getCssValue('background-color') == COLOR_CELL_WEEKEND_HOVER)
		} else if (weekendShouldColor && DateUtils.isSameDay(DATE_HOLIDAY, start)) {
			assert (cell.getCssValue('background-color') == COLOR_CELL_WEEKEND || cell.getCssValue('background-color') == COLOR_CELL_WEEKEND_HOVER)
		}
	}

	static isWeekend(Calendar calendar) {
		return calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
	}

	static isSameDay(Calendar calendarA, Calendar calendarB) {
		return
				calendarA.get(Calendar.YEAR) == calendarB.get(Calendar.YEAR)
				&& calendarA.get(Calendar.MONTH) == calendarB.get(Calendar.MONTH)
				&& calendarA.get(Calendar.DAY_OF_MONTH) == calendarB.get(Calendar.DAY_OF_MONTH)
	}

	static assertNoWeekendOrHolidaysCells() {
		$$zZz('.cell-base').forEach(item ->
				assertCellNoWeekendOrHolidayColor(item)
		)
	}

	static assertCellNoWeekendOrHolidayColor(NuclosWebElement cell) {
		assert cell.getCssValue('background-color') == COLOR_CELL_BASE
				|| cell.getCssValue('background-color') == COLOR_CELL_HOVER
				|| cell.getCssValue('background-color') == COLOR_CELL_TODAY
				|| cell.getCssValue('background-color') == COLOR_CELL_TODAY_HOVER
	}

}
