package org.nuclos.test.webclient.preference

import org.apache.commons.lang.StringEscapeUtils
import org.json.JSONObject
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.MenuComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.preference.PreferenceItem
import org.nuclos.test.webclient.pageobjects.preference.PreferenceType
import org.nuclos.test.webclient.pageobjects.preference.Preferences
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.By
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

/**
 * create, share, customize webclient preferences
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class PreferencesTest extends AbstractRerunnableWebclientTest {

	final String SIDEVIEWCONFIG1 = 'Sideviewconfig1'
	final String ORDERPOSITION1 = 'Orderposition1'
	String[] prefColumns1 = ['example_rest_Order_orderNumber', 'example_rest_Order_customer', 'example_rest_Order_note']
	String[] prefColumns1Names = ['Order number', 'Customer', 'Note']
	String[] prefColumns2 = ['example_rest_Order_note']
	String[] prefColumns2Names = ['Note']
	String[] prefPosition1 = ['example_rest_OrderPosition_article', 'example_rest_OrderPosition_price', 'example_rest_OrderPosition_quantity']
	String[] prefPosition1Names = ['Article', 'Price', 'Quantity']

	@Before
	void createUserAndPreference() {
		RESTHelper.createUser('preferencetest', 'preferencetest', ['Example user'], nuclosSession)
		loginAndCreatePref(prefColumns1)
	}

	@Test
	void runTest_03sharePref() {
		Sidebar.resizeSidebarComponent(500)

		assert Sidebar.getColumnHeaders().equals(['Order number', 'Customer', 'Note'])

		Preferences.open()

		Preferences.filter('name', SIDEVIEWCONFIG1)
		def pref = getFirstPreferenceItem()
		pref.select()
		assert !pref.shared
		pref = getFirstPreferenceItem()
		Preferences.shareItem(PreferenceType.TABLE, pref.name, 'Example user')
		assert getFirstPreferenceItem().shared

		assertSideviewConfigurationWithOtherUser(prefColumns1Names)
	}

	@Test
	void runTest_04_05_customizeAndPublishPref() {
		sharePref(SIDEVIEWCONFIG1)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.openColumnConfigurationPanel()
		SideviewConfiguration.selectSideviewConfigurationInModal(SIDEVIEWCONFIG1)

		SideviewConfiguration.deselectAll()

		prefColumns2.each {
			SideviewConfiguration.selectColumn(it.toString())
		}

		SideviewConfiguration.close()

		Preferences.open()
		Preferences.filter('name', SIDEVIEWCONFIG1)
		def pref = getFirstPreferenceItem()
		pref.select()
		pref = getFirstPreferenceItem()

		assert pref.customized
		pref.publishChanges()

		assert !getFirstPreferenceItem().customized

		assertSideviewConfigurationWithOtherUser(prefColumns2Names)
	}

	@Test
	void runTest_06discardCustomizedPrefInSideview() {

		// customize again
		customizePref(SIDEVIEWCONFIG1, prefColumns2)

		waitUntilTrue({Sidebar.selectedColumns().findAll() { !it.equals('Status') }.sort().equals(prefColumns2Names.sort())}, 20)

		// discard changes
		SideviewConfiguration.openColumnConfigurationPanel()
		SideviewConfiguration.selectSideviewConfigurationInModal(SIDEVIEWCONFIG1)
		SideviewConfiguration.clickButtonOk({
			SideviewConfiguration.discardChanges()
			assert !$$('#sideviewmenu-selector')[0].getAttribute('selectedIndex').equals('-1')
		})
		Sidebar.resizeSidebarComponent(500)

		waitUntilTrue({Sidebar.selectedColumns().findAll() { !it.equals('Status') }.sort().equals(prefColumns1Names.sort())}, 20)
	}

	@Test
	void runTest_06discardCustomizedPrefInManagePrefView() {

		// customize again
		customizePref(SIDEVIEWCONFIG1, prefColumns2)

		// discard changes
		Preferences.open()
		Preferences.filter('name', SIDEVIEWCONFIG1)
		def pref = getFirstPreferenceItem()
		pref.select()
		pref = getFirstPreferenceItem()
		assert pref.customized
		pref.discardChanges()

		assertSideviewConfigurationWithOtherUser(prefColumns1Names)
	}

	@Test
	void runTest_07discardCustomizedPrefInUserMenu() {

		// customize again
		customizePref(SIDEVIEWCONFIG1, prefColumns2)

		// discard changes
		MenuComponent.toggleUserMenu()
		MenuComponent.clickOpenPreferencesResetModal()
		$('#button-reset-allprefs')?.click()

		assertSideviewConfigurationWithOtherUser(prefColumns1Names)

		// customize again
		customizePref(SIDEVIEWCONFIG1, prefColumns2)

		// discard changes
		discardPrefChanges()

		assertSideviewConfigurationWithOtherUser(prefColumns1Names)
	}

	@Test
	void runTest_09testIfManagePreferencesAreAvailable() {
		MenuComponent.toggleUserMenu()
		// NUCLOS-6620 1A: User with SharePreference privilege are allowed to manage preferences
		assert MenuComponent.getOpenPreferencesManage()
		MenuComponent.toggleUserMenu()

		Preferences.open()
		assert Preferences.getPreferenceItems().size() > 0

		logout()
		RESTHelper.createUser('nopreference', 'nopreference', ['Example readonly'], nuclosSession)
		login('nopreference', 'nopreference', false)

		MenuComponent.toggleUserMenu()
		// NUCLOS-6620 1A: User without SharePreference privilege are not allowed to manage preferences
		assert !MenuComponent.getOpenPreferencesManage()
		MenuComponent.toggleUserMenu()

		Preferences.open()
		assert Preferences.getPreferenceItems().size() == 0

	}

	@Test
	void runTest_10shareEntityLessPref() {
		logout()
		login('nuclos', '', false) // user of "Example user" group

		// create preference for dynamic task list (not yet supported by webclient)
		String input = '{"type": "tasklist-table", "name": "OrderDynTasks", "content": {' +
				'"taskListId": "YKY111j2BzkvqTL1iu58",' +
				'"columns": [' +
				'{' +
				'"name": "Ordernumber",' +
				'"column": "Ordernumber",' +
				'"width": 116,' +
				'"position": 0,' +
				'"sort": {' +
				'"direction": "asc",' +
				'"prio": 1' +
				'}' +
				'},' +
				'{' +
				'"name": "Ordersum",' +
				'"column": "Ordersum",' +
				'"width": 121,' +
				'"position": 1' +
				'}' +
				']' +
				'}}'
		RESTHelper.postJson(RESTHelper.REST_BASE_URL + '/preferences', input, nuclosSession)

		MenuComponent.toggleUserMenu()
		Preferences.open()
		Preferences.filter('name', 'OrderDynTasks')
		def pref = Preferences.preferenceItems.get(0)
		pref.select()
		assert !pref.shared
		pref.shareWith('Example user')
		pref = Preferences.preferenceItems.get(0)
		assert pref.shared
		pref.unshareFrom('Example user')
		pref = Preferences.preferenceItems.get(0)
		assert !pref.shared
	}

	@Test
	void runTest_11_deletePreferences() {
		logout()
		login('preferencetest', 'preferencetest', false)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.newSideviewConfiguration('Sideviewconfig2')
		prefColumns1.each {
			SideviewConfiguration.selectColumn(it.toString())
		}
		SideviewConfiguration.saveSideviewConfiguration()
		SideviewConfiguration.close()

		Preferences.open()

		Preferences.filter('name', 'Sideviewconfig2')
		def pref = getFirstPreferenceItem()
		pref.select()
		pref.doNotdelete()

		Preferences.filter('name', 'Sideviewconfig2')
		pref = getFirstPreferenceItem()
		assert pref != null

		pref.select()
		pref.delete()

		Preferences.filter('name', 'Sideviewconfig2')
		pref = getFirstPreferenceItem()
		assert pref == null
	}

	@Test
	void runTest_12_restrictNumberOfRequestsInPreferencesManager() {
		Preferences.open()
		Preferences.resetView()
		assert Preferences.getPreferenceItems().size() > 1

		countBrowserRequests {
			Preferences.getPreferenceItems()[1].select()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_READ_SHARE) == 1
		}

		duplicateCurrentWindow()
		loginAndCreatePref(prefColumns1)

		switchToOtherWindow()
		Preferences.open()
		assert Preferences.getPreferenceItems().size() > 0

		countBrowserRequests {
			Preferences.preferenceItems.get(0).select()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_READ_SHARE) == 1
		}
	}

	@Test
	void runTest_13_selectAllPreferencesViaButton() {
		logout()
		login('test', 'test', false) // user of "Example user" group

		Preferences.open()
		Preferences.resetView()

		assert Preferences.getSelectedPreferenceItems().isEmpty()

		// first button select all
		$$("#collapseSelection button")[0].click()

		assert !Preferences.getSelectedPreferenceItems().isEmpty()
	}

	@Test
	void runTest_14_selectDeselectAllPreferencesViaCheckbox() {
		logout()
		login('test', 'test', false) // user of "Example user" group

		Preferences.open()
		Preferences.resetView()

		assert Preferences.getSelectedPreferenceItems().isEmpty()

		// first button select all
		$(".ag-header-container .ag-header-select-all input").click()

		assert !Preferences.getSelectedPreferenceItems().isEmpty()
	}

	@Test
	void runTest_15_deselectAllPreferencesViaButton() {
		logout()
		login('test', 'test', false) // user of "Example user" group

		Preferences.open()
		Preferences.resetView()

		// first button select all
		$$("#collapseSelection button")[0].click()

		assert !Preferences.getSelectedPreferenceItems().isEmpty()

		// third button deselect all
		$$("#collapseSelection button")[2].click()
		assert Preferences.getSelectedPreferenceItems().isEmpty()
	}

	@Test
	void runTest_16_filterCustomizedViaButtonPreferencesViaButton() {
		logout()
		login('test', 'test', false) // user of "Example user" group

		customizePref('Default', prefColumns1)

		Preferences.open()
		Preferences.resetView()

		assert Preferences.getSelectedPreferenceItems().isEmpty()

		// second button filter all customized
		$$("#collapseSelection button")[1].click()

		assert Preferences.getSelectedPreferenceItems().size() == 1

		discardPrefChanges()
	}

	@Test
	void runTest_17_resetFilterCustomizedViaButtonPreferencesViaButton() {
		logout()
		login('test', 'test', false) // user of "Example user" group

		customizePref('Default', prefColumns1)

		Preferences.open()
		Preferences.resetView()

		// second button filter all customized
		$$("#collapseSelection button")[1].click()

		assert Preferences.getSelectedPreferenceItems().size() == 1

		// fourth button reset all
		$$("#collapseSelection button")[3].click()

		assert Preferences.getSelectedPreferenceItems().isEmpty()
		assert Preferences.getPreferenceItems().size() > 1
	}

	@Test
	void runTest_18_showContentForSinglePreferenceOnly() {
		logout()
		login('test', 'test', false)

		Preferences.open()
		Preferences.resetView()

		//select all
		$$("#collapseSelection button")[0].click()

		assert $$("button[data-target='#collapseTwo']").isEmpty()
	}

	@Test
	void runTest_19_modifyPreferenceContent() {
		logout()
		login('test', 'test', false)

		Preferences.open()
		Preferences.resetView()

		Preferences.getPreferenceItems().get(0).select()

		$$zZz("button[data-bs-target='#collapseSelection']")[0].click()
		$$zZz("button[data-bs-target='#collapseDelete']")[0].click()
		$$zZz("button[data-bs-target='#collapseOne']")[0].click()
		$$zZz("button[data-bs-target='#collapseTwo']")[0].click()

		saveWithoutModification:
		{
			openEditor()

			waitUntilTrue({ -> $$("#preferenceContentEditor")[0] != null })

			//click save button
			$$("#collapseTwo button")[0].click()

			def editor = $$("#preferenceContentEditor")[0]
			assert editor == null

			def json = $$("#selected-preference-json")[0]
			assert json != null
		}

		cancelWithoutModification:
		{
			openEditor()

			def editor = $$("#preferenceContentEditor")[0]
			assert editor != null

			//click cancel button
			$$("#collapseTwo button")[1].click()

			editor = $$("#preferenceContentEditor")[0]
			assert editor == null

			def json = $$("#selected-preference-json")[0]
			assert json != null
		}

		def saveinvalid = { String text ->
			openEditor()

			$("textarea.ace_text-input").sendKeys(text)
			//click save button
			$$("#collapseTwo button")[0].click()

			//expect error
			//cancel button must be there
			assert $$("#collapseTwo button")[1] != null
			assert $("ngb-popover-window") != null
			def errorMessage = $("ngb-popover-window div.popover-body").text
			//error message must not be empty
			assert errorMessage?.trim()

			//delete "invalid"
			def editor_input = $("textarea.ace_text-input")
			//close popover and delete invalid text
			sendKeys(Keys.ESCAPE)
			editor_input.click()

			text.size().times {
				editor_input.sendKeys(Keys.BACK_SPACE)
			}
		}

		saveAfterClientError:
		{
			saveinvalid("invalid")
			//click save button
			$$("#collapseTwo button")[0].click()
			waitForAngularRequestsToFinish()
			//popup must be closed after save
			assert $("ngb-popover-window") == null
		}

		cancelAfterClientError:
		{
			saveinvalid("invalid")
			//click cancel button
			$$("#collapseTwo button")[1].click()
			waitForAngularRequestsToFinish()
			//popup must be closed after cancel
			assert $("ngb-popover-window") == null
		}

		saveValidContent:
		{
			openEditor()

			String jsGetEditorValue = 'return ace.edit("preferenceContentEditor").getValue()'
			String content = AbstractPageObject.executeScript(jsGetEditorValue)
			String modifiedContent = content.replace("\"sideviewMenuWidth\": 200", "\"sideviewMenuWidth\": 300")
			String jsSetEditorValue = 'ace.edit("preferenceContentEditor").setValue("' + StringEscapeUtils.escapeJavaScript(modifiedContent) + '")'
			AbstractPageObject.executeScript(jsSetEditorValue)

			//click save button
			$$("#collapseTwo button")[0].click()

			//get content view
			assert $$("#selected-preference-json")[0].getText().equals(modifiedContent)
		}
	}

	@Test
	void runTest_20_moveColumns() {
		//Open the Article Layout
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)

		//open the preferences view
		$('.view-preferences-button-mainbar').click()

		//get a reference to the buttons, which are responsible for moving columns in the y-direction
		def buttonMoveDown = $('#btn-move-down')
		def buttonMoveUp = $('#btn-move-up')
		def buttonMoveToTop = $('#btn-move-to-top')
		def buttonMoveToBottom = $('#btn-move-to-bottom')

		//at first, when nothing is selected, all buttons should be disabled.
		assert !buttonMoveDown.enabled && !buttonMoveUp.enabled
		assert !buttonMoveToBottom.enabled && !buttonMoveToTop.enabled

		SideviewConfiguration.selectAll();
		//store all list items in a list for later
		def allSelectedItems = SideviewConfiguration.getAllSelected()

		//if a selected element is at the bottom, only the upward movement buttons should be enabled
		allSelectedItems.last().click()

		assert !buttonMoveDown.enabled && !buttonMoveToBottom.enabled
		assert buttonMoveUp.enabled && buttonMoveToTop.enabled

		//if a selected element is neither at the top nor at the bottom
		//all buttons should be enabled
		allSelectedItems[1].click()

		assert buttonMoveDown.enabled && buttonMoveToBottom.enabled
		assert buttonMoveUp.enabled && buttonMoveToTop.enabled

		//select the top element and check if only the downward movement buttons are enabled
		def moveRow = SideviewConfiguration.getAllSelected()[0]
		moveRow.click()

		assert buttonMoveDown.enabled && buttonMoveToBottom.enabled
		assert !buttonMoveUp.enabled && !buttonMoveToTop.enabled

		//move element completely to the bottom
		buttonMoveToBottom.click()
		assert SideviewConfiguration.getAllSelected().last().getAttribute("attr-fqn") == moveRow.getAttribute("attr-fqn")

		//move element one row up
		buttonMoveUp.click()
		assert SideviewConfiguration.getAllSelected()[-2].getAttribute("attr-fqn") == moveRow.getAttribute("attr-fqn")

		//move element one row down
		buttonMoveDown.click()
		assert SideviewConfiguration.getAllSelected().last().getAttribute("attr-fqn") == moveRow.getAttribute("attr-fqn")

		//move element all the way to the top
		buttonMoveToTop.click()
		assert SideviewConfiguration.getAllSelected()[0].getAttribute("attr-fqn") == moveRow.getAttribute("attr-fqn")

		//test & assert that the shift-click marking and the ctrl-click marking work for the list to the right
		helpTestMarkColumns(true, allSelectedItems)

		//unselect all items to test the marking in the unselected list.
		SideviewConfiguration.deselectAll()
		def allUnselectedItems = SideviewConfiguration.getAllUnselected()

		//test & assert that the shift-click marking and the ctrl-click marking work for the list to the left
		helpTestMarkColumns(false, allUnselectedItems)
	}

	private void helpTestMarkColumns(boolean selected, List<NuclosWebElement> allItems) {
		NuclosWebElement firstItem = allItems[0]
		NuclosWebElement thirdItem = allItems[2]
		NuclosWebElement fourthItem = allItems[3]
		NuclosWebElement fifthItem = allItems[4]
		NuclosWebElement sixthItem = allItems[5]

		//test shift selection by selecting the rows from the third item to the sixth
		thirdItem.click()
		clickWithKey([sixthItem], Keys.SHIFT)

		List<NuclosWebElement> expectedSelection = [thirdItem, fourthItem, fifthItem, sixthItem]
		List<NuclosWebElement> realSelection = SideviewConfiguration.getMarked(selected)

		//check if expectedSelection and realSelection match
		assert realSelection.size() == expectedSelection.size()
		realSelection.eachWithIndex{ it, index ->
			assert expectedSelection[index].getAttribute("attr-fqn") == realSelection[index].getAttribute("attr-fqn")
		}

		//Test selecting multiple rows with gaps between by holding the ctrl key
		firstItem.click()
		clickWithKey([thirdItem, fifthItem], Keys.CONTROL)

		//check if the indices {0, 2, 4} have the selection background color
		expectedSelection = [firstItem, thirdItem, fifthItem]
		realSelection = SideviewConfiguration.getMarked(selected)

		assert realSelection.size() == expectedSelection.size()
		realSelection.eachWithIndex{ it, index ->
			assert expectedSelection[index].getAttribute("attr-fqn") == realSelection[index].getAttribute("attr-fqn")
		}
	}

	@Test
	void modifySubformPreferences() {
		deleteConfigurationIfExists(ORDERPOSITION1, 'OrderPosition')
		//create pref
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		Subform sfOrderPosition = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		sfOrderPosition.openViewConfiguration()

		SideviewConfiguration.newSideviewConfiguration(ORDERPOSITION1)
		SideviewConfiguration.deselectAll()
		prefPosition1.each {
			SideviewConfiguration.selectColumn(it as String)
		}
		SideviewConfiguration.saveSideviewConfiguration()
		SideviewConfiguration.close()
		eo.cancel()

		//share pref
		Preferences.open()
		Preferences.filter('name', ORDERPOSITION1)
		def pref = Preferences.preferenceItems.first()
		pref.select()
		pref = Preferences.preferenceItems.first()
		pref.shareWith('Example user')

		//load pref, sort and resize
		eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		sfOrderPosition = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assert sfOrderPosition.getColumnHeaders().toArray() == prefPosition1Names
		sfOrderPosition.clickColumnHeader('example_rest_OrderPosition_article')
		sfOrderPosition.resizeColumn('example_rest_OrderPosition_article', 400)
		eo.cancel()

		//check pref
		Preferences.open()
		Preferences.filter('name', ORDERPOSITION1)
		pref = Preferences.preferenceItems.first()
		pref.select()
		pref = Preferences.preferenceItems.first()
		assert pref.customized
		pref.publishChanges()
		def url = getCurrentUrl()
		def prefId = url.substring(url.lastIndexOf('/') + 1)
		JSONObject prefContent = nuclosSession.getPreferenceContent(prefId)
		assert prefContent != null
		def colArticle = prefContent.getJSONArray('columns').getJSONObject(0)
		assert colArticle.getJSONObject('sort').getString('direction') == 'asc': 'OrderPosition should be sorted by \'article\' in ascending order'
		assert colArticle.getInt('width') == 400: 'column \'article\' should be 400px wide'

		//load pref and change order via SideviewConfiguration
		eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		sfOrderPosition = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		sfOrderPosition.openViewConfiguration()
		SideviewConfiguration.close()
		eo = EntityObjectComponent.forDetail()
		sfOrderPosition = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		sfOrderPosition.getColumnHeaders()[1] == 'Article'
		eo.cancel()
	}

	private void openEditor() {
		while ($zZz("#collapseTwo").canScrollBottom()) {
			$zZz("#collapseTwo").scrollBottom()
		}

		//click edit button
		$$("#collapseTwo button")[0].click()
	}

	private sharePref(String name) {
		Preferences.open()
		Preferences.filter('name', name)
		def pref = getFirstPreferenceItem()
		pref.select()
		pref = getFirstPreferenceItem()
		Preferences.shareItem(PreferenceType.TABLE, pref.name, 'Example user')
	}

	private void customizePref(String name, String[] prefColumns1, boolean shareBefore = true) {
		if (shareBefore) {
			sharePref(name)
		}
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.openColumnConfigurationPanel()
		SideviewConfiguration.selectSideviewConfigurationInModal(name)

		SideviewConfiguration.deselectAll()

		prefColumns1.each {
			SideviewConfiguration.selectColumn(it.toString())
		}
		SideviewConfiguration.close()
		Sidebar.resizeSidebarComponent(500)
	}

	private void discardPrefChanges() {
		// discard changes
		MenuComponent.toggleUserMenu()
		MenuComponent.clickOpenPreferencesResetModal()
		waitForAngularRequestsToFinish()
		waitForNotNull(10, { $('#button-reset-prefs-for-current-entity-class') }).click()
	}

	private PreferenceItem getFirstPreferenceItem() {
		Preferences.getPreferenceItems()[0]
	}

	private void assertSideviewConfigurationWithOtherUser(String[] selectedColumns) {
		logout()
		login('test', 'test', false) // user of "Example user" group

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.selectSideviewConfiguration(SIDEVIEWCONFIG1)
		Sidebar.resizeSidebarComponent(500)

		assert Sidebar.getColumnHeaders().equals(selectedColumns)

		logout()
		login('preferencetest', 'preferencetest', false)
	}

	private void loginAndCreatePref(String[] prefColumns1) {
		// TODO: Make the Test login with the correct user directly
		logout()
		login('preferencetest', 'preferencetest', false)
		deleteConfigurationIfExists(SIDEVIEWCONFIG1, 'Order')
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Sidebar.resizeSidebarComponent(500)

		SideviewConfiguration.openColumnConfigurationPanel()
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		// TODO could be less requests
		countBrowserRequests {
			SideviewConfiguration.newSideviewConfiguration(SIDEVIEWCONFIG1)
		}.with {
			/// TODO fails on test server
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_LOAD) == 1
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_SAVE) == 1
		}

		countBrowserRequests {
			prefColumns1.each {
				SideviewConfiguration.selectColumn(it.toString())
			}
		}.with {
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_LOAD) == 0
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_SAVE) == 0
		}
		countBrowserRequests {
			if (SideviewConfiguration.isSideviewConfigurationModified()) {
				SideviewConfiguration.saveSideviewConfiguration()
			}
		}.with {
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_LOAD) == 0
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_SAVE) == 1
		}
		countBrowserRequests {
			SideviewConfiguration.close()
		}.with {
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_LOAD) == 3
			/// assert it.getCount(RequestCounts.REQUEST_TYPE.PREFERENCE_SAVE) == 0
		}
	}

	private deleteConfigurationIfExists(String config, String entityName) {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		if ($('#cancelEO')) {
			eo.cancel()
		}
		Preferences.open()
		Preferences.filter('name', config)
		Preferences.filter('boName', entityName)
		if (!Preferences.preferenceItems.isEmpty()) {
			def pref = getFirstPreferenceItem()
			pref.select()
			pref.delete()
		}
	}

}
