package org.nuclos.test.webclient.search

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.LoginParams
import org.nuclos.test.webclient.pageobjects.Datepicker
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Searchtemplate
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.preference.PreferenceType
import org.nuclos.test.webclient.pageobjects.preference.Preferences
import org.nuclos.test.webclient.pageobjects.search.Searchbar
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic
import net.lightbody.bmp.proxy.CaptureType

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SearchTest2 extends AbstractRerunnableWebclientTest {

	static final int COUNT_NUCLET_ORDER_SEARCHFILTER = 3
	static final String CUSTOMER_SEARCHFILTER = 'Customer'
	static final String ORDERPOSITION_SEARCHFILTER = 'Order position'
	static final String SYSTEMFIELDS_SEARCHFILTER = 'Systemfields'

	static final String ORDERPOSITION_SEARCHFILTER_EXPERT_WHERECAUSE = ' EXISTS ( SELECT example_rest_OrderPosition.id FROM example_rest_OrderPosition WHERE example_rest_Order.id=example_rest_OrderPosition_order AND example_rest_OrderPosition_quantity = 10015 ) '

	@Test
	void runBasicSearchEditorTest() {
		String filter1 = 'Filter 1'
		String filter2 = 'Filter 2'
		String username = 'SearchTest2'

		final String USER_LOV2_TEST_SEARCHFILTER = 'User-LOV2-Test'

		_00_setup:
		{
			RESTHelper.createUser(username, 'test', ['Example user'], nuclosSession)
			EntityObject user = RESTHelper.findUserByUsername(username, nuclosSession)

			TestDataHelper.insertTestData(nuclosSession)
			def word1Id = createWord('word1')
			def word2Id = createWord('word2')
			def word3Id = createWord('word3')
			createTestDropdowns(word1Id, null)
			createTestDropdowns(word2Id, null)
			createTestDropdowns(word3Id, user.getId())
		}

		_05_useSearchWithoutSearchfilter:
		{
			EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

			countBrowserRequests {
				Searchbar.openSearchEditor()
				Searchbar.clear()
				assert ['[My search]', '[Meine Suche]'].contains(Searchbar.selectedFilter)
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_ALL) == 1
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 0 // no searchfilter change -> no read
			}

			countBrowserRequests {
				Searchbar.selectAttribute('Note')
				Searchbar.setCondition(
						TestEntities.EXAMPLE_REST_ORDER,
						new Searchtemplate.SearchTemplateItem(
								name: 'note',
								operator: '=',
								value: 'A'
						)
				)
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_ALL) == 4 // 2 changes + 2x GET+PUT
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			}

			Sidebar.assertListCount(2)
		}

		_06_saveSearchAsSearchfilter:
		{
			countBrowserRequests {
				Searchbar.openSearchEditor()
				Searchbar.saveAsSearchfilter()
				Searchbar.editName(filter1)
				Searchbar.save()

				assert filter1 == Searchbar.selectedFilter
				assert Searchbar.filters.size() == COUNT_NUCLET_ORDER_SEARCHFILTER + 2
				assert ['[My search]', '[Meine Suche]'].contains(Searchbar.filters.get(0))
			}.with {
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
				assert it.getRequestCount(RequestType.PREFERENCE_SAVE) == 3
			}
		}

		_07_removeSearchItem:
		{
			Searchbar.removeAttribute(TestEntities.EXAMPLE_REST_ORDER.fqn + '_note')
			Sidebar.assertListCount(6)
		}

		_08_addOtherSearchCondition:
		{
			Searchbar.selectAttribute('Note')
			Searchbar.setCondition(
					TestEntities.EXAMPLE_REST_ORDER,
					new Searchtemplate.SearchTemplateItem(
							name: 'note',
							operator: '=',
							value: 'B'
					)
			)

			Sidebar.assertListCount(4)
		}

		_10_createSecondSearchfilter:
		{
			countBrowserRequests {
				Searchbar.openSearchEditor()
				Searchbar.clear()
				Sidebar.assertListCount(6)
			}.with {
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			}

			countBrowserRequests {
				Searchbar.saveAsSearchfilter()
				assert ['Searchfilter 1', 'Suchfilter 1'].contains(Searchbar.selectedFilter)
				assert Searchbar.filters.size() == COUNT_NUCLET_ORDER_SEARCHFILTER + 3
			}.with {
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			}

			countBrowserRequests {
				Searchbar.selectAttribute('Order number')
				Searchbar.setCondition(
						TestEntities.EXAMPLE_REST_ORDER,
						new Searchtemplate.SearchTemplateItem(
								name: 'orderNumber',
								operator: '<',
								value: '10020150'
						)
				)
				Sidebar.assertListCount(3)
			}.with {
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			}

			countBrowserRequests {
				// 2-value-comparison ... '< 10020150 or > 10020157'
				Searchbar.setCondition(
						TestEntities.EXAMPLE_REST_ORDER,
						new Searchtemplate.SearchTemplateItem(
								name: 'orderNumber',
								operator: '<',
								value: '10020150',
								value2: '10020157'
						)
				)
				Sidebar.assertListCount(4)
			}.with {
				// 2 value inputs result in up to 2 requests, depending on the speed of the inputs
				assert it.getRequestCount(RequestType.EO_READ_LIST) <= 2
			}

			countBrowserRequests {
				Searchbar.editName(filter2)
				Searchbar.save()
			}.with {
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 0
			}
		}

		_15_clearSearch:
		{
			countBrowserRequests {
				Searchbar.openSearchEditor()
				Searchbar.clear()
				Sidebar.assertListCount(6)
			}.with {
				assert it.getRequestCount(RequestType.ALL) <= 3
				assert it.getRequestCount(RequestType.PREFERENCE_DESELECT) == 0
			}

			Searchbar.selectAttribute('Order number')
			def orderNumberInput = Searchbar.getValueString(TestEntities.EXAMPLE_REST_ORDER, 'orderNumber')
			orderNumberInput.setInput('10020144')
			waitForAngularRequestsToFinish()
			Sidebar.assertListCount(1)

			refresh()
			Sidebar.assertListCount(1)

			Searchbar.clear()
			Sidebar.assertListCount(6)

			refresh()
			Sidebar.assertListCount(6)
		}

		_20_selectFirstSearchfilter:
		{
			countBrowserRequests {
				Searchbar.selectSearchfilter(filter1)
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
			}
		}

		_21_reload:
		{
			countBrowserRequests {
				refresh()
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 0
			}

			assert Searchbar.selectedFilter == filter1
		}

		_22_setFavorite:
		{
			Searchbar.openSearchEditor()

			countBrowserRequests {
				Searchbar.setFavorite('child_care', 20)
				Searchbar.done()
				Searchbar.selectSearchfilter(filter2);
				Searchbar.setFavorite('people_outline', 10)
				Searchbar.done()
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_ALL) <= 3
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			}
			countBrowserRequests {
				Searchbar.edit()
				Searchbar.save()
				Searchbar.selectSearchfilter(filter1, true);
				Searchbar.edit()
				Searchbar.save()
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_SAVE) == 3
				assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
				assert it.getRequestCount(RequestType.PREFERENCE_READ) == 2
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			}

			assert Searchbar.getFavorites() == ['Filter 2', 'Filter 1']
		}

		_23_selectFavorite:
		{
			Searchbar.openSearchEditor()
			Searchbar.clear()
			Searchbar.closeSearchEditor()

			countBrowserRequests {
				Searchbar.selectFavorite(filter1);
				Sidebar.assertListCount(2)
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
				assert it.getRequestCount(RequestType.ALL) == 2
			}
			countBrowserRequests {
				Searchbar.selectFavorite(filter2);
				Sidebar.assertListCount(4)
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
				assert it.getRequestCount(RequestType.ALL) == 2
			}
			// re-select -> un-select
			countBrowserRequests {
				Searchbar.selectFavorite(filter2);
				Sidebar.assertListCount(6)
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
				assert it.getRequestCount(RequestType.ALL) == 2
			}
		}

		_24_editItem:
		{
			Searchbar.openSearchEditor()
			Searchbar.selectSearchfilter(filter2)

			countBrowserRequests {
				Searchbar.setCondition(
						TestEntities.EXAMPLE_REST_ORDER,
						new Searchtemplate.SearchTemplateItem(
								name: 'orderNumber',
								operator: '=',
								value: '10020150'
						)
				)

				Sidebar.assertListCount(1)
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_READ) <= 2
				assert it.getRequestCount(RequestType.PREFERENCE_READ) ==
						it.getRequestCount(RequestType.PREFERENCE_ALL)
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			}
		}

		_25_toggleItem:
		{
			Searchbar.openSearchEditor()

			countBrowserRequests {
				Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDER, 'orderNumber')

				Sidebar.assertListCount(6)
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_READ) == 0
				assert it.getRequestCount(RequestType.PREFERENCE_ALL) == 0
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			}
		}

		_26_undoChanges:
		{
			Searchbar.openSearchEditor()

			countBrowserRequests {
				Searchbar.edit()
				Searchbar.undo()

				Sidebar.assertListCount(4)
			}.with {
				assert it.getRequestCount(RequestType.PREFERENCE_SELECT) <= 1
				assert it.getRequestCount(RequestType.PREFERENCE_SAVE) <= 1
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			}
		}

		_27_shareSearchfilter:
		{
			Preferences.open()
			Preferences.shareItem(PreferenceType.SEARCHTEMPLATE, filter1, 'Example user')
			this.afterPreferencesShare()
		}

		_28_checkSharedOptions:
		{
			Searchbar.selectFavorite(filter1)
			Searchbar.openSearchEditor()
			Searchbar.edit()
			assert !Searchbar.isDeleteAllowed()
		}

		_29_customize:
		{
			Searchbar.done()
			Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDER, 'note')
			Sidebar.assertListCount(6)

			Searchbar.edit()
			Searchbar.save()
			refresh()
			assert Searchbar.selectedFilter == filter1
			Sidebar.assertListCount(6)
		}

		_30_resetCustomization:
		{
			Searchbar.openSearchEditor()
			Searchbar.edit()
			Searchbar.resetCustomization()
			Sidebar.assertListCount(2)
		}

		_34_unshareAndDeleteSearchfilter:
		{
			Preferences.open()
			Preferences.unshareItem(PreferenceType.SEARCHTEMPLATE, filter1, 'Example user')
			this.afterPreferencesShare()

			countBrowserRequests {
				Searchbar.openSearchEditor()
				Searchbar.delete()
			}.with {
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
				assert it.getRequestCount(RequestType.PREFERENCE_DELETE) == 1
			}
		}

		_35_deleteLastSearchfilter:
		{
			Searchbar.selectFavorite(filter2)

			countBrowserRequests {
				Searchbar.openSearchEditor()
				Searchbar.delete()
			}.with {
				assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
				assert it.getRequestCount(RequestType.PREFERENCE_DELETE) == 1
			}
		}

		_40_searchfilterWithSubform:
		{
			Searchbar.selectSearchfilter(ORDERPOSITION_SEARCHFILTER)

			assert Searchbar.selectedFilter == ORDERPOSITION_SEARCHFILTER
			Sidebar.assertListCount(6)

			Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'quantity')
			Sidebar.assertListCount(3)

			Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'price')
			Sidebar.assertListCount(0)

			Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'quantity')
			Sidebar.assertListCount(1)
		}

		_41_createSearchfilterWithSubform:
		{
			Searchbar.clear()
			Searchbar.closeSearchEditor()
			Searchbar.openSearchEditor()
			Searchbar.enableSubformSearch()

			def subformLov = Searchbar.getSubformLov();
			assert subformLov.getChoices().containsAll(['< Order >', 'Order position'])

			subformLov.selectEntry('Order position')
			def attrLov = Searchbar.getAttributeLov()

			assert attrLov.getChoices().containsAll(['Article', 'Category', 'Name', 'Price', 'Quantity'])
			attrLov.selectEntry('Quantity')

			Sidebar.assertListCount(6)

			def quantityInput = Searchbar.getValueString(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'quantity')
			quantityInput.setInput('224')
			waitForAngularRequestsToFinish()
			Sidebar.assertListCount(1)
		}

		_43_searchSystemVlp:
		{
			Searchbar.selectSearchfilter(SYSTEMFIELDS_SEARCHFILTER)

			// automatic filtering for this system fields:
			def processLov = Searchbar.getValueMultiselect(TestEntities.EXAMPLE_REST_ORDER, 'nuclosProcess')
			def stateLov = Searchbar.getValueMultiselect(TestEntities.EXAMPLE_REST_ORDER, 'nuclosState')

			assert processLov.choices.size() == 3
			processLov.closeViaEscape()
			assert stateLov.choices.size() == 4
		}

		_44_searchVlpWithReferenceParam:
		{
			Searchbar.selectSearchfilter(CUSTOMER_SEARCHFILTER)

			def customerLov = Searchbar.getValueMultiselect(TestEntities.EXAMPLE_REST_ORDER, 'customer')
			def customerAddressLov = Searchbar.getValueMultiselect(TestEntities.EXAMPLE_REST_ORDER, 'customerAddress')

			customerLov.selectEntry('VLPText: Test-Customer')
			sendKeys(Keys.ESCAPE)
			assert customerAddressLov.getChoiceElements().size() == 2 // 2 choices
			customerLov.selectEntry('VLPText: Test-Customer 2')
			sendKeys(Keys.ESCAPE)
			// more than 1 entry selected in customer, but dependent vlp only supports one id -> empty entry only
			assert customerAddressLov.getChoiceElements().size() == 0

			customerLov.clearMultiSelection()
			customerLov.selectEntry('VLPText: Test-Customer 2')
			sendKeys(Keys.ESCAPE)
			assert customerAddressLov.getChoiceElements().size() == 1 // 1 choice
		}

		_45_searchVlpWithNonReferenceParams:
		{
			EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES)
			Searchbar.selectSearchfilter(USER_LOV2_TEST_SEARCHFILTER)

			def booleanValue = Searchbar.getValueBoolean(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'boolean')
			def dateValue = Searchbar.getValueDatepicker(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'date')
			def userlov2Value = Searchbar.getValueMultiselect(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'userlov2')
			assert booleanValue.isYes() && !booleanValue.isNo()                            // VLP: superuser only
			assert getDateFromPicker(dateValue).equals(newDate(2019, 5, 1)) // VLP: created at > this date
			assert userlov2Value.getChoices().containsAll('nuclos')

			setDateInPicker(dateValue, newDate(2200, 2, 2))
			sendKeys(Keys.ESCAPE)
			assert userlov2Value.getChoices().isEmpty()

			Searchbar.edit()
			Searchbar.resetCustomization()
			booleanValue = Searchbar.getValueBoolean(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'boolean')
			userlov2Value = Searchbar.getValueMultiselect(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'userlov2')
			booleanValue.selectNo()
			assert booleanValue.isNo() && !booleanValue.isYes()
			assert userlov2Value.getChoices().containsAll('test', 'SearchTest2')

			userlov2Value.closeViaEscape()

			Searchbar.selectAttribute('Name')
			Searchbar.selectAttribute('Integer')
			// default string value configured is '%' and search is LIKE 'stringParam'
			def stringValue = Searchbar.getValueString(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'name')
			stringValue.setInput('SearchTest2')
			def choices = userlov2Value.getChoices()
			assert choices.containsAll('SearchTest2')
			assert choices.size() == 1

			def integerValue = Searchbar.getValueString(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'integer')
			integerValue.setInput('0')
			choices = userlov2Value.getChoices()
			assert choices.isEmpty()
			assert choices.size() == 0

			integerValue.setInput('1')
			choices = userlov2Value.getChoices()
			assert choices.containsAll('SearchTest2')
			assert choices.size() == 1

			userlov2Value.closeViaEscape()
			stringValue.setInput(null)
			integerValue.setInput(null)
			choices = userlov2Value.getChoices()
			assert choices.containsAll('test', 'SearchTest2')
		}

		_50_testLongReference:
		{
			EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS)

			Searchbar.openSearchEditor()
			Searchbar.clear()
			Sidebar.assertListCount(3)

			Searchbar.selectAttribute('Word')
			Searchbar.setCondition(
					TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS,
					new Searchtemplate.SearchTemplateItem(
							name: 'word',
							operator: '=',
							value: 'word2'
					)
			)
			Searchbar.closeSearchEditor()

			Sidebar.assertListCount(1)
		}

		_51_testUidReference:
		{
			EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS)

			Searchbar.openSearchEditor()
			Searchbar.clear()
			Sidebar.assertListCount(3)

			Searchbar.selectAttribute('Nuc Benutzer')
			Searchbar.setCondition(
					TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS,
					new Searchtemplate.SearchTemplateItem(
							name: 'nucbenutzer',
							operator: '=',
							value: username
					)
			)

			Sidebar.assertListCount(1)
		}

		_52_testSelfReferenceOperatorsAvailable: {
			EntityObject parentNode = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE)
			parentNode.setAttribute('name', 'Node Parent')
			nuclosSession.save(parentNode)

			EntityObject subNode1 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE)
			subNode1.setAttribute('name', 'Sub Node A')
			subNode1.setAttribute('reference', parentNode)
			nuclosSession.save(subNode1)

			EntityObject subNode2 = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE)
			subNode2.setAttribute('name', 'Sub Node B')
			subNode2.setAttribute('reference', parentNode)
			nuclosSession.save(subNode2)

			EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE)
			Searchbar.openSearchEditor()
			Searchbar.enableSubformSearch()
			def subformLov = Searchbar.getSubformLov()
			subformLov.open()
			subformLov.selectEntry('TestInfiniteRecursiveTree')

			def attrLov = Searchbar.getAttributeLov()
			attrLov.open()
			attrLov.selectEntry('name')

			assert $('#search-operator-nuclet_test_other_TestInfiniteRecursiveTree_name').getValue() == '='

			def subNodeNameFilterInput = Searchbar.getValueString(TestEntities.NUCLET_TEST_OTHER_TESTINFINITERECURSIVETREE, 'name')
			subNodeNameFilterInput.setInput('Sub Node A')
			waitForAngularRequestsToFinish()
			Sidebar.assertListCount(1)
		}
		// see also NUCLOS-10739: verify that clicking the next/previous month button in the datechooser, does not close the datechooser when the click is outside the opened search editor popup
		_53_testPickerRemainOpenWhenNavigatingMonthsOrYears: {
			EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES)
			Searchbar.openSearchEditor()
			def dateValue = Searchbar.getValueDatepicker(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES, 'date')
			dateValue.open()

			$$('ngb-datepicker .ngb-dp-arrow').first().click()
			$$('ngb-datepicker .form-select').first().click()

			assert $('ngb-datepicker'): 'Datepicker should still be open'
		}
		// see also NUCLOS-10879: verify, that Booleans with value NULL are included in the "No" Option, when filtering.
		_60_testEqualsFalseIncludesNull: {
			logout()
			loginUnsafe(new LoginParams(username: 'nuclos', locale: Locale.GERMAN, datalanguage: 'Deutsch (DE)'))

			EntityObjectComponent.open(TestEntities.NUCLOS_USER)
			Searchbar.openSearchEditor()
			Searchbar.selectAttribute('Gesperrt')
			def booleanValue = Searchbar.getValueBoolean(TestEntities.NUCLOS_USER, 'locked')
			booleanValue.selectNo()
			Sidebar.assertListCount(3)
			booleanValue.selectYes()
			Sidebar.assertListCount(0)
			booleanValue.selectNo()
			Sidebar.assertListCount(3)
		}
	}

	private afterPreferencesShare() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		// Why aren't the preferences reloaded? Are they cached?
		refresh()
	}

	private void setDateInPicker(Datepicker datepicker, Date date) {
		datepicker.setInput(AbstractNuclosTest.context.dateFormat.format(date))
		sendKeys(Keys.ESCAPE)
	}

	private Date getDateFromPicker(Datepicker datepicker) {
		AbstractNuclosTest.context.dateFormat.parse(datepicker.getInput())
	}

	private Date newDate(int year, int month, int day) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.clear()
		cal.set(Calendar.YEAR, year)
		cal.set(Calendar.MONTH, month - 1)
		cal.set(Calendar.DAY_OF_MONTH, day)
		cal.getTime()
	}

	static def createWord(String word) {
		EntityObject eoWord = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		eoWord.setAttribute('text', word)
		eoWord.setAttribute('times', "1")
		return nuclosSession.save(eoWord)
	}

	static void createTestDropdowns(def wordId, def userId) {
		EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS)
		eo.setAttribute('word', [id: wordId])
		if (userId) {
			eo.setAttribute('nucbenutzer', [id: userId])
		}
		nuclosSession.save(eo)
	}


	@Test
	void runExpertModeTest() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Searchbar.openSearchEditor()
		Searchbar.selectSearchfilter(ORDERPOSITION_SEARCHFILTER)
		Searchbar.toggleItemSelection(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'quantity')
		Sidebar.assertListCount(3)

		Searchbar.expertMode()
		Sidebar.assertListCount(3)

		assert Searchbar.getExpertWhereCause() == ORDERPOSITION_SEARCHFILTER_EXPERT_WHERECAUSE

		def newCause = ORDERPOSITION_SEARCHFILTER_EXPERT_WHERECAUSE.replace('10015', '180')

		countBrowserRequests {
			// Change of expert where cause should not search again
			Searchbar.setExpertWhereCause(ORDERPOSITION_SEARCHFILTER_EXPERT_WHERECAUSE.replace('10015', '180'))
			Sidebar.assertListCount(3)
		}.with {
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 0
		}

		countBrowserRequests {
			Searchbar.refresh()
			Sidebar.assertListCount(1)
		}.with {
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
		}

		// Test save and restore
		Searchbar.edit()
		Searchbar.save()
		EntityObjectComponent.refresh()
		Sidebar.assertListCount(1)
		Searchbar.openSearchEditor()
		assert Searchbar.getExpertWhereCause() == newCause
	}

	@Test
	void runStatusNumberTest() {
		TestDataHelper.insertTestData(nuclosSession)
		def entityObjects = nuclosSession.getEntityObjects(TestEntities.EXAMPLE_REST_ORDER)
		Long eoCount = entityObjects.stream().filter(
				{ eo -> (Integer) eo.getAttribute('nuclosStateNumber') < 79 })
				.count()
		entityObjects.first().changeState(80)
		eoCount--

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Searchbar.openSearchEditor()
		Searchbar.selectAttribute('Statusnumeral')
		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'nuclosStateNumber',
						operator: '<',
						value: '79'
				)
		)
		Sidebar.assertListCount(eoCount.intValue())
	}

	// Tests NUCLOS-9480
	@Test
	void testFilterChangeLikeEqualsOnReference() {
		TestDataHelper.insertTestData(nuclosSession)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Searchbar.openSearchEditor()
		Searchbar.selectAttribute('Customer')
		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'customer',
						operator: '=',
						value: 'VLPText: Test-Customer'
				)
		)
		assert Sidebar.getListEntryCount() == 6: 'All orders should be found'

		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'customer',
						operator: 'like',
						value: 'Test*2'
				)
		)
		assert Sidebar.getListEntryCount() == 0: 'No orders should be found after switching to different filter'

		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'customer',
						operator: 'not like',
						value: 'Test*2'
				)
		)
		assert Sidebar.getListEntryCount() == 6: 'All orders should be found after switching to different filter'

		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'customer',
						operator: '=',
						value: 'VLPText: Test-Customer'
				)
		)
		assert Sidebar.getListEntryCount() == 6: 'All orders should be found after switching again'

		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'customer',
						operator: '!=',
						value: 'VLPText: Test-Customer'
				)
		)
		assert Sidebar.getListEntryCount() == 0: 'No orders should be found after switching again'
	}

	@Test
	void testFilterChangeNotLikeNotEqualsOnTextAttribute() {
		TestDataHelper.insertTestData(nuclosSession)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Searchbar.openSearchEditor()
		Searchbar.selectAttribute('Note')

		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'note',
						operator: '!=',
						value: 'A'
				)
		)
		assert Sidebar.getListEntryCount() == 4: 'Different result list expected for search condition'

		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'note',
						operator: 'not like',
						value: 'B*'
				)
		)
		assert Sidebar.getListEntryCount() == 2: 'Different result list expected for search condition'
	}

	@Test
	void testFilterByID() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Searchbar.openSearchEditor()
		Searchbar.selectAttribute('ID')

		getBrowserHar([CaptureType.REQUEST_HEADERS, CaptureType.RESPONSE_HEADERS, CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT], {
			Searchbar.setCondition(
					TestEntities.EXAMPLE_REST_ORDER,
					new Searchtemplate.SearchTemplateItem(
							name: 'primaryKey',
							operator: '=',
							value: '1234'
					)
			)
		}).with {
			assert it.log.entries*.request.find {it.url.contains("example_rest_Order/query?") && it.postData?.text?.contains('col0 = 1234')}
		}

		assert Sidebar.getListEntryCount() == 0: 'entry with this primary key (1234) should not exist'
	}

	@Test
	void testSearchWithTextAttributeAndVlp() {
		RESTClient client = new RESTClient(
				new LoginParams(username: 'nuclos', password: '', datalanguage: 'de_DE')
		)
		assert client.login().initialDatalanguage == 'de_DE'

		EntityObject<Long> artikeltyp = new EntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKELTYP)
		artikeltyp.setAttribute('artikeltyp', 'Software')
		client.save(artikeltyp)

		EntityObject<Long> artikel = new EntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		artikel.setAttribute('artikelname', 'Nuclos')
		artikel.setAttribute('artikelschlagwort', '1 one')
		artikel.setAttribute('artikeltyp', [id: artikeltyp.getId()])
		client.save(artikel)

		EntityObject<Long> artikel2 = new EntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		artikel2.setAttribute('artikelname', 'Nuclos Addon')
		artikel2.setAttribute('artikelschlagwort', '2 two')
		artikel2.setAttribute('artikeltyp', [id: artikeltyp.getId()])
		client.save(artikel2)

		EntityObjectComponent.open(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		assert Sidebar.getListEntryCount() == 2: 'All articles should be found'

		Searchbar.openSearchEditor()
		Searchbar.selectAttribute('Artikelschlagwort')
		Searchbar.setCondition(
				TestEntities.NUCLET_TEST_I18N_ARTIKEL,
				new Searchtemplate.SearchTemplateItem(
						name: 'artikelschlagwort',
						operator: '=',
						value: '1 one'
				)
		)
		assert Sidebar.getListEntryCount() == 1: 'Articles should be filtered by \'1 one\''

	}

}
