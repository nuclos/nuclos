package org.nuclos.test.webclient.sidebar

import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformRow
import org.openqa.selenium.Keys
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class CollectiveProgressingTest extends AbstractRerunnableWebclientTest {

	@Before
	void setup() {
		TestDataHelper.insertTestData(nuclosSession)

		RESTHelper.createUser('testCollectiveProcessing', 'test', ['Example user','Example user Collective Processing'], nuclosSession)
		RESTHelper.createUser('testBulkEdit', 'test', ['Example user', 'Example user Collective Processing', 'Example user Bulk Edit'], nuclosSession)
	}

	@Test
	void _01_checkCollectiveProcessingNotAllowed() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.toggleMultiSelectionAll()
		assert !CollectiveProgressingComponent.isCollectiveProcessing()

		Sidebar.toggleMultiSelection()
	}

	@Test
	void _02_checkCollectiveProgressingOpens() {
		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.toggleMultiSelectionAll()
		assert CollectiveProgressingComponent.isCollectiveProcessing()

		assert Sidebar.countMultiSelected() == Sidebar.listEntryCount

		refresh()
		waitForAngularRequestsToFinish()

		assert Sidebar.countMultiSelected() == Sidebar.listEntryCount

		Sidebar.toggleMultiSelection()

		assert Sidebar.countMultiSelected() == 0
	}

	@Test
	void _03_checkWeCannotManipulateSingleEntry() {
		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.showDetails()

		assert EntityObjectComponent.newButton == null
		assert EntityObjectComponent.saveButton == null
		assert EntityObjectComponent.cloneButton == null
		assert EntityObjectComponent.deleteButton == null

		CollectiveProgressingComponent.showCollectiveProcessing()
		CollectiveProgressingComponent.cancelCollectiveProcessing()
		waitForAngularRequestsToFinish()
	}

	@Test
	void _04_checkOpeningViaMultiSelectingElementsInSidebar() {
		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.selectRange(0, 4, true)
		assert Sidebar.countMultiSelected() == 5
		assert Sidebar.isMultiSelecting()

		CollectiveProgressingComponent.cancelCollectiveProcessing()
		waitForAngularRequestsToFinish()
		Sidebar.toggleMultiSelection()

		Sidebar.selectRange(0, 2, false)
		assert Sidebar.countMultiSelected() == 2
		assert Sidebar.isMultiSelecting()

		CollectiveProgressingComponent.cancelCollectiveProcessing()
		waitForAngularRequestsToFinish()
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _05_testSelectingViaKeyboardSpace() {
		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.selectEntry(0)

		sendKeysAndWaitForAngular(Keys.SPACE) //Space hit
		assert CollectiveProgressingComponent.isCollectiveProcessing()

		assert Sidebar.countMultiSelected() == 1

		Sidebar.selectEntry(0)

		sendKeysAndWaitForAngular(Keys.ARROW_DOWN)
		sendKeysAndWaitForAngular(Keys.SPACE) //Space hit

		sendKeysAndWaitForAngular(Keys.ARROW_DOWN)
		sendKeysAndWaitForAngular(Keys.SPACE) //Space hit

		assert Sidebar.countMultiSelected() == 3
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _06_checkCANSTATECHANGEDatensatzFreigabe() {
		RESTHelper.createUser('datentest', 'nuclos', ['TesteDatensatzFreigabeStateChange'], nuclosSession)
		waitForAngularRequestsToFinish()

		logout()
		login('datentest', 'nuclos')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()

		assert CollectiveProgressingComponent.stateChangeNames.empty

		waitForAngularRequestsToFinish()
		Sidebar.toggleMultiSelection()

		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()

		assert !CollectiveProgressingComponent.stateChangeNames.empty

		Sidebar.toggleMultiSelection()
	}

	@Test
	void _07_checkStateChangeWorking() {
		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.stateChangeNames.containsAll(['abschließen', 'abbrechen'])

		// assert abbrechen has right text-color (default black)
		assert CollectiveProgressingComponent.stateChangeActions.get(1).$('a').getCssValue('color') == 'rgba(0, 0, 0, 1)'

		assertMessageModalAndConfirm("Soll der Statuswechsel wirklich durchgeführt werden?", "Alles erledigt", {
            CollectiveProgressingComponent.doStateChange('abschließen')
            waitForAngularRequestsToFinish()
        })

		CollectiveProgressingComponent.assertProgress()

		CollectiveProgressingComponent.assertProgressSuccess(6)

		CollectiveProgressingComponent.closeProgress()
		waitForAngularRequestsToFinish()


		Sidebar.toggleMultiSelectionAll()
		Sidebar.toggleMultiSelection()
		waitForAngularRequestsToFinish()
	}

	@Test
	void _08_checkObjGenWithoutDialog() {
		logout()
		login('nuclos', '')
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		// first set address for order to make ObjGen working
		Sidebar.selectEntry(0)
		EntityObjectComponent.forDetail().setAttribute('customerAddress', 'Test 1, 12345, Test 1')
		EntityObjectComponent.forDetail().save()

		Sidebar.selectEntry(1)
		EntityObjectComponent.forDetail().setAttribute('customerAddress', 'Test 1, 12345, Test 1')
		EntityObjectComponent.forDetail().save()

		Sidebar.selectEntry(2)
		EntityObjectComponent.forDetail().setAttribute('customerAddress', 'Test 2, 54321, Test 2')
		EntityObjectComponent.forDetail().save()

		Sidebar.toggleMultiSelection()
		Sidebar.selectRange(0, 2, true)
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.funcNames.containsAll(['generate Invoice'])
		CollectiveProgressingComponent.doFunction('generate Invoice')
		// sleep is necessary for the window handles here,
		// maybe because of the many windows open at the same time
		sleep(2000)
		// waitForAngularRequestsToFinish()

		Log.debug 'Wait for 3 open windows'
		waitUntilTrue { driver.getWindowHandles().size() == 3 }
		Log.debug 'Switch to window 1'
		driver.switchTo().window(driver.getWindowHandles()[0])
		Log.debug 'Close other windows'
		closeOtherWindows()

		CollectiveProgressingComponent.assertProgress()

		CollectiveProgressingComponent.assertProgressSuccess(3)
		def genObjs = CollectiveProgressingComponent.getProgressGenObjectNames(3)
		assert (genObjs.count('1 (Test-Customer)') == 2 || genObjs.count('2 (Test-Customer)') == 2)
		assert (genObjs.count('2 (Test-Customer)') == 1 || genObjs.count('1 (Test-Customer)') == 1)

		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.closeProgress()
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _09_checkObjGenWithDialog() {
		logout()
		login('nuclos', '')
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		// first set address for order to make ObjGen working
		Sidebar.selectEntry(4)
		EntityObjectComponent.forDetail().getLOV('customer').selectEntry('VLPText: Test-Customer 2')
		EntityObjectComponent.forDetail().save()

		Sidebar.selectEntry(5)
		EntityObjectComponent.forDetail().getLOV('customer').selectEntry('VLPText: Test-Customer 2')
		EntityObjectComponent.forDetail().save()

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.funcNames.containsAll(['generate Invoice Dialog'])
		CollectiveProgressingComponent.doFunction('generate Invoice Dialog')
		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.assertProgress()

		// dialog handling
		handleObjectGenDialog('City 4, 44444, Street 4')
		handleObjectGenDialog('Test 2, 54321, Test 2')

		CollectiveProgressingComponent.assertProgressSuccess(6)
		def genObjs = CollectiveProgressingComponent.getProgressGenObjectNames(6)
		assert (genObjs.count('2 (Test-Customer)') == 4 || genObjs.count('1 (Test-Customer)') == 4)
		assert (genObjs.count('2 (Test-Customer 2)') == 2 || genObjs.count('1 (Test-Customer 2)') == 2)

		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.closeProgress()
		CollectiveProgressingComponent.showCollectiveProcessing()
		CollectiveProgressingComponent.cancelCollectiveProcessing()
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _10_checkCustomRulesAction() {
		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		// SendOrder is now hidden in Nuclet:
		//assert CollectiveProgressingComponent.funcNames.containsAll(['Diesen Auftrag sperren...', 'SendOrder'])
		assert CollectiveProgressingComponent.funcNames.containsAll(['Diesen Auftrag sperren...'])

		CollectiveProgressingComponent.doFunction('Diesen Auftrag sperren...')
		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.assertProgress()

		CollectiveProgressingComponent.assertProgressSuccess(6)

		CollectiveProgressingComponent.closeProgress()
		waitForAngularRequestsToFinish()
		Sidebar.toggleMultiSelectionAll()
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _11_testEditAction() {
		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		assert !CollectiveProgressingComponent.actionNames.contains('Felder ändern')
		Sidebar.toggleMultiSelection()

		logout()
		login('testBulkEdit', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.actionNames.contains('Felder ändern')


		CollectiveProgressingComponent.doAction('Felder ändern')

		assert CollectiveEditingComponent.isCollectiveEditing()
		assert !CollectiveEditingComponent.isStartEditingEnabled()

		CollectiveEditingComponent.stopEditing()

		assert !CollectiveEditingComponent.isCollectiveEditing()

		CollectiveProgressingComponent.doAction('Felder ändern')

		assert CollectiveEditingComponent.isCollectiveEditing()

		assert CollectiveEditingComponent.getAttributeSelection().getChoices().containsAll(
				[
						'Approved credit limit',
						'Confirmation',
						'Send date',
						'Delivery date (priority)',
						'Rechnung',
						'Order date',
						'Note',
						'Order',
						'Customer address',
						'Discount',
						'agent',
						'Customer'
				]
		)

		assert !CollectiveEditingComponent.getAttributeTable().isShowing()

		CollectiveEditingComponent.getAttributeSelection().selectEntry('Approved credit limit')

		assert CollectiveEditingComponent.startEditingEnabled
		assert CollectiveEditingComponent.getAttributeTable().isShowing()

		assert CollectiveEditingComponent.getAttributeTable().getHeaderColumns().containsAll(
				[
						'Feld',
						'Neuer Wert',
						'Aktueller Wert',
				]
		)

		List<NuclosWebElement> columnRows = CollectiveEditingComponent.getAttributeTable().getRowColumnsAt(1)

		// Enter new value for Approved credit limit
		columnRows.get(1)
				.$("input")
				.click()

		sendKeys('10')

		// Click button for removing
		columnRows.get(3)
				.$("button")
				.click()

		Dialog.confirmClose()

		assert CollectiveEditingComponent.getAttributeTable().getRows().get(1).hasClass("table-warning")
		columnRows = CollectiveEditingComponent.getAttributeTable().getRowColumnsAt(1)
		columnRows.get(3).text == 'Feld aus der Bearbeitung ausgeschlossen.'

		assert !CollectiveEditingComponent.startEditingEnabled

		CollectiveEditingComponent.getAttributeSelection().selectEntry('Approved credit limit')
		assert !CollectiveEditingComponent.getAttributeTable().getRows().get(1).hasClass("table-warning")

		CollectiveEditingComponent.getAttributeSelection().selectEntry('Customer')
		columnRows = CollectiveEditingComponent.getAttributeTable().getRowColumnsAt(2)
		ListOfValues lovCustomer = ListOfValues.fromElement('nuc-attribute-example_rest_Order_customer',
				columnRows.get(1).$("nuc-dropdown[for-id=nuc-attribute-example_rest_Order_customer]"))

		lovCustomer.open()
		assert lovCustomer.getChoices().containsAll(
				[
						'VLPText: Test-Customer',
						'VLPText: Test-Customer 2'
				]
		)

		int rowSizeBeforeSelect = CollectiveEditingComponent.getAttributeTable().getRows().size()
		lovCustomer.search('VLPText: Test-Customer 2')
		lovCustomer.selectEntry('VLPText: Test-Customer 2')
		waitForAngularRequestsToFinish()

		// due to rule settings there should be 2 fields added automatically (Customer address and Discount)
		waitFor { CollectiveEditingComponent.getAttributeTable().getRows().size() - rowSizeBeforeSelect == 2 }

		// check value table for Customer
		List<List<String>> expectedValues = [['Test-Customer', '6']];
		checkAttributeValuesCountTable(CollectiveEditingComponent.getAttributeTable(), 2, expectedValues)

		// Customer address should contain only one address
		CollectiveProgressingComponent.scrollBottom()
		columnRows = CollectiveEditingComponent.getAttributeTable().getRowColumnsAt(3)
		ListOfValues lovCustomerAddress = ListOfValues.fromElement('nuc-attribute-example_rest_Order_customerAddress',
				columnRows.get(1).$("nuc-dropdown[for-id=nuc-attribute-example_rest_Order_customerAddress]"))

		assert lovCustomerAddress.getChoices().containsAll(
				[
						'City 4, 44444, Street 4'
				]
		)

		lovCustomerAddress.selectEntry('City 4, 44444, Street 4')

		// Discount should have pre-filled value of 5 from Customer
		CollectiveProgressingComponent.scrollBottom()
		columnRows = CollectiveEditingComponent.getAttributeTable().getRowColumnsAt(4)
		assert columnRows.get(1).$("input").value == '5'

		// Enter some Date
		CollectiveEditingComponent.getAttributeSelection().selectEntry('Send date')
		columnRows = CollectiveEditingComponent.getAttributeTable().getRowColumnsAt(5)
		CollectiveProgressingComponent.scrollBottom()
		Datepicker columnDatePicker = new Datepicker(columnRows.get(1))
		columnDatePicker.setInput(AbstractNuclosTest.context.dateFormat.format(newDate(2200, 2, 2)))
		sendKeys(Keys.ESCAPE)

		// Enter some Big Text
		CollectiveEditingComponent.getAttributeSelection().selectEntry('Note')
		CollectiveProgressingComponent.scrollBottom()
		columnRows = CollectiveEditingComponent.getAttributeTable().getRowColumnsAt(6)
		columnRows.get(1).$("textarea").click()
		sendKeys('Some bigger text-input as comment\nwith line break')

		CollectiveEditingComponent.startEditing()

		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.assertProgress()

		// TODO
		// Should we assert more then just the progress here?
		CollectiveProgressingComponent.assertProgressSuccess(6)

		CollectiveProgressingComponent.closeProgress()

		Sidebar.toggleMultiSelection()
	}

	@Test
	void testSetNullEditAction() {
		logout()
		login('testBulkEdit', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.doAction('Felder ändern')

		CollectiveEditingComponent.getAttributeSelection().selectEntry('Note')
		List<List<String>> expectedValues = [['A', '2'], ['B', '4']]
		checkAttributeValuesCountTable(CollectiveEditingComponent.getAttributeTable(), 1, expectedValues)

		CollectiveEditingComponent.startEditing()
		CollectiveProgressingComponent.closeProgress()

		CollectiveProgressingComponent.doAction('Felder ändern')
		CollectiveEditingComponent.getAttributeSelection().selectEntry('Note')

		expectedValues = []
		checkAttributeValuesCountTable(CollectiveEditingComponent.getAttributeTable(), 1, expectedValues)

		List<NuclosWebElement> columnRows = CollectiveEditingComponent.getAttributeTable().getRowColumnsAt(1)

		columnRows.get(1)
				.$("textarea")
				.click()

		sendKeys('test')
		CollectiveEditingComponent.startEditing()
		CollectiveProgressingComponent.closeProgress()

		CollectiveProgressingComponent.doAction('Felder ändern')
		CollectiveEditingComponent.getAttributeSelection().selectEntry('Note')

		expectedValues = [['test', '6']]
		checkAttributeValuesCountTable(CollectiveEditingComponent.getAttributeTable(), 1, expectedValues)

		Sidebar.toggleMultiSelection()
	}

	@Test
	void _12_testDeleteAction() {
		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.actionNames.contains('Löschen')

		assertMessageModalAndConfirm("Löschen", "Sollen die Datensätze wirklich gelöscht werden?", {
            CollectiveProgressingComponent.doAction('Löschen')
		    waitForAngularRequestsToFinish()
        })
        waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.assertProgress()

		CollectiveProgressingComponent.assertProgressSuccess(6, 6)

		CollectiveProgressingComponent.closeProgress()

		assert Sidebar.listEntryCount == 0
		Sidebar.toggleMultiSelection()
	}

	@Test
	void _13_testKeyEventsInDetailsView() {
		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		subform: {
			Subform sfPosition = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

			sfPosition.getRow(0).get_bodyRow().focus()
			sendKeysAndWaitForAngular(Keys.ESCAPE)

			sendKeysAndWaitForAngular(Keys.SPACE)
			assert Sidebar.countMultiSelected() == 0
			assert sfPosition.getRow(0).isSelected()

			assert Sidebar.selectedIndex == 0
			sendKeysAndWaitForAngular(Keys.ARROW_DOWN)
			assert Sidebar.selectedIndex == 0
			sendKeysAndWaitForAngular(Keys.SPACE)
			assert Sidebar.countMultiSelected() == 0
			assert sfPosition.getRow(1).isSelected()

			sendKeysAndWaitForAngular(Keys.ARROW_UP)
			assert Sidebar.selectedIndex == 0
			sendKeysAndWaitForAngular(Keys.SPACE)
			assert Sidebar.countMultiSelected() == 0
			assert !sfPosition.getRow(0).isSelected()
		}

		textarea: {
			NuclosWebElement textAreaElement = eo.getAttributeElement('note')
			textAreaElement.focus()
			sendKeysAndWaitForAngular(Keys.ARROW_DOWN)
			assert Sidebar.selectedIndex == 0
			sendKeysAndWaitForAngular(Keys.ARROW_UP)
			assert Sidebar.selectedIndex == 0
		}
	}

	@Test
	void testProgressUpdateNotification() {
		logout()
		login('testCollectiveProcessing', 'test')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.toggleMultiSelection()
		assert Sidebar.isMultiSelecting()

		Sidebar.toggleMultiSelectionAll()
		waitForAngularRequestsToFinish()

		assert CollectiveProgressingComponent.funcNames.contains('ProcessOrder')
		CollectiveProgressingComponent.doFunction('ProcessOrder', false)

		sleep(1000)
		assert CollectiveProgressingComponent.progressHeader == 'Stapelverarbeitung wird ausgeführt': 'Header should notice we are processing'
		assert CollectiveProgressingComponent.progressGridOverlayVisible(): 'AgGrid Overlay should be visible'

		waitForAngularRequestsToFinish()
		assert CollectiveProgressingComponent.progressHeader == 'Ergebnis Ihrer Stapelverarbeitung': 'After processing, header should change'
		assert !CollectiveProgressingComponent.progressGridOverlayVisible(): 'AgGrid Overlay should be hidden now'

		CollectiveProgressingComponent.closeProgress()

		Sidebar.toggleMultiSelection()
	}

	/**
	 * Test to verify that the ObjectGenerator supports GROUP BY functions for System Entities with UID Primary Keys.
	 * See: NUCLOS-10252
	 */
	@Test
	void testNUCLOS_10252ObjectGen() {
		EntityObject sourceEo1 = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORSOURCEWITHSYSTEMENTITY)
		sourceEo1.setAttribute('name', 'quelle1')
		sourceEo1.setAttribute('sprache', [id: 'de_DE'])

		EntityObject sourceEo2 = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORSOURCEWITHSYSTEMENTITY)
		sourceEo2.setAttribute('name', 'quelle2')
		sourceEo2.setAttribute('sprache', [id: 'de_DE'])

		EntityObject sourceEo3 = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORSOURCEWITHSYSTEMENTITY)
		sourceEo3.setAttribute('name', 'source1')
		sourceEo3.setAttribute('sprache', [id: 'en_GB'])

		nuclosSession.save(sourceEo1)
		nuclosSession.save(sourceEo2)
		nuclosSession.save(sourceEo3)

		logout()
		login('nuclos')

		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATORSOURCEWITHSYSTEMENTITY)

		Sidebar.selectRange(0, 2, true)

		assert Sidebar.countMultiSelected() == 3
		assert Sidebar.isMultiSelecting()
		assert CollectiveProgressingComponent.isCollectiveProcessing()
		assert CollectiveProgressingComponent.funcNames.containsAll(['Generate-With-Group-By-Reference-To-System-Entity-Field'])

		CollectiveProgressingComponent.doFunction('Generate-With-Group-By-Reference-To-System-Entity-Field')

		driver.switchTo().window(driver.windowHandles[2])
		assert Sidebar.getListEntryCount() == 2

		driver.switchTo().window(driver.windowHandles[1])
		assert Sidebar.getListEntryCount() == 2

		driver.switchTo().window(driver.windowHandles[0])
		CollectiveProgressingComponent.closeProgress()
	}

	@Test
	void testNUCLOS_8770ObjectGen() {
		logout()
		login('nuclos')

		// Need to test with two different objects
		testNUCLOS_8770ObjectGenStep('Test', 1)
		testNUCLOS_8770ObjectGenStep('Test1234', 2)
	}

	private testNUCLOS_8770ObjectGenStep(String AddressText, Integer rows) {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		Sidebar.selectEntry(0)
		Sidebar.selectRange(0, 1, true)
		assert Sidebar.countMultiSelected() == 2
		assert Sidebar.isMultiSelecting()
		assert CollectiveProgressingComponent.isCollectiveProcessing()
		assert CollectiveProgressingComponent.funcNames.containsAll(['Add AddressText'])

		CollectiveProgressingComponent.doFunction('Add AddressText')
		waitForAngularRequestsToFinish()

		CollectiveProgressingComponent.assertProgress()
		handleObjectGenNUCLOS8770Dialog(AddressText)

		CollectiveProgressingComponent.closeProgress()
		assert Sidebar.countMultiSelected() == 2

		// assure data is correctly updated
		CollectiveProgressingComponent.showDetails()

		Sidebar.selectEntry(0)
		waitForAngularRequestsToFinish()
		assert EntityObjectComponent.forDetail().getAttribute('customerNumber') == '22222': "First entry should be selected"
		EntityObjectComponent.getTab('Addresses').click()
		Subform subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADRESSES, 'customer')
		assert subform.rowCount == rows
		assert subform.getRow(0).getValue('addresstext') == AddressText

		Sidebar.selectEntry(1)
		waitForAngularRequestsToFinish()
		assert EntityObjectComponent.forDetail().getAttribute('customerNumber') == '9001': "Second entry should be selected"
		EntityObjectComponent.getTab('Addresses').click()
		subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADRESSES, 'customer')
		assert subform.rowCount == rows
		assert subform.getRow(0).getValue('addresstext') == AddressText

		Sidebar.selectEntry(0)
		CollectiveProgressingComponent.showCollectiveProcessing()
		CollectiveProgressingComponent.cancelCollectiveProcessing()
		Sidebar.toggleMultiSelection()
	}

	private Date newDate(int year, int month, int day) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.clear()
		cal.set(Calendar.YEAR, year)
		cal.set(Calendar.MONTH, month - 1)
		cal.set(Calendar.DAY_OF_MONTH, day)
		cal.getTime()
	}

	private void checkAttributeValuesCountTable(TableComponent table, int index, List<List<String>> expectedValues) {
		if (table != null) {
			// Third table column should be a button 'Anzeigen'
			List<NuclosWebElement> columnRows = table.getRowColumnsAt(index)

			// Click button for values
			columnRows.get(2)
					.$("button")
					.click()

			$vVv('nuc-collective-editing-value-dialog')
			waitUntilTrue({ isPresent('nuc-collective-editing-value-dialog') })
			TableComponent valuesTable = TableComponent.within($("nuc-collective-editing-value-dialog .modal-body"))

			assert valuesTable.getHeaderColumns().containsAll(
					[
							'Aktueller Wert',
							'Anzahl'
					]
			)

			if (expectedValues.size() > 0) {
				assert valuesTable.getRows().size() > 1
			} else {
				assert valuesTable.getRows().size() == 1
			}

			expectedValues.eachWithIndex { List<String> row, int i ->
				assert valuesTable.getRowColumnsAt(i + 1).get(0).text == row[0]
				assert valuesTable.getRowColumnsAt(i + 1).get(1).text == row[1]
			}

			$("nuc-collective-editing-value-dialog .modal-footer #button-ok").click()
		}
	}

	private void handleObjectGenDialog(String customerAddress) {
		$vVv('nuc-detail-dialog')
		waitUntilTrue({ isPresent('nuc-detail-dialog') })
		waitForAngularRequestsToFinish()
		$$('nuc-detail-dialog div[name="attribute-customerAddress"] input').last().click()
		sendKeys(customerAddress)
		waitForAngularRequestsToFinish()
		sendKeys(Keys.ARROW_DOWN)
		sendKeys(Keys.ENTER)
		sendKeys(Keys.TAB)
		sendKeys('14.10.2020')
		waitForAngularRequestsToFinish()
		$vVv('div.modal-footer')
		$$('div.modal-footer #button-ok').last().click()
		waitForAngularRequestsToFinish()
	}

	private void handleObjectGenNUCLOS8770Dialog(String addressText) {
		waitUntilTrue({ isPresent('nuc-detail-dialog') })
		waitForAngularRequestsToFinish()

		Subform subform = EntityObjectComponent.getSubform(TestEntities.EXAMPLE_REST_ADDRESSDIALOGSUBFORM, 'addressdialog')
		SubformRow row = subform.newRow()

		row.enterValue('addresstext', addressText, false)

		$$('div.modal-footer #button-ok').last().click()
		waitForAngularRequestsToFinish()
	}
}
