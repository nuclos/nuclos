package org.nuclos.test.webclient.sidebar

import org.json.JSONObject
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.FileComponent
import org.nuclos.test.webclient.pageobjects.ImageComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.search.Searchbar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SidebarColumnTest extends AbstractRerunnableWebclientTest {

	@Before
	void setup() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void runTest() {
		_02openSideview:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

			screenshot('open-sideview')

			SideviewConfiguration.newSideviewConfiguration('Sideviewconfig1')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_nuclosStateIcon')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_orderNumber')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')
			SideviewConfiguration.saveSideviewConfiguration()
			SideviewConfiguration.close()
			Sidebar.resizeSidebarComponent(800)

			assert Sidebar.selectedColumns().size() == 3


			// check if header is shown
			assert Sidebar.selectedColumns().contains('Order number')
			assert Sidebar.selectedColumns().contains('Customer')

			// check if orderNumber of selected entry is also shown in sideview table
			String orderNumberInDetailBlock = eo.getAttribute('orderNumber')
			assert Sidebar.findElementContainingText('.ag-cell', orderNumberInDetailBlock) != null
		}


		_03addColumns:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			assert Sidebar.selectedColumns().size() == 3
			SideviewConfiguration.selectSideviewConfiguration('Sideviewconfig1')
			Sidebar.addColumn(TestEntities.EXAMPLE_REST_ORDER.fqn, 'note')
			Sidebar.addColumn(TestEntities.EXAMPLE_REST_ORDER.fqn, 'primaryKey')

			Sidebar.resizeSidebarComponent(950)

			waitForAngularRequestsToFinish()

			List<String> columns = Sidebar.selectedColumns()
			assert columns.size() == 5
			assert columns.contains('Note')
			assert columns.contains("ID")

			long eoId = eo.id
			assert eoId > 0
			assert Sidebar.findEntryByText("$eo.id")
		}

		_03sortColumns:
		{
			resetSorting()

			// sort note column - prio 1 asc
			$$zZz('nuc-sidebar ag-grid-angular .ag-header-row .ag-header-cell-text')[3].click()

			// sort order number column - prio 2 desc
			$$zZz('nuc-sidebar ag-grid-angular .ag-header-row .ag-header-cell-text')[1].click()
			$$zZz('nuc-sidebar ag-grid-angular .ag-header-row .ag-header-cell-text')[1].click()

			Sidebar.assertValue(1, 1, "10020148")
			Sidebar.assertValue(1, 3, "A")

			Sidebar.assertValue(2, 1, "10020154")
			Sidebar.assertValue(2, 3, "B")

			Sidebar.assertValue(3, 1, "10020150")
			Sidebar.assertValue(3, 3, "B")

			Sidebar.assertValue(4, 1, "10020144")
			Sidebar.assertValue(4, 3, "B")

			Sidebar.assertValue(5, 1, "10020140")
			Sidebar.assertValue(5, 3, "B")
		}

		_03sortCalculatedColumnDisabled:
		{
			getNuclosSession().setSystemParameters(['SORT_CALCULATED_ATTRIBUTES': 'false'])
			refresh()

			resetSorting()

			waitForAngularRequestsToFinish()

			Sidebar.hideColumn(TestEntities.EXAMPLE_REST_ORDER.fqn, 'primaryKey')
			Sidebar.addColumn(TestEntities.EXAMPLE_REST_ORDER.fqn, 'calculated')
			Sidebar.moveColumnUp(TestEntities.EXAMPLE_REST_ORDER.fqn, 'calculated')
			Sidebar.hideColumn(TestEntities.EXAMPLE_REST_ORDER.fqn, 'note')

			// sort calculated column - disabled
			$$zZz('nuc-sidebar ag-grid-angular .ag-header-row .ag-header-cell-text')[3].click()

			// sort order number column - prio 2 asc
			$$zZz('nuc-sidebar ag-grid-angular .ag-header-row .ag-header-cell-text')[1].click()

			Sidebar.assertValue(0, 1, "10020140")
			Sidebar.assertValue(0, 3, "B")

			Sidebar.assertValue(1, 1, "10020144")
			Sidebar.assertValue(1, 3, "B")

			Sidebar.assertValue(2, 1, "10020148")
			Sidebar.assertValue(2, 3, "A")

			Sidebar.assertValue(3, 1, "10020150")
			Sidebar.assertValue(3, 3, "B")

			Sidebar.assertValue(4, 1, "10020154")
			Sidebar.assertValue(4, 3, "B")

		}

		_03sortCalculatedColumnEnabled:
		{
			getNuclosSession().setSystemParameters(['SORT_CALCULATED_ATTRIBUTES': 'true'])
			refresh()

			resetSorting()

			waitForAngularRequestsToFinish()

			// sort calculated column - desc
			$$zZz('nuc-sidebar ag-grid-angular .ag-header-row .ag-header-cell-text')[3].click()

			// sort order number column - prio 2 desc
			$$zZz('nuc-sidebar ag-grid-angular .ag-header-row .ag-header-cell-text')[1].click()
			$$zZz('nuc-sidebar ag-grid-angular .ag-header-row .ag-header-cell-text')[1].click()


			Sidebar.assertValue(1, 1, "10020148")
			Sidebar.assertValue(1, 3, "A")

			Sidebar.assertValue(2, 1, "10020154")
			Sidebar.assertValue(2, 3, "B")

			Sidebar.assertValue(3, 1, "10020150")
			Sidebar.assertValue(3, 3, "B")

			Sidebar.assertValue(4, 1, "10020144")
			Sidebar.assertValue(4, 3, "B")

			Sidebar.assertValue(5, 1, "10020140")
			Sidebar.assertValue(5, 3, "B")
		}

		_04reorderColumns:
		{

			Sidebar.assertValue(0, 1, "10020158")
			Sidebar.assertValue(0, 2, "Test-Customer")
			Sidebar.assertValue(0, 3, "A")

			// move "Customer" after "calculated" column
			Sidebar.moveColumnDown(TestEntities.EXAMPLE_REST_ORDER.fqn, 'customer')

			Sidebar.assertValue(0, 1, "10020158")
			Sidebar.assertValue(0, 2, "A")
			Sidebar.assertValue(0, 3, "Test-Customer")

			// move "Customer" before "Order number" column
			Sidebar.moveColumnUp('example_rest_Order', 'customer')
			Sidebar.moveColumnUp('example_rest_Order', 'customer')

			Sidebar.assertValue(0, 1, "Test-Customer")
			Sidebar.assertValue(0, 2, "10020158")
			Sidebar.assertValue(0, 3, "A")
		}

		/**
		 * test that sideview list data is loaded when scrolling down
		 */
		_05dynamicLoadTest:
		{
			TestEntities entityClass = TestEntities.EXAMPLE_REST_CATEGORY
			// add more sideview entries
			def category =
					[
							boMetaId  : entityClass.fqn,
							attributes: [
									'name': ''
							]
					]

			// TODO: Generate test data server side instead of sending 2.000 requests!
			for (int i = 0; i < 2000; i++) {
				category.attributes['name'] = '' + i
				RESTHelper.createBo(category, nuclosSession)
			}
			EntityObjectComponent eo = EntityObjectComponent.open(entityClass)

			waitForAngularRequestsToFinish()

			def loadedEntries = eo.getListEntryCount()

			assert loadedEntries > 0

			// scroll down sideview list
			def nrOfLoadedRows = getRows().size()
			new Actions(driver).moveToElement(getRows()[nrOfLoadedRows - 1]).build().perform();
			waitForAngularRequestsToFinish()

			assert eo.getListEntryCount() > loadedEntries


			// test dynamic load after searching

			Searchbar.search('11')
			loadedEntries = eo.getListEntryCount()

			assert loadedEntries > 0

			// scroll down sideview list
			nrOfLoadedRows = getRows().size()
			new Actions(driver).moveToElement(getRows()[nrOfLoadedRows - 1]).build().perform();
			waitForAngularRequestsToFinish()

			assert eo.getListEntryCount() > loadedEntries
		}


		_06textsearchColumnChangeTest:
		{

			// test if text search remains after column change

			def searchString = '1000'
			Searchbar.search(searchString)
			waitForAngularRequestsToFinish()

			assert EntityObjectComponent.forDetail().getListEntryCount() == 1

			Sidebar.addColumn(TestEntities.EXAMPLE_REST_CATEGORY.fqn, 'createdAt')


			assert Searchbar.getSearchfilter().getAttribute('value') == searchString
		}


		/**
		 * upload image and check that it is visible in sidebar
		 */
		_07imageAttribute:
		{
			EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)


			SideviewConfiguration.newSideviewConfiguration('Sideviewconfig1')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_name')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_image')
			SideviewConfiguration.saveSideviewConfiguration()
			SideviewConfiguration.close()
			Sidebar.resizeSidebarComponent(800)

			ImageComponent fileComponent = eo.getImageComponent('image')

			assert !Sidebar.isImageVisible(0, TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_image')

			fileComponent.dropFile(TestDataHelper.nuclosPngFile)
			eo.save()

			screenshot('image-in-sidebar')

			assert Sidebar.isImageVisible(0, TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_image')

		}
	}

	@Test
	void testNUCLOS_8931_DefectColumnPref() {
		logout()
		login('nuclos')

		def defaultOrderPreferences = RESTHelper.requestJsonArray(
				RESTHelper.REST_BASE_URL + '/preferences?boMetaId=example_rest_Order&type=table',
				HttpMethod.GET,
				nuclosSession.sessionId
		)
		assert defaultOrderPreferences.length() > 0: "We should have gotten some default preferences for Order"

		def orderExamplePrefs = (JSONObject) defaultOrderPreferences.get(0)
		def columns = orderExamplePrefs.getJSONObject("content")
				.getJSONArray("columns")

		assert columns.length() > 0: "We should have some default columns"

		columns.put([
				"boAttrId": "bQNpT5O21FCHvxIJ1POD",
				"position": 3,
				"selected": true,
				"width"   : 68
		])
		columns.put([
				"boAttrId": "somethingwhichDoesNotExists_oho",
				"position": 4,
				"selected": true,
				"width"   : 68
		])

		orderExamplePrefs.getJSONObject("content")
				.put("columns", columns)


		RESTHelper.putJson(
				RESTHelper.REST_BASE_URL + '/preferences/' + orderExamplePrefs.getString("prefId"),
				orderExamplePrefs.toString(),
				nuclosSession
		)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()
		assert currentUrl.contains('example_rest_Order'): "We should not be redirected if a column pref is defect"
		assert Sidebar.selectedColumns().size() == 3: "Defect column should be not displayed"
		assert Sidebar.listEntryCount == 6: "All orders should have loaded correctly"
	}

	@Test
	void sidebarColumnsAfterLoginTest() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_SIDEBARTEST)
		4.times {
			i ->
				eo.addNew()
				eo.setAttribute('name', i)
				eo.save()
		}

		4.times {
			i ->
				Sidebar.assertValue(i, 0, String.valueOf(3-i))
		}

		logout()
		login('test', 'test')
		waitForAngularRequestsToFinish()

		4.times {
			i ->
				Sidebar.assertValue(i, 0, String.valueOf(3-i))
		}
	}

	@Test
	void sidebarLayoutTypeSearchTest() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		String orderNumber = eo.getAttribute('orderNumber')

		Searchbar.search(orderNumber)
		assert Sidebar.listEntryCount == 1

		SideviewConfiguration.open()
		SideviewConfiguration.deselectColumn(TestEntities.EXAMPLE_REST_ORDER.toString() + '_orderNumber')
		SideviewConfiguration.close()
		assert Sidebar.listEntryCount == 0

		SideviewConfiguration.open()
		$('#card').findElement(By.xpath('./..')).click()
		SideviewConfiguration.close()
		assert Sidebar.listEntryCount == 1

		Searchbar.clearTextSearchfilter()
		assert Sidebar.listEntryCount == 6
	}

	@Test
	void testPinnedColumns() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		SideviewConfiguration.open()
		SideviewConfiguration.deselectAll()
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_orderNumber')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_confirmation')
		SideviewConfiguration.pinAll()
		assertMessageModalAndConfirm('Fehler', 'Es dürfen nicht alle Spalten fixiert werden.') {
			SideviewConfiguration.clickButtonOk {}
		}

		SideviewConfiguration.unpinAll()
		SideviewConfiguration.pin(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')
		SideviewConfiguration.clickButtonOk {}
		assert Sidebar.getPinnedAttributeColumns(TestEntities.EXAMPLE_REST_ORDER) == [TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer']

		SideviewConfiguration.open()
		SideviewConfiguration.unpinAll()
		SideviewConfiguration.clickButtonOk {}
		assert Sidebar.getPinnedAttributeColumns(TestEntities.EXAMPLE_REST_ORDER) == []
	}

	private List<WebElement> getRows() {
		// AgGrid garantiert keine sortierte row-index Reihenfolge im DOM mehr
		// Also sortieren wir hier selbst die elemente aus dem DOM nach row-index, die View im Client hingegehen ist korrekt
		driver.findElements(By.cssSelector('nuc-sidebar ag-grid-angular .ag-center-cols-container [row-index]')).sort({
			first, second -> Integer.parseInt(first.getAttribute('row-index')) < Integer.parseInt(second.getAttribute('row-index')) ? -1 : 1
		})
	}

	private void resetSorting() {
		SideviewConfiguration.openColumnConfigurationPanel()
		SideviewConfiguration.resetSorting()
		SideviewConfiguration.close()
	}
}
