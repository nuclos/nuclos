package org.nuclos.test.webclient.sidebar

import org.apache.commons.math3.geometry.partitioning.Side
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractRerunnableWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.MenuComponent
import org.nuclos.test.webclient.pageobjects.Searchtemplate
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.search.Searchbar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SidebarTest extends AbstractRerunnableWebclientTest {
	final static int DIVIDER_SIZE = 16;
	final static int STATUSBAR_HEIGHT = 22;
	List<EntityObject<Object>> lafParams = []

	@Before
	void setup() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
	}

	@After
	void tearDownSystemParameter() {
		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_Orientation_View': 'false'])
		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_ListOnlyView': 'nuclos_LAF_Frame_off'])
		lafParams.forEach({ EntityObject<Object> eo -> nuclosSession.removeBOLAFParameter(eo) })
	}

	@Test
	void resizeInVerticalViewWorks() {
		assert !Sidebar.isHorizontalResizeBar(): 'We should have in default mode vertical orientation'
		Sidebar.resizeSidebarComponent(150)

		Sidebar.resizeRightMax()
		def destSize = $('#content').getSize().getWidth() // some rounding errors
		def sourceSize = Sidebar.getSizebarWidth()
		assert sourceSize == destSize: 'Sizebar should maximized right size minus divider size (' + destSize + ' -> ' + sourceSize + ')'

		Sidebar.resizeLeftMax()
		sourceSize = Sidebar.getSizebarWidth()
		assert  sourceSize == 150: 'Clicking left button should adjust to width before'

		Sidebar.resizeLeftMax()
		assert Sidebar.getSizebarWidth() == 16:'Sidebar should be collapsed and thus width be 0 after clicking left button again'
	}

	@Test
	void changeToHorizontalWorksViaButtonAndViceVersa() {
		assert !Sidebar.isHorizontalResizeBar(): 'We should have in default mode vertical orientation'
		Sidebar.toggleSidebarMode()
		waitForAngularRequestsToFinish()
		assert Sidebar.isHorizontalResizeBar(): 'We should have switched to horizontal orientation'
		assert Sidebar.getSizebarHeight() == 200: 'There should be a default height of 200 set, but was: ' + Sidebar.getSizebarHeight()

		Sidebar.toggleSidebarMode()
		assert !Sidebar.isHorizontalResizeBar(): 'We should be again in vertical orientation'
	}

	@Test
	void changeDefaultOrientationModeViaSystemParameter() {
		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_Orientation_View': 'true'])

		refresh()
		waitForAngularRequestsToFinish()
		assert Sidebar.isHorizontalResizeBar(): 'We should have default to horizontal orientation'


		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_Orientation_View': 'false'])

		refresh()
		waitForAngularRequestsToFinish()
		assert !Sidebar.isHorizontalResizeBar(): 'We should have default to vertical orientation'


		$('#toolbar > div > button:nth-child(1)')?.click()
		assert Sidebar.isHorizontalResizeBar(): 'We should have switched to horizontal orientation'

		refresh()
		waitForAngularRequestsToFinish()
		assert Sidebar.isHorizontalResizeBar(): 'System Parameter is overwritten by user'

		MenuComponent.toggleUserMenu()
		MenuComponent.clickOpenPreferencesResetModal()
		waitForAngularRequestsToFinish()
		waitForNotNull(10, { $('#button-reset-prefs-for-current-entity-class') }).click()
		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_Orientation_View': 'false'])
	}

	@Test
	void resizeHorizontalBarWorks() {
		resizeBrowserWindow(1360, 800)

		assert !Sidebar.isHorizontalResizeBar(): 'We should have in default mode vertical orientation'
		Sidebar.toggleSidebarMode()
		waitForAngularRequestsToFinish()
		assert Sidebar.isHorizontalResizeBar(): 'We should have switched to horizontal orientation'
		assert Sidebar.getSizebarHeight() == 200: 'There should be a default height of 200 set, but was: ' + Sidebar.getSizebarHeight()
		Sidebar.resizeSidebarComponent(400, true)

		Sidebar.resizeLeftMax()
		assert Sidebar.getSizebarHeight() == 16: 'Clicking left button should collapse height, there is only height for statusbar'

		Sidebar.resizeRightMax()
		def destSize = Sidebar.getSizebarHeight()
		def sourceSize = 289
		assert sourceSize == destSize || 290 == destSize: 'Height should be restored to previous value after clicking right button'

		Sidebar.resizeRightMax()
		sourceSize = Sidebar.getSizebarHeight()
		destSize = $('#content').getSize().getHeight()
		assert sourceSize == destSize: 'Height should be whole content'
	}

	@Test
	void refreshSearchListUpdateDetails() {
		Sidebar.assertListCount(6)
		Sidebar.selectEntryByText('10020154')
		def eoLink = driver.currentUrl

		Searchbar.openSearchEditor()
		Searchbar.clear()
		Searchbar.selectAttribute('Order number')
		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'orderNumber',
						operator: '!=',
						value: '10020154'
				)
		)

		// assert selected eo is still 10020154
		Sidebar.assertListCount(5)
		refresh()
		waitForAngularRequestsToFinish()
		def foundOrderNumber = EntityObjectComponent.forDetail().getAttribute('orderNumber')
		assert foundOrderNumber == '10020154': 'EO should still be selected despite it is not in result list'

		getUrlHash('/dashboard')
		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_RefreshOnSearchList': 'true'])
		refresh()
		waitForAngularRequestsToFinish()
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		waitForAngularRequestsToFinish()

		// with activated LAF parameter the list should now select first index as 10020154 is not in search result anymore
		foundOrderNumber = EntityObjectComponent.forDetail().getAttribute('orderNumber')
		assert foundOrderNumber == '10020158': 'First EO should be selected with activated LAF'

		// navigate via direct link should still work
		getUrl(eoLink, true)
		foundOrderNumber = EntityObjectComponent.forDetail().getAttribute('orderNumber')
		assert foundOrderNumber == '10020154': 'EO should still be selected despite it is not in result list'

		Searchbar.openSearchEditor()
		Searchbar.clear()
		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_RefreshOnSearchList': 'false'])
	}

	@Test
	void testEoModificationAlsoInSidebar() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail();
		// patch customer name and relead
		def testCustomer = nuclosSession.getEntityObjects(TestEntities.EXAMPLE_REST_CUSTOMER).find({ eoCustomer ->
			eoCustomer.getAttribute('name') == 'Test-Customer'
		});
		assert testCustomer != null: 'Could not find Test-Customer via REST'
		testCustomer.setAttribute('name', 'Test-Customer renamed')
		testCustomer.save()

		eo = eo.reload()
		assert Sidebar.findEntryByText(eo.getText('customer')) != null
	}

	@Test
	void testListOnlyView() {
		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_ListOnlyView': 'nuclos_LAF_Frame_new'])
		refresh()
		waitForAngularRequestsToFinish()

		assert !$('#detail-container')?.isDisplayed(): 'There should be no detail container anymore'

		// some margin or padding cost us 1px
		def bodyWidth = $('body').getSize().width
		assert Sidebar.sizebarWidth == bodyWidth: 'Sidebar should be complete window size'

		Sidebar.selectEntry(0)

		assert driver.getWindowHandles().size() == 2: 'Entity should be opened in new tab'

		def oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow
		assert $('header')?.isDisplayed(): 'Header should be visible in this tab'

		EntityObjectComponent.forDetail().setAttribute('orderNumber', 12345)
		EntityObjectComponent.forDetail().save()

		StateComponent.confirmStateChange({StateComponent.changeStateByNumeral(80)})
		waitForAngularRequestsToFinish()

		driver.switchTo().window(oldWindow)
		closeOtherWindows()

		// Sidebar entries should have updated
		Sidebar.assertValue(0, 1, "12345")
		assert Sidebar.getStateIconImage(0)
				.getAttribute('src')
				.endsWith('example_rest_ExampleorderSM_State_80'): 'State change should also be propagated'

		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_ListOnlyView': 'nuclos_LAF_Frame_self'])
		refresh()

		assert !$('#detail-container')?.isDisplayed(): 'There should be no detail container anymore'

		// some margin or padding cost us 1px
		bodyWidth = $('body').getSize().width
		assert Sidebar.sizebarWidth == bodyWidth: 'Sidebar should be complete window size'

		Sidebar.selectEntry(1)
		assert driver.getWindowHandles().size() == 1: 'Entity should be opened in same tab'
		assert driver.currentUrl.contains('#/popup'): 'View should be moved to popup thus sidebar should not be visible anymore'
		assert $('#sidebar-container') == null: 'No Sidebar visible'

		nuclosSession.setSystemParameters(['nuclos_LAF_Webclient_ListOnlyView': 'nuclos_LAF_Frame_off'])
	}

	@Test
	void testOnlySubformListOnly() {
		// add LAF for ListOnlyView to CustomerAddress Entity
		lafParams.add(
				nuclosSession.addBOLAFParameter('nuclos_LAF_Webclient_ListOnlyView', 'nuclos_LAF_Frame_self', 'iCkm9KpNyzMk7Zi47tYn')
		)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		Subform subform = EntityObjectComponent.forDetail().getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer')
		subform.editInWindow(0)
		waitForAngularRequestsToFinish()
		assert driver.getWindowHandles().size() == 2: 'Subform entry created new tab'
		driver.switchTo().window(driver.getWindowHandles().last())

		assert $('#detail-container')?.isDisplayed(): 'There should be a detail container showing the selected entry'
		assert $('header')?.isDisplayed(): 'Header should be visible in this tab'
		driver.close()
		driver.switchTo().window(driver.getWindowHandles().first())
	}

	@Test
	void testSearchEditorDoesNotOverflowHorizontalSidebar() {
		Searchbar.openSearchEditor()
		Searchbar.toggleSearchEditorPinned()
		Searchbar.resizeSearchEditor(600)

		def currentSearchEditorSize = Searchbar.searchEditorContainer()?.getSize()?.getHeight()
		Sidebar.toggleSidebarMode()
		assert Searchbar.searchEditorContainer()?.getSize()?.getHeight() < currentSearchEditorSize: 'If no space is available the height should be adjusting automatically'
	}

	@Test
	void testSearchEditorClosesWhenTooSmall() {
		Searchbar.openSearchEditor()
		Searchbar.toggleSearchEditorPinned()
		Sidebar.toggleSidebarMode()

		assert !Searchbar.isPopOverSearchEditor(): 'Default toggling should not result in popover state'
		Sidebar.resizeLeftMax()
		assert Searchbar.isPopOverSearchEditor(): 'No space left for search editor - so its gonna be popover'

		Searchbar.openSearchEditor()
		Searchbar.toggleSearchEditorPinned()
		assert Searchbar.isPopOverSearchEditor(): 'SearchEditor should be not pinnable in this state'
		Sidebar.resizeRightMax()
		Sidebar.resizeRightMax()

		Searchbar.openSearchEditor()
		Searchbar.toggleSearchEditorPinned()
		assert !Searchbar.isPopOverSearchEditor(): 'After resizing and giving more space, searcheditor should be pinnable'
	}

	@Test
	void testSidebarPaging() {
		TestEntities entityClass = TestEntities.EXAMPLE_REST_CATEGORY
		// add more side view entries
		def category =
				[
						boMetaId  : entityClass.fqn,
						attributes: [
								'name': ''
						]
				]

		// TODO: Generate test data server side instead of sending 1.000 requests!
		for (int i = 0; i < 1000; i++) {
			category.attributes['name'] = '' + i
			RESTHelper.createBo(category, nuclosSession)
		}
		EntityObjectComponent.open(entityClass)

		waitForAngularRequestsToFinish()

		def loadedEntries = Sidebar.getListEntryCount()

		assert loadedEntries > 0: 'List should visible'

		// scroll down side view list
		while ($('nuc-sidebar ag-grid-angular .ag-body-viewport [row-index="999"]') == null) {
			def nrOfLoadedRows = Sidebar.getListEntryCount()
			new Actions(driver).moveToElement(Sidebar.getEntryElements()[nrOfLoadedRows - 1].element).build().perform();
			waitForAngularRequestsToFinish()
			assert Sidebar.getListEntryCount() > 0
		}

	}

	/**
	 * See NUCLOS-10253
	 * When the user was not allowed to the UserAction.WorkspaceCustomizeEntityAndSubFormColumn,
	 * then the default path did not recalculate its layout because it did not receive the onSideviewPrefChanged() callback.
	 * This led to the content div adding scrollbars, because the default path did not decrease in size, when resizing the sidebar
	 */
	@Test
	void testResizeNoScrollbarWhenMissingModifyPrefPermission() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		EntityObjectComponent eo = EntityObjectComponent.forDetail().addNew()

		eo.setAttribute('name', 'Stüker')
		eo.setAttribute('vorname', 'Maik')
		eo.setAttribute('alter', '35')
		eo.setAttribute('kondition', 'ganz gut')

		eo.save()

		RESTHelper.createUser('readonly', 'readonly', ['Example readonly'], nuclosSession)

		logout()
		login('readonly', 'readonly')

		// change to path state Erster Aufstieg
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_BERGSTEIGER)
		Sidebar.selectEntry(0)

		Sidebar.resizeSidebarComponent(800)

		assert !$("#content").isHorizontalScrollbarPresent(): 'There should be no horizontal scrollbar in the #content div'
	}

	/**
	 * See NUCLOS-10784
	 * The switch between records should not be delayed by validation status updates on freshly loaded subform entries,
	 * because they are unnecessary and should therefore not occur
	 */
	@Test
	void testPerformanceOfSwitchBetweenRecords() {
		EntityObject<Long> eo = new EntityObject<>(TestEntities.EXAMPLE_REST_ORDER)
		eo.setAttribute('orderNumber', 1)
		List<EntityObject<Long>> positions = new ArrayList<>()
		30.times {
			EntityObject<Long> pos = new EntityObject<>(TestEntities.EXAMPLE_REST_ORDERPOSITION)
			pos.setAttribute('article', [id: getArticle()])
			pos.setAttribute('price', it)
			positions.add(pos)
		}
		eo.dependents.put(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order', positions)
		nuclosSession.save(eo)

		EntityObject<Long> eo2 = nuclosSession.clone(eo, null)
		eo2.setAttribute('orderNumber', 2)
		eo2.dependents.put(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order', positions)
		eo2.save()

		EntityObjectComponent eoComponent = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		refresh()

		Sidebar.selectEntry(1)
		waitFor(2, {
			Sidebar.selectEntry(0)
			return eoComponent.getAttribute('orderNumber') == 2
		})
	}

	private Long getArticle() {
		nuclosSession.getEntityObjects(TestEntities.EXAMPLE_REST_ARTICLE, new QueryOptions(chunkSize: 1)).first().getId()
	}
}
