#!/bin/sh

export DSN="postgres://hydra:secret@$HYDRA_HOST:$HYDRA_DB_PORT/hydra?sslmode=disable"

/usr/bin/hydra migrate sql --yes "$DSN"
/usr/bin/timeout 24h /usr/bin/hydra serve all --dangerous-force-http --dangerous-allow-insecure-redirect-urls="$CALLBACKS" &
HYDRA_PID=$!

sleep 10

# try to delete before creating the client
/usr/bin/hydra clients delete \
        --endpoint "$HYDRA_ADMIN_URL" \
        nuclos > /dev/null 2>&1

/usr/bin/hydra clients create \
        --endpoint "$HYDRA_ADMIN_URL" \
        --id nuclos \
        --secret solcun \
        --grant-types authorization_code,refresh_token,client_credentials \
        --response-types code \
        --scope openid,offline,email \
        --callbacks "$CALLBACKS" \
        --post-logout-callbacks "$POST_LOGOUT_CALLBACKS" \
        --token-endpoint-auth-method client_secret_basic \

wait $HYDRA_PID
