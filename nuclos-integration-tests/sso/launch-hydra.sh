#!/bin/bash

WORKDIR=$(dirname $0)

while [ $# -gt 0 ]; do
  if [[ $1 == *"--"* ]]; then
    v="${1/--/}"
    declare $v="$2"
  fi
  shift
done

COMMAND=${cmd:-launch}
HYDRA_HOSTIP=${hostip:-127.0.0.1}
HYDRA_HOSTNAME=${hostname:-"$HYDRA_HOSTIP"}
NUCLOS_CALLBACK=${callback_url:-"http://127.0.0.1:4200"}
NUCLOS_LOGOUT_CALLBACK=$NUCLOS_CALLBACK
if [[ $NUCLOS_LOGOUT_CALLBACK != */ ]]; then
  NUCLOS_LOGOUT_CALLBACK=${NUCLOS_LOGOUT_CALLBACK}/
fi
NUCLOS_LOGOUT_CALLBACK=${NUCLOS_LOGOUT_CALLBACK}logout
HYDRA_AUTH_PORT=${port:-9000}
HYDRA_ADMIN_PORT=${admin_port:-9001}
HYDRA_CONSENT_PORT=${consent_port:-9020}
HYDRA_DB_PORT=${db_port:-9010}
TTL_ACCESS_TOKEN=${token_lifetime:-60m}

HYDRA_ADMIN_URL=${admin_url:-"http://$HYDRA_HOSTNAME:$HYDRA_ADMIN_PORT"}
HYDRA_CONSENT=${consent_url:-"http://$HYDRA_HOSTNAME:$HYDRA_CONSENT_PORT"}
URLS_SELF_ISSUER=${issuer_url:-"http://$HYDRA_HOSTNAME:$HYDRA_AUTH_PORT/"}
URLS_CONSENT="$HYDRA_CONSENT/consent"
URLS_LOGIN="$HYDRA_CONSENT/login"
URLS_LOGOUT="$HYDRA_CONSENT/logout"

DOCKER_VERSION=$HYDRA_AUTH_PORT
DOCKER_NAME_EXTENSION=$HYDRA_AUTH_PORT

function kill_hydra() {
  docker kill nuclos-sso-test-hydra-consent-$DOCKER_NAME_EXTENSION > /dev/null 2>&1
  docker kill nuclos-sso-test-hydra-$DOCKER_NAME_EXTENSION > /dev/null 2>&1
  docker kill nuclos-sso-test-hydra-db-$DOCKER_NAME_EXTENSION > /dev/null 2>&1
}

function launch_hydra() {
  cd "$WORKDIR"

  kill_hydra

  # try build 3 times
                docker build -t nuclos-sso-test-hydra-db:$DOCKER_VERSION hydra-db || \
  { sleep 60 && docker build -t nuclos-sso-test-hydra-db:$DOCKER_VERSION hydra-db; } || \
  { sleep 60 && docker build -t nuclos-sso-test-hydra-db:$DOCKER_VERSION hydra-db || exit 1; }
  docker run \
    -p $HYDRA_DB_PORT:5432 \
    --name nuclos-sso-test-hydra-db-$DOCKER_NAME_EXTENSION \
    --rm -d \
    nuclos-sso-test-hydra-db:$DOCKER_VERSION || exit 2

  # wait for hydra db
  timeout 60 bash -c "until echo > /dev/tcp/$HYDRA_HOSTIP/$HYDRA_DB_PORT; do sleep 2; done"
  sleep 15

  # try build 3 times
                docker build -t nuclos-sso-test-hydra:$DOCKER_VERSION hydra || \
  { sleep 60 && docker build -t nuclos-sso-test-hydra:$DOCKER_VERSION hydra; } || \
  { sleep 60 && docker build -t nuclos-sso-test-hydra:$DOCKER_VERSION hydra || exit 3; }
  docker run \
    -p $HYDRA_AUTH_PORT:4444 -p $HYDRA_ADMIN_PORT:4445\
    --env CALLBACKS=$NUCLOS_CALLBACK \
    --env POST_LOGOUT_CALLBACKS=$NUCLOS_LOGOUT_CALLBACK \
    --env HYDRA_HOST=$HYDRA_HOSTNAME \
    --env HYDRA_AUTH_PORT=$HYDRA_AUTH_PORT \
    --env HYDRA_ADMIN_PORT=$HYDRA_ADMIN_PORT \
    --env TTL_ACCESS_TOKEN=$TTL_ACCESS_TOKEN \
    --env HYDRA_DB_PORT=$HYDRA_DB_PORT \
    --env HYDRA_ADMIN_URL=$HYDRA_ADMIN_URL \
    --env URLS_SELF_ISSUER=$URLS_SELF_ISSUER \
    --env URLS_CONSENT=$URLS_CONSENT \
    --env URLS_LOGIN=$URLS_LOGIN \
    --env URLS_LOGOUT=$URLS_LOGOUT \
    --name nuclos-sso-test-hydra-$DOCKER_NAME_EXTENSION \
    --add-host $HYDRA_HOSTNAME:$HYDRA_HOSTIP \
    --rm -d \
    nuclos-sso-test-hydra:$DOCKER_VERSION || exit 4

  # wait for hydra
  timeout 60 bash -c "until echo > /dev/tcp/$HYDRA_HOSTIP/$HYDRA_ADMIN_PORT; do sleep 2; done"
  sleep 15

  # try build 3 times
                docker build -t nuclos-sso-test-hydra-consent:$DOCKER_VERSION hydra-consent || \
  { sleep 60 && docker build -t nuclos-sso-test-hydra-consent:$DOCKER_VERSION hydra-consent; } || \
  { sleep 60 && docker build -t nuclos-sso-test-hydra-consent:$DOCKER_VERSION hydra-consent || exit 5; }
  docker run \
    -p $HYDRA_CONSENT_PORT:3000 \
    --env HYDRA_ADMIN_URL=$HYDRA_ADMIN_URL \
    --name nuclos-sso-test-hydra-consent-$DOCKER_NAME_EXTENSION \
    --add-host $HYDRA_HOSTNAME:$HYDRA_HOSTIP \
    --rm -d \
    nuclos-sso-test-hydra-consent:$DOCKER_VERSION || exit 6
}

if [ "$COMMAND" == "launch" ]; then
  launch_hydra
fi

if [ "$COMMAND" == "kill" ]; then
  kill_hydra
fi
