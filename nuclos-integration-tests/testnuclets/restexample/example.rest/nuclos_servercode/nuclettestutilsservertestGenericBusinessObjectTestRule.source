package nuclet.test.utils.servertest;

import java.math.BigDecimal;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.LogContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;

import example.rest.Article;
import example.rest.Order;
import example.rest.OrderPosition;
import nuclet.test.utils.ServerTest;
import nuclet.test.utils.runServerTest;

@Rule(name="GenericBusinessObjectTestRule", description="GenericBusinessObjectTestRule")
public class GenericBusinessObjectTestRule {

	public static void run(runServerTest server, ServerTest serverTest, LogContext log) {
		String sDocumentFileTestcase = serverTest.getTestcase().replaceAll("genericBusinessObject", "");
		try {
			if ("WithDependents".equals(sDocumentFileTestcase)) {
				// Über eine Rule die rein auf Generic Businessobjekte aufbaut soll die Nummer aus den Generic Dependents verwendet werden.
				Article article = new Article();
				article.setName("WithDependents");
				article.setArticleNumber(0);
				article.setPrice(BigDecimal.TEN);
				article.setActive(true);
				Long articleId = BusinessObjectProvider.insert(article);

				Order order = new Order();
				OrderPosition pos = new OrderPosition();
				pos.setName("123");
				pos.setArticleId(articleId);
				pos.setPrice(BigDecimal.TEN);
				pos.setQuantity(BigDecimal.ONE);
				pos.setShipped(false);
				order.insertOrderPosition(pos);
				final Long orderId = BusinessObjectProvider.insert(order);
				final Order orderById = QueryProvider.getById(Order.class, orderId);
				if (orderById.getOrderNumber().equals(123)) {
					serverTest.setOk(true);
				} else {
					throw new BusinessException("Generic business object rule 'example.rest.NumberableRule' failed.");
				}

			} else if ("WithAttachments".equals(sDocumentFileTestcase)) {
                try {
	                Order order = new Order();
	                order.setOrderNumber(1234);
	                final Long orderId = BusinessObjectProvider.insert(order);
	                
	                //first call of update rule inserts attachment if not present, otherwise attachment is going to be deleted
	                Order orderDb = QueryProvider.getById(Order.class, orderId);
	                orderDb.setOrderNumber(orderDb.getOrderNumber());
	                orderDb.save();
	                
	                //second call deletes attachment if present otherwise inserts it
	                orderDb = QueryProvider.getById(Order.class, orderId);
	                orderDb.setOrderNumber(orderDb.getOrderNumber());
	                orderDb.save();

                    serverTest.setOk(true);
                } catch (BusinessException e) {
					throw new BusinessException("Generic business object rule 'example.rest.NumberableRule' failed.");
				}
                
            } else {
				throw new RuntimeException("Unknown genericBusinessObject Testcase " + sDocumentFileTestcase);
			}
		} catch (Exception e) {
			server.logError(e, serverTest, GenericBusinessObjectTestRule.class.getCanonicalName(), sDocumentFileTestcase, log);
		}
	}
}
