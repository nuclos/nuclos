package nuclet.test.other;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.exception.BusinessException;

/** @name
 * @description
 * @usage
 * @change
 */
@Rule(name="TestBoProxyProxyImpl", description="TestBoProxyProxyImpl")
public class TestBOProxyProxyImpl implements nuclet.test.other.TestBOProxyProxy {

	private static final List<TestBOProxy> lstValues = new ArrayList<>();

	static {
		for (int i = 1; i <= 5000; i++) {
			TestBOProxy bo = new TestBOProxy();
			bo.setName("test" + i);
			bo.setId(Long.valueOf(i));
			lstValues.add(bo);
		}
	}

	public void commit() {
	}

	public void delete(java.lang.Long id) throws org.nuclos.api.exception.BusinessException {
		lstValues.remove(getById(id));
	}

	public List<TestBOProxy> getAll() {
		return lstValues;
	}

	public <SE> List<TestBOProxy> getAll(SearchExpression<SE> se, Long limit, Long offset, Map<Attribute, Boolean> sortingOrders) {
		boolean reverseList = false;

		if (!sortingOrders.isEmpty()) {
			for (Attribute attribute : sortingOrders.keySet()) {
				//ordered by PK in descending order
				if ((attribute.getEntityUid() + "0").equals(attribute.getAttributeUid()) && !sortingOrders.get(attribute)) {
					reverseList = true;
				}
			}

		}

		return !reverseList
				? lstValues.subList(offset.intValue(), Math.min(lstValues.size(), offset.intValue() + limit.intValue()))
				: IntStream.range(0, lstValues.size())
					.map(i -> lstValues.size() - 1 - i)
					.mapToObj(lstValues::get)
					.collect(Collectors.toList())
					.subList(offset.intValue(), Math.min(lstValues.size(), offset.intValue() + limit.intValue()));
	}

	public List<TestBOProxy> getAll(Long limit, Long offset, Map<Attribute, Boolean> sortingOrders) {
		return getAll(null, limit, offset, sortingOrders);
	}

	public List<java.lang.Long> getAllIds(){
		return null;
	}

	public TestBOProxy getById(java.lang.Long id) {
		Optional<TestBOProxy> elem =
				lstValues.stream().filter(testBOProxy -> id.equals(testBOProxy.getId())).findFirst();
		return elem.isPresent() ? elem.get() : null;
	}

	public void insert(TestBOProxy testBOProxy) throws org.nuclos.api.exception.BusinessException {
		this.insertWithId(testBOProxy);
	}

	@Override
	public Object insertWithId(final TestBOProxy testBOProxy) throws BusinessException {
		testBOProxy.setId(Long.valueOf(lstValues.size() + 1));
		lstValues.add(testBOProxy);
		return testBOProxy.getId();
	}

	public void rollback() {
	}

	public void setUser(org.nuclos.api.User user) {
	}

	public void update(TestBOProxy testBOProxy) throws org.nuclos.api.exception.BusinessException {
		nuclet.test.other.TestBOProxy boProxy = getById(testBOProxy.getId());
		boProxy.setName(testBOProxy.getName());
		boProxy.setNumber(testBOProxy.getNumber());
		boProxy.setDate(testBOProxy.getDate());
	}

	public Long count() {
		return Long.valueOf(lstValues.size());
	}

	public <SE> Long count(SearchExpression<SE> se) {
		return Long.valueOf(lstValues.size());
	}
}
