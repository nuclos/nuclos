package nuclet.test.other;

import java.lang.reflect.Method;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.ParameterProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.common.UID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.nuclos.rest.ApiException;
import de.nuclos.rest.models.DebugSql;
import de.nuclos.rest.models.LoginInfo;
import de.nuclos.rest.models.LoginParams;
import de.nuclos.rest.models.Serverstatus;
import example.rest.NuclosRESTAdapter;
import nuclet.test.utils.ServerTest;

/**
 * @name
 * @description
 * @usage
 * @change
 */
@Rule(name = "TestRestAPIsRule", description = "TestRestAPIsRule")
public class TestRestAPIsRule implements InsertRule {

	public static void run(final ServerTest serverTest) {
		StringBuffer sbExceptions = new StringBuffer();

		try {
			@SuppressWarnings("unchecked") Class<? extends Modifiable<UID>> cNuclet = (Class<? extends Modifiable<UID>>) Class.forName("org.nuclos.businessentity.Nuclet");
			Method mGetNucletPackage = cNuclet.getDeclaredMethod("getPackage");

			@SuppressWarnings("unchecked") Class<? extends Modifiable<UID>> cRestApis = (Class<? extends Modifiable<UID>>) Class.forName("org.nuclos.businessentity.Restapis");
			Method mGetNuclet = cRestApis.getDeclaredMethod("getNucletBO");
			Method mGetName = cRestApis.getDeclaredMethod("getName");
			QueryProvider.execute(QueryProvider.create(cRestApis))
					.forEach(restapis -> {
						try {
							final Object nuclet = mGetNuclet.invoke(restapis);
							String nucletPackage = nuclet != null ? (String) mGetNucletPackage.invoke(nuclet) : "org.nuclos.restapis";
							String name = (String) mGetName.invoke(restapis);

							Class.forName(nucletPackage + "." + name + "RESTAdapter");
						} catch (Exception ex) {
							sbExceptions.append(ex.getMessage());
						}
					});
		} catch (Exception ex) {
			sbExceptions.append(ex.getMessage());
		}

		if (sbExceptions.length() > 0) {
			serverTest.setException(sbExceptions.toString());
		} else {
			serverTest.setOk(true);
		}
	}

	public void insert(InsertContext context) throws BusinessException {
		RestAPIsTest testBO = context.getBusinessObject(RestAPIsTest.class);

		String restAPIsBasePath = ParameterProvider.getNucletParameter(TestOtherNucletParameter.RestAPIsBasePath);
		if (StringUtils.isBlank(restAPIsBasePath)) {
			restAPIsBasePath = "http://localhost/nuclos";
		}
		final NuclosRESTAdapter nuclosRESTAdapter = NuclosRESTAdapter.newInstance(restAPIsBasePath);
		try {
			StringBuilder sb = new StringBuilder();
			final Serverstatus serverstatus = nuclosRESTAdapter.getNuclosSystemApi().restServerstatusGet();
			sb.append(new ObjectMapper().writeValueAsString(serverstatus));
			sb.append("\n");

			//LOGIN
			final LoginParams loginParams = new LoginParams();
			loginParams.setUsername("nuclos");
			loginParams.setPassword("");
			final LoginInfo loginInfo = nuclosRESTAdapter.getNuclosAuthenticationApi().login1(loginParams);
			final String sessionId = loginInfo.getSessionId();
			nuclosRESTAdapter.getApiClient().addDefaultCookie("JSESSIONID", sessionId);

			DebugSql debugSQL = new DebugSql();
			debugSQL.setDebugSQL("false");
			nuclosRESTAdapter.getNuclosMaintenanceApi().setDebugSQL(debugSQL);

			debugSQL = nuclosRESTAdapter.getNuclosMaintenanceApi().getDebugSQL();
			sb.append(new ObjectMapper().writeValueAsString(debugSQL));
			sb.append("\n");

			debugSQL = new DebugSql();
			debugSQL.setDebugSQL("true");
			nuclosRESTAdapter.getNuclosMaintenanceApi().setDebugSQL(debugSQL);
			debugSQL = nuclosRESTAdapter.getNuclosMaintenanceApi().getDebugSQL();
			sb.append(new ObjectMapper().writeValueAsString(debugSQL));
			sb.append("\n");

			testBO.setResult(sb.toString());
		} catch (ApiException | JsonProcessingException e) {
			throw new BusinessException(e);
		}
	}
}
