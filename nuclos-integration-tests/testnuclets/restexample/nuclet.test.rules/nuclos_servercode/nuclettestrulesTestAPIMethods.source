package nuclet.test.rules;

import example.rest.Order;
import org.apache.commons.lang.StringUtils;
import org.nuclos.api.NuclosImage;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.common.UID;
import org.nuclos.system.*;
import org.nuclos.system.Process;

import javax.swing.plaf.nimbus.State;
import java.util.Date;

/**
 * @name
 * @description
 * @usage
 * @change
 */
@Rule(name = "Test API Methods", description = "Test API Methods")
public class TestAPIMethods implements CustomRule {

	public void custom(final CustomContext context) throws BusinessException {
		doIt(context.getBusinessObject(TestAPI.class));
	}

	private void doIt(TestAPI bo) {
		// Test attribute getter and setter methods
		bo.setTest("test");
		assert StringUtils.equals(bo.getTest(), "test");

		// Attribute name "123"
		bo.set123("123");
		assert StringUtils.equals(bo.get123(), "123");

		// Attribute name "_a"
		bo.setA("a");
		assert StringUtils.equals(bo.getA(), "a");

		// Attribute name "under_score"
		bo.setUnderscore("under_score");
		assert StringUtils.equals(bo.getUnderscore(), "under_score");

		// Attribute name " x"
		bo.setX("x");
		assert StringUtils.equals(bo.getX(), "x");

		bo.setRefuserId(new UID("nuclos1000"));
		assert bo.getRefuserId().equals(new UID("nuclos1000"));
		assert StringUtils.equals(bo.getRefuser(), "nuclos");

		bo.setGettersettermethods("ok");

		testForeignKeyAttributeUID(bo);
	}

	/**
	 * Tests if ForeignKeyAttribute fields of generated BO classes are correctly typed
	 * with org.nuclos.api.UID.
	 * See http://support.nuclos.de/browse/NUCLOS-5762
	 *
	 * @param bo
	 */
	private void testForeignKeyAttributeUID(TestAPI bo) {
		org.nuclos.api.UID apiUID = new UID("Test");
		SearchExpression<org.nuclos.api.UID> searchExpression = TestAPI.RefuserId.eq(apiUID);

		assert searchExpression != null;

		bo.setForeignkeyattribute("ok");
	}

	/**
	 * Not runnable, only compile test
	 */
	private void testInternalSystemBoClassesForBackwardCompatibility() {
		{
			Mandator mandator = Mandator.get(null);
			org.nuclos.api.UID id = mandator.getId();
			String color = mandator.getColor();
			String name = mandator.getName();
			org.nuclos.api.UID parentMandatorId = mandator.getParentMandatorId();
			String path = mandator.getPath();
			String nuclosTitle = mandator.getNuclosTitle();
		}
		{
			PrintService printService = PrintService.get(null);
			org.nuclos.api.UID defaultTrayId = printService.getDefaultTrayId();
			String description = printService.getDescription();
			String name = printService.getName();
			Boolean useNativePdfSupport = printService.getUseNativePdfSupport();
			org.nuclos.api.UID id = printService.getId();
			String nuclosTitle = printService.getNuclosTitle();
		}
		{
			PrintServiceTray printServiceTray = PrintServiceTray.get(null);
			String description = printServiceTray.getDescription();
			Integer number = printServiceTray.getNumber();
			org.nuclos.api.UID printServiceId = printServiceTray.getPrintServiceId();
			org.nuclos.api.UID id = printServiceTray.getId();
			String nuclosTitle = printServiceTray.getNuclosTitle();
		}
		{
			Process process = Process.get(null);
			String description = process.getDescription();
			UID businessObjectId = process.getBusinessObjectId();
			String name = process.getName();
			UID nucletId = process.getNucletId();
			Date validFrom = process.getValidFrom();
			Date validUntil = process.getValidUntil();
			org.nuclos.api.UID id = process.getId();
			String nuclosTitle = process.getNuclosTitle();
		}
		{
			Role role = Role.get(null);
			String description = role.getDescription();
			String name = role.getName();
			UID nucletId = role.getNucletId();
			org.nuclos.api.UID parentRoleId = role.getParentRoleId();
			org.nuclos.api.UID id = role.getId();
			String nuclosTitle = role.getNuclosTitle();
		}
		{
			RoleUser roleUser = RoleUser.get(null);
			org.nuclos.api.UID roleId = roleUser.getRoleId();
			org.nuclos.api.UID userId = roleUser.getUserId();
			org.nuclos.api.UID id = roleUser.getId();
			String nuclosTitle = roleUser.getNuclosTitle();
		}
		{
			Status status = Status.get(null);
			org.nuclos.api.UID buttonIconId = status.getButtonIconId();
			String buttonResource = status.getButtonResource();
			String color = status.getColor();
			String description = status.getDescription();
			String descriptionResource = status.getDescriptionResource();
			NuclosImage icon = status.getIcon();
			String labelResource = status.getLabelResource();
			org.nuclos.api.UID modelId = status.getModelId();
			String name = status.getName();
			Integer numeral = status.getNumeral();
			String tab = status.getTab();
			org.nuclos.api.UID id = status.getId();
			String nuclosTitle = status.getNuclosTitle();
		}
		{
			// interface NuclosUser
			NuclosUser user = User.get(null);
		}
	}
}
