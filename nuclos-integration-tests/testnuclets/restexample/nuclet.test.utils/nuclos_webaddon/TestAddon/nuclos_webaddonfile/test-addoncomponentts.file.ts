import { Component, Inject, OnDestroy, OnInit } from '@angular/core';

import { TestAddonService } from './test-addon.service';
import {
	DASHBOARD_CONTEXT, AddonContext
} from '@nuclos/nuclos-addon-api';

@Component({
	selector: 'nuc-addon-test-addon',
	styles: [`
        :host {
			display: block;
			width: 100%;
			height: 100%;
			border: 1px dashed lightskyblue;
			text-align: center;
        }
	`],
	template: `
		<div>
			Addon TestAddon works.
			<br>
			<div>
				myBoolean property: <b>{{dashboardContext.getAddonProperty('myBoolean')}}</b>
			</div>
			<div>
				myInteger property: <b>{{dashboardContext.getAddonProperty('myInteger')}}</b>
			</div>
			<div>
				myString property: <b>{{dashboardContext.getAddonProperty('myString')}}</b>
			</div>
		</div>
	`
})
export class TestAddonComponent implements OnInit {

	constructor(
		@Inject(DASHBOARD_CONTEXT) public dashboardContext: AddonContext
	) {
	}

	ngOnInit() {
	}

}
