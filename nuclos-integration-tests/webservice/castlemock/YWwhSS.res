<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<soapMockResponse>
    <body>&lt;soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"&gt;&#13;
	&lt;soapenv:Header/&gt;&#13;
	&lt;soapenv:Body&gt;&#13;
		&lt;ns1:sayHello xmlns:ns1="urn:examples:helloservice"&gt;&#13;
			&lt;greeting&gt;hello from castle mock&lt;/greeting&gt;&#13;
		&lt;/ns1:sayHello&gt;&#13;
	&lt;/soapenv:Body&gt;&#13;
&lt;/soapenv:Envelope&gt;</body>
    <contentEncodings/>
    <httpHeaders/>
    <httpStatusCode>200</httpStatusCode>
    <id>YWwhSS</id>
    <name>Auto-generated mocked response</name>
    <operationId>dCMkPS</operationId>
    <status>ENABLED</status>
    <usingExpressions>false</usingExpressions>
    <xpathExpressions/>
</soapMockResponse>
