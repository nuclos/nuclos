package org.nuclos.common2;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.nuclos.common.RigidUtils;

/**
 * Hilfsmethoden fuer JAXB-Marshalling und -Unmarshalling
 */
public class JaxbMarshalUnmarshalUtil {

	public static <T> T unmarshal(
			String xml,
			Class<T> clss
	) throws JAXBException, XMLStreamException {

		InputStream stream = new ByteArrayInputStream(xml.getBytes());
		return unmarshal(stream, clss);
	}

	public static <T> T unmarshal(
			InputStream xmlStream,
			Class<T> clss
	) throws JAXBException, XMLStreamException {

		JAXBContext jaxbContext = JAXBContext.newInstance(clss);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		final XMLStreamReader xmlStreamReader = RigidUtils.newSecuredXMLInputFactory().createXMLStreamReader(xmlStream);
		return (T) jaxbUnmarshaller.unmarshal(xmlStreamReader);
	}

}
