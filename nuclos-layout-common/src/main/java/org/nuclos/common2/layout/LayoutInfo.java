package org.nuclos.common2.layout;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import org.nuclos.common.UID;

public class LayoutInfo {
	private final UID layoutUID;
	private final String sLayoutML;
	private final boolean restrictionsMustIgnoreGroovyRules;
	private final Collection<UID> entities;
	private final boolean doesLayoutChangesWithProcess;
	
	public LayoutInfo(UID layoutUID, String sLayoutML, boolean restrictionsMustIgnoreGroovyRules, Collection<UID> entities,
			boolean doesLayoutChangesWithProcess) {
		this.layoutUID = layoutUID;
		this.sLayoutML = sLayoutML;
		this.restrictionsMustIgnoreGroovyRules = restrictionsMustIgnoreGroovyRules;
		this.entities = entities;
		this.doesLayoutChangesWithProcess = doesLayoutChangesWithProcess;
	}

	public UID getLayoutUID() {
		return layoutUID;
	}

	public String getLayoutML() {
		return sLayoutML;
	}
	
	public boolean isRestrictionsMustIgnoreGroovyRules() {
		return restrictionsMustIgnoreGroovyRules;
	}
	
	public Collection<UID> getEntities() {
		return Collections.unmodifiableCollection(entities);
	}
	
	public boolean doesLayoutChangesWithProcess() {
		return doesLayoutChangesWithProcess;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LayoutInfo) {
			LayoutInfo that = (LayoutInfo) obj;
			return Objects.equals(layoutUID, that.layoutUID) &&
					Objects.equals(restrictionsMustIgnoreGroovyRules, that.restrictionsMustIgnoreGroovyRules);
			
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return Arrays.hashCode(new Object[]{layoutUID, restrictionsMustIgnoreGroovyRules});
	}
	
}
