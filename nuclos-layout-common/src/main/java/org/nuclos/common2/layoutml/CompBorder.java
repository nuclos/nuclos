package org.nuclos.common2.layoutml;

import org.nuclos.common2.layoutml.LayoutMLConstants;

public abstract class CompBorder implements LayoutMLConstants {
	
	public abstract String getType();
	
	@Override
	public String toString() {
		return "Type:" + getType();
	}

}
