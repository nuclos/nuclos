package org.nuclos.common2.layoutml;

import org.xml.sax.Attributes;

public class EtchedBevelBorder extends CompBorder {
	private final String maintype;
	private final String subtype;
	
	public EtchedBevelBorder(String maintype, Attributes attributes) {
		this.maintype = maintype;
		this.subtype = attributes.getValue(ATTRIBUTE_TYPE);
	}
	
	public String getSubType() {
		return subtype;
	}
	
	@Override
	public String getType() {
		return maintype + subtype;
	}

}
