package org.nuclos.common2.layoutml;

public interface IHasTableLayout {
	
	WebTableLayout getWebTableLayout();

	void setWebTableLayout(WebTableLayout webTableLayout);
	
	void setCompBorder(CompBorder compBorder);

	String getBorderType();

	void setBackground(String background);
	String getBackground();
	
}
