package org.nuclos.common2.layoutml;

public interface ISupportsValueListProvider {
	WebValueListProvider getValueListProvider();
	
	void setValueListProvider(WebValueListProvider valueListProvider);
}
