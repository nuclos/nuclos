package org.nuclos.common2.layoutml;

import java.util.List;

public interface IWebContainer extends IWebComponent {
	
	void addComponent(AbstractWebComponent comp);
	
	List<AbstractWebComponent> getComponents();
	
	void liftGrandChildren();
	
	String getTitle();
	
	String getType();
	
}
