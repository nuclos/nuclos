//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2.layoutml;

import java.awt.Dimension;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Logger;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.XMLUtils;
import org.nuclos.common2.layout.LayoutInfo;
import org.nuclos.common2.layoutml.NuclosRuleAction.NuclosRuleClearAction;
import org.nuclos.common2.layoutml.NuclosRuleAction.NuclosRuleRefreshValueListAction;
import org.nuclos.common2.layoutml.NuclosRuleAction.NuclosRuleReinitSubformAction;
import org.nuclos.common2.layoutml.NuclosRuleAction.NuclosRuleTransferLookedUpValueAction;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.common2.layoutml.exception.LayoutMLParseException;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
/**
 * Parser for the LayoutML. Provides methods to validate a LayoutML document and to extract
 * the names of the collectable nonCloneableFields used in the document.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */

public class LayoutMLParser implements LayoutMLConstants {
	
	private static final Logger LOG = Logger.getLogger(LayoutMLParser.class);
	
	protected static final UID getEntityUID(String entity) {
		if (entity == null) {
			return null;
		}
		if (UID.isStringifiedUID(entity)) {
			return UID.parseUID(entity);
		} else {
			LOG.debug(String.format("Found unstringified entityUID \"%s\" in layout", entity));
			return null;
		}
	}
	
	protected static final UID getFieldUID(String field) {
		if (StringUtils.looksEmpty(field)) {
			return null;
		}
		final UID fieldUID;
		if (UID.isStringifiedUID(field)) {
			fieldUID = UID.parseUID(field);
		} else {
			fieldUID = null;
			LOG.warn(String.format("Found unstringified fieldUID \"%s\" in layout", field));
		}
		return fieldUID;
	}
	
	public LayoutMLParser() {
	}
	
	/**
	 * validates the LayoutML document in <code>inputsource</code>.
	 * @param sLayoutML
	 * @throws org.nuclos.common2.layoutml.exception.LayoutMLParseException when a parse exception occurs.
	 * @throws org.nuclos.common2.layoutml.exception.LayoutMLException when a general exception occurs.
	 * @throws java.io.IOException when an I/O error occurs (rather fatal)
	 */
	public void validate(String sLayoutML) throws LayoutMLException, IOException {
		final BasicHandler handler = new BasicHandler();
		try {
			this.parse(sLayoutML, handler);
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex);
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}

	public Dimension calculateMinimumPanelSize(String sLayoutML) throws LayoutMLException {
		GetTableLayoutHandler handler = new GetTableLayoutHandler();
		try {
			this.parse(sLayoutML, handler);
			List<TableLayout> tableLayouts = handler.getValues();
			for (TableLayout layout : tableLayouts) {
				BigDecimal sumColSizes = handler.sumOfNaturalSizes(layout.columns);
				BigDecimal sumRowSizes = handler.sumOfNaturalSizes(layout.rows);
				if (sumColSizes.compareTo(BigDecimal.ZERO) > 0 && sumRowSizes.compareTo(BigDecimal.ZERO) > 0) {
					return new Dimension(sumColSizes.intValue(), sumRowSizes.intValue());
				}
			}
			return null;
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex);
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}

	/**
	 * retrieves the names of the collectable nonCloneableFields used in the LayoutML
	 * document in <code>inputsource</code>.
	 * 
	 * @param sLayoutML
	 * @return Set&lt;UID&gt; the UIDs of the collectable nonCloneableFields used in the
	 * 		LayoutML document in <code>inputsource</code>.
	 * @throws LayoutMLException when a general exception occurs.
	 */
	public Set<UID> getCollectableFieldUids(String sLayoutML) throws LayoutMLException {
		return this.getElementUids(sLayoutML, LayoutMLConstants.ELEMENT_COLLECTABLECOMPONENT, LayoutMLConstants.ATTRIBUTE_NAME);
	}

	/**
	 * retrieves the names of the subform entities used in the LayoutML 
	 * document in <code>inputsource</code>.
	 * 
	 * @param sLayoutML
	 * @return Set&lt;UID&gt; the names of the subform entities used in the 
	 * 		LayoutML document in <code>inputsource</code>.
	 * @throws LayoutMLException when a general exception occurs.
	 */
	public Set<UID> getSubFormEntityUids(String sLayoutML) throws LayoutMLException {
		return this.getElementUids(sLayoutML, LayoutMLConstants.ELEMENT_SUBFORM, LayoutMLConstants.ATTRIBUTE_ENTITY);
	}

	/**
	 * retrieves the names of the chart entities used in the LayoutML document 
	 * in <code>inputsource</code>.
	 * 
	 * @param sLayoutML
	 * @return Set&lt;String&gt; the UIDs of the chart entities used in the 
	 * 		LayoutML document in <code>inputsource</code>.
	 * @throws LayoutMLException when a general exception occurs.
	 */
	public Set<UID> getChartEntityUids(String sLayoutML) throws LayoutMLException {
		return this.getElementUids(sLayoutML, LayoutMLConstants.ELEMENT_CHART, LayoutMLConstants.ATTRIBUTE_ENTITY);
	}
	
	/**
	 * retrieves the names of the subform entities along with their foreign 
	 * key nonCloneableFields used in the LayoutML document in <code>inputsource</code>.
	 * 
	 * @param sLayoutML
	 * @return Collection&lt;EntityAndField&gt; the names of the collectable 
	 * 		nonCloneableFields used in the LayoutML document in <code>inputsource</code>.
	 * @throws LayoutMLException when a general exception occurs.
	 */
	public Collection<EntityAndField> getSubFormEntityAndForeignKeyFields(String sLayoutML) throws LayoutMLException {
		final GetSubFormEntityAndForeignKeyFieldHandler handler = new GetSubFormEntityAndForeignKeyFieldHandler();
		try {
			this.parse(sLayoutML, handler);
			return handler.getValues();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex);
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}

	/**
	 * retrieves the names of the subform entities along with their parent 
	 * subform entity used in the LayoutML document in <code>inputsource</code>.
	 */
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntities(String sLayoutML) throws LayoutMLException {
		final GetSubFormEntityAndParentSubEntityFormHandler handler = new GetSubFormEntityAndParentSubEntityFormHandler();
		try {
			this.parse(sLayoutML, handler);
			return handler.getValues();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}
	
	public ParsedLayoutComponents getParsedLayoutComponents(LayoutInfo layoutInfo) throws LayoutMLException {
		final GetWebComponentsHandler handler = new GetWebComponentsHandler(layoutInfo.getLayoutUID(), layoutInfo.isRestrictionsMustIgnoreGroovyRules());
		try {
			this.parse(layoutInfo.getLayoutML(), handler);
			return handler.getParsedLayoutComponents();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}

	public CloneRelevantLayoutElements getNonCloneableLayoutElements(String sLayoutML) throws LayoutMLException {
		final GetCloneRelevantLayoutElementsHandler handler = new GetCloneRelevantLayoutElementsHandler();
		try {
			this.parse(sLayoutML, handler);
			return handler.getCloneRelevantLayoutElements();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}
	
	public Map<UID, List<NuclosRuleEvent>> getRuleEvents(String sLayoutML) throws LayoutMLException {
		final GetRuleEventsHandler handler = new GetRuleEventsHandler();
		try {
			this.parse(sLayoutML, handler);
			return handler.getValues();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}
		
	/**
	 * retrieves the names of the subform entities along with their parent 
	 * subform entity used in the LayoutML document in <code>inputsource</code>.
	 */
	public String replaceButtonArguments(String sLayoutML, UID sEntity, String newArgument, String oldArgument) throws LayoutMLException {
		final GetButtonArgumentHandler handler = new GetButtonArgumentHandler(sEntity, sLayoutML, newArgument, oldArgument);
		try {
			this.parse(sLayoutML, handler);
			return handler.getLayout();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}

	public TextModuleSettings getTextModuleSettings(String sLayoutML, UID fieldUid, final String controlType) throws LayoutMLException {
		final GetTextModuleHandler handler = new GetTextModuleHandler(fieldUid, controlType);
		try {
			this.parse(sLayoutML, handler);
			return handler.getTextModuleSettings();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex, handler.getDocumentLocator());
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}

	/**
	 * retrieves the names of the collectable nonCloneableFields used in the LayoutML document in <code>inputsource</code>.
	 * @param sAttribute
	 * @param sLayoutML
	 * @return the names of the collectable nonCloneableFields used in the LayoutML document in <code>inputsource</code>.
	 * @throws org.nuclos.common2.layoutml.exception.LayoutMLParseException when a parse exception occurs.
	 * @throws org.nuclos.common2.layoutml.exception.LayoutMLException when a general exception occurs.
	 * @throws org.nuclos.common2.exception.CommonFatalException when an I/O error occurs.
	 */
	private Set<String> getElementNames(String sLayoutML, String sElement, String sAttribute) throws LayoutMLException {
		final GetElementAttributeHandler handler = new GetElementAttributeHandler(sElement, sAttribute);
		try {
			this.parse(sLayoutML, handler);
			return handler.getValues();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex);
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}
	
	private Set<UID> getElementUids(String sLayoutML, String sElement, String sAttribute) throws LayoutMLException {
		final GetElementUIDHandler handler = new GetElementUIDHandler(sElement, sAttribute);
		try {
			this.parse(sLayoutML, handler);
			return handler.getValues();
		}
		catch (SAXParseException ex) {
			throw new LayoutMLParseException(ex);
		}
		catch (SAXException ex) {
			throw new LayoutMLException(ex, handler.getDocumentLocator());
		}
	}

	/**
	 * WORKAROUND for memory leak in SAXParser.parse().
	 * @param sLayoutML
	 * @param handler
	 * @throws SAXException
	 */
	protected final void parse(String sLayoutML, final DefaultHandler handler) throws SAXException {
		XMLUtils.parse(sLayoutML, handler, null);
	}

	/**
	 * inner class BasicHandler.
	 */
	protected static class BasicHandler extends DefaultHandler {
		private Locator locator;

		/**
		 * This method is called by the SAX parser to provide a locator for this handler.
		 * @param locator
		 * @see #getDocumentLocator()
		 */
		@Override
		public void setDocumentLocator(Locator locator) {
			this.locator = locator;
		}

		/**
		 * @return a Locator reflecting the current location of the parser within the document. Should be non-null,
		 * but that depends on the SAX parser so don't rely on it.
		 */
		public Locator getDocumentLocator() {
			return this.locator;
		}

		/**
		 * called by the underlying SAX parser when a "regular" error occurs.
		 * Just throws <code>ex</code>, so the parser can take correspondent actions.
		 * @param ex
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void error(SAXParseException ex) throws SAXException {
			throw ex;
		}

		/**
		 * called by the underlying SAX parser when a fatal error occurs.
		 * This method just calls its super method.
		 * @param ex
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void fatalError(SAXParseException ex) throws SAXException {
			super.fatalError(ex);
		}

		protected static Boolean getBooleanValue(Attributes attributes, String sAttributeName) {
			return Boolean.TRUE.equals(getBooleanValueNullable(attributes, sAttributeName));
		}

		protected static Boolean getBooleanValueNullable(Attributes attributes, String sAttributeName) {
			final String bValue = attributes.getValue(sAttributeName);
			if (bValue != null) {
				if (bValue.equals(LayoutMLConstants.ATTRIBUTEVALUE_YES)) {
					return true;
				} else {
					return false;
				}
			}
			return null;
		}

		/**
		 * convenience method: gets an int value from an XML attribute.
		 * @param attributes the XML attributes of an XML element
		 * @param sAttributeName the name of the attribute to query
		 * @param iDefault the default value that is taken if there is no attribute with the given name
		 * @return the int value of the attribute or the default, if no matching attribute was found
		 */
		protected static int getIntValue(Attributes attributes, String sAttributeName, int iDefault) throws SAXException {
			final String sValue = attributes.getValue(sAttributeName);
			try {
				return (sValue == null) ? iDefault : Integer.parseInt(sValue);
			}
			catch (NumberFormatException ex) {
				throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.14", sAttributeName));
					//"Ganze Zahl erwartet f\u00fcr Attribut \"" + sAttributeName + "\".");
			}
		}

		/**
		 * convenience method: gets an Integer value from an XML attribute.
		 * @param attributes the XML attributes of an XML element
		 * @param sAttributeName the name of the attribute to query
		 * @return the Integer value of the attribute or null, if no matching attribute was found
		 */
		protected static Integer getIntegerValue(Attributes attributes, String sAttributeName) throws SAXException {
			final String sValue = attributes.getValue(sAttributeName);
			try {
				return (sValue == null) ? null : new Integer(sValue);
			}
			catch (NumberFormatException ex) {
				throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.14", sAttributeName));
					//"Ganze Zahl erwartet f\u00fcr Attribut \"" + sAttributeName + "\".");
			}
		}

		/**
		 * convenience method: gets a double value from an XML attribute.
		 * @param attributes the XML attributes of an XML element
		 * @param sAttributeName the name of the attribute to query
		 * @param dDefault the default value that is taken if there is no attribute with the given name
		 * @return the double value of the attribute or the default, if no matching attribute was found
		 */
		protected static double getDoubleValue(Attributes attributes, String sAttributeName, double dDefault)
				throws SAXException {
			final String sValue = attributes.getValue(sAttributeName);
			try {
				return (sValue == null) ? dDefault : Double.parseDouble(sValue);
			}
			catch (NumberFormatException ex) {
				throw new SAXException(StringUtils.getParameterizedExceptionMessage("LayoutMLParser.15", sAttributeName));
					//"Dezimalzahl erwartet f\u00fcr Attribut \"" + sAttributeName + "\".");
			}
		}


	}  // inner class BasicHandler

	/**
	 * inner class GetElementAttributeHandler.
	 * Gets the values of the given attribute in the given element occuring in the parsed LayoutML definition.
	 */
	private class GetElementAttributeHandler extends BasicHandler {
		private final String sElement;
		private final String sAttribute;

		private final Set<String> stValues = new HashSet<String>();

		GetElementAttributeHandler(String sElement, String sAttribute) {
			this.sElement = sElement;
			this.sAttribute = sAttribute;
		}

		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * Only regards <code>sElement</code> elements and collects their names.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(this.sElement)) {
				this.stValues.add(attributes.getValue(this.sAttribute));
			}
		}

		/**
		 * @return Set<String>
		 */
		public Set<String> getValues() {
			return Collections.unmodifiableSet(this.stValues);
		}

	}  // class GetElementAttributeHandler
	
	/**
	 * inner class GetElementUIDHandler.
	 * Gets the uid values of the given attribute in the given element occuring in the parsed LayoutML definition.
	 */
	private class GetElementUIDHandler extends BasicHandler {
		private final String sElement;
		private final String sAttribute;

		private final Set<UID> stValues = new HashSet<UID>();

		GetElementUIDHandler(String sElement, String sAttribute) {
			this.sElement = sElement;
			this.sAttribute = sAttribute;
		}

		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * Only regards <code>sElement</code> elements and collects their names.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(this.sElement)) {
				this.stValues.add(UID.parseUID(attributes.getValue(this.sAttribute)));
			}
		}

		/**
		 * @return Set<UID>
		 */
		public Set<UID> getValues() {
			return Collections.unmodifiableSet(this.stValues);
		}

	}  // class GetElementAttributeHandler

	private static class TableLayout {
		private final List<BigDecimal> columns;
		private final List<BigDecimal> rows;
		private TableLayout(final List<BigDecimal> columns, final List<BigDecimal> rows) {
			this.columns = columns;
			this.rows = rows;
		}
	}

	private class GetTableLayoutHandler extends BasicHandler {

		List<TableLayout> tableLayouts = new ArrayList<>();

		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_TABLELAYOUT)) {
				final String sColumns = attributes.getValue(LayoutMLConstants.ATTRIBUTE_COLUMNS);
				final String sRows = attributes.getValue(LayoutMLConstants.ATTRIBUTE_ROWS);
				final TableLayout layout = new TableLayout(
						transformStringList(sColumns), transformStringList(sRows));
				this.tableLayouts.add(layout);
			}
		}

		// no percentages and no filling
		private BigDecimal sumOfNaturalSizes(List<BigDecimal> lstTableLayoutSizes) {
			return lstTableLayoutSizes.stream().filter(s -> s.compareTo(BigDecimal.ZERO) >= 1).reduce(BigDecimal.ZERO, BigDecimal::add);
		}

		private List<BigDecimal> transformStringList(String s) {
			return Stream.of(s.split("\\|")).map(t -> new BigDecimal(t.replaceAll(",", ""))).collect(Collectors.toList());
		}

		public List<TableLayout> getValues() {
			return Collections.unmodifiableList(this.tableLayouts);
		}
	}

	/**
	 * inner class GetSubFormEntityAndForeignKeyFieldHandler.
	 */
	private class GetSubFormEntityAndForeignKeyFieldHandler extends BasicHandler {

		private final Set<EntityAndField> stValues = new HashSet<EntityAndField>();

		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_SUBFORM)) {
				final UID entity = getEntityUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_ENTITY));
				final UID foreignKeyField = getFieldUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
				this.stValues.add(new EntityAndField(entity, foreignKeyField));
			}
			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_MULTISELECTION_COMBOBOX)) {
				final UID entity = getEntityUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_ENTITY));
				final UID foreignKeyField = getFieldUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
				this.stValues.add(new EntityAndField(entity, foreignKeyField));
			}
			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_CHART)) {
				final UID entity = getEntityUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_ENTITY));
				final UID foreignKeyField = getFieldUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
				this.stValues.add(new EntityAndField(entity, foreignKeyField));
			}
			if(sQualifiedName.equals(LayoutMLConstants.ELEMENT_MATRIX)) {
				final UID entity = getEntityUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_ENTITY_Y));
				final UID foreignKeyField = getFieldUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_ENTITY_Y_PARENT_FIELD));
				this.stValues.add(new EntityAndField(entity, foreignKeyField));
				final UID uidMatrixEntity = getEntityUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_ENTITY_MATRIX));
				final UID uidMatrixField = getFieldUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_MATRIX_VALUE_FIELD));
				this.stValues.add(new EntityAndField(uidMatrixEntity, uidMatrixField));
			}
		}

		public Set<EntityAndField> getValues() {
			return Collections.unmodifiableSet(this.stValues);
		}

	}  // class GetSubFormEntityAndForeignKeyFieldHandler

	/**
	 * inner class GetSubFormEntityAndSubFormParentHandler.
	 */
	private class GetSubFormEntityAndParentSubEntityFormHandler extends BasicHandler {

		private final Map<EntityAndField, UID> mpValues = new HashMap<EntityAndField, UID>();

		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_SUBFORM)) {
				final UID entity = getEntityUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_ENTITY));
				final UID foreignKeyField = getFieldUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
						
				this.mpValues.put(new EntityAndField(entity, foreignKeyField), 
						attributes.getValue(LayoutMLConstants.ATTRIBUTE_PARENTSUBFORM) == null ? null : getEntityUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_PARENTSUBFORM)));
			}
			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_MATRIX)) {
				final UID matrixEntity = getEntityUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_ENTITY_MATRIX));
				final UID foreignKeyField = getFieldUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_MATRIX_PARENT_FIELD));
				final UID parentSubForm = getEntityUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_ENTITY_Y));
				this.mpValues.put(new EntityAndField(matrixEntity, foreignKeyField), parentSubForm);
			}
		}

		public Map<EntityAndField, UID> getValues() {
			return Collections.unmodifiableMap(this.mpValues);
		}

	}  // class GetSubFormEntityAndSubFormParentHandler
	
	
	/**
	 * inner class GetButtonArgumentHandler.
	 */
	private class GetButtonArgumentHandler extends BasicHandler {

		private static final String PROPERTY_RULE = "ruletoexecute";
		private static final String PROPERTY_STATE = "targetState";
		private static final String PROPERTY_GENERATOR = "generatortoexecute";
		
		private String sLayout;
		private final String sArgNew;
		private final String sArgOld;
		private final String sPropReplacement;
		
		public GetButtonArgumentHandler(UID sEntity, String sLayout, String sArgNew, String sArgOld) {
			this.sLayout = sLayout;
			this.sArgNew = sArgNew;
			this.sArgOld = sArgOld;
			
			if (sEntity.equals(E.SERVERCODE.getUID()))
				sPropReplacement = PROPERTY_RULE;
			else if (sEntity.equals(E.GENERATION.getUID()))
				sPropReplacement = PROPERTY_GENERATOR;
			else if (sEntity.equals(E.STATE.getUID()))
				sPropReplacement = PROPERTY_STATE;
			else
				throw new NuclosFatalException();
		}
		
		public String getLayout() {
			return sLayout;
		}
		
		/**
		 * called by the underlying SAX parser when a start element event occurs.
		 * @param sUriNameSpace
		 * @param sSimpleName
		 * @param sQualifiedName
		 * @param attributes
		 * @throws org.xml.sax.SAXException
		 */
		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_PROPERTY)) {
				final String sName = attributes.getValue(LayoutMLConstants.ATTRIBUTE_NAME);
				if (sName.equals(sPropReplacement)) {
					final String sValue = attributes.getValue(LayoutMLConstants.ATTRIBUTE_VALUE);
					if (sValue.equals(sArgOld)) {
						final String sOld = LayoutMLConstants.ELEMENT_PROPERTY + " " + LayoutMLConstants.ATTRIBUTE_NAME + "=\"" + sPropReplacement + "\" " + LayoutMLConstants.ATTRIBUTE_VALUE + "=\"" + sArgOld + "\"";
						final String sNew = LayoutMLConstants.ELEMENT_PROPERTY + " " + LayoutMLConstants.ATTRIBUTE_NAME + "=\"" + sPropReplacement + "\" " + LayoutMLConstants.ATTRIBUTE_VALUE + "=\"" + sArgNew + "\"";
						
						final String s = sLayout;
						sLayout = s.replaceAll(sOld, sNew);
						if (RigidUtils.equal(sLayout, s)) {
							final StringBuffer sb = new StringBuffer();
							int idx = sLayout.indexOf(sOld);
							sb.append(sLayout.substring(0, idx));
							sb.append(sNew);
							sb.append(sLayout.substring(idx + sNew.length()));
							sLayout = sb.toString();
						}
					}
				}
			}
		}
	}  // class GetButtonArgumentHandler
	
	private static class GetWebComponentsHandler extends BasicHandler {

		private final Map<UID, List<AbstractWebComponent>> mpComps = new HashMap<UID, List<AbstractWebComponent>>();
		private final Map<UID, WebStaticComponent> mpStaticComps = new HashMap<UID, WebStaticComponent>();
		
		private final Stack<AbstractWebComponent> stkComps = new Stack<AbstractWebComponent>();
		private final Stack<IWebContainer> stkContainer = new Stack<IWebContainer>();
		private final Stack<IHasTableLayout> stkHasLayout = new Stack<IHasTableLayout>();
		
		private final Stack<WebSubform> stkSubforms = new Stack<WebSubform>();
		private final Stack<NuclosSubformColumn> stkSubformColumns = new Stack<NuclosSubformColumn>();
		private final Stack<ISupportsValueListProvider> stkSupportsVLPs = new Stack<ISupportsValueListProvider>();
		private final Stack<WebValueListProvider> stkVLPs = new Stack<WebValueListProvider>();

		private final Map<UID, List<NuclosRuleEvent>> mpRuleEvents = new HashMap<UID, List<NuclosRuleEvent>>();
		private NuclosRuleEvent lastRuleEvent;
		
		private final WebRoot webRoot;
		
		private GetWebComponentsHandler(UID layoutUID, boolean restrictionsMustIgnoreGroovyRules) {
			webRoot = new WebRoot(layoutUID);
			webRoot.setRestrictionsMustIgnoreGroovyRules(restrictionsMustIgnoreGroovyRules);
		}

		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_PANEL)) {
				WebPanel webPanel = new WebPanel(attributes);
				addWebComponent(webPanel);
				stkContainer.push(webPanel);
				stkHasLayout.push(webPanel);
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_TABLELAYOUTCONSTRAINTS)) {
				if (!stkComps.empty()) {
					stkComps.peek().setConstraints(new WebTableLayoutConstraints(attributes));
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_MINIMUMSIZE)) {
				if (!stkComps.empty()) {
					stkComps.peek().setSize(IWebConstraints.MINIMUM_SIZE, attributes);
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_PREFERREDSIZE)) {
				if (!stkComps.empty()) {
					stkComps.peek().setSize(IWebConstraints.PREFERED_SIZE, attributes);
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_STRICTSIZE)) {
				if (!stkComps.empty()) {
					stkComps.peek().setSize(IWebConstraints.STRICT_SIZE, attributes);
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_TABLELAYOUT)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setWebTableLayout(new WebTableLayout(attributes));
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_EMPTYBORDER)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setCompBorder(new EmptyBorder(attributes));
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_TITLEDBORDER)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setCompBorder(new TitledBorder(attributes));
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_ETCHEDBORDER)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setCompBorder(new EtchedBevelBorder("etched", attributes));
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_BEVELBORDER)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setCompBorder(new EtchedBevelBorder("bevel", attributes));
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_LINEBORDER)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setCompBorder(new LineBorder(attributes));
				}
												
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_BACKGROUND)) {
				if (!stkHasLayout.empty()) {
					stkHasLayout.peek().setBackground("rgb(" + attributes.getValue(LayoutMLConstants.ATTRIBUTE_RED) + ", " + attributes.getValue(LayoutMLConstants.ATTRIBUTE_GREEN) + ", " + attributes.getValue(LayoutMLConstants.ATTRIBUTE_BLUE) + ")");
					stkComps.peek().setBackground("rgb(" + attributes.getValue(LayoutMLConstants.ATTRIBUTE_RED) + ", " + attributes.getValue(LayoutMLConstants.ATTRIBUTE_GREEN) + ", " + attributes.getValue(LayoutMLConstants.ATTRIBUTE_BLUE) + ")");
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_SPLITPANE)) {
				WebSplitPane splitPane = new WebSplitPane(attributes);
				addWebComponent(splitPane);
				stkContainer.push(splitPane);

			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_SPLITPANECONSTRAINTS)) {
				if (!stkComps.empty()) {
					stkComps.peek().setConstraints(new WebSplitpaneConstraints(attributes));
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_TABBEDPANE)) {
				WebTabbedPane tabbedPane = new WebTabbedPane(attributes);
				addWebComponent(tabbedPane);
				stkContainer.push(tabbedPane);
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_TABBEDPANECONSTRAINTS)) {
				if (!stkComps.empty()) {
					stkComps.peek().setConstraints(new WebTabbedpaneConstraints(attributes));
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_LABEL)) {
				addStaticComponent(sQualifiedName, attributes);
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_TITLEDSEPARATOR)) {
				addStaticComponent(sQualifiedName, attributes);
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_SEPARATOR)) {
				addStaticComponent(sQualifiedName, attributes);
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_BUTTON)) {
				addStaticComponent(sQualifiedName, attributes);
							
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_COLLECTABLECOMPONENT)) {
				WebCollectableComponent collectableComp = new WebCollectableComponent(attributes);
				addWebComponent(collectableComp);
				
				//This is important to be outside of "isJustLabel()"
				stkSupportsVLPs.push(collectableComp);
				
				if (!collectableComp.isJustLabel()) {
					addNuclosComponentToMap(collectableComp.getName(), collectableComp);
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_PLANNINGTABLE)) {
				WebPlanningTable planningTable = new WebPlanningTable(attributes);
				addWebComponent(planningTable);
				if (planningTable.getPlanningTableUID() != null) {
					addNuclosComponentToMap(planningTable.getPlanningTableUID(), planningTable);
				}
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_SUBFORM)) {
				WebSubform nuclosSubform = new WebSubform(attributes);
				addWebComponent(nuclosSubform);
				stkSubforms.push(nuclosSubform);
				if (nuclosSubform.getForeignkeyfield() != null) {
					addNuclosComponentToMap(nuclosSubform.getEntity(), nuclosSubform);					
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_MULTISELECTION_COMBOBOX)) {
				WebMultiselectionCombobox multiselectionCombobox = new WebMultiselectionCombobox(attributes);
				addWebComponent(multiselectionCombobox);
				if (multiselectionCombobox.getForeignkeyfield() != null) {
					addNuclosComponentToMap(multiselectionCombobox.getEntity(), multiselectionCombobox);
				}
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_SUBFORMCOLUMN)) {
				if (!stkSubforms.empty()) {
					WebSubform subform = stkSubforms.peek();
					NuclosSubformColumn subformColumn = new NuclosSubformColumn(attributes, subform.getEntity());
					stkSupportsVLPs.push(subformColumn);
					subform.addSubformColumn(subformColumn);
					stkSubformColumns.push(subformColumn);
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_CHART)) {
				WebChart nuclosChart = new WebChart(attributes);
				addWebComponent(nuclosChart);
				if (nuclosChart.getForeignkeyfield() != null) {
					addNuclosComponentToMap(nuclosChart.getEntity(), nuclosChart);					
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_MATRIX)) {
				WebMatrix matrix = new WebMatrix(attributes);
				if (matrix.getForeignkeyfield() != null) {
					addNuclosComponentToMap(matrix.getUID(), matrix);					
				}
				addWebComponent(matrix);

			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_PROPERTY)) {
				if (!stkComps.empty()) {
					String name = attributes.getValue(LayoutMLConstants.ATTRIBUTE_NAME);
					String value = attributes.getValue(LayoutMLConstants.ATTRIBUTE_VALUE);
					if (!stkSubformColumns.empty()) {
						stkSubformColumns.peek().addProperty(name, value);
					} else {
						stkComps.peek().addProperty(name, value);					
					}
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_VALUELISTPROVIDER)) {
				if (!stkSupportsVLPs.empty()) {
					ISupportsValueListProvider supportsVLP = stkSupportsVLPs.peek();
					WebValueListProvider valueListProvider = new WebValueListProvider(attributes);
					stkVLPs.push(valueListProvider);
					supportsVLP.setValueListProvider(valueListProvider);
				}
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_PARAMETER)) {
				if (!stkVLPs.empty()) {
					stkVLPs.peek().addParameter(attributes);
				}
				
			} else if (isLayoutGroovyElement(sQualifiedName)) {
				sbChars = new StringBuilder();
			} else {
				lastRuleEvent = parseRules(sQualifiedName, attributes, lastRuleEvent, mpRuleEvents);
			}
		}
		
		private void addStaticComponent(String type, Attributes attributes) {
			WebStaticComponent wsComp = new WebStaticComponent(type, attributes);
			addWebComponent(wsComp);
			mpStaticComps.put(wsComp.getUID(), wsComp);
		}
		
		private void addWebComponent(AbstractWebComponent comp) {
			stkComps.push(comp);
			if (stkContainer.empty()) {
				stkContainer.add(webRoot);
			}
			IWebContainer container = stkContainer.peek();
			container.addComponent(comp);
			comp.setContainer(container);
		}
		
		private void addNuclosComponentToMap(UID key, AbstractWebComponent nuclosComponent) {
			if (!mpComps.containsKey(key)) {
				mpComps.put(key, new ArrayList<AbstractWebComponent>());
			}
			mpComps.get(key).add(nuclosComponent);
		}
		
		@Override
		public void endElement(String sUriNameSpace, String sSimpleName, String sQualifiedName)
				throws SAXException {
			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_PANEL)) {
				stkContainer.pop();
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_SPLITPANE)) {
				stkContainer.pop();
				
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_TABBEDPANE)) {
				stkContainer.pop();

			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_SUBFORMCOLUMN)) {
				stkSubformColumns.pop();
				
			} else if (isLayoutGroovyElement(sQualifiedName)) {
				String reduced = StringUtils.reduceWhiteSpacesAndDeleteOtherNonLetterOrDigit(sbChars.toString());
				if (reduced != null && !reduced.isEmpty()) {
					//NUCLOS-4718
					boolean trivialTrue = "return true".equals(reduced) || "true".equals(reduced);
					boolean trivialFalse = "return false".equals(reduced) || "false".equals(reduced);
					boolean elementEnabled = LayoutMLConstants.ELEMENT_ENABLED.equals(sQualifiedName);
					
					if (trivialTrue && elementEnabled) {
						stkComps.peek().setForceDisable(false);
					} else if (trivialTrue) {
						stkComps.peek().addProperty(StringUtils.reduceWhiteSpacesAndDeleteOtherNonLetterOrDigit(sQualifiedName), "true");
					} else if (trivialFalse && elementEnabled) {
						stkComps.peek().setForceDisable(true);						
					} else if (trivialFalse) {
						stkComps.peek().addProperty(StringUtils.reduceWhiteSpacesAndDeleteOtherNonLetterOrDigit(sQualifiedName), "false");						
					} else if (!webRoot.isRestrictionsMustIgnoreGroovyRules()) {
						stkComps.peek().setForceDisable(true);						
					}
				}
				sbChars = null;
			}
		}
		
		private StringBuilder sbChars;
		@Override
		public void characters(char[] ac, int start, int length) throws SAXException {
			if (this.sbChars != null) {
				this.sbChars.append(ac, start, length);
			}
		}

		public ParsedLayoutComponents getParsedLayoutComponents() {
			return new ParsedLayoutComponents(mpComps, mpStaticComps, mpRuleEvents, webRoot);
		}
		
		private static boolean isLayoutGroovyElement(String sQualifiedName) {
			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_ENABLED)) {
				return true;
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_NEW_ENABLED)) {
				return true;
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_EDIT_ENABLED)) {
				return true;
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_DELETE_ENABLED)) {
				return true;
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_CLONE_ENABLED)) {
				return true;
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_DYNAMIC_ROW_COLOR)) {
				return true;
			}
			return false;
		}
	}  // class GetComponentsHandler

	private static class GetCloneRelevantLayoutElementsHandler extends BasicHandler {

		private Set<UID> nonCloneableFields = new HashSet<>();
		private Set<UID> nonCloneableSubformReferences = new HashSet<>();
		private Map<UID, Set<UID>> mpNonCloneableSubformcolumns = new HashMap<>();
		private Set<UID> subformReferences = new HashSet<>();

		private final Stack<UID> stkSubforms = new Stack<>();

		@Override
		public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
			if (qName.equals(LayoutMLConstants.ELEMENT_COLLECTABLECOMPONENT) && LayoutMLConstants.ATTRIBUTEVALUE_CONTROL.equals(attributes.getValue(LayoutMLConstants.ATTRIBUTE_SHOWONLY))) {
				String sNotCloneable = attributes.getValue(LayoutMLConstants.ATTRIBUTE_NOT_CLONEABLE);
				if (sNotCloneable != null && sNotCloneable.equals(LayoutMLConstants.ATTRIBUTEVALUE_YES)) {
					nonCloneableFields.add(UID.parseUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_NAME)));
				}
			} else if (qName.equals(LayoutMLConstants.ELEMENT_SUBFORM)) {
				UID uidSubformReference = UID.parseUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
				stkSubforms.push(uidSubformReference);
				String sNotCloneable = attributes.getValue(LayoutMLConstants.ATTRIBUTE_NOT_CLONEABLE);
				if (sNotCloneable != null && sNotCloneable.equals(LayoutMLConstants.ATTRIBUTEVALUE_YES)) {
					nonCloneableSubformReferences.add(uidSubformReference);
				}
				subformReferences.add(uidSubformReference);
			} else if (qName.equals(LayoutMLConstants.ELEMENT_SUBFORMCOLUMN)) {
				UID uidSubform = stkSubforms.peek();
				if (!nonCloneableSubformReferences.contains(uidSubform)) {
					String sNotCloneable = attributes.getValue(LayoutMLConstants.ATTRIBUTE_NOT_CLONEABLE);
					if (sNotCloneable != null && sNotCloneable.equals(LayoutMLConstants.ATTRIBUTEVALUE_YES)) {
						Set<UID> subformcolumns = mpNonCloneableSubformcolumns.get(uidSubform);
						if (subformcolumns == null) {
							subformcolumns = new HashSet<>();
							mpNonCloneableSubformcolumns.put(uidSubform, subformcolumns);
						}
						subformcolumns.add(UID.parseUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_NAME)));
					}
				}
			}
		}

		@Override
		public void endElement(final String uri, final String localName, final String qName) throws SAXException {
			if (qName.equals(LayoutMLConstants.ELEMENT_SUBFORM)) {
				stkSubforms.pop();
			}
		}

		public CloneRelevantLayoutElements getCloneRelevantLayoutElements() {
			return new CloneRelevantLayoutElements(nonCloneableFields, nonCloneableSubformReferences, mpNonCloneableSubformcolumns, subformReferences);
		}
	}
	
	private static NuclosRuleEvent parseRules(String sQualifiedName, Attributes attributes,
			NuclosRuleEvent lastRuleEvent, Map<UID, List<NuclosRuleEvent>> mpRuleEvents) {
		if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_RULE)) {
		}  else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_RULES)) {
		}  else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_EVENT)) {
			NuclosRuleEvent ruleEvent = new NuclosRuleEvent(attributes);
			//if the rule isn't just for sub-forms. If this is null, while being a layout-ml rule, it's for the main entity.
			//Null is allowed as key in the HashMap.
			UID subform = ruleEvent.getSubform();
			if (!mpRuleEvents.containsKey(subform)) {
				mpRuleEvents.put(subform, new ArrayList<NuclosRuleEvent>());
			}
			mpRuleEvents.get(subform).add(ruleEvent);
			lastRuleEvent = ruleEvent;
		}  else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_REFRESHVALUELIST)) {
			if (lastRuleEvent != null) {
				lastRuleEvent.addRuleAction(new NuclosRuleRefreshValueListAction(attributes));
			}
		} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_REINIT_SUBFORM)) {
			if (lastRuleEvent != null) {
				lastRuleEvent.addRuleAction(new NuclosRuleReinitSubformAction(attributes));
			}
		} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_CLEAR)) {
			if (lastRuleEvent != null) {
				lastRuleEvent.addRuleAction(new NuclosRuleClearAction(attributes));
			}
		}  else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_TRANSFERLOOKEDUPVALUE)) {
			if (lastRuleEvent != null) {
				lastRuleEvent.addRuleAction(new NuclosRuleTransferLookedUpValueAction(attributes));
			}				
		}
		return lastRuleEvent;
	}
	
	private static class GetRuleEventsHandler extends BasicHandler {

		private final Map<UID, List<NuclosRuleEvent>> mpRuleEvents = new HashMap<UID, List<NuclosRuleEvent>>();
		private NuclosRuleEvent lastRuleEvent;

		@Override
		public void startElement(String sUriNameSpace, String sSimpleName, String sQualifiedName, Attributes attributes)
				throws SAXException {
			lastRuleEvent = parseRules(sQualifiedName, attributes, lastRuleEvent, mpRuleEvents);
		}

		public Map<UID, List<NuclosRuleEvent>> getValues() {
			return Collections.unmodifiableMap(this.mpRuleEvents);
		}
	}  // class GetRulesHandler

	private static class GetTextModuleHandler extends BasicHandler {
		private final UID fieldUid;
		private final String controlType;

		private TextModuleSettings textModuleSettings;
		static boolean fieldControlTypeMatched = false;

		public GetTextModuleHandler(final UID fieldUid, final String controlType) {
			this.fieldUid = fieldUid;
			if ("html-editor".equals(controlType)) {
				this.controlType = "org.nuclos.client.ui.collect.component.CollectableEditorPane";
			} else {
				this.controlType = controlType;
			}
		}

		@Override
		public void startElement(final String uri, final String localName, final String qName,
								 final Attributes attributes) throws SAXException {
			parseTextModuleSettings(fieldUid, controlType, qName, attributes);
		}

		public TextModuleSettings getTextModuleSettings() {
			return this.textModuleSettings;
		}

		private void parseTextModuleSettings(UID fieldUid, final String controlType, String sQualifiedName, Attributes attributes) {

			if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_COLLECTABLECOMPONENT)) {
				fieldControlTypeMatched = fieldUid.equals(UID.parseUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_NAME)))
						&& (controlType == null
								|| controlType.equals(attributes.getValue(LayoutMLConstants.ATTRIBUTE_CONTROLTYPE))
								|| controlType.equals(attributes.getValue(LayoutMLConstants.ATTRIBUTE_CONTROLTYPECLASS)));
			} else if (sQualifiedName.equals(LayoutMLConstants.ELEMENT_TEXTMODULE)) {
				if (fieldControlTypeMatched) {
					final UID textModuleEntity = UID.parseUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_TEXTMODULE_ENTITY));
					final UID textModuleEntityField = UID.parseUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_TEXTMODULE_ENTITY_FIELD));
					final UID textModuleEntityFieldName = UID.parseUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_TEXTMODULE_ENTITY_FIELD_NAME));
					final UID textModuleEntityFieldSort = UID.parseUID(attributes.getValue(LayoutMLConstants.ATTRIBUTE_TEXTMODULE_ENTITY_FIELD_SORT));

					this.textModuleSettings = new TextModuleSettings(textModuleEntity, textModuleEntityFieldName, textModuleEntityField, textModuleEntityFieldSort);
					fieldControlTypeMatched = false;
				}
			}
		};
	}
}  // class LayoutMLParser
