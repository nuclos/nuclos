package org.nuclos.common2.layoutml;

import org.xml.sax.Attributes;

/**
 * Class LineBorder
 * Extends CompBorder
 * @author Oliver Brausch
 *
 * TODO: Thickness and Color isn't used yet, LineBorder will be display same as TitledBorder until implementation
 */
public class LineBorder extends CompBorder {
	private final int thickness;
	
	public LineBorder(Attributes attributes) {
		this.thickness = MyLayoutConstants.getIntValue(attributes, ATTRIBUTE_THICKNESS, 1);
	}
	
	public int getThinkness() {
		return thickness;
	}
	
	@Override
	public String getType() {
		return "line";
	}

}
