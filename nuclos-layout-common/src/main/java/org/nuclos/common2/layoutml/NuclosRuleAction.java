package org.nuclos.common2.layoutml;

import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public abstract class NuclosRuleAction implements LayoutMLConstants {
	private final UID entity;
    private final UID targetComponent;
	
	private NuclosRuleAction(Attributes attributes) {
		entity = LayoutMLParser.getEntityUID(attributes.getValue(ATTRIBUTE_ENTITY));
		targetComponent = LayoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_TARGETCOMPONENT));
	}
	
	private NuclosRuleAction() {
		entity = null;
		targetComponent = null;
	}

	public UID getEntity() {
		return entity;
	}

	public UID getTargetComponent() {
		return targetComponent;
	}
	
	public abstract String getActionType();
	
	public abstract String getParameter();
	
	public abstract boolean willChangeValue();
	
	public static class NuclosRuleClearAction extends NuclosRuleAction {

		protected NuclosRuleClearAction(Attributes attributes) {
			super(attributes);
		}
		
		@Override
		public String getActionType() {
			return ELEMENT_CLEAR;
		}

		@Override
		public String getParameter() {
			return null;
		}
		
		@Override
		public boolean willChangeValue() {
			return true;
		}
	}

	public static class NuclosRuleReinitSubformAction extends  NuclosRuleAction {

		protected NuclosRuleReinitSubformAction(Attributes atts) {
			super(atts);
		}
		
		@Override
		public String getActionType() {
			return ELEMENT_REINIT_SUBFORM;
		}

		@Override
		public String getParameter() {
			return null;
		}
		
		@Override
		public boolean willChangeValue() {
			return false;
		}
		
	}
	
	public static class NuclosRuleRefreshValueListAction extends NuclosRuleAction {
		private final String parameterNameForSourceComponent;

		protected NuclosRuleRefreshValueListAction(Attributes attributes) {
			super(attributes);
			String sParameterNameForSourceComponent = attributes.getValue(ATTRIBUTE_PARAMETER_FOR_SOURCECOMPONENT);
			if (sParameterNameForSourceComponent == null) {
				sParameterNameForSourceComponent = "relatedId";
			}
			this.parameterNameForSourceComponent = sParameterNameForSourceComponent;
		}

		public String getParameterNameForSourceComponent() {
			return parameterNameForSourceComponent;
		}
				
		@Override
		public String getActionType() {
			return ELEMENT_REFRESHVALUELIST;
		}

		@Override
		public String getParameter() {
			return parameterNameForSourceComponent;
		}
		
		//Note, that this action changes the elements of a drop-down, but NOT the value of it.
		@Override
		public boolean willChangeValue() {
			return false;
		}
	}
	
	public static class NuclosRuleTransferLookedUpValueAction extends NuclosRuleAction {
		private final UID sourcefield;

		protected NuclosRuleTransferLookedUpValueAction(Attributes attributes) {
			super(attributes);
			this.sourcefield = LayoutMLParser.getFieldUID(attributes.getValue(ATTRIBUTE_SOURCEFIELD));
		}

		public UID getSourceField() {
			return sourcefield;
		}
				
		@Override
		public String getActionType() {
			return ELEMENT_TRANSFERLOOKEDUPVALUE;
		}

		@Override
		public String getParameter() {
			return sourcefield.getString();
		}
		
		@Override
		public boolean willChangeValue() {
			return true;
		}
	}
	
	public static class NuclosCheckLayoutChangeAction extends NuclosRuleAction {

		NuclosCheckLayoutChangeAction() {
			super();
		}
		
		@Override
		public String getActionType() {
			return "check-layout-change";
		}

		@Override
		public String getParameter() {
			return null;
		}
		
		@Override
		public boolean willChangeValue() {
			return false;
		}
	}

	@Override
	public String toString() {
		return "ActionType=" + getActionType();
	}
}
