package org.nuclos.common2.layoutml;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;

public class ParsedLayoutComponents {
	public static final ParsedLayoutComponents dummyParsedLayoutComponents = new ParsedLayoutComponents();
	
	private final Map<UID, List<AbstractWebComponent>> mpComponents;
	private final Map<UID, WebStaticComponent> mpStaticComps;
	private final Map<UID, List<NuclosRuleEvent>> mpRuleEvents;
	private final WebRoot webRoot;
	
	public ParsedLayoutComponents(Map<UID, List<AbstractWebComponent>> mpComponents, Map<UID, WebStaticComponent> mpStaticComps, Map<UID, List<NuclosRuleEvent>> mpRuleEvents, WebRoot webRoot) {
		this.mpComponents = mpComponents;
		this.mpStaticComps = mpStaticComps;
		this.mpRuleEvents = mpRuleEvents;
		this.webRoot = webRoot;
	}
	
	private ParsedLayoutComponents() {
		this(new HashMap<UID, List<AbstractWebComponent>>(), new HashMap<UID, WebStaticComponent>(), new HashMap<UID, List<NuclosRuleEvent>>(), new WebRoot(UID.UID_NULL));
	}

	public Map<UID, List<AbstractWebComponent>> getMpComponents() {
		return Collections.unmodifiableMap(mpComponents);
	}
	
	public Map<UID, WebStaticComponent> getMpStaticComps() {
		return Collections.unmodifiableMap(mpStaticComps);
	}

	/*
	 * return Map of all rule events for a layout. Standard ML-Rules are stored under the key "null". Sub-form-Rules under the UID of the sub-form.
	 */
	public Map<UID, List<NuclosRuleEvent>> getMpRuleEvents() {
		return Collections.unmodifiableMap(mpRuleEvents);
	}

	public WebRoot getWebRoot() {
		return webRoot;
	}
	
	@Override
	public String toString() {
		return "Comps:" + mpComponents + " Rules:" + mpRuleEvents + " WRoot:" + webRoot;
	}

}
