package org.nuclos.common2.layoutml;

import java.util.HashMap;
import java.util.Map;

public class WebComponentSizes {
	Map<Integer, WDimension> mpSizes;
	
	public void addSize(int type, int width, int height) {
		if (mpSizes == null) {
			mpSizes = new HashMap<Integer, WDimension>();
		}
		
		mpSizes.put(type, new WDimension(width, height));

	}
	
	public WDimension getWDimension(int type) {
		return mpSizes.get(type);
	}

	public static class WDimension {
		private final int width;
		private final int height;
		
		private WDimension(int width, int height) {
			this.width = width;
			this.height = height;
		}
		
		public int getWidth() {
			return width;
		}
		
		public int getHeight() {
			return height;
		}
	}
	
}
