package org.nuclos.common2.layoutml;

import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public class WebMultiselectionCombobox extends AbstractWebDependents {

	public static final String SERVICE_IDENTIFIER = "dependence_list";
	private UID entityUID;
	private UID foreignKeyfieldToParent;
	private UID labelAttr;

	protected WebMultiselectionCombobox(final Attributes attributes) {
		super(attributes);
		entityUID = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY));
		foreignKeyfieldToParent = UID.parseUID(attributes.getValue(ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
		labelAttr= UID.parseUID(attributes.getValue(ATTRIBUTE_LABEL_ATTRIBUTE));
	}

	public UID getEntity() {
		return entityUID;
	}

	@Override
	public UID getUID() {
		return entityUID;
	}

	@Override
	public UID getForeignkeyfield() {
		return foreignKeyfieldToParent;
	}

	@Override
	public String getServiceIdentifier() {
		return SERVICE_IDENTIFIER;
	}

	public UID getLabelAttr() {
		return labelAttr;
	}

}
