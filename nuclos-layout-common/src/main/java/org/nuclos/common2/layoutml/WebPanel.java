package org.nuclos.common2.layoutml;

import org.xml.sax.Attributes;

public class WebPanel extends AbstractWebContainer implements IHasTableLayout {
	private final boolean bVisible;
	private WebTableLayout webTableLayout;
	private CompBorder compBorder;
	private String background;
	
	public WebPanel(Attributes attributes) {
		super(attributes);
		this.bVisible = !ATTRIBUTEVALUE_NO.equals(attributes.getValue(ATTRIBUTE_VISIBLE));
	}
	
	@Override
	public boolean isVisible() {
		return bVisible;
	}

	@Override
	public WebTableLayout getWebTableLayout() {
		return webTableLayout;
	}

	@Override
	public void setWebTableLayout(WebTableLayout webTableLayout) {
		this.webTableLayout = webTableLayout;
	}
	
	@Override
	public String getBorderType() {
		return compBorder != null ? compBorder.getType() : null;
	}
	
	@Override
	public void setCompBorder(CompBorder compBorder) {
		this.compBorder = compBorder;
	}
	
	@Override
	public void setBackground(String c) {
		this.background = c;
	}
	
	@Override
	public String getBackground() {
		return this.background;
	}
	
	@Override
	public String getTitle() {
		if (compBorder instanceof TitledBorder) {
			String title = ((TitledBorder)compBorder).getTitle();
			if (title != null && !title.isEmpty()) {
				return title;
			}
		}
		
		if (getConstraints() instanceof WebTabbedpaneConstraints) {
			return ((WebTabbedpaneConstraints)getConstraints()).getTitle();
		}
		
		return getName();		
	}
	
	@Override
	public String toString() {
		return "Name:" + getName() + " Visbl:" + bVisible + " WebTLayout:" + webTableLayout + " Comps:" + componentsString();
	}
	
	@Override
	public String getType() {
		if (getContainer() instanceof WebTabbedPane) {
			return "TAB";
		}
		return "PANEL";
	}
	
}
