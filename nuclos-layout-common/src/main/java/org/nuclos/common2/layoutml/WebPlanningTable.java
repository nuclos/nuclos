package org.nuclos.common2.layoutml;

import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public class WebPlanningTable extends AbstractWebComponent {

	private UID planningTableUID;
	private String dateFrom;
	private String dateUntil;

	public WebPlanningTable(final Attributes attributes) {
		super(attributes);
		addProperty(ATTRIBUTE_PLANNINGTABLE, attributes);
		planningTableUID = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY));
		dateFrom = attributes.getValue(ATTRIBUTE_DATE_FROM);
		dateUntil = attributes.getValue(ATTRIBUTE_DATE_UNTIL);
	}

	public UID getPlanningTableUID() { return this.planningTableUID; }

}
