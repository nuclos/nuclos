package org.nuclos.common2.layoutml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;

public class WebRoot implements IWebContainer, IHasTableLayout {
	private final UID layoutUID;
	private boolean restrictionsMustIgnoreGroovyRules = false;
	private final List<AbstractWebComponent> components = new ArrayList<AbstractWebComponent>();
	private WebTableLayout webTableLayout;
	
	private int tabindexCounter = 1;

	public int getNextTabindex() {
		return tabindexCounter ++;
	}

	public WebRoot(UID layoutUID) {
		this.layoutUID = layoutUID;
	}

	@Override
	public void addComponent(AbstractWebComponent comp) {
		components.add(comp);
	}

	@Override
	public List<AbstractWebComponent> getComponents() {
		return Collections.unmodifiableList(components);
	}

	@Override
	public void liftGrandChildren() {
		if (components.size() == 1 && components.get(0) instanceof IWebContainer) {
			IWebContainer subContainer = (IWebContainer)components.get(0);
			components.clear();
			components.addAll(subContainer.getComponents());
			
			if (subContainer instanceof IHasTableLayout) {
				WebTableLayout layout = ((IHasTableLayout)subContainer).getWebTableLayout();
				if (layout != null) {
					setWebTableLayout(layout);
				}
			}

		}
		
	}
	
	@Override
	public String toString() {
		return "Comps:" + components;
	}

	@Override
	public String getTitle() {
		return "";
	}
	
	@Override
	public boolean isVisible() {
		return true;
	}
	
	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getType() {
		return "ROOT";
	}
	
	public UID getLayoutUID() {
		return layoutUID;
	}
	
	public void setRestrictionsMustIgnoreGroovyRules(boolean restrictionsMustIgnoreGroovyRules) {
		this.restrictionsMustIgnoreGroovyRules = restrictionsMustIgnoreGroovyRules;
	}
	
	public boolean isRestrictionsMustIgnoreGroovyRules() {
		return restrictionsMustIgnoreGroovyRules;
	}

	@Override
	public IWebConstraints getConstraints() {
		return null;
	}

	@Override
	public WebTableLayout getWebTableLayout() {
		return webTableLayout;
	}

	@Override
	public void setWebTableLayout(WebTableLayout webTableLayout) {
		this.webTableLayout = webTableLayout;
	}

	@Override
	public String getBorderType() {
		return null;
	}
	
	@Override
	public void setCompBorder(CompBorder compBorder) {
	}
	
	@Override
	public IWebContainer getContainer() {
		return null;
	}
	
	@Override
	public Map<String, String> getProperties() {
		return null;
	}

	@Override
	public boolean isMemo() {
		return false;
	}

	@Override
	public Integer getTabIndex() {
		return null;
	}

	@Override
	public void setBackground(String background) {
	}

	@Override
	public String getBackground() {
		return null;
	}

}
