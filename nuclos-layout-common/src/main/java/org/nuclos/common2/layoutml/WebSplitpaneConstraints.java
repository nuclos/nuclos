package org.nuclos.common2.layoutml;

import org.xml.sax.Attributes;

public class WebSplitpaneConstraints implements IWebConstraints {
	private final String position;

	public WebSplitpaneConstraints(Attributes attributes) {
		this.position = attributes.getValue(ATTRIBUTE_POSITION);
	}
	
	public String getPosition() {
		return position;
	}

	@Override
	public String toString() {
		return "Position:" + position;
	}
	
	@Override
	public boolean isQuasiEmpty() {
		return false;
	}

	@Override
	public void setSize(int type, int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
}
