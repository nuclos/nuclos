package org.nuclos.common2.layoutml;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public class WebSubform extends AbstractWebDependents {
	
	public static final String SERVICE_IDENTIFIER = "dependence_list";
	
	private final UID entity;
	private final UID foreignkeyfield;
	private final UID parentsubform;
	private final Map<UID, NuclosSubformColumn> mapColumns = new HashMap<UID, NuclosSubformColumn>();
	private final boolean dynamicCellHeights;
	private final boolean ignoreSubLayout;
	private final boolean notClonable;
	
	WebSubform(Attributes attributes) {
		super(attributes);
		entity = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY));
		foreignkeyfield = UID.parseUID(attributes.getValue(ATTRIBUTE_FOREIGNKEYFIELDTOPARENT));
		parentsubform = UID.parseUID(attributes.getValue(ATTRIBUTE_PARENTSUBFORM));
		dynamicCellHeights = !ATTRIBUTEVALUE_NO.equals(attributes.getValue(ATTRIBUTE_DYNAMIC_CELL_HEIGHTS_DEFAULT));
		ignoreSubLayout = ATTRIBUTEVALUE_YES.equals(attributes.getValue(ATTRIBUTE_IGNORE_SUB_LAYOUT));
		notClonable = !ATTRIBUTEVALUE_NO.equals(attributes.getValue(ATTRIBUTE_NOT_CLONEABLE));
	}
	
	public void addSubformColumn(NuclosSubformColumn subformColumn) {
		mapColumns.put(subformColumn.getName(), subformColumn);
	}
	
	public Map<UID, NuclosSubformColumn> getSubFormColumns() {
		return Collections.unmodifiableMap(mapColumns);
	}
	
	public UID getEntity() {
		return entity;
	}
	
	@Override
	public UID getUID() {
		return entity;
	}

	@Override
	public UID getForeignkeyfield() {
		return foreignkeyfield;
	}

	public UID getParentsubform() {
		return parentsubform;
	}
	
	@Override
	public String getServiceIdentifier() {
		return SERVICE_IDENTIFIER;
	}
	
	@Override
	public String toString() {
		return "WebSub=" + entity + " Cols=" + mapColumns;
	}

	public boolean isDynamicCellHeights() {
		return dynamicCellHeights;
	}
	
	public boolean isIgnoreSubLayout() {
		return ignoreSubLayout;
	}

	public boolean isNotClonable() {
		return notClonable;
	}
	
	public boolean isFirstSubformWithinTab() {
		IWebContainer container = getContainer();
		
		for (;;) {
		
			if (container == null) {
				return false; //Subform is not within a Tabbedpane at all
			}
			
			if (container instanceof WebTabbedPane) {
				return true;
			}
			
			for (AbstractWebComponent comp : container.getComponents()) { 
				if (comp == this) {
					break;
				}
				
				if (comp instanceof WebSubform) {
					
					WebSubform that = (WebSubform)comp;
					//Found a subform that comes before and is not a sub-subform.
					if (that.getParentsubform() == null || !that.getParentsubform().equals(getUID())) {
						return false;
					}
					
				}
			}
		
			//Next Level of container
			container = container.getContainer();
		}		
			
	}

}
