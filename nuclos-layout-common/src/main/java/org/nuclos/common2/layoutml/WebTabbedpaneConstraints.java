package org.nuclos.common2.layoutml;

import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public class WebTabbedpaneConstraints implements IWebConstraints {
	private final String title;
	private final UID titleAttribute;
	private final boolean enabled;
	private final String internalname;
	private final String headerColor;

	public WebTabbedpaneConstraints(Attributes attributes) {
		this.title = attributes.getValue(ATTRIBUTE_TITLE);
		this.titleAttribute = UID.parseUID(attributes.getValue(ATTRIBUTE_TITLE_ATTR));
		this.enabled = !ATTRIBUTEVALUE_NO.equals(attributes.getValue(ATTRIBUTE_ENABLED));
		this.internalname = attributes.getValue(ATTRIBUTE_INTERNALNAME);
		this.headerColor = attributes.getValue(ATTRIBUTE_HEADER_COLOR);
	}

	public String getTitle() {
		return title;
	}

	public String getHeaderColor() {
		return headerColor;
	}

	public String getInternalname() {
		return internalname;
	}

	public boolean isEnabled() {
		return enabled;
	}
	
	@Override
	public String toString() {
		return "Title:" + title + "TitleAttribute:" + titleAttribute + "HeaderColor: " + headerColor + " Enabled:" + enabled + " IntnlName:" + internalname;
	}
	
	@Override
	public boolean isQuasiEmpty() {
		return false;
	}

	@Override
	public void setSize(int type, int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
}
