package org.nuclos.common2.layoutml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.xml.sax.Attributes;

public class WebTableLayout implements LayoutMLConstants {
	private final List<Double> lstColumns = new ArrayList<Double>();
	private final List<Double> lstRows = new ArrayList<Double>();
	
	public WebTableLayout(Attributes attributes) {
		String sColumns = attributes.getValue(ATTRIBUTE_COLUMNS);
		fillUpLayout(lstColumns, sColumns);
		
		String sRows = attributes.getValue(ATTRIBUTE_ROWS);
		fillUpLayout(lstRows, sRows);
	}
	
	private static void fillUpLayout(List<Double> lst, String s) {
		String[] ss = s.split("\\|");
		for (String s0 : ss) {
			lst.add(Double.parseDouble(s0));
		}
	}

	public List<Double> getColumns() {
		return Collections.unmodifiableList(lstColumns);
	}

	public List<Double> getRows() {
		return Collections.unmodifiableList(lstRows);
	}
	
	@Override
	public String toString() {
		return "Columns:" + lstColumns + " Rows:" + lstRows;
	}

	public boolean isQuasiEmpty() {
		return (lstColumns.isEmpty() || (lstColumns.size() == 1 && lstColumns.get(0) == -1.0d))
				&& (lstRows.isEmpty() || (lstRows.size() == 1 && lstRows.get(0) == -1.0d));
	}
}
