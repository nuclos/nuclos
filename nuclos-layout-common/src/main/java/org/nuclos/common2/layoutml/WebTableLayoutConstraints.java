package org.nuclos.common2.layoutml;

import org.xml.sax.Attributes;

public class WebTableLayoutConstraints implements IWebConstraints {
	private final int col1;
	private final int col2;
	private final int row1;
	private final int row2;
	private final int hAlign;
	private final int vAlign;
	
	public WebTableLayoutConstraints(Attributes attributes) {
		this.col1 = Integer.parseInt(attributes.getValue("col1"));
		this.col2 = Integer.parseInt(attributes.getValue("col2"));
		this.row1 = Integer.parseInt(attributes.getValue("row1"));
		this.row2 = Integer.parseInt(attributes.getValue("row2"));
		this.hAlign = Integer.parseInt(attributes.getValue("hAlign"));
		this.vAlign = Integer.parseInt(attributes.getValue("vAlign"));
	}

	public int getColumn1() {
		return col1;
	}

	public int getColumn2() {
		return col2;
	}

	public int getRow1() {
		return row1;
	}

	public int getRow2() {
		return row2;
	}

	public int gethAlign() {
		return hAlign;
	}

	public int getvAlign() {
		return vAlign;
	}
	
	@Override
	public String toString() {
		return "Col12:" + col1 + "-" + col2 + " Row12:" + row1 + "-" + row2 + " AlignH/V:" + hAlign + "-" + vAlign;
	}

	@Override
	public boolean isQuasiEmpty() {
		return col1 == 0 && col2 == 0 && row1 == 0 && row2 == 0;
	}
	
	private WebComponentSizes sizes;

	@Override
	public void setSize(int type, int width, int height) {
		if (sizes == null) {
			sizes = new WebComponentSizes();
		}
		
		sizes.addSize(type, width, height);
		
	}
	
	public WebComponentSizes getSizes() {
		return sizes;
	}

}
