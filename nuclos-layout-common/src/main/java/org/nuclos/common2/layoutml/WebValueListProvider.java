package org.nuclos.common2.layoutml;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.nuclos.cache.IFqnCache;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.xml.sax.Attributes;

import com.google.common.collect.ImmutableMap;

public class WebValueListProvider implements LayoutMLConstants {

	public static final String DATASOURCE_IDFIELD = "id-fieldname";
	public static final String DATASOURCE_NAMEFIELD = "fieldname";
	public static final String DATASOURCE_DEFAULTMARKERFIELD = "default-fieldname";
	public static final String DATASOURCE_VALUELISTPROVIDER = "valuelistProvider";
	public static final String DATASOURCE_SEARCHMODE = "searchmode";
	
	private final String type;
	private final UID value;
	private final Map<String, Object> mpParams;
	
	public WebValueListProvider(Attributes attributes) {
		this(attributes.getValue(ATTRIBUTE_TYPE), attributes.getValue(ATTRIBUTE_VALUE));
	}
	
	public WebValueListProvider(String type, String value) {
		this.type = type;
		this.value = UID.parseUID(value);
		this.mpParams = new HashMap<String, Object>();
		// NUCLOS-4300
		this.mpParams.put("searchmode", 0);
	}

	public String getType() {
		return type;
	}

	public UID getValue() {
		return value;
	}
	
	public void addParameter(Attributes attributes) {
		String sName = attributes.getValue(ATTRIBUTE_NAME);
		String sValue = attributes.getValue(ATTRIBUTE_VALUE);
		addParameter(sName, sValue, null);
	}
	
	public void addParameter(String key, String value, IFqnCache fqnCache) {
		if (DATASOURCE_SEARCHMODE.equals(key)
				&& (Boolean.TRUE.toString().equals(value.toLowerCase())
					|| Boolean.FALSE.toString().equals(value.toLowerCase()))) {
			value = String.valueOf(Boolean.valueOf(value) ? 1 : 0);
		}
		if (fqnCache != null) {
			// try to translate FQNs here (NUCLOS-9658)
			value = translateFqnIfAny(value, fqnCache, E.PROCESS);
			value = translateFqnIfAny(value, fqnCache, E.STATE);
		}
		mpParams.put(key, value);
	}

	private String translateFqnIfAny(String value, IFqnCache fqnCache, EntityMeta<?> eMeta) {
		return Optional.ofNullable(fqnCache.translateFqn(eMeta, value))
				.filter(uid -> !Objects.equals(uid.getString(), value)) // validate: filter defaults out 'return UID.parseUID(fqn);'
				.filter(uid -> Objects.equals(fqnCache.translateUid(eMeta, uid), value)) // validate: filter not translated values out
				.map(uid -> uid.getStringifiedDefinitionWithEntity(eMeta))
				.orElse(value);
	}

	public Map<String, Object> getParameters() {
		return ImmutableMap.copyOf(mpParams);
	}
	
	public boolean hasDefaultMarker() {
		return mpParams.containsKey(DATASOURCE_DEFAULTMARKERFIELD);
	}
	
	public boolean hasDatasourceParameters() {
		for (String key : mpParams.keySet()) {
			if (
				key.equals(DATASOURCE_IDFIELD) ||
				key.equals(DATASOURCE_NAMEFIELD) ||
				key.equals(DATASOURCE_DEFAULTMARKERFIELD) ||
				key.equals(DATASOURCE_VALUELISTPROVIDER) ||
				key.equals(DATASOURCE_SEARCHMODE)
					) {
				continue;
			} else {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "Type=" + type + " Value=" + value + " MpParams=" + mpParams;
	}
}
