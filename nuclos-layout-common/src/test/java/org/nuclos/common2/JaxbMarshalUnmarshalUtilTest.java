package org.nuclos.common2;

import java.io.InputStream;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.junit.Test;
import org.nuclos.schema.layout.layoutml.Layoutml;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class JaxbMarshalUnmarshalUtilTest {
	@Test
	public void testUnmarshal() throws JAXBException, XMLStreamException {
		InputStream xml = this.getClass().getResourceAsStream("layoutml.xml");

		Layoutml result = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class);
		assert result != null;
	}
}
