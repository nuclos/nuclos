package org.nuclos.layout.transformation;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A generic LayoutML transformer.
 *
 * @param <T>
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public abstract class AbstractLayoutmlTransformer<T> {
	private static final Logger log = LoggerFactory.getLogger(AbstractLayoutmlTransformer.class);
	protected final Layoutml layoutml;
	protected final IRigidMetaProvider metaProvider;
	protected final ILanguageLocator localeDelegate;

	public AbstractLayoutmlTransformer(
			final IRigidMetaProvider metaProvider,
			final ILanguageLocator localeDelegate,
			final Layoutml layoutml
	) {
		this.metaProvider = metaProvider;
		this.layoutml = layoutml;
		this.localeDelegate = localeDelegate;
	}

	public abstract T transform();

	/**
	 * Tries to lookup the field name for the given UID string of the form "uid{...}".
	 *
	 * TODO: Maybe this can be done completely automatically by a Jackson serializer.
	 *
	 * @param uidString
	 * @return The field name if found, or the given uidString if not found.
	 */
	public String getFieldName(String uidString) {
		UID uid = UID.parseUID(uidString);
		if (uid != null) {
			if(metaProvider.checkEntityField(uid)) {
				try {
					FieldMeta<?> fieldMeta = metaProvider.getEntityField(uid);
					if (fieldMeta != null) {
						return fieldMeta.getFieldName();
					}
				} catch (Exception err) {
					log.warn("Failed to retrieve field with uid: " + uidString, err);
					return uidString;
				}
			} else {
				log.warn("Failed to retrieve field with uid: " + uidString);
			}
		}
		return uidString;
	}

	public String getLocaleLanguage() {
		if (localeDelegate != null) {
			return localeDelegate.getLanguageToUse();
		}
		return null;
	}
}
