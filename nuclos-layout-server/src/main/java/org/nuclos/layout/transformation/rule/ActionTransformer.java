package org.nuclos.layout.transformation.rule;

import org.nuclos.schema.layout.rule.ObjectFactory;
import org.nuclos.schema.layout.rule.RuleAction;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
abstract class ActionTransformer<T, R extends RuleAction> {
	private final Class<T> clss;
	protected ObjectFactory factory;

	ActionTransformer(Class<T> clss) {
		this.clss = clss;

		this.factory = new ObjectFactory();
	}

	abstract R transform(Object input);
}
