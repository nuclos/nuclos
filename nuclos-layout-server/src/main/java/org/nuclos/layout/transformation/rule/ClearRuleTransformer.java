package org.nuclos.layout.transformation.rule;

import org.nuclos.schema.layout.layoutml.Clear;
import org.nuclos.schema.layout.rule.RuleActionClear;


/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class ClearRuleTransformer extends ActionTransformer<Clear, RuleActionClear> {

	ClearRuleTransformer() {
		super(Clear.class);
	}

	@Override
	RuleActionClear transform(final Object obj) {
		Clear input = (Clear) obj;
		RuleActionClear result = factory.createRuleActionClear();

		result.setEntity(input.getEntity());
		result.setTargetcomponent(input.getTargetcomponent());

		return result;
	}
}
