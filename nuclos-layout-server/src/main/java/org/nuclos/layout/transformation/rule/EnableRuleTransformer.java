package org.nuclos.layout.transformation.rule;

import org.nuclos.schema.layout.layoutml.Boolean;
import org.nuclos.schema.layout.layoutml.Enable;
import org.nuclos.schema.layout.rule.RuleActionEnable;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class EnableRuleTransformer extends ActionTransformer<Enable, RuleActionEnable> {

	EnableRuleTransformer() {
		super(Enable.class);
	}

	@Override
	RuleActionEnable transform(final Object obj) {
		Enable input = (Enable) obj;
		RuleActionEnable result = factory.createRuleActionEnable();

		result.setInvertable(input.getInvertable() == Boolean.YES);
		result.setTargetcomponent(input.getTargetcomponent());

		return result;
	}
}
