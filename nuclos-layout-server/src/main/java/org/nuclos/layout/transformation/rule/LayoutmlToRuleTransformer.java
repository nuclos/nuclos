package org.nuclos.layout.transformation.rule;

import java.io.Serializable;
import java.util.List;

import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.layout.transformation.AbstractLayoutmlTransformer;
import org.nuclos.schema.layout.layoutml.Event;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.nuclos.schema.layout.rule.ObjectFactory;
import org.nuclos.schema.layout.rule.Rule;
import org.nuclos.schema.layout.rule.RuleAction;
import org.nuclos.schema.layout.rule.RuleEvent;
import org.nuclos.schema.layout.rule.Rules;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Transforms an instance of {@link Layoutml} to {@link Rule}.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class LayoutmlToRuleTransformer extends AbstractLayoutmlTransformer<Rules> {
	private static final Logger log = LoggerFactory.getLogger(LayoutmlToRuleTransformer.class);

	protected final ObjectFactory factory;

	public LayoutmlToRuleTransformer(
			final IRigidMetaProvider metaProvider,
			final Layoutml layoutml
	) {
		super(metaProvider, null, layoutml);

		factory = new ObjectFactory();
	}

	@Override
	public Rules transform() {
		Rules result = factory.createRules();

		if (layoutml.getRules() != null && layoutml.getRules().getRule() != null) {
			layoutml.getRules().getRule().forEach(itRule -> {
				Rule rule = factory.createRule();
				rule.setName(itRule.getName());
				rule.setEvent(transformEvent(itRule.getEvent()));
				if (itRule.getActions() != null) {
					final List<Serializable> transferLookedupValueOrClearOrEnable = itRule.getActions().getTransferLookedupValueOrClearOrEnable();
					if (transferLookedupValueOrClearOrEnable != null) {
						transferLookedupValueOrClearOrEnable.forEach(itAction -> {
							RuleAction action = transformAction(itAction);
							if (action != null) {
								rule.getActions().add(action);
							}
						});
					}
				}
				result.getRules().add(rule);
			});
		}

		return result;
	}

	RuleEvent transformEvent(Event event) {
		RuleEvent result = factory.createRuleEvent();

		result.setType(event.getType());
		result.setEntity(event.getEntity());
		result.setSourcecomponent(event.getSourcecomponent());

		return result;
	}

	RuleAction transformAction(Object action) {
		RuleAction result = null;

		if (action != null) {
			ActionTransformer<?, ? extends RuleAction> transformer = TransformerRegistry.lookup(action.getClass());
			if (transformer != null) {
				result = transformer.transform(action);
			} else {
				log.warn("Unknown action type {}", action);
			}
		}

		return result;
	}
}
