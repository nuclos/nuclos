package org.nuclos.layout.transformation.rule;

import org.nuclos.schema.layout.layoutml.RefreshValuelist;
import org.nuclos.schema.layout.rule.RuleActionRefreshValuelist;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class RefreshValuelistRuleTransformer extends ActionTransformer<RefreshValuelist, RuleActionRefreshValuelist> {

	RefreshValuelistRuleTransformer() {
		super(RefreshValuelist.class);
	}

	@Override
	RuleActionRefreshValuelist transform(final Object obj) {
		RefreshValuelist input = (RefreshValuelist) obj;
		RuleActionRefreshValuelist result = factory.createRuleActionRefreshValuelist();

		result.setEntity(input.getEntity());
		result.setTargetcomponent(input.getTargetcomponent());
		result.setParameterForSourcecomponent(input.getParameterForSourcecomponent());

		return result;
	}
}
