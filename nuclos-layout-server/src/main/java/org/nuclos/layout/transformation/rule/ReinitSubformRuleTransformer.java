package org.nuclos.layout.transformation.rule;

import org.nuclos.schema.layout.layoutml.ReinitSubform;
import org.nuclos.schema.layout.rule.RuleActionReinitSubform;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class ReinitSubformRuleTransformer extends ActionTransformer<ReinitSubform, RuleActionReinitSubform> {

	ReinitSubformRuleTransformer() {
		super(ReinitSubform.class);
	}

	@Override
	RuleActionReinitSubform transform(final Object obj) {
		ReinitSubform input = (ReinitSubform) obj;
		RuleActionReinitSubform result = factory.createRuleActionReinitSubform();

		result.setEntity(input.getEntity());
		result.setTargetcomponent(input.getTargetcomponent());

		return result;
	}
}
