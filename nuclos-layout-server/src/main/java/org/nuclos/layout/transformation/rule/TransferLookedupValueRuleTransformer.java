package org.nuclos.layout.transformation.rule;

import org.nuclos.schema.layout.layoutml.TransferLookedupValue;
import org.nuclos.schema.layout.rule.RuleActionTransferLookedupValue;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class TransferLookedupValueRuleTransformer extends ActionTransformer<TransferLookedupValue, RuleActionTransferLookedupValue> {

	TransferLookedupValueRuleTransformer() {
		super(TransferLookedupValue.class);
	}

	@Override
	RuleActionTransferLookedupValue transform(final Object obj) {
		TransferLookedupValue input = (TransferLookedupValue) obj;
		RuleActionTransferLookedupValue result = factory.createRuleActionTransferLookedupValue();

		result.setSourcefield(input.getSourcefield());
		result.setTargetcomponent(input.getTargetcomponent());

		return result;
	}
}
