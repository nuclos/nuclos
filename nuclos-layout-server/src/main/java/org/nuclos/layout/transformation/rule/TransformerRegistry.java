package org.nuclos.layout.transformation.rule;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.schema.layout.layoutml.Clear;
import org.nuclos.schema.layout.layoutml.Enable;
import org.nuclos.schema.layout.layoutml.RefreshValuelist;
import org.nuclos.schema.layout.layoutml.ReinitSubform;
import org.nuclos.schema.layout.layoutml.TransferLookedupValue;
import org.nuclos.schema.layout.rule.RuleAction;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class TransformerRegistry {
	static Map<Class, ActionTransformer> map = new HashMap<>();

	static {
		register(Enable.class, new EnableRuleTransformer());
		register(Clear.class, new ClearRuleTransformer());
		register(TransferLookedupValue.class, new TransferLookedupValueRuleTransformer());
		register(ReinitSubform.class, new ReinitSubformRuleTransformer());
		register(RefreshValuelist.class, new RefreshValuelistRuleTransformer());
	}

	static <T> void register(Class<T> clss, ActionTransformer<T, ? extends RuleAction> transformer) {
		map.put(clss, transformer);
	}

	static <T> ActionTransformer<T, ? extends RuleAction> lookup(Class<T> clss) {
		return map.get(clss);
	}
}
