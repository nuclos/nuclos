package org.nuclos.layout.transformation.vlp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.layout.transformation.AbstractLayoutmlTransformer;
import org.nuclos.schema.layout.layoutml.CollectableComponent;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.nuclos.schema.layout.layoutml.Panel;
import org.nuclos.schema.layout.layoutml.Scrollpane;
import org.nuclos.schema.layout.layoutml.ValuelistProvider;
import org.nuclos.schema.layout.vlp.ObjectFactory;
import org.nuclos.schema.layout.vlp.Parameter;
import org.nuclos.schema.layout.vlp.ValueListProvider;
import org.nuclos.schema.layout.vlp.ValueListProviders;

/**
 * Transforms an instance of {@link Layoutml} to {@link ValuelistProvider}.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class LayoutmlToVlpTransformer extends AbstractLayoutmlTransformer<ValueListProviders> {

	protected final ObjectFactory factory;

	public LayoutmlToVlpTransformer(
			final IRigidMetaProvider metaProvider,
			final Layoutml layoutml
	) {
		super(metaProvider, null, layoutml);

		factory = new ObjectFactory();
	}

	@Override
	public ValueListProviders transform() {
		ValueListProviders result = factory.createValueListProviders();

		result.getVlps().addAll(transformRecursive(layoutml.getLayout().getPanel()));

		return result;
	}

	private List<ValueListProvider> transformRecursive(Serializable container) {
		List<ValueListProvider> result = new ArrayList<>();

		List<Serializable> elements = new ArrayList<>();
		if (container instanceof Panel) {
			elements.addAll(((Panel) container).getContainerOrLabelOrTextfield());
		} else if (container instanceof Scrollpane) {
			elements.add(((Scrollpane) container).getContainer());
			elements.add(((Scrollpane) container).getCollectableComponent());
		}
		elements.forEach(it -> {
			if (it instanceof JAXBElement) {
				if (((JAXBElement<?>) it).getValue() instanceof Serializable) {
					result.addAll(transformRecursive((Serializable) ((JAXBElement<?>) it).getValue()));
				}
			} else if (it instanceof CollectableComponent) {
				CollectableComponent clctComponent = (CollectableComponent) it;
				ValuelistProvider vlp = clctComponent.getValuelistProvider();
				if (vlp != null) {
					ValueListProvider vlpResult = factory.createValueListProvider();
					vlpResult.setAttribute(getFieldName(clctComponent.getName()));
					vlpResult.setName(vlp.getName());
					vlpResult.setType(vlp.getType());
					vlpResult.setValue(vlp.getValue());
					if (vlp.getParameter() != null) {
						vlp.getParameter().forEach(it2 -> {
							switch (it2.getName()) {
								case "fieldname":
									vlpResult.setFieldname(it2.getValue());
									break;
								case "id-fieldname":
									vlpResult.setIdFieldname(it2.getValue());
									break;
								case "default-fieldname":
									vlpResult.setDefaultFieldname(it2.getValue());
									break;
							}
							Parameter parameter = factory.createParameter();
							parameter.setName(it2.getName());
							parameter.setValue(it2.getValue());
							vlpResult.getParameters().add(parameter);
						});
					}

					result.add(vlpResult);
				}
			}
		});

		return result;
	}
}
