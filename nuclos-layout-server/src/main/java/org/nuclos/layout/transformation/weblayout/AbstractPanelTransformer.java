package org.nuclos.layout.transformation.weblayout;

import java.util.Objects;

import org.nuclos.schema.layout.layoutml.LineBorder;
import org.nuclos.schema.layout.layoutml.Panel;
import org.nuclos.schema.layout.layoutml.TitledBorder;
import org.nuclos.schema.layout.layoutml.Translation;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebContainer;
import org.nuclos.schema.layout.web.WebPanel;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public abstract class AbstractPanelTransformer extends ElementTransformer<Panel, WebComponent> {
	public AbstractPanelTransformer() {
		super(Panel.class);
	}

	protected WebContainer createPanelOrContainer(LayoutmlToWeblayoutTransformer layoutTransformer, Panel panel) {
		WebContainer result;
		if (hasTitleOrBorder(panel)) {
			result = factory.createWebPanel();
			((WebPanel) result).setTitle(findTitle(layoutTransformer, panel));
			addLineBorder((WebPanel) result, panel);
			setBackgroundInWebComponent(result, panel.getBackground());
		} else {
			result = factory.createWebContainer();
		}
		return result;
	}

	private void addLineBorder(final WebPanel webPanel, final Panel panel) {
		LineBorder border = findLineBorder(panel);

		if (border != null) {
			if (border.getThickness() != null) {
				webPanel.setBorderWidth(border.getThickness() + "px");
			}
			if (border.getRed() != null && border.getGreen() != null && border.getBlue() != null) {
				try {
					webPanel.setBorderColor(String.format(
							"#%02X%02X%02X",
							Integer.valueOf(border.getRed()),
							Integer.valueOf(border.getGreen()),
							Integer.valueOf(border.getBlue()))
					);
				} catch (Exception ex) {
					// Ignore
				}
			}
		}
	}

	private boolean hasTitleOrBorder(Panel panel) {
		return panel.getTitledBorder() != null || panel.getLineBorder() != null;
	}

	/**
	 * Tries to find a titled border and returns its title.
	 *
	 * @param panel
	 */
	private String findTitle(LayoutmlToWeblayoutTransformer layoutTransformer, Panel panel) {
		TitledBorder border = panel.getTitledBorder();

		if (border != null) {
			String title = null;
			if (border.getTranslations() != null
					&& border.getTranslations().getTranslation() != null
					&& !border.getTranslations().getTranslation().isEmpty()
					&& layoutTransformer.getLocaleLanguage() != null) {
				title = border.getTranslations().getTranslation().stream()
						.filter(t -> Objects.equals(t.getLang(), layoutTransformer.getLocaleLanguage()))
						.findAny()
						.map(Translation::getText)
						.orElse(null);
			}
			if (title == null) {
				title = border.getTitle();
			}

			return title;
		}
		return null;
	}

	/**
	 * Tries to find a line border.
	 *
	 * @param panel
	 */
	private LineBorder findLineBorder(Panel panel) {
		LineBorder border = panel.getLineBorder();

		return border;
	}
}
