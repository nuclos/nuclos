package org.nuclos.layout.transformation.weblayout;

import java.util.Objects;

import org.nuclos.schema.layout.layoutml.Background;
import org.nuclos.schema.layout.layoutml.Boolean;
import org.nuclos.schema.layout.layoutml.Button;
import org.nuclos.schema.layout.layoutml.Property;
import org.nuclos.schema.layout.layoutml.Translation;
import org.nuclos.schema.layout.web.WebButton;
import org.nuclos.schema.layout.web.WebButtonAddon;
import org.nuclos.schema.layout.web.WebButtonChangeState;
import org.nuclos.schema.layout.web.WebButtonExecuteRule;
import org.nuclos.schema.layout.web.WebButtonGenerateObject;
import org.nuclos.schema.layout.web.WebButtonHyperlink;
import org.nuclos.schema.layout.web.WebComponent;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class ButtonTransformer extends ElementTransformer<Button, WebComponent> {

	ButtonTransformer() {
		super(Button.class);
	}

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Button button = (Button) obj;
		WebButton result;

		String action = button.getActioncommand();
		if (action.endsWith("ExecuteRuleButtonAction")) {
			result = createRuleButton(button);
		} else if (action.endsWith("ChangeStateButtonAction")) {
			result = createChangeStateButton(button);
		} else if (action.endsWith("GeneratorButtonAction")) {
			result = createObjectGeneratorButton(button);
		} else if (action.endsWith("HyperlinkButtonAction")) {
			result = createHyperlinkButton(button);
		} else if (action.endsWith("DummyButtonAction")) {
			result = factory.createWebButtonDummy();
		} else {
			// Nuclos Java client extension or webaddon
			result = factory.createWebButtonAddon();

			// addon component name should be the same name as the extension Java class name as convention
			final String[] split = action.split("\\.");
			if (split.length >= 1) {
				((WebButtonAddon) result).setAddonComponentName(split[split.length - 1]); // remove package part
			}
		}

		setSizeAndAlign(
				button.getPreferredSize(),
				button.getMinimumSize(),
				button.getLayoutconstraints(),
				result
		);

		result.setName(button.getName());

		String label = null;
		if (button.getTranslations() != null
				&& button.getTranslations().getTranslation() != null
				&& !button.getTranslations().getTranslation().isEmpty()
				&& layoutTransformer.getLocaleLanguage() != null) {
			label = button.getTranslations().getTranslation().stream()
					.filter(t -> Objects.equals(t.getLang(), layoutTransformer.getLocaleLanguage()))
					.map(Translation::getText)
					.findAny()
					.orElse(null);
		}
		if (label == null) {
			label = button.getLabel();
		}
		result.setLabel(label);
		if (button.getLabelAttr() != null) {
			String labelAttr = layoutTransformer.getFieldName(button.getLabelAttr());
			if (labelAttr != null) {
				result.setLabelAttr(labelAttr);
			}
		}
		
		result.setAlternativeTooltip(button.getTooltip() != null ? button.getTooltip() : button.getDescription());
		result.setIcon(button.getIcon());
		if (button.getFont() != null) {
			result.setFontSize(convertFontSize(button.getFont().getSize()));
		}
		result.setCustomBackgroundColor(convertBackground(button.getBackground()));
		setTextFormatInWebComponent(result, button.getTextFormat());

		if (button.getDescriptionAttr() != null) {
			String descriptionAttr = layoutTransformer.getFieldName(button.getDescriptionAttr());
			if (descriptionAttr != null) {
				result.setDescriptionAttr(descriptionAttr);
			}
		}

		if (button.getNextfocusfield() != null) {
			String nextFocusFieldName = layoutTransformer.getFieldName(button.getNextfocusfield());
			if (nextFocusFieldName != null) {
				result.setNextFocusField(nextFocusFieldName);
			}
		}

		if (button.getNextfocuscomponent() != null) {
			result.setNextFocusComponent(button.getNextfocuscomponent());
		}

		if (button.getDisableDuringEdit() == Boolean.YES) {
			result.setDisableDuringEdit(true);
		}

		return result;
	}

	String convertBackground(final Background background) {
		try {
			return String.format(
					"#%02X%02X%02X",
					Integer.valueOf(background.getRed()),
					Integer.valueOf(background.getGreen()),
					Integer.valueOf(background.getBlue())
			);
		} catch (Throwable ignored) {
			return "";
		}
	}

	private WebButton createHyperlinkButton(Button button) {
		WebButtonHyperlink result = factory.createWebButtonHyperlink();
		result.setHyperlinkField(button.getProperty().stream().filter(it -> Objects.equals(it.getName(), "hyperlinkField")).findAny().map(Property::getValue).orElse(null));
		return result;
	}

	private WebButton createRuleButton(Button button) {
		WebButtonExecuteRule result = factory.createWebButtonExecuteRule();
		result.setRule(button.getProperty().stream().filter(it -> Objects.equals(it.getName(), "ruletoexecute")).findAny().map(Property::getValue).orElse(null));
		return result;
	}

	private WebButton createChangeStateButton(Button button) {
		WebButtonChangeState result = factory.createWebButtonChangeState();
		result.setTargetState(button.getProperty().stream().filter(it -> Objects.equals(it.getName(), "targetState")).findAny().map(Property::getValue).orElse(null));
		return result;
	}

	private WebButton createObjectGeneratorButton(Button button) {
		WebButtonGenerateObject result = factory.createWebButtonGenerateObject();
		result.setObjectGenerator(button.getProperty().stream().filter(it -> Objects.equals(it.getName(), "generatortoexecute")).findAny().map(Property::getValue).orElse(null));
		return result;
	}

}
