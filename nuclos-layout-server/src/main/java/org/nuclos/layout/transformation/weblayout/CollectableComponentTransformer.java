package org.nuclos.layout.transformation.weblayout;


import java.util.Objects;

import org.nuclos.common2.StringUtils;
import org.nuclos.schema.layout.layoutml.Boolean;
import org.nuclos.schema.layout.layoutml.CollectableComponent;
import org.nuclos.schema.layout.layoutml.Option;
import org.nuclos.schema.layout.layoutml.Translation;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebInputComponent;
import org.nuclos.schema.layout.web.WebOption;
import org.nuclos.schema.layout.web.WebOptiongroup;
import org.nuclos.schema.layout.web.WebOptions;
import org.nuclos.schema.layout.web.WebTextfield;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class CollectableComponentTransformer extends ElementTransformer<CollectableComponent, WebComponent> {
	private static final Logger log = LoggerFactory.getLogger(LayoutmlToWeblayoutTransformer.class);

	CollectableComponentTransformer() {
		super(CollectableComponent.class);
	}

	public WebComponent transform(
			LayoutmlToWeblayoutTransformer layoutTransformer,
			Object obj
	) {
		CollectableComponent collectableComponent = (CollectableComponent) obj;
		if (collectableComponent.getVisible() == Boolean.NO) {
			return null;
		}

		WebComponent result = null;

		if ("label".equals(collectableComponent.getShowOnly())) {
			result = factory.createWebLabel();
		} else {
			if (collectableComponent.getControltype() == null) {
				// No type means "universal component". Could be a Filechooser.

				result = determineByDataType(layoutTransformer, collectableComponent, result);

				if (result == null) {
					result = determineByControlTypeClass(collectableComponent, result);
				}
			} else {
				switch (collectableComponent.getControltype()) {
					case TEXTFIELD:
						result = factory.createWebTextfield();
						break;
					case PASSWORD:
						result = factory.createWebPassword();
						break;
					case TEXTAREA:
						result = factory.createWebTextarea();
						break;
					case DATECHOOSER:
						result = factory.createWebDatechooser();
						break;
					case LISTOFVALUES:
						result = factory.createWebListofvalues();
						break;
					case COMBOBOX:
						result = factory.createWebCombobox();
						break;
					case CHECKBOX:
						result = factory.createWebCheckbox();
						break;
					case IMAGE:
						result = factory.createWebImage();
						break;
					case EMAIL:
						result = factory.createWebEmail();
						break;
					case HYPERLINK:
						result = factory.createWebHyperlink();
						break;
					case PHONENUMBER:
						result = factory.createWebPhonenumber();
						break;
					case OPTIONGROUP:
						result = factory.createWebOptiongroup();
						break;
					case HTMLFIELD:
						result = factory.createWebHtmlField();
						break;
					default:
						log.debug("Skipping control type: $collectableComponent.controltype");
				}
			}
		}

		if (result != null) {
			result.setName(layoutTransformer.getFieldName(collectableComponent.getName()));
			if (collectableComponent.getFont() != null) {
				result.setFontSize(convertFontSize(collectableComponent.getFont().getSize()));
			}
			setTextFormatInWebComponent(result, collectableComponent.getTextFormat());
			setBackgroundInWebComponent(result, collectableComponent.getBackground());
			findBorders(result, collectableComponent);

			setSizeAndAlign(
					collectableComponent.getPreferredSize(),
					collectableComponent.getMinimumSize(),
					collectableComponent.getLayoutconstraints(),
					result
			);

			if (collectableComponent.getTranslations() != null
					&& collectableComponent.getTranslations().getTranslation() != null
					&& !collectableComponent.getTranslations().getTranslation().isEmpty()
					&& layoutTransformer.getLocaleLanguage() != null) {
				result.setLabel(collectableComponent.getTranslations().getTranslation().stream()
						.filter(t -> Objects.equals(t.getLang(), layoutTransformer.getLocaleLanguage()))
						.findAny()
						.map(Translation::getText)
						.orElse(null));
			}

			if (result instanceof WebInputComponent) {
				((WebInputComponent) result).setInsertable(collectableComponent.getInsertable() != Boolean.NO);
				transferValuelistProvider(
						(WebInputComponent) result,
						collectableComponent.getValuelistProvider()
				);
			}

			if (result instanceof WebOptiongroup) {
				WebOptiongroup wog = (WebOptiongroup) result;
				WebOptions webOptions = new WebOptions();
				webOptions.setName(collectableComponent.getOptions().getName());
				webOptions.setDefault(collectableComponent.getOptions().getDefault());
				webOptions.setOrientation(collectableComponent.getOptions().getOrientation());

				for (Option option : collectableComponent.getOptions().getOption()) {
					WebOption webOption = new WebOption();
					webOption.setName(option.getName());
					webOption.setValue(option.getValue());
					webOption.setLabel(option.getLabel());
					webOptions.getOption().add(webOption);
				}

				wog.setOptions(webOptions);
			}

			if (collectableComponent.getNextfocusfield() != null) {
				String nextFocusFieldName = layoutTransformer.getFieldName(collectableComponent.getNextfocusfield());
				if (nextFocusFieldName != null) {
					result.setNextFocusField(nextFocusFieldName);
				}
			}

			if (collectableComponent.getNextfocuscomponent() != null) {
				result.setNextFocusComponent(collectableComponent.getNextfocuscomponent());
			}

			if (collectableComponent.getDescriptionAttr() != null) {
				String descriptionAttr = layoutTransformer.getFieldName(collectableComponent.getDescriptionAttr());
				if (descriptionAttr != null) {
					result.setDescriptionAttr(descriptionAttr);
				}
			}

			if (!StringUtils.isNullOrEmpty(collectableComponent.getDescription())) {
				result.setAlternativeTooltip(collectableComponent.getDescription());
			}

			if (collectableComponent.getAutocomplete() != null && result instanceof WebTextfield) {
				((WebTextfield) result).setAutocomplete(collectableComponent.getAutocomplete());
			}

			if (collectableComponent.getTextModule() != null) {
				result.setHasTextModules(true);
			}
		}

		return result;
	}

	private WebComponent determineByControlTypeClass(
			CollectableComponent collectableComponent,
			WebComponent result
	) {
		if (collectableComponent.getControltypeclass() != null) {
			switch (collectableComponent.getControltypeclass()) {
				case "org.nuclos.client.ui.collect.component.CollectableEditorPane":
					result = factory.createWebHtmlEditor();
					break;
				case "org.nuclos.client.ui.collect.component.CollectableColorChooserButton":
					result = factory.createWebColorchooser();
					break;
			}
		}
		return result;
	}

	private WebComponent determineByDataType(
			LayoutmlToWeblayoutTransformer layoutTransformer,
			CollectableComponent collectableComponent,
			WebComponent result
	) {
		LayoutmlToWeblayoutTransformer.DataType type = layoutTransformer.getFieldType(collectableComponent.getName());
		if (type == null) {
			return result;
		}
		switch (type) {
			case FILE:
				result = factory.createWebFile();
				break;
			default:
				log.warn("Unknown data type for field $collectableComponent.name");
		}
		return result;
	}
}
