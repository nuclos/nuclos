package org.nuclos.layout.transformation.weblayout;

import java.math.BigInteger;
import java.util.Objects;

import javax.xml.bind.JAXBElement;

import org.nuclos.schema.layout.layoutml.Background;
import org.nuclos.schema.layout.layoutml.Boolean;
import org.nuclos.schema.layout.layoutml.CollectableComponent;
import org.nuclos.schema.layout.layoutml.MinimumSize;
import org.nuclos.schema.layout.layoutml.Parameter;
import org.nuclos.schema.layout.layoutml.PreferredSize;
import org.nuclos.schema.layout.layoutml.TablelayoutConstraints;
import org.nuclos.schema.layout.layoutml.TextFormat;
import org.nuclos.schema.layout.layoutml.ValuelistProvider;
import org.nuclos.schema.layout.web.ObjectFactory;
import org.nuclos.schema.layout.web.WebBorderProperty;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebInputComponent;
import org.nuclos.schema.layout.web.WebSubformColumn;
import org.nuclos.schema.layout.web.WebValuelistProvider;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
abstract class ElementTransformer<T, R extends WebComponent> {
	private final Class<T> clss;
	protected ObjectFactory factory;

	ElementTransformer(Class<T> clss) {
		this.clss = clss;

		this.factory = new ObjectFactory();
	}

	public abstract R transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object input);

	public String convertFontSize(String fontSize) {
		if (fontSize == null || Integer.parseInt(fontSize) == 0) {
			return null;
		}

		try {
			double percent = (68.75 + ( Integer.parseInt(fontSize) * 10)) / 100f;
			return percent + "rem";
		} catch (NumberFormatException ignored) {
			return null;
		}
	}

	String convertBackground(final Background background) {
		try {
			return String.format(
					"#%02X%02X%02X",
					Integer.valueOf(background.getRed()),
					Integer.valueOf(background.getGreen()),
					Integer.valueOf(background.getBlue())
			);
		} catch (Throwable ignored) {
			return "";
		}
	}

	void setTextFormatInWebComponent(WebComponent webComponent, TextFormat textFormat) {
		if (textFormat == null) {
			return;
		}

		if (textFormat.getBold() == Boolean.YES) {
			webComponent.setBold(true);
		}
		if (textFormat.getItalic() == Boolean.YES) {
			webComponent.setItalic(true);
		}
		if (textFormat.getUnderline() == Boolean.YES) {
			webComponent.setUnderline(true);
		}
		if (textFormat.getTextColor() != null) {
			webComponent.setTextColor(getHexColor(
					textFormat.getTextColor().getRed(),
					textFormat.getTextColor().getGreen(),
					textFormat.getTextColor().getBlue()
			));
		}
	}

	void setBackgroundInWebComponent(WebComponent webComponent, Background background) {
		if (background == null) {
			return;
		}

		webComponent.setCustomBackgroundColor(getHexColor(
				background.getRed(),
				background.getGreen(),
				background.getBlue()
		));
	}

	void findBorders(WebComponent destinationWebComponent, CollectableComponent sourceComponent) {
		if (sourceComponent.getLineBorder() != null) {
			setBorderProperty(
					destinationWebComponent,
					sourceComponent.getLineBorder().getRed(),
					sourceComponent.getLineBorder().getGreen(),
					sourceComponent.getLineBorder().getBlue(),
					sourceComponent.getLineBorder().getThickness(),
					"solid"
			);
		}
	}

	void setBorderProperty(WebComponent webComponent, String red, String green, String blue, String thickness, String type) {
		WebBorderProperty.Builder borderBuilder = WebBorderProperty.builder()
				.withThickness(BigInteger.valueOf(Long.parseLong(thickness)))
				.withType(type);

		if (!"".equals(red) && !"".equals(green) && !"".equals(blue)) {
			borderBuilder.withColor(
					getHexColor(red, green, blue)
			);
		}

		webComponent.setBorderProperty(borderBuilder.build());
	}

	String getHexColor(String red, String green, String blue) {
		int redI = getIntValue(red, 0);
		int greenI = getIntValue(green, 0);
		int blueI = getIntValue(blue, 0);

		return String.format("#%02X%02X%02X", redI, greenI, blueI);
	}

	int getIntValue(String sInteger, int iDefault) {
		if (sInteger ==  null) {
			return iDefault;
		}

		try {
			return Integer.parseInt(sInteger);
		} catch (NumberFormatException e) {
			return iDefault;
		}
	}

	void transferValuelistProvider(
			final WebInputComponent webComponent,
			final ValuelistProvider valuelistProvider
	) {
		if (valuelistProvider == null) {
			return;
		}

		WebValuelistProvider vlp = transformValuelistProvider(valuelistProvider);

		webComponent.setValuelistProvider(vlp);
	}

	private WebValuelistProvider transformValuelistProvider(ValuelistProvider valuelistProvider) {
		WebValuelistProvider vlp = factory.createWebValuelistProvider();
		vlp.setName(valuelistProvider.getName());
		vlp.setType(valuelistProvider.getType());
		vlp.setValue(valuelistProvider.getValue());
		vlp.setFieldname(valuelistProvider.getParameter().stream().filter(it -> Objects.equals("fieldname", it.getName())).findAny().map(Parameter::getValue).orElse(null));
		vlp.setIdFieldname(valuelistProvider.getParameter().stream().filter(it -> Objects.equals("id-fieldname", it.getName())).findAny().map(Parameter::getValue).orElse(null));

		valuelistProvider.getParameter().forEach(it -> {
			WebValuelistProvider.Parameter param = factory.createWebValuelistProviderParameter();
			param.setName(it.getName());
			param.setValue(it.getValue());
			vlp.getParameter().add(param);
		});
		return vlp;
	}

	void transferValuelistProvider(
			final WebSubformColumn subformColumn,
			final ValuelistProvider valuelistProvider
	) {
		if (valuelistProvider == null) {
			return;
		}

		WebValuelistProvider vlp = transformValuelistProvider(valuelistProvider);

		subformColumn.setValuelistProvider(vlp);
	}


	void setSizeAndAlign(PreferredSize prefSize, MinimumSize minSize, JAXBElement<?> layoutConstraints, WebComponent result) {
		/**
		 * Set component size in layout
		 */
		if (prefSize != null) {
			result.setPreferredWidth(BigInteger.valueOf(Integer.parseInt(prefSize.getWidth())));
			result.setPreferredHeight(BigInteger.valueOf(Integer.parseInt(prefSize.getHeight())));
		}

		if (minSize!= null) {
			result.setMinimumWidth(BigInteger.valueOf(Integer.parseInt(minSize.getWidth())));
			result.setMinimumHeight(BigInteger.valueOf(Integer.parseInt(minSize.getHeight())));
		}

		/**
		 * Set component direction in layout
		 */
		result.setHorizontalAlign(translateAlignToWeb(layoutConstraints, true));
		result.setVerticalAlign(translateAlignToWeb(layoutConstraints, false));
	}

	String translateAlignToWeb(JAXBElement<?> layoutConstraints, boolean horizontal) {
		if (layoutConstraints != null &&
				layoutConstraints.getValue() instanceof TablelayoutConstraints) {
			TablelayoutConstraints tblConstraints = (TablelayoutConstraints) layoutConstraints.getValue();

			switch (horizontal ? tblConstraints.getHAlign() : tblConstraints.getVAlign()) {
				case "0":
					return "flex-start";
				case "1":
					return "center";
				case "2":
					return "stretch";
				case "3":
					return "flex-end";
			}
		}
		return "center";
	}
}
