package org.nuclos.layout.transformation.weblayout;

import org.nuclos.schema.layout.layoutml.Htmlfield;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebHtmlField;

class HTMLTransformer extends ElementTransformer<Htmlfield, WebComponent> {

	HTMLTransformer() {
		super(Htmlfield.class);
	}

	@Override
	public WebComponent transform(final LayoutmlToWeblayoutTransformer layoutTransformer, final Object obj) {
		Htmlfield label = (Htmlfield) obj;
		WebHtmlField result = factory.createWebHtmlField();

		result.setName(label.getName());

		result.setText(label.getHtml());

		setSizeAndAlign(
				label.getPreferredSize(),
				label.getMinimumSize(),
				label.getLayoutconstraints(),
				result
		);

		if (label.getNextfocusfield() != null) {
			String nextFocusFieldName = layoutTransformer.getFieldName(label.getNextfocusfield());
			if (nextFocusFieldName != null) {
				result.setNextFocusField(nextFocusFieldName);
			}
		}

		if (label.getNextfocuscomponent() != null) {
			result.setNextFocusComponent(label.getNextfocuscomponent());
		}

		if (label.getDescriptionAttr() != null) {
			String descriptionAttr = layoutTransformer.getFieldName(label.getDescriptionAttr());
			if (descriptionAttr != null) {
				result.setDescriptionAttr(descriptionAttr);
			}
		}

		result.setCustomBackgroundColor(convertBackground(label.getBackground()));
		result.setAlternativeTooltip(label.getDescription());
		if (label.getFont() != null) {
			result.setFontSize(convertFontSize(label.getFont().getSize()));
		}
		setTextFormatInWebComponent(result, label.getTextFormat());

		if (label.getLineBorder() != null) {
			setBorderProperty(
					result,
					label.getLineBorder().getRed(),
					label.getLineBorder().getGreen(),
					label.getLineBorder().getBlue(),
					label.getLineBorder().getThickness(),
					"solid"
			);
		}

		return result;
	}
}
