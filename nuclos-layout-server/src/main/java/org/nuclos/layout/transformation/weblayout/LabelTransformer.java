package org.nuclos.layout.transformation.weblayout;

import java.util.Objects;

import org.nuclos.schema.layout.layoutml.Label;
import org.nuclos.schema.layout.layoutml.Translation;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebLabelStatic;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class LabelTransformer extends ElementTransformer<Label, WebComponent> {

	LabelTransformer() {
		super(Label.class);
	}

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Label label = (Label) obj;
		WebLabelStatic result = factory.createWebLabelStatic();

		String text = null;
		if (label.getTranslations() != null
				&& label.getTranslations().getTranslation() != null
				&& !label.getTranslations().getTranslation().isEmpty()
				&& layoutTransformer.getLocaleLanguage() != null) {
			text = label.getTranslations().getTranslation().stream().filter(translation -> Objects.equals(translation.getLang(), layoutTransformer.getLocaleLanguage())).findAny().map(Translation::getText).orElse(null);
		}
		if (text == null) {
			text = label.getText();
		}
		result.setName(label.getName());
		result.setText(text);

		setSizeAndAlign(
				label.getPreferredSize(),
				label.getMinimumSize(),
				label.getLayoutconstraints(),
				result
		);

		if (label.getLineBorder() != null) {
			setBorderProperty(
					result,
					label.getLineBorder().getRed(),
					label.getLineBorder().getGreen(),
					label.getLineBorder().getBlue(),
					label.getLineBorder().getThickness(),
					"solid"
			);
		}

		if (label.getDescriptionAttr() != null) {
			String descriptionAttr = layoutTransformer.getFieldName(label.getDescriptionAttr());
			if (descriptionAttr != null) {
				result.setDescriptionAttr(descriptionAttr);
			}
		}

		result.setCustomBackgroundColor(convertBackground(label.getBackground()));
		result.setAlternativeTooltip(label.getDescription());
		if (label.getFont() != null) {
			result.setFontSize(convertFontSize(label.getFont().getSize()));
		}
		setTextFormatInWebComponent(result, label.getTextFormat());

		return result;
	}

}
