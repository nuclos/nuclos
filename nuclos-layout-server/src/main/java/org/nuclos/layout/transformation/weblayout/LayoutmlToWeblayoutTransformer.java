package org.nuclos.layout.transformation.weblayout;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.JAXBElement;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.layout.transformation.AbstractLayoutmlTransformer;
import org.nuclos.layout.transformation.ILanguageLocator;
import org.nuclos.schema.layout.layoutml.Boolean;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.nuclos.schema.layout.layoutml.Property;
import org.nuclos.schema.layout.layoutml.Subform;
import org.nuclos.schema.layout.layoutml.TablelayoutConstraints;
import org.nuclos.schema.layout.web.ObjectFactory;
import org.nuclos.schema.layout.web.WebAdvancedProperty;
import org.nuclos.schema.layout.web.WebCell;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebContainer;
import org.nuclos.schema.layout.web.WebGrid;
import org.nuclos.schema.layout.web.WebHtmlField;
import org.nuclos.schema.layout.web.WebInputComponent;
import org.nuclos.schema.layout.web.WebLayout;
import org.nuclos.schema.layout.web.WebMatrix;
import org.nuclos.schema.layout.web.WebRow;
import org.nuclos.schema.layout.web.WebSplitpane;
import org.nuclos.schema.layout.web.WebSubform;
import org.nuclos.schema.layout.web.WebSubformColumn;
import org.nuclos.schema.layout.web.WebTabcontainer;
import org.nuclos.schema.layout.web.WebTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Transforms an instance of {@link Layoutml} to {@link WebLayout}.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public abstract class LayoutmlToWeblayoutTransformer extends AbstractLayoutmlTransformer<WebLayout> {

	private static final Logger log = LoggerFactory.getLogger(LayoutmlToWeblayoutTransformer.class);

	private final AttributeIdFactory attributeIdFactory = new AttributeIdFactory();

	protected final ObjectFactory factory;
	protected WebComponentStructure webComponentStructure;

	public LayoutmlToWeblayoutTransformer(
			final IRigidMetaProvider metaProvider,
			final ILanguageLocator localeDelegate,
			final Layoutml layoutml
	) {
		super(metaProvider, localeDelegate, layoutml);

		factory = new ObjectFactory();
	}

	/**
	 * Performs a specific transformation, to be implemented by subclasses.
	 */
	public abstract WebLayout internalTransform();

	/**
	 * Performs the transformation and returns the Weblayout.
	 */
	public final WebLayout transform() {
		webComponentStructure = createWebComponentStructure();

		WebLayout webLayout = internalTransform();

		List<WebComponent> intialList = getInputComponentsWithoutNextFocusField();
		for (int i = 0; i < intialList.size(); i++) {
			if ((i + 1) < intialList.size()) {
				intialList.get(i).setNextFocusField(intialList.get(i + 1).getName());
			} else {
				intialList.get(i).setNextFocusField(intialList.get(0).getName());
			}
		}

		setInitialFocus(webLayout);

		return webLayout;
	}

	private void setInitialFocus(WebLayout webLayout) {
		if (layoutml.getLayout().getInitialFocusComponent() != null) {
			String focusComponentUidString = layoutml.getLayout().getInitialFocusComponent().getName();
			String name = getFieldName(focusComponentUidString);
			final String entity = layoutml.getLayout().getInitialFocusComponent().getEntity();
			if (!StringUtils.isNullOrEmpty(focusComponentUidString) && focusComponentUidString.equals(name)) {
				log.warn(MessageFormat.format("The initial focus field {0} of your layout does not exist in the system.", focusComponentUidString));
			}
			if (name == null) {
				name = layoutml.getLayout().getInitialFocusComponent().getComponent();
			}
			final String finalName = name;

			if (entity == null) {
				webComponentStructure.getComponentsAsFlatList().stream()
						.filter(comp -> comp instanceof WebInputComponent && Objects.equals(comp.getName(), finalName))
						.findFirst()
						.ifPresent(comp -> comp.setInitialFocus(true));
			} else {
				//set focus on subform column
				webComponentStructure.getComponentsAsFlatList().stream()
					.forEach(comp -> {
						if (comp instanceof WebSubform) {
							WebSubform webSubform = (WebSubform) comp;
							if (Objects.equals(webSubform.getEntity(), entity)) {
								webSubform.getSubformColumns().stream()
										.filter(col -> Objects.equals(col.getName(), finalName))
										.forEach(col -> col.setInitialFocus(true));
							}
						}
					});
			}
		}
	}
/**
	 * This magic code seems complex on first view but follows the complex layout rule from the editor:
	 *
	 * The already available componentsFlat on WebComponentStructure does not respect Column/Rows sorting from Panels.
	 * Within Panels the column/row starts over again, so sorting this flat list would lead to wrong tab focus field
	 * information.
	 *
	 * What is done in this function is, we take the first top component which should always be a {@link WebContainer},
	 * below that we've got a bunch of cells and components `calculated.cells.components` we do collect every element
	 * we can find here and sort them after column and row. This should result in a top level view of our layout.
	 *
	 * As this findAll returns us all everything below components which is a list we need to flatten this list.
	 * After that we collect every {@link WebInputComponent}, if we found a {@link WebContainer} then we need to collect
	 * all components from it.
	 *
	 * Again this list needs to be flattened as if we found some {@link WebContainer} we've got a List of
	 * {@link WebComponent} instead of a single {@link WebComponent}.
	 *
	 * The last findAll condition is to filter for {@link WebComponent} which do not have a nextFocusField set.
	 * @return
	 */
	private List<WebComponent> getInputComponentsWithoutNextFocusField() {
		if (webComponentStructure.getComponentsStructured().size() == 1 &&
				webComponentStructure.getComponentsStructured().get(0) instanceof WebContainer) {
			WebContainer webContainer = (WebContainer) webComponentStructure.getComponentsStructured().get(0);
			return _getInputComponentsWithoutNextFocusField(webContainer);
		}

		return Collections.emptyList();
	}

	private static class WebComponentRowColumnComparator implements Comparator<WebComponent> {
		@Override
		public int compare(final WebComponent c1, final WebComponent c2) {
			if (Objects.equals(c1.getRow(), c2.getRow())) {
				return Objects.compare(c1.getColumn(), c2.getColumn(), Comparator.nullsFirst(Comparator.naturalOrder()));
			}
			return Objects.compare(c1.getRow(), c2.getRow(), Comparator.nullsFirst(Comparator.naturalOrder()));
		}
	}

	private List<WebComponent> _getInputComponentsWithoutNextFocusField(WebContainer webContainer) {
		if (webContainer.getCalculated() != null) {
			return RigidUtils.uncheckedCast(webContainer
					.getCalculated().getCells().stream()
					.filter(Objects::nonNull)
					.flatMap(comps -> comps.getComponents().stream().filter(Objects::nonNull))
					.sorted(new WebComponentRowColumnComparator())
					.map(it -> {
						if ((it instanceof WebInputComponent || it instanceof WebHtmlField) &&
								!(it instanceof WebSubform) &&
								!(it instanceof WebMatrix) &&
								!(it instanceof WebTabcontainer) &&
								it.getName() != null) {
							return Stream.of(it);
						} else if (it instanceof WebSplitpane) {
							return ((WebSplitpane) it).getComponents().stream().filter(Objects::nonNull);
						} else if (it instanceof WebContainer) {
							WebContainer webContainer2 = (WebContainer) it;
							if (webContainer2.getCalculated() != null
									&& !webContainer2.getCalculated().getCells().isEmpty()
									&& !webContainer2.getCalculated().getCells().get(0).getComponents().isEmpty()
									&& webContainer2.getCalculated().getCells().get(0).getComponents().get(0) instanceof WebContainer
									&& !(webContainer2.getCalculated().getCells().get(0).getComponents().get(0) instanceof WebSplitpane)) {
								return ((WebContainer) webContainer2.getCalculated().getCells().get(0).getComponents().get(0)).getCalculated().getCells().stream()
										.map(it2 -> it2.getComponents().isEmpty() ? null : it2.getComponents().get(0))
										.filter(Objects::nonNull);
							} else {
								return webContainer2.getCalculated().getCells().stream()
										.filter(Objects::nonNull)
										.flatMap(cell -> cell.getComponents().stream().filter(Objects::nonNull))
										.sorted(new WebComponentRowColumnComparator());
							}
						} else if (it instanceof WebTabcontainer) {
							final WebTabcontainer webTabcontainer = (WebTabcontainer) it;
							return webTabcontainer.getTabs().stream().map(tab -> {
								final WebComponent tabContent = tab.getContent();
								if ((tabContent instanceof WebInputComponent || tabContent instanceof WebHtmlField) &&
										!(tabContent instanceof WebSubform) &&
										!(tabContent instanceof WebMatrix) &&
										!(tabContent instanceof WebTabcontainer) &&
										tabContent.getName() != null) {
									return Stream.of(it);
								} else if (tabContent instanceof WebContainer) {
									return _getInputComponentsWithoutNextFocusField((WebContainer) tabContent).stream();
								} else {
									return null;
								}
							})
									.filter(Objects::nonNull)
									.flatMap(Function.identity());
						} else {
							return null;
						}
					})
					.filter(Objects::nonNull)
					.flatMap(Function.identity())
					.filter(it -> (it instanceof WebInputComponent || it instanceof WebHtmlField) && ((WebComponent) it).getNextFocusField() == null && ((WebComponent) it).getNextFocusComponent() == null)
					.collect(Collectors.toList()));
		}

		return Collections.emptyList();
	}

	/**
	 * Recursively searches for all input nonCloneableFields.
	 */
	private WebComponentStructure createWebComponentStructure() {
		WebComponentStructure structure = new WebComponentStructure();

		layoutml.getLayout().getPanel().getContainerOrLabelOrTextfield().forEach(it -> {
			structure.addAll(getWebComponents(it));
		});

		return structure;
	}

	/**
	 * Tries to lookup the field name for the given UID string of the form "uid{...}".
	 *
	 * @param uidString
	 * @return The field name if found, or the given uidString if not found.
	 */
	DataType getFieldType(String uidString) {
		UID uid = UID.parseUID(uidString);
		if (uid != null) {
			FieldMeta<?> fieldMeta = metaProvider.getEntityField(uid);
			if (fieldMeta != null && Objects.equals(fieldMeta.getForeignEntity(), E.DOCUMENTFILE.getUID())) {
				return DataType.FILE;
			}
		}
		return null;
	}

	/**
	 * Recursively visits and transforms all components in the given (JAXB-) element
	 * to the corresponding WebLayout components.
	 */
	public List<WebComponent> getWebComponents(Object element) {
		List<WebComponent> result = new ArrayList<>();

		if (element instanceof JAXBElement) {
			element = ((JAXBElement) element).getValue();
		}

		ElementTransformer<?, ? extends WebComponent> transformer = TransformerRegistry.lookup(element.getClass());
		if (transformer != null) {
			WebComponent component = transformer.transform(this, element);
			if (component != null) {
				transferConstraints(element, component);
				transferAdvancedProperties(element, component);

				// TODO: Wrong place. There is a SubformTransformer for such stuff!
				if (element instanceof Subform) {
					((Subform)element).getSubformColumn().forEach(it -> {
						if (component instanceof WebSubform) {
							List<WebSubformColumn> subformColumns = ((WebSubform) component).getSubformColumns();
							subformColumns.stream()
									.filter(it2 -> Objects.equals(it2.getName(), it.getName()))
									.findFirst()
									.ifPresent(webSubformColumn -> transferAdvancedProperties(it, webSubformColumn));
						}
					});
				}

				result.add(component);
			}
		} else {
			log.warn("No transformer for element: {}", element);
		}

		return result;
	}


	WebComponent transferAdvancedProperties(final Object element, final WebComponent component) {
		List<WebAdvancedProperty> properties = getAdvancedProperties(element);

		if (!properties.isEmpty()) {
			component.getAdvancedProperties().addAll(properties);
		}

		return component;
	}

	WebSubformColumn transferAdvancedProperties(final Object element, final WebSubformColumn component) {
		List<WebAdvancedProperty> properties = getAdvancedProperties(element);

		if (!properties.isEmpty()) {
			component.getAdvancedProperties().addAll(properties);
		}

		return component;
	}

	private List<WebAdvancedProperty> getAdvancedProperties(Object element) {
		List<WebAdvancedProperty> result = new ArrayList<>();

		try {
			final Method getPropertyMethod = element.getClass().getMethod("getProperty");
			final Object properties = getPropertyMethod.invoke(element);
			if (properties instanceof List) {
				List<Property> propertiesList = RigidUtils.uncheckedCast(properties);
				if (!propertiesList.isEmpty()) {
					result.addAll(propertiesList.stream().map(it -> {
						WebAdvancedProperty property = factory.createWebAdvancedProperty();
						property.setName(((Property) it).getName());
						property.setValue(((Property) it).getValue());
						return property;
					}).collect(Collectors.toList()));
				}
			}
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			// ignore
		}

		return result;
	}

	/**
	 * Tries to extract the old tablelayout constraints from the given LayoutML element
	 * and to transfer them to the given WebLayout component.
	 *
	 * @param element
	 * @param component
	 * @return
	 */
	WebComponent transferConstraints(Object element, WebComponent component) {
		try {
			final Method method = element.getClass().getMethod("getLayoutconstraints");
			final JAXBElement oldConstraints = (JAXBElement) method.invoke(element);
			LayoutmlToWeblayoutTransformer.LayoutConstraints constraints = LayoutmlToWeblayoutTransformer.LayoutConstraints.from(oldConstraints);
			if (constraints != null) {
				component.setRow(constraints.row);
				component.setColumn(constraints.column);
				if (constraints.rowspan.intValue() > 1) {
					component.setRowspan(constraints.rowspan);
				}
				if (constraints.colspan.intValue() > 1) {
					component.setColspan(constraints.colspan);
				}
			}
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			log.warn("Could not transfer layout constraints from element: {}", element);
		}

		try {
			final Method method1 = element.getClass().getMethod("getEnabled");
			final Method method2 = component.getClass().getMethod("isEnabled");
			final Method method3 = component.getClass().getMethod("setEnabled", java.lang.Boolean.class);
			final Boolean bSourceEnabled = (Boolean) method1.invoke(element);
			final Object bTargetEnabled = method2.invoke(component);
			if (bTargetEnabled == null) {
				// If there is an 'enabled' attribute, transfer it.
				// Do not overwrite, if it already exists on the target element.
				method3.invoke(component, RigidUtils.defaultIfNull(bSourceEnabled, Boolean.YES) == Boolean.YES);
			}
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			// ignore
		}

		return component;
	}

	/**
	 * Populates the grid with rows and columns based on the row/column properties of the given components.
	 * Always uses 12 columns.
	 *
	 * @param grid
	 * @param components
	 * @return
	 */
	public WebGrid populateGrid(WebGrid grid, List<WebComponent> components) {
		grid.getRows().addAll(getRows(components, 12));

		return grid;
	}

	/**
	 * Populates the table with rows and columns based on the row/column properties of the given components.
	 * Determines the columns based on the 'column' and 'colspan' properties of the components.
	 *
	 * @param table
	 * @param components
	 * @return
	 */
	public WebTable populateTable(WebTable table, List<WebComponent> components) {
		BigInteger columnCount = components.stream()
				.map(it -> RigidUtils.defaultIfNull(it.getColumn(), BigInteger.ZERO).add(it.getColspan() != null ? it.getColspan() : BigInteger.ONE))
				.max(Comparator.naturalOrder())
				.orElse(null);

		if (columnCount != null) {
			table.getRows().addAll(getRows(components, columnCount.intValue()));
		} else {
			log.warn("No columns for table {}", table);
		}

		return table;
	}

	/**
	 * Creates a list of rows filled with the given components.
	 * Each row has exactly columnCount (possibly empty) columns.
	 *
	 * @param components
	 * @param columnCount
	 * @return
	 */
	List<WebRow> getRows(List<WebComponent> components, Integer columnCount) {
		List<WebRow> result = new ArrayList<>();

		BigInteger rows = null;
		for (WebComponent it : components) {
			if (it != null && it.getRow() != null && it.getRow().compareTo(RigidUtils.defaultIfNull(rows, BigInteger.ZERO)) > 0) {
				rows = it.getRow();
			}
		}

		ComponentMatrix matrix = new ComponentMatrix(components);

		if (rows != null) {
			for (long rowNum = 1; rowNum <= rows.longValue(); rowNum++) {
				WebRow row = factory.createWebRow();
				result.add(row);

				// Each column can contain multiple components
				Map<BigInteger, WebComponent> rowColumns = new HashMap<>();

				final long finalRowNum = rowNum;
				components.stream().filter(it -> Objects.equals(it.getRow(), BigInteger.valueOf(finalRowNum)))
						.forEach(it -> {
					if (rowColumns.containsKey(it.getColumn())) {
						log.warn("Ignoring duplicate component in row {}, column {}", BigInteger.valueOf(finalRowNum), it.getColumn());
						return;
					}
					rowColumns.put(it.getColumn(), it);
				});

				WebComponent previousComponent = null;
				for (long colNum = 1; colNum <= columnCount.longValue(); colNum++) {
					if (isCoveredByPreviousComponent(BigInteger.valueOf(colNum), previousComponent)) {
						continue;
					}

					WebComponent columnComponent = rowColumns.get(BigInteger.valueOf(colNum));
					if (columnComponent != null) {
						WebCell cell = factory.createWebCell();
						if (columnComponent.getColumn() != null && columnComponent.getColumn().intValue() > 1) {
							cell.setColspan(columnComponent.getColspan());
						}
						if (columnComponent.getRowspan() != null && columnComponent.getRowspan().intValue() > 1) {
							cell.setRowspan(columnComponent.getRowspan());
						}
						cell.getComponents().add(columnComponent);
						previousComponent = columnComponent;
						row.getCells().add(cell);
					} else if (!matrix.isFilled(rowNum, colNum)) {
						// TODO: Use a single empty cell with colspan/rowspan instead of multiple adjacent empty cells
						WebCell cell = factory.createWebCell();
						row.getCells().add(cell);
					}
				}
			}
		} else if (components.size() == 1) {
			WebRow row = factory.createWebRow();
			result.add(row);
			WebCell cell = factory.createWebCell();
			if (columnCount > 1) {
				cell.setColspan(BigInteger.valueOf(columnCount.longValue()));
			}
			cell.getComponents().add(components.get(0));
			row.getCells().add(cell);
		}

		return result;
	}

	protected boolean isCoveredByPreviousComponent(BigInteger colNum, WebComponent previousComponent) {
		if (previousComponent != null) {
			BigInteger previousColspan = RigidUtils.defaultIfNull(previousComponent.getColspan(), BigInteger.ONE);
			return RigidUtils.defaultIfNull(previousComponent.getColumn(), BigInteger.ZERO).intValue() + previousColspan.intValue() > colNum.intValue();
		}
		return false;
	}

	public int getNewAttributeId(String attributeName) {
		return attributeIdFactory.getNewId(attributeName);
	}

	public Integer getExistingAttributeId(String attributeName) {
		return attributeIdFactory.getExistingId(attributeName);
	}

	static class LayoutConstraints {
		BigInteger row;
		BigInteger column;
		BigInteger rowspan;
		BigInteger colspan;

		static LayoutConstraints from(JAXBElement element) {
			if (element == null) {
				log.warn("Unknown LayoutConstraints: {}", element);
				return null;
			}
			Object value = element.getValue();
			if (value instanceof TablelayoutConstraints) {
				TablelayoutConstraints constraints = (TablelayoutConstraints) value;
				final LayoutConstraints newConstraints = new LayoutConstraints();
				newConstraints.row = new BigInteger(constraints.getRow1());
				newConstraints.column = new BigInteger(constraints.getCol1());
				newConstraints.rowspan = new BigInteger(constraints.getRow2()).subtract(newConstraints.row).add(BigInteger.ONE);
				newConstraints.colspan = new BigInteger(constraints.getCol2()).subtract(newConstraints.column).add(BigInteger.ONE);
				return newConstraints;
			}

			log.warn("Unknown LayoutConstraints: {}", element);

			return null;
		}
	}

	enum DataType {
		FILE
	}

	/**
	 * Represents the state (filled with a component or empty) of every cell
	 * in a grid or table layout.
	 */
	static class ComponentMatrix {
		final Map<BigInteger, BitSet> rows = new TreeMap<>();

		ComponentMatrix(List<WebComponent> components) {
			components.forEach(component -> {

				BigInteger rowspan = RigidUtils.defaultIfNull(component.getRowspan(), BigInteger.ONE);
				for (long it = 0; it < rowspan.longValue(); it++) {
					BitSet row = getRow(component.getRow().longValue() + it);
					BigInteger colspan = RigidUtils.defaultIfNull(component.getColspan(), BigInteger.ONE);
					row.set(component.getColumn().intValue(), component.getColumn().intValue() + colspan.intValue());
				}
			});
		}

		/**
		 * Determines if the cell at the given row/col is filled by a component.
		 *
		 * @param row
		 * @param col
		 * @return
		 */
		boolean isFilled(long row, long col) {
			final BitSet bitSet = rows.get(BigInteger.valueOf(row));
			if (bitSet != null) {
				return bitSet.get(BigInteger.valueOf(col).intValue());
			}
			return false;
		}

		BitSet getRow(long index) {
			final BigInteger bigInteger = BigInteger.valueOf(index);
			if (rows.get(bigInteger) == null) {
				rows.put(bigInteger, new BitSet());
			}
			return rows.get(bigInteger);
		}

		public String toString() {
			return rows.values().stream().filter(Objects::nonNull).map(Objects::toString).collect(Collectors.joining("\n"));
		}
	}
}
