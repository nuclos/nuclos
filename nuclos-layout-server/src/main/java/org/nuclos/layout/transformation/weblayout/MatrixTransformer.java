package org.nuclos.layout.transformation.weblayout;

import java.util.Optional;

import org.nuclos.schema.layout.layoutml.Matrix;
import org.nuclos.schema.layout.layoutml.MatrixColumn;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebMatrix;
import org.nuclos.schema.layout.web.WebMatrixColumn;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class MatrixTransformer extends ElementTransformer<Matrix, WebComponent> {

	MatrixTransformer() {
		super(Matrix.class);
	}

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Matrix matrix = (Matrix) obj;
		WebMatrix result = factory.createWebMatrix();
		copyProperties(matrix, result);

		matrix.getMatrixColumn().forEach(it -> {
			// Skip invisible matrix columns
			if (it.getVisible() == org.nuclos.schema.layout.layoutml.Boolean.NO) {
				return;
			}

			WebMatrixColumn matrixColumn = factory.createWebMatrixColumn();
			copyProperties(it, matrixColumn);
			result.getMatrixColumns().add(matrixColumn);
		});

		return result;
	}

	private void copyProperties(Matrix fromObject, WebMatrix toObject) {
		if (fromObject != null && toObject != null) {
			toObject.setName(fromObject.getName());
			toObject.setMatrixPreferencesField(fromObject.getMatrixPreferencesField());
			toObject.setEntityX(fromObject.getEntityX());
			toObject.setEntityY(fromObject.getEntityY());
			toObject.setEntityMatrix(fromObject.getEntityMatrix());
			toObject.setEntityFieldMatrixParent(fromObject.getEntityFieldMatrixParent());
			toObject.setEntityFieldMatrixXRefField(fromObject.getEntityFieldMatrixXRefField());
			toObject.setEntityMatrixValueField(fromObject.getEntityMatrixValueField());
			toObject.setEntityMatrixNumberState(fromObject.getEntityMatrixNumberState());
			toObject.setEntityFieldCategorie(fromObject.getEntityFieldCategorie());
			toObject.setEntityFieldX(fromObject.getEntityFieldX());
			toObject.setEntityYParentField(fromObject.getEntityYParentField());
			toObject.setEntityFieldY(fromObject.getEntityFieldY());
			toObject.setEntityXSortingFields(fromObject.getEntityXSortingFields());
			toObject.setEntityYSortingFields(fromObject.getEntityYSortingFields());
			toObject.setEntityXHiddenField(fromObject.getEntityXHiddenField());
			toObject.setEntityYHiddenField(fromObject.getEntityYHiddenField());
			toObject.setEntityXHeader(fromObject.getEntityXHeader());
			toObject.setEntityYHeader(fromObject.getEntityYHeader());
			toObject.setEntityMatrixReferenceField(fromObject.getEntityMatrixReferenceField());
			toObject.setEntityMatrixValueType(fromObject.getEntityMatrixValueType());
			toObject.setEntityXVlpId(fromObject.getEntityXVlpId());
			toObject.setEntityXVlpIdfieldname(fromObject.getEntityXVlpIdfieldname());
			toObject.setEntityXVlpFieldname(fromObject.getEntityXVlpFieldname());
			toObject.setEntityXVlpReferenceParamName(fromObject.getEntityXVlpReferenceParamName());
			toObject.setCellInputType(fromObject.getCellInputType());
		}
	}

	private void copyProperties(MatrixColumn fromObject, WebMatrixColumn toObject) {
		if (fromObject != null && toObject != null) {
			toObject.setName(fromObject.getName());
			toObject.setLabel(fromObject.getLabel());
			toObject.setControltype(objectToString(fromObject.getControltype()));
			toObject.setControltypeclass(fromObject.getControltypeclass());
			toObject.setEnabled(objectToString(fromObject.getEnabled().toString()));
			toObject.setNotcloneable(objectToString(fromObject.getNotcloneable().toString()));
			toObject.setInsertable(objectToString(fromObject.getInsertable().toString()));
			toObject.setRows(fromObject.getRows());
			toObject.setColumns(fromObject.getColumns());
			toObject.setResourceId(fromObject.getResourceId());
			toObject.setWidth(fromObject.getWidth());
			toObject.setNextFocusComponent(fromObject.getNextfocuscomponent());
			toObject.setNextFocusField(fromObject.getNextfocusfield());
		}
	}

	private String objectToString(Object o) {
		return Optional.ofNullable(o).map(Object::toString).orElse(null);
	}
}
