package org.nuclos.layout.transformation.weblayout;

import org.nuclos.schema.layout.layoutml.MultiselectionCombobox;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebMultiselectionCombobox;

public class MultiselectionComboboxTransformer extends ElementTransformer<MultiselectionCombobox, WebComponent> {

	MultiselectionComboboxTransformer() {
		super(MultiselectionCombobox.class);
	}

	@Override
	public WebComponent transform(final LayoutmlToWeblayoutTransformer layoutTransformer, final Object input) {
		MultiselectionCombobox multiselectionCombobox = (MultiselectionCombobox) input;
		WebMultiselectionCombobox result = layoutTransformer.factory.createWebMultiselectionCombobox();

		if (multiselectionCombobox.getNextfocusfield() != null) {
			String nextFocusFieldName = layoutTransformer.getFieldName(multiselectionCombobox.getNextfocusfield());
			if (nextFocusFieldName != null) {
				result.setNextFocusField(nextFocusFieldName);
			}
		}

		if (multiselectionCombobox.getNextfocuscomponent() != null) {
			result.setNextFocusComponent(multiselectionCombobox.getNextfocuscomponent());
		}

		if (multiselectionCombobox.getDescriptionAttr() != null) {
			String descriptionAttr = layoutTransformer.getFieldName(multiselectionCombobox.getDescriptionAttr());
			if (descriptionAttr != null) {
				result.setDescriptionAttr(descriptionAttr);
			}
		}

		setSizeAndAlign(
				multiselectionCombobox.getPreferredSize(),
				multiselectionCombobox.getMinimumSize(),
				multiselectionCombobox.getLayoutconstraints(),
				result
		);
		transferValuelistProvider(result, multiselectionCombobox.getValuelistProvider());
		result.setName(multiselectionCombobox.getName());
		result.setAlternativeTooltip(multiselectionCombobox.getDescription());
		result.setCustomBackgroundColor(convertBackground(multiselectionCombobox.getBackground()));
		result.setEntity(multiselectionCombobox.getEntity());
		result.setForeignkeyfieldToParent(multiselectionCombobox.getForeignkeyfieldToParent());
		result.setLabelAttr(multiselectionCombobox.getLabelAttr());

		return result;
	}

}
