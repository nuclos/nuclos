package org.nuclos.layout.transformation.weblayout;

import static org.nuclos.schema.layout.layoutml.Boolean.NO;

import org.nuclos.schema.layout.layoutml.PlanningTable;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebPlanningTable;

class PlanningTableTransformer extends ElementTransformer<PlanningTable, WebComponent> {

	PlanningTableTransformer() {
		super(PlanningTable.class);
	}

	@Override
	public WebComponent transform(final LayoutmlToWeblayoutTransformer layoutTransformer, final Object input) {
		PlanningTable planningTable = (PlanningTable) input;
		WebPlanningTable result = layoutTransformer.factory.createWebPlanningTable();

		result.setName(planningTable.getName());
		result.setPlanningTable(planningTable.getPlanningTable());
		result.setForeignkeyfieldToParent(planningTable.getForeignkeyfieldToParent());
		result.setEnabled(planningTable.getEnabled() == null || (NO != planningTable.getEnabled()));
		result.setDateFrom(planningTable.getDateFrom());
		result.setDateUntil(planningTable.getDateUntil());
		result.setFieldDateFrom(planningTable.getFieldDateFrom());
		result.setFieldDateUntil(planningTable.getFieldDateUntil());

		setSizeAndAlign(
				planningTable.getPreferredSize(),
				planningTable.getMinimumSize(),
				planningTable.getLayoutconstraints(),
				result
		);

		return result;
	}
}
