package org.nuclos.layout.transformation.weblayout;


import org.nuclos.layout.transformation.weblayout.calculated.LayoutmlToWeblayoutCalcTransformer;
import org.nuclos.schema.layout.layoutml.Scrollpane;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebScrollpane;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class ScrollpaneTransformer extends ElementTransformer<Scrollpane, WebComponent> {

	ScrollpaneTransformer() {
		super(Scrollpane.class);
	}

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Scrollpane scrollpane = (Scrollpane) obj;
		WebScrollpane result = factory.createWebScrollpane();

		result.setHorizontalscrollbar(scrollpane.getHorizontalscrollbar());
		result.setVerticalscrollbar(scrollpane.getVerticalscrollbar());

		result.setCalculated(factory.createWebGridCalculated());
		result.setName(scrollpane.getName());

		((LayoutmlToWeblayoutCalcTransformer) layoutTransformer).populateGridCalculated(
				result.getCalculated(),
				layoutTransformer.getWebComponents(scrollpane.getContainer()),
				null
		);

		if (scrollpane.getLineBorder() != null) {
			setBorderProperty(
					result,
					scrollpane.getLineBorder().getRed(),
					scrollpane.getLineBorder().getGreen(),
					scrollpane.getLineBorder().getBlue(),
					scrollpane.getLineBorder().getThickness(),
					"solid"
			);
		}

		setSizeAndAlign(
				scrollpane.getPreferredSize(),
				scrollpane.getMinimumSize(),
				scrollpane.getLayoutconstraints(),
				result
		);

		return result;
	}
}
