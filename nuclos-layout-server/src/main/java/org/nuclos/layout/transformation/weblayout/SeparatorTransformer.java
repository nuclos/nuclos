package org.nuclos.layout.transformation.weblayout;

import org.nuclos.schema.layout.layoutml.Separator;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebSeparator;

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
class SeparatorTransformer extends ElementTransformer<Separator, WebComponent> {

	SeparatorTransformer() {
		super(Separator.class);
	}

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Separator separator = (Separator) obj;
		WebSeparator result = factory.createWebSeparator();
		result.setOrientation(separator.getOrientation());
		return result;
	}

}
