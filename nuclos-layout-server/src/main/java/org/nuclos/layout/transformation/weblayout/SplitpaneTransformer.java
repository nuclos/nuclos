package org.nuclos.layout.transformation.weblayout;

import java.math.BigInteger;

import javax.xml.bind.JAXBElement;

import org.nuclos.schema.layout.layoutml.Splitpane;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebSplitpane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class SplitpaneTransformer extends ElementTransformer<Splitpane, WebComponent> {
	private static final Logger log = LoggerFactory.getLogger(SplitpaneTransformer.class);

	SplitpaneTransformer() {
		super(Splitpane.class);
	}

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Splitpane splitpane = (Splitpane) obj;
		WebSplitpane result = factory.createWebSplitpane();

		addPaneComponent(result, layoutTransformer, splitpane.getContainer().stream().findFirst().map(JAXBElement::getValue).orElse(null));
		addPaneComponent(result, layoutTransformer, splitpane.getContainer().stream().reduce((first, second) -> second).map(JAXBElement::getValue).orElse(null));

		result.setDividerOrientation(splitpane.getOrientation());
		result.setName(splitpane.getName());
		result.setDividerSize(new BigInteger(splitpane.getDividersize() == null || splitpane.getDividersize().isEmpty() ? "4" : splitpane.getDividersize()));
		result.setDefaultResizeWeight(new Float(splitpane.getResizeweight() == null || splitpane.getResizeweight().isEmpty() ? "0.5" : splitpane.getResizeweight()));
		result.setCollapsable(org.nuclos.schema.layout.layoutml.Boolean.YES == splitpane.getExpandable());

		if (splitpane.getLineBorder() != null) {
			setBorderProperty(
					result,
					splitpane.getLineBorder().getRed(),
					splitpane.getLineBorder().getGreen(),
					splitpane.getLineBorder().getBlue(),
					splitpane.getLineBorder().getThickness(),
					"solid"
			);
		}

		setSizeAndAlign(
				splitpane.getPreferredSize(),
				splitpane.getMinimumSize(),
				splitpane.getLayoutconstraints(),
				result
		);

		return result;
	}

	private void addPaneComponent(WebSplitpane result, LayoutmlToWeblayoutTransformer layoutTransformer, Object object) {
		try {
			ElementTransformer<?, ? extends WebComponent> transformer = TransformerRegistry.lookup(object.getClass());
			if (transformer != null) {
				result.getComponents().add(
						transformer.transform(layoutTransformer, object)
				);
			}
		} catch ( Exception e) {
			log.warn("Could not create pane from given object", object, e);
		}
	}
}
