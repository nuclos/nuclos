package org.nuclos.layout.transformation.weblayout;

import org.nuclos.schema.layout.layoutml.Boolean;
import org.nuclos.schema.layout.layoutml.Controltype;
import org.nuclos.schema.layout.layoutml.Subform;
import org.nuclos.schema.layout.web.SubformControlType;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebSubform;
import org.nuclos.schema.layout.web.WebSubformColumn;
import org.nuclos.schema.layout.web.WebToolbarOrientation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.lang.GroovyShell;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class SubformTransformer extends ElementTransformer<Subform, WebComponent> {
	private static final Logger log = LoggerFactory.getLogger(SubformTransformer.class);

	SubformTransformer() {
		super(Subform.class);
	}

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Subform subform = (Subform) obj;
		WebSubform result = factory.createWebSubform();

		result.setName(subform.getName());

		result.setAutonumberSorting(subform.getAutonumbersorting() == Boolean.YES);

		// TODO
		result.setControllerType(null);

		if (subform.getLineBorder() != null) {
			setBorderProperty(
					result,
					subform.getLineBorder().getRed(),
					subform.getLineBorder().getGreen(),
					subform.getLineBorder().getBlue(),
					subform.getLineBorder().getThickness(),
					"solid"
			);
		}
		result.setCustomBackgroundColor(convertBackground(subform.getBackground()));
		addDynamicallyEnabledButtons(subform, result);

		if (subform.getNextfocusfield() != null) {
			String nextFocusFieldName = layoutTransformer.getFieldName(subform.getNextfocusfield());
			if (nextFocusFieldName != null) {
				result.setNextFocusField(nextFocusFieldName);
			}
		}

		if (subform.getNextfocuscomponent() != null) {
			result.setNextFocusComponent(subform.getNextfocuscomponent());
		}

		result.setDynamicCellHeightsDefault(subform.getDynamicCellHeightsDefault() == Boolean.YES);
		result.setEntity(subform.getEntity());
		result.setForeignkeyfieldToParent(subform.getForeignkeyfieldToParent());
		result.setIgnoreSubLayout(subform.getIgnoreSubLayout() == Boolean.YES);
		result.setMultiEditable(subform.getMultieditable() == Boolean.YES);
		result.setNotCloneable(subform.getNotcloneable() == Boolean.YES);
		result.setOpaque(subform.getOpaque() == Boolean.YES);
		result.setParentSubform(subform.getParentSubform());
		if (subform.getToolbarorientation() != null) {
			result.setToolbarOrientation(WebToolbarOrientation.fromValue(subform.getToolbarorientation().toLowerCase()));
		}
		result.setUniqueMastercolumn(subform.getUniqueMastercolumn());

		subform.getSubformColumn().forEach(it -> {
			WebSubformColumn column = factory.createWebSubformColumn();

			column.setColumns(it.getColumns());
			column.setControlType(getControlType(it.getControltype()));
			column.setControlTypeClass(it.getControltypeclass());
			column.setCustomUsageSearch(it.getCustomusagesearch());
			column.setEnabled(it.getEnabled() == Boolean.YES);
			column.setInsertable(it.getInsertable() == Boolean.YES);
			column.setLabel(it.getLabel());
			column.setName(it.getName());
			column.setNextFocusComponent(it.getNextfocuscomponent());
			column.setNextFocusField(it.getNextfocusfield());
			column.setNotCloneable(it.getNotcloneable() == Boolean.YES);
			column.setResourceId(it.getResourceId());
			column.setRows(it.getRows());
			column.setWidth(it.getWidth());
			column.setVisible(it.getVisible() == Boolean.YES);

			transferValuelistProvider(column, it.getValuelistProvider());

			result.getSubformColumns().add(column);
		});

		return result;
	}

	SubformControlType getControlType(final Controltype controltype) {
		try {
			return SubformControlType.fromValue(controltype.value().toUpperCase());
		} catch (IllegalArgumentException | NullPointerException ignored) {
			// Ignore
			return null;
		}
	}

	private void addDynamicallyEnabledButtons(Subform subform, WebSubform result) {
		if (subform.getNewEnabled() != null) {
			result.setNewEnabled(tryEval(subform.getNewEnabled().getContent()));
		}
		if (subform.getEditEnabled() != null) {
			result.setEditEnabled(tryEval(subform.getEditEnabled().getContent()));
		}
		if (subform.getCloneEnabled() != null) {
			result.setCloneEnabled(tryEval(subform.getCloneEnabled().getContent()));
		}
		if (subform.getDeleteEnabled() != null) {
			result.setDeleteEnabled(tryEval(subform.getDeleteEnabled().getContent()));
		}
	}

	private java.lang.Boolean tryEval(String expression) {
		java.lang.Boolean result = null;

		try {
			result = (java.lang.Boolean) new GroovyShell().evaluate(expression);
		} catch (Exception e) {
			log.warn("Failed to evaluate expression", e);
		}

		return result;
	}
}
