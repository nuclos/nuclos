package org.nuclos.layout.transformation.weblayout;

import java.util.Objects;

import org.nuclos.schema.layout.layoutml.Panel;
import org.nuclos.schema.layout.layoutml.Tabbedpane;
import org.nuclos.schema.layout.layoutml.TabbedpaneConstraints;
import org.nuclos.schema.layout.layoutml.Translation;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebTab;
import org.nuclos.schema.layout.web.WebTabcontainer;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class TabbedpaneTransformer extends ElementTransformer<Tabbedpane, WebComponent> {

	TabbedpaneTransformer() {
		super(Tabbedpane.class);
	}

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Tabbedpane tabbedPane = (Tabbedpane) obj;
		WebTabcontainer result = factory.createWebTabcontainer();

		result.setName(tabbedPane.getName());

		tabbedPane.getContainer().forEach(element ->
			layoutTransformer.getWebComponents(element).forEach(it -> {
				WebTab tab = factory.createWebTab();
				Panel panel = (Panel) element.getValue();
				TabbedpaneConstraints constraints = (TabbedpaneConstraints) panel.getLayoutconstraints().getValue();
				String title = null;
				if (constraints.getTranslations() != null
						&& constraints.getTranslations().getTranslation() != null
						&& !constraints.getTranslations().getTranslation().isEmpty()
						&& layoutTransformer.getLocaleLanguage() != null) {
					title = constraints.getTranslations().getTranslation().stream().filter(translation ->
							Objects.equals(translation.getLang(), layoutTransformer.getLocaleLanguage())).findAny().map(Translation::getText).orElse(null);
				}
				if (title == null) {
					title = constraints.getTitle();
				}
				String internalName = constraints.getInternalname();
				String titleAttr = layoutTransformer.getFieldName(constraints.getTitleAttribute());
				String visibilityAttr = layoutTransformer.getFieldName(constraints.getVisibilityAttribute());
				String background;
				if (constraints.getHeaderColor() != null) {
					background = constraints.getHeaderColor();
				} else {
					background = "";
				}
				tab.setTitle(title);
				tab.setInternalName(internalName);
				tab.setTitleAttribute(titleAttr);
				tab.setVisibilityAttribute(visibilityAttr);
				tab.setHeaderColor(background);
				tab.setContent(it);

				result.getTabs().add(tab);
			})
		);

		if (tabbedPane.getNextfocusfield() != null) {
			String nextFocusFieldName = layoutTransformer.getFieldName(tabbedPane.getNextfocusfield());
			if (nextFocusFieldName != null) {
				result.setNextFocusField(nextFocusFieldName);
			}
		}

		if (tabbedPane.getNextfocuscomponent() != null) {
			result.setNextFocusComponent(tabbedPane.getNextfocuscomponent());
		}

		return result;
	}
}
