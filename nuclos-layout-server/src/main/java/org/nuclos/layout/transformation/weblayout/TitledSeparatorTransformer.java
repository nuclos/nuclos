package org.nuclos.layout.transformation.weblayout;

import java.util.Objects;

import org.nuclos.schema.layout.layoutml.TitledSeparator;
import org.nuclos.schema.layout.layoutml.Translation;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebTitledSeparator;

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
class TitledSeparatorTransformer extends ElementTransformer<TitledSeparator, WebComponent> {

	TitledSeparatorTransformer() {
		super(TitledSeparator.class);
	}

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		TitledSeparator titledSeparator = (TitledSeparator) obj;
		WebTitledSeparator result = factory.createWebTitledSeparator();

		String title = null;
		if (titledSeparator.getTranslations() != null
				&& titledSeparator.getTranslations().getTranslation() != null
				&& !titledSeparator.getTranslations().getTranslation().isEmpty()
				&& layoutTransformer.getLocaleLanguage() != null) {
			title = titledSeparator.getTranslations().getTranslation().stream().filter(translation ->
					Objects.equals(translation.getLang(), layoutTransformer.getLocaleLanguage()))
					.findAny().map(Translation::getText).orElse(null);
		}
		if (title == null) {
			title = titledSeparator.getTitle();
		}
		result.setTitle(titledSeparator.getTitle());
		return result;
	}

}
