package org.nuclos.layout.transformation.weblayout;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.schema.layout.layoutml.Button;
import org.nuclos.schema.layout.layoutml.CollectableComponent;
import org.nuclos.schema.layout.layoutml.Htmlfield;
import org.nuclos.schema.layout.layoutml.Label;
import org.nuclos.schema.layout.layoutml.Matrix;
import org.nuclos.schema.layout.layoutml.MultiselectionCombobox;
import org.nuclos.schema.layout.layoutml.PlanningTable;
import org.nuclos.schema.layout.layoutml.Separator;
import org.nuclos.schema.layout.layoutml.Splitpane;
import org.nuclos.schema.layout.layoutml.Subform;
import org.nuclos.schema.layout.layoutml.Tabbedpane;
import org.nuclos.schema.layout.layoutml.TitledSeparator;
import org.nuclos.schema.layout.layoutml.Webaddon;
import org.nuclos.schema.layout.layoutml.Scrollpane;
import org.nuclos.schema.layout.web.WebComponent;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class TransformerRegistry {
	static Map<Class, ElementTransformer> map = new HashMap<>();

	static {
		register(Tabbedpane.class, new TabbedpaneTransformer());
		register(Scrollpane.class, new ScrollpaneTransformer());
		register(Splitpane.class, new SplitpaneTransformer());
		register(CollectableComponent.class, new CollectableComponentTransformer());
		register(MultiselectionCombobox.class, new MultiselectionComboboxTransformer());
		register(Subform.class, new SubformTransformer());
		register(PlanningTable.class, new PlanningTableTransformer());
		register(Button.class, new ButtonTransformer());
		register(Matrix.class, new MatrixTransformer());
		register(Label.class, new LabelTransformer());
		register(Separator.class, new SeparatorTransformer());
		register(TitledSeparator.class, new TitledSeparatorTransformer());
		register(Webaddon.class, new WebaddonTransformer());
		register(Htmlfield.class, new HTMLTransformer());
	}

	public static <T> void register(Class<T> clss, ElementTransformer<T, ? extends WebComponent> transformer) {
		map.put(clss, transformer);
	}

	static <T> ElementTransformer<T, ? extends WebComponent> lookup(Class<T> clss) {
		return map.get(clss);
	}
}
