package org.nuclos.layout.transformation.weblayout;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebContainer;
import org.nuclos.schema.layout.web.WebGrid;
import org.nuclos.schema.layout.web.WebGridCalculated;
import org.nuclos.schema.layout.web.WebTab;
import org.nuclos.schema.layout.web.WebTabcontainer;
import org.nuclos.schema.layout.web.WebTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class WebComponentStructure {

	private static final Logger log = LoggerFactory.getLogger(WebComponentStructure.class);

	private final List<WebComponent> componentsStructured = new ArrayList<>();
	private final List<WebComponent> componentsFlat = new ArrayList<>();

	void addAll(final List<WebComponent> webComponents) {
		componentsStructured.addAll(webComponents);
		webComponents.forEach(it ->
			componentsFlat.addAll(findAllComponents(it)));
	}

	public List<WebComponent> getComponentsStructured() {
		return componentsStructured;
	}

	public List<WebComponent> getComponentsAsFlatList() {
		return componentsFlat;
	}

	protected List<WebComponent> findAllComponents(Object componentOrContainer) {
		List<WebComponent> result = new ArrayList<>();

		if (componentOrContainer instanceof WebComponent) {
			WebComponent component = (WebComponent) componentOrContainer;
			result.add(component);
			if (component instanceof WebContainer) {
				WebContainer container = (WebContainer) component;
				result.add(container);
				result.addAll(findAllComponents(container.getGrid()));
				result.addAll(findAllComponents(container.getTable()));
				result.addAll(findAllComponents(container.getCalculated()));
				container.getComponents().forEach(it -> {
					result.addAll(findAllComponents(it));
				});
			} else if (component instanceof WebTabcontainer) {
				WebTabcontainer container = (WebTabcontainer) component;
				result.add(container);
				container.getTabs().forEach(it ->
					result.addAll(findAllComponents(it)));
			}
		} else if (componentOrContainer instanceof WebGrid) {
			WebGrid grid = (WebGrid) componentOrContainer;
			grid.getRows().forEach(row ->
				row.getCells().forEach(cell ->
					cell.getComponents().forEach(it ->
						result.addAll(findAllComponents(it)))));
		} else if (componentOrContainer instanceof WebTable) {
			WebTable table = (WebTable) componentOrContainer;
			table.getRows().forEach(row ->
				row.getCells().forEach(cell ->
					cell.getComponents().forEach(it ->
						result.addAll(findAllComponents(it)))));
		} else if (componentOrContainer instanceof WebGridCalculated) {
			WebGridCalculated grid = (WebGridCalculated) componentOrContainer;
			grid.getCells().forEach(cell ->
				cell.getComponents().forEach(it ->
					result.addAll(findAllComponents(it))));
		} else if (componentOrContainer instanceof WebTab) {
			WebTab tab = (WebTab) componentOrContainer;
			result.addAll(findAllComponents(tab.getContent()));
		} else if (componentOrContainer != null) {
			log.warn("Unknown component: {}", componentOrContainer);
		}

		return result;
	}
}

