package org.nuclos.layout.transformation.weblayout;

import org.nuclos.schema.layout.layoutml.Property;
import org.nuclos.schema.layout.layoutml.Webaddon;
import org.nuclos.schema.layout.web.WebAddon;
import org.nuclos.schema.layout.web.WebAdvancedProperty;
import org.nuclos.schema.layout.web.WebComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class WebaddonTransformer extends ElementTransformer<Webaddon, WebComponent> {
	private static final Logger log = LoggerFactory.getLogger(WebaddonTransformer.class);

	WebaddonTransformer() {
		super(Webaddon.class);
	}

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Webaddon webaddon = (Webaddon) obj;
		WebAddon result = factory.createWebAddon();

		result.setId(webaddon.getId());
		result.setName(webaddon.getName());

		webaddon.getPropertyType().forEach(it -> {
			Object value = it.getValue();
			if (value instanceof Property) {
				Property desktopProperty = (Property) value;

				WebAdvancedProperty webProperty = factory.createWebAdvancedProperty();
				webProperty.setName(desktopProperty.getName());
				webProperty.setValue(desktopProperty.getValue());
				result.getAdvancedProperties().add(webProperty);
			}
		});

		setSizeAndAlign(
				webaddon.getPreferredSize(),
				webaddon.getMinimumSize(),
				webaddon.getLayoutconstraints(),
				result
		);

		return result;
	}
}
