package org.nuclos.layout.transformation.weblayout.calculated;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.layout.transformation.ILanguageLocator;
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer;
import org.nuclos.layout.transformation.weblayout.TransformerRegistry;
import org.nuclos.layout.transformation.weblayout.WebComponentStructure;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.nuclos.schema.layout.layoutml.Panel;
import org.nuclos.schema.layout.web.WebCalcCell;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebGridCalculated;
import org.nuclos.schema.layout.web.WebInputComponent;
import org.nuclos.schema.layout.web.WebLabel;
import org.nuclos.schema.layout.web.WebLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Transforms an instance of {@link Layoutml} to {@link WebLayout}.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class LayoutmlToWeblayoutCalcTransformer extends LayoutmlToWeblayoutTransformer {
	private static final Logger log = LoggerFactory.getLogger(LayoutmlToWeblayoutCalcTransformer.class);

	public LayoutmlToWeblayoutCalcTransformer(
			final IRigidMetaProvider metaProvider,
			final ILanguageLocator localeDelegate,
			final Layoutml layoutml
	) {
		super(metaProvider, localeDelegate, layoutml);

		TransformerRegistry.register(Panel.class, new PanelTransformer());
	}

	@Override
	public WebLayout internalTransform() {
		WebLayout result = factory.createWebLayout();

		result.setCalculated(factory.createWebGridCalculated());

		populateGridCalculated(result.getCalculated(), webComponentStructure.getComponentsStructured(), null);
		assignComponentIds(webComponentStructure);
		updateLabelsWithComponentIds(webComponentStructure);

		return result;
	}

	private List<WebComponent> assignComponentIds(WebComponentStructure structure) {
		return structure.getComponentsAsFlatList().stream()
				.filter(it -> WebInputComponent.class.isAssignableFrom(it.getClass()))
				.map(it -> {
					if (it.getName() != null) {
						int id = getNewAttributeId(it.getName());
						it.setId(layoutml.hashCode() + "-" + id);
					}
					return it;
				})
				.collect(Collectors.toList());
	}

	private List<WebComponent> updateLabelsWithComponentIds(WebComponentStructure structure) {
		return structure.getComponentsAsFlatList().stream()
			.filter(it -> WebLabel.class.isAssignableFrom(it.getClass()))
			.map(it -> {
				WebLabel label = (WebLabel) it;
				if (label.getName() != null) {
					Integer forId = getExistingAttributeId(label.getName());
					if (forId != null) {
						label.setForId(layoutml.hashCode() + "-" + forId);
					}
				}
				return label;
			})
			.collect(Collectors.toList());
	}

	public WebGridCalculated populateGridCalculated(
			WebGridCalculated grid,
			List<WebComponent> components,
			PanelTransformer.GridSizes gridSizes
	) {
		components.forEach(it -> {
			WebCalcCell cell = factory.createWebCalcCell();
			cell.getComponents().add(it);

			PanelTransformer.CalculatedPosition pos = gridSizes != null ? gridSizes.getCellPosition(it) : null;
			if (pos != null) {
				cell.setLeft(pos.left);
				cell.setTop(pos.top);
				cell.setWidth(pos.width);
				cell.setHeight(pos.height);
			} else {
				cell.setWidth("100%");
				cell.setHeight("100%");
			}

			grid.getCells().add(cell);
		});

		// Sort cells by column and row (for correct tab order)
		grid.getCells().sort(new Comparator<WebCalcCell>() {
			@Override
			public int compare(final WebCalcCell a, final WebCalcCell b) {
				WebComponent compA = a.getComponents().stream().findFirst().orElse(null);
				WebComponent compB = b.getComponents().stream().findFirst().orElse(null);
				if (compA != null && compB != null &&
						compA.getColumn() != null && compB.getColumn() != null &&
						compA.getRow() != null && compB.getRow() != null) {
					final int compareCols = compA.getColumn().compareTo(compB.getColumn());
					if (compareCols == 0) {
						return compA.getRow().compareTo(compB.getRow());
					} else {
						return compareCols;
					}
				}
				return 1;
			}
		});

		return grid;
	}
}
