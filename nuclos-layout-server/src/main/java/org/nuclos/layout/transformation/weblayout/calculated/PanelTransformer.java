package org.nuclos.layout.transformation.weblayout.calculated;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.nuclos.schema.layout.layoutml.Background;
import org.nuclos.schema.layout.layoutml.Panel;
import org.nuclos.schema.layout.layoutml.Tablelayout;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebContainer;
import org.nuclos.layout.transformation.weblayout.AbstractPanelTransformer;
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class PanelTransformer extends AbstractPanelTransformer {
	private static final Logger log = LoggerFactory.getLogger(PanelTransformer.class);

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Panel panel = (Panel) obj;
		if (panel.getVisible() == org.nuclos.schema.layout.layoutml.Boolean.NO) {
			return null;
		}

		WebContainer result = createPanelOrContainer(layoutTransformer, panel);
		List<WebComponent> components = new ArrayList<>();

		result.setCalculated(factory.createWebGridCalculated());
		result.setName(panel.getName());

		if (panel.getFont() != null) {
			result.setFontSize(convertFontSize(panel.getFont().getSize()));
		}
		if (panel.getBackground() != null) {
			result.setBackgroundColor(convertBackground(panel.getBackground()));
			result.setCustomBackgroundColor(convertBackground(panel.getBackground()));
		}

		result.setOpaque(panel.getOpaque() == org.nuclos.schema.layout.layoutml.Boolean.YES);

		panel.getContainerOrLabelOrTextfield().forEach(it ->
			components.addAll(layoutTransformer.getWebComponents(it)));

		GridSizes gridSizes = getGridSizes(panel);
		result.getCalculated().setMinWidth(gridSizes.fixedColSize);
		result.getCalculated().setMinHeight(gridSizes.fixedRowSize);

		((LayoutmlToWeblayoutCalcTransformer) layoutTransformer).populateGridCalculated(
				result.getCalculated(),
				components,
				gridSizes
		);

		return result;
	}

	String convertBackground(final Background background) {
		try {
			return String.format(
					"#%02X%02X%02X",
					Integer.valueOf(background.getRed()),
					Integer.valueOf(background.getGreen()),
					Integer.valueOf(background.getBlue())
			);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	private GridSizes getGridSizes(Panel panel) {
		GridSizes result = null;
		Object layoutManager = panel.getLayoutmanager().getValue();
		if (layoutManager instanceof Tablelayout) {
			Tablelayout tablelayout = (Tablelayout) layoutManager;
			List<Integer> rowSizes = Arrays.stream(tablelayout.getRows().split("\\|")).map(it ->
				new BigDecimal(it).intValue()).collect(Collectors.toList());

			List<Integer> columnSizes = Arrays.stream(tablelayout.getColumns().split("\\|")).map(it ->
				new BigDecimal(it).intValue()).collect(Collectors.toList());

			result = new GridSizes(
					rowSizes,
					columnSizes
			);
		}
		return result;
	}

	class GridSizes {
		List<Integer> rowSizes;
		List<Integer> columnSizes;

		int dynamicRowCount = 0;
		BigInteger fixedRowSize = BigInteger.ZERO;

		int dynamicColCount = 0;
		BigInteger fixedColSize = BigInteger.ZERO;

		MathContext mc = new MathContext(3, RoundingMode.HALF_UP);

		GridSizes(
				List<Integer> rowSizes,
				List<Integer> columnSizes
		) {
			this.rowSizes = rowSizes;
			this.columnSizes = columnSizes;

			rowSizes.forEach(it -> {
				if (it == -1) {
					dynamicRowCount++;
				} else if (it > 0) {
					fixedRowSize = fixedRowSize.add(BigInteger.valueOf(it.longValue()));
				}
			});
			columnSizes.forEach(it -> {
				if (it == -1) {
					dynamicColCount++;
				} else if (it > 0) {
					fixedColSize = fixedColSize.add(BigInteger.valueOf(it));
				}
			});
		}

		CalculatedPosition getCellPosition(WebComponent component) {
			CellSizeContext rowContext = new CellSizeContext(
					rowSizes,
					component.getRow(),
					component.getRowspan()
			);
			CellSizeContext columnContext = new CellSizeContext(
					columnSizes,
					component.getColumn(),
					component.getColspan()
			);

			return new CalculatedPosition(
					getCellLeft(columnContext),
					getCellTop(rowContext),
					getCellWidth(columnContext),
					getCellHeight(rowContext)
			);
		}

		String getCellLeft(CellSizeContext context) {
			return toOffset(context);
		}

		String getCellTop(CellSizeContext context) {
			return toOffset(context);
		}

		String getCellWidth(CellSizeContext context) {
			return toSize(context);
		}

		String getCellHeight(CellSizeContext context) {
			return toSize(context);
		}

		String toOffset(CellSizeContext context) {
			String result = null;

			if (context.previousDynamic == 0) {
				result = String.format("%dpx", context.previousFixed);
			} else {
				int totalDynamic = context.previousDynamic + context.coveredDynamic + context.followingDynamic;
				int totalFixed = context.previousFixed + context.coveredFixed + context.followingFixed;
				result = String.format("calc((100%% - %dpx) / %d * %d + %dpx)", totalFixed, totalDynamic, context.previousDynamic, context.previousFixed);
			}

			return result;
		}

		String toSize(CellSizeContext context) {
			String result = null;
			if (context.coveredDynamic == 0) {
				result = String.format("%dpx", context.coveredFixed);
			} else {
				int totalDynamic = context.previousDynamic + context.coveredDynamic + context.followingDynamic;
				int uncoveredDynamic = context.previousDynamic + context.followingDynamic;
				if (uncoveredDynamic == 0) {
					result = String.format("max(%dpx, calc( 100%% - %dpx - %dpx))", context.coveredFixed, context.previousFixed, context.followingFixed);
				} else {    // Covered at least 1 dynamic, but there are also uncovered
					int totalFixed = context.previousFixed + context.coveredFixed + context.followingFixed;
					//BigDecimal shiftPixels = new BigDecimal(totalFixed).divide(new BigDecimal(totalDynamic)).round(mc);
					result = String.format("max(%dpx, calc((100%% - %dpx) / %d * %d + %dpx))", context.coveredFixed, totalFixed, totalDynamic, context.coveredDynamic, context.coveredFixed);
				}
			}

			return result;
		}
	}

	/**
	 * Holds row/column size informations necessary to calculate the actual size of a component.
	 */
	class CellSizeContext {
		List<Integer> previousSizes = new ArrayList<>();
		List<Integer> coveredSizes = new ArrayList<>();
		List<Integer> followingSizes = new ArrayList<>();

		/**
		 * Count of dynamically sized ("use remaining space") row/column sizes before the current component.
		 */
		final int previousDynamic;
		/**
		 * Sum of fixed pixel row/column sizes before the current component.
		 */
		final int previousFixed;

		/**
		 * Count of dynamically sized ("use remaining space") row/column sizes covered by the current component.
		 */
		final int coveredDynamic;
		/**
		 * Sum of fixed pixel row/column sizes covered by the current component.
		 */
		final int coveredFixed;

		/**
		 * Count of dynamically sized ("use remaining space") row/column sizes after the current component.
		 */
		final int followingDynamic;
		/**
		 * Sum of fixed pixel row/column sizes after the current component.
		 */
		final int followingFixed;

		CellSizeContext(
				List<Integer> rowOrColumnSizes,
				BigInteger cellStart,
				BigInteger cellSpan
		) {
			for (int index = 0; index < rowOrColumnSizes.size(); index++) {
				Integer size = rowOrColumnSizes.get(index);
				if (index < cellStart.intValue()) {
					previousSizes.add(size);
				} else if (index < cellStart.intValue() + (cellSpan != null ? cellSpan.intValue() : 1)) {
					coveredSizes.add(size);
				} else {
					followingSizes.add(size);
				}
			}

			previousDynamic = countDynamic(previousSizes);
			previousFixed = sumFixed(previousSizes);

			coveredDynamic = countDynamic(coveredSizes);
			coveredFixed = sumFixed(coveredSizes);

			followingDynamic = countDynamic(followingSizes);
			followingFixed = sumFixed(followingSizes);
		}

		int countDynamic(List<Integer> sizes) {
			return Math.toIntExact(sizes.stream().filter(it -> it == -1).count());
		}

		int sumFixed(List<Integer> sizes) {
			return sizes.stream().filter(it -> it > 0).reduce(0, Integer::sum);
		}
	}

	class CalculatedPosition {
		String left;
		String top;
		String width;
		String height;

		CalculatedPosition(
				final String left,
				final String top,
				final String width,
				final String height
		) {
			this.left = left;
			this.top = top;
			this.width = width;
			this.height = height;
		}
	}
}
