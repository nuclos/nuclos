package org.nuclos.layout.transformation.weblayout.fixed;

import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.layout.transformation.ILanguageLocator;
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer;
import org.nuclos.layout.transformation.weblayout.TransformerRegistry;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.nuclos.schema.layout.layoutml.Panel;
import org.nuclos.schema.layout.web.WebLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Transforms an instance of {@link Layoutml} to {@link WebLayout}.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class LayoutmlToWeblayoutFixedTransformer extends LayoutmlToWeblayoutTransformer {
	public LayoutmlToWeblayoutFixedTransformer(final IRigidMetaProvider metaProvider, final ILanguageLocator localeDelegate, final Layoutml layoutml) {
		super(metaProvider, localeDelegate, layoutml);

		TransformerRegistry.register(Panel.class, new PanelTransformer());
	}

	public LayoutmlToWeblayoutFixedTransformer(final IRigidMetaProvider metaProvider, final Layoutml layoutml) {
		this(metaProvider, null, layoutml);
	}

	@Override
	public WebLayout internalTransform() {
		WebLayout result = factory.createWebLayout();

		result.setTable(factory.createWebTable());
		populateTable(result.getTable(), webComponentStructure.getComponentsStructured());

		return result;
	}

	private static final Logger log = LoggerFactory.getLogger(LayoutmlToWeblayoutFixedTransformer.class);
}
