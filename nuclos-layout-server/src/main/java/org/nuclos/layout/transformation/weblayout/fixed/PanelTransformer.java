package org.nuclos.layout.transformation.weblayout.fixed;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.nuclos.layout.transformation.weblayout.AbstractPanelTransformer;
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer;
import org.nuclos.schema.layout.layoutml.Panel;
import org.nuclos.schema.layout.layoutml.Tablelayout;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class PanelTransformer extends AbstractPanelTransformer {
	private static final Logger log = LoggerFactory.getLogger(PanelTransformer.class);

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Panel panel = (Panel) obj;
		if (panel.getVisible() == org.nuclos.schema.layout.layoutml.Boolean.NO) {
			return null;
		}

		WebContainer result = createPanelOrContainer(layoutTransformer, panel);
		List<WebComponent> components = new ArrayList<>();

		result.setTable(factory.createWebTable());

		if (panel.getFont() != null) {
			result.setFontSize(convertFontSize(panel.getFont().getSize()));
		}
		transferSizes(panel, result);

		result.setOpaque(panel.getOpaque() == org.nuclos.schema.layout.layoutml.Boolean.YES);

		panel.getContainerOrLabelOrTextfield().forEach( it ->
			components.addAll(layoutTransformer.getWebComponents(it)));

		layoutTransformer.populateTable(result.getTable(), components);

		return result;
	}

	/**
	 * Transfers column and row sizes.
	 *
	 * @param panel
	 * @param result
	 */
	private void transferSizes(Panel panel, WebContainer result) {
		Object layoutManager = panel.getLayoutmanager().getValue();
		if (layoutManager instanceof Tablelayout) {
			Tablelayout tablelayout = (Tablelayout) layoutManager;
			try {
				result.getTable().getColumnSizes().addAll(Arrays.stream(tablelayout.getColumns().split("\\|")).map(it ->
					new BigDecimal(it).toBigInteger()).collect(Collectors.toList()));
				result.getTable().getRowSizes().addAll(Arrays.stream(tablelayout.getRows().split("\\|")).map(it ->
					new BigDecimal(it).toBigInteger()).collect(Collectors.toList()));
			}
			catch (Exception ex) {
				log.warn("Could not pare TableLayout", ex);
			}
		}
	}
}
