package org.nuclos.layout.transformation.weblayout.responsive;

import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.layout.transformation.ILanguageLocator;
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer;
import org.nuclos.layout.transformation.weblayout.TransformerRegistry;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.nuclos.schema.layout.layoutml.Panel;
import org.nuclos.schema.layout.web.WebLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Transforms an instance of {@link Layoutml} to {@link WebLayout}.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class LayoutmlToWeblayoutResponsiveTransformer extends LayoutmlToWeblayoutTransformer {
	public LayoutmlToWeblayoutResponsiveTransformer(final IRigidMetaProvider metaProvider, final ILanguageLocator localeDelegate, final Layoutml layoutml) {
		super(metaProvider, localeDelegate, layoutml);

		TransformerRegistry.register(Panel.class, new PanelTransformer());
	}

	public LayoutmlToWeblayoutResponsiveTransformer(final IRigidMetaProvider metaProvider, final Layoutml layoutml) {
		this(metaProvider, null, layoutml);
	}

	@Override
	public WebLayout internalTransform() {
		WebLayout result = factory.createWebLayout();

		result.setGrid(factory.createWebGrid());
		populateGrid(result.getGrid(), webComponentStructure.getComponentsStructured());

		return result;
	}

	private static final Logger log = LoggerFactory.getLogger(LayoutmlToWeblayoutResponsiveTransformer.class);
}
