package org.nuclos.layout.transformation.weblayout.responsive;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.schema.layout.layoutml.Panel;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebContainer;
import org.nuclos.layout.transformation.weblayout.AbstractPanelTransformer;
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class PanelTransformer extends AbstractPanelTransformer {

	public WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Object obj) {
		Panel panel = (Panel) obj;
		if (panel.getVisible() == org.nuclos.schema.layout.layoutml.Boolean.NO) {
			return null;
		}

		WebContainer result = factory.createWebContainer();
		List<WebComponent> components = new ArrayList<>();

		result.setGrid(factory.createWebGrid());

		if (panel.getFont() != null) {
			result.setFontSize(convertFontSize(panel.getFont().getSize()));
		}

		panel.getContainerOrLabelOrTextfield().forEach(it ->
			components.addAll(layoutTransformer.getWebComponents(it)));

		layoutTransformer.populateGrid(result.getGrid(), components);

		return result;
	}
}
