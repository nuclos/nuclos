//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.attribute.ejb3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;

import org.nuclos.common.AbstractDisposableRegistrationBean;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.AbstractDalVOBasic;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.DependentDataMap.DependentKey;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.EntityAndFieldWithParent;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.server.attribute.valueobject.LayoutVO;
import org.nuclos.server.autosync.XMLEntities;
import org.nuclos.server.autosync.XmlEoDalUnionProcessor;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.processor.nuclos.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Layout facade encapsulating generic object screen layout management.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
@Component("layoutService")
public class LayoutFacadeBean extends AbstractDisposableRegistrationBean implements LayoutFacadeLocal, LayoutFacadeRemote {

	public static final Logger LOG = LoggerFactory.getLogger(LayoutFacadeBean.class);

	// Spring injection

	private final XMLEntities xmlEntities;

	private final MetaProvider metaProv;

	private final NuclosDalProvider dalProvider;

	// end of Spring injection

	private IEntityObjectProcessor<UID> layoutDalProc;
	private IEntityObjectProcessor<UID> layoutUsageDalProc;

	public LayoutFacadeBean(
			final MetaProvider metaProv,
			final NuclosDalProvider dalProvider,
			final XMLEntities xmlEntities) {
		this.metaProv = metaProv;
		this.dalProvider = dalProvider;
		this.xmlEntities = xmlEntities;
	}

	@PostConstruct
	private void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		createSyncFlowableWithDropStrategyAndRegister(dalProvider.getEntityObjectProcessor(E.ENTITY).observeChangesNotifyBeforeCommit(true),
				notification -> evictWebLayoutCachesOnly());
		createSyncFlowableWithDropStrategyAndRegister(dalProvider.getEntityObjectProcessor(E.LAYOUT).observeChangesNotifyBeforeCommit(true),
				notification -> evictCaches());
		createSyncFlowableWithDropStrategyAndRegister(dalProvider.getEntityObjectProcessor(E.LAYOUTUSAGE).observeChangesNotifyBeforeCommit(true),
				notification -> evictCaches());

		this.layoutDalProc = XmlEoDalUnionProcessor.basedOn(dalProvider.getEntityObjectProcessor(E.LAYOUT));
		this.layoutUsageDalProc = XmlEoDalUnionProcessor.basedOn(dalProvider.getEntityObjectProcessor(E.LAYOUTUSAGE));
	}

	/**
	 * @return true, if detail layout is available for the given entity name, otherwise false
	 */
	@RolesAllowed("Login")
	public boolean isDetailLayoutAvailable(UsageCriteria usage) {
		return getDetailLayoutIDForUsage(usage) == null ? false : true;
	}
	
	public String getLayoutML(UID pk) throws CommonBusinessException {
		return layoutDalProc.getByPrimaryKey(pk).getFieldValue(E.LAYOUT.layoutML);
	}
	
	@Cacheable(value="allLayoutUidsForEntity", key="#p0")
	public Set<UID> getAllLayoutUidsForEntity(UID entityUid) {
		Set<UID> layoutUsages = layoutUsageDalProc.getAll().parallelStream()
				.filter(eo -> Objects.equals(eo.getFieldUid(E.LAYOUTUSAGE.entity), entityUid))
				.map(eo -> eo.getFieldUid(E.LAYOUTUSAGE.layout))
				.collect(Collectors.toSet());
		
		if (E.isNuclosEntity(entityUid)) {
			UID entityNameAsUid = new UID(E.getByUID(entityUid).getEntityName());
			layoutUsages.addAll(xmlEntities.getSystemObjectsWith(E.LAYOUTUSAGE.getUID(), E.LAYOUTUSAGE.entity.getUID(), entityNameAsUid).stream()
					.map(eo -> eo.getFieldUid(E.LAYOUTUSAGE.layout))
					.collect(Collectors.toSet()));
		}

		return layoutUsages;
	}
	
	public UID getNucletUID(UID pk) throws CommonBusinessException {
		return layoutDalProc.getByPrimaryKey(pk).getFieldUid(E.LAYOUT.nuclet);
	}
	
	public UID getDetailLayoutIDForUsage(UsageCriteria usage) {
		return getDetailLayoutIDForUsage(usage, false);
	}
	
	/**
	 * @return the detail layout for the given Usage Criteria name if any, otherwise null
	 */
	@Cacheable(value="layoutIDForUsage", key="#p0.hashCode() + (#p1 ? 1 : 0)")
	public UID getDetailLayoutIDForUsage(UsageCriteria usage, boolean bSearchLayout) {
		if (usage == null) {
			throw new CommonFatalException("UsageCriteria may not be null!");
		}
		
		try {
			UID layoutUsageUID = getLayoutUsageUID(usage, bSearchLayout);
			if (layoutUsageUID != null) {
				return getLayoutUsage(layoutUsageUID).getFieldUid(E.LAYOUTUSAGE.layout);
			}
		} catch (CommonBusinessException cbe) {
			LOG.warn(cbe.getMessage(), cbe);
		}
		
		return null;
	}

	private EntityObjectVO<UID> getLayoutUsage(UID pk) throws CommonBusinessException {
		return layoutUsageDalProc.getByPrimaryKey(pk);
	}

	private UID getLayoutUsageUID(UsageCriteria usage, boolean bSearchlayout) throws CommonBusinessException {
		Collection<UsageCriteria> ucs = getAllUsageCriteriasFromLayoutUsages();
		
		UsageCriteria usagecriteriaBestMatching = UsageCriteria.getBestMatchingUsageCriteria(ucs, usage);
		UID retVal = usagecriteriaBestMatching != null ? getLayoutUsageUIDForExactUsageCriteria(usagecriteriaBestMatching, bSearchlayout) : null;
		
		if (retVal == null && usage.getStatusUID() == null && metaProv.getEntity(usage.getEntityUID()).isStateModel()) {
			UID initialStateUID = UsageCriteria.getInitialStateUID(usage,
					dalProvider.getEntityObjectProcessor(E.STATEMODELUSAGE).getAll(),
					dalProvider.getEntityObjectProcessor(E.STATE).getAll(),
					dalProvider.getEntityObjectProcessor(E.STATETRANSITION).getAll()); 
					
			UsageCriteria usage2 = new UsageCriteria(usage.getEntityUID(), usage.getProcessUID(), initialStateUID, usage.getCustom());
			
			usagecriteriaBestMatching = UsageCriteria.getBestMatchingUsageCriteria(ucs, usage2);
			if (usagecriteriaBestMatching != null
					// prevent endless loop...
						&& !usagecriteriaBestMatching.equals(usage)) {
				
				retVal = getLayoutUsageUID(usagecriteriaBestMatching, bSearchlayout);
			}
		}
		
		return retVal;
	}
	
	public Collection<UID> getEntitiesAssignedToLayoutId(UID layoutUID) throws CommonBusinessException {
		Set<UID> retVal = new HashSet<UID>();
		Set<UsageCriteria> usages = getAllUsageCriteriasFromLayoutUsages();
		for (UsageCriteria usage : usages) {
			UID entityUID = usage.getEntityUID();
			if (retVal.contains(entityUID)) {
				continue;
			}
			
			if (layoutUID.equals(getDetailLayoutIDForUsage(usage))) {
				retVal.add(entityUID);
			}
		}
		
		return retVal;
	}
		
	@Cacheable(value="usageCriteriasFromLayoutUsage")
	Set<UsageCriteria> getAllUsageCriteriasFromLayoutUsages() throws CommonBusinessException {
		return layoutUsageDalProc.getAll().parallelStream()
				.map(eo -> UsageCriteria.createUsageCriteriaFromLayoutUsage(eo, null))
				.collect(Collectors.toSet());
	}
	
	@Cacheable(value="layoutUsageUids", key="#p0.hashCode() + (#p1 ? 1 : 0)")
	UID getLayoutUsageUIDForExactUsageCriteria(UsageCriteria usage, boolean bSearchLayout) throws CommonBusinessException {
		
		if (E.isNuclosEntity(usage.getEntityUID())) {
			// search in system entities...
			final Collection<EntityObjectVO<UID>> layouts = xmlEntities.getSystemObjects(E.LAYOUT.getUID());
			for (EntityObjectVO<UID> layout : layouts) {
				for (EntityObjectVO<UID> layoutusage : layout.getDependents().<UID>getDataPk(E.LAYOUTUSAGE.layout)) {
					if (usage.getEntityUID().equals(layoutusage.getFieldUid(E.LAYOUTUSAGE.entity))) {
						Boolean searchLayout = layoutusage.getFieldValue(E.LAYOUTUSAGE.searchScreen);
						
						if (searchLayout == null) {
							searchLayout = Boolean.FALSE;
						}
						
						if (searchLayout.equals(bSearchLayout)) {
							return layoutusage.getPrimaryKey();							
						}
					}
				}
			}
			return null;
			
		} else {
			return layoutUsageDalProc.getAll().parallelStream()
					.filter(eo -> 	Objects.equals(eo.getFieldUid(E.LAYOUTUSAGE.entity), usage.getEntityUID()) &&
									Objects.equals(eo.getFieldValue(E.LAYOUTUSAGE.custom), usage.getCustom()) &&
									Objects.equals(eo.getFieldUid(E.LAYOUTUSAGE.process), usage.getProcessUID()) &&
									Objects.equals(eo.getFieldUid(E.LAYOUTUSAGE.state), usage.getStatusUID()) &&
									Objects.equals(eo.getFieldValue(E.LAYOUTUSAGE.searchScreen), bSearchLayout))
					.findAny()
					.map(AbstractDalVOBasic::getPrimaryKey)
					.orElse(null);
		}
	}

	/**
	 * returns the entity names of the subform entities along with their foreignkey field
	 * and the referenced parent entity name used in the given layout
	 * Note that this works only for genericobject entities
	 * @param layoutUID
	 */
	@RolesAllowed("Login")
	@Cacheable(value="goLayout", key="#p0.getString()")
	public List<EntityAndFieldWithParent> getSubFormEntityAndParentSubFormEntityNamesByLayoutId(UID layoutUID) {
		String sLayoutML = null;

		try {
			sLayoutML = getLayoutML(layoutUID);
		}
		catch (Exception e) {
			throw new NuclosFatalException(e);
		}

		if (sLayoutML == null) {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("layout.facade.exception.1", layoutUID));
				//"Die Eingabemaske mit der Id \"" + iLayoutId + "\" wurde nicht gefunden.");
		}
		try {
			final List<EntityAndFieldWithParent> result = new ArrayList<>();
			new LayoutMLParser().getSubFormEntityAndParentSubFormEntities(sLayoutML)
					.entrySet().stream()
					.forEach(entry -> result.add(EntityAndFieldWithParent.build(
							entry.getKey(),
							entry.getValue()
					)));
			return result;
		} catch (LayoutMLException e) {
			throw new NuclosFatalException(e);
		}
	}

	@RolesAllowed("Login")
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesMD(UsageCriteria usage) {

		UID uidEntity = usage.getEntityUID();
		UID layoutUID = getDetailLayoutIDForUsage(usage);
		String sLayoutML = null;
		if (layoutUID != null) {
			try {
				sLayoutML = getLayoutML(layoutUID);
			} catch (CommonBusinessException cbe) {
				LOG.warn(cbe.getMessage(), cbe);
			}
			
		}
		
		final Map<EntityAndField, UID> result;
		
		if (sLayoutML == null) {
			// special handling for entities with manually build layouts which are not saved in the database
			
			if (E.isNuclosEntity(uidEntity)) {
				result = new HashMap<EntityAndField, UID>();
				
				if(E.STATEMODEL.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.STATEMODELUSAGE, E.STATEMODELUSAGE.statemodel), null);
					result.put(new EntityAndField(E.STATE, E.STATE.model), null);
				}
				else if(E.STATE.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.STATETRANSITION, E.STATETRANSITION.state2), null);
					result.put(new EntityAndField(E.ROLEATTRIBUTEGROUP, E.ROLEATTRIBUTEGROUP.state), null);
					result.put(new EntityAndField(E.ROLESUBFORM, E.ROLESUBFORM.state), null);
				}
				else if(E.STATETRANSITION.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.ROLETRANSITION, E.ROLETRANSITION.transition), null);
					result.put(new EntityAndField(E.SERVERCODETRANSITION, E.SERVERCODETRANSITION.transition), null);
				}
				else if(E.CHART.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.CHARTUSAGE, E.CHARTUSAGE.chart), null);
				}
				else if(E.DATASOURCE.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.DATASOURCEUSAGE, E.DATASOURCEUSAGE.datasource), null);
				}
				else if(E.REPORT.checkEntityUID(uidEntity)) {
					result.put(new EntityAndField(E.REPORTOUTPUT, E.REPORTOUTPUT.parent), null);				
				}
			}
/* Der "tiefe" XML-Export tut's ueberhaupt nicht mehr mit dem "Original"-Code: */
//			else {
//				throw new NuclosFatalException("Die Eingabemaske f\u00fcr die Entit\u00e4t \"" + entityName + "\" fehlt.");
//			}
/* tentativer Fix (10/2009)- TODO: ueberpruefen!!! */
			else {
				//@see NUCLOSINT-1524. there are entities that are not system entities and do not have an layout defined.!
				/*MasterDataMetaVO metaVO =
					MasterDataMetaCache.getInstance().getMetaData(entityName);
				if (!metaVO.isSystemEntity()) {
					throw new NuclosFatalException(
						StringUtils.getParameterizedExceptionMessage("layout.facade.exception.2", entityName));
				}*/
				result = Collections.emptyMap();
			}
		}
		else {
			try {
				result = getSubFormEntityAndParentSubFormEntityNamesMD(sLayoutML, uidEntity);
			} catch (LayoutMLException e) {
				throw new NuclosFatalException(e);
			}
		}

		return result;
	}

	@Cacheable(value="mdLayout", key="#p1.getString()")
	Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesMD(String sLayoutML, UID entityName) throws LayoutMLException {
		return new LayoutMLParser().getSubFormEntityAndParentSubFormEntities(sLayoutML);
	}

	@RolesAllowed("Login")
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesByGO(UsageCriteria usage) throws CommonFinderException{
		return getSubFormEntityAndParentSubFormEntityNamesByLayoutId(getBestMatchingLayoutUID(usage, false)).stream()
				.collect(HashMap::new, (m,v)->m.put(v.entityAndField, v.parent), HashMap::putAll);
	}

	@Cacheable(value="bestMatchingLayout", key="#p0.toString() + #p1")
	public UID getBestMatchingLayoutUID(final UsageCriteria usage, final boolean searchLayout) throws CommonFinderException {
		return UsageCriteria.getBestMatchingLayout(usage, searchLayout, layoutUsageDalProc.getAll());
	}

	/**
	 * returns the entity names of the subform entities along with their foreignkey field
	 * and the referenced parent entity name used in the given layout
	 * Note that this works only for genericobject entities
	 * 
	 * §ejb.interface-method view-type="local"
	 * §ejb.permission role-name="Login"
	 * 
	 * @param layoutUID
	 */
	public Map<EntityAndField, UID> getSubFormEntityAndParentSubFormEntityNamesById(UID layoutUID) {
		String sLayoutML = null;
		try {
			sLayoutML = getLayoutML(layoutUID);
		}
		catch (Exception e) {
			throw new NuclosFatalException(e);
		}

		if (sLayoutML == null) {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("layout.facade.exception.1", layoutUID));
		}
		try {
			return new LayoutMLParser().getSubFormEntityAndParentSubFormEntities(sLayoutML);
		} catch (LayoutMLException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	/*
	 * @ejb.interface-method view-type="local"
	 * @ejb.permission role-name="Login"
	 */
	@Caching(evict = { 
			@CacheEvict(value="webLayoutCalculated", allEntries=true),
			@CacheEvict(value="webLayoutFixed", allEntries=true),
			@CacheEvict(value="webLayoutResponsive", allEntries=true),
			@CacheEvict(value="webLayoutRules", allEntries=true),
			@CacheEvict(value="webLayoutVlps", allEntries = true),
			@CacheEvict(value="goLayout", allEntries=true),
			@CacheEvict(value="mdLayout", allEntries=true),
			@CacheEvict(value="bestMatchingLayout", allEntries=true),
			@CacheEvict(value="layoutIDForUsage", allEntries=true),
			@CacheEvict(value="layoutUsageUids", allEntries=true),
			@CacheEvict(value="usageCriteriasFromLayoutUsage", allEntries=true),
			@CacheEvict(value="allLayoutUidsForEntity", allEntries=true),
			@CacheEvict(value="dependentKeyBetween", allEntries=true),
			@CacheEvict(value="allLayoutUsageCriterias", allEntries=true)

	})
	public void evictCaches() {
		// no cluster notification here, called from dal observable
	}

	@Caching(evict = {
			@CacheEvict(value = "webLayoutFixed", allEntries = true),
			@CacheEvict(value = "webLayoutCalculated", allEntries = true),
			@CacheEvict(value = "webLayoutResponsive", allEntries = true),
			@CacheEvict(value = "webLayoutRules", allEntries = true),
			@CacheEvict(value = "webLayoutVlps", allEntries = true)
	})
	public void evictWebLayoutCachesOnly() {
		// no cluster notification here, called from dal observable
	}

	@Override
	@RolesAllowed("Login")
	public List<LayoutVO> getMasterDataLayout() {
		return layoutDalProc.getAll().parallelStream()
				.map(LayoutVO::from)
				.collect(Collectors.toList());
	}
	
	/**
	 * get the Dependent Key from Layout,
	 * no support for sub-subforms
	 *
	 * @return null if key is not found or unique
	 */
	@Cacheable(value="dependentKeyBetween", key="#p0.getString() + #p1.getString() + #p2")
	public IDependentKey getDependentKeyBetween(UID master, UID subform, String customUsage) {
		//final UsageCriteria usage = new UsageCriteria(master, null, null, customUsage);
		//return getDependentKeyBetween(usage, subform);

		IDependentKey result = null;

		// find all usage criterias for master entity (NUCLOS-7306)
		Collection<UsageCriteria> allLayoutUsageCriterias = getAllLayoutUsageCriterias(master, customUsage);
		for (UsageCriteria uc : allLayoutUsageCriterias) {
			final IDependentKey dependentKeyBetween = getDependentKeyBetween(uc, subform);
			if (result == null) {
				result = dependentKeyBetween;
			} else if (dependentKeyBetween != null) {
				if (!RigidUtils.equal(result, dependentKeyBetween)) {
					// result is not unique;
					return null;
				}
			}
		}

		if (result == null && customUsage != null) {
			// no result with customUsage... try to search without it
			return getDependentKeyBetween(master, subform, null);
		}

		return result;
	}

	@Cacheable(value="allLayoutUsageCriterias", key="#p0.getString() + #p1")
	public Collection<UsageCriteria> getAllLayoutUsageCriterias(UID entityUID, String customUsage) {
		return layoutUsageDalProc.getAll().parallelStream()
				.filter(eo -> Objects.equals(eo.getFieldUid(E.LAYOUTUSAGE.entity), entityUID) &&
						Objects.equals(eo.getFieldValue(E.LAYOUTUSAGE.custom), customUsage))
				.map(eo -> UsageCriteria.createUsageCriteriaFromLayoutUsage(eo, null))
				.collect(Collectors.toList());
	}
	
	/**
	 * get the Dependent Key from Layout,
	 * no support for sub-subforms
	 *
	 * @return null if key is not found or unique
	 */
	public IDependentKey getDependentKeyBetween(UsageCriteria usageOfMaster, UID subform) {
		Map<EntityAndField, UID> subFormEntityNames;
		EntityMeta<?> masterMeta = metaProv.getEntity(usageOfMaster.getEntityUID());
		
		// 1. Search via MetaData: If only one referencing key found, use it.
		
		UID singleReferencingField = null;
		Map<UID, FieldMeta<?>> mpFields = metaProv.getAllEntityFieldsByEntity(subform);
    	for (UID field : mpFields.keySet()) {
    		FieldMeta<?> efmdv = mpFields.get(field);
    		if (usageOfMaster.getEntityUID().equals(efmdv.getForeignEntity())) {
    			if (singleReferencingField != null) {
    				// more than one found... break
    				singleReferencingField = null;
    				break;
    			} else {
    				singleReferencingField = field;
    			}
    		}
    	}
    	if (singleReferencingField != null) {
			return DependentDataMap.createDependentKey(singleReferencingField);
		}
		
		// 2. If more than one referencing key exists, search via layout.
		
		if (masterMeta.isStateModel()) {
			try {
				subFormEntityNames = getSubFormEntityAndParentSubFormEntityNamesByGO(usageOfMaster);
			} catch (CommonFinderException e) {
				LOG.error(e.getMessage(), e);
				subFormEntityNames = null;
			}
		} else {
			subFormEntityNames = getSubFormEntityAndParentSubFormEntityNamesMD(usageOfMaster);
		}
		if (subFormEntityNames != null) {
			for (Entry<EntityAndField, UID> depLayoutInfo : subFormEntityNames.entrySet()) {
				if (depLayoutInfo.getValue() != null) {
					// has parent key -> sub-subform -> not supported
					continue;
				} else {
					EntityAndField entityAndField = depLayoutInfo.getKey();
					if (entityAndField.getEntity().equals(subform)) {
						if (entityAndField.getField() != null) {
							return new DependentKey(entityAndField.getField());
						}
					}
				}
			}
		}
		
		return null;
	}
}
