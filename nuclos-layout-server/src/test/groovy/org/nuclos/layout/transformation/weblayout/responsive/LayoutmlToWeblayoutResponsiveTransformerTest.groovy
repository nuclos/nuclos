package org.nuclos.layout.transformation.weblayout.responsive

import org.junit.Test
import org.nuclos.common2.JaxbMarshalUnmarshalUtil
import org.nuclos.schema.layout.layoutml.Layoutml
import org.nuclos.schema.layout.web.WebButtonExecuteRule
import org.nuclos.schema.layout.web.WebCheckbox
import org.nuclos.schema.layout.web.WebCombobox
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebContainer
import org.nuclos.schema.layout.web.WebDatechooser
import org.nuclos.schema.layout.web.WebFile
import org.nuclos.schema.layout.web.WebLayout
import org.nuclos.schema.layout.web.WebListofvalues
import org.nuclos.schema.layout.web.WebSubform
import org.nuclos.schema.layout.web.WebTabcontainer
import org.nuclos.schema.layout.web.WebTextfield
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformerTest

import com.fasterxml.jackson.databind.ObjectMapper

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LayoutmlToWeblayoutResponsiveTransformerTest extends LayoutmlToWeblayoutTransformerTest {

	@Test
	void testLayoutml2WeblayoutRespsonsive() {
		InputStream xml = this.getClass().getResourceAsStream('../../layout/nuclet_test_other_TestLayoutComponents.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToWeblayoutResponsiveTransformer(getMetaProvider(), layoutml)
		WebLayout result = transformer.transform()

		assert !result.grid.rows.empty

		List<WebComponent> components = findAllComponents(result.grid)
		assert components.find { it instanceof WebContainer }
		assert components.find { it instanceof WebTextfield }
		assert components.find { it instanceof WebDatechooser }
		assert components.find { it instanceof WebListofvalues }
		assert components.find { it instanceof WebCombobox }
		assert components.find { it instanceof WebTabcontainer }
		assert components.find { it instanceof WebSubform }
		assert components.find { it instanceof WebFile }
		assert components.find { it instanceof WebButtonExecuteRule && it.rule && it.label }
		assert components.find { it instanceof WebCheckbox }

		// TODO: Assert all components are transformed
		// TODO: Assert the layout is correct
		// TODO: Assert all necessary informations are there
	}

	@Test
	void testWeblayout2JSON() {
		InputStream xml = this.getClass().getResourceAsStream('../../layout/example_rest_Order.xml')

		Layoutml layoutml = JaxbMarshalUnmarshalUtil.unmarshal(xml, Layoutml.class)
		def transformer = new LayoutmlToWeblayoutResponsiveTransformer(getMetaProvider(), layoutml)
		WebLayout result = transformer.transform()

		ObjectMapper mapper = getMapper()
		StringWriter stringWriter = new StringWriter()

		mapper.writeValue(stringWriter, result);
		String serializedValue = stringWriter.toString()

		assert serializedValue
	}
}