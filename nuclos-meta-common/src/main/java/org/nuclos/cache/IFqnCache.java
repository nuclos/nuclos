package org.nuclos.cache;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public interface IFqnCache {
	String getFullQualifiedNucletName(final UID nucletUID);

	UID translateFqn(EntityMeta<?> eMeta, String fqn);

	String translateUid(final UID uid);

	String translateUid(
			EntityMeta<?> eMeta,
			final UID uid
	);

	void invalidateCacheForEntity(final UID entityUID);

	<PK> void invalidateCacheForEntityObjectIfNecessary(EntityMeta<?> eMeta, EntityObjectVO<PK> entityObject);
}
