//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.common;

import java.util.Objects;

import org.nuclos.common.dal.vo.AbstractDalVOWithVersion;

public class EntityLafParameterVO extends AbstractDalVOWithVersion<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -86095740980627807L;
	
	private UID entity;
	private String parameter;
	private String value;
	
	public EntityLafParameterVO() {
		this(E.ENTITYLAFPARAMETER.getUID());
	}
	
	public EntityLafParameterVO(UID dalEntity) {
		super(dalEntity);
	}
	
	public UID getEntity() {
		return entity;
	}

	public void setEntity(UID entity) {
		this.entity = entity;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		if (getPrimaryKey() == null)
			return 0;
		return getPrimaryKey().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EntityLafParameterVO) {
			EntityLafParameterVO other = (EntityLafParameterVO) obj;
			return Objects.equals(getPrimaryKey(), other.getPrimaryKey());
		}
		return super.equals(obj);
	}
	
	
	
}
