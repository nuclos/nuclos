//Copyright (C) 2022  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.remoting.TypePreservingMapDeserializer;
import org.nuclos.remoting.TypePreservingMapSerializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class LafParameterByEntityMap {

	@JsonSerialize(using = TypePreservingMapSerializer.class)
	@JsonDeserialize(using = TypePreservingMapDeserializer.class)
	final protected Map<UID, LafParameterMap> wrapped;

	public LafParameterByEntityMap() {
		super();
		this.wrapped = new HashMap<>();
	}

	public LafParameterByEntityMap(Map<UID, LafParameterMap> wrapped) {
		super();
		this.wrapped = wrapped;
	}

	public LafParameterMap get(final UID entityUID) {
		return wrapped.get(entityUID);
	}

	public boolean containsKey(final UID entityUID) {
		return wrapped.containsKey(entityUID);
	}

	public void put(final UID entityUID, final LafParameterMap lafParameterMap) {
		wrapped.put(entityUID, lafParameterMap);
	}

	public int size() {
		return wrapped.size();
	}

	public LafParameterByEntityMap toUnmodifiableMap() {
		return new UnmodifiableLafParameterByEntityMap(this);
	}

	public static class UnmodifiableLafParameterByEntityMap extends LafParameterByEntityMap {

		/**
		 * for deserialization only
		 */
		protected UnmodifiableLafParameterByEntityMap() {
		}

		private UnmodifiableLafParameterByEntityMap(LafParameterByEntityMap map) {
			super(map.wrapped);
		}

		@Override
		public void put(final UID entityUID, final LafParameterMap lafParameterMap) {
			throw new UnsupportedOperationException();
		}
	}

}
