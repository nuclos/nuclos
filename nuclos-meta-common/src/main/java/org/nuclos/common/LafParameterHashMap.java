//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.remoting.TypePreservingMapDeserializer;
import org.nuclos.remoting.TypePreservingMapSerializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class LafParameterHashMap<T,S> implements LafParameterMap<T,S> {

	@JsonSerialize(using = TypePreservingMapSerializer.class)
 	@JsonDeserialize(using = TypePreservingMapDeserializer.class)
	private final Map<Integer, T> valuesWrapped;

	private final List<LafParameter<T,S>> keysWrapped;
	
	public LafParameterHashMap(Map<LafParameter<T,S>,T> wrapped) {
		if (wrapped == null) {
			throw new NullPointerException();
		}
		if (!(wrapped instanceof Serializable)) {
			throw new IllegalStateException("Wrapped Map must be Serializable");
		}
		if (wrapped instanceof LafParameterHashMap) {
			this.keysWrapped = ((LafParameterHashMap) wrapped).keysWrapped;
			this.valuesWrapped = ((LafParameterHashMap) wrapped).valuesWrapped;
		} else {
			this.keysWrapped = new ArrayList<>();
			this.valuesWrapped = new HashMap<>();
		}
	}
	
	public LafParameterHashMap() {
		this(new HashMap<LafParameter<T,S>,T>());
	}

	@Override
	public T getValue(LafParameter<T,S> parameter) {
		return (T) get(parameter);
	}
	
	@Override
	public void putValue(LafParameter<T,S> parameter, T value) {
		put(parameter, value);
	}
	
	@Override
	public S getStorageValue(LafParameter<T, S> parameter) {
		return parameter.convertToStore(getValue(parameter));
	}

	@Override
	public void putStorageValue(LafParameter<T, S> parameter, S storageValue) throws ParseException {
		putValue(parameter, parameter.convertFromStore(storageValue));
	}
	
	// Map methods

	public T get(Object key) {
		final int i = keysWrapped.indexOf(key);
		if (i >= 0) {
			return valuesWrapped.get(i);
		}
		return null;
	}

	public T put(LafParameter<T,S> key, T value) {
		final int i = keysWrapped.indexOf(key);
		if (i >= 0) {
			return valuesWrapped.put(i, value);
		} else {
			final int iNew = keysWrapped.size();
			keysWrapped.add(key);
			return valuesWrapped.put(iNew, value);
		}
	}

	@Override
	public Set<LafParameter<T,S>> keySet() {
		return new HashSet<>(keysWrapped);
	}

}
