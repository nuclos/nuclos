//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.validation.constraints.NotNull;

public class MutableMap<K, V> implements Map<K, V> {

	private Map<K, V> value;

	private Collection<V> valuesUnmodifiable;

	public MutableMap() {
		this(null);
	}

	public MutableMap(final Map<K, V> value) {
		this.value = value;
	}

	public Map<K, V> getValue() {
		return value;
	}

	public Map<K, V> workingCopy() {
		Map<K, V> v = value;
		if (v == null) {
			return new HashMap<>();
		} else {
			return new HashMap<>(v);
		}
	}

	public void setValue(final Map<K, V> value) {
		this.value = value;
		this.valuesUnmodifiable = null;
	}

	public Collection<V> valuesUnmodifiable() {
		Collection<V> result = this.valuesUnmodifiable;
		if (result == null) {
			result = Collections.unmodifiableCollection(values());
			this.valuesUnmodifiable = result;
		}
		return result;
	}

	@Override
	public int size() {
		return Optional.ofNullable(value).map(Map::size).orElse(0);
	}

	@Override
	public boolean isEmpty() {
		return Optional.ofNullable(value).map(Map::isEmpty).orElse(true);
	}

	@Override
	public boolean containsKey(final Object k) {
		return Optional.ofNullable(value).map(value -> value.containsKey(k)).orElse(false);
	}

	@Override
	public boolean containsValue(final Object v) {
		return Optional.ofNullable(value).map(value -> value.containsValue(v)).orElse(false);
	}

	@Override
	public V get(final Object k) {
		return Optional.ofNullable(value).map(value -> value.get(k)).orElse(null);
	}

	@Override
	public V put(final K k, final V v) {
		V result = Optional.ofNullable(value).map(value -> value.put(k, v)).orElse(null);
		this.valuesUnmodifiable = null;
		return result;
	}

	@Override
	public V remove(final Object k) {
		V result = Optional.ofNullable(value).map(value -> value.remove(k)).orElse(null);
		this.valuesUnmodifiable = null;
		return result;
	}

	@Override
	public void putAll(@NotNull final Map<? extends K, ? extends V> map) {
		Optional.ofNullable(value).ifPresent(value -> value.putAll(map));
		this.valuesUnmodifiable = null;
	}

	@Override
	public void clear() {
		Optional.ofNullable(value).ifPresent(Map::clear);
		this.valuesUnmodifiable = null;
	}

	@Override
	public Set<K> keySet() {
		return Optional.ofNullable(value).map(Map::keySet).orElseGet(Collections::emptySet);
	}

	@Override
	public Collection<V> values() {
		return Optional.ofNullable(value).map(Map::values).orElseGet(Collections::emptySet);
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		return Optional.ofNullable(value).map(Map::entrySet).orElseGet(Collections::emptySet);
	}
}
