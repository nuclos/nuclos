//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.ArrayList;
import java.util.Collection;

import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.IVersionVO;
import org.nuclos.common2.InternalTimestamp;

public class NucletFieldMeta<T> extends FieldMetaVO<T> implements IDalVO<UID>, IVersionVO<UID> {

	private static final long serialVersionUID = 8515924380531732345L;
	
	private String processor;
	private int state;
	
	private InternalTimestamp createdAt;
	private String createdBy;
	private InternalTimestamp changedAt;
	private String changedBy;
	private int version = -1;
	private boolean systemFieldsOnRequ = false;
	private boolean keepsVersion = false;
	
	public NucletFieldMeta(UID entityUID) {
		super();
		if (!E.ENTITYFIELD.getUID().equals(entityUID)) {
			throw new IllegalArgumentException();
		}
	}
	
	public NucletFieldMeta() {
		super();
	}
	
	public NucletFieldMeta(FieldMeta<T> other) {
		super(other);
	}
	
	@Override
	public UID getPrimaryKey() {
		return getUID();
	}

	@Override
	public void setPrimaryKey(UID primaryKey) {
		setUID(primaryKey);
	}

	@Override
	public final void flagNew() {
		this.state = STATE_NEW;
	}
	
	public final void reset() {
		this.state = STATE_UNCHANGED;
	}
	
	@Override
	public final void flagUpdate() {
		this.state = STATE_UPDATED;
	}
	
	@Override
	public final void flagRemove() {
		this.state = STATE_REMOVED;
	}

	@Override
	public final boolean isFlagNew() {
		return this.state == STATE_NEW;
	}

	@Override
	public final boolean isFlagUpdated() {
		return this.state == STATE_UPDATED;
	}

	@Override
	public final boolean isFlagRemoved() {
		return this.state == STATE_REMOVED;
	}
	
	@Override
	public boolean isFlagUnchanged() {
		return this.state == STATE_UNCHANGED;
	}

	@Override
	public UID getDalEntity() {
		return E.ENTITY.getUID();
	}

	@Override
	public String processor() {
		return processor;
	}

	@Override
	public void processor(String p) {
		this.processor = p;
	}

	@Override
	public InternalTimestamp getCreatedAt() {
		return createdAt;
	}

	@Override
	public void setCreatedAt(InternalTimestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public String getCreatedBy() {
		return createdBy;
	}

	@Override
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public InternalTimestamp getChangedAt() {
		return changedAt;
	}

	@Override
	public void setChangedAt(InternalTimestamp changedAt) {
		this.changedAt = changedAt;
	}

	@Override
	public String getChangedBy() {
		return changedBy;
	}

	@Override
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}

	@Override
	public int getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		if (version == null) {
			version = Integer.valueOf(-1);
		}
		this.version = version;
	}

	public static Collection<FieldMeta<?>> toFieldMetas(Collection<NucletFieldMeta<?>> fields) {
		return new ArrayList<FieldMeta<?>>(CollectionUtils.transform(fields, new ToFieldMetaTransformer()));
	}
	
	public static class ToFieldMetaTransformer implements Transformer<NucletFieldMeta<?>, FieldMeta<?>> {
		@Override
		public FieldMeta<?> transform(NucletFieldMeta<?> i) {
			return i;
		}
		
	}

	public void setState(int newState) {
		this.state = newState;		
	}
	
	public int getState() {
		return this.state;
	}
	
}
