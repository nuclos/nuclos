//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SimpleMetaProvider implements IRigidMetaProvider {

	private final Collection<EntityMeta<?>> allEntities;
	private final Map<UID, EntityMeta<?>> mapEntitiesByPk;
	private final Map<UID, FieldMeta<?>> mapEntityFieldsByPk;

	public SimpleMetaProvider(final Collection<EntityMeta<?>> allEntities) {
		this.allEntities = allEntities;
		this.mapEntitiesByPk = allEntities.parallelStream().collect(Collectors.toMap(EntityMeta::getUID, Function.identity()));
		this.mapEntityFieldsByPk = allEntities.parallelStream()
				.flatMap(entityMeta -> entityMeta.getFields().stream())
				.collect(Collectors.toMap(FieldMeta::getUID, Function.identity()));
	}

	@Override
	public Collection<EntityMeta<?>> getAllEntities() {
		return allEntities;
	}

	@Override
	public <PK> EntityMeta<PK> getEntity(final UID entityUID) {
		return RigidUtils.uncheckedCast(mapEntitiesByPk.get(entityUID));
	}

	@Override
	public Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(final UID entityUID) {
		return mapEntitiesByPk.get(entityUID).getFields().parallelStream()
				.collect(Collectors.toMap(FieldMeta::getUID, Function.identity()));
	}

	@Override
	public FieldMeta<?> getEntityField(final UID fieldUID) {
		return mapEntityFieldsByPk.get(fieldUID);
	}

	@Override
	public EntityMeta<?> getByTablename(final String sTableName) {
		if (sTableName == null) {
			return null;
		}
		return allEntities.parallelStream().filter(entityMeta ->
					sTableName.equalsIgnoreCase(entityMeta.getDbTable()) ||
					entityMeta.getVirtualEntity() != null && sTableName.equalsIgnoreCase(entityMeta.getVirtualEntity()))
				.findAny()
				.orElse(null);
	}

	@Override
	public boolean isNuclosEntity(final UID entityUID) {
		return E.isNuclosEntity(entityUID);
	}

	@Override
	public boolean checkEntity(final UID entityUID) {
		return mapEntitiesByPk.containsKey(entityUID);
	}

	@Override
	public boolean checkEntityField(final UID entityFieldUID) {
		return mapEntityFieldsByPk.containsKey(entityFieldUID);
	}
}
