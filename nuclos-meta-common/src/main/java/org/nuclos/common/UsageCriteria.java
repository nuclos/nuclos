//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.AbstractDalVOBasic;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.functional.BinaryFunction;
import org.nuclos.common2.functional.FunctionalUtils;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * "UsageCriteria" consisting of module and process. This class is immutable.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class UsageCriteria implements Serializable, Comparable<UsageCriteria> {

	private static final long serialVersionUID = -7357563362566436100L;

	private static final Logger log = Logger.getLogger(UsageCriteria.class);

	private final UID entityUID;
	private final UID processUID;
	private final UID statusUID;
	private final String sCustom;

	/**
	 * for deserialization only
	 */
	protected UsageCriteria() {
		this.entityUID = null;
		this.processUID = null;
		this.statusUID = null;
		this.sCustom = null;
	}
	
	public UsageCriteria(UID entityUID, UID processUID, UID statusUID, String sCustom) {
		this.entityUID = entityUID;
		this.processUID = processUID;
		this.statusUID = statusUID;
		this.sCustom = sCustom;
	}
/*
	//TODO MULTINUCLET use 
	 * getEntityUID
	 * getProcessUID
	 * getStatusUID
	 * ...
	 * 
	public Integer getModuleId() {
		return iModuleId;
	}
	

	public Integer getProcessId() {
		return iProcessId;
	}

	public Integer getStatusId() {
		return iStatusId;
	}
 */
	
	public UID getEntityUID() {
		return this.entityUID;
	}
	
	public UID getStatusUID() {
		return this.statusUID;
	}
	
	public UID getProcessUID() {
		return this.processUID;
	}

	public String getCustom() {
		return sCustom;
	}

	public String getCustom1() {
		return (sCustom != null ? sCustom.split(":")[0] : null);
	}

	public String getCustom2() {
		return (sCustom != null ? (sCustom.contains(":") ? sCustom.split(":")[1] : null) : null);
	}

	@Override
	public boolean equals(Object o) {
		final boolean result;
		if (this == o) {
			result = true;
		}
		else if (!(o instanceof UsageCriteria)) {
			result = false;
		}
		else {
			final UsageCriteria that = (UsageCriteria) o;
			result = Objects.equals(this.getEntityUID(), that.getEntityUID())
					&& Objects.equals(this.getProcessUID(), that.getProcessUID())
							&& Objects.equals(this.getStatusUID(), that.getStatusUID())
								&& Objects.equals(this.getCustom(), that.getCustom());
		}
		return result;
	}

	@Override
	public int hashCode() {
		return mainHashCode() ^ RigidUtils.hashCode(getCustom());
	}

	@Override
	public String toString() {
		return "(ModuleId: " + getEntityUID() + ", ProcessId: " + getProcessUID() + ", StatusId: " + getStatusUID() + ", Custom: " + getCustom() + ")";
	}
	
	public int mainHashCode() {
		return RigidUtils.hashCode(getEntityUID()) ^ RigidUtils.hashCode(getProcessUID()) ^ RigidUtils.hashCode(getStatusUID());
	}

	/**
	 * imposes a partial order on UsageCriteria. Note that not all quintuples are comparable.
	 * For a pair of non-comparable quintuples, this method returns false.
	 * 
	 * §postcondition !this.isComparableTo(that) --&gt; !result
	 * 
	 * @param that
	 * @return Is <code>this &lt;= that</code>?
	 * @see #isComparableTo(UsageCriteria)
	 * @see #compareTo(UsageCriteria)
	 */
	public boolean isLessOrEqual(UsageCriteria that) {
		final boolean result;

		if (this == that) {
			result = true;
		}
		else if (!this.isComparableTo(that)) {
			result = false;
		}
		else {
			result = (this.asBinary() <= that.asBinary());
		}
		assert this.isComparableTo(that) || !result;
		return result;
	}

	/**
	 * §precondition that != null
	 * 
	 * @param that
	 * @return Is this comparable to that?
	 */
	public boolean isComparableTo(UsageCriteria that) {
		if (that == null) {
			throw new NullArgumentException("that");
		}
		return isComparable(this.getEntityUID(), that.getEntityUID())
				&& isComparable(this.getProcessUID(), that.getProcessUID())
				&& isComparable(this.getStatusUID(), that.getStatusUID())
				&& isCustomComparable(this.getCustom1(), that.getCustom1(), this.getCustom2(), that.getCustom2());
	}

	/**
	 * tries to compare this UsageCriteria to another. Note that not all quintuples are comparable.
	 * @param that
	 * @return
	 * @throws NuclosFatalException if <code>this</code> is not comparable to <code>o</code>.
	 */
	@Override
	public int compareTo(UsageCriteria that) {
		final int result;
		if (this.equals(that)) {
			result = 0;
		}
		else {
			if (!this.isComparableTo(that)) {
				throw new NuclosFatalException("The given usage criteria " + this + " and " + that + " are not comparable.");
			}
			result = this.isLessOrEqual(that) ? -1 : 1;
		}
		return result;
	}

	private int asBinary() {
		//TODO MULTINUCLET 
		return ((binary(this.getEntityUID()) << 4) | (binary(this.getProcessUID()) << 3) | (binary(this.getStatusUID()) << 2) | (binary(this.getCustom1()) << 1) | (binary(this.getCustom2())));
	}

	private static int binary(UID uid) {
		return uid == null ? 0 : 1;
	}
	
	private static int binary(String s) {
		return s == null ? 0 : 1;
	}

	private static boolean isComparable(UID uid1, UID uid2) {
		return uid1 == null || uid2 == null || uid1.equals(uid2);
	}
	
	private static boolean isComparable(String s1, String s2) {
		return s1 == null || s2 == null || s1.equals(s2);
	}

	private static boolean isCustomComparable(String c1_1, String c1_2, String c2_1, String c2_2) {
		if (c2_1 != null && c2_2 != null) {
			return isComparable(c1_1, c1_2) && isComparable(c2_1, c2_2);
		} else if (c2_1 != null) {
			return isComparable(c1_2, c2_1) || isComparable(c1_2, c1_1);
		} else if (c2_2 != null) {
			return isComparable(c1_1, c2_2) || isComparable(c1_1, c1_2);
		} else {
			return isComparable(c1_1, c1_2);
		}
	}

	/**
	 * Note that not all quintuples are comparable.
	 * @param q1
	 * @param q2
	 * @return the minimum of q1 and q2
	 * @throws NuclosFatalException if <code>q1</code> is not comparable to <code>q2</code>.
	 */
	public static UsageCriteria min(UsageCriteria q1, UsageCriteria q2) {
		return (q1.compareTo(q2) <= 0) ? q1 : q2;
	}

	/**
	 * Note that not all quintuples are comparable.
	 * @param q1
	 * @param q2
	 * @return the maximum of q1 and q2
	 * @throws NuclosFatalException if <code>q1</code> is not comparable to <code>q2</code>.
	 */
	public static UsageCriteria max(UsageCriteria q1, UsageCriteria q2) {
		return (q1.compareTo(q2) >= 0) ? q1 : q2;
	}

	/**
	 * @param collUsageCriteria
	 * @param usagecriteria
	 * @return the maximum usagecriteria contained in collUsageCriteria that is less or equal to the given usagecriteria.
	 */
	public static UsageCriteria getBestMatchingUsageCriteria(Collection<UsageCriteria> collUsageCriteria, UsageCriteria usagecriteria) {
		UsageCriteria result = null;
		for (UsageCriteria uc : collUsageCriteria) {
			if (uc.isMatchFor(usagecriteria)) {
				log.debug("uc: " + uc + " - usagecriteria: " + usagecriteria);
				if (result == null || uc.isComparableTo(result)) {
					result = (result == null) ? uc : max(result, uc);
				} else {
					log.warn(String.format("usage criterias: %s and %s are both matching requested usage criteria %s and are not comparable. %1$s is skipped.", uc, result, usagecriteria));
				}
			}
		}
		return result;
	}

	/**
	 * @param that
	 * @return this.equals(getGreatestCommonUsageCriteria(this, that)
	 */
	public boolean isMatchFor(UsageCriteria that) {
		return this.equals(getGreatestCommonUsageCriteria(this, that));
	}

	/**
	 * §precondition CollectionUtils.isNonEmpty(collusagecriteria)
	 * 
	 * @param collusagecriteria Collection&lt;UsageCriteria&gt;
	 * @return the greatest common quintuple in the given Collection
	 */
	public static UsageCriteria getGreatestCommonUsageCriteria(Collection<UsageCriteria> collusagecriteria) {
		if (!CollectionUtils.isNonEmpty(collusagecriteria)) {
			throw new IllegalArgumentException("collusagecriteria");
		}

		return FunctionalUtils.foldl1(new GreatestCommonUsageCriteria(), collusagecriteria);
	}

	/**
	 * §postcondition result.isLessOrEqual(q1) &amp;&amp; result.isLessOrEqual(q2)
	 * 
	 * @param q1
	 * @param q2
	 * @return the greatest common factor in terms of quintuples. This is the greatest common factor for each single element.
	 */
	public static UsageCriteria getGreatestCommonUsageCriteria(UsageCriteria q1, UsageCriteria q2) {
		String sCustom1 = gcf(q1.getCustom1(), q2.getCustom1());
		final String sCustom2 = gcf(q1.getCustom2() != null ? q1.getCustom2() : q1.getCustom1(), q2.getCustom2() != null ? q2.getCustom2() : q2.getCustom1());
		if (sCustom1 != null && sCustom2 != null && !sCustom1.equals(sCustom2)) {
			sCustom1 += ":" + sCustom2;
		} else if (sCustom1 == null) {
			sCustom1 = sCustom2;
		}
		final UsageCriteria result = new UsageCriteria(gcf(q1.getEntityUID(), q2.getEntityUID()),
				gcf(q1.getProcessUID(), q2.getProcessUID()), gcf(q1.getStatusUID(), q2.getStatusUID()), sCustom1);
		assert result.isLessOrEqual(q1) && result.isLessOrEqual(q2);
		return result;
	}
	
	public static <PK> UsageCriteria createUsageCriteriaFromLayoutUsage(EntityObjectVO<PK> eo, UsageCriteria ucFallBack) {
		UID entity = eo.getFieldUid(E.LAYOUTUSAGE.entity);
		UID process = eo.getFieldUid(E.LAYOUTUSAGE.process);
		UID state = eo.getFieldUid(E.LAYOUTUSAGE.state);
		String custom = eo.getFieldValue(E.LAYOUTUSAGE.custom);
		
		if (ucFallBack != null) {
			if (process == null) {
				process = ucFallBack.getProcessUID();
			}
			if (state == null) {
				state = ucFallBack.getStatusUID();
			}
			if (custom == null) {
				custom = ucFallBack.getCustom();
			}
		}
		
		return new UsageCriteria(entity, process, state, custom);		
	}
	
	public static <PK> UsageCriteria createUsageCriteriaFromEO(EntityObjectVO<PK> eo, String custom) {
		UID entity = eo.getDalEntity();
		UID process = eo.getFieldUid(SFE.PROCESS_UID.getUID(entity));
		UID state = eo.getFieldUid(SF.STATE_UID.getUID(entity));
		
		return new UsageCriteria(entity, process, state, custom);
	}

	public static UID getBestMatchingLayout(final UsageCriteria uc, final boolean bSearchScreen, final List<EntityObjectVO<UID>> layoutUsages) throws CommonFinderException {
		if (uc != null && layoutUsages != null) {
			final List<Pair<EntityObjectVO<UID>, UsageCriteria>> layoutUsagesByEntity = layoutUsages.parallelStream()
					.filter(eo -> Objects.equals(eo.getFieldUid(E.LAYOUTUSAGE.entity), uc.getEntityUID()))
					.map(eo -> new Pair<>(eo, createUsageCriteriaFromLayoutUsage(eo, null)))
					.collect(Collectors.toList());
			final UsageCriteria ucBestMatching = UsageCriteria.getBestMatchingUsageCriteria(layoutUsagesByEntity.stream().map(Pair::getY).collect(Collectors.toSet()), uc);
			if (ucBestMatching == null) {
				throw new CommonFinderException("No matching layout was found for usagecriteria " + uc + ".");
			}
			return layoutUsagesByEntity.stream()
					.filter(pair -> Objects.equals(pair.getY(), ucBestMatching))
					.findAny()
					.map(Pair::getX)
					.map(eo -> eo.getFieldUid(E.LAYOUTUSAGE.layout))
					.orElse(null);
		}
		return null;
	}

	public static UID getStateModelUID(final UsageCriteria uc, final List<EntityObjectVO<UID>> stateModelUsages) {
		if (uc != null && stateModelUsages != null) {
			return stateModelUsages.parallelStream()
					.filter(eo -> Objects.equals(eo.getFieldUid(E.STATEMODELUSAGE.nuclos_module), uc.getEntityUID()))
					.map(eo -> new Pair<>(eo, new UsageCriteria(
							uc.entityUID,
							eo.getFieldUid(E.STATEMODELUSAGE.process),
							null, null)))
					.filter(pair -> pair.getY().isMatchFor(uc))
					.max(new Comparator<Pair<EntityObjectVO<UID>, UsageCriteria>>() {
						@Override
						public int compare(final Pair<EntityObjectVO<UID>, UsageCriteria> pair1, final Pair<EntityObjectVO<UID>, UsageCriteria> pair2) {
							return pair1.getY().compareTo(pair2.getY());
						}
					})
					.map(pair -> pair.getX().getFieldUid(E.STATEMODELUSAGE.statemodel))
					.orElse(null);
		}
		return null;
	}

	public static UID getInitialStateUID(final UsageCriteria uc, final List<EntityObjectVO<UID>> stateModelUsages, final List<EntityObjectVO<UID>> states, final List<EntityObjectVO<UID>> stateTransitions) {
		return getInitialStateUID(getStateModelUID(uc, stateModelUsages), states, stateTransitions);
	}

	public static UID getInitialStateUID(final UID stateModelUID, final List<EntityObjectVO<UID>> states, final List<EntityObjectVO<UID>> stateTransitions) {
		if (stateModelUID != null && stateTransitions != null) {
			final Set<UID> stateUIDs = states.parallelStream()
					.filter(eo -> Objects.equals(eo.getFieldUid(E.STATE.model), stateModelUID))
					.map(AbstractDalVOBasic::getPrimaryKey)
					.collect(Collectors.toSet());

			return stateTransitions.parallelStream()
					.filter(eo -> eo.getFieldUid(E.STATETRANSITION.state1) == null
									&& stateUIDs.contains(eo.getFieldUid(E.STATETRANSITION.state2)))
					.findAny()
					.map(AbstractDalVOBasic::getPrimaryKey)
					.orElse(null);
		}
		return null;
	}

	/**
	 * §todo Strengthen postcondition:  (uid1 == null || uid2 == null || uid1.intValue() != uid2.intValue()) --> result == null
	 * 
	 * §postcondition (uid1 == null || uid2 == null) --&gt; result == null
	 * §postcondition Objects.equalss(uid1, uid2) --&gt; Objects.equalss(result, uid1)
	 * 
	 * @param uid1
	 * @param uid2
	 * @return the "greatest common factor"
	 */
	private static UID gcf(UID uid1, UID uid2) {
		final UID result = (Objects.equals(uid1, uid2) ? uid1 : null);

		assert !(uid1 == null || uid2 == null) || result == null;
		assert !Objects.equals(uid1, uid2) || Objects.equals(result, uid1);

		return result;
	}
	
	private static String gcf(String s1, String s2) {
		final String result = (Objects.equals(s1, s2) ? s1 : null);
		return result;
	}

	private static class GreatestCommonUsageCriteria implements BinaryFunction<UsageCriteria, UsageCriteria, UsageCriteria, RuntimeException> {
		@Override
		public UsageCriteria execute(UsageCriteria q1, UsageCriteria q2) {
			return getGreatestCommonUsageCriteria(q1, q2);
		}
	}

	public static class UsageCriteriaKeySerializer extends StdSerializer<UsageCriteria> {
		private static final String NULL_KEY_VALUE = "UsageCriteriaJsonNull";
		public UsageCriteriaKeySerializer() {
			super(UsageCriteria.class);
		}
		@SuppressWarnings("VulnerableCodeUsages")
		@Override
		public void serialize(final UsageCriteria uc, final com.fasterxml.jackson.core.JsonGenerator jgen, final SerializerProvider prov) throws IOException {
			jgen.writeFieldName(String.format(
					"%s;%s;%s;%s",
					Optional.ofNullable(uc.entityUID).map(uid -> Base64.encodeBase64String(uid.getString().getBytes(StandardCharsets.UTF_8))).orElse(NULL_KEY_VALUE),
					Optional.ofNullable(uc.processUID).map(uid -> Base64.encodeBase64String(uid.getString().getBytes(StandardCharsets.UTF_8))).orElse(NULL_KEY_VALUE),
					Optional.ofNullable(uc.statusUID).map(uid -> Base64.encodeBase64String(uid.getString().getBytes(StandardCharsets.UTF_8))).orElse(NULL_KEY_VALUE),
					Optional.ofNullable(uc.sCustom).map(s -> Base64.encodeBase64String(s.getBytes(StandardCharsets.UTF_8))).orElse(NULL_KEY_VALUE)
			));
		}
	}

	public static class UsageCriteriaKeyDeserializer extends KeyDeserializer {
		@SuppressWarnings("VulnerableCodeUsages")
		@Override
		public Object deserializeKey(final String s, final DeserializationContext ctx) throws IOException {
			final String[] splitted = s.split(";");
			if (splitted.length == 4) {
				final UID entityUID = UID.parseUID(UsageCriteriaKeySerializer.NULL_KEY_VALUE.equals(splitted[0]) ? null : new String(Base64.decodeBase64(splitted[0]), StandardCharsets.UTF_8));
				final UID processUID = UID.parseUID(UsageCriteriaKeySerializer.NULL_KEY_VALUE.equals(splitted[1]) ? null : new String(Base64.decodeBase64(splitted[1]), StandardCharsets.UTF_8));
				final UID statusUID = UID.parseUID(UsageCriteriaKeySerializer.NULL_KEY_VALUE.equals(splitted[2]) ? null : new String(Base64.decodeBase64(splitted[2]), StandardCharsets.UTF_8));
				final String sCustom = UsageCriteriaKeySerializer.NULL_KEY_VALUE.equals(splitted[3]) ? null : new String(Base64.decodeBase64(splitted[3]), StandardCharsets.UTF_8);
				return new UsageCriteria(entityUID, processUID, statusUID, sCustom);
			}
			throw new IOException(String.format("UsageCriteria '%s' is not deserializable! Wrong format, should by 'Base64(entityUID);Base64(processUID);Base64(statusUID);Base64(sCustom)'", s));
		}
	}

}	// class UsageCriteria
