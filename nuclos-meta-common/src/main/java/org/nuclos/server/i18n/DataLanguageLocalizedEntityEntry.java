//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.i18n;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.InternalTimestamp;

public class DataLanguageLocalizedEntityEntry extends EntityObjectVO implements
		Serializable, org.nuclos.common.dal.vo.IDataLanguageLocalizedEntityEntry {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8166434797927862132L;
	public UID FIELD_LANGUGAE;
	public UID FIELD_ENTITY_ENTRY;
	public List<UID> FIELDS_LOCALIZED_FIELDS;

	/**
	 * for deserialization only
	 */
	protected DataLanguageLocalizedEntityEntry() {
	}
	
	public DataLanguageLocalizedEntityEntry(UID baseEntityUID,
											Long referencedEntityEntry,
											UID referencedDataLanguage,
											Map<UID, Object> mpFieldValues) {
		
		super(NucletEntityMeta.getEntityLanguageUID(baseEntityUID));
		
		setCreatedAt(InternalTimestamp.toInternalTimestamp(new Date()));
		setChangedAt(InternalTimestamp.toInternalTimestamp(new Date()));
		setCreatedBy("nuclos");
		setChangedBy("nuclos");
		setVersion(1);
		
		initMetaStructure(baseEntityUID);
		
		FIELDS_LOCALIZED_FIELDS = new ArrayList<UID>();
		
		// set values of all fields
		for (UID field : mpFieldValues.keySet()) {
			setFieldValue(field, mpFieldValues.get(field));		
			FIELDS_LOCALIZED_FIELDS.add(field);
		}
		
		setFieldId(this.FIELD_ENTITY_ENTRY, referencedEntityEntry);
		setFieldUid(this.FIELD_LANGUGAE, referencedDataLanguage);
	
		flagNew();
		
	}
	
	private void initMetaStructure(UID entityUID) {
		this.FIELD_LANGUGAE = DataLanguageUtils.extractDataLanguageReference(entityUID);
		this.FIELD_ENTITY_ENTRY = DataLanguageUtils.extractForeignEntityReference(entityUID);
		
	}

	/**
	 * Copy Constructor
	 * Initializes a DataLanguageLocalizedEntityEntry to have the same values as the eovo parameter.
	 * @param eovo
	 */
	public DataLanguageLocalizedEntityEntry(DataLanguageLocalizedEntityEntry eovo) {
		super(eovo.getDalEntity());

		UID baseEntity = UID.parseUID(eovo.getDalEntity().getString().replace("_LANG", ""));
		initMetaStructure(baseEntity);

		setPrimaryKey(eovo.getPrimaryKey());
		
		setVersion(eovo.getVersion());
		setChangedAt(eovo.getChangedAt());
		setChangedBy(eovo.getChangedBy());
		setCreatedAt(eovo.getCreatedAt());
		setCreatedBy(eovo.getCreatedBy());

		for (UID field : (Set<UID>) eovo.getFieldValues().keySet()) {
			setFieldValue(field, eovo.getFieldValues().get(field));
		}
		setFieldId(this.FIELD_ENTITY_ENTRY, eovo.getFieldId(this.FIELD_ENTITY_ENTRY));
		setFieldUid(this.FIELD_LANGUGAE, eovo.getFieldUid(this.FIELD_LANGUGAE));
		
		reset();
	}
	
	@Override
	public String getValue(UID field) {
		return this.getFieldValue(field, String.class) != null ? 
				(String) this.getFieldValue(field, String.class) : null;
	}
	
	@Override
	public Map<UID, String> getValueMap() {
		return this.getFieldValues();
	}
	
	@Override
	public void setValue(UID field, String value) {
		this.setFieldValue(field, value);
		if (!isFlagRemoved() && !isFlagNew()) flagUpdate();
	}
	
	@Override
	public boolean isInitialValueSet(UID field) {
		UID fieldFlagged = DataLanguageUtils.extractFlaggedFieldFromLangField(field);
		return getFieldValue(fieldFlagged) != null ? 
				(Boolean) getFieldValue(fieldFlagged) : Boolean.FALSE;
	}
	
	@Override
	public void setEntityEntryPrimaryKey(Long value) {
		this.setFieldId(this.FIELD_ENTITY_ENTRY, value);
	}
	
	@Override
	public Long getEntityEntryPrimaryKey() {
		return this.getFieldId(this.FIELD_ENTITY_ENTRY);
	}
	
	@Override
	public void setLanguage(UID value) {
		this.setFieldUid(this.FIELD_LANGUGAE, value);
	}
	
	@Override
	public UID getLanguage() {
		return this.getFieldUid(this.FIELD_LANGUGAE);
	}
	
	@Override
	public DataLanguageLocalizedEntityEntry copy() {
		return this.copy(false);
	}
	
	@Override
	public DataLanguageLocalizedEntityEntry clone() {
		return this.copy(true);
	}
	
	private DataLanguageLocalizedEntityEntry copy(boolean isCloneWithKeys) {
		DataLanguageLocalizedEntityEntry copy = 
				new DataLanguageLocalizedEntityEntry(this);
		
		if (!isCloneWithKeys) {
			copy.setPrimaryKey(null);
			copy.setEntityEntryPrimaryKey(null);
			copy.flagNew();			
		}
		
		return copy;
	}

	@Override
	public void resetInitialFlags(Collection<FieldMeta<?>> fields) {
		for(FieldMeta field : fields) {
			UID fieldFlagged = DataLanguageUtils.extractFieldUID(field.getUID(), true);
			setFieldValue(fieldFlagged, Boolean.TRUE);
		}
	}

	@Override
	public UID getFieldEntityEntryUID() {
		return FIELD_ENTITY_ENTRY;
	}

}
