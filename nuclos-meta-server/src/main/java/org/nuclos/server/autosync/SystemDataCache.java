//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.ISystemDataCache;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.PredicateUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class SystemDataCache implements ISystemDataCache {

	private final EntityMeta<?> entity;
	private final Map<UID, EntityObjectVO<UID>> elements;

	public SystemDataCache(EntityMeta<?> entity) {
		this.entity = entity;
		this.elements = new HashMap<UID, EntityObjectVO<UID>>();
	}

	protected void addAll(Iterable<EntityObjectVO<UID>> mdvos) {
		for (EntityObjectVO<UID> mdvo : mdvos) {
			UID id = mdvo.getPrimaryKey();
			if (id == null || elements.containsKey(id))
				throw new IllegalArgumentException("Invalid/duplicate id " + id + " for system entity " + entity.getEntityName());
			elements.put(id, mdvo);
		}
	}

	public List<EntityObjectVO<UID>> getAll() {
		List<EntityObjectVO<UID>> result = new ArrayList<EntityObjectVO<UID>>(elements.values());
		Collections.sort(result, new Comparator<EntityObjectVO<UID>>() {
			@Override
			public int compare(EntityObjectVO<UID> o1, EntityObjectVO<UID> o2) {
				return RigidUtils.compare(o1.getPrimaryKey(), o2.getPrimaryKey());
			}});
		return Collections.unmodifiableList(result);
	}

	public Set<UID> getAllIds() {
		return Collections.unmodifiableSet(elements.keySet());
	}

	public EntityObjectVO<UID> getById(UID id) {
		return elements.get(id);
	}

	public EntityObjectVO<UID> findVO(UID fieldUID, Object value) {
		return CollectionUtils.findFirst(elements.values(), new FieldEqualsPredicate(fieldUID, value));
	}

	public EntityObjectVO<UID> findVO(UID fieldUID1, Object value1, Object...opt) {
		return CollectionUtils.findFirst(elements.values(), makeFieldEqualsPredicate(fieldUID1, value1, opt));
	}

	public List<EntityObjectVO<UID>> findAllVO(UID fieldUID1, Object value1) {
		return findAllVO(new FieldEqualsPredicate(fieldUID1, value1));
	}

	public List<EntityObjectVO<UID>> findAllVO(UID fieldUID1, Object value1, Object...opt) {
		return findAllVO(makeFieldEqualsPredicate(fieldUID1, value1, opt));
	}

	private static Predicate<EntityObjectVO<UID>> makeFieldEqualsPredicate(UID fieldUID, Object value, Object[] opt) {
		Predicate<EntityObjectVO<UID>>[] predicates = new Predicate[1 + (opt.length / 2)];
		predicates[0] = new FieldEqualsPredicate(fieldUID, value);
		for (int i = 0, k = 1; i < opt.length; i += 2) {
			predicates[k++] = new FieldEqualsPredicate((UID) opt[i], opt[i + 1]);
		}
		return PredicateUtils.and(predicates);
	}

	public List<EntityObjectVO<UID>> findAllVOIn(UID fieldUID1, Collection<?> col1) {
		return findAllVO(new FieldInPredicate(fieldUID1, col1));
	}

	// TODO: clone to restrict manipulation
	public List<EntityObjectVO<UID>> findAllVO(Predicate<EntityObjectVO<UID>> predicate) {
		if (predicate == null)
			return getAll();
		return CollectionUtils.<EntityObjectVO<UID>>applyFilter(elements.values(), predicate);
	}
	
	private static class FieldInPredicate implements Predicate<EntityObjectVO<UID>> {

		private final UID	fieldUID;
		private final Collection<?> values;

		public FieldInPredicate(UID fieldUID, Collection<?> values) {
			this.fieldUID = fieldUID;
			this.values = values;
		}

		@Override
		public boolean evaluate(EntityObjectVO<UID> mdvo) {
			return values.contains(mdvo.getFieldValue(fieldUID));
		}
	}
	
	private static class FieldEqualsPredicate implements Predicate<EntityObjectVO<UID>> {

		private final UID	fieldUID;
		private final Object value;

		public FieldEqualsPredicate(UID fieldUID, Object value) {
			this.fieldUID = fieldUID;
			this.value = value;
		}

		@Override
		public boolean evaluate(EntityObjectVO<UID> mdvo) {
			return ObjectUtils.equals(mdvo.getFieldValue(fieldUID), value);
		}
	}
}
