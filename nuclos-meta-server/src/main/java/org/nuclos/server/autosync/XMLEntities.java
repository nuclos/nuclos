//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IXMLEntities;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.dal.vo.AbstractDalVOBasic;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class XMLEntities implements IXMLEntities {

	private final Logger LOG = LoggerFactory.getLogger(XMLEntities.class);

	public boolean DEV = true;

	private Map<EntityMeta<?>, SystemDataCache> internalDataCaches;

	@PostConstruct
	private void init()
			// throws IOException, XMLStreamException, ParseException
			//   Do not throw any exceptions here! Glassfish won't startup...
			//   Exception while deploying the app [nuclos-war] : The lifecycle method [init] must not throw a checked exception.
			//   Related annotation information: annotation [@javax.annotation.PostConstruct()]
			//   on annotated element [private void org.nuclos.server.autosync.XMLEntities.init() throws ...
	{
		if (PostConstructManager.isDisabled()) {
			return;
		}
		internalDataCaches = new HashMap<EntityMeta<?>, SystemDataCache>();

		initInternalDataJSON(E.DBTYPE, "dbtype");
		initInternalDataJSON(E.DBOBJECTTYPE, "dbobjecttype");
		initInternalDataJSON(E.LOCALE, "locale");
		initInternalDataJSON(E.LOCALERESOURCE, "localeresource");
		initInternalDataJSON(E.ACTION, "action");
		initInternalDataJSON(E.LAYOUT, "layout");
		initInternalDataJSON(E.DATATYPE, "datatype");
		initInternalDataJSON(E.RELATIONTYPE, "relationtype");
		initInternalDataJSON(E.EMAILCONNECTIONSECURITY, "emailConnectionSecurity");
		initInternalDataJSON(E.EMAILAUTHENTICATIONMETHOD, "emailAuthenticationMethod");
		initInternalDataJSON(E.EMAILINCOMINGSERVERTYPE, "emailIncomingServerType");

		// Add Layout ML strings from resources
		// TODO: Why is layoutML a String field?  It's a physical XML (i.e. it contains an encoding declaration(!))
		for (EntityObjectVO<?> mdvo : getOrCreate(E.LAYOUT).getAll()) {
			final String layoutMLResource = "resources/layoutml/" + mdvo.getFieldValue(E.LAYOUT.name) + ".layoutml";
			final InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(layoutMLResource);
			if (is != null) {
				final BufferedInputStream bis = new BufferedInputStream(is);
				try {
					Charset cs = IOUtils.guessXmlEncoding(bis);
					if (cs == null) {
						cs = XMLUtils.DEFAULT_LAYOUTML;
					}
					final String layoutML = IOUtils.readFromTextStream(bis, cs.name());
					mdvo.setFieldValue(E.LAYOUT.layoutML, layoutML);
				} catch (IOException e) {
					LOG.error("Error reading layout file {}: {}", layoutMLResource, e.getMessage(), e);
				}
			}
		}
	}
	
	public SystemDataCache getData(EntityMeta<?> entity) {
		return internalDataCaches.get(entity);
	}

	public SystemDataCache getData(UID entityUID) {
		return internalDataCaches.get(E.getByUID(entityUID));
	}

	public boolean hasSystemData(UID entityUID) {
		EntityMeta<?> nuclosEntity = E.getByUID(entityUID);
		return nuclosEntity != null && internalDataCaches.containsKey(nuclosEntity);
	}

	public Collection<EntityObjectVO<UID>> getSystemObjects(UID entityUID) {
		if (!hasSystemData(entityUID))
			return Collections.emptyList();
		return getData(entityUID).getAll();
	}

	public Collection<EntityObjectVO<UID>> getSystemObjects(UID entityUID, Predicate<EntityObjectVO<UID>> predicate) {
		if (!hasSystemData(entityUID))
			return Collections.emptyList();
		return getData(entityUID).findAllVO(predicate);
	}

	public Collection<EntityObjectVO<UID>> getSystemObjectsWith(UID entityUID, UID fieldUID, Object value) {
		if (!hasSystemData(entityUID))
			return Collections.emptyList();
		return getData(entityUID).findAllVO(fieldUID, value);
	}

	public Collection<EntityObjectVO<UID>> getSystemObjectsWith(UID entityUID, UID fieldUID, Object...values) {
		if (!hasSystemData(entityUID))
			return Collections.emptyList();
		return getData(entityUID).findAllVOIn(fieldUID, Arrays.asList(values));
	}

	public Collection<UID> getSystemObjectIds(UID entityUID, Predicate<EntityObjectVO<UID>> predicate) {
		return getSystemObjects(entityUID, predicate).parallelStream().map(AbstractDalVOBasic::getPrimaryKey).collect(Collectors.toList());
	}

	public Collection<UID> getSystemObjectIdsWith(UID entityUID, UID fieldUID, Object value) {
		return getSystemObjectsWith(entityUID, fieldUID, value).parallelStream().map(AbstractDalVOBasic::getPrimaryKey).collect(Collectors.toList());
	}

	public EntityObjectVO<UID> getSystemObjectById(UID entityUID, UID id) {
		if (!hasSystemData(entityUID))
			return null;
		return getData(entityUID).getById(id);
	}

	private void registerData(EntityMeta<UID> entity, Collection<EntityObjectVO<UID>> mdvos) {
		for (EntityObjectVO<UID> mdvo : mdvos) {
			IDependentDataMap dependents = mdvo.getDependents();
			if (dependents != null) {
				for (IDependentKey dependentKey : dependents.getKeySet()) {
					boolean registered = false;
					for (EntityMeta<?> dependentEntityMeta : E.getAllEntities()) {
						if (registered) {
							break;
						}
						for (FieldMeta<?> fieldMeta : dependentEntityMeta.getFields()) {
							if (fieldMeta.getUID().equals(dependentKey.getDependentRefFieldUID())) {
								Collection<EntityObjectVO<UID>> dependantVOs = dependents.getDataPk(dependentKey);
								registerData((EntityMeta<UID>) dependentEntityMeta, dependantVOs);
								registered = true;
								break;
							}
						}
					}
					
				}
			}
		}

		SystemDataCache cache = getOrCreate(entity);
		cache.addAll(mdvos);

//		for (EntityObjectVO<UID> mdvo : mdvos) {
//			//create DbOjects
//			if (E.DBSOURCE.checkEntityUID(mdvo.getEntityObject().getDalEntity())) {
//				try {
//					masterDataFacadeHelper.updateDbObject(null, mdvo.getEntityObject(), 1, false);
//				} catch (NuclosBusinessException | CommonFinderException | CommonPermissionException e) {
//					//TODO
//				}
//			}
//		}
	}

	private SystemDataCache getOrCreate(EntityMeta<?> entity) {
		SystemDataCache cache = internalDataCaches.get(entity);
		if (cache == null) {
			cache = new SystemDataCache(entity);
			internalDataCaches.put(entity, cache);
		}
		return cache;
	}

	private void initInternalDataJSON(EntityMeta<UID> entity, String file) {
		try {
			registerData(entity, parseJSON(entity, file));
		} catch (IOException | ParseException e) {
			LOG.error("Error parsing json file {}: {}", file, e.getMessage(), e);
		}
	}

	public Object readJSON(String file) throws IOException, ParseException {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("resources/data/" + file + ".json");
		if (is == null) {
			return null;
		}
		return JSONValue.parseWithException(new BufferedReader(new InputStreamReader(is, "utf-8")));
	}

	public List<EntityObjectVO<UID>> parseJSON(EntityMeta<UID> entity, String file) throws IOException, ParseException {
		return mdvoListFromJSON(entity, (List<?>) readJSON(file));
	}

	public List<EntityObjectVO<UID>> mdvoListFromJSON(EntityMeta<UID> entity, List<?> list) {
		List<EntityObjectVO<UID>> mdvos = new ArrayList<EntityObjectVO<UID>>();
		for (Object obj : list) {
			EntityObjectVO<UID> mdvo = mdvo(entity, (Map<?, ?>) obj);
			if (mdvo != null) {
				mdvos.add(mdvo);
			}
		}
		return mdvos;
	}

	public EntityObjectVO<UID> mdvo(EntityMeta<UID> entity, Map<?, ?> map) {
		return JSONHelper.makeEntityObjectVO(map, entity);
	}

}
