//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.AbstractDalVOWithVersion;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.dal.processor.DalChangeNotification;
import org.nuclos.server.dal.processor.DalChangeNotificationForTransactionSync;
import org.nuclos.server.dal.processor.nuclos.IEntityObjectProcessor;
import org.nuclos.server.dblayer.DbException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import io.reactivex.rxjava3.subjects.PublishSubject;

@Configurable
public class XmlEoDalUnionProcessor implements IEntityObjectProcessor<UID> {

	private IEntityObjectProcessor<UID> nuclosDalProcessor;

	private XMLEntities xmlEntities;

	private SystemDataCache xmlData;

	private XmlEoDalUnionProcessor() {
	}

	public static XmlEoDalUnionProcessor basedOn(final IEntityObjectProcessor<UID> nuclosDalProcessor) {
		XmlEoDalUnionProcessor result = new XmlEoDalUnionProcessor();
		result.nuclosDalProcessor = nuclosDalProcessor;
		result.initData();
		return result;
	}

	@Autowired
	private void inject(XMLEntities xmlEntities) {
		this.xmlEntities = xmlEntities;
	}

	private void initData() {
		this.xmlData = xmlEntities.getData(getEntityUID());
	}

	@Override
	public UID getEntityUID() {
		return nuclosDalProcessor.getEntityUID();
	}

	public boolean isXmlEo(UID id) {
		return id != null && xmlData.getById(id) != null;
	}

	@Override
	public List<EntityObjectVO<UID>> getAll() {
		return extend(nuclosDalProcessor.getAll(), xmlData.getAll());
	}

	@Override
	public List<UID> getAllIds() {
		return extend(nuclosDalProcessor.getAllIds(), xmlData.getAllIds());
	}

	@Override
	public EntityObjectVO<UID> getByPrimaryKey(final UID id) {
		return Optional.ofNullable(nuclosDalProcessor.getByPrimaryKey(id))
				       .orElseGet(() -> xmlData.getById(id));
	}

	@Override
	public EntityObjectVO<UID> getByPrimaryKey(final UID id, final Collection<FieldMeta<?>> fields) {
		return Optional.ofNullable(nuclosDalProcessor.getByPrimaryKey(id, fields))
					   .orElseGet(() -> xmlData.getById(id));
	}

	@Override
	public List<EntityObjectVO<UID>> getByPrimaryKeys(final List<UID> ids) {
		Set<UID> setOfIds = new HashSet<>(ids);
		return extend(nuclosDalProcessor.getByPrimaryKeys(ids),
				xmlData.getAll().stream()
						.filter(eo -> setOfIds.contains(eo.getPrimaryKey()))
						.collect(Collectors.toList()));
	}

	@Override
	public void freeMemory(final boolean includeLinkedProcessors) {
		nuclosDalProcessor.freeMemory(includeLinkedProcessors);
	}

	@Override
	public long getTotalMemoryUsage(final boolean includeLinkedProcessors) {
		return nuclosDalProcessor.getTotalMemoryUsage(includeLinkedProcessors);
	}

	@Override
	public Integer getVersion(final UID uid) {
		return Optional.ofNullable(xmlData.getById(uid)).map(AbstractDalVOWithVersion::getVersion).orElseGet(() -> nuclosDalProcessor.getVersion(uid));
	}

	@Override
	public Object insertOrUpdate(final EntityObjectVO<UID> dalVO) throws DbException {
		if (isXmlEo(dalVO.getPrimaryKey())) {
			throw new IllegalArgumentException(String.format("The EntityObject with UID %s is not updatable", dalVO.getPrimaryKey().getString()));
		}
		return nuclosDalProcessor.insertOrUpdate(dalVO);
	}

	@Override
	public void checkLogicalUniqueConstraint(final EntityObjectVO<UID> dalVO) throws DbException {
		nuclosDalProcessor.checkLogicalUniqueConstraint(dalVO);
	}

	@Override
	public void delete(final Delete<UID> del) throws DbException {
		if (isXmlEo(del.getPrimaryKey())) {
			throw new IllegalArgumentException(String.format("The EntityObject with UID %s is not deletable", del.getPrimaryKey().getString()));
		}
		nuclosDalProcessor.delete(del);
	}

	@Override
	public void setThinReadEnabled(final boolean enabled) {
		nuclosDalProcessor.setThinReadEnabled(enabled);
	}

	@Override
	public boolean isThinReadEnabled() {
		return nuclosDalProcessor.isThinReadEnabled();
	}

	@Override
	public PublishSubject<DalChangeNotification<EntityObjectVO<UID>, UID>> observeAllChangesImmediately(final boolean bNotifyClusterWide) {
		return nuclosDalProcessor.observeAllChangesImmediately(bNotifyClusterWide);
	}

	@Override
	public PublishSubject<DalChangeNotification<EntityObjectVO<UID>, UID>> observeInsertOnlyImmediately(final boolean bNotifyClusterWide) {
		return nuclosDalProcessor.observeInsertOnlyImmediately(bNotifyClusterWide);
	}

	@Override
	public PublishSubject<DalChangeNotification<EntityObjectVO<UID>, UID>> observeUpdateOnlyImmediately(final boolean bNotifyClusterWide) {
		return nuclosDalProcessor.observeUpdateOnlyImmediately(bNotifyClusterWide);
	}

	@Override
	public PublishSubject<DalChangeNotification<EntityObjectVO<UID>, UID>> observeDeleteOnlyImmediately(final boolean bNotifyClusterWide) {
		return nuclosDalProcessor.observeDeleteOnlyImmediately(bNotifyClusterWide);
	}

	@Override
	public PublishSubject<DalChangeNotificationForTransactionSync<UID>> observeChangesNotifyAfterCommit(final boolean bNotifyClusterWide) {
		return nuclosDalProcessor.observeChangesNotifyAfterCommit(bNotifyClusterWide);
	}

	@Override
	public PublishSubject<DalChangeNotificationForTransactionSync<UID>> observeChangesNotifyBeforeCommit(final boolean bNotifyClusterWide) {
		return nuclosDalProcessor.observeChangesNotifyBeforeCommit(bNotifyClusterWide);
	}

	@Override
	public void handleIncomingClusterNotification(final DalChangeNotificationForTransactionSync<UID> notification) {
		nuclosDalProcessor.handleIncomingClusterNotification(notification);
	}

	@Override
	public void destroy() throws Exception {
		nuclosDalProcessor.destroy();
	}

	private <X> List<X> extend(final Collection<X> tryToExtend, final Collection<X> with) {
		if (tryToExtend instanceof ArrayList) {
			tryToExtend.addAll(with);
			return (List<X>) tryToExtend;
		} else {
			List<X> result = new ArrayList<>(tryToExtend.size() + with.size());
			result.addAll(tryToExtend);
			result.addAll(with);
			return result;
		}
	}
}
