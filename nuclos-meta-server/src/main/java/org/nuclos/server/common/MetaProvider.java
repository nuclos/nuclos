//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.nuclos.common.AbstractDisposableRegistrationBean;
import org.nuclos.common.CommonMetaDataServerProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityContext;
import org.nuclos.common.EntityLafParameterVO;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMapTransport;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.IFieldMetaComplementor;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.LafParameter;
import org.nuclos.common.LafParameterByEntityMap;
import org.nuclos.common.LafParameterHashMap;
import org.nuclos.common.LafParameterMap;
import org.nuclos.common.Mutable;
import org.nuclos.common.MutableMap;
import org.nuclos.common.NucletEntityContext;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.util.DalTransformations;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dal.MetaDalUtils;
import org.nuclos.server.dal.processor.DalChangeNotificationForTransactionSync;
import org.nuclos.server.dal.processor.jdbc.impl.ChartMetaDataProcessor;
import org.nuclos.server.i18n.DataLanguageSetup;
import org.nuclos.server.dal.processor.jdbc.impl.DynamicMetaDataProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DynamicTasklistMetaDataProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.EntityContextProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.EntityFieldMetaProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.EntityLafParameterProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.EntityMetaProcessor;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.dal.specification.IDalReadSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.BehaviorSubject;

/**
 * An caching singleton for accessing the meta data information
 * on the server side.
 */
@Component("metaDataProvider")
@Qualifier("metaDataProvider")
public class MetaProvider extends AbstractDisposableRegistrationBean implements IMetaProvider, CommonMetaDataServerProvider, INucletCache {
	
	private static final Logger LOG = LoggerFactory.getLogger(MetaProvider.class);

	private static final Set<IFieldMetaComplementor> FIELD_COMPLEMENTORS = Collections.synchronizedSet(new HashSet<>());

	private final BehaviorSubject<Collection<EntityMeta<?>>> observableEntityMetaRefreshAfterCommit = BehaviorSubject.<Collection<EntityMeta<?>>> createDefault(Collections.emptyList());
	private final ThreadLocal<TransactionSynchronization> transactionSynchronizationsEntityMetaRefresh = new ThreadLocal<>();
	private final ThreadLocal<Collection<EntityMeta<?>>> transactionSynchronizationsEntityMetas = new ThreadLocal<>();

	private final NuclosDalProvider dalProvider;
	private final EntityMetaProcessor entityMetaProcessor;
	private final EntityFieldMetaProcessor entityFieldMetaProcessor;
	private final EntityContextProcessor entityContextProcessor;
	private final DynamicMetaDataProcessor dynamicMetaDataProcessor;
	private final ChartMetaDataProcessor chartMetaDataProcessor;
	private final DynamicTasklistMetaDataProcessor dynamicTasklistMetaDataProcessor;
	private final EntityLafParameterProcessor entityLafParameterProcessor;

	private final UID _mutexEntities = new UID();
	private final MutableMap<UID, EntityMeta<?>> mapAllEntities = new MutableMap<>();
	private final Map<UID, EntityMeta<?>> mapAllEntitiesUnmodifiable = Collections.unmodifiableMap(mapAllEntities);
	private final MutableMap<UID, EntityMeta<?>> mapNucletEntities = new MutableMap<>();
	private final MutableMap<UID, EntityMeta<?>> mapDynamicEntities = new MutableMap<>();
	private final MutableMap<UID, EntityMeta<?>> mapChartEntities = new MutableMap<>();
	private final MutableMap<UID, EntityMeta<?>> mapDynamicTasklistEntities = new MutableMap<>();
	private final MutableMap<UID, FieldMeta<?>> mapEntityFields = new MutableMap<>();
	private final MutableMap<UID, FieldMeta<?>> mapAllEntityFields = new MutableMap<>();
	private final List<NucletEntityContext> lstEntityContext = new ArrayList<>();
	private final UID _mutexLaf = new UID();
	private final MutableMap<UID, LafParameterMap<Object, String>> mapAllLafParameter = new MutableMap<>();
	private final LafParameterByEntityMap lafParameterMap = new LafParameterByEntityMap(RigidUtils.uncheckedCast(mapAllLafParameter));
	private final UID _mutexGenerics = new UID();
	private final MutableMap<UID, List<EntityObjectVO<UID>>> mapGenericImplementationsByGeneric = new MutableMap<>();
	private final MutableMap<UID, List<EntityObjectVO<UID>>> mapGenericFieldMappingByImplementation = new MutableMap<>();

	@Autowired
	public MetaProvider(
			final NuclosDalProvider dalProvider,
			final EntityMetaProcessor entityMetaProcessor,
			final EntityFieldMetaProcessor entityFieldMetaProcessor,
			final EntityContextProcessor entityContextProcessor,
			final DynamicMetaDataProcessor dynamicMetaDataProcessor,
			final ChartMetaDataProcessor chartMetaDataProcessor,
			final DynamicTasklistMetaDataProcessor dynamicTasklistMetaDataProcessor,
			final EntityLafParameterProcessor entityLafParameterProcessor) {
		this.dalProvider = dalProvider;
		this.entityMetaProcessor = entityMetaProcessor;
		this.entityFieldMetaProcessor = entityFieldMetaProcessor;
		this.entityContextProcessor = entityContextProcessor;
		this.dynamicMetaDataProcessor = dynamicMetaDataProcessor;
		this.chartMetaDataProcessor = chartMetaDataProcessor;
		this.dynamicTasklistMetaDataProcessor = dynamicTasklistMetaDataProcessor;
		this.entityLafParameterProcessor = entityLafParameterProcessor;
	}

	@PostConstruct
	public final void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		createAsyncFlowableWithBufferStrategyAndRegister(entityMetaProcessor.observeChangesNotifyBeforeCommit(true), notification ->
				refreshEntities(notification, mapNucletEntities, entityMetaProcessor));
		createAsyncFlowableWithBufferStrategyAndRegister(entityFieldMetaProcessor.observeChangesNotifyBeforeCommit(true),
				this::refreshEntityFields);
		createAsyncFlowableWithBufferStrategyAndRegister(entityContextProcessor.observeChangesNotifyBeforeCommit(true),
				this::refreshEntityContexts);
		createAsyncFlowableWithBufferStrategyAndRegister(dynamicMetaDataProcessor.observeChangesNotifyBeforeCommit(true), notification ->
				refreshEntities(notification, mapDynamicEntities, dynamicMetaDataProcessor));
		createAsyncFlowableWithBufferStrategyAndRegister(chartMetaDataProcessor.observeChangesNotifyBeforeCommit(true), notification ->
				refreshEntities(notification, mapChartEntities, chartMetaDataProcessor));
		createAsyncFlowableWithBufferStrategyAndRegister(dynamicTasklistMetaDataProcessor.observeChangesNotifyBeforeCommit(true), notification ->
				refreshEntities(notification, mapDynamicTasklistEntities, dynamicTasklistMetaDataProcessor));
		createAsyncFlowableWithBufferStrategyAndRegister(entityLafParameterProcessor.observeChangesNotifyBeforeCommit(true),
				this::refreshLafParameters);
		createAsyncFlowableWithBufferStrategyAndRegister(dalProvider.getEntityObjectProcessor(E.ENTITY_GENERIC_IMPLEMENTATION).observeChangesNotifyBeforeCommit(true),
				this::refreshGenericImplementations);
		createAsyncFlowableWithBufferStrategyAndRegister(dalProvider.getEntityObjectProcessor(E.ENTITY_GENERIC_FIELDMAPPING).observeChangesNotifyBeforeCommit(true),
				this::refreshGenericFieldMappings);
		createAsyncFlowableWithBufferStrategyAndRegister(dalProvider.getEntityObjectProcessor(E.TASKLIST).observeChangesNotifyBeforeCommit(true), notification ->
				refreshEntities(null, mapDynamicTasklistEntities, dynamicTasklistMetaDataProcessor));

		mapAllEntities.setValue(E.getAllEntities().stream().collect(Collectors.toMap(EntityMeta::getUID, Function.identity())));
		mapAllEntityFields.setValue(DalTransformations.flattenEntityMetaMap(mapAllEntities));

		forceCompleteRefresh();
	}

	public void forceCompleteRefresh() {
		refreshEntities(null, mapNucletEntities, entityMetaProcessor);
		refreshEntities(null, mapDynamicEntities, dynamicMetaDataProcessor);
		refreshEntities(null, mapChartEntities, chartMetaDataProcessor);
		refreshEntities(null, mapDynamicTasklistEntities, dynamicTasklistMetaDataProcessor);
		refreshEntityFields(null);
		refreshEntityContexts(null);
		refreshLafParameters(null);
		refreshGenericImplementations(null);
		refreshGenericFieldMappings(null);
	}

	private void refreshEntities(
			final DalChangeNotificationForTransactionSync<UID> notification,
			final MutableMap<UID, EntityMeta<?>> mapEntities,
			final IDalReadSpecification<NucletEntityMeta, UID> metaProc) {
		synchronized (_mutexEntities) {
			final boolean refreshNucletEntities = mapNucletEntities == mapEntities;
			final RefreshEntityDataContainer data = new RefreshEntityDataContainer();
			data.workingCopyForAll = mapAllEntities.workingCopy();
			data.workingCopyForAllFields = mapAllEntityFields.workingCopy();
			data.mapAllFieldsByEntity = data.workingCopyForAllFields.values().parallelStream().collect(Collectors.groupingByConcurrent(FieldMeta::getEntity));
			data.mapFieldsByEntity = refreshNucletEntities ?
					mapEntityFields.values().parallelStream().collect(Collectors.groupingByConcurrent(FieldMeta::getEntity)) : null;

			if (notification == null || notification.hasUnidentified() || notification.isRollbackOnly()) {
				data.workingCopy = new HashMap<>();
				mapEntities.keySet().forEach(pk -> remove(data, pk));
				metaProc.getAll().forEach(entityMeta -> replace(data, entityMeta, this));
			} else {
				data.workingCopy = mapEntities.workingCopy();
				Optional.ofNullable(notification.getInsertedAndUpdated()).ifPresent(pks -> pks.forEach(pk -> replace(data, metaProc.getByPrimaryKey(pk), this)));
				Optional.ofNullable(notification.getDeleted()).ifPresent(pks -> pks.forEach(pk -> remove(data, pk)));
			}

			if (refreshNucletEntities) {
				replaceAllEntityContexts(data.workingCopy.values(), lstEntityContext);
			}

			mapEntities.setValue(data.workingCopy);
			mapAllEntities.setValue(data.workingCopyForAll);
			mapAllEntityFields.setValue(data.workingCopyForAllFields);

			notifyRefresh(getAllEntities());
		}
	}

	private static class RefreshEntityDataContainer {
		Map<UID, EntityMeta<?>> workingCopy;
		Map<UID, EntityMeta<?>> workingCopyForAll;
		Map<UID, FieldMeta<?>> workingCopyForAllFields;
		Map<UID, List<FieldMeta<?>>> mapAllFieldsByEntity;
		Map<UID, List<FieldMeta<?>>> mapFieldsByEntity;
	}

	private static void replace(
			final RefreshEntityDataContainer data,
			final NucletEntityMeta entityMeta,
			final IRigidMetaProvider metaProv) {
		entityMeta.setMetaProvider(metaProv);
		data.workingCopy.put(entityMeta.getPrimaryKey(), entityMeta);
		data.workingCopyForAll.put(entityMeta.getPrimaryKey(), entityMeta);

		final List<FieldMeta<?>> entityFields;
		if (data.mapFieldsByEntity != null) {
			// entity meta without fields
			entityFields = Optional.ofNullable(data.mapFieldsByEntity.get(entityMeta.getUID())).orElseGet(ArrayList::new);
			MetaDalUtils.addStaticFields(entityFields, entityMeta);
		} else {
			// entity meta with fields from proc
			entityFields = new ArrayList<>(entityMeta.getFields());
			FieldMeta<?> pkMeta = entityMeta.getPk().getMetaData(entityMeta);
			if (!entityMeta.isChart() && !entityFields.contains(pkMeta)) {
				entityFields.add(pkMeta);
			}
		}

		// process data lang part 1/2
		remove(data, EntityMeta.getEntityLanguageUID(entityMeta));

		setFieldsAndRefreshAllFieldsWorkingCopy(
				entityMeta,
				data.mapAllFieldsByEntity.get(entityMeta.getUID()),
				entityFields,
				data.workingCopyForAllFields
		);

		// process data lang part 2/2
		Optional.ofNullable(DataLanguageSetup.createDataLanguageEntityMetaFor(entityMeta)).ifPresent(langEntityMeta -> {
			data.workingCopyForAll.put(langEntityMeta.getUID(), langEntityMeta);
			data.workingCopyForAllFields.putAll(langEntityMeta.getFields().stream().collect(Collectors.toMap(FieldMeta::getUID, Function.identity())));
		});
	}

	private static void setFieldsAndRefreshAllFieldsWorkingCopy(
			final EntityMeta<?> entityMeta,
			final List<FieldMeta<?>> oldEntityFields,
			final List<FieldMeta<?>> newEntityFields,
			final Map<UID, FieldMeta<?>> workingCopyForAllFields) {
		if (entityMeta instanceof NucletEntityMeta) {
			((NucletEntityMeta) entityMeta).setFields(newEntityFields);
		}
		Optional.ofNullable(oldEntityFields).ifPresent(fields ->
				fields.stream().map(FieldMeta::getUID).forEach(workingCopyForAllFields::remove));
		newEntityFields.forEach(field -> workingCopyForAllFields.put(field.getUID(), field));
	}

	private static void remove(
			final RefreshEntityDataContainer data,
			final UID entityUID) {
		Optional.ofNullable(data.workingCopy.remove(entityUID))
						.ifPresent(entityMeta -> {
							entityMeta.getFields().stream().map(FieldMeta::getUID).forEach(data.workingCopyForAllFields::remove);
						});
		final Set<UID> setFieldUids = data.workingCopyForAllFields.values()
				.parallelStream()
				.filter(fieldMeta -> Objects.equals(fieldMeta.getEntity(), entityUID))
				.map(FieldMeta::getUID)
				.collect(Collectors.toSet());
		setFieldUids.forEach(data.workingCopyForAllFields::remove);
		data.workingCopyForAll.remove(entityUID);
		if (!EntityMeta.isEntityLanguageUID(entityUID)) {
			remove(data, EntityMeta.getEntityLanguageUID(entityUID));
		}
	}

	private void refreshEntityFields(final DalChangeNotificationForTransactionSync<UID> notification) {
		refreshEntityFields(notification, true);
	}

	private void refreshEntityFields(final DalChangeNotificationForTransactionSync<UID> notification,
									 final boolean forwardRefreshNotification) {
		synchronized (_mutexEntities) {
			final RefreshEntityDataContainer entityData = new RefreshEntityDataContainer();
			entityData.workingCopy = mapNucletEntities.workingCopy();
			entityData.workingCopyForAll = mapAllEntities.workingCopy();
			entityData.workingCopyForAllFields = mapAllEntityFields.workingCopy();
			entityData.mapAllFieldsByEntity = entityData.workingCopyForAllFields.values().parallelStream().collect(Collectors.groupingByConcurrent(FieldMeta::getEntity));
			entityData.mapFieldsByEntity = mapEntityFields.values().parallelStream().collect(Collectors.groupingByConcurrent(FieldMeta::getEntity));

			final Map<UID, FieldMeta<?>> workingCopy;
			final Set<UID> entitiesToRefresh = new HashSet<>();

			if (notification == null || notification.hasUnidentified() || notification.isRollbackOnly()) {
				entitiesToRefresh.addAll(entityData.mapFieldsByEntity.keySet());
				workingCopy = new HashMap<>();
				entityFieldMetaProcessor.getAll().forEach(fieldMeta -> putAndComplement(workingCopy, fieldMeta, entitiesToRefresh));
			} else {
				workingCopy = mapEntityFields.workingCopy();
				Optional.ofNullable(notification.getInsertedAndUpdated()).ifPresent(pks -> pks.forEach(pk -> putAndComplement(workingCopy, entityFieldMetaProcessor.getByPrimaryKey(pk), entitiesToRefresh)));
				Optional.ofNullable(notification.getDeleted()).ifPresent(pks -> pks.forEach(pk -> remove(workingCopy, pk, entitiesToRefresh)));
			}

			if (!entitiesToRefresh.isEmpty()) {
				final Map<UID, List<FieldMeta<?>>> mapOldFieldsByEntity = entityData.workingCopyForAllFields.values().parallelStream()
						.filter(field -> entitiesToRefresh.contains(field.getEntity()))
						.collect(Collectors.groupingByConcurrent(FieldMeta::getEntity));
				final Map<UID, List<FieldMeta<?>>> mapNewFieldsByEntity = workingCopy.values().parallelStream()
						.filter(field -> entitiesToRefresh.contains(field.getEntity()))
						.collect(Collectors.groupingByConcurrent(FieldMeta::getEntity));
				entitiesToRefresh.stream()
						.map(mapAllEntities::get)
						.filter(Objects::nonNull)
						.forEach(entityMeta -> {
							// process data lang part 1/2
							remove(entityData, EntityMeta.getEntityLanguageUID(entityMeta));

							List<FieldMeta<?>> entityFields = Optional.ofNullable(mapNewFieldsByEntity.get(entityMeta.getUID())).orElseGet(ArrayList::new);
							MetaDalUtils.addStaticFields(entityFields, entityMeta);
							setFieldsAndRefreshAllFieldsWorkingCopy(
									entityMeta,
									mapOldFieldsByEntity.get(entityMeta.getUID()),
									entityFields,
									entityData.workingCopyForAllFields);

							// process data lang part 2/2
							Optional.ofNullable(DataLanguageSetup.createDataLanguageEntityMetaFor(entityMeta)).ifPresent(langEntityMeta -> {
								entityData.workingCopyForAll.put(langEntityMeta.getUID(), langEntityMeta);
								entityData.workingCopyForAllFields.putAll(langEntityMeta.getFields().stream().collect(Collectors.toMap(FieldMeta::getUID, Function.identity())));
							});
						});

				mapEntityFields.setValue(workingCopy);
				mapAllEntities.setValue(entityData.workingCopyForAll); // <-- necessary for data lang changes
				mapAllEntityFields.setValue(entityData.workingCopyForAllFields);
			}
			if (forwardRefreshNotification) {
				notifyRefresh(getAllEntities());
			}
		}
	}

	private static void putAndComplement(final Map<UID, FieldMeta<?>> workingCopy, final FieldMeta<?> fieldMeta, final Set<UID> entitiesToRefresh) {
		workingCopy.put(fieldMeta.getUID(), complement(fieldMeta));
		entitiesToRefresh.add(fieldMeta.getEntity());
	}

	public static FieldMeta<?> complement(final FieldMeta<?> fieldMeta) {
		if (fieldMeta instanceof FieldMetaVO) {
			FieldMetaVO<?> fieldMetaVO = (FieldMetaVO<?>) fieldMeta;
			FIELD_COMPLEMENTORS.forEach(complementor -> complementor.complementFieldMeta(fieldMetaVO));
		}
		return fieldMeta;
	}

	private static void remove(final Map<UID, FieldMeta<?>> workingCopy, UID fieldUID, final Set<UID> entitiesToRefresh) {
		Optional.ofNullable(workingCopy.remove(fieldUID)).ifPresent(field -> entitiesToRefresh.add(field.getEntity()));
	}

	private void refreshEntityContexts(final DalChangeNotificationForTransactionSync<UID> notification) {
		synchronized (_mutexEntities) {
			if (notification == null || notification.hasUnidentified() || notification.isRollbackOnly()) {
				lstEntityContext.clear();
				lstEntityContext.addAll(entityContextProcessor.getAll());
			} else {
				Optional.ofNullable(notification.getInsertedAndUpdated()).ifPresent(pks -> pks.forEach(pk -> lstEntityContext.add(entityContextProcessor.getByPrimaryKey(pk))));
				Optional.ofNullable(notification.getDeleted()).ifPresent(pks -> lstEntityContext.removeIf(context -> pks.contains(context.getPrimaryKey())));
			}
			replaceAllEntityContexts(mapNucletEntities.values(), lstEntityContext);

			notifyRefresh(getAllEntities());
		}
	}

	private static void replaceAllEntityContexts(Collection<EntityMeta<?>> allMetas, List<NucletEntityContext> lstEntityContext) {
		final Map<UID, List<NucletEntityContext>> mapContextsByMainEntity = lstEntityContext.parallelStream().collect(Collectors.groupingByConcurrent(EntityContext::getMainEntity));
		allMetas.parallelStream().filter(entityMeta -> !entityMeta.getEntityContexts().isEmpty())
				.forEach(entityMeta -> ((EntityMetaVO<?>)entityMeta).setEntityContexts(null));
		allMetas.parallelStream().filter(entityMeta -> mapContextsByMainEntity.containsKey(entityMeta.getUID()))
				.forEach(entityMeta -> ((EntityMetaVO<?>)entityMeta).setEntityContexts(new HashSet<>(mapContextsByMainEntity.get(entityMeta.getUID()))));
	}

	private void refreshLafParameters(final DalChangeNotificationForTransactionSync<UID> notification) {
		synchronized (_mutexLaf) {
			final Map<UID, LafParameterMap<Object, String>> workingCopy;
			final List<EntityLafParameterVO> lstLoaded;
			if (notification == null || notification.hasUnidentified() || notification.hasDeletes() || notification.isRollbackOnly()) {
				workingCopy = new HashMap<>();
				lstLoaded = entityLafParameterProcessor.getAll();
			} else {
				// only inserted and updated
				workingCopy = mapAllLafParameter.workingCopy();
				lstLoaded = entityLafParameterProcessor.getByPrimaryKeys(new ArrayList<>(Optional.ofNullable(notification.getInsertedAndUpdated()).orElseGet(HashSet::new)));
			}
			final Map<UID, LafParameterMap<Object, String>> syncCopy = Collections.synchronizedMap(workingCopy);
			lstLoaded.parallelStream()
					.filter(vo -> LafParameter.PARAMETERS.containsKey(vo.getParameter()))
					.collect(Collectors.groupingByConcurrent(EntityLafParameterVO::getEntity))
					.forEach((entityUID, lstGrouped) -> {
						LafParameterMap<Object, String> map = syncCopy.computeIfAbsent(entityUID, k -> new LafParameterHashMap<>());
						lstGrouped.forEach(vo -> {
							final LafParameter<Object, String> lafParameter = RigidUtils.uncheckedCast(LafParameter.PARAMETERS.get(vo.getParameter()));
							Object value;
							try {
								value = lafParameter.convertFromStore(vo.getValue());
							} catch (ParseException e) {
								LOG.warn("Can't convert LafParameter {} (entityId={}) value {}: {}",
										vo.getParameter(), vo.getEntity(), vo.getValue(), e.toString(), e);
								value = lafParameter.getDefault();
							} catch (Exception e) {
								LOG.warn("Can't read LafParameter {} (entityId={}) value {}: {}",
										vo.getParameter(), vo.getEntity(), vo.getValue(), e.toString(), e);
								value = lafParameter.getDefault();
							}
							map.putValue(lafParameter, value);
						});
					});
			mapAllLafParameter.setValue(workingCopy);
		}
	}

	private void refreshGenericImplementations(final DalChangeNotificationForTransactionSync<UID> notification) {
		synchronized (_mutexGenerics) {
			final Map<UID, List<EntityObjectVO<UID>>> workingCopy;
			final List<EntityObjectVO<UID>> lstLoaded;
			final Set<UID> updatedIds;
			if (notification == null || notification.hasUnidentified() || notification.hasDeletes() || notification.isRollbackOnly()) {
				workingCopy = new HashMap<>();
				lstLoaded = dalProvider.getEntityObjectProcessor(E.ENTITY_GENERIC_IMPLEMENTATION).getAll();
				updatedIds = Collections.emptySet();
			} else {
				// only inserted and updated
				workingCopy = mapGenericImplementationsByGeneric.workingCopy();
				lstLoaded = dalProvider.getEntityObjectProcessor(E.ENTITY_GENERIC_IMPLEMENTATION).getByPrimaryKeys(
						new ArrayList<>(Optional.ofNullable(notification.getInsertedAndUpdated()).orElseGet(HashSet::new)));
				updatedIds = Optional.ofNullable(notification.getUpdated()).orElseGet(HashSet::new);
			}
			Map<UID, List<EntityObjectVO<UID>>> syncCopy = Collections.synchronizedMap(workingCopy);
			lstLoaded.parallelStream()
					.collect(Collectors.groupingByConcurrent(eo -> eo.getFieldUid(E.ENTITY_GENERIC_IMPLEMENTATION.genericEntity)))
					.forEach((entityUID, lstGrouped) -> {
						List<EntityObjectVO<UID>> lstNewWorkingCopy = new ArrayList<>(syncCopy.computeIfAbsent(entityUID, k -> Collections.emptyList()));
						if (!updatedIds.isEmpty()) {
							lstNewWorkingCopy.removeIf(eo -> updatedIds.contains(eo.getPrimaryKey()));
						}
						lstNewWorkingCopy.addAll(lstGrouped);
						syncCopy.put(entityUID, Collections.unmodifiableList(lstNewWorkingCopy));
					});
			mapGenericImplementationsByGeneric.setValue(workingCopy);
		}
	}

	private void refreshGenericFieldMappings(final DalChangeNotificationForTransactionSync<UID> notification) {
		synchronized (_mutexGenerics) {
			final Map<UID, List<EntityObjectVO<UID>>> workingCopy;
			final List<EntityObjectVO<UID>> lstLoaded;
			if (notification == null || notification.hasUnidentified() || notification.hasDeletes() || notification.isRollbackOnly()) {
				workingCopy = new HashMap<>();
				lstLoaded = dalProvider.getEntityObjectProcessor(E.ENTITY_GENERIC_FIELDMAPPING).getAll();
			} else {
				// only inserted and updated
				workingCopy = mapGenericFieldMappingByImplementation.workingCopy();
				lstLoaded = dalProvider.getEntityObjectProcessor(E.ENTITY_GENERIC_FIELDMAPPING).getByPrimaryKeys(
						new ArrayList<>(Optional.ofNullable(notification.getInsertedAndUpdated()).orElseGet(HashSet::new)));
			}
			Map<UID, List<EntityObjectVO<UID>>> syncCopy = Collections.synchronizedMap(workingCopy);
			lstLoaded.parallelStream()
					.collect(Collectors.groupingByConcurrent(eo -> eo.getFieldUid(E.ENTITY_GENERIC_FIELDMAPPING.genericImplementation)))
					.forEach((genericImplUID, lstGrouped) -> {
						List<EntityObjectVO<UID>> lstUpdated = new ArrayList<>(syncCopy.computeIfAbsent(genericImplUID, k -> Collections.emptyList()));
						lstUpdated.addAll(lstGrouped);
						syncCopy.put(genericImplUID, Collections.unmodifiableList(lstUpdated));
					});
			mapGenericFieldMappingByImplementation.setValue(workingCopy);
		}
	}

	public synchronized void registerFieldMetaComplementor(
			final IFieldMetaComplementor complementor,
			final boolean sendRefreshNotification) {
		if (complementor == null) {
			throw new IllegalArgumentException();
		}
		FIELD_COMPLEMENTORS.add(complementor);
		refreshEntityFields(null, sendRefreshNotification);
	}

	private void notifyRefresh(final Collection<EntityMeta<?>> entityMetas) {
		if (TransactionSynchronizationManager.isSynchronizationActive()) {
			TransactionSynchronization ts = transactionSynchronizationsEntityMetaRefresh.get();
			if (ts == null) {
				ts = new TransactionSynchronizationAdapter() {
					@Override
					public int getOrder() {
						return -90;
					}
					@Override
					public void afterCommit() {
						observableEntityMetaRefreshAfterCommit.onNext(transactionSynchronizationsEntityMetas.get());
					}
					@Override
					public void afterCompletion(final int status) {
						transactionSynchronizationsEntityMetaRefresh.remove();
						transactionSynchronizationsEntityMetas.remove();
					}
				};
				transactionSynchronizationsEntityMetaRefresh.set(ts);
				TransactionSynchronizationManager.registerSynchronization(ts);
			}
			transactionSynchronizationsEntityMetas.set(entityMetas);
		} else {
			observableEntityMetaRefreshAfterCommit.onNext(entityMetas);
		}
	}

	public Observable<Collection<EntityMeta<?>>> observeEntityMetaRefreshsAfterCommit() {
		return observableEntityMetaRefreshAfterCommit.debounce(500, TimeUnit.MILLISECONDS);
	}

	@Override
    public Collection<EntityMeta<?>> getAllEntities() {
		return mapAllEntities.valuesUnmodifiable();
	}
	
	@Override
    public Collection<EntityMeta<?>> getAllLanguageEntities() {
		return getAllEntities().parallelStream()
				.filter(entityMeta -> NucletEntityMeta.isEntityLanguageUID(entityMeta.getUID()))
				.collect(Collectors.toList());
	}
	
	public Set<UID> getAllEntityUids() {
		return getAllEntities().parallelStream()
				.map(EntityMeta::getUID)
				.collect(Collectors.toSet());
	}

	@Override
	public boolean checkEntity(UID entityUID) {
		return mapAllEntities.containsKey(entityUID);
	}

	@SuppressWarnings("unchecked")
	@Override
    public <PK> EntityMeta<PK> getEntity(UID entityUID) {
		EntityMeta<PK> result = (EntityMeta<PK>) mapAllEntities.get(entityUID);
		if (result == null) {
			throw new CommonFatalException("Entity with UID " + entityUID + " does not exist.");
		}
		return result;
	}
	
	@Override
	public EntityMeta<?> getByTablename(final String sTableName) {
		assert sTableName != null;
		return getAllEntities().parallelStream()
				.filter(entityMeta ->
						entityMeta.getDbTable().equalsIgnoreCase(sTableName) ||
						(entityMeta.getVirtualEntity() != null && entityMeta.getVirtualEntity().equalsIgnoreCase(sTableName)))
				.findAny()
				.orElseThrow(() -> new CommonFatalException("Table for " + sTableName + " not found!"));
	}
	
	@Override
    public Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(UID entityUID) {
		final EntityMeta<?> entityMeta = mapAllEntities.get(entityUID);
		if (entityMeta == null) {
			return Collections.emptyMap();
		}
		final Collection<FieldMeta<?>> fields = entityMeta.getFields();
		return DalTransformations.transformFieldMap(fields);
	}

	public Collection<UID> getAllFieldsByEntityFilteredByGroups(UID entityUID, Collection<UID> groups, boolean bHaving, boolean bWithSystemFields) {
		final EntityMeta<?> entityMeta = mapAllEntities.get(entityUID);
		if (entityMeta == null) {
			return Collections.emptySet();
		}
		Collection<UID> ret = new HashSet<>();
		for (FieldMeta<?> fm : entityMeta.getFields()) {
			if (SF.isEOField(fm.getFieldName())) {
				if (bWithSystemFields) {
					ret.add(fm.getUID());
				}
				continue;
			}
			if (groups.contains(fm.getFieldGroup())) {
				if (bHaving) {
					ret.add(fm.getUID());
				}
			} else if (!bHaving) {
				ret.add(fm.getUID());
			}
		}
		return ret;
	}

	public Map<UID, EntityMeta<?>> getMapEntityMetaData() {
		return mapAllEntitiesUnmodifiable;
	}

	private static boolean isBlank(Object o) {
		if (o == null) return true;
		if (o instanceof String) {
			return "".equals(o);
		}
		return false;
	}

	public Map<UID, FieldMapTransport> getAllEntityFieldsByEntities(List<UID> entities) {
		// We can simply iterate most inefficiently over the single get results,
		// as these depend on caches themselves. All in all, the only thing
		// happening here is an in-memory cache transformation
	    Map<UID, FieldMapTransport> res = new HashMap<>();
	    for (UID entityUID : entities) {
	    	res.put(entityUID, new FieldMapTransport(getAllEntityFieldsByEntity(entityUID)));
	    }
	    return res;
    }

    @Override
    public boolean checkEntityField(UID fieldUID) {
		final FieldMeta<?> fieldMeta = getEntityField(fieldUID, true);
		return fieldMeta != null;
	}

	@Override
	public FieldMeta<?> getEntityField(UID fieldUID) {
		return getEntityField(fieldUID, false);
	}

	public EntityObjectVO<UID> getNuclet(UID nucletUID) {
		return dalProvider.getEntityObjectProcessor(E.NUCLET).getByPrimaryKey(nucletUID);
	}

	@Override
	public List<EntityObjectVO<UID>> getNuclets() {
		return dalProvider.getEntityObjectProcessor(E.NUCLET).getAll();
	}

	public FieldMeta<?> getEntityField(UID fieldUID, boolean checkOnly) {
		if (fieldUID == null) {
			return null;
		}
		FieldMeta<?> fieldMeta = mapAllEntityFields.get(fieldUID);
		if (fieldMeta != null) {
			return fieldMeta;
		} else {
			final Mutable<Boolean> refresh = new Mutable<>(false);
			final List<FieldMetaVO<Object>> complementedFields = FIELD_COMPLEMENTORS.stream()
					.map(complementor -> complementor.createFieldMeta(fieldUID, refresh))
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
			if (complementedFields.size() == 1) {
				final FieldMetaVO<Object> newFieldMeta = complementedFields.get(0);
				synchronized (_mutexEntities) {
					mapAllEntityFields.put(fieldUID, newFieldMeta);
					if (Boolean.TRUE.equals(refresh.getValue())) {
						DalChangeNotificationForTransactionSync<UID> notification = new DalChangeNotificationForTransactionSync<>(E.ENTITYFIELD.getUID(), true);
						notification.addInserted(fieldUID);
						refreshEntityFields(notification);
					}
				}
				return newFieldMeta;
			}
			if (complementedFields.size() > 1) {
				throw new CommonFatalException("To many complemented entity fields for " + fieldUID + ".");
			}
			if (checkOnly) {
				return null;
			}
			throw new CommonFatalException("entity field " + fieldUID + " does not exist.");
		}
	}
	
	@Override
	public LafParameterByEntityMap getAllLafParameters() {
		return lafParameterMap;
	}

	@Override
	public LafParameterMap getLafParameters(UID entityUID) {
		return getAllLafParameters().get(entityUID);
	}
		
	@Override
	public List<UID> getEntities(UID nuclet) {
		return getAllEntities().parallelStream()
				.filter(entityMeta -> Objects.equals(entityMeta.getNuclet(), nuclet))
				.map(EntityMeta::getUID)
				.collect(Collectors.toList());
	}

	@Override
	public List<EntityObjectVO<UID>> getEntityMenus() {
		return dalProvider.getEntityObjectProcessor(E.ENTITYMENU).getAll();
	}

	@Override
	public boolean isNuclosEntity(UID entityUID) {
		return E.isNuclosEntity(entityUID);
	}

	@Override
	public Collection<FieldMeta<?>> getAllReferencingFields(UID masterUID) {
		return getAllOrFirstReferencingFields(masterUID, true);
	}

	public boolean isReferenced(UID masterUID) {
		return !getAllOrFirstReferencingFields(masterUID, false).isEmpty();
	}

	private Collection<FieldMeta<?>> getAllOrFirstReferencingFields(UID masterUID, boolean all) {
		Collection<FieldMeta<?>> fields = new HashSet<FieldMeta<?>>();
		
		for (FieldMeta<?> fm : mapAllEntityFields.valuesUnmodifiable()) {
			if (masterUID.equals(fm.getForeignEntity())) {
				fields.add(fm);
				if (!all) {
					return fields;
				}
			}
		}
		return fields;
	}

	@ManagedAttribute(description="get the size (number of entities) of dataCache.getMapEntityMetaData()")
	public int getNumberOfEntitiesInCache() {
		return mapAllEntities.size();
	}

	@ManagedAttribute(description="get the size (number of fields) of dataCache.getMapFieldMetaData()")
	public int getNumberOfFieldsInCache() {
		return mapAllEntityFields.size();
	}

	@ManagedAttribute(description="get the size (number of lap parameters) of dataCache.getMapLafParameters()")
	public int getNumberOfLafParametersInCache() {
		return mapAllLafParameter.size();
	}
	
	@Override
	public String getFullQualifiedNucletName(UID nucletUID) {
		return dalProvider.getEntityObjectProcessor(E.NUCLET).getByPrimaryKey(nucletUID).getFieldValue(E.NUCLET.packagefield);
	}

	public boolean hasEntity(UID entityUID) {
		
		boolean retVal = false;
		
		try {
			getEntity(entityUID);
			retVal = true;
		} catch (Exception e) {}
		
		return retVal;
	}
	
	public static boolean isDataEntity(EntityMeta<?> eMeta, boolean includeAllSystemBOs) {
		if (eMeta == null || eMeta.isDynamic() || eMeta.isProxy() || eMeta.isGeneric() || eMeta.isChart() || eMeta.getVirtualEntity() != null) {
			return false;
		}

		if (includeAllSystemBOs) {
			return true;
		}
		
		return !E.isNuclosEntity(eMeta.getUID()) || E.GENERALSEARCHDOCUMENT.checkEntityUID(eMeta.getUID()) || E.USER.checkEntityUID(eMeta.getUID()) || E.DOCUMENTFILE.checkEntityUID(eMeta.getUID());
	}
	
	public Collection<EntityMeta<?>> getDataEntities(final Collection<String> virtualDataEntities) {
		return getAllEntities().parallelStream()
				.filter(entityMeta -> isDataEntity(entityMeta, false) || virtualDataEntities.contains(entityMeta.getVirtualEntity()))
				.collect(Collectors.toSet());
	}

	public Collection<EntityMeta<?>> getVirtualEntities() {
		return getAllEntities().parallelStream()
				.filter(entityMeta -> entityMeta.getVirtualEntity() != null)
				.collect(Collectors.toSet());
	}

	@Override
	public List<EntityObjectVO<UID>> getImplementingEntityDetails(UID genericEntityUID) {
		return CollectionUtils.emptyListIfNull(mapGenericImplementationsByGeneric.get(genericEntityUID));
	}

	@Override
	public List<EntityObjectVO<UID>> getImplementingFieldMapping(UID genericImplementationUID) {
		return CollectionUtils.emptyListIfNull(mapGenericFieldMappingByImplementation.get(genericImplementationUID));
	}

	@Override
	public Set<UID> getImplementingEntities(UID genericEntityUID) {
		final List<EntityObjectVO<UID>> allImplementations = getImplementingEntityDetails(genericEntityUID);
		final Set<UID> result = new HashSet<>();
		for (EntityObjectVO<?> eo : allImplementations) {
			result.add(eo.getFieldUid(E.ENTITY_GENERIC_IMPLEMENTATION.implementingEntity));
		}
		return result;
	}

	public final <PK> List<String> getVersionConflictMessages(String type, EntityObjectVO<PK> newVo, EntityObjectVO<PK> oldVo) {
		List<String> msgs = new ArrayList<>();
		EntityMeta<?> eMeta = getEntity(newVo.getDalEntity());
		msgs.add("version: " + newVo.getVersion() + " (this) != " + oldVo.getVersion());
		for (UID f : newVo.getFieldsWithDifferentValue(oldVo, this)) {
			msgs.add(getEntityField(f) + ": " + newVo.getFieldValue(f) + " != " + oldVo.getFieldValue(f));
		}
		Collections.sort(msgs);
		String rec = type != null? type : "record";
		msgs.add(0, "This " + rec + " " + eMeta + "/" + newVo.getPrimaryKey() + " has been");
		msgs.add(1, "updated by " + oldVo.getChangedBy() + " at " + oldVo.getChangedAt());
		if (!RigidUtils.equal(newVo.getDependents(), oldVo.getDependents())) {
			msgs.add("dependents: " + newVo.getDependents() + " != " + oldVo.getDependents());
		}
		return msgs;
	}
}
