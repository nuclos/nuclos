//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.xml.transform.Source;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.SourceResultHelper;
import org.nuclos.common.UID;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.spring.AnnotationJaxb2Marshaller;
import org.nuclos.server.dal.MetaDalUtils;
import org.nuclos.server.dal.processor.AbstractDalProcessor;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.dal.specification.IDalObservableSpecification;
import org.nuclos.server.dal.specification.IDalReadSpecification;
import org.nuclos.server.datasource.DatasourceMetaVO;
import org.nuclos.server.datasource.DatasourceMetaVO.ColumnMeta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChartMetaDataProcessor extends AbstractDalProcessor<NucletEntityMeta, UID> implements
		IDalReadSpecification<NucletEntityMeta, UID>,
		IDalObservableSpecification<NucletEntityMeta, UID> {

	private static final Logger LOG = LoggerFactory.getLogger(ChartMetaDataProcessor.class);

	private final NuclosDalProvider dalProvider;

	private final AnnotationJaxb2Marshaller jaxb2Marshaller;

	@Autowired
	public ChartMetaDataProcessor(
			final NuclosDalProvider dalProvider,
			final AnnotationJaxb2Marshaller jaxb2Marshaller) {
		super(E.CHART.getUID(), NucletEntityMeta.class, UID.class);
		this.dalProvider = dalProvider;
		this.jaxb2Marshaller = jaxb2Marshaller;
	}

	@Override
	public List<NucletEntityMeta> getAll() {
		return dalProvider.getEntityObjectProcessor(E.CHART).getAll().stream()
				.map(this::buildNucletEntityMeta)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	@Override
	public NucletEntityMeta getByPrimaryKey(final UID uid, Collection<FieldMeta<?>> fields) {
		return getByPrimaryKey(uid);
	}

	@Override
	public NucletEntityMeta getByPrimaryKey(final UID uid) {
		return buildNucletEntityMeta(dalProvider.getEntityObjectProcessor(E.CHART).getByPrimaryKey(uid));
	}

	@Override
	public List<NucletEntityMeta> getByPrimaryKeys(final List<UID> uids) {
		return dalProvider.getEntityObjectProcessor(E.CHART).getByPrimaryKeys(uids).stream()
				.map(this::buildNucletEntityMeta)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	@Override
	public List<UID> getAllIds() {
		return dalProvider.getEntityObjectProcessor(E.CHART).getAllIds();
	}

	public NucletEntityMeta buildNucletEntityMeta(EntityObjectVO<UID> eo) {
		if (eo == null) {
			return null;
		}
		final UID uid = eo.getPrimaryKey(); //tuple.get(E.CHART.getPk());
		final String name = eo.getFieldValue(E.CHART.name); //tuple.get(E.CHART.name);
		final String sMetaXML = eo.getFieldValue(E.CHART.meta); //tuple.get(E.CHART.meta);
		final Source xmlSource = SourceResultHelper.newSource(sMetaXML);
		if (xmlSource != null) {
			xmlSource.setSystemId("E.CHART.meta");
		}
		final String sQuery = eo.getFieldValue(E.CHART.query); //tuple.get(E.CHART.query);
		final UID nucletUID = eo.getFieldUid(E.CHART.nuclet); //tuple.get(E.CHART.nuclet);
		final UID detailsEntityUID = eo.getFieldUid(E.CHART.detailsEntity); //tuple.get(E.CHART.detailsEntity);
		final UID parentEntityUID = eo.getFieldUid(E.CHART.parentEntity); //tuple.get(E.CHART.parentEntity);
		final DatasourceMetaVO dsmeta;
		try {
			dsmeta = (DatasourceMetaVO) jaxb2Marshaller.unmarshal(xmlSource);
		} catch (OutOfMemoryError e) {
			LOG.error("JAXB unmarshal failed: meta= {} xml is:\n{}", sMetaXML, xmlSource, e);
			throw e;
		}
		if (dsmeta != null) {
			try {
				NucletEntityMeta emeta = new NucletEntityMeta();
				emeta.setNuclet(nucletUID);
				emeta.setPrimaryKey(dsmeta.getEntityUID());
				emeta.setUidEntity(false);
				final String entityName = name + "CRT";
				emeta.setSearchable(true);
				emeta.setEditable(false);
				emeta.setCacheable(false);
				emeta.setStateModel(false);
				emeta.setLogBookTracking(false);
				emeta.setTreeRelation(false);
				emeta.setFieldValueEntity(false);
				emeta.setDynamic(false);
				emeta.setChart(true);
				emeta.setEntityName(entityName);
				emeta.setDbTable(sQuery); //emeta.setDbTable("("+ sQuery +") as crtds"); // AS will be added by EntityMeta.getDbSelect(). -- /* Oracle don't like it " AS " + */
				emeta.setDetailEntity(detailsEntityUID);
				emeta.setParentEntity(parentEntityUID);

				Collection<FieldMeta<?>> fields = new ArrayList<>();
				if (dsmeta.getColumns() != null) {
					for (ColumnMeta colmeta : dsmeta.getColumns()) {
						NucletFieldMeta<?> fmeta = MetaDalUtils.getFieldMeta(colmeta);
						fmeta.setEntity(dsmeta.getEntityUID());
						if(DataSourceCaseSensivity.PRIMARY_KEY.equals(colmeta.getColumnName().toUpperCase())) {
							fmeta.setDataType("java.lang.Long");
						}
						if(DataSourceCaseSensivity.REF_ENTITY.equals(colmeta.getColumnName().toUpperCase())) {
							fmeta.setDbColumn(DataSourceCaseSensivity.REF_ENTITY);
							fmeta.setDataType("java.lang.Long");
							// NUCLOS-9: field must be reference field (i.e. foreign entity must be set!)
							if (parentEntityUID != null) {
								fmeta.setForeignEntity(parentEntityUID);
							} else {
								fmeta.setForeignEntity(E.GENERICOBJECT.getUID());
							}
							fmeta.setScale(255);
							fmeta.setPrecision(null);
							if (new DataSourceCaseSensivity(sQuery).isIntidGenericObjectCaseInsensitive()) {
								fmeta.setDynamic(false);
							} else {
								// this is a special column so we do not mark it as dynamic (esp. it's not case-sensitive)
								// (Note: if you need to distinguish it, use the entity's dynamic flag)
								fmeta.setDynamic(true);
							}
						}
						else {
							fmeta.setDynamic(true);
						}
						fmeta.setFallbackLabel(fmeta.getFieldName());
						fmeta.setNullable(true);
						fmeta.setSearchable(true);
						fmeta.setUnique(false);
						fmeta.setIndexed(false);
						fmeta.setLogBookTracking(false);
						fmeta.setInsertable(false);
						fmeta.setReadonly(true);

						fields.add(fmeta);
					}
				}

				emeta.setFields(fields);

				return emeta;
			} catch (Exception ex) {
				LOG.error("Unable to init dynamic entity \"{}\" uid={}: {}",
						  name, uid, ex.getMessage(), ex);
			}
		}
		return null;
	}

}
