//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.xml.transform.Source;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SourceResultHelper;
import org.nuclos.common.UID;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.spring.AnnotationJaxb2Marshaller;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.server.dal.MetaDalUtils;
import org.nuclos.server.dal.processor.AbstractDalProcessor;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.dal.specification.IDalObservableSpecification;
import org.nuclos.server.dal.specification.IDalReadSpecification;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.datasource.DatasourceMetaVO;
import org.nuclos.server.datasource.DatasourceMetaVO.ColumnMeta;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DynamicTasklistMetaDataProcessor extends AbstractDalProcessor<NucletEntityMeta, UID> implements
		IDalReadSpecification<NucletEntityMeta, UID>,
		IDalObservableSpecification<NucletEntityMeta, UID> {

	private static final Logger LOG = LoggerFactory.getLogger(DynamicTasklistMetaDataProcessor.class);

	private final NuclosDalProvider dalProvider;

	private final AnnotationJaxb2Marshaller jaxb2Marshaller;

	private final SpringDataBaseHelper dataBaseHelper;

	@Autowired
	public DynamicTasklistMetaDataProcessor(
			final NuclosDalProvider dalProvider,
			final AnnotationJaxb2Marshaller jaxb2Marshaller,
			final SpringDataBaseHelper dataBaseHelper) {
		super(E.DYNAMICTASKLIST.getUID(), NucletEntityMeta.class, UID.class);
		this.dalProvider = dalProvider;
		this.jaxb2Marshaller = jaxb2Marshaller;
		this.dataBaseHelper = dataBaseHelper;
	}

	@Override
	public List<NucletEntityMeta> getAll() {
		return dalProvider.getEntityObjectProcessor(E.DYNAMICTASKLIST).getAll().stream()
				.map(this::buildNucletEntityMeta)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	@Override
	public NucletEntityMeta getByPrimaryKey(final UID uid, Collection<FieldMeta<?>> fields) {
		return getByPrimaryKey(uid);
	}

	@Override
	public NucletEntityMeta getByPrimaryKey(final UID uid) {
		return buildNucletEntityMeta(dalProvider.getEntityObjectProcessor(E.DYNAMICTASKLIST).getByPrimaryKey(uid));
	}

	@Override
	public List<NucletEntityMeta> getByPrimaryKeys(final List<UID> uids) {
		return dalProvider.getEntityObjectProcessor(E.DYNAMICTASKLIST).getByPrimaryKeys(uids).stream()
				.map(this::buildNucletEntityMeta)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	@Override
	public List<UID> getAllIds() {
		return dalProvider.getEntityObjectProcessor(E.DYNAMICTASKLIST).getAllIds();
	}

	public NucletEntityMeta buildNucletEntityMeta(EntityObjectVO<UID> eo) {
		if (eo == null) {
			return null;
		}
		final UID uid = eo.getPrimaryKey(); //tuple.get(E.DYNAMICTASKLIST.getPk());
		final String name = eo.getFieldValue(E.DYNAMICTASKLIST.name); //tuple.get(E.DYNAMICTASKLIST.name);
		final String sMetaXML = eo.getFieldValue(E.DYNAMICTASKLIST.meta); //tuple.get(E.DYNAMICTASKLIST.meta);
		final Source xmlSource = SourceResultHelper.newSource(sMetaXML);
		if (xmlSource != null) {
			xmlSource.setSystemId("E.DYNAMICTASKLIST.meta");
		}
		final String sQuery = eo.getFieldValue(E.DYNAMICTASKLIST.query); //tuple.get(E.DYNAMICTASKLIST.query);
		final String sDatasourceRule = eo.getFieldValue(E.DYNAMICTASKLIST.datasourceRule); //tuple.get(E.DYNAMICTASKLIST.datasourceRule);
		final UID nucletUID = eo.getFieldUid(E.DYNAMICTASKLIST.nuclet); //tuple.get(E.DYNAMICTASKLIST.nuclet);
		final String comment = eo.getFieldValue(E.DYNAMICTASKLIST.description); //tuple.get(E.DYNAMICTASKLIST.description);

		final DatasourceMetaVO dsmeta;
		try {
			dsmeta = (DatasourceMetaVO) jaxb2Marshaller.unmarshal(xmlSource);
		} catch (OutOfMemoryError e) {
			LOG.error("JAXB unmarshal failed: name={} meta={} xml is:\n{}",
					  name, sMetaXML, xmlSource, e);
			throw e;
		}
		if (dsmeta != null) {
			try {
				final NucletEntityMeta emeta = new NucletEntityMeta();

				final TasklistDefinition tlDef = findTasklistDefinition(uid);
				if (tlDef == null) {
					// not in use by Tasklist. No need to add meta data...
					return null;
				}
				emeta.addProperty(EntityMeta.PROPERTY.TASKLIST_DEFINITION, tlDef);
				if (tlDef.getTaskEntityUID() != null) {
					emeta.setDetailEntity(tlDef.getTaskEntityUID());
					if (emeta.getDetailEntity() != null) {
						emeta.addProperty(EntityMeta.PROPERTY.TASKLIST_FORCE_ENTITY_JUMP, tlDef.isForceEntityJump());
					}
					transferProperties(tlDef.getTaskEntityUID(), emeta);
				}

				emeta.setNuclet(nucletUID);
				emeta.setDataSource(uid);
				emeta.setPrimaryKey(dsmeta.getEntityUID());
				emeta.setUidEntity(false);
				String entityName = name + "DTL";
				emeta.setSearchable(true);
				emeta.setEditable(false);
				emeta.setCacheable(false);
				emeta.setStateModel(false);
				emeta.setLogBookTracking(false);
				emeta.setTreeRelation(false);
				emeta.setFieldValueEntity(false);
				emeta.setDynamicTasklist(true);
				emeta.setDynamic(false);
				emeta.setChart(false);
				emeta.setEntityName(entityName);
				emeta.setDbTable(sDatasourceRule == null ? sQuery : sDatasourceRule); //emeta.setDbTable("("+ sQuery +") as dynds"); // AS will be added by EntityMeta.getDbSelect(). -- /* Oracle don't like it " AS " + */
				emeta.setProxy(sDatasourceRule != null);
				emeta.setComment(comment);

				FieldMeta<Long> pkField = null;
				FieldMeta<?> createdByField = null;
				FieldMeta<?> createdAtField = null;
				FieldMeta<?> changedByField = null;
				FieldMeta<?> changedAtField = null;
				FieldMeta<?> versionField = null;

				FieldMeta<?> titleField = null;
				FieldMeta<?> infoField = null;

				FieldMeta<?> refToEntityMetaField = null;

				Collection<FieldMeta<?>> fields = new ArrayList<>();
				if (dsmeta.getColumns() != null) {
					int iOrder = 1;
					for (ColumnMeta colmeta : dsmeta.getColumns()) {
						NucletFieldMeta<?> fmeta = MetaDalUtils.getFieldMeta(colmeta);
						fmeta.setEntity(dsmeta.getEntityUID());
						if(DataSourceCaseSensivity.PRIMARY_KEY.equals(colmeta.getColumnName().toUpperCase())) {
							fmeta.setDataType("java.lang.Long");
							pkField = (FieldMeta<Long>) fmeta;
							// do not add to fields
							continue;
						} else if (SF.CREATEDBY.getDbColumn().equalsIgnoreCase(colmeta.getColumnName())) {
							createdByField = SF.CREATEDBY.getMetaData(emeta.getUID());
							fields.add(createdByField);
							continue;
						} else if (SF.CREATEDAT.getDbColumn().equalsIgnoreCase(colmeta.getColumnName())) {
							createdAtField = SF.CREATEDAT.getMetaData(emeta.getUID());
							fields.add(createdAtField);
							continue;
						} else if (SF.CHANGEDBY.getDbColumn().equalsIgnoreCase(colmeta.getColumnName())) {
							changedByField = SF.CHANGEDBY.getMetaData(emeta.getUID());
							fields.add(changedByField);
							continue;
						} else if (SF.CHANGEDAT.getDbColumn().equalsIgnoreCase(colmeta.getColumnName())) {
							changedAtField = SF.CHANGEDAT.getMetaData(emeta.getUID());
							fields.add(changedAtField);
							continue;
						} else if (SF.VERSION.getDbColumn().equalsIgnoreCase(colmeta.getColumnName())) {
							versionField = SF.VERSION.getMetaData(emeta.getUID());
							fields.add(versionField);
							continue;
						} else if (tlDef.getDynamicTasklistEntityFieldUid() != null && tlDef.getDynamicTasklistEntityFieldUid().getString().equalsIgnoreCase(colmeta.getColumnName())) {
							refToEntityMetaField = fmeta;
							fmeta.setHidden(true);
						} else if (tlDef.getDynamicTasklistIdFieldUid() != null && tlDef.getDynamicTasklistIdFieldUid().getString().equalsIgnoreCase(colmeta.getColumnName())) {
							fmeta.setHidden(true);
						}
						else {
							fmeta.setDynamic(true);

							if (titleField == null) {
								titleField = fmeta;
							} else if (infoField == null) {
								infoField = fmeta;
							}
						}
						fmeta.setFieldName(fmeta.getFieldName());
						fmeta.setFallbackLabel(fmeta.getFieldName());
						fmeta.setNullable(true);
						fmeta.setSearchable(true);
						fmeta.setUnique(false);
						fmeta.setIndexed(false);
						fmeta.setLogBookTracking(false);
						fmeta.setInsertable(false);
						fmeta.setReadonly(true);
						fmeta.setOrder(iOrder++);

						fields.add(fmeta);
					}
				}

				if (pkField == null) {
					// New requirement is a INTID column. For backwards compatibility we allow old datasources without such a column.
					return null;
				}
				if (tlDef.getTaskEntityUID() == null && refToEntityMetaField == null) {
					// No Details? We ignore those Tasklists.
					return null;
				}

				if (refToEntityMetaField != null) {
					emeta.addProperty(EntityMeta.PROPERTY.TASKLIST_REF_TO_ENTITYMETA_FIELD, refToEntityMetaField);
				}

				emeta.setFields(fields);

				if (titleField != null) {
					emeta.setLocaleResourceIdForTreeView(titleField.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD));
				}
				if (infoField != null) {
					emeta.setLocaleResourceIdForTreeViewDescription(infoField.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD));
				}

				return emeta;
			} catch (Exception ex) {
				LOG.error("Unable to init dynamic entity \"{}\" uid={}: {}",
						  name, uid, ex.getMessage(), ex);
			}
		}
		return null;
	}

	private TasklistDefinition findTasklistDefinition(UID dynamicTasklistUID) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.TASKLIST);
		query.multiselect(t.basePk(),
				t.baseColumn(E.TASKLIST.name),
				t.baseColumn(E.TASKLIST.description),
				t.baseColumn(E.TASKLIST.labelres),
				t.baseColumn(E.TASKLIST.descriptionres),
				t.baseColumn(E.TASKLIST.menupathres),
				t.baseColumn(E.TASKLIST.datasource),
				t.baseColumn(E.TASKLIST.taskentity),
				t.baseColumn(E.TASKLIST.datasourceIdField),
				t.baseColumn(E.TASKLIST.datasourceEntityField),
				t.baseColumn(E.TASKLIST.customRuleIdField),
				t.baseColumn(E.TASKLIST.customRuleEntityField),
				t.baseColumn(E.TASKLIST.forceEntityJump)
				);
		query.where(builder.equalValue(t.baseColumn(E.TASKLIST.datasource), dynamicTasklistUID));

		final List<DbTuple> rows = dataBaseHelper.getDbAccess().executeQuery(query.distinct(true));
		if (rows.isEmpty()) {
			return null;
		}

		DbTuple row = rows.get(0);

		final TasklistDefinition tlDef = new TasklistDefinition(row.get(SF.PK_UID));
		tlDef.setName(row.get(E.TASKLIST.name));
		tlDef.setDescription(row.get(E.TASKLIST.description));
		tlDef.setLabelResourceId(row.get(E.TASKLIST.labelres));
		tlDef.setDescriptionResourceId(row.get(E.TASKLIST.descriptionres));
		tlDef.setMenupathResourceId(row.get(E.TASKLIST.menupathres));
		tlDef.setDynamicTasklistUID(row.get(E.TASKLIST.datasource));
		tlDef.setTaskEntityUID(row.get(E.TASKLIST.taskentity));
		tlDef.setDynamicTasklistIdFieldUid(UID.parseUID(row.get(E.TASKLIST.datasourceIdField)));
		tlDef.setDynamicTasklistEntityFieldUid(UID.parseUID(row.get(E.TASKLIST.datasourceEntityField)));
		tlDef.setCustomRuleIdFieldUid(UID.parseUID(row.get(E.TASKLIST.customRuleIdField)));
		tlDef.setCustomRuleEntityFieldUid(UID.parseUID(row.get(E.TASKLIST.customRuleEntityField)));
		tlDef.setForceEntityJump(Boolean.TRUE.equals(row.get(E.TASKLIST.forceEntityJump)));

		return tlDef;
	}

	private void transferProperties(UID fromEntityUID, NucletEntityMeta toEntityMeta) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.ENTITY);
		query.multiselect(  t.baseColumn(E.ENTITY.resource),
							t.baseColumn(E.ENTITY.nuclosResource));
		query.where(builder.equalValue(t.basePk(), fromEntityUID));

		final List<DbTuple> rows = dataBaseHelper.getDbAccess().executeQuery(query.distinct(true));
		if (rows.isEmpty()) {
			return;
		}
		final DbTuple row = rows.get(0);
		toEntityMeta.setResource((UID) row.get(0));
		toEntityMeta.setNuclosResource((String) row.get(1));
	}
	
}
