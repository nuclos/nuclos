package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.NucletEntityContext;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.server.dal.processor.AbstractProcessorFactory;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IDbValueConverter;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclos.JdbcEntityContextProcessor;
import org.springframework.stereotype.Component;

@Component
public class EntityContextProcessor extends AbstractJdbcDalProcessor<NucletEntityContext, UID>
	implements JdbcEntityContextProcessor {

	private static final Class<? extends IDalVO<UID>> type = NucletEntityContext.class;
	private static final IColumnToVOMapping<UID, UID> uidColumn = AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, E.ENTITYCONTEXT.getUID());

	private static List<IColumnToVOMapping<?, UID>> createAllColumns() {
		final List<IColumnToVOMapping<?, UID>> allColumns = new ArrayList<>();

		allColumns.add(uidColumn);
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.ENTITYCONTEXT.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.ENTITYCONTEXT.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.ENTITYCONTEXT.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.ENTITYCONTEXT.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.ENTITYCONTEXT.getUID()));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYCONTEXT.mainEntity));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYCONTEXT.dependentEntity));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYCONTEXT.dependentEntityField));

		return allColumns;
	}

	public EntityContextProcessor() {
		super(E.ENTITYCONTEXT.getUID(), type, UID.class, createAllColumns());
	}

	@Override
	protected EntityMeta<UID> getMetaData() {
		return E.ENTITYCONTEXT;
	}

	@Override
	protected IRigidMetaProvider getMetaProvider() {
		return SYSTEM_META_PROVIDER;
	}

	@Override
	protected IDbValueConverter getDbValueConverter() {
		return null;
	}

	@Override
	protected IColumnToVOMapping<UID, UID> getPrimaryKeyColumn() {
		return uidColumn;
	}

	@Override
	public List<NucletEntityContext> getAll() {
		return super.getAll();
	}

	@Override
	public List<NucletEntityContext> getByPrimaryKeys(final List<UID> ids) {
		return super.getByPrimaryKeys(allColumns, ids);
	}

}
