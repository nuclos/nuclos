//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.server.dal.processor.AbstractProcessorFactory;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IDbValueConverter;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclos.JdbcEntityFieldMetaDataProcessor;
import org.nuclos.server.dblayer.DbException;
import org.springframework.stereotype.Component;

@Component
public class EntityFieldMetaProcessor extends AbstractJdbcDalProcessor<NucletFieldMeta<?>, UID>
	implements JdbcEntityFieldMetaDataProcessor {

	private static final Class<IDalVO<UID>> type = RigidUtils.getGenericClass(NucletFieldMeta.class);
	private static final IColumnToVOMapping<UID, UID> uidColumn = AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, E.ENTITYFIELD.getUID());
	private static final IColumnToVOMapping<UID, UID> entityUidColumn = AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.entity);

	private static List<IColumnToVOMapping<?, UID>> createAllColumns() {
		final List<IColumnToVOMapping<?, UID>> allColumns = new ArrayList<>();

		allColumns.add(uidColumn);
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.ENTITYFIELD.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.ENTITYFIELD.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.ENTITYFIELD.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.ENTITYFIELD.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.ENTITYFIELD.getUID()));

		final IColumnToVOMapping<UID, UID> entityIdColumn = AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.entity);
		allColumns.add(entityIdColumn);
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.entityfieldgroup, "fieldGroup"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.field, "fieldName"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.dbfield, "dbColumn"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.foreignIntegrationPoint));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.foreignentity, "foreignEntity"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.foreignentityfield, "foreignEntityField"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.lookupentity, "lookupEntity"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.lookupentityfield, "lookupEntityField"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.searchfield, "searchField"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.datatype, "dataType"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.defaultcomponenttype, "defaultComponentType"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.datascale, "scale"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.dataprecision, "precision"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.formatinput, "formatInput"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.formatoutput, "formatOutput"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.foreigndefault, "defaultForeignId"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.valuedefault, "defaultValue"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.readonly));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.unique));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.nullable));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.indexed));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.searchable));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.modifiable));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.insertable));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.hidden));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.logbooktracking, "logBookTracking"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.showmnemonic, "showMnemonic"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.logbookAnonymizePeriod, "logbookAnonymizePeriod"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calcfunction, "calcFunction"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calcAttributeDS));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calcAttributeParamValues));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calcAttributeAllowCustomization));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calcOndemand));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.colorDatasource, "colorDatasource"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.sortationasc, "sortorderASC"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.sortationdesc, "sortorderDESC"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.localeresourcel, "localeResourceIdForLabel"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.localeresourced, "localeResourceIdForDescription"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.defaultmandatory, "defaultMandatory"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.ondeletecascade, "onDeleteCascade"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.order));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calculationscript, "calculationScript"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.backgroundcolorscript, "backgroundColorScript"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.autonumberentity, "autonumberEntity"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.isLocalized, "localized"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.comment, "comment"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.valuelistProvider));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.valuelistProviderConfig));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.clientRules));

		return allColumns;
	}
	
	public EntityFieldMetaProcessor() {
		super(E.ENTITYFIELD.getUID(), type,	UID.class, createAllColumns());
	}
	
	@Override
	public EntityMeta<UID> getMetaData() {
		return E.ENTITYFIELD;
	}

	@Override
	protected IRigidMetaProvider getMetaProvider() {
		return SYSTEM_META_PROVIDER;
	}

	@Override
	protected IDbValueConverter getDbValueConverter() {
		return null;
	}

	@Override
	protected IColumnToVOMapping<UID, UID> getPrimaryKeyColumn() {
		return uidColumn;
	}

	@Override
	public List<NucletFieldMeta<?>> getAll() {
		return super.getAll();
	}

	@Override
	public List<NucletFieldMeta<?>> getByParent(UID entity) {
		return super.getByColumn(allColumns, entityUidColumn, entity);
	}

	@Override
	public void delete(Delete<UID> id) throws DbException {
		super.delete(id);
	}

	@Override
	public NucletFieldMeta<?> getByPrimaryKey(UID uid) {
		return super.getByPrimaryKey(uid);
	}
	
	@Override
	public List<NucletFieldMeta<?>> getByPrimaryKeys(List<UID> uids) {
		return super.getByPrimaryKeys(allColumns, uids);
	}

	@Override
	public Object insertOrUpdate(NucletFieldMeta<?> dalVO) {
		return super.insertOrUpdate(dalVO);
	}

	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<UID> dalVO) throws DbException {
		throw new NotImplementedException();
	}
	
	@Override
	protected void setDebugInfo(NucletFieldMeta<?> dalVO) {
		if (dalVO.getPrimaryKey() != null) {
			dalVO.getPrimaryKey().setDebugInfo(dalVO.getFieldName());
		}
   	}
	
}
