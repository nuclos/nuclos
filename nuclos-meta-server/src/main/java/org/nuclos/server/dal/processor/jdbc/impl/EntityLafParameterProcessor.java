//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.EntityLafParameterVO;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.server.dal.processor.AbstractProcessorFactory;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IDbValueConverter;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclos.JdbcEntityLafParameterProcessor;
import org.springframework.stereotype.Component;

@Component
public class EntityLafParameterProcessor extends AbstractJdbcDalProcessor<EntityLafParameterVO, UID>
	implements JdbcEntityLafParameterProcessor {

	private static final Class<IDalVO<UID>> type = RigidUtils.getGenericClass(EntityLafParameterVO.class);
	private static final IColumnToVOMapping<UID, UID> uidColumn = AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, E.ENTITYLAFPARAMETER.getUID());

	private static List<IColumnToVOMapping<?, UID>> createAllColumns() {
		final List<IColumnToVOMapping<?, UID>> allColumns = new ArrayList<>();
		
		allColumns.add(uidColumn);
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.ENTITYLAFPARAMETER.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.ENTITYLAFPARAMETER.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.ENTITYLAFPARAMETER.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.ENTITYLAFPARAMETER.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.ENTITYLAFPARAMETER.getUID()));

//		allColumns.add(createBeanRefMapping("INTID_T_MD_ENTITY", "T_MD_ENTITY", "ENTITY", JoinType.LEFT, type, "STRENTITY", "STRVALUE_T_MD_ENTITY", "entity", DT_STRING));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYLAFPARAMETER.entity));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYLAFPARAMETER.parameter));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYLAFPARAMETER.value));
		return allColumns;
	}
	
	public EntityLafParameterProcessor() {
		super(E.ENTITYLAFPARAMETER.getUID(), type, UID.class, createAllColumns());
	}
	
	@Override
	public EntityMeta<UID> getMetaData() {
		return E.ENTITYLAFPARAMETER;
	}

	@Override
	protected IRigidMetaProvider getMetaProvider() {
		return SYSTEM_META_PROVIDER;
	}

	@Override
	protected IDbValueConverter getDbValueConverter() {
		return null;
	}

	@Override
	protected IColumnToVOMapping<UID, UID> getPrimaryKeyColumn() {
		return uidColumn;
	}

	@Override
	public List<EntityLafParameterVO> getAll() {
		return super.getAll();
	}

	@Override
	public EntityLafParameterVO getByPrimaryKey(UID id) {
		return super.getByPrimaryKey(id);
	}
	
	@Override
	public List<EntityLafParameterVO> getByPrimaryKeys(List<UID> ids) {
		return super.getByPrimaryKeys(allColumns, ids);
	}

}
