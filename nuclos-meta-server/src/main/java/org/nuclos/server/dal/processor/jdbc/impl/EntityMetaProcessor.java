//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.server.dal.processor.AbstractProcessorFactory;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IDbValueConverter;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclos.JdbcEntityMetaDataProcessor;
import org.nuclos.server.dblayer.DbException;
import org.springframework.stereotype.Component;

@Component
public class EntityMetaProcessor extends AbstractJdbcDalProcessor<NucletEntityMeta, UID>
	implements JdbcEntityMetaDataProcessor {

	private static final Class<? extends IDalVO<UID>> type = NucletEntityMeta.class;
	private static final IColumnToVOMapping<UID, UID> uidColumn = AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, E.ENTITY.getUID());
	
	private static List<IColumnToVOMapping<?, UID>> createAllColumns() {
		final List<IColumnToVOMapping<?, UID>> allColumns = new ArrayList<>();

		/*
		 * Attention: The sequence of initialization is important.
		 * <p>
		 * As a general rule the last 3 items to set are:
		 * <ol>
		 *   <li>virtualEntity</li>
		 *   <li>entity</li>
		 *   <li>dbEntity</li>
		 * </ol>
		 * </p>
		 */
		allColumns.add(uidColumn);
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.ENTITY.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.ENTITY.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.ENTITY.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.ENTITY.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.ENTITY.getUID()));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.nuclet));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.systemidprefix, "systemIdPrefix"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.systemidTransactional, "systemIdTransactional"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.menushortcut, "menuShortcut"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.editable));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.collectiveEditEnabled));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.resultdetailssplitview));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.usesstatemodel, "stateModel"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.logbooktracking, "logBookTracking"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.cacheable));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.searchable));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.showSearch));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.thin));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.treerelation, "treeRelation"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.treeactive, "treeActive"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.expandtreeonopen, "expandTreeOnOpen"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.fieldvalueentity, "fieldValueEntity"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.accelerator));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.acceleratormodifier, "acceleratorModifier"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.fieldsforequality, "fieldsForEquality"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.resource));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.nuclosResource));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourcel, "localeResourceIdForLabel"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourcem, "localeResourceIdForMenuPath"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourced, "localeResourceIdForDescription"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourcetw, "localeResourceIdForTreeView"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourcett, "localeResourceIdForTreeViewDescription"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourcepl, "localeResourceIdForProcess"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourcepd, "localeResourceIdForProcessDescription"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourceobjgen, "localeResourceIdForObjgenListings"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.processgroup, "processGroup"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.idFactory));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.readDelegate));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.rowcolorscript, "rowColorScript"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.clonegenerator, "cloneGenerator"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.virtualentity, "virtualEntity"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.entity, "entityName"));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.dbtable, "dbTable"));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.proxy));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.writeproxy));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.generic));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.mandatorLevel));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.mandatorUnique));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.dataLangRefPath));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.dataLanguageDBTable));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.comment));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.lockMode));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.ownerForeignEntityField));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.unlockMode));

		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.chunksize));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.jumplimit));

		return allColumns;
	}

	public EntityMetaProcessor() {
		super(E.ENTITY.getUID(), type, UID.class, createAllColumns());
	}
	
	@Override
	public EntityMeta<UID> getMetaData() {
		return E.ENTITY;
	}

	@Override
	protected IRigidMetaProvider getMetaProvider() {
		return SYSTEM_META_PROVIDER;
	}

	@Override
	protected IDbValueConverter getDbValueConverter() {
		return null;
	}

	@Override
	protected IColumnToVOMapping<UID, UID> getPrimaryKeyColumn() {
		return uidColumn;
	}

	@Override
	public void delete(Delete<UID> id) throws DbException {
		super.delete(id);
	}

	@Override
	public List<NucletEntityMeta> getAll() {
		return super.getAll();
	}

	@Override
	public NucletEntityMeta getByPrimaryKey(UID uid) {
		return super.getByPrimaryKey(uid);
	}
	
	@Override
	public List<NucletEntityMeta> getByPrimaryKeys(List<UID> ids) {
		return super.getByPrimaryKeys(allColumns, ids);
	}

	@Override
	public Object insertOrUpdate(NucletEntityMeta dalVO) {
		return super.insertOrUpdate(dalVO);
	}

	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<UID> dalVO) throws DbException {
		throw new NotImplementedException();
	}
	
	@Override
	protected void setDebugInfo(NucletEntityMeta dalVO) {
		if (dalVO.getUID() != null) {
			dalVO.getUID().setDebugInfo(dalVO.getEntityName());
		}
   	}
	
}
