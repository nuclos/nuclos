//Copyright (C) 2018  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.datasource;

import javax.xml.transform.Source;

import org.nuclos.common.RigidUtils;
import org.nuclos.common.SourceResultHelper;
import org.nuclos.common.spring.AnnotationJaxb2Marshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DatasourceMetaParser {

	private static final Logger LOG = LoggerFactory.getLogger(DatasourceMetaParser.class);

	private final AnnotationJaxb2Marshaller jaxb2Marshaller;

	@Autowired
	public DatasourceMetaParser(
			final AnnotationJaxb2Marshaller jaxb2Marshaller) {
		this.jaxb2Marshaller = jaxb2Marshaller;
	}

	public DatasourceMetaVO parseDynamictasklist(final String sMetaXML) {
		return parse(sMetaXML, "E.DYNAMICTASKLIST.meta");
	}

	public DatasourceMetaVO parseChart(final String sMetaXML) {
		return parse(sMetaXML, "E.CHART.meta");
	}

	public DatasourceMetaVO parseDynamicEntity(final String sMetaXML) {
		return parse(sMetaXML, "E.DYNAMICENTITY.meta");
	}

	public DatasourceMetaVO parseRecordGrant(final String sMetaXML) {
		return parse(sMetaXML, "E.RECORDGRANT.meta");
	}

	public DatasourceMetaVO parse(final String sMetaXML, final String sSystemId) {
		final Source xmlSource = SourceResultHelper.newSource(RigidUtils.defaultIfNull(sMetaXML, ""));
		if (xmlSource != null && sSystemId != null) {
			xmlSource.setSystemId(sSystemId);
		}
		DatasourceMetaVO dsmeta = null;
		if (xmlSource != null) {
			try {
				dsmeta = (DatasourceMetaVO) jaxb2Marshaller.unmarshal(xmlSource);
			} catch (OutOfMemoryError e) {
				LOG.error("JAXB unmarshal failed: meta={} xml is:\n{}", sMetaXML, xmlSource, e);
				throw e;
			}
		}
		return dsmeta;
	}
}
