//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

public enum ByteUnit {
	BYTE(0, 0L),
	KILOBYTE(1, 1024L),
	MEGABYTE(2, 1024L * 1024L),
	GIGABYTE(3, 1024L * 1024L * 1024L),
	TERABYTE(4, 1024L * 1024L * 1024L * 1024L);

	private final int order;
	private final long start;

	ByteUnit(final int order, final long start) {
		this.order = order;
		this.start = start;
	}

	public int getOrder() {
		return order;
	}

	public long getStart() {
		return start;
	}
}
