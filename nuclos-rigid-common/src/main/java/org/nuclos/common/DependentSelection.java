package org.nuclos.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DependentSelection implements Serializable {

	private static final long serialVersionUID = 1L;

	public ArrayList<String> selectedIds = new ArrayList<>();

	/**
	 * Or parent fqn and foreign key fqn
	 * Key = Dependant FQN + ForeignKey Name
	 * Value = DependentSelection
	 */
	public HashMap<String, DependentSelection> subDependentSelection = new HashMap<>();

	public DependentSelection() {

	}

	@Override
	public String toString() {
		String content = "{selectedIds: " + selectedIds + " subDependentSelections: {";
		for (Map.Entry<String, DependentSelection> entry : subDependentSelection.entrySet()) {
			content += entry.getKey()  + " -> " + entry.getValue().toString();
		}
		content += "}}";
		return content;
	}

}
