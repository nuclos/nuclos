package org.nuclos.common;

public class EntityContextVO extends EntityContext {

	private static final long serialVersionUID = 2977122000431480471L;

	private UID mainEntity;
	private UID dependentEntity;
	private UID dependentEntityField;

	@Override
	public UID getMainEntity() {
		return mainEntity;
	}

	public void setMainEntity(final UID mainEntity) {
		this.mainEntity = mainEntity;
	}

	@Override
	public UID getDependentEntity() {
		return dependentEntity;
	}

	public void setDependentEntity(final UID dependentEntity) {
		this.dependentEntity = dependentEntity;
	}

	@Override
	public UID getDependentEntityField() {
		return dependentEntityField;
	}

	public void setDependentEntityField(final UID dependentEntityField) {
		this.dependentEntityField = dependentEntityField;
	}

}
