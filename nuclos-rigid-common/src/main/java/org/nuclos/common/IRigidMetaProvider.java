package org.nuclos.common;

import java.util.Collection;
import java.util.Map;

public interface IRigidMetaProvider {

	Collection<EntityMeta<?>> getAllEntities();
	
	<PK> EntityMeta<PK> getEntity(UID entityUID);

	Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(UID entityUID);

	FieldMeta<?> getEntityField(UID fieldUID);
	
	EntityMeta<?> getByTablename(String sTableName);
	
	boolean isNuclosEntity(UID entityUID);

	boolean checkEntity(UID entityUID);

	boolean checkEntityField(UID entityFieldUID);

}
