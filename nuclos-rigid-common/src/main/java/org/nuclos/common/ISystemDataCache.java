package org.nuclos.common;

import java.util.Set;

/**
 * Created by Sebastian Debring on 11/12/2020.
 */
public interface ISystemDataCache {
	Set<UID> getAllIds();
}
