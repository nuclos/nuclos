package org.nuclos.common;

/**
 * Created by Sebastian Debring on 11/12/2020.
 */
public interface IXMLEntities {
	ISystemDataCache getData(EntityMeta<?> entity);

	ISystemDataCache getData(UID entityUID);
}
