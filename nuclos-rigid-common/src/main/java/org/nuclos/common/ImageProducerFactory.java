package org.nuclos.common;

import org.nuclos.common2.KMPMatch;

public final class ImageProducerFactory {
	private ImageProducerFactory() {
	}

	public static INuclosImageProducer createImageProducer(byte[] b) {
		if (RigidUtils.testByteArrayForXML(b)) {
			
			if (KMPMatch.indexOf(b, "<svg") > -1) {
				return new SVGImageProducer();				
			}
			
		}
		
		return new DefaultNuclosImageProducer();
	}

}
