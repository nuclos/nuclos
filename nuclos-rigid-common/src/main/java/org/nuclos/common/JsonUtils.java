//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils {

	/**
	 * 
	 * @param jsonObject
	 * @return stringified json object
	 */
	public static String objectToString(JsonObject jsonObject) {
		JsonWriter jsonWriter = null;
		try {
			StringWriter writer = new StringWriter();
			jsonWriter = Json.createWriter(writer);
			jsonWriter.writeObject(jsonObject);
			return writer.toString();
		} finally {
			if (jsonWriter != null) {
				jsonWriter.close();
			}
		}
	}
	
	/**
	 * 
	 * @param jsonObjectString (stringified json object)
	 * @return a JsonObject
	 */
	public static JsonObject stringToObject(String jsonObjectString) {
		JsonReader jsonReader = null;
		try {
			jsonReader = Json.createReader(new StringReader(jsonObjectString));
			return jsonReader.readObject();
		} finally {
			if (jsonReader != null) {
				jsonReader.close();
			}
		}
	}
	
	/**
	 * 
	 * @param jsonObject
	 * @return pretty printed jsonObject
	 */
	public static String prettyPrint(JsonObject jsonObject) {
		String jsonObjectString = objectToString(jsonObject);
		return prettyPrint(jsonObjectString);
	}

	/**
	 *
	 * @param value serializable object
	 * @return pretty printed jsonObject
	 */
	public static String prettyPrint(Object value) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writer(new MyPrettyPrinter()).writeValueAsString(value);
	}

	/**
	 * 
	 * @param jsonString
	 * @return pretty printed jsonString
	 */
	public static String prettyPrint(String jsonString) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			Object value = objectMapper.readValue(jsonString, Object.class);
			return prettyPrint(value);
		} catch (Exception e) {
			throw new NuclosFatalException(e.getMessage(), e);
		}
	}

	static class MyPrettyPrinter extends DefaultPrettyPrinter {

		public MyPrettyPrinter() {
			DefaultPrettyPrinter.Indenter indenter = new DefaultIndenter("  ", System.lineSeparator());
			indentObjectsWith(indenter);
			indentArraysWith(indenter);
			_objectFieldValueSeparatorWithSpaces = ":";
		}

		private MyPrettyPrinter(MyPrettyPrinter pp) {
			super(pp);
		}

		@Override
		public void writeEndArray(JsonGenerator g, int nrOfValues) throws IOException {
			if (!_arrayIndenter.isInline()) {
				--_nesting;
			}
			if (nrOfValues > 0) {
				_arrayIndenter.writeIndentation(g, _nesting);
			}
			g.writeRaw(']');
		}



		@Override
		public DefaultPrettyPrinter createInstance() {
			return new MyPrettyPrinter(this);
		}
	}
}
