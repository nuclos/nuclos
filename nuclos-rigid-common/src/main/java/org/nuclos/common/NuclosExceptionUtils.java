package org.nuclos.common;

import java.util.Arrays;

public class NuclosExceptionUtils {

	public static <T extends Throwable> T reduceStackTraceElements(T ex, int iElementCount) {
		StackTraceElement[] reducedElements = new StackTraceElement[iElementCount];
		System.arraycopy(ex.getStackTrace(), 0, reducedElements, 0, iElementCount);
		ex.setStackTrace(reducedElements);
		return ex;
	}

	public static <T extends Throwable> T reduceStackTraceElementsNuclosOny(T ex) {
		ex.setStackTrace(Arrays.stream(ex.getStackTrace())
			.filter(s -> s.getClassName().startsWith("org.nuclos")).toArray(StackTraceElement[]::new));
		return ex;
	}

}
