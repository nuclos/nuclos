package org.nuclos.common;

//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
public class PostConstructManager {

	private static final ThreadLocal<Boolean> POST_CONSTRUCT_DISABLED = new ThreadLocal<Boolean>() {
		@Override
		protected Boolean initialValue() {
			return Boolean.FALSE;
		}
	};

	public static void setPostConstructDisabled() {
		POST_CONSTRUCT_DISABLED.set(Boolean.TRUE);
	}

	public static void setPostConstructEnabled() {
		POST_CONSTRUCT_DISABLED.set(Boolean.FALSE);
	}

	public static boolean isDisabled() {
		return Boolean.TRUE.equals(POST_CONSTRUCT_DISABLED.get());
	}

}
