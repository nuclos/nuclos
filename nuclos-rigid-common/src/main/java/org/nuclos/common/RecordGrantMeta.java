package org.nuclos.common;

public class RecordGrantMeta<PK> extends EntityMeta<PK> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 811680037879951884L;
	private final String sql;
	private final String alias;
	private final boolean bHasCanSomething;
	private final Class<PK> clz;

	/**
	 * for deserialization only
	 */
	protected RecordGrantMeta() {
		this.sql = null;
		this.alias = null;
		this.bHasCanSomething = false;
		this.clz = null;
	}
	
	public RecordGrantMeta(String sql, String alias, boolean bHasCanSomething, final Class<PK> clz) {
		this.sql = sql;
		this.alias = alias;
		this.bHasCanSomething = bHasCanSomething;
		this.clz = clz;
	}

	@Override 
	public UID getUID() {
		return null;
	}
	
	@Override 
	public Class<PK> getPkClass() {
		return clz;
	}
	
	
	@Override public String getDbTable() {
		return "(" + RigidUtils.cleanSql(sql) + ")";
	}
	
	@Override 
	public String getEntityName() {
		return alias;
	}
	
	public boolean hasCanSomething() {
		return bHasCanSomething;
	}
}
