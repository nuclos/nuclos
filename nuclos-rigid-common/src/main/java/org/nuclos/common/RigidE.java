//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.awt.event.KeyEvent;

import org.nuclos.common2.InternalTimestamp;

public class RigidE {
	
	public static abstract class _Entity extends EntityMeta<UID> {
		private static final long serialVersionUID = 4300782568574927266L;
		public static UID UID = new UID("5E8q");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_MD_ENTITY";}
		@Override public String getEntityName() {return "nuclos_entity";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isCacheable() {return true;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.entity.label";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.main-blue.businessobject.png";}
		
		/**
		 * Field:    <b>entity</b><br>
		 * DbColumn: <b>STRENTITY</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>5E8qa</b>
		 */
		public final Entity entity = new Entity();
		public static class Entity extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("5E8qa");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRENTITY";}
			@Override public String getFieldName() {return "entity";}
			@Override public UID getEntity() {return _Entity.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
		}
		/**
		 * Field:    <b>dbtable</b><br>
		 * DbColumn: <b>STRDBENTITY</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>5E8qb</b>
		 */
		public final Dbtable dbtable = new Dbtable();
		public static class Dbtable extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("5E8qb");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRDBENTITY";}
			@Override public String getFieldName() {return "dbtable";}
			@Override public UID getEntity() {return _Entity.UID;}
			@Override public Integer getScale() {return 30;}
			@Override public boolean isNullable() {return false;}
		}
		/**
		 * Field:    <b>virtualentity</b><br>
		 * DbColumn: <b>STRVIRTUALENTITY</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>5E8qA</b>
		 */
		public final Virtualentity virtualentity = new Virtualentity();
		public static class Virtualentity extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("5E8qA");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRVIRTUALENTITY";}
			@Override public String getFieldName() {return "virtualentity";}
			@Override public UID getEntity() {return _Entity.UID;}
			@Override public Integer getScale() {return 255;}
		}
		/**
		 * Field:    <b>proxy</b><br>
		 * DbColumn: <b>BLNPROXY</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>5E8qF</b>
		 */
		public final Proxy proxy = new Proxy();
		public static class Proxy extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("5E8qF");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNPROXY";}
			@Override public String getFieldName() {return "proxy";}
			@Override public UID getEntity() {return _Entity.UID;}
		}
		/**
		 * Field:    <b>mandatorLevel</b><br>
		 * DbColumn: <b>STRUID_T_MD_MANDATOR_LEVEL</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>5E8qG</b>
		 */
		public final MandatorLevel mandatorLevel = new MandatorLevel();
		public static class MandatorLevel extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("5E8qG");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_T_MD_MANDATOR_LEVEL";}
			@Override public String getFieldName() {return "mandatorLevel";}
			@Override public UID getEntity() {return _Entity.UID;}
			@Override public UID getForeignEntity() {return _MandatorLevel.UID;}
			@Override public String getForeignEntityField() {return stringify(_MandatorLevel.Name.UID);}
			@Override public Integer getScale() {return 255;}
		}
		/**
		 * Since:	 <b>v4.14</b>
		 * Field:    <b>generic</b><br>
		 * DbColumn: <b>BLNGENERIC</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>5E8qae</b>
		 */
		public final Generic generic = new Generic();
		public static class Generic extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("5E8qae");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNGENERIC";}
			@Override public String getFieldName() {return "generic";}
			@Override public UID getEntity() {return _Entity.UID;}
		}
		/**
		 * Field:    <b>writeproxy</b><br>
		 * DbColumn: <b>BLNWRITEPROXY</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>5E8qaf</b>
		 */
		public final WriteProxy writeproxy = new WriteProxy();
		public static class WriteProxy extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("5E8qaf");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNWRITEPROXY";}
			@Override public String getFieldName() {return "writeProxy";}
			@Override public UID getEntity() {return _Entity.UID;}
		}
		/**
		 * Since:	 <b>v4.25</b>
		 * Field:    <b>mandatorUnique</b><br>
		 * DbColumn: <b>BLNMANDATORUNIQUE</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>5E8qag</b>
		 */
		public final MandatorUnique mandatorUnique = new MandatorUnique();
		public static class MandatorUnique extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("5E8qag");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNMANDATORUNIQUE";}
			@Override public String getFieldName() {return "mandatorUnique";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getDefaultMandatory() {return Boolean.FALSE.toString();}
		}
	}
	
	public static abstract class _Entityfield extends EntityMeta<UID> {
		private static final long serialVersionUID = 4082517926090353954L;
		public static UID UID = new UID("Khi5");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_MD_ENTITY_FIELD";}
		@Override public String getEntityName() {return "nuclos_entityfield";}
		@Override public String getBusinessObjectClassName() {return "EntityField";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isCacheable() {return true;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.entity.label";}
		
		/**
		 * Field:    <b>field</b><br>
		 * DbColumn: <b>STRFIELD</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>Khi5c</b>
		 */
		public final Field field = new Field();
		public static class Field extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5c");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRFIELD";}
			@Override public String getFieldName() {return "field";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.field";}
		}
		/**
		 * Field:    <b>dbfield</b><br>
		 * DbColumn: <b>STRDBFIELD</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>Khi5d</b>
		 */
		public final Dbfield dbfield = new Dbfield();
		public static class Dbfield extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5d");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRDBFIELD";}
			@Override public String getFieldName() {return "dbfield";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 30;}
			@Override public boolean isNullable() {return false;}
		}
		/**
		 * Field:    <b>entity</b><br>
		 * DbColumn: <b>STRUID_T_MD_ENTITY</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>Khi5a</b>
		 */
		public final Entity entity = new Entity();
		public static class Entity extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5a");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_T_MD_ENTITY";}
			@Override public String getFieldName() {return "entity";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public UID getForeignEntity() {return _Entity.UID;}
			@Override public String getForeignEntityField() {return stringify(_Entity.Entity.UID);}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.entity";}
		}
		/**
		 * Field:    <b>foreignentity</b><br>
		 * DbColumn: <b>STRFOREIGNENTITY</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>Khi5g</b>
		 */
		public final Foreignentity foreignentity = new Foreignentity();
		public static class Foreignentity extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5g");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_FOREIGNENTITY";}
			@Override public String getFieldName() {return "foreignentity";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public UID getUnreferencedForeignEntity() {return _Entity.UID;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.foreignentity";}
		}
		/**
		 * Field:    <b>foreignentityfield</b><br>
		 * DbColumn: <b>STRFOREIGNENTITYFIELD</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>Khi5h</b>
		 */
		public final Foreignentityfield foreignentityfield = new Foreignentityfield();
		public static class Foreignentityfield extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5h");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRFOREIGNENTITYFIELD";}
			@Override public String getFieldName() {return "foreignentityfield";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 1028;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.foreignentityfield";}
		}
		/**
		 * Field:    <b>lookupentityfield</b><br>
		 * DbColumn: <b>STRLOOKUPENTITYFIELD</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>Khi5j</b>
		 */
		public final Lookupentityfield lookupentityfield = new Lookupentityfield();
		public static class Lookupentityfield extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5j");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRLOOKUPENTITYFIELD";}
			@Override public String getFieldName() {return "lookupentityfield";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 1028;}
		}
		/**
		 * Field:    <b>lookupentity</b><br>
		 * DbColumn: <b>STRLOOKUPENTITY</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>Khi5i</b>
		 */
		public final Lookupentity lookupentity = new Lookupentity();
		public static class Lookupentity extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5i");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_LOOKUPENTITY";}
			@Override public String getFieldName() {return "lookupentity";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public UID getUnreferencedForeignEntity() {return _Entity.UID;}
		}
		/**
		 * Field:    <b>datatype</b><br>
		 * DbColumn: <b>STRDATATYPE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>Khi5e</b>
		 */
		public final Datatype datatype = new Datatype();
		public static class Datatype extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5e");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRDATATYPE";}
			@Override public String getFieldName() {return "datatype";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.datatype";}
		}
		/**
		 * Field:    <b>datascale</b><br>
		 * DbColumn: <b>INTDATASCALE</b><p>
		 * Type:     <b>java.lang.Integer.class</b><br>
		 * UID:      <b>Khi5k</b>
		 */
		public final Datascale datascale = new Datascale();
		public static class Datascale extends FieldMeta.Valueable<java.lang.Integer> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5k");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Integer> getJavaClass() {return java.lang.Integer.class;}
			@Override public String getDbColumn() {return "INTDATASCALE";}
			@Override public String getFieldName() {return "datascale";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 9;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.datascale";}
		}
		/**
		 * Field:    <b>dataprecision</b><br>
		 * DbColumn: <b>INTDATAPRECISION</b><p>
		 * Type:     <b>java.lang.Integer.class</b><br>
		 * UID:      <b>Khi5l</b>
		 */
		public final Dataprecision dataprecision = new Dataprecision();
		public static class Dataprecision extends FieldMeta.Valueable<java.lang.Integer> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5l");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Integer> getJavaClass() {return java.lang.Integer.class;}
			@Override public String getDbColumn() {return "INTDATAPRECISION";}
			@Override public String getFieldName() {return "dataprecision";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 9;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dataprecision";}
		}
		/**
		 * Field:    <b>nullable</b><br>
		 * DbColumn: <b>BLNNULLABLE</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>Khi5r</b>
		 */
		public final Nullable nullable = new Nullable();
		public static class Nullable extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5r");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNNULLABLE";}
			@Override public String getFieldName() {return "nullable";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public boolean isNullable() {return false;}
		}
		/**
		 * Field:    <b>readonly</b><br>
		 * DbColumn: <b>BLNREADONLY</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>Khi5x</b>
		 */
		public final Readonly readonly = new Readonly();
		public static class Readonly extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5x");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNREADONLY";}
			@Override public String getFieldName() {return "readonly";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public boolean isNullable() {return false;}
		}
		/**
		 * Field:    <b>unique</b><br>
		 * DbColumn: <b>BLNUNIQUE</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>Khi5q</b>
		 */
		public final Unique unique = new Unique();
		public static class Unique extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5q");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNUNIQUE";}
			@Override public String getFieldName() {return "unique";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public boolean isNullable() {return false;}
		}
		/**
		 * Field:    <b>indexed</b><br>
		 * DbColumn: <b>BLNINDEXED</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>Khi5E</b>
		 */
		public final Indexed indexed = new Indexed();
		public static class Indexed extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5E");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNINDEXED";}
			@Override public String getFieldName() {return "indexed";}
			@Override public UID getEntity() {return _Entityfield.UID;}
		}
		/**
		 * Field:    <b>ondeletecascade</b><br>
		 * DbColumn: <b>BLNONDELETECASCADE</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>Khi5G</b>
		 */
		public final Ondeletecascade ondeletecascade = new Ondeletecascade();
		public static class Ondeletecascade extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5G");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNONDELETECASCADE";}
			@Override public String getFieldName() {return "ondeletecascade";}
			@Override public UID getEntity() {return _Entityfield.UID;}
		}
		/**
		 * Field:    <b>calcfunction</b><br>
		 * DbColumn: <b>STRCALCFUNCTION</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>Khi5y</b>
		 */
		public final Calcfunction calcfunction = new Calcfunction();
		public static class Calcfunction extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5y");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRCALCFUNCTION";}
			@Override public String getFieldName() {return "calcfunction";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 255;}
		}
		/**
		 * Field:    <b>defaultmandatory</b><br>
		 * DbColumn: <b>STR_DEFAULT_MANDATORY</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>Khi5F</b>
		 */
		public final Defaultmandatory defaultmandatory = new Defaultmandatory();
		public static class Defaultmandatory extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5F");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STR_DEFAULT_MANDATORY";}
			@Override public String getFieldName() {return "defaultmandatory";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 4000;}
		}
		/**
		 * Field:    <b>order</b><br>
		 * DbColumn: <b>INTORDER</b><p>
		 * Type:     <b>java.lang.Integer.class</b><br>
		 * UID:      <b>Khi5H</b>
		 */
		public final Order order = new Order();
		public static class Order extends FieldMeta.Valueable<java.lang.Integer> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5H");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Integer> getJavaClass() {return java.lang.Integer.class;}
			@Override public String getDbColumn() {return "INTORDER";}
			@Override public String getFieldName() {return "order";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 9;}
		}
		/**
		 * Field:    <b>calcAttributeDS</b><br>
		 * DbColumn: <b>STRUID_T_MD_CALCATTRIBUTE</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>Khi5P</b>
		 */
		public final CalcAttributeDS calcAttributeDS = new CalcAttributeDS();
		public static class CalcAttributeDS extends FieldMeta.Valueable<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Khi5P");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_T_MD_CALCATTRIBUTE";}
			@Override public String getFieldName() {return "calcAttributeDS";}
			@Override public UID getEntity() {return _Entityfield.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public UID getForeignEntity() {return _CalcAttribute.UID;}
		}
	}
	
	/**
	 * Entity:         <b>DYNAMICENTITY</b><br>
	 * DbTable:        <b>T_MD_DYNAMICENTITY</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>YxQw</b>
	 */
	public static final _DynamicEntity DYNAMICENTITY = new _DynamicEntity();
	public static class _DynamicEntity extends EntityMeta<UID> {
		private static final long serialVersionUID = -8616075979520146459L;
		public static UID UID = new UID("YxQw");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_MD_DYNAMICENTITY";}
		@Override public String getEntityName() {return "nuclos_dynamicEntity";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isCacheable() {return true;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.dynamicentity.label";}
		@Override public String getLocaleResourceIdForMenuPath() {return "nuclos.entity.dynamicentity.menupath";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.glyphish-blue.12-eye.png";}
		/**
		 * Field:    <b>name</b><br>
		 * DbColumn: <b>STRNAME</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>YxQwa</b>
		 */
		public final Name name = new Name();
		public static class Name extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("YxQwa");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRNAME";}
			@Override public String getFieldName() {return "name";}
			@Override public UID getEntity() {return _DynamicEntity.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dynamicEntity.name.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dynamicEntity.name.description";}
		}
		/**
		 * Field:    <b>description</b><br>
		 * DbColumn: <b>STRDESCRIPTION</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>YxQwb</b>
		 */
		public final Description description = new Description();
		public static class Description extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("YxQwb");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRDESCRIPTION";}
			@Override public String getFieldName() {return "description";}
			@Override public UID getEntity() {return _DynamicEntity.UID;}
			@Override public Integer getScale() {return 4000;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dynamicEntity.description.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dynamicEntity.description.description";}
		}
		/**
		 * Field:    <b>entity</b><br>
		 * DbColumn: <b>STRUID_ENTITY</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>YxQwc</b>
		 */
		public final Entity entity = new Entity();
		public static class Entity extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("YxQwc");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_ENTITY";}
			@Override public String getFieldName() {return "entity";}
			@Override public UID getEntity() {return _DynamicEntity.UID;}
			@Override public UID getUnreferencedForeignEntity() {return _Entity.UID;}
			@Override public String getUnreferencedForeignEntityField() {return stringify(_Entity.Entity.UID);}
			@Override public Integer getScale() {return 255;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dynamicEntity.entity.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dynamicEntity.entity.description";}
		}
		/**
		 * Field:    <b>valid</b><br>
		 * DbColumn: <b>BLNVALID</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>YxQwd</b>
		 */
		public final Valid valid = new Valid();
		public static class Valid extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("YxQwd");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNVALID";}
			@Override public String getFieldName() {return "valid";}
			@Override public UID getEntity() {return _DynamicEntity.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dynamicEntity.valid.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dynamicEntity.valid.description";}
		}
		/**
		 * Field:    <b>nuclet</b><br>
		 * DbColumn: <b>STRUID_T_MD_NUCLET</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>YxQwf</b>
		 */
		public final Nuclet nuclet = new Nuclet();
		public static class Nuclet extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("YxQwf");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_T_MD_NUCLET";}
			@Override public String getFieldName() {return "nuclet";}
			@Override public UID getEntity() {return _DynamicEntity.UID;}
			@Override public UID getForeignEntity() {return _Nuclet.UID;}
			@Override public String getForeignEntityField() {return stringify(_Nuclet.Name.UID);}
			@Override public Integer getScale() {return 255;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclet.foreign.label";}
		}
		/**
		 * Field:    <b>source</b><br>
		 * DbColumn: <b>CLBDATASOURCEXML</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>YxQwe</b>
		 */
		public final Source source = new Source();
		public static class Source extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("YxQwe");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "CLBDATASOURCEXML";}
			@Override public String getFieldName() {return "source";}
			@Override public UID getEntity() {return _DynamicEntity.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dynamicEntity.source.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dynamicEntity.source.description";}
		}
		/**
		 * Field:    <b>meta</b><br>
		 * DbColumn: <b>CLBMETAXML</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>YxQwg</b>
		 */
		public final Meta meta = new Meta();
		public static class Meta extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("YxQwg");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "CLBMETAXML";}
			@Override public String getFieldName() {return "meta";}
			@Override public UID getEntity() {return _DynamicEntity.UID;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dynamicEntity.meta.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dynamicEntity.meta.description";}
		}
		/**
		 * Field:    <b>query</b><br>
		 * DbColumn: <b>CLBQUERY</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>YxQwh</b>
		 */
		public final Query query = new Query();
		public static class Query extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("YxQwh");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "CLBQUERY";}
			@Override public String getFieldName() {return "query";}
			@Override public UID getEntity() {return _DynamicEntity.UID;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dynamicEntity.query.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dynamicEntity.query.description";}
		}
		private UID[][] uniqueFieldCombinations = new UID[][]{ new UID[]{ _DynamicEntity.Name.UID } };
		@Override public UID[][] getUniqueFieldCombinations() {return uniqueFieldCombinations;}

	}

	/**
	 * Entity:         <b>DYNAMICTASKLIST</b><br>
	 * DbTable:        <b>T_MD_DYNAMICTASKLIST</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>fVCk</b>
	 */
	public static final _Dynamictasklist DYNAMICTASKLIST = new _Dynamictasklist();
	public static class _Dynamictasklist extends EntityMeta<UID> {
		private static final long serialVersionUID = 1455295012262216354L;
		public static UID UID = new UID("fVCk");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_MD_DYNAMICTASKLIST";}
		@Override public String getEntityName() {return "nuclos_dynamictasklist";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isCacheable() {return true;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.dynamictasklist.label";}
		@Override public String getLocaleResourceIdForMenuPath() {return "nuclos.entity.dynamictasklist.menupath";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.glyphish-blue.12-eye.png";}

		/**
		 * Field:    <b>meta</b><br>
		 * DbColumn: <b>CLBMETAXML</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>fVCkf</b>
		 */
		public final Meta meta = new Meta();
		public static class Meta extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("fVCkf");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "CLBMETAXML";}
			@Override public String getFieldName() {return "meta";}
			@Override public UID getEntity() {return _Dynamictasklist.UID;}
			@Override public boolean isHidden() {return true;}
		}
	}
	
	/**
	 * Entity:         <b>CHART</b><br>
	 * DbTable:        <b>T_UD_CHART</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>mF5v</b>
	 */
	public static final _Chart CHART = new _Chart();
	public static class _Chart extends EntityMeta<UID> {
		private static final long serialVersionUID = -6871427997125455881L;
		public static UID UID = new UID("mF5v");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_UD_CHART";}
		@Override public String getEntityName() {return "nuclos_chart";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isCacheable() {return true;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.chart.label";}
		@Override public String getLocaleResourceIdForMenuPath() {return "nuclos.entity.chart.menupath";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.glyphish-blue.12-eye.png";}
		/**
		 * Field:    <b>valid</b><br>
		 * DbColumn: <b>BLNVALID</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>mF5va</b>
		 */
		public final Valid valid = new Valid();
		public static class Valid extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("mF5va");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNVALID";}
			@Override public String getFieldName() {return "valid";}
			@Override public UID getEntity() {return _Chart.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.chart.valid.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.chart.valid.description";}
		}
		/**
		 * Field:    <b>description</b><br>
		 * DbColumn: <b>STRDESCRIPTION</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>mF5vc</b>
		 */
		public final Description description = new Description();
		public static class Description extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("mF5vc");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRDESCRIPTION";}
			@Override public String getFieldName() {return "description";}
			@Override public UID getEntity() {return _Chart.UID;}
			@Override public Integer getScale() {return 4000;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.chart.description.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.chart.description.description";}
		}
		/**
		 * Field:    <b>name</b><br>
		 * DbColumn: <b>STRDATASOURCE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>mF5vb</b>
		 */
		public final Name name = new Name();
		public static class Name extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("mF5vb");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRDATASOURCE";}
			@Override public String getFieldName() {return "name";}
			@Override public UID getEntity() {return _Chart.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.chart.name.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.chart.name.description";}
		}
		/**
		 * Field:    <b>nuclet</b><br>
		 * DbColumn: <b>STRUID_T_MD_NUCLET</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>mF5ve</b>
		 */
		public final Nuclet nuclet = new Nuclet();
		public static class Nuclet extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("mF5ve");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_T_MD_NUCLET";}
			@Override public String getFieldName() {return "nuclet";}
			@Override public UID getEntity() {return _Chart.UID;}
			@Override public UID getForeignEntity() {return _Nuclet.UID;}
			@Override public String getForeignEntityField() {return stringify(_Nuclet.Name.UID);}
			@Override public boolean isUnique() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclet.foreign.label";}
		}
		/**
		 * Field:    <b>source</b><br>
		 * DbColumn: <b>CLBDATASOURCEXML</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>mF5vd</b>
		 */
		public final Source source = new Source();
		public static class Source extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("mF5vd");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "CLBDATASOURCEXML";}
			@Override public String getFieldName() {return "source";}
			@Override public UID getEntity() {return _Chart.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public boolean isSearchable() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.chart.source.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.chart.source.description";}
		}
		/**
		 * Field:    <b>meta</b><br>
		 * DbColumn: <b>CLBMETAXML</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>mF5vf</b>
		 */
		public final Meta meta = new Meta();
		public static class Meta extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("mF5vf");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "CLBMETAXML";}
			@Override public String getFieldName() {return "meta";}
			@Override public UID getEntity() {return _Chart.UID;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.chart.meta.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.chart.meta.description";}
		}
		/**
		 * Field:    <b>query</b><br>
		 * DbColumn: <b>CLBQUERY</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>mF5vg</b>
		 */
		public final Query query = new Query();
		public static class Query extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("mF5vg");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "CLBQUERY";}
			@Override public String getFieldName() {return "query";}
			@Override public UID getEntity() {return _Chart.UID;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.chart.query.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.chart.query.description";}
		}
		/**
		 * Field:    <b>detailsEntity</b><br>
		 * DbColumn: <b>STRUID_DETAILS_ENTITY</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>mF5vh</b>
		 */
		public final DetailsEntity detailsEntity = new DetailsEntity();
		public static class DetailsEntity extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("mF5vh");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_DETAILS_ENTITY";}
			@Override public String getFieldName() {return "detailsEntity";}
			@Override public UID getEntity() {return _Chart.UID;}
			@Override public UID getUnreferencedForeignEntity() {return _Entity.UID;}
			@Override public String getUnreferencedForeignEntityField() {return stringify(_Entity.Entity.UID);}
			@Override public Integer getScale() {return 255;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.chart.detailsEntity.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.chart.detailsEntity.description";}
		}
		/**
		 * Field:    <b>parentEntity</b><br>
		 * DbColumn: <b>STRUID_PARENT_ENTITY</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>mF5vi</b>
		 */
		public final ParentEntity parentEntity = new ParentEntity();
		public static class ParentEntity extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("mF5vi");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_PARENT_ENTITY";}
			@Override public String getFieldName() {return "parentEntity";}
			@Override public UID getEntity() {return _Chart.UID;}
			@Override public UID getUnreferencedForeignEntity() {return _Entity.UID;}
			@Override public String getUnreferencedForeignEntityField() {return stringify(_Entity.Entity.UID);}
			@Override public Integer getScale() {return 255;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.chart.parentEntity.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.chart.parentEntity.description";}
		}
		private UID[][] uniqueFieldCombinations = new UID[][]{ new UID[]{ _Chart.Name.UID } };
		@Override public UID[][] getUniqueFieldCombinations() {return uniqueFieldCombinations;}

	}

	/**
	 * Entity:         <b>RELEASEHISTORY</b><br>
	 * DbTable:        <b>T_AD_RELEASE</b><p>
	 * PrimaryKey:     <b>Long.class</b><br>
	 * UID:            <b>ivNv</b>
	 */
	public static final _Releasehistory RELEASEHISTORY = new _Releasehistory();
	public static class _Releasehistory extends EntityMeta<Long> {
		private static final long serialVersionUID = -5882496118700807284L;
		public static UID UID = new UID("ivNv");
		@Override public UID getUID() {return UID;}
		@Override public Class<Long> getPkClass() {return Long.class;}
		@Override public String getDbTable() {return "T_AD_RELEASE";}
		@Override public String getEntityName() {return "nuclos_releasehistory";}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isEditable() {return false;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.releasehistory.label";}
		/**
		 * Field:    <b>release</b><br>
		 * DbColumn: <b>STRRELEASE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>ivNva</b>
		 */
		public final Release release = new Release();
		public static class Release extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("ivNva");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRRELEASE";}
			@Override public String getFieldName() {return "release";}
			@Override public UID getEntity() {return _Releasehistory.UID;}
			@Override public Integer getScale() {return 32;}
			@Override public boolean isNullable() {return false;}
			@Override public boolean isInvariant() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.releasehistory.release.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.releasehistory.release.description";}
		}
		/**
		 * Field:    <b>description</b><br>
		 * DbColumn: <b>STRDESCRIPTION</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>ivNvb</b>
		 */
		public final Description description = new Description();
		public static class Description extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("ivNvb");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRDESCRIPTION";}
			@Override public String getFieldName() {return "description";}
			@Override public UID getEntity() {return _Releasehistory.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isNullable() {return false;}
			@Override public boolean isInvariant() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.releasehistory.description.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.releasehistory.description.description";}
		}
		/**
		 * Field:    <b>delivered</b><br>
		 * DbColumn: <b>DATDELIVERED</b><p>
		 * Type:     <b>java.util.Date.class</b><br>
		 * UID:      <b>ivNvc</b>
		 */
		public final Delivered delivered = new Delivered();
		public static class Delivered extends FieldMeta.Valueable<java.util.Date> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("ivNvc");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.util.Date> getJavaClass() {return java.util.Date.class;}
			@Override public String getDbColumn() {return "DATDELIVERED";}
			@Override public String getFieldName() {return "delivered";}
			@Override public UID getEntity() {return _Releasehistory.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public boolean isInvariant() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.releasehistory.delivered.label";}
		}
		/**
		 * Field:    <b>installed</b><br>
		 * DbColumn: <b>DATINSTALLED</b><p>
		 * Type:     <b>java.util.Date.class</b><br>
		 * UID:      <b>ivNvd</b>
		 */
		public final Installed installed = new Installed();
		public static class Installed extends FieldMeta.Valueable<java.util.Date> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("ivNvd");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.util.Date> getJavaClass() {return java.util.Date.class;}
			@Override public String getDbColumn() {return "DATINSTALLED";}
			@Override public String getFieldName() {return "installed";}
			@Override public UID getEntity() {return _Releasehistory.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public boolean isInvariant() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.releasehistory.installed.label";}
		}
		/**
		 * Field:    <b>data</b><br>
		 * DbColumn: <b>BLBDATA</b><p>
		 * Type:     <b>byte[].class</b><br>
		 * UID:      <b>ivNvf</b>
		 */
		public final Data data = new Data();
		public static class Data extends FieldMeta.Valueable<byte[]> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("ivNvf");
			@Override public UID getUID() {return UID;}
			@Override public Class<byte[]> getJavaClass() {return byte[].class;}
			@Override public String getDbColumn() {return "BLBDATA";}
			@Override public String getFieldName() {return "data";}
			@Override public UID getEntity() {return _Releasehistory.UID;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.releasehistory.data.label";}
		}
		/**
		 * Field:    <b>application</b><br>
		 * DbColumn: <b>STRAPPLICATION</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>ivNve</b>
		 */
		public final Application application = new Application();
		public static class Application extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("ivNve");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRAPPLICATION";}
			@Override public String getFieldName() {return "application";}
			@Override public UID getEntity() {return _Releasehistory.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isNullable() {return false;}
			@Override public boolean isInvariant() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.releasehistory.application.label";}
		}
	}
	
	/**
	 * Entity:         <b>DBOBJECT</b><br>
	 * DbTable:        <b>T_MD_DBOBJECT</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>iJu9</b>
	 */
	public static final _Dbobject DBOBJECT = new _Dbobject();
	public static class _Dbobject extends EntityMeta<UID> {
		private static final long serialVersionUID = -8285236854379097207L;
		public static UID UID = new UID("iJu9");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_MD_DBOBJECT";}
		@Override public String getEntityName() {return "nuclos_dbobject";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isCacheable() {return true;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.dbobject.label";}
		@Override public String getLocaleResourceIdForMenuPath() {return "nuclos.entity.dbobject.menupath";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.glyphish-blue.159-voicemail.png";}
		/**
		 * Field:    <b>nuclet</b><br>
		 * DbColumn: <b>STRUID_T_MD_NUCLET</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>iJu9d</b>
		 */
		public final Nuclet nuclet = new Nuclet();
		public static class Nuclet extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("iJu9d");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_T_MD_NUCLET";}
			@Override public String getFieldName() {return "nuclet";}
			@Override public UID getEntity() {return _Dbobject.UID;}
			@Override public UID getForeignEntity() {return _Nuclet.UID;}
			@Override public String getForeignEntityField() {return SysEntities.stringify(_Nuclet.Name.UID);}
			@Override public boolean isUnique() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclet.foreign.label";}
		}
		/**
		 * Field:    <b>order</b><br>
		 * DbColumn: <b>INTORDER</b><p>
		 * Type:     <b>java.lang.Integer.class</b><br>
		 * UID:      <b>iJu9c</b>
		 */
		public final Order order = new Order();
		public static class Order extends FieldMeta.Valueable<java.lang.Integer> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("iJu9c");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Integer> getJavaClass() {return java.lang.Integer.class;}
			@Override public String getDbColumn() {return "INTORDER";}
			@Override public String getFieldName() {return "order";}
			@Override public UID getEntity() {return _Dbobject.UID;}
			@Override public Integer getScale() {return 9;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dbobject.order.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dbobject.order.description";}
		}
		/**
		 * Field:    <b>dbobjecttype</b><br>
		 * DbColumn: <b>STRDBOBJECTTYPE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>iJu9b</b>
		 */
		public final Dbobjecttype dbobjecttype = new Dbobjecttype();
		public static class Dbobjecttype extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("iJu9b");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRDBOBJECTTYPE";}
			@Override public String getFieldName() {return "dbobjecttype";}
			@Override public UID getEntity() {return _Dbobject.UID;}
			@Override public Integer getScale() {return 30;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dbobject.dbobjecttype.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dbobject.dbobjecttype.description";}
		}
		/**
		 * Field:    <b>name</b><br>
		 * DbColumn: <b>STRDBOBJECT</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>iJu9a</b>
		 */
		public final Name name = new Name();
		public static class Name extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("iJu9a");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRDBOBJECT";}
			@Override public String getFieldName() {return "name";}
			@Override public UID getEntity() {return _Dbobject.UID;}
			@Override public Integer getScale() {return 30;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public String getFormatInput() {return "[A-Z0-9_]*";}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dbobject.dbobject.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dbobject.dbobject.description";}
		}
		
	}
	
	/**
	 * Entity:         <b>DBSOURCE</b><br>
	 * DbTable:        <b>T_MD_DBSOURCE</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>NHOF</b>
	 */
	public static final _Dbsource DBSOURCE = new _Dbsource();
	public static class _Dbsource extends EntityMeta<UID> {
		private static final long serialVersionUID = -2814556379608305114L;
		public static UID UID = new UID("NHOF");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_MD_DBSOURCE";}
		@Override public String getEntityName() {return "nuclos_dbsource";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isCacheable() {return true;}
		@Override public boolean isShowSearch() {return false;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.dbsource.label";}
		@Override public String getLocaleResourceIdForMenuPath() {return "nuclos.entity.dbsource.menupath";}
		@Override public String getLocaleResourceIdForTreeView() {return "nuclos.entity.dbsource.treeview";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.glyphish-blue.159-voicemail.png";}
		/**
		 * Field:    <b>active</b><br>
		 * DbColumn: <b>BLNACTIVE</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>NHOFe</b>
		 */
		public final Active active = new Active();
		public static class Active extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("NHOFe");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNACTIVE";}
			@Override public String getFieldName() {return "active";}
			@Override public UID getEntity() {return _Dbsource.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dbsource.active.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dbsource.active.description";}
			@Override public String getDefaultValue() { return Boolean.TRUE.toString(); }
		}
		/**
		 * Field:    <b>source</b><br>
		 * DbColumn: <b>CLBSOURCE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>NHOFc</b>
		 */
		public final Source source = new Source();
		public static class Source extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("NHOFc");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "CLBSOURCE";}
			@Override public String getFieldName() {return "source";}
			@Override public UID getEntity() {return _Dbsource.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dbsource.source.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dbsource.source.description";}
		}
		/**
		 * Field:    <b>dropstatement</b><br>
		 * DbColumn: <b>CLBDROPSTATEMENT</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>NHOFd</b>
		 */
		public final Dropstatement dropstatement = new Dropstatement();
		public static class Dropstatement extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("NHOFd");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "CLBDROPSTATEMENT";}
			@Override public String getFieldName() {return "dropstatement";}
			@Override public UID getEntity() {return _Dbsource.UID;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dbsource.dropstatement.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dbsource.dropstatement.description";}
		}
		/**
		 * Field:    <b>dbtype</b><br>
		 * DbColumn: <b>STRDBTYPE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>NHOFa</b>
		 */
		public final Dbtype dbtype = new Dbtype();
		public static class Dbtype extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("NHOFa");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRDBTYPE";}
			@Override public String getFieldName() {return "dbtype";}
			@Override public UID getEntity() {return _Dbsource.UID;}
			@Override public Integer getScale() {return 30;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dbsource.dbtype.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dbsource.dbtype.description";}
		}
		/**
		 * Field:    <b>dbobject</b><br>
		 * DbColumn: <b>STRDBOBJECT</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>NHOFb</b>
		 */
		public final Dbobject dbobject = new Dbobject();
		public static class Dbobject extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("NHOFb");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_DBOBJECT";}
			@Override public String getFieldName() {return "dbobject";}
			@Override public UID getEntity() {return _Dbsource.UID;}
			@Override public UID getForeignEntity() {return _Dbobject.UID;}
			@Override public String getForeignEntityField() {return stringify(_Dbobject.Name.UID);}
			@Override public UID getUnreferencedForeignEntity() {return _Dbobject.UID;}
			@Override public String getUnreferencedForeignEntityField() {return stringify(_Dbobject.Name.UID);}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isOnDeleteCascade() {return true;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.dbsource.dbobject.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.dbsource.dbobject.description";}
		}

	}

	/**
	 * Entity:         <b>LAYOUT</b><br>
	 * DbTable:        <b>T_MD_LAYOUT</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>5KNd</b>
	 */
	public static class _Layout extends EntityMeta<UID> {
		private static final long serialVersionUID = -5545884612434268625L;
		public static UID UID = new UID("5KNd");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_MD_LAYOUT";}
		@Override public String getEntityName() {return "nuclos_layout";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.layout.label";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.glyphish-blue.98-palette.png";}
		@Override public String getLocaleResourceIdForMenuPath() {return "nuclos.entity.layout.menupath";}
		@Override public Integer getMenuPathOrder() {return 1;}
		@Override public Integer getAcceleratorModifier() {return KeyEvent.CTRL_DOWN_MASK | KeyEvent.SHIFT_DOWN_MASK;}
		@Override public String getAccelerator() {return "l";}
	}

	/**
	 * Entity:         <b>LOCALERESOURCE</b><br>
	 * DbTable:        <b>T_MD_LOCALERESOURCE</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>x6jK</b>
	 */
	public static final _Localeresource LOCALERESOURCE = new _Localeresource();
	public static class _Localeresource extends EntityMeta<UID> {
		private static final long serialVersionUID = 3929665805634063942L;
		public static UID UID = new UID("x6jK");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_MD_LOCALERESOURCE";}
		@Override public String getEntityName() {return "nuclos_localeresource";}
		@Override public boolean isUidEntity() {return true;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.localeresource.label";}
		/**
		 * Field:    <b>text</b><br>
		 * DbColumn: <b>STRTEXT</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>x6jKc</b>
		 */
		public final Text text = new Text();
		public static class Text extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("x6jKc");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRTEXT";}
			@Override public String getFieldName() {return "text";}
			@Override public UID getEntity() {return _Localeresource.UID;}
			@Override public Integer getScale() {return 4000;}
			@Override public boolean isNullable() {return false;}
			@Override public boolean isSearchable() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.localeresource.text.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.localeresource.text.description";}
		}
		/**
		 * Field:    <b>locale</b><br>
		 * DbColumn: <b>STRLOCALE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>x6jKb</b>
		 */
		public final Locale locale = new Locale();
		public static class Locale extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("x6jKb");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRLOCALE";}
			@Override public String getFieldName() {return "locale";}
			@Override public UID getEntity() {return _Localeresource.UID;}
			@Override public Integer getScale() {return 40;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public boolean isInvariant() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.localeresource.locale.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.localeresource.locale.description";}
		}
		/**
		 * Field:    <b>resourceID</b><br>
		 * DbColumn: <b>STRRESOURCEID</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>x6jKa</b>
		 */
		public final ResourceID resourceID = new ResourceID();
		public static class ResourceID extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("x6jKa");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRRESOURCEID";}
			@Override public String getFieldName() {return "resourceID";}
			@Override public UID getEntity() {return _Localeresource.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isUnique() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public boolean isSearchable() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.localeresource.resourceID.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.localeresource.resourceID.description";}
		}

	}
	
	/**
	 * Entity:         <b>NUCLET</b><br>
	 * DbTable:        <b>T_AD_APPLICATION</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>xojr</b>
	 */
	public static final _Nuclet NUCLET = new _Nuclet();
	public static class _Nuclet extends EntityMeta<UID> {
		private static final long serialVersionUID = 7444855016090909433L;
		public static UID UID = new UID("xojr");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_AD_APPLICATION";}
		@Override public String getEntityName() {return "nuclos_nuclet";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isCacheable() {return true;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.Nuclet.label";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.main-blue.nuclet.png";}
		@Override public String getLocaleResourceIdForMenuPath() {return "nuclos.entity.Nuclet.menupath";}
		@Override public Integer getMenuPathOrder() {return 8;}
		@Override public Integer getAcceleratorModifier() {return KeyEvent.CTRL_DOWN_MASK | KeyEvent.SHIFT_DOWN_MASK;}
		@Override public String getAccelerator() {return "n";}

		/**
		 * Field:    <b>name</b><br>
		 * DbColumn: <b>NAME</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>xojra</b>
		 */
		public final Name name = new Name();
		public static class Name extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("xojra");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "NAME";}
			@Override public String getFieldName() {return "name";}
			@Override public UID getEntity() {return _Nuclet.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.Nuclet.name.label";}
		}
		/**
		 * Field:    <b>description</b><br>
		 * DbColumn: <b>DESCRIPTION</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>xojrb</b>
		 */
		public final Description description = new Description();
		public static class Description extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("xojrb");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "DESCRIPTION";}
			@Override public String getFieldName() {return "description";}
			@Override public UID getEntity() {return _Nuclet.UID;}
			@Override public Integer getScale() {return 4000;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.Nuclet.description.label";}
		}
		/**
		 * Field:    <b>nucletVersion</b><br>
		 * DbColumn: <b>INTNUCLETVERSION</b><p>
		 * Type:     <b>java.lang.Integer.class</b><br>
		 * UID:      <b>xojre</b>
		 */
		public final NucletVersion nucletVersion = new NucletVersion();
		public static class NucletVersion extends FieldMeta.Valueable<java.lang.Integer> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("xojre");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Integer> getJavaClass() {return java.lang.Integer.class;}
			@Override public String getDbColumn() {return "INTNUCLETVERSION";}
			@Override public String getFieldName() {return "nucletVersion";}
			@Override public UID getEntity() {return _Nuclet.UID;}
			@Override public Integer getScale() {return 9;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.Nuclet.version.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.Nuclet.version.description";}
		}
		/**
		 * Field:    <b>package</b><br>
		 * DbColumn: <b>STRPACKAGE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>xojrd</b>
		 */
		public final Package packagefield = new Package();
		public static class Package extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("xojrd");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRPACKAGE";}
			@Override public String getFieldName() {return "package";}
			@Override public UID getEntity() {return _Nuclet.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isNullable() {return false;}
			@Override public String getFormatInput() {return "^([a-z_]{1}[a-z0-9_]*(\\.[a-z_]{1}[a-z0-9_]*)*)$";}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.Nuclet.package.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.Nuclet.package.description";}
		}
		/**
		 * Field:    <b>localidentifier</b><br>
		 * DbColumn: <b>STRLOCALIDENTIFIER</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>xojrf</b>
		 */
		public final Localidentifier localidentifier = new Localidentifier();
		public static class Localidentifier extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("xojrf");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRLOCALIDENTIFIER";}
			@Override public String getFieldName() {return "localidentifier";}
			@Override public UID getEntity() {return _Nuclet.UID;}
			@Override public Integer getScale() {return 4;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.Nuclet.localidentifier.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.Nuclet.localidentifier.description";}
		}
		/**
		 * Field:    <b>nuclon</b><br>
		 * DbColumn: <b>BLNNUCLON</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>xojrg</b>
		 */
		public final Nuclon nuclon = new Nuclon();
		public static class Nuclon extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("xojrg");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNNUCLON";}
			@Override public String getFieldName() {return "nuclon";}
			@Override public UID getEntity() {return _Nuclet.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.Nuclet.nuclon.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.Nuclet.nuclon.description";}
		}
		/**
		 * Field:    <b>source</b><br>
		 * DbColumn: <b>BLNSOURCE</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>xojrh</b>
		 */
		public final Source source = new Source();
		public static class Source extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("xojrh");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNSOURCE";}
			@Override public String getFieldName() {return "source";}
			@Override public UID getEntity() {return _Nuclet.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getDefaultValue() {return Boolean.TRUE.toString();}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.Nuclet.source.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.Nuclet.source.description";}
		}

		/**
		 * Field:    <b>nucletVersionString</b><br>
		 * DbColumn: <b>STRNUCLETVERSION</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>xojri</b>
		 */
		public final NucletVersionString nucletVersionString = new NucletVersionString();
		/**
		 * Field:    <b>masterNuclet</b><br>
		 * DbColumn: <b>BLNMASTERNUCLET</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>xojrj</b>
		 */
		public final MasterNuclet masterNuclet = new MasterNuclet();
		private UID[][] uniqueFieldCombinations = new UID[][]{
				new UID[]{ _Nuclet.Name.UID },
				new UID[]{ _Nuclet.Localidentifier.UID},
				new UID[]{_Nuclet.Package.UID},
		};

		public static class NucletVersionString extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("xojri");

			@Override
			public UID getUID() {
				return UID;
			}

			@Override
			public Class<java.lang.String> getJavaClass() {
				return java.lang.String.class;
			}

			@Override
			public String getDbColumn() {
				return "STRNUCLETVERSION";
			}

			@Override
			public String getFieldName() {
				return "nucletVersionString";
			}

			@Override
			public UID getEntity() {
				return _Nuclet.UID;
			}

			@Override
			public Integer getScale() {
				return 255;
			}

			@Override
			public boolean isNullable() {
				return true;
			}

			@Override
			public String getLocaleResourceIdForLabel() {
				return "nuclos.entityfield.Nuclet.versionstring.label";
			}

			@Override
			public String getLocaleResourceIdForDescription() {
				return "nuclos.entityfield.Nuclet.versionstring.description";
			}
		}

		public static class MasterNuclet extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("xojrj");

			@Override
			public UID getUID() {
				return UID;
			}

			@Override
			public Class<java.lang.Boolean> getJavaClass() {
				return java.lang.Boolean.class;
			}

			@Override
			public String getDbColumn() {
				return "BLNMASTERNUCLET";
			}

			@Override
			public String getFieldName() {
				return "masterNuclet";
			}

			@Override
			public UID getEntity() {
				return _Nuclet.UID;
			}

			@Override
			public boolean isNullable() {
				return false;
			}

			@Override
			public String getDefaultMandatory() {
				return Boolean.FALSE.toString();
			}

			@Override
			public String getDefaultValue() {
				return "nein";
			}

			@Override
			public String getLocaleResourceIdForLabel() {
				return "nuclos.entityfield.Nuclet.masternuclet.label";
			}

			@Override
			public String getLocaleResourceIdForDescription() {
				return "nuclos.entityfield.Nuclet.masternuclet.description";}
		}
		@Override public UID[][] getUniqueFieldCombinations() {return uniqueFieldCombinations;}

	}
	
	/**
	 * Entity:         <b>MANDATOR_LEVEL</b><br>
	 * DbTable:        <b>T_MD_MANDATOR_LEVEL</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>Mw4i</b>
	 */
	public static final _MandatorLevel MANDATOR_LEVEL = new _MandatorLevel();
	public static class _MandatorLevel extends EntityMeta<UID> {
		private static final long serialVersionUID = -7145752642432355552L;
		public static UID UID = new UID("Mw4i");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_MD_MANDATOR_LEVEL";}
		@Override public String getEntityName() {return "nuclos_mandatorLevel";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.mandatorLevel.label";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.glyphish-blue.189-plant.png";}
		/**
		 * Field:    <b>name</b><br>
		 * DbColumn: <b>STRNAME</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>Mw4ia</b>
		 */
		public final Name name = new Name();
		public static class Name extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Mw4ia");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRNAME";}
			@Override public String getFieldName() {return "name";}
			@Override public UID getEntity() {return _MandatorLevel.UID;}
			@Override public Integer getScale() {return 255;}
			@Override public boolean isNullable() {return false;}
			@Override public boolean isUnique() {return true;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.mandatorLevel.name.label";}
		}
		/**
		 * Field:    <b>parentLevel</b><br>
		 * DbColumn: <b>STRUID_PARENTLEVEL</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>Mw4ib</b>
		 */
		public final ParentLevel parentLevel = new ParentLevel();
		public static class ParentLevel extends FieldMeta<org.nuclos.common.UID> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Mw4ib");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_PARENTLEVEL";}
			@Override public String getFieldName() {return "parentLevel";}
			@Override public UID getEntity() {return _MandatorLevel.UID;}
			@Override public UID getForeignEntity() {return _MandatorLevel.UID;}
			@Override public String getForeignEntityField() {return stringify(_MandatorLevel.Name.UID);}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.mandatorLevel.parentLevel.label";}
		}
		/**
		 * Field:    <b>showName</b><br>
		 * DbColumn: <b>BLNSHOWNAME</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>Mw4ic</b>
		 */
		public final ShowName showName = new ShowName();
		public static class ShowName extends FieldMeta.Valueable<java.lang.Boolean> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Mw4ic");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNSHOWNAME";}
			@Override public String getFieldName() {return "showName";}
			@Override public UID getEntity() {return _MandatorLevel.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getDefaultValue() {return Boolean.FALSE.toString();}
			@Override public String getDefaultMandatory() {return Boolean.FALSE.toString();}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.mandatorLevel.showName.label";}
		}
	}
	
	/**
	 * Entity:         <b>CALCATTRIBUTE</b><br>
	 * DbTable:        <b>T_MD_CALCATTRIBUTE</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>d4n1</b>
	 */
	public static final _CalcAttribute CALCATTRIBUTE = new _CalcAttribute();
	public static class _CalcAttribute extends EntityMeta<UID> {
		private static final long serialVersionUID = 1L;
		public static UID UID = new UID("d4n1");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_MD_CALCATTRIBUTE";}
		@Override public String getEntityName() {return "nuclos_calcAttribute";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isCacheable() {return true;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.calcattribute.label";}
		@Override public String getLocaleResourceIdForMenuPath() {return "nuclos.entity.calcattribute.menupath";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.glyphish-blue.12-eye.png";}
	}

	public static final _Session SESSION = new _Session();
	public static final class _Session extends EntityMeta<Long> {
		private static final long serialVersionUID = -5186516364826055808L;

		public static UID UID = new UID("Ytrp");
		@Override public UID getUID() {return UID; }
		@Override public String getDbTable() {return "T_AD_SESSIONS";}
		@Override public String getEntityName() {return "T_AD_SESSIONS";}
		@Override public Class<Long> getPkClass() {return Long.class;}

		public final UserID USER_ID = new UserID();
		public static class UserID extends FieldMeta.Valueable<String> {
			private static final long serialVersionUID = 3819785859369486105L;
			public static UID UID = new UID("Ytrpa");
			@Override public UID getUID() {return UID;}
			@Override public UID getEntity() {return _Session.UID;}
			@Override public String getFieldName() {return "USER_ID";}
			@Override public String getDbColumn() {return "USER_ID";}
			@Override public Class<String> getJavaClass() {return String.class;}
			@Override public Integer getScale() { return 30; }
		};

		public final Application APPLICATION = new Application();
		public static class Application extends FieldMeta.Valueable<String> {
			private static final long serialVersionUID = 3819785859369486105L;
			public static UID UID = new UID("Ytrpb");
			@Override public UID getUID() {return UID;}
			@Override public UID getEntity() {return _Session.UID;}
			@Override public String getFieldName() {return "APPLICATION";}
			@Override public String getDbColumn() {return "APPLICATION";}
			@Override public Class<String> getJavaClass() {return String.class;}
			@Override public Integer getScale() { return 100; }
		};

		public final Logon LOGON = new Logon();
		public static class Logon extends FieldMeta.Valueable.Valueable<InternalTimestamp> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Ytrpc");
			@Override public UID getUID() {return UID;}
			@Override public Class<InternalTimestamp> getJavaClass() {return InternalTimestamp.class;}
			@Override public String getDbColumn() {return "LOGON";}
			@Override public String getFieldName() {return "LOGON";}
			@Override public UID getEntity() {return SESSION.UID;}
		}

		public final Logoff LOGOFF = new Logoff();
		public static class Logoff extends FieldMeta.Valueable<InternalTimestamp> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Ytrpd");
			@Override public UID getUID() {return UID;}
			@Override public Class<InternalTimestamp> getJavaClass() {return InternalTimestamp.class;}
			@Override public String getDbColumn() {return "LOGOFF";}
			@Override public String getFieldName() {return "LOGOFF";}
			@Override public UID getEntity() {return SESSION.UID;}
		}

		public final SessionID SESSION_ID = new SessionID();
		public static class SessionID extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Ytrpe");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "SESSION_ID";}
			@Override public String getFieldName() {return "SESSION_ID";}
			@Override public UID getEntity() {return SESSION.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() { return 255; }
			@Override public String getDefaultValue() {return null;}
			@Override public String getDefaultMandatory() {return Boolean.FALSE.toString();}
		}

		public final ClusterServerID CLUSTER_SERVER_ID = new ClusterServerID();
		public static class ClusterServerID extends FieldMeta.Valueable<java.lang.String> {
			private static final long serialVersionUID = 1L;
			public static UID UID = new UID("Ytrpf");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "CLUSTER_SERVER_ID";}
			@Override public String getFieldName() {return "CLUSTER_SERVER_ID";}
			@Override public UID getEntity() {return SESSION.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() { return 20; }
			@Override public String getDefaultValue() {return null;}
			@Override public String getDefaultMandatory() {return Boolean.FALSE.toString();}
		}

		private UID[][] indexFieldCombinations = new UID[][]{
				new UID[]{ _Session.SessionID.UID, _Session.ClusterServerID.UID },
				new UID[]{ _Session.Logoff.UID, _Session.ClusterServerID.UID }
		};
		@Override public UID[][] getIndexFieldCombinations() {return indexFieldCombinations;}
	}

	/**
	 * Entity:         <b>CLUSTER_SERVER</b><br>
	 * DbTable:        <b>T_AD_CLUSTER_SERVER</b><p>
	 * PrimaryKey:     <b>UID.class</b><br>
	 * UID:            <b>09Z3</b>
	 */
	public static final _ClusterServer CLUSTER_SERVER = new _ClusterServer();
	public static class _ClusterServer extends EntityMeta<UID> {
		private static final long serialVersionUID = -8679184404014372067L;
		public static UID UID = new UID("09Z3");
		@Override public UID getUID() {return UID;}
		@Override public Class<UID> getPkClass() {return UID.class;}
		@Override public String getDbTable() {return "T_AD_CLUSTER_SERVER";}
		@Override public String getEntityName() {return "nuclos_clusterServer";}
		@Override public boolean isUidEntity() {return true;}
		@Override public boolean isEditable() {return false;}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isCacheable() {return true;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.clusterServer.label";}
		@Override public String getLocaleResourceIdForTreeView() {return "nuclos.entity.clusterServer.treeview";}
		@Override public String getLocaleResourceIdForMenuPath() {return "nuclos.entity.clusterServer.menupath";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.glyphish-blue.113-navigation.png";}
		@Override public Integer getMenuPathOrder() {return 0;}

		/**
		 * Field:    <b>protocol</b><br>
		 * DbColumn: <b>STRPROTOCOL</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3a</b>
		 */
		public final Protocol protocol = new Protocol();
		public static class Protocol extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3a");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRPROTOCOL";}
			@Override public String getFieldName() {return "protocol";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() {return 16;}
			@Override public Integer getOrder() {return 0;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.protocol.label";}
		}

		/**
		 * Field:    <b>hostname</b><br>
		 * DbColumn: <b>STRHOSTNAME</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3b</b>
		 */
		public final Hostname hostname = new Hostname();
		public static class Hostname extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3b");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRHOSTNAME";}
			@Override public String getFieldName() {return "hostname";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() {return 128;}
			@Override public Integer getOrder() {return 1;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.hostname.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.clusterServer.hostname.description";}
		}

		/**
		 * Field:    <b>lastStatusUpdate</b><br>
		 * DbColumn: <b>DATLASTSTATUSUPDATE</b><p>
		 * Type:     <b>java.util.Date.class</b><br>
		 * UID:      <b>09Z3c</b>
		 */
		public final LastStatusUpdate lastStatusUpdate = new LastStatusUpdate();
		public static class LastStatusUpdate extends FieldMeta.Valueable<org.nuclos.common2.InternalTimestamp> {
			public static UID UID = new UID("09Z3c");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common2.InternalTimestamp> getJavaClass() {return org.nuclos.common2.InternalTimestamp.class;}
			@Override public String getDbColumn() {return "DATLASTSTATUSUPDATE";}
			@Override public String getFieldName() {return "lastStatusUpdate";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getOrder() {return 7;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.lastStatusUpdate.label";}
		}

		/**
		 * Field:    <b>type</b><br>
		 * DbColumn: <b>STRTYPE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3d</b>
		 */
		public final Type type = new Type();
		public static class Type extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3d");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRTYPE";}
			@Override public String getFieldName() {return "type";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() {return 16;}
			@Override public Integer getOrder() {return 5;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.type.label";}
		}

		/**
		 * Field:    <b>status</b><br>
		 * DbColumn: <b>STRSTATUS</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3e</b>
		 */
		public final Status status = new Status();
		public static class Status extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3e");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRSTATUS";}
			@Override public String getFieldName() {return "status";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() {return 255;}
			@Override public Integer getOrder() {return 6;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.status.label";}
		}

		/**
		 * Field:    <b>Port</b><br>
		 * DbColumn: <b>INTPORT</b><p>
		 * Type:     <b>java.lang.Integer.class</b><br>
		 * UID:      <b>09Z3f</b>
		 */
		public final Port port = new Port();
		public static class Port extends FieldMeta.Valueable<java.lang.Integer> {
			public static UID UID = new UID("09Z3f");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Integer> getJavaClass() {return java.lang.Integer.class;}
			@Override public String getDbColumn() {return "INTPORT";}
			@Override public String getFieldName() {return "port";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() {return 5;}
			@Override public Integer getOrder() {return 3;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.port.label";}
		}

		/**
		 * Field:    <b>context</b><br>
		 * DbColumn: <b>STRCONTEXT</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3g</b>
		 */
		public final Context context = new Context();
		public static class Context extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3g");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRCONTEXT";}
			@Override public String getFieldName() {return "context";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() {return 64;}
			@Override public Integer getOrder() {return 4;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.context.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.clusterServer.context.description";}
		}

		/**
		 * Field:    <b>installedVersion</b><br>
		 * DbColumn: <b>STRINSTALLEDVERSION</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3h</b>
		 */
		public final InstalledVersion installedVersion = new InstalledVersion();
		public static class InstalledVersion extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3h");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRINSTALLEDVERSION";}
			@Override public String getFieldName() {return "installedVersion";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() {return 255;}
			@Override public Integer getOrder() {return 8;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.installedVersion.label";}
		}

		/**
		 * Field:    <b>javaVersion</b><br>
		 * DbColumn: <b>STRJAVAVERSION</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3i</b>
		 */
		public final JavaVersion javaVersion = new JavaVersion();
		public static class JavaVersion extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3i");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRJAVAVERSION";}
			@Override public String getFieldName() {return "javaVersion";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() {return 255;}
			@Override public Integer getOrder() {return 9;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.javaVersion.label";}
		}

		/**
		 * Field:    <b>serverExtensionsHash</b><br>
		 * DbColumn: <b>STRSERVEREXTENSIONSHASH</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3j</b>
		 */
		public final ServerExtensionsHash serverExtensionsHash = new ServerExtensionsHash();
		public static class ServerExtensionsHash extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3j");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRSERVEREXTENSIONSHASH";}
			@Override public String getFieldName() {return "serverExtensionsHash";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return true;}
			@Override public Integer getScale() {return 255;}
			@Override public Integer getOrder() {return 10;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.serverExtensionsHash.label";}
		}

		/**
		 * Field:    <b>balancerProtocol</b><br>
		 * DbColumn: <b>STRBALANCERPROTOCOL</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3k</b>
		 */
		public final BalancerProtocol balancerProtocol = new BalancerProtocol();
		public static class BalancerProtocol extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3k");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRBALANCERPROTOCOL";}
			@Override public String getFieldName() {return "balancerProtocol";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return true;}
			@Override public Integer getScale() {return 16;}
			@Override public Integer getOrder() {return 11;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.balancerProtocol.label";}
		}

		/**
		 * Field:    <b>balancerHostname</b><br>
		 * DbColumn: <b>STRBALANCERHOSTNAME</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3l</b>
		 */
		public final BalancerHostname balancerHostname = new BalancerHostname();
		public static class BalancerHostname extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3l");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRBALANCERHOSTNAME";}
			@Override public String getFieldName() {return "balancerHostname";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return true;}
			@Override public Integer getScale() {return 128;}
			@Override public Integer getOrder() {return 12;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.balancerHostname.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.clusterServer.balancerHostname.description";}
		}

		/**
		 * Field:    <b>balancerPort</b><br>
		 * DbColumn: <b>INTBALANCERPORT</b><p>
		 * Type:     <b>java.lang.Integer.class</b><br>
		 * UID:      <b>09Z3m</b>
		 */
		public final BalancerPort balancerPort = new BalancerPort();
		public static class BalancerPort extends FieldMeta.Valueable<java.lang.Integer> {
			public static UID UID = new UID("09Z3m");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Integer> getJavaClass() {return java.lang.Integer.class;}
			@Override public String getDbColumn() {return "INTBALANCERPORT";}
			@Override public String getFieldName() {return "balancerPort";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return true;}
			@Override public Integer getScale() {return 5;}
			@Override public Integer getOrder() {return 13;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.balancerPort.label";}
		}

		/**
		 * Field:    <b>balancerContext</b><br>
		 * DbColumn: <b>STRBALANCERCONTEXT</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>09Z3n</b>
		 */
		public final BalancerContext balancerContext = new BalancerContext();
		public static class BalancerContext extends FieldMeta.Valueable<java.lang.String> {
			public static UID UID = new UID("09Z3n");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRBALANCERCONTEXT";}
			@Override public String getFieldName() {return "balancerContext";}
			@Override public UID getEntity() {return _ClusterServer.UID;}
			@Override public boolean isNullable() {return true;}
			@Override public Integer getScale() {return 64;}
			@Override public Integer getOrder() {return 14;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterServer.balancerContext.label";}
			@Override public String getLocaleResourceIdForDescription() {return "nuclos.entityfield.clusterServer.balancerContext.description";}
		}

	}

	/**
	 * Entity:         <b>CLUSTER_MESSAGE</b><br>
	 * DbTable:        <b>T_AD_CLUSTER_MESSAGE</b><p>
	 * PrimaryKey:     <b>Long.class</b><br>
	 * UID:            <b>IOTy</b>
	 */
	public static final _ClusterMessage CLUSTER_MESSAGE = new _ClusterMessage();
	public static final class _ClusterMessage extends EntityMeta<Long> {
		public static final UID UID = new UID("IOTy");
		private static final long serialVersionUID = -8919713803732673057L;
		@Override public UID getUID() {return UID;}
		@Override public Class<Long> getPkClass() {return Long.class;}
		@Override public String getDbTable() {return "T_AD_CLUSTER_MESSAGE";}
		@Override public String getEntityName() {return "nuclos_clusterMessage";}
		@Override public boolean isEditable() {return false;}
		@Override public boolean isThin() {return true;}
		@Override public boolean isSearchable() {return false;}
		@Override public boolean isCacheable() {return false;}
		@Override public String getLocaleResourceIdForLabel() {return "nuclos.entity.clusterMessage.label";}
		@Override public String getLocaleResourceIdForTreeView() {return "nuclos.entity.clusterMessage.treeview";}
		@Override public String getNuclosResource() {return "org.nuclos.client.resource.icon.glyphish-blue.124-bullhorn.png";}

		/**
		 * Field:    <b>server</b><br>
		 * DbColumn: <b>STRUID_CLUSTER_SERVER</b><p>
		 * Type:     <b>org.nuclos.common.UID.class</b><br>
		 * UID:      <b>IOTya</b>
		 */
		public final Server server = new Server();
		private static final class Server extends FieldMeta.Valueable<UID> {
			public static final UID UID = new UID("IOTya");
			@Override public UID getUID() {return UID;}
			@Override public Class<org.nuclos.common.UID> getJavaClass() {return org.nuclos.common.UID.class;}
			@Override public String getDbColumn() {return "STRUID_CLUSTER_SERVER";}
			@Override public String getFieldName() {return "server";}
			@Override public UID getEntity() {return _ClusterMessage.UID;}
			@Override public UID getForeignEntity() {return RigidE._ClusterServer.UID;}
			@Override public String getForeignEntityField() {
				return stringify(RigidE._ClusterServer.Hostname.UID) + ":" + stringify(RigidE._ClusterServer.Port.UID) + "/"
						+ stringify(RigidE._ClusterServer.Context.UID);
			}
			@Override public boolean isOnDeleteCascade() {return true;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getOrder() {return 1;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterMessage.server.label";}
		}

		/**
		 * Field:    <b>read</b><br>
		 * DbColumn: <b>BLNREAD</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>IOTyc</b>
		 */
		public final Read read = new Read();
		private static final class Read extends FieldMeta.Valueable<java.lang.Boolean> {
			public static final UID UID = new UID("IOTyc");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNREAD";}
			@Override public String getFieldName() {return "read";}
			@Override public UID getEntity() {return _ClusterMessage.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getDefaultValue() {return Boolean.FALSE.toString();}
			@Override public String getDefaultMandatory() {return Boolean.FALSE.toString();}
			@Override public Integer getOrder() {return 3;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterMessage.read.label";}
		}

		/**
		 * Field:    <b>done</b><br>
		 * DbColumn: <b>BLNDONE</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>IOTyd</b>
		 */
		public final Done done = new Done();
		private static final class Done extends FieldMeta.Valueable<java.lang.Boolean> {
			public static final UID UID = new UID("IOTyd");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNDONE";}
			@Override public String getFieldName() {return "done";}
			@Override public UID getEntity() {return _ClusterMessage.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getDefaultValue() {return Boolean.FALSE.toString();}
			@Override public String getDefaultMandatory() {return Boolean.FALSE.toString();}
			@Override public Integer getOrder() {return 4;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterMessage.done.label";}
		}

		/**
		 * Field:    <b>content</b><br>
		 * DbColumn: <b>STRCONTENT</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>IOTye</b>
		 */
		public final Content content = new Content();
		private static final class Content extends FieldMeta.Valueable<java.lang.String> {
			public static final UID UID = new UID("IOTye");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRCONTENT";}
			@Override public String getFieldName() {return "content";}
			@Override public UID getEntity() {return _ClusterMessage.UID;}
			@Override public boolean isNullable() {return true;}
			@Override public Integer getScale() {return 4000;}
			@Override public Integer getOrder() {return 6;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterMessage.content.label";}
		}

		/**
		 * Field:    <b>type</b><br>
		 * DbColumn: <b>STRTYPE</b><p>
		 * Type:     <b>java.lang.String.class</b><br>
		 * UID:      <b>IOTyf</b>
		 */
		public final Type type = new Type();
		private static final class Type extends FieldMeta.Valueable<java.lang.String> {
			public static final UID UID = new UID("IOTyf");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.String> getJavaClass() {return java.lang.String.class;}
			@Override public String getDbColumn() {return "STRTYPE";}
			@Override public String getFieldName() {return "type";}
			@Override public UID getEntity() {return _ClusterMessage.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public Integer getScale() {return 32;}
			@Override public Integer getOrder() {return 5;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterMessage.type.label";}
		}

		/**
		 * Field:    <b>master</b><br>
		 * DbColumn: <b>BLNMASTER</b><p>
		 * Type:     <b>java.lang.Boolean.class</b><br>
		 * UID:      <b>IOTyg</b>
		 */
		public final Master master = new Master();
		private static final class Master extends FieldMeta.Valueable<java.lang.Boolean> {
			public static final UID UID = new UID("IOTyg");
			@Override public UID getUID() {return UID;}
			@Override public Class<java.lang.Boolean> getJavaClass() {return java.lang.Boolean.class;}
			@Override public String getDbColumn() {return "BLNMASTER";}
			@Override public String getFieldName() {return "master";}
			@Override public UID getEntity() {return _ClusterMessage.UID;}
			@Override public boolean isNullable() {return false;}
			@Override public String getDefaultValue() {return Boolean.FALSE.toString();}
			@Override public String getDefaultMandatory() {return Boolean.FALSE.toString();}
			@Override public Integer getOrder() {return 2;}
			@Override public String getLocaleResourceIdForLabel() {return "nuclos.entityfield.clusterMessage.master.label";}
		}

		private UID[][] indexFieldCombinations = new UID[][]{
				new UID[]{ _ClusterMessage.Read.UID, _ClusterMessage.Master.UID  },
				new UID[]{ _ClusterMessage.Read.UID, _ClusterMessage.Master.UID, _ClusterMessage.Server.UID  }
		};
		@Override public UID[][] getIndexFieldCombinations() {return indexFieldCombinations;}
	}

	public static String stringify(UID uid) {
		return String.format("uid{%s}", uid.getString());
	}

}
