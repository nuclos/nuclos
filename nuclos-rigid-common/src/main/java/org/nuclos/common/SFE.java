//Copyright (C) 2022  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Extended Static Fields
 * These fields can only be generated with full entity metadata, a single entityUID is not sufficient.
 * If you know exactly what you are doing, the {@link SF#getLimitedMetaData(UID)} method can be used for backwards compatibility.
 */
public abstract class SFE<T> extends SF<T> {

	public static final SFEValueable<String> PROCESS = new SFEValueable<String>(10, "nuclosProcess", "STRVALUE_NUCLOSPROCESS", false, String.class, 255, ORDER_PROCESS) {
		private static final long serialVersionUID = 1L;
		@Override
		FieldMeta<?> extendStaticMeta(final FieldMeta<?> fieldMeta, final EntityMeta<?> entityMeta) {
			if (entityMeta.getProcessGroup() != null) {
				((FieldMetaVO<?>) fieldMeta).setFieldGroup(entityMeta.getProcessGroup());
			}
			return fieldMeta;
		}
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createProcess(entityUID);}
	};
	public static final SFE<UID> PROCESS_UID = new SFE<UID>(10, "nuclosProcess", "STRUID_NUCLOSPROCESS", false, UID.class, UID_SCALE, ORDER_PROCESS) {
		private static final long serialVersionUID = 1L;
		@Override
		FieldMeta<?> extendStaticMeta(final FieldMeta<?> fieldMeta, final EntityMeta<?> entityMeta) {
			if (entityMeta.getProcessGroup() != null) {
				((FieldMetaVO<?>) fieldMeta).setFieldGroup(entityMeta.getProcessGroup());
			}
			return fieldMeta;
		}
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createProcess(entityUID);}
	};
	public static final SFEValueable<String> OWNER = new SFEValueable<String>(19, "nuclosOwner", "STRVALUE_NUCLOSOWNER", false, String.class, 255, ORDER_OWNER) {
		private static final long serialVersionUID = 1L;
		@Override
		FieldMeta<?> extendStaticMeta(final FieldMeta<?> fieldMeta, final EntityMeta<?> entityMeta) {
			if (entityMeta.getOwnerForeignEntityField() != null) {
				((FieldMetaVO<?>)fieldMeta).setForeignEntityField(entityMeta.getOwnerForeignEntityField());
			}
			return fieldMeta;
		}
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createOwner();}
	};
	public static final SFE<UID> OWNER_UID = new SFE<UID>(19, "nuclosOwner", "STRUID_NUCLOSOWNER", false, UID.class, UID_SCALE, ORDER_OWNER) {
		private static final long serialVersionUID = 1L;
		@Override
		FieldMeta<?> extendStaticMeta(final FieldMeta<?> fieldMeta, final EntityMeta<?> entityMeta) {
			if (entityMeta.getOwnerForeignEntityField() != null) {
				((FieldMetaVO<?>)fieldMeta).setForeignEntityField(entityMeta.getOwnerForeignEntityField());
			}
			return fieldMeta;
		}
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createOwner();}
	};
	public static final SFEValueable<String> ATTRIBUTE_COLOR = new SFEValueable<String>(null, null, null, false, String.class, ATTRIBUTE_COLOR_SCALE, ORDER_ATTRIBUTE_COLOR) {
		private FieldMeta<?> baseField;

		@Override
		public String getFieldName() {
			if (baseField != null) {
				return baseField.getFieldName() + FieldMeta.ATTRIBUTE_COLOR_DATASOURCE_SUFFIX;
			}
			return super.getFieldName();
		}

		@Override
		public String getDbColumn() {
			if (baseField != null) {
				return baseField.getDbColumn() + FieldMeta.ATTRIBUTE_COLOR_DATASOURCE_SUFFIX;
			}
			return super.getDbColumn();
		}

		@Override
		public boolean checkField(final UID entity, final UID field) {
			return field != null && field.toString().endsWith(FieldMeta.ATTRIBUTE_COLOR_DATASOURCE_SUFFIX);
		}

		@Override
		public UID getUID(final EntityMeta<?> entityMeta) {
			return getUID(entityMeta, baseField);
		}

		@Override
		protected UID createFieldUID(final UID entityUID) {
			return UID.parseUID(baseField.getUID().getString() + FieldMeta.ATTRIBUTE_COLOR_DATASOURCE_SUFFIX);
		}

		@Override
		FieldMeta<?> extendStaticMeta(final FieldMeta<?> fieldMeta, final EntityMeta<?> entityMeta) {
			return fieldMeta;
		}

		@Override
		FieldMeta<?> extendStaticMeta(final FieldMeta<?> fieldMeta, final EntityMeta<?> entityMeta, FieldMeta<?> baseField) {
			this.baseField = baseField;

			FieldMetaVO<?> meta = (FieldMetaVO<?>) fieldMeta;

			meta.setUID(createFieldUID(entityMeta.getUID()));
			meta.getUID().setDebugInfo(getFieldName());

			if (baseField.getColorDatasource() != null) {
				((FieldMetaVO<?>) fieldMeta).setCalcAttributeDS(baseField.getColorDatasource());
				((FieldMetaVO<?>) fieldMeta).setCalcAttributeParamValues(
						"strfield = STR[" + baseField.getFieldName() + "]" +
								" && struidfield = STR[" + baseField.getUID() + "]" +
								" && field_dbcolumn = STR[" + baseField.getDbColumn() + "]"
				);
				((FieldMetaVO<?>) fieldMeta).setDbColumn(baseField.getDbColumn() + FieldMeta.ATTRIBUTE_COLOR_DATASOURCE_SUFFIX);
				((FieldMetaVO<?>) fieldMeta).setFieldName(baseField.getDbColumn() + FieldMeta.ATTRIBUTE_COLOR_DATASOURCE_SUFFIX);
			}
			return fieldMeta;
		}

		@Override
		FieldMeta<?> createStaticMeta(final UID entityUID) {
			return createAttributeColor();
		}



		@Override
		FieldMeta<?> createMeta(final UID entityUID) {
			FieldMetaVO<?> meta = (FieldMetaVO<?>) createStaticMeta(entityUID);

			meta.setEntity(entityUID);
			meta.setDataType(getJavaClass().getName());
			meta.setScale(scale);
			return meta;
		}
	};

	private static FieldMeta<?> createProcess(UID entityUID) {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_WRITE);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setForeignEntity(UID_PROCESS);
		result.setForeignEntityField("uid{ui9lb}");

		result.setLocaleResourceIdForLabel(RESOURCE_LABEL_PROCESS_PREFIX+entityUID.getString());
		result.setLocaleResourceIdForDescription(RESOURCE_DESCRIPTION_PROCESS_PREFIX+entityUID.getString());

		result.setOrder(ORDER_PROCESS);

		return result;
	}

	private static FieldMeta<?> createOwner() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);
		result.setReadonly(true);

		result.setCalcFunction(OWNER_CALC_FUNCTION);
		result.setForeignEntity(UID_USER);
		result.setForeignEntityField("uid{dRxja}");

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.owner.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.owner.description");

		result.setOrder(ORDER_OWNER);

		return result;
	}

	private static FieldMeta<?> createAttributeColor() {
		FieldMetaVO<?> result = new FieldMetaVO<>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setNullable(true);
		result.setModifiable(false);
		result.setSearchable(false);
		result.setLogBookTracking(false);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);
		result.setReadonly(true);
		result.setHidden(true);

		result.setOrder(ORDER_ATTRIBUTE_COLOR);

		return result;
	}

	private transient final SFE.MetaDataCache metaCache;

	SFE(final Integer postfix, final String field, final String dbField, final boolean forceValueSearch, final Class cls, final Integer scale, final Integer order) {
		super(postfix, field, dbField, forceValueSearch, cls, scale, order);
		this.metaCache = new SFE.MetaDataCache(this);
	}

	@Override
	public FieldMeta<T> getMetaData(final EntityMeta<?> entityMeta) {
		return getMetaData(entityMeta, false);
	}

	public FieldMeta<T> getMetaData(final EntityMeta<?> entityMeta, final boolean forceCreate) {
		return (FieldMeta<T>) metaCache.getMetaData(entityMeta, forceCreate);
	}

	public FieldMeta<T> getMetaData(final EntityMeta<?> entityMeta, FieldMeta<?> fieldUID, final boolean forceCreate) {
		return (FieldMeta<T>) metaCache.getMetaData(entityMeta, fieldUID, forceCreate);
	}

	@Override
	public UID getUID(final EntityMeta<?> entityMeta) {
		return metaCache.getMetaData(entityMeta, false).getUID();
	}

	@Override
	public UID getUID(final EntityMeta<?> entityMeta, final FieldMeta<?> field) {
		return metaCache.getMetaData(entityMeta, field, false).getUID();
	}

	abstract FieldMeta<?> extendStaticMeta(final FieldMeta<?> fieldMeta, final EntityMeta<?> entityMeta);

	FieldMeta<?> extendStaticMeta(final FieldMeta<?> fieldMeta, final EntityMeta<?> entityMeta, FieldMeta<?> baseField) {
		return this.extendStaticMeta(fieldMeta, entityMeta);
	}

	private FieldMeta<?> createMeta(final EntityMeta<?> entityMeta) {
		return extendStaticMeta(super.createMeta(entityMeta.getUID()), entityMeta);
	}

	private FieldMeta<?> createMeta(final EntityMeta<?> entityMeta, FieldMeta<?> baseField) {
		return extendStaticMeta(createMeta(entityMeta.getUID()), entityMeta, baseField);
	}

	private static class MetaDataCache {

		private final ConcurrentMap<UID, FieldMeta<?>> entityFields = new ConcurrentHashMap<>();

		private final SFE<?> sfe;

		public MetaDataCache(SFE<?> sfe) {
			this.sfe = sfe;
		}

		FieldMeta<?> getMetaData(final EntityMeta<?> entityMeta, final boolean forceCreate) {
			FieldMeta<?> result;
			if (forceCreate) {
				result = sfe.createMeta(entityMeta);
				entityFields.put(entityMeta.getUID(), result);
			} else {
				result = entityFields.get(entityMeta.getUID());
			}
			if (result != null) {
				return result;
			}
			result = sfe.createMeta(entityMeta);
			entityFields.put(entityMeta.getUID(), result);
			return result;
		}

		FieldMeta<?> getMetaData(final EntityMeta<?> entityMeta, FieldMeta<?> field, final boolean forceCreate) {
			FieldMeta<?> result;
			if (forceCreate) {
				result = sfe.createMeta(entityMeta, field);
				entityFields.put(field.getUID(), result);
			} else {
				result = entityFields.get(field.getUID());
			}
			if (result != null) {
				return result;
			}
			result = sfe.createMeta(entityMeta, field);
			entityFields.put(field.getUID(), result);
			return result;
		}
	}

}
