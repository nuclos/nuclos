//Copyright (C) 2019  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import javax.validation.constraints.NotNull;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

public class SpringApplicationHelper {

	/**
	 * Autowired is processed
	 * @param c
	 * @param <T>
	 * @return
	 */
	public static <T> T newInstanceAutowiredAndInitialized(@NotNull ApplicationContext appContext, @NotNull Class<T> c, Object... constructorParameters) {
		try {
			Object[] parameterValues = constructorParameters;
			Class<?>[] constructorParameterTypes = new Class<?>[constructorParameters.length];
			for (int i = 0; i < constructorParameters.length; i++) {
				Object param = constructorParameters[i];
				if (param == null) {
					throw new IllegalArgumentException("constructor parameter can not be null");
				}
				if (param instanceof ConstructorParameter) {
					ConstructorParameter constructorParameter = ((ConstructorParameter) param);
					constructorParameterTypes[i] = constructorParameter.getConstructorClass();
					parameterValues[i] = constructorParameter.getObject();
				} else {
					constructorParameterTypes[i] = param.getClass();
				}
			}
			Constructor<T> constructor = c.getDeclaredConstructor(constructorParameterTypes);
			constructor.setAccessible(true);
			T t = constructor.newInstance(parameterValues);
			AutowireCapableBeanFactory factory = appContext.getAutowireCapableBeanFactory();
			factory.autowireBean(t);
			factory.initializeBean(t, c.getCanonicalName());
			return t;
		} catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
			throw new NuclosFatalException(e);
		}
	}

}
