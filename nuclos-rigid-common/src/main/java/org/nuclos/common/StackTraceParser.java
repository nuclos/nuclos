package org.nuclos.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class StackTraceParser {
	private final List<String> stackFrames;

	public StackTraceParser(final Throwable throwable) {
		stackFrames = Arrays.asList(ExceptionUtils.getStackFrames(throwable));
	}

	public StackTraceParser(final List<StackTraceElement> stackTraceElements) {
		if (stackTraceElements == null) {
			stackFrames = new ArrayList<>();
		} else {
			stackFrames = stackTraceElements.stream()
					.map(StackTraceElement::toString)
					.collect(Collectors.toList());
		}
	}

	public List<String> getStackFrames() {
		return stackFrames;
	}

	public List<String> getShortenedStackFrames() {
		List<String> result = new ArrayList<>();

		for (List<String> segment : getStackTraceSegments()) {
			final int end = findEnd(segment);
			final int removeCount = segment.size() - end - 1;
			if (removeCount > 1) {
				segment = segment.subList(0, end);

				int omitted = findOmittedCount(segment) + removeCount;
				segment.add("\t... " + omitted + " more");
			}
			result.addAll(segment);
		}

		return result;
	}

	int findEnd(List<String> segment) {
		int end = 5;
		for (int i = end; i < segment.size(); i++) {
			final String stackFrame = segment.get(i);
			if (RigidUtils.isNuclosStackFrame(stackFrame)) {
				end = i;
			}
		}
		return end;
	}

	int findOmittedCount(List<String> segment) {
		int result = 0;
		String lastFrame = segment.get(segment.size() - 1);
		Pattern pattern = Pattern.compile(".*?(\\d+) more");
		Matcher matcher = pattern.matcher(lastFrame);

		if (matcher.find()) {
			result = Integer.parseInt(matcher.group(1));
		}

		return result;
	}

	List<List<String>> getStackTraceSegments() {
		List<List<String>> result = new ArrayList<>();

		if (stackFrames.isEmpty()) {
			return result;
		}

		List<String> currentSegment = new ArrayList<>();
		currentSegment.add(stackFrames.get(0));
		for (int i = 1; i < stackFrames.size(); i++) {
			String stackFrame = stackFrames.get(i);
			if (StringUtils.startsWith(stackFrame, "Caused by: ")) {
				result.add(currentSegment);
				currentSegment = new ArrayList<>();
			}
			currentSegment.add(stackFrame);
		}
		result.add(currentSegment);

		return result;
	}
}
