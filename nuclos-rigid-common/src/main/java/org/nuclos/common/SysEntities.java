//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.dblayer.Schema;
import org.nuclos.common.dblayer.SchemaHelperVersion;

public abstract class SysEntities {
	
	private Map<UID, EntityMeta<?>> mpEntities = null;
	
	private Map<UID, EntityMeta<?>> _getMpEntities() {
		if (mpEntities == null) {
			Map<UID, EntityMeta<?>> initAllEntities = new HashMap<UID, EntityMeta<?>>();
			try {
				// declared before all others...
				addAllEntities(initAllEntities, getClass().getDeclaredFields());
				addAllEntities(initAllEntities, getClass().getFields());
			} catch (Exception e1) {
				throw new RuntimeException(String.format("Receiving all entities from class %s failed: %s", getClass(), e1.getMessage()));
			} 
			mpEntities = initAllEntities;
		}
		return mpEntities;
	}
	
	private static void addAllEntities(Map<UID, EntityMeta<?>> allEntities, Field[] fields) throws Exception {
		for (Field f : fields) {
			try {
				f.setAccessible(true);
				Object o = f.get(null);
				if (o instanceof EntityMeta<?>) {
					EntityMeta<?> em = (EntityMeta<?>)o;
					if (!allEntities.containsKey(em.getUID())) {
						allEntities.put(em.getUID(), em);
					}
				}
			} catch (NullPointerException e) {
				// ignore
			} catch (Exception e) {
				throw e;
			} 
		}
	}
	
	public Collection<EntityMeta<?>> _getAllEntities() {
		return _getMpEntities().values();
	}
	
	public boolean _isNuclosEntity(UID entityUID) {
		return _getMpEntities().containsKey(entityUID);
	}

	public <PK> EntityMeta<PK> _getByUID(UID entityUID) {
		return (EntityMeta<PK>)_getMpEntities().get(entityUID);
	}
	
	public <PK> EntityMeta<PK> _getByName(String entityName) {
		for (EntityMeta<?> entity : _getAllEntities()) {
			if (entity.getEntityName().equals(entityName))
				return (EntityMeta<PK>) entity;
		}
		return null;
	}

	/**
	 * Usable from server and client (LangUtils.getClassLoaderThatWorksForWebStart())
	 */
	public String _getSchemaVersion(ClassLoader cl) {
		if (cl == null) {
			throw new IllegalArgumentException("ClassLoader must not be null");
		}
		final Version nuclosVersion = Version.readNuclosVersion(cl);
		final Schema schemaAnnotation = getClass().getAnnotation(Schema.class);
		String schemaVersion = schemaAnnotation.version();
		if (Schema.AUTO_VERSION.equals(schemaVersion)) {
			schemaVersion = nuclosVersion.getMajorMinorVersionNumber() + "." + schemaAnnotation.changeCount();
		}
		return schemaVersion;
	}

	/**
	 * Use only inside the server
	 */
	public String _getSchemaVersion() {
		return this._getSchemaVersion(Thread.currentThread().getContextClassLoader());
	}
	
	public SchemaHelperVersion _getSchemaHelperVersion() {
		return getClass().getAnnotation(Schema.class).helper();
	}
	
	public static String stringify(UID uid) {
		return RigidE.stringify(uid);
	}
	
	public static String stringify(FieldMeta<?> fm) {
		return RigidE.stringify(fm.getUID());
	}
	
}
