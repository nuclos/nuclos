//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.IOException;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

@XmlType
@XmlRootElement(name="uid")
@JsonIgnoreProperties(ignoreUnknown = true)
//mark-as-generated-in-the-ccce-module
public final class UID implements org.nuclos.api.UID, Serializable, Comparable<UID> {
	
	private static final long serialVersionUID = -4222736191149255213L;
	
	private static final int LENGTH = 20;
	
	public static final char SEPARATOR_CHAR = '.';
	
	public static final String SEPARATOR = SEPARATOR_CHAR+"";
	
	@XmlAttribute(name="uid")
	@JsonProperty
	private final String uid;

	@XmlTransient
	private String debugInfo;
	
	private static final SecureRandom random = RigidUtils.getSecureRandom();
	
	// UID for null
	public static final UID UID_NULL = new UID("null");

	public UID(String uid) {
		super();
		if (uid == null) {
			throw new NuclosFatalException("uid must not be null");
		}
		this.uid = uid;
	}
	
	public UID() {
		this(generateString(LENGTH));
	}

	public String getString() {
		return uid;
	}
	
	public static String getStringifiedUIDPrefix(String uid) {
		String tmp_uid = uid;
		
		if (tmp_uid == null) {
			return null;
		} else if (isStringifiedUID(tmp_uid)) {
			tmp_uid = tmp_uid.substring(4, tmp_uid.length()-1);
			if(tmp_uid.indexOf('.') >= 0) {
				return tmp_uid.substring(0, tmp_uid.indexOf('.'));
			}
		}
		return null;
	}
	
	public static boolean isStringifiedUID(String uid) {
		boolean result = false;
		
		String tmp_uid = uid;
		if (tmp_uid == null) {
			result = false;
		} else {
			if (tmp_uid.startsWith("uid{")) {
				result = true;
				tmp_uid = tmp_uid.substring(4, tmp_uid.length()-1);
			} 
			if (tmp_uid.startsWith("uid{")) {
				throw new IllegalArgumentException(String.format("UID \"%s\" not parsable", tmp_uid));
			}
		}
		return result;
	}
	public static boolean isStringifiedDefinitionWithEntity(String uid) {
		return isStringifiedUID(uid) && uid.contains(SEPARATOR);
	}

	public static UID parseUID(String uid) {
		String tmp_uid = uid;
		
		if (tmp_uid == null) {
			return null;
		}
		
		try {
			if (tmp_uid.startsWith("uid{")) {
				tmp_uid = tmp_uid.substring(4, tmp_uid.length()-1);
			} 
			if (tmp_uid.contains("uid{")) {
				throw new IllegalArgumentException(String.format("UID \"%s\" not parsable", tmp_uid));
			}
			if (tmp_uid.contains(SEPARATOR)) {
				// this is for "entity in uid string" compatibility, e.g. uid{5E8q.DXqG0DYtQdn5ft8jZZQ9}
				tmp_uid = tmp_uid.substring(tmp_uid.indexOf('.')+1);
			}
			return new UID(tmp_uid);
		} catch (Exception ex) {
			throw new NuclosFatalException(String.format("UID \"%s\" not parsable", tmp_uid), ex);
		}
	}

	/**
	 * Tries to find all UIDs in the given String by searching for the Pattern "uid{...}".
	 * A joined UID (consisting of 2 parts, joined by SEPARATOR) is split and only the second part is returned.
	 */
	public static List<UID> parseAllUIDs(String uidString) {
		List<UID> result = new ArrayList<>();

		if (StringUtils.isBlank(uidString)) {
			return result;
		}

		Pattern p = Pattern.compile("uid\\{(?:[a-zA-Z0-9-]+" + Pattern.quote(SEPARATOR) + ")?([a-zA-Z0-9-]+)\\}");
		Matcher m = p.matcher(uidString);

		while(m.find()) {
			String uidMatch = m.group(1);
			try {
				UID uid = UID.parseUID(uidMatch);
				result.add(uid);
			}
			catch (NuclosFatalException ex) {
			}
		}

		return result;
	}

	public static String generateString(int length) {
		return generateStringUsingRandom(random, length);
	}

	public static String generateStringUsingRandom(SecureRandom parRandom, int length) {
		String result = "";
		while (result.length() < length) {
			byte[] binaryData = new byte[length];
			parRandom.nextBytes(binaryData);
			String uidData = Base64.encodeBase64String(binaryData).replace("+", "").replace("/", "").replace("=", "");
			result += uidData;
		}
		if (result.length() > length) {
			result = result.substring(0, length);
		}
		return result;
	}
	
	public void setDebugInfo(String debugInfo) {
		this.debugInfo = debugInfo;
	}
	
	/**
	 * Return the stringified definition of this uid.
	 */
	public String getStringifiedDefinition() {
		return "uid{".concat(uid).concat("}");
	}
	
	/**
	 * Return the stringified definition with entity of this uid.
	 */
	public String getStringifiedDefinitionWithEntity(EntityMeta<?> entity) {
		return getStringifiedDefinitionWithEntity(entity.getUID());
	}
	
	/**
	 * Return the stringified definition with entity of this uid.
	 */
	public String getStringifiedDefinitionWithEntity(UID entityUID) {
		return "uid{".concat(getBaseForStringifiedDefinitionWithEntity(entityUID)).concat("}");
	}
	
	/**
	 * Return base string, without "uid{...}", only "entityUID.thisUID"
	 */
	public String getBaseForStringifiedDefinitionWithEntity(UID entityUID) {
		return getPrefixForStringifiedDefinitionWithEntity(entityUID).concat(uid);
	}
	
	/**
	 * Return prefix only e.g. "WXYZ."
	 */
	public static String getPrefixForStringifiedDefinitionWithEntity(UID entityUID) {
		return entityUID.getString().concat(SEPARATOR);
	}
	
	@Override
	public int hashCode() {
		return uid == null ? super.hashCode() : uid.hashCode();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		} else if (that instanceof UID) {
			return uid.equals(((UID)that).uid);
		}
		return false;
	}
	
	@Override
	public int compareTo(UID that) {
		return getString().compareTo(that.getString());
	}

	@Override
	public String toString() {
		return uid;
	}
	
	public String debugString() {
		String sUID = uid;
		if (sUID.length() > 4) {
			sUID = "*" + sUID.substring(sUID.length()-4);
		}
		if (debugInfo != null) {
			StringBuffer result = new StringBuffer();
			result.append("~");
			result.append(debugInfo);
			result.append("(");
			result.append(sUID);
			result.append(")");
			return result.toString();
		}
		return "~"+sUID;
	}

	/*public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.println(generateString(4));
		}
		System.out.println(new UID(generateString(4)).getStringifiedDefinitionWithEntity(RigidE.NUCLET));
	}*/
	

	public static class Comparator implements java.util.Comparator<UID> {

		@Override
		public int compare(UID o1, UID o2) {
			if (RigidUtils.equal(o1, o2)) {
				return 0;
			} else {
				if (o1 != null) {
					if (o2 != null) {
						return o1.getString().compareTo(o2.getString());
					} else {
						return 1;
					}
				} else {
					return -1;
				}
			}
		}
		
	}

	public static class UIDJsonSerializer extends StdSerializer<org.nuclos.api.UID> {
		private static final long serialVersionUID = 8175792999849580654L;
		private static final String NULL_KEY_VALUE = "UIDJsonNull";
		private final boolean bKeySerializer;
		public UIDJsonSerializer(boolean bKeySerializer) {
			super(org.nuclos.api.UID.class);
			this.bKeySerializer = bKeySerializer;
		}
		@Override
		public void serialize(final org.nuclos.api.UID uid, final com.fasterxml.jackson.core.JsonGenerator jgen, final SerializerProvider prov) throws IOException {
			if (bKeySerializer) {
				if (uid == null) {
					jgen.writeFieldName(NULL_KEY_VALUE);
				} else {
					jgen.writeFieldName(((UID) uid).getString());
				}
			} else {
				jgen.writeString(((UID) uid).getString());
			}
		}
	}

	public static class UIDJsonDeserializer extends StdDeserializer<org.nuclos.api.UID> {
		private static final long serialVersionUID = -1823894328314560862L;
		public UIDJsonDeserializer() {
			super(org.nuclos.api.UID.class);
		}
		@Override
		public org.nuclos.api.UID deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
			return new UID(jpar.getText());
		}
	}

	public static class UIDKeyDeserializer extends KeyDeserializer {
		@Override
		public Object deserializeKey(final String s, final DeserializationContext ctx) throws IOException {
			if (UIDJsonSerializer.NULL_KEY_VALUE.equals(s)) {
				return null;
			}
			return new UID(s);
		}
	}

	public static class UIDImplJsonDeserializer extends StdDeserializer<UID> {
		private static final long serialVersionUID = -7923244086853514846L;
		public UIDImplJsonDeserializer() {
			super(org.nuclos.api.UID.class);
		}
		@Override
		public UID deserialize(final JsonParser jpar, final DeserializationContext ctx) throws IOException, JsonProcessingException {
			return new UID(jpar.getText());
		}
	}

}
