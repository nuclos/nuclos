package org.nuclos.common;

public enum UnlockMode {
	API_ONLY,
	ALL_USERS_MANUALLY
}
