//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collection;

import java.io.Serializable;
import java.util.Optional;

import org.nuclos.common.RigidUtils;
import org.nuclos.remoting.TypePreservingObjectWrapper;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A generic Pair class.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version 01.00.00
 */
@SuppressWarnings("unchecked")
public class Pair<X, Y> implements Serializable {

	private static final long serialVersionUID = 1461169617313551192L;

	/**
	 * only for backwards compatibility during deserialization
	 */
	@JsonIgnore
	private X x;
	/**
	 * only for backwards compatibility during deserialization
	 */
	@JsonIgnore
	private Y y;

	private TypePreservingObjectWrapper xWrapped;
	private TypePreservingObjectWrapper yWrapped;
	
	public static <X, Y> Pair<X, Y> makePair(X x, Y y) {
		return new Pair<X, Y>(x, y);
	}

	public Pair() {
		this(null, null);
	}

	public Pair(X x, Y y) {
		setX(x);
		setY(y);
	}

	public X getX() {
		if (this.x != null) {
			this.xWrapped = new TypePreservingObjectWrapper(this.x);
			this.x = null;
		}
		return (X) Optional.ofNullable(xWrapped).map(TypePreservingObjectWrapper::get).orElse(null);
	}

	public void setX(X x) {
		this.x = null;
		this.xWrapped = new TypePreservingObjectWrapper(x);
	}

	public Y getY() {
		if (this.y != null) {
			this.yWrapped = new TypePreservingObjectWrapper(this.y);
			this.y = null;
		}
		return (Y) Optional.ofNullable(yWrapped).map(TypePreservingObjectWrapper::get).orElse(null);
	}

	public void setY(Y y) {
		this.y = null;
		this.yWrapped = new TypePreservingObjectWrapper(y);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Pair) {
			Pair<?, ?> that = (Pair<?, ?>)o;
			return RigidUtils.equal(getX(), that.getX()) && RigidUtils.equal(getY(), that.getY());
		}
		
		return false;
	}

	@Override
	public int hashCode() {
		return RigidUtils.hashCode(getX()) ^ RigidUtils.hashCode(getY());
	}

	@Override
	public String toString() {
		return "Pair(" + getX() + ", " + getY() + ")";
	}

}	// class Pair
