//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import java.text.Collator;
import java.util.Locale;

public interface CollatorProvider {

	/**
	 * @return the default <code>Collator</code> for this platform, that is the default <code>Collator</code>
	 * for the default <code>Locale</code>. For international applications, this is the collation of choice.
	 * Note that this is locale dependent.
	 */
	Collator getDefaultCollator();

	public static Collator newDefaultCollatorForLocale(Locale locale) {
		if (locale == null) {
			locale = Locale.getDefault();
			// This is ONLY acceptable in an TEST environment!
		}
		Collator result = Collator.getInstance(locale);
		result.setStrength(Collator.SECONDARY);// a == A, a < Ä
		return result;
	}

}
