//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.exception.CommonFatalException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ContentHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.LexicalHandler;

/**
 * XML utilities.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version	01.00.00
 */
public class XMLUtils {

	public static final Charset DEFAULT_LAYOUTML = Charset.forName("ISO-8859-15");

	private static final Logger LOG = Logger.getLogger(XMLUtils.class);

	private XMLUtils() {
	}

	/**
	 * gets the character encoding from the String representation of an XML file.
	 * 
	 * §postcondition result != null
	 * 
	 * @param sXml
	 * @return the encoding specified in the String, or "UTF-8" (default encoding for XML files) if none is specified.
	 * 
	 * @deprecated Use {@link org.nuclos.common2.IOUtils.guessXmlEncoding(String)}.
	 */
	public static String getXMLEncoding(String sXml) {
		final int iEncodingStart = sXml.indexOf("encoding=");
		String result = null;

		if (iEncodingStart > 0) {
			final String encbuf = sXml.substring(iEncodingStart);
			final StringTokenizer tokenizer = new StringTokenizer(encbuf, "\"'", true);
			boolean encfound = false;
			while (tokenizer.hasMoreTokens()) {
				sXml = tokenizer.nextToken();
				if (sXml.equals("'") || sXml.equals("\"")) {
					encfound = true;
				}
				else {
					if (encfound) {
						result = sXml;
						break;
					}
				}
			}
		}
		if (result == null) {
			result = "UTF-8";
		}
		assert result != null;
		return result;
	}

   public static List<Element> getSubElements(Node nd) {
      LinkedList<Element> res = new LinkedList<Element>();
      NodeList crp = nd.getChildNodes();
      for(int i = 0, n = crp.getLength(); i < n; i++)
         if(crp.item(i) instanceof Element)
            res.add((Element) crp.item(i));
      return res;
   }

   public static List<Element> getSubElements(Node nd, final String type) {
      return RigidUtils.applyFilter(getSubElements(nd), new Predicate<Element>() {
         @Override
		public boolean evaluate(Element row) {
            return getTagTransformer.transform(row).equals(type);
         }});
   }

   public static String getAttribute(Element elem, String name, String def) {
   	return elem.hasAttribute(name) ? elem.getAttribute(name) : def;
   }

   public static Transformer<Element, String> getTagTransformer() {
   	return getTagTransformer;
   }

   private static final Transformer<Element, String> getTagTransformer = new Transformer<Element, String>() {
		@Override
		public String transform(Element i) {
			return i.getTagName();
		}};

	public static XMLReader newXMLReader() throws SAXException {
		// bei Problemen: return new org.apache.xerces.parsers.SAXParser();
		final SAXParser saxParser;
		try {
			saxParser = newSecuredSaxParserFactory().newSAXParser();
		}
		catch (ParserConfigurationException e) {
			throw new CommonFatalException(e);
		}
		return saxParser.getXMLReader();
	}
	
	private static void parse(InputSource src, ContentHandler chandler, LexicalHandler lhandler) throws SAXException {
		try {
			final XMLReader reader = newXMLReader();
			reader.setContentHandler(chandler);
			if (lhandler != null) {
				reader.setProperty("http://xml.org/sax/properties/lexical-handler", lhandler);
			}
			final DefaultErrorHandler ehandler = new DefaultErrorHandler(null, null);
			reader.setErrorHandler(ehandler);
			reader.parse(src);
			ehandler.throwFirst();
		}
		catch (IOException e) {
			throw new CommonFatalException(e);
		}
	}
	
	public static void parse(String xmlContent, ContentHandler chandler, LexicalHandler lhandler) throws SAXException {
		parse(newDocTypeAwareXmlInput(xmlContent), chandler, lhandler);
	}

	public static InputSource newDocTypeAwareXmlInput(InputStream inputStream) {
		return newDocTypeAwareXmlInput(new BufferedReader(
				new InputStreamReader(inputStream, StandardCharsets.UTF_8)));
	}

	public static InputSource newDocTypeAwareXmlInput(BufferedReader reader) {
		return newDocTypeAwareXmlInput(reader.lines()
				.collect(Collectors.joining("\n")));
	}

	public static InputSource newDocTypeAwareXmlInput(String xmlContent) {
		return new InputSource(new StringReader(xmlContent.replaceAll("<!DOCTYPE((.|\n|\r)*?)\">", "")));
	}

	public static SAXParserFactory newSecuredSaxParserFactory() throws SAXException, ParserConfigurationException {
		return RigidUtils.newSecuredSaxParserFactory();
	}

	public static XMLStreamReader newXmlStreamReader(InputStream inputStream) throws XMLStreamException {
		return newSecuredXMLInputFactory().createXMLStreamReader(inputStream);
	}

	public static XMLEventReader newXmlEventReader(InputStream inputStream) throws XMLStreamException {
		return newSecuredXMLInputFactory().createXMLEventReader(inputStream);
	}

	public static XMLInputFactory newSecuredXMLInputFactory() {
		return RigidUtils.newSecuredXMLInputFactory();
	}

	public static DocumentBuilder newSecuredDocumentBuilder() throws ParserConfigurationException {
		return newSecuredDocumentBuilderFactory().newDocumentBuilder();
	}

	public static DocumentBuilderFactory newSecuredDocumentBuilderFactory() throws ParserConfigurationException {
		return RigidUtils.newSecuredDocumentBuilderFactory();
	}

	public static Document newDocument(InputStream inputStream) throws ParserConfigurationException, IOException, SAXException {
		return newSecuredDocumentBuilder().parse(newDocTypeAwareXmlInput(inputStream));
	}

	public static Document newDocument(String xmlContent) throws ParserConfigurationException, IOException, SAXException {
		return newSecuredDocumentBuilder().parse(newDocTypeAwareXmlInput(xmlContent));
	}
	
	public static class DefaultErrorHandler implements ErrorHandler {
		
		private final String resource;
		
		private final Logger log;
		
		private SAXException first;
		
		public DefaultErrorHandler(String resource, Logger log) {
			this.resource = resource;
			if (log == null) {
				this.log = LOG;
			}
			else {
				this.log = LOG;
			}
		}
		
		private String toString(SAXParseException e) {
			final StringBuilder result = new StringBuilder();
			if (resource != null) {
				result.append("in '").append(resource).append("' ");
			}
			if (e.getPublicId() != null) {
				result.append("pId=").append(e.getPublicId()).append(" ");
			}
			if (e.getSystemId() != null) {
				result.append("sId=").append(e.getSystemId()).append(" ");
			}
			result.append("at (").append(e.getLineNumber()).append(",").append(e.getColumnNumber()).append(") ");
			result.append(": ").append(e.getMessage());
			if (e.getCause() != null) {
				result.append(", caused by: ").append(e.getCause());
			}
			return result.toString();
		}

		@Override
		public void warning(SAXParseException exception) throws SAXException {
			log.warn(toString(exception));
		}

		@Override
		public void error(SAXParseException exception) throws SAXException {
			if (first == null) {
				first = exception;
			}
			log.error(toString(exception));
		}

		@Override
		public void fatalError(SAXParseException exception) throws SAXException {
			if (first == null) {
				first = exception;
			}
			log.fatal(toString(exception));
		}
		
		public void throwFirst() throws SAXException {
			if (first != null) {
				throw first;
			}
		}
		
	}
	
}  // class XMLUtils
