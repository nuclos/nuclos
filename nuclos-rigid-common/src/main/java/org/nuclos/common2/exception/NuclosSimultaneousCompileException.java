package org.nuclos.common2.exception;

/**
 * Thrown if a compilation process is already running in the background
 */
public class NuclosSimultaneousCompileException extends NuclosCompileException {
	public NuclosSimultaneousCompileException() {
		super("Simultaneous Nuclos rule compilation prevented");
	}
}
