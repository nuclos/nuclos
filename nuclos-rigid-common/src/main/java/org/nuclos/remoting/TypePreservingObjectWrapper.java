//Copyright (C) 2022  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.remoting;

import java.io.Serializable;
import java.util.Objects;

public class TypePreservingObjectWrapper implements Serializable {

	private static final long serialVersionUID = -8690133367148843136L;

	private final Object o;

	public TypePreservingObjectWrapper(final Object o) {
		this.o = o;
	}

	public Object get() {
		return this.o;
	}

	public <T> T get(final Class<T> cls) {
		return (T) this.o;
	}

	@Override
	public boolean equals(final Object o1) {
		if (this == o1) return true;
		if (o1 == null || getClass() != o1.getClass()) return false;
		final TypePreservingObjectWrapper that = (TypePreservingObjectWrapper) o1;
		return o.equals(that.o);
	}

	@Override
	public int hashCode() {
		return Objects.hash(o);
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("TypePreservingObjectWrapper{");
		sb.append('}');
		return sb.toString();
	}

}
