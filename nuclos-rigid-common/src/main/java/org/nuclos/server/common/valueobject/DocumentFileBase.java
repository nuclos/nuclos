//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.valueobject;

import org.apache.log4j.Logger;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common2.File;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * DocumentFile that loads its contents lazily.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:uwe.allner@novabit.de">uwe.allner</a>
 * @version 01.00.00
 */
public abstract class DocumentFileBase extends File {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8664312972120605617L;

	private final static Logger log = Logger.getLogger(DocumentFileBase.class);

	private String documentFileString;
	private org.nuclos.api.UID documentFileUid;
	private Long documentFileLong;
	private boolean bContentsChanged;

	/**
	 * for deserialization only
	 */
	protected DocumentFileBase() {
		super();
	}

	/**
	 * §precondition iDocumentFileId != null
	 * §postcondition this.contents == null
	 * §postcondition !this.getContentsChanged()
	 */
	public DocumentFileBase(String sFileName, Object documentFilePk) {
		super(sFileName);
		setDocumentFilePk(documentFilePk);
		this.bContentsChanged = false;

		assert this.contents == null;
		assert !this.getContentsChanged();
	}
	
	public DocumentFileBase(String sFileName, Object documentFilePk, byte[] abContents) {
		super(sFileName);
		setDocumentFilePk(documentFilePk);
		
		this.contents = abContents;
		this.bContentsChanged = true;
	}
	/**
	 * §precondition abContents != null
	 * §postcondition this.getContentsChanged()
	 */
	public DocumentFileBase(String sFileName, byte[] abContents) {
		super(sFileName, abContents);

		this.bContentsChanged = true;

		assert this.getContentsChanged();
	}

	public Object getDocumentFilePk() {
		if (this.documentFileString != null) {
			return this.documentFileString;
		}
		if (this.documentFileUid != null) {
			return this.documentFileUid;
		}
		if (this.documentFileLong != null) {
			return this.documentFileLong;
		}
		return null;
	}
	
	public void setDocumentFilePk(Object documentFilePk) {
		this.documentFileString = null;
		this.documentFileUid = null;
		this.documentFileLong = null;
		if (documentFilePk != null) {
			if (documentFilePk instanceof String) {
				this.documentFileString = (String) documentFilePk;
			} else
			if (documentFilePk instanceof org.nuclos.api.UID) {
				this.documentFileUid = (org.nuclos.api.UID) documentFilePk;
			} else
			if (documentFilePk instanceof Long) {
				this.documentFileLong = (Long) documentFilePk;
			}
		}
	}
	
	@Override
	public final byte[] getContents() {
		if (this.contents == null) {
			if (this.getDocumentFilePk() != null) {
				log.debug("BEGIN getting stored contents.");
				this.contents = this.getStoredContents();
				log.debug("FINISHED getting stored contents.");
			}
		}
		return contents;
	}

	/**
	 * gets the contents of this file from the (remote) storage.
	 * Note that this is generally not the local file system.
	 * @return the loaded contents.
	 */
	protected abstract byte[] getStoredContents();

	/**
	 * @return Have the contents changed? Must this file be stored?
	 */
	public boolean getContentsChanged() {
		return this.bContentsChanged;
	}
	
	public void setContentsStored() {
		this.bContentsChanged = false;
	}

	@Override
	public boolean equals(Object o) {
		final boolean result;
		if (!(o instanceof DocumentFileBase)) {
			result = super.equals(o);
		}
		else {
			if (this == o) {
				result = true;
			}
			else {
				final DocumentFileBase that = (DocumentFileBase) o;
				result = RigidUtils.equal(this.getFilename(), that.getFilename()) &&
						RigidUtils.equal(this.getFiletype(), that.getFiletype()) &&
						this.areContentsEqual(that);
			}
		}
		return result;
	}

	/**
	 * @param that
	 * @return Are the contents of this and that equal? true, if both haven't been fetched from the (remote) storage,
	 * Otherwise, the contents are compared on a byte-per-byte basis.
	 */
	private boolean areContentsEqual(DocumentFileBase that) {
		return (this.contents == null && that.contents == null) ?
				true : org.apache.commons.lang.ArrayUtils.isEquals(this.getContents(), that.getContents());
	}
	
	public static UID newFileUID() {
		// Cause of file system and LiveSearch issues use only lowercase UID: 
		return new UID(new UID().getString().toLowerCase());
	}

	public static UID newFileUID(int length) {
		// Cause of file system and LiveSearch issues use only lowercase UID:
		return new UID(UID.generateString(length).toLowerCase());
	}

}	// class DocumentFileBase
