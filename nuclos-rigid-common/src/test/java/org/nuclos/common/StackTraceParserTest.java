package org.nuclos.common;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class StackTraceParserTest {
	private StackTraceParser parser;
	private Throwable throwable;

	@Before
	public void initParser() {
		recurse(6, () -> {
			throwable = new NuclosFatalException(
					new IllegalArgumentException(
							new NullPointerException("Test")
					)
			);
			parser = new StackTraceParser(throwable);
		});
	}

	private void recurse(final int n, Runnable callback) {
		if (n == 0) {
			callback.run();
		} else {
			recurse(n - 1, callback);
		}
	}

	@Test
	public void nullThrowable() {
		StackTraceParser parser = new StackTraceParser((Throwable) null);
		assert parser.getStackTraceSegments().size() == 0;
		assert parser.getShortenedStackFrames().size() == 0;
	}

	@Test
	public void nullStackTraceElements() {
		StackTraceParser parser = new StackTraceParser((List<StackTraceElement>) null);
		assert parser.getStackTraceSegments().size() == 0;
		assert parser.getShortenedStackFrames().size() == 0;
	}

	@Test
	public void segments() {
		List<List<String>> segments = parser.getStackTraceSegments();
		assert segments.size() == 3;
	}

	@Test
	public void shortenStackTrace() {
		List<String> stackFrames = parser.getShortenedStackFrames();

		assert stackFrames.size() > 0;
	}

	@Test
	public void findEnd() {
		List<List<String>> segments = parser.getStackTraceSegments();

		// The Lambda functions result in 2 StackFrames per function on some systems,
		// on others only in 1 frame per function...
		// Therefore we can not check the exact number here, but it must be at least 9 matching frames.
		assert parser.findEnd(segments.get(0)) >= 9;
	}

	@Test
	public void findOmittedCount() {
		List<List<String>> segments = parser.getStackTraceSegments();

		assert parser.findOmittedCount(segments.get(1)) > 0;
		assert parser.findOmittedCount(segments.get(2)) > 0;
	}

	@Test
	public void fullStackFrames() {
		List<String> fromParser = parser.getStackFrames();
		String[] fromApache = ExceptionUtils.getStackFrames(throwable);

		assert fromParser.equals(Arrays.asList(fromApache));
	}

	@Test
	public void stackTraceElementList() {
		StackTraceParser parser = new StackTraceParser(Arrays.asList(Thread.currentThread().getStackTrace()));
		List<String> shortenedStackFrames = parser.getShortenedStackFrames();

		assert shortenedStackFrames.size() == 6;
	}
}