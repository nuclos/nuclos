package org.nuclos.common;

import java.util.Date;
import java.util.Objects;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class VersionTest {
	@Test
	public void testVersionNumber() {
		Version v = new Version("nuclos", "Nuclos ERP", "4.11.0-SNAPSHOT", new Date().toString());
		assert v.isSnapshot();

		String versionString = v.getVersionNumber();
		assert "4.11.0-SNAPSHOT".equals(versionString);

		v = new Version("nuclos", "Nuclos ERP", "4.11.0", new Date().toString());
		assert !v.isSnapshot();

		versionString = v.getVersionNumber();

		assert "4.11.0".equals(versionString);
	}

	@Test
	public void testNew2022VersionNumber() {
		// Schreibweisen Tests:
		// Abwärtskompatibilität
		Version vAlt = new Version("nuclos", "nuclos", "4.50.0-SNAPSHOT", "2022-01-01");
		assert "4.50.0-SNAPSHOT".equals(vAlt.getVersionNumber());
		assert "4.50.0 BETA".equals(vAlt.getSimpleVersionNumber());
		assert "4.50.0".equals(vAlt.getMajorMinorVersionNumber());
		// Neue 2022er
		Version vNeu = new Version("nuclos", "nuclos", "4.2022.1", "2022-01-01");
		assert "4.2022.1".equals(vNeu.getVersionNumber());
		assert "4.2022.1".equals(vNeu.getSimpleVersionNumber());
		assert "4.2022.1".equals(vNeu.getMajorMinorVersionNumber());
		Version vNeuMitBugfix = new Version("nuclos", "nuclos", "4.2022.1.1-SNAPSHOT", "2022-01-01");
		assert "4.2022.1.1-SNAPSHOT".equals(vNeuMitBugfix.getVersionNumber());
		assert "4.2022.1.1 BETA".equals(vNeuMitBugfix.getSimpleVersionNumber());
		assert "4.2022.1.1".equals(vNeuMitBugfix.getMajorMinorVersionNumber());
		Version vNeu2 = new Version("nuclos", "nuclos", "4.2022.2", "2022-01-01");
		assert "4.2022.2".equals(vNeu2.getVersionNumber());
		assert "4.2022.2".equals(vNeu2.getSimpleVersionNumber());
		assert "4.2022.2".equals(vNeu2.getMajorMinorVersionNumber());

		// Versionsvergleich Tests:
		// Abwärtskompatibilität
		assert vAlt.compareTo(vNeu) < 0 : "Alt vs Neu muss kleiner sein";
		assert vAlt.compareTo(vNeuMitBugfix) < 0 : "Alt vs NeuMitBugfix muss kleiner sein";
		// Neue 2022er
		assert vNeuMitBugfix.compareTo(vNeu) > 0 : "NeuMitBugfix vs Neu muss größer sein";
		assert vNeu2.compareTo(vNeuMitBugfix) > 0 : "Neu2 vs NeuMitBugfix muss größer sein";
		assert VersionNumber.compare("4.2022.1.0122", "4.2022.1.2.0122") < 0 : "Patch Fix für neue Version Schema muss größer sein";
		assert VersionNumber.compare("4.50.3.0120", "4.2022.1.2.0122") < 0 : "Patch Fix für neue Version Schema muss größer sein";
	}
}
