package org.nuclos.server.autosync;

/**
 * A NotSupportedException is thrown when a migration tries 
 * to call a helper method, but the currently 
 * executing helper does not support it.
 */
public class NotSupportedException extends Exception {

	public NotSupportedException(String method, String helper) {
		super(String.format("Method %s is not supported from %s", method, helper));
	}
	
}
