package org.nuclos.server.autosync.migration.m_04_00_00;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.nuclos.common.RigidUtils;
/**
 * @author Oliver Brausch
 */

//Version
@SuppressWarnings("serial")
class RP_PlanElementOld implements Serializable {
	
	final public static int ENTRY = 1;
	final public static int MILESTONE = 2;
	final public static int RELATION = 3;
	
	final public String[] types = new String[]{
			"None", "Buchung", "Meilenstein", "Verbindung" 
	};
	
	private int type;
	private String entity;
	private String primaryField;
	private String secondaryField;
	
	private String dateFromField;
	private String dateUntilField;
	private String timePeriodsString;
	private String timeFromField;
	private String timeUntilField;

	private String labelText;
	private String toolTipText;
	
	private String scriptingEntryCellMethod;
	
	private int presentation;
	private int fromPresentation;
	private int toPresentation;
	private boolean newRelationFromController;
	
	private String color;
	private String fromIcon;
	private String toIcon;
	
	private int fixedExtend;
	private int order;
	private boolean isCascade;

	private List<RP_PlanElementLocaleOld> planElementLocale;
	
	public boolean isMileStone() {
		return type == MILESTONE;
	}
	
	public boolean hasTime() {
		return timeFromField != null && timeUntilField != null;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RP_PlanElementOld) {
			return hashCode() == obj.hashCode();
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return RigidUtils.hashCode(entity) ^ RigidUtils.hashCode(primaryField) ^ RigidUtils.hashCode(type) 
				^ RigidUtils.hashCode(primaryField) ^ RigidUtils.hashCode(secondaryField);
	}

	@XmlElement(name="entity")
	public String getEntity() {
		return entity;
	}

	public void setEntity(String eEntity) {
		this.entity = eEntity;
	}

	@XmlElement(name="primaryField")
	public String getPrimaryField() {
		return primaryField;
	}

	public void setPrimaryField(String primaryField) {
		this.primaryField = primaryField;
	}
	
	@XmlElement(name="secondaryField")
	public String getSecondaryField() {
		return secondaryField;
	}

	public void setSecondaryField(String secondaryField) {
		this.secondaryField = secondaryField;
	}

	@XmlElement(name="dateFromField")
	public String getDateFromField() {
		return dateFromField;
	}

	public void setDateFromField(String dateFromField) {
		this.dateFromField = dateFromField;
	}

	@XmlElement(name="dateUntilField")
	public String getDateUntilField() {
		return dateUntilField;
	}

	public void setDateUntilField(String dateUntilField) {
		this.dateUntilField = dateUntilField;
	}

	@XmlElement(name="timePeriodsString")
	public String getTimePeriodsString() {
		return timePeriodsString;
	}

	public void setTimePeriodsString(String timePeriodsString) {
		this.timePeriodsString = timePeriodsString;
	}

	@XmlElement(name="timeFromField")
	public String getTimeFromField() {
		return timeFromField;
	}

	public void setTimeFromField(String timeFromField) {
		this.timeFromField = timeFromField;
	}

	@XmlElement(name="timeUntilField")
	public String getTimeUntilField() {
		return timeUntilField;
	}

	public void setTimeUntilField(String timeUntilField) {
		this.timeUntilField = timeUntilField;
	}

	@XmlElement(name="labelText")
	public String getLabelText() {
		return labelText;
	}

	public void setLabelText(String labelText) {
		this.labelText = labelText;
	}

	@XmlElement(name="toolTipText")
	public String getToolTipText() {
		return toolTipText;
	}

	public void setToolTipText(String toolTipText) {
		this.toolTipText = toolTipText;
	}

	@XmlElement(name="scriptingEntryCellMethod")
	public String getScriptingEntryCellMethod() {
		return scriptingEntryCellMethod;
	}

	public void setScriptingEntryCellMethod(String scriptingEntryCellMethod) {
		this.scriptingEntryCellMethod = scriptingEntryCellMethod;
	}

	@XmlElement(name="type")
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@XmlElement(name="presentation")
	public int getPresentation() {
		return presentation;
	}

	public void setPresentation(int presentation) {
		this.presentation = presentation;
	}

	@XmlElement(name="fromPresentation")
	public int getFromPresentation() {
		return fromPresentation;
	}

	public void setFromPresentation(int fromPresentation) {
		this.fromPresentation = fromPresentation;
	}

	@XmlElement(name="toPresentation")
	public int getToPresentation() {
		return toPresentation;
	}

	public void setToPresentation(int toPresentation) {
		this.toPresentation = toPresentation;
	}

	@XmlElement(name="newRelationFromController")
	public boolean isNewRelationFromController() {
		return newRelationFromController;
	}

	public void setNewRelationFromController(boolean newRelationFromController) {
		this.newRelationFromController = newRelationFromController;
	}

	@XmlElement(name="color")
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@XmlElement(name="fromIcon")
	public String getFromIcon() {
		return fromIcon;
	}

	public void setFromIcon(String fromIcon) {
		this.fromIcon = fromIcon;
	}

	@XmlElement(name="toIcon")
	public String getToIcon() {
		return toIcon;
	}

	public void setToIcon(String toIcon) {
		this.toIcon = toIcon;
	}

	@XmlElement(name="planElementLocale")
	public List<RP_PlanElementLocaleOld> getPlanElementLocaleVO() {
		return planElementLocale;
	}

	public void setPlanElementLocaleVO(List<RP_PlanElementLocaleOld> planElementLocale) {
		this.planElementLocale = planElementLocale;
	}
	
	@XmlElement(name="fixedExtends")
	public int getFixedExtend() {
		return fixedExtend;
	}

	public void setFixedExtend(int fixedExtend) {
		this.fixedExtend = fixedExtend;
	}
	
	@XmlElement(name="order")
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	@XmlElement(name="isCascade")
	public boolean isCascade() {
		return isCascade;
	}

	public void setCascade(boolean isCascade) {
		this.isCascade = isCascade;
	}
	
	public String toString() {
		return types[type] + " (" + entity + ") ";
	}
}