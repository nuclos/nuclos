//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.autosync.migration.m_04_00_00;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.log4j.Logger;

//Version
@SuppressWarnings("serial")
@XmlType
@XmlRootElement(name="resplan")
class RP_ResPlanOld implements Serializable {
	
	private static final Logger LOG = Logger.getLogger(RP_ResPlanOld.class);

	@XmlAttribute(name="version", required=true)
	static final String VERSION = "0.2";

	//Group 1: Single Fields
	private String resourceEntity;
	private String resourceSortField;
	
	private String resourceLabelText;
	private String resourceToolTipText;
	private String cornerLabelText;
	private String cornerToolTipText;

	private String defaultViewFrom;
	private String defaultViewUntil;
	
	private List<RP_LocaleOld> resourceLocales;
	private List<RP_PlanElementOld> planElements;
	
	//Group 2: Entry Entity Fields (1:n to to resource Entity)
	private String entryEntity;
	private String referenceField;
	private String milestoneField;

	private String dateFromField;
	private String dateUntilField;
	private String timePeriodsString;
	private String timeFromField;
	private String timeUntilField;

	private String entryLabelText;
	private String entryToolTipText;

	//Group 3: Relation Entity Fields (1:n to Resource Entity)
	private String relationEntity;
	private String relationFromField;
	private String relationToField;
	
	private int relationPresentation;
	private int relationFromPresentation;
	private int relationToPresentation;
	private boolean newRelationFromController;
	//missing: Icons and Color

	//Group 4: Scripting
	//Group 4a: Single Scripting Fields
	private boolean scriptingActivated;
	private String scriptingCode;
	private String backgroundPaintMethod;
	private String scriptingResourceCellMethod;
	//Group 4b: Entry Entity Scripting Fields (1:n to Resource Entity)
	private String scriptingEntryCellMethod;
	
	//Group 5: Old Resources
	private List<RP_ResourceOld> resources;

	public RP_ResPlanOld() {
	}

	@XmlElement(name="resourceEntity")
	public String getResourceEntity() {
		return resourceEntity;
	}

	public void setResourceEntity(String resourceEntity) {
		this.resourceEntity = resourceEntity;
	}
	
	public void setResourceSortField(String resourceSortField) {
		this.resourceSortField = resourceSortField;
	}

	public String getResourceSortField() {
		return resourceSortField;
	}

	@XmlElement(name="planElements")
	public List<RP_PlanElementOld> getPlanElements() {
		return planElements;
	}

	public void setPlanElements(List<RP_PlanElementOld> entries) {
		this.planElements = entries;
	}

	@XmlElement(name="entryEntity")
	public String getEntryEntity() {
		return entryEntity;
	}

	public void setEntryEntity(String entryEntity) {
		this.entryEntity = entryEntity;
	}

	@XmlElement(name="referenceField")
	public String getReferenceField() {
		return referenceField;
	}

	public void setReferenceField(String referenceField) {
		this.referenceField = referenceField;
	}

	@XmlElement(name="dateFromField")
	public String getDateFromField() {
		return dateFromField;
	}

	public void setDateFromField(String dateFromField) {
		this.dateFromField = dateFromField;
	}

	@XmlElement(name="dateToField")
	public String getDateUntilField() {
		return dateUntilField;
	}

	public void setDateUntilField(String dateUntilField) {
		this.dateUntilField = dateUntilField;
	}

	@XmlElement(name="timeFromField")
	public String getTimeFromField() {
		return timeFromField;
	}

	public void setTimeFromField(String timeFromField) {
		this.timeFromField = timeFromField;
	}

	@XmlElement(name="timeToField")
	public String getTimeUntilField() {
		return timeUntilField;
	}

	public void setTimeUntilField(String timeUntilField) {
		this.timeUntilField = timeUntilField;
	}

	@XmlElement(name="timePerods")
	public String getTimePeriodsString() {
		return timePeriodsString;
	}

	public void setTimePeriodsString(String timePeriodsString) {
		this.timePeriodsString = timePeriodsString;
	}

	@XmlElement(name="resourceLabelText")
	public String getResourceLabelText() {
		return resourceLabelText;
	}

	public void setResourceLabelText(String resourceLabelText) {
		this.resourceLabelText = resourceLabelText;
	}

	@XmlElement(name="resourceToolTipText")
	public String getResourceToolTipText() {
		return resourceToolTipText;
	}

	public void setResourceToolTipText(String resourceToolTipText) {
		this.resourceToolTipText = resourceToolTipText;
	}

	@XmlElement(name="entryLabelText")
	public String getEntryLabelText() {
		return entryLabelText;
	}

	public void setEntryLabelText(String entryLabelText) {
		this.entryLabelText = entryLabelText;
	}

	@XmlElement(name="entryToolTipText")
	public String getEntryToolTipText() {
		return entryToolTipText;
	}

	public void setEntryToolTipText(String entryToolTipText) {
		this.entryToolTipText = entryToolTipText;
	}

	@XmlElement(name="cornerLabelText")
	public String getCornerLabelText() {
		return cornerLabelText;
	}

	public void setCornerLabelText(String cornerLabelText) {
		this.cornerLabelText = cornerLabelText;
	}

	@XmlElement(name="cornerToolTipText")
	public void setCornerToolTipText(String cornerToolTipText) {
		this.cornerToolTipText = cornerToolTipText;
	}

	public String getCornerToolTipText() {
		return cornerToolTipText;
	}

	@XmlElement(name="scripting")
	public boolean isScriptingActivated() {
		return scriptingActivated;
	}

	public void setScriptingActivated(boolean scriptingActivated) {
		this.scriptingActivated = scriptingActivated;
	}

	@XmlElement(name="code")
	public String getScriptingCode() {
		return scriptingCode;
	}

	public void setScriptingCode(String scriptingCode) {
		this.scriptingCode = scriptingCode;
	}

	@XmlElement(name="backgroundPaintMethod")
	public String getScriptingBackgroundPaintMethod() {
		return backgroundPaintMethod;
	}

	public void setScriptingBackgroundPaintMethod(String backgroundPaintCellMethod) {
		this.backgroundPaintMethod = backgroundPaintCellMethod;
	}

	@XmlElement(name="resourceCellMethod")
	public String getScriptingResourceCellMethod() {
		return scriptingResourceCellMethod;
	}

	public void setScriptingResourceCellMethod(String scriptingResourceCellMethod) {
		this.scriptingResourceCellMethod = scriptingResourceCellMethod;
	}

	@XmlElement(name="entryCellMethod")
	public String getScriptingEntryCellMethod() {
		return scriptingEntryCellMethod;
	}

	public void setScriptingEntryCellMethod(String scriptingEntryCellMethod) {
		this.scriptingEntryCellMethod = scriptingEntryCellMethod;
	}

	@XmlElement(name="resourceLocales")
	public List<RP_LocaleOld> getResourceLocales() {
		return resourceLocales;
	}

	public void setResourceLocales(List<RP_LocaleOld> resourceLocales) {
		this.resourceLocales = resourceLocales;
	}
	
	@XmlElement(name="resources")
	public List<RP_ResourceOld> getResources() {
		return resources;
	}

	public void setResources(List<RP_ResourceOld> resources) {
		this.resources = resources;
	}
	
	@XmlElement(name="defaultViewFrom")
	public String getDefaultViewFrom() {
		return defaultViewFrom;
	}

	public void setDefaultViewFrom(String defaultViewFrom) {
		this.defaultViewFrom = defaultViewFrom;
	}

	@XmlElement(name="defaultViewUntil")
	public String getDefaultViewUntil() {
		return defaultViewUntil;
	}

	public void setDefaultViewUntil(String defaultViewUntil) {
		this.defaultViewUntil = defaultViewUntil;
	}

	@XmlElement(name="milestoneField")
	public String getMilestoneField() {
		return milestoneField;
	}

	public void setMilestoneField(String milestoneField) {
		this.milestoneField = milestoneField;
	}

	@XmlElement(name="relationEntity")
	public String getRelationEntity() {
		return relationEntity;
	}

	public void setRelationEntity(String relationEntity) {
		this.relationEntity = relationEntity;
	}

	@XmlElement(name="relationFromField")
	public String getRelationFromField() {
		return relationFromField;
	}

	public void setRelationFromField(String relationFromField) {
		this.relationFromField = relationFromField;
	}

	@XmlElement(name="relationToField")
	public String getRelationToField() {
		return relationToField;
	}

	public void setRelationToField(String relationToField) {
		this.relationToField = relationToField;
	}

	@XmlElement(name="relationPresentation")
	public int getRelationPresentation() {
		return relationPresentation;
	}

	public void setRelationPresentation(int relationPresentation) {
		this.relationPresentation = relationPresentation;
	}

	@XmlElement(name="relationFromPresentation")
	public int getRelationFromPresentation() {
		return relationFromPresentation;
	}

	public void setRelationFromPresentation(int relationFromPresentation) {
		this.relationFromPresentation = relationFromPresentation;
	}

	@XmlElement(name="relationToPresentation")
	public int getRelationToPresentation() {
		return relationToPresentation;
	}

	public void setRelationToPresentation(int relationToPresentation) {
		this.relationToPresentation = relationToPresentation;
	}
	
	@XmlElement(name="newRelationFromController")
	public boolean isNewRelationFromController() {
		return newRelationFromController;
	}

	public void setNewRelationFromController(boolean newRelationFromController) {
		this.newRelationFromController = newRelationFromController;
	}

//	public byte[] toBytes() {
//		ByteArrayOutputStream out = new ByteArrayOutputStream();
//		JAXB.marshal(this, out);
//		return out.toByteArray();
//	}

	public static RP_ResPlanOld fromBytes(byte[] b) {
		ByteArrayInputStream in = new ByteArrayInputStream(b);
		try {
			RP_ResPlanOld result = JAXB.unmarshal(in, RP_ResPlanOld.class);
			return result;
		} catch (OutOfMemoryError e) {
			LOG.error("JAXB unmarshal failed: xml is:\n" + new String(b), e);
			throw e;
		}
	}
}

