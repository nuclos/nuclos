package org.nuclos.server.autosync.migration.m_04_12_00;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.Logger;
import org.nuclos.common.SF;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.server.autosync.ConstraintHelper;
import org.nuclos.server.autosync.IMigrationHelper;
import org.nuclos.server.autosync.IndexHelper;
import org.nuclos.server.autosync.RigidEO;
import org.nuclos.server.autosync.migration.AbstractMigration;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.dblayer.MetaDbEntityWrapper;
import org.nuclos.server.dblayer.MetaDbFieldWrapper;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.statements.DbStructureChange.Type;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType;
import org.nuclos.server.dblayer.structure.DbNamedObject;
import org.nuclos.server.dblayer.structure.DbNullable;

public class Migration_04_12_0003 extends AbstractMigration {
	
	private Logger LOG;
	
	private IMigrationHelper helper;
	
	boolean nullDummyCreated;
	
	public class AlreadyMigratedException extends Exception {

		private static final long serialVersionUID = 1L;

		public AlreadyMigratedException() {
			super("Documentfile migration done already");
		}
		
	}
	
	@Override
	public void migrate() {
		if (getContext().isNucletMigration()) {
			return; // no nuclet migration here
		}
		long start = System.currentTimeMillis();
		LOG = getLogger(Migration_04_12_0003.class);
		LOG.info("Migration started");
		
		helper = getContext().getHelper();
		
		try {
			migrateUserBOs();
		} catch (AlreadyMigratedException e) {
			LOG.info("Done by previous version already. Skipping...");
			return;
		}
		migrateGeneralSearchDocuments();
		migrateImportFiles();
		migrateXMLImportFiles();
		migrateTodoFiles();
		migrateWebserviceFiles();
		
		LOG.info("Migration finished in " + new DecimalFormat("0.###").format((System.currentTimeMillis()-start)/1000d/60d) + " minutes");
	}

	@Override
	public SysEntities getSourceMeta() {
		return E_04_12_0002_source.getThis();
	}

	@Override
	public String getSourceStatics() {
		return "2.7";
	}

	@Override
	public SysEntities getTargetMeta() {
		return E_04_12_0003_target.getThis();
	}

	@Override
	public String getTargetStatics() {
		return "2.7";
	}
	
	@Override
	public String getNecessaryUntilSchemaVersion() {
		return "4.12.0001";
	}

	/**
	 * this migration is also included in the special customer Nuclos 4.8.30 version (branch 4.8-documentfile) and may not run twice...
	 */
	@Override
	public String[] getIgnoringSourceSchemaVersions() {
		return new String[]{"4.08.0016"};
	}
	
	private void migrateGeneralSearchDocuments() {
		LOG.info("migrateGeneralSearchDocuments");
		Collection<RigidEO> generalSearchDocuments = helper.getAll(E_04_12_0002_source.GENERALSEARCHDOCUMENT);
		for (RigidEO eo : generalSearchDocuments) {
			LOG.info("migrateGeneralSearchDocument " + eo.getPrimaryKey());
			String fileName = eo.getValue(E_04_12_0002_source.GENERALSEARCHDOCUMENT.file);
			String additionalPath = eo.getValue(E_04_12_0002_source.GENERALSEARCHDOCUMENT.path);
			Long pk = (Long) eo.getPrimaryKey();
			UID documentUID = DocumentFileBase.newFileUID();
			if (renameFile(additionalPath, fileName, pk, documentUID)) {
				createDocumentFile(fileName, documentUID);
				createDocumentFileLink(documentUID, E_04_12_0002_source.GENERALSEARCHDOCUMENT.documentfile.getUID(), pk);
			} else {
				documentUID = null;
			}
			if (documentUID == null) {
				documentUID = getDummyFileUID();
				LOG.warn("General search document with ID=" + pk + " set to 'NULL'");
			}
			eo.setForeignUID(E_04_12_0002_source.GENERALSEARCHDOCUMENT.documentfile, documentUID);
			helper.update(E_04_12_0002_source.GENERALSEARCHDOCUMENT, eo);
		}
	}
	
	private void migrateImportFiles() {
		LOG.info("migrateImportFiles");
		Collection<RigidEO> importFiles = helper.getAll(E_04_12_0002_source.IMPORTFILE);
		for (RigidEO eo : importFiles) {
			UID pk = (UID) eo.getPrimaryKey();
			LOG.info("migrateImportFile " + pk);
			String fileName = eo.getValue(E_04_12_0002_source.IMPORTFILE.name);
			String logFileName = eo.getValue(E_04_12_0002_source.IMPORTFILE.log);
			UID documentUID = DocumentFileBase.newFileUID();
			UID logFileUID = DocumentFileBase.newFileUID();
			if (renameFile(fileName, pk, documentUID)) {
				createDocumentFile(fileName, documentUID);
				createDocumentFileLink(documentUID, E_04_12_0002_source.IMPORTFILE.documentfile.getUID(), pk);
			} else {
				documentUID = null;
			}
			if (documentUID == null) {
				documentUID = getDummyFileUID();
				LOG.warn("Import file with ID=" + pk + " set to 'NULL'");
			}
			eo.setForeignUID(E_04_12_0002_source.IMPORTFILE.documentfile, documentUID);
			if (renameFile(logFileName, pk, logFileUID)) {
				createDocumentFile(logFileName, logFileUID);
				createDocumentFileLink(logFileUID, E_04_12_0002_source.IMPORTFILE.logfile.getUID(), pk);
			} else {
				logFileUID = null;
			}
			eo.setForeignUID(E_04_12_0002_source.IMPORTFILE.logfile, logFileUID);
			helper.update(E_04_12_0002_source.IMPORTFILE, eo);
		}
	}
	
	private void migrateXMLImportFiles() {
		LOG.info("migrateXMLImportFiles");
		Collection<RigidEO> importFiles = helper.getAll(E_04_12_0002_source.XMLIMPORTFILE);
		for (RigidEO eo : importFiles) {
			UID pk = (UID) eo.getPrimaryKey();
			LOG.info("migrateXMLImportFile " + pk);
			String fileName = eo.getValue(E_04_12_0002_source.XMLIMPORTFILE.name);
			String logFileName = eo.getValue(E_04_12_0002_source.XMLIMPORTFILE.log);
			UID documentUID = DocumentFileBase.newFileUID();
			UID logFileUID = DocumentFileBase.newFileUID();
			if (renameFile(fileName, pk, documentUID)) {
				createDocumentFile(fileName, documentUID);
				createDocumentFileLink(documentUID, E_04_12_0002_source.XMLIMPORTFILE.documentfile.getUID(), pk);
			}
			else {
				documentUID = null;
			}
			if (documentUID == null) {
				documentUID = getDummyFileUID();
				LOG.warn("XML import file with ID=" + pk + " set to 'NULL'");
			}
			eo.setForeignUID(E_04_12_0002_source.XMLIMPORTFILE.documentfile, documentUID);
			if (renameFile(logFileName, pk, logFileUID)) {
				createDocumentFile(logFileName, logFileUID);
				createDocumentFileLink(logFileUID, E_04_12_0002_source.XMLIMPORTFILE.logfile.getUID(), pk);
			} else {
				logFileUID = null;
			}
			eo.setForeignUID(E_04_12_0002_source.XMLIMPORTFILE.logfile, logFileUID);
			helper.update(E_04_12_0002_source.XMLIMPORTFILE, eo);
		}
	}
	
	private void migrateTodoFiles() {
		LOG.info("migrateTodoFiles");
		Collection<RigidEO> todoFiles = helper.getAll(E_04_12_0002_source.TODOFILES);
		for (RigidEO eo : todoFiles) {
			Long pk = (Long) eo.getPrimaryKey();
			LOG.info("migrateTodofile " + pk);
			String fileName = eo.getValue(E_04_12_0002_source.TODOFILES.file);
			UID documentUID = DocumentFileBase.newFileUID();
			if (renameFile(fileName, pk, documentUID)) {
				createDocumentFile(fileName, documentUID);
				createDocumentFileLink(documentUID, E_04_12_0002_source.TODOFILES.documentfile.getUID(), pk);
			}
			else {
				documentUID = null;
			}
			if (documentUID == null) {
				documentUID = getDummyFileUID();
				LOG.warn("Todo file with ID=" + pk + " set to 'NULL'");
			}
			eo.setForeignUID(E_04_12_0002_source.TODOFILES.documentfile, documentUID);
			helper.update(E_04_12_0002_source.TODOFILES, eo);
		}
	}
	
	private void migrateWebserviceFiles() {
		LOG.info("migrateWebserviceFiles");
		Collection<RigidEO> webserviceFiles = helper.getAll(E_04_12_0002_source.WEBSERVICE);
		for (RigidEO eo : webserviceFiles) {
			UID pk = (UID) eo.getPrimaryKey();
			LOG.info("migrateWebserviceFiles " + pk);
			String fileName = eo.getValue(E_04_12_0002_source.WEBSERVICE.wsdl);
			UID documentUID = DocumentFileBase.newFileUID();
			if (renameFile(fileName, pk, documentUID)) {
				createDocumentFile(fileName, documentUID);
				createDocumentFileLink(documentUID, E_04_12_0002_source.WEBSERVICE.wsdlfile.getUID(), pk);
			} else {
				documentUID = null;
			}
			if (documentUID == null) {
				documentUID = getDummyFileUID();
				LOG.warn("Webservice with ID=" + pk + " set to 'NULL'");
			}
			eo.setForeignUID(E_04_12_0002_source.WEBSERVICE.wsdlfile, documentUID);
			helper.update(E_04_12_0002_source.WEBSERVICE, eo);
		}
	}

	private void migrateUserBOs() throws AlreadyMigratedException {
		// for user defined keys and indices:
		Collection<UID> involvedFields = new ArrayList<UID>();
		Collection<UID> virtualOrProxyEntities = new ArrayList<UID>();
		Collection<MetaDbEntityWrapper> allEntities = new ArrayList<MetaDbEntityWrapper>();
		Collection<MetaDbFieldWrapper> allFields = new ArrayList<MetaDbFieldWrapper>();
		for (RigidEO entityEO : helper.getAll(E_04_12_0002_source.ENTITY)) {
			// virtual entity
			if (entityEO.getValue(E_04_12_0002_source.ENTITY.virtualentity) != null 
					|| Boolean.TRUE.equals(entityEO.getValue(E_04_12_0002_source.ENTITY.proxy))) {
				virtualOrProxyEntities.add((UID)entityEO.getPrimaryKey());
			}
			allEntities.add(new MetaDbEntityWrapper(entityEO));
			final MetaDbFieldWrapper pkFieldWrapper = new MetaDbFieldWrapper(SF.PK_ID.getMetaData((UID)entityEO.getPrimaryKey()));
			allFields.add(pkFieldWrapper);
		}
		for (RigidEO f : helper.getAll(E_04_12_0002_source.ENTITYFIELD)) {
			// only non virtual entity fields, or non proxy fields, s.a. NUCLOS-6680
			if (!virtualOrProxyEntities.contains(f.getForeignUID(E_04_12_0002_source.ENTITYFIELD.entity))) {
				allFields.add(new MetaDbFieldWrapper(f));
				String dataType = f.getValue(E_04_12_0002_source.ENTITYFIELD.datatype);			
				if (dataType.equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile")) {
					UID foreignEntity = f.getForeignUID(E_04_12_0002_source.ENTITYFIELD.foreignentity);
					if (foreignEntity != null) {
						throw new AlreadyMigratedException();
					}
					involvedFields.add((UID)f.getPrimaryKey());
				}
			}
		}
		
		// drop constraints and indices
		ConstraintHelper.removeConstraints(false, true, false, involvedFields, allEntities, allFields, E_04_12_0003_target.getThis(), LOG);
		IndexHelper.removeIndices(involvedFields, allEntities, allFields, E_04_12_0002_source.getThis(), LOG);
		for (RigidEO field : helper.getAll(E_04_12_0002_source.ENTITYFIELD)) {
			String dataType = field.getValue(E_04_12_0002_source.ENTITYFIELD.datatype);
			if (dataType.equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile")) {
				boolean isVirtualOrProxy = virtualOrProxyEntities.contains(field.getForeignUID(E_04_12_0002_source.ENTITYFIELD.entity));
				migrateUserBOColumn(field, isVirtualOrProxy);
			}
		}
		
		// refill "meta-cache"
		allEntities.clear();
		allFields.clear();
		for (RigidEO entityEO : helper.getAll(E_04_12_0003_target.ENTITY)) {
			allEntities.add(new MetaDbEntityWrapper(entityEO));
			final MetaDbFieldWrapper pkFieldWrapper = new MetaDbFieldWrapper(SF.PK_ID.getMetaData((UID)entityEO.getPrimaryKey()));
			allFields.add(pkFieldWrapper);
		}
		for (RigidEO f : helper.getAll(E_04_12_0003_target.ENTITYFIELD)) {
			allFields.add(new MetaDbFieldWrapper(f));
		}
		
		// create constraints and indices
		ConstraintHelper.createConstraints(false, true, true, involvedFields, allEntities, allFields, E_04_12_0003_target.getThis(), LOG);
		IndexHelper.createIndices(involvedFields, allEntities, allFields, E_04_12_0003_target.getThis(), LOG);
	}
	
	private void migrateUserBOColumn(RigidEO field, boolean isVirtualOrProxy) {
		RigidEO entity = helper.getByPrimaryKey(E_04_12_0002_source.ENTITY, field.getForeignUID(E_04_12_0002_source.ENTITYFIELD.entity));
		String tableName = entity.getValue(E_04_12_0002_source.ENTITY.dbtable);
//		String fieldName = field.getValue(E_04_12_0002_source.ENTITYFIELD.field);
		String oldDbField = field.getValue(E_04_12_0002_source.ENTITYFIELD.dbfield);
		String newDbField_STRVALUE = getNewDbFieldName(field);
		String newDbField_STRUID = "STRUID_" + (newDbField_STRVALUE.substring("STRVALUE_".length()));
		
		Integer order = field.getValue(E_04_12_0002_source.ENTITYFIELD.order);
		Boolean nullable = field.getValue(E_04_12_0002_source.ENTITYFIELD.nullable);
		LOG.info("migrateUserBOColumn " + oldDbField + " from " + tableName + " into " + newDbField_STRUID);
		
		if (isVirtualOrProxy) {
			LOG.warn("Virtual/Proxy BO column " + entity.getValue(E_04_12_0002_source.ENTITY.virtualentity) + "." +
					oldDbField + " could not be migrated. Refactoring of the underlying view/rule is required!");
		} else {
			// create new column
			DbColumn newColumn = new DbColumn(null, new DbNamedObject(null, tableName), newDbField_STRUID, MetaDbHelper.UID_COLUMN_TYPE, DbNullable.NULL, null, order);
			helper.getDbAccess().execute(new DbStructureChange(DbStructureChange.Type.CREATE, newColumn));
			
			// migrate values 
			StringBuilder buf = new StringBuilder();
			buf.append("SELECT INTID,").append(oldDbField).append(" FROM ").append(tableName);
			ResultVO result = helper.getDbAccess().executePlainQuery(buf.toString(), -1, false);
			for (int i=0;i<result.getRowCount();i++) {
				Object[] row = result.getRows().get(i);
				Object pk = row[0];
				if (pk instanceof Integer) {
					pk = ((Integer)pk).longValue();
				}
				String fileName = (String) row[1];
				if (fileName == null && nullable) {
					continue;
				}
				UID documentUID = DocumentFileBase.newFileUID();
				if (renameFile(fileName, pk, documentUID)) {
					createDocumentFile(fileName, documentUID);
					createDocumentFileLink(documentUID, (UID)field.getPrimaryKey(), pk);
				} else {
					documentUID = null;
				}
				if (documentUID == null) {
					documentUID = getDummyFileUID();
					LOG.warn(newDbField_STRUID + " in " + tableName + " with ID=" + pk + " set to 'NULL'");
				}
				buf = new StringBuilder();
				buf.append("UPDATE ").append(tableName).append(" SET ").append(newDbField_STRUID);
				buf.append("='").append(documentUID).append("' ");
				buf.append("WHERE INTID=").append(pk);
				helper.getDbAccess().executePlainUpdate(buf.toString());
			}
			
			if (!nullable) {
				DbColumn newColumnNotNull = new DbColumn(null, new DbNamedObject(null, tableName), newDbField_STRUID, MetaDbHelper.UID_COLUMN_TYPE, DbNullable.NOT_NULL, null, order);
				helper.getDbAccess().execute(new DbStructureChange(Type.MODIFY, newColumn, newColumnNotNull));
			}
		}
		
		// update Metadata
		field.setForeignUID(E_04_12_0002_source.ENTITYFIELD.foreignentity, E_04_12_0002_source.DOCUMENTFILE.getUID());
		field.setValue(E_04_12_0002_source.ENTITYFIELD.foreignentityfield, E_04_12_0002_source.DOCUMENTFILE.filename.getUID().getStringifiedDefinition());
		field.setValue(E_04_12_0002_source.ENTITYFIELD.indexed, !isVirtualOrProxy);
		field.setValue(E_04_12_0002_source.ENTITYFIELD.dbfield, newDbField_STRVALUE);
		helper.update(E_04_12_0002_source.ENTITYFIELD, field);
		
		if (!isVirtualOrProxy) {
			// drop old column
			DbColumn oldColumn = new DbColumn(null, new DbNamedObject(null, tableName), oldDbField, new DbColumnType(DbGenericType.VARCHAR), DbNullable.NULL, null, order);
			helper.getDbAccess().execute(new DbStructureChange(DbStructureChange.Type.DROP, oldColumn));
		}
	}
	
	private String getNewDbFieldName(RigidEO field) {
		String fieldName = field.getValue(E_04_12_0002_source.ENTITYFIELD.field);
		String result = "STRVALUE_" + fieldName;
		if (result.length() > 30) {
			result = result.substring(0, 30);
		}
		UID entityUID = field.getForeignUID(E_04_12_0002_source.ENTITYFIELD.entity);
		result = validateDbField(entityUID, result);
		if (result == null) {
			result = ((UID)field.getPrimaryKey()).getString();
			if (result.length() > 20 || result.contains("-")) {
				// old UID style e.g. 4sbOjXnq-JY0QgndmzAY-ZAv3yojZcFY-vDnRL3uz-Cb0XpBIgA-UsSQ22Cn
				result = result.replaceAll("-", "");
				if (result.length() > 20) {
					result = result.substring(0, 20);
				}
			}
			result = "STRVALUE_" + result.toUpperCase();
		}
		return result;
	}
	
	private String validateDbField(UID entityUID, String dbField) {
		Collection<RigidEO> allFields = helper.getByField(E_04_12_0002_source.ENTITYFIELD, E_04_12_0002_source.ENTITYFIELD.entity, entityUID);
		for (RigidEO field : allFields) {
			String curDbField = field.getValue(E_04_12_0002_source.ENTITYFIELD.dbfield);
			if (dbField.equalsIgnoreCase(curDbField)) {
				// already in use, find free name
				if (dbField.length() > 28) {
					dbField = dbField.substring(0, 28);
				}
				return findFreeDbField(allFields, dbField, 0);
			}
		}
		return dbField;
	}
	
	private String findFreeDbField(Collection<RigidEO> allFields, String dbFieldPrefix, int count) {
		if (count > 100) {
			return null;
		}
		String testDbField = dbFieldPrefix + count;
		for (RigidEO field : allFields) {
			String curDbField = field.getValue(E_04_12_0002_source.ENTITYFIELD.dbfield);
			if (testDbField.equalsIgnoreCase(curDbField)) {
				String testNext = findFreeDbField(allFields, dbFieldPrefix, count+1);
				return testNext;
			}
		}
		return testDbField;
	}
	
	private UID getDummyFileUID() {
		if (!nullDummyCreated) {
			createDocumentFile("null", UID.UID_NULL);
			nullDummyCreated = true;
		}
		return UID.UID_NULL;
	}

	private void createDocumentFile(String fileName, UID uid) {
		RigidEO eo = new RigidEO(E_04_12_0002_source.DOCUMENTFILE.getUID(), uid);
		eo.setValue(E_04_12_0002_source.DOCUMENTFILE.filename, fileName);
		helper.insert(E_04_12_0002_source.DOCUMENTFILE, eo);
		LOG.info("DocumentFile " + uid + " created.");
	}
	
	private void createDocumentFileLink(UID documentFileUID, UID entityfieldUID, Object pk) {
		UID uid = new UID();
		RigidEO eo = new RigidEO(E_04_12_0002_source.DOCUMENTFILELINK.getUID(), uid);
		eo.setForeignUID(E_04_12_0002_source.DOCUMENTFILELINK.documentfile, documentFileUID);
		eo.setForeignUID(E_04_12_0002_source.DOCUMENTFILELINK.entityfield, entityfieldUID);
		if (pk instanceof Long) {
			eo.setValue(E_04_12_0002_source.DOCUMENTFILELINK.entityobjectid, (Long)pk);
			eo.setForeignUID(E_04_12_0002_source.DOCUMENTFILELINK.entityobjectuid, null);
		} else {
			eo.setForeignUID(E_04_12_0002_source.DOCUMENTFILELINK.entityobjectuid, (UID)pk);
			eo.setValue(E_04_12_0002_source.DOCUMENTFILELINK.entityobjectid, null);
		}
		helper.insert(E_04_12_0002_source.DOCUMENTFILELINK, eo);
		LOG.info("DocumentFile " + uid + " created.");
	}
	
	private boolean renameFile(String fileName, Object goId, UID uid) {
		return renameFile(null, fileName, goId, uid);
	}
	
	private boolean renameFile(String additionalPath, String fileName, Object goId, UID uid) {
		fileName = goId + "." + fileName;
		final java.io.File targetDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH);
		java.io.File sourceDir = targetDir;
		if (additionalPath != null && additionalPath.length() > 0) {
			sourceDir = new java.io.File(sourceDir, additionalPath);
		}
		java.io.File file = new java.io.File(sourceDir, fileName);
		String newFileName = getDocumentFileName(uid, fileName);
		java.io.File renamedFile = new java.io.File(targetDir, newFileName);
		if (file.exists()) {
			file.renameTo(renamedFile);
			LOG.info("File renamed from " + fileName + " to " + newFileName);
			return true;
		}
		else {
			LOG.warn("File " + fileName + " does not exist.");
			return false;
		}
	}
	
	private String getDocumentFileName(UID documentFileUID, String filename) {
		String documentFileName = documentFileUID.toString();
		String fileExtension = org.nuclos.common2.File.getExtension(filename);
		if (fileExtension != null)
			documentFileName += "." + fileExtension;
		return documentFileName;
	}
}
