package org.nuclos.server.autosync.migration.m_04_45_00;

import java.text.DecimalFormat;
import java.util.Arrays;

import org.apache.logging.log4j.Logger;
import org.nuclos.common.SysEntities;
import org.nuclos.server.autosync.IMigrationHelper;
import org.nuclos.server.autosync.migration.AbstractMigration;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbType;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.statements.DbBuildableStatement;
import org.nuclos.server.dblayer.statements.DbPlainStatement;

public class Migration_04_45_0099 extends AbstractMigration {

	private Logger LOG;

	@Override
	public void migrate() {
		if (getContext().isNucletMigration()) {
			return; // no nuclet migration here
		}

		long start = System.currentTimeMillis();
		LOG = getLogger(Migration_04_45_0099.class);
		LOG.info("Migration started");

		deleteDbFunctions();

		LOG.info("Migration finished in " + new DecimalFormat("0.###").format((System.currentTimeMillis() - start) / 1000d / 60d) + " minutes");
	}

	@Override
	public SysEntities getSourceMeta() {
		return E_04_45_0099.getThis();
	}

	@Override
	public String getSourceStatics() {
		return "2.8";
	}

	@Override
	public SysEntities getTargetMeta() {
		return org.nuclos.server.autosync.migration.m_04_45_00.E_04_45_0100.getThis();
	}

	@Override
	public String getTargetStatics() {
		return "2.9";
	}

	@Override
	public String getNecessaryUntilSchemaVersion() {
		return "4.45.0099";
	}

	@Override
	public String[] getIgnoringSourceSchemaVersions() {
		return new String[]{};
	}

	private void deleteDbFunctions() {
		PersistentDbAccess dbAccess = getContext().getDbAccess();
		String sType = "FUNCTION";
		String sArguments = "";
		if (DbType.H2.equals(dbAccess.getDbType())) {
			sType = "ALIAS";
		}
		if (DbType.POSTGRESQL.equals(dbAccess.getDbType())) {
			sArguments = "(text)";
		}

		String[] functions = {"DUMMY_CA","JOB_RESULT_ICON_CA"};

		for (String sFunction : functions) {
			String sStatement = String.format("DROP %s %s%s", sType, sFunction, sArguments);
			try {
				DbBuildableStatement statement = new DbPlainStatement(sStatement);
				dbAccess.execute(statement);
				LOG.info(String.format("Dropped %s \"%s\"", sType, sFunction));
			} catch (DbException e) {
				LOG.warn(String.format("\"%s\" could not be executed", sStatement));
			}
		}
	}

}
