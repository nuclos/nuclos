//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster;

public enum ClusterMessageType {
	DAL_CHANGE_NOTIFICATION(1),
	FREE_DAL_MEMORY(2),
	GENERAL_CACHE_INVALIDATION(3),
	SECURITY_RELATED_CHANGE(4),
	REBUILD(5),
	MAINTENANCE(6),
	SCHEDULE_JOB(7),
	UNSCHEDULE_JOB(8),
	EXECUTE_JOB(9),
	USER_PREFERENCES_RELATED_CHANGE(10),
	SSO_AUTH_RELATED_CHANGE(11);

	public final int order;

	private ClusterMessageType(final int order) {
		this.order = order;
	}
}
