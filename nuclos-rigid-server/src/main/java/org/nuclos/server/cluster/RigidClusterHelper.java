//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster;

import java.net.InetAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.RigidE;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.Version;
import org.nuclos.server.common.ServerProperties;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbId;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbBuildableStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.extension.RigidExtensionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RigidClusterHelper implements ClusterConstants {

	private static final Logger LOG = LoggerFactory.getLogger(RigidClusterHelper.class);

	private static Map<String, String> config;

	// just a few performance improvements
	private static Boolean bClusterEnabled = null;
	private static Boolean bIsMaster = null;
	private static UID nodeId = null;
	private static String balancerProtocol = null;
	private static String balancerHostname = null;
	private static Integer balancerPort = null;
	private static String balancerContext = null;

	private static boolean dbEntryChecked = false;

	private static String clusterStatus = null;
	private static final String CLUSTER_STATUS_LOCK = "LOCKED";

	private static final PersistentDbAccess dbAccess = new PersistentDbAccess(SpringDataBaseHelper.getInstance().getDbAccess());

	public static boolean isClusterEnabled() {
		if (bClusterEnabled == null) {
			bClusterEnabled = StringUtils.equals(getConfig().get("mode"), "true");
		}
		return bClusterEnabled;
	}

	/**
	 *  If clustering is enabled, true is returned if this node is the master;
	 *  if clustering is switched off, it is always true
	 */
	public static boolean isMaster() {
		if (isClusterEnabled()) {
			if (bIsMaster == null) {
				String nodeType = getNodeType();
				if (!TYPE_MASTER.equals(nodeType) &&
						!TYPE_SLAVE.equals(nodeType)) {
					throw new RuntimeException(String.format("Cluster config parameter node.type (server.properties) is not valid '%s'! " +
							"Valid values are '%s' or '%s'.", nodeType, TYPE_MASTER, TYPE_SLAVE));
				}
				bIsMaster = TYPE_MASTER.equals(nodeType);
			}
			return bIsMaster;
		} else {
			// no clustering
			return true;
		}
	}

	public static void writeStatus(String status) {
		if (isClusterEnabled()
				|| checkNodeId() // we also want an entry in the cluster server table, even if clustering is not switched on.
		) {
			writeStatusAsync(status);
		}
	}

	private static void writeStatusAsync(String status) {
		synchronized (CLUSTER_STATUS_LOCK) {
			clusterStatus = status;
		}
		Thread t = new Thread("Cluster status updater") {
			@Override
			public void run() {
				writeStatusToDb();
			}
		};
		t.start();
	}

	private synchronized static void writeStatusToDb() {
		synchronized (CLUSTER_STATUS_LOCK) {
			if (isClusterEnabled()) {
				LOG.info("Update cluster status: {}", clusterStatus);
			} else {
				LOG.debug("Update cluster status: {}", clusterStatus);
			}

			DbQueryBuilder builder = dbAccess.getQueryBuilder();

			UID nodeId = getNodeId();

			boolean bUpdate = dbEntryChecked;
			boolean bFirstUpdateDuringStartup = false;
			if (!dbEntryChecked) {
				// db entry exists? 'update' else 'insert'
				final DbQuery<Long> q = builder.createQuery(Long.class);
				final DbFrom<UID> from = q.from(RigidE.CLUSTER_SERVER);
				q.select(builder.countRows());
				q.where(builder.equalValue(from.basePk(), nodeId));
				try {
					Long count = dbAccess.executeQuerySingleResult(q);
					bUpdate = count.intValue() > 0;
					bFirstUpdateDuringStartup = bUpdate;
					dbEntryChecked = true;
				} catch (DbException ex) {
					LOG.debug("Writing cluster status {} failed: {}", clusterStatus, ex.getMessage(), ex);
					// java.sql.SQLException: FEHLER: Relation »t_ad_cluster_server« existiert nicht
					// New scheme or auto setup still pending
					return;
				}
			}

			DbMap valuesMap = new DbMap();
			DbMap condition = new DbMap();

			valuesMap.put(RigidE.CLUSTER_SERVER.status, clusterStatus);
			valuesMap.put(RigidE.CLUSTER_SERVER.lastStatusUpdate, DbCurrentDateTime.CURRENT_DATETIME);

			if (!bUpdate || bFirstUpdateDuringStartup) {
				// insert or first update
				valuesMap.put(RigidE.CLUSTER_SERVER.type, getNodeType());
				valuesMap.put(RigidE.CLUSTER_SERVER.protocol, getNodeProtocol());
				valuesMap.put(RigidE.CLUSTER_SERVER.hostname, getNodeHostname());
				valuesMap.put(RigidE.CLUSTER_SERVER.port, getNodePort());
				valuesMap.put(RigidE.CLUSTER_SERVER.context, getNodeContext());
				valuesMap.put(RigidE.CLUSTER_SERVER.balancerProtocol, getBalancerProtocol());
				valuesMap.put(RigidE.CLUSTER_SERVER.balancerHostname, getBalancerHostname());
				valuesMap.put(RigidE.CLUSTER_SERVER.balancerPort, getBalancerPort());
				valuesMap.put(RigidE.CLUSTER_SERVER.balancerContext, getBalancerContext());
				valuesMap.put(RigidE.CLUSTER_SERVER.installedVersion, Version.readNuclosVersion(RigidClusterHelper.class.getClassLoader()).getLongName());
				valuesMap.put(RigidE.CLUSTER_SERVER.javaVersion, String.format("%s (%s)", System.getProperty("java.version"), System.getProperty("java.vendor")));
				valuesMap.put(RigidE.CLUSTER_SERVER.serverExtensionsHash,
						RigidUtils.defaultIfNull(RigidExtensionUtils.getAllServerExtensionsHash(), "No server extensions found"));

				valuesMap.put(SF.CHANGEDBY, "System");
				valuesMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
				if (bFirstUpdateDuringStartup) {
					valuesMap.put(SF.VERSION, DbIncrement.INCREMENT);
				}
			}
			if (bUpdate) {
				// all updates
				condition.put(RigidE.CLUSTER_SERVER.getPk(), nodeId);
			} else {
				// insert
				valuesMap.put(SF.VERSION, 1);
				valuesMap.put(SF.CREATEDBY, "System");
				valuesMap.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
				valuesMap.put(RigidE.CLUSTER_SERVER.getPk(), nodeId);
			}

			final DbBuildableStatement stmt;
			if (bUpdate) {
				stmt = new DbUpdateStatement(RigidE.CLUSTER_SERVER, valuesMap, condition);
			} else {
				stmt = new DbInsertStatement(RigidE.CLUSTER_SERVER, valuesMap);
			}
			try {
				dbAccess.execute(stmt);
				// dbAccess.commit();
			} catch (DbException ex) {
				LOG.debug("Writing cluster status {} failed: {}", clusterStatus, ex.getMessage(), ex);
				if (STATUS_RUNNING.equals(clusterStatus)) {
					// Now something is really wrong
					throw ex;
				} else {
					// java.sql.SQLException: FEHLER: Relation »t_ad_cluster_server« existiert nicht
					// New scheme or auto setup still pending
					return;
				}
			}
		}
	}

	public static void sendMessage(ClusterMessageType type,
							String optionalMessage) {
		sendMessage(type, optionalMessage, null);
	}

	public static void sendMessage(ClusterMessageType type,
							String optionalMessage,
							UID excludeSlaveId) {
		try {
			if (isMaster()) {
				// send to slaves
				DbQueryBuilder builder = dbAccess.getQueryBuilder();
				final DbQuery<UID> q = builder.createQuery(UID.class);
				final DbFrom<UID> from = q.from(RigidE.CLUSTER_SERVER);
				q.select(from.basePk());
				q.where(builder.equalValue(from.baseColumn(RigidE.CLUSTER_SERVER.type), TYPE_SLAVE));
				if (excludeSlaveId != null) {
					q.addToWhereAsAnd(builder.not(builder.equalValue(from.basePk(), excludeSlaveId)));
				}
				dbAccess.executeQuery(q).forEach(csId ->
						writeClusterMessage(csId, type.name(), optionalMessage, false));
			} else {
				// send to master
				writeClusterMessage(getNodeId(), type.name(), optionalMessage, true);
			}
		}
		catch (Exception e) {
			LOG.error("Sending cluster message of type {} with content \"{}\" failed: {}",
					type.name(), optionalMessage, e.getMessage(), e);
		}
	}

	private static void writeClusterMessage(
			UID serverId,
			String type,
			String content,
			boolean master
	) {
		DbMap valuesMap = new DbMap();

		valuesMap.put(RigidE.CLUSTER_MESSAGE.getPk(), new DbId());
		valuesMap.put(RigidE.CLUSTER_MESSAGE.server, serverId);
		valuesMap.put(RigidE.CLUSTER_MESSAGE.type, type);
		if (content != null) {
			valuesMap.put(RigidE.CLUSTER_MESSAGE.content, content);
		}
		valuesMap.put(RigidE.CLUSTER_MESSAGE.master, master);
		valuesMap.put(RigidE.CLUSTER_MESSAGE.read, false);
		valuesMap.put(RigidE.CLUSTER_MESSAGE.done, false);

		valuesMap.put(SF.CREATEDBY, getNodeId().getString());
		valuesMap.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		valuesMap.put(SF.CHANGEDBY, getNodeId().getString());
		valuesMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		valuesMap.put(SF.VERSION, 1);

		dbAccess.execute(new DbInsertStatement(RigidE.CLUSTER_MESSAGE, valuesMap));
	}

	public static void deleteUnreadMessages() {
		if (!isMaster()) {
			// only for slaves!
			try {
				dbAccess.execute(DbStatementUtils.deleteFrom(
						RigidE.CLUSTER_MESSAGE,
						RigidE.CLUSTER_MESSAGE.server, getNodeId(),
						RigidE.CLUSTER_MESSAGE.read, false));
			} catch (DbException ex) {
				LOG.debug("Delete unread messages failed: {}", ex.getMessage(), ex);
			}
		}
	}

	public static void markAsRead(Long messageId) {
		try {
			dbAccess.execute(DbStatementUtils.updateValues(
					RigidE.CLUSTER_MESSAGE,
					RigidE.CLUSTER_MESSAGE.read, Boolean.TRUE)
			.where(RigidE.CLUSTER_MESSAGE.getPk(), messageId));
		} catch (DbException ex) {
			LOG.error("Mark message as READ with id {} failed: {}", messageId, ex.getMessage(), ex);
		}
	}

	public static void markAsDone(Long messageId) {
		try {
			dbAccess.execute(DbStatementUtils.updateValues(
					RigidE.CLUSTER_MESSAGE,
					RigidE.CLUSTER_MESSAGE.done, Boolean.TRUE)
					.where(RigidE.CLUSTER_MESSAGE.getPk(), messageId));
		} catch (DbException ex) {
			LOG.error("Mark message as DONE with id {} failed: {}", messageId, ex.getMessage(), ex);
		}
	}

	/**
	 *
	 * @param sDefault
	 * @return the node id (cluster server id) if cluster mode is enabled.
	 * 			sDefault otherwise
	 */
	public static String getNodeIdAsString(String sDefault) {
		if (isClusterEnabled()) {
			return getNodeId().getString();
		} else {
			return sDefault;
		}
	}

	public static boolean checkNodeId() {
		return checkValueInConfig("node.id");
	}

	public static UID getNodeId() {
		if (nodeId == null) {
			String sNodeId = getValueFromConfig("node.id");
			nodeId = UID.parseUID(sNodeId);
		}
		return nodeId;
	}

	public static String getBalancerProtocol() {
		if (balancerProtocol == null) {
			if (isClusterEnabled()) {
				if (checkValueInConfig("balancer.protocol")) {
					balancerProtocol = getValueFromConfig("balancer.protocol");
				} else {
					balancerProtocol = getNodeProtocol();
				}
			} else {
				balancerProtocol = NOT_AVAILABLE;
			}
		}
		return balancerProtocol;
	}

	public static String getBalancerHostname() {
		if (balancerHostname == null) {
			if (isClusterEnabled()) {
				if (checkValueInConfig("balancer.hostname")) {
					balancerHostname = getValueFromConfig("balancer.hostname");
				} else {
					balancerHostname = getNodeHostname();
				}
			} else {
				balancerHostname = NOT_AVAILABLE;
			}
		}
		return balancerHostname;
	}

	public static Integer getBalancerPort() {
		if (balancerPort == null) {
			if (isClusterEnabled()) {
				if (checkValueInConfig("balancer.port")) {
					String nodePort = getValueFromConfig("balancer.port");
					try {
						balancerPort = Integer.valueOf(nodePort);
					} catch (NumberFormatException e) {
						throw new RuntimeException(String.format("Cluster config parameter balancer.port (server.properties) is not an integer '%s'! ",
								nodePort));
					}
				} else {
					balancerPort = getNodePort();
				}
			} else {
				balancerPort = -1;
			}
		}
		return balancerPort;
	}

	public static String getBalancerContext() {
		if (balancerContext == null) {
			if (isClusterEnabled()) {
				if (checkValueInConfig("balancer.context")) {
					balancerContext = getValueFromConfig("balancer.context");
				} else {
					balancerContext = getNodeContext();
				}
			} else {
				balancerContext = NOT_AVAILABLE;
			}
		}
		return balancerContext;
	}

	private static String getNodeType() {
		if (isClusterEnabled()) {
			return getValueFromConfig("node.type");
		} else {
			return TYPE_SINGLE;
		}
	}

	private static String getNodeProtocol() {
		String nodeProtocol;
		if (checkValueInConfig("node.protocol") || isClusterEnabled()) {
			nodeProtocol = getValueFromConfig("node.protocol");
			if (!"http".equals(nodeProtocol) &&
					!"https".equals(nodeProtocol)) {
				throw new RuntimeException(String.format("Cluster config parameter node.protocol (server.properties) is not valid '%s'! " +
						"Valid values are 'http' or 'https'.", nodeProtocol));
			}
		} else {
			nodeProtocol = NOT_AVAILABLE;
		}
		return nodeProtocol;
	}

	private static String getNodeHostname() {
		if (checkValueInConfig("node.hostname") || isClusterEnabled()) {
			return getValueFromConfig("node.hostname");
		} else {
			try {
				InetAddress inetAddress = InetAddress.getLocalHost();
				String hostname = inetAddress.getHostName();
				if (hostname != null) {
					return hostname;
				}
				String address = inetAddress.getHostAddress();
				if (address != null) {
					return address;
				}
			} catch (Exception e) {
				LOG.warn("Get local host failed: {}", e.getMessage());
			}
			return NOT_AVAILABLE;
		}
	}

	private static Integer getNodePort() {
		if (checkValueInConfig("node.port") || isClusterEnabled()) {
			String nodePort = getValueFromConfig("node.port");
			try {
				return Integer.valueOf(nodePort);
			} catch (NumberFormatException e) {
				throw new RuntimeException(String.format("Cluster config parameter node.port (server.properties) is not an integer '%s'! ",
						nodePort));
			}
		} else {
			return -1;
		}
	}

	private static String getNodeContext() {
		if (checkValueInConfig("node.context") || isClusterEnabled()) {
			return getValueFromConfig("node.context");
		} else {
			return NOT_AVAILABLE;
		}
	}

	private static boolean checkValueInConfig(String sParam) {
		return !RigidUtils.looksEmpty(getConfig().get(sParam));
	}

	private static String getValueFromConfig(String sParam) {
		String sValue = getConfig().get(sParam);
		if (RigidUtils.looksEmpty(sValue)) {
			throw new RuntimeException(String.format("Cluster config parameter %s (server.properties) looks empty!", sParam));
		}
		return sValue.trim();
	}

	private static Map<String, String> getConfig() {
		if (config == null) {
			config = new ConcurrentHashMap<>();
			for (Map.Entry<?, ?> p : ServerProperties.loadProperties(ServerProperties.JNDI_SERVER_PROPERTIES).entrySet()) {
				if (p.getKey().toString().startsWith("cluster.")) {
					config.put(p.getKey().toString().substring(8), p.getValue().toString());
				}
			}
		}

		return config;
	}

}
