package org.nuclos.server.common;

import java.sql.Connection;
import java.sql.SQLException;

public interface IMyDataBaseConnection {
		
		Connection getConnection() throws SQLException;
		
		void resetMeta();
	
}
