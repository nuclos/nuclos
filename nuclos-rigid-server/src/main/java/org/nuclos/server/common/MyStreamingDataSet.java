package org.nuclos.server.common;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.stream.IDataSetProducer;
import org.dbunit.dataset.stream.StreamingDataSet;
import org.nuclos.server.common.MyDataBaseConnection.IStreamingFilterProvider;

public class MyStreamingDataSet extends StreamingDataSet {
	private final IDataSetProducer source;
	private final IStreamingFilterProvider filterProvider;

	public MyStreamingDataSet(IDataSetProducer source, IStreamingFilterProvider filterProvider) {
		super(source);
		this.source = source;
		this.filterProvider = filterProvider;
	}
	
	@Override
	protected ITableIterator createIterator(boolean reversed)
			throws DataSetException {
	       if (reversed)
	        {
	            throw new UnsupportedOperationException(
	                    "Reverse iterator not supported!");
	        }

		return new MyStreamingIterator(source, filterProvider);
	}

}
