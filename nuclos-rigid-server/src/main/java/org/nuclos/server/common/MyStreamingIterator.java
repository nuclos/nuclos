package org.nuclos.server.common;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.stream.IDataSetProducer;
import org.dbunit.dataset.stream.StreamingIterator;
import org.nuclos.server.common.MyDataBaseConnection.IStreamingFilterProvider;

public class MyStreamingIterator extends StreamingIterator {
	private final IStreamingFilterProvider filterProvider;

	public MyStreamingIterator(IDataSetProducer source, IStreamingFilterProvider filterProvider) throws DataSetException {
		super(source);
		this.filterProvider = filterProvider;
	}
	
	@Override
    public boolean next() throws DataSetException
    {
		boolean ok = super.next();
		if (!ok) {
			filterProvider.commit();
			return false;
		}
		
		for (;;) {
			
			boolean ok2 = filterProvider.prepare(getTableMetaData().getTableName());
			if (ok2) {
				break;
			}
			
			ok = super.next();
			if (!ok) {
				filterProvider.commit();
				break;
			}
		}			
		return ok;
    }
	
}
