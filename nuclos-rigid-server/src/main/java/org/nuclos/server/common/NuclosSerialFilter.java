package org.nuclos.server.common;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.apache.log4j.Logger;
import org.nuclos.common.PostConstructManager;
import org.springframework.beans.factory.InitializingBean;

public class NuclosSerialFilter implements InitializingBean {

	private static final Logger LOG = Logger.getLogger(NuclosSerialFilter.class);

	@Override
	public void afterPropertiesSet() throws Exception {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		setSerialFilter();
	}

	private static void setSerialFilter() {
		try {
			// Try to load the class “java.io.ObjectInputFilter” (only available in Java 9+)
			Class<?> objectInputFilterClass = Class.forName("java.io.ObjectInputFilter");
			Class<?> filterInfoClass = Class.forName("java.io.ObjectInputFilter$FilterInfo");
			Class<?> configClass = Class.forName("java.io.ObjectInputFilter$Config");

			// Create a sample filter object that rejects all classes that are not in certain packages
			Object filterProxy = Proxy.newProxyInstance(
					objectInputFilterClass.getClassLoader(),
					new Class<?>[] { objectInputFilterClass },
					new InvocationHandler() {
						@Override
						public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
							if (method.getName().equals("checkInput")) {
								// The first argument of checkInput() is a FilterInfo object
								Object filterInfo = args[0];
								Method serialClassMethod = filterInfoClass.getMethod("serialClass");
								Class<?> serialClass = (Class<?>) serialClassMethod.invoke(filterInfo);
								if (serialClass != null) {
									String className = ((Class<?>) serialClass).getName();
									if (className.startsWith("org.nuclos")
											|| className.startsWith("net.sf.jasperreports")
											|| className.equals("java.lang.Enum")) {
										return Enum.valueOf((Class<Enum>) method.getReturnType(), "ALLOWED");
									} else {
										LOG.warn("Class rejected: " + className);
										return Enum.valueOf((Class<Enum>) method.getReturnType(), "REJECTED");
									}
								}
							}
							return Enum.valueOf((Class<Enum>) method.getReturnType(), "UNDECIDED");
						}
					}
			);

			// Apply the filter (only available in Java 9+)
			Method setSerialFilterMethod = configClass.getMethod("setSerialFilter", objectInputFilterClass);
			setSerialFilterMethod.invoke(null, filterProxy);

			LOG.info("Deserialization filter applied successfully.");

		} catch (ClassNotFoundException e) {
			LOG.info("ObjectInputFilter is not available in this Java version. Continuing without filter.");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
}
