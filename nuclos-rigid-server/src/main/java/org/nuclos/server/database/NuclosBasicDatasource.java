//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.database;

import static org.nuclos.server.common.ServerProperties.JNDI_SERVER_PROPERTIES;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.RigidUtils;
import org.nuclos.server.common.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.jndi.JndiTemplate;
import org.springframework.util.ResourceUtils;

public class NuclosBasicDatasource extends BasicDataSource {
	private static final Logger LOG = LoggerFactory.getLogger(NuclosBasicDatasource.class);

	private static final String DATABASE_CONTAINER_MANAGED_RESOURCE = "database.container.managed.resource";
	private static final String DATABASE_ADAPTER = "database.adapter";
	private static final String DATABASE_SCHEMA = "database.schema";
	private static final String DATABASE_HOME = "database.home";
	private static final String DATABASE_CONNECTION_POOL_SIZE = "database.connection.pool.size";
	private static final String DATABASE_CONNECTION_POOL_WAIT_SECONDS = "database.connection.pool.wait.seconds";
	private static final String DATABASE_MSSQL_ISOLATION = "database.mssql.isolation";
	private static final String ENVIRONMENT_DEVELOPMENT = "environment.development";

	private Properties properties;

	private boolean containerManageLookupDone = false;

	private DataSource containerManagedDataSource;

	public boolean isContainerManaged() {
		return getContainerManagedDataSource() != null;
	}

	private DataSource getContainerManagedDataSource() {
		if (!containerManageLookupDone) {
			String resourceName = getServerProperties().getProperty(DATABASE_CONTAINER_MANAGED_RESOURCE);
			if (StringUtils.isNotBlank(resourceName)) {
				try {
					containerManagedDataSource = new JndiDataSourceLookup().getDataSource(resourceName.trim());
				} catch (Exception ex) {
					LOG.error("Error during lookup of container managed DataSource: " + ex.getMessage(), ex);
					return null;
				}
			}
			containerManageLookupDone = true;
		}
		return containerManagedDataSource;
	}

	@Override
	protected void createDataSourceInstance() throws SQLException {
		dataSource = getContainerManagedDataSource();
		if (dataSource == null) {
			super.createDataSourceInstance();
		}
	}
	
	@Override
	protected synchronized DataSource createDataSource() throws SQLException {
		if (isContainerManaged()) {
			return getContainerManagedDataSource();
		}
		Properties prop = getServerProperties();
		String sAdapter = prop.getProperty(DATABASE_ADAPTER);
		if ("oracle".equalsIgnoreCase(sAdapter)) {
			setValidationQuery("SELECT 1 FROM DUAL");
		} else {
			setValidationQuery("SELECT 1");
		}
		setValidationQueryTimeout(5);
		setTestOnBorrow(true);
		return super.createDataSource();
	}
	
	@PostConstruct
	public void setInitSqlStatements() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		String sAdapter = getPropertyOrThrowException(DATABASE_ADAPTER).toLowerCase();
		String sDBSchema = getPropertyOrThrowException(DATABASE_SCHEMA);
		String sConnectionPoolSize = getServerProperties().getProperty(DATABASE_CONNECTION_POOL_SIZE);
		String sConnectionPoolWaitSeconds = getServerProperties().getProperty(DATABASE_CONNECTION_POOL_WAIT_SECONDS);
		String sMSSQLIsolation = getServerProperties().getProperty(DATABASE_MSSQL_ISOLATION);
		String sConnectionInitStatement = loadConnectionInitStatement();

		if (!RigidUtils.looksEmpty(sConnectionPoolSize)) {
			try {
				int iConnectionPoolSize = Integer.parseInt(sConnectionPoolSize.trim());
				LOG.info("Set connection pool size: {}", iConnectionPoolSize);
				setMaxActive(iConnectionPoolSize);
				setMaxIdle(iConnectionPoolSize);
			} catch (Exception e) {
				LOG.error("Error during set connection pool size {}: {}", sConnectionPoolSize, e.getMessage(), e);
			}
		}

		if (!RigidUtils.looksEmpty(sConnectionPoolWaitSeconds)) {
			try {
				long iConnectionPoolWaitSeconds = Long.parseLong(sConnectionPoolWaitSeconds.trim());
				LOG.info("Set connection pool wait: {} (seconds)", iConnectionPoolWaitSeconds);
				setMaxWait(iConnectionPoolWaitSeconds * 1000L);
			} catch (Exception e) {
				LOG.error("Error during set connection pool size {}: {}", sConnectionPoolSize, e.getMessage(), e);
			}
		}

		Collection<String> colInitSqls = new ArrayList<String>();
		if (sConnectionInitStatement != null) {
			colInitSqls.add(sConnectionInitStatement);
		} else if ("postgresql".equals(sAdapter)) {
			colInitSqls.add("set search_path to "+ sDBSchema + ",public");
		} else if ("oracle".equals(sAdapter)) {
			colInitSqls.add("alter session set nls_comp='BINARY' nls_sort='BINARY'");
		} else if ("h2".equals(sAdapter)) {
			String url = getUrl() + ":" + getDBFilePath() + ";MODE=PostgreSQL;MV_STORE=FALSE";
			if (Boolean.parseBoolean(properties.getProperty(ENVIRONMENT_DEVELOPMENT))) {
				url += ";AUTO_SERVER=TRUE";
			}
			setUrl(url);
			colInitSqls.add("SET SCHEMA " + sDBSchema);
		} else if ("mssql".equals(sAdapter) && "snapshot".equals(sMSSQLIsolation)) {
			colInitSqls.add("set transaction isolation level snapshot");
		}
		setConnectionInitSqls(colInitSqls);
	}

	public String getPropertyOrThrowException(String propName) {
		return Optional
				.ofNullable(
						getServerProperties().getProperty(propName)
				)
				.orElseThrow(() -> new NuclosFatalException("Property " + propName + " is missing"));
	}
	
	public String getDBFilePath() {
		Properties prop = getServerProperties();
		String sDBSchema = prop.getProperty(DATABASE_SCHEMA);
		String sDBPath = prop.getProperty(DATABASE_HOME);
		if (sDBPath == null || sDBPath.isEmpty()) {
			sDBPath = System.getProperty("user.home") + "/.h2";
		}
		File file = new File(sDBPath);
		if (!file.exists()) {
			file.mkdirs();
		}
		
		return sDBPath + "/" + sDBSchema;
	}

	@Override
	public Connection getConnection() throws SQLException {
		final Connection result = super.getConnection();
		if (connectionPool != null) {
			final int numActive = connectionPool.getNumActive();
			final int maxActive = connectionPool.getMaxActive();
			if (numActive == maxActive) {
				LOG.warn("WARNING, connection pool exhausted ({}/{})", numActive, maxActive);
			} else if (numActive >= maxActive - 3) {
				LOG.info("ATTENTION, connection pool will soon be exhausted ({}/{})", numActive, maxActive);
			} else {
				LOG.debug("Connection pool workload: {}/{}", numActive, maxActive);
			}
		}
		return result;
	}

	private String loadConnectionInitStatement() {
		String sLocation = null;
		try {
			sLocation = new JndiTemplate().lookup("java:comp/env/nuclos-conf-connection-init", String.class);
		} catch (Exception ex) {
			// ignore
		}
		if (sLocation == null) {
			LOG.info("db-connection-init.sql not found. using defaults");
			return null;
		}
		try (InputStream is = new BufferedInputStream(new FileInputStream(ResourceUtils.getFile(sLocation)))) {
			return IOUtils.toString(is, StandardCharsets.UTF_8);
		} catch (Exception e) {
			LOG.warn("Could not load db-connection-init.sql. using defaults");
			return null;
		}
		
	}

	private Properties getServerProperties() {
		if (properties == null) {
			properties = ServerProperties.loadProperties(JNDI_SERVER_PROPERTIES);
		}
		if (properties == null) {
			throw new NuclosFatalException("Missing " + JNDI_SERVER_PROPERTIES + " (server.properties)!");
		}
		return properties;
	}
}
