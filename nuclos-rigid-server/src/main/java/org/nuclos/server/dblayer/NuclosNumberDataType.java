package org.nuclos.server.dblayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.dbunit.dataset.datatype.AbstractDataType;
import org.dbunit.dataset.datatype.NumberDataType;
import org.dbunit.dataset.datatype.TypeCastException;

public class NuclosNumberDataType extends AbstractDataType {
	private final NumberDataType numberDataType;
	
	public NuclosNumberDataType(NumberDataType numberDataType) {
		super(numberDataType.toString(), numberDataType.getSqlType(), numberDataType.getTypeClass(), numberDataType.isNumber());
		this.numberDataType = numberDataType;
	}

	@Override
	public Object typeCast(Object value) throws TypeCastException {
		return numberDataType.typeCast(value);
	}

	public Object getSqlValue(int column, ResultSet resultSet)
            throws SQLException, TypeCastException {
		return numberDataType.getSqlValue(column, resultSet);
    }

    public void setSqlValue(Object value, int column, PreparedStatement statement)
            throws SQLException, TypeCastException {
    	
    	if (value instanceof String) {
    		if ("false".equals(value)) {
    			value = Boolean.FALSE;
    		} else if ("true".equals(value)) {
    			value = Boolean.TRUE;
    		}
    	}
    	numberDataType.setSqlValue(value, column, statement);
    }

}
