package org.nuclos.server.dblayer;

import java.sql.ResultSetMetaData;

import org.nuclos.common.collection.Transformer;

public interface ResultSetMetaDataTransformer<T> extends Transformer<ResultSetMetaData, T> {
	
}
