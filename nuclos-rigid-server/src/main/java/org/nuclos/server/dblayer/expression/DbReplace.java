package org.nuclos.server.dblayer.expression;

/**
 * A special DbValue which represents the REPLACE-function
 */
public class DbReplace implements DbSpecialValue<String> {
	private static final long serialVersionUID = -7002908448942482532L;

	private final String pattern;
	private final String replacement;

	public DbReplace(final String pattern, final String replacement) {
		this.pattern = pattern;
		this.replacement = replacement;
	}

	public String getPattern() {
		return pattern;
	}

	public String getReplacement() {
		return replacement;
	}
}
