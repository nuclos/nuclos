package org.nuclos.server.dblayer.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.server.dblayer.EBatchType;
import org.nuclos.server.dblayer.IPreparedStringExecutor;
import org.nuclos.server.dblayer.IUnit;
import org.nuclos.server.dblayer.impl.util.PreparedString;
import org.nuclos.server.dblayer.statements.DbMap;

public class BatchOpenStatement extends BatchImpl {

	private static final Logger LOG = Logger.getLogger(BatchOpenStatement.class);
	
	public BatchOpenStatement(IUnit unit) {
		super(unit);
	}

	public BatchOpenStatement(List<IUnit> units) {
		super(units);
	}
	
	public static BatchOpenStatement simpleBatch(List<PreparedString> sequence, PreparedString targetSequence, DbMap map) {
		if (sequence == null || sequence.isEmpty()) {
			return null;
		}
		return new BatchOpenStatement(RigidUtils.<IUnit>newOneElementArrayList(
				new SqlSequentialOpenStatementUnit(sequence, targetSequence, map)));
	}
	
	public static BatchOpenStatement simpleBatch(PreparedString sequence,  PreparedString targetSequence, DbMap map) {
		return new BatchOpenStatement(
				RigidUtils.<IUnit>newOneElementArrayList(new SqlSequentialOpenStatementUnit(
						RigidUtils.<PreparedString>newOneElementArrayList(sequence), targetSequence, map)));
	}
	
	@Override
	public DalCallResult process(IPreparedStringExecutor ex, EBatchType type) {
		final EBatchType unitType = (type == EBatchType.FAIL_EARLY) ? EBatchType.FAIL_EARLY : EBatchType.FAIL_NEVER;
		final boolean debug = LOG.isDebugEnabled();
		if (debug) LOG.debug("begin process of " + this);
		final DalCallResult result = new DalCallResult();
		for (IUnit unit: getUnits()) {
			if (!type.equals(EBatchType.FAIL_NEVER_IGNORE_EXCEPTION)) {
				result.add(unit.process(ex, unitType));
			}
			else {
				if (debug) LOG.debug("Potential ignoring execption on unit " + unit);
			}
		}
		if (debug) LOG.debug("end batch process with result: " + result);
		if (type.equals(EBatchType.FAIL_LATE)) {
			result.throwFirstException();
		}
		return result;
	}
}
