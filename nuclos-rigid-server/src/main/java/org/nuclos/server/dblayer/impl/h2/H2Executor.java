package org.nuclos.server.dblayer.impl.h2;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.nuclos.server.dblayer.impl.postgresql.PostgreSQLExecutor;

public class H2Executor extends PostgreSQLExecutor {
	final private String zipFilename;
	
	public H2Executor(DataSource dataSource, String username, String password, String zipFilename) {
		super(dataSource, username, password);
		this.zipFilename = zipFilename;
	}
	
	@Override
	public void makePersistent() throws SQLException {
		super.makePersistent();
		if (zipFilename == null || true) {
			return;
		}
		String sql = "SCRIPT TO '" + zipFilename + "' COMPRESSION ZIP";
		ResultSetRunner<Void> runner = new ResultSetRunner<Void>() {
			@Override
			public Void perform(ResultSet rs) throws SQLException {
				return null;
			}
		};
		executeQuery(sql, runner);
	}
	
}
