//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.query;

import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.dblayer.JoinType;

/**
 * @param <T> Mapped Java type for (both) column types in DB used for joining.
 */
public class DbJoin<T> extends DbFrom {

	private final DbFrom<?> left;
	private final JoinType joinType;
	
	// Old implementation, bad because it does not support non-equi joins.
	// private Pair<String, String> on;
	
	private DbCondition on;
	
	DbJoin(DbQuery<?> query, DbFrom<?> left, JoinType joinType, EntityMeta<?> entity, boolean forceTable) {
		super(query, entity, forceTable);
		if (left == null || joinType == null) throw new NullPointerException();
		this.left = left;
		this.joinType = joinType;
	}

	DbJoin(DbQuery<?> query, DbFrom<?> left, JoinType joinType, String selectStatement) {
		super(query, selectStatement);
		if (left == null || joinType == null) throw new NullPointerException();
		this.left = left;
		this.joinType = joinType;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("table=").append(getFrom());
		result.append(", join=").append(left.getFrom());
		result.append(", type=").append(joinType);
		result.append(", on=").append(on);
		result.append("]");
		return result.toString();
	}

	public DbFrom<?> getParent() {
		return left;
	}

	/**
	 * Simple equi-join.
	 */
	public <T2> DbJoin<T> on(DbField<T2> baseTableColumn, DbField<T2> joinedTableColumn) {
		if (on != null) {
			throw new IllegalStateException("Join criteria already set");
		}
		final DbQueryBuilder b = left.getQuery().getBuilder();
		this.on = b.equal(left.baseColumn(baseTableColumn), baseColumn(joinedTableColumn));
		return (DbJoin<T>) this;
	}
	
	/**
	 * Non equi-join.
	 */
	public DbJoin onAnd(DbCondition...cond) {
		if (on != null) {
			throw new IllegalStateException("Join criteria already set");
		}
		final DbQueryBuilder b = left.getQuery().getBuilder();
		this.on = b.and(cond);
		return this;
	}
	
	
	public JoinType getJoinType() {
		return joinType;
	}
	
	public DbCondition getOn() {
		return on;
	}
	
	protected DbJoin<T> alias(String alias) {
		return (DbJoin<T>) super.alias(alias);
	}
}
