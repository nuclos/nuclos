//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.structure;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.UID;

public class DbReferenceColumn extends DbColumn {

	private final DbNamedObject referencedTable;

	public DbReferenceColumn(final UID uid, final DbNamedObject table, final String columnName, final DbColumnType columnType, final DbNamedObject referencedTable, final DbNullable nullable, final Object defaultValue, final Integer order) {
		super(uid, table, columnName, columnType, nullable, defaultValue, order);
		this.referencedTable = referencedTable;
	}

	public DbNamedObject getReferencedTable() {
		return referencedTable;
	}

	@Override
	protected boolean isUnchanged(DbArtifact a, boolean forceNames) {
		boolean result = super.isUnchanged(a, forceNames);
		if (result && a instanceof DbReferenceColumn) {
			DbReferenceColumn other = (DbReferenceColumn) a;
			if (getReferencedTable() != null && other.getReferencedTable() != null) {
				if (forceNames) {
					result = ObjectUtils.equals(getReferencedTable().getName(), other.getReferencedTable().getName());
				} else {
					result = ObjectUtils.equals(getReferencedTable().getEqualsKey(), other.getReferencedTable().getEqualsKey());
				}
			}
		}
		return result;
	}

	@Override
	public Class getClassForSameTypeCheck() {
		return DbColumn.class;
	}
}
