//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.structure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.statements.DbBatchStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType;

/**
 * Initial data of a table.
 */
public class DbTableData implements Serializable {

	private final String tableName;
	private final List<Pair<String, DbGenericType>> columns;
	private final List<List<Object>> data;

	public DbTableData(String tableName, List<Pair<String, DbGenericType>> columns, List<List<Object>> data) {
		this.tableName = tableName;
		this.columns = columns;
		this.data = data;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("table=").append(tableName);
		result.append(", columns=").append(columns);
		result.append("]");
		return result.toString();
	}

	public String getTableName() {
		return tableName;
	}
	
	public List<Pair<String, DbGenericType>> getColumns() {
		return columns;
	}
	
	public List<List<Object>> getData() {
		return data;
	}
	
	public List<DbStatement> getStatements(boolean deleteAll) {
		List<DbStatement> statements = new ArrayList<DbStatement>();
		final String tableName = getTableName();
		EntityMeta<?> e = new EntityMeta() {
			@Override public UID getUID() {return new UID("dummy");}
			@Override public String getDbTable() {return tableName;}
			@Override public String getEntityName() {return "dummy";}
			@Override public Class getPkClass() {return Object.class;}
			
		};
		if (deleteAll) {			
			statements.add(DbStatementUtils.deleteAllFrom(e));
		}
		DbBatchStatement insertBatch = new DbBatchStatement();
		for (List<Object> row : data) {
			DbMap values = new DbMap();
			for (int i = 0; i < columns.size(); i++) {
				SimpleDbField<Object> field = (SimpleDbField<Object>) 
						SimpleDbField.create(columns.get(i).getX(), row.get(i).getClass());
				values.put(field, row.get(i));
			}
			insertBatch.add(new DbInsertStatement(e, values));  
		}
		statements.add(insertBatch);
		return statements;
	}
}
