//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.extension;

import static org.nuclos.server.common.NuclosSystemParameters.getNuclosHome;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

import org.slf4j.LoggerFactory;

public class RigidExtensionUtils implements ExtensionConstants {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(RigidExtensionUtils.class);

	public static Path getNucletExtensionsHomePath() {
		return resolveNullSafe(getNuclosHome(), "nuclet-extensions");
	}

	public static Path getNucletExtensionsServerHomePath() {
		return resolveNullSafe(getNucletExtensionsHomePath(), "server");
	}

	public static Path getInstallerExtensionsServerHomePath() {
		return resolveNullSafe(resolveNullSafe(getNuclosHome(), "extensions"), "server");
	}

	public static List<Path> getNucletExtensionPathEntries(final Path dir) throws IOException {
		final List<Path> entries = new ArrayList<>();
		if (dir == null || !dir.toFile().exists() || !dir.toFile().isDirectory()) {
			return entries;
		}
		Files.newDirectoryStream(dir, path -> {
			String fileName = path.getFileName().toString().toLowerCase();
			return fileName.contains(NUCLET_EXTENSION_FILE_IDENTIFIER) && fileName.endsWith(".jar");
		}).forEach(entries::add);
		Collections.sort(entries);
		return entries;
	}

	public static List<Path> getInstallerExtensionServerPathEntries() throws IOException {
		final List<Path> entries = new ArrayList<>();
		Path dir = getInstallerExtensionsServerHomePath();
		if (dir == null || !dir.toFile().exists() || !dir.toFile().isDirectory()) {
			return entries;
		}
		Files.newDirectoryStream(dir, path -> path.getFileName().toString().toLowerCase().endsWith(".jar")).forEach(entries::add);
		Collections.sort(entries);
		return entries;
	}

	public static void addServerExtensions(List<File> toFileList, Path extensionsLibDir) {
		try {
			getNucletExtensionPathEntries(extensionsLibDir).stream().forEach(n -> toFileList.add(n.toFile()));
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, extensionsLibDir, ex);
		}
	}

	public static String getAllServerExtensionsHash() {
		String result = null;
		try {
			List<Path> extensionEntries = new ArrayList<>();
			extensionEntries.addAll(getInstallerExtensionServerPathEntries());
			extensionEntries.addAll(getNucletExtensionPathEntries(getNucletExtensionsServerHomePath()));
			if (!extensionEntries.isEmpty()) {
				final MessageDigest digest = MessageDigest.getInstance("SHA-256");
				for (Path path : extensionEntries) {
					try (InputStream is = Files.newInputStream(path);
						 DigestInputStream dis = new DigestInputStream(is, digest)) {
						while (dis.read() != -1) {}
					}
				}
				final byte[] hash = digest.digest();
				result = Base64.getEncoder().encodeToString(hash);
			}
		} catch (Exception e) {
			LOG.error("Generating hash of all server extensions failed: ", e.getMessage(), e);
			result = "Error (see server.log)";
		}
		return result;
	}

	private static Path resolveNullSafe(Path dir, String other) {
		return dir != null ? dir.resolve(other) : null;
	}

}
