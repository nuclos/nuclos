<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<parent>
		<groupId>org.nuclos</groupId>
		<artifactId>nuclos</artifactId>
		<version>4.2025.9-SNAPSHOT</version>
		<relativePath>../pom.xml</relativePath>
	</parent>
	<modelVersion>4.0.0</modelVersion>

	<artifactId>nuclos-schema</artifactId>
	<packaging>jar</packaging>

	<name>nuclos-schema</name>
	<url>http://www.nuclos.de/</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<nuclos.javadoc.prefix>../../../</nuclos.javadoc.prefix>

		<sonar.skip>true</sonar.skip>
	</properties>

	<build>
		<plugins>
			<plugin>
				<groupId>org.jvnet.jaxb2.maven2</groupId>
				<artifactId>maven-jaxb2-plugin</artifactId>
				<version>0.14.0</version>
				<configuration>
					<locale>en_US</locale>
					<packageLevelAnnotations>true</packageLevelAnnotations>
					<!-- markGenerated uses an ugly timestamp -->
					<markGenerated>false</markGenerated>
					<noFileHeader>true</noFileHeader>
					<removeOldOutput>true</removeOldOutput>
					<generateDirectory>${project.basedir}/src/main/java</generateDirectory>
					<episode>false</episode>
					<lineLength>140</lineLength>
					<strict>true</strict>
					<forceRegenerate>true</forceRegenerate>
					<bindingDirectory>${project.basedir}/src/main/resources/org/nuclos/schema/</bindingDirectory>
					<args>
						<arg>-XtoString</arg>
						<arg>-Xequals</arg>
						<arg>-XhashCode</arg>
						<arg>-Xcopyable</arg>
						<arg>-Xfluent-builder</arg>
						<arg>-Xannotate</arg>
						<arg>-XremoveAnnotation</arg>
					</args>
					<plugins>
						<plugin>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-basics</artifactId>
							<version>0.9.0</version>
						</plugin>
						<plugin>
							<groupId>net.codesup.util</groupId>
							<artifactId>jaxb2-rich-contract-plugin</artifactId>
							<version>2.0.1</version>
						</plugin>
						<plugin>
							<groupId>org.jvnet.jaxb2_commons</groupId>
							<artifactId>jaxb2-basics-annotate</artifactId>
							<version>1.1.0</version>
						</plugin>
					</plugins>
				</configuration>
				<dependencies>
					<dependency>
						<groupId>com.fasterxml.jackson.core</groupId>
						<artifactId>jackson-annotations</artifactId>
						<version>${jackson.version}</version>
					</dependency>
				</dependencies>
				<executions>
					<execution>
						<id>generate-java-sources</id>
						<goals>
							<goal>generate</goal>
						</goals>
						<configuration>
							<schemas>
								<schema>
									<fileset>
										<directory>${project.basedir}/src/main/resources/org/nuclos/schema/</directory>
										<includes>
											<include>**/*.xsd</include>
										</includes>
									</fileset>
								</schema>
							</schemas>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>com.google.code.maven-replacer-plugin</groupId>
				<artifactId>replacer</artifactId>
				<version>1.5.3</version>
				<executions>
					<execution>
						<id>cleanup-previous-comments</id>
						<phase>generate-sources</phase>
						<goals>
							<goal>replace</goal>
						</goals>
						<configuration>
							<basedir>${project.basedir}/src/main/java</basedir>
							<includes>
								<include>**/*.java</include>
							</includes>
							<regex>true</regex>
							<replacements>
								<replacement>
									<token>^.*Row removed due to missing sorting of Java source code generation.*$</token>
									<value />
								</replacement>
							</replacements>
						</configuration>
					</execution>
					<execution>
						<id>cleanup-unsorted-jaxb-elements</id>
						<phase>generate-sources</phase>
						<goals>
							<goal>replace</goal>
						</goals>
						<configuration>
							<basedir>${project.basedir}/src/main/java</basedir>
							<includes>
								<include>**/*.java</include>
							</includes>
							<regex>true</regex>
							<replacements>
								<replacement>
									<token>[ ]*\*[ ]*\{@link JAXBElement.*</token>
									<value>JAXBElement_PLACEHOLDER</value>
								</replacement>
							</replacements>
						</configuration>
					</execution>
					<execution>
						<id>cleanup-empty-lines</id>
						<phase>generate-sources</phase>
						<goals>
							<goal>replace</goal>
						</goals>
						<configuration>
							<basedir>${project.basedir}/src/main/java</basedir>
							<includes>
								<include>**/*.java</include>
							</includes>
							<regex>true</regex>
							<replacements>
								<replacement>
									<!-- Ersetzt mehrere gleiche Zeilen durch eine einzige -->
									<token>(\s*JAXBElement_PLACEHOLDER\s*\n)(\s*JAXBElement_PLACEHOLDER)+</token>
									<value>: Row(s) removed due to missing sorting of JAXBElement java doc generation</value>
								</replacement>
								<replacement>
									<!-- Entferne leere Zeilen -->
									<token>^[ \t]*\n</token>
									<value />
								</replacement>
							</replacements>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<artifactId>maven-clean-plugin</artifactId>
				<version>3.1.0</version>
				<configuration>
					<filesets>
						<fileset>
							<!-- Removes the previously used target directory for generated sources -->
							<directory>target/generated-sources</directory>
							<includes>
								<include>**</include>
							</includes>
							<followSymlinks>false</followSymlinks>
						</fileset>
						<fileset>
							<!-- Removes the previously used target directory for generated sources -->
							<directory>src/main/generated</directory>
							<includes>
								<include>**</include>
							</includes>
							<followSymlinks>false</followSymlinks>
						</fileset>
					</filesets>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<dependencies>
		<dependency>
			<groupId>org.jvnet.jaxb2_commons</groupId>
			<artifactId>jaxb2-basics-runtime</artifactId>
			<version>0.6.4</version>
		</dependency>
		<dependency>
			<groupId>javax.annotation</groupId>
			<artifactId>javax.annotation-api</artifactId>
		</dependency>
		<dependency>
			<groupId>javax.xml.bind</groupId>
			<artifactId>jaxb-api</artifactId>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-annotations</artifactId>
		</dependency>
	</dependencies>

</project>
