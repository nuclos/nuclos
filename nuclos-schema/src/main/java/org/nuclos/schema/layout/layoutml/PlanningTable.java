package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;element ref="{}font" minOccurs="0"/&gt;
 *         &lt;element ref="{}description" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="planning-table" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="foreignkeyfield-to-parent" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="enabled" type="{}boolean" /&gt;
 *       &lt;attribute name="date-from" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="date-until" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="field-date-from" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="field-date-until" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "layoutconstraints",
    "clearBorder",
    "lineBorder",
    "etchedBorder",
    "bevelBorder",
    "titledBorder",
    "emptyBorder",
    "border",
    "minimumSize",
    "preferredSize",
    "strictSize",
    "font",
    "description"
})
public class PlanningTable implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutconstraints;
    @XmlElement(name = "clear-border")
    protected ClearBorder clearBorder;
    @XmlElement(name = "line-border")
    protected LineBorder lineBorder;
    @XmlElement(name = "etched-border")
    protected EtchedBorder etchedBorder;
    @XmlElement(name = "bevel-border")
    protected BevelBorder bevelBorder;
    @XmlElement(name = "titled-border")
    protected TitledBorder titledBorder;
    @XmlElement(name = "empty-border")
    protected EmptyBorder emptyBorder;
    protected List<Object> border;
    @XmlElement(name = "minimum-size")
    protected MinimumSize minimumSize;
    @XmlElement(name = "preferred-size")
    protected PreferredSize preferredSize;
    @XmlElement(name = "strict-size")
    protected StrictSize strictSize;
    protected Font font;
    protected String description;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "planning-table")
    @XmlSchemaType(name = "anySimpleType")
    protected String planningTable;
    @XmlAttribute(name = "foreignkeyfield-to-parent")
    @XmlSchemaType(name = "anySimpleType")
    protected String foreignkeyfieldToParent;
    @XmlAttribute(name = "enabled")
    protected Boolean enabled;
    @XmlAttribute(name = "date-from")
    @XmlSchemaType(name = "anySimpleType")
    protected String dateFrom;
    @XmlAttribute(name = "date-until")
    @XmlSchemaType(name = "anySimpleType")
    protected String dateUntil;
    @XmlAttribute(name = "field-date-from")
    @XmlSchemaType(name = "anySimpleType")
    protected String fieldDateFrom;
    @XmlAttribute(name = "field-date-until")
    @XmlSchemaType(name = "anySimpleType")
    protected String fieldDateUntil;

    /**
     * Gets the value of the layoutconstraints property.
     * 
     * @return
     *     possible object is: Row(s) removed due to missing sorting of JAXBElement java doc generation
     *     
     */
    public JAXBElement<?> getLayoutconstraints() {
        return layoutconstraints;
    }

    /**
     * Sets the value of the layoutconstraints property.
     * 
     * @param value
     *     allowed object is: Row(s) removed due to missing sorting of JAXBElement java doc generation
     *     
     */
    public void setLayoutconstraints(JAXBElement<?> value) {
        this.layoutconstraints = value;
    }

    /**
     * Gets the value of the clearBorder property.
     * 
     * @return
     *     possible object is
     *     {@link ClearBorder }
     *     
     */
    public ClearBorder getClearBorder() {
        return clearBorder;
    }

    /**
     * Sets the value of the clearBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClearBorder }
     *     
     */
    public void setClearBorder(ClearBorder value) {
        this.clearBorder = value;
    }

    /**
     * Gets the value of the lineBorder property.
     * 
     * @return
     *     possible object is
     *     {@link LineBorder }
     *     
     */
    public LineBorder getLineBorder() {
        return lineBorder;
    }

    /**
     * Sets the value of the lineBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineBorder }
     *     
     */
    public void setLineBorder(LineBorder value) {
        this.lineBorder = value;
    }

    /**
     * Gets the value of the etchedBorder property.
     * 
     * @return
     *     possible object is
     *     {@link EtchedBorder }
     *     
     */
    public EtchedBorder getEtchedBorder() {
        return etchedBorder;
    }

    /**
     * Sets the value of the etchedBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtchedBorder }
     *     
     */
    public void setEtchedBorder(EtchedBorder value) {
        this.etchedBorder = value;
    }

    /**
     * Gets the value of the bevelBorder property.
     * 
     * @return
     *     possible object is
     *     {@link BevelBorder }
     *     
     */
    public BevelBorder getBevelBorder() {
        return bevelBorder;
    }

    /**
     * Sets the value of the bevelBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link BevelBorder }
     *     
     */
    public void setBevelBorder(BevelBorder value) {
        this.bevelBorder = value;
    }

    /**
     * Gets the value of the titledBorder property.
     * 
     * @return
     *     possible object is
     *     {@link TitledBorder }
     *     
     */
    public TitledBorder getTitledBorder() {
        return titledBorder;
    }

    /**
     * Sets the value of the titledBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link TitledBorder }
     *     
     */
    public void setTitledBorder(TitledBorder value) {
        this.titledBorder = value;
    }

    /**
     * Gets the value of the emptyBorder property.
     * 
     * @return
     *     possible object is
     *     {@link EmptyBorder }
     *     
     */
    public EmptyBorder getEmptyBorder() {
        return emptyBorder;
    }

    /**
     * Sets the value of the emptyBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyBorder }
     *     
     */
    public void setEmptyBorder(EmptyBorder value) {
        this.emptyBorder = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the border property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBorder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getBorder() {
        if (border == null) {
            border = new ArrayList<Object>();
        }
        return this.border;
    }

    /**
     * Gets the value of the minimumSize property.
     * 
     * @return
     *     possible object is
     *     {@link MinimumSize }
     *     
     */
    public MinimumSize getMinimumSize() {
        return minimumSize;
    }

    /**
     * Sets the value of the minimumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumSize }
     *     
     */
    public void setMinimumSize(MinimumSize value) {
        this.minimumSize = value;
    }

    /**
     * Gets the value of the preferredSize property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredSize }
     *     
     */
    public PreferredSize getPreferredSize() {
        return preferredSize;
    }

    /**
     * Sets the value of the preferredSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredSize }
     *     
     */
    public void setPreferredSize(PreferredSize value) {
        this.preferredSize = value;
    }

    /**
     * Gets the value of the strictSize property.
     * 
     * @return
     *     possible object is
     *     {@link StrictSize }
     *     
     */
    public StrictSize getStrictSize() {
        return strictSize;
    }

    /**
     * Sets the value of the strictSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrictSize }
     *     
     */
    public void setStrictSize(StrictSize value) {
        this.strictSize = value;
    }

    /**
     * Gets the value of the font property.
     * 
     * @return
     *     possible object is
     *     {@link Font }
     *     
     */
    public Font getFont() {
        return font;
    }

    /**
     * Sets the value of the font property.
     * 
     * @param value
     *     allowed object is
     *     {@link Font }
     *     
     */
    public void setFont(Font value) {
        this.font = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the planningTable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanningTable() {
        return planningTable;
    }

    /**
     * Sets the value of the planningTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanningTable(String value) {
        this.planningTable = value;
    }

    /**
     * Gets the value of the foreignkeyfieldToParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignkeyfieldToParent() {
        return foreignkeyfieldToParent;
    }

    /**
     * Sets the value of the foreignkeyfieldToParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignkeyfieldToParent(String value) {
        this.foreignkeyfieldToParent = value;
    }

    /**
     * Gets the value of the enabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * Sets the value of the enabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnabled(Boolean value) {
        this.enabled = value;
    }

    /**
     * Gets the value of the dateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateFrom() {
        return dateFrom;
    }

    /**
     * Sets the value of the dateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateFrom(String value) {
        this.dateFrom = value;
    }

    /**
     * Gets the value of the dateUntil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateUntil() {
        return dateUntil;
    }

    /**
     * Sets the value of the dateUntil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateUntil(String value) {
        this.dateUntil = value;
    }

    /**
     * Gets the value of the fieldDateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldDateFrom() {
        return fieldDateFrom;
    }

    /**
     * Sets the value of the fieldDateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldDateFrom(String value) {
        this.fieldDateFrom = value;
    }

    /**
     * Gets the value of the fieldDateUntil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldDateUntil() {
        return fieldDateUntil;
    }

    /**
     * Sets the value of the fieldDateUntil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldDateUntil(String value) {
        this.fieldDateUntil = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            strategy.appendField(locator, this, "layoutconstraints", buffer, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            strategy.appendField(locator, this, "clearBorder", buffer, theClearBorder);
        }
        {
            LineBorder theLineBorder;
            theLineBorder = this.getLineBorder();
            strategy.appendField(locator, this, "lineBorder", buffer, theLineBorder);
        }
        {
            EtchedBorder theEtchedBorder;
            theEtchedBorder = this.getEtchedBorder();
            strategy.appendField(locator, this, "etchedBorder", buffer, theEtchedBorder);
        }
        {
            BevelBorder theBevelBorder;
            theBevelBorder = this.getBevelBorder();
            strategy.appendField(locator, this, "bevelBorder", buffer, theBevelBorder);
        }
        {
            TitledBorder theTitledBorder;
            theTitledBorder = this.getTitledBorder();
            strategy.appendField(locator, this, "titledBorder", buffer, theTitledBorder);
        }
        {
            EmptyBorder theEmptyBorder;
            theEmptyBorder = this.getEmptyBorder();
            strategy.appendField(locator, this, "emptyBorder", buffer, theEmptyBorder);
        }
        {
            List<Object> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            strategy.appendField(locator, this, "border", buffer, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            strategy.appendField(locator, this, "minimumSize", buffer, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            strategy.appendField(locator, this, "preferredSize", buffer, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            strategy.appendField(locator, this, "strictSize", buffer, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            strategy.appendField(locator, this, "font", buffer, theFont);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String thePlanningTable;
            thePlanningTable = this.getPlanningTable();
            strategy.appendField(locator, this, "planningTable", buffer, thePlanningTable);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            strategy.appendField(locator, this, "foreignkeyfieldToParent", buffer, theForeignkeyfieldToParent);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            strategy.appendField(locator, this, "enabled", buffer, theEnabled);
        }
        {
            String theDateFrom;
            theDateFrom = this.getDateFrom();
            strategy.appendField(locator, this, "dateFrom", buffer, theDateFrom);
        }
        {
            String theDateUntil;
            theDateUntil = this.getDateUntil();
            strategy.appendField(locator, this, "dateUntil", buffer, theDateUntil);
        }
        {
            String theFieldDateFrom;
            theFieldDateFrom = this.getFieldDateFrom();
            strategy.appendField(locator, this, "fieldDateFrom", buffer, theFieldDateFrom);
        }
        {
            String theFieldDateUntil;
            theFieldDateUntil = this.getFieldDateUntil();
            strategy.appendField(locator, this, "fieldDateUntil", buffer, theFieldDateUntil);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PlanningTable)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PlanningTable that = ((PlanningTable) object);
        {
            JAXBElement<?> lhsLayoutconstraints;
            lhsLayoutconstraints = this.getLayoutconstraints();
            JAXBElement<?> rhsLayoutconstraints;
            rhsLayoutconstraints = that.getLayoutconstraints();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutconstraints", lhsLayoutconstraints), LocatorUtils.property(thatLocator, "layoutconstraints", rhsLayoutconstraints), lhsLayoutconstraints, rhsLayoutconstraints)) {
                return false;
            }
        }
        {
            ClearBorder lhsClearBorder;
            lhsClearBorder = this.getClearBorder();
            ClearBorder rhsClearBorder;
            rhsClearBorder = that.getClearBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearBorder", lhsClearBorder), LocatorUtils.property(thatLocator, "clearBorder", rhsClearBorder), lhsClearBorder, rhsClearBorder)) {
                return false;
            }
        }
        {
            LineBorder lhsLineBorder;
            lhsLineBorder = this.getLineBorder();
            LineBorder rhsLineBorder;
            rhsLineBorder = that.getLineBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lineBorder", lhsLineBorder), LocatorUtils.property(thatLocator, "lineBorder", rhsLineBorder), lhsLineBorder, rhsLineBorder)) {
                return false;
            }
        }
        {
            EtchedBorder lhsEtchedBorder;
            lhsEtchedBorder = this.getEtchedBorder();
            EtchedBorder rhsEtchedBorder;
            rhsEtchedBorder = that.getEtchedBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "etchedBorder", lhsEtchedBorder), LocatorUtils.property(thatLocator, "etchedBorder", rhsEtchedBorder), lhsEtchedBorder, rhsEtchedBorder)) {
                return false;
            }
        }
        {
            BevelBorder lhsBevelBorder;
            lhsBevelBorder = this.getBevelBorder();
            BevelBorder rhsBevelBorder;
            rhsBevelBorder = that.getBevelBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bevelBorder", lhsBevelBorder), LocatorUtils.property(thatLocator, "bevelBorder", rhsBevelBorder), lhsBevelBorder, rhsBevelBorder)) {
                return false;
            }
        }
        {
            TitledBorder lhsTitledBorder;
            lhsTitledBorder = this.getTitledBorder();
            TitledBorder rhsTitledBorder;
            rhsTitledBorder = that.getTitledBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "titledBorder", lhsTitledBorder), LocatorUtils.property(thatLocator, "titledBorder", rhsTitledBorder), lhsTitledBorder, rhsTitledBorder)) {
                return false;
            }
        }
        {
            EmptyBorder lhsEmptyBorder;
            lhsEmptyBorder = this.getEmptyBorder();
            EmptyBorder rhsEmptyBorder;
            rhsEmptyBorder = that.getEmptyBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "emptyBorder", lhsEmptyBorder), LocatorUtils.property(thatLocator, "emptyBorder", rhsEmptyBorder), lhsEmptyBorder, rhsEmptyBorder)) {
                return false;
            }
        }
        {
            List<Object> lhsBorder;
            lhsBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            List<Object> rhsBorder;
            rhsBorder = (((that.border!= null)&&(!that.border.isEmpty()))?that.getBorder():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "border", lhsBorder), LocatorUtils.property(thatLocator, "border", rhsBorder), lhsBorder, rhsBorder)) {
                return false;
            }
        }
        {
            MinimumSize lhsMinimumSize;
            lhsMinimumSize = this.getMinimumSize();
            MinimumSize rhsMinimumSize;
            rhsMinimumSize = that.getMinimumSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumSize", lhsMinimumSize), LocatorUtils.property(thatLocator, "minimumSize", rhsMinimumSize), lhsMinimumSize, rhsMinimumSize)) {
                return false;
            }
        }
        {
            PreferredSize lhsPreferredSize;
            lhsPreferredSize = this.getPreferredSize();
            PreferredSize rhsPreferredSize;
            rhsPreferredSize = that.getPreferredSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredSize", lhsPreferredSize), LocatorUtils.property(thatLocator, "preferredSize", rhsPreferredSize), lhsPreferredSize, rhsPreferredSize)) {
                return false;
            }
        }
        {
            StrictSize lhsStrictSize;
            lhsStrictSize = this.getStrictSize();
            StrictSize rhsStrictSize;
            rhsStrictSize = that.getStrictSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "strictSize", lhsStrictSize), LocatorUtils.property(thatLocator, "strictSize", rhsStrictSize), lhsStrictSize, rhsStrictSize)) {
                return false;
            }
        }
        {
            Font lhsFont;
            lhsFont = this.getFont();
            Font rhsFont;
            rhsFont = that.getFont();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "font", lhsFont), LocatorUtils.property(thatLocator, "font", rhsFont), lhsFont, rhsFont)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsPlanningTable;
            lhsPlanningTable = this.getPlanningTable();
            String rhsPlanningTable;
            rhsPlanningTable = that.getPlanningTable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "planningTable", lhsPlanningTable), LocatorUtils.property(thatLocator, "planningTable", rhsPlanningTable), lhsPlanningTable, rhsPlanningTable)) {
                return false;
            }
        }
        {
            String lhsForeignkeyfieldToParent;
            lhsForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            String rhsForeignkeyfieldToParent;
            rhsForeignkeyfieldToParent = that.getForeignkeyfieldToParent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "foreignkeyfieldToParent", lhsForeignkeyfieldToParent), LocatorUtils.property(thatLocator, "foreignkeyfieldToParent", rhsForeignkeyfieldToParent), lhsForeignkeyfieldToParent, rhsForeignkeyfieldToParent)) {
                return false;
            }
        }
        {
            Boolean lhsEnabled;
            lhsEnabled = this.getEnabled();
            Boolean rhsEnabled;
            rhsEnabled = that.getEnabled();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "enabled", lhsEnabled), LocatorUtils.property(thatLocator, "enabled", rhsEnabled), lhsEnabled, rhsEnabled)) {
                return false;
            }
        }
        {
            String lhsDateFrom;
            lhsDateFrom = this.getDateFrom();
            String rhsDateFrom;
            rhsDateFrom = that.getDateFrom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateFrom", lhsDateFrom), LocatorUtils.property(thatLocator, "dateFrom", rhsDateFrom), lhsDateFrom, rhsDateFrom)) {
                return false;
            }
        }
        {
            String lhsDateUntil;
            lhsDateUntil = this.getDateUntil();
            String rhsDateUntil;
            rhsDateUntil = that.getDateUntil();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateUntil", lhsDateUntil), LocatorUtils.property(thatLocator, "dateUntil", rhsDateUntil), lhsDateUntil, rhsDateUntil)) {
                return false;
            }
        }
        {
            String lhsFieldDateFrom;
            lhsFieldDateFrom = this.getFieldDateFrom();
            String rhsFieldDateFrom;
            rhsFieldDateFrom = that.getFieldDateFrom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fieldDateFrom", lhsFieldDateFrom), LocatorUtils.property(thatLocator, "fieldDateFrom", rhsFieldDateFrom), lhsFieldDateFrom, rhsFieldDateFrom)) {
                return false;
            }
        }
        {
            String lhsFieldDateUntil;
            lhsFieldDateUntil = this.getFieldDateUntil();
            String rhsFieldDateUntil;
            rhsFieldDateUntil = that.getFieldDateUntil();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fieldDateUntil", lhsFieldDateUntil), LocatorUtils.property(thatLocator, "fieldDateUntil", rhsFieldDateUntil), lhsFieldDateUntil, rhsFieldDateUntil)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutconstraints", theLayoutconstraints), currentHashCode, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearBorder", theClearBorder), currentHashCode, theClearBorder);
        }
        {
            LineBorder theLineBorder;
            theLineBorder = this.getLineBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "lineBorder", theLineBorder), currentHashCode, theLineBorder);
        }
        {
            EtchedBorder theEtchedBorder;
            theEtchedBorder = this.getEtchedBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "etchedBorder", theEtchedBorder), currentHashCode, theEtchedBorder);
        }
        {
            BevelBorder theBevelBorder;
            theBevelBorder = this.getBevelBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bevelBorder", theBevelBorder), currentHashCode, theBevelBorder);
        }
        {
            TitledBorder theTitledBorder;
            theTitledBorder = this.getTitledBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "titledBorder", theTitledBorder), currentHashCode, theTitledBorder);
        }
        {
            EmptyBorder theEmptyBorder;
            theEmptyBorder = this.getEmptyBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "emptyBorder", theEmptyBorder), currentHashCode, theEmptyBorder);
        }
        {
            List<Object> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "border", theBorder), currentHashCode, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumSize", theMinimumSize), currentHashCode, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredSize", thePreferredSize), currentHashCode, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "strictSize", theStrictSize), currentHashCode, theStrictSize);
        }
        {
            Font theFont;
            theFont = this.getFont();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "font", theFont), currentHashCode, theFont);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String thePlanningTable;
            thePlanningTable = this.getPlanningTable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "planningTable", thePlanningTable), currentHashCode, thePlanningTable);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "foreignkeyfieldToParent", theForeignkeyfieldToParent), currentHashCode, theForeignkeyfieldToParent);
        }
        {
            Boolean theEnabled;
            theEnabled = this.getEnabled();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "enabled", theEnabled), currentHashCode, theEnabled);
        }
        {
            String theDateFrom;
            theDateFrom = this.getDateFrom();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dateFrom", theDateFrom), currentHashCode, theDateFrom);
        }
        {
            String theDateUntil;
            theDateUntil = this.getDateUntil();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dateUntil", theDateUntil), currentHashCode, theDateUntil);
        }
        {
            String theFieldDateFrom;
            theFieldDateFrom = this.getFieldDateFrom();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fieldDateFrom", theFieldDateFrom), currentHashCode, theFieldDateFrom);
        }
        {
            String theFieldDateUntil;
            theFieldDateUntil = this.getFieldDateUntil();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fieldDateUntil", theFieldDateUntil), currentHashCode, theFieldDateUntil);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PlanningTable) {
            final PlanningTable copy = ((PlanningTable) draftCopy);
            if (this.layoutconstraints!= null) {
                JAXBElement<?> sourceLayoutconstraints;
                sourceLayoutconstraints = this.getLayoutconstraints();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutconstraints = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutconstraints", sourceLayoutconstraints), sourceLayoutconstraints));
                copy.setLayoutconstraints(copyLayoutconstraints);
            } else {
                copy.layoutconstraints = null;
            }
            if (this.clearBorder!= null) {
                ClearBorder sourceClearBorder;
                sourceClearBorder = this.getClearBorder();
                ClearBorder copyClearBorder = ((ClearBorder) strategy.copy(LocatorUtils.property(locator, "clearBorder", sourceClearBorder), sourceClearBorder));
                copy.setClearBorder(copyClearBorder);
            } else {
                copy.clearBorder = null;
            }
            if (this.lineBorder!= null) {
                LineBorder sourceLineBorder;
                sourceLineBorder = this.getLineBorder();
                LineBorder copyLineBorder = ((LineBorder) strategy.copy(LocatorUtils.property(locator, "lineBorder", sourceLineBorder), sourceLineBorder));
                copy.setLineBorder(copyLineBorder);
            } else {
                copy.lineBorder = null;
            }
            if (this.etchedBorder!= null) {
                EtchedBorder sourceEtchedBorder;
                sourceEtchedBorder = this.getEtchedBorder();
                EtchedBorder copyEtchedBorder = ((EtchedBorder) strategy.copy(LocatorUtils.property(locator, "etchedBorder", sourceEtchedBorder), sourceEtchedBorder));
                copy.setEtchedBorder(copyEtchedBorder);
            } else {
                copy.etchedBorder = null;
            }
            if (this.bevelBorder!= null) {
                BevelBorder sourceBevelBorder;
                sourceBevelBorder = this.getBevelBorder();
                BevelBorder copyBevelBorder = ((BevelBorder) strategy.copy(LocatorUtils.property(locator, "bevelBorder", sourceBevelBorder), sourceBevelBorder));
                copy.setBevelBorder(copyBevelBorder);
            } else {
                copy.bevelBorder = null;
            }
            if (this.titledBorder!= null) {
                TitledBorder sourceTitledBorder;
                sourceTitledBorder = this.getTitledBorder();
                TitledBorder copyTitledBorder = ((TitledBorder) strategy.copy(LocatorUtils.property(locator, "titledBorder", sourceTitledBorder), sourceTitledBorder));
                copy.setTitledBorder(copyTitledBorder);
            } else {
                copy.titledBorder = null;
            }
            if (this.emptyBorder!= null) {
                EmptyBorder sourceEmptyBorder;
                sourceEmptyBorder = this.getEmptyBorder();
                EmptyBorder copyEmptyBorder = ((EmptyBorder) strategy.copy(LocatorUtils.property(locator, "emptyBorder", sourceEmptyBorder), sourceEmptyBorder));
                copy.setEmptyBorder(copyEmptyBorder);
            } else {
                copy.emptyBorder = null;
            }
            if ((this.border!= null)&&(!this.border.isEmpty())) {
                List<Object> sourceBorder;
                sourceBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
                @SuppressWarnings("unchecked")
                List<Object> copyBorder = ((List<Object> ) strategy.copy(LocatorUtils.property(locator, "border", sourceBorder), sourceBorder));
                copy.border = null;
                if (copyBorder!= null) {
                    List<Object> uniqueBorderl = copy.getBorder();
                    uniqueBorderl.addAll(copyBorder);
                }
            } else {
                copy.border = null;
            }
            if (this.minimumSize!= null) {
                MinimumSize sourceMinimumSize;
                sourceMinimumSize = this.getMinimumSize();
                MinimumSize copyMinimumSize = ((MinimumSize) strategy.copy(LocatorUtils.property(locator, "minimumSize", sourceMinimumSize), sourceMinimumSize));
                copy.setMinimumSize(copyMinimumSize);
            } else {
                copy.minimumSize = null;
            }
            if (this.preferredSize!= null) {
                PreferredSize sourcePreferredSize;
                sourcePreferredSize = this.getPreferredSize();
                PreferredSize copyPreferredSize = ((PreferredSize) strategy.copy(LocatorUtils.property(locator, "preferredSize", sourcePreferredSize), sourcePreferredSize));
                copy.setPreferredSize(copyPreferredSize);
            } else {
                copy.preferredSize = null;
            }
            if (this.strictSize!= null) {
                StrictSize sourceStrictSize;
                sourceStrictSize = this.getStrictSize();
                StrictSize copyStrictSize = ((StrictSize) strategy.copy(LocatorUtils.property(locator, "strictSize", sourceStrictSize), sourceStrictSize));
                copy.setStrictSize(copyStrictSize);
            } else {
                copy.strictSize = null;
            }
            if (this.font!= null) {
                Font sourceFont;
                sourceFont = this.getFont();
                Font copyFont = ((Font) strategy.copy(LocatorUtils.property(locator, "font", sourceFont), sourceFont));
                copy.setFont(copyFont);
            } else {
                copy.font = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.planningTable!= null) {
                String sourcePlanningTable;
                sourcePlanningTable = this.getPlanningTable();
                String copyPlanningTable = ((String) strategy.copy(LocatorUtils.property(locator, "planningTable", sourcePlanningTable), sourcePlanningTable));
                copy.setPlanningTable(copyPlanningTable);
            } else {
                copy.planningTable = null;
            }
            if (this.foreignkeyfieldToParent!= null) {
                String sourceForeignkeyfieldToParent;
                sourceForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
                String copyForeignkeyfieldToParent = ((String) strategy.copy(LocatorUtils.property(locator, "foreignkeyfieldToParent", sourceForeignkeyfieldToParent), sourceForeignkeyfieldToParent));
                copy.setForeignkeyfieldToParent(copyForeignkeyfieldToParent);
            } else {
                copy.foreignkeyfieldToParent = null;
            }
            if (this.enabled!= null) {
                Boolean sourceEnabled;
                sourceEnabled = this.getEnabled();
                Boolean copyEnabled = ((Boolean) strategy.copy(LocatorUtils.property(locator, "enabled", sourceEnabled), sourceEnabled));
                copy.setEnabled(copyEnabled);
            } else {
                copy.enabled = null;
            }
            if (this.dateFrom!= null) {
                String sourceDateFrom;
                sourceDateFrom = this.getDateFrom();
                String copyDateFrom = ((String) strategy.copy(LocatorUtils.property(locator, "dateFrom", sourceDateFrom), sourceDateFrom));
                copy.setDateFrom(copyDateFrom);
            } else {
                copy.dateFrom = null;
            }
            if (this.dateUntil!= null) {
                String sourceDateUntil;
                sourceDateUntil = this.getDateUntil();
                String copyDateUntil = ((String) strategy.copy(LocatorUtils.property(locator, "dateUntil", sourceDateUntil), sourceDateUntil));
                copy.setDateUntil(copyDateUntil);
            } else {
                copy.dateUntil = null;
            }
            if (this.fieldDateFrom!= null) {
                String sourceFieldDateFrom;
                sourceFieldDateFrom = this.getFieldDateFrom();
                String copyFieldDateFrom = ((String) strategy.copy(LocatorUtils.property(locator, "fieldDateFrom", sourceFieldDateFrom), sourceFieldDateFrom));
                copy.setFieldDateFrom(copyFieldDateFrom);
            } else {
                copy.fieldDateFrom = null;
            }
            if (this.fieldDateUntil!= null) {
                String sourceFieldDateUntil;
                sourceFieldDateUntil = this.getFieldDateUntil();
                String copyFieldDateUntil = ((String) strategy.copy(LocatorUtils.property(locator, "fieldDateUntil", sourceFieldDateUntil), sourceFieldDateUntil));
                copy.setFieldDateUntil(copyFieldDateUntil);
            } else {
                copy.fieldDateUntil = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PlanningTable();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PlanningTable.Builder<_B> _other) {
        _other.layoutconstraints = this.layoutconstraints;
        _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other));
        _other.lineBorder = ((this.lineBorder == null)?null:this.lineBorder.newCopyBuilder(_other));
        _other.etchedBorder = ((this.etchedBorder == null)?null:this.etchedBorder.newCopyBuilder(_other));
        _other.bevelBorder = ((this.bevelBorder == null)?null:this.bevelBorder.newCopyBuilder(_other));
        _other.titledBorder = ((this.titledBorder == null)?null:this.titledBorder.newCopyBuilder(_other));
        _other.emptyBorder = ((this.emptyBorder == null)?null:this.emptyBorder.newCopyBuilder(_other));
        if (this.border == null) {
            _other.border = null;
        } else {
            _other.border = new ArrayList<Buildable>();
            for (Object _item: this.border) {
                _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other));
        _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other));
        _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other));
        _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other));
        _other.description = this.description;
        _other.name = this.name;
        _other.planningTable = this.planningTable;
        _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        _other.enabled = this.enabled;
        _other.dateFrom = this.dateFrom;
        _other.dateUntil = this.dateUntil;
        _other.fieldDateFrom = this.fieldDateFrom;
        _other.fieldDateUntil = this.fieldDateUntil;
    }

    public<_B >PlanningTable.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PlanningTable.Builder<_B>(_parentBuilder, this, true);
    }

    public PlanningTable.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PlanningTable.Builder<Void> builder() {
        return new PlanningTable.Builder<Void>(null, null, false);
    }

    public static<_B >PlanningTable.Builder<_B> copyOf(final PlanningTable _other) {
        final PlanningTable.Builder<_B> _newBuilder = new PlanningTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PlanningTable.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
            _other.layoutconstraints = this.layoutconstraints;
        }
        final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
            _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other, clearBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree lineBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lineBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(lineBorderPropertyTree!= null):((lineBorderPropertyTree == null)||(!lineBorderPropertyTree.isLeaf())))) {
            _other.lineBorder = ((this.lineBorder == null)?null:this.lineBorder.newCopyBuilder(_other, lineBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree etchedBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("etchedBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(etchedBorderPropertyTree!= null):((etchedBorderPropertyTree == null)||(!etchedBorderPropertyTree.isLeaf())))) {
            _other.etchedBorder = ((this.etchedBorder == null)?null:this.etchedBorder.newCopyBuilder(_other, etchedBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree bevelBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bevelBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bevelBorderPropertyTree!= null):((bevelBorderPropertyTree == null)||(!bevelBorderPropertyTree.isLeaf())))) {
            _other.bevelBorder = ((this.bevelBorder == null)?null:this.bevelBorder.newCopyBuilder(_other, bevelBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree titledBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("titledBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titledBorderPropertyTree!= null):((titledBorderPropertyTree == null)||(!titledBorderPropertyTree.isLeaf())))) {
            _other.titledBorder = ((this.titledBorder == null)?null:this.titledBorder.newCopyBuilder(_other, titledBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree emptyBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("emptyBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(emptyBorderPropertyTree!= null):((emptyBorderPropertyTree == null)||(!emptyBorderPropertyTree.isLeaf())))) {
            _other.emptyBorder = ((this.emptyBorder == null)?null:this.emptyBorder.newCopyBuilder(_other, emptyBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
            if (this.border == null) {
                _other.border = null;
            } else {
                _other.border = new ArrayList<Buildable>();
                for (Object _item: this.border) {
                    _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
            _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other, minimumSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
            _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other, preferredSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
            _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other, strictSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
            _other.font = ((this.font == null)?null:this.font.newCopyBuilder(_other, fontPropertyTree, _propertyTreeUse));
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree planningTablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("planningTable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(planningTablePropertyTree!= null):((planningTablePropertyTree == null)||(!planningTablePropertyTree.isLeaf())))) {
            _other.planningTable = this.planningTable;
        }
        final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
            _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        }
        final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
            _other.enabled = this.enabled;
        }
        final PropertyTree dateFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateFrom"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateFromPropertyTree!= null):((dateFromPropertyTree == null)||(!dateFromPropertyTree.isLeaf())))) {
            _other.dateFrom = this.dateFrom;
        }
        final PropertyTree dateUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateUntil"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateUntilPropertyTree!= null):((dateUntilPropertyTree == null)||(!dateUntilPropertyTree.isLeaf())))) {
            _other.dateUntil = this.dateUntil;
        }
        final PropertyTree fieldDateFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldDateFrom"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldDateFromPropertyTree!= null):((fieldDateFromPropertyTree == null)||(!fieldDateFromPropertyTree.isLeaf())))) {
            _other.fieldDateFrom = this.fieldDateFrom;
        }
        final PropertyTree fieldDateUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldDateUntil"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldDateUntilPropertyTree!= null):((fieldDateUntilPropertyTree == null)||(!fieldDateUntilPropertyTree.isLeaf())))) {
            _other.fieldDateUntil = this.fieldDateUntil;
        }
    }

    public<_B >PlanningTable.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PlanningTable.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PlanningTable.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PlanningTable.Builder<_B> copyOf(final PlanningTable _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PlanningTable.Builder<_B> _newBuilder = new PlanningTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PlanningTable.Builder<Void> copyExcept(final PlanningTable _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PlanningTable.Builder<Void> copyOnly(final PlanningTable _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PlanningTable _storedValue;
        private JAXBElement<?> layoutconstraints;
        private ClearBorder.Builder<PlanningTable.Builder<_B>> clearBorder;
        private LineBorder.Builder<PlanningTable.Builder<_B>> lineBorder;
        private EtchedBorder.Builder<PlanningTable.Builder<_B>> etchedBorder;
        private BevelBorder.Builder<PlanningTable.Builder<_B>> bevelBorder;
        private TitledBorder.Builder<PlanningTable.Builder<_B>> titledBorder;
        private EmptyBorder.Builder<PlanningTable.Builder<_B>> emptyBorder;
        private List<Buildable> border;
        private MinimumSize.Builder<PlanningTable.Builder<_B>> minimumSize;
        private PreferredSize.Builder<PlanningTable.Builder<_B>> preferredSize;
        private StrictSize.Builder<PlanningTable.Builder<_B>> strictSize;
        private Font.Builder<PlanningTable.Builder<_B>> font;
        private String description;
        private String name;
        private String planningTable;
        private String foreignkeyfieldToParent;
        private Boolean enabled;
        private String dateFrom;
        private String dateUntil;
        private String fieldDateFrom;
        private String fieldDateUntil;

        public Builder(final _B _parentBuilder, final PlanningTable _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layoutconstraints = _other.layoutconstraints;
                    this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this));
                    this.lineBorder = ((_other.lineBorder == null)?null:_other.lineBorder.newCopyBuilder(this));
                    this.etchedBorder = ((_other.etchedBorder == null)?null:_other.etchedBorder.newCopyBuilder(this));
                    this.bevelBorder = ((_other.bevelBorder == null)?null:_other.bevelBorder.newCopyBuilder(this));
                    this.titledBorder = ((_other.titledBorder == null)?null:_other.titledBorder.newCopyBuilder(this));
                    this.emptyBorder = ((_other.emptyBorder == null)?null:_other.emptyBorder.newCopyBuilder(this));
                    if (_other.border == null) {
                        this.border = null;
                    } else {
                        this.border = new ArrayList<Buildable>();
                        for (Object _item: _other.border) {
                            this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this));
                    this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this));
                    this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this));
                    this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this));
                    this.description = _other.description;
                    this.name = _other.name;
                    this.planningTable = _other.planningTable;
                    this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                    this.enabled = _other.enabled;
                    this.dateFrom = _other.dateFrom;
                    this.dateUntil = _other.dateUntil;
                    this.fieldDateFrom = _other.fieldDateFrom;
                    this.fieldDateUntil = _other.fieldDateUntil;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PlanningTable _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
                        this.layoutconstraints = _other.layoutconstraints;
                    }
                    final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
                        this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this, clearBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree lineBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lineBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(lineBorderPropertyTree!= null):((lineBorderPropertyTree == null)||(!lineBorderPropertyTree.isLeaf())))) {
                        this.lineBorder = ((_other.lineBorder == null)?null:_other.lineBorder.newCopyBuilder(this, lineBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree etchedBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("etchedBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(etchedBorderPropertyTree!= null):((etchedBorderPropertyTree == null)||(!etchedBorderPropertyTree.isLeaf())))) {
                        this.etchedBorder = ((_other.etchedBorder == null)?null:_other.etchedBorder.newCopyBuilder(this, etchedBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree bevelBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bevelBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bevelBorderPropertyTree!= null):((bevelBorderPropertyTree == null)||(!bevelBorderPropertyTree.isLeaf())))) {
                        this.bevelBorder = ((_other.bevelBorder == null)?null:_other.bevelBorder.newCopyBuilder(this, bevelBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree titledBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("titledBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titledBorderPropertyTree!= null):((titledBorderPropertyTree == null)||(!titledBorderPropertyTree.isLeaf())))) {
                        this.titledBorder = ((_other.titledBorder == null)?null:_other.titledBorder.newCopyBuilder(this, titledBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree emptyBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("emptyBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(emptyBorderPropertyTree!= null):((emptyBorderPropertyTree == null)||(!emptyBorderPropertyTree.isLeaf())))) {
                        this.emptyBorder = ((_other.emptyBorder == null)?null:_other.emptyBorder.newCopyBuilder(this, emptyBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
                        if (_other.border == null) {
                            this.border = null;
                        } else {
                            this.border = new ArrayList<Buildable>();
                            for (Object _item: _other.border) {
                                this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
                        this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this, minimumSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
                        this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this, preferredSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
                        this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this, strictSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree fontPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("font"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontPropertyTree!= null):((fontPropertyTree == null)||(!fontPropertyTree.isLeaf())))) {
                        this.font = ((_other.font == null)?null:_other.font.newCopyBuilder(this, fontPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree planningTablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("planningTable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(planningTablePropertyTree!= null):((planningTablePropertyTree == null)||(!planningTablePropertyTree.isLeaf())))) {
                        this.planningTable = _other.planningTable;
                    }
                    final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
                        this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                    }
                    final PropertyTree enabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("enabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(enabledPropertyTree!= null):((enabledPropertyTree == null)||(!enabledPropertyTree.isLeaf())))) {
                        this.enabled = _other.enabled;
                    }
                    final PropertyTree dateFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateFrom"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateFromPropertyTree!= null):((dateFromPropertyTree == null)||(!dateFromPropertyTree.isLeaf())))) {
                        this.dateFrom = _other.dateFrom;
                    }
                    final PropertyTree dateUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateUntil"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateUntilPropertyTree!= null):((dateUntilPropertyTree == null)||(!dateUntilPropertyTree.isLeaf())))) {
                        this.dateUntil = _other.dateUntil;
                    }
                    final PropertyTree fieldDateFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldDateFrom"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldDateFromPropertyTree!= null):((fieldDateFromPropertyTree == null)||(!fieldDateFromPropertyTree.isLeaf())))) {
                        this.fieldDateFrom = _other.fieldDateFrom;
                    }
                    final PropertyTree fieldDateUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldDateUntil"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldDateUntilPropertyTree!= null):((fieldDateUntilPropertyTree == null)||(!fieldDateUntilPropertyTree.isLeaf())))) {
                        this.fieldDateUntil = _other.fieldDateUntil;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PlanningTable >_P init(final _P _product) {
            _product.layoutconstraints = this.layoutconstraints;
            _product.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.build());
            _product.lineBorder = ((this.lineBorder == null)?null:this.lineBorder.build());
            _product.etchedBorder = ((this.etchedBorder == null)?null:this.etchedBorder.build());
            _product.bevelBorder = ((this.bevelBorder == null)?null:this.bevelBorder.build());
            _product.titledBorder = ((this.titledBorder == null)?null:this.titledBorder.build());
            _product.emptyBorder = ((this.emptyBorder == null)?null:this.emptyBorder.build());
            if (this.border!= null) {
                final List<Object> border = new ArrayList<Object>(this.border.size());
                for (Buildable _item: this.border) {
                    border.add(((Object) _item.build()));
                }
                _product.border = border;
            }
            _product.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.build());
            _product.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.build());
            _product.strictSize = ((this.strictSize == null)?null:this.strictSize.build());
            _product.font = ((this.font == null)?null:this.font.build());
            _product.description = this.description;
            _product.name = this.name;
            _product.planningTable = this.planningTable;
            _product.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
            _product.enabled = this.enabled;
            _product.dateFrom = this.dateFrom;
            _product.dateUntil = this.dateUntil;
            _product.fieldDateFrom = this.fieldDateFrom;
            _product.fieldDateUntil = this.fieldDateUntil;
            return _product;
        }

        /**
         * Sets the new value of "layoutconstraints" (any previous value will be replaced)
         * 
         * @param layoutconstraints
         *     New value of the "layoutconstraints" property.
         */
        public PlanningTable.Builder<_B> withLayoutconstraints(final JAXBElement<?> layoutconstraints) {
            this.layoutconstraints = layoutconstraints;
            return this;
        }

        /**
         * Sets the new value of "clearBorder" (any previous value will be replaced)
         * 
         * @param clearBorder
         *     New value of the "clearBorder" property.
         */
        public PlanningTable.Builder<_B> withClearBorder(final ClearBorder clearBorder) {
            this.clearBorder = ((clearBorder == null)?null:new ClearBorder.Builder<PlanningTable.Builder<_B>>(this, clearBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends PlanningTable.Builder<_B>> withClearBorder() {
            if (this.clearBorder!= null) {
                return this.clearBorder;
            }
            return this.clearBorder = new ClearBorder.Builder<PlanningTable.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "lineBorder" (any previous value will be replaced)
         * 
         * @param lineBorder
         *     New value of the "lineBorder" property.
         */
        public PlanningTable.Builder<_B> withLineBorder(final LineBorder lineBorder) {
            this.lineBorder = ((lineBorder == null)?null:new LineBorder.Builder<PlanningTable.Builder<_B>>(this, lineBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "lineBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.LineBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "lineBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.LineBorder.Builder#end()} to return to the current builder.
         */
        public LineBorder.Builder<? extends PlanningTable.Builder<_B>> withLineBorder() {
            if (this.lineBorder!= null) {
                return this.lineBorder;
            }
            return this.lineBorder = new LineBorder.Builder<PlanningTable.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "etchedBorder" (any previous value will be replaced)
         * 
         * @param etchedBorder
         *     New value of the "etchedBorder" property.
         */
        public PlanningTable.Builder<_B> withEtchedBorder(final EtchedBorder etchedBorder) {
            this.etchedBorder = ((etchedBorder == null)?null:new EtchedBorder.Builder<PlanningTable.Builder<_B>>(this, etchedBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "etchedBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.EtchedBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "etchedBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.EtchedBorder.Builder#end()} to return to the current builder.
         */
        public EtchedBorder.Builder<? extends PlanningTable.Builder<_B>> withEtchedBorder() {
            if (this.etchedBorder!= null) {
                return this.etchedBorder;
            }
            return this.etchedBorder = new EtchedBorder.Builder<PlanningTable.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "bevelBorder" (any previous value will be replaced)
         * 
         * @param bevelBorder
         *     New value of the "bevelBorder" property.
         */
        public PlanningTable.Builder<_B> withBevelBorder(final BevelBorder bevelBorder) {
            this.bevelBorder = ((bevelBorder == null)?null:new BevelBorder.Builder<PlanningTable.Builder<_B>>(this, bevelBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "bevelBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.BevelBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "bevelBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.BevelBorder.Builder#end()} to return to the current builder.
         */
        public BevelBorder.Builder<? extends PlanningTable.Builder<_B>> withBevelBorder() {
            if (this.bevelBorder!= null) {
                return this.bevelBorder;
            }
            return this.bevelBorder = new BevelBorder.Builder<PlanningTable.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "titledBorder" (any previous value will be replaced)
         * 
         * @param titledBorder
         *     New value of the "titledBorder" property.
         */
        public PlanningTable.Builder<_B> withTitledBorder(final TitledBorder titledBorder) {
            this.titledBorder = ((titledBorder == null)?null:new TitledBorder.Builder<PlanningTable.Builder<_B>>(this, titledBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "titledBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.TitledBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "titledBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.TitledBorder.Builder#end()} to return to the current builder.
         */
        public TitledBorder.Builder<? extends PlanningTable.Builder<_B>> withTitledBorder() {
            if (this.titledBorder!= null) {
                return this.titledBorder;
            }
            return this.titledBorder = new TitledBorder.Builder<PlanningTable.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "emptyBorder" (any previous value will be replaced)
         * 
         * @param emptyBorder
         *     New value of the "emptyBorder" property.
         */
        public PlanningTable.Builder<_B> withEmptyBorder(final EmptyBorder emptyBorder) {
            this.emptyBorder = ((emptyBorder == null)?null:new EmptyBorder.Builder<PlanningTable.Builder<_B>>(this, emptyBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "emptyBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.EmptyBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "emptyBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.EmptyBorder.Builder#end()} to return to the current builder.
         */
        public EmptyBorder.Builder<? extends PlanningTable.Builder<_B>> withEmptyBorder() {
            if (this.emptyBorder!= null) {
                return this.emptyBorder;
            }
            return this.emptyBorder = new EmptyBorder.Builder<PlanningTable.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public PlanningTable.Builder<_B> addBorder(final Iterable<?> border) {
            if (border!= null) {
                if (this.border == null) {
                    this.border = new ArrayList<Buildable>();
                }
                for (Object _item: border) {
                    this.border.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public PlanningTable.Builder<_B> withBorder(final Iterable<?> border) {
            if (this.border!= null) {
                this.border.clear();
            }
            return addBorder(border);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public PlanningTable.Builder<_B> addBorder(Object... border) {
            addBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public PlanningTable.Builder<_B> withBorder(Object... border) {
            withBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "minimumSize" (any previous value will be replaced)
         * 
         * @param minimumSize
         *     New value of the "minimumSize" property.
         */
        public PlanningTable.Builder<_B> withMinimumSize(final MinimumSize minimumSize) {
            this.minimumSize = ((minimumSize == null)?null:new MinimumSize.Builder<PlanningTable.Builder<_B>>(this, minimumSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends PlanningTable.Builder<_B>> withMinimumSize() {
            if (this.minimumSize!= null) {
                return this.minimumSize;
            }
            return this.minimumSize = new MinimumSize.Builder<PlanningTable.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "preferredSize" (any previous value will be replaced)
         * 
         * @param preferredSize
         *     New value of the "preferredSize" property.
         */
        public PlanningTable.Builder<_B> withPreferredSize(final PreferredSize preferredSize) {
            this.preferredSize = ((preferredSize == null)?null:new PreferredSize.Builder<PlanningTable.Builder<_B>>(this, preferredSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends PlanningTable.Builder<_B>> withPreferredSize() {
            if (this.preferredSize!= null) {
                return this.preferredSize;
            }
            return this.preferredSize = new PreferredSize.Builder<PlanningTable.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "strictSize" (any previous value will be replaced)
         * 
         * @param strictSize
         *     New value of the "strictSize" property.
         */
        public PlanningTable.Builder<_B> withStrictSize(final StrictSize strictSize) {
            this.strictSize = ((strictSize == null)?null:new StrictSize.Builder<PlanningTable.Builder<_B>>(this, strictSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends PlanningTable.Builder<_B>> withStrictSize() {
            if (this.strictSize!= null) {
                return this.strictSize;
            }
            return this.strictSize = new StrictSize.Builder<PlanningTable.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "font" (any previous value will be replaced)
         * 
         * @param font
         *     New value of the "font" property.
         */
        public PlanningTable.Builder<_B> withFont(final Font font) {
            this.font = ((font == null)?null:new Font.Builder<PlanningTable.Builder<_B>>(this, font, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "font" property.
         * Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "font" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.Font.Builder#end()} to return to the current builder.
         */
        public Font.Builder<? extends PlanningTable.Builder<_B>> withFont() {
            if (this.font!= null) {
                return this.font;
            }
            return this.font = new Font.Builder<PlanningTable.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public PlanningTable.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public PlanningTable.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "planningTable" (any previous value will be replaced)
         * 
         * @param planningTable
         *     New value of the "planningTable" property.
         */
        public PlanningTable.Builder<_B> withPlanningTable(final String planningTable) {
            this.planningTable = planningTable;
            return this;
        }

        /**
         * Sets the new value of "foreignkeyfieldToParent" (any previous value will be replaced)
         * 
         * @param foreignkeyfieldToParent
         *     New value of the "foreignkeyfieldToParent" property.
         */
        public PlanningTable.Builder<_B> withForeignkeyfieldToParent(final String foreignkeyfieldToParent) {
            this.foreignkeyfieldToParent = foreignkeyfieldToParent;
            return this;
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        public PlanningTable.Builder<_B> withEnabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the new value of "dateFrom" (any previous value will be replaced)
         * 
         * @param dateFrom
         *     New value of the "dateFrom" property.
         */
        public PlanningTable.Builder<_B> withDateFrom(final String dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        /**
         * Sets the new value of "dateUntil" (any previous value will be replaced)
         * 
         * @param dateUntil
         *     New value of the "dateUntil" property.
         */
        public PlanningTable.Builder<_B> withDateUntil(final String dateUntil) {
            this.dateUntil = dateUntil;
            return this;
        }

        /**
         * Sets the new value of "fieldDateFrom" (any previous value will be replaced)
         * 
         * @param fieldDateFrom
         *     New value of the "fieldDateFrom" property.
         */
        public PlanningTable.Builder<_B> withFieldDateFrom(final String fieldDateFrom) {
            this.fieldDateFrom = fieldDateFrom;
            return this;
        }

        /**
         * Sets the new value of "fieldDateUntil" (any previous value will be replaced)
         * 
         * @param fieldDateUntil
         *     New value of the "fieldDateUntil" property.
         */
        public PlanningTable.Builder<_B> withFieldDateUntil(final String fieldDateUntil) {
            this.fieldDateUntil = fieldDateUntil;
            return this;
        }

        @Override
        public PlanningTable build() {
            if (_storedValue == null) {
                return this.init(new PlanningTable());
            } else {
                return ((PlanningTable) _storedValue);
            }
        }

        public PlanningTable.Builder<_B> copyOf(final PlanningTable _other) {
            _other.copyTo(this);
            return this;
        }

        public PlanningTable.Builder<_B> copyOf(final PlanningTable.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PlanningTable.Selector<PlanningTable.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PlanningTable.Select _root() {
            return new PlanningTable.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> layoutconstraints = null;
        private ClearBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> clearBorder = null;
        private LineBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> lineBorder = null;
        private EtchedBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> etchedBorder = null;
        private BevelBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> bevelBorder = null;
        private TitledBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> titledBorder = null;
        private EmptyBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> emptyBorder = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> border = null;
        private MinimumSize.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> minimumSize = null;
        private PreferredSize.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> preferredSize = null;
        private StrictSize.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> strictSize = null;
        private Font.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> font = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> description = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> planningTable = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> foreignkeyfieldToParent = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> enabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> dateFrom = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> dateUntil = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> fieldDateFrom = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> fieldDateUntil = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layoutconstraints!= null) {
                products.put("layoutconstraints", this.layoutconstraints.init());
            }
            if (this.clearBorder!= null) {
                products.put("clearBorder", this.clearBorder.init());
            }
            if (this.lineBorder!= null) {
                products.put("lineBorder", this.lineBorder.init());
            }
            if (this.etchedBorder!= null) {
                products.put("etchedBorder", this.etchedBorder.init());
            }
            if (this.bevelBorder!= null) {
                products.put("bevelBorder", this.bevelBorder.init());
            }
            if (this.titledBorder!= null) {
                products.put("titledBorder", this.titledBorder.init());
            }
            if (this.emptyBorder!= null) {
                products.put("emptyBorder", this.emptyBorder.init());
            }
            if (this.border!= null) {
                products.put("border", this.border.init());
            }
            if (this.minimumSize!= null) {
                products.put("minimumSize", this.minimumSize.init());
            }
            if (this.preferredSize!= null) {
                products.put("preferredSize", this.preferredSize.init());
            }
            if (this.strictSize!= null) {
                products.put("strictSize", this.strictSize.init());
            }
            if (this.font!= null) {
                products.put("font", this.font.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.planningTable!= null) {
                products.put("planningTable", this.planningTable.init());
            }
            if (this.foreignkeyfieldToParent!= null) {
                products.put("foreignkeyfieldToParent", this.foreignkeyfieldToParent.init());
            }
            if (this.enabled!= null) {
                products.put("enabled", this.enabled.init());
            }
            if (this.dateFrom!= null) {
                products.put("dateFrom", this.dateFrom.init());
            }
            if (this.dateUntil!= null) {
                products.put("dateUntil", this.dateUntil.init());
            }
            if (this.fieldDateFrom!= null) {
                products.put("fieldDateFrom", this.fieldDateFrom.init());
            }
            if (this.fieldDateUntil!= null) {
                products.put("fieldDateUntil", this.fieldDateUntil.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> layoutconstraints() {
            return ((this.layoutconstraints == null)?this.layoutconstraints = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "layoutconstraints"):this.layoutconstraints);
        }

        public ClearBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> clearBorder() {
            return ((this.clearBorder == null)?this.clearBorder = new ClearBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "clearBorder"):this.clearBorder);
        }

        public LineBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> lineBorder() {
            return ((this.lineBorder == null)?this.lineBorder = new LineBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "lineBorder"):this.lineBorder);
        }

        public EtchedBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> etchedBorder() {
            return ((this.etchedBorder == null)?this.etchedBorder = new EtchedBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "etchedBorder"):this.etchedBorder);
        }

        public BevelBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> bevelBorder() {
            return ((this.bevelBorder == null)?this.bevelBorder = new BevelBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "bevelBorder"):this.bevelBorder);
        }

        public TitledBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> titledBorder() {
            return ((this.titledBorder == null)?this.titledBorder = new TitledBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "titledBorder"):this.titledBorder);
        }

        public EmptyBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> emptyBorder() {
            return ((this.emptyBorder == null)?this.emptyBorder = new EmptyBorder.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "emptyBorder"):this.emptyBorder);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> border() {
            return ((this.border == null)?this.border = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "border"):this.border);
        }

        public MinimumSize.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> minimumSize() {
            return ((this.minimumSize == null)?this.minimumSize = new MinimumSize.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "minimumSize"):this.minimumSize);
        }

        public PreferredSize.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> preferredSize() {
            return ((this.preferredSize == null)?this.preferredSize = new PreferredSize.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "preferredSize"):this.preferredSize);
        }

        public StrictSize.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> strictSize() {
            return ((this.strictSize == null)?this.strictSize = new StrictSize.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "strictSize"):this.strictSize);
        }

        public Font.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> font() {
            return ((this.font == null)?this.font = new Font.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "font"):this.font);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> planningTable() {
            return ((this.planningTable == null)?this.planningTable = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "planningTable"):this.planningTable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> foreignkeyfieldToParent() {
            return ((this.foreignkeyfieldToParent == null)?this.foreignkeyfieldToParent = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "foreignkeyfieldToParent"):this.foreignkeyfieldToParent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> enabled() {
            return ((this.enabled == null)?this.enabled = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "enabled"):this.enabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> dateFrom() {
            return ((this.dateFrom == null)?this.dateFrom = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "dateFrom"):this.dateFrom);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> dateUntil() {
            return ((this.dateUntil == null)?this.dateUntil = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "dateUntil"):this.dateUntil);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> fieldDateFrom() {
            return ((this.fieldDateFrom == null)?this.fieldDateFrom = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "fieldDateFrom"):this.fieldDateFrom);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> fieldDateUntil() {
            return ((this.fieldDateUntil == null)?this.fieldDateUntil = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "fieldDateUntil"):this.fieldDateUntil);
        }

    }

}
