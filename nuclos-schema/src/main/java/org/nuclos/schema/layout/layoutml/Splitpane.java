package org.nuclos.schema.layout.layoutml;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{}layoutconstraints" minOccurs="0"/&gt;
 *         &lt;group ref="{}borders"/&gt;
 *         &lt;group ref="{}sizes"/&gt;
 *         &lt;element ref="{}container" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="orientation" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *             &lt;enumeration value="horizontal"/&gt;
 *             &lt;enumeration value="vertical"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="dividersize" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="resizeweight" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="expandable" type="{}boolean" /&gt;
 *       &lt;attribute name="continuous-layout" type="{}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "layoutconstraints",
    "clearBorder",
    "lineBorder",
    "etchedBorder",
    "bevelBorder",
    "titledBorder",
    "emptyBorder",
    "border",
    "minimumSize",
    "preferredSize",
    "strictSize",
    "container"
})
public class Splitpane implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "layoutconstraints", type = JAXBElement.class, required = false)
    protected JAXBElement<?> layoutconstraints;
    @XmlElement(name = "clear-border")
    protected ClearBorder clearBorder;
    @XmlElement(name = "line-border")
    protected LineBorder lineBorder;
    @XmlElement(name = "etched-border")
    protected EtchedBorder etchedBorder;
    @XmlElement(name = "bevel-border")
    protected BevelBorder bevelBorder;
    @XmlElement(name = "titled-border")
    protected TitledBorder titledBorder;
    @XmlElement(name = "empty-border")
    protected EmptyBorder emptyBorder;
    protected List<Object> border;
    @XmlElement(name = "minimum-size")
    protected MinimumSize minimumSize;
    @XmlElement(name = "preferred-size")
    protected PreferredSize preferredSize;
    @XmlElement(name = "strict-size")
    protected StrictSize strictSize;
    @XmlElementRef(name = "container", type = JAXBElement.class, required = false)
    protected List<JAXBElement<?>> container;
    @XmlAttribute(name = "name")
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "orientation", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String orientation;
    @XmlAttribute(name = "dividersize")
    @XmlSchemaType(name = "anySimpleType")
    protected String dividersize;
    @XmlAttribute(name = "resizeweight")
    @XmlSchemaType(name = "anySimpleType")
    protected String resizeweight;
    @XmlAttribute(name = "expandable")
    protected Boolean expandable;
    @XmlAttribute(name = "continuous-layout")
    protected Boolean continuousLayout;

    /**
     * Gets the value of the layoutconstraints property.
     * 
     * @return
     *     possible object is: Row(s) removed due to missing sorting of JAXBElement java doc generation
     *     
     */
    public JAXBElement<?> getLayoutconstraints() {
        return layoutconstraints;
    }

    /**
     * Sets the value of the layoutconstraints property.
     * 
     * @param value
     *     allowed object is: Row(s) removed due to missing sorting of JAXBElement java doc generation
     *     
     */
    public void setLayoutconstraints(JAXBElement<?> value) {
        this.layoutconstraints = value;
    }

    /**
     * Gets the value of the clearBorder property.
     * 
     * @return
     *     possible object is
     *     {@link ClearBorder }
     *     
     */
    public ClearBorder getClearBorder() {
        return clearBorder;
    }

    /**
     * Sets the value of the clearBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClearBorder }
     *     
     */
    public void setClearBorder(ClearBorder value) {
        this.clearBorder = value;
    }

    /**
     * Gets the value of the lineBorder property.
     * 
     * @return
     *     possible object is
     *     {@link LineBorder }
     *     
     */
    public LineBorder getLineBorder() {
        return lineBorder;
    }

    /**
     * Sets the value of the lineBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineBorder }
     *     
     */
    public void setLineBorder(LineBorder value) {
        this.lineBorder = value;
    }

    /**
     * Gets the value of the etchedBorder property.
     * 
     * @return
     *     possible object is
     *     {@link EtchedBorder }
     *     
     */
    public EtchedBorder getEtchedBorder() {
        return etchedBorder;
    }

    /**
     * Sets the value of the etchedBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtchedBorder }
     *     
     */
    public void setEtchedBorder(EtchedBorder value) {
        this.etchedBorder = value;
    }

    /**
     * Gets the value of the bevelBorder property.
     * 
     * @return
     *     possible object is
     *     {@link BevelBorder }
     *     
     */
    public BevelBorder getBevelBorder() {
        return bevelBorder;
    }

    /**
     * Sets the value of the bevelBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link BevelBorder }
     *     
     */
    public void setBevelBorder(BevelBorder value) {
        this.bevelBorder = value;
    }

    /**
     * Gets the value of the titledBorder property.
     * 
     * @return
     *     possible object is
     *     {@link TitledBorder }
     *     
     */
    public TitledBorder getTitledBorder() {
        return titledBorder;
    }

    /**
     * Sets the value of the titledBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link TitledBorder }
     *     
     */
    public void setTitledBorder(TitledBorder value) {
        this.titledBorder = value;
    }

    /**
     * Gets the value of the emptyBorder property.
     * 
     * @return
     *     possible object is
     *     {@link EmptyBorder }
     *     
     */
    public EmptyBorder getEmptyBorder() {
        return emptyBorder;
    }

    /**
     * Sets the value of the emptyBorder property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmptyBorder }
     *     
     */
    public void setEmptyBorder(EmptyBorder value) {
        this.emptyBorder = value;
    }

    /**
     * Gets the value of the border property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the border property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBorder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getBorder() {
        if (border == null) {
            border = new ArrayList<Object>();
        }
        return this.border;
    }

    /**
     * Gets the value of the minimumSize property.
     * 
     * @return
     *     possible object is
     *     {@link MinimumSize }
     *     
     */
    public MinimumSize getMinimumSize() {
        return minimumSize;
    }

    /**
     * Sets the value of the minimumSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link MinimumSize }
     *     
     */
    public void setMinimumSize(MinimumSize value) {
        this.minimumSize = value;
    }

    /**
     * Gets the value of the preferredSize property.
     * 
     * @return
     *     possible object is
     *     {@link PreferredSize }
     *     
     */
    public PreferredSize getPreferredSize() {
        return preferredSize;
    }

    /**
     * Sets the value of the preferredSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreferredSize }
     *     
     */
    public void setPreferredSize(PreferredSize value) {
        this.preferredSize = value;
    }

    /**
     * Gets the value of the strictSize property.
     * 
     * @return
     *     possible object is
     *     {@link StrictSize }
     *     
     */
    public StrictSize getStrictSize() {
        return strictSize;
    }

    /**
     * Sets the value of the strictSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link StrictSize }
     *     
     */
    public void setStrictSize(StrictSize value) {
        this.strictSize = value;
    }

    /**
     * Gets the value of the container property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the container property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContainer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list: Row(s) removed due to missing sorting of JAXBElement java doc generation
     * 
     * 
     */
    public List<JAXBElement<?>> getContainer() {
        if (container == null) {
            container = new ArrayList<JAXBElement<?>>();
        }
        return this.container;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the orientation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrientation() {
        return orientation;
    }

    /**
     * Sets the value of the orientation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrientation(String value) {
        this.orientation = value;
    }

    /**
     * Gets the value of the dividersize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDividersize() {
        return dividersize;
    }

    /**
     * Sets the value of the dividersize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDividersize(String value) {
        this.dividersize = value;
    }

    /**
     * Gets the value of the resizeweight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResizeweight() {
        return resizeweight;
    }

    /**
     * Sets the value of the resizeweight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResizeweight(String value) {
        this.resizeweight = value;
    }

    /**
     * Gets the value of the expandable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getExpandable() {
        return expandable;
    }

    /**
     * Sets the value of the expandable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpandable(Boolean value) {
        this.expandable = value;
    }

    /**
     * Gets the value of the continuousLayout property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getContinuousLayout() {
        return continuousLayout;
    }

    /**
     * Sets the value of the continuousLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContinuousLayout(Boolean value) {
        this.continuousLayout = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            strategy.appendField(locator, this, "layoutconstraints", buffer, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            strategy.appendField(locator, this, "clearBorder", buffer, theClearBorder);
        }
        {
            LineBorder theLineBorder;
            theLineBorder = this.getLineBorder();
            strategy.appendField(locator, this, "lineBorder", buffer, theLineBorder);
        }
        {
            EtchedBorder theEtchedBorder;
            theEtchedBorder = this.getEtchedBorder();
            strategy.appendField(locator, this, "etchedBorder", buffer, theEtchedBorder);
        }
        {
            BevelBorder theBevelBorder;
            theBevelBorder = this.getBevelBorder();
            strategy.appendField(locator, this, "bevelBorder", buffer, theBevelBorder);
        }
        {
            TitledBorder theTitledBorder;
            theTitledBorder = this.getTitledBorder();
            strategy.appendField(locator, this, "titledBorder", buffer, theTitledBorder);
        }
        {
            EmptyBorder theEmptyBorder;
            theEmptyBorder = this.getEmptyBorder();
            strategy.appendField(locator, this, "emptyBorder", buffer, theEmptyBorder);
        }
        {
            List<Object> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            strategy.appendField(locator, this, "border", buffer, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            strategy.appendField(locator, this, "minimumSize", buffer, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            strategy.appendField(locator, this, "preferredSize", buffer, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            strategy.appendField(locator, this, "strictSize", buffer, theStrictSize);
        }
        {
            List<JAXBElement<?>> theContainer;
            theContainer = (((this.container!= null)&&(!this.container.isEmpty()))?this.getContainer():null);
            strategy.appendField(locator, this, "container", buffer, theContainer);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theOrientation;
            theOrientation = this.getOrientation();
            strategy.appendField(locator, this, "orientation", buffer, theOrientation);
        }
        {
            String theDividersize;
            theDividersize = this.getDividersize();
            strategy.appendField(locator, this, "dividersize", buffer, theDividersize);
        }
        {
            String theResizeweight;
            theResizeweight = this.getResizeweight();
            strategy.appendField(locator, this, "resizeweight", buffer, theResizeweight);
        }
        {
            Boolean theExpandable;
            theExpandable = this.getExpandable();
            strategy.appendField(locator, this, "expandable", buffer, theExpandable);
        }
        {
            Boolean theContinuousLayout;
            theContinuousLayout = this.getContinuousLayout();
            strategy.appendField(locator, this, "continuousLayout", buffer, theContinuousLayout);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Splitpane)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Splitpane that = ((Splitpane) object);
        {
            JAXBElement<?> lhsLayoutconstraints;
            lhsLayoutconstraints = this.getLayoutconstraints();
            JAXBElement<?> rhsLayoutconstraints;
            rhsLayoutconstraints = that.getLayoutconstraints();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "layoutconstraints", lhsLayoutconstraints), LocatorUtils.property(thatLocator, "layoutconstraints", rhsLayoutconstraints), lhsLayoutconstraints, rhsLayoutconstraints)) {
                return false;
            }
        }
        {
            ClearBorder lhsClearBorder;
            lhsClearBorder = this.getClearBorder();
            ClearBorder rhsClearBorder;
            rhsClearBorder = that.getClearBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearBorder", lhsClearBorder), LocatorUtils.property(thatLocator, "clearBorder", rhsClearBorder), lhsClearBorder, rhsClearBorder)) {
                return false;
            }
        }
        {
            LineBorder lhsLineBorder;
            lhsLineBorder = this.getLineBorder();
            LineBorder rhsLineBorder;
            rhsLineBorder = that.getLineBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lineBorder", lhsLineBorder), LocatorUtils.property(thatLocator, "lineBorder", rhsLineBorder), lhsLineBorder, rhsLineBorder)) {
                return false;
            }
        }
        {
            EtchedBorder lhsEtchedBorder;
            lhsEtchedBorder = this.getEtchedBorder();
            EtchedBorder rhsEtchedBorder;
            rhsEtchedBorder = that.getEtchedBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "etchedBorder", lhsEtchedBorder), LocatorUtils.property(thatLocator, "etchedBorder", rhsEtchedBorder), lhsEtchedBorder, rhsEtchedBorder)) {
                return false;
            }
        }
        {
            BevelBorder lhsBevelBorder;
            lhsBevelBorder = this.getBevelBorder();
            BevelBorder rhsBevelBorder;
            rhsBevelBorder = that.getBevelBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bevelBorder", lhsBevelBorder), LocatorUtils.property(thatLocator, "bevelBorder", rhsBevelBorder), lhsBevelBorder, rhsBevelBorder)) {
                return false;
            }
        }
        {
            TitledBorder lhsTitledBorder;
            lhsTitledBorder = this.getTitledBorder();
            TitledBorder rhsTitledBorder;
            rhsTitledBorder = that.getTitledBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "titledBorder", lhsTitledBorder), LocatorUtils.property(thatLocator, "titledBorder", rhsTitledBorder), lhsTitledBorder, rhsTitledBorder)) {
                return false;
            }
        }
        {
            EmptyBorder lhsEmptyBorder;
            lhsEmptyBorder = this.getEmptyBorder();
            EmptyBorder rhsEmptyBorder;
            rhsEmptyBorder = that.getEmptyBorder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "emptyBorder", lhsEmptyBorder), LocatorUtils.property(thatLocator, "emptyBorder", rhsEmptyBorder), lhsEmptyBorder, rhsEmptyBorder)) {
                return false;
            }
        }
        {
            List<Object> lhsBorder;
            lhsBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            List<Object> rhsBorder;
            rhsBorder = (((that.border!= null)&&(!that.border.isEmpty()))?that.getBorder():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "border", lhsBorder), LocatorUtils.property(thatLocator, "border", rhsBorder), lhsBorder, rhsBorder)) {
                return false;
            }
        }
        {
            MinimumSize lhsMinimumSize;
            lhsMinimumSize = this.getMinimumSize();
            MinimumSize rhsMinimumSize;
            rhsMinimumSize = that.getMinimumSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumSize", lhsMinimumSize), LocatorUtils.property(thatLocator, "minimumSize", rhsMinimumSize), lhsMinimumSize, rhsMinimumSize)) {
                return false;
            }
        }
        {
            PreferredSize lhsPreferredSize;
            lhsPreferredSize = this.getPreferredSize();
            PreferredSize rhsPreferredSize;
            rhsPreferredSize = that.getPreferredSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredSize", lhsPreferredSize), LocatorUtils.property(thatLocator, "preferredSize", rhsPreferredSize), lhsPreferredSize, rhsPreferredSize)) {
                return false;
            }
        }
        {
            StrictSize lhsStrictSize;
            lhsStrictSize = this.getStrictSize();
            StrictSize rhsStrictSize;
            rhsStrictSize = that.getStrictSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "strictSize", lhsStrictSize), LocatorUtils.property(thatLocator, "strictSize", rhsStrictSize), lhsStrictSize, rhsStrictSize)) {
                return false;
            }
        }
        {
            List<JAXBElement<?>> lhsContainer;
            lhsContainer = (((this.container!= null)&&(!this.container.isEmpty()))?this.getContainer():null);
            List<JAXBElement<?>> rhsContainer;
            rhsContainer = (((that.container!= null)&&(!that.container.isEmpty()))?that.getContainer():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "container", lhsContainer), LocatorUtils.property(thatLocator, "container", rhsContainer), lhsContainer, rhsContainer)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsOrientation;
            lhsOrientation = this.getOrientation();
            String rhsOrientation;
            rhsOrientation = that.getOrientation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orientation", lhsOrientation), LocatorUtils.property(thatLocator, "orientation", rhsOrientation), lhsOrientation, rhsOrientation)) {
                return false;
            }
        }
        {
            String lhsDividersize;
            lhsDividersize = this.getDividersize();
            String rhsDividersize;
            rhsDividersize = that.getDividersize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dividersize", lhsDividersize), LocatorUtils.property(thatLocator, "dividersize", rhsDividersize), lhsDividersize, rhsDividersize)) {
                return false;
            }
        }
        {
            String lhsResizeweight;
            lhsResizeweight = this.getResizeweight();
            String rhsResizeweight;
            rhsResizeweight = that.getResizeweight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resizeweight", lhsResizeweight), LocatorUtils.property(thatLocator, "resizeweight", rhsResizeweight), lhsResizeweight, rhsResizeweight)) {
                return false;
            }
        }
        {
            Boolean lhsExpandable;
            lhsExpandable = this.getExpandable();
            Boolean rhsExpandable;
            rhsExpandable = that.getExpandable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expandable", lhsExpandable), LocatorUtils.property(thatLocator, "expandable", rhsExpandable), lhsExpandable, rhsExpandable)) {
                return false;
            }
        }
        {
            Boolean lhsContinuousLayout;
            lhsContinuousLayout = this.getContinuousLayout();
            Boolean rhsContinuousLayout;
            rhsContinuousLayout = that.getContinuousLayout();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "continuousLayout", lhsContinuousLayout), LocatorUtils.property(thatLocator, "continuousLayout", rhsContinuousLayout), lhsContinuousLayout, rhsContinuousLayout)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            JAXBElement<?> theLayoutconstraints;
            theLayoutconstraints = this.getLayoutconstraints();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "layoutconstraints", theLayoutconstraints), currentHashCode, theLayoutconstraints);
        }
        {
            ClearBorder theClearBorder;
            theClearBorder = this.getClearBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearBorder", theClearBorder), currentHashCode, theClearBorder);
        }
        {
            LineBorder theLineBorder;
            theLineBorder = this.getLineBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "lineBorder", theLineBorder), currentHashCode, theLineBorder);
        }
        {
            EtchedBorder theEtchedBorder;
            theEtchedBorder = this.getEtchedBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "etchedBorder", theEtchedBorder), currentHashCode, theEtchedBorder);
        }
        {
            BevelBorder theBevelBorder;
            theBevelBorder = this.getBevelBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bevelBorder", theBevelBorder), currentHashCode, theBevelBorder);
        }
        {
            TitledBorder theTitledBorder;
            theTitledBorder = this.getTitledBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "titledBorder", theTitledBorder), currentHashCode, theTitledBorder);
        }
        {
            EmptyBorder theEmptyBorder;
            theEmptyBorder = this.getEmptyBorder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "emptyBorder", theEmptyBorder), currentHashCode, theEmptyBorder);
        }
        {
            List<Object> theBorder;
            theBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "border", theBorder), currentHashCode, theBorder);
        }
        {
            MinimumSize theMinimumSize;
            theMinimumSize = this.getMinimumSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumSize", theMinimumSize), currentHashCode, theMinimumSize);
        }
        {
            PreferredSize thePreferredSize;
            thePreferredSize = this.getPreferredSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredSize", thePreferredSize), currentHashCode, thePreferredSize);
        }
        {
            StrictSize theStrictSize;
            theStrictSize = this.getStrictSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "strictSize", theStrictSize), currentHashCode, theStrictSize);
        }
        {
            List<JAXBElement<?>> theContainer;
            theContainer = (((this.container!= null)&&(!this.container.isEmpty()))?this.getContainer():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "container", theContainer), currentHashCode, theContainer);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theOrientation;
            theOrientation = this.getOrientation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orientation", theOrientation), currentHashCode, theOrientation);
        }
        {
            String theDividersize;
            theDividersize = this.getDividersize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dividersize", theDividersize), currentHashCode, theDividersize);
        }
        {
            String theResizeweight;
            theResizeweight = this.getResizeweight();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resizeweight", theResizeweight), currentHashCode, theResizeweight);
        }
        {
            Boolean theExpandable;
            theExpandable = this.getExpandable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "expandable", theExpandable), currentHashCode, theExpandable);
        }
        {
            Boolean theContinuousLayout;
            theContinuousLayout = this.getContinuousLayout();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "continuousLayout", theContinuousLayout), currentHashCode, theContinuousLayout);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Splitpane) {
            final Splitpane copy = ((Splitpane) draftCopy);
            if (this.layoutconstraints!= null) {
                JAXBElement<?> sourceLayoutconstraints;
                sourceLayoutconstraints = this.getLayoutconstraints();
                @SuppressWarnings("unchecked")
                JAXBElement<?> copyLayoutconstraints = ((JAXBElement<?> ) strategy.copy(LocatorUtils.property(locator, "layoutconstraints", sourceLayoutconstraints), sourceLayoutconstraints));
                copy.setLayoutconstraints(copyLayoutconstraints);
            } else {
                copy.layoutconstraints = null;
            }
            if (this.clearBorder!= null) {
                ClearBorder sourceClearBorder;
                sourceClearBorder = this.getClearBorder();
                ClearBorder copyClearBorder = ((ClearBorder) strategy.copy(LocatorUtils.property(locator, "clearBorder", sourceClearBorder), sourceClearBorder));
                copy.setClearBorder(copyClearBorder);
            } else {
                copy.clearBorder = null;
            }
            if (this.lineBorder!= null) {
                LineBorder sourceLineBorder;
                sourceLineBorder = this.getLineBorder();
                LineBorder copyLineBorder = ((LineBorder) strategy.copy(LocatorUtils.property(locator, "lineBorder", sourceLineBorder), sourceLineBorder));
                copy.setLineBorder(copyLineBorder);
            } else {
                copy.lineBorder = null;
            }
            if (this.etchedBorder!= null) {
                EtchedBorder sourceEtchedBorder;
                sourceEtchedBorder = this.getEtchedBorder();
                EtchedBorder copyEtchedBorder = ((EtchedBorder) strategy.copy(LocatorUtils.property(locator, "etchedBorder", sourceEtchedBorder), sourceEtchedBorder));
                copy.setEtchedBorder(copyEtchedBorder);
            } else {
                copy.etchedBorder = null;
            }
            if (this.bevelBorder!= null) {
                BevelBorder sourceBevelBorder;
                sourceBevelBorder = this.getBevelBorder();
                BevelBorder copyBevelBorder = ((BevelBorder) strategy.copy(LocatorUtils.property(locator, "bevelBorder", sourceBevelBorder), sourceBevelBorder));
                copy.setBevelBorder(copyBevelBorder);
            } else {
                copy.bevelBorder = null;
            }
            if (this.titledBorder!= null) {
                TitledBorder sourceTitledBorder;
                sourceTitledBorder = this.getTitledBorder();
                TitledBorder copyTitledBorder = ((TitledBorder) strategy.copy(LocatorUtils.property(locator, "titledBorder", sourceTitledBorder), sourceTitledBorder));
                copy.setTitledBorder(copyTitledBorder);
            } else {
                copy.titledBorder = null;
            }
            if (this.emptyBorder!= null) {
                EmptyBorder sourceEmptyBorder;
                sourceEmptyBorder = this.getEmptyBorder();
                EmptyBorder copyEmptyBorder = ((EmptyBorder) strategy.copy(LocatorUtils.property(locator, "emptyBorder", sourceEmptyBorder), sourceEmptyBorder));
                copy.setEmptyBorder(copyEmptyBorder);
            } else {
                copy.emptyBorder = null;
            }
            if ((this.border!= null)&&(!this.border.isEmpty())) {
                List<Object> sourceBorder;
                sourceBorder = (((this.border!= null)&&(!this.border.isEmpty()))?this.getBorder():null);
                @SuppressWarnings("unchecked")
                List<Object> copyBorder = ((List<Object> ) strategy.copy(LocatorUtils.property(locator, "border", sourceBorder), sourceBorder));
                copy.border = null;
                if (copyBorder!= null) {
                    List<Object> uniqueBorderl = copy.getBorder();
                    uniqueBorderl.addAll(copyBorder);
                }
            } else {
                copy.border = null;
            }
            if (this.minimumSize!= null) {
                MinimumSize sourceMinimumSize;
                sourceMinimumSize = this.getMinimumSize();
                MinimumSize copyMinimumSize = ((MinimumSize) strategy.copy(LocatorUtils.property(locator, "minimumSize", sourceMinimumSize), sourceMinimumSize));
                copy.setMinimumSize(copyMinimumSize);
            } else {
                copy.minimumSize = null;
            }
            if (this.preferredSize!= null) {
                PreferredSize sourcePreferredSize;
                sourcePreferredSize = this.getPreferredSize();
                PreferredSize copyPreferredSize = ((PreferredSize) strategy.copy(LocatorUtils.property(locator, "preferredSize", sourcePreferredSize), sourcePreferredSize));
                copy.setPreferredSize(copyPreferredSize);
            } else {
                copy.preferredSize = null;
            }
            if (this.strictSize!= null) {
                StrictSize sourceStrictSize;
                sourceStrictSize = this.getStrictSize();
                StrictSize copyStrictSize = ((StrictSize) strategy.copy(LocatorUtils.property(locator, "strictSize", sourceStrictSize), sourceStrictSize));
                copy.setStrictSize(copyStrictSize);
            } else {
                copy.strictSize = null;
            }
            if ((this.container!= null)&&(!this.container.isEmpty())) {
                List<JAXBElement<?>> sourceContainer;
                sourceContainer = (((this.container!= null)&&(!this.container.isEmpty()))?this.getContainer():null);
                @SuppressWarnings("unchecked")
                List<JAXBElement<?>> copyContainer = ((List<JAXBElement<?>> ) strategy.copy(LocatorUtils.property(locator, "container", sourceContainer), sourceContainer));
                copy.container = null;
                if (copyContainer!= null) {
                    List<JAXBElement<?>> uniqueContainerl = copy.getContainer();
                    uniqueContainerl.addAll(copyContainer);
                }
            } else {
                copy.container = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.orientation!= null) {
                String sourceOrientation;
                sourceOrientation = this.getOrientation();
                String copyOrientation = ((String) strategy.copy(LocatorUtils.property(locator, "orientation", sourceOrientation), sourceOrientation));
                copy.setOrientation(copyOrientation);
            } else {
                copy.orientation = null;
            }
            if (this.dividersize!= null) {
                String sourceDividersize;
                sourceDividersize = this.getDividersize();
                String copyDividersize = ((String) strategy.copy(LocatorUtils.property(locator, "dividersize", sourceDividersize), sourceDividersize));
                copy.setDividersize(copyDividersize);
            } else {
                copy.dividersize = null;
            }
            if (this.resizeweight!= null) {
                String sourceResizeweight;
                sourceResizeweight = this.getResizeweight();
                String copyResizeweight = ((String) strategy.copy(LocatorUtils.property(locator, "resizeweight", sourceResizeweight), sourceResizeweight));
                copy.setResizeweight(copyResizeweight);
            } else {
                copy.resizeweight = null;
            }
            if (this.expandable!= null) {
                Boolean sourceExpandable;
                sourceExpandable = this.getExpandable();
                Boolean copyExpandable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "expandable", sourceExpandable), sourceExpandable));
                copy.setExpandable(copyExpandable);
            } else {
                copy.expandable = null;
            }
            if (this.continuousLayout!= null) {
                Boolean sourceContinuousLayout;
                sourceContinuousLayout = this.getContinuousLayout();
                Boolean copyContinuousLayout = ((Boolean) strategy.copy(LocatorUtils.property(locator, "continuousLayout", sourceContinuousLayout), sourceContinuousLayout));
                copy.setContinuousLayout(copyContinuousLayout);
            } else {
                copy.continuousLayout = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Splitpane();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Splitpane.Builder<_B> _other) {
        _other.layoutconstraints = this.layoutconstraints;
        _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other));
        _other.lineBorder = ((this.lineBorder == null)?null:this.lineBorder.newCopyBuilder(_other));
        _other.etchedBorder = ((this.etchedBorder == null)?null:this.etchedBorder.newCopyBuilder(_other));
        _other.bevelBorder = ((this.bevelBorder == null)?null:this.bevelBorder.newCopyBuilder(_other));
        _other.titledBorder = ((this.titledBorder == null)?null:this.titledBorder.newCopyBuilder(_other));
        _other.emptyBorder = ((this.emptyBorder == null)?null:this.emptyBorder.newCopyBuilder(_other));
        if (this.border == null) {
            _other.border = null;
        } else {
            _other.border = new ArrayList<Buildable>();
            for (Object _item: this.border) {
                _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other));
        _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other));
        _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other));
        if (this.container == null) {
            _other.container = null;
        } else {
            _other.container = new ArrayList<Buildable>();
            for (JAXBElement<?> _item: this.container) {
                _other.container.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.name = this.name;
        _other.orientation = this.orientation;
        _other.dividersize = this.dividersize;
        _other.resizeweight = this.resizeweight;
        _other.expandable = this.expandable;
        _other.continuousLayout = this.continuousLayout;
    }

    public<_B >Splitpane.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Splitpane.Builder<_B>(_parentBuilder, this, true);
    }

    public Splitpane.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Splitpane.Builder<Void> builder() {
        return new Splitpane.Builder<Void>(null, null, false);
    }

    public static<_B >Splitpane.Builder<_B> copyOf(final Splitpane _other) {
        final Splitpane.Builder<_B> _newBuilder = new Splitpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Splitpane.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
            _other.layoutconstraints = this.layoutconstraints;
        }
        final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
            _other.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.newCopyBuilder(_other, clearBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree lineBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lineBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(lineBorderPropertyTree!= null):((lineBorderPropertyTree == null)||(!lineBorderPropertyTree.isLeaf())))) {
            _other.lineBorder = ((this.lineBorder == null)?null:this.lineBorder.newCopyBuilder(_other, lineBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree etchedBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("etchedBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(etchedBorderPropertyTree!= null):((etchedBorderPropertyTree == null)||(!etchedBorderPropertyTree.isLeaf())))) {
            _other.etchedBorder = ((this.etchedBorder == null)?null:this.etchedBorder.newCopyBuilder(_other, etchedBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree bevelBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bevelBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bevelBorderPropertyTree!= null):((bevelBorderPropertyTree == null)||(!bevelBorderPropertyTree.isLeaf())))) {
            _other.bevelBorder = ((this.bevelBorder == null)?null:this.bevelBorder.newCopyBuilder(_other, bevelBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree titledBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("titledBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titledBorderPropertyTree!= null):((titledBorderPropertyTree == null)||(!titledBorderPropertyTree.isLeaf())))) {
            _other.titledBorder = ((this.titledBorder == null)?null:this.titledBorder.newCopyBuilder(_other, titledBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree emptyBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("emptyBorder"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(emptyBorderPropertyTree!= null):((emptyBorderPropertyTree == null)||(!emptyBorderPropertyTree.isLeaf())))) {
            _other.emptyBorder = ((this.emptyBorder == null)?null:this.emptyBorder.newCopyBuilder(_other, emptyBorderPropertyTree, _propertyTreeUse));
        }
        final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
            if (this.border == null) {
                _other.border = null;
            } else {
                _other.border = new ArrayList<Buildable>();
                for (Object _item: this.border) {
                    _other.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
            _other.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.newCopyBuilder(_other, minimumSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
            _other.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.newCopyBuilder(_other, preferredSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
            _other.strictSize = ((this.strictSize == null)?null:this.strictSize.newCopyBuilder(_other, strictSizePropertyTree, _propertyTreeUse));
        }
        final PropertyTree containerPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("container"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(containerPropertyTree!= null):((containerPropertyTree == null)||(!containerPropertyTree.isLeaf())))) {
            if (this.container == null) {
                _other.container = null;
            } else {
                _other.container = new ArrayList<Buildable>();
                for (JAXBElement<?> _item: this.container) {
                    _other.container.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree orientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("orientation"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orientationPropertyTree!= null):((orientationPropertyTree == null)||(!orientationPropertyTree.isLeaf())))) {
            _other.orientation = this.orientation;
        }
        final PropertyTree dividersizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dividersize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dividersizePropertyTree!= null):((dividersizePropertyTree == null)||(!dividersizePropertyTree.isLeaf())))) {
            _other.dividersize = this.dividersize;
        }
        final PropertyTree resizeweightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resizeweight"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resizeweightPropertyTree!= null):((resizeweightPropertyTree == null)||(!resizeweightPropertyTree.isLeaf())))) {
            _other.resizeweight = this.resizeweight;
        }
        final PropertyTree expandablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("expandable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(expandablePropertyTree!= null):((expandablePropertyTree == null)||(!expandablePropertyTree.isLeaf())))) {
            _other.expandable = this.expandable;
        }
        final PropertyTree continuousLayoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("continuousLayout"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(continuousLayoutPropertyTree!= null):((continuousLayoutPropertyTree == null)||(!continuousLayoutPropertyTree.isLeaf())))) {
            _other.continuousLayout = this.continuousLayout;
        }
    }

    public<_B >Splitpane.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Splitpane.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Splitpane.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Splitpane.Builder<_B> copyOf(final Splitpane _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Splitpane.Builder<_B> _newBuilder = new Splitpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Splitpane.Builder<Void> copyExcept(final Splitpane _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Splitpane.Builder<Void> copyOnly(final Splitpane _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Splitpane _storedValue;
        private JAXBElement<?> layoutconstraints;
        private ClearBorder.Builder<Splitpane.Builder<_B>> clearBorder;
        private LineBorder.Builder<Splitpane.Builder<_B>> lineBorder;
        private EtchedBorder.Builder<Splitpane.Builder<_B>> etchedBorder;
        private BevelBorder.Builder<Splitpane.Builder<_B>> bevelBorder;
        private TitledBorder.Builder<Splitpane.Builder<_B>> titledBorder;
        private EmptyBorder.Builder<Splitpane.Builder<_B>> emptyBorder;
        private List<Buildable> border;
        private MinimumSize.Builder<Splitpane.Builder<_B>> minimumSize;
        private PreferredSize.Builder<Splitpane.Builder<_B>> preferredSize;
        private StrictSize.Builder<Splitpane.Builder<_B>> strictSize;
        private List<Buildable> container;
        private String name;
        private String orientation;
        private String dividersize;
        private String resizeweight;
        private Boolean expandable;
        private Boolean continuousLayout;

        public Builder(final _B _parentBuilder, final Splitpane _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.layoutconstraints = _other.layoutconstraints;
                    this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this));
                    this.lineBorder = ((_other.lineBorder == null)?null:_other.lineBorder.newCopyBuilder(this));
                    this.etchedBorder = ((_other.etchedBorder == null)?null:_other.etchedBorder.newCopyBuilder(this));
                    this.bevelBorder = ((_other.bevelBorder == null)?null:_other.bevelBorder.newCopyBuilder(this));
                    this.titledBorder = ((_other.titledBorder == null)?null:_other.titledBorder.newCopyBuilder(this));
                    this.emptyBorder = ((_other.emptyBorder == null)?null:_other.emptyBorder.newCopyBuilder(this));
                    if (_other.border == null) {
                        this.border = null;
                    } else {
                        this.border = new ArrayList<Buildable>();
                        for (Object _item: _other.border) {
                            this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this));
                    this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this));
                    this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this));
                    if (_other.container == null) {
                        this.container = null;
                    } else {
                        this.container = new ArrayList<Buildable>();
                        for (JAXBElement<?> _item: _other.container) {
                            this.container.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.name = _other.name;
                    this.orientation = _other.orientation;
                    this.dividersize = _other.dividersize;
                    this.resizeweight = _other.resizeweight;
                    this.expandable = _other.expandable;
                    this.continuousLayout = _other.continuousLayout;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Splitpane _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree layoutconstraintsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("layoutconstraints"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(layoutconstraintsPropertyTree!= null):((layoutconstraintsPropertyTree == null)||(!layoutconstraintsPropertyTree.isLeaf())))) {
                        this.layoutconstraints = _other.layoutconstraints;
                    }
                    final PropertyTree clearBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearBorderPropertyTree!= null):((clearBorderPropertyTree == null)||(!clearBorderPropertyTree.isLeaf())))) {
                        this.clearBorder = ((_other.clearBorder == null)?null:_other.clearBorder.newCopyBuilder(this, clearBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree lineBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("lineBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(lineBorderPropertyTree!= null):((lineBorderPropertyTree == null)||(!lineBorderPropertyTree.isLeaf())))) {
                        this.lineBorder = ((_other.lineBorder == null)?null:_other.lineBorder.newCopyBuilder(this, lineBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree etchedBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("etchedBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(etchedBorderPropertyTree!= null):((etchedBorderPropertyTree == null)||(!etchedBorderPropertyTree.isLeaf())))) {
                        this.etchedBorder = ((_other.etchedBorder == null)?null:_other.etchedBorder.newCopyBuilder(this, etchedBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree bevelBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bevelBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bevelBorderPropertyTree!= null):((bevelBorderPropertyTree == null)||(!bevelBorderPropertyTree.isLeaf())))) {
                        this.bevelBorder = ((_other.bevelBorder == null)?null:_other.bevelBorder.newCopyBuilder(this, bevelBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree titledBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("titledBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titledBorderPropertyTree!= null):((titledBorderPropertyTree == null)||(!titledBorderPropertyTree.isLeaf())))) {
                        this.titledBorder = ((_other.titledBorder == null)?null:_other.titledBorder.newCopyBuilder(this, titledBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree emptyBorderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("emptyBorder"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(emptyBorderPropertyTree!= null):((emptyBorderPropertyTree == null)||(!emptyBorderPropertyTree.isLeaf())))) {
                        this.emptyBorder = ((_other.emptyBorder == null)?null:_other.emptyBorder.newCopyBuilder(this, emptyBorderPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree borderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("border"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyTree!= null):((borderPropertyTree == null)||(!borderPropertyTree.isLeaf())))) {
                        if (_other.border == null) {
                            this.border = null;
                        } else {
                            this.border = new ArrayList<Buildable>();
                            for (Object _item: _other.border) {
                                this.border.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree minimumSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumSizePropertyTree!= null):((minimumSizePropertyTree == null)||(!minimumSizePropertyTree.isLeaf())))) {
                        this.minimumSize = ((_other.minimumSize == null)?null:_other.minimumSize.newCopyBuilder(this, minimumSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree preferredSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredSizePropertyTree!= null):((preferredSizePropertyTree == null)||(!preferredSizePropertyTree.isLeaf())))) {
                        this.preferredSize = ((_other.preferredSize == null)?null:_other.preferredSize.newCopyBuilder(this, preferredSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree strictSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("strictSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(strictSizePropertyTree!= null):((strictSizePropertyTree == null)||(!strictSizePropertyTree.isLeaf())))) {
                        this.strictSize = ((_other.strictSize == null)?null:_other.strictSize.newCopyBuilder(this, strictSizePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree containerPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("container"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(containerPropertyTree!= null):((containerPropertyTree == null)||(!containerPropertyTree.isLeaf())))) {
                        if (_other.container == null) {
                            this.container = null;
                        } else {
                            this.container = new ArrayList<Buildable>();
                            for (JAXBElement<?> _item: _other.container) {
                                this.container.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree orientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("orientation"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orientationPropertyTree!= null):((orientationPropertyTree == null)||(!orientationPropertyTree.isLeaf())))) {
                        this.orientation = _other.orientation;
                    }
                    final PropertyTree dividersizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dividersize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dividersizePropertyTree!= null):((dividersizePropertyTree == null)||(!dividersizePropertyTree.isLeaf())))) {
                        this.dividersize = _other.dividersize;
                    }
                    final PropertyTree resizeweightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resizeweight"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resizeweightPropertyTree!= null):((resizeweightPropertyTree == null)||(!resizeweightPropertyTree.isLeaf())))) {
                        this.resizeweight = _other.resizeweight;
                    }
                    final PropertyTree expandablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("expandable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(expandablePropertyTree!= null):((expandablePropertyTree == null)||(!expandablePropertyTree.isLeaf())))) {
                        this.expandable = _other.expandable;
                    }
                    final PropertyTree continuousLayoutPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("continuousLayout"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(continuousLayoutPropertyTree!= null):((continuousLayoutPropertyTree == null)||(!continuousLayoutPropertyTree.isLeaf())))) {
                        this.continuousLayout = _other.continuousLayout;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Splitpane >_P init(final _P _product) {
            _product.layoutconstraints = this.layoutconstraints;
            _product.clearBorder = ((this.clearBorder == null)?null:this.clearBorder.build());
            _product.lineBorder = ((this.lineBorder == null)?null:this.lineBorder.build());
            _product.etchedBorder = ((this.etchedBorder == null)?null:this.etchedBorder.build());
            _product.bevelBorder = ((this.bevelBorder == null)?null:this.bevelBorder.build());
            _product.titledBorder = ((this.titledBorder == null)?null:this.titledBorder.build());
            _product.emptyBorder = ((this.emptyBorder == null)?null:this.emptyBorder.build());
            if (this.border!= null) {
                final List<Object> border = new ArrayList<Object>(this.border.size());
                for (Buildable _item: this.border) {
                    border.add(((Object) _item.build()));
                }
                _product.border = border;
            }
            _product.minimumSize = ((this.minimumSize == null)?null:this.minimumSize.build());
            _product.preferredSize = ((this.preferredSize == null)?null:this.preferredSize.build());
            _product.strictSize = ((this.strictSize == null)?null:this.strictSize.build());
            if (this.container!= null) {
                final List<JAXBElement<?>> container = new ArrayList<JAXBElement<?>>(this.container.size());
                for (Buildable _item: this.container) {
                    container.add(((JAXBElement<?> ) _item.build()));
                }
                _product.container = container;
            }
            _product.name = this.name;
            _product.orientation = this.orientation;
            _product.dividersize = this.dividersize;
            _product.resizeweight = this.resizeweight;
            _product.expandable = this.expandable;
            _product.continuousLayout = this.continuousLayout;
            return _product;
        }

        /**
         * Sets the new value of "layoutconstraints" (any previous value will be replaced)
         * 
         * @param layoutconstraints
         *     New value of the "layoutconstraints" property.
         */
        public Splitpane.Builder<_B> withLayoutconstraints(final JAXBElement<?> layoutconstraints) {
            this.layoutconstraints = layoutconstraints;
            return this;
        }

        /**
         * Sets the new value of "clearBorder" (any previous value will be replaced)
         * 
         * @param clearBorder
         *     New value of the "clearBorder" property.
         */
        public Splitpane.Builder<_B> withClearBorder(final ClearBorder clearBorder) {
            this.clearBorder = ((clearBorder == null)?null:new ClearBorder.Builder<Splitpane.Builder<_B>>(this, clearBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "clearBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "clearBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.ClearBorder.Builder#end()} to return to the current builder.
         */
        public ClearBorder.Builder<? extends Splitpane.Builder<_B>> withClearBorder() {
            if (this.clearBorder!= null) {
                return this.clearBorder;
            }
            return this.clearBorder = new ClearBorder.Builder<Splitpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "lineBorder" (any previous value will be replaced)
         * 
         * @param lineBorder
         *     New value of the "lineBorder" property.
         */
        public Splitpane.Builder<_B> withLineBorder(final LineBorder lineBorder) {
            this.lineBorder = ((lineBorder == null)?null:new LineBorder.Builder<Splitpane.Builder<_B>>(this, lineBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "lineBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.LineBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "lineBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.LineBorder.Builder#end()} to return to the current builder.
         */
        public LineBorder.Builder<? extends Splitpane.Builder<_B>> withLineBorder() {
            if (this.lineBorder!= null) {
                return this.lineBorder;
            }
            return this.lineBorder = new LineBorder.Builder<Splitpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "etchedBorder" (any previous value will be replaced)
         * 
         * @param etchedBorder
         *     New value of the "etchedBorder" property.
         */
        public Splitpane.Builder<_B> withEtchedBorder(final EtchedBorder etchedBorder) {
            this.etchedBorder = ((etchedBorder == null)?null:new EtchedBorder.Builder<Splitpane.Builder<_B>>(this, etchedBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "etchedBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.EtchedBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "etchedBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.EtchedBorder.Builder#end()} to return to the current builder.
         */
        public EtchedBorder.Builder<? extends Splitpane.Builder<_B>> withEtchedBorder() {
            if (this.etchedBorder!= null) {
                return this.etchedBorder;
            }
            return this.etchedBorder = new EtchedBorder.Builder<Splitpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "bevelBorder" (any previous value will be replaced)
         * 
         * @param bevelBorder
         *     New value of the "bevelBorder" property.
         */
        public Splitpane.Builder<_B> withBevelBorder(final BevelBorder bevelBorder) {
            this.bevelBorder = ((bevelBorder == null)?null:new BevelBorder.Builder<Splitpane.Builder<_B>>(this, bevelBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "bevelBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.BevelBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "bevelBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.BevelBorder.Builder#end()} to return to the current builder.
         */
        public BevelBorder.Builder<? extends Splitpane.Builder<_B>> withBevelBorder() {
            if (this.bevelBorder!= null) {
                return this.bevelBorder;
            }
            return this.bevelBorder = new BevelBorder.Builder<Splitpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "titledBorder" (any previous value will be replaced)
         * 
         * @param titledBorder
         *     New value of the "titledBorder" property.
         */
        public Splitpane.Builder<_B> withTitledBorder(final TitledBorder titledBorder) {
            this.titledBorder = ((titledBorder == null)?null:new TitledBorder.Builder<Splitpane.Builder<_B>>(this, titledBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "titledBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.TitledBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "titledBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.TitledBorder.Builder#end()} to return to the current builder.
         */
        public TitledBorder.Builder<? extends Splitpane.Builder<_B>> withTitledBorder() {
            if (this.titledBorder!= null) {
                return this.titledBorder;
            }
            return this.titledBorder = new TitledBorder.Builder<Splitpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "emptyBorder" (any previous value will be replaced)
         * 
         * @param emptyBorder
         *     New value of the "emptyBorder" property.
         */
        public Splitpane.Builder<_B> withEmptyBorder(final EmptyBorder emptyBorder) {
            this.emptyBorder = ((emptyBorder == null)?null:new EmptyBorder.Builder<Splitpane.Builder<_B>>(this, emptyBorder, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "emptyBorder" property.
         * Use {@link org.nuclos.schema.layout.layoutml.EmptyBorder.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "emptyBorder" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.EmptyBorder.Builder#end()} to return to the current builder.
         */
        public EmptyBorder.Builder<? extends Splitpane.Builder<_B>> withEmptyBorder() {
            if (this.emptyBorder!= null) {
                return this.emptyBorder;
            }
            return this.emptyBorder = new EmptyBorder.Builder<Splitpane.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Splitpane.Builder<_B> addBorder(final Iterable<?> border) {
            if (border!= null) {
                if (this.border == null) {
                    this.border = new ArrayList<Buildable>();
                }
                for (Object _item: border) {
                    this.border.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Splitpane.Builder<_B> withBorder(final Iterable<?> border) {
            if (this.border!= null) {
                this.border.clear();
            }
            return addBorder(border);
        }

        /**
         * Adds the given items to the value of "border"
         * 
         * @param border
         *     Items to add to the value of the "border" property
         */
        public Splitpane.Builder<_B> addBorder(Object... border) {
            addBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "border" (any previous value will be replaced)
         * 
         * @param border
         *     New value of the "border" property.
         */
        public Splitpane.Builder<_B> withBorder(Object... border) {
            withBorder(Arrays.asList(border));
            return this;
        }

        /**
         * Sets the new value of "minimumSize" (any previous value will be replaced)
         * 
         * @param minimumSize
         *     New value of the "minimumSize" property.
         */
        public Splitpane.Builder<_B> withMinimumSize(final MinimumSize minimumSize) {
            this.minimumSize = ((minimumSize == null)?null:new MinimumSize.Builder<Splitpane.Builder<_B>>(this, minimumSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "minimumSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "minimumSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.MinimumSize.Builder#end()} to return to the current builder.
         */
        public MinimumSize.Builder<? extends Splitpane.Builder<_B>> withMinimumSize() {
            if (this.minimumSize!= null) {
                return this.minimumSize;
            }
            return this.minimumSize = new MinimumSize.Builder<Splitpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "preferredSize" (any previous value will be replaced)
         * 
         * @param preferredSize
         *     New value of the "preferredSize" property.
         */
        public Splitpane.Builder<_B> withPreferredSize(final PreferredSize preferredSize) {
            this.preferredSize = ((preferredSize == null)?null:new PreferredSize.Builder<Splitpane.Builder<_B>>(this, preferredSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "preferredSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "preferredSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.PreferredSize.Builder#end()} to return to the current builder.
         */
        public PreferredSize.Builder<? extends Splitpane.Builder<_B>> withPreferredSize() {
            if (this.preferredSize!= null) {
                return this.preferredSize;
            }
            return this.preferredSize = new PreferredSize.Builder<Splitpane.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "strictSize" (any previous value will be replaced)
         * 
         * @param strictSize
         *     New value of the "strictSize" property.
         */
        public Splitpane.Builder<_B> withStrictSize(final StrictSize strictSize) {
            this.strictSize = ((strictSize == null)?null:new StrictSize.Builder<Splitpane.Builder<_B>>(this, strictSize, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "strictSize" property.
         * Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "strictSize" property.
         *     Use {@link org.nuclos.schema.layout.layoutml.StrictSize.Builder#end()} to return to the current builder.
         */
        public StrictSize.Builder<? extends Splitpane.Builder<_B>> withStrictSize() {
            if (this.strictSize!= null) {
                return this.strictSize;
            }
            return this.strictSize = new StrictSize.Builder<Splitpane.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "container"
         * 
         * @param container
         *     Items to add to the value of the "container" property
         */
        public Splitpane.Builder<_B> addContainer(final Iterable<? extends JAXBElement<?>> container) {
            if (container!= null) {
                if (this.container == null) {
                    this.container = new ArrayList<Buildable>();
                }
                for (JAXBElement<?> _item: container) {
                    this.container.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "container" (any previous value will be replaced)
         * 
         * @param container
         *     New value of the "container" property.
         */
        public Splitpane.Builder<_B> withContainer(final Iterable<? extends JAXBElement<?>> container) {
            if (this.container!= null) {
                this.container.clear();
            }
            return addContainer(container);
        }

        /**
         * Adds the given items to the value of "container"
         * 
         * @param container
         *     Items to add to the value of the "container" property
         */
        public Splitpane.Builder<_B> addContainer(JAXBElement<?> ... container) {
            addContainer(Arrays.asList(container));
            return this;
        }

        /**
         * Sets the new value of "container" (any previous value will be replaced)
         * 
         * @param container
         *     New value of the "container" property.
         */
        public Splitpane.Builder<_B> withContainer(JAXBElement<?> ... container) {
            withContainer(Arrays.asList(container));
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public Splitpane.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "orientation" (any previous value will be replaced)
         * 
         * @param orientation
         *     New value of the "orientation" property.
         */
        public Splitpane.Builder<_B> withOrientation(final String orientation) {
            this.orientation = orientation;
            return this;
        }

        /**
         * Sets the new value of "dividersize" (any previous value will be replaced)
         * 
         * @param dividersize
         *     New value of the "dividersize" property.
         */
        public Splitpane.Builder<_B> withDividersize(final String dividersize) {
            this.dividersize = dividersize;
            return this;
        }

        /**
         * Sets the new value of "resizeweight" (any previous value will be replaced)
         * 
         * @param resizeweight
         *     New value of the "resizeweight" property.
         */
        public Splitpane.Builder<_B> withResizeweight(final String resizeweight) {
            this.resizeweight = resizeweight;
            return this;
        }

        /**
         * Sets the new value of "expandable" (any previous value will be replaced)
         * 
         * @param expandable
         *     New value of the "expandable" property.
         */
        public Splitpane.Builder<_B> withExpandable(final Boolean expandable) {
            this.expandable = expandable;
            return this;
        }

        /**
         * Sets the new value of "continuousLayout" (any previous value will be replaced)
         * 
         * @param continuousLayout
         *     New value of the "continuousLayout" property.
         */
        public Splitpane.Builder<_B> withContinuousLayout(final Boolean continuousLayout) {
            this.continuousLayout = continuousLayout;
            return this;
        }

        @Override
        public Splitpane build() {
            if (_storedValue == null) {
                return this.init(new Splitpane());
            } else {
                return ((Splitpane) _storedValue);
            }
        }

        public Splitpane.Builder<_B> copyOf(final Splitpane _other) {
            _other.copyTo(this);
            return this;
        }

        public Splitpane.Builder<_B> copyOf(final Splitpane.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Splitpane.Selector<Splitpane.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Splitpane.Select _root() {
            return new Splitpane.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> layoutconstraints = null;
        private ClearBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> clearBorder = null;
        private LineBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> lineBorder = null;
        private EtchedBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> etchedBorder = null;
        private BevelBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> bevelBorder = null;
        private TitledBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> titledBorder = null;
        private EmptyBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> emptyBorder = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> border = null;
        private MinimumSize.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> minimumSize = null;
        private PreferredSize.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> preferredSize = null;
        private StrictSize.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> strictSize = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> container = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> orientation = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> dividersize = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> resizeweight = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> expandable = null;
        private com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> continuousLayout = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.layoutconstraints!= null) {
                products.put("layoutconstraints", this.layoutconstraints.init());
            }
            if (this.clearBorder!= null) {
                products.put("clearBorder", this.clearBorder.init());
            }
            if (this.lineBorder!= null) {
                products.put("lineBorder", this.lineBorder.init());
            }
            if (this.etchedBorder!= null) {
                products.put("etchedBorder", this.etchedBorder.init());
            }
            if (this.bevelBorder!= null) {
                products.put("bevelBorder", this.bevelBorder.init());
            }
            if (this.titledBorder!= null) {
                products.put("titledBorder", this.titledBorder.init());
            }
            if (this.emptyBorder!= null) {
                products.put("emptyBorder", this.emptyBorder.init());
            }
            if (this.border!= null) {
                products.put("border", this.border.init());
            }
            if (this.minimumSize!= null) {
                products.put("minimumSize", this.minimumSize.init());
            }
            if (this.preferredSize!= null) {
                products.put("preferredSize", this.preferredSize.init());
            }
            if (this.strictSize!= null) {
                products.put("strictSize", this.strictSize.init());
            }
            if (this.container!= null) {
                products.put("container", this.container.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.orientation!= null) {
                products.put("orientation", this.orientation.init());
            }
            if (this.dividersize!= null) {
                products.put("dividersize", this.dividersize.init());
            }
            if (this.resizeweight!= null) {
                products.put("resizeweight", this.resizeweight.init());
            }
            if (this.expandable!= null) {
                products.put("expandable", this.expandable.init());
            }
            if (this.continuousLayout!= null) {
                products.put("continuousLayout", this.continuousLayout.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> layoutconstraints() {
            return ((this.layoutconstraints == null)?this.layoutconstraints = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "layoutconstraints"):this.layoutconstraints);
        }

        public ClearBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> clearBorder() {
            return ((this.clearBorder == null)?this.clearBorder = new ClearBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "clearBorder"):this.clearBorder);
        }

        public LineBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> lineBorder() {
            return ((this.lineBorder == null)?this.lineBorder = new LineBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "lineBorder"):this.lineBorder);
        }

        public EtchedBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> etchedBorder() {
            return ((this.etchedBorder == null)?this.etchedBorder = new EtchedBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "etchedBorder"):this.etchedBorder);
        }

        public BevelBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> bevelBorder() {
            return ((this.bevelBorder == null)?this.bevelBorder = new BevelBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "bevelBorder"):this.bevelBorder);
        }

        public TitledBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> titledBorder() {
            return ((this.titledBorder == null)?this.titledBorder = new TitledBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "titledBorder"):this.titledBorder);
        }

        public EmptyBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> emptyBorder() {
            return ((this.emptyBorder == null)?this.emptyBorder = new EmptyBorder.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "emptyBorder"):this.emptyBorder);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> border() {
            return ((this.border == null)?this.border = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "border"):this.border);
        }

        public MinimumSize.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> minimumSize() {
            return ((this.minimumSize == null)?this.minimumSize = new MinimumSize.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "minimumSize"):this.minimumSize);
        }

        public PreferredSize.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> preferredSize() {
            return ((this.preferredSize == null)?this.preferredSize = new PreferredSize.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "preferredSize"):this.preferredSize);
        }

        public StrictSize.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> strictSize() {
            return ((this.strictSize == null)?this.strictSize = new StrictSize.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "strictSize"):this.strictSize);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> container() {
            return ((this.container == null)?this.container = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "container"):this.container);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> orientation() {
            return ((this.orientation == null)?this.orientation = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "orientation"):this.orientation);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> dividersize() {
            return ((this.dividersize == null)?this.dividersize = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "dividersize"):this.dividersize);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> resizeweight() {
            return ((this.resizeweight == null)?this.resizeweight = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "resizeweight"):this.resizeweight);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> expandable() {
            return ((this.expandable == null)?this.expandable = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "expandable"):this.expandable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>> continuousLayout() {
            return ((this.continuousLayout == null)?this.continuousLayout = new com.kscs.util.jaxb.Selector<TRoot, Splitpane.Selector<TRoot, TParent>>(this._root, this, "continuousLayout"):this.continuousLayout);
        }

    }

}
