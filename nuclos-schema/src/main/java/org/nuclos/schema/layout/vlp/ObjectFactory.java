package org.nuclos.schema.layout.vlp;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.nuclos.schema.layout.vlp package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.nuclos.schema.layout.vlp
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValueListProviders }
     * 
     */
    public ValueListProviders createValueListProviders() {
        return new ValueListProviders();
    }

    /**
     * Create an instance of {@link ValueListProvider }
     * 
     */
    public ValueListProvider createValueListProvider() {
        return new ValueListProvider();
    }

    /**
     * Create an instance of {@link Parameter }
     * 
     */
    public Parameter createParameter() {
        return new Parameter();
    }

}
