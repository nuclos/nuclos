package org.nuclos.schema.layout.vlp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Type for an value list provider.
 * 
 * <p>Java class for value-list-provider complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="value-list-provider"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parameters" type="{urn:org.nuclos.schema.layout.vlp}parameter" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="attribute" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="value" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="idFieldname" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="fieldname" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="defaultFieldname" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "value-list-provider", propOrder = {
    "parameters"
})
public class ValueListProvider implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<Parameter> parameters;
    @XmlAttribute(name = "attribute", required = true)
    protected String attribute;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "type", required = true)
    protected String type;
    @XmlAttribute(name = "value", required = true)
    protected String value;
    @XmlAttribute(name = "idFieldname", required = true)
    protected String idFieldname;
    @XmlAttribute(name = "fieldname", required = true)
    protected String fieldname;
    @XmlAttribute(name = "defaultFieldname", required = true)
    protected String defaultFieldname;

    /**
     * Gets the value of the parameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Parameter }
     * 
     * 
     */
    public List<Parameter> getParameters() {
        if (parameters == null) {
            parameters = new ArrayList<Parameter>();
        }
        return this.parameters;
    }

    /**
     * Gets the value of the attribute property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute() {
        return attribute;
    }

    /**
     * Sets the value of the attribute property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute(String value) {
        this.attribute = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the idFieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFieldname() {
        return idFieldname;
    }

    /**
     * Sets the value of the idFieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFieldname(String value) {
        this.idFieldname = value;
    }

    /**
     * Gets the value of the fieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldname() {
        return fieldname;
    }

    /**
     * Sets the value of the fieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldname(String value) {
        this.fieldname = value;
    }

    /**
     * Gets the value of the defaultFieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultFieldname() {
        return defaultFieldname;
    }

    /**
     * Sets the value of the defaultFieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultFieldname(String value) {
        this.defaultFieldname = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<Parameter> theParameters;
            theParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
            strategy.appendField(locator, this, "parameters", buffer, theParameters);
        }
        {
            String theAttribute;
            theAttribute = this.getAttribute();
            strategy.appendField(locator, this, "attribute", buffer, theAttribute);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            String theValue;
            theValue = this.getValue();
            strategy.appendField(locator, this, "value", buffer, theValue);
        }
        {
            String theIdFieldname;
            theIdFieldname = this.getIdFieldname();
            strategy.appendField(locator, this, "idFieldname", buffer, theIdFieldname);
        }
        {
            String theFieldname;
            theFieldname = this.getFieldname();
            strategy.appendField(locator, this, "fieldname", buffer, theFieldname);
        }
        {
            String theDefaultFieldname;
            theDefaultFieldname = this.getDefaultFieldname();
            strategy.appendField(locator, this, "defaultFieldname", buffer, theDefaultFieldname);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ValueListProvider)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ValueListProvider that = ((ValueListProvider) object);
        {
            List<Parameter> lhsParameters;
            lhsParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
            List<Parameter> rhsParameters;
            rhsParameters = (((that.parameters!= null)&&(!that.parameters.isEmpty()))?that.getParameters():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameters", lhsParameters), LocatorUtils.property(thatLocator, "parameters", rhsParameters), lhsParameters, rhsParameters)) {
                return false;
            }
        }
        {
            String lhsAttribute;
            lhsAttribute = this.getAttribute();
            String rhsAttribute;
            rhsAttribute = that.getAttribute();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attribute", lhsAttribute), LocatorUtils.property(thatLocator, "attribute", rhsAttribute), lhsAttribute, rhsAttribute)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            String lhsValue;
            lhsValue = this.getValue();
            String rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            String lhsIdFieldname;
            lhsIdFieldname = this.getIdFieldname();
            String rhsIdFieldname;
            rhsIdFieldname = that.getIdFieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "idFieldname", lhsIdFieldname), LocatorUtils.property(thatLocator, "idFieldname", rhsIdFieldname), lhsIdFieldname, rhsIdFieldname)) {
                return false;
            }
        }
        {
            String lhsFieldname;
            lhsFieldname = this.getFieldname();
            String rhsFieldname;
            rhsFieldname = that.getFieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fieldname", lhsFieldname), LocatorUtils.property(thatLocator, "fieldname", rhsFieldname), lhsFieldname, rhsFieldname)) {
                return false;
            }
        }
        {
            String lhsDefaultFieldname;
            lhsDefaultFieldname = this.getDefaultFieldname();
            String rhsDefaultFieldname;
            rhsDefaultFieldname = that.getDefaultFieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "defaultFieldname", lhsDefaultFieldname), LocatorUtils.property(thatLocator, "defaultFieldname", rhsDefaultFieldname), lhsDefaultFieldname, rhsDefaultFieldname)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<Parameter> theParameters;
            theParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "parameters", theParameters), currentHashCode, theParameters);
        }
        {
            String theAttribute;
            theAttribute = this.getAttribute();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attribute", theAttribute), currentHashCode, theAttribute);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            String theValue;
            theValue = this.getValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
        }
        {
            String theIdFieldname;
            theIdFieldname = this.getIdFieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idFieldname", theIdFieldname), currentHashCode, theIdFieldname);
        }
        {
            String theFieldname;
            theFieldname = this.getFieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fieldname", theFieldname), currentHashCode, theFieldname);
        }
        {
            String theDefaultFieldname;
            theDefaultFieldname = this.getDefaultFieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "defaultFieldname", theDefaultFieldname), currentHashCode, theDefaultFieldname);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ValueListProvider) {
            final ValueListProvider copy = ((ValueListProvider) draftCopy);
            if ((this.parameters!= null)&&(!this.parameters.isEmpty())) {
                List<Parameter> sourceParameters;
                sourceParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
                @SuppressWarnings("unchecked")
                List<Parameter> copyParameters = ((List<Parameter> ) strategy.copy(LocatorUtils.property(locator, "parameters", sourceParameters), sourceParameters));
                copy.parameters = null;
                if (copyParameters!= null) {
                    List<Parameter> uniqueParametersl = copy.getParameters();
                    uniqueParametersl.addAll(copyParameters);
                }
            } else {
                copy.parameters = null;
            }
            if (this.attribute!= null) {
                String sourceAttribute;
                sourceAttribute = this.getAttribute();
                String copyAttribute = ((String) strategy.copy(LocatorUtils.property(locator, "attribute", sourceAttribute), sourceAttribute));
                copy.setAttribute(copyAttribute);
            } else {
                copy.attribute = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            if (this.value!= null) {
                String sourceValue;
                sourceValue = this.getValue();
                String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.idFieldname!= null) {
                String sourceIdFieldname;
                sourceIdFieldname = this.getIdFieldname();
                String copyIdFieldname = ((String) strategy.copy(LocatorUtils.property(locator, "idFieldname", sourceIdFieldname), sourceIdFieldname));
                copy.setIdFieldname(copyIdFieldname);
            } else {
                copy.idFieldname = null;
            }
            if (this.fieldname!= null) {
                String sourceFieldname;
                sourceFieldname = this.getFieldname();
                String copyFieldname = ((String) strategy.copy(LocatorUtils.property(locator, "fieldname", sourceFieldname), sourceFieldname));
                copy.setFieldname(copyFieldname);
            } else {
                copy.fieldname = null;
            }
            if (this.defaultFieldname!= null) {
                String sourceDefaultFieldname;
                sourceDefaultFieldname = this.getDefaultFieldname();
                String copyDefaultFieldname = ((String) strategy.copy(LocatorUtils.property(locator, "defaultFieldname", sourceDefaultFieldname), sourceDefaultFieldname));
                copy.setDefaultFieldname(copyDefaultFieldname);
            } else {
                copy.defaultFieldname = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ValueListProvider();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ValueListProvider.Builder<_B> _other) {
        if (this.parameters == null) {
            _other.parameters = null;
        } else {
            _other.parameters = new ArrayList<Parameter.Builder<ValueListProvider.Builder<_B>>>();
            for (Parameter _item: this.parameters) {
                _other.parameters.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.attribute = this.attribute;
        _other.name = this.name;
        _other.type = this.type;
        _other.value = this.value;
        _other.idFieldname = this.idFieldname;
        _other.fieldname = this.fieldname;
        _other.defaultFieldname = this.defaultFieldname;
    }

    public<_B >ValueListProvider.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new ValueListProvider.Builder<_B>(_parentBuilder, this, true);
    }

    public ValueListProvider.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static ValueListProvider.Builder<Void> builder() {
        return new ValueListProvider.Builder<Void>(null, null, false);
    }

    public static<_B >ValueListProvider.Builder<_B> copyOf(final ValueListProvider _other) {
        final ValueListProvider.Builder<_B> _newBuilder = new ValueListProvider.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ValueListProvider.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree parametersPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameters"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parametersPropertyTree!= null):((parametersPropertyTree == null)||(!parametersPropertyTree.isLeaf())))) {
            if (this.parameters == null) {
                _other.parameters = null;
            } else {
                _other.parameters = new ArrayList<Parameter.Builder<ValueListProvider.Builder<_B>>>();
                for (Parameter _item: this.parameters) {
                    _other.parameters.add(((_item == null)?null:_item.newCopyBuilder(_other, parametersPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree attributePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attribute"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attributePropertyTree!= null):((attributePropertyTree == null)||(!attributePropertyTree.isLeaf())))) {
            _other.attribute = this.attribute;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
            _other.value = this.value;
        }
        final PropertyTree idFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("idFieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idFieldnamePropertyTree!= null):((idFieldnamePropertyTree == null)||(!idFieldnamePropertyTree.isLeaf())))) {
            _other.idFieldname = this.idFieldname;
        }
        final PropertyTree fieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldnamePropertyTree!= null):((fieldnamePropertyTree == null)||(!fieldnamePropertyTree.isLeaf())))) {
            _other.fieldname = this.fieldname;
        }
        final PropertyTree defaultFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("defaultFieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(defaultFieldnamePropertyTree!= null):((defaultFieldnamePropertyTree == null)||(!defaultFieldnamePropertyTree.isLeaf())))) {
            _other.defaultFieldname = this.defaultFieldname;
        }
    }

    public<_B >ValueListProvider.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new ValueListProvider.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public ValueListProvider.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >ValueListProvider.Builder<_B> copyOf(final ValueListProvider _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final ValueListProvider.Builder<_B> _newBuilder = new ValueListProvider.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static ValueListProvider.Builder<Void> copyExcept(final ValueListProvider _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static ValueListProvider.Builder<Void> copyOnly(final ValueListProvider _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final ValueListProvider _storedValue;
        private List<Parameter.Builder<ValueListProvider.Builder<_B>>> parameters;
        private String attribute;
        private String name;
        private String type;
        private String value;
        private String idFieldname;
        private String fieldname;
        private String defaultFieldname;

        public Builder(final _B _parentBuilder, final ValueListProvider _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.parameters == null) {
                        this.parameters = null;
                    } else {
                        this.parameters = new ArrayList<Parameter.Builder<ValueListProvider.Builder<_B>>>();
                        for (Parameter _item: _other.parameters) {
                            this.parameters.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.attribute = _other.attribute;
                    this.name = _other.name;
                    this.type = _other.type;
                    this.value = _other.value;
                    this.idFieldname = _other.idFieldname;
                    this.fieldname = _other.fieldname;
                    this.defaultFieldname = _other.defaultFieldname;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final ValueListProvider _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree parametersPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameters"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parametersPropertyTree!= null):((parametersPropertyTree == null)||(!parametersPropertyTree.isLeaf())))) {
                        if (_other.parameters == null) {
                            this.parameters = null;
                        } else {
                            this.parameters = new ArrayList<Parameter.Builder<ValueListProvider.Builder<_B>>>();
                            for (Parameter _item: _other.parameters) {
                                this.parameters.add(((_item == null)?null:_item.newCopyBuilder(this, parametersPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree attributePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attribute"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attributePropertyTree!= null):((attributePropertyTree == null)||(!attributePropertyTree.isLeaf())))) {
                        this.attribute = _other.attribute;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                        this.value = _other.value;
                    }
                    final PropertyTree idFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("idFieldname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idFieldnamePropertyTree!= null):((idFieldnamePropertyTree == null)||(!idFieldnamePropertyTree.isLeaf())))) {
                        this.idFieldname = _other.idFieldname;
                    }
                    final PropertyTree fieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldnamePropertyTree!= null):((fieldnamePropertyTree == null)||(!fieldnamePropertyTree.isLeaf())))) {
                        this.fieldname = _other.fieldname;
                    }
                    final PropertyTree defaultFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("defaultFieldname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(defaultFieldnamePropertyTree!= null):((defaultFieldnamePropertyTree == null)||(!defaultFieldnamePropertyTree.isLeaf())))) {
                        this.defaultFieldname = _other.defaultFieldname;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends ValueListProvider >_P init(final _P _product) {
            if (this.parameters!= null) {
                final List<Parameter> parameters = new ArrayList<Parameter>(this.parameters.size());
                for (Parameter.Builder<ValueListProvider.Builder<_B>> _item: this.parameters) {
                    parameters.add(_item.build());
                }
                _product.parameters = parameters;
            }
            _product.attribute = this.attribute;
            _product.name = this.name;
            _product.type = this.type;
            _product.value = this.value;
            _product.idFieldname = this.idFieldname;
            _product.fieldname = this.fieldname;
            _product.defaultFieldname = this.defaultFieldname;
            return _product;
        }

        /**
         * Adds the given items to the value of "parameters"
         * 
         * @param parameters
         *     Items to add to the value of the "parameters" property
         */
        public ValueListProvider.Builder<_B> addParameters(final Iterable<? extends Parameter> parameters) {
            if (parameters!= null) {
                if (this.parameters == null) {
                    this.parameters = new ArrayList<Parameter.Builder<ValueListProvider.Builder<_B>>>();
                }
                for (Parameter _item: parameters) {
                    this.parameters.add(new Parameter.Builder<ValueListProvider.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "parameters" (any previous value will be replaced)
         * 
         * @param parameters
         *     New value of the "parameters" property.
         */
        public ValueListProvider.Builder<_B> withParameters(final Iterable<? extends Parameter> parameters) {
            if (this.parameters!= null) {
                this.parameters.clear();
            }
            return addParameters(parameters);
        }

        /**
         * Adds the given items to the value of "parameters"
         * 
         * @param parameters
         *     Items to add to the value of the "parameters" property
         */
        public ValueListProvider.Builder<_B> addParameters(Parameter... parameters) {
            addParameters(Arrays.asList(parameters));
            return this;
        }

        /**
         * Sets the new value of "parameters" (any previous value will be replaced)
         * 
         * @param parameters
         *     New value of the "parameters" property.
         */
        public ValueListProvider.Builder<_B> withParameters(Parameter... parameters) {
            withParameters(Arrays.asList(parameters));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Parameters" property.
         * Use {@link org.nuclos.schema.layout.vlp.Parameter.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Parameters" property.
         *     Use {@link org.nuclos.schema.layout.vlp.Parameter.Builder#end()} to return to the current builder.
         */
        public Parameter.Builder<? extends ValueListProvider.Builder<_B>> addParameters() {
            if (this.parameters == null) {
                this.parameters = new ArrayList<Parameter.Builder<ValueListProvider.Builder<_B>>>();
            }
            final Parameter.Builder<ValueListProvider.Builder<_B>> parameters_Builder = new Parameter.Builder<ValueListProvider.Builder<_B>>(this, null, false);
            this.parameters.add(parameters_Builder);
            return parameters_Builder;
        }

        /**
         * Sets the new value of "attribute" (any previous value will be replaced)
         * 
         * @param attribute
         *     New value of the "attribute" property.
         */
        public ValueListProvider.Builder<_B> withAttribute(final String attribute) {
            this.attribute = attribute;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public ValueListProvider.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public ValueListProvider.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "value" (any previous value will be replaced)
         * 
         * @param value
         *     New value of the "value" property.
         */
        public ValueListProvider.Builder<_B> withValue(final String value) {
            this.value = value;
            return this;
        }

        /**
         * Sets the new value of "idFieldname" (any previous value will be replaced)
         * 
         * @param idFieldname
         *     New value of the "idFieldname" property.
         */
        public ValueListProvider.Builder<_B> withIdFieldname(final String idFieldname) {
            this.idFieldname = idFieldname;
            return this;
        }

        /**
         * Sets the new value of "fieldname" (any previous value will be replaced)
         * 
         * @param fieldname
         *     New value of the "fieldname" property.
         */
        public ValueListProvider.Builder<_B> withFieldname(final String fieldname) {
            this.fieldname = fieldname;
            return this;
        }

        /**
         * Sets the new value of "defaultFieldname" (any previous value will be replaced)
         * 
         * @param defaultFieldname
         *     New value of the "defaultFieldname" property.
         */
        public ValueListProvider.Builder<_B> withDefaultFieldname(final String defaultFieldname) {
            this.defaultFieldname = defaultFieldname;
            return this;
        }

        @Override
        public ValueListProvider build() {
            if (_storedValue == null) {
                return this.init(new ValueListProvider());
            } else {
                return ((ValueListProvider) _storedValue);
            }
        }

        public ValueListProvider.Builder<_B> copyOf(final ValueListProvider _other) {
            _other.copyTo(this);
            return this;
        }

        public ValueListProvider.Builder<_B> copyOf(final ValueListProvider.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends ValueListProvider.Selector<ValueListProvider.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static ValueListProvider.Select _root() {
            return new ValueListProvider.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Parameter.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> parameters = null;
        private com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> attribute = null;
        private com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> value = null;
        private com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> idFieldname = null;
        private com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> fieldname = null;
        private com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> defaultFieldname = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.parameters!= null) {
                products.put("parameters", this.parameters.init());
            }
            if (this.attribute!= null) {
                products.put("attribute", this.attribute.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.value!= null) {
                products.put("value", this.value.init());
            }
            if (this.idFieldname!= null) {
                products.put("idFieldname", this.idFieldname.init());
            }
            if (this.fieldname!= null) {
                products.put("fieldname", this.fieldname.init());
            }
            if (this.defaultFieldname!= null) {
                products.put("defaultFieldname", this.defaultFieldname.init());
            }
            return products;
        }

        public Parameter.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> parameters() {
            return ((this.parameters == null)?this.parameters = new Parameter.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>>(this._root, this, "parameters"):this.parameters);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> attribute() {
            return ((this.attribute == null)?this.attribute = new com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>>(this._root, this, "attribute"):this.attribute);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> value() {
            return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> idFieldname() {
            return ((this.idFieldname == null)?this.idFieldname = new com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>>(this._root, this, "idFieldname"):this.idFieldname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> fieldname() {
            return ((this.fieldname == null)?this.fieldname = new com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>>(this._root, this, "fieldname"):this.fieldname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>> defaultFieldname() {
            return ((this.defaultFieldname == null)?this.defaultFieldname = new com.kscs.util.jaxb.Selector<TRoot, ValueListProvider.Selector<TRoot, TParent>>(this._root, this, "defaultFieldname"):this.defaultFieldname);
        }

    }

}
