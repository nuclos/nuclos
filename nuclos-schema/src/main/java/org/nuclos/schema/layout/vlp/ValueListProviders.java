package org.nuclos.schema.layout.vlp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A list of value list providers.
 * 
 * <p>Java class for value-list-providers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="value-list-providers"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="vlps" type="{urn:org.nuclos.schema.layout.vlp}value-list-provider" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "value-list-providers", propOrder = {
    "vlps"
})
public class ValueListProviders implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<ValueListProvider> vlps;

    /**
     * Gets the value of the vlps property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vlps property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVlps().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValueListProvider }
     * 
     * 
     */
    public List<ValueListProvider> getVlps() {
        if (vlps == null) {
            vlps = new ArrayList<ValueListProvider>();
        }
        return this.vlps;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<ValueListProvider> theVlps;
            theVlps = (((this.vlps!= null)&&(!this.vlps.isEmpty()))?this.getVlps():null);
            strategy.appendField(locator, this, "vlps", buffer, theVlps);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ValueListProviders)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ValueListProviders that = ((ValueListProviders) object);
        {
            List<ValueListProvider> lhsVlps;
            lhsVlps = (((this.vlps!= null)&&(!this.vlps.isEmpty()))?this.getVlps():null);
            List<ValueListProvider> rhsVlps;
            rhsVlps = (((that.vlps!= null)&&(!that.vlps.isEmpty()))?that.getVlps():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vlps", lhsVlps), LocatorUtils.property(thatLocator, "vlps", rhsVlps), lhsVlps, rhsVlps)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<ValueListProvider> theVlps;
            theVlps = (((this.vlps!= null)&&(!this.vlps.isEmpty()))?this.getVlps():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlps", theVlps), currentHashCode, theVlps);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof ValueListProviders) {
            final ValueListProviders copy = ((ValueListProviders) draftCopy);
            if ((this.vlps!= null)&&(!this.vlps.isEmpty())) {
                List<ValueListProvider> sourceVlps;
                sourceVlps = (((this.vlps!= null)&&(!this.vlps.isEmpty()))?this.getVlps():null);
                @SuppressWarnings("unchecked")
                List<ValueListProvider> copyVlps = ((List<ValueListProvider> ) strategy.copy(LocatorUtils.property(locator, "vlps", sourceVlps), sourceVlps));
                copy.vlps = null;
                if (copyVlps!= null) {
                    List<ValueListProvider> uniqueVlpsl = copy.getVlps();
                    uniqueVlpsl.addAll(copyVlps);
                }
            } else {
                copy.vlps = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new ValueListProviders();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ValueListProviders.Builder<_B> _other) {
        if (this.vlps == null) {
            _other.vlps = null;
        } else {
            _other.vlps = new ArrayList<ValueListProvider.Builder<ValueListProviders.Builder<_B>>>();
            for (ValueListProvider _item: this.vlps) {
                _other.vlps.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >ValueListProviders.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new ValueListProviders.Builder<_B>(_parentBuilder, this, true);
    }

    public ValueListProviders.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static ValueListProviders.Builder<Void> builder() {
        return new ValueListProviders.Builder<Void>(null, null, false);
    }

    public static<_B >ValueListProviders.Builder<_B> copyOf(final ValueListProviders _other) {
        final ValueListProviders.Builder<_B> _newBuilder = new ValueListProviders.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final ValueListProviders.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree vlpsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlps"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpsPropertyTree!= null):((vlpsPropertyTree == null)||(!vlpsPropertyTree.isLeaf())))) {
            if (this.vlps == null) {
                _other.vlps = null;
            } else {
                _other.vlps = new ArrayList<ValueListProvider.Builder<ValueListProviders.Builder<_B>>>();
                for (ValueListProvider _item: this.vlps) {
                    _other.vlps.add(((_item == null)?null:_item.newCopyBuilder(_other, vlpsPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >ValueListProviders.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new ValueListProviders.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public ValueListProviders.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >ValueListProviders.Builder<_B> copyOf(final ValueListProviders _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final ValueListProviders.Builder<_B> _newBuilder = new ValueListProviders.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static ValueListProviders.Builder<Void> copyExcept(final ValueListProviders _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static ValueListProviders.Builder<Void> copyOnly(final ValueListProviders _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final ValueListProviders _storedValue;
        private List<ValueListProvider.Builder<ValueListProviders.Builder<_B>>> vlps;

        public Builder(final _B _parentBuilder, final ValueListProviders _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.vlps == null) {
                        this.vlps = null;
                    } else {
                        this.vlps = new ArrayList<ValueListProvider.Builder<ValueListProviders.Builder<_B>>>();
                        for (ValueListProvider _item: _other.vlps) {
                            this.vlps.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final ValueListProviders _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree vlpsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlps"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpsPropertyTree!= null):((vlpsPropertyTree == null)||(!vlpsPropertyTree.isLeaf())))) {
                        if (_other.vlps == null) {
                            this.vlps = null;
                        } else {
                            this.vlps = new ArrayList<ValueListProvider.Builder<ValueListProviders.Builder<_B>>>();
                            for (ValueListProvider _item: _other.vlps) {
                                this.vlps.add(((_item == null)?null:_item.newCopyBuilder(this, vlpsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends ValueListProviders >_P init(final _P _product) {
            if (this.vlps!= null) {
                final List<ValueListProvider> vlps = new ArrayList<ValueListProvider>(this.vlps.size());
                for (ValueListProvider.Builder<ValueListProviders.Builder<_B>> _item: this.vlps) {
                    vlps.add(_item.build());
                }
                _product.vlps = vlps;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "vlps"
         * 
         * @param vlps
         *     Items to add to the value of the "vlps" property
         */
        public ValueListProviders.Builder<_B> addVlps(final Iterable<? extends ValueListProvider> vlps) {
            if (vlps!= null) {
                if (this.vlps == null) {
                    this.vlps = new ArrayList<ValueListProvider.Builder<ValueListProviders.Builder<_B>>>();
                }
                for (ValueListProvider _item: vlps) {
                    this.vlps.add(new ValueListProvider.Builder<ValueListProviders.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "vlps" (any previous value will be replaced)
         * 
         * @param vlps
         *     New value of the "vlps" property.
         */
        public ValueListProviders.Builder<_B> withVlps(final Iterable<? extends ValueListProvider> vlps) {
            if (this.vlps!= null) {
                this.vlps.clear();
            }
            return addVlps(vlps);
        }

        /**
         * Adds the given items to the value of "vlps"
         * 
         * @param vlps
         *     Items to add to the value of the "vlps" property
         */
        public ValueListProviders.Builder<_B> addVlps(ValueListProvider... vlps) {
            addVlps(Arrays.asList(vlps));
            return this;
        }

        /**
         * Sets the new value of "vlps" (any previous value will be replaced)
         * 
         * @param vlps
         *     New value of the "vlps" property.
         */
        public ValueListProviders.Builder<_B> withVlps(ValueListProvider... vlps) {
            withVlps(Arrays.asList(vlps));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Vlps" property.
         * Use {@link org.nuclos.schema.layout.vlp.ValueListProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Vlps" property.
         *     Use {@link org.nuclos.schema.layout.vlp.ValueListProvider.Builder#end()} to return to the current builder.
         */
        public ValueListProvider.Builder<? extends ValueListProviders.Builder<_B>> addVlps() {
            if (this.vlps == null) {
                this.vlps = new ArrayList<ValueListProvider.Builder<ValueListProviders.Builder<_B>>>();
            }
            final ValueListProvider.Builder<ValueListProviders.Builder<_B>> vlps_Builder = new ValueListProvider.Builder<ValueListProviders.Builder<_B>>(this, null, false);
            this.vlps.add(vlps_Builder);
            return vlps_Builder;
        }

        @Override
        public ValueListProviders build() {
            if (_storedValue == null) {
                return this.init(new ValueListProviders());
            } else {
                return ((ValueListProviders) _storedValue);
            }
        }

        public ValueListProviders.Builder<_B> copyOf(final ValueListProviders _other) {
            _other.copyTo(this);
            return this;
        }

        public ValueListProviders.Builder<_B> copyOf(final ValueListProviders.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends ValueListProviders.Selector<ValueListProviders.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static ValueListProviders.Select _root() {
            return new ValueListProviders.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private ValueListProvider.Selector<TRoot, ValueListProviders.Selector<TRoot, TParent>> vlps = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.vlps!= null) {
                products.put("vlps", this.vlps.init());
            }
            return products;
        }

        public ValueListProvider.Selector<TRoot, ValueListProviders.Selector<TRoot, TParent>> vlps() {
            return ((this.vlps == null)?this.vlps = new ValueListProvider.Selector<TRoot, ValueListProviders.Selector<TRoot, TParent>>(this._root, this, "vlps"):this.vlps);
        }

    }

}
