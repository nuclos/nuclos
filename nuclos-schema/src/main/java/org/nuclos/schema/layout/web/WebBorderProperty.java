package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * An property object describing how border should look like on web.
 * 
 * <p>Java class for web-border-property complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-border-property"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="thickness" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-border-property")
public class WebBorderProperty implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "type", required = true)
    protected String type;
    @XmlAttribute(name = "thickness", required = true)
    protected BigInteger thickness;
    @XmlAttribute(name = "color")
    protected String color;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the thickness property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getThickness() {
        return thickness;
    }

    /**
     * Sets the value of the thickness property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setThickness(BigInteger value) {
        this.thickness = value;
    }

    /**
     * Gets the value of the color property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the value of the color property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            BigInteger theThickness;
            theThickness = this.getThickness();
            strategy.appendField(locator, this, "thickness", buffer, theThickness);
        }
        {
            String theColor;
            theColor = this.getColor();
            strategy.appendField(locator, this, "color", buffer, theColor);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebBorderProperty)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebBorderProperty that = ((WebBorderProperty) object);
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            BigInteger lhsThickness;
            lhsThickness = this.getThickness();
            BigInteger rhsThickness;
            rhsThickness = that.getThickness();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "thickness", lhsThickness), LocatorUtils.property(thatLocator, "thickness", rhsThickness), lhsThickness, rhsThickness)) {
                return false;
            }
        }
        {
            String lhsColor;
            lhsColor = this.getColor();
            String rhsColor;
            rhsColor = that.getColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "color", lhsColor), LocatorUtils.property(thatLocator, "color", rhsColor), lhsColor, rhsColor)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            BigInteger theThickness;
            theThickness = this.getThickness();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "thickness", theThickness), currentHashCode, theThickness);
        }
        {
            String theColor;
            theColor = this.getColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "color", theColor), currentHashCode, theColor);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof WebBorderProperty) {
            final WebBorderProperty copy = ((WebBorderProperty) draftCopy);
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            if (this.thickness!= null) {
                BigInteger sourceThickness;
                sourceThickness = this.getThickness();
                BigInteger copyThickness = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "thickness", sourceThickness), sourceThickness));
                copy.setThickness(copyThickness);
            } else {
                copy.thickness = null;
            }
            if (this.color!= null) {
                String sourceColor;
                sourceColor = this.getColor();
                String copyColor = ((String) strategy.copy(LocatorUtils.property(locator, "color", sourceColor), sourceColor));
                copy.setColor(copyColor);
            } else {
                copy.color = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebBorderProperty();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebBorderProperty.Builder<_B> _other) {
        _other.type = this.type;
        _other.thickness = this.thickness;
        _other.color = this.color;
    }

    public<_B >WebBorderProperty.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebBorderProperty.Builder<_B>(_parentBuilder, this, true);
    }

    public WebBorderProperty.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebBorderProperty.Builder<Void> builder() {
        return new WebBorderProperty.Builder<Void>(null, null, false);
    }

    public static<_B >WebBorderProperty.Builder<_B> copyOf(final WebBorderProperty _other) {
        final WebBorderProperty.Builder<_B> _newBuilder = new WebBorderProperty.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebBorderProperty.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree thicknessPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("thickness"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(thicknessPropertyTree!= null):((thicknessPropertyTree == null)||(!thicknessPropertyTree.isLeaf())))) {
            _other.thickness = this.thickness;
        }
        final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
            _other.color = this.color;
        }
    }

    public<_B >WebBorderProperty.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebBorderProperty.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public WebBorderProperty.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebBorderProperty.Builder<_B> copyOf(final WebBorderProperty _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebBorderProperty.Builder<_B> _newBuilder = new WebBorderProperty.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebBorderProperty.Builder<Void> copyExcept(final WebBorderProperty _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebBorderProperty.Builder<Void> copyOnly(final WebBorderProperty _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebBorderProperty _storedValue;
        private String type;
        private BigInteger thickness;
        private String color;

        public Builder(final _B _parentBuilder, final WebBorderProperty _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.type = _other.type;
                    this.thickness = _other.thickness;
                    this.color = _other.color;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebBorderProperty _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree thicknessPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("thickness"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(thicknessPropertyTree!= null):((thicknessPropertyTree == null)||(!thicknessPropertyTree.isLeaf())))) {
                        this.thickness = _other.thickness;
                    }
                    final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
                        this.color = _other.color;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebBorderProperty >_P init(final _P _product) {
            _product.type = this.type;
            _product.thickness = this.thickness;
            _product.color = this.color;
            return _product;
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public WebBorderProperty.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "thickness" (any previous value will be replaced)
         * 
         * @param thickness
         *     New value of the "thickness" property.
         */
        public WebBorderProperty.Builder<_B> withThickness(final BigInteger thickness) {
            this.thickness = thickness;
            return this;
        }

        /**
         * Sets the new value of "color" (any previous value will be replaced)
         * 
         * @param color
         *     New value of the "color" property.
         */
        public WebBorderProperty.Builder<_B> withColor(final String color) {
            this.color = color;
            return this;
        }

        @Override
        public WebBorderProperty build() {
            if (_storedValue == null) {
                return this.init(new WebBorderProperty());
            } else {
                return ((WebBorderProperty) _storedValue);
            }
        }

        public WebBorderProperty.Builder<_B> copyOf(final WebBorderProperty _other) {
            _other.copyTo(this);
            return this;
        }

        public WebBorderProperty.Builder<_B> copyOf(final WebBorderProperty.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebBorderProperty.Selector<WebBorderProperty.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebBorderProperty.Select _root() {
            return new WebBorderProperty.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, WebBorderProperty.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebBorderProperty.Selector<TRoot, TParent>> thickness = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebBorderProperty.Selector<TRoot, TParent>> color = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.thickness!= null) {
                products.put("thickness", this.thickness.init());
            }
            if (this.color!= null) {
                products.put("color", this.color.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebBorderProperty.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, WebBorderProperty.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebBorderProperty.Selector<TRoot, TParent>> thickness() {
            return ((this.thickness == null)?this.thickness = new com.kscs.util.jaxb.Selector<TRoot, WebBorderProperty.Selector<TRoot, TParent>>(this._root, this, "thickness"):this.thickness);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebBorderProperty.Selector<TRoot, TParent>> color() {
            return ((this.color == null)?this.color = new com.kscs.util.jaxb.Selector<TRoot, WebBorderProperty.Selector<TRoot, TParent>>(this._root, this, "color"):this.color);
        }

    }

}
