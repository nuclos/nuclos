package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Base type for any web component.
 * 
 * <p>Java class for web-component complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-component"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="advanced-properties" type="{NuclosWebLayout}web-advanced-property" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="border-property" type="{NuclosWebLayout}web-border-property" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" /&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="column" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
 *       &lt;attribute name="row" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" /&gt;
 *       &lt;attribute name="colspan" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *       &lt;attribute name="rowspan" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" /&gt;
 *       &lt;attribute name="font-size" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="text-color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="custom-background-color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="bold" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="italic" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="underline" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="vertical-align" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="horizontal-align" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="minimum-width" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="label" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="minimum-height" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="preferred-width" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="preferred-height" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="next-focus-component" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="next-focus-field" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="description-attr" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="initial-focus" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="alternative-tooltip" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="has-text-modules" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-component", propOrder = {
    "advancedProperties",
    "borderProperty"
})
@XmlSeeAlso({
    WebContainer.class,
    WebLabel.class,
    WebLabelStatic.class,
    WebHtmlField.class,
    WebSeparator.class,
    WebTitledSeparator.class,
    WebAddon.class,
    WebInputComponent.class
})
public abstract class WebComponent implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "advanced-properties")
    protected List<WebAdvancedProperty> advancedProperties;
    @XmlElement(name = "border-property")
    protected WebBorderProperty borderProperty;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;
    @XmlAttribute(name = "name", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String name;
    @XmlAttribute(name = "column")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger column;
    @XmlAttribute(name = "row")
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger row;
    @XmlAttribute(name = "colspan")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger colspan;
    @XmlAttribute(name = "rowspan")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger rowspan;
    @XmlAttribute(name = "font-size")
    protected String fontSize;
    @XmlAttribute(name = "text-color")
    protected String textColor;
    @XmlAttribute(name = "custom-background-color")
    protected String customBackgroundColor;
    @XmlAttribute(name = "bold")
    protected Boolean bold;
    @XmlAttribute(name = "italic")
    protected Boolean italic;
    @XmlAttribute(name = "underline")
    protected Boolean underline;
    @XmlAttribute(name = "vertical-align")
    protected String verticalAlign;
    @XmlAttribute(name = "horizontal-align")
    protected String horizontalAlign;
    @XmlAttribute(name = "minimum-width")
    protected BigInteger minimumWidth;
    @XmlAttribute(name = "label")
    protected String label;
    @XmlAttribute(name = "minimum-height")
    protected BigInteger minimumHeight;
    @XmlAttribute(name = "preferred-width")
    protected BigInteger preferredWidth;
    @XmlAttribute(name = "preferred-height")
    protected BigInteger preferredHeight;
    @XmlAttribute(name = "next-focus-component")
    protected String nextFocusComponent;
    @XmlAttribute(name = "next-focus-field")
    protected String nextFocusField;
    @XmlAttribute(name = "description-attr")
    protected String descriptionAttr;
    @XmlAttribute(name = "initial-focus")
    protected Boolean initialFocus;
    @XmlAttribute(name = "alternative-tooltip")
    protected String alternativeTooltip;
    @XmlAttribute(name = "has-text-modules")
    protected Boolean hasTextModules;

    /**
     * Gets the value of the advancedProperties property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the advancedProperties property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdvancedProperties().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WebAdvancedProperty }
     * 
     * 
     */
    public List<WebAdvancedProperty> getAdvancedProperties() {
        if (advancedProperties == null) {
            advancedProperties = new ArrayList<WebAdvancedProperty>();
        }
        return this.advancedProperties;
    }

    /**
     * Gets the value of the borderProperty property.
     * 
     * @return
     *     possible object is
     *     {@link WebBorderProperty }
     *     
     */
    public WebBorderProperty getBorderProperty() {
        return borderProperty;
    }

    /**
     * Sets the value of the borderProperty property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebBorderProperty }
     *     
     */
    public void setBorderProperty(WebBorderProperty value) {
        this.borderProperty = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the column property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getColumn() {
        return column;
    }

    /**
     * Sets the value of the column property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setColumn(BigInteger value) {
        this.column = value;
    }

    /**
     * Gets the value of the row property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRow() {
        return row;
    }

    /**
     * Sets the value of the row property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRow(BigInteger value) {
        this.row = value;
    }

    /**
     * Gets the value of the colspan property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getColspan() {
        return colspan;
    }

    /**
     * Sets the value of the colspan property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setColspan(BigInteger value) {
        this.colspan = value;
    }

    /**
     * Gets the value of the rowspan property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRowspan() {
        return rowspan;
    }

    /**
     * Sets the value of the rowspan property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRowspan(BigInteger value) {
        this.rowspan = value;
    }

    /**
     * Gets the value of the fontSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFontSize() {
        return fontSize;
    }

    /**
     * Sets the value of the fontSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFontSize(String value) {
        this.fontSize = value;
    }

    /**
     * Gets the value of the textColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * Sets the value of the textColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextColor(String value) {
        this.textColor = value;
    }

    /**
     * Gets the value of the customBackgroundColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomBackgroundColor() {
        return customBackgroundColor;
    }

    /**
     * Sets the value of the customBackgroundColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomBackgroundColor(String value) {
        this.customBackgroundColor = value;
    }

    /**
     * Gets the value of the bold property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBold() {
        return bold;
    }

    /**
     * Sets the value of the bold property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBold(Boolean value) {
        this.bold = value;
    }

    /**
     * Gets the value of the italic property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItalic() {
        return italic;
    }

    /**
     * Sets the value of the italic property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItalic(Boolean value) {
        this.italic = value;
    }

    /**
     * Gets the value of the underline property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUnderline() {
        return underline;
    }

    /**
     * Sets the value of the underline property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUnderline(Boolean value) {
        this.underline = value;
    }

    /**
     * Gets the value of the verticalAlign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerticalAlign() {
        return verticalAlign;
    }

    /**
     * Sets the value of the verticalAlign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerticalAlign(String value) {
        this.verticalAlign = value;
    }

    /**
     * Gets the value of the horizontalAlign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHorizontalAlign() {
        return horizontalAlign;
    }

    /**
     * Sets the value of the horizontalAlign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHorizontalAlign(String value) {
        this.horizontalAlign = value;
    }

    /**
     * Gets the value of the minimumWidth property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumWidth() {
        return minimumWidth;
    }

    /**
     * Sets the value of the minimumWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumWidth(BigInteger value) {
        this.minimumWidth = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the minimumHeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinimumHeight() {
        return minimumHeight;
    }

    /**
     * Sets the value of the minimumHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinimumHeight(BigInteger value) {
        this.minimumHeight = value;
    }

    /**
     * Gets the value of the preferredWidth property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPreferredWidth() {
        return preferredWidth;
    }

    /**
     * Sets the value of the preferredWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPreferredWidth(BigInteger value) {
        this.preferredWidth = value;
    }

    /**
     * Gets the value of the preferredHeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPreferredHeight() {
        return preferredHeight;
    }

    /**
     * Sets the value of the preferredHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPreferredHeight(BigInteger value) {
        this.preferredHeight = value;
    }

    /**
     * Gets the value of the nextFocusComponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextFocusComponent() {
        return nextFocusComponent;
    }

    /**
     * Sets the value of the nextFocusComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextFocusComponent(String value) {
        this.nextFocusComponent = value;
    }

    /**
     * Gets the value of the nextFocusField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextFocusField() {
        return nextFocusField;
    }

    /**
     * Sets the value of the nextFocusField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextFocusField(String value) {
        this.nextFocusField = value;
    }

    /**
     * Gets the value of the descriptionAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescriptionAttr() {
        return descriptionAttr;
    }

    /**
     * Sets the value of the descriptionAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescriptionAttr(String value) {
        this.descriptionAttr = value;
    }

    /**
     * Gets the value of the initialFocus property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInitialFocus() {
        return initialFocus;
    }

    /**
     * Sets the value of the initialFocus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInitialFocus(Boolean value) {
        this.initialFocus = value;
    }

    /**
     * Gets the value of the alternativeTooltip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternativeTooltip() {
        return alternativeTooltip;
    }

    /**
     * Sets the value of the alternativeTooltip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternativeTooltip(String value) {
        this.alternativeTooltip = value;
    }

    /**
     * Gets the value of the hasTextModules property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasTextModules() {
        return hasTextModules;
    }

    /**
     * Sets the value of the hasTextModules property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasTextModules(Boolean value) {
        this.hasTextModules = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<WebAdvancedProperty> theAdvancedProperties;
            theAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
            strategy.appendField(locator, this, "advancedProperties", buffer, theAdvancedProperties);
        }
        {
            WebBorderProperty theBorderProperty;
            theBorderProperty = this.getBorderProperty();
            strategy.appendField(locator, this, "borderProperty", buffer, theBorderProperty);
        }
        {
            String theId;
            theId = this.getId();
            strategy.appendField(locator, this, "id", buffer, theId);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            BigInteger theColumn;
            theColumn = this.getColumn();
            strategy.appendField(locator, this, "column", buffer, theColumn);
        }
        {
            BigInteger theRow;
            theRow = this.getRow();
            strategy.appendField(locator, this, "row", buffer, theRow);
        }
        {
            BigInteger theColspan;
            theColspan = this.getColspan();
            strategy.appendField(locator, this, "colspan", buffer, theColspan);
        }
        {
            BigInteger theRowspan;
            theRowspan = this.getRowspan();
            strategy.appendField(locator, this, "rowspan", buffer, theRowspan);
        }
        {
            String theFontSize;
            theFontSize = this.getFontSize();
            strategy.appendField(locator, this, "fontSize", buffer, theFontSize);
        }
        {
            String theTextColor;
            theTextColor = this.getTextColor();
            strategy.appendField(locator, this, "textColor", buffer, theTextColor);
        }
        {
            String theCustomBackgroundColor;
            theCustomBackgroundColor = this.getCustomBackgroundColor();
            strategy.appendField(locator, this, "customBackgroundColor", buffer, theCustomBackgroundColor);
        }
        {
            Boolean theBold;
            theBold = this.isBold();
            strategy.appendField(locator, this, "bold", buffer, theBold);
        }
        {
            Boolean theItalic;
            theItalic = this.isItalic();
            strategy.appendField(locator, this, "italic", buffer, theItalic);
        }
        {
            Boolean theUnderline;
            theUnderline = this.isUnderline();
            strategy.appendField(locator, this, "underline", buffer, theUnderline);
        }
        {
            String theVerticalAlign;
            theVerticalAlign = this.getVerticalAlign();
            strategy.appendField(locator, this, "verticalAlign", buffer, theVerticalAlign);
        }
        {
            String theHorizontalAlign;
            theHorizontalAlign = this.getHorizontalAlign();
            strategy.appendField(locator, this, "horizontalAlign", buffer, theHorizontalAlign);
        }
        {
            BigInteger theMinimumWidth;
            theMinimumWidth = this.getMinimumWidth();
            strategy.appendField(locator, this, "minimumWidth", buffer, theMinimumWidth);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            BigInteger theMinimumHeight;
            theMinimumHeight = this.getMinimumHeight();
            strategy.appendField(locator, this, "minimumHeight", buffer, theMinimumHeight);
        }
        {
            BigInteger thePreferredWidth;
            thePreferredWidth = this.getPreferredWidth();
            strategy.appendField(locator, this, "preferredWidth", buffer, thePreferredWidth);
        }
        {
            BigInteger thePreferredHeight;
            thePreferredHeight = this.getPreferredHeight();
            strategy.appendField(locator, this, "preferredHeight", buffer, thePreferredHeight);
        }
        {
            String theNextFocusComponent;
            theNextFocusComponent = this.getNextFocusComponent();
            strategy.appendField(locator, this, "nextFocusComponent", buffer, theNextFocusComponent);
        }
        {
            String theNextFocusField;
            theNextFocusField = this.getNextFocusField();
            strategy.appendField(locator, this, "nextFocusField", buffer, theNextFocusField);
        }
        {
            String theDescriptionAttr;
            theDescriptionAttr = this.getDescriptionAttr();
            strategy.appendField(locator, this, "descriptionAttr", buffer, theDescriptionAttr);
        }
        {
            Boolean theInitialFocus;
            theInitialFocus = this.isInitialFocus();
            strategy.appendField(locator, this, "initialFocus", buffer, theInitialFocus);
        }
        {
            String theAlternativeTooltip;
            theAlternativeTooltip = this.getAlternativeTooltip();
            strategy.appendField(locator, this, "alternativeTooltip", buffer, theAlternativeTooltip);
        }
        {
            Boolean theHasTextModules;
            theHasTextModules = this.isHasTextModules();
            strategy.appendField(locator, this, "hasTextModules", buffer, theHasTextModules);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebComponent)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final WebComponent that = ((WebComponent) object);
        {
            List<WebAdvancedProperty> lhsAdvancedProperties;
            lhsAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
            List<WebAdvancedProperty> rhsAdvancedProperties;
            rhsAdvancedProperties = (((that.advancedProperties!= null)&&(!that.advancedProperties.isEmpty()))?that.getAdvancedProperties():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "advancedProperties", lhsAdvancedProperties), LocatorUtils.property(thatLocator, "advancedProperties", rhsAdvancedProperties), lhsAdvancedProperties, rhsAdvancedProperties)) {
                return false;
            }
        }
        {
            WebBorderProperty lhsBorderProperty;
            lhsBorderProperty = this.getBorderProperty();
            WebBorderProperty rhsBorderProperty;
            rhsBorderProperty = that.getBorderProperty();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "borderProperty", lhsBorderProperty), LocatorUtils.property(thatLocator, "borderProperty", rhsBorderProperty), lhsBorderProperty, rhsBorderProperty)) {
                return false;
            }
        }
        {
            String lhsId;
            lhsId = this.getId();
            String rhsId;
            rhsId = that.getId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            BigInteger lhsColumn;
            lhsColumn = this.getColumn();
            BigInteger rhsColumn;
            rhsColumn = that.getColumn();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "column", lhsColumn), LocatorUtils.property(thatLocator, "column", rhsColumn), lhsColumn, rhsColumn)) {
                return false;
            }
        }
        {
            BigInteger lhsRow;
            lhsRow = this.getRow();
            BigInteger rhsRow;
            rhsRow = that.getRow();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "row", lhsRow), LocatorUtils.property(thatLocator, "row", rhsRow), lhsRow, rhsRow)) {
                return false;
            }
        }
        {
            BigInteger lhsColspan;
            lhsColspan = this.getColspan();
            BigInteger rhsColspan;
            rhsColspan = that.getColspan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "colspan", lhsColspan), LocatorUtils.property(thatLocator, "colspan", rhsColspan), lhsColspan, rhsColspan)) {
                return false;
            }
        }
        {
            BigInteger lhsRowspan;
            lhsRowspan = this.getRowspan();
            BigInteger rhsRowspan;
            rhsRowspan = that.getRowspan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rowspan", lhsRowspan), LocatorUtils.property(thatLocator, "rowspan", rhsRowspan), lhsRowspan, rhsRowspan)) {
                return false;
            }
        }
        {
            String lhsFontSize;
            lhsFontSize = this.getFontSize();
            String rhsFontSize;
            rhsFontSize = that.getFontSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fontSize", lhsFontSize), LocatorUtils.property(thatLocator, "fontSize", rhsFontSize), lhsFontSize, rhsFontSize)) {
                return false;
            }
        }
        {
            String lhsTextColor;
            lhsTextColor = this.getTextColor();
            String rhsTextColor;
            rhsTextColor = that.getTextColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textColor", lhsTextColor), LocatorUtils.property(thatLocator, "textColor", rhsTextColor), lhsTextColor, rhsTextColor)) {
                return false;
            }
        }
        {
            String lhsCustomBackgroundColor;
            lhsCustomBackgroundColor = this.getCustomBackgroundColor();
            String rhsCustomBackgroundColor;
            rhsCustomBackgroundColor = that.getCustomBackgroundColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customBackgroundColor", lhsCustomBackgroundColor), LocatorUtils.property(thatLocator, "customBackgroundColor", rhsCustomBackgroundColor), lhsCustomBackgroundColor, rhsCustomBackgroundColor)) {
                return false;
            }
        }
        {
            Boolean lhsBold;
            lhsBold = this.isBold();
            Boolean rhsBold;
            rhsBold = that.isBold();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bold", lhsBold), LocatorUtils.property(thatLocator, "bold", rhsBold), lhsBold, rhsBold)) {
                return false;
            }
        }
        {
            Boolean lhsItalic;
            lhsItalic = this.isItalic();
            Boolean rhsItalic;
            rhsItalic = that.isItalic();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "italic", lhsItalic), LocatorUtils.property(thatLocator, "italic", rhsItalic), lhsItalic, rhsItalic)) {
                return false;
            }
        }
        {
            Boolean lhsUnderline;
            lhsUnderline = this.isUnderline();
            Boolean rhsUnderline;
            rhsUnderline = that.isUnderline();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "underline", lhsUnderline), LocatorUtils.property(thatLocator, "underline", rhsUnderline), lhsUnderline, rhsUnderline)) {
                return false;
            }
        }
        {
            String lhsVerticalAlign;
            lhsVerticalAlign = this.getVerticalAlign();
            String rhsVerticalAlign;
            rhsVerticalAlign = that.getVerticalAlign();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "verticalAlign", lhsVerticalAlign), LocatorUtils.property(thatLocator, "verticalAlign", rhsVerticalAlign), lhsVerticalAlign, rhsVerticalAlign)) {
                return false;
            }
        }
        {
            String lhsHorizontalAlign;
            lhsHorizontalAlign = this.getHorizontalAlign();
            String rhsHorizontalAlign;
            rhsHorizontalAlign = that.getHorizontalAlign();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "horizontalAlign", lhsHorizontalAlign), LocatorUtils.property(thatLocator, "horizontalAlign", rhsHorizontalAlign), lhsHorizontalAlign, rhsHorizontalAlign)) {
                return false;
            }
        }
        {
            BigInteger lhsMinimumWidth;
            lhsMinimumWidth = this.getMinimumWidth();
            BigInteger rhsMinimumWidth;
            rhsMinimumWidth = that.getMinimumWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumWidth", lhsMinimumWidth), LocatorUtils.property(thatLocator, "minimumWidth", rhsMinimumWidth), lhsMinimumWidth, rhsMinimumWidth)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            BigInteger lhsMinimumHeight;
            lhsMinimumHeight = this.getMinimumHeight();
            BigInteger rhsMinimumHeight;
            rhsMinimumHeight = that.getMinimumHeight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "minimumHeight", lhsMinimumHeight), LocatorUtils.property(thatLocator, "minimumHeight", rhsMinimumHeight), lhsMinimumHeight, rhsMinimumHeight)) {
                return false;
            }
        }
        {
            BigInteger lhsPreferredWidth;
            lhsPreferredWidth = this.getPreferredWidth();
            BigInteger rhsPreferredWidth;
            rhsPreferredWidth = that.getPreferredWidth();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredWidth", lhsPreferredWidth), LocatorUtils.property(thatLocator, "preferredWidth", rhsPreferredWidth), lhsPreferredWidth, rhsPreferredWidth)) {
                return false;
            }
        }
        {
            BigInteger lhsPreferredHeight;
            lhsPreferredHeight = this.getPreferredHeight();
            BigInteger rhsPreferredHeight;
            rhsPreferredHeight = that.getPreferredHeight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferredHeight", lhsPreferredHeight), LocatorUtils.property(thatLocator, "preferredHeight", rhsPreferredHeight), lhsPreferredHeight, rhsPreferredHeight)) {
                return false;
            }
        }
        {
            String lhsNextFocusComponent;
            lhsNextFocusComponent = this.getNextFocusComponent();
            String rhsNextFocusComponent;
            rhsNextFocusComponent = that.getNextFocusComponent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextFocusComponent", lhsNextFocusComponent), LocatorUtils.property(thatLocator, "nextFocusComponent", rhsNextFocusComponent), lhsNextFocusComponent, rhsNextFocusComponent)) {
                return false;
            }
        }
        {
            String lhsNextFocusField;
            lhsNextFocusField = this.getNextFocusField();
            String rhsNextFocusField;
            rhsNextFocusField = that.getNextFocusField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nextFocusField", lhsNextFocusField), LocatorUtils.property(thatLocator, "nextFocusField", rhsNextFocusField), lhsNextFocusField, rhsNextFocusField)) {
                return false;
            }
        }
        {
            String lhsDescriptionAttr;
            lhsDescriptionAttr = this.getDescriptionAttr();
            String rhsDescriptionAttr;
            rhsDescriptionAttr = that.getDescriptionAttr();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "descriptionAttr", lhsDescriptionAttr), LocatorUtils.property(thatLocator, "descriptionAttr", rhsDescriptionAttr), lhsDescriptionAttr, rhsDescriptionAttr)) {
                return false;
            }
        }
        {
            Boolean lhsInitialFocus;
            lhsInitialFocus = this.isInitialFocus();
            Boolean rhsInitialFocus;
            rhsInitialFocus = that.isInitialFocus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "initialFocus", lhsInitialFocus), LocatorUtils.property(thatLocator, "initialFocus", rhsInitialFocus), lhsInitialFocus, rhsInitialFocus)) {
                return false;
            }
        }
        {
            String lhsAlternativeTooltip;
            lhsAlternativeTooltip = this.getAlternativeTooltip();
            String rhsAlternativeTooltip;
            rhsAlternativeTooltip = that.getAlternativeTooltip();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "alternativeTooltip", lhsAlternativeTooltip), LocatorUtils.property(thatLocator, "alternativeTooltip", rhsAlternativeTooltip), lhsAlternativeTooltip, rhsAlternativeTooltip)) {
                return false;
            }
        }
        {
            Boolean lhsHasTextModules;
            lhsHasTextModules = this.isHasTextModules();
            Boolean rhsHasTextModules;
            rhsHasTextModules = that.isHasTextModules();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hasTextModules", lhsHasTextModules), LocatorUtils.property(thatLocator, "hasTextModules", rhsHasTextModules), lhsHasTextModules, rhsHasTextModules)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<WebAdvancedProperty> theAdvancedProperties;
            theAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "advancedProperties", theAdvancedProperties), currentHashCode, theAdvancedProperties);
        }
        {
            WebBorderProperty theBorderProperty;
            theBorderProperty = this.getBorderProperty();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "borderProperty", theBorderProperty), currentHashCode, theBorderProperty);
        }
        {
            String theId;
            theId = this.getId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            BigInteger theColumn;
            theColumn = this.getColumn();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "column", theColumn), currentHashCode, theColumn);
        }
        {
            BigInteger theRow;
            theRow = this.getRow();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "row", theRow), currentHashCode, theRow);
        }
        {
            BigInteger theColspan;
            theColspan = this.getColspan();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "colspan", theColspan), currentHashCode, theColspan);
        }
        {
            BigInteger theRowspan;
            theRowspan = this.getRowspan();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rowspan", theRowspan), currentHashCode, theRowspan);
        }
        {
            String theFontSize;
            theFontSize = this.getFontSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fontSize", theFontSize), currentHashCode, theFontSize);
        }
        {
            String theTextColor;
            theTextColor = this.getTextColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "textColor", theTextColor), currentHashCode, theTextColor);
        }
        {
            String theCustomBackgroundColor;
            theCustomBackgroundColor = this.getCustomBackgroundColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customBackgroundColor", theCustomBackgroundColor), currentHashCode, theCustomBackgroundColor);
        }
        {
            Boolean theBold;
            theBold = this.isBold();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bold", theBold), currentHashCode, theBold);
        }
        {
            Boolean theItalic;
            theItalic = this.isItalic();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "italic", theItalic), currentHashCode, theItalic);
        }
        {
            Boolean theUnderline;
            theUnderline = this.isUnderline();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "underline", theUnderline), currentHashCode, theUnderline);
        }
        {
            String theVerticalAlign;
            theVerticalAlign = this.getVerticalAlign();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "verticalAlign", theVerticalAlign), currentHashCode, theVerticalAlign);
        }
        {
            String theHorizontalAlign;
            theHorizontalAlign = this.getHorizontalAlign();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "horizontalAlign", theHorizontalAlign), currentHashCode, theHorizontalAlign);
        }
        {
            BigInteger theMinimumWidth;
            theMinimumWidth = this.getMinimumWidth();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumWidth", theMinimumWidth), currentHashCode, theMinimumWidth);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            BigInteger theMinimumHeight;
            theMinimumHeight = this.getMinimumHeight();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "minimumHeight", theMinimumHeight), currentHashCode, theMinimumHeight);
        }
        {
            BigInteger thePreferredWidth;
            thePreferredWidth = this.getPreferredWidth();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredWidth", thePreferredWidth), currentHashCode, thePreferredWidth);
        }
        {
            BigInteger thePreferredHeight;
            thePreferredHeight = this.getPreferredHeight();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferredHeight", thePreferredHeight), currentHashCode, thePreferredHeight);
        }
        {
            String theNextFocusComponent;
            theNextFocusComponent = this.getNextFocusComponent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextFocusComponent", theNextFocusComponent), currentHashCode, theNextFocusComponent);
        }
        {
            String theNextFocusField;
            theNextFocusField = this.getNextFocusField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nextFocusField", theNextFocusField), currentHashCode, theNextFocusField);
        }
        {
            String theDescriptionAttr;
            theDescriptionAttr = this.getDescriptionAttr();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "descriptionAttr", theDescriptionAttr), currentHashCode, theDescriptionAttr);
        }
        {
            Boolean theInitialFocus;
            theInitialFocus = this.isInitialFocus();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "initialFocus", theInitialFocus), currentHashCode, theInitialFocus);
        }
        {
            String theAlternativeTooltip;
            theAlternativeTooltip = this.getAlternativeTooltip();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "alternativeTooltip", theAlternativeTooltip), currentHashCode, theAlternativeTooltip);
        }
        {
            Boolean theHasTextModules;
            theHasTextModules = this.isHasTextModules();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "hasTextModules", theHasTextModules), currentHashCode, theHasTextModules);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        if (target instanceof WebComponent) {
            final WebComponent copy = ((WebComponent) target);
            if ((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty())) {
                List<WebAdvancedProperty> sourceAdvancedProperties;
                sourceAdvancedProperties = (((this.advancedProperties!= null)&&(!this.advancedProperties.isEmpty()))?this.getAdvancedProperties():null);
                @SuppressWarnings("unchecked")
                List<WebAdvancedProperty> copyAdvancedProperties = ((List<WebAdvancedProperty> ) strategy.copy(LocatorUtils.property(locator, "advancedProperties", sourceAdvancedProperties), sourceAdvancedProperties));
                copy.advancedProperties = null;
                if (copyAdvancedProperties!= null) {
                    List<WebAdvancedProperty> uniqueAdvancedPropertiesl = copy.getAdvancedProperties();
                    uniqueAdvancedPropertiesl.addAll(copyAdvancedProperties);
                }
            } else {
                copy.advancedProperties = null;
            }
            if (this.borderProperty!= null) {
                WebBorderProperty sourceBorderProperty;
                sourceBorderProperty = this.getBorderProperty();
                WebBorderProperty copyBorderProperty = ((WebBorderProperty) strategy.copy(LocatorUtils.property(locator, "borderProperty", sourceBorderProperty), sourceBorderProperty));
                copy.setBorderProperty(copyBorderProperty);
            } else {
                copy.borderProperty = null;
            }
            if (this.id!= null) {
                String sourceId;
                sourceId = this.getId();
                String copyId = ((String) strategy.copy(LocatorUtils.property(locator, "id", sourceId), sourceId));
                copy.setId(copyId);
            } else {
                copy.id = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.column!= null) {
                BigInteger sourceColumn;
                sourceColumn = this.getColumn();
                BigInteger copyColumn = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "column", sourceColumn), sourceColumn));
                copy.setColumn(copyColumn);
            } else {
                copy.column = null;
            }
            if (this.row!= null) {
                BigInteger sourceRow;
                sourceRow = this.getRow();
                BigInteger copyRow = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "row", sourceRow), sourceRow));
                copy.setRow(copyRow);
            } else {
                copy.row = null;
            }
            if (this.colspan!= null) {
                BigInteger sourceColspan;
                sourceColspan = this.getColspan();
                BigInteger copyColspan = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "colspan", sourceColspan), sourceColspan));
                copy.setColspan(copyColspan);
            } else {
                copy.colspan = null;
            }
            if (this.rowspan!= null) {
                BigInteger sourceRowspan;
                sourceRowspan = this.getRowspan();
                BigInteger copyRowspan = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "rowspan", sourceRowspan), sourceRowspan));
                copy.setRowspan(copyRowspan);
            } else {
                copy.rowspan = null;
            }
            if (this.fontSize!= null) {
                String sourceFontSize;
                sourceFontSize = this.getFontSize();
                String copyFontSize = ((String) strategy.copy(LocatorUtils.property(locator, "fontSize", sourceFontSize), sourceFontSize));
                copy.setFontSize(copyFontSize);
            } else {
                copy.fontSize = null;
            }
            if (this.textColor!= null) {
                String sourceTextColor;
                sourceTextColor = this.getTextColor();
                String copyTextColor = ((String) strategy.copy(LocatorUtils.property(locator, "textColor", sourceTextColor), sourceTextColor));
                copy.setTextColor(copyTextColor);
            } else {
                copy.textColor = null;
            }
            if (this.customBackgroundColor!= null) {
                String sourceCustomBackgroundColor;
                sourceCustomBackgroundColor = this.getCustomBackgroundColor();
                String copyCustomBackgroundColor = ((String) strategy.copy(LocatorUtils.property(locator, "customBackgroundColor", sourceCustomBackgroundColor), sourceCustomBackgroundColor));
                copy.setCustomBackgroundColor(copyCustomBackgroundColor);
            } else {
                copy.customBackgroundColor = null;
            }
            if (this.bold!= null) {
                Boolean sourceBold;
                sourceBold = this.isBold();
                Boolean copyBold = ((Boolean) strategy.copy(LocatorUtils.property(locator, "bold", sourceBold), sourceBold));
                copy.setBold(copyBold);
            } else {
                copy.bold = null;
            }
            if (this.italic!= null) {
                Boolean sourceItalic;
                sourceItalic = this.isItalic();
                Boolean copyItalic = ((Boolean) strategy.copy(LocatorUtils.property(locator, "italic", sourceItalic), sourceItalic));
                copy.setItalic(copyItalic);
            } else {
                copy.italic = null;
            }
            if (this.underline!= null) {
                Boolean sourceUnderline;
                sourceUnderline = this.isUnderline();
                Boolean copyUnderline = ((Boolean) strategy.copy(LocatorUtils.property(locator, "underline", sourceUnderline), sourceUnderline));
                copy.setUnderline(copyUnderline);
            } else {
                copy.underline = null;
            }
            if (this.verticalAlign!= null) {
                String sourceVerticalAlign;
                sourceVerticalAlign = this.getVerticalAlign();
                String copyVerticalAlign = ((String) strategy.copy(LocatorUtils.property(locator, "verticalAlign", sourceVerticalAlign), sourceVerticalAlign));
                copy.setVerticalAlign(copyVerticalAlign);
            } else {
                copy.verticalAlign = null;
            }
            if (this.horizontalAlign!= null) {
                String sourceHorizontalAlign;
                sourceHorizontalAlign = this.getHorizontalAlign();
                String copyHorizontalAlign = ((String) strategy.copy(LocatorUtils.property(locator, "horizontalAlign", sourceHorizontalAlign), sourceHorizontalAlign));
                copy.setHorizontalAlign(copyHorizontalAlign);
            } else {
                copy.horizontalAlign = null;
            }
            if (this.minimumWidth!= null) {
                BigInteger sourceMinimumWidth;
                sourceMinimumWidth = this.getMinimumWidth();
                BigInteger copyMinimumWidth = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "minimumWidth", sourceMinimumWidth), sourceMinimumWidth));
                copy.setMinimumWidth(copyMinimumWidth);
            } else {
                copy.minimumWidth = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.minimumHeight!= null) {
                BigInteger sourceMinimumHeight;
                sourceMinimumHeight = this.getMinimumHeight();
                BigInteger copyMinimumHeight = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "minimumHeight", sourceMinimumHeight), sourceMinimumHeight));
                copy.setMinimumHeight(copyMinimumHeight);
            } else {
                copy.minimumHeight = null;
            }
            if (this.preferredWidth!= null) {
                BigInteger sourcePreferredWidth;
                sourcePreferredWidth = this.getPreferredWidth();
                BigInteger copyPreferredWidth = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "preferredWidth", sourcePreferredWidth), sourcePreferredWidth));
                copy.setPreferredWidth(copyPreferredWidth);
            } else {
                copy.preferredWidth = null;
            }
            if (this.preferredHeight!= null) {
                BigInteger sourcePreferredHeight;
                sourcePreferredHeight = this.getPreferredHeight();
                BigInteger copyPreferredHeight = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "preferredHeight", sourcePreferredHeight), sourcePreferredHeight));
                copy.setPreferredHeight(copyPreferredHeight);
            } else {
                copy.preferredHeight = null;
            }
            if (this.nextFocusComponent!= null) {
                String sourceNextFocusComponent;
                sourceNextFocusComponent = this.getNextFocusComponent();
                String copyNextFocusComponent = ((String) strategy.copy(LocatorUtils.property(locator, "nextFocusComponent", sourceNextFocusComponent), sourceNextFocusComponent));
                copy.setNextFocusComponent(copyNextFocusComponent);
            } else {
                copy.nextFocusComponent = null;
            }
            if (this.nextFocusField!= null) {
                String sourceNextFocusField;
                sourceNextFocusField = this.getNextFocusField();
                String copyNextFocusField = ((String) strategy.copy(LocatorUtils.property(locator, "nextFocusField", sourceNextFocusField), sourceNextFocusField));
                copy.setNextFocusField(copyNextFocusField);
            } else {
                copy.nextFocusField = null;
            }
            if (this.descriptionAttr!= null) {
                String sourceDescriptionAttr;
                sourceDescriptionAttr = this.getDescriptionAttr();
                String copyDescriptionAttr = ((String) strategy.copy(LocatorUtils.property(locator, "descriptionAttr", sourceDescriptionAttr), sourceDescriptionAttr));
                copy.setDescriptionAttr(copyDescriptionAttr);
            } else {
                copy.descriptionAttr = null;
            }
            if (this.initialFocus!= null) {
                Boolean sourceInitialFocus;
                sourceInitialFocus = this.isInitialFocus();
                Boolean copyInitialFocus = ((Boolean) strategy.copy(LocatorUtils.property(locator, "initialFocus", sourceInitialFocus), sourceInitialFocus));
                copy.setInitialFocus(copyInitialFocus);
            } else {
                copy.initialFocus = null;
            }
            if (this.alternativeTooltip!= null) {
                String sourceAlternativeTooltip;
                sourceAlternativeTooltip = this.getAlternativeTooltip();
                String copyAlternativeTooltip = ((String) strategy.copy(LocatorUtils.property(locator, "alternativeTooltip", sourceAlternativeTooltip), sourceAlternativeTooltip));
                copy.setAlternativeTooltip(copyAlternativeTooltip);
            } else {
                copy.alternativeTooltip = null;
            }
            if (this.hasTextModules!= null) {
                Boolean sourceHasTextModules;
                sourceHasTextModules = this.isHasTextModules();
                Boolean copyHasTextModules = ((Boolean) strategy.copy(LocatorUtils.property(locator, "hasTextModules", sourceHasTextModules), sourceHasTextModules));
                copy.setHasTextModules(copyHasTextModules);
            } else {
                copy.hasTextModules = null;
            }
        }
        return target;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebComponent.Builder<_B> _other) {
        if (this.advancedProperties == null) {
            _other.advancedProperties = null;
        } else {
            _other.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
            for (WebAdvancedProperty _item: this.advancedProperties) {
                _other.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.borderProperty = ((this.borderProperty == null)?null:this.borderProperty.newCopyBuilder(_other));
        _other.id = this.id;
        _other.name = this.name;
        _other.column = this.column;
        _other.row = this.row;
        _other.colspan = this.colspan;
        _other.rowspan = this.rowspan;
        _other.fontSize = this.fontSize;
        _other.textColor = this.textColor;
        _other.customBackgroundColor = this.customBackgroundColor;
        _other.bold = this.bold;
        _other.italic = this.italic;
        _other.underline = this.underline;
        _other.verticalAlign = this.verticalAlign;
        _other.horizontalAlign = this.horizontalAlign;
        _other.minimumWidth = this.minimumWidth;
        _other.label = this.label;
        _other.minimumHeight = this.minimumHeight;
        _other.preferredWidth = this.preferredWidth;
        _other.preferredHeight = this.preferredHeight;
        _other.nextFocusComponent = this.nextFocusComponent;
        _other.nextFocusField = this.nextFocusField;
        _other.descriptionAttr = this.descriptionAttr;
        _other.initialFocus = this.initialFocus;
        _other.alternativeTooltip = this.alternativeTooltip;
        _other.hasTextModules = this.hasTextModules;
    }

    public abstract<_B >WebComponent.Builder<_B> newCopyBuilder(final _B _parentBuilder);

    public abstract WebComponent.Builder<Void> newCopyBuilder();

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebComponent.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree advancedPropertiesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("advancedProperties"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(advancedPropertiesPropertyTree!= null):((advancedPropertiesPropertyTree == null)||(!advancedPropertiesPropertyTree.isLeaf())))) {
            if (this.advancedProperties == null) {
                _other.advancedProperties = null;
            } else {
                _other.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
                for (WebAdvancedProperty _item: this.advancedProperties) {
                    _other.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(_other, advancedPropertiesPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree borderPropertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("borderProperty"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyPropertyTree!= null):((borderPropertyPropertyTree == null)||(!borderPropertyPropertyTree.isLeaf())))) {
            _other.borderProperty = ((this.borderProperty == null)?null:this.borderProperty.newCopyBuilder(_other, borderPropertyPropertyTree, _propertyTreeUse));
        }
        final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
            _other.id = this.id;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree columnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("column"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnPropertyTree!= null):((columnPropertyTree == null)||(!columnPropertyTree.isLeaf())))) {
            _other.column = this.column;
        }
        final PropertyTree rowPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("row"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowPropertyTree!= null):((rowPropertyTree == null)||(!rowPropertyTree.isLeaf())))) {
            _other.row = this.row;
        }
        final PropertyTree colspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("colspan"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colspanPropertyTree!= null):((colspanPropertyTree == null)||(!colspanPropertyTree.isLeaf())))) {
            _other.colspan = this.colspan;
        }
        final PropertyTree rowspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rowspan"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowspanPropertyTree!= null):((rowspanPropertyTree == null)||(!rowspanPropertyTree.isLeaf())))) {
            _other.rowspan = this.rowspan;
        }
        final PropertyTree fontSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fontSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontSizePropertyTree!= null):((fontSizePropertyTree == null)||(!fontSizePropertyTree.isLeaf())))) {
            _other.fontSize = this.fontSize;
        }
        final PropertyTree textColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textColor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textColorPropertyTree!= null):((textColorPropertyTree == null)||(!textColorPropertyTree.isLeaf())))) {
            _other.textColor = this.textColor;
        }
        final PropertyTree customBackgroundColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customBackgroundColor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customBackgroundColorPropertyTree!= null):((customBackgroundColorPropertyTree == null)||(!customBackgroundColorPropertyTree.isLeaf())))) {
            _other.customBackgroundColor = this.customBackgroundColor;
        }
        final PropertyTree boldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bold"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boldPropertyTree!= null):((boldPropertyTree == null)||(!boldPropertyTree.isLeaf())))) {
            _other.bold = this.bold;
        }
        final PropertyTree italicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("italic"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(italicPropertyTree!= null):((italicPropertyTree == null)||(!italicPropertyTree.isLeaf())))) {
            _other.italic = this.italic;
        }
        final PropertyTree underlinePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("underline"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(underlinePropertyTree!= null):((underlinePropertyTree == null)||(!underlinePropertyTree.isLeaf())))) {
            _other.underline = this.underline;
        }
        final PropertyTree verticalAlignPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("verticalAlign"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(verticalAlignPropertyTree!= null):((verticalAlignPropertyTree == null)||(!verticalAlignPropertyTree.isLeaf())))) {
            _other.verticalAlign = this.verticalAlign;
        }
        final PropertyTree horizontalAlignPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("horizontalAlign"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(horizontalAlignPropertyTree!= null):((horizontalAlignPropertyTree == null)||(!horizontalAlignPropertyTree.isLeaf())))) {
            _other.horizontalAlign = this.horizontalAlign;
        }
        final PropertyTree minimumWidthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumWidth"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumWidthPropertyTree!= null):((minimumWidthPropertyTree == null)||(!minimumWidthPropertyTree.isLeaf())))) {
            _other.minimumWidth = this.minimumWidth;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree minimumHeightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumHeight"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumHeightPropertyTree!= null):((minimumHeightPropertyTree == null)||(!minimumHeightPropertyTree.isLeaf())))) {
            _other.minimumHeight = this.minimumHeight;
        }
        final PropertyTree preferredWidthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredWidth"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredWidthPropertyTree!= null):((preferredWidthPropertyTree == null)||(!preferredWidthPropertyTree.isLeaf())))) {
            _other.preferredWidth = this.preferredWidth;
        }
        final PropertyTree preferredHeightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredHeight"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredHeightPropertyTree!= null):((preferredHeightPropertyTree == null)||(!preferredHeightPropertyTree.isLeaf())))) {
            _other.preferredHeight = this.preferredHeight;
        }
        final PropertyTree nextFocusComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusComponent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusComponentPropertyTree!= null):((nextFocusComponentPropertyTree == null)||(!nextFocusComponentPropertyTree.isLeaf())))) {
            _other.nextFocusComponent = this.nextFocusComponent;
        }
        final PropertyTree nextFocusFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusFieldPropertyTree!= null):((nextFocusFieldPropertyTree == null)||(!nextFocusFieldPropertyTree.isLeaf())))) {
            _other.nextFocusField = this.nextFocusField;
        }
        final PropertyTree descriptionAttrPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("descriptionAttr"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionAttrPropertyTree!= null):((descriptionAttrPropertyTree == null)||(!descriptionAttrPropertyTree.isLeaf())))) {
            _other.descriptionAttr = this.descriptionAttr;
        }
        final PropertyTree initialFocusPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("initialFocus"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(initialFocusPropertyTree!= null):((initialFocusPropertyTree == null)||(!initialFocusPropertyTree.isLeaf())))) {
            _other.initialFocus = this.initialFocus;
        }
        final PropertyTree alternativeTooltipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("alternativeTooltip"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(alternativeTooltipPropertyTree!= null):((alternativeTooltipPropertyTree == null)||(!alternativeTooltipPropertyTree.isLeaf())))) {
            _other.alternativeTooltip = this.alternativeTooltip;
        }
        final PropertyTree hasTextModulesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("hasTextModules"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(hasTextModulesPropertyTree!= null):((hasTextModulesPropertyTree == null)||(!hasTextModulesPropertyTree.isLeaf())))) {
            _other.hasTextModules = this.hasTextModules;
        }
    }

    public abstract<_B >WebComponent.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse);

    public abstract WebComponent.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse);

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final WebComponent _storedValue;
        private List<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>> advancedProperties;
        private WebBorderProperty.Builder<WebComponent.Builder<_B>> borderProperty;
        private String id;
        private String name;
        private BigInteger column;
        private BigInteger row;
        private BigInteger colspan;
        private BigInteger rowspan;
        private String fontSize;
        private String textColor;
        private String customBackgroundColor;
        private Boolean bold;
        private Boolean italic;
        private Boolean underline;
        private String verticalAlign;
        private String horizontalAlign;
        private BigInteger minimumWidth;
        private String label;
        private BigInteger minimumHeight;
        private BigInteger preferredWidth;
        private BigInteger preferredHeight;
        private String nextFocusComponent;
        private String nextFocusField;
        private String descriptionAttr;
        private Boolean initialFocus;
        private String alternativeTooltip;
        private Boolean hasTextModules;

        public Builder(final _B _parentBuilder, final WebComponent _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.advancedProperties == null) {
                        this.advancedProperties = null;
                    } else {
                        this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
                        for (WebAdvancedProperty _item: _other.advancedProperties) {
                            this.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.borderProperty = ((_other.borderProperty == null)?null:_other.borderProperty.newCopyBuilder(this));
                    this.id = _other.id;
                    this.name = _other.name;
                    this.column = _other.column;
                    this.row = _other.row;
                    this.colspan = _other.colspan;
                    this.rowspan = _other.rowspan;
                    this.fontSize = _other.fontSize;
                    this.textColor = _other.textColor;
                    this.customBackgroundColor = _other.customBackgroundColor;
                    this.bold = _other.bold;
                    this.italic = _other.italic;
                    this.underline = _other.underline;
                    this.verticalAlign = _other.verticalAlign;
                    this.horizontalAlign = _other.horizontalAlign;
                    this.minimumWidth = _other.minimumWidth;
                    this.label = _other.label;
                    this.minimumHeight = _other.minimumHeight;
                    this.preferredWidth = _other.preferredWidth;
                    this.preferredHeight = _other.preferredHeight;
                    this.nextFocusComponent = _other.nextFocusComponent;
                    this.nextFocusField = _other.nextFocusField;
                    this.descriptionAttr = _other.descriptionAttr;
                    this.initialFocus = _other.initialFocus;
                    this.alternativeTooltip = _other.alternativeTooltip;
                    this.hasTextModules = _other.hasTextModules;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final WebComponent _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree advancedPropertiesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("advancedProperties"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(advancedPropertiesPropertyTree!= null):((advancedPropertiesPropertyTree == null)||(!advancedPropertiesPropertyTree.isLeaf())))) {
                        if (_other.advancedProperties == null) {
                            this.advancedProperties = null;
                        } else {
                            this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
                            for (WebAdvancedProperty _item: _other.advancedProperties) {
                                this.advancedProperties.add(((_item == null)?null:_item.newCopyBuilder(this, advancedPropertiesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree borderPropertyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("borderProperty"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(borderPropertyPropertyTree!= null):((borderPropertyPropertyTree == null)||(!borderPropertyPropertyTree.isLeaf())))) {
                        this.borderProperty = ((_other.borderProperty == null)?null:_other.borderProperty.newCopyBuilder(this, borderPropertyPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
                        this.id = _other.id;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree columnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("column"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(columnPropertyTree!= null):((columnPropertyTree == null)||(!columnPropertyTree.isLeaf())))) {
                        this.column = _other.column;
                    }
                    final PropertyTree rowPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("row"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowPropertyTree!= null):((rowPropertyTree == null)||(!rowPropertyTree.isLeaf())))) {
                        this.row = _other.row;
                    }
                    final PropertyTree colspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("colspan"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colspanPropertyTree!= null):((colspanPropertyTree == null)||(!colspanPropertyTree.isLeaf())))) {
                        this.colspan = _other.colspan;
                    }
                    final PropertyTree rowspanPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rowspan"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowspanPropertyTree!= null):((rowspanPropertyTree == null)||(!rowspanPropertyTree.isLeaf())))) {
                        this.rowspan = _other.rowspan;
                    }
                    final PropertyTree fontSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fontSize"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontSizePropertyTree!= null):((fontSizePropertyTree == null)||(!fontSizePropertyTree.isLeaf())))) {
                        this.fontSize = _other.fontSize;
                    }
                    final PropertyTree textColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("textColor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(textColorPropertyTree!= null):((textColorPropertyTree == null)||(!textColorPropertyTree.isLeaf())))) {
                        this.textColor = _other.textColor;
                    }
                    final PropertyTree customBackgroundColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("customBackgroundColor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(customBackgroundColorPropertyTree!= null):((customBackgroundColorPropertyTree == null)||(!customBackgroundColorPropertyTree.isLeaf())))) {
                        this.customBackgroundColor = _other.customBackgroundColor;
                    }
                    final PropertyTree boldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bold"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boldPropertyTree!= null):((boldPropertyTree == null)||(!boldPropertyTree.isLeaf())))) {
                        this.bold = _other.bold;
                    }
                    final PropertyTree italicPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("italic"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(italicPropertyTree!= null):((italicPropertyTree == null)||(!italicPropertyTree.isLeaf())))) {
                        this.italic = _other.italic;
                    }
                    final PropertyTree underlinePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("underline"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(underlinePropertyTree!= null):((underlinePropertyTree == null)||(!underlinePropertyTree.isLeaf())))) {
                        this.underline = _other.underline;
                    }
                    final PropertyTree verticalAlignPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("verticalAlign"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(verticalAlignPropertyTree!= null):((verticalAlignPropertyTree == null)||(!verticalAlignPropertyTree.isLeaf())))) {
                        this.verticalAlign = _other.verticalAlign;
                    }
                    final PropertyTree horizontalAlignPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("horizontalAlign"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(horizontalAlignPropertyTree!= null):((horizontalAlignPropertyTree == null)||(!horizontalAlignPropertyTree.isLeaf())))) {
                        this.horizontalAlign = _other.horizontalAlign;
                    }
                    final PropertyTree minimumWidthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumWidth"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumWidthPropertyTree!= null):((minimumWidthPropertyTree == null)||(!minimumWidthPropertyTree.isLeaf())))) {
                        this.minimumWidth = _other.minimumWidth;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree minimumHeightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("minimumHeight"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(minimumHeightPropertyTree!= null):((minimumHeightPropertyTree == null)||(!minimumHeightPropertyTree.isLeaf())))) {
                        this.minimumHeight = _other.minimumHeight;
                    }
                    final PropertyTree preferredWidthPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredWidth"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredWidthPropertyTree!= null):((preferredWidthPropertyTree == null)||(!preferredWidthPropertyTree.isLeaf())))) {
                        this.preferredWidth = _other.preferredWidth;
                    }
                    final PropertyTree preferredHeightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("preferredHeight"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(preferredHeightPropertyTree!= null):((preferredHeightPropertyTree == null)||(!preferredHeightPropertyTree.isLeaf())))) {
                        this.preferredHeight = _other.preferredHeight;
                    }
                    final PropertyTree nextFocusComponentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusComponent"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusComponentPropertyTree!= null):((nextFocusComponentPropertyTree == null)||(!nextFocusComponentPropertyTree.isLeaf())))) {
                        this.nextFocusComponent = _other.nextFocusComponent;
                    }
                    final PropertyTree nextFocusFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nextFocusField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nextFocusFieldPropertyTree!= null):((nextFocusFieldPropertyTree == null)||(!nextFocusFieldPropertyTree.isLeaf())))) {
                        this.nextFocusField = _other.nextFocusField;
                    }
                    final PropertyTree descriptionAttrPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("descriptionAttr"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionAttrPropertyTree!= null):((descriptionAttrPropertyTree == null)||(!descriptionAttrPropertyTree.isLeaf())))) {
                        this.descriptionAttr = _other.descriptionAttr;
                    }
                    final PropertyTree initialFocusPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("initialFocus"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(initialFocusPropertyTree!= null):((initialFocusPropertyTree == null)||(!initialFocusPropertyTree.isLeaf())))) {
                        this.initialFocus = _other.initialFocus;
                    }
                    final PropertyTree alternativeTooltipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("alternativeTooltip"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(alternativeTooltipPropertyTree!= null):((alternativeTooltipPropertyTree == null)||(!alternativeTooltipPropertyTree.isLeaf())))) {
                        this.alternativeTooltip = _other.alternativeTooltip;
                    }
                    final PropertyTree hasTextModulesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("hasTextModules"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(hasTextModulesPropertyTree!= null):((hasTextModulesPropertyTree == null)||(!hasTextModulesPropertyTree.isLeaf())))) {
                        this.hasTextModules = _other.hasTextModules;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends WebComponent >_P init(final _P _product) {
            if (this.advancedProperties!= null) {
                final List<WebAdvancedProperty> advancedProperties = new ArrayList<WebAdvancedProperty>(this.advancedProperties.size());
                for (WebAdvancedProperty.Builder<WebComponent.Builder<_B>> _item: this.advancedProperties) {
                    advancedProperties.add(_item.build());
                }
                _product.advancedProperties = advancedProperties;
            }
            _product.borderProperty = ((this.borderProperty == null)?null:this.borderProperty.build());
            _product.id = this.id;
            _product.name = this.name;
            _product.column = this.column;
            _product.row = this.row;
            _product.colspan = this.colspan;
            _product.rowspan = this.rowspan;
            _product.fontSize = this.fontSize;
            _product.textColor = this.textColor;
            _product.customBackgroundColor = this.customBackgroundColor;
            _product.bold = this.bold;
            _product.italic = this.italic;
            _product.underline = this.underline;
            _product.verticalAlign = this.verticalAlign;
            _product.horizontalAlign = this.horizontalAlign;
            _product.minimumWidth = this.minimumWidth;
            _product.label = this.label;
            _product.minimumHeight = this.minimumHeight;
            _product.preferredWidth = this.preferredWidth;
            _product.preferredHeight = this.preferredHeight;
            _product.nextFocusComponent = this.nextFocusComponent;
            _product.nextFocusField = this.nextFocusField;
            _product.descriptionAttr = this.descriptionAttr;
            _product.initialFocus = this.initialFocus;
            _product.alternativeTooltip = this.alternativeTooltip;
            _product.hasTextModules = this.hasTextModules;
            return _product;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        public WebComponent.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            if (advancedProperties!= null) {
                if (this.advancedProperties == null) {
                    this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
                }
                for (WebAdvancedProperty _item: advancedProperties) {
                    this.advancedProperties.add(new WebAdvancedProperty.Builder<WebComponent.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        public WebComponent.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            if (this.advancedProperties!= null) {
                this.advancedProperties.clear();
            }
            return addAdvancedProperties(advancedProperties);
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        public WebComponent.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            addAdvancedProperties(Arrays.asList(advancedProperties));
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        public WebComponent.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            withAdvancedProperties(Arrays.asList(advancedProperties));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "AdvancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "AdvancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        public WebAdvancedProperty.Builder<? extends WebComponent.Builder<_B>> addAdvancedProperties() {
            if (this.advancedProperties == null) {
                this.advancedProperties = new ArrayList<WebAdvancedProperty.Builder<WebComponent.Builder<_B>>>();
            }
            final WebAdvancedProperty.Builder<WebComponent.Builder<_B>> advancedProperties_Builder = new WebAdvancedProperty.Builder<WebComponent.Builder<_B>>(this, null, false);
            this.advancedProperties.add(advancedProperties_Builder);
            return advancedProperties_Builder;
        }

        /**
         * Sets the new value of "borderProperty" (any previous value will be replaced)
         * 
         * @param borderProperty
         *     New value of the "borderProperty" property.
         */
        public WebComponent.Builder<_B> withBorderProperty(final WebBorderProperty borderProperty) {
            this.borderProperty = ((borderProperty == null)?null:new WebBorderProperty.Builder<WebComponent.Builder<_B>>(this, borderProperty, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "borderProperty" property.
         * Use {@link org.nuclos.schema.layout.web.WebBorderProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "borderProperty" property.
         *     Use {@link org.nuclos.schema.layout.web.WebBorderProperty.Builder#end()} to return to the current builder.
         */
        public WebBorderProperty.Builder<? extends WebComponent.Builder<_B>> withBorderProperty() {
            if (this.borderProperty!= null) {
                return this.borderProperty;
            }
            return this.borderProperty = new WebBorderProperty.Builder<WebComponent.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        public WebComponent.Builder<_B> withId(final String id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public WebComponent.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        public WebComponent.Builder<_B> withColumn(final BigInteger column) {
            this.column = column;
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        public WebComponent.Builder<_B> withRow(final BigInteger row) {
            this.row = row;
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        public WebComponent.Builder<_B> withColspan(final BigInteger colspan) {
            this.colspan = colspan;
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        public WebComponent.Builder<_B> withRowspan(final BigInteger rowspan) {
            this.rowspan = rowspan;
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        public WebComponent.Builder<_B> withFontSize(final String fontSize) {
            this.fontSize = fontSize;
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        public WebComponent.Builder<_B> withTextColor(final String textColor) {
            this.textColor = textColor;
            return this;
        }

        /**
         * Sets the new value of "customBackgroundColor" (any previous value will be replaced)
         * 
         * @param customBackgroundColor
         *     New value of the "customBackgroundColor" property.
         */
        public WebComponent.Builder<_B> withCustomBackgroundColor(final String customBackgroundColor) {
            this.customBackgroundColor = customBackgroundColor;
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        public WebComponent.Builder<_B> withBold(final Boolean bold) {
            this.bold = bold;
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        public WebComponent.Builder<_B> withItalic(final Boolean italic) {
            this.italic = italic;
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        public WebComponent.Builder<_B> withUnderline(final Boolean underline) {
            this.underline = underline;
            return this;
        }

        /**
         * Sets the new value of "verticalAlign" (any previous value will be replaced)
         * 
         * @param verticalAlign
         *     New value of the "verticalAlign" property.
         */
        public WebComponent.Builder<_B> withVerticalAlign(final String verticalAlign) {
            this.verticalAlign = verticalAlign;
            return this;
        }

        /**
         * Sets the new value of "horizontalAlign" (any previous value will be replaced)
         * 
         * @param horizontalAlign
         *     New value of the "horizontalAlign" property.
         */
        public WebComponent.Builder<_B> withHorizontalAlign(final String horizontalAlign) {
            this.horizontalAlign = horizontalAlign;
            return this;
        }

        /**
         * Sets the new value of "minimumWidth" (any previous value will be replaced)
         * 
         * @param minimumWidth
         *     New value of the "minimumWidth" property.
         */
        public WebComponent.Builder<_B> withMinimumWidth(final BigInteger minimumWidth) {
            this.minimumWidth = minimumWidth;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public WebComponent.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "minimumHeight" (any previous value will be replaced)
         * 
         * @param minimumHeight
         *     New value of the "minimumHeight" property.
         */
        public WebComponent.Builder<_B> withMinimumHeight(final BigInteger minimumHeight) {
            this.minimumHeight = minimumHeight;
            return this;
        }

        /**
         * Sets the new value of "preferredWidth" (any previous value will be replaced)
         * 
         * @param preferredWidth
         *     New value of the "preferredWidth" property.
         */
        public WebComponent.Builder<_B> withPreferredWidth(final BigInteger preferredWidth) {
            this.preferredWidth = preferredWidth;
            return this;
        }

        /**
         * Sets the new value of "preferredHeight" (any previous value will be replaced)
         * 
         * @param preferredHeight
         *     New value of the "preferredHeight" property.
         */
        public WebComponent.Builder<_B> withPreferredHeight(final BigInteger preferredHeight) {
            this.preferredHeight = preferredHeight;
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        public WebComponent.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            this.nextFocusComponent = nextFocusComponent;
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        public WebComponent.Builder<_B> withNextFocusField(final String nextFocusField) {
            this.nextFocusField = nextFocusField;
            return this;
        }

        /**
         * Sets the new value of "descriptionAttr" (any previous value will be replaced)
         * 
         * @param descriptionAttr
         *     New value of the "descriptionAttr" property.
         */
        public WebComponent.Builder<_B> withDescriptionAttr(final String descriptionAttr) {
            this.descriptionAttr = descriptionAttr;
            return this;
        }

        /**
         * Sets the new value of "initialFocus" (any previous value will be replaced)
         * 
         * @param initialFocus
         *     New value of the "initialFocus" property.
         */
        public WebComponent.Builder<_B> withInitialFocus(final Boolean initialFocus) {
            this.initialFocus = initialFocus;
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        public WebComponent.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            this.alternativeTooltip = alternativeTooltip;
            return this;
        }

        /**
         * Sets the new value of "hasTextModules" (any previous value will be replaced)
         * 
         * @param hasTextModules
         *     New value of the "hasTextModules" property.
         */
        public WebComponent.Builder<_B> withHasTextModules(final Boolean hasTextModules) {
            this.hasTextModules = hasTextModules;
            return this;
        }

        @Override
        public WebComponent build() {
            return ((WebComponent) _storedValue);
        }

        public WebComponent.Builder<_B> copyOf(final WebComponent _other) {
            _other.copyTo(this);
            return this;
        }

        public WebComponent.Builder<_B> copyOf(final WebComponent.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebComponent.Selector<WebComponent.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebComponent.Select _root() {
            return new WebComponent.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private WebAdvancedProperty.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> advancedProperties = null;
        private WebBorderProperty.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> borderProperty = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> id = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> column = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> row = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> colspan = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> rowspan = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> fontSize = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> textColor = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> customBackgroundColor = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> bold = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> italic = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> underline = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> verticalAlign = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> horizontalAlign = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> minimumWidth = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> minimumHeight = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> preferredWidth = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> preferredHeight = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> nextFocusComponent = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> nextFocusField = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> descriptionAttr = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> initialFocus = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> alternativeTooltip = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> hasTextModules = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.advancedProperties!= null) {
                products.put("advancedProperties", this.advancedProperties.init());
            }
            if (this.borderProperty!= null) {
                products.put("borderProperty", this.borderProperty.init());
            }
            if (this.id!= null) {
                products.put("id", this.id.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.column!= null) {
                products.put("column", this.column.init());
            }
            if (this.row!= null) {
                products.put("row", this.row.init());
            }
            if (this.colspan!= null) {
                products.put("colspan", this.colspan.init());
            }
            if (this.rowspan!= null) {
                products.put("rowspan", this.rowspan.init());
            }
            if (this.fontSize!= null) {
                products.put("fontSize", this.fontSize.init());
            }
            if (this.textColor!= null) {
                products.put("textColor", this.textColor.init());
            }
            if (this.customBackgroundColor!= null) {
                products.put("customBackgroundColor", this.customBackgroundColor.init());
            }
            if (this.bold!= null) {
                products.put("bold", this.bold.init());
            }
            if (this.italic!= null) {
                products.put("italic", this.italic.init());
            }
            if (this.underline!= null) {
                products.put("underline", this.underline.init());
            }
            if (this.verticalAlign!= null) {
                products.put("verticalAlign", this.verticalAlign.init());
            }
            if (this.horizontalAlign!= null) {
                products.put("horizontalAlign", this.horizontalAlign.init());
            }
            if (this.minimumWidth!= null) {
                products.put("minimumWidth", this.minimumWidth.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.minimumHeight!= null) {
                products.put("minimumHeight", this.minimumHeight.init());
            }
            if (this.preferredWidth!= null) {
                products.put("preferredWidth", this.preferredWidth.init());
            }
            if (this.preferredHeight!= null) {
                products.put("preferredHeight", this.preferredHeight.init());
            }
            if (this.nextFocusComponent!= null) {
                products.put("nextFocusComponent", this.nextFocusComponent.init());
            }
            if (this.nextFocusField!= null) {
                products.put("nextFocusField", this.nextFocusField.init());
            }
            if (this.descriptionAttr!= null) {
                products.put("descriptionAttr", this.descriptionAttr.init());
            }
            if (this.initialFocus!= null) {
                products.put("initialFocus", this.initialFocus.init());
            }
            if (this.alternativeTooltip!= null) {
                products.put("alternativeTooltip", this.alternativeTooltip.init());
            }
            if (this.hasTextModules!= null) {
                products.put("hasTextModules", this.hasTextModules.init());
            }
            return products;
        }

        public WebAdvancedProperty.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> advancedProperties() {
            return ((this.advancedProperties == null)?this.advancedProperties = new WebAdvancedProperty.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "advancedProperties"):this.advancedProperties);
        }

        public WebBorderProperty.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> borderProperty() {
            return ((this.borderProperty == null)?this.borderProperty = new WebBorderProperty.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "borderProperty"):this.borderProperty);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> id() {
            return ((this.id == null)?this.id = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "id"):this.id);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> column() {
            return ((this.column == null)?this.column = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "column"):this.column);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> row() {
            return ((this.row == null)?this.row = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "row"):this.row);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> colspan() {
            return ((this.colspan == null)?this.colspan = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "colspan"):this.colspan);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> rowspan() {
            return ((this.rowspan == null)?this.rowspan = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "rowspan"):this.rowspan);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> fontSize() {
            return ((this.fontSize == null)?this.fontSize = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "fontSize"):this.fontSize);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> textColor() {
            return ((this.textColor == null)?this.textColor = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "textColor"):this.textColor);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> customBackgroundColor() {
            return ((this.customBackgroundColor == null)?this.customBackgroundColor = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "customBackgroundColor"):this.customBackgroundColor);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> bold() {
            return ((this.bold == null)?this.bold = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "bold"):this.bold);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> italic() {
            return ((this.italic == null)?this.italic = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "italic"):this.italic);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> underline() {
            return ((this.underline == null)?this.underline = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "underline"):this.underline);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> verticalAlign() {
            return ((this.verticalAlign == null)?this.verticalAlign = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "verticalAlign"):this.verticalAlign);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> horizontalAlign() {
            return ((this.horizontalAlign == null)?this.horizontalAlign = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "horizontalAlign"):this.horizontalAlign);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> minimumWidth() {
            return ((this.minimumWidth == null)?this.minimumWidth = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "minimumWidth"):this.minimumWidth);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> minimumHeight() {
            return ((this.minimumHeight == null)?this.minimumHeight = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "minimumHeight"):this.minimumHeight);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> preferredWidth() {
            return ((this.preferredWidth == null)?this.preferredWidth = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "preferredWidth"):this.preferredWidth);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> preferredHeight() {
            return ((this.preferredHeight == null)?this.preferredHeight = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "preferredHeight"):this.preferredHeight);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> nextFocusComponent() {
            return ((this.nextFocusComponent == null)?this.nextFocusComponent = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "nextFocusComponent"):this.nextFocusComponent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> nextFocusField() {
            return ((this.nextFocusField == null)?this.nextFocusField = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "nextFocusField"):this.nextFocusField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> descriptionAttr() {
            return ((this.descriptionAttr == null)?this.descriptionAttr = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "descriptionAttr"):this.descriptionAttr);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> initialFocus() {
            return ((this.initialFocus == null)?this.initialFocus = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "initialFocus"):this.initialFocus);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> alternativeTooltip() {
            return ((this.alternativeTooltip == null)?this.alternativeTooltip = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "alternativeTooltip"):this.alternativeTooltip);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>> hasTextModules() {
            return ((this.hasTextModules == null)?this.hasTextModules = new com.kscs.util.jaxb.Selector<TRoot, WebComponent.Selector<TRoot, TParent>>(this._root, this, "hasTextModules"):this.hasTextModules);
        }

    }

}
