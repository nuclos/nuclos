package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A multi-selection combobox.
 * 
 * <p>Java class for web-multiselection-combobox complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-multiselection-combobox"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{NuclosWebLayout}web-input-component"&gt;
 *       &lt;attribute name="entity" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="foreignkeyfield-to-parent" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="label-attr" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-multiselection-combobox")
public class WebMultiselectionCombobox
    extends WebInputComponent
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "entity", required = true)
    protected String entity;
    @XmlAttribute(name = "foreignkeyfield-to-parent")
    protected String foreignkeyfieldToParent;
    @XmlAttribute(name = "label-attr")
    protected String labelAttr;

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the foreignkeyfieldToParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignkeyfieldToParent() {
        return foreignkeyfieldToParent;
    }

    /**
     * Sets the value of the foreignkeyfieldToParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignkeyfieldToParent(String value) {
        this.foreignkeyfieldToParent = value;
    }

    /**
     * Gets the value of the labelAttr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabelAttr() {
        return labelAttr;
    }

    /**
     * Sets the value of the labelAttr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabelAttr(String value) {
        this.labelAttr = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            strategy.appendField(locator, this, "foreignkeyfieldToParent", buffer, theForeignkeyfieldToParent);
        }
        {
            String theLabelAttr;
            theLabelAttr = this.getLabelAttr();
            strategy.appendField(locator, this, "labelAttr", buffer, theLabelAttr);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebMultiselectionCombobox)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebMultiselectionCombobox that = ((WebMultiselectionCombobox) object);
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            String lhsForeignkeyfieldToParent;
            lhsForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            String rhsForeignkeyfieldToParent;
            rhsForeignkeyfieldToParent = that.getForeignkeyfieldToParent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "foreignkeyfieldToParent", lhsForeignkeyfieldToParent), LocatorUtils.property(thatLocator, "foreignkeyfieldToParent", rhsForeignkeyfieldToParent), lhsForeignkeyfieldToParent, rhsForeignkeyfieldToParent)) {
                return false;
            }
        }
        {
            String lhsLabelAttr;
            lhsLabelAttr = this.getLabelAttr();
            String rhsLabelAttr;
            rhsLabelAttr = that.getLabelAttr();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "labelAttr", lhsLabelAttr), LocatorUtils.property(thatLocator, "labelAttr", rhsLabelAttr), lhsLabelAttr, rhsLabelAttr)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "foreignkeyfieldToParent", theForeignkeyfieldToParent), currentHashCode, theForeignkeyfieldToParent);
        }
        {
            String theLabelAttr;
            theLabelAttr = this.getLabelAttr();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "labelAttr", theLabelAttr), currentHashCode, theLabelAttr);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebMultiselectionCombobox) {
            final WebMultiselectionCombobox copy = ((WebMultiselectionCombobox) draftCopy);
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.foreignkeyfieldToParent!= null) {
                String sourceForeignkeyfieldToParent;
                sourceForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
                String copyForeignkeyfieldToParent = ((String) strategy.copy(LocatorUtils.property(locator, "foreignkeyfieldToParent", sourceForeignkeyfieldToParent), sourceForeignkeyfieldToParent));
                copy.setForeignkeyfieldToParent(copyForeignkeyfieldToParent);
            } else {
                copy.foreignkeyfieldToParent = null;
            }
            if (this.labelAttr!= null) {
                String sourceLabelAttr;
                sourceLabelAttr = this.getLabelAttr();
                String copyLabelAttr = ((String) strategy.copy(LocatorUtils.property(locator, "labelAttr", sourceLabelAttr), sourceLabelAttr));
                copy.setLabelAttr(copyLabelAttr);
            } else {
                copy.labelAttr = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebMultiselectionCombobox();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebMultiselectionCombobox.Builder<_B> _other) {
        super.copyTo(_other);
        _other.entity = this.entity;
        _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        _other.labelAttr = this.labelAttr;
    }

    @Override
    public<_B >WebMultiselectionCombobox.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebMultiselectionCombobox.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebMultiselectionCombobox.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebMultiselectionCombobox.Builder<Void> builder() {
        return new WebMultiselectionCombobox.Builder<Void>(null, null, false);
    }

    public static<_B >WebMultiselectionCombobox.Builder<_B> copyOf(final WebComponent _other) {
        final WebMultiselectionCombobox.Builder<_B> _newBuilder = new WebMultiselectionCombobox.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebMultiselectionCombobox.Builder<_B> copyOf(final WebInputComponent _other) {
        final WebMultiselectionCombobox.Builder<_B> _newBuilder = new WebMultiselectionCombobox.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebMultiselectionCombobox.Builder<_B> copyOf(final WebMultiselectionCombobox _other) {
        final WebMultiselectionCombobox.Builder<_B> _newBuilder = new WebMultiselectionCombobox.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebMultiselectionCombobox.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
            _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        }
        final PropertyTree labelAttrPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("labelAttr"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelAttrPropertyTree!= null):((labelAttrPropertyTree == null)||(!labelAttrPropertyTree.isLeaf())))) {
            _other.labelAttr = this.labelAttr;
        }
    }

    @Override
    public<_B >WebMultiselectionCombobox.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebMultiselectionCombobox.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebMultiselectionCombobox.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebMultiselectionCombobox.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebMultiselectionCombobox.Builder<_B> _newBuilder = new WebMultiselectionCombobox.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebMultiselectionCombobox.Builder<_B> copyOf(final WebInputComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebMultiselectionCombobox.Builder<_B> _newBuilder = new WebMultiselectionCombobox.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebMultiselectionCombobox.Builder<_B> copyOf(final WebMultiselectionCombobox _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebMultiselectionCombobox.Builder<_B> _newBuilder = new WebMultiselectionCombobox.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebMultiselectionCombobox.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebMultiselectionCombobox.Builder<Void> copyExcept(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebMultiselectionCombobox.Builder<Void> copyExcept(final WebMultiselectionCombobox _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebMultiselectionCombobox.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebMultiselectionCombobox.Builder<Void> copyOnly(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebMultiselectionCombobox.Builder<Void> copyOnly(final WebMultiselectionCombobox _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebInputComponent.Builder<_B>
        implements Buildable
    {

        private String entity;
        private String foreignkeyfieldToParent;
        private String labelAttr;

        public Builder(final _B _parentBuilder, final WebMultiselectionCombobox _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.entity = _other.entity;
                this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                this.labelAttr = _other.labelAttr;
            }
        }

        public Builder(final _B _parentBuilder, final WebMultiselectionCombobox _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                    this.entity = _other.entity;
                }
                final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
                    this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                }
                final PropertyTree labelAttrPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("labelAttr"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelAttrPropertyTree!= null):((labelAttrPropertyTree == null)||(!labelAttrPropertyTree.isLeaf())))) {
                    this.labelAttr = _other.labelAttr;
                }
            }
        }

        protected<_P extends WebMultiselectionCombobox >_P init(final _P _product) {
            _product.entity = this.entity;
            _product.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
            _product.labelAttr = this.labelAttr;
            return super.init(_product);
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public WebMultiselectionCombobox.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "foreignkeyfieldToParent" (any previous value will be replaced)
         * 
         * @param foreignkeyfieldToParent
         *     New value of the "foreignkeyfieldToParent" property.
         */
        public WebMultiselectionCombobox.Builder<_B> withForeignkeyfieldToParent(final String foreignkeyfieldToParent) {
            this.foreignkeyfieldToParent = foreignkeyfieldToParent;
            return this;
        }

        /**
         * Sets the new value of "labelAttr" (any previous value will be replaced)
         * 
         * @param labelAttr
         *     New value of the "labelAttr" property.
         */
        public WebMultiselectionCombobox.Builder<_B> withLabelAttr(final String labelAttr) {
            this.labelAttr = labelAttr;
            return this;
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withValuelistProvider(final WebValuelistProvider valuelistProvider) {
            super.withValuelistProvider(valuelistProvider);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Builder<? extends WebMultiselectionCombobox.Builder<_B>> withValuelistProvider() {
            return ((WebValuelistProvider.Builder<? extends WebMultiselectionCombobox.Builder<_B>> ) super.withValuelistProvider());
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withEnabled(final Boolean enabled) {
            super.withEnabled(enabled);
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withEditable(final Boolean editable) {
            super.withEditable(editable);
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withInsertable(final Boolean insertable) {
            super.withInsertable(insertable);
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withColumns(final String columns) {
            super.withColumns(columns);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebMultiselectionCombobox.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebMultiselectionCombobox.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "borderProperty" (any previous value will be replaced)
         * 
         * @param borderProperty
         *     New value of the "borderProperty" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withBorderProperty(final WebBorderProperty borderProperty) {
            super.withBorderProperty(borderProperty);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "borderProperty" property.
         * Use {@link org.nuclos.schema.layout.web.WebBorderProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "borderProperty" property.
         *     Use {@link org.nuclos.schema.layout.web.WebBorderProperty.Builder#end()} to return to the current builder.
         */
        public WebBorderProperty.Builder<? extends WebMultiselectionCombobox.Builder<_B>> withBorderProperty() {
            return ((WebBorderProperty.Builder<? extends WebMultiselectionCombobox.Builder<_B>> ) super.withBorderProperty());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "customBackgroundColor" (any previous value will be replaced)
         * 
         * @param customBackgroundColor
         *     New value of the "customBackgroundColor" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withCustomBackgroundColor(final String customBackgroundColor) {
            super.withCustomBackgroundColor(customBackgroundColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "verticalAlign" (any previous value will be replaced)
         * 
         * @param verticalAlign
         *     New value of the "verticalAlign" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withVerticalAlign(final String verticalAlign) {
            super.withVerticalAlign(verticalAlign);
            return this;
        }

        /**
         * Sets the new value of "horizontalAlign" (any previous value will be replaced)
         * 
         * @param horizontalAlign
         *     New value of the "horizontalAlign" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withHorizontalAlign(final String horizontalAlign) {
            super.withHorizontalAlign(horizontalAlign);
            return this;
        }

        /**
         * Sets the new value of "minimumWidth" (any previous value will be replaced)
         * 
         * @param minimumWidth
         *     New value of the "minimumWidth" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withMinimumWidth(final BigInteger minimumWidth) {
            super.withMinimumWidth(minimumWidth);
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withLabel(final String label) {
            super.withLabel(label);
            return this;
        }

        /**
         * Sets the new value of "minimumHeight" (any previous value will be replaced)
         * 
         * @param minimumHeight
         *     New value of the "minimumHeight" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withMinimumHeight(final BigInteger minimumHeight) {
            super.withMinimumHeight(minimumHeight);
            return this;
        }

        /**
         * Sets the new value of "preferredWidth" (any previous value will be replaced)
         * 
         * @param preferredWidth
         *     New value of the "preferredWidth" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withPreferredWidth(final BigInteger preferredWidth) {
            super.withPreferredWidth(preferredWidth);
            return this;
        }

        /**
         * Sets the new value of "preferredHeight" (any previous value will be replaced)
         * 
         * @param preferredHeight
         *     New value of the "preferredHeight" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withPreferredHeight(final BigInteger preferredHeight) {
            super.withPreferredHeight(preferredHeight);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "descriptionAttr" (any previous value will be replaced)
         * 
         * @param descriptionAttr
         *     New value of the "descriptionAttr" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withDescriptionAttr(final String descriptionAttr) {
            super.withDescriptionAttr(descriptionAttr);
            return this;
        }

        /**
         * Sets the new value of "initialFocus" (any previous value will be replaced)
         * 
         * @param initialFocus
         *     New value of the "initialFocus" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withInitialFocus(final Boolean initialFocus) {
            super.withInitialFocus(initialFocus);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        /**
         * Sets the new value of "hasTextModules" (any previous value will be replaced)
         * 
         * @param hasTextModules
         *     New value of the "hasTextModules" property.
         */
        @Override
        public WebMultiselectionCombobox.Builder<_B> withHasTextModules(final Boolean hasTextModules) {
            super.withHasTextModules(hasTextModules);
            return this;
        }

        @Override
        public WebMultiselectionCombobox build() {
            if (_storedValue == null) {
                return this.init(new WebMultiselectionCombobox());
            } else {
                return ((WebMultiselectionCombobox) _storedValue);
            }
        }

        public WebMultiselectionCombobox.Builder<_B> copyOf(final WebMultiselectionCombobox _other) {
            _other.copyTo(this);
            return this;
        }

        public WebMultiselectionCombobox.Builder<_B> copyOf(final WebMultiselectionCombobox.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebMultiselectionCombobox.Selector<WebMultiselectionCombobox.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebMultiselectionCombobox.Select _root() {
            return new WebMultiselectionCombobox.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebInputComponent.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, WebMultiselectionCombobox.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMultiselectionCombobox.Selector<TRoot, TParent>> foreignkeyfieldToParent = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebMultiselectionCombobox.Selector<TRoot, TParent>> labelAttr = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.foreignkeyfieldToParent!= null) {
                products.put("foreignkeyfieldToParent", this.foreignkeyfieldToParent.init());
            }
            if (this.labelAttr!= null) {
                products.put("labelAttr", this.labelAttr.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMultiselectionCombobox.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, WebMultiselectionCombobox.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMultiselectionCombobox.Selector<TRoot, TParent>> foreignkeyfieldToParent() {
            return ((this.foreignkeyfieldToParent == null)?this.foreignkeyfieldToParent = new com.kscs.util.jaxb.Selector<TRoot, WebMultiselectionCombobox.Selector<TRoot, TParent>>(this._root, this, "foreignkeyfieldToParent"):this.foreignkeyfieldToParent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebMultiselectionCombobox.Selector<TRoot, TParent>> labelAttr() {
            return ((this.labelAttr == null)?this.labelAttr = new com.kscs.util.jaxb.Selector<TRoot, WebMultiselectionCombobox.Selector<TRoot, TParent>>(this._root, this, "labelAttr"):this.labelAttr);
        }

    }

}
