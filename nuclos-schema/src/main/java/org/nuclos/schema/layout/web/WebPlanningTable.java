package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A container with the PlanningTable Component inside.
 * 
 * <p>Java class for web-planning-table complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-planning-table"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{NuclosWebLayout}web-input-component"&gt;
 *       &lt;attribute name="planning-table" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="foreignkeyfield-to-parent" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="date-from" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="date-until" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="field-date-from" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="field-date-until" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-planning-table")
public class WebPlanningTable
    extends WebInputComponent
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "planning-table", required = true)
    protected String planningTable;
    @XmlAttribute(name = "foreignkeyfield-to-parent")
    protected String foreignkeyfieldToParent;
    @XmlAttribute(name = "date-from")
    protected String dateFrom;
    @XmlAttribute(name = "date-until")
    protected String dateUntil;
    @XmlAttribute(name = "field-date-from")
    protected String fieldDateFrom;
    @XmlAttribute(name = "field-date-until")
    protected String fieldDateUntil;

    /**
     * Gets the value of the planningTable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanningTable() {
        return planningTable;
    }

    /**
     * Sets the value of the planningTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanningTable(String value) {
        this.planningTable = value;
    }

    /**
     * Gets the value of the foreignkeyfieldToParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignkeyfieldToParent() {
        return foreignkeyfieldToParent;
    }

    /**
     * Sets the value of the foreignkeyfieldToParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignkeyfieldToParent(String value) {
        this.foreignkeyfieldToParent = value;
    }

    /**
     * Gets the value of the dateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateFrom() {
        return dateFrom;
    }

    /**
     * Sets the value of the dateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateFrom(String value) {
        this.dateFrom = value;
    }

    /**
     * Gets the value of the dateUntil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateUntil() {
        return dateUntil;
    }

    /**
     * Sets the value of the dateUntil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateUntil(String value) {
        this.dateUntil = value;
    }

    /**
     * Gets the value of the fieldDateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldDateFrom() {
        return fieldDateFrom;
    }

    /**
     * Sets the value of the fieldDateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldDateFrom(String value) {
        this.fieldDateFrom = value;
    }

    /**
     * Gets the value of the fieldDateUntil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldDateUntil() {
        return fieldDateUntil;
    }

    /**
     * Sets the value of the fieldDateUntil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldDateUntil(String value) {
        this.fieldDateUntil = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String thePlanningTable;
            thePlanningTable = this.getPlanningTable();
            strategy.appendField(locator, this, "planningTable", buffer, thePlanningTable);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            strategy.appendField(locator, this, "foreignkeyfieldToParent", buffer, theForeignkeyfieldToParent);
        }
        {
            String theDateFrom;
            theDateFrom = this.getDateFrom();
            strategy.appendField(locator, this, "dateFrom", buffer, theDateFrom);
        }
        {
            String theDateUntil;
            theDateUntil = this.getDateUntil();
            strategy.appendField(locator, this, "dateUntil", buffer, theDateUntil);
        }
        {
            String theFieldDateFrom;
            theFieldDateFrom = this.getFieldDateFrom();
            strategy.appendField(locator, this, "fieldDateFrom", buffer, theFieldDateFrom);
        }
        {
            String theFieldDateUntil;
            theFieldDateUntil = this.getFieldDateUntil();
            strategy.appendField(locator, this, "fieldDateUntil", buffer, theFieldDateUntil);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebPlanningTable)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebPlanningTable that = ((WebPlanningTable) object);
        {
            String lhsPlanningTable;
            lhsPlanningTable = this.getPlanningTable();
            String rhsPlanningTable;
            rhsPlanningTable = that.getPlanningTable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "planningTable", lhsPlanningTable), LocatorUtils.property(thatLocator, "planningTable", rhsPlanningTable), lhsPlanningTable, rhsPlanningTable)) {
                return false;
            }
        }
        {
            String lhsForeignkeyfieldToParent;
            lhsForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            String rhsForeignkeyfieldToParent;
            rhsForeignkeyfieldToParent = that.getForeignkeyfieldToParent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "foreignkeyfieldToParent", lhsForeignkeyfieldToParent), LocatorUtils.property(thatLocator, "foreignkeyfieldToParent", rhsForeignkeyfieldToParent), lhsForeignkeyfieldToParent, rhsForeignkeyfieldToParent)) {
                return false;
            }
        }
        {
            String lhsDateFrom;
            lhsDateFrom = this.getDateFrom();
            String rhsDateFrom;
            rhsDateFrom = that.getDateFrom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateFrom", lhsDateFrom), LocatorUtils.property(thatLocator, "dateFrom", rhsDateFrom), lhsDateFrom, rhsDateFrom)) {
                return false;
            }
        }
        {
            String lhsDateUntil;
            lhsDateUntil = this.getDateUntil();
            String rhsDateUntil;
            rhsDateUntil = that.getDateUntil();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateUntil", lhsDateUntil), LocatorUtils.property(thatLocator, "dateUntil", rhsDateUntil), lhsDateUntil, rhsDateUntil)) {
                return false;
            }
        }
        {
            String lhsFieldDateFrom;
            lhsFieldDateFrom = this.getFieldDateFrom();
            String rhsFieldDateFrom;
            rhsFieldDateFrom = that.getFieldDateFrom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fieldDateFrom", lhsFieldDateFrom), LocatorUtils.property(thatLocator, "fieldDateFrom", rhsFieldDateFrom), lhsFieldDateFrom, rhsFieldDateFrom)) {
                return false;
            }
        }
        {
            String lhsFieldDateUntil;
            lhsFieldDateUntil = this.getFieldDateUntil();
            String rhsFieldDateUntil;
            rhsFieldDateUntil = that.getFieldDateUntil();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fieldDateUntil", lhsFieldDateUntil), LocatorUtils.property(thatLocator, "fieldDateUntil", rhsFieldDateUntil), lhsFieldDateUntil, rhsFieldDateUntil)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String thePlanningTable;
            thePlanningTable = this.getPlanningTable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "planningTable", thePlanningTable), currentHashCode, thePlanningTable);
        }
        {
            String theForeignkeyfieldToParent;
            theForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "foreignkeyfieldToParent", theForeignkeyfieldToParent), currentHashCode, theForeignkeyfieldToParent);
        }
        {
            String theDateFrom;
            theDateFrom = this.getDateFrom();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dateFrom", theDateFrom), currentHashCode, theDateFrom);
        }
        {
            String theDateUntil;
            theDateUntil = this.getDateUntil();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dateUntil", theDateUntil), currentHashCode, theDateUntil);
        }
        {
            String theFieldDateFrom;
            theFieldDateFrom = this.getFieldDateFrom();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fieldDateFrom", theFieldDateFrom), currentHashCode, theFieldDateFrom);
        }
        {
            String theFieldDateUntil;
            theFieldDateUntil = this.getFieldDateUntil();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fieldDateUntil", theFieldDateUntil), currentHashCode, theFieldDateUntil);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebPlanningTable) {
            final WebPlanningTable copy = ((WebPlanningTable) draftCopy);
            if (this.planningTable!= null) {
                String sourcePlanningTable;
                sourcePlanningTable = this.getPlanningTable();
                String copyPlanningTable = ((String) strategy.copy(LocatorUtils.property(locator, "planningTable", sourcePlanningTable), sourcePlanningTable));
                copy.setPlanningTable(copyPlanningTable);
            } else {
                copy.planningTable = null;
            }
            if (this.foreignkeyfieldToParent!= null) {
                String sourceForeignkeyfieldToParent;
                sourceForeignkeyfieldToParent = this.getForeignkeyfieldToParent();
                String copyForeignkeyfieldToParent = ((String) strategy.copy(LocatorUtils.property(locator, "foreignkeyfieldToParent", sourceForeignkeyfieldToParent), sourceForeignkeyfieldToParent));
                copy.setForeignkeyfieldToParent(copyForeignkeyfieldToParent);
            } else {
                copy.foreignkeyfieldToParent = null;
            }
            if (this.dateFrom!= null) {
                String sourceDateFrom;
                sourceDateFrom = this.getDateFrom();
                String copyDateFrom = ((String) strategy.copy(LocatorUtils.property(locator, "dateFrom", sourceDateFrom), sourceDateFrom));
                copy.setDateFrom(copyDateFrom);
            } else {
                copy.dateFrom = null;
            }
            if (this.dateUntil!= null) {
                String sourceDateUntil;
                sourceDateUntil = this.getDateUntil();
                String copyDateUntil = ((String) strategy.copy(LocatorUtils.property(locator, "dateUntil", sourceDateUntil), sourceDateUntil));
                copy.setDateUntil(copyDateUntil);
            } else {
                copy.dateUntil = null;
            }
            if (this.fieldDateFrom!= null) {
                String sourceFieldDateFrom;
                sourceFieldDateFrom = this.getFieldDateFrom();
                String copyFieldDateFrom = ((String) strategy.copy(LocatorUtils.property(locator, "fieldDateFrom", sourceFieldDateFrom), sourceFieldDateFrom));
                copy.setFieldDateFrom(copyFieldDateFrom);
            } else {
                copy.fieldDateFrom = null;
            }
            if (this.fieldDateUntil!= null) {
                String sourceFieldDateUntil;
                sourceFieldDateUntil = this.getFieldDateUntil();
                String copyFieldDateUntil = ((String) strategy.copy(LocatorUtils.property(locator, "fieldDateUntil", sourceFieldDateUntil), sourceFieldDateUntil));
                copy.setFieldDateUntil(copyFieldDateUntil);
            } else {
                copy.fieldDateUntil = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebPlanningTable();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebPlanningTable.Builder<_B> _other) {
        super.copyTo(_other);
        _other.planningTable = this.planningTable;
        _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        _other.dateFrom = this.dateFrom;
        _other.dateUntil = this.dateUntil;
        _other.fieldDateFrom = this.fieldDateFrom;
        _other.fieldDateUntil = this.fieldDateUntil;
    }

    @Override
    public<_B >WebPlanningTable.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebPlanningTable.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebPlanningTable.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebPlanningTable.Builder<Void> builder() {
        return new WebPlanningTable.Builder<Void>(null, null, false);
    }

    public static<_B >WebPlanningTable.Builder<_B> copyOf(final WebComponent _other) {
        final WebPlanningTable.Builder<_B> _newBuilder = new WebPlanningTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebPlanningTable.Builder<_B> copyOf(final WebInputComponent _other) {
        final WebPlanningTable.Builder<_B> _newBuilder = new WebPlanningTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebPlanningTable.Builder<_B> copyOf(final WebPlanningTable _other) {
        final WebPlanningTable.Builder<_B> _newBuilder = new WebPlanningTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebPlanningTable.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree planningTablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("planningTable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(planningTablePropertyTree!= null):((planningTablePropertyTree == null)||(!planningTablePropertyTree.isLeaf())))) {
            _other.planningTable = this.planningTable;
        }
        final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
            _other.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
        }
        final PropertyTree dateFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateFrom"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateFromPropertyTree!= null):((dateFromPropertyTree == null)||(!dateFromPropertyTree.isLeaf())))) {
            _other.dateFrom = this.dateFrom;
        }
        final PropertyTree dateUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateUntil"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateUntilPropertyTree!= null):((dateUntilPropertyTree == null)||(!dateUntilPropertyTree.isLeaf())))) {
            _other.dateUntil = this.dateUntil;
        }
        final PropertyTree fieldDateFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldDateFrom"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldDateFromPropertyTree!= null):((fieldDateFromPropertyTree == null)||(!fieldDateFromPropertyTree.isLeaf())))) {
            _other.fieldDateFrom = this.fieldDateFrom;
        }
        final PropertyTree fieldDateUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldDateUntil"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldDateUntilPropertyTree!= null):((fieldDateUntilPropertyTree == null)||(!fieldDateUntilPropertyTree.isLeaf())))) {
            _other.fieldDateUntil = this.fieldDateUntil;
        }
    }

    @Override
    public<_B >WebPlanningTable.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebPlanningTable.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebPlanningTable.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebPlanningTable.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebPlanningTable.Builder<_B> _newBuilder = new WebPlanningTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebPlanningTable.Builder<_B> copyOf(final WebInputComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebPlanningTable.Builder<_B> _newBuilder = new WebPlanningTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebPlanningTable.Builder<_B> copyOf(final WebPlanningTable _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebPlanningTable.Builder<_B> _newBuilder = new WebPlanningTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebPlanningTable.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebPlanningTable.Builder<Void> copyExcept(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebPlanningTable.Builder<Void> copyExcept(final WebPlanningTable _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebPlanningTable.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebPlanningTable.Builder<Void> copyOnly(final WebInputComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebPlanningTable.Builder<Void> copyOnly(final WebPlanningTable _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebInputComponent.Builder<_B>
        implements Buildable
    {

        private String planningTable;
        private String foreignkeyfieldToParent;
        private String dateFrom;
        private String dateUntil;
        private String fieldDateFrom;
        private String fieldDateUntil;

        public Builder(final _B _parentBuilder, final WebPlanningTable _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.planningTable = _other.planningTable;
                this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                this.dateFrom = _other.dateFrom;
                this.dateUntil = _other.dateUntil;
                this.fieldDateFrom = _other.fieldDateFrom;
                this.fieldDateUntil = _other.fieldDateUntil;
            }
        }

        public Builder(final _B _parentBuilder, final WebPlanningTable _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree planningTablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("planningTable"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(planningTablePropertyTree!= null):((planningTablePropertyTree == null)||(!planningTablePropertyTree.isLeaf())))) {
                    this.planningTable = _other.planningTable;
                }
                final PropertyTree foreignkeyfieldToParentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("foreignkeyfieldToParent"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(foreignkeyfieldToParentPropertyTree!= null):((foreignkeyfieldToParentPropertyTree == null)||(!foreignkeyfieldToParentPropertyTree.isLeaf())))) {
                    this.foreignkeyfieldToParent = _other.foreignkeyfieldToParent;
                }
                final PropertyTree dateFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateFrom"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateFromPropertyTree!= null):((dateFromPropertyTree == null)||(!dateFromPropertyTree.isLeaf())))) {
                    this.dateFrom = _other.dateFrom;
                }
                final PropertyTree dateUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateUntil"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateUntilPropertyTree!= null):((dateUntilPropertyTree == null)||(!dateUntilPropertyTree.isLeaf())))) {
                    this.dateUntil = _other.dateUntil;
                }
                final PropertyTree fieldDateFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldDateFrom"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldDateFromPropertyTree!= null):((fieldDateFromPropertyTree == null)||(!fieldDateFromPropertyTree.isLeaf())))) {
                    this.fieldDateFrom = _other.fieldDateFrom;
                }
                final PropertyTree fieldDateUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldDateUntil"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldDateUntilPropertyTree!= null):((fieldDateUntilPropertyTree == null)||(!fieldDateUntilPropertyTree.isLeaf())))) {
                    this.fieldDateUntil = _other.fieldDateUntil;
                }
            }
        }

        protected<_P extends WebPlanningTable >_P init(final _P _product) {
            _product.planningTable = this.planningTable;
            _product.foreignkeyfieldToParent = this.foreignkeyfieldToParent;
            _product.dateFrom = this.dateFrom;
            _product.dateUntil = this.dateUntil;
            _product.fieldDateFrom = this.fieldDateFrom;
            _product.fieldDateUntil = this.fieldDateUntil;
            return super.init(_product);
        }

        /**
         * Sets the new value of "planningTable" (any previous value will be replaced)
         * 
         * @param planningTable
         *     New value of the "planningTable" property.
         */
        public WebPlanningTable.Builder<_B> withPlanningTable(final String planningTable) {
            this.planningTable = planningTable;
            return this;
        }

        /**
         * Sets the new value of "foreignkeyfieldToParent" (any previous value will be replaced)
         * 
         * @param foreignkeyfieldToParent
         *     New value of the "foreignkeyfieldToParent" property.
         */
        public WebPlanningTable.Builder<_B> withForeignkeyfieldToParent(final String foreignkeyfieldToParent) {
            this.foreignkeyfieldToParent = foreignkeyfieldToParent;
            return this;
        }

        /**
         * Sets the new value of "dateFrom" (any previous value will be replaced)
         * 
         * @param dateFrom
         *     New value of the "dateFrom" property.
         */
        public WebPlanningTable.Builder<_B> withDateFrom(final String dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        /**
         * Sets the new value of "dateUntil" (any previous value will be replaced)
         * 
         * @param dateUntil
         *     New value of the "dateUntil" property.
         */
        public WebPlanningTable.Builder<_B> withDateUntil(final String dateUntil) {
            this.dateUntil = dateUntil;
            return this;
        }

        /**
         * Sets the new value of "fieldDateFrom" (any previous value will be replaced)
         * 
         * @param fieldDateFrom
         *     New value of the "fieldDateFrom" property.
         */
        public WebPlanningTable.Builder<_B> withFieldDateFrom(final String fieldDateFrom) {
            this.fieldDateFrom = fieldDateFrom;
            return this;
        }

        /**
         * Sets the new value of "fieldDateUntil" (any previous value will be replaced)
         * 
         * @param fieldDateUntil
         *     New value of the "fieldDateUntil" property.
         */
        public WebPlanningTable.Builder<_B> withFieldDateUntil(final String fieldDateUntil) {
            this.fieldDateUntil = fieldDateUntil;
            return this;
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withValuelistProvider(final WebValuelistProvider valuelistProvider) {
            super.withValuelistProvider(valuelistProvider);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "valuelistProvider" property.
         * Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "valuelistProvider" property.
         *     Use {@link org.nuclos.schema.layout.web.WebValuelistProvider.Builder#end()} to return to the current builder.
         */
        public WebValuelistProvider.Builder<? extends WebPlanningTable.Builder<_B>> withValuelistProvider() {
            return ((WebValuelistProvider.Builder<? extends WebPlanningTable.Builder<_B>> ) super.withValuelistProvider());
        }

        /**
         * Sets the new value of "enabled" (any previous value will be replaced)
         * 
         * @param enabled
         *     New value of the "enabled" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withEnabled(final Boolean enabled) {
            super.withEnabled(enabled);
            return this;
        }

        /**
         * Sets the new value of "editable" (any previous value will be replaced)
         * 
         * @param editable
         *     New value of the "editable" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withEditable(final Boolean editable) {
            super.withEditable(editable);
            return this;
        }

        /**
         * Sets the new value of "insertable" (any previous value will be replaced)
         * 
         * @param insertable
         *     New value of the "insertable" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withInsertable(final Boolean insertable) {
            super.withInsertable(insertable);
            return this;
        }

        /**
         * Sets the new value of "columns" (any previous value will be replaced)
         * 
         * @param columns
         *     New value of the "columns" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withColumns(final String columns) {
            super.withColumns(columns);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebPlanningTable.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebPlanningTable.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebPlanningTable.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebPlanningTable.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "borderProperty" (any previous value will be replaced)
         * 
         * @param borderProperty
         *     New value of the "borderProperty" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withBorderProperty(final WebBorderProperty borderProperty) {
            super.withBorderProperty(borderProperty);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "borderProperty" property.
         * Use {@link org.nuclos.schema.layout.web.WebBorderProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "borderProperty" property.
         *     Use {@link org.nuclos.schema.layout.web.WebBorderProperty.Builder#end()} to return to the current builder.
         */
        public WebBorderProperty.Builder<? extends WebPlanningTable.Builder<_B>> withBorderProperty() {
            return ((WebBorderProperty.Builder<? extends WebPlanningTable.Builder<_B>> ) super.withBorderProperty());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "customBackgroundColor" (any previous value will be replaced)
         * 
         * @param customBackgroundColor
         *     New value of the "customBackgroundColor" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withCustomBackgroundColor(final String customBackgroundColor) {
            super.withCustomBackgroundColor(customBackgroundColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "verticalAlign" (any previous value will be replaced)
         * 
         * @param verticalAlign
         *     New value of the "verticalAlign" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withVerticalAlign(final String verticalAlign) {
            super.withVerticalAlign(verticalAlign);
            return this;
        }

        /**
         * Sets the new value of "horizontalAlign" (any previous value will be replaced)
         * 
         * @param horizontalAlign
         *     New value of the "horizontalAlign" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withHorizontalAlign(final String horizontalAlign) {
            super.withHorizontalAlign(horizontalAlign);
            return this;
        }

        /**
         * Sets the new value of "minimumWidth" (any previous value will be replaced)
         * 
         * @param minimumWidth
         *     New value of the "minimumWidth" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withMinimumWidth(final BigInteger minimumWidth) {
            super.withMinimumWidth(minimumWidth);
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withLabel(final String label) {
            super.withLabel(label);
            return this;
        }

        /**
         * Sets the new value of "minimumHeight" (any previous value will be replaced)
         * 
         * @param minimumHeight
         *     New value of the "minimumHeight" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withMinimumHeight(final BigInteger minimumHeight) {
            super.withMinimumHeight(minimumHeight);
            return this;
        }

        /**
         * Sets the new value of "preferredWidth" (any previous value will be replaced)
         * 
         * @param preferredWidth
         *     New value of the "preferredWidth" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withPreferredWidth(final BigInteger preferredWidth) {
            super.withPreferredWidth(preferredWidth);
            return this;
        }

        /**
         * Sets the new value of "preferredHeight" (any previous value will be replaced)
         * 
         * @param preferredHeight
         *     New value of the "preferredHeight" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withPreferredHeight(final BigInteger preferredHeight) {
            super.withPreferredHeight(preferredHeight);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "descriptionAttr" (any previous value will be replaced)
         * 
         * @param descriptionAttr
         *     New value of the "descriptionAttr" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withDescriptionAttr(final String descriptionAttr) {
            super.withDescriptionAttr(descriptionAttr);
            return this;
        }

        /**
         * Sets the new value of "initialFocus" (any previous value will be replaced)
         * 
         * @param initialFocus
         *     New value of the "initialFocus" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withInitialFocus(final Boolean initialFocus) {
            super.withInitialFocus(initialFocus);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        /**
         * Sets the new value of "hasTextModules" (any previous value will be replaced)
         * 
         * @param hasTextModules
         *     New value of the "hasTextModules" property.
         */
        @Override
        public WebPlanningTable.Builder<_B> withHasTextModules(final Boolean hasTextModules) {
            super.withHasTextModules(hasTextModules);
            return this;
        }

        @Override
        public WebPlanningTable build() {
            if (_storedValue == null) {
                return this.init(new WebPlanningTable());
            } else {
                return ((WebPlanningTable) _storedValue);
            }
        }

        public WebPlanningTable.Builder<_B> copyOf(final WebPlanningTable _other) {
            _other.copyTo(this);
            return this;
        }

        public WebPlanningTable.Builder<_B> copyOf(final WebPlanningTable.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebPlanningTable.Selector<WebPlanningTable.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebPlanningTable.Select _root() {
            return new WebPlanningTable.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebInputComponent.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> planningTable = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> foreignkeyfieldToParent = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> dateFrom = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> dateUntil = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> fieldDateFrom = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> fieldDateUntil = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.planningTable!= null) {
                products.put("planningTable", this.planningTable.init());
            }
            if (this.foreignkeyfieldToParent!= null) {
                products.put("foreignkeyfieldToParent", this.foreignkeyfieldToParent.init());
            }
            if (this.dateFrom!= null) {
                products.put("dateFrom", this.dateFrom.init());
            }
            if (this.dateUntil!= null) {
                products.put("dateUntil", this.dateUntil.init());
            }
            if (this.fieldDateFrom!= null) {
                products.put("fieldDateFrom", this.fieldDateFrom.init());
            }
            if (this.fieldDateUntil!= null) {
                products.put("fieldDateUntil", this.fieldDateUntil.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> planningTable() {
            return ((this.planningTable == null)?this.planningTable = new com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>>(this._root, this, "planningTable"):this.planningTable);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> foreignkeyfieldToParent() {
            return ((this.foreignkeyfieldToParent == null)?this.foreignkeyfieldToParent = new com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>>(this._root, this, "foreignkeyfieldToParent"):this.foreignkeyfieldToParent);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> dateFrom() {
            return ((this.dateFrom == null)?this.dateFrom = new com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>>(this._root, this, "dateFrom"):this.dateFrom);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> dateUntil() {
            return ((this.dateUntil == null)?this.dateUntil = new com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>>(this._root, this, "dateUntil"):this.dateUntil);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> fieldDateFrom() {
            return ((this.fieldDateFrom == null)?this.fieldDateFrom = new com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>>(this._root, this, "fieldDateFrom"):this.fieldDateFrom);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>> fieldDateUntil() {
            return ((this.fieldDateUntil == null)?this.fieldDateUntil = new com.kscs.util.jaxb.Selector<TRoot, WebPlanningTable.Selector<TRoot, TParent>>(this._root, this, "fieldDateUntil"):this.fieldDateUntil);
        }

    }

}
