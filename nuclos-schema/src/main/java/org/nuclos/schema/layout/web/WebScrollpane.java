package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A container element which can hold more web-elements and is scrollable.
 * 
 * <p>Java class for web-scrollpane complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-scrollpane"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{NuclosWebLayout}web-container"&gt;
 *       &lt;attribute name="horizontalscrollbar" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="verticalscrollbar" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-scrollpane")
public class WebScrollpane
    extends WebContainer
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "horizontalscrollbar")
    protected String horizontalscrollbar;
    @XmlAttribute(name = "verticalscrollbar")
    protected String verticalscrollbar;

    /**
     * Gets the value of the horizontalscrollbar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHorizontalscrollbar() {
        return horizontalscrollbar;
    }

    /**
     * Sets the value of the horizontalscrollbar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHorizontalscrollbar(String value) {
        this.horizontalscrollbar = value;
    }

    /**
     * Gets the value of the verticalscrollbar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVerticalscrollbar() {
        return verticalscrollbar;
    }

    /**
     * Sets the value of the verticalscrollbar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVerticalscrollbar(String value) {
        this.verticalscrollbar = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theHorizontalscrollbar;
            theHorizontalscrollbar = this.getHorizontalscrollbar();
            strategy.appendField(locator, this, "horizontalscrollbar", buffer, theHorizontalscrollbar);
        }
        {
            String theVerticalscrollbar;
            theVerticalscrollbar = this.getVerticalscrollbar();
            strategy.appendField(locator, this, "verticalscrollbar", buffer, theVerticalscrollbar);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebScrollpane)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebScrollpane that = ((WebScrollpane) object);
        {
            String lhsHorizontalscrollbar;
            lhsHorizontalscrollbar = this.getHorizontalscrollbar();
            String rhsHorizontalscrollbar;
            rhsHorizontalscrollbar = that.getHorizontalscrollbar();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "horizontalscrollbar", lhsHorizontalscrollbar), LocatorUtils.property(thatLocator, "horizontalscrollbar", rhsHorizontalscrollbar), lhsHorizontalscrollbar, rhsHorizontalscrollbar)) {
                return false;
            }
        }
        {
            String lhsVerticalscrollbar;
            lhsVerticalscrollbar = this.getVerticalscrollbar();
            String rhsVerticalscrollbar;
            rhsVerticalscrollbar = that.getVerticalscrollbar();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "verticalscrollbar", lhsVerticalscrollbar), LocatorUtils.property(thatLocator, "verticalscrollbar", rhsVerticalscrollbar), lhsVerticalscrollbar, rhsVerticalscrollbar)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theHorizontalscrollbar;
            theHorizontalscrollbar = this.getHorizontalscrollbar();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "horizontalscrollbar", theHorizontalscrollbar), currentHashCode, theHorizontalscrollbar);
        }
        {
            String theVerticalscrollbar;
            theVerticalscrollbar = this.getVerticalscrollbar();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "verticalscrollbar", theVerticalscrollbar), currentHashCode, theVerticalscrollbar);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebScrollpane) {
            final WebScrollpane copy = ((WebScrollpane) draftCopy);
            if (this.horizontalscrollbar!= null) {
                String sourceHorizontalscrollbar;
                sourceHorizontalscrollbar = this.getHorizontalscrollbar();
                String copyHorizontalscrollbar = ((String) strategy.copy(LocatorUtils.property(locator, "horizontalscrollbar", sourceHorizontalscrollbar), sourceHorizontalscrollbar));
                copy.setHorizontalscrollbar(copyHorizontalscrollbar);
            } else {
                copy.horizontalscrollbar = null;
            }
            if (this.verticalscrollbar!= null) {
                String sourceVerticalscrollbar;
                sourceVerticalscrollbar = this.getVerticalscrollbar();
                String copyVerticalscrollbar = ((String) strategy.copy(LocatorUtils.property(locator, "verticalscrollbar", sourceVerticalscrollbar), sourceVerticalscrollbar));
                copy.setVerticalscrollbar(copyVerticalscrollbar);
            } else {
                copy.verticalscrollbar = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebScrollpane();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebScrollpane.Builder<_B> _other) {
        super.copyTo(_other);
        _other.horizontalscrollbar = this.horizontalscrollbar;
        _other.verticalscrollbar = this.verticalscrollbar;
    }

    @Override
    public<_B >WebScrollpane.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebScrollpane.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebScrollpane.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebScrollpane.Builder<Void> builder() {
        return new WebScrollpane.Builder<Void>(null, null, false);
    }

    public static<_B >WebScrollpane.Builder<_B> copyOf(final WebComponent _other) {
        final WebScrollpane.Builder<_B> _newBuilder = new WebScrollpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebScrollpane.Builder<_B> copyOf(final WebContainer _other) {
        final WebScrollpane.Builder<_B> _newBuilder = new WebScrollpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebScrollpane.Builder<_B> copyOf(final WebScrollpane _other) {
        final WebScrollpane.Builder<_B> _newBuilder = new WebScrollpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebScrollpane.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree horizontalscrollbarPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("horizontalscrollbar"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(horizontalscrollbarPropertyTree!= null):((horizontalscrollbarPropertyTree == null)||(!horizontalscrollbarPropertyTree.isLeaf())))) {
            _other.horizontalscrollbar = this.horizontalscrollbar;
        }
        final PropertyTree verticalscrollbarPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("verticalscrollbar"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(verticalscrollbarPropertyTree!= null):((verticalscrollbarPropertyTree == null)||(!verticalscrollbarPropertyTree.isLeaf())))) {
            _other.verticalscrollbar = this.verticalscrollbar;
        }
    }

    @Override
    public<_B >WebScrollpane.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebScrollpane.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebScrollpane.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebScrollpane.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebScrollpane.Builder<_B> _newBuilder = new WebScrollpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebScrollpane.Builder<_B> copyOf(final WebContainer _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebScrollpane.Builder<_B> _newBuilder = new WebScrollpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebScrollpane.Builder<_B> copyOf(final WebScrollpane _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebScrollpane.Builder<_B> _newBuilder = new WebScrollpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebScrollpane.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebScrollpane.Builder<Void> copyExcept(final WebContainer _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebScrollpane.Builder<Void> copyExcept(final WebScrollpane _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebScrollpane.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebScrollpane.Builder<Void> copyOnly(final WebContainer _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebScrollpane.Builder<Void> copyOnly(final WebScrollpane _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebContainer.Builder<_B>
        implements Buildable
    {

        private String horizontalscrollbar;
        private String verticalscrollbar;

        public Builder(final _B _parentBuilder, final WebScrollpane _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.horizontalscrollbar = _other.horizontalscrollbar;
                this.verticalscrollbar = _other.verticalscrollbar;
            }
        }

        public Builder(final _B _parentBuilder, final WebScrollpane _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree horizontalscrollbarPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("horizontalscrollbar"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(horizontalscrollbarPropertyTree!= null):((horizontalscrollbarPropertyTree == null)||(!horizontalscrollbarPropertyTree.isLeaf())))) {
                    this.horizontalscrollbar = _other.horizontalscrollbar;
                }
                final PropertyTree verticalscrollbarPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("verticalscrollbar"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(verticalscrollbarPropertyTree!= null):((verticalscrollbarPropertyTree == null)||(!verticalscrollbarPropertyTree.isLeaf())))) {
                    this.verticalscrollbar = _other.verticalscrollbar;
                }
            }
        }

        protected<_P extends WebScrollpane >_P init(final _P _product) {
            _product.horizontalscrollbar = this.horizontalscrollbar;
            _product.verticalscrollbar = this.verticalscrollbar;
            return super.init(_product);
        }

        /**
         * Sets the new value of "horizontalscrollbar" (any previous value will be replaced)
         * 
         * @param horizontalscrollbar
         *     New value of the "horizontalscrollbar" property.
         */
        public WebScrollpane.Builder<_B> withHorizontalscrollbar(final String horizontalscrollbar) {
            this.horizontalscrollbar = horizontalscrollbar;
            return this;
        }

        /**
         * Sets the new value of "verticalscrollbar" (any previous value will be replaced)
         * 
         * @param verticalscrollbar
         *     New value of the "verticalscrollbar" property.
         */
        public WebScrollpane.Builder<_B> withVerticalscrollbar(final String verticalscrollbar) {
            this.verticalscrollbar = verticalscrollbar;
            return this;
        }

        /**
         * Sets the new value of "grid" (any previous value will be replaced)
         * 
         * @param grid
         *     New value of the "grid" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withGrid(final WebGrid grid) {
            super.withGrid(grid);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "grid" property.
         * Use {@link org.nuclos.schema.layout.web.WebGrid.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "grid" property.
         *     Use {@link org.nuclos.schema.layout.web.WebGrid.Builder#end()} to return to the current builder.
         */
        public WebGrid.Builder<? extends WebScrollpane.Builder<_B>> withGrid() {
            return ((WebGrid.Builder<? extends WebScrollpane.Builder<_B>> ) super.withGrid());
        }

        /**
         * Sets the new value of "table" (any previous value will be replaced)
         * 
         * @param table
         *     New value of the "table" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withTable(final WebTable table) {
            super.withTable(table);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "table" property.
         * Use {@link org.nuclos.schema.layout.web.WebTable.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "table" property.
         *     Use {@link org.nuclos.schema.layout.web.WebTable.Builder#end()} to return to the current builder.
         */
        public WebTable.Builder<? extends WebScrollpane.Builder<_B>> withTable() {
            return ((WebTable.Builder<? extends WebScrollpane.Builder<_B>> ) super.withTable());
        }

        /**
         * Sets the new value of "calculated" (any previous value will be replaced)
         * 
         * @param calculated
         *     New value of the "calculated" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withCalculated(final WebGridCalculated calculated) {
            super.withCalculated(calculated);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "calculated" property.
         * Use {@link org.nuclos.schema.layout.web.WebGridCalculated.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "calculated" property.
         *     Use {@link org.nuclos.schema.layout.web.WebGridCalculated.Builder#end()} to return to the current builder.
         */
        public WebGridCalculated.Builder<? extends WebScrollpane.Builder<_B>> withCalculated() {
            return ((WebGridCalculated.Builder<? extends WebScrollpane.Builder<_B>> ) super.withCalculated());
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        @Override
        public WebScrollpane.Builder<_B> addComponents(final Iterable<? extends WebComponent> components) {
            super.addComponents(components);
            return this;
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        @Override
        public WebScrollpane.Builder<_B> addComponents(WebComponent... components) {
            super.addComponents(components);
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withComponents(final Iterable<? extends WebComponent> components) {
            super.withComponents(components);
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withComponents(WebComponent... components) {
            super.withComponents(components);
            return this;
        }

        /**
         * Sets the new value of "opaque" (any previous value will be replaced)
         * 
         * @param opaque
         *     New value of the "opaque" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withOpaque(final Boolean opaque) {
            super.withOpaque(opaque);
            return this;
        }

        /**
         * Sets the new value of "backgroundColor" (any previous value will be replaced)
         * 
         * @param backgroundColor
         *     New value of the "backgroundColor" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withBackgroundColor(final String backgroundColor) {
            super.withBackgroundColor(backgroundColor);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebScrollpane.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebScrollpane.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebScrollpane.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebScrollpane.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "borderProperty" (any previous value will be replaced)
         * 
         * @param borderProperty
         *     New value of the "borderProperty" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withBorderProperty(final WebBorderProperty borderProperty) {
            super.withBorderProperty(borderProperty);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "borderProperty" property.
         * Use {@link org.nuclos.schema.layout.web.WebBorderProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "borderProperty" property.
         *     Use {@link org.nuclos.schema.layout.web.WebBorderProperty.Builder#end()} to return to the current builder.
         */
        public WebBorderProperty.Builder<? extends WebScrollpane.Builder<_B>> withBorderProperty() {
            return ((WebBorderProperty.Builder<? extends WebScrollpane.Builder<_B>> ) super.withBorderProperty());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "customBackgroundColor" (any previous value will be replaced)
         * 
         * @param customBackgroundColor
         *     New value of the "customBackgroundColor" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withCustomBackgroundColor(final String customBackgroundColor) {
            super.withCustomBackgroundColor(customBackgroundColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "verticalAlign" (any previous value will be replaced)
         * 
         * @param verticalAlign
         *     New value of the "verticalAlign" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withVerticalAlign(final String verticalAlign) {
            super.withVerticalAlign(verticalAlign);
            return this;
        }

        /**
         * Sets the new value of "horizontalAlign" (any previous value will be replaced)
         * 
         * @param horizontalAlign
         *     New value of the "horizontalAlign" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withHorizontalAlign(final String horizontalAlign) {
            super.withHorizontalAlign(horizontalAlign);
            return this;
        }

        /**
         * Sets the new value of "minimumWidth" (any previous value will be replaced)
         * 
         * @param minimumWidth
         *     New value of the "minimumWidth" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withMinimumWidth(final BigInteger minimumWidth) {
            super.withMinimumWidth(minimumWidth);
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withLabel(final String label) {
            super.withLabel(label);
            return this;
        }

        /**
         * Sets the new value of "minimumHeight" (any previous value will be replaced)
         * 
         * @param minimumHeight
         *     New value of the "minimumHeight" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withMinimumHeight(final BigInteger minimumHeight) {
            super.withMinimumHeight(minimumHeight);
            return this;
        }

        /**
         * Sets the new value of "preferredWidth" (any previous value will be replaced)
         * 
         * @param preferredWidth
         *     New value of the "preferredWidth" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withPreferredWidth(final BigInteger preferredWidth) {
            super.withPreferredWidth(preferredWidth);
            return this;
        }

        /**
         * Sets the new value of "preferredHeight" (any previous value will be replaced)
         * 
         * @param preferredHeight
         *     New value of the "preferredHeight" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withPreferredHeight(final BigInteger preferredHeight) {
            super.withPreferredHeight(preferredHeight);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "descriptionAttr" (any previous value will be replaced)
         * 
         * @param descriptionAttr
         *     New value of the "descriptionAttr" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withDescriptionAttr(final String descriptionAttr) {
            super.withDescriptionAttr(descriptionAttr);
            return this;
        }

        /**
         * Sets the new value of "initialFocus" (any previous value will be replaced)
         * 
         * @param initialFocus
         *     New value of the "initialFocus" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withInitialFocus(final Boolean initialFocus) {
            super.withInitialFocus(initialFocus);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        /**
         * Sets the new value of "hasTextModules" (any previous value will be replaced)
         * 
         * @param hasTextModules
         *     New value of the "hasTextModules" property.
         */
        @Override
        public WebScrollpane.Builder<_B> withHasTextModules(final Boolean hasTextModules) {
            super.withHasTextModules(hasTextModules);
            return this;
        }

        @Override
        public WebScrollpane build() {
            if (_storedValue == null) {
                return this.init(new WebScrollpane());
            } else {
                return ((WebScrollpane) _storedValue);
            }
        }

        public WebScrollpane.Builder<_B> copyOf(final WebScrollpane _other) {
            _other.copyTo(this);
            return this;
        }

        public WebScrollpane.Builder<_B> copyOf(final WebScrollpane.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebScrollpane.Selector<WebScrollpane.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebScrollpane.Select _root() {
            return new WebScrollpane.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebContainer.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, WebScrollpane.Selector<TRoot, TParent>> horizontalscrollbar = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebScrollpane.Selector<TRoot, TParent>> verticalscrollbar = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.horizontalscrollbar!= null) {
                products.put("horizontalscrollbar", this.horizontalscrollbar.init());
            }
            if (this.verticalscrollbar!= null) {
                products.put("verticalscrollbar", this.verticalscrollbar.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebScrollpane.Selector<TRoot, TParent>> horizontalscrollbar() {
            return ((this.horizontalscrollbar == null)?this.horizontalscrollbar = new com.kscs.util.jaxb.Selector<TRoot, WebScrollpane.Selector<TRoot, TParent>>(this._root, this, "horizontalscrollbar"):this.horizontalscrollbar);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebScrollpane.Selector<TRoot, TParent>> verticalscrollbar() {
            return ((this.verticalscrollbar == null)?this.verticalscrollbar = new com.kscs.util.jaxb.Selector<TRoot, WebScrollpane.Selector<TRoot, TParent>>(this._root, this, "verticalscrollbar"):this.verticalscrollbar);
        }

    }

}
