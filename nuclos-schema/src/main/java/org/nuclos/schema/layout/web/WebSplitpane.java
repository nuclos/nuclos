package org.nuclos.schema.layout.web;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A container element which can hold 2 panels and is split by a divider.
 * 
 * <p>Java class for web-splitpane complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="web-splitpane"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{NuclosWebLayout}web-container"&gt;
 *       &lt;attribute name="dividerOrientation" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="defaultResizeWeight" type="{http://www.w3.org/2001/XMLSchema}float" /&gt;
 *       &lt;attribute name="dividerSize" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="collapsable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "web-splitpane")
public class WebSplitpane
    extends WebContainer
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "dividerOrientation")
    protected String dividerOrientation;
    @XmlAttribute(name = "defaultResizeWeight")
    protected Float defaultResizeWeight;
    @XmlAttribute(name = "dividerSize")
    protected BigInteger dividerSize;
    @XmlAttribute(name = "collapsable")
    protected Boolean collapsable;

    /**
     * Gets the value of the dividerOrientation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDividerOrientation() {
        return dividerOrientation;
    }

    /**
     * Sets the value of the dividerOrientation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDividerOrientation(String value) {
        this.dividerOrientation = value;
    }

    /**
     * Gets the value of the defaultResizeWeight property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getDefaultResizeWeight() {
        return defaultResizeWeight;
    }

    /**
     * Sets the value of the defaultResizeWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setDefaultResizeWeight(Float value) {
        this.defaultResizeWeight = value;
    }

    /**
     * Gets the value of the dividerSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDividerSize() {
        return dividerSize;
    }

    /**
     * Sets the value of the dividerSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDividerSize(BigInteger value) {
        this.dividerSize = value;
    }

    /**
     * Gets the value of the collapsable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCollapsable() {
        return collapsable;
    }

    /**
     * Sets the value of the collapsable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCollapsable(Boolean value) {
        this.collapsable = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theDividerOrientation;
            theDividerOrientation = this.getDividerOrientation();
            strategy.appendField(locator, this, "dividerOrientation", buffer, theDividerOrientation);
        }
        {
            Float theDefaultResizeWeight;
            theDefaultResizeWeight = this.getDefaultResizeWeight();
            strategy.appendField(locator, this, "defaultResizeWeight", buffer, theDefaultResizeWeight);
        }
        {
            BigInteger theDividerSize;
            theDividerSize = this.getDividerSize();
            strategy.appendField(locator, this, "dividerSize", buffer, theDividerSize);
        }
        {
            Boolean theCollapsable;
            theCollapsable = this.isCollapsable();
            strategy.appendField(locator, this, "collapsable", buffer, theCollapsable);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof WebSplitpane)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final WebSplitpane that = ((WebSplitpane) object);
        {
            String lhsDividerOrientation;
            lhsDividerOrientation = this.getDividerOrientation();
            String rhsDividerOrientation;
            rhsDividerOrientation = that.getDividerOrientation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dividerOrientation", lhsDividerOrientation), LocatorUtils.property(thatLocator, "dividerOrientation", rhsDividerOrientation), lhsDividerOrientation, rhsDividerOrientation)) {
                return false;
            }
        }
        {
            Float lhsDefaultResizeWeight;
            lhsDefaultResizeWeight = this.getDefaultResizeWeight();
            Float rhsDefaultResizeWeight;
            rhsDefaultResizeWeight = that.getDefaultResizeWeight();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "defaultResizeWeight", lhsDefaultResizeWeight), LocatorUtils.property(thatLocator, "defaultResizeWeight", rhsDefaultResizeWeight), lhsDefaultResizeWeight, rhsDefaultResizeWeight)) {
                return false;
            }
        }
        {
            BigInteger lhsDividerSize;
            lhsDividerSize = this.getDividerSize();
            BigInteger rhsDividerSize;
            rhsDividerSize = that.getDividerSize();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dividerSize", lhsDividerSize), LocatorUtils.property(thatLocator, "dividerSize", rhsDividerSize), lhsDividerSize, rhsDividerSize)) {
                return false;
            }
        }
        {
            Boolean lhsCollapsable;
            lhsCollapsable = this.isCollapsable();
            Boolean rhsCollapsable;
            rhsCollapsable = that.isCollapsable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "collapsable", lhsCollapsable), LocatorUtils.property(thatLocator, "collapsable", rhsCollapsable), lhsCollapsable, rhsCollapsable)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theDividerOrientation;
            theDividerOrientation = this.getDividerOrientation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dividerOrientation", theDividerOrientation), currentHashCode, theDividerOrientation);
        }
        {
            Float theDefaultResizeWeight;
            theDefaultResizeWeight = this.getDefaultResizeWeight();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "defaultResizeWeight", theDefaultResizeWeight), currentHashCode, theDefaultResizeWeight);
        }
        {
            BigInteger theDividerSize;
            theDividerSize = this.getDividerSize();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dividerSize", theDividerSize), currentHashCode, theDividerSize);
        }
        {
            Boolean theCollapsable;
            theCollapsable = this.isCollapsable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "collapsable", theCollapsable), currentHashCode, theCollapsable);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof WebSplitpane) {
            final WebSplitpane copy = ((WebSplitpane) draftCopy);
            if (this.dividerOrientation!= null) {
                String sourceDividerOrientation;
                sourceDividerOrientation = this.getDividerOrientation();
                String copyDividerOrientation = ((String) strategy.copy(LocatorUtils.property(locator, "dividerOrientation", sourceDividerOrientation), sourceDividerOrientation));
                copy.setDividerOrientation(copyDividerOrientation);
            } else {
                copy.dividerOrientation = null;
            }
            if (this.defaultResizeWeight!= null) {
                Float sourceDefaultResizeWeight;
                sourceDefaultResizeWeight = this.getDefaultResizeWeight();
                Float copyDefaultResizeWeight = ((Float) strategy.copy(LocatorUtils.property(locator, "defaultResizeWeight", sourceDefaultResizeWeight), sourceDefaultResizeWeight));
                copy.setDefaultResizeWeight(copyDefaultResizeWeight);
            } else {
                copy.defaultResizeWeight = null;
            }
            if (this.dividerSize!= null) {
                BigInteger sourceDividerSize;
                sourceDividerSize = this.getDividerSize();
                BigInteger copyDividerSize = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "dividerSize", sourceDividerSize), sourceDividerSize));
                copy.setDividerSize(copyDividerSize);
            } else {
                copy.dividerSize = null;
            }
            if (this.collapsable!= null) {
                Boolean sourceCollapsable;
                sourceCollapsable = this.isCollapsable();
                Boolean copyCollapsable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "collapsable", sourceCollapsable), sourceCollapsable));
                copy.setCollapsable(copyCollapsable);
            } else {
                copy.collapsable = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new WebSplitpane();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebSplitpane.Builder<_B> _other) {
        super.copyTo(_other);
        _other.dividerOrientation = this.dividerOrientation;
        _other.defaultResizeWeight = this.defaultResizeWeight;
        _other.dividerSize = this.dividerSize;
        _other.collapsable = this.collapsable;
    }

    @Override
    public<_B >WebSplitpane.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new WebSplitpane.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public WebSplitpane.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static WebSplitpane.Builder<Void> builder() {
        return new WebSplitpane.Builder<Void>(null, null, false);
    }

    public static<_B >WebSplitpane.Builder<_B> copyOf(final WebComponent _other) {
        final WebSplitpane.Builder<_B> _newBuilder = new WebSplitpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebSplitpane.Builder<_B> copyOf(final WebContainer _other) {
        final WebSplitpane.Builder<_B> _newBuilder = new WebSplitpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >WebSplitpane.Builder<_B> copyOf(final WebSplitpane _other) {
        final WebSplitpane.Builder<_B> _newBuilder = new WebSplitpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final WebSplitpane.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree dividerOrientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dividerOrientation"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dividerOrientationPropertyTree!= null):((dividerOrientationPropertyTree == null)||(!dividerOrientationPropertyTree.isLeaf())))) {
            _other.dividerOrientation = this.dividerOrientation;
        }
        final PropertyTree defaultResizeWeightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("defaultResizeWeight"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(defaultResizeWeightPropertyTree!= null):((defaultResizeWeightPropertyTree == null)||(!defaultResizeWeightPropertyTree.isLeaf())))) {
            _other.defaultResizeWeight = this.defaultResizeWeight;
        }
        final PropertyTree dividerSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dividerSize"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dividerSizePropertyTree!= null):((dividerSizePropertyTree == null)||(!dividerSizePropertyTree.isLeaf())))) {
            _other.dividerSize = this.dividerSize;
        }
        final PropertyTree collapsablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("collapsable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(collapsablePropertyTree!= null):((collapsablePropertyTree == null)||(!collapsablePropertyTree.isLeaf())))) {
            _other.collapsable = this.collapsable;
        }
    }

    @Override
    public<_B >WebSplitpane.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new WebSplitpane.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public WebSplitpane.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >WebSplitpane.Builder<_B> copyOf(final WebComponent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebSplitpane.Builder<_B> _newBuilder = new WebSplitpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebSplitpane.Builder<_B> copyOf(final WebContainer _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebSplitpane.Builder<_B> _newBuilder = new WebSplitpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >WebSplitpane.Builder<_B> copyOf(final WebSplitpane _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final WebSplitpane.Builder<_B> _newBuilder = new WebSplitpane.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static WebSplitpane.Builder<Void> copyExcept(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebSplitpane.Builder<Void> copyExcept(final WebContainer _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebSplitpane.Builder<Void> copyExcept(final WebSplitpane _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static WebSplitpane.Builder<Void> copyOnly(final WebComponent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebSplitpane.Builder<Void> copyOnly(final WebContainer _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static WebSplitpane.Builder<Void> copyOnly(final WebSplitpane _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends WebContainer.Builder<_B>
        implements Buildable
    {

        private String dividerOrientation;
        private Float defaultResizeWeight;
        private BigInteger dividerSize;
        private Boolean collapsable;

        public Builder(final _B _parentBuilder, final WebSplitpane _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.dividerOrientation = _other.dividerOrientation;
                this.defaultResizeWeight = _other.defaultResizeWeight;
                this.dividerSize = _other.dividerSize;
                this.collapsable = _other.collapsable;
            }
        }

        public Builder(final _B _parentBuilder, final WebSplitpane _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree dividerOrientationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dividerOrientation"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dividerOrientationPropertyTree!= null):((dividerOrientationPropertyTree == null)||(!dividerOrientationPropertyTree.isLeaf())))) {
                    this.dividerOrientation = _other.dividerOrientation;
                }
                final PropertyTree defaultResizeWeightPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("defaultResizeWeight"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(defaultResizeWeightPropertyTree!= null):((defaultResizeWeightPropertyTree == null)||(!defaultResizeWeightPropertyTree.isLeaf())))) {
                    this.defaultResizeWeight = _other.defaultResizeWeight;
                }
                final PropertyTree dividerSizePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dividerSize"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dividerSizePropertyTree!= null):((dividerSizePropertyTree == null)||(!dividerSizePropertyTree.isLeaf())))) {
                    this.dividerSize = _other.dividerSize;
                }
                final PropertyTree collapsablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("collapsable"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(collapsablePropertyTree!= null):((collapsablePropertyTree == null)||(!collapsablePropertyTree.isLeaf())))) {
                    this.collapsable = _other.collapsable;
                }
            }
        }

        protected<_P extends WebSplitpane >_P init(final _P _product) {
            _product.dividerOrientation = this.dividerOrientation;
            _product.defaultResizeWeight = this.defaultResizeWeight;
            _product.dividerSize = this.dividerSize;
            _product.collapsable = this.collapsable;
            return super.init(_product);
        }

        /**
         * Sets the new value of "dividerOrientation" (any previous value will be replaced)
         * 
         * @param dividerOrientation
         *     New value of the "dividerOrientation" property.
         */
        public WebSplitpane.Builder<_B> withDividerOrientation(final String dividerOrientation) {
            this.dividerOrientation = dividerOrientation;
            return this;
        }

        /**
         * Sets the new value of "defaultResizeWeight" (any previous value will be replaced)
         * 
         * @param defaultResizeWeight
         *     New value of the "defaultResizeWeight" property.
         */
        public WebSplitpane.Builder<_B> withDefaultResizeWeight(final Float defaultResizeWeight) {
            this.defaultResizeWeight = defaultResizeWeight;
            return this;
        }

        /**
         * Sets the new value of "dividerSize" (any previous value will be replaced)
         * 
         * @param dividerSize
         *     New value of the "dividerSize" property.
         */
        public WebSplitpane.Builder<_B> withDividerSize(final BigInteger dividerSize) {
            this.dividerSize = dividerSize;
            return this;
        }

        /**
         * Sets the new value of "collapsable" (any previous value will be replaced)
         * 
         * @param collapsable
         *     New value of the "collapsable" property.
         */
        public WebSplitpane.Builder<_B> withCollapsable(final Boolean collapsable) {
            this.collapsable = collapsable;
            return this;
        }

        /**
         * Sets the new value of "grid" (any previous value will be replaced)
         * 
         * @param grid
         *     New value of the "grid" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withGrid(final WebGrid grid) {
            super.withGrid(grid);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "grid" property.
         * Use {@link org.nuclos.schema.layout.web.WebGrid.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "grid" property.
         *     Use {@link org.nuclos.schema.layout.web.WebGrid.Builder#end()} to return to the current builder.
         */
        public WebGrid.Builder<? extends WebSplitpane.Builder<_B>> withGrid() {
            return ((WebGrid.Builder<? extends WebSplitpane.Builder<_B>> ) super.withGrid());
        }

        /**
         * Sets the new value of "table" (any previous value will be replaced)
         * 
         * @param table
         *     New value of the "table" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withTable(final WebTable table) {
            super.withTable(table);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "table" property.
         * Use {@link org.nuclos.schema.layout.web.WebTable.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "table" property.
         *     Use {@link org.nuclos.schema.layout.web.WebTable.Builder#end()} to return to the current builder.
         */
        public WebTable.Builder<? extends WebSplitpane.Builder<_B>> withTable() {
            return ((WebTable.Builder<? extends WebSplitpane.Builder<_B>> ) super.withTable());
        }

        /**
         * Sets the new value of "calculated" (any previous value will be replaced)
         * 
         * @param calculated
         *     New value of the "calculated" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withCalculated(final WebGridCalculated calculated) {
            super.withCalculated(calculated);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "calculated" property.
         * Use {@link org.nuclos.schema.layout.web.WebGridCalculated.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "calculated" property.
         *     Use {@link org.nuclos.schema.layout.web.WebGridCalculated.Builder#end()} to return to the current builder.
         */
        public WebGridCalculated.Builder<? extends WebSplitpane.Builder<_B>> withCalculated() {
            return ((WebGridCalculated.Builder<? extends WebSplitpane.Builder<_B>> ) super.withCalculated());
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        @Override
        public WebSplitpane.Builder<_B> addComponents(final Iterable<? extends WebComponent> components) {
            super.addComponents(components);
            return this;
        }

        /**
         * Adds the given items to the value of "components"
         * 
         * @param components
         *     Items to add to the value of the "components" property
         */
        @Override
        public WebSplitpane.Builder<_B> addComponents(WebComponent... components) {
            super.addComponents(components);
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withComponents(final Iterable<? extends WebComponent> components) {
            super.withComponents(components);
            return this;
        }

        /**
         * Sets the new value of "components" (any previous value will be replaced)
         * 
         * @param components
         *     New value of the "components" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withComponents(WebComponent... components) {
            super.withComponents(components);
            return this;
        }

        /**
         * Sets the new value of "opaque" (any previous value will be replaced)
         * 
         * @param opaque
         *     New value of the "opaque" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withOpaque(final Boolean opaque) {
            super.withOpaque(opaque);
            return this;
        }

        /**
         * Sets the new value of "backgroundColor" (any previous value will be replaced)
         * 
         * @param backgroundColor
         *     New value of the "backgroundColor" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withBackgroundColor(final String backgroundColor) {
            super.withBackgroundColor(backgroundColor);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebSplitpane.Builder<_B> addAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Adds the given items to the value of "advancedProperties"
         * 
         * @param advancedProperties
         *     Items to add to the value of the "advancedProperties" property
         */
        @Override
        public WebSplitpane.Builder<_B> addAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.addAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withAdvancedProperties(final Iterable<? extends WebAdvancedProperty> advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Sets the new value of "advancedProperties" (any previous value will be replaced)
         * 
         * @param advancedProperties
         *     New value of the "advancedProperties" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withAdvancedProperties(WebAdvancedProperty... advancedProperties) {
            super.withAdvancedProperties(advancedProperties);
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "advancedProperties" property.
         * Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "advancedProperties" property.
         *     Use {@link org.nuclos.schema.layout.web.WebAdvancedProperty.Builder#end()} to return to the current builder.
         */
        @Override
        public WebAdvancedProperty.Builder<? extends WebSplitpane.Builder<_B>> addAdvancedProperties() {
            return ((WebAdvancedProperty.Builder<? extends WebSplitpane.Builder<_B>> ) super.addAdvancedProperties());
        }

        /**
         * Sets the new value of "borderProperty" (any previous value will be replaced)
         * 
         * @param borderProperty
         *     New value of the "borderProperty" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withBorderProperty(final WebBorderProperty borderProperty) {
            super.withBorderProperty(borderProperty);
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "borderProperty" property.
         * Use {@link org.nuclos.schema.layout.web.WebBorderProperty.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "borderProperty" property.
         *     Use {@link org.nuclos.schema.layout.web.WebBorderProperty.Builder#end()} to return to the current builder.
         */
        public WebBorderProperty.Builder<? extends WebSplitpane.Builder<_B>> withBorderProperty() {
            return ((WebBorderProperty.Builder<? extends WebSplitpane.Builder<_B>> ) super.withBorderProperty());
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withId(final String id) {
            super.withId(id);
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withName(final String name) {
            super.withName(name);
            return this;
        }

        /**
         * Sets the new value of "column" (any previous value will be replaced)
         * 
         * @param column
         *     New value of the "column" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withColumn(final BigInteger column) {
            super.withColumn(column);
            return this;
        }

        /**
         * Sets the new value of "row" (any previous value will be replaced)
         * 
         * @param row
         *     New value of the "row" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withRow(final BigInteger row) {
            super.withRow(row);
            return this;
        }

        /**
         * Sets the new value of "colspan" (any previous value will be replaced)
         * 
         * @param colspan
         *     New value of the "colspan" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withColspan(final BigInteger colspan) {
            super.withColspan(colspan);
            return this;
        }

        /**
         * Sets the new value of "rowspan" (any previous value will be replaced)
         * 
         * @param rowspan
         *     New value of the "rowspan" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withRowspan(final BigInteger rowspan) {
            super.withRowspan(rowspan);
            return this;
        }

        /**
         * Sets the new value of "fontSize" (any previous value will be replaced)
         * 
         * @param fontSize
         *     New value of the "fontSize" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withFontSize(final String fontSize) {
            super.withFontSize(fontSize);
            return this;
        }

        /**
         * Sets the new value of "textColor" (any previous value will be replaced)
         * 
         * @param textColor
         *     New value of the "textColor" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withTextColor(final String textColor) {
            super.withTextColor(textColor);
            return this;
        }

        /**
         * Sets the new value of "customBackgroundColor" (any previous value will be replaced)
         * 
         * @param customBackgroundColor
         *     New value of the "customBackgroundColor" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withCustomBackgroundColor(final String customBackgroundColor) {
            super.withCustomBackgroundColor(customBackgroundColor);
            return this;
        }

        /**
         * Sets the new value of "bold" (any previous value will be replaced)
         * 
         * @param bold
         *     New value of the "bold" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withBold(final Boolean bold) {
            super.withBold(bold);
            return this;
        }

        /**
         * Sets the new value of "italic" (any previous value will be replaced)
         * 
         * @param italic
         *     New value of the "italic" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withItalic(final Boolean italic) {
            super.withItalic(italic);
            return this;
        }

        /**
         * Sets the new value of "underline" (any previous value will be replaced)
         * 
         * @param underline
         *     New value of the "underline" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withUnderline(final Boolean underline) {
            super.withUnderline(underline);
            return this;
        }

        /**
         * Sets the new value of "verticalAlign" (any previous value will be replaced)
         * 
         * @param verticalAlign
         *     New value of the "verticalAlign" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withVerticalAlign(final String verticalAlign) {
            super.withVerticalAlign(verticalAlign);
            return this;
        }

        /**
         * Sets the new value of "horizontalAlign" (any previous value will be replaced)
         * 
         * @param horizontalAlign
         *     New value of the "horizontalAlign" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withHorizontalAlign(final String horizontalAlign) {
            super.withHorizontalAlign(horizontalAlign);
            return this;
        }

        /**
         * Sets the new value of "minimumWidth" (any previous value will be replaced)
         * 
         * @param minimumWidth
         *     New value of the "minimumWidth" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withMinimumWidth(final BigInteger minimumWidth) {
            super.withMinimumWidth(minimumWidth);
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withLabel(final String label) {
            super.withLabel(label);
            return this;
        }

        /**
         * Sets the new value of "minimumHeight" (any previous value will be replaced)
         * 
         * @param minimumHeight
         *     New value of the "minimumHeight" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withMinimumHeight(final BigInteger minimumHeight) {
            super.withMinimumHeight(minimumHeight);
            return this;
        }

        /**
         * Sets the new value of "preferredWidth" (any previous value will be replaced)
         * 
         * @param preferredWidth
         *     New value of the "preferredWidth" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withPreferredWidth(final BigInteger preferredWidth) {
            super.withPreferredWidth(preferredWidth);
            return this;
        }

        /**
         * Sets the new value of "preferredHeight" (any previous value will be replaced)
         * 
         * @param preferredHeight
         *     New value of the "preferredHeight" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withPreferredHeight(final BigInteger preferredHeight) {
            super.withPreferredHeight(preferredHeight);
            return this;
        }

        /**
         * Sets the new value of "nextFocusComponent" (any previous value will be replaced)
         * 
         * @param nextFocusComponent
         *     New value of the "nextFocusComponent" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withNextFocusComponent(final String nextFocusComponent) {
            super.withNextFocusComponent(nextFocusComponent);
            return this;
        }

        /**
         * Sets the new value of "nextFocusField" (any previous value will be replaced)
         * 
         * @param nextFocusField
         *     New value of the "nextFocusField" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withNextFocusField(final String nextFocusField) {
            super.withNextFocusField(nextFocusField);
            return this;
        }

        /**
         * Sets the new value of "descriptionAttr" (any previous value will be replaced)
         * 
         * @param descriptionAttr
         *     New value of the "descriptionAttr" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withDescriptionAttr(final String descriptionAttr) {
            super.withDescriptionAttr(descriptionAttr);
            return this;
        }

        /**
         * Sets the new value of "initialFocus" (any previous value will be replaced)
         * 
         * @param initialFocus
         *     New value of the "initialFocus" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withInitialFocus(final Boolean initialFocus) {
            super.withInitialFocus(initialFocus);
            return this;
        }

        /**
         * Sets the new value of "alternativeTooltip" (any previous value will be replaced)
         * 
         * @param alternativeTooltip
         *     New value of the "alternativeTooltip" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withAlternativeTooltip(final String alternativeTooltip) {
            super.withAlternativeTooltip(alternativeTooltip);
            return this;
        }

        /**
         * Sets the new value of "hasTextModules" (any previous value will be replaced)
         * 
         * @param hasTextModules
         *     New value of the "hasTextModules" property.
         */
        @Override
        public WebSplitpane.Builder<_B> withHasTextModules(final Boolean hasTextModules) {
            super.withHasTextModules(hasTextModules);
            return this;
        }

        @Override
        public WebSplitpane build() {
            if (_storedValue == null) {
                return this.init(new WebSplitpane());
            } else {
                return ((WebSplitpane) _storedValue);
            }
        }

        public WebSplitpane.Builder<_B> copyOf(final WebSplitpane _other) {
            _other.copyTo(this);
            return this;
        }

        public WebSplitpane.Builder<_B> copyOf(final WebSplitpane.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends WebSplitpane.Selector<WebSplitpane.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static WebSplitpane.Select _root() {
            return new WebSplitpane.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends WebContainer.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>> dividerOrientation = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>> defaultResizeWeight = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>> dividerSize = null;
        private com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>> collapsable = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.dividerOrientation!= null) {
                products.put("dividerOrientation", this.dividerOrientation.init());
            }
            if (this.defaultResizeWeight!= null) {
                products.put("defaultResizeWeight", this.defaultResizeWeight.init());
            }
            if (this.dividerSize!= null) {
                products.put("dividerSize", this.dividerSize.init());
            }
            if (this.collapsable!= null) {
                products.put("collapsable", this.collapsable.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>> dividerOrientation() {
            return ((this.dividerOrientation == null)?this.dividerOrientation = new com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>>(this._root, this, "dividerOrientation"):this.dividerOrientation);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>> defaultResizeWeight() {
            return ((this.defaultResizeWeight == null)?this.defaultResizeWeight = new com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>>(this._root, this, "defaultResizeWeight"):this.defaultResizeWeight);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>> dividerSize() {
            return ((this.dividerSize == null)?this.dividerSize = new com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>>(this._root, this, "dividerSize"):this.dividerSize);
        }

        public com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>> collapsable() {
            return ((this.collapsable == null)?this.collapsable = new com.kscs.util.jaxb.Selector<TRoot, WebSplitpane.Selector<TRoot, TParent>>(this._root, this, "collapsable"):this.collapsable);
        }

    }

}
