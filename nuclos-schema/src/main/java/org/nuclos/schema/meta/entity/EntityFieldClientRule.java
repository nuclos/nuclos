package org.nuclos.schema.meta.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for entity-field-client-rule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-field-client-rule"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="clearValueStatements" type="{urn:org.nuclos.schema.meta.entity}entity-field-client-rule-statement-clear-value" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="transferLookedupValueStatements" type="{urn:org.nuclos.schema.meta.entity}entity-field-client-rule-statement-transfer-lookedup-value" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="triggerOnValueChange" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-field-client-rule", propOrder = {
    "clearValueStatements",
    "transferLookedupValueStatements"
})
public class EntityFieldClientRule implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<EntityFieldClientRuleStatementClearValue> clearValueStatements;
    protected List<EntityFieldClientRuleStatementTransferLookedupValue> transferLookedupValueStatements;
    @XmlAttribute(name = "triggerOnValueChange", required = true)
    protected boolean triggerOnValueChange;

    /**
     * Gets the value of the clearValueStatements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the clearValueStatements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClearValueStatements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityFieldClientRuleStatementClearValue }
     * 
     * 
     */
    public List<EntityFieldClientRuleStatementClearValue> getClearValueStatements() {
        if (clearValueStatements == null) {
            clearValueStatements = new ArrayList<EntityFieldClientRuleStatementClearValue>();
        }
        return this.clearValueStatements;
    }

    /**
     * Gets the value of the transferLookedupValueStatements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transferLookedupValueStatements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransferLookedupValueStatements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityFieldClientRuleStatementTransferLookedupValue }
     * 
     * 
     */
    public List<EntityFieldClientRuleStatementTransferLookedupValue> getTransferLookedupValueStatements() {
        if (transferLookedupValueStatements == null) {
            transferLookedupValueStatements = new ArrayList<EntityFieldClientRuleStatementTransferLookedupValue>();
        }
        return this.transferLookedupValueStatements;
    }

    /**
     * Gets the value of the triggerOnValueChange property.
     * 
     */
    public boolean isTriggerOnValueChange() {
        return triggerOnValueChange;
    }

    /**
     * Sets the value of the triggerOnValueChange property.
     * 
     */
    public void setTriggerOnValueChange(boolean value) {
        this.triggerOnValueChange = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<EntityFieldClientRuleStatementClearValue> theClearValueStatements;
            theClearValueStatements = (((this.clearValueStatements!= null)&&(!this.clearValueStatements.isEmpty()))?this.getClearValueStatements():null);
            strategy.appendField(locator, this, "clearValueStatements", buffer, theClearValueStatements);
        }
        {
            List<EntityFieldClientRuleStatementTransferLookedupValue> theTransferLookedupValueStatements;
            theTransferLookedupValueStatements = (((this.transferLookedupValueStatements!= null)&&(!this.transferLookedupValueStatements.isEmpty()))?this.getTransferLookedupValueStatements():null);
            strategy.appendField(locator, this, "transferLookedupValueStatements", buffer, theTransferLookedupValueStatements);
        }
        {
            boolean theTriggerOnValueChange;
            theTriggerOnValueChange = this.isTriggerOnValueChange();
            strategy.appendField(locator, this, "triggerOnValueChange", buffer, theTriggerOnValueChange);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityFieldClientRule)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityFieldClientRule that = ((EntityFieldClientRule) object);
        {
            List<EntityFieldClientRuleStatementClearValue> lhsClearValueStatements;
            lhsClearValueStatements = (((this.clearValueStatements!= null)&&(!this.clearValueStatements.isEmpty()))?this.getClearValueStatements():null);
            List<EntityFieldClientRuleStatementClearValue> rhsClearValueStatements;
            rhsClearValueStatements = (((that.clearValueStatements!= null)&&(!that.clearValueStatements.isEmpty()))?that.getClearValueStatements():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clearValueStatements", lhsClearValueStatements), LocatorUtils.property(thatLocator, "clearValueStatements", rhsClearValueStatements), lhsClearValueStatements, rhsClearValueStatements)) {
                return false;
            }
        }
        {
            List<EntityFieldClientRuleStatementTransferLookedupValue> lhsTransferLookedupValueStatements;
            lhsTransferLookedupValueStatements = (((this.transferLookedupValueStatements!= null)&&(!this.transferLookedupValueStatements.isEmpty()))?this.getTransferLookedupValueStatements():null);
            List<EntityFieldClientRuleStatementTransferLookedupValue> rhsTransferLookedupValueStatements;
            rhsTransferLookedupValueStatements = (((that.transferLookedupValueStatements!= null)&&(!that.transferLookedupValueStatements.isEmpty()))?that.getTransferLookedupValueStatements():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transferLookedupValueStatements", lhsTransferLookedupValueStatements), LocatorUtils.property(thatLocator, "transferLookedupValueStatements", rhsTransferLookedupValueStatements), lhsTransferLookedupValueStatements, rhsTransferLookedupValueStatements)) {
                return false;
            }
        }
        {
            boolean lhsTriggerOnValueChange;
            lhsTriggerOnValueChange = this.isTriggerOnValueChange();
            boolean rhsTriggerOnValueChange;
            rhsTriggerOnValueChange = that.isTriggerOnValueChange();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "triggerOnValueChange", lhsTriggerOnValueChange), LocatorUtils.property(thatLocator, "triggerOnValueChange", rhsTriggerOnValueChange), lhsTriggerOnValueChange, rhsTriggerOnValueChange)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<EntityFieldClientRuleStatementClearValue> theClearValueStatements;
            theClearValueStatements = (((this.clearValueStatements!= null)&&(!this.clearValueStatements.isEmpty()))?this.getClearValueStatements():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clearValueStatements", theClearValueStatements), currentHashCode, theClearValueStatements);
        }
        {
            List<EntityFieldClientRuleStatementTransferLookedupValue> theTransferLookedupValueStatements;
            theTransferLookedupValueStatements = (((this.transferLookedupValueStatements!= null)&&(!this.transferLookedupValueStatements.isEmpty()))?this.getTransferLookedupValueStatements():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "transferLookedupValueStatements", theTransferLookedupValueStatements), currentHashCode, theTransferLookedupValueStatements);
        }
        {
            boolean theTriggerOnValueChange;
            theTriggerOnValueChange = this.isTriggerOnValueChange();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "triggerOnValueChange", theTriggerOnValueChange), currentHashCode, theTriggerOnValueChange);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityFieldClientRule) {
            final EntityFieldClientRule copy = ((EntityFieldClientRule) draftCopy);
            if ((this.clearValueStatements!= null)&&(!this.clearValueStatements.isEmpty())) {
                List<EntityFieldClientRuleStatementClearValue> sourceClearValueStatements;
                sourceClearValueStatements = (((this.clearValueStatements!= null)&&(!this.clearValueStatements.isEmpty()))?this.getClearValueStatements():null);
                @SuppressWarnings("unchecked")
                List<EntityFieldClientRuleStatementClearValue> copyClearValueStatements = ((List<EntityFieldClientRuleStatementClearValue> ) strategy.copy(LocatorUtils.property(locator, "clearValueStatements", sourceClearValueStatements), sourceClearValueStatements));
                copy.clearValueStatements = null;
                if (copyClearValueStatements!= null) {
                    List<EntityFieldClientRuleStatementClearValue> uniqueClearValueStatementsl = copy.getClearValueStatements();
                    uniqueClearValueStatementsl.addAll(copyClearValueStatements);
                }
            } else {
                copy.clearValueStatements = null;
            }
            if ((this.transferLookedupValueStatements!= null)&&(!this.transferLookedupValueStatements.isEmpty())) {
                List<EntityFieldClientRuleStatementTransferLookedupValue> sourceTransferLookedupValueStatements;
                sourceTransferLookedupValueStatements = (((this.transferLookedupValueStatements!= null)&&(!this.transferLookedupValueStatements.isEmpty()))?this.getTransferLookedupValueStatements():null);
                @SuppressWarnings("unchecked")
                List<EntityFieldClientRuleStatementTransferLookedupValue> copyTransferLookedupValueStatements = ((List<EntityFieldClientRuleStatementTransferLookedupValue> ) strategy.copy(LocatorUtils.property(locator, "transferLookedupValueStatements", sourceTransferLookedupValueStatements), sourceTransferLookedupValueStatements));
                copy.transferLookedupValueStatements = null;
                if (copyTransferLookedupValueStatements!= null) {
                    List<EntityFieldClientRuleStatementTransferLookedupValue> uniqueTransferLookedupValueStatementsl = copy.getTransferLookedupValueStatements();
                    uniqueTransferLookedupValueStatementsl.addAll(copyTransferLookedupValueStatements);
                }
            } else {
                copy.transferLookedupValueStatements = null;
            }
            {
                boolean sourceTriggerOnValueChange;
                sourceTriggerOnValueChange = this.isTriggerOnValueChange();
                boolean copyTriggerOnValueChange = strategy.copy(LocatorUtils.property(locator, "triggerOnValueChange", sourceTriggerOnValueChange), sourceTriggerOnValueChange);
                copy.setTriggerOnValueChange(copyTriggerOnValueChange);
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityFieldClientRule();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldClientRule.Builder<_B> _other) {
        if (this.clearValueStatements == null) {
            _other.clearValueStatements = null;
        } else {
            _other.clearValueStatements = new ArrayList<EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>>>();
            for (EntityFieldClientRuleStatementClearValue _item: this.clearValueStatements) {
                _other.clearValueStatements.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.transferLookedupValueStatements == null) {
            _other.transferLookedupValueStatements = null;
        } else {
            _other.transferLookedupValueStatements = new ArrayList<EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>>>();
            for (EntityFieldClientRuleStatementTransferLookedupValue _item: this.transferLookedupValueStatements) {
                _other.transferLookedupValueStatements.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.triggerOnValueChange = this.triggerOnValueChange;
    }

    public<_B >EntityFieldClientRule.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityFieldClientRule.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityFieldClientRule.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityFieldClientRule.Builder<Void> builder() {
        return new EntityFieldClientRule.Builder<Void>(null, null, false);
    }

    public static<_B >EntityFieldClientRule.Builder<_B> copyOf(final EntityFieldClientRule _other) {
        final EntityFieldClientRule.Builder<_B> _newBuilder = new EntityFieldClientRule.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldClientRule.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree clearValueStatementsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearValueStatements"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearValueStatementsPropertyTree!= null):((clearValueStatementsPropertyTree == null)||(!clearValueStatementsPropertyTree.isLeaf())))) {
            if (this.clearValueStatements == null) {
                _other.clearValueStatements = null;
            } else {
                _other.clearValueStatements = new ArrayList<EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>>>();
                for (EntityFieldClientRuleStatementClearValue _item: this.clearValueStatements) {
                    _other.clearValueStatements.add(((_item == null)?null:_item.newCopyBuilder(_other, clearValueStatementsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree transferLookedupValueStatementsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("transferLookedupValueStatements"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(transferLookedupValueStatementsPropertyTree!= null):((transferLookedupValueStatementsPropertyTree == null)||(!transferLookedupValueStatementsPropertyTree.isLeaf())))) {
            if (this.transferLookedupValueStatements == null) {
                _other.transferLookedupValueStatements = null;
            } else {
                _other.transferLookedupValueStatements = new ArrayList<EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>>>();
                for (EntityFieldClientRuleStatementTransferLookedupValue _item: this.transferLookedupValueStatements) {
                    _other.transferLookedupValueStatements.add(((_item == null)?null:_item.newCopyBuilder(_other, transferLookedupValueStatementsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree triggerOnValueChangePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("triggerOnValueChange"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(triggerOnValueChangePropertyTree!= null):((triggerOnValueChangePropertyTree == null)||(!triggerOnValueChangePropertyTree.isLeaf())))) {
            _other.triggerOnValueChange = this.triggerOnValueChange;
        }
    }

    public<_B >EntityFieldClientRule.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityFieldClientRule.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityFieldClientRule.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityFieldClientRule.Builder<_B> copyOf(final EntityFieldClientRule _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityFieldClientRule.Builder<_B> _newBuilder = new EntityFieldClientRule.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityFieldClientRule.Builder<Void> copyExcept(final EntityFieldClientRule _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityFieldClientRule.Builder<Void> copyOnly(final EntityFieldClientRule _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityFieldClientRule _storedValue;
        private List<EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>>> clearValueStatements;
        private List<EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>>> transferLookedupValueStatements;
        private boolean triggerOnValueChange;

        public Builder(final _B _parentBuilder, final EntityFieldClientRule _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.clearValueStatements == null) {
                        this.clearValueStatements = null;
                    } else {
                        this.clearValueStatements = new ArrayList<EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>>>();
                        for (EntityFieldClientRuleStatementClearValue _item: _other.clearValueStatements) {
                            this.clearValueStatements.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.transferLookedupValueStatements == null) {
                        this.transferLookedupValueStatements = null;
                    } else {
                        this.transferLookedupValueStatements = new ArrayList<EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>>>();
                        for (EntityFieldClientRuleStatementTransferLookedupValue _item: _other.transferLookedupValueStatements) {
                            this.transferLookedupValueStatements.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.triggerOnValueChange = _other.triggerOnValueChange;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityFieldClientRule _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree clearValueStatementsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clearValueStatements"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clearValueStatementsPropertyTree!= null):((clearValueStatementsPropertyTree == null)||(!clearValueStatementsPropertyTree.isLeaf())))) {
                        if (_other.clearValueStatements == null) {
                            this.clearValueStatements = null;
                        } else {
                            this.clearValueStatements = new ArrayList<EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>>>();
                            for (EntityFieldClientRuleStatementClearValue _item: _other.clearValueStatements) {
                                this.clearValueStatements.add(((_item == null)?null:_item.newCopyBuilder(this, clearValueStatementsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree transferLookedupValueStatementsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("transferLookedupValueStatements"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(transferLookedupValueStatementsPropertyTree!= null):((transferLookedupValueStatementsPropertyTree == null)||(!transferLookedupValueStatementsPropertyTree.isLeaf())))) {
                        if (_other.transferLookedupValueStatements == null) {
                            this.transferLookedupValueStatements = null;
                        } else {
                            this.transferLookedupValueStatements = new ArrayList<EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>>>();
                            for (EntityFieldClientRuleStatementTransferLookedupValue _item: _other.transferLookedupValueStatements) {
                                this.transferLookedupValueStatements.add(((_item == null)?null:_item.newCopyBuilder(this, transferLookedupValueStatementsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree triggerOnValueChangePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("triggerOnValueChange"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(triggerOnValueChangePropertyTree!= null):((triggerOnValueChangePropertyTree == null)||(!triggerOnValueChangePropertyTree.isLeaf())))) {
                        this.triggerOnValueChange = _other.triggerOnValueChange;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityFieldClientRule >_P init(final _P _product) {
            if (this.clearValueStatements!= null) {
                final List<EntityFieldClientRuleStatementClearValue> clearValueStatements = new ArrayList<EntityFieldClientRuleStatementClearValue>(this.clearValueStatements.size());
                for (EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>> _item: this.clearValueStatements) {
                    clearValueStatements.add(_item.build());
                }
                _product.clearValueStatements = clearValueStatements;
            }
            if (this.transferLookedupValueStatements!= null) {
                final List<EntityFieldClientRuleStatementTransferLookedupValue> transferLookedupValueStatements = new ArrayList<EntityFieldClientRuleStatementTransferLookedupValue>(this.transferLookedupValueStatements.size());
                for (EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>> _item: this.transferLookedupValueStatements) {
                    transferLookedupValueStatements.add(_item.build());
                }
                _product.transferLookedupValueStatements = transferLookedupValueStatements;
            }
            _product.triggerOnValueChange = this.triggerOnValueChange;
            return _product;
        }

        /**
         * Adds the given items to the value of "clearValueStatements"
         * 
         * @param clearValueStatements
         *     Items to add to the value of the "clearValueStatements" property
         */
        public EntityFieldClientRule.Builder<_B> addClearValueStatements(final Iterable<? extends EntityFieldClientRuleStatementClearValue> clearValueStatements) {
            if (clearValueStatements!= null) {
                if (this.clearValueStatements == null) {
                    this.clearValueStatements = new ArrayList<EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>>>();
                }
                for (EntityFieldClientRuleStatementClearValue _item: clearValueStatements) {
                    this.clearValueStatements.add(new EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "clearValueStatements" (any previous value will be replaced)
         * 
         * @param clearValueStatements
         *     New value of the "clearValueStatements" property.
         */
        public EntityFieldClientRule.Builder<_B> withClearValueStatements(final Iterable<? extends EntityFieldClientRuleStatementClearValue> clearValueStatements) {
            if (this.clearValueStatements!= null) {
                this.clearValueStatements.clear();
            }
            return addClearValueStatements(clearValueStatements);
        }

        /**
         * Adds the given items to the value of "clearValueStatements"
         * 
         * @param clearValueStatements
         *     Items to add to the value of the "clearValueStatements" property
         */
        public EntityFieldClientRule.Builder<_B> addClearValueStatements(EntityFieldClientRuleStatementClearValue... clearValueStatements) {
            addClearValueStatements(Arrays.asList(clearValueStatements));
            return this;
        }

        /**
         * Sets the new value of "clearValueStatements" (any previous value will be replaced)
         * 
         * @param clearValueStatements
         *     New value of the "clearValueStatements" property.
         */
        public EntityFieldClientRule.Builder<_B> withClearValueStatements(EntityFieldClientRuleStatementClearValue... clearValueStatements) {
            withClearValueStatements(Arrays.asList(clearValueStatements));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "ClearValueStatements" property.
         * Use {@link org.nuclos.schema.meta.entity.EntityFieldClientRuleStatementClearValue.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "ClearValueStatements" property.
         *     Use {@link org.nuclos.schema.meta.entity.EntityFieldClientRuleStatementClearValue.Builder#end()} to return to the current builder.
         */
        public EntityFieldClientRuleStatementClearValue.Builder<? extends EntityFieldClientRule.Builder<_B>> addClearValueStatements() {
            if (this.clearValueStatements == null) {
                this.clearValueStatements = new ArrayList<EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>>>();
            }
            final EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>> clearValueStatements_Builder = new EntityFieldClientRuleStatementClearValue.Builder<EntityFieldClientRule.Builder<_B>>(this, null, false);
            this.clearValueStatements.add(clearValueStatements_Builder);
            return clearValueStatements_Builder;
        }

        /**
         * Adds the given items to the value of "transferLookedupValueStatements"
         * 
         * @param transferLookedupValueStatements
         *     Items to add to the value of the "transferLookedupValueStatements" property
         */
        public EntityFieldClientRule.Builder<_B> addTransferLookedupValueStatements(final Iterable<? extends EntityFieldClientRuleStatementTransferLookedupValue> transferLookedupValueStatements) {
            if (transferLookedupValueStatements!= null) {
                if (this.transferLookedupValueStatements == null) {
                    this.transferLookedupValueStatements = new ArrayList<EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>>>();
                }
                for (EntityFieldClientRuleStatementTransferLookedupValue _item: transferLookedupValueStatements) {
                    this.transferLookedupValueStatements.add(new EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "transferLookedupValueStatements" (any previous value will be replaced)
         * 
         * @param transferLookedupValueStatements
         *     New value of the "transferLookedupValueStatements" property.
         */
        public EntityFieldClientRule.Builder<_B> withTransferLookedupValueStatements(final Iterable<? extends EntityFieldClientRuleStatementTransferLookedupValue> transferLookedupValueStatements) {
            if (this.transferLookedupValueStatements!= null) {
                this.transferLookedupValueStatements.clear();
            }
            return addTransferLookedupValueStatements(transferLookedupValueStatements);
        }

        /**
         * Adds the given items to the value of "transferLookedupValueStatements"
         * 
         * @param transferLookedupValueStatements
         *     Items to add to the value of the "transferLookedupValueStatements" property
         */
        public EntityFieldClientRule.Builder<_B> addTransferLookedupValueStatements(EntityFieldClientRuleStatementTransferLookedupValue... transferLookedupValueStatements) {
            addTransferLookedupValueStatements(Arrays.asList(transferLookedupValueStatements));
            return this;
        }

        /**
         * Sets the new value of "transferLookedupValueStatements" (any previous value will be replaced)
         * 
         * @param transferLookedupValueStatements
         *     New value of the "transferLookedupValueStatements" property.
         */
        public EntityFieldClientRule.Builder<_B> withTransferLookedupValueStatements(EntityFieldClientRuleStatementTransferLookedupValue... transferLookedupValueStatements) {
            withTransferLookedupValueStatements(Arrays.asList(transferLookedupValueStatements));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "TransferLookedupValueStatements" property.
         * Use {@link org.nuclos.schema.meta.entity.EntityFieldClientRuleStatementTransferLookedupValue.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "TransferLookedupValueStatements" property.
         *     Use {@link org.nuclos.schema.meta.entity.EntityFieldClientRuleStatementTransferLookedupValue.Builder#end()} to return to the current builder.
         */
        public EntityFieldClientRuleStatementTransferLookedupValue.Builder<? extends EntityFieldClientRule.Builder<_B>> addTransferLookedupValueStatements() {
            if (this.transferLookedupValueStatements == null) {
                this.transferLookedupValueStatements = new ArrayList<EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>>>();
            }
            final EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>> transferLookedupValueStatements_Builder = new EntityFieldClientRuleStatementTransferLookedupValue.Builder<EntityFieldClientRule.Builder<_B>>(this, null, false);
            this.transferLookedupValueStatements.add(transferLookedupValueStatements_Builder);
            return transferLookedupValueStatements_Builder;
        }

        /**
         * Sets the new value of "triggerOnValueChange" (any previous value will be replaced)
         * 
         * @param triggerOnValueChange
         *     New value of the "triggerOnValueChange" property.
         */
        public EntityFieldClientRule.Builder<_B> withTriggerOnValueChange(final boolean triggerOnValueChange) {
            this.triggerOnValueChange = triggerOnValueChange;
            return this;
        }

        @Override
        public EntityFieldClientRule build() {
            if (_storedValue == null) {
                return this.init(new EntityFieldClientRule());
            } else {
                return ((EntityFieldClientRule) _storedValue);
            }
        }

        public EntityFieldClientRule.Builder<_B> copyOf(final EntityFieldClientRule _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityFieldClientRule.Builder<_B> copyOf(final EntityFieldClientRule.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityFieldClientRule.Selector<EntityFieldClientRule.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityFieldClientRule.Select _root() {
            return new EntityFieldClientRule.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private EntityFieldClientRuleStatementClearValue.Selector<TRoot, EntityFieldClientRule.Selector<TRoot, TParent>> clearValueStatements = null;
        private EntityFieldClientRuleStatementTransferLookedupValue.Selector<TRoot, EntityFieldClientRule.Selector<TRoot, TParent>> transferLookedupValueStatements = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.clearValueStatements!= null) {
                products.put("clearValueStatements", this.clearValueStatements.init());
            }
            if (this.transferLookedupValueStatements!= null) {
                products.put("transferLookedupValueStatements", this.transferLookedupValueStatements.init());
            }
            return products;
        }

        public EntityFieldClientRuleStatementClearValue.Selector<TRoot, EntityFieldClientRule.Selector<TRoot, TParent>> clearValueStatements() {
            return ((this.clearValueStatements == null)?this.clearValueStatements = new EntityFieldClientRuleStatementClearValue.Selector<TRoot, EntityFieldClientRule.Selector<TRoot, TParent>>(this._root, this, "clearValueStatements"):this.clearValueStatements);
        }

        public EntityFieldClientRuleStatementTransferLookedupValue.Selector<TRoot, EntityFieldClientRule.Selector<TRoot, TParent>> transferLookedupValueStatements() {
            return ((this.transferLookedupValueStatements == null)?this.transferLookedupValueStatements = new EntityFieldClientRuleStatementTransferLookedupValue.Selector<TRoot, EntityFieldClientRule.Selector<TRoot, TParent>>(this._root, this, "transferLookedupValueStatements"):this.transferLookedupValueStatements);
        }

    }

}
