package org.nuclos.schema.meta.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * A statement to be executed by the rule.
 * 
 * <p>Java class for entity-field-client-rule-statement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-field-client-rule-statement"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="order" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-field-client-rule-statement")
@XmlSeeAlso({
    EntityFieldClientRuleStatementTransferLookedupValue.class,
    EntityFieldClientRuleStatementClearValue.class
})
public abstract class EntityFieldClientRuleStatement implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "order", required = true)
    protected BigInteger order;

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOrder(BigInteger value) {
        this.order = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            BigInteger theOrder;
            theOrder = this.getOrder();
            strategy.appendField(locator, this, "order", buffer, theOrder);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityFieldClientRuleStatement)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityFieldClientRuleStatement that = ((EntityFieldClientRuleStatement) object);
        {
            BigInteger lhsOrder;
            lhsOrder = this.getOrder();
            BigInteger rhsOrder;
            rhsOrder = that.getOrder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "order", lhsOrder), LocatorUtils.property(thatLocator, "order", rhsOrder), lhsOrder, rhsOrder)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            BigInteger theOrder;
            theOrder = this.getOrder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "order", theOrder), currentHashCode, theOrder);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        if (null == target) {
            throw new IllegalArgumentException("Target argument must not be null for abstract copyable classes.");
        }
        if (target instanceof EntityFieldClientRuleStatement) {
            final EntityFieldClientRuleStatement copy = ((EntityFieldClientRuleStatement) target);
            if (this.order!= null) {
                BigInteger sourceOrder;
                sourceOrder = this.getOrder();
                BigInteger copyOrder = ((BigInteger) strategy.copy(LocatorUtils.property(locator, "order", sourceOrder), sourceOrder));
                copy.setOrder(copyOrder);
            } else {
                copy.order = null;
            }
        }
        return target;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldClientRuleStatement.Builder<_B> _other) {
        _other.order = this.order;
    }

    public abstract<_B >EntityFieldClientRuleStatement.Builder<_B> newCopyBuilder(final _B _parentBuilder);

    public abstract EntityFieldClientRuleStatement.Builder<Void> newCopyBuilder();

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldClientRuleStatement.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree orderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("order"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orderPropertyTree!= null):((orderPropertyTree == null)||(!orderPropertyTree.isLeaf())))) {
            _other.order = this.order;
        }
    }

    public abstract<_B >EntityFieldClientRuleStatement.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse);

    public abstract EntityFieldClientRuleStatement.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse);

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityFieldClientRuleStatement _storedValue;
        private BigInteger order;

        public Builder(final _B _parentBuilder, final EntityFieldClientRuleStatement _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.order = _other.order;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityFieldClientRuleStatement _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree orderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("order"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(orderPropertyTree!= null):((orderPropertyTree == null)||(!orderPropertyTree.isLeaf())))) {
                        this.order = _other.order;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityFieldClientRuleStatement >_P init(final _P _product) {
            _product.order = this.order;
            return _product;
        }

        /**
         * Sets the new value of "order" (any previous value will be replaced)
         * 
         * @param order
         *     New value of the "order" property.
         */
        public EntityFieldClientRuleStatement.Builder<_B> withOrder(final BigInteger order) {
            this.order = order;
            return this;
        }

        @Override
        public EntityFieldClientRuleStatement build() {
            return ((EntityFieldClientRuleStatement) _storedValue);
        }

        public EntityFieldClientRuleStatement.Builder<_B> copyOf(final EntityFieldClientRuleStatement _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityFieldClientRuleStatement.Builder<_B> copyOf(final EntityFieldClientRuleStatement.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityFieldClientRuleStatement.Selector<EntityFieldClientRuleStatement.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityFieldClientRuleStatement.Select _root() {
            return new EntityFieldClientRuleStatement.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatement.Selector<TRoot, TParent>> order = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.order!= null) {
                products.put("order", this.order.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatement.Selector<TRoot, TParent>> order() {
            return ((this.order == null)?this.order = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatement.Selector<TRoot, TParent>>(this._root, this, "order"):this.order);
        }

    }

}
