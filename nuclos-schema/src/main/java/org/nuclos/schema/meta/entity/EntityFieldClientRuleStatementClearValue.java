package org.nuclos.schema.meta.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Clears a target field.
 * 
 * <p>Java class for entity-field-client-rule-statement-clear-value complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-field-client-rule-statement-clear-value"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.meta.entity}entity-field-client-rule-statement"&gt;
 *       &lt;attribute name="targetEntityFieldId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-field-client-rule-statement-clear-value")
public class EntityFieldClientRuleStatementClearValue
    extends EntityFieldClientRuleStatement
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "targetEntityFieldId", required = true)
    protected String targetEntityFieldId;

    /**
     * Gets the value of the targetEntityFieldId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetEntityFieldId() {
        return targetEntityFieldId;
    }

    /**
     * Sets the value of the targetEntityFieldId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetEntityFieldId(String value) {
        this.targetEntityFieldId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theTargetEntityFieldId;
            theTargetEntityFieldId = this.getTargetEntityFieldId();
            strategy.appendField(locator, this, "targetEntityFieldId", buffer, theTargetEntityFieldId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityFieldClientRuleStatementClearValue)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final EntityFieldClientRuleStatementClearValue that = ((EntityFieldClientRuleStatementClearValue) object);
        {
            String lhsTargetEntityFieldId;
            lhsTargetEntityFieldId = this.getTargetEntityFieldId();
            String rhsTargetEntityFieldId;
            rhsTargetEntityFieldId = that.getTargetEntityFieldId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetEntityFieldId", lhsTargetEntityFieldId), LocatorUtils.property(thatLocator, "targetEntityFieldId", rhsTargetEntityFieldId), lhsTargetEntityFieldId, rhsTargetEntityFieldId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theTargetEntityFieldId;
            theTargetEntityFieldId = this.getTargetEntityFieldId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "targetEntityFieldId", theTargetEntityFieldId), currentHashCode, theTargetEntityFieldId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof EntityFieldClientRuleStatementClearValue) {
            final EntityFieldClientRuleStatementClearValue copy = ((EntityFieldClientRuleStatementClearValue) draftCopy);
            if (this.targetEntityFieldId!= null) {
                String sourceTargetEntityFieldId;
                sourceTargetEntityFieldId = this.getTargetEntityFieldId();
                String copyTargetEntityFieldId = ((String) strategy.copy(LocatorUtils.property(locator, "targetEntityFieldId", sourceTargetEntityFieldId), sourceTargetEntityFieldId));
                copy.setTargetEntityFieldId(copyTargetEntityFieldId);
            } else {
                copy.targetEntityFieldId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityFieldClientRuleStatementClearValue();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldClientRuleStatementClearValue.Builder<_B> _other) {
        super.copyTo(_other);
        _other.targetEntityFieldId = this.targetEntityFieldId;
    }

    @Override
    public<_B >EntityFieldClientRuleStatementClearValue.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityFieldClientRuleStatementClearValue.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public EntityFieldClientRuleStatementClearValue.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityFieldClientRuleStatementClearValue.Builder<Void> builder() {
        return new EntityFieldClientRuleStatementClearValue.Builder<Void>(null, null, false);
    }

    public static<_B >EntityFieldClientRuleStatementClearValue.Builder<_B> copyOf(final EntityFieldClientRuleStatement _other) {
        final EntityFieldClientRuleStatementClearValue.Builder<_B> _newBuilder = new EntityFieldClientRuleStatementClearValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >EntityFieldClientRuleStatementClearValue.Builder<_B> copyOf(final EntityFieldClientRuleStatementClearValue _other) {
        final EntityFieldClientRuleStatementClearValue.Builder<_B> _newBuilder = new EntityFieldClientRuleStatementClearValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldClientRuleStatementClearValue.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree targetEntityFieldIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetEntityFieldId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetEntityFieldIdPropertyTree!= null):((targetEntityFieldIdPropertyTree == null)||(!targetEntityFieldIdPropertyTree.isLeaf())))) {
            _other.targetEntityFieldId = this.targetEntityFieldId;
        }
    }

    @Override
    public<_B >EntityFieldClientRuleStatementClearValue.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityFieldClientRuleStatementClearValue.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public EntityFieldClientRuleStatementClearValue.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityFieldClientRuleStatementClearValue.Builder<_B> copyOf(final EntityFieldClientRuleStatement _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityFieldClientRuleStatementClearValue.Builder<_B> _newBuilder = new EntityFieldClientRuleStatementClearValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >EntityFieldClientRuleStatementClearValue.Builder<_B> copyOf(final EntityFieldClientRuleStatementClearValue _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityFieldClientRuleStatementClearValue.Builder<_B> _newBuilder = new EntityFieldClientRuleStatementClearValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityFieldClientRuleStatementClearValue.Builder<Void> copyExcept(final EntityFieldClientRuleStatement _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityFieldClientRuleStatementClearValue.Builder<Void> copyExcept(final EntityFieldClientRuleStatementClearValue _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityFieldClientRuleStatementClearValue.Builder<Void> copyOnly(final EntityFieldClientRuleStatement _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static EntityFieldClientRuleStatementClearValue.Builder<Void> copyOnly(final EntityFieldClientRuleStatementClearValue _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends EntityFieldClientRuleStatement.Builder<_B>
        implements Buildable
    {

        private String targetEntityFieldId;

        public Builder(final _B _parentBuilder, final EntityFieldClientRuleStatementClearValue _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.targetEntityFieldId = _other.targetEntityFieldId;
            }
        }

        public Builder(final _B _parentBuilder, final EntityFieldClientRuleStatementClearValue _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree targetEntityFieldIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetEntityFieldId"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetEntityFieldIdPropertyTree!= null):((targetEntityFieldIdPropertyTree == null)||(!targetEntityFieldIdPropertyTree.isLeaf())))) {
                    this.targetEntityFieldId = _other.targetEntityFieldId;
                }
            }
        }

        protected<_P extends EntityFieldClientRuleStatementClearValue >_P init(final _P _product) {
            _product.targetEntityFieldId = this.targetEntityFieldId;
            return super.init(_product);
        }

        /**
         * Sets the new value of "targetEntityFieldId" (any previous value will be replaced)
         * 
         * @param targetEntityFieldId
         *     New value of the "targetEntityFieldId" property.
         */
        public EntityFieldClientRuleStatementClearValue.Builder<_B> withTargetEntityFieldId(final String targetEntityFieldId) {
            this.targetEntityFieldId = targetEntityFieldId;
            return this;
        }

        /**
         * Sets the new value of "order" (any previous value will be replaced)
         * 
         * @param order
         *     New value of the "order" property.
         */
        @Override
        public EntityFieldClientRuleStatementClearValue.Builder<_B> withOrder(final BigInteger order) {
            super.withOrder(order);
            return this;
        }

        @Override
        public EntityFieldClientRuleStatementClearValue build() {
            if (_storedValue == null) {
                return this.init(new EntityFieldClientRuleStatementClearValue());
            } else {
                return ((EntityFieldClientRuleStatementClearValue) _storedValue);
            }
        }

        public EntityFieldClientRuleStatementClearValue.Builder<_B> copyOf(final EntityFieldClientRuleStatementClearValue _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityFieldClientRuleStatementClearValue.Builder<_B> copyOf(final EntityFieldClientRuleStatementClearValue.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityFieldClientRuleStatementClearValue.Selector<EntityFieldClientRuleStatementClearValue.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityFieldClientRuleStatementClearValue.Select _root() {
            return new EntityFieldClientRuleStatementClearValue.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends EntityFieldClientRuleStatement.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatementClearValue.Selector<TRoot, TParent>> targetEntityFieldId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.targetEntityFieldId!= null) {
                products.put("targetEntityFieldId", this.targetEntityFieldId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatementClearValue.Selector<TRoot, TParent>> targetEntityFieldId() {
            return ((this.targetEntityFieldId == null)?this.targetEntityFieldId = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatementClearValue.Selector<TRoot, TParent>>(this._root, this, "targetEntityFieldId"):this.targetEntityFieldId);
        }

    }

}
