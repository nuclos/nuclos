package org.nuclos.schema.meta.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Transfers a value from one field to another field.
 * 
 * <p>Java class for entity-field-client-rule-statement-transfer-lookedup-value complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-field-client-rule-statement-transfer-lookedup-value"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:org.nuclos.schema.meta.entity}entity-field-client-rule-statement"&gt;
 *       &lt;attribute name="sourceEntityFieldId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="targetEntityFieldId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-field-client-rule-statement-transfer-lookedup-value")
public class EntityFieldClientRuleStatementTransferLookedupValue
    extends EntityFieldClientRuleStatement
    implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "sourceEntityFieldId", required = true)
    protected String sourceEntityFieldId;
    @XmlAttribute(name = "targetEntityFieldId", required = true)
    protected String targetEntityFieldId;

    /**
     * Gets the value of the sourceEntityFieldId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceEntityFieldId() {
        return sourceEntityFieldId;
    }

    /**
     * Sets the value of the sourceEntityFieldId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceEntityFieldId(String value) {
        this.sourceEntityFieldId = value;
    }

    /**
     * Gets the value of the targetEntityFieldId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetEntityFieldId() {
        return targetEntityFieldId;
    }

    /**
     * Sets the value of the targetEntityFieldId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetEntityFieldId(String value) {
        this.targetEntityFieldId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        super.appendFields(locator, buffer, strategy);
        {
            String theSourceEntityFieldId;
            theSourceEntityFieldId = this.getSourceEntityFieldId();
            strategy.appendField(locator, this, "sourceEntityFieldId", buffer, theSourceEntityFieldId);
        }
        {
            String theTargetEntityFieldId;
            theTargetEntityFieldId = this.getTargetEntityFieldId();
            strategy.appendField(locator, this, "targetEntityFieldId", buffer, theTargetEntityFieldId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityFieldClientRuleStatementTransferLookedupValue)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (!super.equals(thisLocator, thatLocator, object, strategy)) {
            return false;
        }
        final EntityFieldClientRuleStatementTransferLookedupValue that = ((EntityFieldClientRuleStatementTransferLookedupValue) object);
        {
            String lhsSourceEntityFieldId;
            lhsSourceEntityFieldId = this.getSourceEntityFieldId();
            String rhsSourceEntityFieldId;
            rhsSourceEntityFieldId = that.getSourceEntityFieldId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sourceEntityFieldId", lhsSourceEntityFieldId), LocatorUtils.property(thatLocator, "sourceEntityFieldId", rhsSourceEntityFieldId), lhsSourceEntityFieldId, rhsSourceEntityFieldId)) {
                return false;
            }
        }
        {
            String lhsTargetEntityFieldId;
            lhsTargetEntityFieldId = this.getTargetEntityFieldId();
            String rhsTargetEntityFieldId;
            rhsTargetEntityFieldId = that.getTargetEntityFieldId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "targetEntityFieldId", lhsTargetEntityFieldId), LocatorUtils.property(thatLocator, "targetEntityFieldId", rhsTargetEntityFieldId), lhsTargetEntityFieldId, rhsTargetEntityFieldId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = super.hashCode(locator, strategy);
        {
            String theSourceEntityFieldId;
            theSourceEntityFieldId = this.getSourceEntityFieldId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "sourceEntityFieldId", theSourceEntityFieldId), currentHashCode, theSourceEntityFieldId);
        }
        {
            String theTargetEntityFieldId;
            theTargetEntityFieldId = this.getTargetEntityFieldId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "targetEntityFieldId", theTargetEntityFieldId), currentHashCode, theTargetEntityFieldId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        super.copyTo(locator, draftCopy, strategy);
        if (draftCopy instanceof EntityFieldClientRuleStatementTransferLookedupValue) {
            final EntityFieldClientRuleStatementTransferLookedupValue copy = ((EntityFieldClientRuleStatementTransferLookedupValue) draftCopy);
            if (this.sourceEntityFieldId!= null) {
                String sourceSourceEntityFieldId;
                sourceSourceEntityFieldId = this.getSourceEntityFieldId();
                String copySourceEntityFieldId = ((String) strategy.copy(LocatorUtils.property(locator, "sourceEntityFieldId", sourceSourceEntityFieldId), sourceSourceEntityFieldId));
                copy.setSourceEntityFieldId(copySourceEntityFieldId);
            } else {
                copy.sourceEntityFieldId = null;
            }
            if (this.targetEntityFieldId!= null) {
                String sourceTargetEntityFieldId;
                sourceTargetEntityFieldId = this.getTargetEntityFieldId();
                String copyTargetEntityFieldId = ((String) strategy.copy(LocatorUtils.property(locator, "targetEntityFieldId", sourceTargetEntityFieldId), sourceTargetEntityFieldId));
                copy.setTargetEntityFieldId(copyTargetEntityFieldId);
            } else {
                copy.targetEntityFieldId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityFieldClientRuleStatementTransferLookedupValue();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> _other) {
        super.copyTo(_other);
        _other.sourceEntityFieldId = this.sourceEntityFieldId;
        _other.targetEntityFieldId = this.targetEntityFieldId;
    }

    @Override
    public<_B >EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B>(_parentBuilder, this, true);
    }

    @Override
    public EntityFieldClientRuleStatementTransferLookedupValue.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityFieldClientRuleStatementTransferLookedupValue.Builder<Void> builder() {
        return new EntityFieldClientRuleStatementTransferLookedupValue.Builder<Void>(null, null, false);
    }

    public static<_B >EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> copyOf(final EntityFieldClientRuleStatement _other) {
        final EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> _newBuilder = new EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    public static<_B >EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> copyOf(final EntityFieldClientRuleStatementTransferLookedupValue _other) {
        final EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> _newBuilder = new EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        super.copyTo(_other, _propertyTree, _propertyTreeUse);
        final PropertyTree sourceEntityFieldIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sourceEntityFieldId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sourceEntityFieldIdPropertyTree!= null):((sourceEntityFieldIdPropertyTree == null)||(!sourceEntityFieldIdPropertyTree.isLeaf())))) {
            _other.sourceEntityFieldId = this.sourceEntityFieldId;
        }
        final PropertyTree targetEntityFieldIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetEntityFieldId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetEntityFieldIdPropertyTree!= null):((targetEntityFieldIdPropertyTree == null)||(!targetEntityFieldIdPropertyTree.isLeaf())))) {
            _other.targetEntityFieldId = this.targetEntityFieldId;
        }
    }

    @Override
    public<_B >EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    @Override
    public EntityFieldClientRuleStatementTransferLookedupValue.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> copyOf(final EntityFieldClientRuleStatement _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> _newBuilder = new EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static<_B >EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> copyOf(final EntityFieldClientRuleStatementTransferLookedupValue _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> _newBuilder = new EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityFieldClientRuleStatementTransferLookedupValue.Builder<Void> copyExcept(final EntityFieldClientRuleStatement _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityFieldClientRuleStatementTransferLookedupValue.Builder<Void> copyExcept(final EntityFieldClientRuleStatementTransferLookedupValue _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityFieldClientRuleStatementTransferLookedupValue.Builder<Void> copyOnly(final EntityFieldClientRuleStatement _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static EntityFieldClientRuleStatementTransferLookedupValue.Builder<Void> copyOnly(final EntityFieldClientRuleStatementTransferLookedupValue _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >
        extends EntityFieldClientRuleStatement.Builder<_B>
        implements Buildable
    {

        private String sourceEntityFieldId;
        private String targetEntityFieldId;

        public Builder(final _B _parentBuilder, final EntityFieldClientRuleStatementTransferLookedupValue _other, final boolean _copy) {
            super(_parentBuilder, _other, _copy);
            if (_other!= null) {
                this.sourceEntityFieldId = _other.sourceEntityFieldId;
                this.targetEntityFieldId = _other.targetEntityFieldId;
            }
        }

        public Builder(final _B _parentBuilder, final EntityFieldClientRuleStatementTransferLookedupValue _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            super(_parentBuilder, _other, _copy, _propertyTree, _propertyTreeUse);
            if (_other!= null) {
                final PropertyTree sourceEntityFieldIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sourceEntityFieldId"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sourceEntityFieldIdPropertyTree!= null):((sourceEntityFieldIdPropertyTree == null)||(!sourceEntityFieldIdPropertyTree.isLeaf())))) {
                    this.sourceEntityFieldId = _other.sourceEntityFieldId;
                }
                final PropertyTree targetEntityFieldIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("targetEntityFieldId"));
                if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(targetEntityFieldIdPropertyTree!= null):((targetEntityFieldIdPropertyTree == null)||(!targetEntityFieldIdPropertyTree.isLeaf())))) {
                    this.targetEntityFieldId = _other.targetEntityFieldId;
                }
            }
        }

        protected<_P extends EntityFieldClientRuleStatementTransferLookedupValue >_P init(final _P _product) {
            _product.sourceEntityFieldId = this.sourceEntityFieldId;
            _product.targetEntityFieldId = this.targetEntityFieldId;
            return super.init(_product);
        }

        /**
         * Sets the new value of "sourceEntityFieldId" (any previous value will be replaced)
         * 
         * @param sourceEntityFieldId
         *     New value of the "sourceEntityFieldId" property.
         */
        public EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> withSourceEntityFieldId(final String sourceEntityFieldId) {
            this.sourceEntityFieldId = sourceEntityFieldId;
            return this;
        }

        /**
         * Sets the new value of "targetEntityFieldId" (any previous value will be replaced)
         * 
         * @param targetEntityFieldId
         *     New value of the "targetEntityFieldId" property.
         */
        public EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> withTargetEntityFieldId(final String targetEntityFieldId) {
            this.targetEntityFieldId = targetEntityFieldId;
            return this;
        }

        /**
         * Sets the new value of "order" (any previous value will be replaced)
         * 
         * @param order
         *     New value of the "order" property.
         */
        @Override
        public EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> withOrder(final BigInteger order) {
            super.withOrder(order);
            return this;
        }

        @Override
        public EntityFieldClientRuleStatementTransferLookedupValue build() {
            if (_storedValue == null) {
                return this.init(new EntityFieldClientRuleStatementTransferLookedupValue());
            } else {
                return ((EntityFieldClientRuleStatementTransferLookedupValue) _storedValue);
            }
        }

        public EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> copyOf(final EntityFieldClientRuleStatementTransferLookedupValue _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityFieldClientRuleStatementTransferLookedupValue.Builder<_B> copyOf(final EntityFieldClientRuleStatementTransferLookedupValue.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityFieldClientRuleStatementTransferLookedupValue.Selector<EntityFieldClientRuleStatementTransferLookedupValue.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityFieldClientRuleStatementTransferLookedupValue.Select _root() {
            return new EntityFieldClientRuleStatementTransferLookedupValue.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends EntityFieldClientRuleStatement.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatementTransferLookedupValue.Selector<TRoot, TParent>> sourceEntityFieldId = null;
        private com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatementTransferLookedupValue.Selector<TRoot, TParent>> targetEntityFieldId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.sourceEntityFieldId!= null) {
                products.put("sourceEntityFieldId", this.sourceEntityFieldId.init());
            }
            if (this.targetEntityFieldId!= null) {
                products.put("targetEntityFieldId", this.targetEntityFieldId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatementTransferLookedupValue.Selector<TRoot, TParent>> sourceEntityFieldId() {
            return ((this.sourceEntityFieldId == null)?this.sourceEntityFieldId = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatementTransferLookedupValue.Selector<TRoot, TParent>>(this._root, this, "sourceEntityFieldId"):this.sourceEntityFieldId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatementTransferLookedupValue.Selector<TRoot, TParent>> targetEntityFieldId() {
            return ((this.targetEntityFieldId == null)?this.targetEntityFieldId = new com.kscs.util.jaxb.Selector<TRoot, EntityFieldClientRuleStatementTransferLookedupValue.Selector<TRoot, TParent>>(this._root, this, "targetEntityFieldId"):this.targetEntityFieldId);
        }

    }

}
