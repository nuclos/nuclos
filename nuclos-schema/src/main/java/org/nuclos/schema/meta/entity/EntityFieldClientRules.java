package org.nuclos.schema.meta.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for entity-field-client-rules complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entity-field-client-rules"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rules" type="{urn:org.nuclos.schema.meta.entity}entity-field-client-rule" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entity-field-client-rules", propOrder = {
    "rules"
})
public class EntityFieldClientRules implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<EntityFieldClientRule> rules;

    /**
     * Gets the value of the rules property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rules property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRules().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntityFieldClientRule }
     * 
     * 
     */
    public List<EntityFieldClientRule> getRules() {
        if (rules == null) {
            rules = new ArrayList<EntityFieldClientRule>();
        }
        return this.rules;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<EntityFieldClientRule> theRules;
            theRules = (((this.rules!= null)&&(!this.rules.isEmpty()))?this.getRules():null);
            strategy.appendField(locator, this, "rules", buffer, theRules);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EntityFieldClientRules)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EntityFieldClientRules that = ((EntityFieldClientRules) object);
        {
            List<EntityFieldClientRule> lhsRules;
            lhsRules = (((this.rules!= null)&&(!this.rules.isEmpty()))?this.getRules():null);
            List<EntityFieldClientRule> rhsRules;
            rhsRules = (((that.rules!= null)&&(!that.rules.isEmpty()))?that.getRules():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rules", lhsRules), LocatorUtils.property(thatLocator, "rules", rhsRules), lhsRules, rhsRules)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<EntityFieldClientRule> theRules;
            theRules = (((this.rules!= null)&&(!this.rules.isEmpty()))?this.getRules():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rules", theRules), currentHashCode, theRules);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EntityFieldClientRules) {
            final EntityFieldClientRules copy = ((EntityFieldClientRules) draftCopy);
            if ((this.rules!= null)&&(!this.rules.isEmpty())) {
                List<EntityFieldClientRule> sourceRules;
                sourceRules = (((this.rules!= null)&&(!this.rules.isEmpty()))?this.getRules():null);
                @SuppressWarnings("unchecked")
                List<EntityFieldClientRule> copyRules = ((List<EntityFieldClientRule> ) strategy.copy(LocatorUtils.property(locator, "rules", sourceRules), sourceRules));
                copy.rules = null;
                if (copyRules!= null) {
                    List<EntityFieldClientRule> uniqueRulesl = copy.getRules();
                    uniqueRulesl.addAll(copyRules);
                }
            } else {
                copy.rules = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EntityFieldClientRules();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldClientRules.Builder<_B> _other) {
        if (this.rules == null) {
            _other.rules = null;
        } else {
            _other.rules = new ArrayList<EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>>>();
            for (EntityFieldClientRule _item: this.rules) {
                _other.rules.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >EntityFieldClientRules.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EntityFieldClientRules.Builder<_B>(_parentBuilder, this, true);
    }

    public EntityFieldClientRules.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EntityFieldClientRules.Builder<Void> builder() {
        return new EntityFieldClientRules.Builder<Void>(null, null, false);
    }

    public static<_B >EntityFieldClientRules.Builder<_B> copyOf(final EntityFieldClientRules _other) {
        final EntityFieldClientRules.Builder<_B> _newBuilder = new EntityFieldClientRules.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EntityFieldClientRules.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree rulesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rules"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rulesPropertyTree!= null):((rulesPropertyTree == null)||(!rulesPropertyTree.isLeaf())))) {
            if (this.rules == null) {
                _other.rules = null;
            } else {
                _other.rules = new ArrayList<EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>>>();
                for (EntityFieldClientRule _item: this.rules) {
                    _other.rules.add(((_item == null)?null:_item.newCopyBuilder(_other, rulesPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >EntityFieldClientRules.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EntityFieldClientRules.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EntityFieldClientRules.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EntityFieldClientRules.Builder<_B> copyOf(final EntityFieldClientRules _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EntityFieldClientRules.Builder<_B> _newBuilder = new EntityFieldClientRules.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EntityFieldClientRules.Builder<Void> copyExcept(final EntityFieldClientRules _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EntityFieldClientRules.Builder<Void> copyOnly(final EntityFieldClientRules _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EntityFieldClientRules _storedValue;
        private List<EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>>> rules;

        public Builder(final _B _parentBuilder, final EntityFieldClientRules _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.rules == null) {
                        this.rules = null;
                    } else {
                        this.rules = new ArrayList<EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>>>();
                        for (EntityFieldClientRule _item: _other.rules) {
                            this.rules.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EntityFieldClientRules _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree rulesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rules"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rulesPropertyTree!= null):((rulesPropertyTree == null)||(!rulesPropertyTree.isLeaf())))) {
                        if (_other.rules == null) {
                            this.rules = null;
                        } else {
                            this.rules = new ArrayList<EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>>>();
                            for (EntityFieldClientRule _item: _other.rules) {
                                this.rules.add(((_item == null)?null:_item.newCopyBuilder(this, rulesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EntityFieldClientRules >_P init(final _P _product) {
            if (this.rules!= null) {
                final List<EntityFieldClientRule> rules = new ArrayList<EntityFieldClientRule>(this.rules.size());
                for (EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>> _item: this.rules) {
                    rules.add(_item.build());
                }
                _product.rules = rules;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "rules"
         * 
         * @param rules
         *     Items to add to the value of the "rules" property
         */
        public EntityFieldClientRules.Builder<_B> addRules(final Iterable<? extends EntityFieldClientRule> rules) {
            if (rules!= null) {
                if (this.rules == null) {
                    this.rules = new ArrayList<EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>>>();
                }
                for (EntityFieldClientRule _item: rules) {
                    this.rules.add(new EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "rules" (any previous value will be replaced)
         * 
         * @param rules
         *     New value of the "rules" property.
         */
        public EntityFieldClientRules.Builder<_B> withRules(final Iterable<? extends EntityFieldClientRule> rules) {
            if (this.rules!= null) {
                this.rules.clear();
            }
            return addRules(rules);
        }

        /**
         * Adds the given items to the value of "rules"
         * 
         * @param rules
         *     Items to add to the value of the "rules" property
         */
        public EntityFieldClientRules.Builder<_B> addRules(EntityFieldClientRule... rules) {
            addRules(Arrays.asList(rules));
            return this;
        }

        /**
         * Sets the new value of "rules" (any previous value will be replaced)
         * 
         * @param rules
         *     New value of the "rules" property.
         */
        public EntityFieldClientRules.Builder<_B> withRules(EntityFieldClientRule... rules) {
            withRules(Arrays.asList(rules));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Rules" property.
         * Use {@link org.nuclos.schema.meta.entity.EntityFieldClientRule.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Rules" property.
         *     Use {@link org.nuclos.schema.meta.entity.EntityFieldClientRule.Builder#end()} to return to the current builder.
         */
        public EntityFieldClientRule.Builder<? extends EntityFieldClientRules.Builder<_B>> addRules() {
            if (this.rules == null) {
                this.rules = new ArrayList<EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>>>();
            }
            final EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>> rules_Builder = new EntityFieldClientRule.Builder<EntityFieldClientRules.Builder<_B>>(this, null, false);
            this.rules.add(rules_Builder);
            return rules_Builder;
        }

        @Override
        public EntityFieldClientRules build() {
            if (_storedValue == null) {
                return this.init(new EntityFieldClientRules());
            } else {
                return ((EntityFieldClientRules) _storedValue);
            }
        }

        public EntityFieldClientRules.Builder<_B> copyOf(final EntityFieldClientRules _other) {
            _other.copyTo(this);
            return this;
        }

        public EntityFieldClientRules.Builder<_B> copyOf(final EntityFieldClientRules.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EntityFieldClientRules.Selector<EntityFieldClientRules.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EntityFieldClientRules.Select _root() {
            return new EntityFieldClientRules.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private EntityFieldClientRule.Selector<TRoot, EntityFieldClientRules.Selector<TRoot, TParent>> rules = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.rules!= null) {
                products.put("rules", this.rules.init());
            }
            return products;
        }

        public EntityFieldClientRule.Selector<TRoot, EntityFieldClientRules.Selector<TRoot, TParent>> rules() {
            return ((this.rules == null)?this.rules = new EntityFieldClientRule.Selector<TRoot, EntityFieldClientRules.Selector<TRoot, TParent>>(this._root, this, "rules"):this.rules);
        }

    }

}
