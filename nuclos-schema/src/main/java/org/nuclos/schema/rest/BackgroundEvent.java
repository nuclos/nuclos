package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for background-event complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="background-event"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="eventEntity" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="eventName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="eventDate" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="eventTip" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "background-event")
public class BackgroundEvent implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "eventEntity", required = true)
    protected String eventEntity;
    @XmlAttribute(name = "eventName", required = true)
    protected String eventName;
    @XmlAttribute(name = "eventDate", required = true)
    protected String eventDate;
    @XmlAttribute(name = "eventTip", required = true)
    protected String eventTip;

    /**
     * Gets the value of the eventEntity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventEntity() {
        return eventEntity;
    }

    /**
     * Sets the value of the eventEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventEntity(String value) {
        this.eventEntity = value;
    }

    /**
     * Gets the value of the eventName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Sets the value of the eventName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

    /**
     * Gets the value of the eventDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventDate() {
        return eventDate;
    }

    /**
     * Sets the value of the eventDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventDate(String value) {
        this.eventDate = value;
    }

    /**
     * Gets the value of the eventTip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventTip() {
        return eventTip;
    }

    /**
     * Sets the value of the eventTip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventTip(String value) {
        this.eventTip = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theEventEntity;
            theEventEntity = this.getEventEntity();
            strategy.appendField(locator, this, "eventEntity", buffer, theEventEntity);
        }
        {
            String theEventName;
            theEventName = this.getEventName();
            strategy.appendField(locator, this, "eventName", buffer, theEventName);
        }
        {
            String theEventDate;
            theEventDate = this.getEventDate();
            strategy.appendField(locator, this, "eventDate", buffer, theEventDate);
        }
        {
            String theEventTip;
            theEventTip = this.getEventTip();
            strategy.appendField(locator, this, "eventTip", buffer, theEventTip);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BackgroundEvent)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final BackgroundEvent that = ((BackgroundEvent) object);
        {
            String lhsEventEntity;
            lhsEventEntity = this.getEventEntity();
            String rhsEventEntity;
            rhsEventEntity = that.getEventEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventEntity", lhsEventEntity), LocatorUtils.property(thatLocator, "eventEntity", rhsEventEntity), lhsEventEntity, rhsEventEntity)) {
                return false;
            }
        }
        {
            String lhsEventName;
            lhsEventName = this.getEventName();
            String rhsEventName;
            rhsEventName = that.getEventName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventName", lhsEventName), LocatorUtils.property(thatLocator, "eventName", rhsEventName), lhsEventName, rhsEventName)) {
                return false;
            }
        }
        {
            String lhsEventDate;
            lhsEventDate = this.getEventDate();
            String rhsEventDate;
            rhsEventDate = that.getEventDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventDate", lhsEventDate), LocatorUtils.property(thatLocator, "eventDate", rhsEventDate), lhsEventDate, rhsEventDate)) {
                return false;
            }
        }
        {
            String lhsEventTip;
            lhsEventTip = this.getEventTip();
            String rhsEventTip;
            rhsEventTip = that.getEventTip();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventTip", lhsEventTip), LocatorUtils.property(thatLocator, "eventTip", rhsEventTip), lhsEventTip, rhsEventTip)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEventEntity;
            theEventEntity = this.getEventEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "eventEntity", theEventEntity), currentHashCode, theEventEntity);
        }
        {
            String theEventName;
            theEventName = this.getEventName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "eventName", theEventName), currentHashCode, theEventName);
        }
        {
            String theEventDate;
            theEventDate = this.getEventDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "eventDate", theEventDate), currentHashCode, theEventDate);
        }
        {
            String theEventTip;
            theEventTip = this.getEventTip();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "eventTip", theEventTip), currentHashCode, theEventTip);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof BackgroundEvent) {
            final BackgroundEvent copy = ((BackgroundEvent) draftCopy);
            if (this.eventEntity!= null) {
                String sourceEventEntity;
                sourceEventEntity = this.getEventEntity();
                String copyEventEntity = ((String) strategy.copy(LocatorUtils.property(locator, "eventEntity", sourceEventEntity), sourceEventEntity));
                copy.setEventEntity(copyEventEntity);
            } else {
                copy.eventEntity = null;
            }
            if (this.eventName!= null) {
                String sourceEventName;
                sourceEventName = this.getEventName();
                String copyEventName = ((String) strategy.copy(LocatorUtils.property(locator, "eventName", sourceEventName), sourceEventName));
                copy.setEventName(copyEventName);
            } else {
                copy.eventName = null;
            }
            if (this.eventDate!= null) {
                String sourceEventDate;
                sourceEventDate = this.getEventDate();
                String copyEventDate = ((String) strategy.copy(LocatorUtils.property(locator, "eventDate", sourceEventDate), sourceEventDate));
                copy.setEventDate(copyEventDate);
            } else {
                copy.eventDate = null;
            }
            if (this.eventTip!= null) {
                String sourceEventTip;
                sourceEventTip = this.getEventTip();
                String copyEventTip = ((String) strategy.copy(LocatorUtils.property(locator, "eventTip", sourceEventTip), sourceEventTip));
                copy.setEventTip(copyEventTip);
            } else {
                copy.eventTip = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BackgroundEvent();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final BackgroundEvent.Builder<_B> _other) {
        _other.eventEntity = this.eventEntity;
        _other.eventName = this.eventName;
        _other.eventDate = this.eventDate;
        _other.eventTip = this.eventTip;
    }

    public<_B >BackgroundEvent.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new BackgroundEvent.Builder<_B>(_parentBuilder, this, true);
    }

    public BackgroundEvent.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static BackgroundEvent.Builder<Void> builder() {
        return new BackgroundEvent.Builder<Void>(null, null, false);
    }

    public static<_B >BackgroundEvent.Builder<_B> copyOf(final BackgroundEvent _other) {
        final BackgroundEvent.Builder<_B> _newBuilder = new BackgroundEvent.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final BackgroundEvent.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree eventEntityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("eventEntity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventEntityPropertyTree!= null):((eventEntityPropertyTree == null)||(!eventEntityPropertyTree.isLeaf())))) {
            _other.eventEntity = this.eventEntity;
        }
        final PropertyTree eventNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("eventName"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventNamePropertyTree!= null):((eventNamePropertyTree == null)||(!eventNamePropertyTree.isLeaf())))) {
            _other.eventName = this.eventName;
        }
        final PropertyTree eventDatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("eventDate"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventDatePropertyTree!= null):((eventDatePropertyTree == null)||(!eventDatePropertyTree.isLeaf())))) {
            _other.eventDate = this.eventDate;
        }
        final PropertyTree eventTipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("eventTip"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventTipPropertyTree!= null):((eventTipPropertyTree == null)||(!eventTipPropertyTree.isLeaf())))) {
            _other.eventTip = this.eventTip;
        }
    }

    public<_B >BackgroundEvent.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new BackgroundEvent.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public BackgroundEvent.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >BackgroundEvent.Builder<_B> copyOf(final BackgroundEvent _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final BackgroundEvent.Builder<_B> _newBuilder = new BackgroundEvent.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static BackgroundEvent.Builder<Void> copyExcept(final BackgroundEvent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static BackgroundEvent.Builder<Void> copyOnly(final BackgroundEvent _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final BackgroundEvent _storedValue;
        private String eventEntity;
        private String eventName;
        private String eventDate;
        private String eventTip;

        public Builder(final _B _parentBuilder, final BackgroundEvent _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.eventEntity = _other.eventEntity;
                    this.eventName = _other.eventName;
                    this.eventDate = _other.eventDate;
                    this.eventTip = _other.eventTip;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final BackgroundEvent _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree eventEntityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("eventEntity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventEntityPropertyTree!= null):((eventEntityPropertyTree == null)||(!eventEntityPropertyTree.isLeaf())))) {
                        this.eventEntity = _other.eventEntity;
                    }
                    final PropertyTree eventNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("eventName"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventNamePropertyTree!= null):((eventNamePropertyTree == null)||(!eventNamePropertyTree.isLeaf())))) {
                        this.eventName = _other.eventName;
                    }
                    final PropertyTree eventDatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("eventDate"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventDatePropertyTree!= null):((eventDatePropertyTree == null)||(!eventDatePropertyTree.isLeaf())))) {
                        this.eventDate = _other.eventDate;
                    }
                    final PropertyTree eventTipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("eventTip"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(eventTipPropertyTree!= null):((eventTipPropertyTree == null)||(!eventTipPropertyTree.isLeaf())))) {
                        this.eventTip = _other.eventTip;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends BackgroundEvent >_P init(final _P _product) {
            _product.eventEntity = this.eventEntity;
            _product.eventName = this.eventName;
            _product.eventDate = this.eventDate;
            _product.eventTip = this.eventTip;
            return _product;
        }

        /**
         * Sets the new value of "eventEntity" (any previous value will be replaced)
         * 
         * @param eventEntity
         *     New value of the "eventEntity" property.
         */
        public BackgroundEvent.Builder<_B> withEventEntity(final String eventEntity) {
            this.eventEntity = eventEntity;
            return this;
        }

        /**
         * Sets the new value of "eventName" (any previous value will be replaced)
         * 
         * @param eventName
         *     New value of the "eventName" property.
         */
        public BackgroundEvent.Builder<_B> withEventName(final String eventName) {
            this.eventName = eventName;
            return this;
        }

        /**
         * Sets the new value of "eventDate" (any previous value will be replaced)
         * 
         * @param eventDate
         *     New value of the "eventDate" property.
         */
        public BackgroundEvent.Builder<_B> withEventDate(final String eventDate) {
            this.eventDate = eventDate;
            return this;
        }

        /**
         * Sets the new value of "eventTip" (any previous value will be replaced)
         * 
         * @param eventTip
         *     New value of the "eventTip" property.
         */
        public BackgroundEvent.Builder<_B> withEventTip(final String eventTip) {
            this.eventTip = eventTip;
            return this;
        }

        @Override
        public BackgroundEvent build() {
            if (_storedValue == null) {
                return this.init(new BackgroundEvent());
            } else {
                return ((BackgroundEvent) _storedValue);
            }
        }

        public BackgroundEvent.Builder<_B> copyOf(final BackgroundEvent _other) {
            _other.copyTo(this);
            return this;
        }

        public BackgroundEvent.Builder<_B> copyOf(final BackgroundEvent.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends BackgroundEvent.Selector<BackgroundEvent.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static BackgroundEvent.Select _root() {
            return new BackgroundEvent.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>> eventEntity = null;
        private com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>> eventName = null;
        private com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>> eventDate = null;
        private com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>> eventTip = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.eventEntity!= null) {
                products.put("eventEntity", this.eventEntity.init());
            }
            if (this.eventName!= null) {
                products.put("eventName", this.eventName.init());
            }
            if (this.eventDate!= null) {
                products.put("eventDate", this.eventDate.init());
            }
            if (this.eventTip!= null) {
                products.put("eventTip", this.eventTip.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>> eventEntity() {
            return ((this.eventEntity == null)?this.eventEntity = new com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>>(this._root, this, "eventEntity"):this.eventEntity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>> eventName() {
            return ((this.eventName == null)?this.eventName = new com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>>(this._root, this, "eventName"):this.eventName);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>> eventDate() {
            return ((this.eventDate == null)?this.eventDate = new com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>>(this._root, this, "eventDate"):this.eventDate);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>> eventTip() {
            return ((this.eventTip == null)?this.eventTip = new com.kscs.util.jaxb.Selector<TRoot, BackgroundEvent.Selector<TRoot, TParent>>(this._root, this, "eventTip"):this.eventTip);
        }

    }

}
