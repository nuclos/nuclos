package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for booking complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="booking"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="resource" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="booker" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entityMeta" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="fromTime" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="untilTime" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="fromDate" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="untilDate" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="toolTip" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="backgroundColor" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "booking")
public class Booking implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "resource", required = true)
    protected String resource;
    @XmlAttribute(name = "booker", required = true)
    protected String booker;
    @XmlAttribute(name = "entityMeta", required = true)
    protected String entityMeta;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "fromTime", required = true)
    protected String fromTime;
    @XmlAttribute(name = "untilTime", required = true)
    protected String untilTime;
    @XmlAttribute(name = "fromDate", required = true)
    protected String fromDate;
    @XmlAttribute(name = "untilDate", required = true)
    protected String untilDate;
    @XmlAttribute(name = "label", required = true)
    protected String label;
    @XmlAttribute(name = "toolTip", required = true)
    protected String toolTip;
    @XmlAttribute(name = "backgroundColor", required = true)
    protected String backgroundColor;

    /**
     * Gets the value of the resource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResource() {
        return resource;
    }

    /**
     * Sets the value of the resource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResource(String value) {
        this.resource = value;
    }

    /**
     * Gets the value of the booker property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBooker() {
        return booker;
    }

    /**
     * Sets the value of the booker property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBooker(String value) {
        this.booker = value;
    }

    /**
     * Gets the value of the entityMeta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMeta() {
        return entityMeta;
    }

    /**
     * Sets the value of the entityMeta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMeta(String value) {
        this.entityMeta = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the fromTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromTime() {
        return fromTime;
    }

    /**
     * Sets the value of the fromTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromTime(String value) {
        this.fromTime = value;
    }

    /**
     * Gets the value of the untilTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUntilTime() {
        return untilTime;
    }

    /**
     * Sets the value of the untilTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUntilTime(String value) {
        this.untilTime = value;
    }

    /**
     * Gets the value of the fromDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromDate() {
        return fromDate;
    }

    /**
     * Sets the value of the fromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromDate(String value) {
        this.fromDate = value;
    }

    /**
     * Gets the value of the untilDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUntilDate() {
        return untilDate;
    }

    /**
     * Sets the value of the untilDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUntilDate(String value) {
        this.untilDate = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the toolTip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToolTip() {
        return toolTip;
    }

    /**
     * Sets the value of the toolTip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToolTip(String value) {
        this.toolTip = value;
    }

    /**
     * Gets the value of the backgroundColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * Sets the value of the backgroundColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBackgroundColor(String value) {
        this.backgroundColor = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theResource;
            theResource = this.getResource();
            strategy.appendField(locator, this, "resource", buffer, theResource);
        }
        {
            String theBooker;
            theBooker = this.getBooker();
            strategy.appendField(locator, this, "booker", buffer, theBooker);
        }
        {
            String theEntityMeta;
            theEntityMeta = this.getEntityMeta();
            strategy.appendField(locator, this, "entityMeta", buffer, theEntityMeta);
        }
        {
            String theId;
            theId = this.getId();
            strategy.appendField(locator, this, "id", buffer, theId);
        }
        {
            String theFromTime;
            theFromTime = this.getFromTime();
            strategy.appendField(locator, this, "fromTime", buffer, theFromTime);
        }
        {
            String theUntilTime;
            theUntilTime = this.getUntilTime();
            strategy.appendField(locator, this, "untilTime", buffer, theUntilTime);
        }
        {
            String theFromDate;
            theFromDate = this.getFromDate();
            strategy.appendField(locator, this, "fromDate", buffer, theFromDate);
        }
        {
            String theUntilDate;
            theUntilDate = this.getUntilDate();
            strategy.appendField(locator, this, "untilDate", buffer, theUntilDate);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            String theToolTip;
            theToolTip = this.getToolTip();
            strategy.appendField(locator, this, "toolTip", buffer, theToolTip);
        }
        {
            String theBackgroundColor;
            theBackgroundColor = this.getBackgroundColor();
            strategy.appendField(locator, this, "backgroundColor", buffer, theBackgroundColor);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Booking)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Booking that = ((Booking) object);
        {
            String lhsResource;
            lhsResource = this.getResource();
            String rhsResource;
            rhsResource = that.getResource();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resource", lhsResource), LocatorUtils.property(thatLocator, "resource", rhsResource), lhsResource, rhsResource)) {
                return false;
            }
        }
        {
            String lhsBooker;
            lhsBooker = this.getBooker();
            String rhsBooker;
            rhsBooker = that.getBooker();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "booker", lhsBooker), LocatorUtils.property(thatLocator, "booker", rhsBooker), lhsBooker, rhsBooker)) {
                return false;
            }
        }
        {
            String lhsEntityMeta;
            lhsEntityMeta = this.getEntityMeta();
            String rhsEntityMeta;
            rhsEntityMeta = that.getEntityMeta();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMeta", lhsEntityMeta), LocatorUtils.property(thatLocator, "entityMeta", rhsEntityMeta), lhsEntityMeta, rhsEntityMeta)) {
                return false;
            }
        }
        {
            String lhsId;
            lhsId = this.getId();
            String rhsId;
            rhsId = that.getId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
                return false;
            }
        }
        {
            String lhsFromTime;
            lhsFromTime = this.getFromTime();
            String rhsFromTime;
            rhsFromTime = that.getFromTime();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fromTime", lhsFromTime), LocatorUtils.property(thatLocator, "fromTime", rhsFromTime), lhsFromTime, rhsFromTime)) {
                return false;
            }
        }
        {
            String lhsUntilTime;
            lhsUntilTime = this.getUntilTime();
            String rhsUntilTime;
            rhsUntilTime = that.getUntilTime();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "untilTime", lhsUntilTime), LocatorUtils.property(thatLocator, "untilTime", rhsUntilTime), lhsUntilTime, rhsUntilTime)) {
                return false;
            }
        }
        {
            String lhsFromDate;
            lhsFromDate = this.getFromDate();
            String rhsFromDate;
            rhsFromDate = that.getFromDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fromDate", lhsFromDate), LocatorUtils.property(thatLocator, "fromDate", rhsFromDate), lhsFromDate, rhsFromDate)) {
                return false;
            }
        }
        {
            String lhsUntilDate;
            lhsUntilDate = this.getUntilDate();
            String rhsUntilDate;
            rhsUntilDate = that.getUntilDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "untilDate", lhsUntilDate), LocatorUtils.property(thatLocator, "untilDate", rhsUntilDate), lhsUntilDate, rhsUntilDate)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            String lhsToolTip;
            lhsToolTip = this.getToolTip();
            String rhsToolTip;
            rhsToolTip = that.getToolTip();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "toolTip", lhsToolTip), LocatorUtils.property(thatLocator, "toolTip", rhsToolTip), lhsToolTip, rhsToolTip)) {
                return false;
            }
        }
        {
            String lhsBackgroundColor;
            lhsBackgroundColor = this.getBackgroundColor();
            String rhsBackgroundColor;
            rhsBackgroundColor = that.getBackgroundColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "backgroundColor", lhsBackgroundColor), LocatorUtils.property(thatLocator, "backgroundColor", rhsBackgroundColor), lhsBackgroundColor, rhsBackgroundColor)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theResource;
            theResource = this.getResource();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resource", theResource), currentHashCode, theResource);
        }
        {
            String theBooker;
            theBooker = this.getBooker();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "booker", theBooker), currentHashCode, theBooker);
        }
        {
            String theEntityMeta;
            theEntityMeta = this.getEntityMeta();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMeta", theEntityMeta), currentHashCode, theEntityMeta);
        }
        {
            String theId;
            theId = this.getId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId);
        }
        {
            String theFromTime;
            theFromTime = this.getFromTime();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fromTime", theFromTime), currentHashCode, theFromTime);
        }
        {
            String theUntilTime;
            theUntilTime = this.getUntilTime();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "untilTime", theUntilTime), currentHashCode, theUntilTime);
        }
        {
            String theFromDate;
            theFromDate = this.getFromDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fromDate", theFromDate), currentHashCode, theFromDate);
        }
        {
            String theUntilDate;
            theUntilDate = this.getUntilDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "untilDate", theUntilDate), currentHashCode, theUntilDate);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            String theToolTip;
            theToolTip = this.getToolTip();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "toolTip", theToolTip), currentHashCode, theToolTip);
        }
        {
            String theBackgroundColor;
            theBackgroundColor = this.getBackgroundColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "backgroundColor", theBackgroundColor), currentHashCode, theBackgroundColor);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Booking) {
            final Booking copy = ((Booking) draftCopy);
            if (this.resource!= null) {
                String sourceResource;
                sourceResource = this.getResource();
                String copyResource = ((String) strategy.copy(LocatorUtils.property(locator, "resource", sourceResource), sourceResource));
                copy.setResource(copyResource);
            } else {
                copy.resource = null;
            }
            if (this.booker!= null) {
                String sourceBooker;
                sourceBooker = this.getBooker();
                String copyBooker = ((String) strategy.copy(LocatorUtils.property(locator, "booker", sourceBooker), sourceBooker));
                copy.setBooker(copyBooker);
            } else {
                copy.booker = null;
            }
            if (this.entityMeta!= null) {
                String sourceEntityMeta;
                sourceEntityMeta = this.getEntityMeta();
                String copyEntityMeta = ((String) strategy.copy(LocatorUtils.property(locator, "entityMeta", sourceEntityMeta), sourceEntityMeta));
                copy.setEntityMeta(copyEntityMeta);
            } else {
                copy.entityMeta = null;
            }
            if (this.id!= null) {
                String sourceId;
                sourceId = this.getId();
                String copyId = ((String) strategy.copy(LocatorUtils.property(locator, "id", sourceId), sourceId));
                copy.setId(copyId);
            } else {
                copy.id = null;
            }
            if (this.fromTime!= null) {
                String sourceFromTime;
                sourceFromTime = this.getFromTime();
                String copyFromTime = ((String) strategy.copy(LocatorUtils.property(locator, "fromTime", sourceFromTime), sourceFromTime));
                copy.setFromTime(copyFromTime);
            } else {
                copy.fromTime = null;
            }
            if (this.untilTime!= null) {
                String sourceUntilTime;
                sourceUntilTime = this.getUntilTime();
                String copyUntilTime = ((String) strategy.copy(LocatorUtils.property(locator, "untilTime", sourceUntilTime), sourceUntilTime));
                copy.setUntilTime(copyUntilTime);
            } else {
                copy.untilTime = null;
            }
            if (this.fromDate!= null) {
                String sourceFromDate;
                sourceFromDate = this.getFromDate();
                String copyFromDate = ((String) strategy.copy(LocatorUtils.property(locator, "fromDate", sourceFromDate), sourceFromDate));
                copy.setFromDate(copyFromDate);
            } else {
                copy.fromDate = null;
            }
            if (this.untilDate!= null) {
                String sourceUntilDate;
                sourceUntilDate = this.getUntilDate();
                String copyUntilDate = ((String) strategy.copy(LocatorUtils.property(locator, "untilDate", sourceUntilDate), sourceUntilDate));
                copy.setUntilDate(copyUntilDate);
            } else {
                copy.untilDate = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.toolTip!= null) {
                String sourceToolTip;
                sourceToolTip = this.getToolTip();
                String copyToolTip = ((String) strategy.copy(LocatorUtils.property(locator, "toolTip", sourceToolTip), sourceToolTip));
                copy.setToolTip(copyToolTip);
            } else {
                copy.toolTip = null;
            }
            if (this.backgroundColor!= null) {
                String sourceBackgroundColor;
                sourceBackgroundColor = this.getBackgroundColor();
                String copyBackgroundColor = ((String) strategy.copy(LocatorUtils.property(locator, "backgroundColor", sourceBackgroundColor), sourceBackgroundColor));
                copy.setBackgroundColor(copyBackgroundColor);
            } else {
                copy.backgroundColor = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Booking();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Booking.Builder<_B> _other) {
        _other.resource = this.resource;
        _other.booker = this.booker;
        _other.entityMeta = this.entityMeta;
        _other.id = this.id;
        _other.fromTime = this.fromTime;
        _other.untilTime = this.untilTime;
        _other.fromDate = this.fromDate;
        _other.untilDate = this.untilDate;
        _other.label = this.label;
        _other.toolTip = this.toolTip;
        _other.backgroundColor = this.backgroundColor;
    }

    public<_B >Booking.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Booking.Builder<_B>(_parentBuilder, this, true);
    }

    public Booking.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Booking.Builder<Void> builder() {
        return new Booking.Builder<Void>(null, null, false);
    }

    public static<_B >Booking.Builder<_B> copyOf(final Booking _other) {
        final Booking.Builder<_B> _newBuilder = new Booking.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Booking.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree resourcePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resource"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourcePropertyTree!= null):((resourcePropertyTree == null)||(!resourcePropertyTree.isLeaf())))) {
            _other.resource = this.resource;
        }
        final PropertyTree bookerPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("booker"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bookerPropertyTree!= null):((bookerPropertyTree == null)||(!bookerPropertyTree.isLeaf())))) {
            _other.booker = this.booker;
        }
        final PropertyTree entityMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMeta"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMetaPropertyTree!= null):((entityMetaPropertyTree == null)||(!entityMetaPropertyTree.isLeaf())))) {
            _other.entityMeta = this.entityMeta;
        }
        final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
            _other.id = this.id;
        }
        final PropertyTree fromTimePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromTime"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromTimePropertyTree!= null):((fromTimePropertyTree == null)||(!fromTimePropertyTree.isLeaf())))) {
            _other.fromTime = this.fromTime;
        }
        final PropertyTree untilTimePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("untilTime"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(untilTimePropertyTree!= null):((untilTimePropertyTree == null)||(!untilTimePropertyTree.isLeaf())))) {
            _other.untilTime = this.untilTime;
        }
        final PropertyTree fromDatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromDate"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromDatePropertyTree!= null):((fromDatePropertyTree == null)||(!fromDatePropertyTree.isLeaf())))) {
            _other.fromDate = this.fromDate;
        }
        final PropertyTree untilDatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("untilDate"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(untilDatePropertyTree!= null):((untilDatePropertyTree == null)||(!untilDatePropertyTree.isLeaf())))) {
            _other.untilDate = this.untilDate;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree toolTipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toolTip"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toolTipPropertyTree!= null):((toolTipPropertyTree == null)||(!toolTipPropertyTree.isLeaf())))) {
            _other.toolTip = this.toolTip;
        }
        final PropertyTree backgroundColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("backgroundColor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundColorPropertyTree!= null):((backgroundColorPropertyTree == null)||(!backgroundColorPropertyTree.isLeaf())))) {
            _other.backgroundColor = this.backgroundColor;
        }
    }

    public<_B >Booking.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Booking.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Booking.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Booking.Builder<_B> copyOf(final Booking _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Booking.Builder<_B> _newBuilder = new Booking.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Booking.Builder<Void> copyExcept(final Booking _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Booking.Builder<Void> copyOnly(final Booking _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Booking _storedValue;
        private String resource;
        private String booker;
        private String entityMeta;
        private String id;
        private String fromTime;
        private String untilTime;
        private String fromDate;
        private String untilDate;
        private String label;
        private String toolTip;
        private String backgroundColor;

        public Builder(final _B _parentBuilder, final Booking _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.resource = _other.resource;
                    this.booker = _other.booker;
                    this.entityMeta = _other.entityMeta;
                    this.id = _other.id;
                    this.fromTime = _other.fromTime;
                    this.untilTime = _other.untilTime;
                    this.fromDate = _other.fromDate;
                    this.untilDate = _other.untilDate;
                    this.label = _other.label;
                    this.toolTip = _other.toolTip;
                    this.backgroundColor = _other.backgroundColor;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Booking _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree resourcePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resource"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourcePropertyTree!= null):((resourcePropertyTree == null)||(!resourcePropertyTree.isLeaf())))) {
                        this.resource = _other.resource;
                    }
                    final PropertyTree bookerPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("booker"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bookerPropertyTree!= null):((bookerPropertyTree == null)||(!bookerPropertyTree.isLeaf())))) {
                        this.booker = _other.booker;
                    }
                    final PropertyTree entityMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMeta"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMetaPropertyTree!= null):((entityMetaPropertyTree == null)||(!entityMetaPropertyTree.isLeaf())))) {
                        this.entityMeta = _other.entityMeta;
                    }
                    final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
                        this.id = _other.id;
                    }
                    final PropertyTree fromTimePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromTime"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromTimePropertyTree!= null):((fromTimePropertyTree == null)||(!fromTimePropertyTree.isLeaf())))) {
                        this.fromTime = _other.fromTime;
                    }
                    final PropertyTree untilTimePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("untilTime"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(untilTimePropertyTree!= null):((untilTimePropertyTree == null)||(!untilTimePropertyTree.isLeaf())))) {
                        this.untilTime = _other.untilTime;
                    }
                    final PropertyTree fromDatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromDate"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromDatePropertyTree!= null):((fromDatePropertyTree == null)||(!fromDatePropertyTree.isLeaf())))) {
                        this.fromDate = _other.fromDate;
                    }
                    final PropertyTree untilDatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("untilDate"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(untilDatePropertyTree!= null):((untilDatePropertyTree == null)||(!untilDatePropertyTree.isLeaf())))) {
                        this.untilDate = _other.untilDate;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree toolTipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toolTip"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toolTipPropertyTree!= null):((toolTipPropertyTree == null)||(!toolTipPropertyTree.isLeaf())))) {
                        this.toolTip = _other.toolTip;
                    }
                    final PropertyTree backgroundColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("backgroundColor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundColorPropertyTree!= null):((backgroundColorPropertyTree == null)||(!backgroundColorPropertyTree.isLeaf())))) {
                        this.backgroundColor = _other.backgroundColor;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Booking >_P init(final _P _product) {
            _product.resource = this.resource;
            _product.booker = this.booker;
            _product.entityMeta = this.entityMeta;
            _product.id = this.id;
            _product.fromTime = this.fromTime;
            _product.untilTime = this.untilTime;
            _product.fromDate = this.fromDate;
            _product.untilDate = this.untilDate;
            _product.label = this.label;
            _product.toolTip = this.toolTip;
            _product.backgroundColor = this.backgroundColor;
            return _product;
        }

        /**
         * Sets the new value of "resource" (any previous value will be replaced)
         * 
         * @param resource
         *     New value of the "resource" property.
         */
        public Booking.Builder<_B> withResource(final String resource) {
            this.resource = resource;
            return this;
        }

        /**
         * Sets the new value of "booker" (any previous value will be replaced)
         * 
         * @param booker
         *     New value of the "booker" property.
         */
        public Booking.Builder<_B> withBooker(final String booker) {
            this.booker = booker;
            return this;
        }

        /**
         * Sets the new value of "entityMeta" (any previous value will be replaced)
         * 
         * @param entityMeta
         *     New value of the "entityMeta" property.
         */
        public Booking.Builder<_B> withEntityMeta(final String entityMeta) {
            this.entityMeta = entityMeta;
            return this;
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        public Booking.Builder<_B> withId(final String id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the new value of "fromTime" (any previous value will be replaced)
         * 
         * @param fromTime
         *     New value of the "fromTime" property.
         */
        public Booking.Builder<_B> withFromTime(final String fromTime) {
            this.fromTime = fromTime;
            return this;
        }

        /**
         * Sets the new value of "untilTime" (any previous value will be replaced)
         * 
         * @param untilTime
         *     New value of the "untilTime" property.
         */
        public Booking.Builder<_B> withUntilTime(final String untilTime) {
            this.untilTime = untilTime;
            return this;
        }

        /**
         * Sets the new value of "fromDate" (any previous value will be replaced)
         * 
         * @param fromDate
         *     New value of the "fromDate" property.
         */
        public Booking.Builder<_B> withFromDate(final String fromDate) {
            this.fromDate = fromDate;
            return this;
        }

        /**
         * Sets the new value of "untilDate" (any previous value will be replaced)
         * 
         * @param untilDate
         *     New value of the "untilDate" property.
         */
        public Booking.Builder<_B> withUntilDate(final String untilDate) {
            this.untilDate = untilDate;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public Booking.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "toolTip" (any previous value will be replaced)
         * 
         * @param toolTip
         *     New value of the "toolTip" property.
         */
        public Booking.Builder<_B> withToolTip(final String toolTip) {
            this.toolTip = toolTip;
            return this;
        }

        /**
         * Sets the new value of "backgroundColor" (any previous value will be replaced)
         * 
         * @param backgroundColor
         *     New value of the "backgroundColor" property.
         */
        public Booking.Builder<_B> withBackgroundColor(final String backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        @Override
        public Booking build() {
            if (_storedValue == null) {
                return this.init(new Booking());
            } else {
                return ((Booking) _storedValue);
            }
        }

        public Booking.Builder<_B> copyOf(final Booking _other) {
            _other.copyTo(this);
            return this;
        }

        public Booking.Builder<_B> copyOf(final Booking.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Booking.Selector<Booking.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Booking.Select _root() {
            return new Booking.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> resource = null;
        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> booker = null;
        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> entityMeta = null;
        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> id = null;
        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> fromTime = null;
        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> untilTime = null;
        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> fromDate = null;
        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> untilDate = null;
        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> toolTip = null;
        private com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> backgroundColor = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.resource!= null) {
                products.put("resource", this.resource.init());
            }
            if (this.booker!= null) {
                products.put("booker", this.booker.init());
            }
            if (this.entityMeta!= null) {
                products.put("entityMeta", this.entityMeta.init());
            }
            if (this.id!= null) {
                products.put("id", this.id.init());
            }
            if (this.fromTime!= null) {
                products.put("fromTime", this.fromTime.init());
            }
            if (this.untilTime!= null) {
                products.put("untilTime", this.untilTime.init());
            }
            if (this.fromDate!= null) {
                products.put("fromDate", this.fromDate.init());
            }
            if (this.untilDate!= null) {
                products.put("untilDate", this.untilDate.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.toolTip!= null) {
                products.put("toolTip", this.toolTip.init());
            }
            if (this.backgroundColor!= null) {
                products.put("backgroundColor", this.backgroundColor.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> resource() {
            return ((this.resource == null)?this.resource = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "resource"):this.resource);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> booker() {
            return ((this.booker == null)?this.booker = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "booker"):this.booker);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> entityMeta() {
            return ((this.entityMeta == null)?this.entityMeta = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "entityMeta"):this.entityMeta);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> id() {
            return ((this.id == null)?this.id = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "id"):this.id);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> fromTime() {
            return ((this.fromTime == null)?this.fromTime = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "fromTime"):this.fromTime);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> untilTime() {
            return ((this.untilTime == null)?this.untilTime = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "untilTime"):this.untilTime);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> fromDate() {
            return ((this.fromDate == null)?this.fromDate = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "fromDate"):this.fromDate);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> untilDate() {
            return ((this.untilDate == null)?this.untilDate = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "untilDate"):this.untilDate);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> toolTip() {
            return ((this.toolTip == null)?this.toolTip = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "toolTip"):this.toolTip);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>> backgroundColor() {
            return ((this.backgroundColor == null)?this.backgroundColor = new com.kscs.util.jaxb.Selector<TRoot, Booking.Selector<TRoot, TParent>>(this._root, this, "backgroundColor"):this.backgroundColor);
        }

    }

}
