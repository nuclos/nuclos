package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for booking-def complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="booking-def"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="businessHours" type="{urn:org.nuclos.schema.rest}business-hour" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="resource-ref-field" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="booker-field" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="date-from-field" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="date-until-field" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="time-from-field" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="time-until-field" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="color" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="with-time" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "booking-def", propOrder = {
    "businessHours"
})
public class BookingDef implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<BusinessHour> businessHours;
    @XmlAttribute(name = "resource-ref-field", required = true)
    protected String resourceRefField;
    @XmlAttribute(name = "booker-field", required = true)
    protected String bookerField;
    @XmlAttribute(name = "entity", required = true)
    protected String entity;
    @XmlAttribute(name = "date-from-field", required = true)
    protected String dateFromField;
    @XmlAttribute(name = "date-until-field", required = true)
    protected String dateUntilField;
    @XmlAttribute(name = "time-from-field", required = true)
    protected String timeFromField;
    @XmlAttribute(name = "time-until-field", required = true)
    protected String timeUntilField;
    @XmlAttribute(name = "color", required = true)
    protected String color;
    @XmlAttribute(name = "with-time", required = true)
    protected boolean withTime;

    /**
     * Gets the value of the businessHours property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the businessHours property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBusinessHours().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessHour }
     * 
     * 
     */
    public List<BusinessHour> getBusinessHours() {
        if (businessHours == null) {
            businessHours = new ArrayList<BusinessHour>();
        }
        return this.businessHours;
    }

    /**
     * Gets the value of the resourceRefField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceRefField() {
        return resourceRefField;
    }

    /**
     * Sets the value of the resourceRefField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceRefField(String value) {
        this.resourceRefField = value;
    }

    /**
     * Gets the value of the bookerField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookerField() {
        return bookerField;
    }

    /**
     * Sets the value of the bookerField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookerField(String value) {
        this.bookerField = value;
    }

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the dateFromField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateFromField() {
        return dateFromField;
    }

    /**
     * Sets the value of the dateFromField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateFromField(String value) {
        this.dateFromField = value;
    }

    /**
     * Gets the value of the dateUntilField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateUntilField() {
        return dateUntilField;
    }

    /**
     * Sets the value of the dateUntilField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateUntilField(String value) {
        this.dateUntilField = value;
    }

    /**
     * Gets the value of the timeFromField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeFromField() {
        return timeFromField;
    }

    /**
     * Sets the value of the timeFromField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeFromField(String value) {
        this.timeFromField = value;
    }

    /**
     * Gets the value of the timeUntilField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeUntilField() {
        return timeUntilField;
    }

    /**
     * Sets the value of the timeUntilField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeUntilField(String value) {
        this.timeUntilField = value;
    }

    /**
     * Gets the value of the color property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the value of the color property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    /**
     * Gets the value of the withTime property.
     * 
     */
    public boolean isWithTime() {
        return withTime;
    }

    /**
     * Sets the value of the withTime property.
     * 
     */
    public void setWithTime(boolean value) {
        this.withTime = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<BusinessHour> theBusinessHours;
            theBusinessHours = (((this.businessHours!= null)&&(!this.businessHours.isEmpty()))?this.getBusinessHours():null);
            strategy.appendField(locator, this, "businessHours", buffer, theBusinessHours);
        }
        {
            String theResourceRefField;
            theResourceRefField = this.getResourceRefField();
            strategy.appendField(locator, this, "resourceRefField", buffer, theResourceRefField);
        }
        {
            String theBookerField;
            theBookerField = this.getBookerField();
            strategy.appendField(locator, this, "bookerField", buffer, theBookerField);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            String theDateFromField;
            theDateFromField = this.getDateFromField();
            strategy.appendField(locator, this, "dateFromField", buffer, theDateFromField);
        }
        {
            String theDateUntilField;
            theDateUntilField = this.getDateUntilField();
            strategy.appendField(locator, this, "dateUntilField", buffer, theDateUntilField);
        }
        {
            String theTimeFromField;
            theTimeFromField = this.getTimeFromField();
            strategy.appendField(locator, this, "timeFromField", buffer, theTimeFromField);
        }
        {
            String theTimeUntilField;
            theTimeUntilField = this.getTimeUntilField();
            strategy.appendField(locator, this, "timeUntilField", buffer, theTimeUntilField);
        }
        {
            String theColor;
            theColor = this.getColor();
            strategy.appendField(locator, this, "color", buffer, theColor);
        }
        {
            boolean theWithTime;
            theWithTime = this.isWithTime();
            strategy.appendField(locator, this, "withTime", buffer, theWithTime);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BookingDef)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final BookingDef that = ((BookingDef) object);
        {
            List<BusinessHour> lhsBusinessHours;
            lhsBusinessHours = (((this.businessHours!= null)&&(!this.businessHours.isEmpty()))?this.getBusinessHours():null);
            List<BusinessHour> rhsBusinessHours;
            rhsBusinessHours = (((that.businessHours!= null)&&(!that.businessHours.isEmpty()))?that.getBusinessHours():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "businessHours", lhsBusinessHours), LocatorUtils.property(thatLocator, "businessHours", rhsBusinessHours), lhsBusinessHours, rhsBusinessHours)) {
                return false;
            }
        }
        {
            String lhsResourceRefField;
            lhsResourceRefField = this.getResourceRefField();
            String rhsResourceRefField;
            rhsResourceRefField = that.getResourceRefField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceRefField", lhsResourceRefField), LocatorUtils.property(thatLocator, "resourceRefField", rhsResourceRefField), lhsResourceRefField, rhsResourceRefField)) {
                return false;
            }
        }
        {
            String lhsBookerField;
            lhsBookerField = this.getBookerField();
            String rhsBookerField;
            rhsBookerField = that.getBookerField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bookerField", lhsBookerField), LocatorUtils.property(thatLocator, "bookerField", rhsBookerField), lhsBookerField, rhsBookerField)) {
                return false;
            }
        }
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            String lhsDateFromField;
            lhsDateFromField = this.getDateFromField();
            String rhsDateFromField;
            rhsDateFromField = that.getDateFromField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateFromField", lhsDateFromField), LocatorUtils.property(thatLocator, "dateFromField", rhsDateFromField), lhsDateFromField, rhsDateFromField)) {
                return false;
            }
        }
        {
            String lhsDateUntilField;
            lhsDateUntilField = this.getDateUntilField();
            String rhsDateUntilField;
            rhsDateUntilField = that.getDateUntilField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateUntilField", lhsDateUntilField), LocatorUtils.property(thatLocator, "dateUntilField", rhsDateUntilField), lhsDateUntilField, rhsDateUntilField)) {
                return false;
            }
        }
        {
            String lhsTimeFromField;
            lhsTimeFromField = this.getTimeFromField();
            String rhsTimeFromField;
            rhsTimeFromField = that.getTimeFromField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "timeFromField", lhsTimeFromField), LocatorUtils.property(thatLocator, "timeFromField", rhsTimeFromField), lhsTimeFromField, rhsTimeFromField)) {
                return false;
            }
        }
        {
            String lhsTimeUntilField;
            lhsTimeUntilField = this.getTimeUntilField();
            String rhsTimeUntilField;
            rhsTimeUntilField = that.getTimeUntilField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "timeUntilField", lhsTimeUntilField), LocatorUtils.property(thatLocator, "timeUntilField", rhsTimeUntilField), lhsTimeUntilField, rhsTimeUntilField)) {
                return false;
            }
        }
        {
            String lhsColor;
            lhsColor = this.getColor();
            String rhsColor;
            rhsColor = that.getColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "color", lhsColor), LocatorUtils.property(thatLocator, "color", rhsColor), lhsColor, rhsColor)) {
                return false;
            }
        }
        {
            boolean lhsWithTime;
            lhsWithTime = this.isWithTime();
            boolean rhsWithTime;
            rhsWithTime = that.isWithTime();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "withTime", lhsWithTime), LocatorUtils.property(thatLocator, "withTime", rhsWithTime), lhsWithTime, rhsWithTime)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<BusinessHour> theBusinessHours;
            theBusinessHours = (((this.businessHours!= null)&&(!this.businessHours.isEmpty()))?this.getBusinessHours():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "businessHours", theBusinessHours), currentHashCode, theBusinessHours);
        }
        {
            String theResourceRefField;
            theResourceRefField = this.getResourceRefField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceRefField", theResourceRefField), currentHashCode, theResourceRefField);
        }
        {
            String theBookerField;
            theBookerField = this.getBookerField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bookerField", theBookerField), currentHashCode, theBookerField);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            String theDateFromField;
            theDateFromField = this.getDateFromField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dateFromField", theDateFromField), currentHashCode, theDateFromField);
        }
        {
            String theDateUntilField;
            theDateUntilField = this.getDateUntilField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dateUntilField", theDateUntilField), currentHashCode, theDateUntilField);
        }
        {
            String theTimeFromField;
            theTimeFromField = this.getTimeFromField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "timeFromField", theTimeFromField), currentHashCode, theTimeFromField);
        }
        {
            String theTimeUntilField;
            theTimeUntilField = this.getTimeUntilField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "timeUntilField", theTimeUntilField), currentHashCode, theTimeUntilField);
        }
        {
            String theColor;
            theColor = this.getColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "color", theColor), currentHashCode, theColor);
        }
        {
            boolean theWithTime;
            theWithTime = this.isWithTime();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "withTime", theWithTime), currentHashCode, theWithTime);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof BookingDef) {
            final BookingDef copy = ((BookingDef) draftCopy);
            if ((this.businessHours!= null)&&(!this.businessHours.isEmpty())) {
                List<BusinessHour> sourceBusinessHours;
                sourceBusinessHours = (((this.businessHours!= null)&&(!this.businessHours.isEmpty()))?this.getBusinessHours():null);
                @SuppressWarnings("unchecked")
                List<BusinessHour> copyBusinessHours = ((List<BusinessHour> ) strategy.copy(LocatorUtils.property(locator, "businessHours", sourceBusinessHours), sourceBusinessHours));
                copy.businessHours = null;
                if (copyBusinessHours!= null) {
                    List<BusinessHour> uniqueBusinessHoursl = copy.getBusinessHours();
                    uniqueBusinessHoursl.addAll(copyBusinessHours);
                }
            } else {
                copy.businessHours = null;
            }
            if (this.resourceRefField!= null) {
                String sourceResourceRefField;
                sourceResourceRefField = this.getResourceRefField();
                String copyResourceRefField = ((String) strategy.copy(LocatorUtils.property(locator, "resourceRefField", sourceResourceRefField), sourceResourceRefField));
                copy.setResourceRefField(copyResourceRefField);
            } else {
                copy.resourceRefField = null;
            }
            if (this.bookerField!= null) {
                String sourceBookerField;
                sourceBookerField = this.getBookerField();
                String copyBookerField = ((String) strategy.copy(LocatorUtils.property(locator, "bookerField", sourceBookerField), sourceBookerField));
                copy.setBookerField(copyBookerField);
            } else {
                copy.bookerField = null;
            }
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.dateFromField!= null) {
                String sourceDateFromField;
                sourceDateFromField = this.getDateFromField();
                String copyDateFromField = ((String) strategy.copy(LocatorUtils.property(locator, "dateFromField", sourceDateFromField), sourceDateFromField));
                copy.setDateFromField(copyDateFromField);
            } else {
                copy.dateFromField = null;
            }
            if (this.dateUntilField!= null) {
                String sourceDateUntilField;
                sourceDateUntilField = this.getDateUntilField();
                String copyDateUntilField = ((String) strategy.copy(LocatorUtils.property(locator, "dateUntilField", sourceDateUntilField), sourceDateUntilField));
                copy.setDateUntilField(copyDateUntilField);
            } else {
                copy.dateUntilField = null;
            }
            if (this.timeFromField!= null) {
                String sourceTimeFromField;
                sourceTimeFromField = this.getTimeFromField();
                String copyTimeFromField = ((String) strategy.copy(LocatorUtils.property(locator, "timeFromField", sourceTimeFromField), sourceTimeFromField));
                copy.setTimeFromField(copyTimeFromField);
            } else {
                copy.timeFromField = null;
            }
            if (this.timeUntilField!= null) {
                String sourceTimeUntilField;
                sourceTimeUntilField = this.getTimeUntilField();
                String copyTimeUntilField = ((String) strategy.copy(LocatorUtils.property(locator, "timeUntilField", sourceTimeUntilField), sourceTimeUntilField));
                copy.setTimeUntilField(copyTimeUntilField);
            } else {
                copy.timeUntilField = null;
            }
            if (this.color!= null) {
                String sourceColor;
                sourceColor = this.getColor();
                String copyColor = ((String) strategy.copy(LocatorUtils.property(locator, "color", sourceColor), sourceColor));
                copy.setColor(copyColor);
            } else {
                copy.color = null;
            }
            {
                boolean sourceWithTime;
                sourceWithTime = this.isWithTime();
                boolean copyWithTime = strategy.copy(LocatorUtils.property(locator, "withTime", sourceWithTime), sourceWithTime);
                copy.setWithTime(copyWithTime);
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BookingDef();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final BookingDef.Builder<_B> _other) {
        if (this.businessHours == null) {
            _other.businessHours = null;
        } else {
            _other.businessHours = new ArrayList<BusinessHour.Builder<BookingDef.Builder<_B>>>();
            for (BusinessHour _item: this.businessHours) {
                _other.businessHours.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.resourceRefField = this.resourceRefField;
        _other.bookerField = this.bookerField;
        _other.entity = this.entity;
        _other.dateFromField = this.dateFromField;
        _other.dateUntilField = this.dateUntilField;
        _other.timeFromField = this.timeFromField;
        _other.timeUntilField = this.timeUntilField;
        _other.color = this.color;
        _other.withTime = this.withTime;
    }

    public<_B >BookingDef.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new BookingDef.Builder<_B>(_parentBuilder, this, true);
    }

    public BookingDef.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static BookingDef.Builder<Void> builder() {
        return new BookingDef.Builder<Void>(null, null, false);
    }

    public static<_B >BookingDef.Builder<_B> copyOf(final BookingDef _other) {
        final BookingDef.Builder<_B> _newBuilder = new BookingDef.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final BookingDef.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree businessHoursPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("businessHours"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(businessHoursPropertyTree!= null):((businessHoursPropertyTree == null)||(!businessHoursPropertyTree.isLeaf())))) {
            if (this.businessHours == null) {
                _other.businessHours = null;
            } else {
                _other.businessHours = new ArrayList<BusinessHour.Builder<BookingDef.Builder<_B>>>();
                for (BusinessHour _item: this.businessHours) {
                    _other.businessHours.add(((_item == null)?null:_item.newCopyBuilder(_other, businessHoursPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree resourceRefFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceRefField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceRefFieldPropertyTree!= null):((resourceRefFieldPropertyTree == null)||(!resourceRefFieldPropertyTree.isLeaf())))) {
            _other.resourceRefField = this.resourceRefField;
        }
        final PropertyTree bookerFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bookerField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bookerFieldPropertyTree!= null):((bookerFieldPropertyTree == null)||(!bookerFieldPropertyTree.isLeaf())))) {
            _other.bookerField = this.bookerField;
        }
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree dateFromFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateFromField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateFromFieldPropertyTree!= null):((dateFromFieldPropertyTree == null)||(!dateFromFieldPropertyTree.isLeaf())))) {
            _other.dateFromField = this.dateFromField;
        }
        final PropertyTree dateUntilFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateUntilField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateUntilFieldPropertyTree!= null):((dateUntilFieldPropertyTree == null)||(!dateUntilFieldPropertyTree.isLeaf())))) {
            _other.dateUntilField = this.dateUntilField;
        }
        final PropertyTree timeFromFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("timeFromField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(timeFromFieldPropertyTree!= null):((timeFromFieldPropertyTree == null)||(!timeFromFieldPropertyTree.isLeaf())))) {
            _other.timeFromField = this.timeFromField;
        }
        final PropertyTree timeUntilFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("timeUntilField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(timeUntilFieldPropertyTree!= null):((timeUntilFieldPropertyTree == null)||(!timeUntilFieldPropertyTree.isLeaf())))) {
            _other.timeUntilField = this.timeUntilField;
        }
        final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
            _other.color = this.color;
        }
        final PropertyTree withTimePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withTime"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withTimePropertyTree!= null):((withTimePropertyTree == null)||(!withTimePropertyTree.isLeaf())))) {
            _other.withTime = this.withTime;
        }
    }

    public<_B >BookingDef.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new BookingDef.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public BookingDef.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >BookingDef.Builder<_B> copyOf(final BookingDef _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final BookingDef.Builder<_B> _newBuilder = new BookingDef.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static BookingDef.Builder<Void> copyExcept(final BookingDef _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static BookingDef.Builder<Void> copyOnly(final BookingDef _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final BookingDef _storedValue;
        private List<BusinessHour.Builder<BookingDef.Builder<_B>>> businessHours;
        private String resourceRefField;
        private String bookerField;
        private String entity;
        private String dateFromField;
        private String dateUntilField;
        private String timeFromField;
        private String timeUntilField;
        private String color;
        private boolean withTime;

        public Builder(final _B _parentBuilder, final BookingDef _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.businessHours == null) {
                        this.businessHours = null;
                    } else {
                        this.businessHours = new ArrayList<BusinessHour.Builder<BookingDef.Builder<_B>>>();
                        for (BusinessHour _item: _other.businessHours) {
                            this.businessHours.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.resourceRefField = _other.resourceRefField;
                    this.bookerField = _other.bookerField;
                    this.entity = _other.entity;
                    this.dateFromField = _other.dateFromField;
                    this.dateUntilField = _other.dateUntilField;
                    this.timeFromField = _other.timeFromField;
                    this.timeUntilField = _other.timeUntilField;
                    this.color = _other.color;
                    this.withTime = _other.withTime;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final BookingDef _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree businessHoursPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("businessHours"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(businessHoursPropertyTree!= null):((businessHoursPropertyTree == null)||(!businessHoursPropertyTree.isLeaf())))) {
                        if (_other.businessHours == null) {
                            this.businessHours = null;
                        } else {
                            this.businessHours = new ArrayList<BusinessHour.Builder<BookingDef.Builder<_B>>>();
                            for (BusinessHour _item: _other.businessHours) {
                                this.businessHours.add(((_item == null)?null:_item.newCopyBuilder(this, businessHoursPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree resourceRefFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceRefField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceRefFieldPropertyTree!= null):((resourceRefFieldPropertyTree == null)||(!resourceRefFieldPropertyTree.isLeaf())))) {
                        this.resourceRefField = _other.resourceRefField;
                    }
                    final PropertyTree bookerFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bookerField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bookerFieldPropertyTree!= null):((bookerFieldPropertyTree == null)||(!bookerFieldPropertyTree.isLeaf())))) {
                        this.bookerField = _other.bookerField;
                    }
                    final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                        this.entity = _other.entity;
                    }
                    final PropertyTree dateFromFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateFromField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateFromFieldPropertyTree!= null):((dateFromFieldPropertyTree == null)||(!dateFromFieldPropertyTree.isLeaf())))) {
                        this.dateFromField = _other.dateFromField;
                    }
                    final PropertyTree dateUntilFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateUntilField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateUntilFieldPropertyTree!= null):((dateUntilFieldPropertyTree == null)||(!dateUntilFieldPropertyTree.isLeaf())))) {
                        this.dateUntilField = _other.dateUntilField;
                    }
                    final PropertyTree timeFromFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("timeFromField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(timeFromFieldPropertyTree!= null):((timeFromFieldPropertyTree == null)||(!timeFromFieldPropertyTree.isLeaf())))) {
                        this.timeFromField = _other.timeFromField;
                    }
                    final PropertyTree timeUntilFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("timeUntilField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(timeUntilFieldPropertyTree!= null):((timeUntilFieldPropertyTree == null)||(!timeUntilFieldPropertyTree.isLeaf())))) {
                        this.timeUntilField = _other.timeUntilField;
                    }
                    final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
                        this.color = _other.color;
                    }
                    final PropertyTree withTimePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("withTime"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(withTimePropertyTree!= null):((withTimePropertyTree == null)||(!withTimePropertyTree.isLeaf())))) {
                        this.withTime = _other.withTime;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends BookingDef >_P init(final _P _product) {
            if (this.businessHours!= null) {
                final List<BusinessHour> businessHours = new ArrayList<BusinessHour>(this.businessHours.size());
                for (BusinessHour.Builder<BookingDef.Builder<_B>> _item: this.businessHours) {
                    businessHours.add(_item.build());
                }
                _product.businessHours = businessHours;
            }
            _product.resourceRefField = this.resourceRefField;
            _product.bookerField = this.bookerField;
            _product.entity = this.entity;
            _product.dateFromField = this.dateFromField;
            _product.dateUntilField = this.dateUntilField;
            _product.timeFromField = this.timeFromField;
            _product.timeUntilField = this.timeUntilField;
            _product.color = this.color;
            _product.withTime = this.withTime;
            return _product;
        }

        /**
         * Adds the given items to the value of "businessHours"
         * 
         * @param businessHours
         *     Items to add to the value of the "businessHours" property
         */
        public BookingDef.Builder<_B> addBusinessHours(final Iterable<? extends BusinessHour> businessHours) {
            if (businessHours!= null) {
                if (this.businessHours == null) {
                    this.businessHours = new ArrayList<BusinessHour.Builder<BookingDef.Builder<_B>>>();
                }
                for (BusinessHour _item: businessHours) {
                    this.businessHours.add(new BusinessHour.Builder<BookingDef.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "businessHours" (any previous value will be replaced)
         * 
         * @param businessHours
         *     New value of the "businessHours" property.
         */
        public BookingDef.Builder<_B> withBusinessHours(final Iterable<? extends BusinessHour> businessHours) {
            if (this.businessHours!= null) {
                this.businessHours.clear();
            }
            return addBusinessHours(businessHours);
        }

        /**
         * Adds the given items to the value of "businessHours"
         * 
         * @param businessHours
         *     Items to add to the value of the "businessHours" property
         */
        public BookingDef.Builder<_B> addBusinessHours(BusinessHour... businessHours) {
            addBusinessHours(Arrays.asList(businessHours));
            return this;
        }

        /**
         * Sets the new value of "businessHours" (any previous value will be replaced)
         * 
         * @param businessHours
         *     New value of the "businessHours" property.
         */
        public BookingDef.Builder<_B> withBusinessHours(BusinessHour... businessHours) {
            withBusinessHours(Arrays.asList(businessHours));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "BusinessHours" property.
         * Use {@link org.nuclos.schema.rest.BusinessHour.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "BusinessHours" property.
         *     Use {@link org.nuclos.schema.rest.BusinessHour.Builder#end()} to return to the current builder.
         */
        public BusinessHour.Builder<? extends BookingDef.Builder<_B>> addBusinessHours() {
            if (this.businessHours == null) {
                this.businessHours = new ArrayList<BusinessHour.Builder<BookingDef.Builder<_B>>>();
            }
            final BusinessHour.Builder<BookingDef.Builder<_B>> businessHours_Builder = new BusinessHour.Builder<BookingDef.Builder<_B>>(this, null, false);
            this.businessHours.add(businessHours_Builder);
            return businessHours_Builder;
        }

        /**
         * Sets the new value of "resourceRefField" (any previous value will be replaced)
         * 
         * @param resourceRefField
         *     New value of the "resourceRefField" property.
         */
        public BookingDef.Builder<_B> withResourceRefField(final String resourceRefField) {
            this.resourceRefField = resourceRefField;
            return this;
        }

        /**
         * Sets the new value of "bookerField" (any previous value will be replaced)
         * 
         * @param bookerField
         *     New value of the "bookerField" property.
         */
        public BookingDef.Builder<_B> withBookerField(final String bookerField) {
            this.bookerField = bookerField;
            return this;
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public BookingDef.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "dateFromField" (any previous value will be replaced)
         * 
         * @param dateFromField
         *     New value of the "dateFromField" property.
         */
        public BookingDef.Builder<_B> withDateFromField(final String dateFromField) {
            this.dateFromField = dateFromField;
            return this;
        }

        /**
         * Sets the new value of "dateUntilField" (any previous value will be replaced)
         * 
         * @param dateUntilField
         *     New value of the "dateUntilField" property.
         */
        public BookingDef.Builder<_B> withDateUntilField(final String dateUntilField) {
            this.dateUntilField = dateUntilField;
            return this;
        }

        /**
         * Sets the new value of "timeFromField" (any previous value will be replaced)
         * 
         * @param timeFromField
         *     New value of the "timeFromField" property.
         */
        public BookingDef.Builder<_B> withTimeFromField(final String timeFromField) {
            this.timeFromField = timeFromField;
            return this;
        }

        /**
         * Sets the new value of "timeUntilField" (any previous value will be replaced)
         * 
         * @param timeUntilField
         *     New value of the "timeUntilField" property.
         */
        public BookingDef.Builder<_B> withTimeUntilField(final String timeUntilField) {
            this.timeUntilField = timeUntilField;
            return this;
        }

        /**
         * Sets the new value of "color" (any previous value will be replaced)
         * 
         * @param color
         *     New value of the "color" property.
         */
        public BookingDef.Builder<_B> withColor(final String color) {
            this.color = color;
            return this;
        }

        /**
         * Sets the new value of "withTime" (any previous value will be replaced)
         * 
         * @param withTime
         *     New value of the "withTime" property.
         */
        public BookingDef.Builder<_B> withWithTime(final boolean withTime) {
            this.withTime = withTime;
            return this;
        }

        @Override
        public BookingDef build() {
            if (_storedValue == null) {
                return this.init(new BookingDef());
            } else {
                return ((BookingDef) _storedValue);
            }
        }

        public BookingDef.Builder<_B> copyOf(final BookingDef _other) {
            _other.copyTo(this);
            return this;
        }

        public BookingDef.Builder<_B> copyOf(final BookingDef.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends BookingDef.Selector<BookingDef.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static BookingDef.Select _root() {
            return new BookingDef.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private BusinessHour.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> businessHours = null;
        private com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> resourceRefField = null;
        private com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> bookerField = null;
        private com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> dateFromField = null;
        private com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> dateUntilField = null;
        private com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> timeFromField = null;
        private com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> timeUntilField = null;
        private com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> color = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.businessHours!= null) {
                products.put("businessHours", this.businessHours.init());
            }
            if (this.resourceRefField!= null) {
                products.put("resourceRefField", this.resourceRefField.init());
            }
            if (this.bookerField!= null) {
                products.put("bookerField", this.bookerField.init());
            }
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.dateFromField!= null) {
                products.put("dateFromField", this.dateFromField.init());
            }
            if (this.dateUntilField!= null) {
                products.put("dateUntilField", this.dateUntilField.init());
            }
            if (this.timeFromField!= null) {
                products.put("timeFromField", this.timeFromField.init());
            }
            if (this.timeUntilField!= null) {
                products.put("timeUntilField", this.timeUntilField.init());
            }
            if (this.color!= null) {
                products.put("color", this.color.init());
            }
            return products;
        }

        public BusinessHour.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> businessHours() {
            return ((this.businessHours == null)?this.businessHours = new BusinessHour.Selector<TRoot, BookingDef.Selector<TRoot, TParent>>(this._root, this, "businessHours"):this.businessHours);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> resourceRefField() {
            return ((this.resourceRefField == null)?this.resourceRefField = new com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>>(this._root, this, "resourceRefField"):this.resourceRefField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> bookerField() {
            return ((this.bookerField == null)?this.bookerField = new com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>>(this._root, this, "bookerField"):this.bookerField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> dateFromField() {
            return ((this.dateFromField == null)?this.dateFromField = new com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>>(this._root, this, "dateFromField"):this.dateFromField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> dateUntilField() {
            return ((this.dateUntilField == null)?this.dateUntilField = new com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>>(this._root, this, "dateUntilField"):this.dateUntilField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> timeFromField() {
            return ((this.timeFromField == null)?this.timeFromField = new com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>>(this._root, this, "timeFromField"):this.timeFromField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> timeUntilField() {
            return ((this.timeUntilField == null)?this.timeUntilField = new com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>>(this._root, this, "timeUntilField"):this.timeUntilField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>> color() {
            return ((this.color == null)?this.color = new com.kscs.util.jaxb.Selector<TRoot, BookingDef.Selector<TRoot, TParent>>(this._root, this, "color"):this.color);
        }

    }

}
