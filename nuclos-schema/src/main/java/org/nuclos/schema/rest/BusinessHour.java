package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for business-hour complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="business-hour"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="from" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="until" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "business-hour")
public class BusinessHour implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "from", required = true)
    protected String from;
    @XmlAttribute(name = "until", required = true)
    protected String until;

    /**
     * Gets the value of the from property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrom() {
        return from;
    }

    /**
     * Sets the value of the from property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrom(String value) {
        this.from = value;
    }

    /**
     * Gets the value of the until property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUntil() {
        return until;
    }

    /**
     * Sets the value of the until property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUntil(String value) {
        this.until = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theFrom;
            theFrom = this.getFrom();
            strategy.appendField(locator, this, "from", buffer, theFrom);
        }
        {
            String theUntil;
            theUntil = this.getUntil();
            strategy.appendField(locator, this, "until", buffer, theUntil);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof BusinessHour)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final BusinessHour that = ((BusinessHour) object);
        {
            String lhsFrom;
            lhsFrom = this.getFrom();
            String rhsFrom;
            rhsFrom = that.getFrom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "from", lhsFrom), LocatorUtils.property(thatLocator, "from", rhsFrom), lhsFrom, rhsFrom)) {
                return false;
            }
        }
        {
            String lhsUntil;
            lhsUntil = this.getUntil();
            String rhsUntil;
            rhsUntil = that.getUntil();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "until", lhsUntil), LocatorUtils.property(thatLocator, "until", rhsUntil), lhsUntil, rhsUntil)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theFrom;
            theFrom = this.getFrom();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "from", theFrom), currentHashCode, theFrom);
        }
        {
            String theUntil;
            theUntil = this.getUntil();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "until", theUntil), currentHashCode, theUntil);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof BusinessHour) {
            final BusinessHour copy = ((BusinessHour) draftCopy);
            if (this.from!= null) {
                String sourceFrom;
                sourceFrom = this.getFrom();
                String copyFrom = ((String) strategy.copy(LocatorUtils.property(locator, "from", sourceFrom), sourceFrom));
                copy.setFrom(copyFrom);
            } else {
                copy.from = null;
            }
            if (this.until!= null) {
                String sourceUntil;
                sourceUntil = this.getUntil();
                String copyUntil = ((String) strategy.copy(LocatorUtils.property(locator, "until", sourceUntil), sourceUntil));
                copy.setUntil(copyUntil);
            } else {
                copy.until = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new BusinessHour();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final BusinessHour.Builder<_B> _other) {
        _other.from = this.from;
        _other.until = this.until;
    }

    public<_B >BusinessHour.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new BusinessHour.Builder<_B>(_parentBuilder, this, true);
    }

    public BusinessHour.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static BusinessHour.Builder<Void> builder() {
        return new BusinessHour.Builder<Void>(null, null, false);
    }

    public static<_B >BusinessHour.Builder<_B> copyOf(final BusinessHour _other) {
        final BusinessHour.Builder<_B> _newBuilder = new BusinessHour.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final BusinessHour.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree fromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("from"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromPropertyTree!= null):((fromPropertyTree == null)||(!fromPropertyTree.isLeaf())))) {
            _other.from = this.from;
        }
        final PropertyTree untilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("until"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(untilPropertyTree!= null):((untilPropertyTree == null)||(!untilPropertyTree.isLeaf())))) {
            _other.until = this.until;
        }
    }

    public<_B >BusinessHour.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new BusinessHour.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public BusinessHour.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >BusinessHour.Builder<_B> copyOf(final BusinessHour _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final BusinessHour.Builder<_B> _newBuilder = new BusinessHour.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static BusinessHour.Builder<Void> copyExcept(final BusinessHour _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static BusinessHour.Builder<Void> copyOnly(final BusinessHour _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final BusinessHour _storedValue;
        private String from;
        private String until;

        public Builder(final _B _parentBuilder, final BusinessHour _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.from = _other.from;
                    this.until = _other.until;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final BusinessHour _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree fromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("from"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromPropertyTree!= null):((fromPropertyTree == null)||(!fromPropertyTree.isLeaf())))) {
                        this.from = _other.from;
                    }
                    final PropertyTree untilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("until"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(untilPropertyTree!= null):((untilPropertyTree == null)||(!untilPropertyTree.isLeaf())))) {
                        this.until = _other.until;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends BusinessHour >_P init(final _P _product) {
            _product.from = this.from;
            _product.until = this.until;
            return _product;
        }

        /**
         * Sets the new value of "from" (any previous value will be replaced)
         * 
         * @param from
         *     New value of the "from" property.
         */
        public BusinessHour.Builder<_B> withFrom(final String from) {
            this.from = from;
            return this;
        }

        /**
         * Sets the new value of "until" (any previous value will be replaced)
         * 
         * @param until
         *     New value of the "until" property.
         */
        public BusinessHour.Builder<_B> withUntil(final String until) {
            this.until = until;
            return this;
        }

        @Override
        public BusinessHour build() {
            if (_storedValue == null) {
                return this.init(new BusinessHour());
            } else {
                return ((BusinessHour) _storedValue);
            }
        }

        public BusinessHour.Builder<_B> copyOf(final BusinessHour _other) {
            _other.copyTo(this);
            return this;
        }

        public BusinessHour.Builder<_B> copyOf(final BusinessHour.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends BusinessHour.Selector<BusinessHour.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static BusinessHour.Select _root() {
            return new BusinessHour.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, BusinessHour.Selector<TRoot, TParent>> from = null;
        private com.kscs.util.jaxb.Selector<TRoot, BusinessHour.Selector<TRoot, TParent>> until = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.from!= null) {
                products.put("from", this.from.init());
            }
            if (this.until!= null) {
                products.put("until", this.until.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, BusinessHour.Selector<TRoot, TParent>> from() {
            return ((this.from == null)?this.from = new com.kscs.util.jaxb.Selector<TRoot, BusinessHour.Selector<TRoot, TParent>>(this._root, this, "from"):this.from);
        }

        public com.kscs.util.jaxb.Selector<TRoot, BusinessHour.Selector<TRoot, TParent>> until() {
            return ((this.until == null)?this.until = new com.kscs.util.jaxb.Selector<TRoot, BusinessHour.Selector<TRoot, TParent>>(this._root, this, "until"):this.until);
        }

    }

}
