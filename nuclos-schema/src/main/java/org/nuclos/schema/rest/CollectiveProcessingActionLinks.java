package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for collective-processing-action-links complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="collective-processing-action-links"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="execute" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *         &lt;element name="info" type="{urn:org.nuclos.schema.rest}rest-link"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collective-processing-action-links", propOrder = {
    "execute",
    "info"
})
public class CollectiveProcessingActionLinks implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected RestLink execute;
    @XmlElement(required = true)
    protected RestLink info;

    /**
     * Gets the value of the execute property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getExecute() {
        return execute;
    }

    /**
     * Sets the value of the execute property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setExecute(RestLink value) {
        this.execute = value;
    }

    /**
     * Gets the value of the info property.
     * 
     * @return
     *     possible object is
     *     {@link RestLink }
     *     
     */
    public RestLink getInfo() {
        return info;
    }

    /**
     * Sets the value of the info property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestLink }
     *     
     */
    public void setInfo(RestLink value) {
        this.info = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            RestLink theExecute;
            theExecute = this.getExecute();
            strategy.appendField(locator, this, "execute", buffer, theExecute);
        }
        {
            RestLink theInfo;
            theInfo = this.getInfo();
            strategy.appendField(locator, this, "info", buffer, theInfo);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollectiveProcessingActionLinks)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollectiveProcessingActionLinks that = ((CollectiveProcessingActionLinks) object);
        {
            RestLink lhsExecute;
            lhsExecute = this.getExecute();
            RestLink rhsExecute;
            rhsExecute = that.getExecute();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "execute", lhsExecute), LocatorUtils.property(thatLocator, "execute", rhsExecute), lhsExecute, rhsExecute)) {
                return false;
            }
        }
        {
            RestLink lhsInfo;
            lhsInfo = this.getInfo();
            RestLink rhsInfo;
            rhsInfo = that.getInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "info", lhsInfo), LocatorUtils.property(thatLocator, "info", rhsInfo), lhsInfo, rhsInfo)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            RestLink theExecute;
            theExecute = this.getExecute();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "execute", theExecute), currentHashCode, theExecute);
        }
        {
            RestLink theInfo;
            theInfo = this.getInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "info", theInfo), currentHashCode, theInfo);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollectiveProcessingActionLinks) {
            final CollectiveProcessingActionLinks copy = ((CollectiveProcessingActionLinks) draftCopy);
            if (this.execute!= null) {
                RestLink sourceExecute;
                sourceExecute = this.getExecute();
                RestLink copyExecute = ((RestLink) strategy.copy(LocatorUtils.property(locator, "execute", sourceExecute), sourceExecute));
                copy.setExecute(copyExecute);
            } else {
                copy.execute = null;
            }
            if (this.info!= null) {
                RestLink sourceInfo;
                sourceInfo = this.getInfo();
                RestLink copyInfo = ((RestLink) strategy.copy(LocatorUtils.property(locator, "info", sourceInfo), sourceInfo));
                copy.setInfo(copyInfo);
            } else {
                copy.info = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollectiveProcessingActionLinks();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingActionLinks.Builder<_B> _other) {
        _other.execute = ((this.execute == null)?null:this.execute.newCopyBuilder(_other));
        _other.info = ((this.info == null)?null:this.info.newCopyBuilder(_other));
    }

    public<_B >CollectiveProcessingActionLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new CollectiveProcessingActionLinks.Builder<_B>(_parentBuilder, this, true);
    }

    public CollectiveProcessingActionLinks.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static CollectiveProcessingActionLinks.Builder<Void> builder() {
        return new CollectiveProcessingActionLinks.Builder<Void>(null, null, false);
    }

    public static<_B >CollectiveProcessingActionLinks.Builder<_B> copyOf(final CollectiveProcessingActionLinks _other) {
        final CollectiveProcessingActionLinks.Builder<_B> _newBuilder = new CollectiveProcessingActionLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingActionLinks.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree executePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("execute"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(executePropertyTree!= null):((executePropertyTree == null)||(!executePropertyTree.isLeaf())))) {
            _other.execute = ((this.execute == null)?null:this.execute.newCopyBuilder(_other, executePropertyTree, _propertyTreeUse));
        }
        final PropertyTree infoPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("info"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(infoPropertyTree!= null):((infoPropertyTree == null)||(!infoPropertyTree.isLeaf())))) {
            _other.info = ((this.info == null)?null:this.info.newCopyBuilder(_other, infoPropertyTree, _propertyTreeUse));
        }
    }

    public<_B >CollectiveProcessingActionLinks.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new CollectiveProcessingActionLinks.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public CollectiveProcessingActionLinks.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >CollectiveProcessingActionLinks.Builder<_B> copyOf(final CollectiveProcessingActionLinks _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final CollectiveProcessingActionLinks.Builder<_B> _newBuilder = new CollectiveProcessingActionLinks.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static CollectiveProcessingActionLinks.Builder<Void> copyExcept(final CollectiveProcessingActionLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static CollectiveProcessingActionLinks.Builder<Void> copyOnly(final CollectiveProcessingActionLinks _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final CollectiveProcessingActionLinks _storedValue;
        private RestLink.Builder<CollectiveProcessingActionLinks.Builder<_B>> execute;
        private RestLink.Builder<CollectiveProcessingActionLinks.Builder<_B>> info;

        public Builder(final _B _parentBuilder, final CollectiveProcessingActionLinks _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.execute = ((_other.execute == null)?null:_other.execute.newCopyBuilder(this));
                    this.info = ((_other.info == null)?null:_other.info.newCopyBuilder(this));
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final CollectiveProcessingActionLinks _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree executePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("execute"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(executePropertyTree!= null):((executePropertyTree == null)||(!executePropertyTree.isLeaf())))) {
                        this.execute = ((_other.execute == null)?null:_other.execute.newCopyBuilder(this, executePropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree infoPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("info"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(infoPropertyTree!= null):((infoPropertyTree == null)||(!infoPropertyTree.isLeaf())))) {
                        this.info = ((_other.info == null)?null:_other.info.newCopyBuilder(this, infoPropertyTree, _propertyTreeUse));
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends CollectiveProcessingActionLinks >_P init(final _P _product) {
            _product.execute = ((this.execute == null)?null:this.execute.build());
            _product.info = ((this.info == null)?null:this.info.build());
            return _product;
        }

        /**
         * Sets the new value of "execute" (any previous value will be replaced)
         * 
         * @param execute
         *     New value of the "execute" property.
         */
        public CollectiveProcessingActionLinks.Builder<_B> withExecute(final RestLink execute) {
            this.execute = ((execute == null)?null:new RestLink.Builder<CollectiveProcessingActionLinks.Builder<_B>>(this, execute, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "execute" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "execute" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends CollectiveProcessingActionLinks.Builder<_B>> withExecute() {
            if (this.execute!= null) {
                return this.execute;
            }
            return this.execute = new RestLink.Builder<CollectiveProcessingActionLinks.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "info" (any previous value will be replaced)
         * 
         * @param info
         *     New value of the "info" property.
         */
        public CollectiveProcessingActionLinks.Builder<_B> withInfo(final RestLink info) {
            this.info = ((info == null)?null:new RestLink.Builder<CollectiveProcessingActionLinks.Builder<_B>>(this, info, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "info" property.
         * Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "info" property.
         *     Use {@link org.nuclos.schema.rest.RestLink.Builder#end()} to return to the current builder.
         */
        public RestLink.Builder<? extends CollectiveProcessingActionLinks.Builder<_B>> withInfo() {
            if (this.info!= null) {
                return this.info;
            }
            return this.info = new RestLink.Builder<CollectiveProcessingActionLinks.Builder<_B>>(this, null, false);
        }

        @Override
        public CollectiveProcessingActionLinks build() {
            if (_storedValue == null) {
                return this.init(new CollectiveProcessingActionLinks());
            } else {
                return ((CollectiveProcessingActionLinks) _storedValue);
            }
        }

        public CollectiveProcessingActionLinks.Builder<_B> copyOf(final CollectiveProcessingActionLinks _other) {
            _other.copyTo(this);
            return this;
        }

        public CollectiveProcessingActionLinks.Builder<_B> copyOf(final CollectiveProcessingActionLinks.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends CollectiveProcessingActionLinks.Selector<CollectiveProcessingActionLinks.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static CollectiveProcessingActionLinks.Select _root() {
            return new CollectiveProcessingActionLinks.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private RestLink.Selector<TRoot, CollectiveProcessingActionLinks.Selector<TRoot, TParent>> execute = null;
        private RestLink.Selector<TRoot, CollectiveProcessingActionLinks.Selector<TRoot, TParent>> info = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.execute!= null) {
                products.put("execute", this.execute.init());
            }
            if (this.info!= null) {
                products.put("info", this.info.init());
            }
            return products;
        }

        public RestLink.Selector<TRoot, CollectiveProcessingActionLinks.Selector<TRoot, TParent>> execute() {
            return ((this.execute == null)?this.execute = new RestLink.Selector<TRoot, CollectiveProcessingActionLinks.Selector<TRoot, TParent>>(this._root, this, "execute"):this.execute);
        }

        public RestLink.Selector<TRoot, CollectiveProcessingActionLinks.Selector<TRoot, TParent>> info() {
            return ((this.info == null)?this.info = new RestLink.Selector<TRoot, CollectiveProcessingActionLinks.Selector<TRoot, TParent>>(this._root, this, "info"):this.info);
        }

    }

}
