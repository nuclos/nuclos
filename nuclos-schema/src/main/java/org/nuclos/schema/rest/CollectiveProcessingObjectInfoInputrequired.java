package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for collective-processing-object-info-inputrequired complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="collective-processing-object-info-inputrequired"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="result" type="{urn:org.nuclos.schema.rest}input-required-result-map-item" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="specification" type="{urn:org.nuclos.schema.rest}input-required-specification"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="applyForMultiEdit" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collective-processing-object-info-inputrequired", propOrder = {
    "result",
    "specification"
})
public class CollectiveProcessingObjectInfoInputrequired implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<InputRequiredResultMapItem> result;
    @XmlElement(required = true)
    protected InputRequiredSpecification specification;
    @XmlAttribute(name = "applyForMultiEdit")
    protected Boolean applyForMultiEdit;

    /**
     * Gets the value of the result property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the result property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InputRequiredResultMapItem }
     * 
     * 
     */
    public List<InputRequiredResultMapItem> getResult() {
        if (result == null) {
            result = new ArrayList<InputRequiredResultMapItem>();
        }
        return this.result;
    }

    /**
     * Gets the value of the specification property.
     * 
     * @return
     *     possible object is
     *     {@link InputRequiredSpecification }
     *     
     */
    public InputRequiredSpecification getSpecification() {
        return specification;
    }

    /**
     * Sets the value of the specification property.
     * 
     * @param value
     *     allowed object is
     *     {@link InputRequiredSpecification }
     *     
     */
    public void setSpecification(InputRequiredSpecification value) {
        this.specification = value;
    }

    /**
     * Gets the value of the applyForMultiEdit property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isApplyForMultiEdit() {
        return applyForMultiEdit;
    }

    /**
     * Sets the value of the applyForMultiEdit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setApplyForMultiEdit(Boolean value) {
        this.applyForMultiEdit = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<InputRequiredResultMapItem> theResult;
            theResult = (((this.result!= null)&&(!this.result.isEmpty()))?this.getResult():null);
            strategy.appendField(locator, this, "result", buffer, theResult);
        }
        {
            InputRequiredSpecification theSpecification;
            theSpecification = this.getSpecification();
            strategy.appendField(locator, this, "specification", buffer, theSpecification);
        }
        {
            Boolean theApplyForMultiEdit;
            theApplyForMultiEdit = this.isApplyForMultiEdit();
            strategy.appendField(locator, this, "applyForMultiEdit", buffer, theApplyForMultiEdit);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CollectiveProcessingObjectInfoInputrequired)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CollectiveProcessingObjectInfoInputrequired that = ((CollectiveProcessingObjectInfoInputrequired) object);
        {
            List<InputRequiredResultMapItem> lhsResult;
            lhsResult = (((this.result!= null)&&(!this.result.isEmpty()))?this.getResult():null);
            List<InputRequiredResultMapItem> rhsResult;
            rhsResult = (((that.result!= null)&&(!that.result.isEmpty()))?that.getResult():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "result", lhsResult), LocatorUtils.property(thatLocator, "result", rhsResult), lhsResult, rhsResult)) {
                return false;
            }
        }
        {
            InputRequiredSpecification lhsSpecification;
            lhsSpecification = this.getSpecification();
            InputRequiredSpecification rhsSpecification;
            rhsSpecification = that.getSpecification();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "specification", lhsSpecification), LocatorUtils.property(thatLocator, "specification", rhsSpecification), lhsSpecification, rhsSpecification)) {
                return false;
            }
        }
        {
            Boolean lhsApplyForMultiEdit;
            lhsApplyForMultiEdit = this.isApplyForMultiEdit();
            Boolean rhsApplyForMultiEdit;
            rhsApplyForMultiEdit = that.isApplyForMultiEdit();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "applyForMultiEdit", lhsApplyForMultiEdit), LocatorUtils.property(thatLocator, "applyForMultiEdit", rhsApplyForMultiEdit), lhsApplyForMultiEdit, rhsApplyForMultiEdit)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<InputRequiredResultMapItem> theResult;
            theResult = (((this.result!= null)&&(!this.result.isEmpty()))?this.getResult():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "result", theResult), currentHashCode, theResult);
        }
        {
            InputRequiredSpecification theSpecification;
            theSpecification = this.getSpecification();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "specification", theSpecification), currentHashCode, theSpecification);
        }
        {
            Boolean theApplyForMultiEdit;
            theApplyForMultiEdit = this.isApplyForMultiEdit();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "applyForMultiEdit", theApplyForMultiEdit), currentHashCode, theApplyForMultiEdit);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof CollectiveProcessingObjectInfoInputrequired) {
            final CollectiveProcessingObjectInfoInputrequired copy = ((CollectiveProcessingObjectInfoInputrequired) draftCopy);
            if ((this.result!= null)&&(!this.result.isEmpty())) {
                List<InputRequiredResultMapItem> sourceResult;
                sourceResult = (((this.result!= null)&&(!this.result.isEmpty()))?this.getResult():null);
                @SuppressWarnings("unchecked")
                List<InputRequiredResultMapItem> copyResult = ((List<InputRequiredResultMapItem> ) strategy.copy(LocatorUtils.property(locator, "result", sourceResult), sourceResult));
                copy.result = null;
                if (copyResult!= null) {
                    List<InputRequiredResultMapItem> uniqueResultl = copy.getResult();
                    uniqueResultl.addAll(copyResult);
                }
            } else {
                copy.result = null;
            }
            if (this.specification!= null) {
                InputRequiredSpecification sourceSpecification;
                sourceSpecification = this.getSpecification();
                InputRequiredSpecification copySpecification = ((InputRequiredSpecification) strategy.copy(LocatorUtils.property(locator, "specification", sourceSpecification), sourceSpecification));
                copy.setSpecification(copySpecification);
            } else {
                copy.specification = null;
            }
            if (this.applyForMultiEdit!= null) {
                Boolean sourceApplyForMultiEdit;
                sourceApplyForMultiEdit = this.isApplyForMultiEdit();
                Boolean copyApplyForMultiEdit = ((Boolean) strategy.copy(LocatorUtils.property(locator, "applyForMultiEdit", sourceApplyForMultiEdit), sourceApplyForMultiEdit));
                copy.setApplyForMultiEdit(copyApplyForMultiEdit);
            } else {
                copy.applyForMultiEdit = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new CollectiveProcessingObjectInfoInputrequired();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingObjectInfoInputrequired.Builder<_B> _other) {
        if (this.result == null) {
            _other.result = null;
        } else {
            _other.result = new ArrayList<InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>>();
            for (InputRequiredResultMapItem _item: this.result) {
                _other.result.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.specification = ((this.specification == null)?null:this.specification.newCopyBuilder(_other));
        _other.applyForMultiEdit = this.applyForMultiEdit;
    }

    public<_B >CollectiveProcessingObjectInfoInputrequired.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new CollectiveProcessingObjectInfoInputrequired.Builder<_B>(_parentBuilder, this, true);
    }

    public CollectiveProcessingObjectInfoInputrequired.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static CollectiveProcessingObjectInfoInputrequired.Builder<Void> builder() {
        return new CollectiveProcessingObjectInfoInputrequired.Builder<Void>(null, null, false);
    }

    public static<_B >CollectiveProcessingObjectInfoInputrequired.Builder<_B> copyOf(final CollectiveProcessingObjectInfoInputrequired _other) {
        final CollectiveProcessingObjectInfoInputrequired.Builder<_B> _newBuilder = new CollectiveProcessingObjectInfoInputrequired.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final CollectiveProcessingObjectInfoInputrequired.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree resultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("result"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resultPropertyTree!= null):((resultPropertyTree == null)||(!resultPropertyTree.isLeaf())))) {
            if (this.result == null) {
                _other.result = null;
            } else {
                _other.result = new ArrayList<InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>>();
                for (InputRequiredResultMapItem _item: this.result) {
                    _other.result.add(((_item == null)?null:_item.newCopyBuilder(_other, resultPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree specificationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("specification"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(specificationPropertyTree!= null):((specificationPropertyTree == null)||(!specificationPropertyTree.isLeaf())))) {
            _other.specification = ((this.specification == null)?null:this.specification.newCopyBuilder(_other, specificationPropertyTree, _propertyTreeUse));
        }
        final PropertyTree applyForMultiEditPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("applyForMultiEdit"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(applyForMultiEditPropertyTree!= null):((applyForMultiEditPropertyTree == null)||(!applyForMultiEditPropertyTree.isLeaf())))) {
            _other.applyForMultiEdit = this.applyForMultiEdit;
        }
    }

    public<_B >CollectiveProcessingObjectInfoInputrequired.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new CollectiveProcessingObjectInfoInputrequired.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public CollectiveProcessingObjectInfoInputrequired.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >CollectiveProcessingObjectInfoInputrequired.Builder<_B> copyOf(final CollectiveProcessingObjectInfoInputrequired _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final CollectiveProcessingObjectInfoInputrequired.Builder<_B> _newBuilder = new CollectiveProcessingObjectInfoInputrequired.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static CollectiveProcessingObjectInfoInputrequired.Builder<Void> copyExcept(final CollectiveProcessingObjectInfoInputrequired _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static CollectiveProcessingObjectInfoInputrequired.Builder<Void> copyOnly(final CollectiveProcessingObjectInfoInputrequired _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final CollectiveProcessingObjectInfoInputrequired _storedValue;
        private List<InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>> result;
        private InputRequiredSpecification.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>> specification;
        private Boolean applyForMultiEdit;

        public Builder(final _B _parentBuilder, final CollectiveProcessingObjectInfoInputrequired _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.result == null) {
                        this.result = null;
                    } else {
                        this.result = new ArrayList<InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>>();
                        for (InputRequiredResultMapItem _item: _other.result) {
                            this.result.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.specification = ((_other.specification == null)?null:_other.specification.newCopyBuilder(this));
                    this.applyForMultiEdit = _other.applyForMultiEdit;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final CollectiveProcessingObjectInfoInputrequired _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree resultPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("result"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resultPropertyTree!= null):((resultPropertyTree == null)||(!resultPropertyTree.isLeaf())))) {
                        if (_other.result == null) {
                            this.result = null;
                        } else {
                            this.result = new ArrayList<InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>>();
                            for (InputRequiredResultMapItem _item: _other.result) {
                                this.result.add(((_item == null)?null:_item.newCopyBuilder(this, resultPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree specificationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("specification"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(specificationPropertyTree!= null):((specificationPropertyTree == null)||(!specificationPropertyTree.isLeaf())))) {
                        this.specification = ((_other.specification == null)?null:_other.specification.newCopyBuilder(this, specificationPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree applyForMultiEditPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("applyForMultiEdit"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(applyForMultiEditPropertyTree!= null):((applyForMultiEditPropertyTree == null)||(!applyForMultiEditPropertyTree.isLeaf())))) {
                        this.applyForMultiEdit = _other.applyForMultiEdit;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends CollectiveProcessingObjectInfoInputrequired >_P init(final _P _product) {
            if (this.result!= null) {
                final List<InputRequiredResultMapItem> result = new ArrayList<InputRequiredResultMapItem>(this.result.size());
                for (InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>> _item: this.result) {
                    result.add(_item.build());
                }
                _product.result = result;
            }
            _product.specification = ((this.specification == null)?null:this.specification.build());
            _product.applyForMultiEdit = this.applyForMultiEdit;
            return _product;
        }

        /**
         * Adds the given items to the value of "result"
         * 
         * @param result
         *     Items to add to the value of the "result" property
         */
        public CollectiveProcessingObjectInfoInputrequired.Builder<_B> addResult(final Iterable<? extends InputRequiredResultMapItem> result) {
            if (result!= null) {
                if (this.result == null) {
                    this.result = new ArrayList<InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>>();
                }
                for (InputRequiredResultMapItem _item: result) {
                    this.result.add(new InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "result" (any previous value will be replaced)
         * 
         * @param result
         *     New value of the "result" property.
         */
        public CollectiveProcessingObjectInfoInputrequired.Builder<_B> withResult(final Iterable<? extends InputRequiredResultMapItem> result) {
            if (this.result!= null) {
                this.result.clear();
            }
            return addResult(result);
        }

        /**
         * Adds the given items to the value of "result"
         * 
         * @param result
         *     Items to add to the value of the "result" property
         */
        public CollectiveProcessingObjectInfoInputrequired.Builder<_B> addResult(InputRequiredResultMapItem... result) {
            addResult(Arrays.asList(result));
            return this;
        }

        /**
         * Sets the new value of "result" (any previous value will be replaced)
         * 
         * @param result
         *     New value of the "result" property.
         */
        public CollectiveProcessingObjectInfoInputrequired.Builder<_B> withResult(InputRequiredResultMapItem... result) {
            withResult(Arrays.asList(result));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Result" property.
         * Use {@link org.nuclos.schema.rest.InputRequiredResultMapItem.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Result" property.
         *     Use {@link org.nuclos.schema.rest.InputRequiredResultMapItem.Builder#end()} to return to the current builder.
         */
        public InputRequiredResultMapItem.Builder<? extends CollectiveProcessingObjectInfoInputrequired.Builder<_B>> addResult() {
            if (this.result == null) {
                this.result = new ArrayList<InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>>();
            }
            final InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>> result_Builder = new InputRequiredResultMapItem.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>(this, null, false);
            this.result.add(result_Builder);
            return result_Builder;
        }

        /**
         * Sets the new value of "specification" (any previous value will be replaced)
         * 
         * @param specification
         *     New value of the "specification" property.
         */
        public CollectiveProcessingObjectInfoInputrequired.Builder<_B> withSpecification(final InputRequiredSpecification specification) {
            this.specification = ((specification == null)?null:new InputRequiredSpecification.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>(this, specification, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "specification" property.
         * Use {@link org.nuclos.schema.rest.InputRequiredSpecification.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "specification" property.
         *     Use {@link org.nuclos.schema.rest.InputRequiredSpecification.Builder#end()} to return to the current builder.
         */
        public InputRequiredSpecification.Builder<? extends CollectiveProcessingObjectInfoInputrequired.Builder<_B>> withSpecification() {
            if (this.specification!= null) {
                return this.specification;
            }
            return this.specification = new InputRequiredSpecification.Builder<CollectiveProcessingObjectInfoInputrequired.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "applyForMultiEdit" (any previous value will be replaced)
         * 
         * @param applyForMultiEdit
         *     New value of the "applyForMultiEdit" property.
         */
        public CollectiveProcessingObjectInfoInputrequired.Builder<_B> withApplyForMultiEdit(final Boolean applyForMultiEdit) {
            this.applyForMultiEdit = applyForMultiEdit;
            return this;
        }

        @Override
        public CollectiveProcessingObjectInfoInputrequired build() {
            if (_storedValue == null) {
                return this.init(new CollectiveProcessingObjectInfoInputrequired());
            } else {
                return ((CollectiveProcessingObjectInfoInputrequired) _storedValue);
            }
        }

        public CollectiveProcessingObjectInfoInputrequired.Builder<_B> copyOf(final CollectiveProcessingObjectInfoInputrequired _other) {
            _other.copyTo(this);
            return this;
        }

        public CollectiveProcessingObjectInfoInputrequired.Builder<_B> copyOf(final CollectiveProcessingObjectInfoInputrequired.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends CollectiveProcessingObjectInfoInputrequired.Selector<CollectiveProcessingObjectInfoInputrequired.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static CollectiveProcessingObjectInfoInputrequired.Select _root() {
            return new CollectiveProcessingObjectInfoInputrequired.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private InputRequiredResultMapItem.Selector<TRoot, CollectiveProcessingObjectInfoInputrequired.Selector<TRoot, TParent>> result = null;
        private InputRequiredSpecification.Selector<TRoot, CollectiveProcessingObjectInfoInputrequired.Selector<TRoot, TParent>> specification = null;
        private com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfoInputrequired.Selector<TRoot, TParent>> applyForMultiEdit = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.result!= null) {
                products.put("result", this.result.init());
            }
            if (this.specification!= null) {
                products.put("specification", this.specification.init());
            }
            if (this.applyForMultiEdit!= null) {
                products.put("applyForMultiEdit", this.applyForMultiEdit.init());
            }
            return products;
        }

        public InputRequiredResultMapItem.Selector<TRoot, CollectiveProcessingObjectInfoInputrequired.Selector<TRoot, TParent>> result() {
            return ((this.result == null)?this.result = new InputRequiredResultMapItem.Selector<TRoot, CollectiveProcessingObjectInfoInputrequired.Selector<TRoot, TParent>>(this._root, this, "result"):this.result);
        }

        public InputRequiredSpecification.Selector<TRoot, CollectiveProcessingObjectInfoInputrequired.Selector<TRoot, TParent>> specification() {
            return ((this.specification == null)?this.specification = new InputRequiredSpecification.Selector<TRoot, CollectiveProcessingObjectInfoInputrequired.Selector<TRoot, TParent>>(this._root, this, "specification"):this.specification);
        }

        public com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfoInputrequired.Selector<TRoot, TParent>> applyForMultiEdit() {
            return ((this.applyForMultiEdit == null)?this.applyForMultiEdit = new com.kscs.util.jaxb.Selector<TRoot, CollectiveProcessingObjectInfoInputrequired.Selector<TRoot, TParent>>(this._root, this, "applyForMultiEdit"):this.applyForMultiEdit);
        }

    }

}
