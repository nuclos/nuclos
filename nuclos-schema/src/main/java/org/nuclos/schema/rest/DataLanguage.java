package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for data-language complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="data-language"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="datalanguage" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="datalanguageLabel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="primary" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "data-language")
public class DataLanguage implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "datalanguage", required = true)
    protected String datalanguage;
    @XmlAttribute(name = "datalanguageLabel")
    protected String datalanguageLabel;
    @XmlAttribute(name = "primary")
    protected Boolean primary;

    /**
     * Gets the value of the datalanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatalanguage() {
        return datalanguage;
    }

    /**
     * Sets the value of the datalanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatalanguage(String value) {
        this.datalanguage = value;
    }

    /**
     * Gets the value of the datalanguageLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatalanguageLabel() {
        return datalanguageLabel;
    }

    /**
     * Sets the value of the datalanguageLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatalanguageLabel(String value) {
        this.datalanguageLabel = value;
    }

    /**
     * Gets the value of the primary property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPrimary() {
        return primary;
    }

    /**
     * Sets the value of the primary property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPrimary(Boolean value) {
        this.primary = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theDatalanguage;
            theDatalanguage = this.getDatalanguage();
            strategy.appendField(locator, this, "datalanguage", buffer, theDatalanguage);
        }
        {
            String theDatalanguageLabel;
            theDatalanguageLabel = this.getDatalanguageLabel();
            strategy.appendField(locator, this, "datalanguageLabel", buffer, theDatalanguageLabel);
        }
        {
            Boolean thePrimary;
            thePrimary = this.isPrimary();
            strategy.appendField(locator, this, "primary", buffer, thePrimary);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DataLanguage)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final DataLanguage that = ((DataLanguage) object);
        {
            String lhsDatalanguage;
            lhsDatalanguage = this.getDatalanguage();
            String rhsDatalanguage;
            rhsDatalanguage = that.getDatalanguage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "datalanguage", lhsDatalanguage), LocatorUtils.property(thatLocator, "datalanguage", rhsDatalanguage), lhsDatalanguage, rhsDatalanguage)) {
                return false;
            }
        }
        {
            String lhsDatalanguageLabel;
            lhsDatalanguageLabel = this.getDatalanguageLabel();
            String rhsDatalanguageLabel;
            rhsDatalanguageLabel = that.getDatalanguageLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "datalanguageLabel", lhsDatalanguageLabel), LocatorUtils.property(thatLocator, "datalanguageLabel", rhsDatalanguageLabel), lhsDatalanguageLabel, rhsDatalanguageLabel)) {
                return false;
            }
        }
        {
            Boolean lhsPrimary;
            lhsPrimary = this.isPrimary();
            Boolean rhsPrimary;
            rhsPrimary = that.isPrimary();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "primary", lhsPrimary), LocatorUtils.property(thatLocator, "primary", rhsPrimary), lhsPrimary, rhsPrimary)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theDatalanguage;
            theDatalanguage = this.getDatalanguage();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "datalanguage", theDatalanguage), currentHashCode, theDatalanguage);
        }
        {
            String theDatalanguageLabel;
            theDatalanguageLabel = this.getDatalanguageLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "datalanguageLabel", theDatalanguageLabel), currentHashCode, theDatalanguageLabel);
        }
        {
            Boolean thePrimary;
            thePrimary = this.isPrimary();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "primary", thePrimary), currentHashCode, thePrimary);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof DataLanguage) {
            final DataLanguage copy = ((DataLanguage) draftCopy);
            if (this.datalanguage!= null) {
                String sourceDatalanguage;
                sourceDatalanguage = this.getDatalanguage();
                String copyDatalanguage = ((String) strategy.copy(LocatorUtils.property(locator, "datalanguage", sourceDatalanguage), sourceDatalanguage));
                copy.setDatalanguage(copyDatalanguage);
            } else {
                copy.datalanguage = null;
            }
            if (this.datalanguageLabel!= null) {
                String sourceDatalanguageLabel;
                sourceDatalanguageLabel = this.getDatalanguageLabel();
                String copyDatalanguageLabel = ((String) strategy.copy(LocatorUtils.property(locator, "datalanguageLabel", sourceDatalanguageLabel), sourceDatalanguageLabel));
                copy.setDatalanguageLabel(copyDatalanguageLabel);
            } else {
                copy.datalanguageLabel = null;
            }
            if (this.primary!= null) {
                Boolean sourcePrimary;
                sourcePrimary = this.isPrimary();
                Boolean copyPrimary = ((Boolean) strategy.copy(LocatorUtils.property(locator, "primary", sourcePrimary), sourcePrimary));
                copy.setPrimary(copyPrimary);
            } else {
                copy.primary = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new DataLanguage();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final DataLanguage.Builder<_B> _other) {
        _other.datalanguage = this.datalanguage;
        _other.datalanguageLabel = this.datalanguageLabel;
        _other.primary = this.primary;
    }

    public<_B >DataLanguage.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new DataLanguage.Builder<_B>(_parentBuilder, this, true);
    }

    public DataLanguage.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static DataLanguage.Builder<Void> builder() {
        return new DataLanguage.Builder<Void>(null, null, false);
    }

    public static<_B >DataLanguage.Builder<_B> copyOf(final DataLanguage _other) {
        final DataLanguage.Builder<_B> _newBuilder = new DataLanguage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final DataLanguage.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree datalanguagePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("datalanguage"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(datalanguagePropertyTree!= null):((datalanguagePropertyTree == null)||(!datalanguagePropertyTree.isLeaf())))) {
            _other.datalanguage = this.datalanguage;
        }
        final PropertyTree datalanguageLabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("datalanguageLabel"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(datalanguageLabelPropertyTree!= null):((datalanguageLabelPropertyTree == null)||(!datalanguageLabelPropertyTree.isLeaf())))) {
            _other.datalanguageLabel = this.datalanguageLabel;
        }
        final PropertyTree primaryPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("primary"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(primaryPropertyTree!= null):((primaryPropertyTree == null)||(!primaryPropertyTree.isLeaf())))) {
            _other.primary = this.primary;
        }
    }

    public<_B >DataLanguage.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new DataLanguage.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public DataLanguage.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >DataLanguage.Builder<_B> copyOf(final DataLanguage _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final DataLanguage.Builder<_B> _newBuilder = new DataLanguage.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static DataLanguage.Builder<Void> copyExcept(final DataLanguage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static DataLanguage.Builder<Void> copyOnly(final DataLanguage _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final DataLanguage _storedValue;
        private String datalanguage;
        private String datalanguageLabel;
        private Boolean primary;

        public Builder(final _B _parentBuilder, final DataLanguage _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.datalanguage = _other.datalanguage;
                    this.datalanguageLabel = _other.datalanguageLabel;
                    this.primary = _other.primary;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final DataLanguage _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree datalanguagePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("datalanguage"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(datalanguagePropertyTree!= null):((datalanguagePropertyTree == null)||(!datalanguagePropertyTree.isLeaf())))) {
                        this.datalanguage = _other.datalanguage;
                    }
                    final PropertyTree datalanguageLabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("datalanguageLabel"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(datalanguageLabelPropertyTree!= null):((datalanguageLabelPropertyTree == null)||(!datalanguageLabelPropertyTree.isLeaf())))) {
                        this.datalanguageLabel = _other.datalanguageLabel;
                    }
                    final PropertyTree primaryPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("primary"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(primaryPropertyTree!= null):((primaryPropertyTree == null)||(!primaryPropertyTree.isLeaf())))) {
                        this.primary = _other.primary;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends DataLanguage >_P init(final _P _product) {
            _product.datalanguage = this.datalanguage;
            _product.datalanguageLabel = this.datalanguageLabel;
            _product.primary = this.primary;
            return _product;
        }

        /**
         * Sets the new value of "datalanguage" (any previous value will be replaced)
         * 
         * @param datalanguage
         *     New value of the "datalanguage" property.
         */
        public DataLanguage.Builder<_B> withDatalanguage(final String datalanguage) {
            this.datalanguage = datalanguage;
            return this;
        }

        /**
         * Sets the new value of "datalanguageLabel" (any previous value will be replaced)
         * 
         * @param datalanguageLabel
         *     New value of the "datalanguageLabel" property.
         */
        public DataLanguage.Builder<_B> withDatalanguageLabel(final String datalanguageLabel) {
            this.datalanguageLabel = datalanguageLabel;
            return this;
        }

        /**
         * Sets the new value of "primary" (any previous value will be replaced)
         * 
         * @param primary
         *     New value of the "primary" property.
         */
        public DataLanguage.Builder<_B> withPrimary(final Boolean primary) {
            this.primary = primary;
            return this;
        }

        @Override
        public DataLanguage build() {
            if (_storedValue == null) {
                return this.init(new DataLanguage());
            } else {
                return ((DataLanguage) _storedValue);
            }
        }

        public DataLanguage.Builder<_B> copyOf(final DataLanguage _other) {
            _other.copyTo(this);
            return this;
        }

        public DataLanguage.Builder<_B> copyOf(final DataLanguage.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends DataLanguage.Selector<DataLanguage.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static DataLanguage.Select _root() {
            return new DataLanguage.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, DataLanguage.Selector<TRoot, TParent>> datalanguage = null;
        private com.kscs.util.jaxb.Selector<TRoot, DataLanguage.Selector<TRoot, TParent>> datalanguageLabel = null;
        private com.kscs.util.jaxb.Selector<TRoot, DataLanguage.Selector<TRoot, TParent>> primary = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.datalanguage!= null) {
                products.put("datalanguage", this.datalanguage.init());
            }
            if (this.datalanguageLabel!= null) {
                products.put("datalanguageLabel", this.datalanguageLabel.init());
            }
            if (this.primary!= null) {
                products.put("primary", this.primary.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, DataLanguage.Selector<TRoot, TParent>> datalanguage() {
            return ((this.datalanguage == null)?this.datalanguage = new com.kscs.util.jaxb.Selector<TRoot, DataLanguage.Selector<TRoot, TParent>>(this._root, this, "datalanguage"):this.datalanguage);
        }

        public com.kscs.util.jaxb.Selector<TRoot, DataLanguage.Selector<TRoot, TParent>> datalanguageLabel() {
            return ((this.datalanguageLabel == null)?this.datalanguageLabel = new com.kscs.util.jaxb.Selector<TRoot, DataLanguage.Selector<TRoot, TParent>>(this._root, this, "datalanguageLabel"):this.datalanguageLabel);
        }

        public com.kscs.util.jaxb.Selector<TRoot, DataLanguage.Selector<TRoot, TParent>> primary() {
            return ((this.primary == null)?this.primary = new com.kscs.util.jaxb.Selector<TRoot, DataLanguage.Selector<TRoot, TParent>>(this._root, this, "primary"):this.primary);
        }

    }

}
