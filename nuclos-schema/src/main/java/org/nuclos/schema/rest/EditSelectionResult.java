package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for edit-selection-result complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="edit-selection-result"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="attributes" type="{urn:org.nuclos.schema.rest}edit-attribute-map" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "edit-selection-result", propOrder = {
    "attributes"
})
public class EditSelectionResult implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<EditAttributeMap> attributes;

    /**
     * Gets the value of the attributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EditAttributeMap }
     * 
     * 
     */
    public List<EditAttributeMap> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<EditAttributeMap>();
        }
        return this.attributes;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<EditAttributeMap> theAttributes;
            theAttributes = (((this.attributes!= null)&&(!this.attributes.isEmpty()))?this.getAttributes():null);
            strategy.appendField(locator, this, "attributes", buffer, theAttributes);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof EditSelectionResult)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EditSelectionResult that = ((EditSelectionResult) object);
        {
            List<EditAttributeMap> lhsAttributes;
            lhsAttributes = (((this.attributes!= null)&&(!this.attributes.isEmpty()))?this.getAttributes():null);
            List<EditAttributeMap> rhsAttributes;
            rhsAttributes = (((that.attributes!= null)&&(!that.attributes.isEmpty()))?that.getAttributes():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributes", lhsAttributes), LocatorUtils.property(thatLocator, "attributes", rhsAttributes), lhsAttributes, rhsAttributes)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<EditAttributeMap> theAttributes;
            theAttributes = (((this.attributes!= null)&&(!this.attributes.isEmpty()))?this.getAttributes():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attributes", theAttributes), currentHashCode, theAttributes);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof EditSelectionResult) {
            final EditSelectionResult copy = ((EditSelectionResult) draftCopy);
            if ((this.attributes!= null)&&(!this.attributes.isEmpty())) {
                List<EditAttributeMap> sourceAttributes;
                sourceAttributes = (((this.attributes!= null)&&(!this.attributes.isEmpty()))?this.getAttributes():null);
                @SuppressWarnings("unchecked")
                List<EditAttributeMap> copyAttributes = ((List<EditAttributeMap> ) strategy.copy(LocatorUtils.property(locator, "attributes", sourceAttributes), sourceAttributes));
                copy.attributes = null;
                if (copyAttributes!= null) {
                    List<EditAttributeMap> uniqueAttributesl = copy.getAttributes();
                    uniqueAttributesl.addAll(copyAttributes);
                }
            } else {
                copy.attributes = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new EditSelectionResult();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EditSelectionResult.Builder<_B> _other) {
        if (this.attributes == null) {
            _other.attributes = null;
        } else {
            _other.attributes = new ArrayList<EditAttributeMap.Builder<EditSelectionResult.Builder<_B>>>();
            for (EditAttributeMap _item: this.attributes) {
                _other.attributes.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >EditSelectionResult.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new EditSelectionResult.Builder<_B>(_parentBuilder, this, true);
    }

    public EditSelectionResult.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static EditSelectionResult.Builder<Void> builder() {
        return new EditSelectionResult.Builder<Void>(null, null, false);
    }

    public static<_B >EditSelectionResult.Builder<_B> copyOf(final EditSelectionResult _other) {
        final EditSelectionResult.Builder<_B> _newBuilder = new EditSelectionResult.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final EditSelectionResult.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree attributesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attributes"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attributesPropertyTree!= null):((attributesPropertyTree == null)||(!attributesPropertyTree.isLeaf())))) {
            if (this.attributes == null) {
                _other.attributes = null;
            } else {
                _other.attributes = new ArrayList<EditAttributeMap.Builder<EditSelectionResult.Builder<_B>>>();
                for (EditAttributeMap _item: this.attributes) {
                    _other.attributes.add(((_item == null)?null:_item.newCopyBuilder(_other, attributesPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >EditSelectionResult.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new EditSelectionResult.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public EditSelectionResult.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >EditSelectionResult.Builder<_B> copyOf(final EditSelectionResult _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final EditSelectionResult.Builder<_B> _newBuilder = new EditSelectionResult.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static EditSelectionResult.Builder<Void> copyExcept(final EditSelectionResult _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static EditSelectionResult.Builder<Void> copyOnly(final EditSelectionResult _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final EditSelectionResult _storedValue;
        private List<EditAttributeMap.Builder<EditSelectionResult.Builder<_B>>> attributes;

        public Builder(final _B _parentBuilder, final EditSelectionResult _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.attributes == null) {
                        this.attributes = null;
                    } else {
                        this.attributes = new ArrayList<EditAttributeMap.Builder<EditSelectionResult.Builder<_B>>>();
                        for (EditAttributeMap _item: _other.attributes) {
                            this.attributes.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final EditSelectionResult _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree attributesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("attributes"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(attributesPropertyTree!= null):((attributesPropertyTree == null)||(!attributesPropertyTree.isLeaf())))) {
                        if (_other.attributes == null) {
                            this.attributes = null;
                        } else {
                            this.attributes = new ArrayList<EditAttributeMap.Builder<EditSelectionResult.Builder<_B>>>();
                            for (EditAttributeMap _item: _other.attributes) {
                                this.attributes.add(((_item == null)?null:_item.newCopyBuilder(this, attributesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends EditSelectionResult >_P init(final _P _product) {
            if (this.attributes!= null) {
                final List<EditAttributeMap> attributes = new ArrayList<EditAttributeMap>(this.attributes.size());
                for (EditAttributeMap.Builder<EditSelectionResult.Builder<_B>> _item: this.attributes) {
                    attributes.add(_item.build());
                }
                _product.attributes = attributes;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "attributes"
         * 
         * @param attributes
         *     Items to add to the value of the "attributes" property
         */
        public EditSelectionResult.Builder<_B> addAttributes(final Iterable<? extends EditAttributeMap> attributes) {
            if (attributes!= null) {
                if (this.attributes == null) {
                    this.attributes = new ArrayList<EditAttributeMap.Builder<EditSelectionResult.Builder<_B>>>();
                }
                for (EditAttributeMap _item: attributes) {
                    this.attributes.add(new EditAttributeMap.Builder<EditSelectionResult.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "attributes" (any previous value will be replaced)
         * 
         * @param attributes
         *     New value of the "attributes" property.
         */
        public EditSelectionResult.Builder<_B> withAttributes(final Iterable<? extends EditAttributeMap> attributes) {
            if (this.attributes!= null) {
                this.attributes.clear();
            }
            return addAttributes(attributes);
        }

        /**
         * Adds the given items to the value of "attributes"
         * 
         * @param attributes
         *     Items to add to the value of the "attributes" property
         */
        public EditSelectionResult.Builder<_B> addAttributes(EditAttributeMap... attributes) {
            addAttributes(Arrays.asList(attributes));
            return this;
        }

        /**
         * Sets the new value of "attributes" (any previous value will be replaced)
         * 
         * @param attributes
         *     New value of the "attributes" property.
         */
        public EditSelectionResult.Builder<_B> withAttributes(EditAttributeMap... attributes) {
            withAttributes(Arrays.asList(attributes));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Attributes" property.
         * Use {@link org.nuclos.schema.rest.EditAttributeMap.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Attributes" property.
         *     Use {@link org.nuclos.schema.rest.EditAttributeMap.Builder#end()} to return to the current builder.
         */
        public EditAttributeMap.Builder<? extends EditSelectionResult.Builder<_B>> addAttributes() {
            if (this.attributes == null) {
                this.attributes = new ArrayList<EditAttributeMap.Builder<EditSelectionResult.Builder<_B>>>();
            }
            final EditAttributeMap.Builder<EditSelectionResult.Builder<_B>> attributes_Builder = new EditAttributeMap.Builder<EditSelectionResult.Builder<_B>>(this, null, false);
            this.attributes.add(attributes_Builder);
            return attributes_Builder;
        }

        @Override
        public EditSelectionResult build() {
            if (_storedValue == null) {
                return this.init(new EditSelectionResult());
            } else {
                return ((EditSelectionResult) _storedValue);
            }
        }

        public EditSelectionResult.Builder<_B> copyOf(final EditSelectionResult _other) {
            _other.copyTo(this);
            return this;
        }

        public EditSelectionResult.Builder<_B> copyOf(final EditSelectionResult.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends EditSelectionResult.Selector<EditSelectionResult.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static EditSelectionResult.Select _root() {
            return new EditSelectionResult.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private EditAttributeMap.Selector<TRoot, EditSelectionResult.Selector<TRoot, TParent>> attributes = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.attributes!= null) {
                products.put("attributes", this.attributes.init());
            }
            return products;
        }

        public EditAttributeMap.Selector<TRoot, EditSelectionResult.Selector<TRoot, TParent>> attributes() {
            return ((this.attributes == null)?this.attributes = new EditAttributeMap.Selector<TRoot, EditSelectionResult.Selector<TRoot, TParent>>(this._root, this, "attributes"):this.attributes);
        }

    }

}
