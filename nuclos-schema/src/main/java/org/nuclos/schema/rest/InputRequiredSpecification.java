package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for input-required-specification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="input-required-specification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="options" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="key" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="message" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="title" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="defaultoption" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "input-required-specification", propOrder = {
    "options"
})
public class InputRequiredSpecification implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected List<String> options;
    @XmlAttribute(name = "key")
    protected String key;
    @XmlAttribute(name = "message")
    protected String message;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "defaultoption")
    protected String defaultoption;

    /**
     * Gets the value of the options property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the options property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOptions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOptions() {
        if (options == null) {
            options = new ArrayList<String>();
        }
        return this.options;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the defaultoption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultoption() {
        return defaultoption;
    }

    /**
     * Sets the value of the defaultoption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultoption(String value) {
        this.defaultoption = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<String> theOptions;
            theOptions = (((this.options!= null)&&(!this.options.isEmpty()))?this.getOptions():null);
            strategy.appendField(locator, this, "options", buffer, theOptions);
        }
        {
            String theKey;
            theKey = this.getKey();
            strategy.appendField(locator, this, "key", buffer, theKey);
        }
        {
            String theMessage;
            theMessage = this.getMessage();
            strategy.appendField(locator, this, "message", buffer, theMessage);
        }
        {
            String theTitle;
            theTitle = this.getTitle();
            strategy.appendField(locator, this, "title", buffer, theTitle);
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            String theDefaultoption;
            theDefaultoption = this.getDefaultoption();
            strategy.appendField(locator, this, "defaultoption", buffer, theDefaultoption);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof InputRequiredSpecification)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InputRequiredSpecification that = ((InputRequiredSpecification) object);
        {
            List<String> lhsOptions;
            lhsOptions = (((this.options!= null)&&(!this.options.isEmpty()))?this.getOptions():null);
            List<String> rhsOptions;
            rhsOptions = (((that.options!= null)&&(!that.options.isEmpty()))?that.getOptions():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "options", lhsOptions), LocatorUtils.property(thatLocator, "options", rhsOptions), lhsOptions, rhsOptions)) {
                return false;
            }
        }
        {
            String lhsKey;
            lhsKey = this.getKey();
            String rhsKey;
            rhsKey = that.getKey();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "key", lhsKey), LocatorUtils.property(thatLocator, "key", rhsKey), lhsKey, rhsKey)) {
                return false;
            }
        }
        {
            String lhsMessage;
            lhsMessage = this.getMessage();
            String rhsMessage;
            rhsMessage = that.getMessage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "message", lhsMessage), LocatorUtils.property(thatLocator, "message", rhsMessage), lhsMessage, rhsMessage)) {
                return false;
            }
        }
        {
            String lhsTitle;
            lhsTitle = this.getTitle();
            String rhsTitle;
            rhsTitle = that.getTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "title", lhsTitle), LocatorUtils.property(thatLocator, "title", rhsTitle), lhsTitle, rhsTitle)) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            String lhsDefaultoption;
            lhsDefaultoption = this.getDefaultoption();
            String rhsDefaultoption;
            rhsDefaultoption = that.getDefaultoption();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "defaultoption", lhsDefaultoption), LocatorUtils.property(thatLocator, "defaultoption", rhsDefaultoption), lhsDefaultoption, rhsDefaultoption)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<String> theOptions;
            theOptions = (((this.options!= null)&&(!this.options.isEmpty()))?this.getOptions():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "options", theOptions), currentHashCode, theOptions);
        }
        {
            String theKey;
            theKey = this.getKey();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "key", theKey), currentHashCode, theKey);
        }
        {
            String theMessage;
            theMessage = this.getMessage();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "message", theMessage), currentHashCode, theMessage);
        }
        {
            String theTitle;
            theTitle = this.getTitle();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "title", theTitle), currentHashCode, theTitle);
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            String theDefaultoption;
            theDefaultoption = this.getDefaultoption();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "defaultoption", theDefaultoption), currentHashCode, theDefaultoption);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof InputRequiredSpecification) {
            final InputRequiredSpecification copy = ((InputRequiredSpecification) draftCopy);
            if ((this.options!= null)&&(!this.options.isEmpty())) {
                List<String> sourceOptions;
                sourceOptions = (((this.options!= null)&&(!this.options.isEmpty()))?this.getOptions():null);
                @SuppressWarnings("unchecked")
                List<String> copyOptions = ((List<String> ) strategy.copy(LocatorUtils.property(locator, "options", sourceOptions), sourceOptions));
                copy.options = null;
                if (copyOptions!= null) {
                    List<String> uniqueOptionsl = copy.getOptions();
                    uniqueOptionsl.addAll(copyOptions);
                }
            } else {
                copy.options = null;
            }
            if (this.key!= null) {
                String sourceKey;
                sourceKey = this.getKey();
                String copyKey = ((String) strategy.copy(LocatorUtils.property(locator, "key", sourceKey), sourceKey));
                copy.setKey(copyKey);
            } else {
                copy.key = null;
            }
            if (this.message!= null) {
                String sourceMessage;
                sourceMessage = this.getMessage();
                String copyMessage = ((String) strategy.copy(LocatorUtils.property(locator, "message", sourceMessage), sourceMessage));
                copy.setMessage(copyMessage);
            } else {
                copy.message = null;
            }
            if (this.title!= null) {
                String sourceTitle;
                sourceTitle = this.getTitle();
                String copyTitle = ((String) strategy.copy(LocatorUtils.property(locator, "title", sourceTitle), sourceTitle));
                copy.setTitle(copyTitle);
            } else {
                copy.title = null;
            }
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            if (this.defaultoption!= null) {
                String sourceDefaultoption;
                sourceDefaultoption = this.getDefaultoption();
                String copyDefaultoption = ((String) strategy.copy(LocatorUtils.property(locator, "defaultoption", sourceDefaultoption), sourceDefaultoption));
                copy.setDefaultoption(copyDefaultoption);
            } else {
                copy.defaultoption = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new InputRequiredSpecification();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final InputRequiredSpecification.Builder<_B> _other) {
        if (this.options == null) {
            _other.options = null;
        } else {
            _other.options = new ArrayList<Buildable>();
            for (String _item: this.options) {
                _other.options.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
        _other.key = this.key;
        _other.message = this.message;
        _other.title = this.title;
        _other.type = this.type;
        _other.defaultoption = this.defaultoption;
    }

    public<_B >InputRequiredSpecification.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new InputRequiredSpecification.Builder<_B>(_parentBuilder, this, true);
    }

    public InputRequiredSpecification.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static InputRequiredSpecification.Builder<Void> builder() {
        return new InputRequiredSpecification.Builder<Void>(null, null, false);
    }

    public static<_B >InputRequiredSpecification.Builder<_B> copyOf(final InputRequiredSpecification _other) {
        final InputRequiredSpecification.Builder<_B> _newBuilder = new InputRequiredSpecification.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final InputRequiredSpecification.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree optionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("options"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(optionsPropertyTree!= null):((optionsPropertyTree == null)||(!optionsPropertyTree.isLeaf())))) {
            if (this.options == null) {
                _other.options = null;
            } else {
                _other.options = new ArrayList<Buildable>();
                for (String _item: this.options) {
                    _other.options.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
        final PropertyTree keyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("key"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(keyPropertyTree!= null):((keyPropertyTree == null)||(!keyPropertyTree.isLeaf())))) {
            _other.key = this.key;
        }
        final PropertyTree messagePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("message"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(messagePropertyTree!= null):((messagePropertyTree == null)||(!messagePropertyTree.isLeaf())))) {
            _other.message = this.message;
        }
        final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
            _other.title = this.title;
        }
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree defaultoptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("defaultoption"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(defaultoptionPropertyTree!= null):((defaultoptionPropertyTree == null)||(!defaultoptionPropertyTree.isLeaf())))) {
            _other.defaultoption = this.defaultoption;
        }
    }

    public<_B >InputRequiredSpecification.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new InputRequiredSpecification.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public InputRequiredSpecification.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >InputRequiredSpecification.Builder<_B> copyOf(final InputRequiredSpecification _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final InputRequiredSpecification.Builder<_B> _newBuilder = new InputRequiredSpecification.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static InputRequiredSpecification.Builder<Void> copyExcept(final InputRequiredSpecification _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static InputRequiredSpecification.Builder<Void> copyOnly(final InputRequiredSpecification _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final InputRequiredSpecification _storedValue;
        private List<Buildable> options;
        private String key;
        private String message;
        private String title;
        private String type;
        private String defaultoption;

        public Builder(final _B _parentBuilder, final InputRequiredSpecification _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.options == null) {
                        this.options = null;
                    } else {
                        this.options = new ArrayList<Buildable>();
                        for (String _item: _other.options) {
                            this.options.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                    this.key = _other.key;
                    this.message = _other.message;
                    this.title = _other.title;
                    this.type = _other.type;
                    this.defaultoption = _other.defaultoption;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final InputRequiredSpecification _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree optionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("options"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(optionsPropertyTree!= null):((optionsPropertyTree == null)||(!optionsPropertyTree.isLeaf())))) {
                        if (_other.options == null) {
                            this.options = null;
                        } else {
                            this.options = new ArrayList<Buildable>();
                            for (String _item: _other.options) {
                                this.options.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                    final PropertyTree keyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("key"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(keyPropertyTree!= null):((keyPropertyTree == null)||(!keyPropertyTree.isLeaf())))) {
                        this.key = _other.key;
                    }
                    final PropertyTree messagePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("message"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(messagePropertyTree!= null):((messagePropertyTree == null)||(!messagePropertyTree.isLeaf())))) {
                        this.message = _other.message;
                    }
                    final PropertyTree titlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("title"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(titlePropertyTree!= null):((titlePropertyTree == null)||(!titlePropertyTree.isLeaf())))) {
                        this.title = _other.title;
                    }
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree defaultoptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("defaultoption"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(defaultoptionPropertyTree!= null):((defaultoptionPropertyTree == null)||(!defaultoptionPropertyTree.isLeaf())))) {
                        this.defaultoption = _other.defaultoption;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends InputRequiredSpecification >_P init(final _P _product) {
            if (this.options!= null) {
                final List<String> options = new ArrayList<String>(this.options.size());
                for (Buildable _item: this.options) {
                    options.add(((String) _item.build()));
                }
                _product.options = options;
            }
            _product.key = this.key;
            _product.message = this.message;
            _product.title = this.title;
            _product.type = this.type;
            _product.defaultoption = this.defaultoption;
            return _product;
        }

        /**
         * Adds the given items to the value of "options"
         * 
         * @param options
         *     Items to add to the value of the "options" property
         */
        public InputRequiredSpecification.Builder<_B> addOptions(final Iterable<? extends String> options) {
            if (options!= null) {
                if (this.options == null) {
                    this.options = new ArrayList<Buildable>();
                }
                for (String _item: options) {
                    this.options.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "options" (any previous value will be replaced)
         * 
         * @param options
         *     New value of the "options" property.
         */
        public InputRequiredSpecification.Builder<_B> withOptions(final Iterable<? extends String> options) {
            if (this.options!= null) {
                this.options.clear();
            }
            return addOptions(options);
        }

        /**
         * Adds the given items to the value of "options"
         * 
         * @param options
         *     Items to add to the value of the "options" property
         */
        public InputRequiredSpecification.Builder<_B> addOptions(String... options) {
            addOptions(Arrays.asList(options));
            return this;
        }

        /**
         * Sets the new value of "options" (any previous value will be replaced)
         * 
         * @param options
         *     New value of the "options" property.
         */
        public InputRequiredSpecification.Builder<_B> withOptions(String... options) {
            withOptions(Arrays.asList(options));
            return this;
        }

        /**
         * Sets the new value of "key" (any previous value will be replaced)
         * 
         * @param key
         *     New value of the "key" property.
         */
        public InputRequiredSpecification.Builder<_B> withKey(final String key) {
            this.key = key;
            return this;
        }

        /**
         * Sets the new value of "message" (any previous value will be replaced)
         * 
         * @param message
         *     New value of the "message" property.
         */
        public InputRequiredSpecification.Builder<_B> withMessage(final String message) {
            this.message = message;
            return this;
        }

        /**
         * Sets the new value of "title" (any previous value will be replaced)
         * 
         * @param title
         *     New value of the "title" property.
         */
        public InputRequiredSpecification.Builder<_B> withTitle(final String title) {
            this.title = title;
            return this;
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public InputRequiredSpecification.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "defaultoption" (any previous value will be replaced)
         * 
         * @param defaultoption
         *     New value of the "defaultoption" property.
         */
        public InputRequiredSpecification.Builder<_B> withDefaultoption(final String defaultoption) {
            this.defaultoption = defaultoption;
            return this;
        }

        @Override
        public InputRequiredSpecification build() {
            if (_storedValue == null) {
                return this.init(new InputRequiredSpecification());
            } else {
                return ((InputRequiredSpecification) _storedValue);
            }
        }

        public InputRequiredSpecification.Builder<_B> copyOf(final InputRequiredSpecification _other) {
            _other.copyTo(this);
            return this;
        }

        public InputRequiredSpecification.Builder<_B> copyOf(final InputRequiredSpecification.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends InputRequiredSpecification.Selector<InputRequiredSpecification.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static InputRequiredSpecification.Select _root() {
            return new InputRequiredSpecification.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> options = null;
        private com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> key = null;
        private com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> message = null;
        private com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> title = null;
        private com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> defaultoption = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.options!= null) {
                products.put("options", this.options.init());
            }
            if (this.key!= null) {
                products.put("key", this.key.init());
            }
            if (this.message!= null) {
                products.put("message", this.message.init());
            }
            if (this.title!= null) {
                products.put("title", this.title.init());
            }
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.defaultoption!= null) {
                products.put("defaultoption", this.defaultoption.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> options() {
            return ((this.options == null)?this.options = new com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>>(this._root, this, "options"):this.options);
        }

        public com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> key() {
            return ((this.key == null)?this.key = new com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>>(this._root, this, "key"):this.key);
        }

        public com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> message() {
            return ((this.message == null)?this.message = new com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>>(this._root, this, "message"):this.message);
        }

        public com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> title() {
            return ((this.title == null)?this.title = new com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>>(this._root, this, "title"):this.title);
        }

        public com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>> defaultoption() {
            return ((this.defaultoption == null)?this.defaultoption = new com.kscs.util.jaxb.Selector<TRoot, InputRequiredSpecification.Selector<TRoot, TParent>>(this._root, this, "defaultoption"):this.defaultoption);
        }

    }

}
