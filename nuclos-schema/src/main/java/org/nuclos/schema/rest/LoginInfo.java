package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for login-info complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="login-info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allowedActions"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="SharePreferences" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="ConfigureCharts" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="ConfigurePerspectives" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="PrintSearchResultList" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="WorkspaceCustomizeEntityAndSubFormColumn" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="bulkEdit" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="collectiveProcessing" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="executeReports" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *                 &lt;attribute name="configureDashboards" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="links" type="{urn:org.nuclos.schema.rest}login-info-links"/&gt;
 *         &lt;element name="mandator" type="{urn:org.nuclos.schema.rest}mandator" minOccurs="0"/&gt;
 *         &lt;element name="mandators" type="{urn:org.nuclos.schema.rest}mandator" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="sessionId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="username" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="locale" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="datalanguage" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ldapActive" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ssoActive" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ssoLogoutUri" type="{http://www.w3.org/2001/XMLSchema}anyURI" /&gt;
 *       &lt;attribute name="maintenanceMode" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="superUser" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="showRichClientCompleted" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="clientIp" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="initialEntity" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "login-info", propOrder = {
    "allowedActions",
    "links",
    "mandator",
    "mandators"
})
public class LoginInfo implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected LoginInfo.AllowedActions allowedActions;
    @XmlElement(required = true)
    protected LoginInfoLinks links;
    protected Mandator mandator;
    protected List<Mandator> mandators;
    @XmlAttribute(name = "sessionId", required = true)
    protected String sessionId;
    @XmlAttribute(name = "username", required = true)
    protected String username;
    @XmlAttribute(name = "locale", required = true)
    protected String locale;
    @XmlAttribute(name = "datalanguage")
    protected String datalanguage;
    @XmlAttribute(name = "ldapActive")
    protected Boolean ldapActive;
    @XmlAttribute(name = "ssoActive")
    protected Boolean ssoActive;
    @XmlAttribute(name = "ssoLogoutUri")
    @XmlSchemaType(name = "anyURI")
    protected String ssoLogoutUri;
    @XmlAttribute(name = "maintenanceMode", required = true)
    protected boolean maintenanceMode;
    @XmlAttribute(name = "superUser", required = true)
    protected boolean superUser;
    @XmlAttribute(name = "showRichClientCompleted")
    protected Boolean showRichClientCompleted;
    @XmlAttribute(name = "clientIp")
    protected String clientIp;
    @XmlAttribute(name = "initialEntity")
    protected String initialEntity;

    /**
     * Gets the value of the allowedActions property.
     * 
     * @return
     *     possible object is
     *     {@link LoginInfo.AllowedActions }
     *     
     */
    public LoginInfo.AllowedActions getAllowedActions() {
        return allowedActions;
    }

    /**
     * Sets the value of the allowedActions property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoginInfo.AllowedActions }
     *     
     */
    public void setAllowedActions(LoginInfo.AllowedActions value) {
        this.allowedActions = value;
    }

    /**
     * Gets the value of the links property.
     * 
     * @return
     *     possible object is
     *     {@link LoginInfoLinks }
     *     
     */
    public LoginInfoLinks getLinks() {
        return links;
    }

    /**
     * Sets the value of the links property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoginInfoLinks }
     *     
     */
    public void setLinks(LoginInfoLinks value) {
        this.links = value;
    }

    /**
     * Gets the value of the mandator property.
     * 
     * @return
     *     possible object is
     *     {@link Mandator }
     *     
     */
    public Mandator getMandator() {
        return mandator;
    }

    /**
     * Sets the value of the mandator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Mandator }
     *     
     */
    public void setMandator(Mandator value) {
        this.mandator = value;
    }

    /**
     * Gets the value of the mandators property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mandators property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMandators().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Mandator }
     * 
     * 
     */
    public List<Mandator> getMandators() {
        if (mandators == null) {
            mandators = new ArrayList<Mandator>();
        }
        return this.mandators;
    }

    /**
     * Gets the value of the sessionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the value of the sessionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionId(String value) {
        this.sessionId = value;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Gets the value of the locale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Sets the value of the locale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocale(String value) {
        this.locale = value;
    }

    /**
     * Gets the value of the datalanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatalanguage() {
        return datalanguage;
    }

    /**
     * Sets the value of the datalanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatalanguage(String value) {
        this.datalanguage = value;
    }

    /**
     * Gets the value of the ldapActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLdapActive() {
        return ldapActive;
    }

    /**
     * Sets the value of the ldapActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLdapActive(Boolean value) {
        this.ldapActive = value;
    }

    /**
     * Gets the value of the ssoActive property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSsoActive() {
        return ssoActive;
    }

    /**
     * Sets the value of the ssoActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSsoActive(Boolean value) {
        this.ssoActive = value;
    }

    /**
     * Gets the value of the ssoLogoutUri property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSsoLogoutUri() {
        return ssoLogoutUri;
    }

    /**
     * Sets the value of the ssoLogoutUri property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSsoLogoutUri(String value) {
        this.ssoLogoutUri = value;
    }

    /**
     * Gets the value of the maintenanceMode property.
     * 
     */
    public boolean isMaintenanceMode() {
        return maintenanceMode;
    }

    /**
     * Sets the value of the maintenanceMode property.
     * 
     */
    public void setMaintenanceMode(boolean value) {
        this.maintenanceMode = value;
    }

    /**
     * Gets the value of the superUser property.
     * 
     */
    public boolean isSuperUser() {
        return superUser;
    }

    /**
     * Sets the value of the superUser property.
     * 
     */
    public void setSuperUser(boolean value) {
        this.superUser = value;
    }

    /**
     * Gets the value of the showRichClientCompleted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowRichClientCompleted() {
        return showRichClientCompleted;
    }

    /**
     * Sets the value of the showRichClientCompleted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowRichClientCompleted(Boolean value) {
        this.showRichClientCompleted = value;
    }

    /**
     * Gets the value of the clientIp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientIp() {
        return clientIp;
    }

    /**
     * Sets the value of the clientIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientIp(String value) {
        this.clientIp = value;
    }

    /**
     * Gets the value of the initialEntity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInitialEntity() {
        return initialEntity;
    }

    /**
     * Sets the value of the initialEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInitialEntity(String value) {
        this.initialEntity = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            LoginInfo.AllowedActions theAllowedActions;
            theAllowedActions = this.getAllowedActions();
            strategy.appendField(locator, this, "allowedActions", buffer, theAllowedActions);
        }
        {
            LoginInfoLinks theLinks;
            theLinks = this.getLinks();
            strategy.appendField(locator, this, "links", buffer, theLinks);
        }
        {
            Mandator theMandator;
            theMandator = this.getMandator();
            strategy.appendField(locator, this, "mandator", buffer, theMandator);
        }
        {
            List<Mandator> theMandators;
            theMandators = (((this.mandators!= null)&&(!this.mandators.isEmpty()))?this.getMandators():null);
            strategy.appendField(locator, this, "mandators", buffer, theMandators);
        }
        {
            String theSessionId;
            theSessionId = this.getSessionId();
            strategy.appendField(locator, this, "sessionId", buffer, theSessionId);
        }
        {
            String theUsername;
            theUsername = this.getUsername();
            strategy.appendField(locator, this, "username", buffer, theUsername);
        }
        {
            String theLocale;
            theLocale = this.getLocale();
            strategy.appendField(locator, this, "locale", buffer, theLocale);
        }
        {
            String theDatalanguage;
            theDatalanguage = this.getDatalanguage();
            strategy.appendField(locator, this, "datalanguage", buffer, theDatalanguage);
        }
        {
            Boolean theLdapActive;
            theLdapActive = this.isLdapActive();
            strategy.appendField(locator, this, "ldapActive", buffer, theLdapActive);
        }
        {
            Boolean theSsoActive;
            theSsoActive = this.isSsoActive();
            strategy.appendField(locator, this, "ssoActive", buffer, theSsoActive);
        }
        {
            String theSsoLogoutUri;
            theSsoLogoutUri = this.getSsoLogoutUri();
            strategy.appendField(locator, this, "ssoLogoutUri", buffer, theSsoLogoutUri);
        }
        {
            boolean theMaintenanceMode;
            theMaintenanceMode = this.isMaintenanceMode();
            strategy.appendField(locator, this, "maintenanceMode", buffer, theMaintenanceMode);
        }
        {
            boolean theSuperUser;
            theSuperUser = this.isSuperUser();
            strategy.appendField(locator, this, "superUser", buffer, theSuperUser);
        }
        {
            Boolean theShowRichClientCompleted;
            theShowRichClientCompleted = this.isShowRichClientCompleted();
            strategy.appendField(locator, this, "showRichClientCompleted", buffer, theShowRichClientCompleted);
        }
        {
            String theClientIp;
            theClientIp = this.getClientIp();
            strategy.appendField(locator, this, "clientIp", buffer, theClientIp);
        }
        {
            String theInitialEntity;
            theInitialEntity = this.getInitialEntity();
            strategy.appendField(locator, this, "initialEntity", buffer, theInitialEntity);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LoginInfo)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LoginInfo that = ((LoginInfo) object);
        {
            LoginInfo.AllowedActions lhsAllowedActions;
            lhsAllowedActions = this.getAllowedActions();
            LoginInfo.AllowedActions rhsAllowedActions;
            rhsAllowedActions = that.getAllowedActions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "allowedActions", lhsAllowedActions), LocatorUtils.property(thatLocator, "allowedActions", rhsAllowedActions), lhsAllowedActions, rhsAllowedActions)) {
                return false;
            }
        }
        {
            LoginInfoLinks lhsLinks;
            lhsLinks = this.getLinks();
            LoginInfoLinks rhsLinks;
            rhsLinks = that.getLinks();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "links", lhsLinks), LocatorUtils.property(thatLocator, "links", rhsLinks), lhsLinks, rhsLinks)) {
                return false;
            }
        }
        {
            Mandator lhsMandator;
            lhsMandator = this.getMandator();
            Mandator rhsMandator;
            rhsMandator = that.getMandator();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mandator", lhsMandator), LocatorUtils.property(thatLocator, "mandator", rhsMandator), lhsMandator, rhsMandator)) {
                return false;
            }
        }
        {
            List<Mandator> lhsMandators;
            lhsMandators = (((this.mandators!= null)&&(!this.mandators.isEmpty()))?this.getMandators():null);
            List<Mandator> rhsMandators;
            rhsMandators = (((that.mandators!= null)&&(!that.mandators.isEmpty()))?that.getMandators():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mandators", lhsMandators), LocatorUtils.property(thatLocator, "mandators", rhsMandators), lhsMandators, rhsMandators)) {
                return false;
            }
        }
        {
            String lhsSessionId;
            lhsSessionId = this.getSessionId();
            String rhsSessionId;
            rhsSessionId = that.getSessionId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sessionId", lhsSessionId), LocatorUtils.property(thatLocator, "sessionId", rhsSessionId), lhsSessionId, rhsSessionId)) {
                return false;
            }
        }
        {
            String lhsUsername;
            lhsUsername = this.getUsername();
            String rhsUsername;
            rhsUsername = that.getUsername();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "username", lhsUsername), LocatorUtils.property(thatLocator, "username", rhsUsername), lhsUsername, rhsUsername)) {
                return false;
            }
        }
        {
            String lhsLocale;
            lhsLocale = this.getLocale();
            String rhsLocale;
            rhsLocale = that.getLocale();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locale", lhsLocale), LocatorUtils.property(thatLocator, "locale", rhsLocale), lhsLocale, rhsLocale)) {
                return false;
            }
        }
        {
            String lhsDatalanguage;
            lhsDatalanguage = this.getDatalanguage();
            String rhsDatalanguage;
            rhsDatalanguage = that.getDatalanguage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "datalanguage", lhsDatalanguage), LocatorUtils.property(thatLocator, "datalanguage", rhsDatalanguage), lhsDatalanguage, rhsDatalanguage)) {
                return false;
            }
        }
        {
            Boolean lhsLdapActive;
            lhsLdapActive = this.isLdapActive();
            Boolean rhsLdapActive;
            rhsLdapActive = that.isLdapActive();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ldapActive", lhsLdapActive), LocatorUtils.property(thatLocator, "ldapActive", rhsLdapActive), lhsLdapActive, rhsLdapActive)) {
                return false;
            }
        }
        {
            Boolean lhsSsoActive;
            lhsSsoActive = this.isSsoActive();
            Boolean rhsSsoActive;
            rhsSsoActive = that.isSsoActive();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ssoActive", lhsSsoActive), LocatorUtils.property(thatLocator, "ssoActive", rhsSsoActive), lhsSsoActive, rhsSsoActive)) {
                return false;
            }
        }
        {
            String lhsSsoLogoutUri;
            lhsSsoLogoutUri = this.getSsoLogoutUri();
            String rhsSsoLogoutUri;
            rhsSsoLogoutUri = that.getSsoLogoutUri();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ssoLogoutUri", lhsSsoLogoutUri), LocatorUtils.property(thatLocator, "ssoLogoutUri", rhsSsoLogoutUri), lhsSsoLogoutUri, rhsSsoLogoutUri)) {
                return false;
            }
        }
        {
            boolean lhsMaintenanceMode;
            lhsMaintenanceMode = this.isMaintenanceMode();
            boolean rhsMaintenanceMode;
            rhsMaintenanceMode = that.isMaintenanceMode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "maintenanceMode", lhsMaintenanceMode), LocatorUtils.property(thatLocator, "maintenanceMode", rhsMaintenanceMode), lhsMaintenanceMode, rhsMaintenanceMode)) {
                return false;
            }
        }
        {
            boolean lhsSuperUser;
            lhsSuperUser = this.isSuperUser();
            boolean rhsSuperUser;
            rhsSuperUser = that.isSuperUser();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "superUser", lhsSuperUser), LocatorUtils.property(thatLocator, "superUser", rhsSuperUser), lhsSuperUser, rhsSuperUser)) {
                return false;
            }
        }
        {
            Boolean lhsShowRichClientCompleted;
            lhsShowRichClientCompleted = this.isShowRichClientCompleted();
            Boolean rhsShowRichClientCompleted;
            rhsShowRichClientCompleted = that.isShowRichClientCompleted();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "showRichClientCompleted", lhsShowRichClientCompleted), LocatorUtils.property(thatLocator, "showRichClientCompleted", rhsShowRichClientCompleted), lhsShowRichClientCompleted, rhsShowRichClientCompleted)) {
                return false;
            }
        }
        {
            String lhsClientIp;
            lhsClientIp = this.getClientIp();
            String rhsClientIp;
            rhsClientIp = that.getClientIp();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clientIp", lhsClientIp), LocatorUtils.property(thatLocator, "clientIp", rhsClientIp), lhsClientIp, rhsClientIp)) {
                return false;
            }
        }
        {
            String lhsInitialEntity;
            lhsInitialEntity = this.getInitialEntity();
            String rhsInitialEntity;
            rhsInitialEntity = that.getInitialEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "initialEntity", lhsInitialEntity), LocatorUtils.property(thatLocator, "initialEntity", rhsInitialEntity), lhsInitialEntity, rhsInitialEntity)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            LoginInfo.AllowedActions theAllowedActions;
            theAllowedActions = this.getAllowedActions();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "allowedActions", theAllowedActions), currentHashCode, theAllowedActions);
        }
        {
            LoginInfoLinks theLinks;
            theLinks = this.getLinks();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "links", theLinks), currentHashCode, theLinks);
        }
        {
            Mandator theMandator;
            theMandator = this.getMandator();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "mandator", theMandator), currentHashCode, theMandator);
        }
        {
            List<Mandator> theMandators;
            theMandators = (((this.mandators!= null)&&(!this.mandators.isEmpty()))?this.getMandators():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "mandators", theMandators), currentHashCode, theMandators);
        }
        {
            String theSessionId;
            theSessionId = this.getSessionId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "sessionId", theSessionId), currentHashCode, theSessionId);
        }
        {
            String theUsername;
            theUsername = this.getUsername();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "username", theUsername), currentHashCode, theUsername);
        }
        {
            String theLocale;
            theLocale = this.getLocale();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locale", theLocale), currentHashCode, theLocale);
        }
        {
            String theDatalanguage;
            theDatalanguage = this.getDatalanguage();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "datalanguage", theDatalanguage), currentHashCode, theDatalanguage);
        }
        {
            Boolean theLdapActive;
            theLdapActive = this.isLdapActive();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ldapActive", theLdapActive), currentHashCode, theLdapActive);
        }
        {
            Boolean theSsoActive;
            theSsoActive = this.isSsoActive();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ssoActive", theSsoActive), currentHashCode, theSsoActive);
        }
        {
            String theSsoLogoutUri;
            theSsoLogoutUri = this.getSsoLogoutUri();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ssoLogoutUri", theSsoLogoutUri), currentHashCode, theSsoLogoutUri);
        }
        {
            boolean theMaintenanceMode;
            theMaintenanceMode = this.isMaintenanceMode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "maintenanceMode", theMaintenanceMode), currentHashCode, theMaintenanceMode);
        }
        {
            boolean theSuperUser;
            theSuperUser = this.isSuperUser();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "superUser", theSuperUser), currentHashCode, theSuperUser);
        }
        {
            Boolean theShowRichClientCompleted;
            theShowRichClientCompleted = this.isShowRichClientCompleted();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "showRichClientCompleted", theShowRichClientCompleted), currentHashCode, theShowRichClientCompleted);
        }
        {
            String theClientIp;
            theClientIp = this.getClientIp();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clientIp", theClientIp), currentHashCode, theClientIp);
        }
        {
            String theInitialEntity;
            theInitialEntity = this.getInitialEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "initialEntity", theInitialEntity), currentHashCode, theInitialEntity);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof LoginInfo) {
            final LoginInfo copy = ((LoginInfo) draftCopy);
            if (this.allowedActions!= null) {
                LoginInfo.AllowedActions sourceAllowedActions;
                sourceAllowedActions = this.getAllowedActions();
                LoginInfo.AllowedActions copyAllowedActions = ((LoginInfo.AllowedActions) strategy.copy(LocatorUtils.property(locator, "allowedActions", sourceAllowedActions), sourceAllowedActions));
                copy.setAllowedActions(copyAllowedActions);
            } else {
                copy.allowedActions = null;
            }
            if (this.links!= null) {
                LoginInfoLinks sourceLinks;
                sourceLinks = this.getLinks();
                LoginInfoLinks copyLinks = ((LoginInfoLinks) strategy.copy(LocatorUtils.property(locator, "links", sourceLinks), sourceLinks));
                copy.setLinks(copyLinks);
            } else {
                copy.links = null;
            }
            if (this.mandator!= null) {
                Mandator sourceMandator;
                sourceMandator = this.getMandator();
                Mandator copyMandator = ((Mandator) strategy.copy(LocatorUtils.property(locator, "mandator", sourceMandator), sourceMandator));
                copy.setMandator(copyMandator);
            } else {
                copy.mandator = null;
            }
            if ((this.mandators!= null)&&(!this.mandators.isEmpty())) {
                List<Mandator> sourceMandators;
                sourceMandators = (((this.mandators!= null)&&(!this.mandators.isEmpty()))?this.getMandators():null);
                @SuppressWarnings("unchecked")
                List<Mandator> copyMandators = ((List<Mandator> ) strategy.copy(LocatorUtils.property(locator, "mandators", sourceMandators), sourceMandators));
                copy.mandators = null;
                if (copyMandators!= null) {
                    List<Mandator> uniqueMandatorsl = copy.getMandators();
                    uniqueMandatorsl.addAll(copyMandators);
                }
            } else {
                copy.mandators = null;
            }
            if (this.sessionId!= null) {
                String sourceSessionId;
                sourceSessionId = this.getSessionId();
                String copySessionId = ((String) strategy.copy(LocatorUtils.property(locator, "sessionId", sourceSessionId), sourceSessionId));
                copy.setSessionId(copySessionId);
            } else {
                copy.sessionId = null;
            }
            if (this.username!= null) {
                String sourceUsername;
                sourceUsername = this.getUsername();
                String copyUsername = ((String) strategy.copy(LocatorUtils.property(locator, "username", sourceUsername), sourceUsername));
                copy.setUsername(copyUsername);
            } else {
                copy.username = null;
            }
            if (this.locale!= null) {
                String sourceLocale;
                sourceLocale = this.getLocale();
                String copyLocale = ((String) strategy.copy(LocatorUtils.property(locator, "locale", sourceLocale), sourceLocale));
                copy.setLocale(copyLocale);
            } else {
                copy.locale = null;
            }
            if (this.datalanguage!= null) {
                String sourceDatalanguage;
                sourceDatalanguage = this.getDatalanguage();
                String copyDatalanguage = ((String) strategy.copy(LocatorUtils.property(locator, "datalanguage", sourceDatalanguage), sourceDatalanguage));
                copy.setDatalanguage(copyDatalanguage);
            } else {
                copy.datalanguage = null;
            }
            if (this.ldapActive!= null) {
                Boolean sourceLdapActive;
                sourceLdapActive = this.isLdapActive();
                Boolean copyLdapActive = ((Boolean) strategy.copy(LocatorUtils.property(locator, "ldapActive", sourceLdapActive), sourceLdapActive));
                copy.setLdapActive(copyLdapActive);
            } else {
                copy.ldapActive = null;
            }
            if (this.ssoActive!= null) {
                Boolean sourceSsoActive;
                sourceSsoActive = this.isSsoActive();
                Boolean copySsoActive = ((Boolean) strategy.copy(LocatorUtils.property(locator, "ssoActive", sourceSsoActive), sourceSsoActive));
                copy.setSsoActive(copySsoActive);
            } else {
                copy.ssoActive = null;
            }
            if (this.ssoLogoutUri!= null) {
                String sourceSsoLogoutUri;
                sourceSsoLogoutUri = this.getSsoLogoutUri();
                String copySsoLogoutUri = ((String) strategy.copy(LocatorUtils.property(locator, "ssoLogoutUri", sourceSsoLogoutUri), sourceSsoLogoutUri));
                copy.setSsoLogoutUri(copySsoLogoutUri);
            } else {
                copy.ssoLogoutUri = null;
            }
            {
                boolean sourceMaintenanceMode;
                sourceMaintenanceMode = this.isMaintenanceMode();
                boolean copyMaintenanceMode = strategy.copy(LocatorUtils.property(locator, "maintenanceMode", sourceMaintenanceMode), sourceMaintenanceMode);
                copy.setMaintenanceMode(copyMaintenanceMode);
            }
            {
                boolean sourceSuperUser;
                sourceSuperUser = this.isSuperUser();
                boolean copySuperUser = strategy.copy(LocatorUtils.property(locator, "superUser", sourceSuperUser), sourceSuperUser);
                copy.setSuperUser(copySuperUser);
            }
            if (this.showRichClientCompleted!= null) {
                Boolean sourceShowRichClientCompleted;
                sourceShowRichClientCompleted = this.isShowRichClientCompleted();
                Boolean copyShowRichClientCompleted = ((Boolean) strategy.copy(LocatorUtils.property(locator, "showRichClientCompleted", sourceShowRichClientCompleted), sourceShowRichClientCompleted));
                copy.setShowRichClientCompleted(copyShowRichClientCompleted);
            } else {
                copy.showRichClientCompleted = null;
            }
            if (this.clientIp!= null) {
                String sourceClientIp;
                sourceClientIp = this.getClientIp();
                String copyClientIp = ((String) strategy.copy(LocatorUtils.property(locator, "clientIp", sourceClientIp), sourceClientIp));
                copy.setClientIp(copyClientIp);
            } else {
                copy.clientIp = null;
            }
            if (this.initialEntity!= null) {
                String sourceInitialEntity;
                sourceInitialEntity = this.getInitialEntity();
                String copyInitialEntity = ((String) strategy.copy(LocatorUtils.property(locator, "initialEntity", sourceInitialEntity), sourceInitialEntity));
                copy.setInitialEntity(copyInitialEntity);
            } else {
                copy.initialEntity = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new LoginInfo();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LoginInfo.Builder<_B> _other) {
        _other.allowedActions = ((this.allowedActions == null)?null:this.allowedActions.newCopyBuilder(_other));
        _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other));
        _other.mandator = ((this.mandator == null)?null:this.mandator.newCopyBuilder(_other));
        if (this.mandators == null) {
            _other.mandators = null;
        } else {
            _other.mandators = new ArrayList<Mandator.Builder<LoginInfo.Builder<_B>>>();
            for (Mandator _item: this.mandators) {
                _other.mandators.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.sessionId = this.sessionId;
        _other.username = this.username;
        _other.locale = this.locale;
        _other.datalanguage = this.datalanguage;
        _other.ldapActive = this.ldapActive;
        _other.ssoActive = this.ssoActive;
        _other.ssoLogoutUri = this.ssoLogoutUri;
        _other.maintenanceMode = this.maintenanceMode;
        _other.superUser = this.superUser;
        _other.showRichClientCompleted = this.showRichClientCompleted;
        _other.clientIp = this.clientIp;
        _other.initialEntity = this.initialEntity;
    }

    public<_B >LoginInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new LoginInfo.Builder<_B>(_parentBuilder, this, true);
    }

    public LoginInfo.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static LoginInfo.Builder<Void> builder() {
        return new LoginInfo.Builder<Void>(null, null, false);
    }

    public static<_B >LoginInfo.Builder<_B> copyOf(final LoginInfo _other) {
        final LoginInfo.Builder<_B> _newBuilder = new LoginInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final LoginInfo.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree allowedActionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("allowedActions"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(allowedActionsPropertyTree!= null):((allowedActionsPropertyTree == null)||(!allowedActionsPropertyTree.isLeaf())))) {
            _other.allowedActions = ((this.allowedActions == null)?null:this.allowedActions.newCopyBuilder(_other, allowedActionsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
            _other.links = ((this.links == null)?null:this.links.newCopyBuilder(_other, linksPropertyTree, _propertyTreeUse));
        }
        final PropertyTree mandatorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mandator"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mandatorPropertyTree!= null):((mandatorPropertyTree == null)||(!mandatorPropertyTree.isLeaf())))) {
            _other.mandator = ((this.mandator == null)?null:this.mandator.newCopyBuilder(_other, mandatorPropertyTree, _propertyTreeUse));
        }
        final PropertyTree mandatorsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mandators"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mandatorsPropertyTree!= null):((mandatorsPropertyTree == null)||(!mandatorsPropertyTree.isLeaf())))) {
            if (this.mandators == null) {
                _other.mandators = null;
            } else {
                _other.mandators = new ArrayList<Mandator.Builder<LoginInfo.Builder<_B>>>();
                for (Mandator _item: this.mandators) {
                    _other.mandators.add(((_item == null)?null:_item.newCopyBuilder(_other, mandatorsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree sessionIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sessionId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sessionIdPropertyTree!= null):((sessionIdPropertyTree == null)||(!sessionIdPropertyTree.isLeaf())))) {
            _other.sessionId = this.sessionId;
        }
        final PropertyTree usernamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("username"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(usernamePropertyTree!= null):((usernamePropertyTree == null)||(!usernamePropertyTree.isLeaf())))) {
            _other.username = this.username;
        }
        final PropertyTree localePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("locale"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(localePropertyTree!= null):((localePropertyTree == null)||(!localePropertyTree.isLeaf())))) {
            _other.locale = this.locale;
        }
        final PropertyTree datalanguagePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("datalanguage"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(datalanguagePropertyTree!= null):((datalanguagePropertyTree == null)||(!datalanguagePropertyTree.isLeaf())))) {
            _other.datalanguage = this.datalanguage;
        }
        final PropertyTree ldapActivePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ldapActive"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ldapActivePropertyTree!= null):((ldapActivePropertyTree == null)||(!ldapActivePropertyTree.isLeaf())))) {
            _other.ldapActive = this.ldapActive;
        }
        final PropertyTree ssoActivePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ssoActive"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ssoActivePropertyTree!= null):((ssoActivePropertyTree == null)||(!ssoActivePropertyTree.isLeaf())))) {
            _other.ssoActive = this.ssoActive;
        }
        final PropertyTree ssoLogoutUriPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ssoLogoutUri"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ssoLogoutUriPropertyTree!= null):((ssoLogoutUriPropertyTree == null)||(!ssoLogoutUriPropertyTree.isLeaf())))) {
            _other.ssoLogoutUri = this.ssoLogoutUri;
        }
        final PropertyTree maintenanceModePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("maintenanceMode"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(maintenanceModePropertyTree!= null):((maintenanceModePropertyTree == null)||(!maintenanceModePropertyTree.isLeaf())))) {
            _other.maintenanceMode = this.maintenanceMode;
        }
        final PropertyTree superUserPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("superUser"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(superUserPropertyTree!= null):((superUserPropertyTree == null)||(!superUserPropertyTree.isLeaf())))) {
            _other.superUser = this.superUser;
        }
        final PropertyTree showRichClientCompletedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("showRichClientCompleted"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(showRichClientCompletedPropertyTree!= null):((showRichClientCompletedPropertyTree == null)||(!showRichClientCompletedPropertyTree.isLeaf())))) {
            _other.showRichClientCompleted = this.showRichClientCompleted;
        }
        final PropertyTree clientIpPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clientIp"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clientIpPropertyTree!= null):((clientIpPropertyTree == null)||(!clientIpPropertyTree.isLeaf())))) {
            _other.clientIp = this.clientIp;
        }
        final PropertyTree initialEntityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("initialEntity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(initialEntityPropertyTree!= null):((initialEntityPropertyTree == null)||(!initialEntityPropertyTree.isLeaf())))) {
            _other.initialEntity = this.initialEntity;
        }
    }

    public<_B >LoginInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new LoginInfo.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public LoginInfo.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >LoginInfo.Builder<_B> copyOf(final LoginInfo _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final LoginInfo.Builder<_B> _newBuilder = new LoginInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static LoginInfo.Builder<Void> copyExcept(final LoginInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static LoginInfo.Builder<Void> copyOnly(final LoginInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="SharePreferences" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="ConfigureCharts" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="ConfigurePerspectives" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="PrintSearchResultList" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="WorkspaceCustomizeEntityAndSubFormColumn" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="bulkEdit" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="collectiveProcessing" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="executeReports" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *       &lt;attribute name="configureDashboards" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AllowedActions implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
    {

        private final static long serialVersionUID = 1L;
        @XmlAttribute(name = "SharePreferences")
        protected Boolean sharePreferences;
        @XmlAttribute(name = "ConfigureCharts")
        protected Boolean configureCharts;
        @XmlAttribute(name = "ConfigurePerspectives")
        protected Boolean configurePerspectives;
        @XmlAttribute(name = "PrintSearchResultList")
        protected Boolean printSearchResultList;
        @XmlAttribute(name = "WorkspaceCustomizeEntityAndSubFormColumn")
        protected Boolean workspaceCustomizeEntityAndSubFormColumn;
        @XmlAttribute(name = "bulkEdit")
        protected Boolean bulkEdit;
        @XmlAttribute(name = "collectiveProcessing")
        protected Boolean collectiveProcessing;
        @XmlAttribute(name = "executeReports")
        protected Boolean executeReports;
        @XmlAttribute(name = "configureDashboards")
        protected Boolean configureDashboards;

        /**
         * Gets the value of the sharePreferences property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        @JsonProperty("SharePreferences")
        public Boolean isSharePreferences() {
            return sharePreferences;
        }

        /**
         * Sets the value of the sharePreferences property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setSharePreferences(Boolean value) {
            this.sharePreferences = value;
        }

        /**
         * Gets the value of the configureCharts property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        @JsonProperty("ConfigureCharts")
        public Boolean isConfigureCharts() {
            return configureCharts;
        }

        /**
         * Sets the value of the configureCharts property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setConfigureCharts(Boolean value) {
            this.configureCharts = value;
        }

        /**
         * Gets the value of the configurePerspectives property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        @JsonProperty("ConfigurePerspectives")
        public Boolean isConfigurePerspectives() {
            return configurePerspectives;
        }

        /**
         * Sets the value of the configurePerspectives property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setConfigurePerspectives(Boolean value) {
            this.configurePerspectives = value;
        }

        /**
         * Gets the value of the printSearchResultList property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        @JsonProperty("PrintSearchResultList")
        public Boolean isPrintSearchResultList() {
            return printSearchResultList;
        }

        /**
         * Sets the value of the printSearchResultList property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setPrintSearchResultList(Boolean value) {
            this.printSearchResultList = value;
        }

        /**
         * Gets the value of the workspaceCustomizeEntityAndSubFormColumn property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        @JsonProperty("WorkspaceCustomizeEntityAndSubFormColumn")
        public Boolean isWorkspaceCustomizeEntityAndSubFormColumn() {
            return workspaceCustomizeEntityAndSubFormColumn;
        }

        /**
         * Sets the value of the workspaceCustomizeEntityAndSubFormColumn property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setWorkspaceCustomizeEntityAndSubFormColumn(Boolean value) {
            this.workspaceCustomizeEntityAndSubFormColumn = value;
        }

        /**
         * Gets the value of the bulkEdit property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        @JsonProperty("BulkEdit")
        public Boolean isBulkEdit() {
            return bulkEdit;
        }

        /**
         * Sets the value of the bulkEdit property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setBulkEdit(Boolean value) {
            this.bulkEdit = value;
        }

        /**
         * Gets the value of the collectiveProcessing property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        @JsonProperty("CollectiveProcessing")
        public Boolean isCollectiveProcessing() {
            return collectiveProcessing;
        }

        /**
         * Sets the value of the collectiveProcessing property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setCollectiveProcessing(Boolean value) {
            this.collectiveProcessing = value;
        }

        /**
         * Gets the value of the executeReports property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        @JsonProperty("ExecuteReports")
        public Boolean isExecuteReports() {
            return executeReports;
        }

        /**
         * Sets the value of the executeReports property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setExecuteReports(Boolean value) {
            this.executeReports = value;
        }

        /**
         * Gets the value of the configureDashboards property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        @JsonProperty("ConfigureDashboards")
        public Boolean isConfigureDashboards() {
            return configureDashboards;
        }

        /**
         * Sets the value of the configureDashboards property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setConfigureDashboards(Boolean value) {
            this.configureDashboards = value;
        }

        public String toString() {
            final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
            final StringBuilder buffer = new StringBuilder();
            append(null, buffer, strategy);
            return buffer.toString();
        }

        public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
            strategy.appendStart(locator, this, buffer);
            appendFields(locator, buffer, strategy);
            strategy.appendEnd(locator, this, buffer);
            return buffer;
        }

        public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
            {
                Boolean theSharePreferences;
                theSharePreferences = this.isSharePreferences();
                strategy.appendField(locator, this, "sharePreferences", buffer, theSharePreferences);
            }
            {
                Boolean theConfigureCharts;
                theConfigureCharts = this.isConfigureCharts();
                strategy.appendField(locator, this, "configureCharts", buffer, theConfigureCharts);
            }
            {
                Boolean theConfigurePerspectives;
                theConfigurePerspectives = this.isConfigurePerspectives();
                strategy.appendField(locator, this, "configurePerspectives", buffer, theConfigurePerspectives);
            }
            {
                Boolean thePrintSearchResultList;
                thePrintSearchResultList = this.isPrintSearchResultList();
                strategy.appendField(locator, this, "printSearchResultList", buffer, thePrintSearchResultList);
            }
            {
                Boolean theWorkspaceCustomizeEntityAndSubFormColumn;
                theWorkspaceCustomizeEntityAndSubFormColumn = this.isWorkspaceCustomizeEntityAndSubFormColumn();
                strategy.appendField(locator, this, "workspaceCustomizeEntityAndSubFormColumn", buffer, theWorkspaceCustomizeEntityAndSubFormColumn);
            }
            {
                Boolean theBulkEdit;
                theBulkEdit = this.isBulkEdit();
                strategy.appendField(locator, this, "bulkEdit", buffer, theBulkEdit);
            }
            {
                Boolean theCollectiveProcessing;
                theCollectiveProcessing = this.isCollectiveProcessing();
                strategy.appendField(locator, this, "collectiveProcessing", buffer, theCollectiveProcessing);
            }
            {
                Boolean theExecuteReports;
                theExecuteReports = this.isExecuteReports();
                strategy.appendField(locator, this, "executeReports", buffer, theExecuteReports);
            }
            {
                Boolean theConfigureDashboards;
                theConfigureDashboards = this.isConfigureDashboards();
                strategy.appendField(locator, this, "configureDashboards", buffer, theConfigureDashboards);
            }
            return buffer;
        }

        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
            if (!(object instanceof LoginInfo.AllowedActions)) {
                return false;
            }
            if (this == object) {
                return true;
            }
            final LoginInfo.AllowedActions that = ((LoginInfo.AllowedActions) object);
            {
                Boolean lhsSharePreferences;
                lhsSharePreferences = this.isSharePreferences();
                Boolean rhsSharePreferences;
                rhsSharePreferences = that.isSharePreferences();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "sharePreferences", lhsSharePreferences), LocatorUtils.property(thatLocator, "sharePreferences", rhsSharePreferences), lhsSharePreferences, rhsSharePreferences)) {
                    return false;
                }
            }
            {
                Boolean lhsConfigureCharts;
                lhsConfigureCharts = this.isConfigureCharts();
                Boolean rhsConfigureCharts;
                rhsConfigureCharts = that.isConfigureCharts();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "configureCharts", lhsConfigureCharts), LocatorUtils.property(thatLocator, "configureCharts", rhsConfigureCharts), lhsConfigureCharts, rhsConfigureCharts)) {
                    return false;
                }
            }
            {
                Boolean lhsConfigurePerspectives;
                lhsConfigurePerspectives = this.isConfigurePerspectives();
                Boolean rhsConfigurePerspectives;
                rhsConfigurePerspectives = that.isConfigurePerspectives();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "configurePerspectives", lhsConfigurePerspectives), LocatorUtils.property(thatLocator, "configurePerspectives", rhsConfigurePerspectives), lhsConfigurePerspectives, rhsConfigurePerspectives)) {
                    return false;
                }
            }
            {
                Boolean lhsPrintSearchResultList;
                lhsPrintSearchResultList = this.isPrintSearchResultList();
                Boolean rhsPrintSearchResultList;
                rhsPrintSearchResultList = that.isPrintSearchResultList();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "printSearchResultList", lhsPrintSearchResultList), LocatorUtils.property(thatLocator, "printSearchResultList", rhsPrintSearchResultList), lhsPrintSearchResultList, rhsPrintSearchResultList)) {
                    return false;
                }
            }
            {
                Boolean lhsWorkspaceCustomizeEntityAndSubFormColumn;
                lhsWorkspaceCustomizeEntityAndSubFormColumn = this.isWorkspaceCustomizeEntityAndSubFormColumn();
                Boolean rhsWorkspaceCustomizeEntityAndSubFormColumn;
                rhsWorkspaceCustomizeEntityAndSubFormColumn = that.isWorkspaceCustomizeEntityAndSubFormColumn();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "workspaceCustomizeEntityAndSubFormColumn", lhsWorkspaceCustomizeEntityAndSubFormColumn), LocatorUtils.property(thatLocator, "workspaceCustomizeEntityAndSubFormColumn", rhsWorkspaceCustomizeEntityAndSubFormColumn), lhsWorkspaceCustomizeEntityAndSubFormColumn, rhsWorkspaceCustomizeEntityAndSubFormColumn)) {
                    return false;
                }
            }
            {
                Boolean lhsBulkEdit;
                lhsBulkEdit = this.isBulkEdit();
                Boolean rhsBulkEdit;
                rhsBulkEdit = that.isBulkEdit();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "bulkEdit", lhsBulkEdit), LocatorUtils.property(thatLocator, "bulkEdit", rhsBulkEdit), lhsBulkEdit, rhsBulkEdit)) {
                    return false;
                }
            }
            {
                Boolean lhsCollectiveProcessing;
                lhsCollectiveProcessing = this.isCollectiveProcessing();
                Boolean rhsCollectiveProcessing;
                rhsCollectiveProcessing = that.isCollectiveProcessing();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "collectiveProcessing", lhsCollectiveProcessing), LocatorUtils.property(thatLocator, "collectiveProcessing", rhsCollectiveProcessing), lhsCollectiveProcessing, rhsCollectiveProcessing)) {
                    return false;
                }
            }
            {
                Boolean lhsExecuteReports;
                lhsExecuteReports = this.isExecuteReports();
                Boolean rhsExecuteReports;
                rhsExecuteReports = that.isExecuteReports();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "executeReports", lhsExecuteReports), LocatorUtils.property(thatLocator, "executeReports", rhsExecuteReports), lhsExecuteReports, rhsExecuteReports)) {
                    return false;
                }
            }
            {
                Boolean lhsConfigureDashboards;
                lhsConfigureDashboards = this.isConfigureDashboards();
                Boolean rhsConfigureDashboards;
                rhsConfigureDashboards = that.isConfigureDashboards();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "configureDashboards", lhsConfigureDashboards), LocatorUtils.property(thatLocator, "configureDashboards", rhsConfigureDashboards), lhsConfigureDashboards, rhsConfigureDashboards)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object object) {
            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
            return equals(null, null, object, strategy);
        }

        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
            int currentHashCode = 1;
            {
                Boolean theSharePreferences;
                theSharePreferences = this.isSharePreferences();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "sharePreferences", theSharePreferences), currentHashCode, theSharePreferences);
            }
            {
                Boolean theConfigureCharts;
                theConfigureCharts = this.isConfigureCharts();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "configureCharts", theConfigureCharts), currentHashCode, theConfigureCharts);
            }
            {
                Boolean theConfigurePerspectives;
                theConfigurePerspectives = this.isConfigurePerspectives();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "configurePerspectives", theConfigurePerspectives), currentHashCode, theConfigurePerspectives);
            }
            {
                Boolean thePrintSearchResultList;
                thePrintSearchResultList = this.isPrintSearchResultList();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "printSearchResultList", thePrintSearchResultList), currentHashCode, thePrintSearchResultList);
            }
            {
                Boolean theWorkspaceCustomizeEntityAndSubFormColumn;
                theWorkspaceCustomizeEntityAndSubFormColumn = this.isWorkspaceCustomizeEntityAndSubFormColumn();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "workspaceCustomizeEntityAndSubFormColumn", theWorkspaceCustomizeEntityAndSubFormColumn), currentHashCode, theWorkspaceCustomizeEntityAndSubFormColumn);
            }
            {
                Boolean theBulkEdit;
                theBulkEdit = this.isBulkEdit();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bulkEdit", theBulkEdit), currentHashCode, theBulkEdit);
            }
            {
                Boolean theCollectiveProcessing;
                theCollectiveProcessing = this.isCollectiveProcessing();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "collectiveProcessing", theCollectiveProcessing), currentHashCode, theCollectiveProcessing);
            }
            {
                Boolean theExecuteReports;
                theExecuteReports = this.isExecuteReports();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "executeReports", theExecuteReports), currentHashCode, theExecuteReports);
            }
            {
                Boolean theConfigureDashboards;
                theConfigureDashboards = this.isConfigureDashboards();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "configureDashboards", theConfigureDashboards), currentHashCode, theConfigureDashboards);
            }
            return currentHashCode;
        }

        public int hashCode() {
            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
            return this.hashCode(null, strategy);
        }

        public Object clone() {
            return copyTo(createNewInstance());
        }

        public Object copyTo(Object target) {
            final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
            return copyTo(null, target, strategy);
        }

        public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
            final Object draftCopy = ((target == null)?createNewInstance():target);
            if (draftCopy instanceof LoginInfo.AllowedActions) {
                final LoginInfo.AllowedActions copy = ((LoginInfo.AllowedActions) draftCopy);
                if (this.sharePreferences!= null) {
                    Boolean sourceSharePreferences;
                    sourceSharePreferences = this.isSharePreferences();
                    Boolean copySharePreferences = ((Boolean) strategy.copy(LocatorUtils.property(locator, "sharePreferences", sourceSharePreferences), sourceSharePreferences));
                    copy.setSharePreferences(copySharePreferences);
                } else {
                    copy.sharePreferences = null;
                }
                if (this.configureCharts!= null) {
                    Boolean sourceConfigureCharts;
                    sourceConfigureCharts = this.isConfigureCharts();
                    Boolean copyConfigureCharts = ((Boolean) strategy.copy(LocatorUtils.property(locator, "configureCharts", sourceConfigureCharts), sourceConfigureCharts));
                    copy.setConfigureCharts(copyConfigureCharts);
                } else {
                    copy.configureCharts = null;
                }
                if (this.configurePerspectives!= null) {
                    Boolean sourceConfigurePerspectives;
                    sourceConfigurePerspectives = this.isConfigurePerspectives();
                    Boolean copyConfigurePerspectives = ((Boolean) strategy.copy(LocatorUtils.property(locator, "configurePerspectives", sourceConfigurePerspectives), sourceConfigurePerspectives));
                    copy.setConfigurePerspectives(copyConfigurePerspectives);
                } else {
                    copy.configurePerspectives = null;
                }
                if (this.printSearchResultList!= null) {
                    Boolean sourcePrintSearchResultList;
                    sourcePrintSearchResultList = this.isPrintSearchResultList();
                    Boolean copyPrintSearchResultList = ((Boolean) strategy.copy(LocatorUtils.property(locator, "printSearchResultList", sourcePrintSearchResultList), sourcePrintSearchResultList));
                    copy.setPrintSearchResultList(copyPrintSearchResultList);
                } else {
                    copy.printSearchResultList = null;
                }
                if (this.workspaceCustomizeEntityAndSubFormColumn!= null) {
                    Boolean sourceWorkspaceCustomizeEntityAndSubFormColumn;
                    sourceWorkspaceCustomizeEntityAndSubFormColumn = this.isWorkspaceCustomizeEntityAndSubFormColumn();
                    Boolean copyWorkspaceCustomizeEntityAndSubFormColumn = ((Boolean) strategy.copy(LocatorUtils.property(locator, "workspaceCustomizeEntityAndSubFormColumn", sourceWorkspaceCustomizeEntityAndSubFormColumn), sourceWorkspaceCustomizeEntityAndSubFormColumn));
                    copy.setWorkspaceCustomizeEntityAndSubFormColumn(copyWorkspaceCustomizeEntityAndSubFormColumn);
                } else {
                    copy.workspaceCustomizeEntityAndSubFormColumn = null;
                }
                if (this.bulkEdit!= null) {
                    Boolean sourceBulkEdit;
                    sourceBulkEdit = this.isBulkEdit();
                    Boolean copyBulkEdit = ((Boolean) strategy.copy(LocatorUtils.property(locator, "bulkEdit", sourceBulkEdit), sourceBulkEdit));
                    copy.setBulkEdit(copyBulkEdit);
                } else {
                    copy.bulkEdit = null;
                }
                if (this.collectiveProcessing!= null) {
                    Boolean sourceCollectiveProcessing;
                    sourceCollectiveProcessing = this.isCollectiveProcessing();
                    Boolean copyCollectiveProcessing = ((Boolean) strategy.copy(LocatorUtils.property(locator, "collectiveProcessing", sourceCollectiveProcessing), sourceCollectiveProcessing));
                    copy.setCollectiveProcessing(copyCollectiveProcessing);
                } else {
                    copy.collectiveProcessing = null;
                }
                if (this.executeReports!= null) {
                    Boolean sourceExecuteReports;
                    sourceExecuteReports = this.isExecuteReports();
                    Boolean copyExecuteReports = ((Boolean) strategy.copy(LocatorUtils.property(locator, "executeReports", sourceExecuteReports), sourceExecuteReports));
                    copy.setExecuteReports(copyExecuteReports);
                } else {
                    copy.executeReports = null;
                }
                if (this.configureDashboards!= null) {
                    Boolean sourceConfigureDashboards;
                    sourceConfigureDashboards = this.isConfigureDashboards();
                    Boolean copyConfigureDashboards = ((Boolean) strategy.copy(LocatorUtils.property(locator, "configureDashboards", sourceConfigureDashboards), sourceConfigureDashboards));
                    copy.setConfigureDashboards(copyConfigureDashboards);
                } else {
                    copy.configureDashboards = null;
                }
            }
            return draftCopy;
        }

        public Object createNewInstance() {
            return new LoginInfo.AllowedActions();
        }

        /**
         * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
         * 
         * @param _other
         *     A builder instance to which the state of this object will be copied.
         */
        public<_B >void copyTo(final LoginInfo.AllowedActions.Builder<_B> _other) {
            _other.sharePreferences = this.sharePreferences;
            _other.configureCharts = this.configureCharts;
            _other.configurePerspectives = this.configurePerspectives;
            _other.printSearchResultList = this.printSearchResultList;
            _other.workspaceCustomizeEntityAndSubFormColumn = this.workspaceCustomizeEntityAndSubFormColumn;
            _other.bulkEdit = this.bulkEdit;
            _other.collectiveProcessing = this.collectiveProcessing;
            _other.executeReports = this.executeReports;
            _other.configureDashboards = this.configureDashboards;
        }

        public<_B >LoginInfo.AllowedActions.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
            return new LoginInfo.AllowedActions.Builder<_B>(_parentBuilder, this, true);
        }

        public LoginInfo.AllowedActions.Builder<Void> newCopyBuilder() {
            return newCopyBuilder(null);
        }

        public static LoginInfo.AllowedActions.Builder<Void> builder() {
            return new LoginInfo.AllowedActions.Builder<Void>(null, null, false);
        }

        public static<_B >LoginInfo.AllowedActions.Builder<_B> copyOf(final LoginInfo.AllowedActions _other) {
            final LoginInfo.AllowedActions.Builder<_B> _newBuilder = new LoginInfo.AllowedActions.Builder<_B>(null, null, false);
            _other.copyTo(_newBuilder);
            return _newBuilder;
        }

        /**
         * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
         * 
         * @param _other
         *     A builder instance to which the state of this object will be copied.
         */
        public<_B >void copyTo(final LoginInfo.AllowedActions.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            final PropertyTree sharePreferencesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sharePreferences"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sharePreferencesPropertyTree!= null):((sharePreferencesPropertyTree == null)||(!sharePreferencesPropertyTree.isLeaf())))) {
                _other.sharePreferences = this.sharePreferences;
            }
            final PropertyTree configureChartsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("configureCharts"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(configureChartsPropertyTree!= null):((configureChartsPropertyTree == null)||(!configureChartsPropertyTree.isLeaf())))) {
                _other.configureCharts = this.configureCharts;
            }
            final PropertyTree configurePerspectivesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("configurePerspectives"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(configurePerspectivesPropertyTree!= null):((configurePerspectivesPropertyTree == null)||(!configurePerspectivesPropertyTree.isLeaf())))) {
                _other.configurePerspectives = this.configurePerspectives;
            }
            final PropertyTree printSearchResultListPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("printSearchResultList"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(printSearchResultListPropertyTree!= null):((printSearchResultListPropertyTree == null)||(!printSearchResultListPropertyTree.isLeaf())))) {
                _other.printSearchResultList = this.printSearchResultList;
            }
            final PropertyTree workspaceCustomizeEntityAndSubFormColumnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("workspaceCustomizeEntityAndSubFormColumn"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(workspaceCustomizeEntityAndSubFormColumnPropertyTree!= null):((workspaceCustomizeEntityAndSubFormColumnPropertyTree == null)||(!workspaceCustomizeEntityAndSubFormColumnPropertyTree.isLeaf())))) {
                _other.workspaceCustomizeEntityAndSubFormColumn = this.workspaceCustomizeEntityAndSubFormColumn;
            }
            final PropertyTree bulkEditPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bulkEdit"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bulkEditPropertyTree!= null):((bulkEditPropertyTree == null)||(!bulkEditPropertyTree.isLeaf())))) {
                _other.bulkEdit = this.bulkEdit;
            }
            final PropertyTree collectiveProcessingPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("collectiveProcessing"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(collectiveProcessingPropertyTree!= null):((collectiveProcessingPropertyTree == null)||(!collectiveProcessingPropertyTree.isLeaf())))) {
                _other.collectiveProcessing = this.collectiveProcessing;
            }
            final PropertyTree executeReportsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("executeReports"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(executeReportsPropertyTree!= null):((executeReportsPropertyTree == null)||(!executeReportsPropertyTree.isLeaf())))) {
                _other.executeReports = this.executeReports;
            }
            final PropertyTree configureDashboardsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("configureDashboards"));
            if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(configureDashboardsPropertyTree!= null):((configureDashboardsPropertyTree == null)||(!configureDashboardsPropertyTree.isLeaf())))) {
                _other.configureDashboards = this.configureDashboards;
            }
        }

        public<_B >LoginInfo.AllowedActions.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            return new LoginInfo.AllowedActions.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
        }

        public LoginInfo.AllowedActions.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
        }

        public static<_B >LoginInfo.AllowedActions.Builder<_B> copyOf(final LoginInfo.AllowedActions _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            final LoginInfo.AllowedActions.Builder<_B> _newBuilder = new LoginInfo.AllowedActions.Builder<_B>(null, null, false);
            _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
            return _newBuilder;
        }

        public static LoginInfo.AllowedActions.Builder<Void> copyExcept(final LoginInfo.AllowedActions _other, final PropertyTree _propertyTree) {
            return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
        }

        public static LoginInfo.AllowedActions.Builder<Void> copyOnly(final LoginInfo.AllowedActions _other, final PropertyTree _propertyTree) {
            return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
        }

        public static class Builder<_B >implements Buildable
        {

            protected final _B _parentBuilder;
            protected final LoginInfo.AllowedActions _storedValue;
            private Boolean sharePreferences;
            private Boolean configureCharts;
            private Boolean configurePerspectives;
            private Boolean printSearchResultList;
            private Boolean workspaceCustomizeEntityAndSubFormColumn;
            private Boolean bulkEdit;
            private Boolean collectiveProcessing;
            private Boolean executeReports;
            private Boolean configureDashboards;

            public Builder(final _B _parentBuilder, final LoginInfo.AllowedActions _other, final boolean _copy) {
                this._parentBuilder = _parentBuilder;
                if (_other!= null) {
                    if (_copy) {
                        _storedValue = null;
                        this.sharePreferences = _other.sharePreferences;
                        this.configureCharts = _other.configureCharts;
                        this.configurePerspectives = _other.configurePerspectives;
                        this.printSearchResultList = _other.printSearchResultList;
                        this.workspaceCustomizeEntityAndSubFormColumn = _other.workspaceCustomizeEntityAndSubFormColumn;
                        this.bulkEdit = _other.bulkEdit;
                        this.collectiveProcessing = _other.collectiveProcessing;
                        this.executeReports = _other.executeReports;
                        this.configureDashboards = _other.configureDashboards;
                    } else {
                        _storedValue = _other;
                    }
                } else {
                    _storedValue = null;
                }
            }

            public Builder(final _B _parentBuilder, final LoginInfo.AllowedActions _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
                this._parentBuilder = _parentBuilder;
                if (_other!= null) {
                    if (_copy) {
                        _storedValue = null;
                        final PropertyTree sharePreferencesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sharePreferences"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sharePreferencesPropertyTree!= null):((sharePreferencesPropertyTree == null)||(!sharePreferencesPropertyTree.isLeaf())))) {
                            this.sharePreferences = _other.sharePreferences;
                        }
                        final PropertyTree configureChartsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("configureCharts"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(configureChartsPropertyTree!= null):((configureChartsPropertyTree == null)||(!configureChartsPropertyTree.isLeaf())))) {
                            this.configureCharts = _other.configureCharts;
                        }
                        final PropertyTree configurePerspectivesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("configurePerspectives"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(configurePerspectivesPropertyTree!= null):((configurePerspectivesPropertyTree == null)||(!configurePerspectivesPropertyTree.isLeaf())))) {
                            this.configurePerspectives = _other.configurePerspectives;
                        }
                        final PropertyTree printSearchResultListPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("printSearchResultList"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(printSearchResultListPropertyTree!= null):((printSearchResultListPropertyTree == null)||(!printSearchResultListPropertyTree.isLeaf())))) {
                            this.printSearchResultList = _other.printSearchResultList;
                        }
                        final PropertyTree workspaceCustomizeEntityAndSubFormColumnPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("workspaceCustomizeEntityAndSubFormColumn"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(workspaceCustomizeEntityAndSubFormColumnPropertyTree!= null):((workspaceCustomizeEntityAndSubFormColumnPropertyTree == null)||(!workspaceCustomizeEntityAndSubFormColumnPropertyTree.isLeaf())))) {
                            this.workspaceCustomizeEntityAndSubFormColumn = _other.workspaceCustomizeEntityAndSubFormColumn;
                        }
                        final PropertyTree bulkEditPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bulkEdit"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bulkEditPropertyTree!= null):((bulkEditPropertyTree == null)||(!bulkEditPropertyTree.isLeaf())))) {
                            this.bulkEdit = _other.bulkEdit;
                        }
                        final PropertyTree collectiveProcessingPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("collectiveProcessing"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(collectiveProcessingPropertyTree!= null):((collectiveProcessingPropertyTree == null)||(!collectiveProcessingPropertyTree.isLeaf())))) {
                            this.collectiveProcessing = _other.collectiveProcessing;
                        }
                        final PropertyTree executeReportsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("executeReports"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(executeReportsPropertyTree!= null):((executeReportsPropertyTree == null)||(!executeReportsPropertyTree.isLeaf())))) {
                            this.executeReports = _other.executeReports;
                        }
                        final PropertyTree configureDashboardsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("configureDashboards"));
                        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(configureDashboardsPropertyTree!= null):((configureDashboardsPropertyTree == null)||(!configureDashboardsPropertyTree.isLeaf())))) {
                            this.configureDashboards = _other.configureDashboards;
                        }
                    } else {
                        _storedValue = _other;
                    }
                } else {
                    _storedValue = null;
                }
            }

            public _B end() {
                return this._parentBuilder;
            }

            protected<_P extends LoginInfo.AllowedActions >_P init(final _P _product) {
                _product.sharePreferences = this.sharePreferences;
                _product.configureCharts = this.configureCharts;
                _product.configurePerspectives = this.configurePerspectives;
                _product.printSearchResultList = this.printSearchResultList;
                _product.workspaceCustomizeEntityAndSubFormColumn = this.workspaceCustomizeEntityAndSubFormColumn;
                _product.bulkEdit = this.bulkEdit;
                _product.collectiveProcessing = this.collectiveProcessing;
                _product.executeReports = this.executeReports;
                _product.configureDashboards = this.configureDashboards;
                return _product;
            }

            /**
             * Sets the new value of "sharePreferences" (any previous value will be replaced)
             * 
             * @param sharePreferences
             *     New value of the "sharePreferences" property.
             */
            public LoginInfo.AllowedActions.Builder<_B> withSharePreferences(final Boolean sharePreferences) {
                this.sharePreferences = sharePreferences;
                return this;
            }

            /**
             * Sets the new value of "configureCharts" (any previous value will be replaced)
             * 
             * @param configureCharts
             *     New value of the "configureCharts" property.
             */
            public LoginInfo.AllowedActions.Builder<_B> withConfigureCharts(final Boolean configureCharts) {
                this.configureCharts = configureCharts;
                return this;
            }

            /**
             * Sets the new value of "configurePerspectives" (any previous value will be replaced)
             * 
             * @param configurePerspectives
             *     New value of the "configurePerspectives" property.
             */
            public LoginInfo.AllowedActions.Builder<_B> withConfigurePerspectives(final Boolean configurePerspectives) {
                this.configurePerspectives = configurePerspectives;
                return this;
            }

            /**
             * Sets the new value of "printSearchResultList" (any previous value will be replaced)
             * 
             * @param printSearchResultList
             *     New value of the "printSearchResultList" property.
             */
            public LoginInfo.AllowedActions.Builder<_B> withPrintSearchResultList(final Boolean printSearchResultList) {
                this.printSearchResultList = printSearchResultList;
                return this;
            }

            /**
             * Sets the new value of "workspaceCustomizeEntityAndSubFormColumn" (any previous value will be replaced)
             * 
             * @param workspaceCustomizeEntityAndSubFormColumn
             *     New value of the "workspaceCustomizeEntityAndSubFormColumn" property.
             */
            public LoginInfo.AllowedActions.Builder<_B> withWorkspaceCustomizeEntityAndSubFormColumn(final Boolean workspaceCustomizeEntityAndSubFormColumn) {
                this.workspaceCustomizeEntityAndSubFormColumn = workspaceCustomizeEntityAndSubFormColumn;
                return this;
            }

            /**
             * Sets the new value of "bulkEdit" (any previous value will be replaced)
             * 
             * @param bulkEdit
             *     New value of the "bulkEdit" property.
             */
            public LoginInfo.AllowedActions.Builder<_B> withBulkEdit(final Boolean bulkEdit) {
                this.bulkEdit = bulkEdit;
                return this;
            }

            /**
             * Sets the new value of "collectiveProcessing" (any previous value will be replaced)
             * 
             * @param collectiveProcessing
             *     New value of the "collectiveProcessing" property.
             */
            public LoginInfo.AllowedActions.Builder<_B> withCollectiveProcessing(final Boolean collectiveProcessing) {
                this.collectiveProcessing = collectiveProcessing;
                return this;
            }

            /**
             * Sets the new value of "executeReports" (any previous value will be replaced)
             * 
             * @param executeReports
             *     New value of the "executeReports" property.
             */
            public LoginInfo.AllowedActions.Builder<_B> withExecuteReports(final Boolean executeReports) {
                this.executeReports = executeReports;
                return this;
            }

            /**
             * Sets the new value of "configureDashboards" (any previous value will be replaced)
             * 
             * @param configureDashboards
             *     New value of the "configureDashboards" property.
             */
            public LoginInfo.AllowedActions.Builder<_B> withConfigureDashboards(final Boolean configureDashboards) {
                this.configureDashboards = configureDashboards;
                return this;
            }

            @Override
            public LoginInfo.AllowedActions build() {
                if (_storedValue == null) {
                    return this.init(new LoginInfo.AllowedActions());
                } else {
                    return ((LoginInfo.AllowedActions) _storedValue);
                }
            }

            public LoginInfo.AllowedActions.Builder<_B> copyOf(final LoginInfo.AllowedActions _other) {
                _other.copyTo(this);
                return this;
            }

            public LoginInfo.AllowedActions.Builder<_B> copyOf(final LoginInfo.AllowedActions.Builder _other) {
                return copyOf(_other.build());
            }

        }

        public static class Select
            extends LoginInfo.AllowedActions.Selector<LoginInfo.AllowedActions.Select, Void>
        {


            Select() {
                super(null, null, null);
            }

            public static LoginInfo.AllowedActions.Select _root() {
                return new LoginInfo.AllowedActions.Select();
            }

        }

        public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
            extends com.kscs.util.jaxb.Selector<TRoot, TParent>
        {

            private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> sharePreferences = null;
            private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> configureCharts = null;
            private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> configurePerspectives = null;
            private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> printSearchResultList = null;
            private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> workspaceCustomizeEntityAndSubFormColumn = null;
            private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> bulkEdit = null;
            private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> collectiveProcessing = null;
            private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> executeReports = null;
            private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> configureDashboards = null;

            public Selector(final TRoot root, final TParent parent, final String propertyName) {
                super(root, parent, propertyName);
            }

            @Override
            public Map<String, PropertyTree> buildChildren() {
                final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
                products.putAll(super.buildChildren());
                if (this.sharePreferences!= null) {
                    products.put("sharePreferences", this.sharePreferences.init());
                }
                if (this.configureCharts!= null) {
                    products.put("configureCharts", this.configureCharts.init());
                }
                if (this.configurePerspectives!= null) {
                    products.put("configurePerspectives", this.configurePerspectives.init());
                }
                if (this.printSearchResultList!= null) {
                    products.put("printSearchResultList", this.printSearchResultList.init());
                }
                if (this.workspaceCustomizeEntityAndSubFormColumn!= null) {
                    products.put("workspaceCustomizeEntityAndSubFormColumn", this.workspaceCustomizeEntityAndSubFormColumn.init());
                }
                if (this.bulkEdit!= null) {
                    products.put("bulkEdit", this.bulkEdit.init());
                }
                if (this.collectiveProcessing!= null) {
                    products.put("collectiveProcessing", this.collectiveProcessing.init());
                }
                if (this.executeReports!= null) {
                    products.put("executeReports", this.executeReports.init());
                }
                if (this.configureDashboards!= null) {
                    products.put("configureDashboards", this.configureDashboards.init());
                }
                return products;
            }

            public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> sharePreferences() {
                return ((this.sharePreferences == null)?this.sharePreferences = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>>(this._root, this, "sharePreferences"):this.sharePreferences);
            }

            public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> configureCharts() {
                return ((this.configureCharts == null)?this.configureCharts = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>>(this._root, this, "configureCharts"):this.configureCharts);
            }

            public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> configurePerspectives() {
                return ((this.configurePerspectives == null)?this.configurePerspectives = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>>(this._root, this, "configurePerspectives"):this.configurePerspectives);
            }

            public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> printSearchResultList() {
                return ((this.printSearchResultList == null)?this.printSearchResultList = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>>(this._root, this, "printSearchResultList"):this.printSearchResultList);
            }

            public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> workspaceCustomizeEntityAndSubFormColumn() {
                return ((this.workspaceCustomizeEntityAndSubFormColumn == null)?this.workspaceCustomizeEntityAndSubFormColumn = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>>(this._root, this, "workspaceCustomizeEntityAndSubFormColumn"):this.workspaceCustomizeEntityAndSubFormColumn);
            }

            public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> bulkEdit() {
                return ((this.bulkEdit == null)?this.bulkEdit = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>>(this._root, this, "bulkEdit"):this.bulkEdit);
            }

            public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> collectiveProcessing() {
                return ((this.collectiveProcessing == null)?this.collectiveProcessing = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>>(this._root, this, "collectiveProcessing"):this.collectiveProcessing);
            }

            public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> executeReports() {
                return ((this.executeReports == null)?this.executeReports = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>>(this._root, this, "executeReports"):this.executeReports);
            }

            public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>> configureDashboards() {
                return ((this.configureDashboards == null)?this.configureDashboards = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.AllowedActions.Selector<TRoot, TParent>>(this._root, this, "configureDashboards"):this.configureDashboards);
            }

        }

    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final LoginInfo _storedValue;
        private LoginInfo.AllowedActions.Builder<LoginInfo.Builder<_B>> allowedActions;
        private LoginInfoLinks.Builder<LoginInfo.Builder<_B>> links;
        private Mandator.Builder<LoginInfo.Builder<_B>> mandator;
        private List<Mandator.Builder<LoginInfo.Builder<_B>>> mandators;
        private String sessionId;
        private String username;
        private String locale;
        private String datalanguage;
        private Boolean ldapActive;
        private Boolean ssoActive;
        private String ssoLogoutUri;
        private boolean maintenanceMode;
        private boolean superUser;
        private Boolean showRichClientCompleted;
        private String clientIp;
        private String initialEntity;

        public Builder(final _B _parentBuilder, final LoginInfo _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.allowedActions = ((_other.allowedActions == null)?null:_other.allowedActions.newCopyBuilder(this));
                    this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this));
                    this.mandator = ((_other.mandator == null)?null:_other.mandator.newCopyBuilder(this));
                    if (_other.mandators == null) {
                        this.mandators = null;
                    } else {
                        this.mandators = new ArrayList<Mandator.Builder<LoginInfo.Builder<_B>>>();
                        for (Mandator _item: _other.mandators) {
                            this.mandators.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.sessionId = _other.sessionId;
                    this.username = _other.username;
                    this.locale = _other.locale;
                    this.datalanguage = _other.datalanguage;
                    this.ldapActive = _other.ldapActive;
                    this.ssoActive = _other.ssoActive;
                    this.ssoLogoutUri = _other.ssoLogoutUri;
                    this.maintenanceMode = _other.maintenanceMode;
                    this.superUser = _other.superUser;
                    this.showRichClientCompleted = _other.showRichClientCompleted;
                    this.clientIp = _other.clientIp;
                    this.initialEntity = _other.initialEntity;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final LoginInfo _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree allowedActionsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("allowedActions"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(allowedActionsPropertyTree!= null):((allowedActionsPropertyTree == null)||(!allowedActionsPropertyTree.isLeaf())))) {
                        this.allowedActions = ((_other.allowedActions == null)?null:_other.allowedActions.newCopyBuilder(this, allowedActionsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree linksPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("links"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(linksPropertyTree!= null):((linksPropertyTree == null)||(!linksPropertyTree.isLeaf())))) {
                        this.links = ((_other.links == null)?null:_other.links.newCopyBuilder(this, linksPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree mandatorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mandator"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mandatorPropertyTree!= null):((mandatorPropertyTree == null)||(!mandatorPropertyTree.isLeaf())))) {
                        this.mandator = ((_other.mandator == null)?null:_other.mandator.newCopyBuilder(this, mandatorPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree mandatorsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("mandators"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(mandatorsPropertyTree!= null):((mandatorsPropertyTree == null)||(!mandatorsPropertyTree.isLeaf())))) {
                        if (_other.mandators == null) {
                            this.mandators = null;
                        } else {
                            this.mandators = new ArrayList<Mandator.Builder<LoginInfo.Builder<_B>>>();
                            for (Mandator _item: _other.mandators) {
                                this.mandators.add(((_item == null)?null:_item.newCopyBuilder(this, mandatorsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree sessionIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sessionId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sessionIdPropertyTree!= null):((sessionIdPropertyTree == null)||(!sessionIdPropertyTree.isLeaf())))) {
                        this.sessionId = _other.sessionId;
                    }
                    final PropertyTree usernamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("username"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(usernamePropertyTree!= null):((usernamePropertyTree == null)||(!usernamePropertyTree.isLeaf())))) {
                        this.username = _other.username;
                    }
                    final PropertyTree localePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("locale"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(localePropertyTree!= null):((localePropertyTree == null)||(!localePropertyTree.isLeaf())))) {
                        this.locale = _other.locale;
                    }
                    final PropertyTree datalanguagePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("datalanguage"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(datalanguagePropertyTree!= null):((datalanguagePropertyTree == null)||(!datalanguagePropertyTree.isLeaf())))) {
                        this.datalanguage = _other.datalanguage;
                    }
                    final PropertyTree ldapActivePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ldapActive"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ldapActivePropertyTree!= null):((ldapActivePropertyTree == null)||(!ldapActivePropertyTree.isLeaf())))) {
                        this.ldapActive = _other.ldapActive;
                    }
                    final PropertyTree ssoActivePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ssoActive"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ssoActivePropertyTree!= null):((ssoActivePropertyTree == null)||(!ssoActivePropertyTree.isLeaf())))) {
                        this.ssoActive = _other.ssoActive;
                    }
                    final PropertyTree ssoLogoutUriPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ssoLogoutUri"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ssoLogoutUriPropertyTree!= null):((ssoLogoutUriPropertyTree == null)||(!ssoLogoutUriPropertyTree.isLeaf())))) {
                        this.ssoLogoutUri = _other.ssoLogoutUri;
                    }
                    final PropertyTree maintenanceModePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("maintenanceMode"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(maintenanceModePropertyTree!= null):((maintenanceModePropertyTree == null)||(!maintenanceModePropertyTree.isLeaf())))) {
                        this.maintenanceMode = _other.maintenanceMode;
                    }
                    final PropertyTree superUserPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("superUser"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(superUserPropertyTree!= null):((superUserPropertyTree == null)||(!superUserPropertyTree.isLeaf())))) {
                        this.superUser = _other.superUser;
                    }
                    final PropertyTree showRichClientCompletedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("showRichClientCompleted"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(showRichClientCompletedPropertyTree!= null):((showRichClientCompletedPropertyTree == null)||(!showRichClientCompletedPropertyTree.isLeaf())))) {
                        this.showRichClientCompleted = _other.showRichClientCompleted;
                    }
                    final PropertyTree clientIpPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("clientIp"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(clientIpPropertyTree!= null):((clientIpPropertyTree == null)||(!clientIpPropertyTree.isLeaf())))) {
                        this.clientIp = _other.clientIp;
                    }
                    final PropertyTree initialEntityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("initialEntity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(initialEntityPropertyTree!= null):((initialEntityPropertyTree == null)||(!initialEntityPropertyTree.isLeaf())))) {
                        this.initialEntity = _other.initialEntity;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends LoginInfo >_P init(final _P _product) {
            _product.allowedActions = ((this.allowedActions == null)?null:this.allowedActions.build());
            _product.links = ((this.links == null)?null:this.links.build());
            _product.mandator = ((this.mandator == null)?null:this.mandator.build());
            if (this.mandators!= null) {
                final List<Mandator> mandators = new ArrayList<Mandator>(this.mandators.size());
                for (Mandator.Builder<LoginInfo.Builder<_B>> _item: this.mandators) {
                    mandators.add(_item.build());
                }
                _product.mandators = mandators;
            }
            _product.sessionId = this.sessionId;
            _product.username = this.username;
            _product.locale = this.locale;
            _product.datalanguage = this.datalanguage;
            _product.ldapActive = this.ldapActive;
            _product.ssoActive = this.ssoActive;
            _product.ssoLogoutUri = this.ssoLogoutUri;
            _product.maintenanceMode = this.maintenanceMode;
            _product.superUser = this.superUser;
            _product.showRichClientCompleted = this.showRichClientCompleted;
            _product.clientIp = this.clientIp;
            _product.initialEntity = this.initialEntity;
            return _product;
        }

        /**
         * Sets the new value of "allowedActions" (any previous value will be replaced)
         * 
         * @param allowedActions
         *     New value of the "allowedActions" property.
         */
        public LoginInfo.Builder<_B> withAllowedActions(final LoginInfo.AllowedActions allowedActions) {
            this.allowedActions = ((allowedActions == null)?null:new LoginInfo.AllowedActions.Builder<LoginInfo.Builder<_B>>(this, allowedActions, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "allowedActions" property.
         * Use {@link org.nuclos.schema.rest.LoginInfo.AllowedActions.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "allowedActions" property.
         *     Use {@link org.nuclos.schema.rest.LoginInfo.AllowedActions.Builder#end()} to return to the current builder.
         */
        public LoginInfo.AllowedActions.Builder<? extends LoginInfo.Builder<_B>> withAllowedActions() {
            if (this.allowedActions!= null) {
                return this.allowedActions;
            }
            return this.allowedActions = new LoginInfo.AllowedActions.Builder<LoginInfo.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "links" (any previous value will be replaced)
         * 
         * @param links
         *     New value of the "links" property.
         */
        public LoginInfo.Builder<_B> withLinks(final LoginInfoLinks links) {
            this.links = ((links == null)?null:new LoginInfoLinks.Builder<LoginInfo.Builder<_B>>(this, links, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "links" property.
         * Use {@link org.nuclos.schema.rest.LoginInfoLinks.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "links" property.
         *     Use {@link org.nuclos.schema.rest.LoginInfoLinks.Builder#end()} to return to the current builder.
         */
        public LoginInfoLinks.Builder<? extends LoginInfo.Builder<_B>> withLinks() {
            if (this.links!= null) {
                return this.links;
            }
            return this.links = new LoginInfoLinks.Builder<LoginInfo.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "mandator" (any previous value will be replaced)
         * 
         * @param mandator
         *     New value of the "mandator" property.
         */
        public LoginInfo.Builder<_B> withMandator(final Mandator mandator) {
            this.mandator = ((mandator == null)?null:new Mandator.Builder<LoginInfo.Builder<_B>>(this, mandator, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "mandator" property.
         * Use {@link org.nuclos.schema.rest.Mandator.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "mandator" property.
         *     Use {@link org.nuclos.schema.rest.Mandator.Builder#end()} to return to the current builder.
         */
        public Mandator.Builder<? extends LoginInfo.Builder<_B>> withMandator() {
            if (this.mandator!= null) {
                return this.mandator;
            }
            return this.mandator = new Mandator.Builder<LoginInfo.Builder<_B>>(this, null, false);
        }

        /**
         * Adds the given items to the value of "mandators"
         * 
         * @param mandators
         *     Items to add to the value of the "mandators" property
         */
        public LoginInfo.Builder<_B> addMandators(final Iterable<? extends Mandator> mandators) {
            if (mandators!= null) {
                if (this.mandators == null) {
                    this.mandators = new ArrayList<Mandator.Builder<LoginInfo.Builder<_B>>>();
                }
                for (Mandator _item: mandators) {
                    this.mandators.add(new Mandator.Builder<LoginInfo.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "mandators" (any previous value will be replaced)
         * 
         * @param mandators
         *     New value of the "mandators" property.
         */
        public LoginInfo.Builder<_B> withMandators(final Iterable<? extends Mandator> mandators) {
            if (this.mandators!= null) {
                this.mandators.clear();
            }
            return addMandators(mandators);
        }

        /**
         * Adds the given items to the value of "mandators"
         * 
         * @param mandators
         *     Items to add to the value of the "mandators" property
         */
        public LoginInfo.Builder<_B> addMandators(Mandator... mandators) {
            addMandators(Arrays.asList(mandators));
            return this;
        }

        /**
         * Sets the new value of "mandators" (any previous value will be replaced)
         * 
         * @param mandators
         *     New value of the "mandators" property.
         */
        public LoginInfo.Builder<_B> withMandators(Mandator... mandators) {
            withMandators(Arrays.asList(mandators));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Mandators" property.
         * Use {@link org.nuclos.schema.rest.Mandator.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Mandators" property.
         *     Use {@link org.nuclos.schema.rest.Mandator.Builder#end()} to return to the current builder.
         */
        public Mandator.Builder<? extends LoginInfo.Builder<_B>> addMandators() {
            if (this.mandators == null) {
                this.mandators = new ArrayList<Mandator.Builder<LoginInfo.Builder<_B>>>();
            }
            final Mandator.Builder<LoginInfo.Builder<_B>> mandators_Builder = new Mandator.Builder<LoginInfo.Builder<_B>>(this, null, false);
            this.mandators.add(mandators_Builder);
            return mandators_Builder;
        }

        /**
         * Sets the new value of "sessionId" (any previous value will be replaced)
         * 
         * @param sessionId
         *     New value of the "sessionId" property.
         */
        public LoginInfo.Builder<_B> withSessionId(final String sessionId) {
            this.sessionId = sessionId;
            return this;
        }

        /**
         * Sets the new value of "username" (any previous value will be replaced)
         * 
         * @param username
         *     New value of the "username" property.
         */
        public LoginInfo.Builder<_B> withUsername(final String username) {
            this.username = username;
            return this;
        }

        /**
         * Sets the new value of "locale" (any previous value will be replaced)
         * 
         * @param locale
         *     New value of the "locale" property.
         */
        public LoginInfo.Builder<_B> withLocale(final String locale) {
            this.locale = locale;
            return this;
        }

        /**
         * Sets the new value of "datalanguage" (any previous value will be replaced)
         * 
         * @param datalanguage
         *     New value of the "datalanguage" property.
         */
        public LoginInfo.Builder<_B> withDatalanguage(final String datalanguage) {
            this.datalanguage = datalanguage;
            return this;
        }

        /**
         * Sets the new value of "ldapActive" (any previous value will be replaced)
         * 
         * @param ldapActive
         *     New value of the "ldapActive" property.
         */
        public LoginInfo.Builder<_B> withLdapActive(final Boolean ldapActive) {
            this.ldapActive = ldapActive;
            return this;
        }

        /**
         * Sets the new value of "ssoActive" (any previous value will be replaced)
         * 
         * @param ssoActive
         *     New value of the "ssoActive" property.
         */
        public LoginInfo.Builder<_B> withSsoActive(final Boolean ssoActive) {
            this.ssoActive = ssoActive;
            return this;
        }

        /**
         * Sets the new value of "ssoLogoutUri" (any previous value will be replaced)
         * 
         * @param ssoLogoutUri
         *     New value of the "ssoLogoutUri" property.
         */
        public LoginInfo.Builder<_B> withSsoLogoutUri(final String ssoLogoutUri) {
            this.ssoLogoutUri = ssoLogoutUri;
            return this;
        }

        /**
         * Sets the new value of "maintenanceMode" (any previous value will be replaced)
         * 
         * @param maintenanceMode
         *     New value of the "maintenanceMode" property.
         */
        public LoginInfo.Builder<_B> withMaintenanceMode(final boolean maintenanceMode) {
            this.maintenanceMode = maintenanceMode;
            return this;
        }

        /**
         * Sets the new value of "superUser" (any previous value will be replaced)
         * 
         * @param superUser
         *     New value of the "superUser" property.
         */
        public LoginInfo.Builder<_B> withSuperUser(final boolean superUser) {
            this.superUser = superUser;
            return this;
        }

        /**
         * Sets the new value of "showRichClientCompleted" (any previous value will be replaced)
         * 
         * @param showRichClientCompleted
         *     New value of the "showRichClientCompleted" property.
         */
        public LoginInfo.Builder<_B> withShowRichClientCompleted(final Boolean showRichClientCompleted) {
            this.showRichClientCompleted = showRichClientCompleted;
            return this;
        }

        /**
         * Sets the new value of "clientIp" (any previous value will be replaced)
         * 
         * @param clientIp
         *     New value of the "clientIp" property.
         */
        public LoginInfo.Builder<_B> withClientIp(final String clientIp) {
            this.clientIp = clientIp;
            return this;
        }

        /**
         * Sets the new value of "initialEntity" (any previous value will be replaced)
         * 
         * @param initialEntity
         *     New value of the "initialEntity" property.
         */
        public LoginInfo.Builder<_B> withInitialEntity(final String initialEntity) {
            this.initialEntity = initialEntity;
            return this;
        }

        @Override
        public LoginInfo build() {
            if (_storedValue == null) {
                return this.init(new LoginInfo());
            } else {
                return ((LoginInfo) _storedValue);
            }
        }

        public LoginInfo.Builder<_B> copyOf(final LoginInfo _other) {
            _other.copyTo(this);
            return this;
        }

        public LoginInfo.Builder<_B> copyOf(final LoginInfo.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends LoginInfo.Selector<LoginInfo.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static LoginInfo.Select _root() {
            return new LoginInfo.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private LoginInfo.AllowedActions.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> allowedActions = null;
        private LoginInfoLinks.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> links = null;
        private Mandator.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> mandator = null;
        private Mandator.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> mandators = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> sessionId = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> username = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> locale = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> datalanguage = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> ldapActive = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> ssoActive = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> ssoLogoutUri = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> showRichClientCompleted = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> clientIp = null;
        private com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> initialEntity = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.allowedActions!= null) {
                products.put("allowedActions", this.allowedActions.init());
            }
            if (this.links!= null) {
                products.put("links", this.links.init());
            }
            if (this.mandator!= null) {
                products.put("mandator", this.mandator.init());
            }
            if (this.mandators!= null) {
                products.put("mandators", this.mandators.init());
            }
            if (this.sessionId!= null) {
                products.put("sessionId", this.sessionId.init());
            }
            if (this.username!= null) {
                products.put("username", this.username.init());
            }
            if (this.locale!= null) {
                products.put("locale", this.locale.init());
            }
            if (this.datalanguage!= null) {
                products.put("datalanguage", this.datalanguage.init());
            }
            if (this.ldapActive!= null) {
                products.put("ldapActive", this.ldapActive.init());
            }
            if (this.ssoActive!= null) {
                products.put("ssoActive", this.ssoActive.init());
            }
            if (this.ssoLogoutUri!= null) {
                products.put("ssoLogoutUri", this.ssoLogoutUri.init());
            }
            if (this.showRichClientCompleted!= null) {
                products.put("showRichClientCompleted", this.showRichClientCompleted.init());
            }
            if (this.clientIp!= null) {
                products.put("clientIp", this.clientIp.init());
            }
            if (this.initialEntity!= null) {
                products.put("initialEntity", this.initialEntity.init());
            }
            return products;
        }

        public LoginInfo.AllowedActions.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> allowedActions() {
            return ((this.allowedActions == null)?this.allowedActions = new LoginInfo.AllowedActions.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "allowedActions"):this.allowedActions);
        }

        public LoginInfoLinks.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> links() {
            return ((this.links == null)?this.links = new LoginInfoLinks.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "links"):this.links);
        }

        public Mandator.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> mandator() {
            return ((this.mandator == null)?this.mandator = new Mandator.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "mandator"):this.mandator);
        }

        public Mandator.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> mandators() {
            return ((this.mandators == null)?this.mandators = new Mandator.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "mandators"):this.mandators);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> sessionId() {
            return ((this.sessionId == null)?this.sessionId = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "sessionId"):this.sessionId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> username() {
            return ((this.username == null)?this.username = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "username"):this.username);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> locale() {
            return ((this.locale == null)?this.locale = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "locale"):this.locale);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> datalanguage() {
            return ((this.datalanguage == null)?this.datalanguage = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "datalanguage"):this.datalanguage);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> ldapActive() {
            return ((this.ldapActive == null)?this.ldapActive = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "ldapActive"):this.ldapActive);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> ssoActive() {
            return ((this.ssoActive == null)?this.ssoActive = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "ssoActive"):this.ssoActive);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> ssoLogoutUri() {
            return ((this.ssoLogoutUri == null)?this.ssoLogoutUri = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "ssoLogoutUri"):this.ssoLogoutUri);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> showRichClientCompleted() {
            return ((this.showRichClientCompleted == null)?this.showRichClientCompleted = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "showRichClientCompleted"):this.showRichClientCompleted);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> clientIp() {
            return ((this.clientIp == null)?this.clientIp = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "clientIp"):this.clientIp);
        }

        public com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>> initialEntity() {
            return ((this.initialEntity == null)?this.initialEntity = new com.kscs.util.jaxb.Selector<TRoot, LoginInfo.Selector<TRoot, TParent>>(this._root, this, "initialEntity"):this.initialEntity);
        }

    }

}
