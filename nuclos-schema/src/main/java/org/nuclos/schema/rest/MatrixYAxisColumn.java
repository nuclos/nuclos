package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for matrix-y-axis-column complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="matrix-y-axis-column"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="boAttrId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "matrix-y-axis-column", propOrder = {
    "value"
})
public class MatrixYAxisColumn implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected Object value;
    @XmlAttribute(name = "boAttrId")
    protected String boAttrId;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * Gets the value of the boAttrId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoAttrId() {
        return boAttrId;
    }

    /**
     * Sets the value of the boAttrId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoAttrId(String value) {
        this.boAttrId = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Object theValue;
            theValue = this.getValue();
            strategy.appendField(locator, this, "value", buffer, theValue);
        }
        {
            String theBoAttrId;
            theBoAttrId = this.getBoAttrId();
            strategy.appendField(locator, this, "boAttrId", buffer, theBoAttrId);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MatrixYAxisColumn)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MatrixYAxisColumn that = ((MatrixYAxisColumn) object);
        {
            Object lhsValue;
            lhsValue = this.getValue();
            Object rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            String lhsBoAttrId;
            lhsBoAttrId = this.getBoAttrId();
            String rhsBoAttrId;
            rhsBoAttrId = that.getBoAttrId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boAttrId", lhsBoAttrId), LocatorUtils.property(thatLocator, "boAttrId", rhsBoAttrId), lhsBoAttrId, rhsBoAttrId)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Object theValue;
            theValue = this.getValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
        }
        {
            String theBoAttrId;
            theBoAttrId = this.getBoAttrId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boAttrId", theBoAttrId), currentHashCode, theBoAttrId);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MatrixYAxisColumn) {
            final MatrixYAxisColumn copy = ((MatrixYAxisColumn) draftCopy);
            if (this.value!= null) {
                Object sourceValue;
                sourceValue = this.getValue();
                Object copyValue = ((Object) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.boAttrId!= null) {
                String sourceBoAttrId;
                sourceBoAttrId = this.getBoAttrId();
                String copyBoAttrId = ((String) strategy.copy(LocatorUtils.property(locator, "boAttrId", sourceBoAttrId), sourceBoAttrId));
                copy.setBoAttrId(copyBoAttrId);
            } else {
                copy.boAttrId = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MatrixYAxisColumn();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixYAxisColumn.Builder<_B> _other) {
        _other.value = this.value;
        _other.boAttrId = this.boAttrId;
    }

    public<_B >MatrixYAxisColumn.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new MatrixYAxisColumn.Builder<_B>(_parentBuilder, this, true);
    }

    public MatrixYAxisColumn.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static MatrixYAxisColumn.Builder<Void> builder() {
        return new MatrixYAxisColumn.Builder<Void>(null, null, false);
    }

    public static<_B >MatrixYAxisColumn.Builder<_B> copyOf(final MatrixYAxisColumn _other) {
        final MatrixYAxisColumn.Builder<_B> _newBuilder = new MatrixYAxisColumn.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixYAxisColumn.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
            _other.value = this.value;
        }
        final PropertyTree boAttrIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boAttrId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boAttrIdPropertyTree!= null):((boAttrIdPropertyTree == null)||(!boAttrIdPropertyTree.isLeaf())))) {
            _other.boAttrId = this.boAttrId;
        }
    }

    public<_B >MatrixYAxisColumn.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new MatrixYAxisColumn.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public MatrixYAxisColumn.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >MatrixYAxisColumn.Builder<_B> copyOf(final MatrixYAxisColumn _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final MatrixYAxisColumn.Builder<_B> _newBuilder = new MatrixYAxisColumn.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static MatrixYAxisColumn.Builder<Void> copyExcept(final MatrixYAxisColumn _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static MatrixYAxisColumn.Builder<Void> copyOnly(final MatrixYAxisColumn _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final MatrixYAxisColumn _storedValue;
        private Object value;
        private String boAttrId;

        public Builder(final _B _parentBuilder, final MatrixYAxisColumn _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.value = _other.value;
                    this.boAttrId = _other.boAttrId;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final MatrixYAxisColumn _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                        this.value = _other.value;
                    }
                    final PropertyTree boAttrIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boAttrId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boAttrIdPropertyTree!= null):((boAttrIdPropertyTree == null)||(!boAttrIdPropertyTree.isLeaf())))) {
                        this.boAttrId = _other.boAttrId;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends MatrixYAxisColumn >_P init(final _P _product) {
            _product.value = this.value;
            _product.boAttrId = this.boAttrId;
            return _product;
        }

        /**
         * Sets the new value of "value" (any previous value will be replaced)
         * 
         * @param value
         *     New value of the "value" property.
         */
        public MatrixYAxisColumn.Builder<_B> withValue(final Object value) {
            this.value = value;
            return this;
        }

        /**
         * Sets the new value of "boAttrId" (any previous value will be replaced)
         * 
         * @param boAttrId
         *     New value of the "boAttrId" property.
         */
        public MatrixYAxisColumn.Builder<_B> withBoAttrId(final String boAttrId) {
            this.boAttrId = boAttrId;
            return this;
        }

        @Override
        public MatrixYAxisColumn build() {
            if (_storedValue == null) {
                return this.init(new MatrixYAxisColumn());
            } else {
                return ((MatrixYAxisColumn) _storedValue);
            }
        }

        public MatrixYAxisColumn.Builder<_B> copyOf(final MatrixYAxisColumn _other) {
            _other.copyTo(this);
            return this;
        }

        public MatrixYAxisColumn.Builder<_B> copyOf(final MatrixYAxisColumn.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends MatrixYAxisColumn.Selector<MatrixYAxisColumn.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static MatrixYAxisColumn.Select _root() {
            return new MatrixYAxisColumn.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisColumn.Selector<TRoot, TParent>> value = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisColumn.Selector<TRoot, TParent>> boAttrId = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.value!= null) {
                products.put("value", this.value.init());
            }
            if (this.boAttrId!= null) {
                products.put("boAttrId", this.boAttrId.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisColumn.Selector<TRoot, TParent>> value() {
            return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisColumn.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisColumn.Selector<TRoot, TParent>> boAttrId() {
            return ((this.boAttrId == null)?this.boAttrId = new com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisColumn.Selector<TRoot, TParent>>(this._root, this, "boAttrId"):this.boAttrId);
        }

    }

}
