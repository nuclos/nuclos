package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for matrix-y-axis-object complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="matrix-y-axis-object"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="yAxisColumns" type="{urn:org.nuclos.schema.rest}matrix-y-axis-column" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="boId" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="evaluatedHeaderTitle" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="rowColor" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "matrix-y-axis-object", propOrder = {
    "yAxisColumns"
})
public class MatrixYAxisObject implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<MatrixYAxisColumn> yAxisColumns;
    @XmlAttribute(name = "boId")
    protected Long boId;
    @XmlAttribute(name = "evaluatedHeaderTitle")
    protected String evaluatedHeaderTitle;
    @XmlAttribute(name = "rowColor")
    protected String rowColor;

    /**
     * Gets the value of the yAxisColumns property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the yAxisColumns property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getYAxisColumns().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MatrixYAxisColumn }
     * 
     * 
     */
    public List<MatrixYAxisColumn> getYAxisColumns() {
        if (yAxisColumns == null) {
            yAxisColumns = new ArrayList<MatrixYAxisColumn>();
        }
        return this.yAxisColumns;
    }

    /**
     * Gets the value of the boId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBoId() {
        return boId;
    }

    /**
     * Sets the value of the boId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBoId(Long value) {
        this.boId = value;
    }

    /**
     * Gets the value of the evaluatedHeaderTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEvaluatedHeaderTitle() {
        return evaluatedHeaderTitle;
    }

    /**
     * Sets the value of the evaluatedHeaderTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEvaluatedHeaderTitle(String value) {
        this.evaluatedHeaderTitle = value;
    }

    /**
     * Gets the value of the rowColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRowColor() {
        return rowColor;
    }

    /**
     * Sets the value of the rowColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRowColor(String value) {
        this.rowColor = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<MatrixYAxisColumn> theYAxisColumns;
            theYAxisColumns = (((this.yAxisColumns!= null)&&(!this.yAxisColumns.isEmpty()))?this.getYAxisColumns():null);
            strategy.appendField(locator, this, "yAxisColumns", buffer, theYAxisColumns);
        }
        {
            Long theBoId;
            theBoId = this.getBoId();
            strategy.appendField(locator, this, "boId", buffer, theBoId);
        }
        {
            String theEvaluatedHeaderTitle;
            theEvaluatedHeaderTitle = this.getEvaluatedHeaderTitle();
            strategy.appendField(locator, this, "evaluatedHeaderTitle", buffer, theEvaluatedHeaderTitle);
        }
        {
            String theRowColor;
            theRowColor = this.getRowColor();
            strategy.appendField(locator, this, "rowColor", buffer, theRowColor);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MatrixYAxisObject)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MatrixYAxisObject that = ((MatrixYAxisObject) object);
        {
            List<MatrixYAxisColumn> lhsYAxisColumns;
            lhsYAxisColumns = (((this.yAxisColumns!= null)&&(!this.yAxisColumns.isEmpty()))?this.getYAxisColumns():null);
            List<MatrixYAxisColumn> rhsYAxisColumns;
            rhsYAxisColumns = (((that.yAxisColumns!= null)&&(!that.yAxisColumns.isEmpty()))?that.getYAxisColumns():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "yAxisColumns", lhsYAxisColumns), LocatorUtils.property(thatLocator, "yAxisColumns", rhsYAxisColumns), lhsYAxisColumns, rhsYAxisColumns)) {
                return false;
            }
        }
        {
            Long lhsBoId;
            lhsBoId = this.getBoId();
            Long rhsBoId;
            rhsBoId = that.getBoId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "boId", lhsBoId), LocatorUtils.property(thatLocator, "boId", rhsBoId), lhsBoId, rhsBoId)) {
                return false;
            }
        }
        {
            String lhsEvaluatedHeaderTitle;
            lhsEvaluatedHeaderTitle = this.getEvaluatedHeaderTitle();
            String rhsEvaluatedHeaderTitle;
            rhsEvaluatedHeaderTitle = that.getEvaluatedHeaderTitle();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "evaluatedHeaderTitle", lhsEvaluatedHeaderTitle), LocatorUtils.property(thatLocator, "evaluatedHeaderTitle", rhsEvaluatedHeaderTitle), lhsEvaluatedHeaderTitle, rhsEvaluatedHeaderTitle)) {
                return false;
            }
        }
        {
            String lhsRowColor;
            lhsRowColor = this.getRowColor();
            String rhsRowColor;
            rhsRowColor = that.getRowColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rowColor", lhsRowColor), LocatorUtils.property(thatLocator, "rowColor", rhsRowColor), lhsRowColor, rhsRowColor)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<MatrixYAxisColumn> theYAxisColumns;
            theYAxisColumns = (((this.yAxisColumns!= null)&&(!this.yAxisColumns.isEmpty()))?this.getYAxisColumns():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "yAxisColumns", theYAxisColumns), currentHashCode, theYAxisColumns);
        }
        {
            Long theBoId;
            theBoId = this.getBoId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "boId", theBoId), currentHashCode, theBoId);
        }
        {
            String theEvaluatedHeaderTitle;
            theEvaluatedHeaderTitle = this.getEvaluatedHeaderTitle();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "evaluatedHeaderTitle", theEvaluatedHeaderTitle), currentHashCode, theEvaluatedHeaderTitle);
        }
        {
            String theRowColor;
            theRowColor = this.getRowColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rowColor", theRowColor), currentHashCode, theRowColor);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MatrixYAxisObject) {
            final MatrixYAxisObject copy = ((MatrixYAxisObject) draftCopy);
            if ((this.yAxisColumns!= null)&&(!this.yAxisColumns.isEmpty())) {
                List<MatrixYAxisColumn> sourceYAxisColumns;
                sourceYAxisColumns = (((this.yAxisColumns!= null)&&(!this.yAxisColumns.isEmpty()))?this.getYAxisColumns():null);
                @SuppressWarnings("unchecked")
                List<MatrixYAxisColumn> copyYAxisColumns = ((List<MatrixYAxisColumn> ) strategy.copy(LocatorUtils.property(locator, "yAxisColumns", sourceYAxisColumns), sourceYAxisColumns));
                copy.yAxisColumns = null;
                if (copyYAxisColumns!= null) {
                    List<MatrixYAxisColumn> uniqueYAxisColumnsl = copy.getYAxisColumns();
                    uniqueYAxisColumnsl.addAll(copyYAxisColumns);
                }
            } else {
                copy.yAxisColumns = null;
            }
            if (this.boId!= null) {
                Long sourceBoId;
                sourceBoId = this.getBoId();
                Long copyBoId = ((Long) strategy.copy(LocatorUtils.property(locator, "boId", sourceBoId), sourceBoId));
                copy.setBoId(copyBoId);
            } else {
                copy.boId = null;
            }
            if (this.evaluatedHeaderTitle!= null) {
                String sourceEvaluatedHeaderTitle;
                sourceEvaluatedHeaderTitle = this.getEvaluatedHeaderTitle();
                String copyEvaluatedHeaderTitle = ((String) strategy.copy(LocatorUtils.property(locator, "evaluatedHeaderTitle", sourceEvaluatedHeaderTitle), sourceEvaluatedHeaderTitle));
                copy.setEvaluatedHeaderTitle(copyEvaluatedHeaderTitle);
            } else {
                copy.evaluatedHeaderTitle = null;
            }
            if (this.rowColor!= null) {
                String sourceRowColor;
                sourceRowColor = this.getRowColor();
                String copyRowColor = ((String) strategy.copy(LocatorUtils.property(locator, "rowColor", sourceRowColor), sourceRowColor));
                copy.setRowColor(copyRowColor);
            } else {
                copy.rowColor = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MatrixYAxisObject();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixYAxisObject.Builder<_B> _other) {
        if (this.yAxisColumns == null) {
            _other.yAxisColumns = null;
        } else {
            _other.yAxisColumns = new ArrayList<MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>>>();
            for (MatrixYAxisColumn _item: this.yAxisColumns) {
                _other.yAxisColumns.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.boId = this.boId;
        _other.evaluatedHeaderTitle = this.evaluatedHeaderTitle;
        _other.rowColor = this.rowColor;
    }

    public<_B >MatrixYAxisObject.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new MatrixYAxisObject.Builder<_B>(_parentBuilder, this, true);
    }

    public MatrixYAxisObject.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static MatrixYAxisObject.Builder<Void> builder() {
        return new MatrixYAxisObject.Builder<Void>(null, null, false);
    }

    public static<_B >MatrixYAxisObject.Builder<_B> copyOf(final MatrixYAxisObject _other) {
        final MatrixYAxisObject.Builder<_B> _newBuilder = new MatrixYAxisObject.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MatrixYAxisObject.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree yAxisColumnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("yAxisColumns"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(yAxisColumnsPropertyTree!= null):((yAxisColumnsPropertyTree == null)||(!yAxisColumnsPropertyTree.isLeaf())))) {
            if (this.yAxisColumns == null) {
                _other.yAxisColumns = null;
            } else {
                _other.yAxisColumns = new ArrayList<MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>>>();
                for (MatrixYAxisColumn _item: this.yAxisColumns) {
                    _other.yAxisColumns.add(((_item == null)?null:_item.newCopyBuilder(_other, yAxisColumnsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
            _other.boId = this.boId;
        }
        final PropertyTree evaluatedHeaderTitlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("evaluatedHeaderTitle"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(evaluatedHeaderTitlePropertyTree!= null):((evaluatedHeaderTitlePropertyTree == null)||(!evaluatedHeaderTitlePropertyTree.isLeaf())))) {
            _other.evaluatedHeaderTitle = this.evaluatedHeaderTitle;
        }
        final PropertyTree rowColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rowColor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowColorPropertyTree!= null):((rowColorPropertyTree == null)||(!rowColorPropertyTree.isLeaf())))) {
            _other.rowColor = this.rowColor;
        }
    }

    public<_B >MatrixYAxisObject.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new MatrixYAxisObject.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public MatrixYAxisObject.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >MatrixYAxisObject.Builder<_B> copyOf(final MatrixYAxisObject _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final MatrixYAxisObject.Builder<_B> _newBuilder = new MatrixYAxisObject.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static MatrixYAxisObject.Builder<Void> copyExcept(final MatrixYAxisObject _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static MatrixYAxisObject.Builder<Void> copyOnly(final MatrixYAxisObject _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final MatrixYAxisObject _storedValue;
        private List<MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>>> yAxisColumns;
        private Long boId;
        private String evaluatedHeaderTitle;
        private String rowColor;

        public Builder(final _B _parentBuilder, final MatrixYAxisObject _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.yAxisColumns == null) {
                        this.yAxisColumns = null;
                    } else {
                        this.yAxisColumns = new ArrayList<MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>>>();
                        for (MatrixYAxisColumn _item: _other.yAxisColumns) {
                            this.yAxisColumns.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.boId = _other.boId;
                    this.evaluatedHeaderTitle = _other.evaluatedHeaderTitle;
                    this.rowColor = _other.rowColor;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final MatrixYAxisObject _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree yAxisColumnsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("yAxisColumns"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(yAxisColumnsPropertyTree!= null):((yAxisColumnsPropertyTree == null)||(!yAxisColumnsPropertyTree.isLeaf())))) {
                        if (_other.yAxisColumns == null) {
                            this.yAxisColumns = null;
                        } else {
                            this.yAxisColumns = new ArrayList<MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>>>();
                            for (MatrixYAxisColumn _item: _other.yAxisColumns) {
                                this.yAxisColumns.add(((_item == null)?null:_item.newCopyBuilder(this, yAxisColumnsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree boIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("boId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(boIdPropertyTree!= null):((boIdPropertyTree == null)||(!boIdPropertyTree.isLeaf())))) {
                        this.boId = _other.boId;
                    }
                    final PropertyTree evaluatedHeaderTitlePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("evaluatedHeaderTitle"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(evaluatedHeaderTitlePropertyTree!= null):((evaluatedHeaderTitlePropertyTree == null)||(!evaluatedHeaderTitlePropertyTree.isLeaf())))) {
                        this.evaluatedHeaderTitle = _other.evaluatedHeaderTitle;
                    }
                    final PropertyTree rowColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rowColor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rowColorPropertyTree!= null):((rowColorPropertyTree == null)||(!rowColorPropertyTree.isLeaf())))) {
                        this.rowColor = _other.rowColor;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends MatrixYAxisObject >_P init(final _P _product) {
            if (this.yAxisColumns!= null) {
                final List<MatrixYAxisColumn> yAxisColumns = new ArrayList<MatrixYAxisColumn>(this.yAxisColumns.size());
                for (MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>> _item: this.yAxisColumns) {
                    yAxisColumns.add(_item.build());
                }
                _product.yAxisColumns = yAxisColumns;
            }
            _product.boId = this.boId;
            _product.evaluatedHeaderTitle = this.evaluatedHeaderTitle;
            _product.rowColor = this.rowColor;
            return _product;
        }

        /**
         * Adds the given items to the value of "yAxisColumns"
         * 
         * @param yAxisColumns
         *     Items to add to the value of the "yAxisColumns" property
         */
        public MatrixYAxisObject.Builder<_B> addYAxisColumns(final Iterable<? extends MatrixYAxisColumn> yAxisColumns) {
            if (yAxisColumns!= null) {
                if (this.yAxisColumns == null) {
                    this.yAxisColumns = new ArrayList<MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>>>();
                }
                for (MatrixYAxisColumn _item: yAxisColumns) {
                    this.yAxisColumns.add(new MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "yAxisColumns" (any previous value will be replaced)
         * 
         * @param yAxisColumns
         *     New value of the "yAxisColumns" property.
         */
        public MatrixYAxisObject.Builder<_B> withYAxisColumns(final Iterable<? extends MatrixYAxisColumn> yAxisColumns) {
            if (this.yAxisColumns!= null) {
                this.yAxisColumns.clear();
            }
            return addYAxisColumns(yAxisColumns);
        }

        /**
         * Adds the given items to the value of "yAxisColumns"
         * 
         * @param yAxisColumns
         *     Items to add to the value of the "yAxisColumns" property
         */
        public MatrixYAxisObject.Builder<_B> addYAxisColumns(MatrixYAxisColumn... yAxisColumns) {
            addYAxisColumns(Arrays.asList(yAxisColumns));
            return this;
        }

        /**
         * Sets the new value of "yAxisColumns" (any previous value will be replaced)
         * 
         * @param yAxisColumns
         *     New value of the "yAxisColumns" property.
         */
        public MatrixYAxisObject.Builder<_B> withYAxisColumns(MatrixYAxisColumn... yAxisColumns) {
            withYAxisColumns(Arrays.asList(yAxisColumns));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "YAxisColumns" property.
         * Use {@link org.nuclos.schema.rest.MatrixYAxisColumn.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "YAxisColumns" property.
         *     Use {@link org.nuclos.schema.rest.MatrixYAxisColumn.Builder#end()} to return to the current builder.
         */
        public MatrixYAxisColumn.Builder<? extends MatrixYAxisObject.Builder<_B>> addYAxisColumns() {
            if (this.yAxisColumns == null) {
                this.yAxisColumns = new ArrayList<MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>>>();
            }
            final MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>> yAxisColumns_Builder = new MatrixYAxisColumn.Builder<MatrixYAxisObject.Builder<_B>>(this, null, false);
            this.yAxisColumns.add(yAxisColumns_Builder);
            return yAxisColumns_Builder;
        }

        /**
         * Sets the new value of "boId" (any previous value will be replaced)
         * 
         * @param boId
         *     New value of the "boId" property.
         */
        public MatrixYAxisObject.Builder<_B> withBoId(final Long boId) {
            this.boId = boId;
            return this;
        }

        /**
         * Sets the new value of "evaluatedHeaderTitle" (any previous value will be replaced)
         * 
         * @param evaluatedHeaderTitle
         *     New value of the "evaluatedHeaderTitle" property.
         */
        public MatrixYAxisObject.Builder<_B> withEvaluatedHeaderTitle(final String evaluatedHeaderTitle) {
            this.evaluatedHeaderTitle = evaluatedHeaderTitle;
            return this;
        }

        /**
         * Sets the new value of "rowColor" (any previous value will be replaced)
         * 
         * @param rowColor
         *     New value of the "rowColor" property.
         */
        public MatrixYAxisObject.Builder<_B> withRowColor(final String rowColor) {
            this.rowColor = rowColor;
            return this;
        }

        @Override
        public MatrixYAxisObject build() {
            if (_storedValue == null) {
                return this.init(new MatrixYAxisObject());
            } else {
                return ((MatrixYAxisObject) _storedValue);
            }
        }

        public MatrixYAxisObject.Builder<_B> copyOf(final MatrixYAxisObject _other) {
            _other.copyTo(this);
            return this;
        }

        public MatrixYAxisObject.Builder<_B> copyOf(final MatrixYAxisObject.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends MatrixYAxisObject.Selector<MatrixYAxisObject.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static MatrixYAxisObject.Select _root() {
            return new MatrixYAxisObject.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private MatrixYAxisColumn.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> yAxisColumns = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> boId = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> evaluatedHeaderTitle = null;
        private com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> rowColor = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.yAxisColumns!= null) {
                products.put("yAxisColumns", this.yAxisColumns.init());
            }
            if (this.boId!= null) {
                products.put("boId", this.boId.init());
            }
            if (this.evaluatedHeaderTitle!= null) {
                products.put("evaluatedHeaderTitle", this.evaluatedHeaderTitle.init());
            }
            if (this.rowColor!= null) {
                products.put("rowColor", this.rowColor.init());
            }
            return products;
        }

        public MatrixYAxisColumn.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> yAxisColumns() {
            return ((this.yAxisColumns == null)?this.yAxisColumns = new MatrixYAxisColumn.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>>(this._root, this, "yAxisColumns"):this.yAxisColumns);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> boId() {
            return ((this.boId == null)?this.boId = new com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>>(this._root, this, "boId"):this.boId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> evaluatedHeaderTitle() {
            return ((this.evaluatedHeaderTitle == null)?this.evaluatedHeaderTitle = new com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>>(this._root, this, "evaluatedHeaderTitle"):this.evaluatedHeaderTitle);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>> rowColor() {
            return ((this.rowColor == null)?this.rowColor = new com.kscs.util.jaxb.Selector<TRoot, MatrixYAxisObject.Selector<TRoot, TParent>>(this._root, this, "rowColor"):this.rowColor);
        }

    }

}
