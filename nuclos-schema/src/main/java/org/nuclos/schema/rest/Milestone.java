package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for milestone complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="milestone"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="resource" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entityMeta" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="fromDate" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="backgroundColor" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="icon" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "milestone")
public class Milestone implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "resource", required = true)
    protected String resource;
    @XmlAttribute(name = "entityMeta", required = true)
    protected String entityMeta;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "fromDate", required = true)
    protected String fromDate;
    @XmlAttribute(name = "backgroundColor", required = true)
    protected String backgroundColor;
    @XmlAttribute(name = "icon", required = true)
    protected String icon;

    /**
     * Gets the value of the resource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResource() {
        return resource;
    }

    /**
     * Sets the value of the resource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResource(String value) {
        this.resource = value;
    }

    /**
     * Gets the value of the entityMeta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMeta() {
        return entityMeta;
    }

    /**
     * Sets the value of the entityMeta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMeta(String value) {
        this.entityMeta = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the fromDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromDate() {
        return fromDate;
    }

    /**
     * Sets the value of the fromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromDate(String value) {
        this.fromDate = value;
    }

    /**
     * Gets the value of the backgroundColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * Sets the value of the backgroundColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBackgroundColor(String value) {
        this.backgroundColor = value;
    }

    /**
     * Gets the value of the icon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Sets the value of the icon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcon(String value) {
        this.icon = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theResource;
            theResource = this.getResource();
            strategy.appendField(locator, this, "resource", buffer, theResource);
        }
        {
            String theEntityMeta;
            theEntityMeta = this.getEntityMeta();
            strategy.appendField(locator, this, "entityMeta", buffer, theEntityMeta);
        }
        {
            String theId;
            theId = this.getId();
            strategy.appendField(locator, this, "id", buffer, theId);
        }
        {
            String theFromDate;
            theFromDate = this.getFromDate();
            strategy.appendField(locator, this, "fromDate", buffer, theFromDate);
        }
        {
            String theBackgroundColor;
            theBackgroundColor = this.getBackgroundColor();
            strategy.appendField(locator, this, "backgroundColor", buffer, theBackgroundColor);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            strategy.appendField(locator, this, "icon", buffer, theIcon);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Milestone)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Milestone that = ((Milestone) object);
        {
            String lhsResource;
            lhsResource = this.getResource();
            String rhsResource;
            rhsResource = that.getResource();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resource", lhsResource), LocatorUtils.property(thatLocator, "resource", rhsResource), lhsResource, rhsResource)) {
                return false;
            }
        }
        {
            String lhsEntityMeta;
            lhsEntityMeta = this.getEntityMeta();
            String rhsEntityMeta;
            rhsEntityMeta = that.getEntityMeta();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMeta", lhsEntityMeta), LocatorUtils.property(thatLocator, "entityMeta", rhsEntityMeta), lhsEntityMeta, rhsEntityMeta)) {
                return false;
            }
        }
        {
            String lhsId;
            lhsId = this.getId();
            String rhsId;
            rhsId = that.getId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
                return false;
            }
        }
        {
            String lhsFromDate;
            lhsFromDate = this.getFromDate();
            String rhsFromDate;
            rhsFromDate = that.getFromDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fromDate", lhsFromDate), LocatorUtils.property(thatLocator, "fromDate", rhsFromDate), lhsFromDate, rhsFromDate)) {
                return false;
            }
        }
        {
            String lhsBackgroundColor;
            lhsBackgroundColor = this.getBackgroundColor();
            String rhsBackgroundColor;
            rhsBackgroundColor = that.getBackgroundColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "backgroundColor", lhsBackgroundColor), LocatorUtils.property(thatLocator, "backgroundColor", rhsBackgroundColor), lhsBackgroundColor, rhsBackgroundColor)) {
                return false;
            }
        }
        {
            String lhsIcon;
            lhsIcon = this.getIcon();
            String rhsIcon;
            rhsIcon = that.getIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "icon", lhsIcon), LocatorUtils.property(thatLocator, "icon", rhsIcon), lhsIcon, rhsIcon)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theResource;
            theResource = this.getResource();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resource", theResource), currentHashCode, theResource);
        }
        {
            String theEntityMeta;
            theEntityMeta = this.getEntityMeta();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMeta", theEntityMeta), currentHashCode, theEntityMeta);
        }
        {
            String theId;
            theId = this.getId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId);
        }
        {
            String theFromDate;
            theFromDate = this.getFromDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fromDate", theFromDate), currentHashCode, theFromDate);
        }
        {
            String theBackgroundColor;
            theBackgroundColor = this.getBackgroundColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "backgroundColor", theBackgroundColor), currentHashCode, theBackgroundColor);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "icon", theIcon), currentHashCode, theIcon);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Milestone) {
            final Milestone copy = ((Milestone) draftCopy);
            if (this.resource!= null) {
                String sourceResource;
                sourceResource = this.getResource();
                String copyResource = ((String) strategy.copy(LocatorUtils.property(locator, "resource", sourceResource), sourceResource));
                copy.setResource(copyResource);
            } else {
                copy.resource = null;
            }
            if (this.entityMeta!= null) {
                String sourceEntityMeta;
                sourceEntityMeta = this.getEntityMeta();
                String copyEntityMeta = ((String) strategy.copy(LocatorUtils.property(locator, "entityMeta", sourceEntityMeta), sourceEntityMeta));
                copy.setEntityMeta(copyEntityMeta);
            } else {
                copy.entityMeta = null;
            }
            if (this.id!= null) {
                String sourceId;
                sourceId = this.getId();
                String copyId = ((String) strategy.copy(LocatorUtils.property(locator, "id", sourceId), sourceId));
                copy.setId(copyId);
            } else {
                copy.id = null;
            }
            if (this.fromDate!= null) {
                String sourceFromDate;
                sourceFromDate = this.getFromDate();
                String copyFromDate = ((String) strategy.copy(LocatorUtils.property(locator, "fromDate", sourceFromDate), sourceFromDate));
                copy.setFromDate(copyFromDate);
            } else {
                copy.fromDate = null;
            }
            if (this.backgroundColor!= null) {
                String sourceBackgroundColor;
                sourceBackgroundColor = this.getBackgroundColor();
                String copyBackgroundColor = ((String) strategy.copy(LocatorUtils.property(locator, "backgroundColor", sourceBackgroundColor), sourceBackgroundColor));
                copy.setBackgroundColor(copyBackgroundColor);
            } else {
                copy.backgroundColor = null;
            }
            if (this.icon!= null) {
                String sourceIcon;
                sourceIcon = this.getIcon();
                String copyIcon = ((String) strategy.copy(LocatorUtils.property(locator, "icon", sourceIcon), sourceIcon));
                copy.setIcon(copyIcon);
            } else {
                copy.icon = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Milestone();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Milestone.Builder<_B> _other) {
        _other.resource = this.resource;
        _other.entityMeta = this.entityMeta;
        _other.id = this.id;
        _other.fromDate = this.fromDate;
        _other.backgroundColor = this.backgroundColor;
        _other.icon = this.icon;
    }

    public<_B >Milestone.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Milestone.Builder<_B>(_parentBuilder, this, true);
    }

    public Milestone.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Milestone.Builder<Void> builder() {
        return new Milestone.Builder<Void>(null, null, false);
    }

    public static<_B >Milestone.Builder<_B> copyOf(final Milestone _other) {
        final Milestone.Builder<_B> _newBuilder = new Milestone.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Milestone.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree resourcePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resource"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourcePropertyTree!= null):((resourcePropertyTree == null)||(!resourcePropertyTree.isLeaf())))) {
            _other.resource = this.resource;
        }
        final PropertyTree entityMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMeta"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMetaPropertyTree!= null):((entityMetaPropertyTree == null)||(!entityMetaPropertyTree.isLeaf())))) {
            _other.entityMeta = this.entityMeta;
        }
        final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
            _other.id = this.id;
        }
        final PropertyTree fromDatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromDate"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromDatePropertyTree!= null):((fromDatePropertyTree == null)||(!fromDatePropertyTree.isLeaf())))) {
            _other.fromDate = this.fromDate;
        }
        final PropertyTree backgroundColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("backgroundColor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundColorPropertyTree!= null):((backgroundColorPropertyTree == null)||(!backgroundColorPropertyTree.isLeaf())))) {
            _other.backgroundColor = this.backgroundColor;
        }
        final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
            _other.icon = this.icon;
        }
    }

    public<_B >Milestone.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Milestone.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Milestone.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Milestone.Builder<_B> copyOf(final Milestone _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Milestone.Builder<_B> _newBuilder = new Milestone.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Milestone.Builder<Void> copyExcept(final Milestone _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Milestone.Builder<Void> copyOnly(final Milestone _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Milestone _storedValue;
        private String resource;
        private String entityMeta;
        private String id;
        private String fromDate;
        private String backgroundColor;
        private String icon;

        public Builder(final _B _parentBuilder, final Milestone _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.resource = _other.resource;
                    this.entityMeta = _other.entityMeta;
                    this.id = _other.id;
                    this.fromDate = _other.fromDate;
                    this.backgroundColor = _other.backgroundColor;
                    this.icon = _other.icon;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Milestone _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree resourcePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resource"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourcePropertyTree!= null):((resourcePropertyTree == null)||(!resourcePropertyTree.isLeaf())))) {
                        this.resource = _other.resource;
                    }
                    final PropertyTree entityMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMeta"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMetaPropertyTree!= null):((entityMetaPropertyTree == null)||(!entityMetaPropertyTree.isLeaf())))) {
                        this.entityMeta = _other.entityMeta;
                    }
                    final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
                        this.id = _other.id;
                    }
                    final PropertyTree fromDatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromDate"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromDatePropertyTree!= null):((fromDatePropertyTree == null)||(!fromDatePropertyTree.isLeaf())))) {
                        this.fromDate = _other.fromDate;
                    }
                    final PropertyTree backgroundColorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("backgroundColor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundColorPropertyTree!= null):((backgroundColorPropertyTree == null)||(!backgroundColorPropertyTree.isLeaf())))) {
                        this.backgroundColor = _other.backgroundColor;
                    }
                    final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
                        this.icon = _other.icon;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Milestone >_P init(final _P _product) {
            _product.resource = this.resource;
            _product.entityMeta = this.entityMeta;
            _product.id = this.id;
            _product.fromDate = this.fromDate;
            _product.backgroundColor = this.backgroundColor;
            _product.icon = this.icon;
            return _product;
        }

        /**
         * Sets the new value of "resource" (any previous value will be replaced)
         * 
         * @param resource
         *     New value of the "resource" property.
         */
        public Milestone.Builder<_B> withResource(final String resource) {
            this.resource = resource;
            return this;
        }

        /**
         * Sets the new value of "entityMeta" (any previous value will be replaced)
         * 
         * @param entityMeta
         *     New value of the "entityMeta" property.
         */
        public Milestone.Builder<_B> withEntityMeta(final String entityMeta) {
            this.entityMeta = entityMeta;
            return this;
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        public Milestone.Builder<_B> withId(final String id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the new value of "fromDate" (any previous value will be replaced)
         * 
         * @param fromDate
         *     New value of the "fromDate" property.
         */
        public Milestone.Builder<_B> withFromDate(final String fromDate) {
            this.fromDate = fromDate;
            return this;
        }

        /**
         * Sets the new value of "backgroundColor" (any previous value will be replaced)
         * 
         * @param backgroundColor
         *     New value of the "backgroundColor" property.
         */
        public Milestone.Builder<_B> withBackgroundColor(final String backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        /**
         * Sets the new value of "icon" (any previous value will be replaced)
         * 
         * @param icon
         *     New value of the "icon" property.
         */
        public Milestone.Builder<_B> withIcon(final String icon) {
            this.icon = icon;
            return this;
        }

        @Override
        public Milestone build() {
            if (_storedValue == null) {
                return this.init(new Milestone());
            } else {
                return ((Milestone) _storedValue);
            }
        }

        public Milestone.Builder<_B> copyOf(final Milestone _other) {
            _other.copyTo(this);
            return this;
        }

        public Milestone.Builder<_B> copyOf(final Milestone.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Milestone.Selector<Milestone.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Milestone.Select _root() {
            return new Milestone.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> resource = null;
        private com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> entityMeta = null;
        private com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> id = null;
        private com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> fromDate = null;
        private com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> backgroundColor = null;
        private com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> icon = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.resource!= null) {
                products.put("resource", this.resource.init());
            }
            if (this.entityMeta!= null) {
                products.put("entityMeta", this.entityMeta.init());
            }
            if (this.id!= null) {
                products.put("id", this.id.init());
            }
            if (this.fromDate!= null) {
                products.put("fromDate", this.fromDate.init());
            }
            if (this.backgroundColor!= null) {
                products.put("backgroundColor", this.backgroundColor.init());
            }
            if (this.icon!= null) {
                products.put("icon", this.icon.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> resource() {
            return ((this.resource == null)?this.resource = new com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>>(this._root, this, "resource"):this.resource);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> entityMeta() {
            return ((this.entityMeta == null)?this.entityMeta = new com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>>(this._root, this, "entityMeta"):this.entityMeta);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> id() {
            return ((this.id == null)?this.id = new com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>>(this._root, this, "id"):this.id);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> fromDate() {
            return ((this.fromDate == null)?this.fromDate = new com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>>(this._root, this, "fromDate"):this.fromDate);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> backgroundColor() {
            return ((this.backgroundColor == null)?this.backgroundColor = new com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>>(this._root, this, "backgroundColor"):this.backgroundColor);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>> icon() {
            return ((this.icon == null)?this.icon = new com.kscs.util.jaxb.Selector<TRoot, Milestone.Selector<TRoot, TParent>>(this._root, this, "icon"):this.icon);
        }

    }

}
