package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for milestone-def complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="milestone-def"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="resource-ref-field" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entity" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="date-from-field" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="icon" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "milestone-def")
public class MilestoneDef implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "resource-ref-field", required = true)
    protected String resourceRefField;
    @XmlAttribute(name = "entity", required = true)
    protected String entity;
    @XmlAttribute(name = "date-from-field", required = true)
    protected String dateFromField;
    @XmlAttribute(name = "icon", required = true)
    protected String icon;

    /**
     * Gets the value of the resourceRefField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceRefField() {
        return resourceRefField;
    }

    /**
     * Sets the value of the resourceRefField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceRefField(String value) {
        this.resourceRefField = value;
    }

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the dateFromField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateFromField() {
        return dateFromField;
    }

    /**
     * Sets the value of the dateFromField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateFromField(String value) {
        this.dateFromField = value;
    }

    /**
     * Gets the value of the icon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Sets the value of the icon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcon(String value) {
        this.icon = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theResourceRefField;
            theResourceRefField = this.getResourceRefField();
            strategy.appendField(locator, this, "resourceRefField", buffer, theResourceRefField);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            String theDateFromField;
            theDateFromField = this.getDateFromField();
            strategy.appendField(locator, this, "dateFromField", buffer, theDateFromField);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            strategy.appendField(locator, this, "icon", buffer, theIcon);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof MilestoneDef)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final MilestoneDef that = ((MilestoneDef) object);
        {
            String lhsResourceRefField;
            lhsResourceRefField = this.getResourceRefField();
            String rhsResourceRefField;
            rhsResourceRefField = that.getResourceRefField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceRefField", lhsResourceRefField), LocatorUtils.property(thatLocator, "resourceRefField", rhsResourceRefField), lhsResourceRefField, rhsResourceRefField)) {
                return false;
            }
        }
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            String lhsDateFromField;
            lhsDateFromField = this.getDateFromField();
            String rhsDateFromField;
            rhsDateFromField = that.getDateFromField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateFromField", lhsDateFromField), LocatorUtils.property(thatLocator, "dateFromField", rhsDateFromField), lhsDateFromField, rhsDateFromField)) {
                return false;
            }
        }
        {
            String lhsIcon;
            lhsIcon = this.getIcon();
            String rhsIcon;
            rhsIcon = that.getIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "icon", lhsIcon), LocatorUtils.property(thatLocator, "icon", rhsIcon), lhsIcon, rhsIcon)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theResourceRefField;
            theResourceRefField = this.getResourceRefField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceRefField", theResourceRefField), currentHashCode, theResourceRefField);
        }
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            String theDateFromField;
            theDateFromField = this.getDateFromField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dateFromField", theDateFromField), currentHashCode, theDateFromField);
        }
        {
            String theIcon;
            theIcon = this.getIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "icon", theIcon), currentHashCode, theIcon);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof MilestoneDef) {
            final MilestoneDef copy = ((MilestoneDef) draftCopy);
            if (this.resourceRefField!= null) {
                String sourceResourceRefField;
                sourceResourceRefField = this.getResourceRefField();
                String copyResourceRefField = ((String) strategy.copy(LocatorUtils.property(locator, "resourceRefField", sourceResourceRefField), sourceResourceRefField));
                copy.setResourceRefField(copyResourceRefField);
            } else {
                copy.resourceRefField = null;
            }
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.dateFromField!= null) {
                String sourceDateFromField;
                sourceDateFromField = this.getDateFromField();
                String copyDateFromField = ((String) strategy.copy(LocatorUtils.property(locator, "dateFromField", sourceDateFromField), sourceDateFromField));
                copy.setDateFromField(copyDateFromField);
            } else {
                copy.dateFromField = null;
            }
            if (this.icon!= null) {
                String sourceIcon;
                sourceIcon = this.getIcon();
                String copyIcon = ((String) strategy.copy(LocatorUtils.property(locator, "icon", sourceIcon), sourceIcon));
                copy.setIcon(copyIcon);
            } else {
                copy.icon = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new MilestoneDef();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MilestoneDef.Builder<_B> _other) {
        _other.resourceRefField = this.resourceRefField;
        _other.entity = this.entity;
        _other.dateFromField = this.dateFromField;
        _other.icon = this.icon;
    }

    public<_B >MilestoneDef.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new MilestoneDef.Builder<_B>(_parentBuilder, this, true);
    }

    public MilestoneDef.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static MilestoneDef.Builder<Void> builder() {
        return new MilestoneDef.Builder<Void>(null, null, false);
    }

    public static<_B >MilestoneDef.Builder<_B> copyOf(final MilestoneDef _other) {
        final MilestoneDef.Builder<_B> _newBuilder = new MilestoneDef.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final MilestoneDef.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree resourceRefFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceRefField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceRefFieldPropertyTree!= null):((resourceRefFieldPropertyTree == null)||(!resourceRefFieldPropertyTree.isLeaf())))) {
            _other.resourceRefField = this.resourceRefField;
        }
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree dateFromFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateFromField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateFromFieldPropertyTree!= null):((dateFromFieldPropertyTree == null)||(!dateFromFieldPropertyTree.isLeaf())))) {
            _other.dateFromField = this.dateFromField;
        }
        final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
            _other.icon = this.icon;
        }
    }

    public<_B >MilestoneDef.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new MilestoneDef.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public MilestoneDef.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >MilestoneDef.Builder<_B> copyOf(final MilestoneDef _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final MilestoneDef.Builder<_B> _newBuilder = new MilestoneDef.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static MilestoneDef.Builder<Void> copyExcept(final MilestoneDef _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static MilestoneDef.Builder<Void> copyOnly(final MilestoneDef _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final MilestoneDef _storedValue;
        private String resourceRefField;
        private String entity;
        private String dateFromField;
        private String icon;

        public Builder(final _B _parentBuilder, final MilestoneDef _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.resourceRefField = _other.resourceRefField;
                    this.entity = _other.entity;
                    this.dateFromField = _other.dateFromField;
                    this.icon = _other.icon;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final MilestoneDef _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree resourceRefFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceRefField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceRefFieldPropertyTree!= null):((resourceRefFieldPropertyTree == null)||(!resourceRefFieldPropertyTree.isLeaf())))) {
                        this.resourceRefField = _other.resourceRefField;
                    }
                    final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                        this.entity = _other.entity;
                    }
                    final PropertyTree dateFromFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateFromField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateFromFieldPropertyTree!= null):((dateFromFieldPropertyTree == null)||(!dateFromFieldPropertyTree.isLeaf())))) {
                        this.dateFromField = _other.dateFromField;
                    }
                    final PropertyTree iconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("icon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(iconPropertyTree!= null):((iconPropertyTree == null)||(!iconPropertyTree.isLeaf())))) {
                        this.icon = _other.icon;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends MilestoneDef >_P init(final _P _product) {
            _product.resourceRefField = this.resourceRefField;
            _product.entity = this.entity;
            _product.dateFromField = this.dateFromField;
            _product.icon = this.icon;
            return _product;
        }

        /**
         * Sets the new value of "resourceRefField" (any previous value will be replaced)
         * 
         * @param resourceRefField
         *     New value of the "resourceRefField" property.
         */
        public MilestoneDef.Builder<_B> withResourceRefField(final String resourceRefField) {
            this.resourceRefField = resourceRefField;
            return this;
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public MilestoneDef.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "dateFromField" (any previous value will be replaced)
         * 
         * @param dateFromField
         *     New value of the "dateFromField" property.
         */
        public MilestoneDef.Builder<_B> withDateFromField(final String dateFromField) {
            this.dateFromField = dateFromField;
            return this;
        }

        /**
         * Sets the new value of "icon" (any previous value will be replaced)
         * 
         * @param icon
         *     New value of the "icon" property.
         */
        public MilestoneDef.Builder<_B> withIcon(final String icon) {
            this.icon = icon;
            return this;
        }

        @Override
        public MilestoneDef build() {
            if (_storedValue == null) {
                return this.init(new MilestoneDef());
            } else {
                return ((MilestoneDef) _storedValue);
            }
        }

        public MilestoneDef.Builder<_B> copyOf(final MilestoneDef _other) {
            _other.copyTo(this);
            return this;
        }

        public MilestoneDef.Builder<_B> copyOf(final MilestoneDef.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends MilestoneDef.Selector<MilestoneDef.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static MilestoneDef.Select _root() {
            return new MilestoneDef.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>> resourceRefField = null;
        private com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>> dateFromField = null;
        private com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>> icon = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.resourceRefField!= null) {
                products.put("resourceRefField", this.resourceRefField.init());
            }
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.dateFromField!= null) {
                products.put("dateFromField", this.dateFromField.init());
            }
            if (this.icon!= null) {
                products.put("icon", this.icon.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>> resourceRefField() {
            return ((this.resourceRefField == null)?this.resourceRefField = new com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>>(this._root, this, "resourceRefField"):this.resourceRefField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>> dateFromField() {
            return ((this.dateFromField == null)?this.dateFromField = new com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>>(this._root, this, "dateFromField"):this.dateFromField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>> icon() {
            return ((this.icon == null)?this.icon = new com.kscs.util.jaxb.Selector<TRoot, MilestoneDef.Selector<TRoot, TParent>>(this._root, this, "icon"):this.icon);
        }

    }

}
