package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for nuclet-extensions-styles complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nuclet-extensions-styles"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="css" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nuclet-extensions-styles", propOrder = {
    "css"
})
public class NucletExtensionsStyles implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<String> css;

    /**
     * Gets the value of the css property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the css property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCss().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCss() {
        if (css == null) {
            css = new ArrayList<String>();
        }
        return this.css;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<String> theCss;
            theCss = (((this.css!= null)&&(!this.css.isEmpty()))?this.getCss():null);
            strategy.appendField(locator, this, "css", buffer, theCss);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof NucletExtensionsStyles)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final NucletExtensionsStyles that = ((NucletExtensionsStyles) object);
        {
            List<String> lhsCss;
            lhsCss = (((this.css!= null)&&(!this.css.isEmpty()))?this.getCss():null);
            List<String> rhsCss;
            rhsCss = (((that.css!= null)&&(!that.css.isEmpty()))?that.getCss():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "css", lhsCss), LocatorUtils.property(thatLocator, "css", rhsCss), lhsCss, rhsCss)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<String> theCss;
            theCss = (((this.css!= null)&&(!this.css.isEmpty()))?this.getCss():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "css", theCss), currentHashCode, theCss);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof NucletExtensionsStyles) {
            final NucletExtensionsStyles copy = ((NucletExtensionsStyles) draftCopy);
            if ((this.css!= null)&&(!this.css.isEmpty())) {
                List<String> sourceCss;
                sourceCss = (((this.css!= null)&&(!this.css.isEmpty()))?this.getCss():null);
                @SuppressWarnings("unchecked")
                List<String> copyCss = ((List<String> ) strategy.copy(LocatorUtils.property(locator, "css", sourceCss), sourceCss));
                copy.css = null;
                if (copyCss!= null) {
                    List<String> uniqueCssl = copy.getCss();
                    uniqueCssl.addAll(copyCss);
                }
            } else {
                copy.css = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new NucletExtensionsStyles();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final NucletExtensionsStyles.Builder<_B> _other) {
        if (this.css == null) {
            _other.css = null;
        } else {
            _other.css = new ArrayList<Buildable>();
            for (String _item: this.css) {
                _other.css.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
            }
        }
    }

    public<_B >NucletExtensionsStyles.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new NucletExtensionsStyles.Builder<_B>(_parentBuilder, this, true);
    }

    public NucletExtensionsStyles.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static NucletExtensionsStyles.Builder<Void> builder() {
        return new NucletExtensionsStyles.Builder<Void>(null, null, false);
    }

    public static<_B >NucletExtensionsStyles.Builder<_B> copyOf(final NucletExtensionsStyles _other) {
        final NucletExtensionsStyles.Builder<_B> _newBuilder = new NucletExtensionsStyles.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final NucletExtensionsStyles.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree cssPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("css"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cssPropertyTree!= null):((cssPropertyTree == null)||(!cssPropertyTree.isLeaf())))) {
            if (this.css == null) {
                _other.css = null;
            } else {
                _other.css = new ArrayList<Buildable>();
                for (String _item: this.css) {
                    _other.css.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                }
            }
        }
    }

    public<_B >NucletExtensionsStyles.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new NucletExtensionsStyles.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public NucletExtensionsStyles.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >NucletExtensionsStyles.Builder<_B> copyOf(final NucletExtensionsStyles _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final NucletExtensionsStyles.Builder<_B> _newBuilder = new NucletExtensionsStyles.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static NucletExtensionsStyles.Builder<Void> copyExcept(final NucletExtensionsStyles _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static NucletExtensionsStyles.Builder<Void> copyOnly(final NucletExtensionsStyles _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final NucletExtensionsStyles _storedValue;
        private List<Buildable> css;

        public Builder(final _B _parentBuilder, final NucletExtensionsStyles _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.css == null) {
                        this.css = null;
                    } else {
                        this.css = new ArrayList<Buildable>();
                        for (String _item: _other.css) {
                            this.css.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final NucletExtensionsStyles _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree cssPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("css"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(cssPropertyTree!= null):((cssPropertyTree == null)||(!cssPropertyTree.isLeaf())))) {
                        if (_other.css == null) {
                            this.css = null;
                        } else {
                            this.css = new ArrayList<Buildable>();
                            for (String _item: _other.css) {
                                this.css.add(((_item == null)?null:new Buildable.PrimitiveBuildable(_item)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends NucletExtensionsStyles >_P init(final _P _product) {
            if (this.css!= null) {
                final List<String> css = new ArrayList<String>(this.css.size());
                for (Buildable _item: this.css) {
                    css.add(((String) _item.build()));
                }
                _product.css = css;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "css"
         * 
         * @param css
         *     Items to add to the value of the "css" property
         */
        public NucletExtensionsStyles.Builder<_B> addCss(final Iterable<? extends String> css) {
            if (css!= null) {
                if (this.css == null) {
                    this.css = new ArrayList<Buildable>();
                }
                for (String _item: css) {
                    this.css.add(new Buildable.PrimitiveBuildable(_item));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "css" (any previous value will be replaced)
         * 
         * @param css
         *     New value of the "css" property.
         */
        public NucletExtensionsStyles.Builder<_B> withCss(final Iterable<? extends String> css) {
            if (this.css!= null) {
                this.css.clear();
            }
            return addCss(css);
        }

        /**
         * Adds the given items to the value of "css"
         * 
         * @param css
         *     Items to add to the value of the "css" property
         */
        public NucletExtensionsStyles.Builder<_B> addCss(String... css) {
            addCss(Arrays.asList(css));
            return this;
        }

        /**
         * Sets the new value of "css" (any previous value will be replaced)
         * 
         * @param css
         *     New value of the "css" property.
         */
        public NucletExtensionsStyles.Builder<_B> withCss(String... css) {
            withCss(Arrays.asList(css));
            return this;
        }

        @Override
        public NucletExtensionsStyles build() {
            if (_storedValue == null) {
                return this.init(new NucletExtensionsStyles());
            } else {
                return ((NucletExtensionsStyles) _storedValue);
            }
        }

        public NucletExtensionsStyles.Builder<_B> copyOf(final NucletExtensionsStyles _other) {
            _other.copyTo(this);
            return this;
        }

        public NucletExtensionsStyles.Builder<_B> copyOf(final NucletExtensionsStyles.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends NucletExtensionsStyles.Selector<NucletExtensionsStyles.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static NucletExtensionsStyles.Select _root() {
            return new NucletExtensionsStyles.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, NucletExtensionsStyles.Selector<TRoot, TParent>> css = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.css!= null) {
                products.put("css", this.css.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, NucletExtensionsStyles.Selector<TRoot, TParent>> css() {
            return ((this.css == null)?this.css = new com.kscs.util.jaxb.Selector<TRoot, NucletExtensionsStyles.Selector<TRoot, TParent>>(this._root, this, "css"):this.css);
        }

    }

}
