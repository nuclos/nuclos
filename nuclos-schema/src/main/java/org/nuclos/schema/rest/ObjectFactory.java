package org.nuclos.schema.rest;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.nuclos.schema.rest package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.nuclos.schema.rest
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LoginInfo }
     * 
     */
    public LoginInfo createLoginInfo() {
        return new LoginInfo();
    }

    /**
     * Create an instance of {@link LegalDisclaimer }
     * 
     */
    public LegalDisclaimer createLegalDisclaimer() {
        return new LegalDisclaimer();
    }

    /**
     * Create an instance of {@link News }
     * 
     */
    public News createNews() {
        return new News();
    }

    /**
     * Create an instance of {@link RestLink }
     * 
     */
    public RestLink createRestLink() {
        return new RestLink();
    }

    /**
     * Create an instance of {@link SsoAuthEndpoint }
     * 
     */
    public SsoAuthEndpoint createSsoAuthEndpoint() {
        return new SsoAuthEndpoint();
    }

    /**
     * Create an instance of {@link MandatorLinks }
     * 
     */
    public MandatorLinks createMandatorLinks() {
        return new MandatorLinks();
    }

    /**
     * Create an instance of {@link Mandator }
     * 
     */
    public Mandator createMandator() {
        return new Mandator();
    }

    /**
     * Create an instance of {@link LoginParams }
     * 
     */
    public LoginParams createLoginParams() {
        return new LoginParams();
    }

    /**
     * Create an instance of {@link QueryParam }
     * 
     */
    public QueryParam createQueryParam() {
        return new QueryParam();
    }

    /**
     * Create an instance of {@link LoginInfoLinks }
     * 
     */
    public LoginInfoLinks createLoginInfoLinks() {
        return new LoginInfoLinks();
    }

    /**
     * Create an instance of {@link LocaleInfo }
     * 
     */
    public LocaleInfo createLocaleInfo() {
        return new LocaleInfo();
    }

    /**
     * Create an instance of {@link DataLanguage }
     * 
     */
    public DataLanguage createDataLanguage() {
        return new DataLanguage();
    }

    /**
     * Create an instance of {@link ServerStatus }
     * 
     */
    public ServerStatus createServerStatus() {
        return new ServerStatus();
    }

    /**
     * Create an instance of {@link ObjectId }
     * 
     */
    public ObjectId createObjectId() {
        return new ObjectId();
    }

    /**
     * Create an instance of {@link MultiSelectionResult }
     * 
     */
    public MultiSelectionResult createMultiSelectionResult() {
        return new MultiSelectionResult();
    }

    /**
     * Create an instance of {@link EditAttributeMap }
     * 
     */
    public EditAttributeMap createEditAttributeMap() {
        return new EditAttributeMap();
    }

    /**
     * Create an instance of {@link EditSelectionResult }
     * 
     */
    public EditSelectionResult createEditSelectionResult() {
        return new EditSelectionResult();
    }

    /**
     * Create an instance of {@link CollectiveProcessingActionLinks }
     * 
     */
    public CollectiveProcessingActionLinks createCollectiveProcessingActionLinks() {
        return new CollectiveProcessingActionLinks();
    }

    /**
     * Create an instance of {@link CollectiveProcessingAction }
     * 
     */
    public CollectiveProcessingAction createCollectiveProcessingAction() {
        return new CollectiveProcessingAction();
    }

    /**
     * Create an instance of {@link CollectiveProcessingSelectionOptions }
     * 
     */
    public CollectiveProcessingSelectionOptions createCollectiveProcessingSelectionOptions() {
        return new CollectiveProcessingSelectionOptions();
    }

    /**
     * Create an instance of {@link CollectiveProcessingExecutionLinks }
     * 
     */
    public CollectiveProcessingExecutionLinks createCollectiveProcessingExecutionLinks() {
        return new CollectiveProcessingExecutionLinks();
    }

    /**
     * Create an instance of {@link CollectiveProcessingExecution }
     * 
     */
    public CollectiveProcessingExecution createCollectiveProcessingExecution() {
        return new CollectiveProcessingExecution();
    }

    /**
     * Create an instance of {@link CollectiveProcessingProgressInfo }
     * 
     */
    public CollectiveProcessingProgressInfo createCollectiveProcessingProgressInfo() {
        return new CollectiveProcessingProgressInfo();
    }

    /**
     * Create an instance of {@link InputRequiredResultMapItem }
     * 
     */
    public InputRequiredResultMapItem createInputRequiredResultMapItem() {
        return new InputRequiredResultMapItem();
    }

    /**
     * Create an instance of {@link InputRequiredSpecification }
     * 
     */
    public InputRequiredSpecification createInputRequiredSpecification() {
        return new InputRequiredSpecification();
    }

    /**
     * Create an instance of {@link CollectiveProcessingObjectInfoInputrequired }
     * 
     */
    public CollectiveProcessingObjectInfoInputrequired createCollectiveProcessingObjectInfoInputrequired() {
        return new CollectiveProcessingObjectInfoInputrequired();
    }

    /**
     * Create an instance of {@link CollectiveProcessingObjectInfo }
     * 
     */
    public CollectiveProcessingObjectInfo createCollectiveProcessingObjectInfo() {
        return new CollectiveProcessingObjectInfo();
    }

    /**
     * Create an instance of {@link EntityMetaBaseLinks }
     * 
     */
    public EntityMetaBaseLinks createEntityMetaBaseLinks() {
        return new EntityMetaBaseLinks();
    }

    /**
     * Create an instance of {@link EntityMetaBase }
     * 
     */
    public EntityMetaBase createEntityMetaBase() {
        return new EntityMetaBase();
    }

    /**
     * Create an instance of {@link EntityMetaOverview }
     * 
     */
    public EntityMetaOverview createEntityMetaOverview() {
        return new EntityMetaOverview();
    }

    /**
     * Create an instance of {@link SearchfilterEntityMeta }
     * 
     */
    public SearchfilterEntityMeta createSearchfilterEntityMeta() {
        return new SearchfilterEntityMeta();
    }

    /**
     * Create an instance of {@link TaskEntityMeta }
     * 
     */
    public TaskEntityMeta createTaskEntityMeta() {
        return new TaskEntityMeta();
    }

    /**
     * Create an instance of {@link EntityObjectAttributes }
     * 
     */
    public EntityObjectAttributes createEntityObjectAttributes() {
        return new EntityObjectAttributes();
    }

    /**
     * Create an instance of {@link EntityObjectLinks }
     * 
     */
    public EntityObjectLinks createEntityObjectLinks() {
        return new EntityObjectLinks();
    }

    /**
     * Create an instance of {@link EntityObject }
     * 
     */
    public EntityObject createEntityObject() {
        return new EntityObject();
    }

    /**
     * Create an instance of {@link EntityObjectResultList }
     * 
     */
    public EntityObjectResultList createEntityObjectResultList() {
        return new EntityObjectResultList();
    }

    /**
     * Create an instance of {@link VlpParameter }
     * 
     */
    public VlpParameter createVlpParameter() {
        return new VlpParameter();
    }

    /**
     * Create an instance of {@link Vlp }
     * 
     */
    public Vlp createVlp() {
        return new Vlp();
    }

    /**
     * Create an instance of {@link PrintoutOutputFormatParameter }
     * 
     */
    public PrintoutOutputFormatParameter createPrintoutOutputFormatParameter() {
        return new PrintoutOutputFormatParameter();
    }

    /**
     * Create an instance of {@link PrintoutOutputFormat }
     * 
     */
    public PrintoutOutputFormat createPrintoutOutputFormat() {
        return new PrintoutOutputFormat();
    }

    /**
     * Create an instance of {@link PrintoutList }
     * 
     */
    public PrintoutList createPrintoutList() {
        return new PrintoutList();
    }

    /**
     * Create an instance of {@link InputRequiredContext }
     * 
     */
    public InputRequiredContext createInputRequiredContext() {
        return new InputRequiredContext();
    }

    /**
     * Create an instance of {@link InputRequired }
     * 
     */
    public InputRequired createInputRequired() {
        return new InputRequired();
    }

    /**
     * Create an instance of {@link QueryContext }
     * 
     */
    public QueryContext createQueryContext() {
        return new QueryContext();
    }

    /**
     * Create an instance of {@link LovEntry }
     * 
     */
    public LovEntry createLovEntry() {
        return new LovEntry();
    }

    /**
     * Create an instance of {@link GlobalSearchResult }
     * 
     */
    public GlobalSearchResult createGlobalSearchResult() {
        return new GlobalSearchResult();
    }

    /**
     * Create an instance of {@link StatusInfo }
     * 
     */
    public StatusInfo createStatusInfo() {
        return new StatusInfo();
    }

    /**
     * Create an instance of {@link StatusPath }
     * 
     */
    public StatusPath createStatusPath() {
        return new StatusPath();
    }

    /**
     * Create an instance of {@link EvaluatedTitleExpression }
     * 
     */
    public EvaluatedTitleExpression createEvaluatedTitleExpression() {
        return new EvaluatedTitleExpression();
    }

    /**
     * Create an instance of {@link Logger }
     * 
     */
    public Logger createLogger() {
        return new Logger();
    }

    /**
     * Create an instance of {@link Loggers }
     * 
     */
    public Loggers createLoggers() {
        return new Loggers();
    }

    /**
     * Create an instance of {@link LogLevel }
     * 
     */
    public LogLevel createLogLevel() {
        return new LogLevel();
    }

    /**
     * Create an instance of {@link DebugSql }
     * 
     */
    public DebugSql createDebugSql() {
        return new DebugSql();
    }

    /**
     * Create an instance of {@link JobStatus }
     * 
     */
    public JobStatus createJobStatus() {
        return new JobStatus();
    }

    /**
     * Create an instance of {@link RestPreference }
     * 
     */
    public RestPreference createRestPreference() {
        return new RestPreference();
    }

    /**
     * Create an instance of {@link RestPreferenceUserRole }
     * 
     */
    public RestPreferenceUserRole createRestPreferenceUserRole() {
        return new RestPreferenceUserRole();
    }

    /**
     * Create an instance of {@link RestPreferenceShare }
     * 
     */
    public RestPreferenceShare createRestPreferenceShare() {
        return new RestPreferenceShare();
    }

    /**
     * Create an instance of {@link SearchfilterInfo }
     * 
     */
    public SearchfilterInfo createSearchfilterInfo() {
        return new SearchfilterInfo();
    }

    /**
     * Create an instance of {@link BackgroundEvent }
     * 
     */
    public BackgroundEvent createBackgroundEvent() {
        return new BackgroundEvent();
    }

    /**
     * Create an instance of {@link PlanningResource }
     * 
     */
    public PlanningResource createPlanningResource() {
        return new PlanningResource();
    }

    /**
     * Create an instance of {@link BusinessHour }
     * 
     */
    public BusinessHour createBusinessHour() {
        return new BusinessHour();
    }

    /**
     * Create an instance of {@link Booking }
     * 
     */
    public Booking createBooking() {
        return new Booking();
    }

    /**
     * Create an instance of {@link Milestone }
     * 
     */
    public Milestone createMilestone() {
        return new Milestone();
    }

    /**
     * Create an instance of {@link Relation }
     * 
     */
    public Relation createRelation() {
        return new Relation();
    }

    /**
     * Create an instance of {@link PlanningTable }
     * 
     */
    public PlanningTable createPlanningTable() {
        return new PlanningTable();
    }

    /**
     * Create an instance of {@link MilestoneDef }
     * 
     */
    public MilestoneDef createMilestoneDef() {
        return new MilestoneDef();
    }

    /**
     * Create an instance of {@link BookingDef }
     * 
     */
    public BookingDef createBookingDef() {
        return new BookingDef();
    }

    /**
     * Create an instance of {@link RelationDef }
     * 
     */
    public RelationDef createRelationDef() {
        return new RelationDef();
    }

    /**
     * Create an instance of {@link PlanElementDef }
     * 
     */
    public PlanElementDef createPlanElementDef() {
        return new PlanElementDef();
    }

    /**
     * Create an instance of {@link PlanElementDefList }
     * 
     */
    public PlanElementDefList createPlanElementDefList() {
        return new PlanElementDefList();
    }

    /**
     * Create an instance of {@link RestReport }
     * 
     */
    public RestReport createRestReport() {
        return new RestReport();
    }

    /**
     * Create an instance of {@link NucletExtensionsStyles }
     * 
     */
    public NucletExtensionsStyles createNucletExtensionsStyles() {
        return new NucletExtensionsStyles();
    }

    /**
     * Create an instance of {@link RestSystemparameters }
     * 
     */
    public RestSystemparameters createRestSystemparameters() {
        return new RestSystemparameters();
    }

    /**
     * Create an instance of {@link MenuEntry }
     * 
     */
    public MenuEntry createMenuEntry() {
        return new MenuEntry();
    }

    /**
     * Create an instance of {@link TaskMenuEntry }
     * 
     */
    public TaskMenuEntry createTaskMenuEntry() {
        return new TaskMenuEntry();
    }

    /**
     * Create an instance of {@link LayoutLinks }
     * 
     */
    public LayoutLinks createLayoutLinks() {
        return new LayoutLinks();
    }

    /**
     * Create an instance of {@link ProcessLayoutLinks }
     * 
     */
    public ProcessLayoutLinks createProcessLayoutLinks() {
        return new ProcessLayoutLinks();
    }

    /**
     * Create an instance of {@link TreeNode }
     * 
     */
    public TreeNode createTreeNode() {
        return new TreeNode();
    }

    /**
     * Create an instance of {@link MatrixXAxisObject }
     * 
     */
    public MatrixXAxisObject createMatrixXAxisObject() {
        return new MatrixXAxisObject();
    }

    /**
     * Create an instance of {@link MatrixYAxisObject }
     * 
     */
    public MatrixYAxisObject createMatrixYAxisObject() {
        return new MatrixYAxisObject();
    }

    /**
     * Create an instance of {@link MatrixYAxisColumn }
     * 
     */
    public MatrixYAxisColumn createMatrixYAxisColumn() {
        return new MatrixYAxisColumn();
    }

    /**
     * Create an instance of {@link MatrixData }
     * 
     */
    public MatrixData createMatrixData() {
        return new MatrixData();
    }

    /**
     * Create an instance of {@link MatrixRequestParameters }
     * 
     */
    public MatrixRequestParameters createMatrixRequestParameters() {
        return new MatrixRequestParameters();
    }

    /**
     * Create an instance of {@link LoginInfo.AllowedActions }
     * 
     */
    public LoginInfo.AllowedActions createLoginInfoAllowedActions() {
        return new LoginInfo.AllowedActions();
    }

}
