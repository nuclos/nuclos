package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for plan-element-def complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="plan-element-def"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="milestone-def" type="{urn:org.nuclos.schema.rest}milestone-def"/&gt;
 *         &lt;element name="booking-def" type="{urn:org.nuclos.schema.rest}booking-def"/&gt;
 *         &lt;element name="relation-def" type="{urn:org.nuclos.schema.rest}relation-def"/&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="element-type" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "plan-element-def", propOrder = {
    "milestoneDef",
    "bookingDef",
    "relationDef"
})
public class PlanElementDef implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "milestone-def")
    protected MilestoneDef milestoneDef;
    @XmlElement(name = "booking-def")
    protected BookingDef bookingDef;
    @XmlElement(name = "relation-def")
    protected RelationDef relationDef;
    @XmlAttribute(name = "element-type", required = true)
    protected String elementType;

    /**
     * Gets the value of the milestoneDef property.
     * 
     * @return
     *     possible object is
     *     {@link MilestoneDef }
     *     
     */
    public MilestoneDef getMilestoneDef() {
        return milestoneDef;
    }

    /**
     * Sets the value of the milestoneDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link MilestoneDef }
     *     
     */
    public void setMilestoneDef(MilestoneDef value) {
        this.milestoneDef = value;
    }

    /**
     * Gets the value of the bookingDef property.
     * 
     * @return
     *     possible object is
     *     {@link BookingDef }
     *     
     */
    public BookingDef getBookingDef() {
        return bookingDef;
    }

    /**
     * Sets the value of the bookingDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingDef }
     *     
     */
    public void setBookingDef(BookingDef value) {
        this.bookingDef = value;
    }

    /**
     * Gets the value of the relationDef property.
     * 
     * @return
     *     possible object is
     *     {@link RelationDef }
     *     
     */
    public RelationDef getRelationDef() {
        return relationDef;
    }

    /**
     * Sets the value of the relationDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationDef }
     *     
     */
    public void setRelationDef(RelationDef value) {
        this.relationDef = value;
    }

    /**
     * Gets the value of the elementType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementType() {
        return elementType;
    }

    /**
     * Sets the value of the elementType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementType(String value) {
        this.elementType = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            MilestoneDef theMilestoneDef;
            theMilestoneDef = this.getMilestoneDef();
            strategy.appendField(locator, this, "milestoneDef", buffer, theMilestoneDef);
        }
        {
            BookingDef theBookingDef;
            theBookingDef = this.getBookingDef();
            strategy.appendField(locator, this, "bookingDef", buffer, theBookingDef);
        }
        {
            RelationDef theRelationDef;
            theRelationDef = this.getRelationDef();
            strategy.appendField(locator, this, "relationDef", buffer, theRelationDef);
        }
        {
            String theElementType;
            theElementType = this.getElementType();
            strategy.appendField(locator, this, "elementType", buffer, theElementType);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PlanElementDef)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PlanElementDef that = ((PlanElementDef) object);
        {
            MilestoneDef lhsMilestoneDef;
            lhsMilestoneDef = this.getMilestoneDef();
            MilestoneDef rhsMilestoneDef;
            rhsMilestoneDef = that.getMilestoneDef();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "milestoneDef", lhsMilestoneDef), LocatorUtils.property(thatLocator, "milestoneDef", rhsMilestoneDef), lhsMilestoneDef, rhsMilestoneDef)) {
                return false;
            }
        }
        {
            BookingDef lhsBookingDef;
            lhsBookingDef = this.getBookingDef();
            BookingDef rhsBookingDef;
            rhsBookingDef = that.getBookingDef();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bookingDef", lhsBookingDef), LocatorUtils.property(thatLocator, "bookingDef", rhsBookingDef), lhsBookingDef, rhsBookingDef)) {
                return false;
            }
        }
        {
            RelationDef lhsRelationDef;
            lhsRelationDef = this.getRelationDef();
            RelationDef rhsRelationDef;
            rhsRelationDef = that.getRelationDef();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "relationDef", lhsRelationDef), LocatorUtils.property(thatLocator, "relationDef", rhsRelationDef), lhsRelationDef, rhsRelationDef)) {
                return false;
            }
        }
        {
            String lhsElementType;
            lhsElementType = this.getElementType();
            String rhsElementType;
            rhsElementType = that.getElementType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "elementType", lhsElementType), LocatorUtils.property(thatLocator, "elementType", rhsElementType), lhsElementType, rhsElementType)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            MilestoneDef theMilestoneDef;
            theMilestoneDef = this.getMilestoneDef();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "milestoneDef", theMilestoneDef), currentHashCode, theMilestoneDef);
        }
        {
            BookingDef theBookingDef;
            theBookingDef = this.getBookingDef();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bookingDef", theBookingDef), currentHashCode, theBookingDef);
        }
        {
            RelationDef theRelationDef;
            theRelationDef = this.getRelationDef();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "relationDef", theRelationDef), currentHashCode, theRelationDef);
        }
        {
            String theElementType;
            theElementType = this.getElementType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "elementType", theElementType), currentHashCode, theElementType);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PlanElementDef) {
            final PlanElementDef copy = ((PlanElementDef) draftCopy);
            if (this.milestoneDef!= null) {
                MilestoneDef sourceMilestoneDef;
                sourceMilestoneDef = this.getMilestoneDef();
                MilestoneDef copyMilestoneDef = ((MilestoneDef) strategy.copy(LocatorUtils.property(locator, "milestoneDef", sourceMilestoneDef), sourceMilestoneDef));
                copy.setMilestoneDef(copyMilestoneDef);
            } else {
                copy.milestoneDef = null;
            }
            if (this.bookingDef!= null) {
                BookingDef sourceBookingDef;
                sourceBookingDef = this.getBookingDef();
                BookingDef copyBookingDef = ((BookingDef) strategy.copy(LocatorUtils.property(locator, "bookingDef", sourceBookingDef), sourceBookingDef));
                copy.setBookingDef(copyBookingDef);
            } else {
                copy.bookingDef = null;
            }
            if (this.relationDef!= null) {
                RelationDef sourceRelationDef;
                sourceRelationDef = this.getRelationDef();
                RelationDef copyRelationDef = ((RelationDef) strategy.copy(LocatorUtils.property(locator, "relationDef", sourceRelationDef), sourceRelationDef));
                copy.setRelationDef(copyRelationDef);
            } else {
                copy.relationDef = null;
            }
            if (this.elementType!= null) {
                String sourceElementType;
                sourceElementType = this.getElementType();
                String copyElementType = ((String) strategy.copy(LocatorUtils.property(locator, "elementType", sourceElementType), sourceElementType));
                copy.setElementType(copyElementType);
            } else {
                copy.elementType = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PlanElementDef();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PlanElementDef.Builder<_B> _other) {
        _other.milestoneDef = ((this.milestoneDef == null)?null:this.milestoneDef.newCopyBuilder(_other));
        _other.bookingDef = ((this.bookingDef == null)?null:this.bookingDef.newCopyBuilder(_other));
        _other.relationDef = ((this.relationDef == null)?null:this.relationDef.newCopyBuilder(_other));
        _other.elementType = this.elementType;
    }

    public<_B >PlanElementDef.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PlanElementDef.Builder<_B>(_parentBuilder, this, true);
    }

    public PlanElementDef.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PlanElementDef.Builder<Void> builder() {
        return new PlanElementDef.Builder<Void>(null, null, false);
    }

    public static<_B >PlanElementDef.Builder<_B> copyOf(final PlanElementDef _other) {
        final PlanElementDef.Builder<_B> _newBuilder = new PlanElementDef.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PlanElementDef.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree milestoneDefPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("milestoneDef"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(milestoneDefPropertyTree!= null):((milestoneDefPropertyTree == null)||(!milestoneDefPropertyTree.isLeaf())))) {
            _other.milestoneDef = ((this.milestoneDef == null)?null:this.milestoneDef.newCopyBuilder(_other, milestoneDefPropertyTree, _propertyTreeUse));
        }
        final PropertyTree bookingDefPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bookingDef"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bookingDefPropertyTree!= null):((bookingDefPropertyTree == null)||(!bookingDefPropertyTree.isLeaf())))) {
            _other.bookingDef = ((this.bookingDef == null)?null:this.bookingDef.newCopyBuilder(_other, bookingDefPropertyTree, _propertyTreeUse));
        }
        final PropertyTree relationDefPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("relationDef"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(relationDefPropertyTree!= null):((relationDefPropertyTree == null)||(!relationDefPropertyTree.isLeaf())))) {
            _other.relationDef = ((this.relationDef == null)?null:this.relationDef.newCopyBuilder(_other, relationDefPropertyTree, _propertyTreeUse));
        }
        final PropertyTree elementTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("elementType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(elementTypePropertyTree!= null):((elementTypePropertyTree == null)||(!elementTypePropertyTree.isLeaf())))) {
            _other.elementType = this.elementType;
        }
    }

    public<_B >PlanElementDef.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PlanElementDef.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PlanElementDef.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PlanElementDef.Builder<_B> copyOf(final PlanElementDef _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PlanElementDef.Builder<_B> _newBuilder = new PlanElementDef.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PlanElementDef.Builder<Void> copyExcept(final PlanElementDef _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PlanElementDef.Builder<Void> copyOnly(final PlanElementDef _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PlanElementDef _storedValue;
        private MilestoneDef.Builder<PlanElementDef.Builder<_B>> milestoneDef;
        private BookingDef.Builder<PlanElementDef.Builder<_B>> bookingDef;
        private RelationDef.Builder<PlanElementDef.Builder<_B>> relationDef;
        private String elementType;

        public Builder(final _B _parentBuilder, final PlanElementDef _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.milestoneDef = ((_other.milestoneDef == null)?null:_other.milestoneDef.newCopyBuilder(this));
                    this.bookingDef = ((_other.bookingDef == null)?null:_other.bookingDef.newCopyBuilder(this));
                    this.relationDef = ((_other.relationDef == null)?null:_other.relationDef.newCopyBuilder(this));
                    this.elementType = _other.elementType;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PlanElementDef _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree milestoneDefPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("milestoneDef"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(milestoneDefPropertyTree!= null):((milestoneDefPropertyTree == null)||(!milestoneDefPropertyTree.isLeaf())))) {
                        this.milestoneDef = ((_other.milestoneDef == null)?null:_other.milestoneDef.newCopyBuilder(this, milestoneDefPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree bookingDefPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bookingDef"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bookingDefPropertyTree!= null):((bookingDefPropertyTree == null)||(!bookingDefPropertyTree.isLeaf())))) {
                        this.bookingDef = ((_other.bookingDef == null)?null:_other.bookingDef.newCopyBuilder(this, bookingDefPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree relationDefPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("relationDef"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(relationDefPropertyTree!= null):((relationDefPropertyTree == null)||(!relationDefPropertyTree.isLeaf())))) {
                        this.relationDef = ((_other.relationDef == null)?null:_other.relationDef.newCopyBuilder(this, relationDefPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree elementTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("elementType"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(elementTypePropertyTree!= null):((elementTypePropertyTree == null)||(!elementTypePropertyTree.isLeaf())))) {
                        this.elementType = _other.elementType;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PlanElementDef >_P init(final _P _product) {
            _product.milestoneDef = ((this.milestoneDef == null)?null:this.milestoneDef.build());
            _product.bookingDef = ((this.bookingDef == null)?null:this.bookingDef.build());
            _product.relationDef = ((this.relationDef == null)?null:this.relationDef.build());
            _product.elementType = this.elementType;
            return _product;
        }

        /**
         * Sets the new value of "milestoneDef" (any previous value will be replaced)
         * 
         * @param milestoneDef
         *     New value of the "milestoneDef" property.
         */
        public PlanElementDef.Builder<_B> withMilestoneDef(final MilestoneDef milestoneDef) {
            this.milestoneDef = ((milestoneDef == null)?null:new MilestoneDef.Builder<PlanElementDef.Builder<_B>>(this, milestoneDef, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "milestoneDef" property.
         * Use {@link org.nuclos.schema.rest.MilestoneDef.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "milestoneDef" property.
         *     Use {@link org.nuclos.schema.rest.MilestoneDef.Builder#end()} to return to the current builder.
         */
        public MilestoneDef.Builder<? extends PlanElementDef.Builder<_B>> withMilestoneDef() {
            if (this.milestoneDef!= null) {
                return this.milestoneDef;
            }
            return this.milestoneDef = new MilestoneDef.Builder<PlanElementDef.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "bookingDef" (any previous value will be replaced)
         * 
         * @param bookingDef
         *     New value of the "bookingDef" property.
         */
        public PlanElementDef.Builder<_B> withBookingDef(final BookingDef bookingDef) {
            this.bookingDef = ((bookingDef == null)?null:new BookingDef.Builder<PlanElementDef.Builder<_B>>(this, bookingDef, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "bookingDef" property.
         * Use {@link org.nuclos.schema.rest.BookingDef.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "bookingDef" property.
         *     Use {@link org.nuclos.schema.rest.BookingDef.Builder#end()} to return to the current builder.
         */
        public BookingDef.Builder<? extends PlanElementDef.Builder<_B>> withBookingDef() {
            if (this.bookingDef!= null) {
                return this.bookingDef;
            }
            return this.bookingDef = new BookingDef.Builder<PlanElementDef.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "relationDef" (any previous value will be replaced)
         * 
         * @param relationDef
         *     New value of the "relationDef" property.
         */
        public PlanElementDef.Builder<_B> withRelationDef(final RelationDef relationDef) {
            this.relationDef = ((relationDef == null)?null:new RelationDef.Builder<PlanElementDef.Builder<_B>>(this, relationDef, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "relationDef" property.
         * Use {@link org.nuclos.schema.rest.RelationDef.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "relationDef" property.
         *     Use {@link org.nuclos.schema.rest.RelationDef.Builder#end()} to return to the current builder.
         */
        public RelationDef.Builder<? extends PlanElementDef.Builder<_B>> withRelationDef() {
            if (this.relationDef!= null) {
                return this.relationDef;
            }
            return this.relationDef = new RelationDef.Builder<PlanElementDef.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "elementType" (any previous value will be replaced)
         * 
         * @param elementType
         *     New value of the "elementType" property.
         */
        public PlanElementDef.Builder<_B> withElementType(final String elementType) {
            this.elementType = elementType;
            return this;
        }

        @Override
        public PlanElementDef build() {
            if (_storedValue == null) {
                return this.init(new PlanElementDef());
            } else {
                return ((PlanElementDef) _storedValue);
            }
        }

        public PlanElementDef.Builder<_B> copyOf(final PlanElementDef _other) {
            _other.copyTo(this);
            return this;
        }

        public PlanElementDef.Builder<_B> copyOf(final PlanElementDef.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PlanElementDef.Selector<PlanElementDef.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PlanElementDef.Select _root() {
            return new PlanElementDef.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private MilestoneDef.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>> milestoneDef = null;
        private BookingDef.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>> bookingDef = null;
        private RelationDef.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>> relationDef = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>> elementType = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.milestoneDef!= null) {
                products.put("milestoneDef", this.milestoneDef.init());
            }
            if (this.bookingDef!= null) {
                products.put("bookingDef", this.bookingDef.init());
            }
            if (this.relationDef!= null) {
                products.put("relationDef", this.relationDef.init());
            }
            if (this.elementType!= null) {
                products.put("elementType", this.elementType.init());
            }
            return products;
        }

        public MilestoneDef.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>> milestoneDef() {
            return ((this.milestoneDef == null)?this.milestoneDef = new MilestoneDef.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>>(this._root, this, "milestoneDef"):this.milestoneDef);
        }

        public BookingDef.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>> bookingDef() {
            return ((this.bookingDef == null)?this.bookingDef = new BookingDef.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>>(this._root, this, "bookingDef"):this.bookingDef);
        }

        public RelationDef.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>> relationDef() {
            return ((this.relationDef == null)?this.relationDef = new RelationDef.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>>(this._root, this, "relationDef"):this.relationDef);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>> elementType() {
            return ((this.elementType == null)?this.elementType = new com.kscs.util.jaxb.Selector<TRoot, PlanElementDef.Selector<TRoot, TParent>>(this._root, this, "elementType"):this.elementType);
        }

    }

}
