package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for plan-element-def-list complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="plan-element-def-list"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="planElementDefs" type="{urn:org.nuclos.schema.rest}plan-element-def" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "plan-element-def-list", propOrder = {
    "planElementDefs"
})
public class PlanElementDefList implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<PlanElementDef> planElementDefs;

    /**
     * Gets the value of the planElementDefs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the planElementDefs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlanElementDefs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlanElementDef }
     * 
     * 
     */
    public List<PlanElementDef> getPlanElementDefs() {
        if (planElementDefs == null) {
            planElementDefs = new ArrayList<PlanElementDef>();
        }
        return this.planElementDefs;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<PlanElementDef> thePlanElementDefs;
            thePlanElementDefs = (((this.planElementDefs!= null)&&(!this.planElementDefs.isEmpty()))?this.getPlanElementDefs():null);
            strategy.appendField(locator, this, "planElementDefs", buffer, thePlanElementDefs);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PlanElementDefList)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PlanElementDefList that = ((PlanElementDefList) object);
        {
            List<PlanElementDef> lhsPlanElementDefs;
            lhsPlanElementDefs = (((this.planElementDefs!= null)&&(!this.planElementDefs.isEmpty()))?this.getPlanElementDefs():null);
            List<PlanElementDef> rhsPlanElementDefs;
            rhsPlanElementDefs = (((that.planElementDefs!= null)&&(!that.planElementDefs.isEmpty()))?that.getPlanElementDefs():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "planElementDefs", lhsPlanElementDefs), LocatorUtils.property(thatLocator, "planElementDefs", rhsPlanElementDefs), lhsPlanElementDefs, rhsPlanElementDefs)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<PlanElementDef> thePlanElementDefs;
            thePlanElementDefs = (((this.planElementDefs!= null)&&(!this.planElementDefs.isEmpty()))?this.getPlanElementDefs():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "planElementDefs", thePlanElementDefs), currentHashCode, thePlanElementDefs);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PlanElementDefList) {
            final PlanElementDefList copy = ((PlanElementDefList) draftCopy);
            if ((this.planElementDefs!= null)&&(!this.planElementDefs.isEmpty())) {
                List<PlanElementDef> sourcePlanElementDefs;
                sourcePlanElementDefs = (((this.planElementDefs!= null)&&(!this.planElementDefs.isEmpty()))?this.getPlanElementDefs():null);
                @SuppressWarnings("unchecked")
                List<PlanElementDef> copyPlanElementDefs = ((List<PlanElementDef> ) strategy.copy(LocatorUtils.property(locator, "planElementDefs", sourcePlanElementDefs), sourcePlanElementDefs));
                copy.planElementDefs = null;
                if (copyPlanElementDefs!= null) {
                    List<PlanElementDef> uniquePlanElementDefsl = copy.getPlanElementDefs();
                    uniquePlanElementDefsl.addAll(copyPlanElementDefs);
                }
            } else {
                copy.planElementDefs = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PlanElementDefList();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PlanElementDefList.Builder<_B> _other) {
        if (this.planElementDefs == null) {
            _other.planElementDefs = null;
        } else {
            _other.planElementDefs = new ArrayList<PlanElementDef.Builder<PlanElementDefList.Builder<_B>>>();
            for (PlanElementDef _item: this.planElementDefs) {
                _other.planElementDefs.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
    }

    public<_B >PlanElementDefList.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PlanElementDefList.Builder<_B>(_parentBuilder, this, true);
    }

    public PlanElementDefList.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PlanElementDefList.Builder<Void> builder() {
        return new PlanElementDefList.Builder<Void>(null, null, false);
    }

    public static<_B >PlanElementDefList.Builder<_B> copyOf(final PlanElementDefList _other) {
        final PlanElementDefList.Builder<_B> _newBuilder = new PlanElementDefList.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PlanElementDefList.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree planElementDefsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("planElementDefs"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(planElementDefsPropertyTree!= null):((planElementDefsPropertyTree == null)||(!planElementDefsPropertyTree.isLeaf())))) {
            if (this.planElementDefs == null) {
                _other.planElementDefs = null;
            } else {
                _other.planElementDefs = new ArrayList<PlanElementDef.Builder<PlanElementDefList.Builder<_B>>>();
                for (PlanElementDef _item: this.planElementDefs) {
                    _other.planElementDefs.add(((_item == null)?null:_item.newCopyBuilder(_other, planElementDefsPropertyTree, _propertyTreeUse)));
                }
            }
        }
    }

    public<_B >PlanElementDefList.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PlanElementDefList.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PlanElementDefList.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PlanElementDefList.Builder<_B> copyOf(final PlanElementDefList _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PlanElementDefList.Builder<_B> _newBuilder = new PlanElementDefList.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PlanElementDefList.Builder<Void> copyExcept(final PlanElementDefList _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PlanElementDefList.Builder<Void> copyOnly(final PlanElementDefList _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PlanElementDefList _storedValue;
        private List<PlanElementDef.Builder<PlanElementDefList.Builder<_B>>> planElementDefs;

        public Builder(final _B _parentBuilder, final PlanElementDefList _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.planElementDefs == null) {
                        this.planElementDefs = null;
                    } else {
                        this.planElementDefs = new ArrayList<PlanElementDef.Builder<PlanElementDefList.Builder<_B>>>();
                        for (PlanElementDef _item: _other.planElementDefs) {
                            this.planElementDefs.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PlanElementDefList _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree planElementDefsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("planElementDefs"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(planElementDefsPropertyTree!= null):((planElementDefsPropertyTree == null)||(!planElementDefsPropertyTree.isLeaf())))) {
                        if (_other.planElementDefs == null) {
                            this.planElementDefs = null;
                        } else {
                            this.planElementDefs = new ArrayList<PlanElementDef.Builder<PlanElementDefList.Builder<_B>>>();
                            for (PlanElementDef _item: _other.planElementDefs) {
                                this.planElementDefs.add(((_item == null)?null:_item.newCopyBuilder(this, planElementDefsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PlanElementDefList >_P init(final _P _product) {
            if (this.planElementDefs!= null) {
                final List<PlanElementDef> planElementDefs = new ArrayList<PlanElementDef>(this.planElementDefs.size());
                for (PlanElementDef.Builder<PlanElementDefList.Builder<_B>> _item: this.planElementDefs) {
                    planElementDefs.add(_item.build());
                }
                _product.planElementDefs = planElementDefs;
            }
            return _product;
        }

        /**
         * Adds the given items to the value of "planElementDefs"
         * 
         * @param planElementDefs
         *     Items to add to the value of the "planElementDefs" property
         */
        public PlanElementDefList.Builder<_B> addPlanElementDefs(final Iterable<? extends PlanElementDef> planElementDefs) {
            if (planElementDefs!= null) {
                if (this.planElementDefs == null) {
                    this.planElementDefs = new ArrayList<PlanElementDef.Builder<PlanElementDefList.Builder<_B>>>();
                }
                for (PlanElementDef _item: planElementDefs) {
                    this.planElementDefs.add(new PlanElementDef.Builder<PlanElementDefList.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "planElementDefs" (any previous value will be replaced)
         * 
         * @param planElementDefs
         *     New value of the "planElementDefs" property.
         */
        public PlanElementDefList.Builder<_B> withPlanElementDefs(final Iterable<? extends PlanElementDef> planElementDefs) {
            if (this.planElementDefs!= null) {
                this.planElementDefs.clear();
            }
            return addPlanElementDefs(planElementDefs);
        }

        /**
         * Adds the given items to the value of "planElementDefs"
         * 
         * @param planElementDefs
         *     Items to add to the value of the "planElementDefs" property
         */
        public PlanElementDefList.Builder<_B> addPlanElementDefs(PlanElementDef... planElementDefs) {
            addPlanElementDefs(Arrays.asList(planElementDefs));
            return this;
        }

        /**
         * Sets the new value of "planElementDefs" (any previous value will be replaced)
         * 
         * @param planElementDefs
         *     New value of the "planElementDefs" property.
         */
        public PlanElementDefList.Builder<_B> withPlanElementDefs(PlanElementDef... planElementDefs) {
            withPlanElementDefs(Arrays.asList(planElementDefs));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "PlanElementDefs" property.
         * Use {@link org.nuclos.schema.rest.PlanElementDef.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "PlanElementDefs" property.
         *     Use {@link org.nuclos.schema.rest.PlanElementDef.Builder#end()} to return to the current builder.
         */
        public PlanElementDef.Builder<? extends PlanElementDefList.Builder<_B>> addPlanElementDefs() {
            if (this.planElementDefs == null) {
                this.planElementDefs = new ArrayList<PlanElementDef.Builder<PlanElementDefList.Builder<_B>>>();
            }
            final PlanElementDef.Builder<PlanElementDefList.Builder<_B>> planElementDefs_Builder = new PlanElementDef.Builder<PlanElementDefList.Builder<_B>>(this, null, false);
            this.planElementDefs.add(planElementDefs_Builder);
            return planElementDefs_Builder;
        }

        @Override
        public PlanElementDefList build() {
            if (_storedValue == null) {
                return this.init(new PlanElementDefList());
            } else {
                return ((PlanElementDefList) _storedValue);
            }
        }

        public PlanElementDefList.Builder<_B> copyOf(final PlanElementDefList _other) {
            _other.copyTo(this);
            return this;
        }

        public PlanElementDefList.Builder<_B> copyOf(final PlanElementDefList.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PlanElementDefList.Selector<PlanElementDefList.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PlanElementDefList.Select _root() {
            return new PlanElementDefList.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private PlanElementDef.Selector<TRoot, PlanElementDefList.Selector<TRoot, TParent>> planElementDefs = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.planElementDefs!= null) {
                products.put("planElementDefs", this.planElementDefs.init());
            }
            return products;
        }

        public PlanElementDef.Selector<TRoot, PlanElementDefList.Selector<TRoot, TParent>> planElementDefs() {
            return ((this.planElementDefs == null)?this.planElementDefs = new PlanElementDef.Selector<TRoot, PlanElementDefList.Selector<TRoot, TParent>>(this._root, this, "planElementDefs"):this.planElementDefs);
        }

    }

}
