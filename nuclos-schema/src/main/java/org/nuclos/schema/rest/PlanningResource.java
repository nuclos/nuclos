package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for planning-resource complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="planning-resource"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="entityMeta" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="resourceId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="tooltip" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "planning-resource")
public class PlanningResource implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "entityMeta", required = true)
    protected String entityMeta;
    @XmlAttribute(name = "resourceId", required = true)
    protected String resourceId;
    @XmlAttribute(name = "label", required = true)
    protected String label;
    @XmlAttribute(name = "tooltip", required = true)
    protected String tooltip;

    /**
     * Gets the value of the entityMeta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMeta() {
        return entityMeta;
    }

    /**
     * Sets the value of the entityMeta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMeta(String value) {
        this.entityMeta = value;
    }

    /**
     * Gets the value of the resourceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Sets the value of the resourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceId(String value) {
        this.resourceId = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the tooltip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTooltip() {
        return tooltip;
    }

    /**
     * Sets the value of the tooltip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTooltip(String value) {
        this.tooltip = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theEntityMeta;
            theEntityMeta = this.getEntityMeta();
            strategy.appendField(locator, this, "entityMeta", buffer, theEntityMeta);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            strategy.appendField(locator, this, "resourceId", buffer, theResourceId);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            String theTooltip;
            theTooltip = this.getTooltip();
            strategy.appendField(locator, this, "tooltip", buffer, theTooltip);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PlanningResource)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PlanningResource that = ((PlanningResource) object);
        {
            String lhsEntityMeta;
            lhsEntityMeta = this.getEntityMeta();
            String rhsEntityMeta;
            rhsEntityMeta = that.getEntityMeta();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMeta", lhsEntityMeta), LocatorUtils.property(thatLocator, "entityMeta", rhsEntityMeta), lhsEntityMeta, rhsEntityMeta)) {
                return false;
            }
        }
        {
            String lhsResourceId;
            lhsResourceId = this.getResourceId();
            String rhsResourceId;
            rhsResourceId = that.getResourceId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resourceId", lhsResourceId), LocatorUtils.property(thatLocator, "resourceId", rhsResourceId), lhsResourceId, rhsResourceId)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            String lhsTooltip;
            lhsTooltip = this.getTooltip();
            String rhsTooltip;
            rhsTooltip = that.getTooltip();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tooltip", lhsTooltip), LocatorUtils.property(thatLocator, "tooltip", rhsTooltip), lhsTooltip, rhsTooltip)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEntityMeta;
            theEntityMeta = this.getEntityMeta();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMeta", theEntityMeta), currentHashCode, theEntityMeta);
        }
        {
            String theResourceId;
            theResourceId = this.getResourceId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resourceId", theResourceId), currentHashCode, theResourceId);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            String theTooltip;
            theTooltip = this.getTooltip();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tooltip", theTooltip), currentHashCode, theTooltip);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PlanningResource) {
            final PlanningResource copy = ((PlanningResource) draftCopy);
            if (this.entityMeta!= null) {
                String sourceEntityMeta;
                sourceEntityMeta = this.getEntityMeta();
                String copyEntityMeta = ((String) strategy.copy(LocatorUtils.property(locator, "entityMeta", sourceEntityMeta), sourceEntityMeta));
                copy.setEntityMeta(copyEntityMeta);
            } else {
                copy.entityMeta = null;
            }
            if (this.resourceId!= null) {
                String sourceResourceId;
                sourceResourceId = this.getResourceId();
                String copyResourceId = ((String) strategy.copy(LocatorUtils.property(locator, "resourceId", sourceResourceId), sourceResourceId));
                copy.setResourceId(copyResourceId);
            } else {
                copy.resourceId = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.tooltip!= null) {
                String sourceTooltip;
                sourceTooltip = this.getTooltip();
                String copyTooltip = ((String) strategy.copy(LocatorUtils.property(locator, "tooltip", sourceTooltip), sourceTooltip));
                copy.setTooltip(copyTooltip);
            } else {
                copy.tooltip = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PlanningResource();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PlanningResource.Builder<_B> _other) {
        _other.entityMeta = this.entityMeta;
        _other.resourceId = this.resourceId;
        _other.label = this.label;
        _other.tooltip = this.tooltip;
    }

    public<_B >PlanningResource.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PlanningResource.Builder<_B>(_parentBuilder, this, true);
    }

    public PlanningResource.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PlanningResource.Builder<Void> builder() {
        return new PlanningResource.Builder<Void>(null, null, false);
    }

    public static<_B >PlanningResource.Builder<_B> copyOf(final PlanningResource _other) {
        final PlanningResource.Builder<_B> _newBuilder = new PlanningResource.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PlanningResource.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree entityMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMeta"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMetaPropertyTree!= null):((entityMetaPropertyTree == null)||(!entityMetaPropertyTree.isLeaf())))) {
            _other.entityMeta = this.entityMeta;
        }
        final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
            _other.resourceId = this.resourceId;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree tooltipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("tooltip"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tooltipPropertyTree!= null):((tooltipPropertyTree == null)||(!tooltipPropertyTree.isLeaf())))) {
            _other.tooltip = this.tooltip;
        }
    }

    public<_B >PlanningResource.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PlanningResource.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PlanningResource.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PlanningResource.Builder<_B> copyOf(final PlanningResource _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PlanningResource.Builder<_B> _newBuilder = new PlanningResource.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PlanningResource.Builder<Void> copyExcept(final PlanningResource _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PlanningResource.Builder<Void> copyOnly(final PlanningResource _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PlanningResource _storedValue;
        private String entityMeta;
        private String resourceId;
        private String label;
        private String tooltip;

        public Builder(final _B _parentBuilder, final PlanningResource _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.entityMeta = _other.entityMeta;
                    this.resourceId = _other.resourceId;
                    this.label = _other.label;
                    this.tooltip = _other.tooltip;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PlanningResource _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree entityMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMeta"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMetaPropertyTree!= null):((entityMetaPropertyTree == null)||(!entityMetaPropertyTree.isLeaf())))) {
                        this.entityMeta = _other.entityMeta;
                    }
                    final PropertyTree resourceIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resourceId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourceIdPropertyTree!= null):((resourceIdPropertyTree == null)||(!resourceIdPropertyTree.isLeaf())))) {
                        this.resourceId = _other.resourceId;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree tooltipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("tooltip"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(tooltipPropertyTree!= null):((tooltipPropertyTree == null)||(!tooltipPropertyTree.isLeaf())))) {
                        this.tooltip = _other.tooltip;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PlanningResource >_P init(final _P _product) {
            _product.entityMeta = this.entityMeta;
            _product.resourceId = this.resourceId;
            _product.label = this.label;
            _product.tooltip = this.tooltip;
            return _product;
        }

        /**
         * Sets the new value of "entityMeta" (any previous value will be replaced)
         * 
         * @param entityMeta
         *     New value of the "entityMeta" property.
         */
        public PlanningResource.Builder<_B> withEntityMeta(final String entityMeta) {
            this.entityMeta = entityMeta;
            return this;
        }

        /**
         * Sets the new value of "resourceId" (any previous value will be replaced)
         * 
         * @param resourceId
         *     New value of the "resourceId" property.
         */
        public PlanningResource.Builder<_B> withResourceId(final String resourceId) {
            this.resourceId = resourceId;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public PlanningResource.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "tooltip" (any previous value will be replaced)
         * 
         * @param tooltip
         *     New value of the "tooltip" property.
         */
        public PlanningResource.Builder<_B> withTooltip(final String tooltip) {
            this.tooltip = tooltip;
            return this;
        }

        @Override
        public PlanningResource build() {
            if (_storedValue == null) {
                return this.init(new PlanningResource());
            } else {
                return ((PlanningResource) _storedValue);
            }
        }

        public PlanningResource.Builder<_B> copyOf(final PlanningResource _other) {
            _other.copyTo(this);
            return this;
        }

        public PlanningResource.Builder<_B> copyOf(final PlanningResource.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PlanningResource.Selector<PlanningResource.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PlanningResource.Select _root() {
            return new PlanningResource.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>> entityMeta = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>> resourceId = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>> tooltip = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.entityMeta!= null) {
                products.put("entityMeta", this.entityMeta.init());
            }
            if (this.resourceId!= null) {
                products.put("resourceId", this.resourceId.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.tooltip!= null) {
                products.put("tooltip", this.tooltip.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>> entityMeta() {
            return ((this.entityMeta == null)?this.entityMeta = new com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>>(this._root, this, "entityMeta"):this.entityMeta);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>> resourceId() {
            return ((this.resourceId == null)?this.resourceId = new com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>>(this._root, this, "resourceId"):this.resourceId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>> tooltip() {
            return ((this.tooltip == null)?this.tooltip = new com.kscs.util.jaxb.Selector<TRoot, PlanningResource.Selector<TRoot, TParent>>(this._root, this, "tooltip"):this.tooltip);
        }

    }

}
