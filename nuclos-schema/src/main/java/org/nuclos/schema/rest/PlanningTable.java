package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for planning-table complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="planning-table"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="resources" type="{urn:org.nuclos.schema.rest}planning-resource" maxOccurs="unbounded"/&gt;
 *         &lt;element name="bookings" type="{urn:org.nuclos.schema.rest}booking" maxOccurs="unbounded"/&gt;
 *         &lt;element name="milestones" type="{urn:org.nuclos.schema.rest}milestone" maxOccurs="unbounded"/&gt;
 *         &lt;element name="relations" type="{urn:org.nuclos.schema.rest}relation" maxOccurs="unbounded"/&gt;
 *         &lt;element name="backgroundEvents" type="{urn:org.nuclos.schema.rest}background-event" maxOccurs="unbounded"/&gt;
 *         &lt;element name="searchFilters" type="{urn:org.nuclos.schema.rest}searchfilter-info" maxOccurs="unbounded"/&gt;
 *         &lt;element name="businessHours" type="{urn:org.nuclos.schema.rest}business-hour" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="showBackgroundEvents" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="componentLabel" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="componentMenupath" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="dateFrom" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="dateUntil" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="legendLabel" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="legendTooltip" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="dateChooserPolicy" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="usePeriodicRefresh" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="periodicRefreshInterval" use="required" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "planning-table", propOrder = {
    "resources",
    "bookings",
    "milestones",
    "relations",
    "backgroundEvents",
    "searchFilters",
    "businessHours"
})
public class PlanningTable implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<PlanningResource> resources;
    @XmlElement(required = true)
    protected List<Booking> bookings;
    @XmlElement(required = true)
    protected List<Milestone> milestones;
    @XmlElement(required = true)
    protected List<Relation> relations;
    @XmlElement(required = true)
    protected List<BackgroundEvent> backgroundEvents;
    @XmlElement(required = true)
    protected List<SearchfilterInfo> searchFilters;
    @XmlElement(required = true)
    protected List<BusinessHour> businessHours;
    @XmlAttribute(name = "showBackgroundEvents", required = true)
    protected String showBackgroundEvents;
    @XmlAttribute(name = "componentLabel", required = true)
    protected String componentLabel;
    @XmlAttribute(name = "componentMenupath", required = true)
    protected String componentMenupath;
    @XmlAttribute(name = "dateFrom", required = true)
    protected String dateFrom;
    @XmlAttribute(name = "dateUntil", required = true)
    protected String dateUntil;
    @XmlAttribute(name = "legendLabel", required = true)
    protected String legendLabel;
    @XmlAttribute(name = "legendTooltip", required = true)
    protected String legendTooltip;
    @XmlAttribute(name = "dateChooserPolicy", required = true)
    protected String dateChooserPolicy;
    @XmlAttribute(name = "usePeriodicRefresh", required = true)
    protected boolean usePeriodicRefresh;
    @XmlAttribute(name = "periodicRefreshInterval", required = true)
    protected long periodicRefreshInterval;

    /**
     * Gets the value of the resources property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resources property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResources().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlanningResource }
     * 
     * 
     */
    public List<PlanningResource> getResources() {
        if (resources == null) {
            resources = new ArrayList<PlanningResource>();
        }
        return this.resources;
    }

    /**
     * Gets the value of the bookings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Booking }
     * 
     * 
     */
    public List<Booking> getBookings() {
        if (bookings == null) {
            bookings = new ArrayList<Booking>();
        }
        return this.bookings;
    }

    /**
     * Gets the value of the milestones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the milestones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMilestones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Milestone }
     * 
     * 
     */
    public List<Milestone> getMilestones() {
        if (milestones == null) {
            milestones = new ArrayList<Milestone>();
        }
        return this.milestones;
    }

    /**
     * Gets the value of the relations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Relation }
     * 
     * 
     */
    public List<Relation> getRelations() {
        if (relations == null) {
            relations = new ArrayList<Relation>();
        }
        return this.relations;
    }

    /**
     * Gets the value of the backgroundEvents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the backgroundEvents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBackgroundEvents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BackgroundEvent }
     * 
     * 
     */
    public List<BackgroundEvent> getBackgroundEvents() {
        if (backgroundEvents == null) {
            backgroundEvents = new ArrayList<BackgroundEvent>();
        }
        return this.backgroundEvents;
    }

    /**
     * Gets the value of the searchFilters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the searchFilters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchFilters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchfilterInfo }
     * 
     * 
     */
    public List<SearchfilterInfo> getSearchFilters() {
        if (searchFilters == null) {
            searchFilters = new ArrayList<SearchfilterInfo>();
        }
        return this.searchFilters;
    }

    /**
     * Gets the value of the businessHours property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the businessHours property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBusinessHours().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessHour }
     * 
     * 
     */
    public List<BusinessHour> getBusinessHours() {
        if (businessHours == null) {
            businessHours = new ArrayList<BusinessHour>();
        }
        return this.businessHours;
    }

    /**
     * Gets the value of the showBackgroundEvents property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShowBackgroundEvents() {
        return showBackgroundEvents;
    }

    /**
     * Sets the value of the showBackgroundEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShowBackgroundEvents(String value) {
        this.showBackgroundEvents = value;
    }

    /**
     * Gets the value of the componentLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComponentLabel() {
        return componentLabel;
    }

    /**
     * Sets the value of the componentLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComponentLabel(String value) {
        this.componentLabel = value;
    }

    /**
     * Gets the value of the componentMenupath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComponentMenupath() {
        return componentMenupath;
    }

    /**
     * Sets the value of the componentMenupath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComponentMenupath(String value) {
        this.componentMenupath = value;
    }

    /**
     * Gets the value of the dateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateFrom() {
        return dateFrom;
    }

    /**
     * Sets the value of the dateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateFrom(String value) {
        this.dateFrom = value;
    }

    /**
     * Gets the value of the dateUntil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateUntil() {
        return dateUntil;
    }

    /**
     * Sets the value of the dateUntil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateUntil(String value) {
        this.dateUntil = value;
    }

    /**
     * Gets the value of the legendLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegendLabel() {
        return legendLabel;
    }

    /**
     * Sets the value of the legendLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegendLabel(String value) {
        this.legendLabel = value;
    }

    /**
     * Gets the value of the legendTooltip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegendTooltip() {
        return legendTooltip;
    }

    /**
     * Sets the value of the legendTooltip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegendTooltip(String value) {
        this.legendTooltip = value;
    }

    /**
     * Gets the value of the dateChooserPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateChooserPolicy() {
        return dateChooserPolicy;
    }

    /**
     * Sets the value of the dateChooserPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateChooserPolicy(String value) {
        this.dateChooserPolicy = value;
    }

    /**
     * Gets the value of the usePeriodicRefresh property.
     * 
     */
    public boolean isUsePeriodicRefresh() {
        return usePeriodicRefresh;
    }

    /**
     * Sets the value of the usePeriodicRefresh property.
     * 
     */
    public void setUsePeriodicRefresh(boolean value) {
        this.usePeriodicRefresh = value;
    }

    /**
     * Gets the value of the periodicRefreshInterval property.
     * 
     */
    public long getPeriodicRefreshInterval() {
        return periodicRefreshInterval;
    }

    /**
     * Sets the value of the periodicRefreshInterval property.
     * 
     */
    public void setPeriodicRefreshInterval(long value) {
        this.periodicRefreshInterval = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<PlanningResource> theResources;
            theResources = (((this.resources!= null)&&(!this.resources.isEmpty()))?this.getResources():null);
            strategy.appendField(locator, this, "resources", buffer, theResources);
        }
        {
            List<Booking> theBookings;
            theBookings = (((this.bookings!= null)&&(!this.bookings.isEmpty()))?this.getBookings():null);
            strategy.appendField(locator, this, "bookings", buffer, theBookings);
        }
        {
            List<Milestone> theMilestones;
            theMilestones = (((this.milestones!= null)&&(!this.milestones.isEmpty()))?this.getMilestones():null);
            strategy.appendField(locator, this, "milestones", buffer, theMilestones);
        }
        {
            List<Relation> theRelations;
            theRelations = (((this.relations!= null)&&(!this.relations.isEmpty()))?this.getRelations():null);
            strategy.appendField(locator, this, "relations", buffer, theRelations);
        }
        {
            List<BackgroundEvent> theBackgroundEvents;
            theBackgroundEvents = (((this.backgroundEvents!= null)&&(!this.backgroundEvents.isEmpty()))?this.getBackgroundEvents():null);
            strategy.appendField(locator, this, "backgroundEvents", buffer, theBackgroundEvents);
        }
        {
            List<SearchfilterInfo> theSearchFilters;
            theSearchFilters = (((this.searchFilters!= null)&&(!this.searchFilters.isEmpty()))?this.getSearchFilters():null);
            strategy.appendField(locator, this, "searchFilters", buffer, theSearchFilters);
        }
        {
            List<BusinessHour> theBusinessHours;
            theBusinessHours = (((this.businessHours!= null)&&(!this.businessHours.isEmpty()))?this.getBusinessHours():null);
            strategy.appendField(locator, this, "businessHours", buffer, theBusinessHours);
        }
        {
            String theShowBackgroundEvents;
            theShowBackgroundEvents = this.getShowBackgroundEvents();
            strategy.appendField(locator, this, "showBackgroundEvents", buffer, theShowBackgroundEvents);
        }
        {
            String theComponentLabel;
            theComponentLabel = this.getComponentLabel();
            strategy.appendField(locator, this, "componentLabel", buffer, theComponentLabel);
        }
        {
            String theComponentMenupath;
            theComponentMenupath = this.getComponentMenupath();
            strategy.appendField(locator, this, "componentMenupath", buffer, theComponentMenupath);
        }
        {
            String theDateFrom;
            theDateFrom = this.getDateFrom();
            strategy.appendField(locator, this, "dateFrom", buffer, theDateFrom);
        }
        {
            String theDateUntil;
            theDateUntil = this.getDateUntil();
            strategy.appendField(locator, this, "dateUntil", buffer, theDateUntil);
        }
        {
            String theLegendLabel;
            theLegendLabel = this.getLegendLabel();
            strategy.appendField(locator, this, "legendLabel", buffer, theLegendLabel);
        }
        {
            String theLegendTooltip;
            theLegendTooltip = this.getLegendTooltip();
            strategy.appendField(locator, this, "legendTooltip", buffer, theLegendTooltip);
        }
        {
            String theDateChooserPolicy;
            theDateChooserPolicy = this.getDateChooserPolicy();
            strategy.appendField(locator, this, "dateChooserPolicy", buffer, theDateChooserPolicy);
        }
        {
            boolean theUsePeriodicRefresh;
            theUsePeriodicRefresh = this.isUsePeriodicRefresh();
            strategy.appendField(locator, this, "usePeriodicRefresh", buffer, theUsePeriodicRefresh);
        }
        {
            long thePeriodicRefreshInterval;
            thePeriodicRefreshInterval = this.getPeriodicRefreshInterval();
            strategy.appendField(locator, this, "periodicRefreshInterval", buffer, thePeriodicRefreshInterval);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PlanningTable)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PlanningTable that = ((PlanningTable) object);
        {
            List<PlanningResource> lhsResources;
            lhsResources = (((this.resources!= null)&&(!this.resources.isEmpty()))?this.getResources():null);
            List<PlanningResource> rhsResources;
            rhsResources = (((that.resources!= null)&&(!that.resources.isEmpty()))?that.getResources():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "resources", lhsResources), LocatorUtils.property(thatLocator, "resources", rhsResources), lhsResources, rhsResources)) {
                return false;
            }
        }
        {
            List<Booking> lhsBookings;
            lhsBookings = (((this.bookings!= null)&&(!this.bookings.isEmpty()))?this.getBookings():null);
            List<Booking> rhsBookings;
            rhsBookings = (((that.bookings!= null)&&(!that.bookings.isEmpty()))?that.getBookings():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "bookings", lhsBookings), LocatorUtils.property(thatLocator, "bookings", rhsBookings), lhsBookings, rhsBookings)) {
                return false;
            }
        }
        {
            List<Milestone> lhsMilestones;
            lhsMilestones = (((this.milestones!= null)&&(!this.milestones.isEmpty()))?this.getMilestones():null);
            List<Milestone> rhsMilestones;
            rhsMilestones = (((that.milestones!= null)&&(!that.milestones.isEmpty()))?that.getMilestones():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "milestones", lhsMilestones), LocatorUtils.property(thatLocator, "milestones", rhsMilestones), lhsMilestones, rhsMilestones)) {
                return false;
            }
        }
        {
            List<Relation> lhsRelations;
            lhsRelations = (((this.relations!= null)&&(!this.relations.isEmpty()))?this.getRelations():null);
            List<Relation> rhsRelations;
            rhsRelations = (((that.relations!= null)&&(!that.relations.isEmpty()))?that.getRelations():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "relations", lhsRelations), LocatorUtils.property(thatLocator, "relations", rhsRelations), lhsRelations, rhsRelations)) {
                return false;
            }
        }
        {
            List<BackgroundEvent> lhsBackgroundEvents;
            lhsBackgroundEvents = (((this.backgroundEvents!= null)&&(!this.backgroundEvents.isEmpty()))?this.getBackgroundEvents():null);
            List<BackgroundEvent> rhsBackgroundEvents;
            rhsBackgroundEvents = (((that.backgroundEvents!= null)&&(!that.backgroundEvents.isEmpty()))?that.getBackgroundEvents():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "backgroundEvents", lhsBackgroundEvents), LocatorUtils.property(thatLocator, "backgroundEvents", rhsBackgroundEvents), lhsBackgroundEvents, rhsBackgroundEvents)) {
                return false;
            }
        }
        {
            List<SearchfilterInfo> lhsSearchFilters;
            lhsSearchFilters = (((this.searchFilters!= null)&&(!this.searchFilters.isEmpty()))?this.getSearchFilters():null);
            List<SearchfilterInfo> rhsSearchFilters;
            rhsSearchFilters = (((that.searchFilters!= null)&&(!that.searchFilters.isEmpty()))?that.getSearchFilters():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "searchFilters", lhsSearchFilters), LocatorUtils.property(thatLocator, "searchFilters", rhsSearchFilters), lhsSearchFilters, rhsSearchFilters)) {
                return false;
            }
        }
        {
            List<BusinessHour> lhsBusinessHours;
            lhsBusinessHours = (((this.businessHours!= null)&&(!this.businessHours.isEmpty()))?this.getBusinessHours():null);
            List<BusinessHour> rhsBusinessHours;
            rhsBusinessHours = (((that.businessHours!= null)&&(!that.businessHours.isEmpty()))?that.getBusinessHours():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "businessHours", lhsBusinessHours), LocatorUtils.property(thatLocator, "businessHours", rhsBusinessHours), lhsBusinessHours, rhsBusinessHours)) {
                return false;
            }
        }
        {
            String lhsShowBackgroundEvents;
            lhsShowBackgroundEvents = this.getShowBackgroundEvents();
            String rhsShowBackgroundEvents;
            rhsShowBackgroundEvents = that.getShowBackgroundEvents();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "showBackgroundEvents", lhsShowBackgroundEvents), LocatorUtils.property(thatLocator, "showBackgroundEvents", rhsShowBackgroundEvents), lhsShowBackgroundEvents, rhsShowBackgroundEvents)) {
                return false;
            }
        }
        {
            String lhsComponentLabel;
            lhsComponentLabel = this.getComponentLabel();
            String rhsComponentLabel;
            rhsComponentLabel = that.getComponentLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "componentLabel", lhsComponentLabel), LocatorUtils.property(thatLocator, "componentLabel", rhsComponentLabel), lhsComponentLabel, rhsComponentLabel)) {
                return false;
            }
        }
        {
            String lhsComponentMenupath;
            lhsComponentMenupath = this.getComponentMenupath();
            String rhsComponentMenupath;
            rhsComponentMenupath = that.getComponentMenupath();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "componentMenupath", lhsComponentMenupath), LocatorUtils.property(thatLocator, "componentMenupath", rhsComponentMenupath), lhsComponentMenupath, rhsComponentMenupath)) {
                return false;
            }
        }
        {
            String lhsDateFrom;
            lhsDateFrom = this.getDateFrom();
            String rhsDateFrom;
            rhsDateFrom = that.getDateFrom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateFrom", lhsDateFrom), LocatorUtils.property(thatLocator, "dateFrom", rhsDateFrom), lhsDateFrom, rhsDateFrom)) {
                return false;
            }
        }
        {
            String lhsDateUntil;
            lhsDateUntil = this.getDateUntil();
            String rhsDateUntil;
            rhsDateUntil = that.getDateUntil();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateUntil", lhsDateUntil), LocatorUtils.property(thatLocator, "dateUntil", rhsDateUntil), lhsDateUntil, rhsDateUntil)) {
                return false;
            }
        }
        {
            String lhsLegendLabel;
            lhsLegendLabel = this.getLegendLabel();
            String rhsLegendLabel;
            rhsLegendLabel = that.getLegendLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "legendLabel", lhsLegendLabel), LocatorUtils.property(thatLocator, "legendLabel", rhsLegendLabel), lhsLegendLabel, rhsLegendLabel)) {
                return false;
            }
        }
        {
            String lhsLegendTooltip;
            lhsLegendTooltip = this.getLegendTooltip();
            String rhsLegendTooltip;
            rhsLegendTooltip = that.getLegendTooltip();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "legendTooltip", lhsLegendTooltip), LocatorUtils.property(thatLocator, "legendTooltip", rhsLegendTooltip), lhsLegendTooltip, rhsLegendTooltip)) {
                return false;
            }
        }
        {
            String lhsDateChooserPolicy;
            lhsDateChooserPolicy = this.getDateChooserPolicy();
            String rhsDateChooserPolicy;
            rhsDateChooserPolicy = that.getDateChooserPolicy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dateChooserPolicy", lhsDateChooserPolicy), LocatorUtils.property(thatLocator, "dateChooserPolicy", rhsDateChooserPolicy), lhsDateChooserPolicy, rhsDateChooserPolicy)) {
                return false;
            }
        }
        {
            boolean lhsUsePeriodicRefresh;
            lhsUsePeriodicRefresh = this.isUsePeriodicRefresh();
            boolean rhsUsePeriodicRefresh;
            rhsUsePeriodicRefresh = that.isUsePeriodicRefresh();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "usePeriodicRefresh", lhsUsePeriodicRefresh), LocatorUtils.property(thatLocator, "usePeriodicRefresh", rhsUsePeriodicRefresh), lhsUsePeriodicRefresh, rhsUsePeriodicRefresh)) {
                return false;
            }
        }
        {
            long lhsPeriodicRefreshInterval;
            lhsPeriodicRefreshInterval = this.getPeriodicRefreshInterval();
            long rhsPeriodicRefreshInterval;
            rhsPeriodicRefreshInterval = that.getPeriodicRefreshInterval();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "periodicRefreshInterval", lhsPeriodicRefreshInterval), LocatorUtils.property(thatLocator, "periodicRefreshInterval", rhsPeriodicRefreshInterval), lhsPeriodicRefreshInterval, rhsPeriodicRefreshInterval)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<PlanningResource> theResources;
            theResources = (((this.resources!= null)&&(!this.resources.isEmpty()))?this.getResources():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "resources", theResources), currentHashCode, theResources);
        }
        {
            List<Booking> theBookings;
            theBookings = (((this.bookings!= null)&&(!this.bookings.isEmpty()))?this.getBookings():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "bookings", theBookings), currentHashCode, theBookings);
        }
        {
            List<Milestone> theMilestones;
            theMilestones = (((this.milestones!= null)&&(!this.milestones.isEmpty()))?this.getMilestones():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "milestones", theMilestones), currentHashCode, theMilestones);
        }
        {
            List<Relation> theRelations;
            theRelations = (((this.relations!= null)&&(!this.relations.isEmpty()))?this.getRelations():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "relations", theRelations), currentHashCode, theRelations);
        }
        {
            List<BackgroundEvent> theBackgroundEvents;
            theBackgroundEvents = (((this.backgroundEvents!= null)&&(!this.backgroundEvents.isEmpty()))?this.getBackgroundEvents():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "backgroundEvents", theBackgroundEvents), currentHashCode, theBackgroundEvents);
        }
        {
            List<SearchfilterInfo> theSearchFilters;
            theSearchFilters = (((this.searchFilters!= null)&&(!this.searchFilters.isEmpty()))?this.getSearchFilters():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "searchFilters", theSearchFilters), currentHashCode, theSearchFilters);
        }
        {
            List<BusinessHour> theBusinessHours;
            theBusinessHours = (((this.businessHours!= null)&&(!this.businessHours.isEmpty()))?this.getBusinessHours():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "businessHours", theBusinessHours), currentHashCode, theBusinessHours);
        }
        {
            String theShowBackgroundEvents;
            theShowBackgroundEvents = this.getShowBackgroundEvents();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "showBackgroundEvents", theShowBackgroundEvents), currentHashCode, theShowBackgroundEvents);
        }
        {
            String theComponentLabel;
            theComponentLabel = this.getComponentLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "componentLabel", theComponentLabel), currentHashCode, theComponentLabel);
        }
        {
            String theComponentMenupath;
            theComponentMenupath = this.getComponentMenupath();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "componentMenupath", theComponentMenupath), currentHashCode, theComponentMenupath);
        }
        {
            String theDateFrom;
            theDateFrom = this.getDateFrom();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dateFrom", theDateFrom), currentHashCode, theDateFrom);
        }
        {
            String theDateUntil;
            theDateUntil = this.getDateUntil();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dateUntil", theDateUntil), currentHashCode, theDateUntil);
        }
        {
            String theLegendLabel;
            theLegendLabel = this.getLegendLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "legendLabel", theLegendLabel), currentHashCode, theLegendLabel);
        }
        {
            String theLegendTooltip;
            theLegendTooltip = this.getLegendTooltip();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "legendTooltip", theLegendTooltip), currentHashCode, theLegendTooltip);
        }
        {
            String theDateChooserPolicy;
            theDateChooserPolicy = this.getDateChooserPolicy();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dateChooserPolicy", theDateChooserPolicy), currentHashCode, theDateChooserPolicy);
        }
        {
            boolean theUsePeriodicRefresh;
            theUsePeriodicRefresh = this.isUsePeriodicRefresh();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "usePeriodicRefresh", theUsePeriodicRefresh), currentHashCode, theUsePeriodicRefresh);
        }
        {
            long thePeriodicRefreshInterval;
            thePeriodicRefreshInterval = this.getPeriodicRefreshInterval();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "periodicRefreshInterval", thePeriodicRefreshInterval), currentHashCode, thePeriodicRefreshInterval);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PlanningTable) {
            final PlanningTable copy = ((PlanningTable) draftCopy);
            if ((this.resources!= null)&&(!this.resources.isEmpty())) {
                List<PlanningResource> sourceResources;
                sourceResources = (((this.resources!= null)&&(!this.resources.isEmpty()))?this.getResources():null);
                @SuppressWarnings("unchecked")
                List<PlanningResource> copyResources = ((List<PlanningResource> ) strategy.copy(LocatorUtils.property(locator, "resources", sourceResources), sourceResources));
                copy.resources = null;
                if (copyResources!= null) {
                    List<PlanningResource> uniqueResourcesl = copy.getResources();
                    uniqueResourcesl.addAll(copyResources);
                }
            } else {
                copy.resources = null;
            }
            if ((this.bookings!= null)&&(!this.bookings.isEmpty())) {
                List<Booking> sourceBookings;
                sourceBookings = (((this.bookings!= null)&&(!this.bookings.isEmpty()))?this.getBookings():null);
                @SuppressWarnings("unchecked")
                List<Booking> copyBookings = ((List<Booking> ) strategy.copy(LocatorUtils.property(locator, "bookings", sourceBookings), sourceBookings));
                copy.bookings = null;
                if (copyBookings!= null) {
                    List<Booking> uniqueBookingsl = copy.getBookings();
                    uniqueBookingsl.addAll(copyBookings);
                }
            } else {
                copy.bookings = null;
            }
            if ((this.milestones!= null)&&(!this.milestones.isEmpty())) {
                List<Milestone> sourceMilestones;
                sourceMilestones = (((this.milestones!= null)&&(!this.milestones.isEmpty()))?this.getMilestones():null);
                @SuppressWarnings("unchecked")
                List<Milestone> copyMilestones = ((List<Milestone> ) strategy.copy(LocatorUtils.property(locator, "milestones", sourceMilestones), sourceMilestones));
                copy.milestones = null;
                if (copyMilestones!= null) {
                    List<Milestone> uniqueMilestonesl = copy.getMilestones();
                    uniqueMilestonesl.addAll(copyMilestones);
                }
            } else {
                copy.milestones = null;
            }
            if ((this.relations!= null)&&(!this.relations.isEmpty())) {
                List<Relation> sourceRelations;
                sourceRelations = (((this.relations!= null)&&(!this.relations.isEmpty()))?this.getRelations():null);
                @SuppressWarnings("unchecked")
                List<Relation> copyRelations = ((List<Relation> ) strategy.copy(LocatorUtils.property(locator, "relations", sourceRelations), sourceRelations));
                copy.relations = null;
                if (copyRelations!= null) {
                    List<Relation> uniqueRelationsl = copy.getRelations();
                    uniqueRelationsl.addAll(copyRelations);
                }
            } else {
                copy.relations = null;
            }
            if ((this.backgroundEvents!= null)&&(!this.backgroundEvents.isEmpty())) {
                List<BackgroundEvent> sourceBackgroundEvents;
                sourceBackgroundEvents = (((this.backgroundEvents!= null)&&(!this.backgroundEvents.isEmpty()))?this.getBackgroundEvents():null);
                @SuppressWarnings("unchecked")
                List<BackgroundEvent> copyBackgroundEvents = ((List<BackgroundEvent> ) strategy.copy(LocatorUtils.property(locator, "backgroundEvents", sourceBackgroundEvents), sourceBackgroundEvents));
                copy.backgroundEvents = null;
                if (copyBackgroundEvents!= null) {
                    List<BackgroundEvent> uniqueBackgroundEventsl = copy.getBackgroundEvents();
                    uniqueBackgroundEventsl.addAll(copyBackgroundEvents);
                }
            } else {
                copy.backgroundEvents = null;
            }
            if ((this.searchFilters!= null)&&(!this.searchFilters.isEmpty())) {
                List<SearchfilterInfo> sourceSearchFilters;
                sourceSearchFilters = (((this.searchFilters!= null)&&(!this.searchFilters.isEmpty()))?this.getSearchFilters():null);
                @SuppressWarnings("unchecked")
                List<SearchfilterInfo> copySearchFilters = ((List<SearchfilterInfo> ) strategy.copy(LocatorUtils.property(locator, "searchFilters", sourceSearchFilters), sourceSearchFilters));
                copy.searchFilters = null;
                if (copySearchFilters!= null) {
                    List<SearchfilterInfo> uniqueSearchFiltersl = copy.getSearchFilters();
                    uniqueSearchFiltersl.addAll(copySearchFilters);
                }
            } else {
                copy.searchFilters = null;
            }
            if ((this.businessHours!= null)&&(!this.businessHours.isEmpty())) {
                List<BusinessHour> sourceBusinessHours;
                sourceBusinessHours = (((this.businessHours!= null)&&(!this.businessHours.isEmpty()))?this.getBusinessHours():null);
                @SuppressWarnings("unchecked")
                List<BusinessHour> copyBusinessHours = ((List<BusinessHour> ) strategy.copy(LocatorUtils.property(locator, "businessHours", sourceBusinessHours), sourceBusinessHours));
                copy.businessHours = null;
                if (copyBusinessHours!= null) {
                    List<BusinessHour> uniqueBusinessHoursl = copy.getBusinessHours();
                    uniqueBusinessHoursl.addAll(copyBusinessHours);
                }
            } else {
                copy.businessHours = null;
            }
            if (this.showBackgroundEvents!= null) {
                String sourceShowBackgroundEvents;
                sourceShowBackgroundEvents = this.getShowBackgroundEvents();
                String copyShowBackgroundEvents = ((String) strategy.copy(LocatorUtils.property(locator, "showBackgroundEvents", sourceShowBackgroundEvents), sourceShowBackgroundEvents));
                copy.setShowBackgroundEvents(copyShowBackgroundEvents);
            } else {
                copy.showBackgroundEvents = null;
            }
            if (this.componentLabel!= null) {
                String sourceComponentLabel;
                sourceComponentLabel = this.getComponentLabel();
                String copyComponentLabel = ((String) strategy.copy(LocatorUtils.property(locator, "componentLabel", sourceComponentLabel), sourceComponentLabel));
                copy.setComponentLabel(copyComponentLabel);
            } else {
                copy.componentLabel = null;
            }
            if (this.componentMenupath!= null) {
                String sourceComponentMenupath;
                sourceComponentMenupath = this.getComponentMenupath();
                String copyComponentMenupath = ((String) strategy.copy(LocatorUtils.property(locator, "componentMenupath", sourceComponentMenupath), sourceComponentMenupath));
                copy.setComponentMenupath(copyComponentMenupath);
            } else {
                copy.componentMenupath = null;
            }
            if (this.dateFrom!= null) {
                String sourceDateFrom;
                sourceDateFrom = this.getDateFrom();
                String copyDateFrom = ((String) strategy.copy(LocatorUtils.property(locator, "dateFrom", sourceDateFrom), sourceDateFrom));
                copy.setDateFrom(copyDateFrom);
            } else {
                copy.dateFrom = null;
            }
            if (this.dateUntil!= null) {
                String sourceDateUntil;
                sourceDateUntil = this.getDateUntil();
                String copyDateUntil = ((String) strategy.copy(LocatorUtils.property(locator, "dateUntil", sourceDateUntil), sourceDateUntil));
                copy.setDateUntil(copyDateUntil);
            } else {
                copy.dateUntil = null;
            }
            if (this.legendLabel!= null) {
                String sourceLegendLabel;
                sourceLegendLabel = this.getLegendLabel();
                String copyLegendLabel = ((String) strategy.copy(LocatorUtils.property(locator, "legendLabel", sourceLegendLabel), sourceLegendLabel));
                copy.setLegendLabel(copyLegendLabel);
            } else {
                copy.legendLabel = null;
            }
            if (this.legendTooltip!= null) {
                String sourceLegendTooltip;
                sourceLegendTooltip = this.getLegendTooltip();
                String copyLegendTooltip = ((String) strategy.copy(LocatorUtils.property(locator, "legendTooltip", sourceLegendTooltip), sourceLegendTooltip));
                copy.setLegendTooltip(copyLegendTooltip);
            } else {
                copy.legendTooltip = null;
            }
            if (this.dateChooserPolicy!= null) {
                String sourceDateChooserPolicy;
                sourceDateChooserPolicy = this.getDateChooserPolicy();
                String copyDateChooserPolicy = ((String) strategy.copy(LocatorUtils.property(locator, "dateChooserPolicy", sourceDateChooserPolicy), sourceDateChooserPolicy));
                copy.setDateChooserPolicy(copyDateChooserPolicy);
            } else {
                copy.dateChooserPolicy = null;
            }
            {
                boolean sourceUsePeriodicRefresh;
                sourceUsePeriodicRefresh = this.isUsePeriodicRefresh();
                boolean copyUsePeriodicRefresh = strategy.copy(LocatorUtils.property(locator, "usePeriodicRefresh", sourceUsePeriodicRefresh), sourceUsePeriodicRefresh);
                copy.setUsePeriodicRefresh(copyUsePeriodicRefresh);
            }
            {
                long sourcePeriodicRefreshInterval;
                sourcePeriodicRefreshInterval = this.getPeriodicRefreshInterval();
                long copyPeriodicRefreshInterval = strategy.copy(LocatorUtils.property(locator, "periodicRefreshInterval", sourcePeriodicRefreshInterval), sourcePeriodicRefreshInterval);
                copy.setPeriodicRefreshInterval(copyPeriodicRefreshInterval);
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PlanningTable();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PlanningTable.Builder<_B> _other) {
        if (this.resources == null) {
            _other.resources = null;
        } else {
            _other.resources = new ArrayList<PlanningResource.Builder<PlanningTable.Builder<_B>>>();
            for (PlanningResource _item: this.resources) {
                _other.resources.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.bookings == null) {
            _other.bookings = null;
        } else {
            _other.bookings = new ArrayList<Booking.Builder<PlanningTable.Builder<_B>>>();
            for (Booking _item: this.bookings) {
                _other.bookings.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.milestones == null) {
            _other.milestones = null;
        } else {
            _other.milestones = new ArrayList<Milestone.Builder<PlanningTable.Builder<_B>>>();
            for (Milestone _item: this.milestones) {
                _other.milestones.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.relations == null) {
            _other.relations = null;
        } else {
            _other.relations = new ArrayList<Relation.Builder<PlanningTable.Builder<_B>>>();
            for (Relation _item: this.relations) {
                _other.relations.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.backgroundEvents == null) {
            _other.backgroundEvents = null;
        } else {
            _other.backgroundEvents = new ArrayList<BackgroundEvent.Builder<PlanningTable.Builder<_B>>>();
            for (BackgroundEvent _item: this.backgroundEvents) {
                _other.backgroundEvents.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.searchFilters == null) {
            _other.searchFilters = null;
        } else {
            _other.searchFilters = new ArrayList<SearchfilterInfo.Builder<PlanningTable.Builder<_B>>>();
            for (SearchfilterInfo _item: this.searchFilters) {
                _other.searchFilters.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        if (this.businessHours == null) {
            _other.businessHours = null;
        } else {
            _other.businessHours = new ArrayList<BusinessHour.Builder<PlanningTable.Builder<_B>>>();
            for (BusinessHour _item: this.businessHours) {
                _other.businessHours.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.showBackgroundEvents = this.showBackgroundEvents;
        _other.componentLabel = this.componentLabel;
        _other.componentMenupath = this.componentMenupath;
        _other.dateFrom = this.dateFrom;
        _other.dateUntil = this.dateUntil;
        _other.legendLabel = this.legendLabel;
        _other.legendTooltip = this.legendTooltip;
        _other.dateChooserPolicy = this.dateChooserPolicy;
        _other.usePeriodicRefresh = this.usePeriodicRefresh;
        _other.periodicRefreshInterval = this.periodicRefreshInterval;
    }

    public<_B >PlanningTable.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PlanningTable.Builder<_B>(_parentBuilder, this, true);
    }

    public PlanningTable.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PlanningTable.Builder<Void> builder() {
        return new PlanningTable.Builder<Void>(null, null, false);
    }

    public static<_B >PlanningTable.Builder<_B> copyOf(final PlanningTable _other) {
        final PlanningTable.Builder<_B> _newBuilder = new PlanningTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PlanningTable.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree resourcesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resources"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourcesPropertyTree!= null):((resourcesPropertyTree == null)||(!resourcesPropertyTree.isLeaf())))) {
            if (this.resources == null) {
                _other.resources = null;
            } else {
                _other.resources = new ArrayList<PlanningResource.Builder<PlanningTable.Builder<_B>>>();
                for (PlanningResource _item: this.resources) {
                    _other.resources.add(((_item == null)?null:_item.newCopyBuilder(_other, resourcesPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree bookingsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bookings"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bookingsPropertyTree!= null):((bookingsPropertyTree == null)||(!bookingsPropertyTree.isLeaf())))) {
            if (this.bookings == null) {
                _other.bookings = null;
            } else {
                _other.bookings = new ArrayList<Booking.Builder<PlanningTable.Builder<_B>>>();
                for (Booking _item: this.bookings) {
                    _other.bookings.add(((_item == null)?null:_item.newCopyBuilder(_other, bookingsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree milestonesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("milestones"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(milestonesPropertyTree!= null):((milestonesPropertyTree == null)||(!milestonesPropertyTree.isLeaf())))) {
            if (this.milestones == null) {
                _other.milestones = null;
            } else {
                _other.milestones = new ArrayList<Milestone.Builder<PlanningTable.Builder<_B>>>();
                for (Milestone _item: this.milestones) {
                    _other.milestones.add(((_item == null)?null:_item.newCopyBuilder(_other, milestonesPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree relationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("relations"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(relationsPropertyTree!= null):((relationsPropertyTree == null)||(!relationsPropertyTree.isLeaf())))) {
            if (this.relations == null) {
                _other.relations = null;
            } else {
                _other.relations = new ArrayList<Relation.Builder<PlanningTable.Builder<_B>>>();
                for (Relation _item: this.relations) {
                    _other.relations.add(((_item == null)?null:_item.newCopyBuilder(_other, relationsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree backgroundEventsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("backgroundEvents"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundEventsPropertyTree!= null):((backgroundEventsPropertyTree == null)||(!backgroundEventsPropertyTree.isLeaf())))) {
            if (this.backgroundEvents == null) {
                _other.backgroundEvents = null;
            } else {
                _other.backgroundEvents = new ArrayList<BackgroundEvent.Builder<PlanningTable.Builder<_B>>>();
                for (BackgroundEvent _item: this.backgroundEvents) {
                    _other.backgroundEvents.add(((_item == null)?null:_item.newCopyBuilder(_other, backgroundEventsPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree searchFiltersPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("searchFilters"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchFiltersPropertyTree!= null):((searchFiltersPropertyTree == null)||(!searchFiltersPropertyTree.isLeaf())))) {
            if (this.searchFilters == null) {
                _other.searchFilters = null;
            } else {
                _other.searchFilters = new ArrayList<SearchfilterInfo.Builder<PlanningTable.Builder<_B>>>();
                for (SearchfilterInfo _item: this.searchFilters) {
                    _other.searchFilters.add(((_item == null)?null:_item.newCopyBuilder(_other, searchFiltersPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree businessHoursPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("businessHours"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(businessHoursPropertyTree!= null):((businessHoursPropertyTree == null)||(!businessHoursPropertyTree.isLeaf())))) {
            if (this.businessHours == null) {
                _other.businessHours = null;
            } else {
                _other.businessHours = new ArrayList<BusinessHour.Builder<PlanningTable.Builder<_B>>>();
                for (BusinessHour _item: this.businessHours) {
                    _other.businessHours.add(((_item == null)?null:_item.newCopyBuilder(_other, businessHoursPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree showBackgroundEventsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("showBackgroundEvents"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(showBackgroundEventsPropertyTree!= null):((showBackgroundEventsPropertyTree == null)||(!showBackgroundEventsPropertyTree.isLeaf())))) {
            _other.showBackgroundEvents = this.showBackgroundEvents;
        }
        final PropertyTree componentLabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("componentLabel"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(componentLabelPropertyTree!= null):((componentLabelPropertyTree == null)||(!componentLabelPropertyTree.isLeaf())))) {
            _other.componentLabel = this.componentLabel;
        }
        final PropertyTree componentMenupathPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("componentMenupath"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(componentMenupathPropertyTree!= null):((componentMenupathPropertyTree == null)||(!componentMenupathPropertyTree.isLeaf())))) {
            _other.componentMenupath = this.componentMenupath;
        }
        final PropertyTree dateFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateFrom"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateFromPropertyTree!= null):((dateFromPropertyTree == null)||(!dateFromPropertyTree.isLeaf())))) {
            _other.dateFrom = this.dateFrom;
        }
        final PropertyTree dateUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateUntil"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateUntilPropertyTree!= null):((dateUntilPropertyTree == null)||(!dateUntilPropertyTree.isLeaf())))) {
            _other.dateUntil = this.dateUntil;
        }
        final PropertyTree legendLabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("legendLabel"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(legendLabelPropertyTree!= null):((legendLabelPropertyTree == null)||(!legendLabelPropertyTree.isLeaf())))) {
            _other.legendLabel = this.legendLabel;
        }
        final PropertyTree legendTooltipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("legendTooltip"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(legendTooltipPropertyTree!= null):((legendTooltipPropertyTree == null)||(!legendTooltipPropertyTree.isLeaf())))) {
            _other.legendTooltip = this.legendTooltip;
        }
        final PropertyTree dateChooserPolicyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateChooserPolicy"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateChooserPolicyPropertyTree!= null):((dateChooserPolicyPropertyTree == null)||(!dateChooserPolicyPropertyTree.isLeaf())))) {
            _other.dateChooserPolicy = this.dateChooserPolicy;
        }
        final PropertyTree usePeriodicRefreshPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("usePeriodicRefresh"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(usePeriodicRefreshPropertyTree!= null):((usePeriodicRefreshPropertyTree == null)||(!usePeriodicRefreshPropertyTree.isLeaf())))) {
            _other.usePeriodicRefresh = this.usePeriodicRefresh;
        }
        final PropertyTree periodicRefreshIntervalPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("periodicRefreshInterval"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(periodicRefreshIntervalPropertyTree!= null):((periodicRefreshIntervalPropertyTree == null)||(!periodicRefreshIntervalPropertyTree.isLeaf())))) {
            _other.periodicRefreshInterval = this.periodicRefreshInterval;
        }
    }

    public<_B >PlanningTable.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PlanningTable.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PlanningTable.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PlanningTable.Builder<_B> copyOf(final PlanningTable _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PlanningTable.Builder<_B> _newBuilder = new PlanningTable.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PlanningTable.Builder<Void> copyExcept(final PlanningTable _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PlanningTable.Builder<Void> copyOnly(final PlanningTable _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PlanningTable _storedValue;
        private List<PlanningResource.Builder<PlanningTable.Builder<_B>>> resources;
        private List<Booking.Builder<PlanningTable.Builder<_B>>> bookings;
        private List<Milestone.Builder<PlanningTable.Builder<_B>>> milestones;
        private List<Relation.Builder<PlanningTable.Builder<_B>>> relations;
        private List<BackgroundEvent.Builder<PlanningTable.Builder<_B>>> backgroundEvents;
        private List<SearchfilterInfo.Builder<PlanningTable.Builder<_B>>> searchFilters;
        private List<BusinessHour.Builder<PlanningTable.Builder<_B>>> businessHours;
        private String showBackgroundEvents;
        private String componentLabel;
        private String componentMenupath;
        private String dateFrom;
        private String dateUntil;
        private String legendLabel;
        private String legendTooltip;
        private String dateChooserPolicy;
        private boolean usePeriodicRefresh;
        private long periodicRefreshInterval;

        public Builder(final _B _parentBuilder, final PlanningTable _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.resources == null) {
                        this.resources = null;
                    } else {
                        this.resources = new ArrayList<PlanningResource.Builder<PlanningTable.Builder<_B>>>();
                        for (PlanningResource _item: _other.resources) {
                            this.resources.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.bookings == null) {
                        this.bookings = null;
                    } else {
                        this.bookings = new ArrayList<Booking.Builder<PlanningTable.Builder<_B>>>();
                        for (Booking _item: _other.bookings) {
                            this.bookings.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.milestones == null) {
                        this.milestones = null;
                    } else {
                        this.milestones = new ArrayList<Milestone.Builder<PlanningTable.Builder<_B>>>();
                        for (Milestone _item: _other.milestones) {
                            this.milestones.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.relations == null) {
                        this.relations = null;
                    } else {
                        this.relations = new ArrayList<Relation.Builder<PlanningTable.Builder<_B>>>();
                        for (Relation _item: _other.relations) {
                            this.relations.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.backgroundEvents == null) {
                        this.backgroundEvents = null;
                    } else {
                        this.backgroundEvents = new ArrayList<BackgroundEvent.Builder<PlanningTable.Builder<_B>>>();
                        for (BackgroundEvent _item: _other.backgroundEvents) {
                            this.backgroundEvents.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.searchFilters == null) {
                        this.searchFilters = null;
                    } else {
                        this.searchFilters = new ArrayList<SearchfilterInfo.Builder<PlanningTable.Builder<_B>>>();
                        for (SearchfilterInfo _item: _other.searchFilters) {
                            this.searchFilters.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    if (_other.businessHours == null) {
                        this.businessHours = null;
                    } else {
                        this.businessHours = new ArrayList<BusinessHour.Builder<PlanningTable.Builder<_B>>>();
                        for (BusinessHour _item: _other.businessHours) {
                            this.businessHours.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.showBackgroundEvents = _other.showBackgroundEvents;
                    this.componentLabel = _other.componentLabel;
                    this.componentMenupath = _other.componentMenupath;
                    this.dateFrom = _other.dateFrom;
                    this.dateUntil = _other.dateUntil;
                    this.legendLabel = _other.legendLabel;
                    this.legendTooltip = _other.legendTooltip;
                    this.dateChooserPolicy = _other.dateChooserPolicy;
                    this.usePeriodicRefresh = _other.usePeriodicRefresh;
                    this.periodicRefreshInterval = _other.periodicRefreshInterval;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PlanningTable _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree resourcesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("resources"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(resourcesPropertyTree!= null):((resourcesPropertyTree == null)||(!resourcesPropertyTree.isLeaf())))) {
                        if (_other.resources == null) {
                            this.resources = null;
                        } else {
                            this.resources = new ArrayList<PlanningResource.Builder<PlanningTable.Builder<_B>>>();
                            for (PlanningResource _item: _other.resources) {
                                this.resources.add(((_item == null)?null:_item.newCopyBuilder(this, resourcesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree bookingsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("bookings"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(bookingsPropertyTree!= null):((bookingsPropertyTree == null)||(!bookingsPropertyTree.isLeaf())))) {
                        if (_other.bookings == null) {
                            this.bookings = null;
                        } else {
                            this.bookings = new ArrayList<Booking.Builder<PlanningTable.Builder<_B>>>();
                            for (Booking _item: _other.bookings) {
                                this.bookings.add(((_item == null)?null:_item.newCopyBuilder(this, bookingsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree milestonesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("milestones"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(milestonesPropertyTree!= null):((milestonesPropertyTree == null)||(!milestonesPropertyTree.isLeaf())))) {
                        if (_other.milestones == null) {
                            this.milestones = null;
                        } else {
                            this.milestones = new ArrayList<Milestone.Builder<PlanningTable.Builder<_B>>>();
                            for (Milestone _item: _other.milestones) {
                                this.milestones.add(((_item == null)?null:_item.newCopyBuilder(this, milestonesPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree relationsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("relations"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(relationsPropertyTree!= null):((relationsPropertyTree == null)||(!relationsPropertyTree.isLeaf())))) {
                        if (_other.relations == null) {
                            this.relations = null;
                        } else {
                            this.relations = new ArrayList<Relation.Builder<PlanningTable.Builder<_B>>>();
                            for (Relation _item: _other.relations) {
                                this.relations.add(((_item == null)?null:_item.newCopyBuilder(this, relationsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree backgroundEventsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("backgroundEvents"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(backgroundEventsPropertyTree!= null):((backgroundEventsPropertyTree == null)||(!backgroundEventsPropertyTree.isLeaf())))) {
                        if (_other.backgroundEvents == null) {
                            this.backgroundEvents = null;
                        } else {
                            this.backgroundEvents = new ArrayList<BackgroundEvent.Builder<PlanningTable.Builder<_B>>>();
                            for (BackgroundEvent _item: _other.backgroundEvents) {
                                this.backgroundEvents.add(((_item == null)?null:_item.newCopyBuilder(this, backgroundEventsPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree searchFiltersPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("searchFilters"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(searchFiltersPropertyTree!= null):((searchFiltersPropertyTree == null)||(!searchFiltersPropertyTree.isLeaf())))) {
                        if (_other.searchFilters == null) {
                            this.searchFilters = null;
                        } else {
                            this.searchFilters = new ArrayList<SearchfilterInfo.Builder<PlanningTable.Builder<_B>>>();
                            for (SearchfilterInfo _item: _other.searchFilters) {
                                this.searchFilters.add(((_item == null)?null:_item.newCopyBuilder(this, searchFiltersPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree businessHoursPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("businessHours"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(businessHoursPropertyTree!= null):((businessHoursPropertyTree == null)||(!businessHoursPropertyTree.isLeaf())))) {
                        if (_other.businessHours == null) {
                            this.businessHours = null;
                        } else {
                            this.businessHours = new ArrayList<BusinessHour.Builder<PlanningTable.Builder<_B>>>();
                            for (BusinessHour _item: _other.businessHours) {
                                this.businessHours.add(((_item == null)?null:_item.newCopyBuilder(this, businessHoursPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree showBackgroundEventsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("showBackgroundEvents"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(showBackgroundEventsPropertyTree!= null):((showBackgroundEventsPropertyTree == null)||(!showBackgroundEventsPropertyTree.isLeaf())))) {
                        this.showBackgroundEvents = _other.showBackgroundEvents;
                    }
                    final PropertyTree componentLabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("componentLabel"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(componentLabelPropertyTree!= null):((componentLabelPropertyTree == null)||(!componentLabelPropertyTree.isLeaf())))) {
                        this.componentLabel = _other.componentLabel;
                    }
                    final PropertyTree componentMenupathPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("componentMenupath"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(componentMenupathPropertyTree!= null):((componentMenupathPropertyTree == null)||(!componentMenupathPropertyTree.isLeaf())))) {
                        this.componentMenupath = _other.componentMenupath;
                    }
                    final PropertyTree dateFromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateFrom"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateFromPropertyTree!= null):((dateFromPropertyTree == null)||(!dateFromPropertyTree.isLeaf())))) {
                        this.dateFrom = _other.dateFrom;
                    }
                    final PropertyTree dateUntilPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateUntil"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateUntilPropertyTree!= null):((dateUntilPropertyTree == null)||(!dateUntilPropertyTree.isLeaf())))) {
                        this.dateUntil = _other.dateUntil;
                    }
                    final PropertyTree legendLabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("legendLabel"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(legendLabelPropertyTree!= null):((legendLabelPropertyTree == null)||(!legendLabelPropertyTree.isLeaf())))) {
                        this.legendLabel = _other.legendLabel;
                    }
                    final PropertyTree legendTooltipPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("legendTooltip"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(legendTooltipPropertyTree!= null):((legendTooltipPropertyTree == null)||(!legendTooltipPropertyTree.isLeaf())))) {
                        this.legendTooltip = _other.legendTooltip;
                    }
                    final PropertyTree dateChooserPolicyPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("dateChooserPolicy"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(dateChooserPolicyPropertyTree!= null):((dateChooserPolicyPropertyTree == null)||(!dateChooserPolicyPropertyTree.isLeaf())))) {
                        this.dateChooserPolicy = _other.dateChooserPolicy;
                    }
                    final PropertyTree usePeriodicRefreshPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("usePeriodicRefresh"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(usePeriodicRefreshPropertyTree!= null):((usePeriodicRefreshPropertyTree == null)||(!usePeriodicRefreshPropertyTree.isLeaf())))) {
                        this.usePeriodicRefresh = _other.usePeriodicRefresh;
                    }
                    final PropertyTree periodicRefreshIntervalPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("periodicRefreshInterval"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(periodicRefreshIntervalPropertyTree!= null):((periodicRefreshIntervalPropertyTree == null)||(!periodicRefreshIntervalPropertyTree.isLeaf())))) {
                        this.periodicRefreshInterval = _other.periodicRefreshInterval;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PlanningTable >_P init(final _P _product) {
            if (this.resources!= null) {
                final List<PlanningResource> resources = new ArrayList<PlanningResource>(this.resources.size());
                for (PlanningResource.Builder<PlanningTable.Builder<_B>> _item: this.resources) {
                    resources.add(_item.build());
                }
                _product.resources = resources;
            }
            if (this.bookings!= null) {
                final List<Booking> bookings = new ArrayList<Booking>(this.bookings.size());
                for (Booking.Builder<PlanningTable.Builder<_B>> _item: this.bookings) {
                    bookings.add(_item.build());
                }
                _product.bookings = bookings;
            }
            if (this.milestones!= null) {
                final List<Milestone> milestones = new ArrayList<Milestone>(this.milestones.size());
                for (Milestone.Builder<PlanningTable.Builder<_B>> _item: this.milestones) {
                    milestones.add(_item.build());
                }
                _product.milestones = milestones;
            }
            if (this.relations!= null) {
                final List<Relation> relations = new ArrayList<Relation>(this.relations.size());
                for (Relation.Builder<PlanningTable.Builder<_B>> _item: this.relations) {
                    relations.add(_item.build());
                }
                _product.relations = relations;
            }
            if (this.backgroundEvents!= null) {
                final List<BackgroundEvent> backgroundEvents = new ArrayList<BackgroundEvent>(this.backgroundEvents.size());
                for (BackgroundEvent.Builder<PlanningTable.Builder<_B>> _item: this.backgroundEvents) {
                    backgroundEvents.add(_item.build());
                }
                _product.backgroundEvents = backgroundEvents;
            }
            if (this.searchFilters!= null) {
                final List<SearchfilterInfo> searchFilters = new ArrayList<SearchfilterInfo>(this.searchFilters.size());
                for (SearchfilterInfo.Builder<PlanningTable.Builder<_B>> _item: this.searchFilters) {
                    searchFilters.add(_item.build());
                }
                _product.searchFilters = searchFilters;
            }
            if (this.businessHours!= null) {
                final List<BusinessHour> businessHours = new ArrayList<BusinessHour>(this.businessHours.size());
                for (BusinessHour.Builder<PlanningTable.Builder<_B>> _item: this.businessHours) {
                    businessHours.add(_item.build());
                }
                _product.businessHours = businessHours;
            }
            _product.showBackgroundEvents = this.showBackgroundEvents;
            _product.componentLabel = this.componentLabel;
            _product.componentMenupath = this.componentMenupath;
            _product.dateFrom = this.dateFrom;
            _product.dateUntil = this.dateUntil;
            _product.legendLabel = this.legendLabel;
            _product.legendTooltip = this.legendTooltip;
            _product.dateChooserPolicy = this.dateChooserPolicy;
            _product.usePeriodicRefresh = this.usePeriodicRefresh;
            _product.periodicRefreshInterval = this.periodicRefreshInterval;
            return _product;
        }

        /**
         * Adds the given items to the value of "resources"
         * 
         * @param resources
         *     Items to add to the value of the "resources" property
         */
        public PlanningTable.Builder<_B> addResources(final Iterable<? extends PlanningResource> resources) {
            if (resources!= null) {
                if (this.resources == null) {
                    this.resources = new ArrayList<PlanningResource.Builder<PlanningTable.Builder<_B>>>();
                }
                for (PlanningResource _item: resources) {
                    this.resources.add(new PlanningResource.Builder<PlanningTable.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "resources" (any previous value will be replaced)
         * 
         * @param resources
         *     New value of the "resources" property.
         */
        public PlanningTable.Builder<_B> withResources(final Iterable<? extends PlanningResource> resources) {
            if (this.resources!= null) {
                this.resources.clear();
            }
            return addResources(resources);
        }

        /**
         * Adds the given items to the value of "resources"
         * 
         * @param resources
         *     Items to add to the value of the "resources" property
         */
        public PlanningTable.Builder<_B> addResources(PlanningResource... resources) {
            addResources(Arrays.asList(resources));
            return this;
        }

        /**
         * Sets the new value of "resources" (any previous value will be replaced)
         * 
         * @param resources
         *     New value of the "resources" property.
         */
        public PlanningTable.Builder<_B> withResources(PlanningResource... resources) {
            withResources(Arrays.asList(resources));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Resources" property.
         * Use {@link org.nuclos.schema.rest.PlanningResource.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Resources" property.
         *     Use {@link org.nuclos.schema.rest.PlanningResource.Builder#end()} to return to the current builder.
         */
        public PlanningResource.Builder<? extends PlanningTable.Builder<_B>> addResources() {
            if (this.resources == null) {
                this.resources = new ArrayList<PlanningResource.Builder<PlanningTable.Builder<_B>>>();
            }
            final PlanningResource.Builder<PlanningTable.Builder<_B>> resources_Builder = new PlanningResource.Builder<PlanningTable.Builder<_B>>(this, null, false);
            this.resources.add(resources_Builder);
            return resources_Builder;
        }

        /**
         * Adds the given items to the value of "bookings"
         * 
         * @param bookings
         *     Items to add to the value of the "bookings" property
         */
        public PlanningTable.Builder<_B> addBookings(final Iterable<? extends Booking> bookings) {
            if (bookings!= null) {
                if (this.bookings == null) {
                    this.bookings = new ArrayList<Booking.Builder<PlanningTable.Builder<_B>>>();
                }
                for (Booking _item: bookings) {
                    this.bookings.add(new Booking.Builder<PlanningTable.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "bookings" (any previous value will be replaced)
         * 
         * @param bookings
         *     New value of the "bookings" property.
         */
        public PlanningTable.Builder<_B> withBookings(final Iterable<? extends Booking> bookings) {
            if (this.bookings!= null) {
                this.bookings.clear();
            }
            return addBookings(bookings);
        }

        /**
         * Adds the given items to the value of "bookings"
         * 
         * @param bookings
         *     Items to add to the value of the "bookings" property
         */
        public PlanningTable.Builder<_B> addBookings(Booking... bookings) {
            addBookings(Arrays.asList(bookings));
            return this;
        }

        /**
         * Sets the new value of "bookings" (any previous value will be replaced)
         * 
         * @param bookings
         *     New value of the "bookings" property.
         */
        public PlanningTable.Builder<_B> withBookings(Booking... bookings) {
            withBookings(Arrays.asList(bookings));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Bookings" property.
         * Use {@link org.nuclos.schema.rest.Booking.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Bookings" property.
         *     Use {@link org.nuclos.schema.rest.Booking.Builder#end()} to return to the current builder.
         */
        public Booking.Builder<? extends PlanningTable.Builder<_B>> addBookings() {
            if (this.bookings == null) {
                this.bookings = new ArrayList<Booking.Builder<PlanningTable.Builder<_B>>>();
            }
            final Booking.Builder<PlanningTable.Builder<_B>> bookings_Builder = new Booking.Builder<PlanningTable.Builder<_B>>(this, null, false);
            this.bookings.add(bookings_Builder);
            return bookings_Builder;
        }

        /**
         * Adds the given items to the value of "milestones"
         * 
         * @param milestones
         *     Items to add to the value of the "milestones" property
         */
        public PlanningTable.Builder<_B> addMilestones(final Iterable<? extends Milestone> milestones) {
            if (milestones!= null) {
                if (this.milestones == null) {
                    this.milestones = new ArrayList<Milestone.Builder<PlanningTable.Builder<_B>>>();
                }
                for (Milestone _item: milestones) {
                    this.milestones.add(new Milestone.Builder<PlanningTable.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "milestones" (any previous value will be replaced)
         * 
         * @param milestones
         *     New value of the "milestones" property.
         */
        public PlanningTable.Builder<_B> withMilestones(final Iterable<? extends Milestone> milestones) {
            if (this.milestones!= null) {
                this.milestones.clear();
            }
            return addMilestones(milestones);
        }

        /**
         * Adds the given items to the value of "milestones"
         * 
         * @param milestones
         *     Items to add to the value of the "milestones" property
         */
        public PlanningTable.Builder<_B> addMilestones(Milestone... milestones) {
            addMilestones(Arrays.asList(milestones));
            return this;
        }

        /**
         * Sets the new value of "milestones" (any previous value will be replaced)
         * 
         * @param milestones
         *     New value of the "milestones" property.
         */
        public PlanningTable.Builder<_B> withMilestones(Milestone... milestones) {
            withMilestones(Arrays.asList(milestones));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Milestones" property.
         * Use {@link org.nuclos.schema.rest.Milestone.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Milestones" property.
         *     Use {@link org.nuclos.schema.rest.Milestone.Builder#end()} to return to the current builder.
         */
        public Milestone.Builder<? extends PlanningTable.Builder<_B>> addMilestones() {
            if (this.milestones == null) {
                this.milestones = new ArrayList<Milestone.Builder<PlanningTable.Builder<_B>>>();
            }
            final Milestone.Builder<PlanningTable.Builder<_B>> milestones_Builder = new Milestone.Builder<PlanningTable.Builder<_B>>(this, null, false);
            this.milestones.add(milestones_Builder);
            return milestones_Builder;
        }

        /**
         * Adds the given items to the value of "relations"
         * 
         * @param relations
         *     Items to add to the value of the "relations" property
         */
        public PlanningTable.Builder<_B> addRelations(final Iterable<? extends Relation> relations) {
            if (relations!= null) {
                if (this.relations == null) {
                    this.relations = new ArrayList<Relation.Builder<PlanningTable.Builder<_B>>>();
                }
                for (Relation _item: relations) {
                    this.relations.add(new Relation.Builder<PlanningTable.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "relations" (any previous value will be replaced)
         * 
         * @param relations
         *     New value of the "relations" property.
         */
        public PlanningTable.Builder<_B> withRelations(final Iterable<? extends Relation> relations) {
            if (this.relations!= null) {
                this.relations.clear();
            }
            return addRelations(relations);
        }

        /**
         * Adds the given items to the value of "relations"
         * 
         * @param relations
         *     Items to add to the value of the "relations" property
         */
        public PlanningTable.Builder<_B> addRelations(Relation... relations) {
            addRelations(Arrays.asList(relations));
            return this;
        }

        /**
         * Sets the new value of "relations" (any previous value will be replaced)
         * 
         * @param relations
         *     New value of the "relations" property.
         */
        public PlanningTable.Builder<_B> withRelations(Relation... relations) {
            withRelations(Arrays.asList(relations));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Relations" property.
         * Use {@link org.nuclos.schema.rest.Relation.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Relations" property.
         *     Use {@link org.nuclos.schema.rest.Relation.Builder#end()} to return to the current builder.
         */
        public Relation.Builder<? extends PlanningTable.Builder<_B>> addRelations() {
            if (this.relations == null) {
                this.relations = new ArrayList<Relation.Builder<PlanningTable.Builder<_B>>>();
            }
            final Relation.Builder<PlanningTable.Builder<_B>> relations_Builder = new Relation.Builder<PlanningTable.Builder<_B>>(this, null, false);
            this.relations.add(relations_Builder);
            return relations_Builder;
        }

        /**
         * Adds the given items to the value of "backgroundEvents"
         * 
         * @param backgroundEvents
         *     Items to add to the value of the "backgroundEvents" property
         */
        public PlanningTable.Builder<_B> addBackgroundEvents(final Iterable<? extends BackgroundEvent> backgroundEvents) {
            if (backgroundEvents!= null) {
                if (this.backgroundEvents == null) {
                    this.backgroundEvents = new ArrayList<BackgroundEvent.Builder<PlanningTable.Builder<_B>>>();
                }
                for (BackgroundEvent _item: backgroundEvents) {
                    this.backgroundEvents.add(new BackgroundEvent.Builder<PlanningTable.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "backgroundEvents" (any previous value will be replaced)
         * 
         * @param backgroundEvents
         *     New value of the "backgroundEvents" property.
         */
        public PlanningTable.Builder<_B> withBackgroundEvents(final Iterable<? extends BackgroundEvent> backgroundEvents) {
            if (this.backgroundEvents!= null) {
                this.backgroundEvents.clear();
            }
            return addBackgroundEvents(backgroundEvents);
        }

        /**
         * Adds the given items to the value of "backgroundEvents"
         * 
         * @param backgroundEvents
         *     Items to add to the value of the "backgroundEvents" property
         */
        public PlanningTable.Builder<_B> addBackgroundEvents(BackgroundEvent... backgroundEvents) {
            addBackgroundEvents(Arrays.asList(backgroundEvents));
            return this;
        }

        /**
         * Sets the new value of "backgroundEvents" (any previous value will be replaced)
         * 
         * @param backgroundEvents
         *     New value of the "backgroundEvents" property.
         */
        public PlanningTable.Builder<_B> withBackgroundEvents(BackgroundEvent... backgroundEvents) {
            withBackgroundEvents(Arrays.asList(backgroundEvents));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "BackgroundEvents" property.
         * Use {@link org.nuclos.schema.rest.BackgroundEvent.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "BackgroundEvents" property.
         *     Use {@link org.nuclos.schema.rest.BackgroundEvent.Builder#end()} to return to the current builder.
         */
        public BackgroundEvent.Builder<? extends PlanningTable.Builder<_B>> addBackgroundEvents() {
            if (this.backgroundEvents == null) {
                this.backgroundEvents = new ArrayList<BackgroundEvent.Builder<PlanningTable.Builder<_B>>>();
            }
            final BackgroundEvent.Builder<PlanningTable.Builder<_B>> backgroundEvents_Builder = new BackgroundEvent.Builder<PlanningTable.Builder<_B>>(this, null, false);
            this.backgroundEvents.add(backgroundEvents_Builder);
            return backgroundEvents_Builder;
        }

        /**
         * Adds the given items to the value of "searchFilters"
         * 
         * @param searchFilters
         *     Items to add to the value of the "searchFilters" property
         */
        public PlanningTable.Builder<_B> addSearchFilters(final Iterable<? extends SearchfilterInfo> searchFilters) {
            if (searchFilters!= null) {
                if (this.searchFilters == null) {
                    this.searchFilters = new ArrayList<SearchfilterInfo.Builder<PlanningTable.Builder<_B>>>();
                }
                for (SearchfilterInfo _item: searchFilters) {
                    this.searchFilters.add(new SearchfilterInfo.Builder<PlanningTable.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "searchFilters" (any previous value will be replaced)
         * 
         * @param searchFilters
         *     New value of the "searchFilters" property.
         */
        public PlanningTable.Builder<_B> withSearchFilters(final Iterable<? extends SearchfilterInfo> searchFilters) {
            if (this.searchFilters!= null) {
                this.searchFilters.clear();
            }
            return addSearchFilters(searchFilters);
        }

        /**
         * Adds the given items to the value of "searchFilters"
         * 
         * @param searchFilters
         *     Items to add to the value of the "searchFilters" property
         */
        public PlanningTable.Builder<_B> addSearchFilters(SearchfilterInfo... searchFilters) {
            addSearchFilters(Arrays.asList(searchFilters));
            return this;
        }

        /**
         * Sets the new value of "searchFilters" (any previous value will be replaced)
         * 
         * @param searchFilters
         *     New value of the "searchFilters" property.
         */
        public PlanningTable.Builder<_B> withSearchFilters(SearchfilterInfo... searchFilters) {
            withSearchFilters(Arrays.asList(searchFilters));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "SearchFilters" property.
         * Use {@link org.nuclos.schema.rest.SearchfilterInfo.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "SearchFilters" property.
         *     Use {@link org.nuclos.schema.rest.SearchfilterInfo.Builder#end()} to return to the current builder.
         */
        public SearchfilterInfo.Builder<? extends PlanningTable.Builder<_B>> addSearchFilters() {
            if (this.searchFilters == null) {
                this.searchFilters = new ArrayList<SearchfilterInfo.Builder<PlanningTable.Builder<_B>>>();
            }
            final SearchfilterInfo.Builder<PlanningTable.Builder<_B>> searchFilters_Builder = new SearchfilterInfo.Builder<PlanningTable.Builder<_B>>(this, null, false);
            this.searchFilters.add(searchFilters_Builder);
            return searchFilters_Builder;
        }

        /**
         * Adds the given items to the value of "businessHours"
         * 
         * @param businessHours
         *     Items to add to the value of the "businessHours" property
         */
        public PlanningTable.Builder<_B> addBusinessHours(final Iterable<? extends BusinessHour> businessHours) {
            if (businessHours!= null) {
                if (this.businessHours == null) {
                    this.businessHours = new ArrayList<BusinessHour.Builder<PlanningTable.Builder<_B>>>();
                }
                for (BusinessHour _item: businessHours) {
                    this.businessHours.add(new BusinessHour.Builder<PlanningTable.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "businessHours" (any previous value will be replaced)
         * 
         * @param businessHours
         *     New value of the "businessHours" property.
         */
        public PlanningTable.Builder<_B> withBusinessHours(final Iterable<? extends BusinessHour> businessHours) {
            if (this.businessHours!= null) {
                this.businessHours.clear();
            }
            return addBusinessHours(businessHours);
        }

        /**
         * Adds the given items to the value of "businessHours"
         * 
         * @param businessHours
         *     Items to add to the value of the "businessHours" property
         */
        public PlanningTable.Builder<_B> addBusinessHours(BusinessHour... businessHours) {
            addBusinessHours(Arrays.asList(businessHours));
            return this;
        }

        /**
         * Sets the new value of "businessHours" (any previous value will be replaced)
         * 
         * @param businessHours
         *     New value of the "businessHours" property.
         */
        public PlanningTable.Builder<_B> withBusinessHours(BusinessHour... businessHours) {
            withBusinessHours(Arrays.asList(businessHours));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "BusinessHours" property.
         * Use {@link org.nuclos.schema.rest.BusinessHour.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "BusinessHours" property.
         *     Use {@link org.nuclos.schema.rest.BusinessHour.Builder#end()} to return to the current builder.
         */
        public BusinessHour.Builder<? extends PlanningTable.Builder<_B>> addBusinessHours() {
            if (this.businessHours == null) {
                this.businessHours = new ArrayList<BusinessHour.Builder<PlanningTable.Builder<_B>>>();
            }
            final BusinessHour.Builder<PlanningTable.Builder<_B>> businessHours_Builder = new BusinessHour.Builder<PlanningTable.Builder<_B>>(this, null, false);
            this.businessHours.add(businessHours_Builder);
            return businessHours_Builder;
        }

        /**
         * Sets the new value of "showBackgroundEvents" (any previous value will be replaced)
         * 
         * @param showBackgroundEvents
         *     New value of the "showBackgroundEvents" property.
         */
        public PlanningTable.Builder<_B> withShowBackgroundEvents(final String showBackgroundEvents) {
            this.showBackgroundEvents = showBackgroundEvents;
            return this;
        }

        /**
         * Sets the new value of "componentLabel" (any previous value will be replaced)
         * 
         * @param componentLabel
         *     New value of the "componentLabel" property.
         */
        public PlanningTable.Builder<_B> withComponentLabel(final String componentLabel) {
            this.componentLabel = componentLabel;
            return this;
        }

        /**
         * Sets the new value of "componentMenupath" (any previous value will be replaced)
         * 
         * @param componentMenupath
         *     New value of the "componentMenupath" property.
         */
        public PlanningTable.Builder<_B> withComponentMenupath(final String componentMenupath) {
            this.componentMenupath = componentMenupath;
            return this;
        }

        /**
         * Sets the new value of "dateFrom" (any previous value will be replaced)
         * 
         * @param dateFrom
         *     New value of the "dateFrom" property.
         */
        public PlanningTable.Builder<_B> withDateFrom(final String dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        /**
         * Sets the new value of "dateUntil" (any previous value will be replaced)
         * 
         * @param dateUntil
         *     New value of the "dateUntil" property.
         */
        public PlanningTable.Builder<_B> withDateUntil(final String dateUntil) {
            this.dateUntil = dateUntil;
            return this;
        }

        /**
         * Sets the new value of "legendLabel" (any previous value will be replaced)
         * 
         * @param legendLabel
         *     New value of the "legendLabel" property.
         */
        public PlanningTable.Builder<_B> withLegendLabel(final String legendLabel) {
            this.legendLabel = legendLabel;
            return this;
        }

        /**
         * Sets the new value of "legendTooltip" (any previous value will be replaced)
         * 
         * @param legendTooltip
         *     New value of the "legendTooltip" property.
         */
        public PlanningTable.Builder<_B> withLegendTooltip(final String legendTooltip) {
            this.legendTooltip = legendTooltip;
            return this;
        }

        /**
         * Sets the new value of "dateChooserPolicy" (any previous value will be replaced)
         * 
         * @param dateChooserPolicy
         *     New value of the "dateChooserPolicy" property.
         */
        public PlanningTable.Builder<_B> withDateChooserPolicy(final String dateChooserPolicy) {
            this.dateChooserPolicy = dateChooserPolicy;
            return this;
        }

        /**
         * Sets the new value of "usePeriodicRefresh" (any previous value will be replaced)
         * 
         * @param usePeriodicRefresh
         *     New value of the "usePeriodicRefresh" property.
         */
        public PlanningTable.Builder<_B> withUsePeriodicRefresh(final boolean usePeriodicRefresh) {
            this.usePeriodicRefresh = usePeriodicRefresh;
            return this;
        }

        /**
         * Sets the new value of "periodicRefreshInterval" (any previous value will be replaced)
         * 
         * @param periodicRefreshInterval
         *     New value of the "periodicRefreshInterval" property.
         */
        public PlanningTable.Builder<_B> withPeriodicRefreshInterval(final long periodicRefreshInterval) {
            this.periodicRefreshInterval = periodicRefreshInterval;
            return this;
        }

        @Override
        public PlanningTable build() {
            if (_storedValue == null) {
                return this.init(new PlanningTable());
            } else {
                return ((PlanningTable) _storedValue);
            }
        }

        public PlanningTable.Builder<_B> copyOf(final PlanningTable _other) {
            _other.copyTo(this);
            return this;
        }

        public PlanningTable.Builder<_B> copyOf(final PlanningTable.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PlanningTable.Selector<PlanningTable.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PlanningTable.Select _root() {
            return new PlanningTable.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private PlanningResource.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> resources = null;
        private Booking.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> bookings = null;
        private Milestone.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> milestones = null;
        private Relation.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> relations = null;
        private BackgroundEvent.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> backgroundEvents = null;
        private SearchfilterInfo.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> searchFilters = null;
        private BusinessHour.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> businessHours = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> showBackgroundEvents = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> componentLabel = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> componentMenupath = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> dateFrom = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> dateUntil = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> legendLabel = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> legendTooltip = null;
        private com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> dateChooserPolicy = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.resources!= null) {
                products.put("resources", this.resources.init());
            }
            if (this.bookings!= null) {
                products.put("bookings", this.bookings.init());
            }
            if (this.milestones!= null) {
                products.put("milestones", this.milestones.init());
            }
            if (this.relations!= null) {
                products.put("relations", this.relations.init());
            }
            if (this.backgroundEvents!= null) {
                products.put("backgroundEvents", this.backgroundEvents.init());
            }
            if (this.searchFilters!= null) {
                products.put("searchFilters", this.searchFilters.init());
            }
            if (this.businessHours!= null) {
                products.put("businessHours", this.businessHours.init());
            }
            if (this.showBackgroundEvents!= null) {
                products.put("showBackgroundEvents", this.showBackgroundEvents.init());
            }
            if (this.componentLabel!= null) {
                products.put("componentLabel", this.componentLabel.init());
            }
            if (this.componentMenupath!= null) {
                products.put("componentMenupath", this.componentMenupath.init());
            }
            if (this.dateFrom!= null) {
                products.put("dateFrom", this.dateFrom.init());
            }
            if (this.dateUntil!= null) {
                products.put("dateUntil", this.dateUntil.init());
            }
            if (this.legendLabel!= null) {
                products.put("legendLabel", this.legendLabel.init());
            }
            if (this.legendTooltip!= null) {
                products.put("legendTooltip", this.legendTooltip.init());
            }
            if (this.dateChooserPolicy!= null) {
                products.put("dateChooserPolicy", this.dateChooserPolicy.init());
            }
            return products;
        }

        public PlanningResource.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> resources() {
            return ((this.resources == null)?this.resources = new PlanningResource.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "resources"):this.resources);
        }

        public Booking.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> bookings() {
            return ((this.bookings == null)?this.bookings = new Booking.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "bookings"):this.bookings);
        }

        public Milestone.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> milestones() {
            return ((this.milestones == null)?this.milestones = new Milestone.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "milestones"):this.milestones);
        }

        public Relation.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> relations() {
            return ((this.relations == null)?this.relations = new Relation.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "relations"):this.relations);
        }

        public BackgroundEvent.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> backgroundEvents() {
            return ((this.backgroundEvents == null)?this.backgroundEvents = new BackgroundEvent.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "backgroundEvents"):this.backgroundEvents);
        }

        public SearchfilterInfo.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> searchFilters() {
            return ((this.searchFilters == null)?this.searchFilters = new SearchfilterInfo.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "searchFilters"):this.searchFilters);
        }

        public BusinessHour.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> businessHours() {
            return ((this.businessHours == null)?this.businessHours = new BusinessHour.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "businessHours"):this.businessHours);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> showBackgroundEvents() {
            return ((this.showBackgroundEvents == null)?this.showBackgroundEvents = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "showBackgroundEvents"):this.showBackgroundEvents);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> componentLabel() {
            return ((this.componentLabel == null)?this.componentLabel = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "componentLabel"):this.componentLabel);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> componentMenupath() {
            return ((this.componentMenupath == null)?this.componentMenupath = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "componentMenupath"):this.componentMenupath);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> dateFrom() {
            return ((this.dateFrom == null)?this.dateFrom = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "dateFrom"):this.dateFrom);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> dateUntil() {
            return ((this.dateUntil == null)?this.dateUntil = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "dateUntil"):this.dateUntil);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> legendLabel() {
            return ((this.legendLabel == null)?this.legendLabel = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "legendLabel"):this.legendLabel);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> legendTooltip() {
            return ((this.legendTooltip == null)?this.legendTooltip = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "legendTooltip"):this.legendTooltip);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>> dateChooserPolicy() {
            return ((this.dateChooserPolicy == null)?this.dateChooserPolicy = new com.kscs.util.jaxb.Selector<TRoot, PlanningTable.Selector<TRoot, TParent>>(this._root, this, "dateChooserPolicy"):this.dateChooserPolicy);
        }

    }

}
