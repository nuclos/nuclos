package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for printout-output-format complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="printout-output-format"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parameters" type="{urn:org.nuclos.schema.rest}printout-output-format-parameter" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="outputFormatId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="selected" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="fileName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "printout-output-format", propOrder = {
    "parameters"
})
public class PrintoutOutputFormat implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<PrintoutOutputFormatParameter> parameters;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "outputFormatId")
    protected String outputFormatId;
    @XmlAttribute(name = "selected")
    protected Boolean selected;
    @XmlAttribute(name = "fileName")
    protected String fileName;

    /**
     * Gets the value of the parameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrintoutOutputFormatParameter }
     * 
     * 
     */
    public List<PrintoutOutputFormatParameter> getParameters() {
        if (parameters == null) {
            parameters = new ArrayList<PrintoutOutputFormatParameter>();
        }
        return this.parameters;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the outputFormatId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutputFormatId() {
        return outputFormatId;
    }

    /**
     * Sets the value of the outputFormatId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutputFormatId(String value) {
        this.outputFormatId = value;
    }

    /**
     * Gets the value of the selected property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSelected() {
        return selected;
    }

    /**
     * Sets the value of the selected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSelected(Boolean value) {
        this.selected = value;
    }

    /**
     * Gets the value of the fileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the value of the fileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<PrintoutOutputFormatParameter> theParameters;
            theParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
            strategy.appendField(locator, this, "parameters", buffer, theParameters);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theOutputFormatId;
            theOutputFormatId = this.getOutputFormatId();
            strategy.appendField(locator, this, "outputFormatId", buffer, theOutputFormatId);
        }
        {
            Boolean theSelected;
            theSelected = this.isSelected();
            strategy.appendField(locator, this, "selected", buffer, theSelected);
        }
        {
            String theFileName;
            theFileName = this.getFileName();
            strategy.appendField(locator, this, "fileName", buffer, theFileName);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PrintoutOutputFormat)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PrintoutOutputFormat that = ((PrintoutOutputFormat) object);
        {
            List<PrintoutOutputFormatParameter> lhsParameters;
            lhsParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
            List<PrintoutOutputFormatParameter> rhsParameters;
            rhsParameters = (((that.parameters!= null)&&(!that.parameters.isEmpty()))?that.getParameters():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameters", lhsParameters), LocatorUtils.property(thatLocator, "parameters", rhsParameters), lhsParameters, rhsParameters)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsOutputFormatId;
            lhsOutputFormatId = this.getOutputFormatId();
            String rhsOutputFormatId;
            rhsOutputFormatId = that.getOutputFormatId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "outputFormatId", lhsOutputFormatId), LocatorUtils.property(thatLocator, "outputFormatId", rhsOutputFormatId), lhsOutputFormatId, rhsOutputFormatId)) {
                return false;
            }
        }
        {
            Boolean lhsSelected;
            lhsSelected = this.isSelected();
            Boolean rhsSelected;
            rhsSelected = that.isSelected();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "selected", lhsSelected), LocatorUtils.property(thatLocator, "selected", rhsSelected), lhsSelected, rhsSelected)) {
                return false;
            }
        }
        {
            String lhsFileName;
            lhsFileName = this.getFileName();
            String rhsFileName;
            rhsFileName = that.getFileName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fileName", lhsFileName), LocatorUtils.property(thatLocator, "fileName", rhsFileName), lhsFileName, rhsFileName)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<PrintoutOutputFormatParameter> theParameters;
            theParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "parameters", theParameters), currentHashCode, theParameters);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theOutputFormatId;
            theOutputFormatId = this.getOutputFormatId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "outputFormatId", theOutputFormatId), currentHashCode, theOutputFormatId);
        }
        {
            Boolean theSelected;
            theSelected = this.isSelected();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "selected", theSelected), currentHashCode, theSelected);
        }
        {
            String theFileName;
            theFileName = this.getFileName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fileName", theFileName), currentHashCode, theFileName);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PrintoutOutputFormat) {
            final PrintoutOutputFormat copy = ((PrintoutOutputFormat) draftCopy);
            if ((this.parameters!= null)&&(!this.parameters.isEmpty())) {
                List<PrintoutOutputFormatParameter> sourceParameters;
                sourceParameters = (((this.parameters!= null)&&(!this.parameters.isEmpty()))?this.getParameters():null);
                @SuppressWarnings("unchecked")
                List<PrintoutOutputFormatParameter> copyParameters = ((List<PrintoutOutputFormatParameter> ) strategy.copy(LocatorUtils.property(locator, "parameters", sourceParameters), sourceParameters));
                copy.parameters = null;
                if (copyParameters!= null) {
                    List<PrintoutOutputFormatParameter> uniqueParametersl = copy.getParameters();
                    uniqueParametersl.addAll(copyParameters);
                }
            } else {
                copy.parameters = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.outputFormatId!= null) {
                String sourceOutputFormatId;
                sourceOutputFormatId = this.getOutputFormatId();
                String copyOutputFormatId = ((String) strategy.copy(LocatorUtils.property(locator, "outputFormatId", sourceOutputFormatId), sourceOutputFormatId));
                copy.setOutputFormatId(copyOutputFormatId);
            } else {
                copy.outputFormatId = null;
            }
            if (this.selected!= null) {
                Boolean sourceSelected;
                sourceSelected = this.isSelected();
                Boolean copySelected = ((Boolean) strategy.copy(LocatorUtils.property(locator, "selected", sourceSelected), sourceSelected));
                copy.setSelected(copySelected);
            } else {
                copy.selected = null;
            }
            if (this.fileName!= null) {
                String sourceFileName;
                sourceFileName = this.getFileName();
                String copyFileName = ((String) strategy.copy(LocatorUtils.property(locator, "fileName", sourceFileName), sourceFileName));
                copy.setFileName(copyFileName);
            } else {
                copy.fileName = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PrintoutOutputFormat();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PrintoutOutputFormat.Builder<_B> _other) {
        if (this.parameters == null) {
            _other.parameters = null;
        } else {
            _other.parameters = new ArrayList<PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>>>();
            for (PrintoutOutputFormatParameter _item: this.parameters) {
                _other.parameters.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.name = this.name;
        _other.outputFormatId = this.outputFormatId;
        _other.selected = this.selected;
        _other.fileName = this.fileName;
    }

    public<_B >PrintoutOutputFormat.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PrintoutOutputFormat.Builder<_B>(_parentBuilder, this, true);
    }

    public PrintoutOutputFormat.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PrintoutOutputFormat.Builder<Void> builder() {
        return new PrintoutOutputFormat.Builder<Void>(null, null, false);
    }

    public static<_B >PrintoutOutputFormat.Builder<_B> copyOf(final PrintoutOutputFormat _other) {
        final PrintoutOutputFormat.Builder<_B> _newBuilder = new PrintoutOutputFormat.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PrintoutOutputFormat.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree parametersPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameters"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parametersPropertyTree!= null):((parametersPropertyTree == null)||(!parametersPropertyTree.isLeaf())))) {
            if (this.parameters == null) {
                _other.parameters = null;
            } else {
                _other.parameters = new ArrayList<PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>>>();
                for (PrintoutOutputFormatParameter _item: this.parameters) {
                    _other.parameters.add(((_item == null)?null:_item.newCopyBuilder(_other, parametersPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree outputFormatIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("outputFormatId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(outputFormatIdPropertyTree!= null):((outputFormatIdPropertyTree == null)||(!outputFormatIdPropertyTree.isLeaf())))) {
            _other.outputFormatId = this.outputFormatId;
        }
        final PropertyTree selectedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("selected"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(selectedPropertyTree!= null):((selectedPropertyTree == null)||(!selectedPropertyTree.isLeaf())))) {
            _other.selected = this.selected;
        }
        final PropertyTree fileNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fileName"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fileNamePropertyTree!= null):((fileNamePropertyTree == null)||(!fileNamePropertyTree.isLeaf())))) {
            _other.fileName = this.fileName;
        }
    }

    public<_B >PrintoutOutputFormat.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PrintoutOutputFormat.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PrintoutOutputFormat.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PrintoutOutputFormat.Builder<_B> copyOf(final PrintoutOutputFormat _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PrintoutOutputFormat.Builder<_B> _newBuilder = new PrintoutOutputFormat.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PrintoutOutputFormat.Builder<Void> copyExcept(final PrintoutOutputFormat _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PrintoutOutputFormat.Builder<Void> copyOnly(final PrintoutOutputFormat _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PrintoutOutputFormat _storedValue;
        private List<PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>>> parameters;
        private String name;
        private String outputFormatId;
        private Boolean selected;
        private String fileName;

        public Builder(final _B _parentBuilder, final PrintoutOutputFormat _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.parameters == null) {
                        this.parameters = null;
                    } else {
                        this.parameters = new ArrayList<PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>>>();
                        for (PrintoutOutputFormatParameter _item: _other.parameters) {
                            this.parameters.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.name = _other.name;
                    this.outputFormatId = _other.outputFormatId;
                    this.selected = _other.selected;
                    this.fileName = _other.fileName;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PrintoutOutputFormat _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree parametersPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameters"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parametersPropertyTree!= null):((parametersPropertyTree == null)||(!parametersPropertyTree.isLeaf())))) {
                        if (_other.parameters == null) {
                            this.parameters = null;
                        } else {
                            this.parameters = new ArrayList<PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>>>();
                            for (PrintoutOutputFormatParameter _item: _other.parameters) {
                                this.parameters.add(((_item == null)?null:_item.newCopyBuilder(this, parametersPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree outputFormatIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("outputFormatId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(outputFormatIdPropertyTree!= null):((outputFormatIdPropertyTree == null)||(!outputFormatIdPropertyTree.isLeaf())))) {
                        this.outputFormatId = _other.outputFormatId;
                    }
                    final PropertyTree selectedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("selected"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(selectedPropertyTree!= null):((selectedPropertyTree == null)||(!selectedPropertyTree.isLeaf())))) {
                        this.selected = _other.selected;
                    }
                    final PropertyTree fileNamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fileName"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fileNamePropertyTree!= null):((fileNamePropertyTree == null)||(!fileNamePropertyTree.isLeaf())))) {
                        this.fileName = _other.fileName;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PrintoutOutputFormat >_P init(final _P _product) {
            if (this.parameters!= null) {
                final List<PrintoutOutputFormatParameter> parameters = new ArrayList<PrintoutOutputFormatParameter>(this.parameters.size());
                for (PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>> _item: this.parameters) {
                    parameters.add(_item.build());
                }
                _product.parameters = parameters;
            }
            _product.name = this.name;
            _product.outputFormatId = this.outputFormatId;
            _product.selected = this.selected;
            _product.fileName = this.fileName;
            return _product;
        }

        /**
         * Adds the given items to the value of "parameters"
         * 
         * @param parameters
         *     Items to add to the value of the "parameters" property
         */
        public PrintoutOutputFormat.Builder<_B> addParameters(final Iterable<? extends PrintoutOutputFormatParameter> parameters) {
            if (parameters!= null) {
                if (this.parameters == null) {
                    this.parameters = new ArrayList<PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>>>();
                }
                for (PrintoutOutputFormatParameter _item: parameters) {
                    this.parameters.add(new PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "parameters" (any previous value will be replaced)
         * 
         * @param parameters
         *     New value of the "parameters" property.
         */
        public PrintoutOutputFormat.Builder<_B> withParameters(final Iterable<? extends PrintoutOutputFormatParameter> parameters) {
            if (this.parameters!= null) {
                this.parameters.clear();
            }
            return addParameters(parameters);
        }

        /**
         * Adds the given items to the value of "parameters"
         * 
         * @param parameters
         *     Items to add to the value of the "parameters" property
         */
        public PrintoutOutputFormat.Builder<_B> addParameters(PrintoutOutputFormatParameter... parameters) {
            addParameters(Arrays.asList(parameters));
            return this;
        }

        /**
         * Sets the new value of "parameters" (any previous value will be replaced)
         * 
         * @param parameters
         *     New value of the "parameters" property.
         */
        public PrintoutOutputFormat.Builder<_B> withParameters(PrintoutOutputFormatParameter... parameters) {
            withParameters(Arrays.asList(parameters));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Parameters" property.
         * Use {@link org.nuclos.schema.rest.PrintoutOutputFormatParameter.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Parameters" property.
         *     Use {@link org.nuclos.schema.rest.PrintoutOutputFormatParameter.Builder#end()} to return to the current builder.
         */
        public PrintoutOutputFormatParameter.Builder<? extends PrintoutOutputFormat.Builder<_B>> addParameters() {
            if (this.parameters == null) {
                this.parameters = new ArrayList<PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>>>();
            }
            final PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>> parameters_Builder = new PrintoutOutputFormatParameter.Builder<PrintoutOutputFormat.Builder<_B>>(this, null, false);
            this.parameters.add(parameters_Builder);
            return parameters_Builder;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public PrintoutOutputFormat.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "outputFormatId" (any previous value will be replaced)
         * 
         * @param outputFormatId
         *     New value of the "outputFormatId" property.
         */
        public PrintoutOutputFormat.Builder<_B> withOutputFormatId(final String outputFormatId) {
            this.outputFormatId = outputFormatId;
            return this;
        }

        /**
         * Sets the new value of "selected" (any previous value will be replaced)
         * 
         * @param selected
         *     New value of the "selected" property.
         */
        public PrintoutOutputFormat.Builder<_B> withSelected(final Boolean selected) {
            this.selected = selected;
            return this;
        }

        /**
         * Sets the new value of "fileName" (any previous value will be replaced)
         * 
         * @param fileName
         *     New value of the "fileName" property.
         */
        public PrintoutOutputFormat.Builder<_B> withFileName(final String fileName) {
            this.fileName = fileName;
            return this;
        }

        @Override
        public PrintoutOutputFormat build() {
            if (_storedValue == null) {
                return this.init(new PrintoutOutputFormat());
            } else {
                return ((PrintoutOutputFormat) _storedValue);
            }
        }

        public PrintoutOutputFormat.Builder<_B> copyOf(final PrintoutOutputFormat _other) {
            _other.copyTo(this);
            return this;
        }

        public PrintoutOutputFormat.Builder<_B> copyOf(final PrintoutOutputFormat.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PrintoutOutputFormat.Selector<PrintoutOutputFormat.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PrintoutOutputFormat.Select _root() {
            return new PrintoutOutputFormat.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private PrintoutOutputFormatParameter.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> parameters = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> outputFormatId = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> selected = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> fileName = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.parameters!= null) {
                products.put("parameters", this.parameters.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.outputFormatId!= null) {
                products.put("outputFormatId", this.outputFormatId.init());
            }
            if (this.selected!= null) {
                products.put("selected", this.selected.init());
            }
            if (this.fileName!= null) {
                products.put("fileName", this.fileName.init());
            }
            return products;
        }

        public PrintoutOutputFormatParameter.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> parameters() {
            return ((this.parameters == null)?this.parameters = new PrintoutOutputFormatParameter.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>>(this._root, this, "parameters"):this.parameters);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> outputFormatId() {
            return ((this.outputFormatId == null)?this.outputFormatId = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>>(this._root, this, "outputFormatId"):this.outputFormatId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> selected() {
            return ((this.selected == null)?this.selected = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>>(this._root, this, "selected"):this.selected);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>> fileName() {
            return ((this.fileName == null)?this.fileName = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormat.Selector<TRoot, TParent>>(this._root, this, "fileName"):this.fileName);
        }

    }

}
