package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for printout-output-format-parameter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="printout-output-format-parameter"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="vlp" type="{urn:org.nuclos.schema.rest}vlp" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="parameter" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="nullable" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "printout-output-format-parameter", propOrder = {
    "vlp"
})
public class PrintoutOutputFormatParameter implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    protected Vlp vlp;
    @XmlAttribute(name = "parameter")
    protected String parameter;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "value")
    @XmlSchemaType(name = "anySimpleType")
    protected String value;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "nullable")
    protected Boolean nullable;

    /**
     * Gets the value of the vlp property.
     * 
     * @return
     *     possible object is
     *     {@link Vlp }
     *     
     */
    public Vlp getVlp() {
        return vlp;
    }

    /**
     * Sets the value of the vlp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vlp }
     *     
     */
    public void setVlp(Vlp value) {
        this.vlp = value;
    }

    /**
     * Gets the value of the parameter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParameter() {
        return parameter;
    }

    /**
     * Sets the value of the parameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParameter(String value) {
        this.parameter = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the nullable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNullable() {
        return nullable;
    }

    /**
     * Sets the value of the nullable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNullable(Boolean value) {
        this.nullable = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Vlp theVlp;
            theVlp = this.getVlp();
            strategy.appendField(locator, this, "vlp", buffer, theVlp);
        }
        {
            String theParameter;
            theParameter = this.getParameter();
            strategy.appendField(locator, this, "parameter", buffer, theParameter);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theValue;
            theValue = this.getValue();
            strategy.appendField(locator, this, "value", buffer, theValue);
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            Boolean theNullable;
            theNullable = this.isNullable();
            strategy.appendField(locator, this, "nullable", buffer, theNullable);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof PrintoutOutputFormatParameter)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PrintoutOutputFormatParameter that = ((PrintoutOutputFormatParameter) object);
        {
            Vlp lhsVlp;
            lhsVlp = this.getVlp();
            Vlp rhsVlp;
            rhsVlp = that.getVlp();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vlp", lhsVlp), LocatorUtils.property(thatLocator, "vlp", rhsVlp), lhsVlp, rhsVlp)) {
                return false;
            }
        }
        {
            String lhsParameter;
            lhsParameter = this.getParameter();
            String rhsParameter;
            rhsParameter = that.getParameter();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "parameter", lhsParameter), LocatorUtils.property(thatLocator, "parameter", rhsParameter), lhsParameter, rhsParameter)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsValue;
            lhsValue = this.getValue();
            String rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            Boolean lhsNullable;
            lhsNullable = this.isNullable();
            Boolean rhsNullable;
            rhsNullable = that.isNullable();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nullable", lhsNullable), LocatorUtils.property(thatLocator, "nullable", rhsNullable), lhsNullable, rhsNullable)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Vlp theVlp;
            theVlp = this.getVlp();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vlp", theVlp), currentHashCode, theVlp);
        }
        {
            String theParameter;
            theParameter = this.getParameter();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "parameter", theParameter), currentHashCode, theParameter);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theValue;
            theValue = this.getValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            Boolean theNullable;
            theNullable = this.isNullable();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nullable", theNullable), currentHashCode, theNullable);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof PrintoutOutputFormatParameter) {
            final PrintoutOutputFormatParameter copy = ((PrintoutOutputFormatParameter) draftCopy);
            if (this.vlp!= null) {
                Vlp sourceVlp;
                sourceVlp = this.getVlp();
                Vlp copyVlp = ((Vlp) strategy.copy(LocatorUtils.property(locator, "vlp", sourceVlp), sourceVlp));
                copy.setVlp(copyVlp);
            } else {
                copy.vlp = null;
            }
            if (this.parameter!= null) {
                String sourceParameter;
                sourceParameter = this.getParameter();
                String copyParameter = ((String) strategy.copy(LocatorUtils.property(locator, "parameter", sourceParameter), sourceParameter));
                copy.setParameter(copyParameter);
            } else {
                copy.parameter = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.value!= null) {
                String sourceValue;
                sourceValue = this.getValue();
                String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            if (this.nullable!= null) {
                Boolean sourceNullable;
                sourceNullable = this.isNullable();
                Boolean copyNullable = ((Boolean) strategy.copy(LocatorUtils.property(locator, "nullable", sourceNullable), sourceNullable));
                copy.setNullable(copyNullable);
            } else {
                copy.nullable = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new PrintoutOutputFormatParameter();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PrintoutOutputFormatParameter.Builder<_B> _other) {
        _other.vlp = ((this.vlp == null)?null:this.vlp.newCopyBuilder(_other));
        _other.parameter = this.parameter;
        _other.name = this.name;
        _other.value = this.value;
        _other.type = this.type;
        _other.nullable = this.nullable;
    }

    public<_B >PrintoutOutputFormatParameter.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new PrintoutOutputFormatParameter.Builder<_B>(_parentBuilder, this, true);
    }

    public PrintoutOutputFormatParameter.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static PrintoutOutputFormatParameter.Builder<Void> builder() {
        return new PrintoutOutputFormatParameter.Builder<Void>(null, null, false);
    }

    public static<_B >PrintoutOutputFormatParameter.Builder<_B> copyOf(final PrintoutOutputFormatParameter _other) {
        final PrintoutOutputFormatParameter.Builder<_B> _newBuilder = new PrintoutOutputFormatParameter.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final PrintoutOutputFormatParameter.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree vlpPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlp"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpPropertyTree!= null):((vlpPropertyTree == null)||(!vlpPropertyTree.isLeaf())))) {
            _other.vlp = ((this.vlp == null)?null:this.vlp.newCopyBuilder(_other, vlpPropertyTree, _propertyTreeUse));
        }
        final PropertyTree parameterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameter"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parameterPropertyTree!= null):((parameterPropertyTree == null)||(!parameterPropertyTree.isLeaf())))) {
            _other.parameter = this.parameter;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
            _other.value = this.value;
        }
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree nullablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nullable"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nullablePropertyTree!= null):((nullablePropertyTree == null)||(!nullablePropertyTree.isLeaf())))) {
            _other.nullable = this.nullable;
        }
    }

    public<_B >PrintoutOutputFormatParameter.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new PrintoutOutputFormatParameter.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public PrintoutOutputFormatParameter.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >PrintoutOutputFormatParameter.Builder<_B> copyOf(final PrintoutOutputFormatParameter _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PrintoutOutputFormatParameter.Builder<_B> _newBuilder = new PrintoutOutputFormatParameter.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static PrintoutOutputFormatParameter.Builder<Void> copyExcept(final PrintoutOutputFormatParameter _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static PrintoutOutputFormatParameter.Builder<Void> copyOnly(final PrintoutOutputFormatParameter _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final PrintoutOutputFormatParameter _storedValue;
        private Vlp.Builder<PrintoutOutputFormatParameter.Builder<_B>> vlp;
        private String parameter;
        private String name;
        private String value;
        private String type;
        private Boolean nullable;

        public Builder(final _B _parentBuilder, final PrintoutOutputFormatParameter _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.vlp = ((_other.vlp == null)?null:_other.vlp.newCopyBuilder(this));
                    this.parameter = _other.parameter;
                    this.name = _other.name;
                    this.value = _other.value;
                    this.type = _other.type;
                    this.nullable = _other.nullable;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final PrintoutOutputFormatParameter _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree vlpPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("vlp"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(vlpPropertyTree!= null):((vlpPropertyTree == null)||(!vlpPropertyTree.isLeaf())))) {
                        this.vlp = ((_other.vlp == null)?null:_other.vlp.newCopyBuilder(this, vlpPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree parameterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("parameter"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(parameterPropertyTree!= null):((parameterPropertyTree == null)||(!parameterPropertyTree.isLeaf())))) {
                        this.parameter = _other.parameter;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                        this.value = _other.value;
                    }
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree nullablePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nullable"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nullablePropertyTree!= null):((nullablePropertyTree == null)||(!nullablePropertyTree.isLeaf())))) {
                        this.nullable = _other.nullable;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends PrintoutOutputFormatParameter >_P init(final _P _product) {
            _product.vlp = ((this.vlp == null)?null:this.vlp.build());
            _product.parameter = this.parameter;
            _product.name = this.name;
            _product.value = this.value;
            _product.type = this.type;
            _product.nullable = this.nullable;
            return _product;
        }

        /**
         * Sets the new value of "vlp" (any previous value will be replaced)
         * 
         * @param vlp
         *     New value of the "vlp" property.
         */
        public PrintoutOutputFormatParameter.Builder<_B> withVlp(final Vlp vlp) {
            this.vlp = ((vlp == null)?null:new Vlp.Builder<PrintoutOutputFormatParameter.Builder<_B>>(this, vlp, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "vlp" property.
         * Use {@link org.nuclos.schema.rest.Vlp.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "vlp" property.
         *     Use {@link org.nuclos.schema.rest.Vlp.Builder#end()} to return to the current builder.
         */
        public Vlp.Builder<? extends PrintoutOutputFormatParameter.Builder<_B>> withVlp() {
            if (this.vlp!= null) {
                return this.vlp;
            }
            return this.vlp = new Vlp.Builder<PrintoutOutputFormatParameter.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "parameter" (any previous value will be replaced)
         * 
         * @param parameter
         *     New value of the "parameter" property.
         */
        public PrintoutOutputFormatParameter.Builder<_B> withParameter(final String parameter) {
            this.parameter = parameter;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public PrintoutOutputFormatParameter.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "value" (any previous value will be replaced)
         * 
         * @param value
         *     New value of the "value" property.
         */
        public PrintoutOutputFormatParameter.Builder<_B> withValue(final String value) {
            this.value = value;
            return this;
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public PrintoutOutputFormatParameter.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "nullable" (any previous value will be replaced)
         * 
         * @param nullable
         *     New value of the "nullable" property.
         */
        public PrintoutOutputFormatParameter.Builder<_B> withNullable(final Boolean nullable) {
            this.nullable = nullable;
            return this;
        }

        @Override
        public PrintoutOutputFormatParameter build() {
            if (_storedValue == null) {
                return this.init(new PrintoutOutputFormatParameter());
            } else {
                return ((PrintoutOutputFormatParameter) _storedValue);
            }
        }

        public PrintoutOutputFormatParameter.Builder<_B> copyOf(final PrintoutOutputFormatParameter _other) {
            _other.copyTo(this);
            return this;
        }

        public PrintoutOutputFormatParameter.Builder<_B> copyOf(final PrintoutOutputFormatParameter.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends PrintoutOutputFormatParameter.Selector<PrintoutOutputFormatParameter.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static PrintoutOutputFormatParameter.Select _root() {
            return new PrintoutOutputFormatParameter.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private Vlp.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> vlp = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> parameter = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> value = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> nullable = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.vlp!= null) {
                products.put("vlp", this.vlp.init());
            }
            if (this.parameter!= null) {
                products.put("parameter", this.parameter.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.value!= null) {
                products.put("value", this.value.init());
            }
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.nullable!= null) {
                products.put("nullable", this.nullable.init());
            }
            return products;
        }

        public Vlp.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> vlp() {
            return ((this.vlp == null)?this.vlp = new Vlp.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>>(this._root, this, "vlp"):this.vlp);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> parameter() {
            return ((this.parameter == null)?this.parameter = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>>(this._root, this, "parameter"):this.parameter);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> value() {
            return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>> nullable() {
            return ((this.nullable == null)?this.nullable = new com.kscs.util.jaxb.Selector<TRoot, PrintoutOutputFormatParameter.Selector<TRoot, TParent>>(this._root, this, "nullable"):this.nullable);
        }

    }

}
