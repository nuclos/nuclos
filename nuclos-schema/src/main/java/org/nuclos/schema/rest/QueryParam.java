package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for query-param complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="query-param"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="param" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "query-param", propOrder = {
    "param",
    "value"
})
public class QueryParam implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String param;
    @XmlElement(required = true)
    protected String value;

    /**
     * Gets the value of the param property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParam() {
        return param;
    }

    /**
     * Sets the value of the param property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParam(String value) {
        this.param = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theParam;
            theParam = this.getParam();
            strategy.appendField(locator, this, "param", buffer, theParam);
        }
        {
            String theValue;
            theValue = this.getValue();
            strategy.appendField(locator, this, "value", buffer, theValue);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof QueryParam)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QueryParam that = ((QueryParam) object);
        {
            String lhsParam;
            lhsParam = this.getParam();
            String rhsParam;
            rhsParam = that.getParam();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "param", lhsParam), LocatorUtils.property(thatLocator, "param", rhsParam), lhsParam, rhsParam)) {
                return false;
            }
        }
        {
            String lhsValue;
            lhsValue = this.getValue();
            String rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theParam;
            theParam = this.getParam();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "param", theParam), currentHashCode, theParam);
        }
        {
            String theValue;
            theValue = this.getValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof QueryParam) {
            final QueryParam copy = ((QueryParam) draftCopy);
            if (this.param!= null) {
                String sourceParam;
                sourceParam = this.getParam();
                String copyParam = ((String) strategy.copy(LocatorUtils.property(locator, "param", sourceParam), sourceParam));
                copy.setParam(copyParam);
            } else {
                copy.param = null;
            }
            if (this.value!= null) {
                String sourceValue;
                sourceValue = this.getValue();
                String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new QueryParam();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final QueryParam.Builder<_B> _other) {
        _other.param = this.param;
        _other.value = this.value;
    }

    public<_B >QueryParam.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new QueryParam.Builder<_B>(_parentBuilder, this, true);
    }

    public QueryParam.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static QueryParam.Builder<Void> builder() {
        return new QueryParam.Builder<Void>(null, null, false);
    }

    public static<_B >QueryParam.Builder<_B> copyOf(final QueryParam _other) {
        final QueryParam.Builder<_B> _newBuilder = new QueryParam.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final QueryParam.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree paramPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("param"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(paramPropertyTree!= null):((paramPropertyTree == null)||(!paramPropertyTree.isLeaf())))) {
            _other.param = this.param;
        }
        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
            _other.value = this.value;
        }
    }

    public<_B >QueryParam.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new QueryParam.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public QueryParam.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >QueryParam.Builder<_B> copyOf(final QueryParam _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final QueryParam.Builder<_B> _newBuilder = new QueryParam.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static QueryParam.Builder<Void> copyExcept(final QueryParam _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static QueryParam.Builder<Void> copyOnly(final QueryParam _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final QueryParam _storedValue;
        private String param;
        private String value;

        public Builder(final _B _parentBuilder, final QueryParam _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.param = _other.param;
                    this.value = _other.value;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final QueryParam _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree paramPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("param"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(paramPropertyTree!= null):((paramPropertyTree == null)||(!paramPropertyTree.isLeaf())))) {
                        this.param = _other.param;
                    }
                    final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                        this.value = _other.value;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends QueryParam >_P init(final _P _product) {
            _product.param = this.param;
            _product.value = this.value;
            return _product;
        }

        /**
         * Sets the new value of "param" (any previous value will be replaced)
         * 
         * @param param
         *     New value of the "param" property.
         */
        public QueryParam.Builder<_B> withParam(final String param) {
            this.param = param;
            return this;
        }

        /**
         * Sets the new value of "value" (any previous value will be replaced)
         * 
         * @param value
         *     New value of the "value" property.
         */
        public QueryParam.Builder<_B> withValue(final String value) {
            this.value = value;
            return this;
        }

        @Override
        public QueryParam build() {
            if (_storedValue == null) {
                return this.init(new QueryParam());
            } else {
                return ((QueryParam) _storedValue);
            }
        }

        public QueryParam.Builder<_B> copyOf(final QueryParam _other) {
            _other.copyTo(this);
            return this;
        }

        public QueryParam.Builder<_B> copyOf(final QueryParam.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends QueryParam.Selector<QueryParam.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static QueryParam.Select _root() {
            return new QueryParam.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, QueryParam.Selector<TRoot, TParent>> param = null;
        private com.kscs.util.jaxb.Selector<TRoot, QueryParam.Selector<TRoot, TParent>> value = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.param!= null) {
                products.put("param", this.param.init());
            }
            if (this.value!= null) {
                products.put("value", this.value.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryParam.Selector<TRoot, TParent>> param() {
            return ((this.param == null)?this.param = new com.kscs.util.jaxb.Selector<TRoot, QueryParam.Selector<TRoot, TParent>>(this._root, this, "param"):this.param);
        }

        public com.kscs.util.jaxb.Selector<TRoot, QueryParam.Selector<TRoot, TParent>> value() {
            return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, QueryParam.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
        }

    }

}
