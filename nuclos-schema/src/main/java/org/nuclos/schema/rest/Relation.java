package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for relation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="relation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="entityMeta" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="from" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="to" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="startDecoration" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="endDecoration" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="presentationMode" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="color" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "relation")
public class Relation implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "entityMeta", required = true)
    protected String entityMeta;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "from", required = true)
    protected String from;
    @XmlAttribute(name = "to", required = true)
    protected String to;
    @XmlAttribute(name = "startDecoration", required = true)
    protected String startDecoration;
    @XmlAttribute(name = "endDecoration", required = true)
    protected String endDecoration;
    @XmlAttribute(name = "presentationMode", required = true)
    protected String presentationMode;
    @XmlAttribute(name = "color", required = true)
    protected String color;

    /**
     * Gets the value of the entityMeta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityMeta() {
        return entityMeta;
    }

    /**
     * Sets the value of the entityMeta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityMeta(String value) {
        this.entityMeta = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the from property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrom() {
        return from;
    }

    /**
     * Sets the value of the from property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrom(String value) {
        this.from = value;
    }

    /**
     * Gets the value of the to property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTo() {
        return to;
    }

    /**
     * Sets the value of the to property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTo(String value) {
        this.to = value;
    }

    /**
     * Gets the value of the startDecoration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartDecoration() {
        return startDecoration;
    }

    /**
     * Sets the value of the startDecoration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartDecoration(String value) {
        this.startDecoration = value;
    }

    /**
     * Gets the value of the endDecoration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndDecoration() {
        return endDecoration;
    }

    /**
     * Sets the value of the endDecoration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndDecoration(String value) {
        this.endDecoration = value;
    }

    /**
     * Gets the value of the presentationMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPresentationMode() {
        return presentationMode;
    }

    /**
     * Sets the value of the presentationMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPresentationMode(String value) {
        this.presentationMode = value;
    }

    /**
     * Gets the value of the color property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the value of the color property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theEntityMeta;
            theEntityMeta = this.getEntityMeta();
            strategy.appendField(locator, this, "entityMeta", buffer, theEntityMeta);
        }
        {
            String theId;
            theId = this.getId();
            strategy.appendField(locator, this, "id", buffer, theId);
        }
        {
            String theFrom;
            theFrom = this.getFrom();
            strategy.appendField(locator, this, "from", buffer, theFrom);
        }
        {
            String theTo;
            theTo = this.getTo();
            strategy.appendField(locator, this, "to", buffer, theTo);
        }
        {
            String theStartDecoration;
            theStartDecoration = this.getStartDecoration();
            strategy.appendField(locator, this, "startDecoration", buffer, theStartDecoration);
        }
        {
            String theEndDecoration;
            theEndDecoration = this.getEndDecoration();
            strategy.appendField(locator, this, "endDecoration", buffer, theEndDecoration);
        }
        {
            String thePresentationMode;
            thePresentationMode = this.getPresentationMode();
            strategy.appendField(locator, this, "presentationMode", buffer, thePresentationMode);
        }
        {
            String theColor;
            theColor = this.getColor();
            strategy.appendField(locator, this, "color", buffer, theColor);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Relation)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Relation that = ((Relation) object);
        {
            String lhsEntityMeta;
            lhsEntityMeta = this.getEntityMeta();
            String rhsEntityMeta;
            rhsEntityMeta = that.getEntityMeta();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entityMeta", lhsEntityMeta), LocatorUtils.property(thatLocator, "entityMeta", rhsEntityMeta), lhsEntityMeta, rhsEntityMeta)) {
                return false;
            }
        }
        {
            String lhsId;
            lhsId = this.getId();
            String rhsId;
            rhsId = that.getId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId)) {
                return false;
            }
        }
        {
            String lhsFrom;
            lhsFrom = this.getFrom();
            String rhsFrom;
            rhsFrom = that.getFrom();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "from", lhsFrom), LocatorUtils.property(thatLocator, "from", rhsFrom), lhsFrom, rhsFrom)) {
                return false;
            }
        }
        {
            String lhsTo;
            lhsTo = this.getTo();
            String rhsTo;
            rhsTo = that.getTo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "to", lhsTo), LocatorUtils.property(thatLocator, "to", rhsTo), lhsTo, rhsTo)) {
                return false;
            }
        }
        {
            String lhsStartDecoration;
            lhsStartDecoration = this.getStartDecoration();
            String rhsStartDecoration;
            rhsStartDecoration = that.getStartDecoration();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "startDecoration", lhsStartDecoration), LocatorUtils.property(thatLocator, "startDecoration", rhsStartDecoration), lhsStartDecoration, rhsStartDecoration)) {
                return false;
            }
        }
        {
            String lhsEndDecoration;
            lhsEndDecoration = this.getEndDecoration();
            String rhsEndDecoration;
            rhsEndDecoration = that.getEndDecoration();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "endDecoration", lhsEndDecoration), LocatorUtils.property(thatLocator, "endDecoration", rhsEndDecoration), lhsEndDecoration, rhsEndDecoration)) {
                return false;
            }
        }
        {
            String lhsPresentationMode;
            lhsPresentationMode = this.getPresentationMode();
            String rhsPresentationMode;
            rhsPresentationMode = that.getPresentationMode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "presentationMode", lhsPresentationMode), LocatorUtils.property(thatLocator, "presentationMode", rhsPresentationMode), lhsPresentationMode, rhsPresentationMode)) {
                return false;
            }
        }
        {
            String lhsColor;
            lhsColor = this.getColor();
            String rhsColor;
            rhsColor = that.getColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "color", lhsColor), LocatorUtils.property(thatLocator, "color", rhsColor), lhsColor, rhsColor)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEntityMeta;
            theEntityMeta = this.getEntityMeta();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entityMeta", theEntityMeta), currentHashCode, theEntityMeta);
        }
        {
            String theId;
            theId = this.getId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId);
        }
        {
            String theFrom;
            theFrom = this.getFrom();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "from", theFrom), currentHashCode, theFrom);
        }
        {
            String theTo;
            theTo = this.getTo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "to", theTo), currentHashCode, theTo);
        }
        {
            String theStartDecoration;
            theStartDecoration = this.getStartDecoration();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "startDecoration", theStartDecoration), currentHashCode, theStartDecoration);
        }
        {
            String theEndDecoration;
            theEndDecoration = this.getEndDecoration();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "endDecoration", theEndDecoration), currentHashCode, theEndDecoration);
        }
        {
            String thePresentationMode;
            thePresentationMode = this.getPresentationMode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "presentationMode", thePresentationMode), currentHashCode, thePresentationMode);
        }
        {
            String theColor;
            theColor = this.getColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "color", theColor), currentHashCode, theColor);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Relation) {
            final Relation copy = ((Relation) draftCopy);
            if (this.entityMeta!= null) {
                String sourceEntityMeta;
                sourceEntityMeta = this.getEntityMeta();
                String copyEntityMeta = ((String) strategy.copy(LocatorUtils.property(locator, "entityMeta", sourceEntityMeta), sourceEntityMeta));
                copy.setEntityMeta(copyEntityMeta);
            } else {
                copy.entityMeta = null;
            }
            if (this.id!= null) {
                String sourceId;
                sourceId = this.getId();
                String copyId = ((String) strategy.copy(LocatorUtils.property(locator, "id", sourceId), sourceId));
                copy.setId(copyId);
            } else {
                copy.id = null;
            }
            if (this.from!= null) {
                String sourceFrom;
                sourceFrom = this.getFrom();
                String copyFrom = ((String) strategy.copy(LocatorUtils.property(locator, "from", sourceFrom), sourceFrom));
                copy.setFrom(copyFrom);
            } else {
                copy.from = null;
            }
            if (this.to!= null) {
                String sourceTo;
                sourceTo = this.getTo();
                String copyTo = ((String) strategy.copy(LocatorUtils.property(locator, "to", sourceTo), sourceTo));
                copy.setTo(copyTo);
            } else {
                copy.to = null;
            }
            if (this.startDecoration!= null) {
                String sourceStartDecoration;
                sourceStartDecoration = this.getStartDecoration();
                String copyStartDecoration = ((String) strategy.copy(LocatorUtils.property(locator, "startDecoration", sourceStartDecoration), sourceStartDecoration));
                copy.setStartDecoration(copyStartDecoration);
            } else {
                copy.startDecoration = null;
            }
            if (this.endDecoration!= null) {
                String sourceEndDecoration;
                sourceEndDecoration = this.getEndDecoration();
                String copyEndDecoration = ((String) strategy.copy(LocatorUtils.property(locator, "endDecoration", sourceEndDecoration), sourceEndDecoration));
                copy.setEndDecoration(copyEndDecoration);
            } else {
                copy.endDecoration = null;
            }
            if (this.presentationMode!= null) {
                String sourcePresentationMode;
                sourcePresentationMode = this.getPresentationMode();
                String copyPresentationMode = ((String) strategy.copy(LocatorUtils.property(locator, "presentationMode", sourcePresentationMode), sourcePresentationMode));
                copy.setPresentationMode(copyPresentationMode);
            } else {
                copy.presentationMode = null;
            }
            if (this.color!= null) {
                String sourceColor;
                sourceColor = this.getColor();
                String copyColor = ((String) strategy.copy(LocatorUtils.property(locator, "color", sourceColor), sourceColor));
                copy.setColor(copyColor);
            } else {
                copy.color = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Relation();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Relation.Builder<_B> _other) {
        _other.entityMeta = this.entityMeta;
        _other.id = this.id;
        _other.from = this.from;
        _other.to = this.to;
        _other.startDecoration = this.startDecoration;
        _other.endDecoration = this.endDecoration;
        _other.presentationMode = this.presentationMode;
        _other.color = this.color;
    }

    public<_B >Relation.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Relation.Builder<_B>(_parentBuilder, this, true);
    }

    public Relation.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Relation.Builder<Void> builder() {
        return new Relation.Builder<Void>(null, null, false);
    }

    public static<_B >Relation.Builder<_B> copyOf(final Relation _other) {
        final Relation.Builder<_B> _newBuilder = new Relation.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Relation.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree entityMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMeta"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMetaPropertyTree!= null):((entityMetaPropertyTree == null)||(!entityMetaPropertyTree.isLeaf())))) {
            _other.entityMeta = this.entityMeta;
        }
        final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
            _other.id = this.id;
        }
        final PropertyTree fromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("from"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromPropertyTree!= null):((fromPropertyTree == null)||(!fromPropertyTree.isLeaf())))) {
            _other.from = this.from;
        }
        final PropertyTree toPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("to"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toPropertyTree!= null):((toPropertyTree == null)||(!toPropertyTree.isLeaf())))) {
            _other.to = this.to;
        }
        final PropertyTree startDecorationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("startDecoration"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(startDecorationPropertyTree!= null):((startDecorationPropertyTree == null)||(!startDecorationPropertyTree.isLeaf())))) {
            _other.startDecoration = this.startDecoration;
        }
        final PropertyTree endDecorationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("endDecoration"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(endDecorationPropertyTree!= null):((endDecorationPropertyTree == null)||(!endDecorationPropertyTree.isLeaf())))) {
            _other.endDecoration = this.endDecoration;
        }
        final PropertyTree presentationModePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("presentationMode"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(presentationModePropertyTree!= null):((presentationModePropertyTree == null)||(!presentationModePropertyTree.isLeaf())))) {
            _other.presentationMode = this.presentationMode;
        }
        final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
            _other.color = this.color;
        }
    }

    public<_B >Relation.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Relation.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Relation.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Relation.Builder<_B> copyOf(final Relation _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Relation.Builder<_B> _newBuilder = new Relation.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Relation.Builder<Void> copyExcept(final Relation _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Relation.Builder<Void> copyOnly(final Relation _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Relation _storedValue;
        private String entityMeta;
        private String id;
        private String from;
        private String to;
        private String startDecoration;
        private String endDecoration;
        private String presentationMode;
        private String color;

        public Builder(final _B _parentBuilder, final Relation _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.entityMeta = _other.entityMeta;
                    this.id = _other.id;
                    this.from = _other.from;
                    this.to = _other.to;
                    this.startDecoration = _other.startDecoration;
                    this.endDecoration = _other.endDecoration;
                    this.presentationMode = _other.presentationMode;
                    this.color = _other.color;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Relation _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree entityMetaPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entityMeta"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityMetaPropertyTree!= null):((entityMetaPropertyTree == null)||(!entityMetaPropertyTree.isLeaf())))) {
                        this.entityMeta = _other.entityMeta;
                    }
                    final PropertyTree idPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("id"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idPropertyTree!= null):((idPropertyTree == null)||(!idPropertyTree.isLeaf())))) {
                        this.id = _other.id;
                    }
                    final PropertyTree fromPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("from"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromPropertyTree!= null):((fromPropertyTree == null)||(!fromPropertyTree.isLeaf())))) {
                        this.from = _other.from;
                    }
                    final PropertyTree toPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("to"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toPropertyTree!= null):((toPropertyTree == null)||(!toPropertyTree.isLeaf())))) {
                        this.to = _other.to;
                    }
                    final PropertyTree startDecorationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("startDecoration"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(startDecorationPropertyTree!= null):((startDecorationPropertyTree == null)||(!startDecorationPropertyTree.isLeaf())))) {
                        this.startDecoration = _other.startDecoration;
                    }
                    final PropertyTree endDecorationPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("endDecoration"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(endDecorationPropertyTree!= null):((endDecorationPropertyTree == null)||(!endDecorationPropertyTree.isLeaf())))) {
                        this.endDecoration = _other.endDecoration;
                    }
                    final PropertyTree presentationModePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("presentationMode"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(presentationModePropertyTree!= null):((presentationModePropertyTree == null)||(!presentationModePropertyTree.isLeaf())))) {
                        this.presentationMode = _other.presentationMode;
                    }
                    final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
                        this.color = _other.color;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Relation >_P init(final _P _product) {
            _product.entityMeta = this.entityMeta;
            _product.id = this.id;
            _product.from = this.from;
            _product.to = this.to;
            _product.startDecoration = this.startDecoration;
            _product.endDecoration = this.endDecoration;
            _product.presentationMode = this.presentationMode;
            _product.color = this.color;
            return _product;
        }

        /**
         * Sets the new value of "entityMeta" (any previous value will be replaced)
         * 
         * @param entityMeta
         *     New value of the "entityMeta" property.
         */
        public Relation.Builder<_B> withEntityMeta(final String entityMeta) {
            this.entityMeta = entityMeta;
            return this;
        }

        /**
         * Sets the new value of "id" (any previous value will be replaced)
         * 
         * @param id
         *     New value of the "id" property.
         */
        public Relation.Builder<_B> withId(final String id) {
            this.id = id;
            return this;
        }

        /**
         * Sets the new value of "from" (any previous value will be replaced)
         * 
         * @param from
         *     New value of the "from" property.
         */
        public Relation.Builder<_B> withFrom(final String from) {
            this.from = from;
            return this;
        }

        /**
         * Sets the new value of "to" (any previous value will be replaced)
         * 
         * @param to
         *     New value of the "to" property.
         */
        public Relation.Builder<_B> withTo(final String to) {
            this.to = to;
            return this;
        }

        /**
         * Sets the new value of "startDecoration" (any previous value will be replaced)
         * 
         * @param startDecoration
         *     New value of the "startDecoration" property.
         */
        public Relation.Builder<_B> withStartDecoration(final String startDecoration) {
            this.startDecoration = startDecoration;
            return this;
        }

        /**
         * Sets the new value of "endDecoration" (any previous value will be replaced)
         * 
         * @param endDecoration
         *     New value of the "endDecoration" property.
         */
        public Relation.Builder<_B> withEndDecoration(final String endDecoration) {
            this.endDecoration = endDecoration;
            return this;
        }

        /**
         * Sets the new value of "presentationMode" (any previous value will be replaced)
         * 
         * @param presentationMode
         *     New value of the "presentationMode" property.
         */
        public Relation.Builder<_B> withPresentationMode(final String presentationMode) {
            this.presentationMode = presentationMode;
            return this;
        }

        /**
         * Sets the new value of "color" (any previous value will be replaced)
         * 
         * @param color
         *     New value of the "color" property.
         */
        public Relation.Builder<_B> withColor(final String color) {
            this.color = color;
            return this;
        }

        @Override
        public Relation build() {
            if (_storedValue == null) {
                return this.init(new Relation());
            } else {
                return ((Relation) _storedValue);
            }
        }

        public Relation.Builder<_B> copyOf(final Relation _other) {
            _other.copyTo(this);
            return this;
        }

        public Relation.Builder<_B> copyOf(final Relation.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Relation.Selector<Relation.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Relation.Select _root() {
            return new Relation.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> entityMeta = null;
        private com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> id = null;
        private com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> from = null;
        private com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> to = null;
        private com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> startDecoration = null;
        private com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> endDecoration = null;
        private com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> presentationMode = null;
        private com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> color = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.entityMeta!= null) {
                products.put("entityMeta", this.entityMeta.init());
            }
            if (this.id!= null) {
                products.put("id", this.id.init());
            }
            if (this.from!= null) {
                products.put("from", this.from.init());
            }
            if (this.to!= null) {
                products.put("to", this.to.init());
            }
            if (this.startDecoration!= null) {
                products.put("startDecoration", this.startDecoration.init());
            }
            if (this.endDecoration!= null) {
                products.put("endDecoration", this.endDecoration.init());
            }
            if (this.presentationMode!= null) {
                products.put("presentationMode", this.presentationMode.init());
            }
            if (this.color!= null) {
                products.put("color", this.color.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> entityMeta() {
            return ((this.entityMeta == null)?this.entityMeta = new com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>>(this._root, this, "entityMeta"):this.entityMeta);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> id() {
            return ((this.id == null)?this.id = new com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>>(this._root, this, "id"):this.id);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> from() {
            return ((this.from == null)?this.from = new com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>>(this._root, this, "from"):this.from);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> to() {
            return ((this.to == null)?this.to = new com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>>(this._root, this, "to"):this.to);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> startDecoration() {
            return ((this.startDecoration == null)?this.startDecoration = new com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>>(this._root, this, "startDecoration"):this.startDecoration);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> endDecoration() {
            return ((this.endDecoration == null)?this.endDecoration = new com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>>(this._root, this, "endDecoration"):this.endDecoration);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> presentationMode() {
            return ((this.presentationMode == null)?this.presentationMode = new com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>>(this._root, this, "presentationMode"):this.presentationMode);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>> color() {
            return ((this.color == null)?this.color = new com.kscs.util.jaxb.Selector<TRoot, Relation.Selector<TRoot, TParent>>(this._root, this, "color"):this.color);
        }

    }

}
