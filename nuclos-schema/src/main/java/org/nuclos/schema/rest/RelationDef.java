package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for relation-def complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="relation-def"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="entity" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="from-field" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="to-field" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="from-line-cap" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="to-line-cap" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="from-icon" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="to-icon" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="color" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "relation-def")
public class RelationDef implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "entity", required = true)
    protected String entity;
    @XmlAttribute(name = "from-field", required = true)
    protected String fromField;
    @XmlAttribute(name = "to-field", required = true)
    protected String toField;
    @XmlAttribute(name = "from-line-cap", required = true)
    protected String fromLineCap;
    @XmlAttribute(name = "to-line-cap", required = true)
    protected String toLineCap;
    @XmlAttribute(name = "from-icon", required = true)
    protected String fromIcon;
    @XmlAttribute(name = "to-icon", required = true)
    protected String toIcon;
    @XmlAttribute(name = "color", required = true)
    protected String color;

    /**
     * Gets the value of the entity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntity(String value) {
        this.entity = value;
    }

    /**
     * Gets the value of the fromField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromField() {
        return fromField;
    }

    /**
     * Sets the value of the fromField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromField(String value) {
        this.fromField = value;
    }

    /**
     * Gets the value of the toField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToField() {
        return toField;
    }

    /**
     * Sets the value of the toField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToField(String value) {
        this.toField = value;
    }

    /**
     * Gets the value of the fromLineCap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromLineCap() {
        return fromLineCap;
    }

    /**
     * Sets the value of the fromLineCap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromLineCap(String value) {
        this.fromLineCap = value;
    }

    /**
     * Gets the value of the toLineCap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToLineCap() {
        return toLineCap;
    }

    /**
     * Sets the value of the toLineCap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToLineCap(String value) {
        this.toLineCap = value;
    }

    /**
     * Gets the value of the fromIcon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromIcon() {
        return fromIcon;
    }

    /**
     * Sets the value of the fromIcon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromIcon(String value) {
        this.fromIcon = value;
    }

    /**
     * Gets the value of the toIcon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToIcon() {
        return toIcon;
    }

    /**
     * Sets the value of the toIcon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToIcon(String value) {
        this.toIcon = value;
    }

    /**
     * Gets the value of the color property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the value of the color property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theEntity;
            theEntity = this.getEntity();
            strategy.appendField(locator, this, "entity", buffer, theEntity);
        }
        {
            String theFromField;
            theFromField = this.getFromField();
            strategy.appendField(locator, this, "fromField", buffer, theFromField);
        }
        {
            String theToField;
            theToField = this.getToField();
            strategy.appendField(locator, this, "toField", buffer, theToField);
        }
        {
            String theFromLineCap;
            theFromLineCap = this.getFromLineCap();
            strategy.appendField(locator, this, "fromLineCap", buffer, theFromLineCap);
        }
        {
            String theToLineCap;
            theToLineCap = this.getToLineCap();
            strategy.appendField(locator, this, "toLineCap", buffer, theToLineCap);
        }
        {
            String theFromIcon;
            theFromIcon = this.getFromIcon();
            strategy.appendField(locator, this, "fromIcon", buffer, theFromIcon);
        }
        {
            String theToIcon;
            theToIcon = this.getToIcon();
            strategy.appendField(locator, this, "toIcon", buffer, theToIcon);
        }
        {
            String theColor;
            theColor = this.getColor();
            strategy.appendField(locator, this, "color", buffer, theColor);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RelationDef)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RelationDef that = ((RelationDef) object);
        {
            String lhsEntity;
            lhsEntity = this.getEntity();
            String rhsEntity;
            rhsEntity = that.getEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "entity", lhsEntity), LocatorUtils.property(thatLocator, "entity", rhsEntity), lhsEntity, rhsEntity)) {
                return false;
            }
        }
        {
            String lhsFromField;
            lhsFromField = this.getFromField();
            String rhsFromField;
            rhsFromField = that.getFromField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fromField", lhsFromField), LocatorUtils.property(thatLocator, "fromField", rhsFromField), lhsFromField, rhsFromField)) {
                return false;
            }
        }
        {
            String lhsToField;
            lhsToField = this.getToField();
            String rhsToField;
            rhsToField = that.getToField();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "toField", lhsToField), LocatorUtils.property(thatLocator, "toField", rhsToField), lhsToField, rhsToField)) {
                return false;
            }
        }
        {
            String lhsFromLineCap;
            lhsFromLineCap = this.getFromLineCap();
            String rhsFromLineCap;
            rhsFromLineCap = that.getFromLineCap();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fromLineCap", lhsFromLineCap), LocatorUtils.property(thatLocator, "fromLineCap", rhsFromLineCap), lhsFromLineCap, rhsFromLineCap)) {
                return false;
            }
        }
        {
            String lhsToLineCap;
            lhsToLineCap = this.getToLineCap();
            String rhsToLineCap;
            rhsToLineCap = that.getToLineCap();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "toLineCap", lhsToLineCap), LocatorUtils.property(thatLocator, "toLineCap", rhsToLineCap), lhsToLineCap, rhsToLineCap)) {
                return false;
            }
        }
        {
            String lhsFromIcon;
            lhsFromIcon = this.getFromIcon();
            String rhsFromIcon;
            rhsFromIcon = that.getFromIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fromIcon", lhsFromIcon), LocatorUtils.property(thatLocator, "fromIcon", rhsFromIcon), lhsFromIcon, rhsFromIcon)) {
                return false;
            }
        }
        {
            String lhsToIcon;
            lhsToIcon = this.getToIcon();
            String rhsToIcon;
            rhsToIcon = that.getToIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "toIcon", lhsToIcon), LocatorUtils.property(thatLocator, "toIcon", rhsToIcon), lhsToIcon, rhsToIcon)) {
                return false;
            }
        }
        {
            String lhsColor;
            lhsColor = this.getColor();
            String rhsColor;
            rhsColor = that.getColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "color", lhsColor), LocatorUtils.property(thatLocator, "color", rhsColor), lhsColor, rhsColor)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEntity;
            theEntity = this.getEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "entity", theEntity), currentHashCode, theEntity);
        }
        {
            String theFromField;
            theFromField = this.getFromField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fromField", theFromField), currentHashCode, theFromField);
        }
        {
            String theToField;
            theToField = this.getToField();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "toField", theToField), currentHashCode, theToField);
        }
        {
            String theFromLineCap;
            theFromLineCap = this.getFromLineCap();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fromLineCap", theFromLineCap), currentHashCode, theFromLineCap);
        }
        {
            String theToLineCap;
            theToLineCap = this.getToLineCap();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "toLineCap", theToLineCap), currentHashCode, theToLineCap);
        }
        {
            String theFromIcon;
            theFromIcon = this.getFromIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fromIcon", theFromIcon), currentHashCode, theFromIcon);
        }
        {
            String theToIcon;
            theToIcon = this.getToIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "toIcon", theToIcon), currentHashCode, theToIcon);
        }
        {
            String theColor;
            theColor = this.getColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "color", theColor), currentHashCode, theColor);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RelationDef) {
            final RelationDef copy = ((RelationDef) draftCopy);
            if (this.entity!= null) {
                String sourceEntity;
                sourceEntity = this.getEntity();
                String copyEntity = ((String) strategy.copy(LocatorUtils.property(locator, "entity", sourceEntity), sourceEntity));
                copy.setEntity(copyEntity);
            } else {
                copy.entity = null;
            }
            if (this.fromField!= null) {
                String sourceFromField;
                sourceFromField = this.getFromField();
                String copyFromField = ((String) strategy.copy(LocatorUtils.property(locator, "fromField", sourceFromField), sourceFromField));
                copy.setFromField(copyFromField);
            } else {
                copy.fromField = null;
            }
            if (this.toField!= null) {
                String sourceToField;
                sourceToField = this.getToField();
                String copyToField = ((String) strategy.copy(LocatorUtils.property(locator, "toField", sourceToField), sourceToField));
                copy.setToField(copyToField);
            } else {
                copy.toField = null;
            }
            if (this.fromLineCap!= null) {
                String sourceFromLineCap;
                sourceFromLineCap = this.getFromLineCap();
                String copyFromLineCap = ((String) strategy.copy(LocatorUtils.property(locator, "fromLineCap", sourceFromLineCap), sourceFromLineCap));
                copy.setFromLineCap(copyFromLineCap);
            } else {
                copy.fromLineCap = null;
            }
            if (this.toLineCap!= null) {
                String sourceToLineCap;
                sourceToLineCap = this.getToLineCap();
                String copyToLineCap = ((String) strategy.copy(LocatorUtils.property(locator, "toLineCap", sourceToLineCap), sourceToLineCap));
                copy.setToLineCap(copyToLineCap);
            } else {
                copy.toLineCap = null;
            }
            if (this.fromIcon!= null) {
                String sourceFromIcon;
                sourceFromIcon = this.getFromIcon();
                String copyFromIcon = ((String) strategy.copy(LocatorUtils.property(locator, "fromIcon", sourceFromIcon), sourceFromIcon));
                copy.setFromIcon(copyFromIcon);
            } else {
                copy.fromIcon = null;
            }
            if (this.toIcon!= null) {
                String sourceToIcon;
                sourceToIcon = this.getToIcon();
                String copyToIcon = ((String) strategy.copy(LocatorUtils.property(locator, "toIcon", sourceToIcon), sourceToIcon));
                copy.setToIcon(copyToIcon);
            } else {
                copy.toIcon = null;
            }
            if (this.color!= null) {
                String sourceColor;
                sourceColor = this.getColor();
                String copyColor = ((String) strategy.copy(LocatorUtils.property(locator, "color", sourceColor), sourceColor));
                copy.setColor(copyColor);
            } else {
                copy.color = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RelationDef();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RelationDef.Builder<_B> _other) {
        _other.entity = this.entity;
        _other.fromField = this.fromField;
        _other.toField = this.toField;
        _other.fromLineCap = this.fromLineCap;
        _other.toLineCap = this.toLineCap;
        _other.fromIcon = this.fromIcon;
        _other.toIcon = this.toIcon;
        _other.color = this.color;
    }

    public<_B >RelationDef.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RelationDef.Builder<_B>(_parentBuilder, this, true);
    }

    public RelationDef.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RelationDef.Builder<Void> builder() {
        return new RelationDef.Builder<Void>(null, null, false);
    }

    public static<_B >RelationDef.Builder<_B> copyOf(final RelationDef _other) {
        final RelationDef.Builder<_B> _newBuilder = new RelationDef.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RelationDef.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
            _other.entity = this.entity;
        }
        final PropertyTree fromFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromFieldPropertyTree!= null):((fromFieldPropertyTree == null)||(!fromFieldPropertyTree.isLeaf())))) {
            _other.fromField = this.fromField;
        }
        final PropertyTree toFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toField"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toFieldPropertyTree!= null):((toFieldPropertyTree == null)||(!toFieldPropertyTree.isLeaf())))) {
            _other.toField = this.toField;
        }
        final PropertyTree fromLineCapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromLineCap"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromLineCapPropertyTree!= null):((fromLineCapPropertyTree == null)||(!fromLineCapPropertyTree.isLeaf())))) {
            _other.fromLineCap = this.fromLineCap;
        }
        final PropertyTree toLineCapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toLineCap"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toLineCapPropertyTree!= null):((toLineCapPropertyTree == null)||(!toLineCapPropertyTree.isLeaf())))) {
            _other.toLineCap = this.toLineCap;
        }
        final PropertyTree fromIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromIcon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromIconPropertyTree!= null):((fromIconPropertyTree == null)||(!fromIconPropertyTree.isLeaf())))) {
            _other.fromIcon = this.fromIcon;
        }
        final PropertyTree toIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toIcon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toIconPropertyTree!= null):((toIconPropertyTree == null)||(!toIconPropertyTree.isLeaf())))) {
            _other.toIcon = this.toIcon;
        }
        final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
            _other.color = this.color;
        }
    }

    public<_B >RelationDef.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RelationDef.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public RelationDef.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RelationDef.Builder<_B> copyOf(final RelationDef _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RelationDef.Builder<_B> _newBuilder = new RelationDef.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RelationDef.Builder<Void> copyExcept(final RelationDef _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RelationDef.Builder<Void> copyOnly(final RelationDef _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final RelationDef _storedValue;
        private String entity;
        private String fromField;
        private String toField;
        private String fromLineCap;
        private String toLineCap;
        private String fromIcon;
        private String toIcon;
        private String color;

        public Builder(final _B _parentBuilder, final RelationDef _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.entity = _other.entity;
                    this.fromField = _other.fromField;
                    this.toField = _other.toField;
                    this.fromLineCap = _other.fromLineCap;
                    this.toLineCap = _other.toLineCap;
                    this.fromIcon = _other.fromIcon;
                    this.toIcon = _other.toIcon;
                    this.color = _other.color;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final RelationDef _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree entityPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("entity"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(entityPropertyTree!= null):((entityPropertyTree == null)||(!entityPropertyTree.isLeaf())))) {
                        this.entity = _other.entity;
                    }
                    final PropertyTree fromFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromFieldPropertyTree!= null):((fromFieldPropertyTree == null)||(!fromFieldPropertyTree.isLeaf())))) {
                        this.fromField = _other.fromField;
                    }
                    final PropertyTree toFieldPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toField"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toFieldPropertyTree!= null):((toFieldPropertyTree == null)||(!toFieldPropertyTree.isLeaf())))) {
                        this.toField = _other.toField;
                    }
                    final PropertyTree fromLineCapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromLineCap"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromLineCapPropertyTree!= null):((fromLineCapPropertyTree == null)||(!fromLineCapPropertyTree.isLeaf())))) {
                        this.fromLineCap = _other.fromLineCap;
                    }
                    final PropertyTree toLineCapPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toLineCap"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toLineCapPropertyTree!= null):((toLineCapPropertyTree == null)||(!toLineCapPropertyTree.isLeaf())))) {
                        this.toLineCap = _other.toLineCap;
                    }
                    final PropertyTree fromIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fromIcon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fromIconPropertyTree!= null):((fromIconPropertyTree == null)||(!fromIconPropertyTree.isLeaf())))) {
                        this.fromIcon = _other.fromIcon;
                    }
                    final PropertyTree toIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("toIcon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(toIconPropertyTree!= null):((toIconPropertyTree == null)||(!toIconPropertyTree.isLeaf())))) {
                        this.toIcon = _other.toIcon;
                    }
                    final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
                        this.color = _other.color;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends RelationDef >_P init(final _P _product) {
            _product.entity = this.entity;
            _product.fromField = this.fromField;
            _product.toField = this.toField;
            _product.fromLineCap = this.fromLineCap;
            _product.toLineCap = this.toLineCap;
            _product.fromIcon = this.fromIcon;
            _product.toIcon = this.toIcon;
            _product.color = this.color;
            return _product;
        }

        /**
         * Sets the new value of "entity" (any previous value will be replaced)
         * 
         * @param entity
         *     New value of the "entity" property.
         */
        public RelationDef.Builder<_B> withEntity(final String entity) {
            this.entity = entity;
            return this;
        }

        /**
         * Sets the new value of "fromField" (any previous value will be replaced)
         * 
         * @param fromField
         *     New value of the "fromField" property.
         */
        public RelationDef.Builder<_B> withFromField(final String fromField) {
            this.fromField = fromField;
            return this;
        }

        /**
         * Sets the new value of "toField" (any previous value will be replaced)
         * 
         * @param toField
         *     New value of the "toField" property.
         */
        public RelationDef.Builder<_B> withToField(final String toField) {
            this.toField = toField;
            return this;
        }

        /**
         * Sets the new value of "fromLineCap" (any previous value will be replaced)
         * 
         * @param fromLineCap
         *     New value of the "fromLineCap" property.
         */
        public RelationDef.Builder<_B> withFromLineCap(final String fromLineCap) {
            this.fromLineCap = fromLineCap;
            return this;
        }

        /**
         * Sets the new value of "toLineCap" (any previous value will be replaced)
         * 
         * @param toLineCap
         *     New value of the "toLineCap" property.
         */
        public RelationDef.Builder<_B> withToLineCap(final String toLineCap) {
            this.toLineCap = toLineCap;
            return this;
        }

        /**
         * Sets the new value of "fromIcon" (any previous value will be replaced)
         * 
         * @param fromIcon
         *     New value of the "fromIcon" property.
         */
        public RelationDef.Builder<_B> withFromIcon(final String fromIcon) {
            this.fromIcon = fromIcon;
            return this;
        }

        /**
         * Sets the new value of "toIcon" (any previous value will be replaced)
         * 
         * @param toIcon
         *     New value of the "toIcon" property.
         */
        public RelationDef.Builder<_B> withToIcon(final String toIcon) {
            this.toIcon = toIcon;
            return this;
        }

        /**
         * Sets the new value of "color" (any previous value will be replaced)
         * 
         * @param color
         *     New value of the "color" property.
         */
        public RelationDef.Builder<_B> withColor(final String color) {
            this.color = color;
            return this;
        }

        @Override
        public RelationDef build() {
            if (_storedValue == null) {
                return this.init(new RelationDef());
            } else {
                return ((RelationDef) _storedValue);
            }
        }

        public RelationDef.Builder<_B> copyOf(final RelationDef _other) {
            _other.copyTo(this);
            return this;
        }

        public RelationDef.Builder<_B> copyOf(final RelationDef.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RelationDef.Selector<RelationDef.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RelationDef.Select _root() {
            return new RelationDef.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> entity = null;
        private com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> fromField = null;
        private com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> toField = null;
        private com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> fromLineCap = null;
        private com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> toLineCap = null;
        private com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> fromIcon = null;
        private com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> toIcon = null;
        private com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> color = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.entity!= null) {
                products.put("entity", this.entity.init());
            }
            if (this.fromField!= null) {
                products.put("fromField", this.fromField.init());
            }
            if (this.toField!= null) {
                products.put("toField", this.toField.init());
            }
            if (this.fromLineCap!= null) {
                products.put("fromLineCap", this.fromLineCap.init());
            }
            if (this.toLineCap!= null) {
                products.put("toLineCap", this.toLineCap.init());
            }
            if (this.fromIcon!= null) {
                products.put("fromIcon", this.fromIcon.init());
            }
            if (this.toIcon!= null) {
                products.put("toIcon", this.toIcon.init());
            }
            if (this.color!= null) {
                products.put("color", this.color.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> entity() {
            return ((this.entity == null)?this.entity = new com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>>(this._root, this, "entity"):this.entity);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> fromField() {
            return ((this.fromField == null)?this.fromField = new com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>>(this._root, this, "fromField"):this.fromField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> toField() {
            return ((this.toField == null)?this.toField = new com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>>(this._root, this, "toField"):this.toField);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> fromLineCap() {
            return ((this.fromLineCap == null)?this.fromLineCap = new com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>>(this._root, this, "fromLineCap"):this.fromLineCap);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> toLineCap() {
            return ((this.toLineCap == null)?this.toLineCap = new com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>>(this._root, this, "toLineCap"):this.toLineCap);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> fromIcon() {
            return ((this.fromIcon == null)?this.fromIcon = new com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>>(this._root, this, "fromIcon"):this.fromIcon);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> toIcon() {
            return ((this.toIcon == null)?this.toIcon = new com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>>(this._root, this, "toIcon"):this.toIcon);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>> color() {
            return ((this.color == null)?this.color = new com.kscs.util.jaxb.Selector<TRoot, RelationDef.Selector<TRoot, TParent>>(this._root, this, "color"):this.color);
        }

    }

}
