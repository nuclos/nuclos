package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for rest-report complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rest-report"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="reportId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="reportType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="description" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="datasource" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rest-report", propOrder = {
    "content"
})
public class RestReport implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected Object content;
    @XmlAttribute(name = "reportId", required = true)
    protected String reportId;
    @XmlAttribute(name = "reportType", required = true)
    protected String reportType;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "description", required = true)
    protected String description;
    @XmlAttribute(name = "datasource", required = true)
    protected String datasource;

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setContent(Object value) {
        this.content = value;
    }

    /**
     * Gets the value of the reportId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportId() {
        return reportId;
    }

    /**
     * Sets the value of the reportId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportId(String value) {
        this.reportId = value;
    }

    /**
     * Gets the value of the reportType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportType() {
        return reportType;
    }

    /**
     * Sets the value of the reportType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportType(String value) {
        this.reportType = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the datasource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatasource() {
        return datasource;
    }

    /**
     * Sets the value of the datasource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatasource(String value) {
        this.datasource = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Object theContent;
            theContent = this.getContent();
            strategy.appendField(locator, this, "content", buffer, theContent);
        }
        {
            String theReportId;
            theReportId = this.getReportId();
            strategy.appendField(locator, this, "reportId", buffer, theReportId);
        }
        {
            String theReportType;
            theReportType = this.getReportType();
            strategy.appendField(locator, this, "reportType", buffer, theReportType);
        }
        {
            String theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            String theDatasource;
            theDatasource = this.getDatasource();
            strategy.appendField(locator, this, "datasource", buffer, theDatasource);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RestReport)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RestReport that = ((RestReport) object);
        {
            Object lhsContent;
            lhsContent = this.getContent();
            Object rhsContent;
            rhsContent = that.getContent();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "content", lhsContent), LocatorUtils.property(thatLocator, "content", rhsContent), lhsContent, rhsContent)) {
                return false;
            }
        }
        {
            String lhsReportId;
            lhsReportId = this.getReportId();
            String rhsReportId;
            rhsReportId = that.getReportId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "reportId", lhsReportId), LocatorUtils.property(thatLocator, "reportId", rhsReportId), lhsReportId, rhsReportId)) {
                return false;
            }
        }
        {
            String lhsReportType;
            lhsReportType = this.getReportType();
            String rhsReportType;
            rhsReportType = that.getReportType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "reportType", lhsReportType), LocatorUtils.property(thatLocator, "reportType", rhsReportType), lhsReportType, rhsReportType)) {
                return false;
            }
        }
        {
            String lhsName;
            lhsName = this.getName();
            String rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            String lhsDatasource;
            lhsDatasource = this.getDatasource();
            String rhsDatasource;
            rhsDatasource = that.getDatasource();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "datasource", lhsDatasource), LocatorUtils.property(thatLocator, "datasource", rhsDatasource), lhsDatasource, rhsDatasource)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Object theContent;
            theContent = this.getContent();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "content", theContent), currentHashCode, theContent);
        }
        {
            String theReportId;
            theReportId = this.getReportId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "reportId", theReportId), currentHashCode, theReportId);
        }
        {
            String theReportType;
            theReportType = this.getReportType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "reportType", theReportType), currentHashCode, theReportType);
        }
        {
            String theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            String theDatasource;
            theDatasource = this.getDatasource();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "datasource", theDatasource), currentHashCode, theDatasource);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RestReport) {
            final RestReport copy = ((RestReport) draftCopy);
            if (this.content!= null) {
                Object sourceContent;
                sourceContent = this.getContent();
                Object copyContent = ((Object) strategy.copy(LocatorUtils.property(locator, "content", sourceContent), sourceContent));
                copy.setContent(copyContent);
            } else {
                copy.content = null;
            }
            if (this.reportId!= null) {
                String sourceReportId;
                sourceReportId = this.getReportId();
                String copyReportId = ((String) strategy.copy(LocatorUtils.property(locator, "reportId", sourceReportId), sourceReportId));
                copy.setReportId(copyReportId);
            } else {
                copy.reportId = null;
            }
            if (this.reportType!= null) {
                String sourceReportType;
                sourceReportType = this.getReportType();
                String copyReportType = ((String) strategy.copy(LocatorUtils.property(locator, "reportType", sourceReportType), sourceReportType));
                copy.setReportType(copyReportType);
            } else {
                copy.reportType = null;
            }
            if (this.name!= null) {
                String sourceName;
                sourceName = this.getName();
                String copyName = ((String) strategy.copy(LocatorUtils.property(locator, "name", sourceName), sourceName));
                copy.setName(copyName);
            } else {
                copy.name = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.datasource!= null) {
                String sourceDatasource;
                sourceDatasource = this.getDatasource();
                String copyDatasource = ((String) strategy.copy(LocatorUtils.property(locator, "datasource", sourceDatasource), sourceDatasource));
                copy.setDatasource(copyDatasource);
            } else {
                copy.datasource = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RestReport();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestReport.Builder<_B> _other) {
        _other.content = this.content;
        _other.reportId = this.reportId;
        _other.reportType = this.reportType;
        _other.name = this.name;
        _other.description = this.description;
        _other.datasource = this.datasource;
    }

    public<_B >RestReport.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RestReport.Builder<_B>(_parentBuilder, this, true);
    }

    public RestReport.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RestReport.Builder<Void> builder() {
        return new RestReport.Builder<Void>(null, null, false);
    }

    public static<_B >RestReport.Builder<_B> copyOf(final RestReport _other) {
        final RestReport.Builder<_B> _newBuilder = new RestReport.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestReport.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree contentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("content"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(contentPropertyTree!= null):((contentPropertyTree == null)||(!contentPropertyTree.isLeaf())))) {
            _other.content = this.content;
        }
        final PropertyTree reportIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("reportId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(reportIdPropertyTree!= null):((reportIdPropertyTree == null)||(!reportIdPropertyTree.isLeaf())))) {
            _other.reportId = this.reportId;
        }
        final PropertyTree reportTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("reportType"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(reportTypePropertyTree!= null):((reportTypePropertyTree == null)||(!reportTypePropertyTree.isLeaf())))) {
            _other.reportType = this.reportType;
        }
        final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
            _other.name = this.name;
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree datasourcePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("datasource"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(datasourcePropertyTree!= null):((datasourcePropertyTree == null)||(!datasourcePropertyTree.isLeaf())))) {
            _other.datasource = this.datasource;
        }
    }

    public<_B >RestReport.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RestReport.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public RestReport.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RestReport.Builder<_B> copyOf(final RestReport _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RestReport.Builder<_B> _newBuilder = new RestReport.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RestReport.Builder<Void> copyExcept(final RestReport _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RestReport.Builder<Void> copyOnly(final RestReport _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final RestReport _storedValue;
        private Object content;
        private String reportId;
        private String reportType;
        private String name;
        private String description;
        private String datasource;

        public Builder(final _B _parentBuilder, final RestReport _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.content = _other.content;
                    this.reportId = _other.reportId;
                    this.reportType = _other.reportType;
                    this.name = _other.name;
                    this.description = _other.description;
                    this.datasource = _other.datasource;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final RestReport _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree contentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("content"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(contentPropertyTree!= null):((contentPropertyTree == null)||(!contentPropertyTree.isLeaf())))) {
                        this.content = _other.content;
                    }
                    final PropertyTree reportIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("reportId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(reportIdPropertyTree!= null):((reportIdPropertyTree == null)||(!reportIdPropertyTree.isLeaf())))) {
                        this.reportId = _other.reportId;
                    }
                    final PropertyTree reportTypePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("reportType"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(reportTypePropertyTree!= null):((reportTypePropertyTree == null)||(!reportTypePropertyTree.isLeaf())))) {
                        this.reportType = _other.reportType;
                    }
                    final PropertyTree namePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("name"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(namePropertyTree!= null):((namePropertyTree == null)||(!namePropertyTree.isLeaf())))) {
                        this.name = _other.name;
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree datasourcePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("datasource"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(datasourcePropertyTree!= null):((datasourcePropertyTree == null)||(!datasourcePropertyTree.isLeaf())))) {
                        this.datasource = _other.datasource;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends RestReport >_P init(final _P _product) {
            _product.content = this.content;
            _product.reportId = this.reportId;
            _product.reportType = this.reportType;
            _product.name = this.name;
            _product.description = this.description;
            _product.datasource = this.datasource;
            return _product;
        }

        /**
         * Sets the new value of "content" (any previous value will be replaced)
         * 
         * @param content
         *     New value of the "content" property.
         */
        public RestReport.Builder<_B> withContent(final Object content) {
            this.content = content;
            return this;
        }

        /**
         * Sets the new value of "reportId" (any previous value will be replaced)
         * 
         * @param reportId
         *     New value of the "reportId" property.
         */
        public RestReport.Builder<_B> withReportId(final String reportId) {
            this.reportId = reportId;
            return this;
        }

        /**
         * Sets the new value of "reportType" (any previous value will be replaced)
         * 
         * @param reportType
         *     New value of the "reportType" property.
         */
        public RestReport.Builder<_B> withReportType(final String reportType) {
            this.reportType = reportType;
            return this;
        }

        /**
         * Sets the new value of "name" (any previous value will be replaced)
         * 
         * @param name
         *     New value of the "name" property.
         */
        public RestReport.Builder<_B> withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public RestReport.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the new value of "datasource" (any previous value will be replaced)
         * 
         * @param datasource
         *     New value of the "datasource" property.
         */
        public RestReport.Builder<_B> withDatasource(final String datasource) {
            this.datasource = datasource;
            return this;
        }

        @Override
        public RestReport build() {
            if (_storedValue == null) {
                return this.init(new RestReport());
            } else {
                return ((RestReport) _storedValue);
            }
        }

        public RestReport.Builder<_B> copyOf(final RestReport _other) {
            _other.copyTo(this);
            return this;
        }

        public RestReport.Builder<_B> copyOf(final RestReport.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RestReport.Selector<RestReport.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RestReport.Select _root() {
            return new RestReport.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> content = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> reportId = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> reportType = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> name = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> description = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> datasource = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.content!= null) {
                products.put("content", this.content.init());
            }
            if (this.reportId!= null) {
                products.put("reportId", this.reportId.init());
            }
            if (this.reportType!= null) {
                products.put("reportType", this.reportType.init());
            }
            if (this.name!= null) {
                products.put("name", this.name.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.datasource!= null) {
                products.put("datasource", this.datasource.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> content() {
            return ((this.content == null)?this.content = new com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>>(this._root, this, "content"):this.content);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> reportId() {
            return ((this.reportId == null)?this.reportId = new com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>>(this._root, this, "reportId"):this.reportId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> reportType() {
            return ((this.reportType == null)?this.reportType = new com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>>(this._root, this, "reportType"):this.reportType);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> name() {
            return ((this.name == null)?this.name = new com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>>(this._root, this, "name"):this.name);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>> datasource() {
            return ((this.datasource == null)?this.datasource = new com.kscs.util.jaxb.Selector<TRoot, RestReport.Selector<TRoot, TParent>>(this._root, this, "datasource"):this.datasource);
        }

    }

}
