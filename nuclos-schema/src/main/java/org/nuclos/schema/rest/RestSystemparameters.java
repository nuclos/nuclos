package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for rest-systemparameters complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rest-systemparameters"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ANONYMOUS_USER_ACCESS_ENABLED" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ENVIRONMENT_DEVELOPMENT" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FORGOT_LOGIN_DETAILS_ENABLED" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FULLTEXT_SEARCH_ENABLED" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="FUNCTION_BLOCK_DEV" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="QUICKSEARCH_DELAY_TIME" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="USER_REGISTRATION_ENABLED" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="APP_NAME" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="WEBCLIENT_TITLE_TEMPLATE" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="WEBCLIENT_CSS" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="SORT_CALCULATED_ATTRIBUTES" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="CONFIRM_REPLACING_DOCUMENTS" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="SECURITY_PASSWORD_STRENGTH_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ADD_STAR_TO_MANDATORY_FIELD_LABEL" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ROOTPANE_BACKGROUND_COLOR" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="ROOTPANE_SU_BACKGROUND_COLOR" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="USERNAME_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rest-systemparameters")
public class RestSystemparameters implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ANONYMOUS_USER_ACCESS_ENABLED")
    protected Boolean anonymoususeraccessenabled;
    @XmlAttribute(name = "ENVIRONMENT_DEVELOPMENT")
    protected Boolean environmentdevelopment;
    @XmlAttribute(name = "FORGOT_LOGIN_DETAILS_ENABLED")
    protected Boolean forgotlogindetailsenabled;
    @XmlAttribute(name = "FULLTEXT_SEARCH_ENABLED")
    protected Boolean fulltextsearchenabled;
    @XmlAttribute(name = "FUNCTION_BLOCK_DEV")
    protected Boolean functionblockdev;
    @XmlAttribute(name = "QUICKSEARCH_DELAY_TIME")
    protected Integer quicksearchdelaytime;
    @XmlAttribute(name = "USER_REGISTRATION_ENABLED")
    protected Boolean userregistrationenabled;
    @XmlAttribute(name = "APP_NAME")
    protected String appname;
    @XmlAttribute(name = "WEBCLIENT_TITLE_TEMPLATE")
    protected String webclienttitletemplate;
    @XmlAttribute(name = "WEBCLIENT_CSS")
    protected String webclientcss;
    @XmlAttribute(name = "SORT_CALCULATED_ATTRIBUTES")
    protected Boolean sortcalculatedattributes;
    @XmlAttribute(name = "CONFIRM_REPLACING_DOCUMENTS")
    protected Boolean confirmreplacingdocuments;
    @XmlAttribute(name = "SECURITY_PASSWORD_STRENGTH_DESCRIPTION")
    protected String securitypasswordstrengthdescription;
    @XmlAttribute(name = "DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB")
    protected String defaultdashboardtasklistopennewtab;
    @XmlAttribute(name = "ADD_STAR_TO_MANDATORY_FIELD_LABEL")
    protected String addstartomandatoryfieldlabel;
    @XmlAttribute(name = "SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED")
    protected Boolean securitypasswordbasedauthenticationallowed;
    @XmlAttribute(name = "ROOTPANE_BACKGROUND_COLOR")
    protected String rootpanebackgroundcolor;
    @XmlAttribute(name = "ROOTPANE_SU_BACKGROUND_COLOR")
    protected String rootpanesubackgroundcolor;
    @XmlAttribute(name = "USERNAME_DESCRIPTION")
    protected String usernamedescription;

    /**
     * Gets the value of the anonymoususeraccessenabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @JsonProperty("ANONYMOUS_USER_ACCESS_ENABLED")
    public Boolean isANONYMOUSUSERACCESSENABLED() {
        return anonymoususeraccessenabled;
    }

    /**
     * Sets the value of the anonymoususeraccessenabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setANONYMOUSUSERACCESSENABLED(Boolean value) {
        this.anonymoususeraccessenabled = value;
    }

    /**
     * Gets the value of the environmentdevelopment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @JsonProperty("ENVIRONMENT_DEVELOPMENT")
    public Boolean isENVIRONMENTDEVELOPMENT() {
        return environmentdevelopment;
    }

    /**
     * Sets the value of the environmentdevelopment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setENVIRONMENTDEVELOPMENT(Boolean value) {
        this.environmentdevelopment = value;
    }

    /**
     * Gets the value of the forgotlogindetailsenabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @JsonProperty("FORGOT_LOGIN_DETAILS_ENABLED")
    public Boolean isFORGOTLOGINDETAILSENABLED() {
        return forgotlogindetailsenabled;
    }

    /**
     * Sets the value of the forgotlogindetailsenabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFORGOTLOGINDETAILSENABLED(Boolean value) {
        this.forgotlogindetailsenabled = value;
    }

    /**
     * Gets the value of the fulltextsearchenabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @JsonProperty("FULLTEXT_SEARCH_ENABLED")
    public Boolean isFULLTEXTSEARCHENABLED() {
        return fulltextsearchenabled;
    }

    /**
     * Sets the value of the fulltextsearchenabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFULLTEXTSEARCHENABLED(Boolean value) {
        this.fulltextsearchenabled = value;
    }

    /**
     * Gets the value of the functionblockdev property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @JsonProperty("FUNCTION_BLOCK_DEV")
    public Boolean isFUNCTIONBLOCKDEV() {
        return functionblockdev;
    }

    /**
     * Sets the value of the functionblockdev property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFUNCTIONBLOCKDEV(Boolean value) {
        this.functionblockdev = value;
    }

    /**
     * Gets the value of the quicksearchdelaytime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    @JsonProperty("QUICKSEARCH_DELAY_TIME")
    public Integer getQUICKSEARCHDELAYTIME() {
        return quicksearchdelaytime;
    }

    /**
     * Sets the value of the quicksearchdelaytime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQUICKSEARCHDELAYTIME(Integer value) {
        this.quicksearchdelaytime = value;
    }

    /**
     * Gets the value of the userregistrationenabled property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @JsonProperty("USER_REGISTRATION_ENABLED")
    public Boolean isUSERREGISTRATIONENABLED() {
        return userregistrationenabled;
    }

    /**
     * Sets the value of the userregistrationenabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUSERREGISTRATIONENABLED(Boolean value) {
        this.userregistrationenabled = value;
    }

    /**
     * Gets the value of the appname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @JsonProperty("APP_NAME")
    public String getAPPNAME() {
        return appname;
    }

    /**
     * Sets the value of the appname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPPNAME(String value) {
        this.appname = value;
    }

    /**
     * Gets the value of the webclienttitletemplate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @JsonProperty("WEBCLIENT_TITLE_TEMPLATE")
    public String getWEBCLIENTTITLETEMPLATE() {
        return webclienttitletemplate;
    }

    /**
     * Sets the value of the webclienttitletemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWEBCLIENTTITLETEMPLATE(String value) {
        this.webclienttitletemplate = value;
    }

    /**
     * Gets the value of the webclientcss property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @JsonProperty("WEBCLIENT_CSS")
    public String getWEBCLIENTCSS() {
        return webclientcss;
    }

    /**
     * Sets the value of the webclientcss property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWEBCLIENTCSS(String value) {
        this.webclientcss = value;
    }

    /**
     * Gets the value of the sortcalculatedattributes property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @JsonProperty("SORT_CALCULATED_ATTRIBUTES")
    public Boolean isSORTCALCULATEDATTRIBUTES() {
        return sortcalculatedattributes;
    }

    /**
     * Sets the value of the sortcalculatedattributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSORTCALCULATEDATTRIBUTES(Boolean value) {
        this.sortcalculatedattributes = value;
    }

    /**
     * Gets the value of the confirmreplacingdocuments property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @JsonProperty("CONFIRM_REPLACING_DOCUMENTS")
    public Boolean isCONFIRMREPLACINGDOCUMENTS() {
        return confirmreplacingdocuments;
    }

    /**
     * Sets the value of the confirmreplacingdocuments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCONFIRMREPLACINGDOCUMENTS(Boolean value) {
        this.confirmreplacingdocuments = value;
    }

    /**
     * Gets the value of the securitypasswordstrengthdescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @JsonProperty("SECURITY_PASSWORD_STRENGTH_DESCRIPTION")
    public String getSECURITYPASSWORDSTRENGTHDESCRIPTION() {
        return securitypasswordstrengthdescription;
    }

    /**
     * Sets the value of the securitypasswordstrengthdescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSECURITYPASSWORDSTRENGTHDESCRIPTION(String value) {
        this.securitypasswordstrengthdescription = value;
    }

    /**
     * Gets the value of the defaultdashboardtasklistopennewtab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @JsonProperty("DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB")
    public String getDEFAULTDASHBOARDTASKLISTOPENNEWTAB() {
        return defaultdashboardtasklistopennewtab;
    }

    /**
     * Sets the value of the defaultdashboardtasklistopennewtab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDEFAULTDASHBOARDTASKLISTOPENNEWTAB(String value) {
        this.defaultdashboardtasklistopennewtab = value;
    }

    /**
     * Gets the value of the addstartomandatoryfieldlabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @JsonProperty("ADD_STAR_TO_MANDATORY_FIELD_LABEL")
    public String getADDSTARTOMANDATORYFIELDLABEL() {
        return addstartomandatoryfieldlabel;
    }

    /**
     * Sets the value of the addstartomandatoryfieldlabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDSTARTOMANDATORYFIELDLABEL(String value) {
        this.addstartomandatoryfieldlabel = value;
    }

    /**
     * Gets the value of the securitypasswordbasedauthenticationallowed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    @JsonProperty("SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED")
    public Boolean isSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED() {
        return securitypasswordbasedauthenticationallowed;
    }

    /**
     * Sets the value of the securitypasswordbasedauthenticationallowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED(Boolean value) {
        this.securitypasswordbasedauthenticationallowed = value;
    }

    /**
     * Gets the value of the rootpanebackgroundcolor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @JsonProperty("ROOTPANE_BACKGROUND_COLOR")
    public String getROOTPANEBACKGROUNDCOLOR() {
        return rootpanebackgroundcolor;
    }

    /**
     * Sets the value of the rootpanebackgroundcolor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setROOTPANEBACKGROUNDCOLOR(String value) {
        this.rootpanebackgroundcolor = value;
    }

    /**
     * Gets the value of the rootpanesubackgroundcolor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @JsonProperty("ROOTPANE_SU_BACKGROUND_COLOR")
    public String getROOTPANESUBACKGROUNDCOLOR() {
        return rootpanesubackgroundcolor;
    }

    /**
     * Sets the value of the rootpanesubackgroundcolor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setROOTPANESUBACKGROUNDCOLOR(String value) {
        this.rootpanesubackgroundcolor = value;
    }

    /**
     * Gets the value of the usernamedescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @JsonProperty("USERNAME_DESCRIPTION")
    public String getUSERNAMEDESCRIPTION() {
        return usernamedescription;
    }

    /**
     * Sets the value of the usernamedescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSERNAMEDESCRIPTION(String value) {
        this.usernamedescription = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            Boolean theANONYMOUSUSERACCESSENABLED;
            theANONYMOUSUSERACCESSENABLED = this.isANONYMOUSUSERACCESSENABLED();
            strategy.appendField(locator, this, "anonymoususeraccessenabled", buffer, theANONYMOUSUSERACCESSENABLED);
        }
        {
            Boolean theENVIRONMENTDEVELOPMENT;
            theENVIRONMENTDEVELOPMENT = this.isENVIRONMENTDEVELOPMENT();
            strategy.appendField(locator, this, "environmentdevelopment", buffer, theENVIRONMENTDEVELOPMENT);
        }
        {
            Boolean theFORGOTLOGINDETAILSENABLED;
            theFORGOTLOGINDETAILSENABLED = this.isFORGOTLOGINDETAILSENABLED();
            strategy.appendField(locator, this, "forgotlogindetailsenabled", buffer, theFORGOTLOGINDETAILSENABLED);
        }
        {
            Boolean theFULLTEXTSEARCHENABLED;
            theFULLTEXTSEARCHENABLED = this.isFULLTEXTSEARCHENABLED();
            strategy.appendField(locator, this, "fulltextsearchenabled", buffer, theFULLTEXTSEARCHENABLED);
        }
        {
            Boolean theFUNCTIONBLOCKDEV;
            theFUNCTIONBLOCKDEV = this.isFUNCTIONBLOCKDEV();
            strategy.appendField(locator, this, "functionblockdev", buffer, theFUNCTIONBLOCKDEV);
        }
        {
            Integer theQUICKSEARCHDELAYTIME;
            theQUICKSEARCHDELAYTIME = this.getQUICKSEARCHDELAYTIME();
            strategy.appendField(locator, this, "quicksearchdelaytime", buffer, theQUICKSEARCHDELAYTIME);
        }
        {
            Boolean theUSERREGISTRATIONENABLED;
            theUSERREGISTRATIONENABLED = this.isUSERREGISTRATIONENABLED();
            strategy.appendField(locator, this, "userregistrationenabled", buffer, theUSERREGISTRATIONENABLED);
        }
        {
            String theAPPNAME;
            theAPPNAME = this.getAPPNAME();
            strategy.appendField(locator, this, "appname", buffer, theAPPNAME);
        }
        {
            String theWEBCLIENTTITLETEMPLATE;
            theWEBCLIENTTITLETEMPLATE = this.getWEBCLIENTTITLETEMPLATE();
            strategy.appendField(locator, this, "webclienttitletemplate", buffer, theWEBCLIENTTITLETEMPLATE);
        }
        {
            String theWEBCLIENTCSS;
            theWEBCLIENTCSS = this.getWEBCLIENTCSS();
            strategy.appendField(locator, this, "webclientcss", buffer, theWEBCLIENTCSS);
        }
        {
            Boolean theSORTCALCULATEDATTRIBUTES;
            theSORTCALCULATEDATTRIBUTES = this.isSORTCALCULATEDATTRIBUTES();
            strategy.appendField(locator, this, "sortcalculatedattributes", buffer, theSORTCALCULATEDATTRIBUTES);
        }
        {
            Boolean theCONFIRMREPLACINGDOCUMENTS;
            theCONFIRMREPLACINGDOCUMENTS = this.isCONFIRMREPLACINGDOCUMENTS();
            strategy.appendField(locator, this, "confirmreplacingdocuments", buffer, theCONFIRMREPLACINGDOCUMENTS);
        }
        {
            String theSECURITYPASSWORDSTRENGTHDESCRIPTION;
            theSECURITYPASSWORDSTRENGTHDESCRIPTION = this.getSECURITYPASSWORDSTRENGTHDESCRIPTION();
            strategy.appendField(locator, this, "securitypasswordstrengthdescription", buffer, theSECURITYPASSWORDSTRENGTHDESCRIPTION);
        }
        {
            String theDEFAULTDASHBOARDTASKLISTOPENNEWTAB;
            theDEFAULTDASHBOARDTASKLISTOPENNEWTAB = this.getDEFAULTDASHBOARDTASKLISTOPENNEWTAB();
            strategy.appendField(locator, this, "defaultdashboardtasklistopennewtab", buffer, theDEFAULTDASHBOARDTASKLISTOPENNEWTAB);
        }
        {
            String theADDSTARTOMANDATORYFIELDLABEL;
            theADDSTARTOMANDATORYFIELDLABEL = this.getADDSTARTOMANDATORYFIELDLABEL();
            strategy.appendField(locator, this, "addstartomandatoryfieldlabel", buffer, theADDSTARTOMANDATORYFIELDLABEL);
        }
        {
            Boolean theSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED;
            theSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED = this.isSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED();
            strategy.appendField(locator, this, "securitypasswordbasedauthenticationallowed", buffer, theSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED);
        }
        {
            String theROOTPANEBACKGROUNDCOLOR;
            theROOTPANEBACKGROUNDCOLOR = this.getROOTPANEBACKGROUNDCOLOR();
            strategy.appendField(locator, this, "rootpanebackgroundcolor", buffer, theROOTPANEBACKGROUNDCOLOR);
        }
        {
            String theROOTPANESUBACKGROUNDCOLOR;
            theROOTPANESUBACKGROUNDCOLOR = this.getROOTPANESUBACKGROUNDCOLOR();
            strategy.appendField(locator, this, "rootpanesubackgroundcolor", buffer, theROOTPANESUBACKGROUNDCOLOR);
        }
        {
            String theUSERNAMEDESCRIPTION;
            theUSERNAMEDESCRIPTION = this.getUSERNAMEDESCRIPTION();
            strategy.appendField(locator, this, "usernamedescription", buffer, theUSERNAMEDESCRIPTION);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof RestSystemparameters)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final RestSystemparameters that = ((RestSystemparameters) object);
        {
            Boolean lhsANONYMOUSUSERACCESSENABLED;
            lhsANONYMOUSUSERACCESSENABLED = this.isANONYMOUSUSERACCESSENABLED();
            Boolean rhsANONYMOUSUSERACCESSENABLED;
            rhsANONYMOUSUSERACCESSENABLED = that.isANONYMOUSUSERACCESSENABLED();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "anonymoususeraccessenabled", lhsANONYMOUSUSERACCESSENABLED), LocatorUtils.property(thatLocator, "anonymoususeraccessenabled", rhsANONYMOUSUSERACCESSENABLED), lhsANONYMOUSUSERACCESSENABLED, rhsANONYMOUSUSERACCESSENABLED)) {
                return false;
            }
        }
        {
            Boolean lhsENVIRONMENTDEVELOPMENT;
            lhsENVIRONMENTDEVELOPMENT = this.isENVIRONMENTDEVELOPMENT();
            Boolean rhsENVIRONMENTDEVELOPMENT;
            rhsENVIRONMENTDEVELOPMENT = that.isENVIRONMENTDEVELOPMENT();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "environmentdevelopment", lhsENVIRONMENTDEVELOPMENT), LocatorUtils.property(thatLocator, "environmentdevelopment", rhsENVIRONMENTDEVELOPMENT), lhsENVIRONMENTDEVELOPMENT, rhsENVIRONMENTDEVELOPMENT)) {
                return false;
            }
        }
        {
            Boolean lhsFORGOTLOGINDETAILSENABLED;
            lhsFORGOTLOGINDETAILSENABLED = this.isFORGOTLOGINDETAILSENABLED();
            Boolean rhsFORGOTLOGINDETAILSENABLED;
            rhsFORGOTLOGINDETAILSENABLED = that.isFORGOTLOGINDETAILSENABLED();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "forgotlogindetailsenabled", lhsFORGOTLOGINDETAILSENABLED), LocatorUtils.property(thatLocator, "forgotlogindetailsenabled", rhsFORGOTLOGINDETAILSENABLED), lhsFORGOTLOGINDETAILSENABLED, rhsFORGOTLOGINDETAILSENABLED)) {
                return false;
            }
        }
        {
            Boolean lhsFULLTEXTSEARCHENABLED;
            lhsFULLTEXTSEARCHENABLED = this.isFULLTEXTSEARCHENABLED();
            Boolean rhsFULLTEXTSEARCHENABLED;
            rhsFULLTEXTSEARCHENABLED = that.isFULLTEXTSEARCHENABLED();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fulltextsearchenabled", lhsFULLTEXTSEARCHENABLED), LocatorUtils.property(thatLocator, "fulltextsearchenabled", rhsFULLTEXTSEARCHENABLED), lhsFULLTEXTSEARCHENABLED, rhsFULLTEXTSEARCHENABLED)) {
                return false;
            }
        }
        {
            Boolean lhsFUNCTIONBLOCKDEV;
            lhsFUNCTIONBLOCKDEV = this.isFUNCTIONBLOCKDEV();
            Boolean rhsFUNCTIONBLOCKDEV;
            rhsFUNCTIONBLOCKDEV = that.isFUNCTIONBLOCKDEV();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "functionblockdev", lhsFUNCTIONBLOCKDEV), LocatorUtils.property(thatLocator, "functionblockdev", rhsFUNCTIONBLOCKDEV), lhsFUNCTIONBLOCKDEV, rhsFUNCTIONBLOCKDEV)) {
                return false;
            }
        }
        {
            Integer lhsQUICKSEARCHDELAYTIME;
            lhsQUICKSEARCHDELAYTIME = this.getQUICKSEARCHDELAYTIME();
            Integer rhsQUICKSEARCHDELAYTIME;
            rhsQUICKSEARCHDELAYTIME = that.getQUICKSEARCHDELAYTIME();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "quicksearchdelaytime", lhsQUICKSEARCHDELAYTIME), LocatorUtils.property(thatLocator, "quicksearchdelaytime", rhsQUICKSEARCHDELAYTIME), lhsQUICKSEARCHDELAYTIME, rhsQUICKSEARCHDELAYTIME)) {
                return false;
            }
        }
        {
            Boolean lhsUSERREGISTRATIONENABLED;
            lhsUSERREGISTRATIONENABLED = this.isUSERREGISTRATIONENABLED();
            Boolean rhsUSERREGISTRATIONENABLED;
            rhsUSERREGISTRATIONENABLED = that.isUSERREGISTRATIONENABLED();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "userregistrationenabled", lhsUSERREGISTRATIONENABLED), LocatorUtils.property(thatLocator, "userregistrationenabled", rhsUSERREGISTRATIONENABLED), lhsUSERREGISTRATIONENABLED, rhsUSERREGISTRATIONENABLED)) {
                return false;
            }
        }
        {
            String lhsAPPNAME;
            lhsAPPNAME = this.getAPPNAME();
            String rhsAPPNAME;
            rhsAPPNAME = that.getAPPNAME();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "appname", lhsAPPNAME), LocatorUtils.property(thatLocator, "appname", rhsAPPNAME), lhsAPPNAME, rhsAPPNAME)) {
                return false;
            }
        }
        {
            String lhsWEBCLIENTTITLETEMPLATE;
            lhsWEBCLIENTTITLETEMPLATE = this.getWEBCLIENTTITLETEMPLATE();
            String rhsWEBCLIENTTITLETEMPLATE;
            rhsWEBCLIENTTITLETEMPLATE = that.getWEBCLIENTTITLETEMPLATE();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "webclienttitletemplate", lhsWEBCLIENTTITLETEMPLATE), LocatorUtils.property(thatLocator, "webclienttitletemplate", rhsWEBCLIENTTITLETEMPLATE), lhsWEBCLIENTTITLETEMPLATE, rhsWEBCLIENTTITLETEMPLATE)) {
                return false;
            }
        }
        {
            String lhsWEBCLIENTCSS;
            lhsWEBCLIENTCSS = this.getWEBCLIENTCSS();
            String rhsWEBCLIENTCSS;
            rhsWEBCLIENTCSS = that.getWEBCLIENTCSS();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "webclientcss", lhsWEBCLIENTCSS), LocatorUtils.property(thatLocator, "webclientcss", rhsWEBCLIENTCSS), lhsWEBCLIENTCSS, rhsWEBCLIENTCSS)) {
                return false;
            }
        }
        {
            Boolean lhsSORTCALCULATEDATTRIBUTES;
            lhsSORTCALCULATEDATTRIBUTES = this.isSORTCALCULATEDATTRIBUTES();
            Boolean rhsSORTCALCULATEDATTRIBUTES;
            rhsSORTCALCULATEDATTRIBUTES = that.isSORTCALCULATEDATTRIBUTES();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sortcalculatedattributes", lhsSORTCALCULATEDATTRIBUTES), LocatorUtils.property(thatLocator, "sortcalculatedattributes", rhsSORTCALCULATEDATTRIBUTES), lhsSORTCALCULATEDATTRIBUTES, rhsSORTCALCULATEDATTRIBUTES)) {
                return false;
            }
        }
        {
            Boolean lhsCONFIRMREPLACINGDOCUMENTS;
            lhsCONFIRMREPLACINGDOCUMENTS = this.isCONFIRMREPLACINGDOCUMENTS();
            Boolean rhsCONFIRMREPLACINGDOCUMENTS;
            rhsCONFIRMREPLACINGDOCUMENTS = that.isCONFIRMREPLACINGDOCUMENTS();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "confirmreplacingdocuments", lhsCONFIRMREPLACINGDOCUMENTS), LocatorUtils.property(thatLocator, "confirmreplacingdocuments", rhsCONFIRMREPLACINGDOCUMENTS), lhsCONFIRMREPLACINGDOCUMENTS, rhsCONFIRMREPLACINGDOCUMENTS)) {
                return false;
            }
        }
        {
            String lhsSECURITYPASSWORDSTRENGTHDESCRIPTION;
            lhsSECURITYPASSWORDSTRENGTHDESCRIPTION = this.getSECURITYPASSWORDSTRENGTHDESCRIPTION();
            String rhsSECURITYPASSWORDSTRENGTHDESCRIPTION;
            rhsSECURITYPASSWORDSTRENGTHDESCRIPTION = that.getSECURITYPASSWORDSTRENGTHDESCRIPTION();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "securitypasswordstrengthdescription", lhsSECURITYPASSWORDSTRENGTHDESCRIPTION), LocatorUtils.property(thatLocator, "securitypasswordstrengthdescription", rhsSECURITYPASSWORDSTRENGTHDESCRIPTION), lhsSECURITYPASSWORDSTRENGTHDESCRIPTION, rhsSECURITYPASSWORDSTRENGTHDESCRIPTION)) {
                return false;
            }
        }
        {
            String lhsDEFAULTDASHBOARDTASKLISTOPENNEWTAB;
            lhsDEFAULTDASHBOARDTASKLISTOPENNEWTAB = this.getDEFAULTDASHBOARDTASKLISTOPENNEWTAB();
            String rhsDEFAULTDASHBOARDTASKLISTOPENNEWTAB;
            rhsDEFAULTDASHBOARDTASKLISTOPENNEWTAB = that.getDEFAULTDASHBOARDTASKLISTOPENNEWTAB();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "defaultdashboardtasklistopennewtab", lhsDEFAULTDASHBOARDTASKLISTOPENNEWTAB), LocatorUtils.property(thatLocator, "defaultdashboardtasklistopennewtab", rhsDEFAULTDASHBOARDTASKLISTOPENNEWTAB), lhsDEFAULTDASHBOARDTASKLISTOPENNEWTAB, rhsDEFAULTDASHBOARDTASKLISTOPENNEWTAB)) {
                return false;
            }
        }
        {
            String lhsADDSTARTOMANDATORYFIELDLABEL;
            lhsADDSTARTOMANDATORYFIELDLABEL = this.getADDSTARTOMANDATORYFIELDLABEL();
            String rhsADDSTARTOMANDATORYFIELDLABEL;
            rhsADDSTARTOMANDATORYFIELDLABEL = that.getADDSTARTOMANDATORYFIELDLABEL();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "addstartomandatoryfieldlabel", lhsADDSTARTOMANDATORYFIELDLABEL), LocatorUtils.property(thatLocator, "addstartomandatoryfieldlabel", rhsADDSTARTOMANDATORYFIELDLABEL), lhsADDSTARTOMANDATORYFIELDLABEL, rhsADDSTARTOMANDATORYFIELDLABEL)) {
                return false;
            }
        }
        {
            Boolean lhsSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED;
            lhsSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED = this.isSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED();
            Boolean rhsSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED;
            rhsSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED = that.isSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "securitypasswordbasedauthenticationallowed", lhsSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED), LocatorUtils.property(thatLocator, "securitypasswordbasedauthenticationallowed", rhsSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED), lhsSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED, rhsSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED)) {
                return false;
            }
        }
        {
            String lhsROOTPANEBACKGROUNDCOLOR;
            lhsROOTPANEBACKGROUNDCOLOR = this.getROOTPANEBACKGROUNDCOLOR();
            String rhsROOTPANEBACKGROUNDCOLOR;
            rhsROOTPANEBACKGROUNDCOLOR = that.getROOTPANEBACKGROUNDCOLOR();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rootpanebackgroundcolor", lhsROOTPANEBACKGROUNDCOLOR), LocatorUtils.property(thatLocator, "rootpanebackgroundcolor", rhsROOTPANEBACKGROUNDCOLOR), lhsROOTPANEBACKGROUNDCOLOR, rhsROOTPANEBACKGROUNDCOLOR)) {
                return false;
            }
        }
        {
            String lhsROOTPANESUBACKGROUNDCOLOR;
            lhsROOTPANESUBACKGROUNDCOLOR = this.getROOTPANESUBACKGROUNDCOLOR();
            String rhsROOTPANESUBACKGROUNDCOLOR;
            rhsROOTPANESUBACKGROUNDCOLOR = that.getROOTPANESUBACKGROUNDCOLOR();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rootpanesubackgroundcolor", lhsROOTPANESUBACKGROUNDCOLOR), LocatorUtils.property(thatLocator, "rootpanesubackgroundcolor", rhsROOTPANESUBACKGROUNDCOLOR), lhsROOTPANESUBACKGROUNDCOLOR, rhsROOTPANESUBACKGROUNDCOLOR)) {
                return false;
            }
        }
        {
            String lhsUSERNAMEDESCRIPTION;
            lhsUSERNAMEDESCRIPTION = this.getUSERNAMEDESCRIPTION();
            String rhsUSERNAMEDESCRIPTION;
            rhsUSERNAMEDESCRIPTION = that.getUSERNAMEDESCRIPTION();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "usernamedescription", lhsUSERNAMEDESCRIPTION), LocatorUtils.property(thatLocator, "usernamedescription", rhsUSERNAMEDESCRIPTION), lhsUSERNAMEDESCRIPTION, rhsUSERNAMEDESCRIPTION)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            Boolean theANONYMOUSUSERACCESSENABLED;
            theANONYMOUSUSERACCESSENABLED = this.isANONYMOUSUSERACCESSENABLED();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "anonymoususeraccessenabled", theANONYMOUSUSERACCESSENABLED), currentHashCode, theANONYMOUSUSERACCESSENABLED);
        }
        {
            Boolean theENVIRONMENTDEVELOPMENT;
            theENVIRONMENTDEVELOPMENT = this.isENVIRONMENTDEVELOPMENT();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "environmentdevelopment", theENVIRONMENTDEVELOPMENT), currentHashCode, theENVIRONMENTDEVELOPMENT);
        }
        {
            Boolean theFORGOTLOGINDETAILSENABLED;
            theFORGOTLOGINDETAILSENABLED = this.isFORGOTLOGINDETAILSENABLED();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "forgotlogindetailsenabled", theFORGOTLOGINDETAILSENABLED), currentHashCode, theFORGOTLOGINDETAILSENABLED);
        }
        {
            Boolean theFULLTEXTSEARCHENABLED;
            theFULLTEXTSEARCHENABLED = this.isFULLTEXTSEARCHENABLED();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fulltextsearchenabled", theFULLTEXTSEARCHENABLED), currentHashCode, theFULLTEXTSEARCHENABLED);
        }
        {
            Boolean theFUNCTIONBLOCKDEV;
            theFUNCTIONBLOCKDEV = this.isFUNCTIONBLOCKDEV();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "functionblockdev", theFUNCTIONBLOCKDEV), currentHashCode, theFUNCTIONBLOCKDEV);
        }
        {
            Integer theQUICKSEARCHDELAYTIME;
            theQUICKSEARCHDELAYTIME = this.getQUICKSEARCHDELAYTIME();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "quicksearchdelaytime", theQUICKSEARCHDELAYTIME), currentHashCode, theQUICKSEARCHDELAYTIME);
        }
        {
            Boolean theUSERREGISTRATIONENABLED;
            theUSERREGISTRATIONENABLED = this.isUSERREGISTRATIONENABLED();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "userregistrationenabled", theUSERREGISTRATIONENABLED), currentHashCode, theUSERREGISTRATIONENABLED);
        }
        {
            String theAPPNAME;
            theAPPNAME = this.getAPPNAME();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "appname", theAPPNAME), currentHashCode, theAPPNAME);
        }
        {
            String theWEBCLIENTTITLETEMPLATE;
            theWEBCLIENTTITLETEMPLATE = this.getWEBCLIENTTITLETEMPLATE();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "webclienttitletemplate", theWEBCLIENTTITLETEMPLATE), currentHashCode, theWEBCLIENTTITLETEMPLATE);
        }
        {
            String theWEBCLIENTCSS;
            theWEBCLIENTCSS = this.getWEBCLIENTCSS();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "webclientcss", theWEBCLIENTCSS), currentHashCode, theWEBCLIENTCSS);
        }
        {
            Boolean theSORTCALCULATEDATTRIBUTES;
            theSORTCALCULATEDATTRIBUTES = this.isSORTCALCULATEDATTRIBUTES();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "sortcalculatedattributes", theSORTCALCULATEDATTRIBUTES), currentHashCode, theSORTCALCULATEDATTRIBUTES);
        }
        {
            Boolean theCONFIRMREPLACINGDOCUMENTS;
            theCONFIRMREPLACINGDOCUMENTS = this.isCONFIRMREPLACINGDOCUMENTS();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "confirmreplacingdocuments", theCONFIRMREPLACINGDOCUMENTS), currentHashCode, theCONFIRMREPLACINGDOCUMENTS);
        }
        {
            String theSECURITYPASSWORDSTRENGTHDESCRIPTION;
            theSECURITYPASSWORDSTRENGTHDESCRIPTION = this.getSECURITYPASSWORDSTRENGTHDESCRIPTION();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "securitypasswordstrengthdescription", theSECURITYPASSWORDSTRENGTHDESCRIPTION), currentHashCode, theSECURITYPASSWORDSTRENGTHDESCRIPTION);
        }
        {
            String theDEFAULTDASHBOARDTASKLISTOPENNEWTAB;
            theDEFAULTDASHBOARDTASKLISTOPENNEWTAB = this.getDEFAULTDASHBOARDTASKLISTOPENNEWTAB();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "defaultdashboardtasklistopennewtab", theDEFAULTDASHBOARDTASKLISTOPENNEWTAB), currentHashCode, theDEFAULTDASHBOARDTASKLISTOPENNEWTAB);
        }
        {
            String theADDSTARTOMANDATORYFIELDLABEL;
            theADDSTARTOMANDATORYFIELDLABEL = this.getADDSTARTOMANDATORYFIELDLABEL();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addstartomandatoryfieldlabel", theADDSTARTOMANDATORYFIELDLABEL), currentHashCode, theADDSTARTOMANDATORYFIELDLABEL);
        }
        {
            Boolean theSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED;
            theSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED = this.isSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "securitypasswordbasedauthenticationallowed", theSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED), currentHashCode, theSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED);
        }
        {
            String theROOTPANEBACKGROUNDCOLOR;
            theROOTPANEBACKGROUNDCOLOR = this.getROOTPANEBACKGROUNDCOLOR();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rootpanebackgroundcolor", theROOTPANEBACKGROUNDCOLOR), currentHashCode, theROOTPANEBACKGROUNDCOLOR);
        }
        {
            String theROOTPANESUBACKGROUNDCOLOR;
            theROOTPANESUBACKGROUNDCOLOR = this.getROOTPANESUBACKGROUNDCOLOR();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rootpanesubackgroundcolor", theROOTPANESUBACKGROUNDCOLOR), currentHashCode, theROOTPANESUBACKGROUNDCOLOR);
        }
        {
            String theUSERNAMEDESCRIPTION;
            theUSERNAMEDESCRIPTION = this.getUSERNAMEDESCRIPTION();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "usernamedescription", theUSERNAMEDESCRIPTION), currentHashCode, theUSERNAMEDESCRIPTION);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof RestSystemparameters) {
            final RestSystemparameters copy = ((RestSystemparameters) draftCopy);
            if (this.anonymoususeraccessenabled!= null) {
                Boolean sourceANONYMOUSUSERACCESSENABLED;
                sourceANONYMOUSUSERACCESSENABLED = this.isANONYMOUSUSERACCESSENABLED();
                Boolean copyANONYMOUSUSERACCESSENABLED = ((Boolean) strategy.copy(LocatorUtils.property(locator, "anonymoususeraccessenabled", sourceANONYMOUSUSERACCESSENABLED), sourceANONYMOUSUSERACCESSENABLED));
                copy.setANONYMOUSUSERACCESSENABLED(copyANONYMOUSUSERACCESSENABLED);
            } else {
                copy.anonymoususeraccessenabled = null;
            }
            if (this.environmentdevelopment!= null) {
                Boolean sourceENVIRONMENTDEVELOPMENT;
                sourceENVIRONMENTDEVELOPMENT = this.isENVIRONMENTDEVELOPMENT();
                Boolean copyENVIRONMENTDEVELOPMENT = ((Boolean) strategy.copy(LocatorUtils.property(locator, "environmentdevelopment", sourceENVIRONMENTDEVELOPMENT), sourceENVIRONMENTDEVELOPMENT));
                copy.setENVIRONMENTDEVELOPMENT(copyENVIRONMENTDEVELOPMENT);
            } else {
                copy.environmentdevelopment = null;
            }
            if (this.forgotlogindetailsenabled!= null) {
                Boolean sourceFORGOTLOGINDETAILSENABLED;
                sourceFORGOTLOGINDETAILSENABLED = this.isFORGOTLOGINDETAILSENABLED();
                Boolean copyFORGOTLOGINDETAILSENABLED = ((Boolean) strategy.copy(LocatorUtils.property(locator, "forgotlogindetailsenabled", sourceFORGOTLOGINDETAILSENABLED), sourceFORGOTLOGINDETAILSENABLED));
                copy.setFORGOTLOGINDETAILSENABLED(copyFORGOTLOGINDETAILSENABLED);
            } else {
                copy.forgotlogindetailsenabled = null;
            }
            if (this.fulltextsearchenabled!= null) {
                Boolean sourceFULLTEXTSEARCHENABLED;
                sourceFULLTEXTSEARCHENABLED = this.isFULLTEXTSEARCHENABLED();
                Boolean copyFULLTEXTSEARCHENABLED = ((Boolean) strategy.copy(LocatorUtils.property(locator, "fulltextsearchenabled", sourceFULLTEXTSEARCHENABLED), sourceFULLTEXTSEARCHENABLED));
                copy.setFULLTEXTSEARCHENABLED(copyFULLTEXTSEARCHENABLED);
            } else {
                copy.fulltextsearchenabled = null;
            }
            if (this.functionblockdev!= null) {
                Boolean sourceFUNCTIONBLOCKDEV;
                sourceFUNCTIONBLOCKDEV = this.isFUNCTIONBLOCKDEV();
                Boolean copyFUNCTIONBLOCKDEV = ((Boolean) strategy.copy(LocatorUtils.property(locator, "functionblockdev", sourceFUNCTIONBLOCKDEV), sourceFUNCTIONBLOCKDEV));
                copy.setFUNCTIONBLOCKDEV(copyFUNCTIONBLOCKDEV);
            } else {
                copy.functionblockdev = null;
            }
            if (this.quicksearchdelaytime!= null) {
                Integer sourceQUICKSEARCHDELAYTIME;
                sourceQUICKSEARCHDELAYTIME = this.getQUICKSEARCHDELAYTIME();
                Integer copyQUICKSEARCHDELAYTIME = ((Integer) strategy.copy(LocatorUtils.property(locator, "quicksearchdelaytime", sourceQUICKSEARCHDELAYTIME), sourceQUICKSEARCHDELAYTIME));
                copy.setQUICKSEARCHDELAYTIME(copyQUICKSEARCHDELAYTIME);
            } else {
                copy.quicksearchdelaytime = null;
            }
            if (this.userregistrationenabled!= null) {
                Boolean sourceUSERREGISTRATIONENABLED;
                sourceUSERREGISTRATIONENABLED = this.isUSERREGISTRATIONENABLED();
                Boolean copyUSERREGISTRATIONENABLED = ((Boolean) strategy.copy(LocatorUtils.property(locator, "userregistrationenabled", sourceUSERREGISTRATIONENABLED), sourceUSERREGISTRATIONENABLED));
                copy.setUSERREGISTRATIONENABLED(copyUSERREGISTRATIONENABLED);
            } else {
                copy.userregistrationenabled = null;
            }
            if (this.appname!= null) {
                String sourceAPPNAME;
                sourceAPPNAME = this.getAPPNAME();
                String copyAPPNAME = ((String) strategy.copy(LocatorUtils.property(locator, "appname", sourceAPPNAME), sourceAPPNAME));
                copy.setAPPNAME(copyAPPNAME);
            } else {
                copy.appname = null;
            }
            if (this.webclienttitletemplate!= null) {
                String sourceWEBCLIENTTITLETEMPLATE;
                sourceWEBCLIENTTITLETEMPLATE = this.getWEBCLIENTTITLETEMPLATE();
                String copyWEBCLIENTTITLETEMPLATE = ((String) strategy.copy(LocatorUtils.property(locator, "webclienttitletemplate", sourceWEBCLIENTTITLETEMPLATE), sourceWEBCLIENTTITLETEMPLATE));
                copy.setWEBCLIENTTITLETEMPLATE(copyWEBCLIENTTITLETEMPLATE);
            } else {
                copy.webclienttitletemplate = null;
            }
            if (this.webclientcss!= null) {
                String sourceWEBCLIENTCSS;
                sourceWEBCLIENTCSS = this.getWEBCLIENTCSS();
                String copyWEBCLIENTCSS = ((String) strategy.copy(LocatorUtils.property(locator, "webclientcss", sourceWEBCLIENTCSS), sourceWEBCLIENTCSS));
                copy.setWEBCLIENTCSS(copyWEBCLIENTCSS);
            } else {
                copy.webclientcss = null;
            }
            if (this.sortcalculatedattributes!= null) {
                Boolean sourceSORTCALCULATEDATTRIBUTES;
                sourceSORTCALCULATEDATTRIBUTES = this.isSORTCALCULATEDATTRIBUTES();
                Boolean copySORTCALCULATEDATTRIBUTES = ((Boolean) strategy.copy(LocatorUtils.property(locator, "sortcalculatedattributes", sourceSORTCALCULATEDATTRIBUTES), sourceSORTCALCULATEDATTRIBUTES));
                copy.setSORTCALCULATEDATTRIBUTES(copySORTCALCULATEDATTRIBUTES);
            } else {
                copy.sortcalculatedattributes = null;
            }
            if (this.confirmreplacingdocuments!= null) {
                Boolean sourceCONFIRMREPLACINGDOCUMENTS;
                sourceCONFIRMREPLACINGDOCUMENTS = this.isCONFIRMREPLACINGDOCUMENTS();
                Boolean copyCONFIRMREPLACINGDOCUMENTS = ((Boolean) strategy.copy(LocatorUtils.property(locator, "confirmreplacingdocuments", sourceCONFIRMREPLACINGDOCUMENTS), sourceCONFIRMREPLACINGDOCUMENTS));
                copy.setCONFIRMREPLACINGDOCUMENTS(copyCONFIRMREPLACINGDOCUMENTS);
            } else {
                copy.confirmreplacingdocuments = null;
            }
            if (this.securitypasswordstrengthdescription!= null) {
                String sourceSECURITYPASSWORDSTRENGTHDESCRIPTION;
                sourceSECURITYPASSWORDSTRENGTHDESCRIPTION = this.getSECURITYPASSWORDSTRENGTHDESCRIPTION();
                String copySECURITYPASSWORDSTRENGTHDESCRIPTION = ((String) strategy.copy(LocatorUtils.property(locator, "securitypasswordstrengthdescription", sourceSECURITYPASSWORDSTRENGTHDESCRIPTION), sourceSECURITYPASSWORDSTRENGTHDESCRIPTION));
                copy.setSECURITYPASSWORDSTRENGTHDESCRIPTION(copySECURITYPASSWORDSTRENGTHDESCRIPTION);
            } else {
                copy.securitypasswordstrengthdescription = null;
            }
            if (this.defaultdashboardtasklistopennewtab!= null) {
                String sourceDEFAULTDASHBOARDTASKLISTOPENNEWTAB;
                sourceDEFAULTDASHBOARDTASKLISTOPENNEWTAB = this.getDEFAULTDASHBOARDTASKLISTOPENNEWTAB();
                String copyDEFAULTDASHBOARDTASKLISTOPENNEWTAB = ((String) strategy.copy(LocatorUtils.property(locator, "defaultdashboardtasklistopennewtab", sourceDEFAULTDASHBOARDTASKLISTOPENNEWTAB), sourceDEFAULTDASHBOARDTASKLISTOPENNEWTAB));
                copy.setDEFAULTDASHBOARDTASKLISTOPENNEWTAB(copyDEFAULTDASHBOARDTASKLISTOPENNEWTAB);
            } else {
                copy.defaultdashboardtasklistopennewtab = null;
            }
            if (this.addstartomandatoryfieldlabel!= null) {
                String sourceADDSTARTOMANDATORYFIELDLABEL;
                sourceADDSTARTOMANDATORYFIELDLABEL = this.getADDSTARTOMANDATORYFIELDLABEL();
                String copyADDSTARTOMANDATORYFIELDLABEL = ((String) strategy.copy(LocatorUtils.property(locator, "addstartomandatoryfieldlabel", sourceADDSTARTOMANDATORYFIELDLABEL), sourceADDSTARTOMANDATORYFIELDLABEL));
                copy.setADDSTARTOMANDATORYFIELDLABEL(copyADDSTARTOMANDATORYFIELDLABEL);
            } else {
                copy.addstartomandatoryfieldlabel = null;
            }
            if (this.securitypasswordbasedauthenticationallowed!= null) {
                Boolean sourceSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED;
                sourceSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED = this.isSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED();
                Boolean copySECURITYPASSWORDBASEDAUTHENTICATIONALLOWED = ((Boolean) strategy.copy(LocatorUtils.property(locator, "securitypasswordbasedauthenticationallowed", sourceSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED), sourceSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED));
                copy.setSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED(copySECURITYPASSWORDBASEDAUTHENTICATIONALLOWED);
            } else {
                copy.securitypasswordbasedauthenticationallowed = null;
            }
            if (this.rootpanebackgroundcolor!= null) {
                String sourceROOTPANEBACKGROUNDCOLOR;
                sourceROOTPANEBACKGROUNDCOLOR = this.getROOTPANEBACKGROUNDCOLOR();
                String copyROOTPANEBACKGROUNDCOLOR = ((String) strategy.copy(LocatorUtils.property(locator, "rootpanebackgroundcolor", sourceROOTPANEBACKGROUNDCOLOR), sourceROOTPANEBACKGROUNDCOLOR));
                copy.setROOTPANEBACKGROUNDCOLOR(copyROOTPANEBACKGROUNDCOLOR);
            } else {
                copy.rootpanebackgroundcolor = null;
            }
            if (this.rootpanesubackgroundcolor!= null) {
                String sourceROOTPANESUBACKGROUNDCOLOR;
                sourceROOTPANESUBACKGROUNDCOLOR = this.getROOTPANESUBACKGROUNDCOLOR();
                String copyROOTPANESUBACKGROUNDCOLOR = ((String) strategy.copy(LocatorUtils.property(locator, "rootpanesubackgroundcolor", sourceROOTPANESUBACKGROUNDCOLOR), sourceROOTPANESUBACKGROUNDCOLOR));
                copy.setROOTPANESUBACKGROUNDCOLOR(copyROOTPANESUBACKGROUNDCOLOR);
            } else {
                copy.rootpanesubackgroundcolor = null;
            }
            if (this.usernamedescription!= null) {
                String sourceUSERNAMEDESCRIPTION;
                sourceUSERNAMEDESCRIPTION = this.getUSERNAMEDESCRIPTION();
                String copyUSERNAMEDESCRIPTION = ((String) strategy.copy(LocatorUtils.property(locator, "usernamedescription", sourceUSERNAMEDESCRIPTION), sourceUSERNAMEDESCRIPTION));
                copy.setUSERNAMEDESCRIPTION(copyUSERNAMEDESCRIPTION);
            } else {
                copy.usernamedescription = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new RestSystemparameters();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestSystemparameters.Builder<_B> _other) {
        _other.anonymoususeraccessenabled = this.anonymoususeraccessenabled;
        _other.environmentdevelopment = this.environmentdevelopment;
        _other.forgotlogindetailsenabled = this.forgotlogindetailsenabled;
        _other.fulltextsearchenabled = this.fulltextsearchenabled;
        _other.functionblockdev = this.functionblockdev;
        _other.quicksearchdelaytime = this.quicksearchdelaytime;
        _other.userregistrationenabled = this.userregistrationenabled;
        _other.appname = this.appname;
        _other.webclienttitletemplate = this.webclienttitletemplate;
        _other.webclientcss = this.webclientcss;
        _other.sortcalculatedattributes = this.sortcalculatedattributes;
        _other.confirmreplacingdocuments = this.confirmreplacingdocuments;
        _other.securitypasswordstrengthdescription = this.securitypasswordstrengthdescription;
        _other.defaultdashboardtasklistopennewtab = this.defaultdashboardtasklistopennewtab;
        _other.addstartomandatoryfieldlabel = this.addstartomandatoryfieldlabel;
        _other.securitypasswordbasedauthenticationallowed = this.securitypasswordbasedauthenticationallowed;
        _other.rootpanebackgroundcolor = this.rootpanebackgroundcolor;
        _other.rootpanesubackgroundcolor = this.rootpanesubackgroundcolor;
        _other.usernamedescription = this.usernamedescription;
    }

    public<_B >RestSystemparameters.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new RestSystemparameters.Builder<_B>(_parentBuilder, this, true);
    }

    public RestSystemparameters.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static RestSystemparameters.Builder<Void> builder() {
        return new RestSystemparameters.Builder<Void>(null, null, false);
    }

    public static<_B >RestSystemparameters.Builder<_B> copyOf(final RestSystemparameters _other) {
        final RestSystemparameters.Builder<_B> _newBuilder = new RestSystemparameters.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final RestSystemparameters.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree anonymoususeraccessenabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("anonymoususeraccessenabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(anonymoususeraccessenabledPropertyTree!= null):((anonymoususeraccessenabledPropertyTree == null)||(!anonymoususeraccessenabledPropertyTree.isLeaf())))) {
            _other.anonymoususeraccessenabled = this.anonymoususeraccessenabled;
        }
        final PropertyTree environmentdevelopmentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("environmentdevelopment"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(environmentdevelopmentPropertyTree!= null):((environmentdevelopmentPropertyTree == null)||(!environmentdevelopmentPropertyTree.isLeaf())))) {
            _other.environmentdevelopment = this.environmentdevelopment;
        }
        final PropertyTree forgotlogindetailsenabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("forgotlogindetailsenabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(forgotlogindetailsenabledPropertyTree!= null):((forgotlogindetailsenabledPropertyTree == null)||(!forgotlogindetailsenabledPropertyTree.isLeaf())))) {
            _other.forgotlogindetailsenabled = this.forgotlogindetailsenabled;
        }
        final PropertyTree fulltextsearchenabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fulltextsearchenabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fulltextsearchenabledPropertyTree!= null):((fulltextsearchenabledPropertyTree == null)||(!fulltextsearchenabledPropertyTree.isLeaf())))) {
            _other.fulltextsearchenabled = this.fulltextsearchenabled;
        }
        final PropertyTree functionblockdevPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("functionblockdev"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(functionblockdevPropertyTree!= null):((functionblockdevPropertyTree == null)||(!functionblockdevPropertyTree.isLeaf())))) {
            _other.functionblockdev = this.functionblockdev;
        }
        final PropertyTree quicksearchdelaytimePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("quicksearchdelaytime"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(quicksearchdelaytimePropertyTree!= null):((quicksearchdelaytimePropertyTree == null)||(!quicksearchdelaytimePropertyTree.isLeaf())))) {
            _other.quicksearchdelaytime = this.quicksearchdelaytime;
        }
        final PropertyTree userregistrationenabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("userregistrationenabled"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(userregistrationenabledPropertyTree!= null):((userregistrationenabledPropertyTree == null)||(!userregistrationenabledPropertyTree.isLeaf())))) {
            _other.userregistrationenabled = this.userregistrationenabled;
        }
        final PropertyTree appnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("appname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(appnamePropertyTree!= null):((appnamePropertyTree == null)||(!appnamePropertyTree.isLeaf())))) {
            _other.appname = this.appname;
        }
        final PropertyTree webclienttitletemplatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("webclienttitletemplate"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(webclienttitletemplatePropertyTree!= null):((webclienttitletemplatePropertyTree == null)||(!webclienttitletemplatePropertyTree.isLeaf())))) {
            _other.webclienttitletemplate = this.webclienttitletemplate;
        }
        final PropertyTree webclientcssPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("webclientcss"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(webclientcssPropertyTree!= null):((webclientcssPropertyTree == null)||(!webclientcssPropertyTree.isLeaf())))) {
            _other.webclientcss = this.webclientcss;
        }
        final PropertyTree sortcalculatedattributesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sortcalculatedattributes"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sortcalculatedattributesPropertyTree!= null):((sortcalculatedattributesPropertyTree == null)||(!sortcalculatedattributesPropertyTree.isLeaf())))) {
            _other.sortcalculatedattributes = this.sortcalculatedattributes;
        }
        final PropertyTree confirmreplacingdocumentsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("confirmreplacingdocuments"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(confirmreplacingdocumentsPropertyTree!= null):((confirmreplacingdocumentsPropertyTree == null)||(!confirmreplacingdocumentsPropertyTree.isLeaf())))) {
            _other.confirmreplacingdocuments = this.confirmreplacingdocuments;
        }
        final PropertyTree securitypasswordstrengthdescriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("securitypasswordstrengthdescription"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(securitypasswordstrengthdescriptionPropertyTree!= null):((securitypasswordstrengthdescriptionPropertyTree == null)||(!securitypasswordstrengthdescriptionPropertyTree.isLeaf())))) {
            _other.securitypasswordstrengthdescription = this.securitypasswordstrengthdescription;
        }
        final PropertyTree defaultdashboardtasklistopennewtabPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("defaultdashboardtasklistopennewtab"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(defaultdashboardtasklistopennewtabPropertyTree!= null):((defaultdashboardtasklistopennewtabPropertyTree == null)||(!defaultdashboardtasklistopennewtabPropertyTree.isLeaf())))) {
            _other.defaultdashboardtasklistopennewtab = this.defaultdashboardtasklistopennewtab;
        }
        final PropertyTree addstartomandatoryfieldlabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("addstartomandatoryfieldlabel"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(addstartomandatoryfieldlabelPropertyTree!= null):((addstartomandatoryfieldlabelPropertyTree == null)||(!addstartomandatoryfieldlabelPropertyTree.isLeaf())))) {
            _other.addstartomandatoryfieldlabel = this.addstartomandatoryfieldlabel;
        }
        final PropertyTree securitypasswordbasedauthenticationallowedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("securitypasswordbasedauthenticationallowed"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(securitypasswordbasedauthenticationallowedPropertyTree!= null):((securitypasswordbasedauthenticationallowedPropertyTree == null)||(!securitypasswordbasedauthenticationallowedPropertyTree.isLeaf())))) {
            _other.securitypasswordbasedauthenticationallowed = this.securitypasswordbasedauthenticationallowed;
        }
        final PropertyTree rootpanebackgroundcolorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rootpanebackgroundcolor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rootpanebackgroundcolorPropertyTree!= null):((rootpanebackgroundcolorPropertyTree == null)||(!rootpanebackgroundcolorPropertyTree.isLeaf())))) {
            _other.rootpanebackgroundcolor = this.rootpanebackgroundcolor;
        }
        final PropertyTree rootpanesubackgroundcolorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rootpanesubackgroundcolor"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rootpanesubackgroundcolorPropertyTree!= null):((rootpanesubackgroundcolorPropertyTree == null)||(!rootpanesubackgroundcolorPropertyTree.isLeaf())))) {
            _other.rootpanesubackgroundcolor = this.rootpanesubackgroundcolor;
        }
        final PropertyTree usernamedescriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("usernamedescription"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(usernamedescriptionPropertyTree!= null):((usernamedescriptionPropertyTree == null)||(!usernamedescriptionPropertyTree.isLeaf())))) {
            _other.usernamedescription = this.usernamedescription;
        }
    }

    public<_B >RestSystemparameters.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new RestSystemparameters.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public RestSystemparameters.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >RestSystemparameters.Builder<_B> copyOf(final RestSystemparameters _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final RestSystemparameters.Builder<_B> _newBuilder = new RestSystemparameters.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static RestSystemparameters.Builder<Void> copyExcept(final RestSystemparameters _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static RestSystemparameters.Builder<Void> copyOnly(final RestSystemparameters _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final RestSystemparameters _storedValue;
        private Boolean anonymoususeraccessenabled;
        private Boolean environmentdevelopment;
        private Boolean forgotlogindetailsenabled;
        private Boolean fulltextsearchenabled;
        private Boolean functionblockdev;
        private Integer quicksearchdelaytime;
        private Boolean userregistrationenabled;
        private String appname;
        private String webclienttitletemplate;
        private String webclientcss;
        private Boolean sortcalculatedattributes;
        private Boolean confirmreplacingdocuments;
        private String securitypasswordstrengthdescription;
        private String defaultdashboardtasklistopennewtab;
        private String addstartomandatoryfieldlabel;
        private Boolean securitypasswordbasedauthenticationallowed;
        private String rootpanebackgroundcolor;
        private String rootpanesubackgroundcolor;
        private String usernamedescription;

        public Builder(final _B _parentBuilder, final RestSystemparameters _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.anonymoususeraccessenabled = _other.anonymoususeraccessenabled;
                    this.environmentdevelopment = _other.environmentdevelopment;
                    this.forgotlogindetailsenabled = _other.forgotlogindetailsenabled;
                    this.fulltextsearchenabled = _other.fulltextsearchenabled;
                    this.functionblockdev = _other.functionblockdev;
                    this.quicksearchdelaytime = _other.quicksearchdelaytime;
                    this.userregistrationenabled = _other.userregistrationenabled;
                    this.appname = _other.appname;
                    this.webclienttitletemplate = _other.webclienttitletemplate;
                    this.webclientcss = _other.webclientcss;
                    this.sortcalculatedattributes = _other.sortcalculatedattributes;
                    this.confirmreplacingdocuments = _other.confirmreplacingdocuments;
                    this.securitypasswordstrengthdescription = _other.securitypasswordstrengthdescription;
                    this.defaultdashboardtasklistopennewtab = _other.defaultdashboardtasklistopennewtab;
                    this.addstartomandatoryfieldlabel = _other.addstartomandatoryfieldlabel;
                    this.securitypasswordbasedauthenticationallowed = _other.securitypasswordbasedauthenticationallowed;
                    this.rootpanebackgroundcolor = _other.rootpanebackgroundcolor;
                    this.rootpanesubackgroundcolor = _other.rootpanesubackgroundcolor;
                    this.usernamedescription = _other.usernamedescription;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final RestSystemparameters _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree anonymoususeraccessenabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("anonymoususeraccessenabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(anonymoususeraccessenabledPropertyTree!= null):((anonymoususeraccessenabledPropertyTree == null)||(!anonymoususeraccessenabledPropertyTree.isLeaf())))) {
                        this.anonymoususeraccessenabled = _other.anonymoususeraccessenabled;
                    }
                    final PropertyTree environmentdevelopmentPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("environmentdevelopment"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(environmentdevelopmentPropertyTree!= null):((environmentdevelopmentPropertyTree == null)||(!environmentdevelopmentPropertyTree.isLeaf())))) {
                        this.environmentdevelopment = _other.environmentdevelopment;
                    }
                    final PropertyTree forgotlogindetailsenabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("forgotlogindetailsenabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(forgotlogindetailsenabledPropertyTree!= null):((forgotlogindetailsenabledPropertyTree == null)||(!forgotlogindetailsenabledPropertyTree.isLeaf())))) {
                        this.forgotlogindetailsenabled = _other.forgotlogindetailsenabled;
                    }
                    final PropertyTree fulltextsearchenabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fulltextsearchenabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fulltextsearchenabledPropertyTree!= null):((fulltextsearchenabledPropertyTree == null)||(!fulltextsearchenabledPropertyTree.isLeaf())))) {
                        this.fulltextsearchenabled = _other.fulltextsearchenabled;
                    }
                    final PropertyTree functionblockdevPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("functionblockdev"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(functionblockdevPropertyTree!= null):((functionblockdevPropertyTree == null)||(!functionblockdevPropertyTree.isLeaf())))) {
                        this.functionblockdev = _other.functionblockdev;
                    }
                    final PropertyTree quicksearchdelaytimePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("quicksearchdelaytime"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(quicksearchdelaytimePropertyTree!= null):((quicksearchdelaytimePropertyTree == null)||(!quicksearchdelaytimePropertyTree.isLeaf())))) {
                        this.quicksearchdelaytime = _other.quicksearchdelaytime;
                    }
                    final PropertyTree userregistrationenabledPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("userregistrationenabled"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(userregistrationenabledPropertyTree!= null):((userregistrationenabledPropertyTree == null)||(!userregistrationenabledPropertyTree.isLeaf())))) {
                        this.userregistrationenabled = _other.userregistrationenabled;
                    }
                    final PropertyTree appnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("appname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(appnamePropertyTree!= null):((appnamePropertyTree == null)||(!appnamePropertyTree.isLeaf())))) {
                        this.appname = _other.appname;
                    }
                    final PropertyTree webclienttitletemplatePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("webclienttitletemplate"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(webclienttitletemplatePropertyTree!= null):((webclienttitletemplatePropertyTree == null)||(!webclienttitletemplatePropertyTree.isLeaf())))) {
                        this.webclienttitletemplate = _other.webclienttitletemplate;
                    }
                    final PropertyTree webclientcssPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("webclientcss"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(webclientcssPropertyTree!= null):((webclientcssPropertyTree == null)||(!webclientcssPropertyTree.isLeaf())))) {
                        this.webclientcss = _other.webclientcss;
                    }
                    final PropertyTree sortcalculatedattributesPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("sortcalculatedattributes"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(sortcalculatedattributesPropertyTree!= null):((sortcalculatedattributesPropertyTree == null)||(!sortcalculatedattributesPropertyTree.isLeaf())))) {
                        this.sortcalculatedattributes = _other.sortcalculatedattributes;
                    }
                    final PropertyTree confirmreplacingdocumentsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("confirmreplacingdocuments"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(confirmreplacingdocumentsPropertyTree!= null):((confirmreplacingdocumentsPropertyTree == null)||(!confirmreplacingdocumentsPropertyTree.isLeaf())))) {
                        this.confirmreplacingdocuments = _other.confirmreplacingdocuments;
                    }
                    final PropertyTree securitypasswordstrengthdescriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("securitypasswordstrengthdescription"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(securitypasswordstrengthdescriptionPropertyTree!= null):((securitypasswordstrengthdescriptionPropertyTree == null)||(!securitypasswordstrengthdescriptionPropertyTree.isLeaf())))) {
                        this.securitypasswordstrengthdescription = _other.securitypasswordstrengthdescription;
                    }
                    final PropertyTree defaultdashboardtasklistopennewtabPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("defaultdashboardtasklistopennewtab"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(defaultdashboardtasklistopennewtabPropertyTree!= null):((defaultdashboardtasklistopennewtabPropertyTree == null)||(!defaultdashboardtasklistopennewtabPropertyTree.isLeaf())))) {
                        this.defaultdashboardtasklistopennewtab = _other.defaultdashboardtasklistopennewtab;
                    }
                    final PropertyTree addstartomandatoryfieldlabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("addstartomandatoryfieldlabel"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(addstartomandatoryfieldlabelPropertyTree!= null):((addstartomandatoryfieldlabelPropertyTree == null)||(!addstartomandatoryfieldlabelPropertyTree.isLeaf())))) {
                        this.addstartomandatoryfieldlabel = _other.addstartomandatoryfieldlabel;
                    }
                    final PropertyTree securitypasswordbasedauthenticationallowedPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("securitypasswordbasedauthenticationallowed"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(securitypasswordbasedauthenticationallowedPropertyTree!= null):((securitypasswordbasedauthenticationallowedPropertyTree == null)||(!securitypasswordbasedauthenticationallowedPropertyTree.isLeaf())))) {
                        this.securitypasswordbasedauthenticationallowed = _other.securitypasswordbasedauthenticationallowed;
                    }
                    final PropertyTree rootpanebackgroundcolorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rootpanebackgroundcolor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rootpanebackgroundcolorPropertyTree!= null):((rootpanebackgroundcolorPropertyTree == null)||(!rootpanebackgroundcolorPropertyTree.isLeaf())))) {
                        this.rootpanebackgroundcolor = _other.rootpanebackgroundcolor;
                    }
                    final PropertyTree rootpanesubackgroundcolorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("rootpanesubackgroundcolor"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(rootpanesubackgroundcolorPropertyTree!= null):((rootpanesubackgroundcolorPropertyTree == null)||(!rootpanesubackgroundcolorPropertyTree.isLeaf())))) {
                        this.rootpanesubackgroundcolor = _other.rootpanesubackgroundcolor;
                    }
                    final PropertyTree usernamedescriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("usernamedescription"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(usernamedescriptionPropertyTree!= null):((usernamedescriptionPropertyTree == null)||(!usernamedescriptionPropertyTree.isLeaf())))) {
                        this.usernamedescription = _other.usernamedescription;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends RestSystemparameters >_P init(final _P _product) {
            _product.anonymoususeraccessenabled = this.anonymoususeraccessenabled;
            _product.environmentdevelopment = this.environmentdevelopment;
            _product.forgotlogindetailsenabled = this.forgotlogindetailsenabled;
            _product.fulltextsearchenabled = this.fulltextsearchenabled;
            _product.functionblockdev = this.functionblockdev;
            _product.quicksearchdelaytime = this.quicksearchdelaytime;
            _product.userregistrationenabled = this.userregistrationenabled;
            _product.appname = this.appname;
            _product.webclienttitletemplate = this.webclienttitletemplate;
            _product.webclientcss = this.webclientcss;
            _product.sortcalculatedattributes = this.sortcalculatedattributes;
            _product.confirmreplacingdocuments = this.confirmreplacingdocuments;
            _product.securitypasswordstrengthdescription = this.securitypasswordstrengthdescription;
            _product.defaultdashboardtasklistopennewtab = this.defaultdashboardtasklistopennewtab;
            _product.addstartomandatoryfieldlabel = this.addstartomandatoryfieldlabel;
            _product.securitypasswordbasedauthenticationallowed = this.securitypasswordbasedauthenticationallowed;
            _product.rootpanebackgroundcolor = this.rootpanebackgroundcolor;
            _product.rootpanesubackgroundcolor = this.rootpanesubackgroundcolor;
            _product.usernamedescription = this.usernamedescription;
            return _product;
        }

        /**
         * Sets the new value of "anonymoususeraccessenabled" (any previous value will be replaced)
         * 
         * @param anonymoususeraccessenabled
         *     New value of the "anonymoususeraccessenabled" property.
         */
        public RestSystemparameters.Builder<_B> withANONYMOUSUSERACCESSENABLED(final Boolean anonymoususeraccessenabled) {
            this.anonymoususeraccessenabled = anonymoususeraccessenabled;
            return this;
        }

        /**
         * Sets the new value of "environmentdevelopment" (any previous value will be replaced)
         * 
         * @param environmentdevelopment
         *     New value of the "environmentdevelopment" property.
         */
        public RestSystemparameters.Builder<_B> withENVIRONMENTDEVELOPMENT(final Boolean environmentdevelopment) {
            this.environmentdevelopment = environmentdevelopment;
            return this;
        }

        /**
         * Sets the new value of "forgotlogindetailsenabled" (any previous value will be replaced)
         * 
         * @param forgotlogindetailsenabled
         *     New value of the "forgotlogindetailsenabled" property.
         */
        public RestSystemparameters.Builder<_B> withFORGOTLOGINDETAILSENABLED(final Boolean forgotlogindetailsenabled) {
            this.forgotlogindetailsenabled = forgotlogindetailsenabled;
            return this;
        }

        /**
         * Sets the new value of "fulltextsearchenabled" (any previous value will be replaced)
         * 
         * @param fulltextsearchenabled
         *     New value of the "fulltextsearchenabled" property.
         */
        public RestSystemparameters.Builder<_B> withFULLTEXTSEARCHENABLED(final Boolean fulltextsearchenabled) {
            this.fulltextsearchenabled = fulltextsearchenabled;
            return this;
        }

        /**
         * Sets the new value of "functionblockdev" (any previous value will be replaced)
         * 
         * @param functionblockdev
         *     New value of the "functionblockdev" property.
         */
        public RestSystemparameters.Builder<_B> withFUNCTIONBLOCKDEV(final Boolean functionblockdev) {
            this.functionblockdev = functionblockdev;
            return this;
        }

        /**
         * Sets the new value of "quicksearchdelaytime" (any previous value will be replaced)
         * 
         * @param quicksearchdelaytime
         *     New value of the "quicksearchdelaytime" property.
         */
        public RestSystemparameters.Builder<_B> withQUICKSEARCHDELAYTIME(final Integer quicksearchdelaytime) {
            this.quicksearchdelaytime = quicksearchdelaytime;
            return this;
        }

        /**
         * Sets the new value of "userregistrationenabled" (any previous value will be replaced)
         * 
         * @param userregistrationenabled
         *     New value of the "userregistrationenabled" property.
         */
        public RestSystemparameters.Builder<_B> withUSERREGISTRATIONENABLED(final Boolean userregistrationenabled) {
            this.userregistrationenabled = userregistrationenabled;
            return this;
        }

        /**
         * Sets the new value of "appname" (any previous value will be replaced)
         * 
         * @param appname
         *     New value of the "appname" property.
         */
        public RestSystemparameters.Builder<_B> withAPPNAME(final String appname) {
            this.appname = appname;
            return this;
        }

        /**
         * Sets the new value of "webclienttitletemplate" (any previous value will be replaced)
         * 
         * @param webclienttitletemplate
         *     New value of the "webclienttitletemplate" property.
         */
        public RestSystemparameters.Builder<_B> withWEBCLIENTTITLETEMPLATE(final String webclienttitletemplate) {
            this.webclienttitletemplate = webclienttitletemplate;
            return this;
        }

        /**
         * Sets the new value of "webclientcss" (any previous value will be replaced)
         * 
         * @param webclientcss
         *     New value of the "webclientcss" property.
         */
        public RestSystemparameters.Builder<_B> withWEBCLIENTCSS(final String webclientcss) {
            this.webclientcss = webclientcss;
            return this;
        }

        /**
         * Sets the new value of "sortcalculatedattributes" (any previous value will be replaced)
         * 
         * @param sortcalculatedattributes
         *     New value of the "sortcalculatedattributes" property.
         */
        public RestSystemparameters.Builder<_B> withSORTCALCULATEDATTRIBUTES(final Boolean sortcalculatedattributes) {
            this.sortcalculatedattributes = sortcalculatedattributes;
            return this;
        }

        /**
         * Sets the new value of "confirmreplacingdocuments" (any previous value will be replaced)
         * 
         * @param confirmreplacingdocuments
         *     New value of the "confirmreplacingdocuments" property.
         */
        public RestSystemparameters.Builder<_B> withCONFIRMREPLACINGDOCUMENTS(final Boolean confirmreplacingdocuments) {
            this.confirmreplacingdocuments = confirmreplacingdocuments;
            return this;
        }

        /**
         * Sets the new value of "securitypasswordstrengthdescription" (any previous value will be replaced)
         * 
         * @param securitypasswordstrengthdescription
         *     New value of the "securitypasswordstrengthdescription" property.
         */
        public RestSystemparameters.Builder<_B> withSECURITYPASSWORDSTRENGTHDESCRIPTION(final String securitypasswordstrengthdescription) {
            this.securitypasswordstrengthdescription = securitypasswordstrengthdescription;
            return this;
        }

        /**
         * Sets the new value of "defaultdashboardtasklistopennewtab" (any previous value will be replaced)
         * 
         * @param defaultdashboardtasklistopennewtab
         *     New value of the "defaultdashboardtasklistopennewtab" property.
         */
        public RestSystemparameters.Builder<_B> withDEFAULTDASHBOARDTASKLISTOPENNEWTAB(final String defaultdashboardtasklistopennewtab) {
            this.defaultdashboardtasklistopennewtab = defaultdashboardtasklistopennewtab;
            return this;
        }

        /**
         * Sets the new value of "addstartomandatoryfieldlabel" (any previous value will be replaced)
         * 
         * @param addstartomandatoryfieldlabel
         *     New value of the "addstartomandatoryfieldlabel" property.
         */
        public RestSystemparameters.Builder<_B> withADDSTARTOMANDATORYFIELDLABEL(final String addstartomandatoryfieldlabel) {
            this.addstartomandatoryfieldlabel = addstartomandatoryfieldlabel;
            return this;
        }

        /**
         * Sets the new value of "securitypasswordbasedauthenticationallowed" (any previous value will be replaced)
         * 
         * @param securitypasswordbasedauthenticationallowed
         *     New value of the "securitypasswordbasedauthenticationallowed" property.
         */
        public RestSystemparameters.Builder<_B> withSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED(final Boolean securitypasswordbasedauthenticationallowed) {
            this.securitypasswordbasedauthenticationallowed = securitypasswordbasedauthenticationallowed;
            return this;
        }

        /**
         * Sets the new value of "rootpanebackgroundcolor" (any previous value will be replaced)
         * 
         * @param rootpanebackgroundcolor
         *     New value of the "rootpanebackgroundcolor" property.
         */
        public RestSystemparameters.Builder<_B> withROOTPANEBACKGROUNDCOLOR(final String rootpanebackgroundcolor) {
            this.rootpanebackgroundcolor = rootpanebackgroundcolor;
            return this;
        }

        /**
         * Sets the new value of "rootpanesubackgroundcolor" (any previous value will be replaced)
         * 
         * @param rootpanesubackgroundcolor
         *     New value of the "rootpanesubackgroundcolor" property.
         */
        public RestSystemparameters.Builder<_B> withROOTPANESUBACKGROUNDCOLOR(final String rootpanesubackgroundcolor) {
            this.rootpanesubackgroundcolor = rootpanesubackgroundcolor;
            return this;
        }

        /**
         * Sets the new value of "usernamedescription" (any previous value will be replaced)
         * 
         * @param usernamedescription
         *     New value of the "usernamedescription" property.
         */
        public RestSystemparameters.Builder<_B> withUSERNAMEDESCRIPTION(final String usernamedescription) {
            this.usernamedescription = usernamedescription;
            return this;
        }

        @Override
        public RestSystemparameters build() {
            if (_storedValue == null) {
                return this.init(new RestSystemparameters());
            } else {
                return ((RestSystemparameters) _storedValue);
            }
        }

        public RestSystemparameters.Builder<_B> copyOf(final RestSystemparameters _other) {
            _other.copyTo(this);
            return this;
        }

        public RestSystemparameters.Builder<_B> copyOf(final RestSystemparameters.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends RestSystemparameters.Selector<RestSystemparameters.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static RestSystemparameters.Select _root() {
            return new RestSystemparameters.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> anonymoususeraccessenabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> environmentdevelopment = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> forgotlogindetailsenabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> fulltextsearchenabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> functionblockdev = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> quicksearchdelaytime = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> userregistrationenabled = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> appname = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> webclienttitletemplate = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> webclientcss = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> sortcalculatedattributes = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> confirmreplacingdocuments = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> securitypasswordstrengthdescription = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> defaultdashboardtasklistopennewtab = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> addstartomandatoryfieldlabel = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> securitypasswordbasedauthenticationallowed = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> rootpanebackgroundcolor = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> rootpanesubackgroundcolor = null;
        private com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> usernamedescription = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.anonymoususeraccessenabled!= null) {
                products.put("anonymoususeraccessenabled", this.anonymoususeraccessenabled.init());
            }
            if (this.environmentdevelopment!= null) {
                products.put("environmentdevelopment", this.environmentdevelopment.init());
            }
            if (this.forgotlogindetailsenabled!= null) {
                products.put("forgotlogindetailsenabled", this.forgotlogindetailsenabled.init());
            }
            if (this.fulltextsearchenabled!= null) {
                products.put("fulltextsearchenabled", this.fulltextsearchenabled.init());
            }
            if (this.functionblockdev!= null) {
                products.put("functionblockdev", this.functionblockdev.init());
            }
            if (this.quicksearchdelaytime!= null) {
                products.put("quicksearchdelaytime", this.quicksearchdelaytime.init());
            }
            if (this.userregistrationenabled!= null) {
                products.put("userregistrationenabled", this.userregistrationenabled.init());
            }
            if (this.appname!= null) {
                products.put("appname", this.appname.init());
            }
            if (this.webclienttitletemplate!= null) {
                products.put("webclienttitletemplate", this.webclienttitletemplate.init());
            }
            if (this.webclientcss!= null) {
                products.put("webclientcss", this.webclientcss.init());
            }
            if (this.sortcalculatedattributes!= null) {
                products.put("sortcalculatedattributes", this.sortcalculatedattributes.init());
            }
            if (this.confirmreplacingdocuments!= null) {
                products.put("confirmreplacingdocuments", this.confirmreplacingdocuments.init());
            }
            if (this.securitypasswordstrengthdescription!= null) {
                products.put("securitypasswordstrengthdescription", this.securitypasswordstrengthdescription.init());
            }
            if (this.defaultdashboardtasklistopennewtab!= null) {
                products.put("defaultdashboardtasklistopennewtab", this.defaultdashboardtasklistopennewtab.init());
            }
            if (this.addstartomandatoryfieldlabel!= null) {
                products.put("addstartomandatoryfieldlabel", this.addstartomandatoryfieldlabel.init());
            }
            if (this.securitypasswordbasedauthenticationallowed!= null) {
                products.put("securitypasswordbasedauthenticationallowed", this.securitypasswordbasedauthenticationallowed.init());
            }
            if (this.rootpanebackgroundcolor!= null) {
                products.put("rootpanebackgroundcolor", this.rootpanebackgroundcolor.init());
            }
            if (this.rootpanesubackgroundcolor!= null) {
                products.put("rootpanesubackgroundcolor", this.rootpanesubackgroundcolor.init());
            }
            if (this.usernamedescription!= null) {
                products.put("usernamedescription", this.usernamedescription.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> anonymoususeraccessenabled() {
            return ((this.anonymoususeraccessenabled == null)?this.anonymoususeraccessenabled = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "anonymoususeraccessenabled"):this.anonymoususeraccessenabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> environmentdevelopment() {
            return ((this.environmentdevelopment == null)?this.environmentdevelopment = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "environmentdevelopment"):this.environmentdevelopment);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> forgotlogindetailsenabled() {
            return ((this.forgotlogindetailsenabled == null)?this.forgotlogindetailsenabled = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "forgotlogindetailsenabled"):this.forgotlogindetailsenabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> fulltextsearchenabled() {
            return ((this.fulltextsearchenabled == null)?this.fulltextsearchenabled = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "fulltextsearchenabled"):this.fulltextsearchenabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> functionblockdev() {
            return ((this.functionblockdev == null)?this.functionblockdev = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "functionblockdev"):this.functionblockdev);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> quicksearchdelaytime() {
            return ((this.quicksearchdelaytime == null)?this.quicksearchdelaytime = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "quicksearchdelaytime"):this.quicksearchdelaytime);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> userregistrationenabled() {
            return ((this.userregistrationenabled == null)?this.userregistrationenabled = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "userregistrationenabled"):this.userregistrationenabled);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> appname() {
            return ((this.appname == null)?this.appname = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "appname"):this.appname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> webclienttitletemplate() {
            return ((this.webclienttitletemplate == null)?this.webclienttitletemplate = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "webclienttitletemplate"):this.webclienttitletemplate);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> webclientcss() {
            return ((this.webclientcss == null)?this.webclientcss = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "webclientcss"):this.webclientcss);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> sortcalculatedattributes() {
            return ((this.sortcalculatedattributes == null)?this.sortcalculatedattributes = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "sortcalculatedattributes"):this.sortcalculatedattributes);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> confirmreplacingdocuments() {
            return ((this.confirmreplacingdocuments == null)?this.confirmreplacingdocuments = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "confirmreplacingdocuments"):this.confirmreplacingdocuments);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> securitypasswordstrengthdescription() {
            return ((this.securitypasswordstrengthdescription == null)?this.securitypasswordstrengthdescription = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "securitypasswordstrengthdescription"):this.securitypasswordstrengthdescription);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> defaultdashboardtasklistopennewtab() {
            return ((this.defaultdashboardtasklistopennewtab == null)?this.defaultdashboardtasklistopennewtab = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "defaultdashboardtasklistopennewtab"):this.defaultdashboardtasklistopennewtab);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> addstartomandatoryfieldlabel() {
            return ((this.addstartomandatoryfieldlabel == null)?this.addstartomandatoryfieldlabel = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "addstartomandatoryfieldlabel"):this.addstartomandatoryfieldlabel);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> securitypasswordbasedauthenticationallowed() {
            return ((this.securitypasswordbasedauthenticationallowed == null)?this.securitypasswordbasedauthenticationallowed = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "securitypasswordbasedauthenticationallowed"):this.securitypasswordbasedauthenticationallowed);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> rootpanebackgroundcolor() {
            return ((this.rootpanebackgroundcolor == null)?this.rootpanebackgroundcolor = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "rootpanebackgroundcolor"):this.rootpanebackgroundcolor);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> rootpanesubackgroundcolor() {
            return ((this.rootpanesubackgroundcolor == null)?this.rootpanesubackgroundcolor = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "rootpanesubackgroundcolor"):this.rootpanesubackgroundcolor);
        }

        public com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>> usernamedescription() {
            return ((this.usernamedescription == null)?this.usernamedescription = new com.kscs.util.jaxb.Selector<TRoot, RestSystemparameters.Selector<TRoot, TParent>>(this._root, this, "usernamedescription"):this.usernamedescription);
        }

    }

}
