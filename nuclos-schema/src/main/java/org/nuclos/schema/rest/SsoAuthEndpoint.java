package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for sso-auth-endpoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sso-auth-endpoint"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="ssoAuthId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="label" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="imageIcon" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="fontAwesomeIcon" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sso-auth-endpoint")
public class SsoAuthEndpoint implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ssoAuthId", required = true)
    protected String ssoAuthId;
    @XmlAttribute(name = "label", required = true)
    protected String label;
    @XmlAttribute(name = "color")
    protected String color;
    @XmlAttribute(name = "imageIcon")
    protected String imageIcon;
    @XmlAttribute(name = "fontAwesomeIcon")
    protected String fontAwesomeIcon;

    /**
     * Gets the value of the ssoAuthId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSsoAuthId() {
        return ssoAuthId;
    }

    /**
     * Sets the value of the ssoAuthId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSsoAuthId(String value) {
        this.ssoAuthId = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the color property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the value of the color property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    /**
     * Gets the value of the imageIcon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImageIcon() {
        return imageIcon;
    }

    /**
     * Sets the value of the imageIcon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImageIcon(String value) {
        this.imageIcon = value;
    }

    /**
     * Gets the value of the fontAwesomeIcon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFontAwesomeIcon() {
        return fontAwesomeIcon;
    }

    /**
     * Sets the value of the fontAwesomeIcon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFontAwesomeIcon(String value) {
        this.fontAwesomeIcon = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theSsoAuthId;
            theSsoAuthId = this.getSsoAuthId();
            strategy.appendField(locator, this, "ssoAuthId", buffer, theSsoAuthId);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            strategy.appendField(locator, this, "label", buffer, theLabel);
        }
        {
            String theColor;
            theColor = this.getColor();
            strategy.appendField(locator, this, "color", buffer, theColor);
        }
        {
            String theImageIcon;
            theImageIcon = this.getImageIcon();
            strategy.appendField(locator, this, "imageIcon", buffer, theImageIcon);
        }
        {
            String theFontAwesomeIcon;
            theFontAwesomeIcon = this.getFontAwesomeIcon();
            strategy.appendField(locator, this, "fontAwesomeIcon", buffer, theFontAwesomeIcon);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof SsoAuthEndpoint)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final SsoAuthEndpoint that = ((SsoAuthEndpoint) object);
        {
            String lhsSsoAuthId;
            lhsSsoAuthId = this.getSsoAuthId();
            String rhsSsoAuthId;
            rhsSsoAuthId = that.getSsoAuthId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ssoAuthId", lhsSsoAuthId), LocatorUtils.property(thatLocator, "ssoAuthId", rhsSsoAuthId), lhsSsoAuthId, rhsSsoAuthId)) {
                return false;
            }
        }
        {
            String lhsLabel;
            lhsLabel = this.getLabel();
            String rhsLabel;
            rhsLabel = that.getLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "label", lhsLabel), LocatorUtils.property(thatLocator, "label", rhsLabel), lhsLabel, rhsLabel)) {
                return false;
            }
        }
        {
            String lhsColor;
            lhsColor = this.getColor();
            String rhsColor;
            rhsColor = that.getColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "color", lhsColor), LocatorUtils.property(thatLocator, "color", rhsColor), lhsColor, rhsColor)) {
                return false;
            }
        }
        {
            String lhsImageIcon;
            lhsImageIcon = this.getImageIcon();
            String rhsImageIcon;
            rhsImageIcon = that.getImageIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "imageIcon", lhsImageIcon), LocatorUtils.property(thatLocator, "imageIcon", rhsImageIcon), lhsImageIcon, rhsImageIcon)) {
                return false;
            }
        }
        {
            String lhsFontAwesomeIcon;
            lhsFontAwesomeIcon = this.getFontAwesomeIcon();
            String rhsFontAwesomeIcon;
            rhsFontAwesomeIcon = that.getFontAwesomeIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fontAwesomeIcon", lhsFontAwesomeIcon), LocatorUtils.property(thatLocator, "fontAwesomeIcon", rhsFontAwesomeIcon), lhsFontAwesomeIcon, rhsFontAwesomeIcon)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theSsoAuthId;
            theSsoAuthId = this.getSsoAuthId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ssoAuthId", theSsoAuthId), currentHashCode, theSsoAuthId);
        }
        {
            String theLabel;
            theLabel = this.getLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "label", theLabel), currentHashCode, theLabel);
        }
        {
            String theColor;
            theColor = this.getColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "color", theColor), currentHashCode, theColor);
        }
        {
            String theImageIcon;
            theImageIcon = this.getImageIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "imageIcon", theImageIcon), currentHashCode, theImageIcon);
        }
        {
            String theFontAwesomeIcon;
            theFontAwesomeIcon = this.getFontAwesomeIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fontAwesomeIcon", theFontAwesomeIcon), currentHashCode, theFontAwesomeIcon);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof SsoAuthEndpoint) {
            final SsoAuthEndpoint copy = ((SsoAuthEndpoint) draftCopy);
            if (this.ssoAuthId!= null) {
                String sourceSsoAuthId;
                sourceSsoAuthId = this.getSsoAuthId();
                String copySsoAuthId = ((String) strategy.copy(LocatorUtils.property(locator, "ssoAuthId", sourceSsoAuthId), sourceSsoAuthId));
                copy.setSsoAuthId(copySsoAuthId);
            } else {
                copy.ssoAuthId = null;
            }
            if (this.label!= null) {
                String sourceLabel;
                sourceLabel = this.getLabel();
                String copyLabel = ((String) strategy.copy(LocatorUtils.property(locator, "label", sourceLabel), sourceLabel));
                copy.setLabel(copyLabel);
            } else {
                copy.label = null;
            }
            if (this.color!= null) {
                String sourceColor;
                sourceColor = this.getColor();
                String copyColor = ((String) strategy.copy(LocatorUtils.property(locator, "color", sourceColor), sourceColor));
                copy.setColor(copyColor);
            } else {
                copy.color = null;
            }
            if (this.imageIcon!= null) {
                String sourceImageIcon;
                sourceImageIcon = this.getImageIcon();
                String copyImageIcon = ((String) strategy.copy(LocatorUtils.property(locator, "imageIcon", sourceImageIcon), sourceImageIcon));
                copy.setImageIcon(copyImageIcon);
            } else {
                copy.imageIcon = null;
            }
            if (this.fontAwesomeIcon!= null) {
                String sourceFontAwesomeIcon;
                sourceFontAwesomeIcon = this.getFontAwesomeIcon();
                String copyFontAwesomeIcon = ((String) strategy.copy(LocatorUtils.property(locator, "fontAwesomeIcon", sourceFontAwesomeIcon), sourceFontAwesomeIcon));
                copy.setFontAwesomeIcon(copyFontAwesomeIcon);
            } else {
                copy.fontAwesomeIcon = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new SsoAuthEndpoint();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SsoAuthEndpoint.Builder<_B> _other) {
        _other.ssoAuthId = this.ssoAuthId;
        _other.label = this.label;
        _other.color = this.color;
        _other.imageIcon = this.imageIcon;
        _other.fontAwesomeIcon = this.fontAwesomeIcon;
    }

    public<_B >SsoAuthEndpoint.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new SsoAuthEndpoint.Builder<_B>(_parentBuilder, this, true);
    }

    public SsoAuthEndpoint.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static SsoAuthEndpoint.Builder<Void> builder() {
        return new SsoAuthEndpoint.Builder<Void>(null, null, false);
    }

    public static<_B >SsoAuthEndpoint.Builder<_B> copyOf(final SsoAuthEndpoint _other) {
        final SsoAuthEndpoint.Builder<_B> _newBuilder = new SsoAuthEndpoint.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final SsoAuthEndpoint.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree ssoAuthIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ssoAuthId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ssoAuthIdPropertyTree!= null):((ssoAuthIdPropertyTree == null)||(!ssoAuthIdPropertyTree.isLeaf())))) {
            _other.ssoAuthId = this.ssoAuthId;
        }
        final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
            _other.label = this.label;
        }
        final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
            _other.color = this.color;
        }
        final PropertyTree imageIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("imageIcon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(imageIconPropertyTree!= null):((imageIconPropertyTree == null)||(!imageIconPropertyTree.isLeaf())))) {
            _other.imageIcon = this.imageIcon;
        }
        final PropertyTree fontAwesomeIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fontAwesomeIcon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontAwesomeIconPropertyTree!= null):((fontAwesomeIconPropertyTree == null)||(!fontAwesomeIconPropertyTree.isLeaf())))) {
            _other.fontAwesomeIcon = this.fontAwesomeIcon;
        }
    }

    public<_B >SsoAuthEndpoint.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new SsoAuthEndpoint.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public SsoAuthEndpoint.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >SsoAuthEndpoint.Builder<_B> copyOf(final SsoAuthEndpoint _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final SsoAuthEndpoint.Builder<_B> _newBuilder = new SsoAuthEndpoint.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static SsoAuthEndpoint.Builder<Void> copyExcept(final SsoAuthEndpoint _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static SsoAuthEndpoint.Builder<Void> copyOnly(final SsoAuthEndpoint _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final SsoAuthEndpoint _storedValue;
        private String ssoAuthId;
        private String label;
        private String color;
        private String imageIcon;
        private String fontAwesomeIcon;

        public Builder(final _B _parentBuilder, final SsoAuthEndpoint _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.ssoAuthId = _other.ssoAuthId;
                    this.label = _other.label;
                    this.color = _other.color;
                    this.imageIcon = _other.imageIcon;
                    this.fontAwesomeIcon = _other.fontAwesomeIcon;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final SsoAuthEndpoint _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree ssoAuthIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("ssoAuthId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(ssoAuthIdPropertyTree!= null):((ssoAuthIdPropertyTree == null)||(!ssoAuthIdPropertyTree.isLeaf())))) {
                        this.ssoAuthId = _other.ssoAuthId;
                    }
                    final PropertyTree labelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("label"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(labelPropertyTree!= null):((labelPropertyTree == null)||(!labelPropertyTree.isLeaf())))) {
                        this.label = _other.label;
                    }
                    final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
                        this.color = _other.color;
                    }
                    final PropertyTree imageIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("imageIcon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(imageIconPropertyTree!= null):((imageIconPropertyTree == null)||(!imageIconPropertyTree.isLeaf())))) {
                        this.imageIcon = _other.imageIcon;
                    }
                    final PropertyTree fontAwesomeIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fontAwesomeIcon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fontAwesomeIconPropertyTree!= null):((fontAwesomeIconPropertyTree == null)||(!fontAwesomeIconPropertyTree.isLeaf())))) {
                        this.fontAwesomeIcon = _other.fontAwesomeIcon;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends SsoAuthEndpoint >_P init(final _P _product) {
            _product.ssoAuthId = this.ssoAuthId;
            _product.label = this.label;
            _product.color = this.color;
            _product.imageIcon = this.imageIcon;
            _product.fontAwesomeIcon = this.fontAwesomeIcon;
            return _product;
        }

        /**
         * Sets the new value of "ssoAuthId" (any previous value will be replaced)
         * 
         * @param ssoAuthId
         *     New value of the "ssoAuthId" property.
         */
        public SsoAuthEndpoint.Builder<_B> withSsoAuthId(final String ssoAuthId) {
            this.ssoAuthId = ssoAuthId;
            return this;
        }

        /**
         * Sets the new value of "label" (any previous value will be replaced)
         * 
         * @param label
         *     New value of the "label" property.
         */
        public SsoAuthEndpoint.Builder<_B> withLabel(final String label) {
            this.label = label;
            return this;
        }

        /**
         * Sets the new value of "color" (any previous value will be replaced)
         * 
         * @param color
         *     New value of the "color" property.
         */
        public SsoAuthEndpoint.Builder<_B> withColor(final String color) {
            this.color = color;
            return this;
        }

        /**
         * Sets the new value of "imageIcon" (any previous value will be replaced)
         * 
         * @param imageIcon
         *     New value of the "imageIcon" property.
         */
        public SsoAuthEndpoint.Builder<_B> withImageIcon(final String imageIcon) {
            this.imageIcon = imageIcon;
            return this;
        }

        /**
         * Sets the new value of "fontAwesomeIcon" (any previous value will be replaced)
         * 
         * @param fontAwesomeIcon
         *     New value of the "fontAwesomeIcon" property.
         */
        public SsoAuthEndpoint.Builder<_B> withFontAwesomeIcon(final String fontAwesomeIcon) {
            this.fontAwesomeIcon = fontAwesomeIcon;
            return this;
        }

        @Override
        public SsoAuthEndpoint build() {
            if (_storedValue == null) {
                return this.init(new SsoAuthEndpoint());
            } else {
                return ((SsoAuthEndpoint) _storedValue);
            }
        }

        public SsoAuthEndpoint.Builder<_B> copyOf(final SsoAuthEndpoint _other) {
            _other.copyTo(this);
            return this;
        }

        public SsoAuthEndpoint.Builder<_B> copyOf(final SsoAuthEndpoint.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends SsoAuthEndpoint.Selector<SsoAuthEndpoint.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static SsoAuthEndpoint.Select _root() {
            return new SsoAuthEndpoint.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>> ssoAuthId = null;
        private com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>> label = null;
        private com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>> color = null;
        private com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>> imageIcon = null;
        private com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>> fontAwesomeIcon = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.ssoAuthId!= null) {
                products.put("ssoAuthId", this.ssoAuthId.init());
            }
            if (this.label!= null) {
                products.put("label", this.label.init());
            }
            if (this.color!= null) {
                products.put("color", this.color.init());
            }
            if (this.imageIcon!= null) {
                products.put("imageIcon", this.imageIcon.init());
            }
            if (this.fontAwesomeIcon!= null) {
                products.put("fontAwesomeIcon", this.fontAwesomeIcon.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>> ssoAuthId() {
            return ((this.ssoAuthId == null)?this.ssoAuthId = new com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>>(this._root, this, "ssoAuthId"):this.ssoAuthId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>> label() {
            return ((this.label == null)?this.label = new com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>>(this._root, this, "label"):this.label);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>> color() {
            return ((this.color == null)?this.color = new com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>>(this._root, this, "color"):this.color);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>> imageIcon() {
            return ((this.imageIcon == null)?this.imageIcon = new com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>>(this._root, this, "imageIcon"):this.imageIcon);
        }

        public com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>> fontAwesomeIcon() {
            return ((this.fontAwesomeIcon == null)?this.fontAwesomeIcon = new com.kscs.util.jaxb.Selector<TRoot, SsoAuthEndpoint.Selector<TRoot, TParent>>(this._root, this, "fontAwesomeIcon"):this.fontAwesomeIcon);
        }

    }

}
