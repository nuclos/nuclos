package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for status-info complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="status-info"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="stateId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="statename" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="numeral" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="color" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="buttonIcon" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="buttonLabel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="nonstop" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="transitionLabel" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="transitionDescription" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="wayBack" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "status-info")
public class StatusInfo implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "stateId")
    protected String stateId;
    @XmlAttribute(name = "statename")
    protected String statename;
    @XmlAttribute(name = "numeral")
    protected Long numeral;
    @XmlAttribute(name = "description")
    protected String description;
    @XmlAttribute(name = "color")
    protected String color;
    @XmlAttribute(name = "buttonIcon")
    protected String buttonIcon;
    @XmlAttribute(name = "buttonLabel")
    protected String buttonLabel;
    @XmlAttribute(name = "nonstop")
    protected Boolean nonstop;
    @XmlAttribute(name = "transitionLabel")
    protected String transitionLabel;
    @XmlAttribute(name = "transitionDescription")
    protected String transitionDescription;
    @XmlAttribute(name = "wayBack")
    protected Boolean wayBack;

    /**
     * Gets the value of the stateId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateId() {
        return stateId;
    }

    /**
     * Sets the value of the stateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateId(String value) {
        this.stateId = value;
    }

    /**
     * Gets the value of the statename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatename() {
        return statename;
    }

    /**
     * Sets the value of the statename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatename(String value) {
        this.statename = value;
    }

    /**
     * Gets the value of the numeral property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeral() {
        return numeral;
    }

    /**
     * Sets the value of the numeral property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeral(Long value) {
        this.numeral = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the color property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the value of the color property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColor(String value) {
        this.color = value;
    }

    /**
     * Gets the value of the buttonIcon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getButtonIcon() {
        return buttonIcon;
    }

    /**
     * Sets the value of the buttonIcon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setButtonIcon(String value) {
        this.buttonIcon = value;
    }

    /**
     * Gets the value of the buttonLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getButtonLabel() {
        return buttonLabel;
    }

    /**
     * Sets the value of the buttonLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setButtonLabel(String value) {
        this.buttonLabel = value;
    }

    /**
     * Gets the value of the nonstop property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNonstop() {
        return nonstop;
    }

    /**
     * Sets the value of the nonstop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNonstop(Boolean value) {
        this.nonstop = value;
    }

    /**
     * Gets the value of the transitionLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransitionLabel() {
        return transitionLabel;
    }

    /**
     * Sets the value of the transitionLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransitionLabel(String value) {
        this.transitionLabel = value;
    }

    /**
     * Gets the value of the transitionDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransitionDescription() {
        return transitionDescription;
    }

    /**
     * Sets the value of the transitionDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransitionDescription(String value) {
        this.transitionDescription = value;
    }

    /**
     * Gets the value of the wayBack property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWayBack() {
        return wayBack;
    }

    /**
     * Sets the value of the wayBack property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWayBack(Boolean value) {
        this.wayBack = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theStateId;
            theStateId = this.getStateId();
            strategy.appendField(locator, this, "stateId", buffer, theStateId);
        }
        {
            String theStatename;
            theStatename = this.getStatename();
            strategy.appendField(locator, this, "statename", buffer, theStatename);
        }
        {
            Long theNumeral;
            theNumeral = this.getNumeral();
            strategy.appendField(locator, this, "numeral", buffer, theNumeral);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            String theColor;
            theColor = this.getColor();
            strategy.appendField(locator, this, "color", buffer, theColor);
        }
        {
            String theButtonIcon;
            theButtonIcon = this.getButtonIcon();
            strategy.appendField(locator, this, "buttonIcon", buffer, theButtonIcon);
        }
        {
            String theButtonLabel;
            theButtonLabel = this.getButtonLabel();
            strategy.appendField(locator, this, "buttonLabel", buffer, theButtonLabel);
        }
        {
            Boolean theNonstop;
            theNonstop = this.isNonstop();
            strategy.appendField(locator, this, "nonstop", buffer, theNonstop);
        }
        {
            String theTransitionLabel;
            theTransitionLabel = this.getTransitionLabel();
            strategy.appendField(locator, this, "transitionLabel", buffer, theTransitionLabel);
        }
        {
            String theTransitionDescription;
            theTransitionDescription = this.getTransitionDescription();
            strategy.appendField(locator, this, "transitionDescription", buffer, theTransitionDescription);
        }
        {
            Boolean theWayBack;
            theWayBack = this.isWayBack();
            strategy.appendField(locator, this, "wayBack", buffer, theWayBack);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof StatusInfo)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final StatusInfo that = ((StatusInfo) object);
        {
            String lhsStateId;
            lhsStateId = this.getStateId();
            String rhsStateId;
            rhsStateId = that.getStateId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "stateId", lhsStateId), LocatorUtils.property(thatLocator, "stateId", rhsStateId), lhsStateId, rhsStateId)) {
                return false;
            }
        }
        {
            String lhsStatename;
            lhsStatename = this.getStatename();
            String rhsStatename;
            rhsStatename = that.getStatename();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "statename", lhsStatename), LocatorUtils.property(thatLocator, "statename", rhsStatename), lhsStatename, rhsStatename)) {
                return false;
            }
        }
        {
            Long lhsNumeral;
            lhsNumeral = this.getNumeral();
            Long rhsNumeral;
            rhsNumeral = that.getNumeral();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "numeral", lhsNumeral), LocatorUtils.property(thatLocator, "numeral", rhsNumeral), lhsNumeral, rhsNumeral)) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            String lhsColor;
            lhsColor = this.getColor();
            String rhsColor;
            rhsColor = that.getColor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "color", lhsColor), LocatorUtils.property(thatLocator, "color", rhsColor), lhsColor, rhsColor)) {
                return false;
            }
        }
        {
            String lhsButtonIcon;
            lhsButtonIcon = this.getButtonIcon();
            String rhsButtonIcon;
            rhsButtonIcon = that.getButtonIcon();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "buttonIcon", lhsButtonIcon), LocatorUtils.property(thatLocator, "buttonIcon", rhsButtonIcon), lhsButtonIcon, rhsButtonIcon)) {
                return false;
            }
        }
        {
            String lhsButtonLabel;
            lhsButtonLabel = this.getButtonLabel();
            String rhsButtonLabel;
            rhsButtonLabel = that.getButtonLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "buttonLabel", lhsButtonLabel), LocatorUtils.property(thatLocator, "buttonLabel", rhsButtonLabel), lhsButtonLabel, rhsButtonLabel)) {
                return false;
            }
        }
        {
            Boolean lhsNonstop;
            lhsNonstop = this.isNonstop();
            Boolean rhsNonstop;
            rhsNonstop = that.isNonstop();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nonstop", lhsNonstop), LocatorUtils.property(thatLocator, "nonstop", rhsNonstop), lhsNonstop, rhsNonstop)) {
                return false;
            }
        }
        {
            String lhsTransitionLabel;
            lhsTransitionLabel = this.getTransitionLabel();
            String rhsTransitionLabel;
            rhsTransitionLabel = that.getTransitionLabel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transitionLabel", lhsTransitionLabel), LocatorUtils.property(thatLocator, "transitionLabel", rhsTransitionLabel), lhsTransitionLabel, rhsTransitionLabel)) {
                return false;
            }
        }
        {
            String lhsTransitionDescription;
            lhsTransitionDescription = this.getTransitionDescription();
            String rhsTransitionDescription;
            rhsTransitionDescription = that.getTransitionDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "transitionDescription", lhsTransitionDescription), LocatorUtils.property(thatLocator, "transitionDescription", rhsTransitionDescription), lhsTransitionDescription, rhsTransitionDescription)) {
                return false;
            }
        }
        {
            Boolean lhsWayBack;
            lhsWayBack = this.isWayBack();
            Boolean rhsWayBack;
            rhsWayBack = that.isWayBack();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "wayBack", lhsWayBack), LocatorUtils.property(thatLocator, "wayBack", rhsWayBack), lhsWayBack, rhsWayBack)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theStateId;
            theStateId = this.getStateId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "stateId", theStateId), currentHashCode, theStateId);
        }
        {
            String theStatename;
            theStatename = this.getStatename();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "statename", theStatename), currentHashCode, theStatename);
        }
        {
            Long theNumeral;
            theNumeral = this.getNumeral();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "numeral", theNumeral), currentHashCode, theNumeral);
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            String theColor;
            theColor = this.getColor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "color", theColor), currentHashCode, theColor);
        }
        {
            String theButtonIcon;
            theButtonIcon = this.getButtonIcon();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "buttonIcon", theButtonIcon), currentHashCode, theButtonIcon);
        }
        {
            String theButtonLabel;
            theButtonLabel = this.getButtonLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "buttonLabel", theButtonLabel), currentHashCode, theButtonLabel);
        }
        {
            Boolean theNonstop;
            theNonstop = this.isNonstop();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nonstop", theNonstop), currentHashCode, theNonstop);
        }
        {
            String theTransitionLabel;
            theTransitionLabel = this.getTransitionLabel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "transitionLabel", theTransitionLabel), currentHashCode, theTransitionLabel);
        }
        {
            String theTransitionDescription;
            theTransitionDescription = this.getTransitionDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "transitionDescription", theTransitionDescription), currentHashCode, theTransitionDescription);
        }
        {
            Boolean theWayBack;
            theWayBack = this.isWayBack();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "wayBack", theWayBack), currentHashCode, theWayBack);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof StatusInfo) {
            final StatusInfo copy = ((StatusInfo) draftCopy);
            if (this.stateId!= null) {
                String sourceStateId;
                sourceStateId = this.getStateId();
                String copyStateId = ((String) strategy.copy(LocatorUtils.property(locator, "stateId", sourceStateId), sourceStateId));
                copy.setStateId(copyStateId);
            } else {
                copy.stateId = null;
            }
            if (this.statename!= null) {
                String sourceStatename;
                sourceStatename = this.getStatename();
                String copyStatename = ((String) strategy.copy(LocatorUtils.property(locator, "statename", sourceStatename), sourceStatename));
                copy.setStatename(copyStatename);
            } else {
                copy.statename = null;
            }
            if (this.numeral!= null) {
                Long sourceNumeral;
                sourceNumeral = this.getNumeral();
                Long copyNumeral = ((Long) strategy.copy(LocatorUtils.property(locator, "numeral", sourceNumeral), sourceNumeral));
                copy.setNumeral(copyNumeral);
            } else {
                copy.numeral = null;
            }
            if (this.description!= null) {
                String sourceDescription;
                sourceDescription = this.getDescription();
                String copyDescription = ((String) strategy.copy(LocatorUtils.property(locator, "description", sourceDescription), sourceDescription));
                copy.setDescription(copyDescription);
            } else {
                copy.description = null;
            }
            if (this.color!= null) {
                String sourceColor;
                sourceColor = this.getColor();
                String copyColor = ((String) strategy.copy(LocatorUtils.property(locator, "color", sourceColor), sourceColor));
                copy.setColor(copyColor);
            } else {
                copy.color = null;
            }
            if (this.buttonIcon!= null) {
                String sourceButtonIcon;
                sourceButtonIcon = this.getButtonIcon();
                String copyButtonIcon = ((String) strategy.copy(LocatorUtils.property(locator, "buttonIcon", sourceButtonIcon), sourceButtonIcon));
                copy.setButtonIcon(copyButtonIcon);
            } else {
                copy.buttonIcon = null;
            }
            if (this.buttonLabel!= null) {
                String sourceButtonLabel;
                sourceButtonLabel = this.getButtonLabel();
                String copyButtonLabel = ((String) strategy.copy(LocatorUtils.property(locator, "buttonLabel", sourceButtonLabel), sourceButtonLabel));
                copy.setButtonLabel(copyButtonLabel);
            } else {
                copy.buttonLabel = null;
            }
            if (this.nonstop!= null) {
                Boolean sourceNonstop;
                sourceNonstop = this.isNonstop();
                Boolean copyNonstop = ((Boolean) strategy.copy(LocatorUtils.property(locator, "nonstop", sourceNonstop), sourceNonstop));
                copy.setNonstop(copyNonstop);
            } else {
                copy.nonstop = null;
            }
            if (this.transitionLabel!= null) {
                String sourceTransitionLabel;
                sourceTransitionLabel = this.getTransitionLabel();
                String copyTransitionLabel = ((String) strategy.copy(LocatorUtils.property(locator, "transitionLabel", sourceTransitionLabel), sourceTransitionLabel));
                copy.setTransitionLabel(copyTransitionLabel);
            } else {
                copy.transitionLabel = null;
            }
            if (this.transitionDescription!= null) {
                String sourceTransitionDescription;
                sourceTransitionDescription = this.getTransitionDescription();
                String copyTransitionDescription = ((String) strategy.copy(LocatorUtils.property(locator, "transitionDescription", sourceTransitionDescription), sourceTransitionDescription));
                copy.setTransitionDescription(copyTransitionDescription);
            } else {
                copy.transitionDescription = null;
            }
            if (this.wayBack!= null) {
                Boolean sourceWayBack;
                sourceWayBack = this.isWayBack();
                Boolean copyWayBack = ((Boolean) strategy.copy(LocatorUtils.property(locator, "wayBack", sourceWayBack), sourceWayBack));
                copy.setWayBack(copyWayBack);
            } else {
                copy.wayBack = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new StatusInfo();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final StatusInfo.Builder<_B> _other) {
        _other.stateId = this.stateId;
        _other.statename = this.statename;
        _other.numeral = this.numeral;
        _other.description = this.description;
        _other.color = this.color;
        _other.buttonIcon = this.buttonIcon;
        _other.buttonLabel = this.buttonLabel;
        _other.nonstop = this.nonstop;
        _other.transitionLabel = this.transitionLabel;
        _other.transitionDescription = this.transitionDescription;
        _other.wayBack = this.wayBack;
    }

    public<_B >StatusInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new StatusInfo.Builder<_B>(_parentBuilder, this, true);
    }

    public StatusInfo.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static StatusInfo.Builder<Void> builder() {
        return new StatusInfo.Builder<Void>(null, null, false);
    }

    public static<_B >StatusInfo.Builder<_B> copyOf(final StatusInfo _other) {
        final StatusInfo.Builder<_B> _newBuilder = new StatusInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final StatusInfo.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree stateIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("stateId"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(stateIdPropertyTree!= null):((stateIdPropertyTree == null)||(!stateIdPropertyTree.isLeaf())))) {
            _other.stateId = this.stateId;
        }
        final PropertyTree statenamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("statename"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(statenamePropertyTree!= null):((statenamePropertyTree == null)||(!statenamePropertyTree.isLeaf())))) {
            _other.statename = this.statename;
        }
        final PropertyTree numeralPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("numeral"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(numeralPropertyTree!= null):((numeralPropertyTree == null)||(!numeralPropertyTree.isLeaf())))) {
            _other.numeral = this.numeral;
        }
        final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
            _other.description = this.description;
        }
        final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
            _other.color = this.color;
        }
        final PropertyTree buttonIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("buttonIcon"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(buttonIconPropertyTree!= null):((buttonIconPropertyTree == null)||(!buttonIconPropertyTree.isLeaf())))) {
            _other.buttonIcon = this.buttonIcon;
        }
        final PropertyTree buttonLabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("buttonLabel"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(buttonLabelPropertyTree!= null):((buttonLabelPropertyTree == null)||(!buttonLabelPropertyTree.isLeaf())))) {
            _other.buttonLabel = this.buttonLabel;
        }
        final PropertyTree nonstopPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nonstop"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nonstopPropertyTree!= null):((nonstopPropertyTree == null)||(!nonstopPropertyTree.isLeaf())))) {
            _other.nonstop = this.nonstop;
        }
        final PropertyTree transitionLabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("transitionLabel"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(transitionLabelPropertyTree!= null):((transitionLabelPropertyTree == null)||(!transitionLabelPropertyTree.isLeaf())))) {
            _other.transitionLabel = this.transitionLabel;
        }
        final PropertyTree transitionDescriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("transitionDescription"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(transitionDescriptionPropertyTree!= null):((transitionDescriptionPropertyTree == null)||(!transitionDescriptionPropertyTree.isLeaf())))) {
            _other.transitionDescription = this.transitionDescription;
        }
        final PropertyTree wayBackPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("wayBack"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(wayBackPropertyTree!= null):((wayBackPropertyTree == null)||(!wayBackPropertyTree.isLeaf())))) {
            _other.wayBack = this.wayBack;
        }
    }

    public<_B >StatusInfo.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new StatusInfo.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public StatusInfo.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >StatusInfo.Builder<_B> copyOf(final StatusInfo _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final StatusInfo.Builder<_B> _newBuilder = new StatusInfo.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static StatusInfo.Builder<Void> copyExcept(final StatusInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static StatusInfo.Builder<Void> copyOnly(final StatusInfo _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final StatusInfo _storedValue;
        private String stateId;
        private String statename;
        private Long numeral;
        private String description;
        private String color;
        private String buttonIcon;
        private String buttonLabel;
        private Boolean nonstop;
        private String transitionLabel;
        private String transitionDescription;
        private Boolean wayBack;

        public Builder(final _B _parentBuilder, final StatusInfo _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.stateId = _other.stateId;
                    this.statename = _other.statename;
                    this.numeral = _other.numeral;
                    this.description = _other.description;
                    this.color = _other.color;
                    this.buttonIcon = _other.buttonIcon;
                    this.buttonLabel = _other.buttonLabel;
                    this.nonstop = _other.nonstop;
                    this.transitionLabel = _other.transitionLabel;
                    this.transitionDescription = _other.transitionDescription;
                    this.wayBack = _other.wayBack;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final StatusInfo _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree stateIdPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("stateId"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(stateIdPropertyTree!= null):((stateIdPropertyTree == null)||(!stateIdPropertyTree.isLeaf())))) {
                        this.stateId = _other.stateId;
                    }
                    final PropertyTree statenamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("statename"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(statenamePropertyTree!= null):((statenamePropertyTree == null)||(!statenamePropertyTree.isLeaf())))) {
                        this.statename = _other.statename;
                    }
                    final PropertyTree numeralPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("numeral"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(numeralPropertyTree!= null):((numeralPropertyTree == null)||(!numeralPropertyTree.isLeaf())))) {
                        this.numeral = _other.numeral;
                    }
                    final PropertyTree descriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("description"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(descriptionPropertyTree!= null):((descriptionPropertyTree == null)||(!descriptionPropertyTree.isLeaf())))) {
                        this.description = _other.description;
                    }
                    final PropertyTree colorPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("color"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(colorPropertyTree!= null):((colorPropertyTree == null)||(!colorPropertyTree.isLeaf())))) {
                        this.color = _other.color;
                    }
                    final PropertyTree buttonIconPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("buttonIcon"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(buttonIconPropertyTree!= null):((buttonIconPropertyTree == null)||(!buttonIconPropertyTree.isLeaf())))) {
                        this.buttonIcon = _other.buttonIcon;
                    }
                    final PropertyTree buttonLabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("buttonLabel"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(buttonLabelPropertyTree!= null):((buttonLabelPropertyTree == null)||(!buttonLabelPropertyTree.isLeaf())))) {
                        this.buttonLabel = _other.buttonLabel;
                    }
                    final PropertyTree nonstopPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("nonstop"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(nonstopPropertyTree!= null):((nonstopPropertyTree == null)||(!nonstopPropertyTree.isLeaf())))) {
                        this.nonstop = _other.nonstop;
                    }
                    final PropertyTree transitionLabelPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("transitionLabel"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(transitionLabelPropertyTree!= null):((transitionLabelPropertyTree == null)||(!transitionLabelPropertyTree.isLeaf())))) {
                        this.transitionLabel = _other.transitionLabel;
                    }
                    final PropertyTree transitionDescriptionPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("transitionDescription"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(transitionDescriptionPropertyTree!= null):((transitionDescriptionPropertyTree == null)||(!transitionDescriptionPropertyTree.isLeaf())))) {
                        this.transitionDescription = _other.transitionDescription;
                    }
                    final PropertyTree wayBackPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("wayBack"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(wayBackPropertyTree!= null):((wayBackPropertyTree == null)||(!wayBackPropertyTree.isLeaf())))) {
                        this.wayBack = _other.wayBack;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends StatusInfo >_P init(final _P _product) {
            _product.stateId = this.stateId;
            _product.statename = this.statename;
            _product.numeral = this.numeral;
            _product.description = this.description;
            _product.color = this.color;
            _product.buttonIcon = this.buttonIcon;
            _product.buttonLabel = this.buttonLabel;
            _product.nonstop = this.nonstop;
            _product.transitionLabel = this.transitionLabel;
            _product.transitionDescription = this.transitionDescription;
            _product.wayBack = this.wayBack;
            return _product;
        }

        /**
         * Sets the new value of "stateId" (any previous value will be replaced)
         * 
         * @param stateId
         *     New value of the "stateId" property.
         */
        public StatusInfo.Builder<_B> withStateId(final String stateId) {
            this.stateId = stateId;
            return this;
        }

        /**
         * Sets the new value of "statename" (any previous value will be replaced)
         * 
         * @param statename
         *     New value of the "statename" property.
         */
        public StatusInfo.Builder<_B> withStatename(final String statename) {
            this.statename = statename;
            return this;
        }

        /**
         * Sets the new value of "numeral" (any previous value will be replaced)
         * 
         * @param numeral
         *     New value of the "numeral" property.
         */
        public StatusInfo.Builder<_B> withNumeral(final Long numeral) {
            this.numeral = numeral;
            return this;
        }

        /**
         * Sets the new value of "description" (any previous value will be replaced)
         * 
         * @param description
         *     New value of the "description" property.
         */
        public StatusInfo.Builder<_B> withDescription(final String description) {
            this.description = description;
            return this;
        }

        /**
         * Sets the new value of "color" (any previous value will be replaced)
         * 
         * @param color
         *     New value of the "color" property.
         */
        public StatusInfo.Builder<_B> withColor(final String color) {
            this.color = color;
            return this;
        }

        /**
         * Sets the new value of "buttonIcon" (any previous value will be replaced)
         * 
         * @param buttonIcon
         *     New value of the "buttonIcon" property.
         */
        public StatusInfo.Builder<_B> withButtonIcon(final String buttonIcon) {
            this.buttonIcon = buttonIcon;
            return this;
        }

        /**
         * Sets the new value of "buttonLabel" (any previous value will be replaced)
         * 
         * @param buttonLabel
         *     New value of the "buttonLabel" property.
         */
        public StatusInfo.Builder<_B> withButtonLabel(final String buttonLabel) {
            this.buttonLabel = buttonLabel;
            return this;
        }

        /**
         * Sets the new value of "nonstop" (any previous value will be replaced)
         * 
         * @param nonstop
         *     New value of the "nonstop" property.
         */
        public StatusInfo.Builder<_B> withNonstop(final Boolean nonstop) {
            this.nonstop = nonstop;
            return this;
        }

        /**
         * Sets the new value of "transitionLabel" (any previous value will be replaced)
         * 
         * @param transitionLabel
         *     New value of the "transitionLabel" property.
         */
        public StatusInfo.Builder<_B> withTransitionLabel(final String transitionLabel) {
            this.transitionLabel = transitionLabel;
            return this;
        }

        /**
         * Sets the new value of "transitionDescription" (any previous value will be replaced)
         * 
         * @param transitionDescription
         *     New value of the "transitionDescription" property.
         */
        public StatusInfo.Builder<_B> withTransitionDescription(final String transitionDescription) {
            this.transitionDescription = transitionDescription;
            return this;
        }

        /**
         * Sets the new value of "wayBack" (any previous value will be replaced)
         * 
         * @param wayBack
         *     New value of the "wayBack" property.
         */
        public StatusInfo.Builder<_B> withWayBack(final Boolean wayBack) {
            this.wayBack = wayBack;
            return this;
        }

        @Override
        public StatusInfo build() {
            if (_storedValue == null) {
                return this.init(new StatusInfo());
            } else {
                return ((StatusInfo) _storedValue);
            }
        }

        public StatusInfo.Builder<_B> copyOf(final StatusInfo _other) {
            _other.copyTo(this);
            return this;
        }

        public StatusInfo.Builder<_B> copyOf(final StatusInfo.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends StatusInfo.Selector<StatusInfo.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static StatusInfo.Select _root() {
            return new StatusInfo.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> stateId = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> statename = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> numeral = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> description = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> color = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> buttonIcon = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> buttonLabel = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> nonstop = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> transitionLabel = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> transitionDescription = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> wayBack = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.stateId!= null) {
                products.put("stateId", this.stateId.init());
            }
            if (this.statename!= null) {
                products.put("statename", this.statename.init());
            }
            if (this.numeral!= null) {
                products.put("numeral", this.numeral.init());
            }
            if (this.description!= null) {
                products.put("description", this.description.init());
            }
            if (this.color!= null) {
                products.put("color", this.color.init());
            }
            if (this.buttonIcon!= null) {
                products.put("buttonIcon", this.buttonIcon.init());
            }
            if (this.buttonLabel!= null) {
                products.put("buttonLabel", this.buttonLabel.init());
            }
            if (this.nonstop!= null) {
                products.put("nonstop", this.nonstop.init());
            }
            if (this.transitionLabel!= null) {
                products.put("transitionLabel", this.transitionLabel.init());
            }
            if (this.transitionDescription!= null) {
                products.put("transitionDescription", this.transitionDescription.init());
            }
            if (this.wayBack!= null) {
                products.put("wayBack", this.wayBack.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> stateId() {
            return ((this.stateId == null)?this.stateId = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "stateId"):this.stateId);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> statename() {
            return ((this.statename == null)?this.statename = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "statename"):this.statename);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> numeral() {
            return ((this.numeral == null)?this.numeral = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "numeral"):this.numeral);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> description() {
            return ((this.description == null)?this.description = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "description"):this.description);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> color() {
            return ((this.color == null)?this.color = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "color"):this.color);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> buttonIcon() {
            return ((this.buttonIcon == null)?this.buttonIcon = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "buttonIcon"):this.buttonIcon);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> buttonLabel() {
            return ((this.buttonLabel == null)?this.buttonLabel = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "buttonLabel"):this.buttonLabel);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> nonstop() {
            return ((this.nonstop == null)?this.nonstop = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "nonstop"):this.nonstop);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> transitionLabel() {
            return ((this.transitionLabel == null)?this.transitionLabel = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "transitionLabel"):this.transitionLabel);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> transitionDescription() {
            return ((this.transitionDescription == null)?this.transitionDescription = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "transitionDescription"):this.transitionDescription);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>> wayBack() {
            return ((this.wayBack == null)?this.wayBack = new com.kscs.util.jaxb.Selector<TRoot, StatusInfo.Selector<TRoot, TParent>>(this._root, this, "wayBack"):this.wayBack);
        }

    }

}
