package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for status-path complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="status-path"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="infos" type="{urn:org.nuclos.schema.rest}status-info" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="moreBefore" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="moreAfter" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="outside" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="wayBack" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="end" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "status-path", propOrder = {
    "infos"
})
public class StatusPath implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<StatusInfo> infos;
    @XmlAttribute(name = "moreBefore")
    protected Boolean moreBefore;
    @XmlAttribute(name = "moreAfter")
    protected Boolean moreAfter;
    @XmlAttribute(name = "outside")
    protected Boolean outside;
    @XmlAttribute(name = "wayBack")
    protected Boolean wayBack;
    @XmlAttribute(name = "end")
    protected Boolean end;

    /**
     * Gets the value of the infos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusInfo }
     * 
     * 
     */
    public List<StatusInfo> getInfos() {
        if (infos == null) {
            infos = new ArrayList<StatusInfo>();
        }
        return this.infos;
    }

    /**
     * Gets the value of the moreBefore property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMoreBefore() {
        return moreBefore;
    }

    /**
     * Sets the value of the moreBefore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMoreBefore(Boolean value) {
        this.moreBefore = value;
    }

    /**
     * Gets the value of the moreAfter property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMoreAfter() {
        return moreAfter;
    }

    /**
     * Sets the value of the moreAfter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMoreAfter(Boolean value) {
        this.moreAfter = value;
    }

    /**
     * Gets the value of the outside property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOutside() {
        return outside;
    }

    /**
     * Sets the value of the outside property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOutside(Boolean value) {
        this.outside = value;
    }

    /**
     * Gets the value of the wayBack property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWayBack() {
        return wayBack;
    }

    /**
     * Sets the value of the wayBack property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWayBack(Boolean value) {
        this.wayBack = value;
    }

    /**
     * Gets the value of the end property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnd() {
        return end;
    }

    /**
     * Sets the value of the end property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnd(Boolean value) {
        this.end = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<StatusInfo> theInfos;
            theInfos = (((this.infos!= null)&&(!this.infos.isEmpty()))?this.getInfos():null);
            strategy.appendField(locator, this, "infos", buffer, theInfos);
        }
        {
            Boolean theMoreBefore;
            theMoreBefore = this.isMoreBefore();
            strategy.appendField(locator, this, "moreBefore", buffer, theMoreBefore);
        }
        {
            Boolean theMoreAfter;
            theMoreAfter = this.isMoreAfter();
            strategy.appendField(locator, this, "moreAfter", buffer, theMoreAfter);
        }
        {
            Boolean theOutside;
            theOutside = this.isOutside();
            strategy.appendField(locator, this, "outside", buffer, theOutside);
        }
        {
            Boolean theWayBack;
            theWayBack = this.isWayBack();
            strategy.appendField(locator, this, "wayBack", buffer, theWayBack);
        }
        {
            Boolean theEnd;
            theEnd = this.isEnd();
            strategy.appendField(locator, this, "end", buffer, theEnd);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof StatusPath)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final StatusPath that = ((StatusPath) object);
        {
            List<StatusInfo> lhsInfos;
            lhsInfos = (((this.infos!= null)&&(!this.infos.isEmpty()))?this.getInfos():null);
            List<StatusInfo> rhsInfos;
            rhsInfos = (((that.infos!= null)&&(!that.infos.isEmpty()))?that.getInfos():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "infos", lhsInfos), LocatorUtils.property(thatLocator, "infos", rhsInfos), lhsInfos, rhsInfos)) {
                return false;
            }
        }
        {
            Boolean lhsMoreBefore;
            lhsMoreBefore = this.isMoreBefore();
            Boolean rhsMoreBefore;
            rhsMoreBefore = that.isMoreBefore();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "moreBefore", lhsMoreBefore), LocatorUtils.property(thatLocator, "moreBefore", rhsMoreBefore), lhsMoreBefore, rhsMoreBefore)) {
                return false;
            }
        }
        {
            Boolean lhsMoreAfter;
            lhsMoreAfter = this.isMoreAfter();
            Boolean rhsMoreAfter;
            rhsMoreAfter = that.isMoreAfter();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "moreAfter", lhsMoreAfter), LocatorUtils.property(thatLocator, "moreAfter", rhsMoreAfter), lhsMoreAfter, rhsMoreAfter)) {
                return false;
            }
        }
        {
            Boolean lhsOutside;
            lhsOutside = this.isOutside();
            Boolean rhsOutside;
            rhsOutside = that.isOutside();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "outside", lhsOutside), LocatorUtils.property(thatLocator, "outside", rhsOutside), lhsOutside, rhsOutside)) {
                return false;
            }
        }
        {
            Boolean lhsWayBack;
            lhsWayBack = this.isWayBack();
            Boolean rhsWayBack;
            rhsWayBack = that.isWayBack();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "wayBack", lhsWayBack), LocatorUtils.property(thatLocator, "wayBack", rhsWayBack), lhsWayBack, rhsWayBack)) {
                return false;
            }
        }
        {
            Boolean lhsEnd;
            lhsEnd = this.isEnd();
            Boolean rhsEnd;
            rhsEnd = that.isEnd();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "end", lhsEnd), LocatorUtils.property(thatLocator, "end", rhsEnd), lhsEnd, rhsEnd)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<StatusInfo> theInfos;
            theInfos = (((this.infos!= null)&&(!this.infos.isEmpty()))?this.getInfos():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "infos", theInfos), currentHashCode, theInfos);
        }
        {
            Boolean theMoreBefore;
            theMoreBefore = this.isMoreBefore();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "moreBefore", theMoreBefore), currentHashCode, theMoreBefore);
        }
        {
            Boolean theMoreAfter;
            theMoreAfter = this.isMoreAfter();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "moreAfter", theMoreAfter), currentHashCode, theMoreAfter);
        }
        {
            Boolean theOutside;
            theOutside = this.isOutside();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "outside", theOutside), currentHashCode, theOutside);
        }
        {
            Boolean theWayBack;
            theWayBack = this.isWayBack();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "wayBack", theWayBack), currentHashCode, theWayBack);
        }
        {
            Boolean theEnd;
            theEnd = this.isEnd();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "end", theEnd), currentHashCode, theEnd);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof StatusPath) {
            final StatusPath copy = ((StatusPath) draftCopy);
            if ((this.infos!= null)&&(!this.infos.isEmpty())) {
                List<StatusInfo> sourceInfos;
                sourceInfos = (((this.infos!= null)&&(!this.infos.isEmpty()))?this.getInfos():null);
                @SuppressWarnings("unchecked")
                List<StatusInfo> copyInfos = ((List<StatusInfo> ) strategy.copy(LocatorUtils.property(locator, "infos", sourceInfos), sourceInfos));
                copy.infos = null;
                if (copyInfos!= null) {
                    List<StatusInfo> uniqueInfosl = copy.getInfos();
                    uniqueInfosl.addAll(copyInfos);
                }
            } else {
                copy.infos = null;
            }
            if (this.moreBefore!= null) {
                Boolean sourceMoreBefore;
                sourceMoreBefore = this.isMoreBefore();
                Boolean copyMoreBefore = ((Boolean) strategy.copy(LocatorUtils.property(locator, "moreBefore", sourceMoreBefore), sourceMoreBefore));
                copy.setMoreBefore(copyMoreBefore);
            } else {
                copy.moreBefore = null;
            }
            if (this.moreAfter!= null) {
                Boolean sourceMoreAfter;
                sourceMoreAfter = this.isMoreAfter();
                Boolean copyMoreAfter = ((Boolean) strategy.copy(LocatorUtils.property(locator, "moreAfter", sourceMoreAfter), sourceMoreAfter));
                copy.setMoreAfter(copyMoreAfter);
            } else {
                copy.moreAfter = null;
            }
            if (this.outside!= null) {
                Boolean sourceOutside;
                sourceOutside = this.isOutside();
                Boolean copyOutside = ((Boolean) strategy.copy(LocatorUtils.property(locator, "outside", sourceOutside), sourceOutside));
                copy.setOutside(copyOutside);
            } else {
                copy.outside = null;
            }
            if (this.wayBack!= null) {
                Boolean sourceWayBack;
                sourceWayBack = this.isWayBack();
                Boolean copyWayBack = ((Boolean) strategy.copy(LocatorUtils.property(locator, "wayBack", sourceWayBack), sourceWayBack));
                copy.setWayBack(copyWayBack);
            } else {
                copy.wayBack = null;
            }
            if (this.end!= null) {
                Boolean sourceEnd;
                sourceEnd = this.isEnd();
                Boolean copyEnd = ((Boolean) strategy.copy(LocatorUtils.property(locator, "end", sourceEnd), sourceEnd));
                copy.setEnd(copyEnd);
            } else {
                copy.end = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new StatusPath();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final StatusPath.Builder<_B> _other) {
        if (this.infos == null) {
            _other.infos = null;
        } else {
            _other.infos = new ArrayList<StatusInfo.Builder<StatusPath.Builder<_B>>>();
            for (StatusInfo _item: this.infos) {
                _other.infos.add(((_item == null)?null:_item.newCopyBuilder(_other)));
            }
        }
        _other.moreBefore = this.moreBefore;
        _other.moreAfter = this.moreAfter;
        _other.outside = this.outside;
        _other.wayBack = this.wayBack;
        _other.end = this.end;
    }

    public<_B >StatusPath.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new StatusPath.Builder<_B>(_parentBuilder, this, true);
    }

    public StatusPath.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static StatusPath.Builder<Void> builder() {
        return new StatusPath.Builder<Void>(null, null, false);
    }

    public static<_B >StatusPath.Builder<_B> copyOf(final StatusPath _other) {
        final StatusPath.Builder<_B> _newBuilder = new StatusPath.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final StatusPath.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree infosPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("infos"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(infosPropertyTree!= null):((infosPropertyTree == null)||(!infosPropertyTree.isLeaf())))) {
            if (this.infos == null) {
                _other.infos = null;
            } else {
                _other.infos = new ArrayList<StatusInfo.Builder<StatusPath.Builder<_B>>>();
                for (StatusInfo _item: this.infos) {
                    _other.infos.add(((_item == null)?null:_item.newCopyBuilder(_other, infosPropertyTree, _propertyTreeUse)));
                }
            }
        }
        final PropertyTree moreBeforePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("moreBefore"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(moreBeforePropertyTree!= null):((moreBeforePropertyTree == null)||(!moreBeforePropertyTree.isLeaf())))) {
            _other.moreBefore = this.moreBefore;
        }
        final PropertyTree moreAfterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("moreAfter"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(moreAfterPropertyTree!= null):((moreAfterPropertyTree == null)||(!moreAfterPropertyTree.isLeaf())))) {
            _other.moreAfter = this.moreAfter;
        }
        final PropertyTree outsidePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("outside"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(outsidePropertyTree!= null):((outsidePropertyTree == null)||(!outsidePropertyTree.isLeaf())))) {
            _other.outside = this.outside;
        }
        final PropertyTree wayBackPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("wayBack"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(wayBackPropertyTree!= null):((wayBackPropertyTree == null)||(!wayBackPropertyTree.isLeaf())))) {
            _other.wayBack = this.wayBack;
        }
        final PropertyTree endPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("end"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(endPropertyTree!= null):((endPropertyTree == null)||(!endPropertyTree.isLeaf())))) {
            _other.end = this.end;
        }
    }

    public<_B >StatusPath.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new StatusPath.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public StatusPath.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >StatusPath.Builder<_B> copyOf(final StatusPath _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final StatusPath.Builder<_B> _newBuilder = new StatusPath.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static StatusPath.Builder<Void> copyExcept(final StatusPath _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static StatusPath.Builder<Void> copyOnly(final StatusPath _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final StatusPath _storedValue;
        private List<StatusInfo.Builder<StatusPath.Builder<_B>>> infos;
        private Boolean moreBefore;
        private Boolean moreAfter;
        private Boolean outside;
        private Boolean wayBack;
        private Boolean end;

        public Builder(final _B _parentBuilder, final StatusPath _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    if (_other.infos == null) {
                        this.infos = null;
                    } else {
                        this.infos = new ArrayList<StatusInfo.Builder<StatusPath.Builder<_B>>>();
                        for (StatusInfo _item: _other.infos) {
                            this.infos.add(((_item == null)?null:_item.newCopyBuilder(this)));
                        }
                    }
                    this.moreBefore = _other.moreBefore;
                    this.moreAfter = _other.moreAfter;
                    this.outside = _other.outside;
                    this.wayBack = _other.wayBack;
                    this.end = _other.end;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final StatusPath _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree infosPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("infos"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(infosPropertyTree!= null):((infosPropertyTree == null)||(!infosPropertyTree.isLeaf())))) {
                        if (_other.infos == null) {
                            this.infos = null;
                        } else {
                            this.infos = new ArrayList<StatusInfo.Builder<StatusPath.Builder<_B>>>();
                            for (StatusInfo _item: _other.infos) {
                                this.infos.add(((_item == null)?null:_item.newCopyBuilder(this, infosPropertyTree, _propertyTreeUse)));
                            }
                        }
                    }
                    final PropertyTree moreBeforePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("moreBefore"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(moreBeforePropertyTree!= null):((moreBeforePropertyTree == null)||(!moreBeforePropertyTree.isLeaf())))) {
                        this.moreBefore = _other.moreBefore;
                    }
                    final PropertyTree moreAfterPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("moreAfter"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(moreAfterPropertyTree!= null):((moreAfterPropertyTree == null)||(!moreAfterPropertyTree.isLeaf())))) {
                        this.moreAfter = _other.moreAfter;
                    }
                    final PropertyTree outsidePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("outside"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(outsidePropertyTree!= null):((outsidePropertyTree == null)||(!outsidePropertyTree.isLeaf())))) {
                        this.outside = _other.outside;
                    }
                    final PropertyTree wayBackPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("wayBack"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(wayBackPropertyTree!= null):((wayBackPropertyTree == null)||(!wayBackPropertyTree.isLeaf())))) {
                        this.wayBack = _other.wayBack;
                    }
                    final PropertyTree endPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("end"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(endPropertyTree!= null):((endPropertyTree == null)||(!endPropertyTree.isLeaf())))) {
                        this.end = _other.end;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends StatusPath >_P init(final _P _product) {
            if (this.infos!= null) {
                final List<StatusInfo> infos = new ArrayList<StatusInfo>(this.infos.size());
                for (StatusInfo.Builder<StatusPath.Builder<_B>> _item: this.infos) {
                    infos.add(_item.build());
                }
                _product.infos = infos;
            }
            _product.moreBefore = this.moreBefore;
            _product.moreAfter = this.moreAfter;
            _product.outside = this.outside;
            _product.wayBack = this.wayBack;
            _product.end = this.end;
            return _product;
        }

        /**
         * Adds the given items to the value of "infos"
         * 
         * @param infos
         *     Items to add to the value of the "infos" property
         */
        public StatusPath.Builder<_B> addInfos(final Iterable<? extends StatusInfo> infos) {
            if (infos!= null) {
                if (this.infos == null) {
                    this.infos = new ArrayList<StatusInfo.Builder<StatusPath.Builder<_B>>>();
                }
                for (StatusInfo _item: infos) {
                    this.infos.add(new StatusInfo.Builder<StatusPath.Builder<_B>>(this, _item, false));
                }
            }
            return this;
        }

        /**
         * Sets the new value of "infos" (any previous value will be replaced)
         * 
         * @param infos
         *     New value of the "infos" property.
         */
        public StatusPath.Builder<_B> withInfos(final Iterable<? extends StatusInfo> infos) {
            if (this.infos!= null) {
                this.infos.clear();
            }
            return addInfos(infos);
        }

        /**
         * Adds the given items to the value of "infos"
         * 
         * @param infos
         *     Items to add to the value of the "infos" property
         */
        public StatusPath.Builder<_B> addInfos(StatusInfo... infos) {
            addInfos(Arrays.asList(infos));
            return this;
        }

        /**
         * Sets the new value of "infos" (any previous value will be replaced)
         * 
         * @param infos
         *     New value of the "infos" property.
         */
        public StatusPath.Builder<_B> withInfos(StatusInfo... infos) {
            withInfos(Arrays.asList(infos));
            return this;
        }

        /**
         * Returns a new builder to build an additional value of the "Infos" property.
         * Use {@link org.nuclos.schema.rest.StatusInfo.Builder#end()} to return to the current builder.
         * 
         * @return
         *     a new builder to build an additional value of the "Infos" property.
         *     Use {@link org.nuclos.schema.rest.StatusInfo.Builder#end()} to return to the current builder.
         */
        public StatusInfo.Builder<? extends StatusPath.Builder<_B>> addInfos() {
            if (this.infos == null) {
                this.infos = new ArrayList<StatusInfo.Builder<StatusPath.Builder<_B>>>();
            }
            final StatusInfo.Builder<StatusPath.Builder<_B>> infos_Builder = new StatusInfo.Builder<StatusPath.Builder<_B>>(this, null, false);
            this.infos.add(infos_Builder);
            return infos_Builder;
        }

        /**
         * Sets the new value of "moreBefore" (any previous value will be replaced)
         * 
         * @param moreBefore
         *     New value of the "moreBefore" property.
         */
        public StatusPath.Builder<_B> withMoreBefore(final Boolean moreBefore) {
            this.moreBefore = moreBefore;
            return this;
        }

        /**
         * Sets the new value of "moreAfter" (any previous value will be replaced)
         * 
         * @param moreAfter
         *     New value of the "moreAfter" property.
         */
        public StatusPath.Builder<_B> withMoreAfter(final Boolean moreAfter) {
            this.moreAfter = moreAfter;
            return this;
        }

        /**
         * Sets the new value of "outside" (any previous value will be replaced)
         * 
         * @param outside
         *     New value of the "outside" property.
         */
        public StatusPath.Builder<_B> withOutside(final Boolean outside) {
            this.outside = outside;
            return this;
        }

        /**
         * Sets the new value of "wayBack" (any previous value will be replaced)
         * 
         * @param wayBack
         *     New value of the "wayBack" property.
         */
        public StatusPath.Builder<_B> withWayBack(final Boolean wayBack) {
            this.wayBack = wayBack;
            return this;
        }

        /**
         * Sets the new value of "end" (any previous value will be replaced)
         * 
         * @param end
         *     New value of the "end" property.
         */
        public StatusPath.Builder<_B> withEnd(final Boolean end) {
            this.end = end;
            return this;
        }

        @Override
        public StatusPath build() {
            if (_storedValue == null) {
                return this.init(new StatusPath());
            } else {
                return ((StatusPath) _storedValue);
            }
        }

        public StatusPath.Builder<_B> copyOf(final StatusPath _other) {
            _other.copyTo(this);
            return this;
        }

        public StatusPath.Builder<_B> copyOf(final StatusPath.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends StatusPath.Selector<StatusPath.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static StatusPath.Select _root() {
            return new StatusPath.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private StatusInfo.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> infos = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> moreBefore = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> moreAfter = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> outside = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> wayBack = null;
        private com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> end = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.infos!= null) {
                products.put("infos", this.infos.init());
            }
            if (this.moreBefore!= null) {
                products.put("moreBefore", this.moreBefore.init());
            }
            if (this.moreAfter!= null) {
                products.put("moreAfter", this.moreAfter.init());
            }
            if (this.outside!= null) {
                products.put("outside", this.outside.init());
            }
            if (this.wayBack!= null) {
                products.put("wayBack", this.wayBack.init());
            }
            if (this.end!= null) {
                products.put("end", this.end.init());
            }
            return products;
        }

        public StatusInfo.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> infos() {
            return ((this.infos == null)?this.infos = new StatusInfo.Selector<TRoot, StatusPath.Selector<TRoot, TParent>>(this._root, this, "infos"):this.infos);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> moreBefore() {
            return ((this.moreBefore == null)?this.moreBefore = new com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>>(this._root, this, "moreBefore"):this.moreBefore);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> moreAfter() {
            return ((this.moreAfter == null)?this.moreAfter = new com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>>(this._root, this, "moreAfter"):this.moreAfter);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> outside() {
            return ((this.outside == null)?this.outside = new com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>>(this._root, this, "outside"):this.outside);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> wayBack() {
            return ((this.wayBack == null)?this.wayBack = new com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>>(this._root, this, "wayBack"):this.wayBack);
        }

        public com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>> end() {
            return ((this.end == null)?this.end = new com.kscs.util.jaxb.Selector<TRoot, StatusPath.Selector<TRoot, TParent>>(this._root, this, "end"):this.end);
        }

    }

}
