package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for vlp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="vlp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="params" type="{urn:org.nuclos.schema.rest}vlp-parameter"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="type" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "vlp", propOrder = {
    "params"
})
public class Vlp implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected VlpParameter params;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "value")
    protected String value;

    /**
     * Gets the value of the params property.
     * 
     * @return
     *     possible object is
     *     {@link VlpParameter }
     *     
     */
    public VlpParameter getParams() {
        return params;
    }

    /**
     * Sets the value of the params property.
     * 
     * @param value
     *     allowed object is
     *     {@link VlpParameter }
     *     
     */
    public void setParams(VlpParameter value) {
        this.params = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            VlpParameter theParams;
            theParams = this.getParams();
            strategy.appendField(locator, this, "params", buffer, theParams);
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType);
        }
        {
            String theValue;
            theValue = this.getValue();
            strategy.appendField(locator, this, "value", buffer, theValue);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Vlp)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Vlp that = ((Vlp) object);
        {
            VlpParameter lhsParams;
            lhsParams = this.getParams();
            VlpParameter rhsParams;
            rhsParams = that.getParams();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "params", lhsParams), LocatorUtils.property(thatLocator, "params", rhsParams), lhsParams, rhsParams)) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType)) {
                return false;
            }
        }
        {
            String lhsValue;
            lhsValue = this.getValue();
            String rhsValue;
            rhsValue = that.getValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            VlpParameter theParams;
            theParams = this.getParams();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "params", theParams), currentHashCode, theParams);
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType);
        }
        {
            String theValue;
            theValue = this.getValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof Vlp) {
            final Vlp copy = ((Vlp) draftCopy);
            if (this.params!= null) {
                VlpParameter sourceParams;
                sourceParams = this.getParams();
                VlpParameter copyParams = ((VlpParameter) strategy.copy(LocatorUtils.property(locator, "params", sourceParams), sourceParams));
                copy.setParams(copyParams);
            } else {
                copy.params = null;
            }
            if (this.type!= null) {
                String sourceType;
                sourceType = this.getType();
                String copyType = ((String) strategy.copy(LocatorUtils.property(locator, "type", sourceType), sourceType));
                copy.setType(copyType);
            } else {
                copy.type = null;
            }
            if (this.value!= null) {
                String sourceValue;
                sourceValue = this.getValue();
                String copyValue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourceValue), sourceValue));
                copy.setValue(copyValue);
            } else {
                copy.value = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new Vlp();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Vlp.Builder<_B> _other) {
        _other.params = ((this.params == null)?null:this.params.newCopyBuilder(_other));
        _other.type = this.type;
        _other.value = this.value;
    }

    public<_B >Vlp.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new Vlp.Builder<_B>(_parentBuilder, this, true);
    }

    public Vlp.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static Vlp.Builder<Void> builder() {
        return new Vlp.Builder<Void>(null, null, false);
    }

    public static<_B >Vlp.Builder<_B> copyOf(final Vlp _other) {
        final Vlp.Builder<_B> _newBuilder = new Vlp.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final Vlp.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree paramsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("params"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(paramsPropertyTree!= null):((paramsPropertyTree == null)||(!paramsPropertyTree.isLeaf())))) {
            _other.params = ((this.params == null)?null:this.params.newCopyBuilder(_other, paramsPropertyTree, _propertyTreeUse));
        }
        final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
            _other.type = this.type;
        }
        final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
            _other.value = this.value;
        }
    }

    public<_B >Vlp.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new Vlp.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public Vlp.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >Vlp.Builder<_B> copyOf(final Vlp _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final Vlp.Builder<_B> _newBuilder = new Vlp.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static Vlp.Builder<Void> copyExcept(final Vlp _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static Vlp.Builder<Void> copyOnly(final Vlp _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final Vlp _storedValue;
        private VlpParameter.Builder<Vlp.Builder<_B>> params;
        private String type;
        private String value;

        public Builder(final _B _parentBuilder, final Vlp _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.params = ((_other.params == null)?null:_other.params.newCopyBuilder(this));
                    this.type = _other.type;
                    this.value = _other.value;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final Vlp _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree paramsPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("params"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(paramsPropertyTree!= null):((paramsPropertyTree == null)||(!paramsPropertyTree.isLeaf())))) {
                        this.params = ((_other.params == null)?null:_other.params.newCopyBuilder(this, paramsPropertyTree, _propertyTreeUse));
                    }
                    final PropertyTree typePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("type"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(typePropertyTree!= null):((typePropertyTree == null)||(!typePropertyTree.isLeaf())))) {
                        this.type = _other.type;
                    }
                    final PropertyTree valuePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("value"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuePropertyTree!= null):((valuePropertyTree == null)||(!valuePropertyTree.isLeaf())))) {
                        this.value = _other.value;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends Vlp >_P init(final _P _product) {
            _product.params = ((this.params == null)?null:this.params.build());
            _product.type = this.type;
            _product.value = this.value;
            return _product;
        }

        /**
         * Sets the new value of "params" (any previous value will be replaced)
         * 
         * @param params
         *     New value of the "params" property.
         */
        public Vlp.Builder<_B> withParams(final VlpParameter params) {
            this.params = ((params == null)?null:new VlpParameter.Builder<Vlp.Builder<_B>>(this, params, false));
            return this;
        }

        /**
         * Returns the existing builder or a new builder to build the value of the "params" property.
         * Use {@link org.nuclos.schema.rest.VlpParameter.Builder#end()} to return to the current builder.
         * 
         * @return
         *     A new builder to build the value of the "params" property.
         *     Use {@link org.nuclos.schema.rest.VlpParameter.Builder#end()} to return to the current builder.
         */
        public VlpParameter.Builder<? extends Vlp.Builder<_B>> withParams() {
            if (this.params!= null) {
                return this.params;
            }
            return this.params = new VlpParameter.Builder<Vlp.Builder<_B>>(this, null, false);
        }

        /**
         * Sets the new value of "type" (any previous value will be replaced)
         * 
         * @param type
         *     New value of the "type" property.
         */
        public Vlp.Builder<_B> withType(final String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the new value of "value" (any previous value will be replaced)
         * 
         * @param value
         *     New value of the "value" property.
         */
        public Vlp.Builder<_B> withValue(final String value) {
            this.value = value;
            return this;
        }

        @Override
        public Vlp build() {
            if (_storedValue == null) {
                return this.init(new Vlp());
            } else {
                return ((Vlp) _storedValue);
            }
        }

        public Vlp.Builder<_B> copyOf(final Vlp _other) {
            _other.copyTo(this);
            return this;
        }

        public Vlp.Builder<_B> copyOf(final Vlp.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends Vlp.Selector<Vlp.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static Vlp.Select _root() {
            return new Vlp.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private VlpParameter.Selector<TRoot, Vlp.Selector<TRoot, TParent>> params = null;
        private com.kscs.util.jaxb.Selector<TRoot, Vlp.Selector<TRoot, TParent>> type = null;
        private com.kscs.util.jaxb.Selector<TRoot, Vlp.Selector<TRoot, TParent>> value = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.params!= null) {
                products.put("params", this.params.init());
            }
            if (this.type!= null) {
                products.put("type", this.type.init());
            }
            if (this.value!= null) {
                products.put("value", this.value.init());
            }
            return products;
        }

        public VlpParameter.Selector<TRoot, Vlp.Selector<TRoot, TParent>> params() {
            return ((this.params == null)?this.params = new VlpParameter.Selector<TRoot, Vlp.Selector<TRoot, TParent>>(this._root, this, "params"):this.params);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Vlp.Selector<TRoot, TParent>> type() {
            return ((this.type == null)?this.type = new com.kscs.util.jaxb.Selector<TRoot, Vlp.Selector<TRoot, TParent>>(this._root, this, "type"):this.type);
        }

        public com.kscs.util.jaxb.Selector<TRoot, Vlp.Selector<TRoot, TParent>> value() {
            return ((this.value == null)?this.value = new com.kscs.util.jaxb.Selector<TRoot, Vlp.Selector<TRoot, TParent>>(this._root, this, "value"):this.value);
        }

    }

}
