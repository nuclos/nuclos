package org.nuclos.schema.rest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import com.kscs.util.jaxb.Buildable;
import com.kscs.util.jaxb.PropertyTree;
import com.kscs.util.jaxb.PropertyTreeUse;
import org.jvnet.jaxb2_commons.lang.CopyStrategy;
import org.jvnet.jaxb2_commons.lang.CopyTo;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBCopyStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for vlp-parameter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="vlp-parameter"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="fieldname" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="idFieldname" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="valuelistProvider" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "vlp-parameter")
public class VlpParameter implements Serializable, Cloneable, CopyTo, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "fieldname")
    protected String fieldname;
    @XmlAttribute(name = "idFieldname")
    protected String idFieldname;
    @XmlAttribute(name = "valuelistProvider")
    protected String valuelistProvider;

    /**
     * Gets the value of the fieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldname() {
        return fieldname;
    }

    /**
     * Sets the value of the fieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldname(String value) {
        this.fieldname = value;
    }

    /**
     * Gets the value of the idFieldname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFieldname() {
        return idFieldname;
    }

    /**
     * Sets the value of the idFieldname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFieldname(String value) {
        this.idFieldname = value;
    }

    /**
     * Gets the value of the valuelistProvider property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValuelistProvider() {
        return valuelistProvider;
    }

    /**
     * Sets the value of the valuelistProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValuelistProvider(String value) {
        this.valuelistProvider = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theFieldname;
            theFieldname = this.getFieldname();
            strategy.appendField(locator, this, "fieldname", buffer, theFieldname);
        }
        {
            String theIdFieldname;
            theIdFieldname = this.getIdFieldname();
            strategy.appendField(locator, this, "idFieldname", buffer, theIdFieldname);
        }
        {
            String theValuelistProvider;
            theValuelistProvider = this.getValuelistProvider();
            strategy.appendField(locator, this, "valuelistProvider", buffer, theValuelistProvider);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof VlpParameter)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final VlpParameter that = ((VlpParameter) object);
        {
            String lhsFieldname;
            lhsFieldname = this.getFieldname();
            String rhsFieldname;
            rhsFieldname = that.getFieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fieldname", lhsFieldname), LocatorUtils.property(thatLocator, "fieldname", rhsFieldname), lhsFieldname, rhsFieldname)) {
                return false;
            }
        }
        {
            String lhsIdFieldname;
            lhsIdFieldname = this.getIdFieldname();
            String rhsIdFieldname;
            rhsIdFieldname = that.getIdFieldname();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "idFieldname", lhsIdFieldname), LocatorUtils.property(thatLocator, "idFieldname", rhsIdFieldname), lhsIdFieldname, rhsIdFieldname)) {
                return false;
            }
        }
        {
            String lhsValuelistProvider;
            lhsValuelistProvider = this.getValuelistProvider();
            String rhsValuelistProvider;
            rhsValuelistProvider = that.getValuelistProvider();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "valuelistProvider", lhsValuelistProvider), LocatorUtils.property(thatLocator, "valuelistProvider", rhsValuelistProvider), lhsValuelistProvider, rhsValuelistProvider)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theFieldname;
            theFieldname = this.getFieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fieldname", theFieldname), currentHashCode, theFieldname);
        }
        {
            String theIdFieldname;
            theIdFieldname = this.getIdFieldname();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "idFieldname", theIdFieldname), currentHashCode, theIdFieldname);
        }
        {
            String theValuelistProvider;
            theValuelistProvider = this.getValuelistProvider();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "valuelistProvider", theValuelistProvider), currentHashCode, theValuelistProvider);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public Object clone() {
        return copyTo(createNewInstance());
    }

    public Object copyTo(Object target) {
        final CopyStrategy strategy = JAXBCopyStrategy.INSTANCE;
        return copyTo(null, target, strategy);
    }

    public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
        final Object draftCopy = ((target == null)?createNewInstance():target);
        if (draftCopy instanceof VlpParameter) {
            final VlpParameter copy = ((VlpParameter) draftCopy);
            if (this.fieldname!= null) {
                String sourceFieldname;
                sourceFieldname = this.getFieldname();
                String copyFieldname = ((String) strategy.copy(LocatorUtils.property(locator, "fieldname", sourceFieldname), sourceFieldname));
                copy.setFieldname(copyFieldname);
            } else {
                copy.fieldname = null;
            }
            if (this.idFieldname!= null) {
                String sourceIdFieldname;
                sourceIdFieldname = this.getIdFieldname();
                String copyIdFieldname = ((String) strategy.copy(LocatorUtils.property(locator, "idFieldname", sourceIdFieldname), sourceIdFieldname));
                copy.setIdFieldname(copyIdFieldname);
            } else {
                copy.idFieldname = null;
            }
            if (this.valuelistProvider!= null) {
                String sourceValuelistProvider;
                sourceValuelistProvider = this.getValuelistProvider();
                String copyValuelistProvider = ((String) strategy.copy(LocatorUtils.property(locator, "valuelistProvider", sourceValuelistProvider), sourceValuelistProvider));
                copy.setValuelistProvider(copyValuelistProvider);
            } else {
                copy.valuelistProvider = null;
            }
        }
        return draftCopy;
    }

    public Object createNewInstance() {
        return new VlpParameter();
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final VlpParameter.Builder<_B> _other) {
        _other.fieldname = this.fieldname;
        _other.idFieldname = this.idFieldname;
        _other.valuelistProvider = this.valuelistProvider;
    }

    public<_B >VlpParameter.Builder<_B> newCopyBuilder(final _B _parentBuilder) {
        return new VlpParameter.Builder<_B>(_parentBuilder, this, true);
    }

    public VlpParameter.Builder<Void> newCopyBuilder() {
        return newCopyBuilder(null);
    }

    public static VlpParameter.Builder<Void> builder() {
        return new VlpParameter.Builder<Void>(null, null, false);
    }

    public static<_B >VlpParameter.Builder<_B> copyOf(final VlpParameter _other) {
        final VlpParameter.Builder<_B> _newBuilder = new VlpParameter.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder);
        return _newBuilder;
    }

    /**
     * Copies all state of this object to a builder. This method is used by the {@link #copyOf} method and should not be called directly by client code.
     * 
     * @param _other
     *     A builder instance to which the state of this object will be copied.
     */
    public<_B >void copyTo(final VlpParameter.Builder<_B> _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final PropertyTree fieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldnamePropertyTree!= null):((fieldnamePropertyTree == null)||(!fieldnamePropertyTree.isLeaf())))) {
            _other.fieldname = this.fieldname;
        }
        final PropertyTree idFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("idFieldname"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idFieldnamePropertyTree!= null):((idFieldnamePropertyTree == null)||(!idFieldnamePropertyTree.isLeaf())))) {
            _other.idFieldname = this.idFieldname;
        }
        final PropertyTree valuelistProviderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valuelistProvider"));
        if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuelistProviderPropertyTree!= null):((valuelistProviderPropertyTree == null)||(!valuelistProviderPropertyTree.isLeaf())))) {
            _other.valuelistProvider = this.valuelistProvider;
        }
    }

    public<_B >VlpParameter.Builder<_B> newCopyBuilder(final _B _parentBuilder, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return new VlpParameter.Builder<_B>(_parentBuilder, this, true, _propertyTree, _propertyTreeUse);
    }

    public VlpParameter.Builder<Void> newCopyBuilder(final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        return newCopyBuilder(null, _propertyTree, _propertyTreeUse);
    }

    public static<_B >VlpParameter.Builder<_B> copyOf(final VlpParameter _other, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
        final VlpParameter.Builder<_B> _newBuilder = new VlpParameter.Builder<_B>(null, null, false);
        _other.copyTo(_newBuilder, _propertyTree, _propertyTreeUse);
        return _newBuilder;
    }

    public static VlpParameter.Builder<Void> copyExcept(final VlpParameter _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.EXCLUDE);
    }

    public static VlpParameter.Builder<Void> copyOnly(final VlpParameter _other, final PropertyTree _propertyTree) {
        return copyOf(_other, _propertyTree, PropertyTreeUse.INCLUDE);
    }

    public static class Builder<_B >implements Buildable
    {

        protected final _B _parentBuilder;
        protected final VlpParameter _storedValue;
        private String fieldname;
        private String idFieldname;
        private String valuelistProvider;

        public Builder(final _B _parentBuilder, final VlpParameter _other, final boolean _copy) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    this.fieldname = _other.fieldname;
                    this.idFieldname = _other.idFieldname;
                    this.valuelistProvider = _other.valuelistProvider;
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public Builder(final _B _parentBuilder, final VlpParameter _other, final boolean _copy, final PropertyTree _propertyTree, final PropertyTreeUse _propertyTreeUse) {
            this._parentBuilder = _parentBuilder;
            if (_other!= null) {
                if (_copy) {
                    _storedValue = null;
                    final PropertyTree fieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("fieldname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(fieldnamePropertyTree!= null):((fieldnamePropertyTree == null)||(!fieldnamePropertyTree.isLeaf())))) {
                        this.fieldname = _other.fieldname;
                    }
                    final PropertyTree idFieldnamePropertyTree = ((_propertyTree == null)?null:_propertyTree.get("idFieldname"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(idFieldnamePropertyTree!= null):((idFieldnamePropertyTree == null)||(!idFieldnamePropertyTree.isLeaf())))) {
                        this.idFieldname = _other.idFieldname;
                    }
                    final PropertyTree valuelistProviderPropertyTree = ((_propertyTree == null)?null:_propertyTree.get("valuelistProvider"));
                    if (((_propertyTreeUse == PropertyTreeUse.INCLUDE)?(valuelistProviderPropertyTree!= null):((valuelistProviderPropertyTree == null)||(!valuelistProviderPropertyTree.isLeaf())))) {
                        this.valuelistProvider = _other.valuelistProvider;
                    }
                } else {
                    _storedValue = _other;
                }
            } else {
                _storedValue = null;
            }
        }

        public _B end() {
            return this._parentBuilder;
        }

        protected<_P extends VlpParameter >_P init(final _P _product) {
            _product.fieldname = this.fieldname;
            _product.idFieldname = this.idFieldname;
            _product.valuelistProvider = this.valuelistProvider;
            return _product;
        }

        /**
         * Sets the new value of "fieldname" (any previous value will be replaced)
         * 
         * @param fieldname
         *     New value of the "fieldname" property.
         */
        public VlpParameter.Builder<_B> withFieldname(final String fieldname) {
            this.fieldname = fieldname;
            return this;
        }

        /**
         * Sets the new value of "idFieldname" (any previous value will be replaced)
         * 
         * @param idFieldname
         *     New value of the "idFieldname" property.
         */
        public VlpParameter.Builder<_B> withIdFieldname(final String idFieldname) {
            this.idFieldname = idFieldname;
            return this;
        }

        /**
         * Sets the new value of "valuelistProvider" (any previous value will be replaced)
         * 
         * @param valuelistProvider
         *     New value of the "valuelistProvider" property.
         */
        public VlpParameter.Builder<_B> withValuelistProvider(final String valuelistProvider) {
            this.valuelistProvider = valuelistProvider;
            return this;
        }

        @Override
        public VlpParameter build() {
            if (_storedValue == null) {
                return this.init(new VlpParameter());
            } else {
                return ((VlpParameter) _storedValue);
            }
        }

        public VlpParameter.Builder<_B> copyOf(final VlpParameter _other) {
            _other.copyTo(this);
            return this;
        }

        public VlpParameter.Builder<_B> copyOf(final VlpParameter.Builder _other) {
            return copyOf(_other.build());
        }

    }

    public static class Select
        extends VlpParameter.Selector<VlpParameter.Select, Void>
    {


        Select() {
            super(null, null, null);
        }

        public static VlpParameter.Select _root() {
            return new VlpParameter.Select();
        }

    }

    public static class Selector<TRoot extends com.kscs.util.jaxb.Selector<TRoot, ?> , TParent >
        extends com.kscs.util.jaxb.Selector<TRoot, TParent>
    {

        private com.kscs.util.jaxb.Selector<TRoot, VlpParameter.Selector<TRoot, TParent>> fieldname = null;
        private com.kscs.util.jaxb.Selector<TRoot, VlpParameter.Selector<TRoot, TParent>> idFieldname = null;
        private com.kscs.util.jaxb.Selector<TRoot, VlpParameter.Selector<TRoot, TParent>> valuelistProvider = null;

        public Selector(final TRoot root, final TParent parent, final String propertyName) {
            super(root, parent, propertyName);
        }

        @Override
        public Map<String, PropertyTree> buildChildren() {
            final Map<String, PropertyTree> products = new HashMap<String, PropertyTree>();
            products.putAll(super.buildChildren());
            if (this.fieldname!= null) {
                products.put("fieldname", this.fieldname.init());
            }
            if (this.idFieldname!= null) {
                products.put("idFieldname", this.idFieldname.init());
            }
            if (this.valuelistProvider!= null) {
                products.put("valuelistProvider", this.valuelistProvider.init());
            }
            return products;
        }

        public com.kscs.util.jaxb.Selector<TRoot, VlpParameter.Selector<TRoot, TParent>> fieldname() {
            return ((this.fieldname == null)?this.fieldname = new com.kscs.util.jaxb.Selector<TRoot, VlpParameter.Selector<TRoot, TParent>>(this._root, this, "fieldname"):this.fieldname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, VlpParameter.Selector<TRoot, TParent>> idFieldname() {
            return ((this.idFieldname == null)?this.idFieldname = new com.kscs.util.jaxb.Selector<TRoot, VlpParameter.Selector<TRoot, TParent>>(this._root, this, "idFieldname"):this.idFieldname);
        }

        public com.kscs.util.jaxb.Selector<TRoot, VlpParameter.Selector<TRoot, TParent>> valuelistProvider() {
            return ((this.valuelistProvider == null)?this.valuelistProvider = new com.kscs.util.jaxb.Selector<TRoot, VlpParameter.Selector<TRoot, TParent>>(this._root, this, "valuelistProvider"):this.valuelistProvider);
        }

    }

}
