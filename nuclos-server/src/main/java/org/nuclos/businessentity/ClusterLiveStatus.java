//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_clusterLiveStatus
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: [proxy]
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class ClusterLiveStatus extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "kbbM", "kbbM3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "kbbM", "kbbM4", java.lang.String.class);


/**
 * Attribute: context
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Context = new StringAttribute<>("Context", "org.nuclos.businessentity", "kbbM", "kbbMg", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "kbbM", "kbbM1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "kbbM", "kbbM2", java.lang.String.class);


/**
 * Attribute: hostname
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRHOSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Hostname = new StringAttribute<>("Hostname", "org.nuclos.businessentity", "kbbM", "kbbMc", java.lang.String.class);


/**
 * Attribute: port
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: INTPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 5
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Port = 
	new NumericAttribute<>("Port", "org.nuclos.businessentity", "kbbM", "kbbMf", java.lang.Integer.class);


/**
 * Attribute: protocol
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRPROTOCOL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Protocol = new StringAttribute<>("Protocol", "org.nuclos.businessentity", "kbbM", "kbbMb", java.lang.String.class);


/**
 * Attribute: server
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> ServerId = 
	new ForeignKeyAttribute<>("ServerId", "org.nuclos.businessentity", "kbbM", "kbbMa", org.nuclos.common.UID.class);


/**
 * Attribute: status
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRSTATUS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Status = new StringAttribute<>("Status", "org.nuclos.businessentity", "kbbM", "kbbMe", java.lang.String.class);


/**
 * Attribute: tester
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: BLNTESTER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Tester = 
	new Attribute<>("Tester", "org.nuclos.businessentity", "kbbM", "kbbMh", java.lang.Boolean.class);


/**
 * Attribute: type
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Type = new StringAttribute<>("Type", "org.nuclos.businessentity", "kbbM", "kbbMd", java.lang.String.class);


public ClusterLiveStatus() {
		super("kbbM");
		setTester(java.lang.Boolean.FALSE);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(ClusterLiveStatus boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public ClusterLiveStatus copy() {
		return super.copy(ClusterLiveStatus.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("kbbM"), id);
}
/**
* Static Get by Id
*/
public static ClusterLiveStatus get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(ClusterLiveStatus.class, id, attributes);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("kbbM3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("kbbM4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: context
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getContext() {
		return getField("kbbMg", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("kbbM1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("kbbM2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getEntity() {
		return "nuclos_clusterLiveStatus";
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("kbbM");
}


/**
 * Getter-Method for attribute: hostname
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRHOSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getHostname() {
		return getField("kbbMc", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: port
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: INTPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 5
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Integer getPort() {
		return getField("kbbMf", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: protocol
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRPROTOCOL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getProtocol() {
		return getField("kbbMb", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.businessentity.ClusterServer getServerBO() {
		return getReferencedBO(org.nuclos.businessentity.ClusterServer.class, getFieldUid("kbbMa"), "kbbMa", "09Z3");
}


/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.common.UID getServerId() {
		return getFieldUid("kbbMa");
}


/**
 * Getter-Method for attribute: status
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRSTATUS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getStatus() {
		return getField("kbbMe", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: tester
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: BLNTESTER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getTester() {
		return getField("kbbMh", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getType() {
		return getField("kbbMd", java.lang.String.class); 
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setChangedAt( java.util.Date pChangedAt) {
		setField("kbbM3", pChangedAt); 
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("kbbM4", pChangedBy); 
}


/**
 * Setter-Method for attribute: context
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public void setContext(java.lang.String pContext) {
		setField("kbbMg", pContext); 
}


/**
 * Setter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setCreatedAt( java.util.Date pCreatedAt) {
		setField("kbbM1", pCreatedAt); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("kbbM2", pCreatedBy); 
}


/**
 * Setter-Method for attribute: hostname
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRHOSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public void setHostname(java.lang.String pHostname) {
		setField("kbbMc", pHostname); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: port
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: INTPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 5
 *<br>Precision: null
**/
public void setPort(java.lang.Integer pPort) {
		setField("kbbMf", pPort); 
}


/**
 * Setter-Method for attribute: protocol
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRPROTOCOL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public void setProtocol(java.lang.String pProtocol) {
		setField("kbbMb", pProtocol); 
}


/**
 * Setter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setServer(java.lang.String pServer) {
		setField("kbbMa", pServer); 
}


/**
 * Setter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setServerId(org.nuclos.common.UID pServerId) {
		setFieldId("kbbMa", pServerId); 
}


/**
 * Setter-Method for attribute: status
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRSTATUS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setStatus(java.lang.String pStatus) {
		setField("kbbMe", pStatus); 
}


/**
 * Setter-Method for attribute: tester
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: BLNTESTER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setTester(java.lang.Boolean pTester) {
		setField("kbbMh", pTester); 
}


/**
 * Setter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public void setType(java.lang.String pType) {
		setField("kbbMd", pType); 
}
 }
