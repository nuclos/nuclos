//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_clusterMessage
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_AD_CLUSTER_MESSAGE
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class ClusterMessage extends AbstractBusinessObject<java.lang.Long> implements Modifiable<java.lang.Long> {


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "IOTy", "IOTy3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "IOTy", "IOTy4", java.lang.String.class);


/**
 * Attribute: content
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRCONTENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Content = new StringAttribute<>("Content", "org.nuclos.businessentity", "IOTy", "IOTye", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "IOTy", "IOTy1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "IOTy", "IOTy2", java.lang.String.class);


/**
 * Attribute: done
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: BLNDONE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Done = 
	new Attribute<>("Done", "org.nuclos.businessentity", "IOTy", "IOTyd", java.lang.Boolean.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<java.lang.Long> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "IOTy", "IOTy0", java.lang.Long.class);


/**
 * Attribute: master
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: BLNMASTER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Master = 
	new Attribute<>("Master", "org.nuclos.businessentity", "IOTy", "IOTyg", java.lang.Boolean.class);


/**
 * Attribute: read
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: BLNREAD
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Read = 
	new Attribute<>("Read", "org.nuclos.businessentity", "IOTy", "IOTyc", java.lang.Boolean.class);


/**
 * Attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> ServerId = 
	new ForeignKeyAttribute<>("ServerId", "org.nuclos.businessentity", "IOTy", "IOTya", org.nuclos.common.UID.class);


/**
 * Attribute: type
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 32
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Type = new StringAttribute<>("Type", "org.nuclos.businessentity", "IOTy", "IOTyf", java.lang.String.class);


public ClusterMessage() {
		super("IOTy");
		setDone(java.lang.Boolean.FALSE);
		setMaster(java.lang.Boolean.FALSE);
		setRead(java.lang.Boolean.FALSE);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(ClusterMessage boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public ClusterMessage copy() {
		return super.copy(ClusterMessage.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(java.lang.Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("IOTy"), id);
}
/**
* Static Get by Id
*/
public static ClusterMessage get(java.lang.Long id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(ClusterMessage.class, id, attributes);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("IOTy3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("IOTy4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRCONTENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getContent() {
		return getField("IOTye", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("IOTy1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("IOTy2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: done
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: BLNDONE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getDone() {
		return getField("IOTyd", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("IOTy");
}


/**
 * Getter-Method for attribute: master
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: BLNMASTER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getMaster() {
		return getField("IOTyg", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: read
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: BLNREAD
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getRead() {
		return getField("IOTyc", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.businessentity.ClusterServer getServerBO() {
		return getReferencedBO(org.nuclos.businessentity.ClusterServer.class, getFieldUid("IOTya"), "IOTya", "09Z3");
}


/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.common.UID getServerId() {
		return getFieldUid("IOTya");
}


/**
 * Getter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 32
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getType() {
		return getField("IOTyf", java.lang.String.class); 
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("IOTy4", pChangedBy); 
}


/**
 * Setter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRCONTENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setContent(java.lang.String pContent) {
		setField("IOTye", pContent); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("IOTy2", pCreatedBy); 
}


/**
 * Setter-Method for attribute: done
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: BLNDONE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setDone(java.lang.Boolean pDone) {
		setField("IOTyd", pDone); 
}


/**
 * Setter-Method for attribute: master
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: BLNMASTER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setMaster(java.lang.Boolean pMaster) {
		setField("IOTyg", pMaster); 
}


/**
 * Setter-Method for attribute: read
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: BLNREAD
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setRead(java.lang.Boolean pRead) {
		setField("IOTyc", pRead); 
}


/**
 * Setter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setServerId(org.nuclos.common.UID pServerId) {
		setFieldId("IOTya", pServerId); 
}


/**
 * Setter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 32
 *<br>Precision: null
**/
public void setType(java.lang.String pType) {
		setField("IOTyf", pType); 
}
 }
