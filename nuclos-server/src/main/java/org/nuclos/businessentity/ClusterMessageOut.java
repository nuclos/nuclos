//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_clusterMessageOut
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: [proxy]
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class ClusterMessageOut extends AbstractBusinessObject<java.lang.Long> implements Modifiable<java.lang.Long> {


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "Xkui", "Xkui3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "Xkui", "Xkui4", java.lang.String.class);


/**
 * Attribute: content
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRCONTENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Content = new StringAttribute<>("Content", "org.nuclos.businessentity", "Xkui", "Xkuie", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "Xkui", "Xkui1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "Xkui", "Xkui2", java.lang.String.class);


/**
 * Attribute: done
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: BLNDONE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Done = 
	new Attribute<>("Done", "org.nuclos.businessentity", "Xkui", "Xkuid", java.lang.Boolean.class);


/**
 * Attribute: read
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: BLNREAD
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Read = 
	new Attribute<>("Read", "org.nuclos.businessentity", "Xkui", "Xkuic", java.lang.Boolean.class);


/**
 * Attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> ServerId = 
	new ForeignKeyAttribute<>("ServerId", "org.nuclos.businessentity", "Xkui", "Xkuia", org.nuclos.common.UID.class);


/**
 * Attribute: timestamp
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRTIMESTAMP
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 32
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Timestamp = new StringAttribute<>("Timestamp", "org.nuclos.businessentity", "Xkui", "Xkuig", java.lang.String.class);


/**
 * Attribute: toServer
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRTOCLUSTERSERVER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ToServer = new StringAttribute<>("ToServer", "org.nuclos.businessentity", "Xkui", "Xkuib", java.lang.String.class);


/**
 * Attribute: type
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 32
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Type = new StringAttribute<>("Type", "org.nuclos.businessentity", "Xkui", "Xkuif", java.lang.String.class);


public ClusterMessageOut() {
		super("Xkui");
		setDone(java.lang.Boolean.FALSE);
		setRead(java.lang.Boolean.FALSE);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(ClusterMessageOut boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public ClusterMessageOut copy() {
		return super.copy(ClusterMessageOut.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(java.lang.Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("Xkui"), id);
}
/**
* Static Get by Id
*/
public static ClusterMessageOut get(java.lang.Long id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(ClusterMessageOut.class, id, attributes);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("Xkui3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("Xkui4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRCONTENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getContent() {
		return getField("Xkuie", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("Xkui1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("Xkui2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: done
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: BLNDONE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getDone() {
		return getField("Xkuid", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getEntity() {
		return "nuclos_clusterMessageOut";
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("Xkui");
}


/**
 * Getter-Method for attribute: read
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: BLNREAD
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getRead() {
		return getField("Xkuic", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.businessentity.ClusterServer getServerBO() {
		return getReferencedBO(org.nuclos.businessentity.ClusterServer.class, getFieldUid("Xkuia"), "Xkuia", "09Z3");
}


/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.common.UID getServerId() {
		return getFieldUid("Xkuia");
}


/**
 * Getter-Method for attribute: timestamp
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRTIMESTAMP
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 32
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getTimestamp() {
		return getField("Xkuig", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: toServer
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRTOCLUSTERSERVER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getToServer() {
		return getField("Xkuib", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 32
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getType() {
		return getField("Xkuif", java.lang.String.class); 
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setChangedAt( java.util.Date pChangedAt) {
		setField("Xkui3", pChangedAt); 
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("Xkui4", pChangedBy); 
}


/**
 * Setter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRCONTENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setContent(java.lang.String pContent) {
		setField("Xkuie", pContent); 
}


/**
 * Setter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setCreatedAt( java.util.Date pCreatedAt) {
		setField("Xkui1", pCreatedAt); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("Xkui2", pCreatedBy); 
}


/**
 * Setter-Method for attribute: done
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: BLNDONE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setDone(java.lang.Boolean pDone) {
		setField("Xkuid", pDone); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
@java.lang.Override
public void setId(java.lang.Long id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: read
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: BLNREAD
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setRead(java.lang.Boolean pRead) {
		setField("Xkuic", pRead); 
}


/**
 * Setter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setServer(java.lang.String pServer) {
		setField("Xkuia", pServer); 
}


/**
 * Setter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setServerId(org.nuclos.common.UID pServerId) {
		setFieldId("Xkuia", pServerId); 
}


/**
 * Setter-Method for attribute: timestamp
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRTIMESTAMP
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 32
 *<br>Precision: null
**/
public void setTimestamp(java.lang.String pTimestamp) {
		setField("Xkuig", pTimestamp); 
}


/**
 * Setter-Method for attribute: toServer
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRTOCLUSTERSERVER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setToServer(java.lang.String pToServer) {
		setField("Xkuib", pToServer); 
}


/**
 * Setter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 32
 *<br>Precision: null
**/
public void setType(java.lang.String pType) {
		setField("Xkuif", pType); 
}
 }
