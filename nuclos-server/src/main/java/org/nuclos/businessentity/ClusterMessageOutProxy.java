//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.List; 
import java.util.Map; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.SearchExpression; 
import org.nuclos.api.businessobject.attribute.Attribute; 

/**
 * BusinessObject: nuclos_clusterMessageOut
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: [proxy]
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public interface ClusterMessageOutProxy {

default Long count() {
	throw new java.lang.UnsupportedOperationException("You need to implement this method in order to use the proxy businessobject aside from subforms.");
}

default <SE> Long count(SearchExpression<SE> searchExpression) {
	throw new java.lang.UnsupportedOperationException("You need to implement this method in order to use the proxy businessobject aside from subforms.");
}

public List<ClusterMessageOut> getAll();
default <SE> List<ClusterMessageOut> getAll(SearchExpression<SE> searchExpression, Long limit, Long offset, Map<Attribute, Boolean> sortingOrders) {
	throw new java.lang.UnsupportedOperationException("You need to implement this method in order to use the proxy businessobject aside from subforms.");
}

default List<ClusterMessageOut> getAll(Long limit, Long offset, Map<Attribute, Boolean> sortingOrders) {
	throw new java.lang.UnsupportedOperationException("You need to implement this method in order to use the proxy businessobject aside from subforms.");
}

public List<java.lang.Long> getAllIds();
public ClusterMessageOut getById(java.lang.Long id);

/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<ClusterMessageOut> getByServer(org.nuclos.common.UID pNuclosclusterServerId);
public void setUser(org.nuclos.api.User user); }
