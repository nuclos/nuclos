//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_clusterServer
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_AD_CLUSTER_SERVER
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class ClusterServer extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;


public static final Dependent<org.nuclos.businessentity.ClusterLiveStatus> _ClusterLiveStatus = 
	new Dependent<>("_ClusterLiveStatus", "null", "ClusterLiveStatus", "kbbM", "server", "kbbMa", org.nuclos.businessentity.ClusterLiveStatus.class);

public static final Dependent<org.nuclos.businessentity.ClusterMessage> _ClusterMessage = 
	new Dependent<>("_ClusterMessage", "null", "ClusterMessage", "IOTy", "server", "IOTya", org.nuclos.businessentity.ClusterMessage.class);

public static final Dependent<org.nuclos.businessentity.ClusterMessageIn> _ClusterMessageIn = 
	new Dependent<>("_ClusterMessageIn", "null", "ClusterMessageIn", "z82M", "server", "z82Ma", org.nuclos.businessentity.ClusterMessageIn.class);

public static final Dependent<org.nuclos.businessentity.ClusterMessageOut> _ClusterMessageOut = 
	new Dependent<>("_ClusterMessageOut", "null", "ClusterMessageOut", "Xkui", "server", "Xkuia", org.nuclos.businessentity.ClusterMessageOut.class);


/**
 * Attribute: balancerContext
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRBALANCERCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> BalancerContext = new StringAttribute<>("BalancerContext", "org.nuclos.businessentity", "09Z3", "09Z3n", java.lang.String.class);


/**
 * Attribute: balancerHostname
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRBALANCERHOSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> BalancerHostname = new StringAttribute<>("BalancerHostname", "org.nuclos.businessentity", "09Z3", "09Z3l", java.lang.String.class);


/**
 * Attribute: balancerPort
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: INTBALANCERPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 5
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> BalancerPort = 
	new NumericAttribute<>("BalancerPort", "org.nuclos.businessentity", "09Z3", "09Z3m", java.lang.Integer.class);


/**
 * Attribute: balancerProtocol
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRBALANCERPROTOCOL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> BalancerProtocol = new StringAttribute<>("BalancerProtocol", "org.nuclos.businessentity", "09Z3", "09Z3k", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "09Z3", "09Z33", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "09Z3", "09Z34", java.lang.String.class);


/**
 * Attribute: context
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Context = new StringAttribute<>("Context", "org.nuclos.businessentity", "09Z3", "09Z3g", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "09Z3", "09Z31", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "09Z3", "09Z32", java.lang.String.class);


/**
 * Attribute: hostname
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRHOSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Hostname = new StringAttribute<>("Hostname", "org.nuclos.businessentity", "09Z3", "09Z3b", java.lang.String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "09Z3", "09Z30", org.nuclos.common.UID.class);


/**
 * Attribute: installedVersion
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRINSTALLEDVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> InstalledVersion = new StringAttribute<>("InstalledVersion", "org.nuclos.businessentity", "09Z3", "09Z3h", java.lang.String.class);


/**
 * Attribute: javaVersion
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRJAVAVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> JavaVersion = new StringAttribute<>("JavaVersion", "org.nuclos.businessentity", "09Z3", "09Z3i", java.lang.String.class);


/**
 * Attribute: lastStatusUpdate
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: DATLASTSTATUSUPDATE
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> LastStatusUpdate = new NumericAttribute<>("LastStatusUpdate", "org.nuclos.businessentity", "09Z3", "09Z3c", java.util.Date.class);


/**
 * Attribute: port
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: INTPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 5
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Port = 
	new NumericAttribute<>("Port", "org.nuclos.businessentity", "09Z3", "09Z3f", java.lang.Integer.class);


/**
 * Attribute: protocol
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRPROTOCOL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Protocol = new StringAttribute<>("Protocol", "org.nuclos.businessentity", "09Z3", "09Z3a", java.lang.String.class);


/**
 * Attribute: serverExtensionsHash
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRSERVEREXTENSIONSHASH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ServerExtensionsHash = new StringAttribute<>("ServerExtensionsHash", "org.nuclos.businessentity", "09Z3", "09Z3j", java.lang.String.class);


/**
 * Attribute: status
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRSTATUS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Status = new StringAttribute<>("Status", "org.nuclos.businessentity", "09Z3", "09Z3e", java.lang.String.class);


/**
 * Attribute: type
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Type = new StringAttribute<>("Type", "org.nuclos.businessentity", "09Z3", "09Z3d", java.lang.String.class);


public ClusterServer() {
		super("09Z3");
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(ClusterServer boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public ClusterServer copy() {
		return super.copy(ClusterServer.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("09Z3"), id);
}


/**
 * Delete-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteClusterLiveStatus(org.nuclos.businessentity.ClusterLiveStatus pClusterLiveStatus) {
		deleteDependent(_ClusterLiveStatus, pClusterLiveStatus);
}


/**
 * Delete-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteClusterMessage(org.nuclos.businessentity.ClusterMessage pClusterMessage) {
		deleteDependent(_ClusterMessage, pClusterMessage);
}


/**
 * Delete-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageIn
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteClusterMessageIn(org.nuclos.businessentity.ClusterMessageIn pClusterMessageIn) {
		deleteDependent(_ClusterMessageIn, pClusterMessageIn);
}


/**
 * Delete-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteClusterMessageOut(org.nuclos.businessentity.ClusterMessageOut pClusterMessageOut) {
		deleteDependent(_ClusterMessageOut, pClusterMessageOut);
}
/**
* Static Get by Id
*/
public static ClusterServer get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(ClusterServer.class, id, attributes);
}


/**
 * Getter-Method for attribute: balancerContext
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRBALANCERCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getBalancerContext() {
		return getField("09Z3n", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: balancerHostname
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRBALANCERHOSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getBalancerHostname() {
		return getField("09Z3l", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: balancerPort
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: INTBALANCERPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 5
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.Integer getBalancerPort() {
		return getField("09Z3m", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: balancerProtocol
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRBALANCERPROTOCOL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getBalancerProtocol() {
		return getField("09Z3k", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("09Z33", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("09Z34", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.ClusterLiveStatus> getClusterLiveStatus(Flag... flags) {
		return getDependents(_ClusterLiveStatus, flags); 
}


/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.ClusterMessage> getClusterMessage(Flag... flags) {
		return getDependents(_ClusterMessage, flags); 
}


/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageIn
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.ClusterMessageIn> getClusterMessageIn(Flag... flags) {
		return getDependents(_ClusterMessageIn, flags); 
}


/**
 * Getter-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.ClusterMessageOut> getClusterMessageOut(Flag... flags) {
		return getDependents(_ClusterMessageOut, flags); 
}


/**
 * Getter-Method for attribute: context
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getContext() {
		return getField("09Z3g", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("09Z31", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("09Z32", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("09Z3");
}


/**
 * Getter-Method for attribute: hostname
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRHOSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getHostname() {
		return getField("09Z3b", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: installedVersion
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRINSTALLEDVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getInstalledVersion() {
		return getField("09Z3h", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: javaVersion
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRJAVAVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getJavaVersion() {
		return getField("09Z3i", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: lastStatusUpdate
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: DATLASTSTATUSUPDATE
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.util.Date getLastStatusUpdate() {
		return getField("09Z3c", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: port
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: INTPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 5
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Integer getPort() {
		return getField("09Z3f", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: protocol
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRPROTOCOL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getProtocol() {
		return getField("09Z3a", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: serverExtensionsHash
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRSERVEREXTENSIONSHASH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getServerExtensionsHash() {
		return getField("09Z3j", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: status
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRSTATUS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getStatus() {
		return getField("09Z3e", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getType() {
		return getField("09Z3d", java.lang.String.class); 
}


/**
 * Insert-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterLiveStatus
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertClusterLiveStatus(org.nuclos.businessentity.ClusterLiveStatus pClusterLiveStatus) {
		insertDependent(_ClusterLiveStatus, pClusterLiveStatus);
}


/**
 * Insert-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessage
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertClusterMessage(org.nuclos.businessentity.ClusterMessage pClusterMessage) {
		insertDependent(_ClusterMessage, pClusterMessage);
}


/**
 * Insert-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageIn
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertClusterMessageIn(org.nuclos.businessentity.ClusterMessageIn pClusterMessageIn) {
		insertDependent(_ClusterMessageIn, pClusterMessageIn);
}


/**
 * Insert-Method for attribute: server
 *<br>
 *<br>Entity: nuclos_clusterMessageOut
 *<br>DB-Name: STRUID_CLUSTER_SERVER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_clusterServer
 *<br>Reference field: hostname:port/context
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertClusterMessageOut(org.nuclos.businessentity.ClusterMessageOut pClusterMessageOut) {
		insertDependent(_ClusterMessageOut, pClusterMessageOut);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: balancerContext
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRBALANCERCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public void setBalancerContext(java.lang.String pBalancerContext) {
		setField("09Z3n", pBalancerContext); 
}


/**
 * Setter-Method for attribute: balancerHostname
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRBALANCERHOSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public void setBalancerHostname(java.lang.String pBalancerHostname) {
		setField("09Z3l", pBalancerHostname); 
}


/**
 * Setter-Method for attribute: balancerPort
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: INTBALANCERPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 5
 *<br>Precision: null
**/
public void setBalancerPort(java.lang.Integer pBalancerPort) {
		setField("09Z3m", pBalancerPort); 
}


/**
 * Setter-Method for attribute: balancerProtocol
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRBALANCERPROTOCOL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public void setBalancerProtocol(java.lang.String pBalancerProtocol) {
		setField("09Z3k", pBalancerProtocol); 
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("09Z34", pChangedBy); 
}


/**
 * Setter-Method for attribute: context
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRCONTEXT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public void setContext(java.lang.String pContext) {
		setField("09Z3g", pContext); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("09Z32", pCreatedBy); 
}


/**
 * Setter-Method for attribute: hostname
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRHOSTNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public void setHostname(java.lang.String pHostname) {
		setField("09Z3b", pHostname); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: installedVersion
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRINSTALLEDVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setInstalledVersion(java.lang.String pInstalledVersion) {
		setField("09Z3h", pInstalledVersion); 
}


/**
 * Setter-Method for attribute: javaVersion
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRJAVAVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setJavaVersion(java.lang.String pJavaVersion) {
		setField("09Z3i", pJavaVersion); 
}


/**
 * Setter-Method for attribute: lastStatusUpdate
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: DATLASTSTATUSUPDATE
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setLastStatusUpdate( java.util.Date pLastStatusUpdate) {
		setField("09Z3c", pLastStatusUpdate); 
}


/**
 * Setter-Method for attribute: port
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: INTPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 5
 *<br>Precision: null
**/
public void setPort(java.lang.Integer pPort) {
		setField("09Z3f", pPort); 
}


/**
 * Setter-Method for attribute: protocol
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRPROTOCOL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public void setProtocol(java.lang.String pProtocol) {
		setField("09Z3a", pProtocol); 
}


/**
 * Setter-Method for attribute: serverExtensionsHash
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRSERVEREXTENSIONSHASH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setServerExtensionsHash(java.lang.String pServerExtensionsHash) {
		setField("09Z3j", pServerExtensionsHash); 
}


/**
 * Setter-Method for attribute: status
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRSTATUS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setStatus(java.lang.String pStatus) {
		setField("09Z3e", pStatus); 
}


/**
 * Setter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_clusterServer
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public void setType(java.lang.String pType) {
		setField("09Z3d", pType); 
}
 }
