//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_documentDir
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_UD_DOCUMENTDIR
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class DocumentDir extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "MaSd", "MaSd3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "MaSd", "MaSd4", java.lang.String.class);


/**
 * Attribute: count
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: INTCOUNT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Count = 
	new NumericAttribute<>("Count", "org.nuclos.businessentity", "MaSd", "MaSdb", java.lang.Integer.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "MaSd", "MaSd1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "MaSd", "MaSd2", java.lang.String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "MaSd", "MaSd0", org.nuclos.common.UID.class);


/**
 * Attribute: parent
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRPARENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Parent = new StringAttribute<>("Parent", "org.nuclos.businessentity", "MaSd", "MaSda", java.lang.String.class);


public DocumentDir() {
		super("MaSd");
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(DocumentDir boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public DocumentDir copy() {
		return super.copy(DocumentDir.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("MaSd"), id);
}
/**
* Static Get by Id
*/
public static DocumentDir get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(DocumentDir.class, id, attributes);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("MaSd3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("MaSd4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: count
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: INTCOUNT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Integer getCount() {
		return getField("MaSdb", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("MaSd1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("MaSd2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("MaSd");
}


/**
 * Getter-Method for attribute: parent
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRPARENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getParent() {
		return getField("MaSda", java.lang.String.class); 
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("MaSd4", pChangedBy); 
}


/**
 * Setter-Method for attribute: count
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: INTCOUNT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setCount(java.lang.Integer pCount) {
		setField("MaSdb", pCount); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("MaSd2", pCreatedBy); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: parent
 *<br>
 *<br>Entity: nuclos_documentDir
 *<br>DB-Name: STRPARENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setParent(java.lang.String pParent) {
		setField("MaSda", pParent); 
}
 }
