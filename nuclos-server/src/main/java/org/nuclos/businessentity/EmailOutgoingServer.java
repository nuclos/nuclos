//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_emailOutgoingServer
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_AD_EMAILOUTGOINGSERVER
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class EmailOutgoingServer extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: active
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Active = 
	new Attribute<>("Active", "org.nuclos.businessentity", "k35u", "k35um", java.lang.Boolean.class);


/**
 * Attribute: authenticationMethod
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRAUTHENTICATIONMETHOD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> AuthenticationMethod = new StringAttribute<>("AuthenticationMethod", "org.nuclos.businessentity", "k35u", "k35uf", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "k35u", "k35u3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "k35u", "k35u4", java.lang.String.class);


/**
 * Attribute: connectionSecurity
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRCONNECTIONSECURITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ConnectionSecurity = new StringAttribute<>("ConnectionSecurity", "org.nuclos.businessentity", "k35u", "k35ue", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "k35u", "k35u1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "k35u", "k35u2", java.lang.String.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Description = new StringAttribute<>("Description", "org.nuclos.businessentity", "k35u", "k35ub", java.lang.String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "k35u", "k35u0", org.nuclos.common.UID.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "k35u", "k35ua", java.lang.String.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NucletId = 
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.businessentity", "k35u", "k35uk", org.nuclos.common.UID.class);


/**
 * Attribute: oAuth2AccessToken
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_ACCESS_TOKEN
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2AccessToken = new StringAttribute<>("OAuth2AccessToken", "org.nuclos.businessentity", "k35u", "k35uu", java.lang.String.class);


/**
 * Attribute: oAuth2Callback
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_CALLBACK
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2Callback = new StringAttribute<>("OAuth2Callback", "org.nuclos.businessentity", "k35u", "k35ut", java.lang.String.class);


/**
 * Attribute: oAuth2ClientId
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_CLIENTID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2ClientId = new StringAttribute<>("OAuth2ClientId", "org.nuclos.businessentity", "k35u", "k35uq", java.lang.String.class);


/**
 * Attribute: oAuth2ClientSecret
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_CLIENTSECRET
 *<br>Data type: org.nuclos.common.NuclosPassword
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2ClientSecret = new StringAttribute<>("OAuth2ClientSecret", "org.nuclos.businessentity", "k35u", "k35ur", java.lang.String.class);


/**
 * Attribute: oAuth2Endpoint
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_ENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2Endpoint = new StringAttribute<>("OAuth2Endpoint", "org.nuclos.businessentity", "k35u", "k35uo", java.lang.String.class);


/**
 * Attribute: oAuth2Expires
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_EXPIRES
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Long> OAuth2Expires = 
	new NumericAttribute<>("OAuth2Expires", "org.nuclos.businessentity", "k35u", "k35uw", java.lang.Long.class);


/**
 * Attribute: oAuth2RefreshToken
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_REFRESH_TOKEN
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2RefreshToken = new StringAttribute<>("OAuth2RefreshToken", "org.nuclos.businessentity", "k35u", "k35uv", java.lang.String.class);


/**
 * Attribute: oAuth2Scopes
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_SCOPES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2Scopes = new StringAttribute<>("OAuth2Scopes", "org.nuclos.businessentity", "k35u", "k35us", java.lang.String.class);


/**
 * Attribute: oAuth2TokenEndpoint
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_TOKENENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2TokenEndpoint = new StringAttribute<>("OAuth2TokenEndpoint", "org.nuclos.businessentity", "k35u", "k35up", java.lang.String.class);


/**
 * Attribute: order
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Order = 
	new NumericAttribute<>("Order", "org.nuclos.businessentity", "k35u", "k35uj", java.lang.Integer.class);


/**
 * Attribute: password
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRPASSWORD
 *<br>Data type: org.nuclos.common.NuclosPassword
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Password = new StringAttribute<>("Password", "org.nuclos.businessentity", "k35u", "k35uh", java.lang.String.class);


/**
 * Attribute: port
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: INTPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Port = 
	new NumericAttribute<>("Port", "org.nuclos.businessentity", "k35u", "k35ud", java.lang.Integer.class);


/**
 * Attribute: sender
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRSENDER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Sender = new StringAttribute<>("Sender", "org.nuclos.businessentity", "k35u", "k35ui", java.lang.String.class);


/**
 * Attribute: serverName
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRSERVERNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ServerName = new StringAttribute<>("ServerName", "org.nuclos.businessentity", "k35u", "k35uc", java.lang.String.class);


/**
 * Attribute: testRecipient
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRTESTRECIPIENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> TestRecipient = new StringAttribute<>("TestRecipient", "org.nuclos.businessentity", "k35u", "k35un", java.lang.String.class);


/**
 * Attribute: testResult
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRTESTRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> TestResult = new StringAttribute<>("TestResult", "org.nuclos.businessentity", "k35u", "k35ul", java.lang.String.class);


/**
 * Attribute: userName
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRUSERNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> UserName = new StringAttribute<>("UserName", "org.nuclos.businessentity", "k35u", "k35ug", java.lang.String.class);


public EmailOutgoingServer() {
		super("k35u");
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(EmailOutgoingServer boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public EmailOutgoingServer copy() {
		return super.copy(EmailOutgoingServer.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("k35u"), id);
}
/**
* Static Get by Id
*/
public static EmailOutgoingServer get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(EmailOutgoingServer.class, id, attributes);
}


/**
 * Getter-Method for attribute: active
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.Boolean getActive() {
		return getField("k35um", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: authenticationMethod
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRAUTHENTICATIONMETHOD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getAuthenticationMethod() {
		return getField("k35uf", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("k35u3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("k35u4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: connectionSecurity
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRCONNECTIONSECURITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getConnectionSecurity() {
		return getField("k35ue", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("k35u1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("k35u2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getDescription() {
		return getField("k35ub", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("k35u");
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getName() {
		return getField("k35ua", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.businessentity.Nuclet getNucletBO() {
		return getReferencedBO(org.nuclos.businessentity.Nuclet.class, getFieldUid("k35uk"), "k35uk", "xojr");
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("k35uk");
}


/**
 * Getter-Method for attribute: oAuth2AccessToken
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_ACCESS_TOKEN
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2AccessToken() {
		return getField("k35uu", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2Callback
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_CALLBACK
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2Callback() {
		return getField("k35ut", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2ClientId
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_CLIENTID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2ClientId() {
		return getField("k35uq", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2ClientSecret
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_CLIENTSECRET
 *<br>Data type: org.nuclos.common.NuclosPassword
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2ClientSecret() {
		return getField("k35ur", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2Endpoint
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_ENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2Endpoint() {
		return getField("k35uo", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2Expires
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_EXPIRES
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.Long getOAuth2Expires() {
		return getField("k35uw", java.lang.Long.class); 
}


/**
 * Getter-Method for attribute: oAuth2RefreshToken
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_REFRESH_TOKEN
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2RefreshToken() {
		return getField("k35uv", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2Scopes
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_SCOPES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2Scopes() {
		return getField("k35us", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2TokenEndpoint
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_TOKENENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2TokenEndpoint() {
		return getField("k35up", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: order
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Integer getOrder() {
		return getField("k35uj", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: password
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRPASSWORD
 *<br>Data type: org.nuclos.common.NuclosPassword
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getPassword() {
		return getField("k35uh", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: port
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: INTPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Integer getPort() {
		return getField("k35ud", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: sender
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRSENDER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getSender() {
		return getField("k35ui", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: serverName
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRSERVERNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getServerName() {
		return getField("k35uc", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: testRecipient
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRTESTRECIPIENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getTestRecipient() {
		return getField("k35un", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: testResult
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRTESTRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getTestResult() {
		return getField("k35ul", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: userName
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRUSERNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getUserName() {
		return getField("k35ug", java.lang.String.class); 
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: active
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setActive(java.lang.Boolean pActive) {
		setField("k35um", pActive); 
}


/**
 * Setter-Method for attribute: authenticationMethod
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRAUTHENTICATIONMETHOD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public void setAuthenticationMethod(java.lang.String pAuthenticationMethod) {
		setField("k35uf", pAuthenticationMethod); 
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("k35u4", pChangedBy); 
}


/**
 * Setter-Method for attribute: connectionSecurity
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRCONNECTIONSECURITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public void setConnectionSecurity(java.lang.String pConnectionSecurity) {
		setField("k35ue", pConnectionSecurity); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("k35u2", pCreatedBy); 
}


/**
 * Setter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setDescription(java.lang.String pDescription) {
		setField("k35ub", pDescription); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("k35ua", pName); 
}


/**
 * Setter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNucletId(org.nuclos.common.UID pNucletId) {
		setFieldId("k35uk", pNucletId); 
}


/**
 * Setter-Method for attribute: oAuth2AccessToken
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_ACCESS_TOKEN
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setOAuth2AccessToken(java.lang.String pOAuth2AccessToken) {
		setField("k35uu", pOAuth2AccessToken); 
}


/**
 * Setter-Method for attribute: oAuth2Callback
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_CALLBACK
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2Callback(java.lang.String pOAuth2Callback) {
		setField("k35ut", pOAuth2Callback); 
}


/**
 * Setter-Method for attribute: oAuth2ClientId
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_CLIENTID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2ClientId(java.lang.String pOAuth2ClientId) {
		setField("k35uq", pOAuth2ClientId); 
}


/**
 * Setter-Method for attribute: oAuth2ClientSecret
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_CLIENTSECRET
 *<br>Data type: org.nuclos.common.NuclosPassword
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2ClientSecret(java.lang.String pOAuth2ClientSecret) {
		setField("k35ur", pOAuth2ClientSecret); 
}


/**
 * Setter-Method for attribute: oAuth2Endpoint
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_ENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2Endpoint(java.lang.String pOAuth2Endpoint) {
		setField("k35uo", pOAuth2Endpoint); 
}


/**
 * Setter-Method for attribute: oAuth2Expires
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_EXPIRES
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public void setOAuth2Expires(java.lang.Long pOAuth2Expires) {
		setField("k35uw", pOAuth2Expires); 
}


/**
 * Setter-Method for attribute: oAuth2RefreshToken
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_REFRESH_TOKEN
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setOAuth2RefreshToken(java.lang.String pOAuth2RefreshToken) {
		setField("k35uv", pOAuth2RefreshToken); 
}


/**
 * Setter-Method for attribute: oAuth2Scopes
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_SCOPES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2Scopes(java.lang.String pOAuth2Scopes) {
		setField("k35us", pOAuth2Scopes); 
}


/**
 * Setter-Method for attribute: oAuth2TokenEndpoint
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STROAUTH2_TOKENENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2TokenEndpoint(java.lang.String pOAuth2TokenEndpoint) {
		setField("k35up", pOAuth2TokenEndpoint); 
}


/**
 * Setter-Method for attribute: order
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setOrder(java.lang.Integer pOrder) {
		setField("k35uj", pOrder); 
}


/**
 * Setter-Method for attribute: password
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRPASSWORD
 *<br>Data type: org.nuclos.common.NuclosPassword
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public void setPassword(java.lang.String pPassword) {
		setField("k35uh", pPassword); 
}


/**
 * Setter-Method for attribute: port
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: INTPORT
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setPort(java.lang.Integer pPort) {
		setField("k35ud", pPort); 
}


/**
 * Setter-Method for attribute: sender
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRSENDER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public void setSender(java.lang.String pSender) {
		setField("k35ui", pSender); 
}


/**
 * Setter-Method for attribute: serverName
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRSERVERNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public void setServerName(java.lang.String pServerName) {
		setField("k35uc", pServerName); 
}


/**
 * Setter-Method for attribute: testRecipient
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRTESTRECIPIENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public void setTestRecipient(java.lang.String pTestRecipient) {
		setField("k35un", pTestRecipient); 
}


/**
 * Setter-Method for attribute: testResult
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRTESTRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setTestResult(java.lang.String pTestResult) {
		setField("k35ul", pTestResult); 
}


/**
 * Setter-Method for attribute: userName
 *<br>
 *<br>Entity: nuclos_emailOutgoingServer
 *<br>DB-Name: STRUSERNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 256
 *<br>Precision: null
**/
public void setUserName(java.lang.String pUserName) {
		setField("k35ug", pUserName); 
}
 }
