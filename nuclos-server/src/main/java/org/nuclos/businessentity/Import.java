//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_import
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_IMPORT
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class Import extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "rY7H", "rY7H3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "rY7H", "rY7H4", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "rY7H", "rY7H1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "rY7H", "rY7H2", java.lang.String.class);


/**
 * Attribute: delete
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: BLNDELETE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Delete = 
	new Attribute<>("Delete", "org.nuclos.businessentity", "rY7H", "rY7Hi", java.lang.Boolean.class);


/**
 * Attribute: delimiter
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRDELIMITER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Delimiter = new StringAttribute<>("Delimiter", "org.nuclos.businessentity", "rY7H", "rY7Hd", java.lang.String.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Description = new StringAttribute<>("Description", "org.nuclos.businessentity", "rY7H", "rY7He", java.lang.String.class);


/**
 * Attribute: encoding
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRENCODING
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Encoding = new StringAttribute<>("Encoding", "org.nuclos.businessentity", "rY7H", "rY7Hk", java.lang.String.class);


/**
 * Attribute: entity
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_AD_MASTERDATA
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> EntityId = 
	new ForeignKeyAttribute<>("EntityId", "org.nuclos.businessentity", "rY7H", "rY7Hf", org.nuclos.common.UID.class);


/**
 * Attribute: headerlines
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: INTHEADERLINES
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Headerlines = 
	new NumericAttribute<>("Headerlines", "org.nuclos.businessentity", "rY7H", "rY7Hc", java.lang.Integer.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "rY7H", "rY7H0", org.nuclos.common.UID.class);


/**
 * Attribute: insert
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: BLNINSERT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Insert = 
	new Attribute<>("Insert", "org.nuclos.businessentity", "rY7H", "rY7Hg", java.lang.Boolean.class);


/**
 * Attribute: mode
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 40
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Mode = new StringAttribute<>("Mode", "org.nuclos.businessentity", "rY7H", "rY7Hb", java.lang.String.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "rY7H", "rY7Ha", java.lang.String.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NucletId = 
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.businessentity", "rY7H", "rY7Hj", org.nuclos.common.UID.class);


/**
 * Attribute: update
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: BLNUPDATE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Update = 
	new Attribute<>("Update", "org.nuclos.businessentity", "rY7H", "rY7Hh", java.lang.Boolean.class);


public Import() {
		super("rY7H");
		setDelete(java.lang.Boolean.FALSE);
		setInsert(java.lang.Boolean.FALSE);
		setUpdate(java.lang.Boolean.FALSE);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(Import boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public Import copy() {
		return super.copy(Import.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("rY7H"), id);
}
/**
* Static Get by Id
*/
public static Import get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(Import.class, id, attributes);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("rY7H3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("rY7H4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("rY7H1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("rY7H2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: delete
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: BLNDELETE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getDelete() {
		return getField("rY7Hi", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: delimiter
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRDELIMITER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getDelimiter() {
		return getField("rY7Hd", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getDescription() {
		return getField("rY7He", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: encoding
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRENCODING
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getEncoding() {
		return getField("rY7Hk", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_AD_MASTERDATA
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.businessentity.Entity getEntityBO() {
		return getReferencedBO(org.nuclos.businessentity.Entity.class, getFieldUid("rY7Hf"), "rY7Hf", "5E8q");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_AD_MASTERDATA
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.common.UID getEntityId() {
		return getFieldUid("rY7Hf");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("rY7H");
}


/**
 * Getter-Method for attribute: headerlines
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: INTHEADERLINES
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Integer getHeaderlines() {
		return getField("rY7Hc", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: insert
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: BLNINSERT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getInsert() {
		return getField("rY7Hg", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: mode
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 40
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getMode() {
		return getField("rY7Hb", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getName() {
		return getField("rY7Ha", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.businessentity.Nuclet getNucletBO() {
		return getReferencedBO(org.nuclos.businessentity.Nuclet.class, getFieldUid("rY7Hj"), "rY7Hj", "xojr");
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("rY7Hj");
}


/**
 * Getter-Method for attribute: update
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: BLNUPDATE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getUpdate() {
		return getField("rY7Hh", java.lang.Boolean.class); 
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("rY7H4", pChangedBy); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("rY7H2", pCreatedBy); 
}


/**
 * Setter-Method for attribute: delete
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: BLNDELETE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setDelete(java.lang.Boolean pDelete) {
		setField("rY7Hi", pDelete); 
}


/**
 * Setter-Method for attribute: delimiter
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRDELIMITER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public void setDelimiter(java.lang.String pDelimiter) {
		setField("rY7Hd", pDelimiter); 
}


/**
 * Setter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setDescription(java.lang.String pDescription) {
		setField("rY7He", pDescription); 
}


/**
 * Setter-Method for attribute: encoding
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRENCODING
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public void setEncoding(java.lang.String pEncoding) {
		setField("rY7Hk", pEncoding); 
}


/**
 * Setter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_AD_MASTERDATA
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setEntityId(org.nuclos.common.UID pEntityId) {
		setFieldId("rY7Hf", pEntityId); 
}


/**
 * Setter-Method for attribute: headerlines
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: INTHEADERLINES
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setHeaderlines(java.lang.Integer pHeaderlines) {
		setField("rY7Hc", pHeaderlines); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: insert
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: BLNINSERT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setInsert(java.lang.Boolean pInsert) {
		setField("rY7Hg", pInsert); 
}


/**
 * Setter-Method for attribute: mode
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 40
 *<br>Precision: null
**/
public void setMode(java.lang.String pMode) {
		setField("rY7Hb", pMode); 
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("rY7Ha", pName); 
}


/**
 * Setter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNucletId(org.nuclos.common.UID pNucletId) {
		setFieldId("rY7Hj", pNucletId); 
}


/**
 * Setter-Method for attribute: update
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: BLNUPDATE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setUpdate(java.lang.Boolean pUpdate) {
		setField("rY7Hh", pUpdate); 
}
 }
