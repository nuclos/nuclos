//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_importfile
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_IMPORTFILE
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class Importfile extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: atomic
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: BLNATOMIC
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Atomic = 
	new Attribute<>("Atomic", "org.nuclos.businessentity", "npYQ", "npYQf", java.lang.Boolean.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "npYQ", "npYQ3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "npYQ", "npYQ4", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "npYQ", "npYQ1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "npYQ", "npYQ2", java.lang.String.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Description = new StringAttribute<>("Description", "org.nuclos.businessentity", "npYQ", "npYQb", java.lang.String.class);


/**
 * Attribute: documentfile
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRUID_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> DocumentfileId = 
	new ForeignKeyAttribute<>("DocumentfileId", "org.nuclos.businessentity", "npYQ", "npYQa", org.nuclos.common.UID.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "npYQ", "npYQ0", org.nuclos.common.UID.class);


/**
 * Attribute: laststate
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRLASTSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Laststate = new StringAttribute<>("Laststate", "org.nuclos.businessentity", "npYQ", "npYQd", java.lang.String.class);


/**
 * Attribute: logfile
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRUID_LOGFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> LogfileId = 
	new ForeignKeyAttribute<>("LogfileId", "org.nuclos.businessentity", "npYQ", "npYQg", org.nuclos.common.UID.class);


/**
 * Attribute: mode
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 40
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Mode = new StringAttribute<>("Mode", "org.nuclos.businessentity", "npYQ", "npYQe", java.lang.String.class);


/**
 * Attribute: result
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Result = new StringAttribute<>("Result", "org.nuclos.businessentity", "npYQ", "npYQc", java.lang.String.class);


public Importfile() {
		super("npYQ");
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(Importfile boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public Importfile copy() {
		return super.copy(Importfile.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("npYQ"), id);
}
/**
* Static Get by Id
*/
public static Importfile get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(Importfile.class, id, attributes);
}


/**
 * Getter-Method for attribute: atomic
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: BLNATOMIC
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.Boolean getAtomic() {
		return getField("npYQf", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("npYQ3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("npYQ4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("npYQ1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("npYQ2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getDescription() {
		return getField("npYQb", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRUID_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.api.common.NuclosFile getDocumentfile() {
		return getNuclosFile("npYQa");
}


/**
 * Getter-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRUID_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> T getDocumentfile(Class<T> pDocumentfile) {
		return super.getNuclosFile(pDocumentfile,"npYQa");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("npYQ");
}


/**
 * Getter-Method for attribute: laststate
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRLASTSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getLaststate() {
		return getField("npYQd", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: logfile
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRUID_LOGFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.api.common.NuclosFile getLogfile() {
		return getNuclosFile("npYQg");
}


/**
 * Getter-Method for attribute: logfile
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRUID_LOGFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> T getLogfile(Class<T> pLogfile) {
		return super.getNuclosFile(pLogfile,"npYQg");
}


/**
 * Getter-Method for attribute: mode
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 40
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getMode() {
		return getField("npYQe", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: result
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getResult() {
		return getField("npYQc", java.lang.String.class); 
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: atomic
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: BLNATOMIC
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setAtomic(java.lang.Boolean pAtomic) {
		setField("npYQf", pAtomic); 
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("npYQ4", pChangedBy); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("npYQ2", pCreatedBy); 
}


/**
 * Setter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setDescription(java.lang.String pDescription) {
		setField("npYQb", pDescription); 
}


/**
 * Setter-Method for attribute: documentfile
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRUID_DOCUMENTFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> void setDocumentfile(T pDocumentfile) {
		super.setNuclosFile(pDocumentfile,"npYQa");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: laststate
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRLASTSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public void setLaststate(java.lang.String pLaststate) {
		setField("npYQd", pLaststate); 
}


/**
 * Setter-Method for attribute: logfile
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRUID_LOGFILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> void setLogfile(T pLogfile) {
		super.setNuclosFile(pLogfile,"npYQg");
}


/**
 * Setter-Method for attribute: mode
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 40
 *<br>Precision: null
**/
public void setMode(java.lang.String pMode) {
		setField("npYQe", pMode); 
}


/**
 * Setter-Method for attribute: result
 *<br>
 *<br>Entity: nuclos_importfile
 *<br>DB-Name: STRRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setResult(java.lang.String pResult) {
		setField("npYQc", pResult); 
}
 }
