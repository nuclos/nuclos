//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_jobQueueRun
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: [proxy]
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class JobQueueRun extends AbstractBusinessObject<java.lang.Long> implements Modifiable<java.lang.Long> {


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "JQEr", "JQEr3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "JQEr", "JQEr4", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "JQEr", "JQEr1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "JQEr", "JQEr2", java.lang.String.class);


/**
 * Attribute: job
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBCONTROLLER
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> JobId = 
	new ForeignKeyAttribute<>("JobId", "org.nuclos.businessentity", "JQEr", "JQErc", org.nuclos.common.UID.class);


/**
 * Attribute: jobQueue
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBQUEUE
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> JobQueueId = 
	new ForeignKeyAttribute<>("JobQueueId", "org.nuclos.businessentity", "JQEr", "JQEra", org.nuclos.common.UID.class);


/**
 * Attribute: order
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Order = 
	new NumericAttribute<>("Order", "org.nuclos.businessentity", "JQEr", "JQErb", java.lang.Integer.class);


public JobQueueRun() {
		super("JQEr");
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(JobQueueRun boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public JobQueueRun copy() {
		return super.copy(JobQueueRun.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(java.lang.Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("JQEr"), id);
}
/**
* Static Get by Id
*/
public static JobQueueRun get(java.lang.Long id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(JobQueueRun.class, id, attributes);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("JQEr3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("JQEr4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("JQEr1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("JQEr2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getEntity() {
		return "nuclos_jobQueueRun";
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("JQEr");
}


/**
 * Getter-Method for attribute: job
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBCONTROLLER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobcontroller
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getJob() {
		return getField("JQErc", org.nuclos.common.UID.class); 
}


/**
 * Getter-Method for attribute: job
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBCONTROLLER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobcontroller
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.businessentity.Jobcontroller getJobBO() {
		return getReferencedBO(org.nuclos.businessentity.Jobcontroller.class, getFieldUid("JQErc"), "JQErc", "LGXb");
}


/**
 * Getter-Method for attribute: job
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBCONTROLLER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobcontroller
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.common.UID getJobId() {
		return getFieldUid("JQErc");
}


/**
 * Getter-Method for attribute: jobQueue
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBQUEUE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobQueue
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getJobQueue() {
		return getField("JQEra", org.nuclos.common.UID.class); 
}


/**
 * Getter-Method for attribute: jobQueue
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBQUEUE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobQueue
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.businessentity.JobQueue getJobQueueBO() {
		return getReferencedBO(org.nuclos.businessentity.JobQueue.class, getFieldUid("JQEra"), "JQEra", "JGXq");
}


/**
 * Getter-Method for attribute: jobQueue
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBQUEUE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobQueue
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.common.UID getJobQueueId() {
		return getFieldUid("JQEra");
}


/**
 * Getter-Method for attribute: order
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Integer getOrder() {
		return getField("JQErb", java.lang.Integer.class); 
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setChangedAt( java.util.Date pChangedAt) {
		setField("JQEr3", pChangedAt); 
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("JQEr4", pChangedBy); 
}


/**
 * Setter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setCreatedAt( java.util.Date pCreatedAt) {
		setField("JQEr1", pCreatedAt); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("JQEr2", pCreatedBy); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
@java.lang.Override
public void setId(java.lang.Long id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: job
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBCONTROLLER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobcontroller
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setJob(java.lang.String pJob) {
		setField("JQErc", pJob); 
}


/**
 * Setter-Method for attribute: job
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBCONTROLLER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobcontroller
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setJobId(org.nuclos.common.UID pJobId) {
		setFieldId("JQErc", pJobId); 
}


/**
 * Setter-Method for attribute: jobQueue
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBQUEUE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobQueue
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setJobQueue(java.lang.String pJobQueue) {
		setField("JQEra", pJobQueue); 
}


/**
 * Setter-Method for attribute: jobQueue
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBQUEUE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobQueue
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setJobQueueId(org.nuclos.common.UID pJobQueueId) {
		setFieldId("JQEra", pJobQueueId); 
}


/**
 * Setter-Method for attribute: order
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setOrder(java.lang.Integer pOrder) {
		setField("JQErb", pOrder); 
}
 }
