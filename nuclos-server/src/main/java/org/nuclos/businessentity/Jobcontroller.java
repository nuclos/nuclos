//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_jobcontroller
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_JOBCONTROLLER
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class Jobcontroller extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;


public static final Dependent<org.nuclos.businessentity.JobQueueRun> _JobQueueRun = 
	new Dependent<>("_JobQueueRun", "null", "JobQueueRun", "JQEr", "job", "JQErc", org.nuclos.businessentity.JobQueueRun.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "LGXb", "LGXb3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "LGXb", "LGXb4", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "LGXb", "LGXb1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "LGXb", "LGXb2", java.lang.String.class);


/**
 * Attribute: cronexpression
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRCRONEXPRESSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Cronexpression = new StringAttribute<>("Cronexpression", "org.nuclos.businessentity", "LGXb", "LGXbi", java.lang.String.class);


/**
 * Attribute: days
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: INTDAYSFORLOGINFO
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
public static final NumericAttribute<java.lang.Integer> Days = 
	new NumericAttribute<>("Days", "org.nuclos.businessentity", "LGXb", "LGXbl", java.lang.Integer.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Description = new StringAttribute<>("Description", "org.nuclos.businessentity", "LGXb", "LGXbc", java.lang.String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "LGXb", "LGXb0", org.nuclos.common.UID.class);


/**
 * Attribute: interval
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: INTINTERVAL
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
public static final NumericAttribute<java.lang.Integer> Interval = 
	new NumericAttribute<>("Interval", "org.nuclos.businessentity", "LGXb", "LGXbf", java.lang.Integer.class);


/**
 * Attribute: lastfiretime
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRLASTFIRETIME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Lastfiretime = new StringAttribute<>("Lastfiretime", "org.nuclos.businessentity", "LGXb", "LGXbo", java.lang.String.class);


/**
 * Attribute: laststate
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRJOBLASTSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Laststate = new StringAttribute<>("Laststate", "org.nuclos.businessentity", "LGXb", "LGXbm", java.lang.String.class);


/**
 * Attribute: level
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRMESSAGELEVEL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Level = new StringAttribute<>("Level", "org.nuclos.businessentity", "LGXb", "LGXbk", java.lang.String.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRJOBNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "LGXb", "LGXba", java.lang.String.class);


/**
 * Attribute: nextfiretime
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRNEXTFIRETIME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Nextfiretime = new StringAttribute<>("Nextfiretime", "org.nuclos.businessentity", "LGXb", "LGXbp", java.lang.String.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NucletId = 
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.businessentity", "LGXb", "LGXbs", org.nuclos.common.UID.class);


/**
 * Attribute: queue
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_QUEUE
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> QueueId = 
	new ForeignKeyAttribute<>("QueueId", "org.nuclos.businessentity", "LGXb", "LGXbt", org.nuclos.common.UID.class);


/**
 * Attribute: result
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Result = new StringAttribute<>("Result", "org.nuclos.businessentity", "LGXb", "LGXbq", java.lang.String.class);


/**
 * Attribute: resultdetails
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRRESULTDETAILS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Resultdetails = new StringAttribute<>("Resultdetails", "org.nuclos.businessentity", "LGXb", "LGXbr", java.lang.String.class);


/**
 * Attribute: running
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: BLNRUNNING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Running = 
	new Attribute<>("Running", "org.nuclos.businessentity", "LGXb", "LGXbn", java.lang.Boolean.class);


/**
 * Attribute: startdate
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: DATSTARTDATE
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> Startdate = 
	new NumericAttribute<>("Startdate", "org.nuclos.businessentity", "LGXb", "LGXbd", java.util.Date.class);


/**
 * Attribute: starttime
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRSTARTTIME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Starttime = new StringAttribute<>("Starttime", "org.nuclos.businessentity", "LGXb", "LGXbe", java.lang.String.class);


/**
 * Attribute: type
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRJOBTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Type = new StringAttribute<>("Type", "org.nuclos.businessentity", "LGXb", "LGXbb", java.lang.String.class);


/**
 * Attribute: unit
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUNIT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Unit = new StringAttribute<>("Unit", "org.nuclos.businessentity", "LGXb", "LGXbg", java.lang.String.class);


/**
 * Attribute: usecronexpression
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: BLNUSECRONEXPRESSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Usecronexpression = 
	new Attribute<>("Usecronexpression", "org.nuclos.businessentity", "LGXb", "LGXbh", java.lang.Boolean.class);


/**
 * Attribute: user
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_USER
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> UserId = 
	new ForeignKeyAttribute<>("UserId", "org.nuclos.businessentity", "LGXb", "LGXbj", org.nuclos.common.UID.class);


public Jobcontroller() {
		super("LGXb");
		setRunning(java.lang.Boolean.FALSE);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(Jobcontroller boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public Jobcontroller copy() {
		return super.copy(Jobcontroller.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("LGXb"), id);
}


/**
 * Delete-Method for attribute: job
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBCONTROLLER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobcontroller
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteJobQueueRun(org.nuclos.businessentity.JobQueueRun pJobQueueRun) {
		deleteDependent(_JobQueueRun, pJobQueueRun);
}
/**
* Static Get by Id
*/
public static Jobcontroller get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(Jobcontroller.class, id, attributes);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("LGXb3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("LGXb4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("LGXb1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("LGXb2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: cronexpression
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRCRONEXPRESSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCronexpression() {
		return getField("LGXbi", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: days
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: INTDAYSFORLOGINFO
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
@javax.annotation.Nullable
public java.lang.Integer getDays() {
		return getField("LGXbl", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getDescription() {
		return getField("LGXbc", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("LGXb");
}


/**
 * Getter-Method for attribute: interval
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: INTINTERVAL
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
@javax.annotation.Nullable
public java.lang.Integer getInterval() {
		return getField("LGXbf", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: job
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBCONTROLLER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobcontroller
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.JobQueueRun> getJobQueueRun(Flag... flags) {
		return getDependents(_JobQueueRun, flags); 
}


/**
 * Getter-Method for attribute: lastfiretime
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRLASTFIRETIME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getLastfiretime() {
		return getField("LGXbo", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: laststate
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRJOBLASTSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getLaststate() {
		return getField("LGXbm", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: level
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRMESSAGELEVEL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getLevel() {
		return getField("LGXbk", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRJOBNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getName() {
		return getField("LGXba", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nextfiretime
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRNEXTFIRETIME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getNextfiretime() {
		return getField("LGXbp", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.businessentity.Nuclet getNucletBO() {
		return getReferencedBO(org.nuclos.businessentity.Nuclet.class, getFieldUid("LGXbs"), "LGXbs", "xojr");
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("LGXbs");
}


/**
 * Getter-Method for attribute: queue
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_QUEUE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobQueue
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.businessentity.JobQueue getQueueBO() {
		return getReferencedBO(org.nuclos.businessentity.JobQueue.class, getFieldUid("LGXbt"), "LGXbt", "JGXq");
}


/**
 * Getter-Method for attribute: queue
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_QUEUE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobQueue
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.common.UID getQueueId() {
		return getFieldUid("LGXbt");
}


/**
 * Getter-Method for attribute: result
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getResult() {
		return getField("LGXbq", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: resultdetails
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRRESULTDETAILS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getResultdetails() {
		return getField("LGXbr", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: running
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: BLNRUNNING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getRunning() {
		return getField("LGXbn", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: startdate
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: DATSTARTDATE
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getStartdate() {
		return getField("LGXbd", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: starttime
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRSTARTTIME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getStarttime() {
		return getField("LGXbe", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRJOBTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getType() {
		return getField("LGXbb", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: unit
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUNIT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getUnit() {
		return getField("LGXbg", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: usecronexpression
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: BLNUSECRONEXPRESSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.Boolean getUsecronexpression() {
		return getField("LGXbh", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: user
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_USER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_user
 *<br>Reference field: username
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.api.UID getUserId() {
		return getFieldUid("LGXbj");
}


/**
 * Insert-Method for attribute: job
 *<br>
 *<br>Entity: nuclos_jobQueueRun
 *<br>DB-Name: STRUID_T_MD_JOBCONTROLLER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobcontroller
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertJobQueueRun(org.nuclos.businessentity.JobQueueRun pJobQueueRun) {
		insertDependent(_JobQueueRun, pJobQueueRun);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("LGXb4", pChangedBy); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("LGXb2", pCreatedBy); 
}


/**
 * Setter-Method for attribute: cronexpression
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRCRONEXPRESSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCronexpression(java.lang.String pCronexpression) {
		setField("LGXbi", pCronexpression); 
}


/**
 * Setter-Method for attribute: days
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: INTDAYSFORLOGINFO
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
public void setDays(java.lang.Integer pDays) {
		setField("LGXbl", pDays); 
}


/**
 * Setter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setDescription(java.lang.String pDescription) {
		setField("LGXbc", pDescription); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: interval
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: INTINTERVAL
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
public void setInterval(java.lang.Integer pInterval) {
		setField("LGXbf", pInterval); 
}


/**
 * Setter-Method for attribute: lastfiretime
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRLASTFIRETIME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public void setLastfiretime(java.lang.String pLastfiretime) {
		setField("LGXbo", pLastfiretime); 
}


/**
 * Setter-Method for attribute: laststate
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRJOBLASTSTATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public void setLaststate(java.lang.String pLaststate) {
		setField("LGXbm", pLaststate); 
}


/**
 * Setter-Method for attribute: level
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRMESSAGELEVEL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public void setLevel(java.lang.String pLevel) {
		setField("LGXbk", pLevel); 
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRJOBNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("LGXba", pName); 
}


/**
 * Setter-Method for attribute: nextfiretime
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRNEXTFIRETIME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public void setNextfiretime(java.lang.String pNextfiretime) {
		setField("LGXbp", pNextfiretime); 
}


/**
 * Setter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNucletId(org.nuclos.common.UID pNucletId) {
		setFieldId("LGXbs", pNucletId); 
}


/**
 * Setter-Method for attribute: queue
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_QUEUE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_jobQueue
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setQueueId(org.nuclos.common.UID pQueueId) {
		setFieldId("LGXbt", pQueueId); 
}


/**
 * Setter-Method for attribute: result
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public void setResult(java.lang.String pResult) {
		setField("LGXbq", pResult); 
}


/**
 * Setter-Method for attribute: resultdetails
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRRESULTDETAILS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setResultdetails(java.lang.String pResultdetails) {
		setField("LGXbr", pResultdetails); 
}


/**
 * Setter-Method for attribute: running
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: BLNRUNNING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public void setRunning(java.lang.Boolean pRunning) {
		setField("LGXbn", pRunning); 
}


/**
 * Setter-Method for attribute: startdate
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: DATSTARTDATE
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setStartdate(java.util.Date pStartdate) {
		setField("LGXbd", pStartdate); 
}


/**
 * Setter-Method for attribute: starttime
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRSTARTTIME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public void setStarttime(java.lang.String pStarttime) {
		setField("LGXbe", pStarttime); 
}


/**
 * Setter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRJOBTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setType(java.lang.String pType) {
		setField("LGXbb", pType); 
}


/**
 * Setter-Method for attribute: unit
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUNIT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public void setUnit(java.lang.String pUnit) {
		setField("LGXbg", pUnit); 
}


/**
 * Setter-Method for attribute: usecronexpression
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: BLNUSECRONEXPRESSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setUsecronexpression(java.lang.Boolean pUsecronexpression) {
		setField("LGXbh", pUsecronexpression); 
}


/**
 * Setter-Method for attribute: user
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_USER
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_user
 *<br>Reference field: username
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setUserId(org.nuclos.api.UID pUserId) {
		setFieldId("LGXbj", pUserId); 
}
 }
