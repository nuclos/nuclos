//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_nuclet
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_AD_APPLICATION
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class Nuclet extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;


public static final Dependent<org.nuclos.businessentity.Entity> _Entity = 
	new Dependent<>("_Entity", "null", "Entity", "5E8q", "nuclet", "5E8qc", org.nuclos.businessentity.Entity.class);

public static final Dependent<org.nuclos.businessentity.Import> _Import = 
	new Dependent<>("_Import", "null", "Import", "rY7H", "nuclet", "rY7Hj", org.nuclos.businessentity.Import.class);

public static final Dependent<org.nuclos.businessentity.Jobcontroller> _Jobcontroller = 
	new Dependent<>("_Jobcontroller", "null", "Jobcontroller", "LGXb", "nuclet", "LGXbs", org.nuclos.businessentity.Jobcontroller.class);

public static final Dependent<org.nuclos.businessentity.JobQueue> _JobQueue = 
	new Dependent<>("_JobQueue", "null", "JobQueue", "JGXq", "nuclet", "JGXqs", org.nuclos.businessentity.JobQueue.class);

public static final Dependent<org.nuclos.businessentity.NucletExtension> _NucletExtension = 
	new Dependent<>("_NucletExtension", "null", "NucletExtension", "hajl", "nuclet", "hajla", org.nuclos.businessentity.NucletExtension.class);

public static final Dependent<org.nuclos.businessentity.NucletImport> _NucletImport = 
	new Dependent<>("_NucletImport", "null", "NucletImport", "mVjz", "nuclet", "mVjza", org.nuclos.businessentity.NucletImport.class);

public static final Dependent<org.nuclos.businessentity.NucletIntegrationPoint> _NucletIntegrationPoint1 = 
	new Dependent<>("_NucletIntegrationPoint1", "null", "NucletIntegrationPoint", "kIL5", "nuclet", "kIL5a", org.nuclos.businessentity.NucletIntegrationPoint.class);

public static final Dependent<org.nuclos.businessentity.NucletIntegrationPoint> _NucletIntegrationPoint2 = 
	new Dependent<>("_NucletIntegrationPoint2", "null", "NucletIntegrationPoint", "kIL5", "targetNuclet", "kIL5d", org.nuclos.businessentity.NucletIntegrationPoint.class);

public static final Dependent<org.nuclos.businessentity.NucletSqlConfig> _NucletSqlConfig = 
	new Dependent<>("_NucletSqlConfig", "null", "NucletSqlConfig", "H4n3", "nuclet", "H4n3a", org.nuclos.businessentity.NucletSqlConfig.class);

public static final Dependent<org.nuclos.businessentity.Restapis> _Restapis = 
	new Dependent<>("_Restapis", "null", "Restapis", "I5mQ", "nuclet", "I5mQa", org.nuclos.businessentity.Restapis.class);

public static final Dependent<org.nuclos.businessentity.WebAddon> _WebAddon = 
	new Dependent<>("_WebAddon", "null", "WebAddon", "hyVG", "nuclet", "hyVGa", org.nuclos.businessentity.WebAddon.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "xojr", "xojr3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "xojr", "xojr4", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "xojr", "xojr1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "xojr", "xojr2", java.lang.String.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Description = new StringAttribute<>("Description", "org.nuclos.businessentity", "xojr", "xojrb", java.lang.String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "xojr", "xojr0", org.nuclos.common.UID.class);


/**
 * Attribute: localidentifier
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRLOCALIDENTIFIER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Localidentifier = new StringAttribute<>("Localidentifier", "org.nuclos.businessentity", "xojr", "xojrf", java.lang.String.class);


/**
 * Attribute: masterNuclet
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNMASTERNUCLET
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> MasterNuclet = 
	new Attribute<>("MasterNuclet", "org.nuclos.businessentity", "xojr", "xojrj", java.lang.Boolean.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: NAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "xojr", "xojra", java.lang.String.class);


/**
 * Attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: INTNUCLETVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> NucletVersion = 
	new NumericAttribute<>("NucletVersion", "org.nuclos.businessentity", "xojr", "xojre", java.lang.Integer.class);


/**
 * Attribute: nucletVersionString
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRNUCLETVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> NucletVersionString = new StringAttribute<>("NucletVersionString", "org.nuclos.businessentity", "xojr", "xojri", java.lang.String.class);


/**
 * Attribute: nuclon
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNNUCLON
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Nuclon = 
	new Attribute<>("Nuclon", "org.nuclos.businessentity", "xojr", "xojrg", java.lang.Boolean.class);


/**
 * Attribute: package
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRPACKAGE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Package = new StringAttribute<>("Package", "org.nuclos.businessentity", "xojr", "xojrd", java.lang.String.class);


/**
 * Attribute: source
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNSOURCE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Source = 
	new Attribute<>("Source", "org.nuclos.businessentity", "xojr", "xojrh", java.lang.Boolean.class);


public Nuclet() {
		super("xojr");
		setMasterNuclet(java.lang.Boolean.FALSE);
		setNuclon(java.lang.Boolean.FALSE);
		setSource(java.lang.Boolean.FALSE);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(Nuclet boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public Nuclet copy() {
		return super.copy(Nuclet.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("xojr"), id);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteEntity(org.nuclos.businessentity.Entity pEntity) {
		deleteDependent(_Entity, pEntity);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteImport(org.nuclos.businessentity.Import pImport) {
		deleteDependent(_Import, pImport);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteJobcontroller(org.nuclos.businessentity.Jobcontroller pJobcontroller) {
		deleteDependent(_Jobcontroller, pJobcontroller);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_jobQueue
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteJobQueue(org.nuclos.businessentity.JobQueue pJobQueue) {
		deleteDependent(_JobQueue, pJobQueue);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletExtension(org.nuclos.businessentity.NucletExtension pNucletExtension) {
		deleteDependent(_NucletExtension, pNucletExtension);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletImport(org.nuclos.businessentity.NucletImport pNucletImport) {
		deleteDependent(_NucletImport, pNucletImport);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletIntegrationPoint1(org.nuclos.businessentity.NucletIntegrationPoint pNucletIntegrationPoint) {
		deleteDependent(_NucletIntegrationPoint1, pNucletIntegrationPoint);
}


/**
 * Delete-Method for attribute: targetNuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletIntegrationPoint2(org.nuclos.businessentity.NucletIntegrationPoint pNucletIntegrationPoint) {
		deleteDependent(_NucletIntegrationPoint2, pNucletIntegrationPoint);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletSqlConfig(org.nuclos.businessentity.NucletSqlConfig pNucletSqlConfig) {
		deleteDependent(_NucletSqlConfig, pNucletSqlConfig);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_restapis
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteRestapis(org.nuclos.businessentity.Restapis pRestapis) {
		deleteDependent(_Restapis, pRestapis);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteWebAddon(org.nuclos.businessentity.WebAddon pWebAddon) {
		deleteDependent(_WebAddon, pWebAddon);
}
/**
* Static Get by Id
*/
public static Nuclet get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(Nuclet.class, id, attributes);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("xojr3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("xojr4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("xojr1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("xojr2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getDescription() {
		return getField("xojrb", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.Entity> getEntity(Flag... flags) {
		return getDependents(_Entity, flags); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("xojr");
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.Import> getImport(Flag... flags) {
		return getDependents(_Import, flags); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.Jobcontroller> getJobcontroller(Flag... flags) {
		return getDependents(_Jobcontroller, flags); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_jobQueue
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.JobQueue> getJobQueue(Flag... flags) {
		return getDependents(_JobQueue, flags); 
}


/**
 * Getter-Method for attribute: localidentifier
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRLOCALIDENTIFIER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getLocalidentifier() {
		return getField("xojrf", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: masterNuclet
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNMASTERNUCLET
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getMasterNuclet() {
		return getField("xojrj", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: NAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getName() {
		return getField("xojra", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletExtension> getNucletExtension(Flag... flags) {
		return getDependents(_NucletExtension, flags); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletImport> getNucletImport(Flag... flags) {
		return getDependents(_NucletImport, flags); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletIntegrationPoint> getNucletIntegrationPoint1(Flag... flags) {
		return getDependents(_NucletIntegrationPoint1, flags); 
}


/**
 * Getter-Method for attribute: targetNuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletIntegrationPoint> getNucletIntegrationPoint2(Flag... flags) {
		return getDependents(_NucletIntegrationPoint2, flags); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletSqlConfig> getNucletSqlConfig(Flag... flags) {
		return getDependents(_NucletSqlConfig, flags); 
}


/**
 * Getter-Method for attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: INTNUCLETVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.Integer getNucletVersion() {
		return getField("xojre", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: nucletVersionString
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRNUCLETVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getNucletVersionString() {
		return getField("xojri", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclon
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNNUCLON
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getNuclon() {
		return getField("xojrg", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: package
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRPACKAGE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getPackage() {
		return getField("xojrd", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_restapis
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.Restapis> getRestapis(Flag... flags) {
		return getDependents(_Restapis, flags); 
}


/**
 * Getter-Method for attribute: source
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNSOURCE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getSource() {
		return getField("xojrh", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.WebAddon> getWebAddon(Flag... flags) {
		return getDependents(_WebAddon, flags); 
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertEntity(org.nuclos.businessentity.Entity pEntity) {
		insertDependent(_Entity, pEntity);
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_import
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertImport(org.nuclos.businessentity.Import pImport) {
		insertDependent(_Import, pImport);
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_jobcontroller
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertJobcontroller(org.nuclos.businessentity.Jobcontroller pJobcontroller) {
		insertDependent(_Jobcontroller, pJobcontroller);
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_jobQueue
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertJobQueue(org.nuclos.businessentity.JobQueue pJobQueue) {
		insertDependent(_JobQueue, pJobQueue);
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletExtension(org.nuclos.businessentity.NucletExtension pNucletExtension) {
		insertDependent(_NucletExtension, pNucletExtension);
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletImport(org.nuclos.businessentity.NucletImport pNucletImport) {
		insertDependent(_NucletImport, pNucletImport);
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletIntegrationPoint1(org.nuclos.businessentity.NucletIntegrationPoint pNucletIntegrationPoint) {
		insertDependent(_NucletIntegrationPoint1, pNucletIntegrationPoint);
}


/**
 * Insert-Method for attribute: targetNuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletIntegrationPoint2(org.nuclos.businessentity.NucletIntegrationPoint pNucletIntegrationPoint) {
		insertDependent(_NucletIntegrationPoint2, pNucletIntegrationPoint);
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletSqlConfig(org.nuclos.businessentity.NucletSqlConfig pNucletSqlConfig) {
		insertDependent(_NucletSqlConfig, pNucletSqlConfig);
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_restapis
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertRestapis(org.nuclos.businessentity.Restapis pRestapis) {
		insertDependent(_Restapis, pRestapis);
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertWebAddon(org.nuclos.businessentity.WebAddon pWebAddon) {
		insertDependent(_WebAddon, pWebAddon);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("xojr4", pChangedBy); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("xojr2", pCreatedBy); 
}


/**
 * Setter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setDescription(java.lang.String pDescription) {
		setField("xojrb", pDescription); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: localidentifier
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRLOCALIDENTIFIER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4
 *<br>Precision: null
**/
public void setLocalidentifier(java.lang.String pLocalidentifier) {
		setField("xojrf", pLocalidentifier); 
}


/**
 * Setter-Method for attribute: masterNuclet
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNMASTERNUCLET
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setMasterNuclet(java.lang.Boolean pMasterNuclet) {
		setField("xojrj", pMasterNuclet); 
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: NAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("xojra", pName); 
}


/**
 * Setter-Method for attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: INTNUCLETVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setNucletVersion(java.lang.Integer pNucletVersion) {
		setField("xojre", pNucletVersion); 
}


/**
 * Setter-Method for attribute: nucletVersionString
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRNUCLETVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setNucletVersionString(java.lang.String pNucletVersionString) {
		setField("xojri", pNucletVersionString); 
}


/**
 * Setter-Method for attribute: nuclon
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNNUCLON
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNuclon(java.lang.Boolean pNuclon) {
		setField("xojrg", pNuclon); 
}


/**
 * Setter-Method for attribute: package
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRPACKAGE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setPackage(java.lang.String pPackage) {
		setField("xojrd", pPackage); 
}


/**
 * Setter-Method for attribute: source
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNSOURCE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setSource(java.lang.Boolean pSource) {
		setField("xojrh", pSource); 
}
 }
