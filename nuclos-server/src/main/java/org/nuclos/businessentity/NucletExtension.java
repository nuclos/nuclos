//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_nucletExtension
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_NUCLETEXTENSION
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class NucletExtension extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "hajl", "hajl3", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "hajl", "hajl4", java.lang.String.class);


/**
 * Attribute: clientExtension
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNCLIENTEXTENSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> ClientExtension = 
	new Attribute<>("ClientExtension", "org.nuclos.businessentity", "hajl", "hajlc", java.lang.Boolean.class);


/**
 * Attribute: content
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLBCONTENT
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<byte[]> Content = 
	new Attribute<>("Content", "org.nuclos.businessentity", "hajl", "hajlf", byte[].class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "hajl", "hajl1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "hajl", "hajl2", java.lang.String.class);


/**
 * Attribute: dependencyAddAsProjectToPom
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNDEPENDENCYADDASPROJECTTOPOM
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> DependencyAddAsProjectToPom = 
	new Attribute<>("DependencyAddAsProjectToPom", "org.nuclos.businessentity", "hajl", "hajlj", java.lang.Boolean.class);


/**
 * Attribute: dependencyArtifactId
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRDEPENDENCYARTIFACTID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> DependencyArtifactId = new StringAttribute<>("DependencyArtifactId", "org.nuclos.businessentity", "hajl", "hajlh", java.lang.String.class);


/**
 * Attribute: dependencyGroupId
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRDEPENDENCYGROUPID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> DependencyGroupId = new StringAttribute<>("DependencyGroupId", "org.nuclos.businessentity", "hajl", "hajlg", java.lang.String.class);


/**
 * Attribute: dependencyVersion
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRDEPENDENCYVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> DependencyVersion = new StringAttribute<>("DependencyVersion", "org.nuclos.businessentity", "hajl", "hajli", java.lang.String.class);


/**
 * Attribute: file
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_FILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> FileId = 
	new ForeignKeyAttribute<>("FileId", "org.nuclos.businessentity", "hajl", "hajlb", org.nuclos.common.UID.class);


/**
 * Attribute: hash
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRHASH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Hash = new StringAttribute<>("Hash", "org.nuclos.businessentity", "hajl", "hajle", java.lang.String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "hajl", "hajl0", org.nuclos.common.UID.class);


/**
 * Attribute: jasperExtension
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNJASPEREXTENSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> JasperExtension = 
	new Attribute<>("JasperExtension", "org.nuclos.businessentity", "hajl", "hajlk", java.lang.Boolean.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NucletId = 
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.businessentity", "hajl", "hajla", org.nuclos.common.UID.class);


/**
 * Attribute: serverExtension
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNSERVEREXTENSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> ServerExtension = 
	new Attribute<>("ServerExtension", "org.nuclos.businessentity", "hajl", "hajld", java.lang.Boolean.class);


public NucletExtension() {
		super("hajl");
		setClientExtension(java.lang.Boolean.FALSE);
		setDependencyAddAsProjectToPom(java.lang.Boolean.FALSE);
		setJasperExtension(java.lang.Boolean.FALSE);
		setServerExtension(java.lang.Boolean.FALSE);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(NucletExtension boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public NucletExtension copy() {
		return super.copy(NucletExtension.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("hajl"), id);
}
/**
* Static Get by Id
*/
public static NucletExtension get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(NucletExtension.class, id, attributes);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("hajl3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("hajl4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: clientExtension
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNCLIENTEXTENSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getClientExtension() {
		return getField("hajlc", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLBCONTENT
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public byte[] getContent() {
		return getField("hajlf", byte[].class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("hajl1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("hajl2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: dependencyAddAsProjectToPom
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNDEPENDENCYADDASPROJECTTOPOM
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getDependencyAddAsProjectToPom() {
		return getField("hajlj", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: dependencyArtifactId
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRDEPENDENCYARTIFACTID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getDependencyArtifactId() {
		return getField("hajlh", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: dependencyGroupId
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRDEPENDENCYGROUPID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getDependencyGroupId() {
		return getField("hajlg", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: dependencyVersion
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRDEPENDENCYVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getDependencyVersion() {
		return getField("hajli", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("hajl");
}


/**
 * Getter-Method for attribute: file
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_FILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.api.common.NuclosFile getFile() {
		return getNuclosFile("hajlb");
}


/**
 * Getter-Method for attribute: file
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_FILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> T getFile(Class<T> pFile) {
		return super.getNuclosFile(pFile,"hajlb");
}


/**
 * Getter-Method for attribute: hash
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRHASH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getHash() {
		return getField("hajle", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: jasperExtension
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNJASPEREXTENSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getJasperExtension() {
		return getField("hajlk", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.businessentity.Nuclet getNucletBO() {
		return getReferencedBO(org.nuclos.businessentity.Nuclet.class, getFieldUid("hajla"), "hajla", "xojr");
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("hajla");
}


/**
 * Getter-Method for attribute: serverExtension
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNSERVEREXTENSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getServerExtension() {
		return getField("hajld", java.lang.Boolean.class); 
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("hajl4", pChangedBy); 
}


/**
 * Setter-Method for attribute: clientExtension
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNCLIENTEXTENSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setClientExtension(java.lang.Boolean pClientExtension) {
		setField("hajlc", pClientExtension); 
}


/**
 * Setter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLBCONTENT
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setContent(byte[] pContent) {
		setField("hajlf", pContent); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("hajl2", pCreatedBy); 
}


/**
 * Setter-Method for attribute: dependencyAddAsProjectToPom
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNDEPENDENCYADDASPROJECTTOPOM
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setDependencyAddAsProjectToPom(java.lang.Boolean pDependencyAddAsProjectToPom) {
		setField("hajlj", pDependencyAddAsProjectToPom); 
}


/**
 * Setter-Method for attribute: dependencyArtifactId
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRDEPENDENCYARTIFACTID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setDependencyArtifactId(java.lang.String pDependencyArtifactId) {
		setField("hajlh", pDependencyArtifactId); 
}


/**
 * Setter-Method for attribute: dependencyGroupId
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRDEPENDENCYGROUPID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setDependencyGroupId(java.lang.String pDependencyGroupId) {
		setField("hajlg", pDependencyGroupId); 
}


/**
 * Setter-Method for attribute: dependencyVersion
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRDEPENDENCYVERSION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setDependencyVersion(java.lang.String pDependencyVersion) {
		setField("hajli", pDependencyVersion); 
}


/**
 * Setter-Method for attribute: file
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_FILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> void setFile(T pFile) {
		super.setNuclosFile(pFile,"hajlb");
}


/**
 * Setter-Method for attribute: hash
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRHASH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setHash(java.lang.String pHash) {
		setField("hajle", pHash); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: jasperExtension
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNJASPEREXTENSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setJasperExtension(java.lang.Boolean pJasperExtension) {
		setField("hajlk", pJasperExtension); 
}


/**
 * Setter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNucletId(org.nuclos.common.UID pNucletId) {
		setFieldId("hajla", pNucletId); 
}


/**
 * Setter-Method for attribute: serverExtension
 *<br>
 *<br>Entity: nuclos_nucletExtension
 *<br>DB-Name: BLNSERVEREXTENSION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setServerExtension(java.lang.Boolean pServerExtension) {
		setField("hajld", pServerExtension); 
}
 }
