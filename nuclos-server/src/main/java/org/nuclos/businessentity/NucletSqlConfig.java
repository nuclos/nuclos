//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_nucletSqlConfig
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_NUCLETSQLCONFIG
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class NucletSqlConfig extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: afterImport
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: BLNAFTERIMPORT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> AfterImport = 
	new Attribute<>("AfterImport", "org.nuclos.businessentity", "H4n3", "H4n3f", java.lang.Boolean.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "H4n3", "H4n33", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "H4n3", "H4n34", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "H4n3", "H4n31", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "H4n3", "H4n32", java.lang.String.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRDESCR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Description = new StringAttribute<>("Description", "org.nuclos.businessentity", "H4n3", "H4n3d", java.lang.String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "H4n3", "H4n30", org.nuclos.common.UID.class);


/**
 * Attribute: isBreaking
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: BLNBREAK
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> IsBreaking = 
	new Attribute<>("IsBreaking", "org.nuclos.businessentity", "H4n3", "H4n3i", java.lang.Boolean.class);


/**
 * Attribute: isLastRunError
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: BLNLASTRUNERROR
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> IsLastRunError = 
	new Attribute<>("IsLastRunError", "org.nuclos.businessentity", "H4n3", "H4n3n", java.lang.Boolean.class);


/**
 * Attribute: lastRun
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: DATLASTRUN
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> LastRun = 
	new NumericAttribute<>("LastRun", "org.nuclos.businessentity", "H4n3", "H4n3k", java.util.Date.class);


/**
 * Attribute: lastRunCkSum
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRLASTRUNCKSUM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> LastRunCkSum = new StringAttribute<>("LastRunCkSum", "org.nuclos.businessentity", "H4n3", "H4n3l", java.lang.String.class);


/**
 * Attribute: lastRunResult
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: CLBNLASTRUNRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> LastRunResult = new StringAttribute<>("LastRunResult", "org.nuclos.businessentity", "H4n3", "H4n3m", java.lang.String.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "H4n3", "H4n3c", java.lang.String.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NucletId = 
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.businessentity", "H4n3", "H4n3a", org.nuclos.common.UID.class);


/**
 * Attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: INTNUCVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
public static final NumericAttribute<java.lang.Integer> NucletVersion = 
	new NumericAttribute<>("NucletVersion", "org.nuclos.businessentity", "H4n3", "H4n3h", java.lang.Integer.class);


/**
 * Attribute: order
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
public static final NumericAttribute<java.lang.Integer> Order = 
	new NumericAttribute<>("Order", "org.nuclos.businessentity", "H4n3", "H4n3b", java.lang.Integer.class);


/**
 * Attribute: sql
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: CLBSQL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Sql = new StringAttribute<>("Sql", "org.nuclos.businessentity", "H4n3", "H4n3e", java.lang.String.class);


/**
 * Attribute: tags
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRTAGS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Tags = new StringAttribute<>("Tags", "org.nuclos.businessentity", "H4n3", "H4n3j", java.lang.String.class);


public NucletSqlConfig() {
		super("H4n3");
		setAfterImport(java.lang.Boolean.FALSE);
		setIsBreaking(java.lang.Boolean.FALSE);
		setIsLastRunError(java.lang.Boolean.FALSE);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(NucletSqlConfig boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public NucletSqlConfig copy() {
		return super.copy(NucletSqlConfig.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("H4n3"), id);
}
/**
* Static Get by Id
*/
public static NucletSqlConfig get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(NucletSqlConfig.class, id, attributes);
}


/**
 * Getter-Method for attribute: afterImport
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: BLNAFTERIMPORT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getAfterImport() {
		return getField("H4n3f", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("H4n33", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("H4n34", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("H4n31", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("H4n32", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRDESCR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getDescription() {
		return getField("H4n3d", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("H4n3");
}


/**
 * Getter-Method for attribute: isBreaking
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: BLNBREAK
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getIsBreaking() {
		return getField("H4n3i", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: isLastRunError
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: BLNLASTRUNERROR
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getIsLastRunError() {
		return getField("H4n3n", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: lastRun
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: DATLASTRUN
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getLastRun() {
		return getField("H4n3k", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: lastRunCkSum
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRLASTRUNCKSUM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getLastRunCkSum() {
		return getField("H4n3l", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: lastRunResult
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: CLBNLASTRUNRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getLastRunResult() {
		return getField("H4n3m", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getName() {
		return getField("H4n3c", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.businessentity.Nuclet getNucletBO() {
		return getReferencedBO(org.nuclos.businessentity.Nuclet.class, getFieldUid("H4n3a"), "H4n3a", "xojr");
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("H4n3a");
}


/**
 * Getter-Method for attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: INTNUCVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
@javax.validation.constraints.NotNull
public java.lang.Integer getNucletVersion() {
		return getField("H4n3h", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: order
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
@javax.annotation.Nullable
public java.lang.Integer getOrder() {
		return getField("H4n3b", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: sql
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: CLBSQL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getSql() {
		return getField("H4n3e", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: tags
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRTAGS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getTags() {
		return getField("H4n3j", java.lang.String.class); 
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: afterImport
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: BLNAFTERIMPORT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setAfterImport(java.lang.Boolean pAfterImport) {
		setField("H4n3f", pAfterImport); 
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("H4n34", pChangedBy); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("H4n32", pCreatedBy); 
}


/**
 * Setter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRDESCR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setDescription(java.lang.String pDescription) {
		setField("H4n3d", pDescription); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: isBreaking
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: BLNBREAK
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setIsBreaking(java.lang.Boolean pIsBreaking) {
		setField("H4n3i", pIsBreaking); 
}


/**
 * Setter-Method for attribute: isLastRunError
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: BLNLASTRUNERROR
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setIsLastRunError(java.lang.Boolean pIsLastRunError) {
		setField("H4n3n", pIsLastRunError); 
}


/**
 * Setter-Method for attribute: lastRun
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: DATLASTRUN
 *<br>Data type: java.util.Date
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setLastRun(java.util.Date pLastRun) {
		setField("H4n3k", pLastRun); 
}


/**
 * Setter-Method for attribute: lastRunCkSum
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRLASTRUNCKSUM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setLastRunCkSum(java.lang.String pLastRunCkSum) {
		setField("H4n3l", pLastRunCkSum); 
}


/**
 * Setter-Method for attribute: lastRunResult
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: CLBNLASTRUNRESULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setLastRunResult(java.lang.String pLastRunResult) {
		setField("H4n3m", pLastRunResult); 
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("H4n3c", pName); 
}


/**
 * Setter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNucletId(org.nuclos.common.UID pNucletId) {
		setFieldId("H4n3a", pNucletId); 
}


/**
 * Setter-Method for attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: INTNUCVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
public void setNucletVersion(java.lang.Integer pNucletVersion) {
		setField("H4n3h", pNucletVersion); 
}


/**
 * Setter-Method for attribute: order
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: 0
**/
public void setOrder(java.lang.Integer pOrder) {
		setField("H4n3b", pOrder); 
}


/**
 * Setter-Method for attribute: sql
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: CLBSQL
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setSql(java.lang.String pSql) {
		setField("H4n3e", pSql); 
}


/**
 * Setter-Method for attribute: tags
 *<br>
 *<br>Entity: nuclos_nucletSqlConfig
 *<br>DB-Name: STRTAGS
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setTags(java.lang.String pTags) {
		setField("H4n3j", pTags); 
}
 }
