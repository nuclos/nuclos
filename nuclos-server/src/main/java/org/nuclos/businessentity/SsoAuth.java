//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.ArrayList; 
import java.util.Date; 
import java.util.List; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import org.nuclos.server.nbo.AbstractBusinessObject; 

/**
 * BusinessObject: nuclos_ssoAuth
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_AD_SSOAUTH
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public class SsoAuth extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;


public static final Dependent<org.nuclos.businessentity.SsoUserIdentifier> _SsoUserIdentifier = 
	new Dependent<>("_SsoUserIdentifier", "null", "SsoUserIdentifier", "6mlG", "ssoAuth", "6mlGb", org.nuclos.businessentity.SsoUserIdentifier.class);


/**
 * Attribute: acrValues
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRACR_VALUES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> AcrValues = new StringAttribute<>("AcrValues", "org.nuclos.businessentity", "MUp9", "MUp9y", java.lang.String.class);


/**
 * Attribute: active
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Active = 
	new Attribute<>("Active", "org.nuclos.businessentity", "MUp9", "MUp9b", java.lang.Boolean.class);


/**
 * Attribute: authLevel
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: INTAUTH_LEVEL
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> AuthLevel = 
	new NumericAttribute<>("AuthLevel", "org.nuclos.businessentity", "MUp9", "MUp9z", java.lang.Integer.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "MUp9", "MUp93", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "MUp9", "MUp94", java.lang.String.class);


/**
 * Attribute: color
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Color = new StringAttribute<>("Color", "org.nuclos.businessentity", "MUp9", "MUp9d", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "MUp9", "MUp91", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "MUp9", "MUp92", java.lang.String.class);


/**
 * Attribute: debug
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: BLNDEBUG
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Debug = 
	new Attribute<>("Debug", "org.nuclos.businessentity", "MUp9", "MUp9u", java.lang.Boolean.class);


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Description = new StringAttribute<>("Description", "org.nuclos.businessentity", "MUp9", "MUp9c", java.lang.String.class);


/**
 * Attribute: fontAwesomeIcon
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRFONTAWESOMEICON
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> FontAwesomeIcon = new StringAttribute<>("FontAwesomeIcon", "org.nuclos.businessentity", "MUp9", "MUp9t", java.lang.String.class);


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "MUp9", "MUp90", org.nuclos.common.UID.class);


/**
 * Attribute: imageIcon
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: OBJIMAGEICON
 *<br>Data type: org.nuclos.common.NuclosImage
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<org.nuclos.api.NuclosImage> ImageIcon = 
	new Attribute<>("ImageIcon", "org.nuclos.businessentity", "MUp9", "MUp9e", org.nuclos.api.NuclosImage.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "MUp9", "MUp9a", java.lang.String.class);


/**
 * Attribute: oAuth2Callback
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_CALLBACK
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2Callback = new StringAttribute<>("OAuth2Callback", "org.nuclos.businessentity", "MUp9", "MUp9j", java.lang.String.class);


/**
 * Attribute: oAuth2ClientId
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_CLIENTID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2ClientId = new StringAttribute<>("OAuth2ClientId", "org.nuclos.businessentity", "MUp9", "MUp9g", java.lang.String.class);


/**
 * Attribute: oAuth2ClientSecret
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_CLIENTSECRET
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2ClientSecret = new StringAttribute<>("OAuth2ClientSecret", "org.nuclos.businessentity", "MUp9", "MUp9h", java.lang.String.class);


/**
 * Attribute: oAuth2Endpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_ENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2Endpoint = new StringAttribute<>("OAuth2Endpoint", "org.nuclos.businessentity", "MUp9", "MUp9f", java.lang.String.class);


/**
 * Attribute: oAuth2Scopes
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_SCOPES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2Scopes = new StringAttribute<>("OAuth2Scopes", "org.nuclos.businessentity", "MUp9", "MUp9i", java.lang.String.class);


/**
 * Attribute: oAuth2TokenEndpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_TOKENENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OAuth2TokenEndpoint = new StringAttribute<>("OAuth2TokenEndpoint", "org.nuclos.businessentity", "MUp9", "MUp9r", java.lang.String.class);


/**
 * Attribute: oidcCustomClaim
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_CUSTOMCLAIM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OidcCustomClaim = new StringAttribute<>("OidcCustomClaim", "org.nuclos.businessentity", "MUp9", "MUp9w", java.lang.String.class);


/**
 * Attribute: oidcEmailClaim
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_EMAILCLAIM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OidcEmailClaim = new StringAttribute<>("OidcEmailClaim", "org.nuclos.businessentity", "MUp9", "MUp9k", java.lang.String.class);


/**
 * Attribute: oidcEndSessionEndpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_ENDSESSIONENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OidcEndSessionEndpoint = new StringAttribute<>("OidcEndSessionEndpoint", "org.nuclos.businessentity", "MUp9", "MUp9x", java.lang.String.class);


/**
 * Attribute: oidcIdTokenIssuer
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_IDTOKENISSUER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OidcIdTokenIssuer = new StringAttribute<>("OidcIdTokenIssuer", "org.nuclos.businessentity", "MUp9", "MUp9o", java.lang.String.class);


/**
 * Attribute: oidcIdTokenJwkSet
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_IDTOKENJWKSET
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OidcIdTokenJwkSet = new StringAttribute<>("OidcIdTokenJwkSet", "org.nuclos.businessentity", "MUp9", "MUp9n", java.lang.String.class);


/**
 * Attribute: oidcIdTokenJwsAlgorithm
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_IDTOKENJWSALGORITHM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OidcIdTokenJwsAlgorithm = new StringAttribute<>("OidcIdTokenJwsAlgorithm", "org.nuclos.businessentity", "MUp9", "MUp9q", java.lang.String.class);


/**
 * Attribute: oidcIdTokenNonce
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: BLNOIDC_IDTOKENNONCE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> OidcIdTokenNonce = 
	new Attribute<>("OidcIdTokenNonce", "org.nuclos.businessentity", "MUp9", "MUp9v", java.lang.Boolean.class);


/**
 * Attribute: oidcUserInfoEndpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_USERINFOENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OidcUserInfoEndpoint = new StringAttribute<>("OidcUserInfoEndpoint", "org.nuclos.businessentity", "MUp9", "MUp9m", java.lang.String.class);


/**
 * Attribute: oidcUsernameClaim
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_USERNAMECLAIM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OidcUsernameClaim = new StringAttribute<>("OidcUsernameClaim", "org.nuclos.businessentity", "MUp9", "MUp9l", java.lang.String.class);


/**
 * Attribute: order
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Order = 
	new NumericAttribute<>("Order", "org.nuclos.businessentity", "MUp9", "MUp9s", java.lang.Integer.class);


public SsoAuth() {
		super("MUp9");
		setActive(java.lang.Boolean.FALSE);
		setDebug(java.lang.Boolean.FALSE);
		setOidcIdTokenNonce(java.lang.Boolean.FALSE);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(SsoAuth boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public SsoAuth copy() {
		return super.copy(SsoAuth.class);
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("MUp9"), id);
}


/**
 * Delete-Method for attribute: ssoAuth
 *<br>
 *<br>Entity: nuclos_ssoUserIdentifier
 *<br>DB-Name: STRUID_SSOAUTH
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_ssoAuth
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteSsoUserIdentifier(org.nuclos.businessentity.SsoUserIdentifier pSsoUserIdentifier) {
		deleteDependent(_SsoUserIdentifier, pSsoUserIdentifier);
}
/**
* Static Get by Id
*/
public static SsoAuth get(org.nuclos.common.UID id, org.nuclos.api.businessobject.attribute.Attribute<?> ... attributes) {
		return get(SsoAuth.class, id, attributes);
}


/**
 * Getter-Method for attribute: acrValues
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRACR_VALUES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getAcrValues() {
		return getField("MUp9y", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: active
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getActive() {
		return getField("MUp9b", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: authLevel
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: INTAUTH_LEVEL
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.Integer getAuthLevel() {
		return getField("MUp9z", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getChangedAt() {
		return getField("MUp93", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getChangedBy() {
		return getField("MUp94", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: color
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getColor() {
		return getField("MUp9d", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.util.Date getCreatedAt() {
		return getField("MUp91", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getCreatedBy() {
		return getField("MUp92", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: debug
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: BLNDEBUG
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getDebug() {
		return getField("MUp9u", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getDescription() {
		return getField("MUp9c", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("MUp9");
}


/**
 * Getter-Method for attribute: fontAwesomeIcon
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRFONTAWESOMEICON
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getFontAwesomeIcon() {
		return getField("MUp9t", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: imageIcon
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: OBJIMAGEICON
 *<br>Data type: org.nuclos.common.NuclosImage
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.annotation.Nullable
public org.nuclos.api.NuclosImage getImageIcon() {
		return getField("MUp9e", org.nuclos.api.NuclosImage.class); 
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.String getName() {
		return getField("MUp9a", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2Callback
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_CALLBACK
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2Callback() {
		return getField("MUp9j", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2ClientId
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_CLIENTID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2ClientId() {
		return getField("MUp9g", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2ClientSecret
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_CLIENTSECRET
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2ClientSecret() {
		return getField("MUp9h", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2Endpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_ENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2Endpoint() {
		return getField("MUp9f", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2Scopes
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_SCOPES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2Scopes() {
		return getField("MUp9i", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oAuth2TokenEndpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_TOKENENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOAuth2TokenEndpoint() {
		return getField("MUp9r", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oidcCustomClaim
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_CUSTOMCLAIM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOidcCustomClaim() {
		return getField("MUp9w", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oidcEmailClaim
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_EMAILCLAIM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOidcEmailClaim() {
		return getField("MUp9k", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oidcEndSessionEndpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_ENDSESSIONENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOidcEndSessionEndpoint() {
		return getField("MUp9x", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oidcIdTokenIssuer
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_IDTOKENISSUER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOidcIdTokenIssuer() {
		return getField("MUp9o", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oidcIdTokenJwkSet
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_IDTOKENJWKSET
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOidcIdTokenJwkSet() {
		return getField("MUp9n", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oidcIdTokenJwsAlgorithm
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_IDTOKENJWSALGORITHM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOidcIdTokenJwsAlgorithm() {
		return getField("MUp9q", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oidcIdTokenNonce
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: BLNOIDC_IDTOKENNONCE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Boolean getOidcIdTokenNonce() {
		return getField("MUp9v", java.lang.Boolean.class); 
}


/**
 * Getter-Method for attribute: oidcUserInfoEndpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_USERINFOENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOidcUserInfoEndpoint() {
		return getField("MUp9m", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: oidcUsernameClaim
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_USERNAMECLAIM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
@javax.annotation.Nullable
public java.lang.String getOidcUsernameClaim() {
		return getField("MUp9l", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: order
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
@javax.validation.constraints.NotNull
public java.lang.Integer getOrder() {
		return getField("MUp9s", java.lang.Integer.class); 
}


/**
 * Getter-Method for attribute: ssoAuth
 *<br>
 *<br>Entity: nuclos_ssoUserIdentifier
 *<br>DB-Name: STRUID_SSOAUTH
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_ssoAuth
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.SsoUserIdentifier> getSsoUserIdentifier(Flag... flags) {
		return getDependents(_SsoUserIdentifier, flags); 
}


/**
 * Insert-Method for attribute: ssoAuth
 *<br>
 *<br>Entity: nuclos_ssoUserIdentifier
 *<br>DB-Name: STRUID_SSOAUTH
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_ssoAuth
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertSsoUserIdentifier(org.nuclos.businessentity.SsoUserIdentifier pSsoUserIdentifier) {
		insertDependent(_SsoUserIdentifier, pSsoUserIdentifier);
}
/**
* Refresh this BO with data from the db layer, interface or similar
*/
public void refresh() throws org.nuclos.api.exception.BusinessException {
	if (this.getId() == null) {
		throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
	}
	super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
}


/**
 * Setter-Method for attribute: acrValues
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRACR_VALUES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setAcrValues(java.lang.String pAcrValues) {
		setField("MUp9y", pAcrValues); 
}


/**
 * Setter-Method for attribute: active
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setActive(java.lang.Boolean pActive) {
		setField("MUp9b", pActive); 
}


/**
 * Setter-Method for attribute: authLevel
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: INTAUTH_LEVEL
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setAuthLevel(java.lang.Integer pAuthLevel) {
		setField("MUp9z", pAuthLevel); 
}


/**
 * Setter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setChangedBy(java.lang.String pChangedBy) {
		setField("MUp94", pChangedBy); 
}


/**
 * Setter-Method for attribute: color
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 10
 *<br>Precision: null
**/
public void setColor(java.lang.String pColor) {
		setField("MUp9d", pColor); 
}


/**
 * Setter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCreatedBy(java.lang.String pCreatedBy) {
		setField("MUp92", pCreatedBy); 
}


/**
 * Setter-Method for attribute: debug
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: BLNDEBUG
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setDebug(java.lang.Boolean pDebug) {
		setField("MUp9u", pDebug); 
}


/**
 * Setter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRDESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setDescription(java.lang.String pDescription) {
		setField("MUp9c", pDescription); 
}


/**
 * Setter-Method for attribute: fontAwesomeIcon
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRFONTAWESOMEICON
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setFontAwesomeIcon(java.lang.String pFontAwesomeIcon) {
		setField("MUp9t", pFontAwesomeIcon); 
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Setter-Method for attribute: imageIcon
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: OBJIMAGEICON
 *<br>Data type: org.nuclos.common.NuclosImage
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setImageIcon(org.nuclos.api.NuclosImage pImageIcon) {
		setField("MUp9e", pImageIcon); 
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("MUp9a", pName); 
}


/**
 * Setter-Method for attribute: oAuth2Callback
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_CALLBACK
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2Callback(java.lang.String pOAuth2Callback) {
		setField("MUp9j", pOAuth2Callback); 
}


/**
 * Setter-Method for attribute: oAuth2ClientId
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_CLIENTID
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2ClientId(java.lang.String pOAuth2ClientId) {
		setField("MUp9g", pOAuth2ClientId); 
}


/**
 * Setter-Method for attribute: oAuth2ClientSecret
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_CLIENTSECRET
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2ClientSecret(java.lang.String pOAuth2ClientSecret) {
		setField("MUp9h", pOAuth2ClientSecret); 
}


/**
 * Setter-Method for attribute: oAuth2Endpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_ENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2Endpoint(java.lang.String pOAuth2Endpoint) {
		setField("MUp9f", pOAuth2Endpoint); 
}


/**
 * Setter-Method for attribute: oAuth2Scopes
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_SCOPES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2Scopes(java.lang.String pOAuth2Scopes) {
		setField("MUp9i", pOAuth2Scopes); 
}


/**
 * Setter-Method for attribute: oAuth2TokenEndpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROAUTH2_TOKENENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOAuth2TokenEndpoint(java.lang.String pOAuth2TokenEndpoint) {
		setField("MUp9r", pOAuth2TokenEndpoint); 
}


/**
 * Setter-Method for attribute: oidcCustomClaim
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_CUSTOMCLAIM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public void setOidcCustomClaim(java.lang.String pOidcCustomClaim) {
		setField("MUp9w", pOidcCustomClaim); 
}


/**
 * Setter-Method for attribute: oidcEmailClaim
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_EMAILCLAIM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public void setOidcEmailClaim(java.lang.String pOidcEmailClaim) {
		setField("MUp9k", pOidcEmailClaim); 
}


/**
 * Setter-Method for attribute: oidcEndSessionEndpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_ENDSESSIONENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOidcEndSessionEndpoint(java.lang.String pOidcEndSessionEndpoint) {
		setField("MUp9x", pOidcEndSessionEndpoint); 
}


/**
 * Setter-Method for attribute: oidcIdTokenIssuer
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_IDTOKENISSUER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOidcIdTokenIssuer(java.lang.String pOidcIdTokenIssuer) {
		setField("MUp9o", pOidcIdTokenIssuer); 
}


/**
 * Setter-Method for attribute: oidcIdTokenJwkSet
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_IDTOKENJWKSET
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOidcIdTokenJwkSet(java.lang.String pOidcIdTokenJwkSet) {
		setField("MUp9n", pOidcIdTokenJwkSet); 
}


/**
 * Setter-Method for attribute: oidcIdTokenJwsAlgorithm
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_IDTOKENJWSALGORITHM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 16
 *<br>Precision: null
**/
public void setOidcIdTokenJwsAlgorithm(java.lang.String pOidcIdTokenJwsAlgorithm) {
		setField("MUp9q", pOidcIdTokenJwsAlgorithm); 
}


/**
 * Setter-Method for attribute: oidcIdTokenNonce
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: BLNOIDC_IDTOKENNONCE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setOidcIdTokenNonce(java.lang.Boolean pOidcIdTokenNonce) {
		setField("MUp9v", pOidcIdTokenNonce); 
}


/**
 * Setter-Method for attribute: oidcUserInfoEndpoint
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_USERINFOENDPOINT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOidcUserInfoEndpoint(java.lang.String pOidcUserInfoEndpoint) {
		setField("MUp9m", pOidcUserInfoEndpoint); 
}


/**
 * Setter-Method for attribute: oidcUsernameClaim
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: STROIDC_USERNAMECLAIM
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 64
 *<br>Precision: null
**/
public void setOidcUsernameClaim(java.lang.String pOidcUsernameClaim) {
		setField("MUp9l", pOidcUsernameClaim); 
}


/**
 * Setter-Method for attribute: order
 *<br>
 *<br>Entity: nuclos_ssoAuth
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setOrder(java.lang.Integer pOrder) {
		setField("MUp9s", pOrder); 
}
 }
