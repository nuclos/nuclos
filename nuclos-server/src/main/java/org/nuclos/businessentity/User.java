//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.api.businessobject.Dependent;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.UID;
import org.nuclos.common.E;
import org.nuclos.server.nbo.AbstractBusinessObject;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.api.businessobject.facade.Modifiable;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

/**
 * BusinessObject: nuclos_user
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_USER
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
 **/
public class User extends AbstractBusinessObject<org.nuclos.common.UID> implements NuclosUser, Modifiable<org.nuclos.common.UID> {
	private static final long serialVersionUID = 1L;



	/**
	 * Attribute: primaryKey
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRUID
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 128
	 *<br>Precision: null
	 **/
	public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id =
			new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "dRxj", "dRxj0", org.nuclos.common.UID.class);


	/**
	 * Attribute: superuser
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNSUPERUSER
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final Attribute<java.lang.Boolean> Superuser =
			new Attribute<>("Superuser", "org.nuclos.businessentity", "dRxj", "dRxjh", java.lang.Boolean.class);


	/**
	 * Attribute: locked
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNLOCKED
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final Attribute<java.lang.Boolean> Locked =
			new Attribute<>("Locked", "org.nuclos.businessentity", "dRxj", "dRxji", java.lang.Boolean.class);


	/**
	 * Attribute: lastPasswordChange
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATPASSWORDCHANGED
	 *<br>Data type: java.util.Date
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final NumericAttribute<java.util.Date> LastPasswordChange =
			new NumericAttribute<>("LastPasswordChange", "org.nuclos.businessentity", "dRxj", "dRxjj", java.util.Date.class);


	/**
	 * Attribute: expirationDate
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATEXPIRATIONDATE
	 *<br>Data type: java.util.Date
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final NumericAttribute<java.util.Date> ExpirationDate =
			new NumericAttribute<>("ExpirationDate", "org.nuclos.businessentity", "dRxj", "dRxjk", java.util.Date.class);


	/**
	 * Attribute: passwordChangeRequired
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNREQUIREPASSWORDCHANGE
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final Attribute<java.lang.Boolean> PasswordChangeRequired =
			new Attribute<>("PasswordChangeRequired", "org.nuclos.businessentity", "dRxj", "dRxjl", java.lang.Boolean.class);


	/**
	 * Attribute: lastLogin
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATLASTLOGIN
	 *<br>Data type: java.util.Date
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final NumericAttribute<java.util.Date> LastLogin =
			new NumericAttribute<>("LastLogin", "org.nuclos.businessentity", "dRxj", "dRxjm", java.util.Date.class);


	/**
	 * Attribute: loginAttempts
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: INTLOGINATTEMPTS
	 *<br>Data type: java.lang.Integer
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 9
	 *<br>Precision: null
	 **/
	public static final NumericAttribute<java.lang.Integer> LoginAttempts =
			new NumericAttribute<>("LoginAttempts", "org.nuclos.businessentity", "dRxj", "dRxjn", java.lang.Integer.class);


	/**
	 * Attribute: communicationAccountPhone
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRUID_COMPORT_PHONE
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final ForeignKeyAttribute<org.nuclos.common.UID> CommunicationAccountPhoneId =
			new ForeignKeyAttribute<>("CommunicationAccountPhoneId", "org.nuclos.businessentity", "dRxj", "dRxjo", org.nuclos.common.UID.class);


	/**
	 * Attribute: username
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRUSER
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 30
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Username = new StringAttribute<>("Username", "org.nuclos.businessentity", "dRxj", "dRxja", java.lang.String.class);


	/**
	 * Attribute: email
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STREMAIL
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Email = new StringAttribute<>("Email", "org.nuclos.businessentity", "dRxj", "dRxjb", java.lang.String.class);


	/**
	 * Attribute: lastname
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRLASTNAME
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Lastname = new StringAttribute<>("Lastname", "org.nuclos.businessentity", "dRxj", "dRxjc", java.lang.String.class);


	/**
	 * Attribute: firstname
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRFIRSTNAME
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Firstname = new StringAttribute<>("Firstname", "org.nuclos.businessentity", "dRxj", "dRxjd", java.lang.String.class);


	/**
	 * Attribute: password
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRPASSWORD
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> Password = new StringAttribute<>("Password", "org.nuclos.businessentity", "dRxj", "dRxjg", java.lang.String.class);


	/**
	 * Attribute: activationCode
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRACTIVATIONCODE
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> ActivationCode = new StringAttribute<>("ActivationCode", "org.nuclos.businessentity", "dRxj", "dRxjp", java.lang.String.class);


	/**
	 * Attribute: createdAt
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATCREATED
	 *<br>Data type: org.nuclos.common2.InternalTimestamp
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "dRxj", "dRxj1", java.util.Date.class);


	/**
	 * Attribute: privacyConsentAccepted
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNPRIVACYCONSENT
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final Attribute<java.lang.Boolean> PrivacyConsentAccepted =
			new Attribute<>("PrivacyConsentAccepted", "org.nuclos.businessentity", "dRxj", "dRxjq", java.lang.Boolean.class);


	/**
	 * Attribute: createdBy
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRCREATED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "dRxj", "dRxj2", java.lang.String.class);


	/**
	 * Attribute: passwordResetCode
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRPASSWORDRESETCODE
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> PasswordResetCode = new StringAttribute<>("PasswordResetCode", "org.nuclos.businessentity", "dRxj", "dRxjr", java.lang.String.class);


	/**
	 * Attribute: changedAt
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATCHANGED
	 *<br>Data type: org.nuclos.common2.InternalTimestamp
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "dRxj", "dRxj3", java.util.Date.class);


	/**
	 * Attribute: loginWithEmailAllowed
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNEMAILLOGIN
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public static final Attribute<java.lang.Boolean> LoginWithEmailAllowed =
			new Attribute<>("LoginWithEmailAllowed", "org.nuclos.businessentity", "dRxj", "dRxjs", java.lang.Boolean.class);


	/**
	 * Attribute: changedBy
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRCHANGED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "dRxj", "dRxj4", java.lang.String.class);

	public static final Dependent<org.nuclos.businessentity.NewsViewed> _NewsViewed =
			new Dependent<>("_NewsViewed", "null", "NewsViewed", "qIx7", "user", "qIx7b", org.nuclos.businessentity.NewsViewed.class);

	public static final Dependent<org.nuclos.businessentity.NewsConfirmed> _NewsConfirmed =
			new Dependent<>("_NewsConfirmed", "null", "NewsConfirmed", "6ABA", "user", "6ABAb", org.nuclos.businessentity.NewsConfirmed.class);


	public User() {
		super("dRxj");
		setSuperuser(java.lang.Boolean.FALSE);
		setPrivacyConsentAccepted(java.lang.Boolean.FALSE);
		setLoginWithEmailAllowed(java.lang.Boolean.FALSE);
	}


	/**
	 * Getter-Method for attribute: entity
	 *<br>
	 *<br>Entity: nuclos_entity
	 *<br>DB-Name: STRENTITY
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("dRxj");
	}


	/**
	 * Getter-Method for attribute: primaryKey
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRUID
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 128
	 *<br>Precision: null
	 **/
	@java.lang.Override
	public void setId(org.nuclos.common.UID id) {
		super.setId(id);
	}


	/**
	 * Getter-Method for attribute: superuser
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNSUPERUSER
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.lang.Boolean getSuperuser() {
		return getField("dRxjh", java.lang.Boolean.class);
	}


	/**
	 * Setter-Method for attribute: superuser
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNSUPERUSER
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void setSuperuser(java.lang.Boolean pSuperuser) {
		setField("dRxjh", pSuperuser);
	}


	/**
	 * Getter-Method for attribute: locked
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNLOCKED
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.lang.Boolean getLocked() {
		return getField("dRxji", java.lang.Boolean.class);
	}


	/**
	 * Setter-Method for attribute: locked
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNLOCKED
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void setLocked(java.lang.Boolean pLocked) {
		setField("dRxji", pLocked);
	}


	/**
	 * Getter-Method for attribute: lastPasswordChange
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATPASSWORDCHANGED
	 *<br>Data type: java.util.Date
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.util.Date getLastPasswordChange() {
		return getField("dRxjj", java.util.Date.class);
	}


	/**
	 * Getter-Method for attribute: expirationDate
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATEXPIRATIONDATE
	 *<br>Data type: java.util.Date
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.util.Date getExpirationDate() {
		return getField("dRxjk", java.util.Date.class);
	}


	/**
	 * Setter-Method for attribute: expirationDate
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATEXPIRATIONDATE
	 *<br>Data type: java.util.Date
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void setExpirationDate(java.util.Date pExpirationDate) {
		setField("dRxjk", pExpirationDate);
	}


	/**
	 * Getter-Method for attribute: passwordChangeRequired
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNREQUIREPASSWORDCHANGE
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.lang.Boolean getPasswordChangeRequired() {
		return getField("dRxjl", java.lang.Boolean.class);
	}


	/**
	 * Setter-Method for attribute: passwordChangeRequired
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNREQUIREPASSWORDCHANGE
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void setPasswordChangeRequired(java.lang.Boolean pPasswordChangeRequired) {
		setField("dRxjl", pPasswordChangeRequired);
	}


	/**
	 * Getter-Method for attribute: lastLogin
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATLASTLOGIN
	 *<br>Data type: java.util.Date
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.util.Date getLastLogin() {
		return getField("dRxjm", java.util.Date.class);
	}


	/**
	 * Getter-Method for attribute: loginAttempts
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: INTLOGINATTEMPTS
	 *<br>Data type: java.lang.Integer
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 9
	 *<br>Precision: null
	 **/
	public java.lang.Integer getLoginAttempts() {
		return getField("dRxjn", java.lang.Integer.class);
	}


	/**
	 * Getter-Method for attribute: communicationAccountPhone
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRUID_COMPORT_PHONE
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Reference entity: nuclos_userCommunicationAccount
	 *<br>Reference field: communicationPort (account)
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public org.nuclos.api.UID getCommunicationAccountPhoneId() {
		return getFieldUid("dRxjo");
	}


	/**
	 * Getter-Method for attribute: username
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRUSER
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 30
	 *<br>Precision: null
	 **/
	public java.lang.String getUsername() {
		return getField("dRxja", java.lang.String.class);
	}


	/**
	 * Setter-Method for attribute: username
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRUSER
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 30
	 *<br>Precision: null
	 **/
	public void setUsername(java.lang.String pUsername) {
		setField("dRxja", pUsername);
	}


	/**
	 * Getter-Method for attribute: email
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STREMAIL
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getEmail() {
		return getField("dRxjb", java.lang.String.class);
	}


	/**
	 * Setter-Method for attribute: email
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STREMAIL
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setEmail(java.lang.String pEmail) {
		setField("dRxjb", pEmail);
	}


	/**
	 * Getter-Method for attribute: lastname
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRLASTNAME
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getLastname() {
		return getField("dRxjc", java.lang.String.class);
	}


	/**
	 * Setter-Method for attribute: lastname
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRLASTNAME
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setLastname(java.lang.String pLastname) {
		setField("dRxjc", pLastname);
	}


	/**
	 * Getter-Method for attribute: firstname
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRFIRSTNAME
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getFirstname() {
		return getField("dRxjd", java.lang.String.class);
	}


	/**
	 * Setter-Method for attribute: firstname
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRFIRSTNAME
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setFirstname(java.lang.String pFirstname) {
		setField("dRxjd", pFirstname);
	}


	/**
	 * Getter-Method for attribute: activationCode
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRACTIVATIONCODE
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getActivationCode() {
		return getField("dRxjp", java.lang.String.class);
	}


	/**
	 * Setter-Method for attribute: activationCode
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRACTIVATIONCODE
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setActivationCode(java.lang.String pActivationCode) {
		setField("dRxjp", pActivationCode);
	}


	/**
	 * Getter-Method for attribute: createdAt
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATCREATED
	 *<br>Data type: org.nuclos.common2.InternalTimestamp
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.util.Date getCreatedAt() {
		return getField("dRxj1", java.util.Date.class);
	}


	/**
	 * Getter-Method for attribute: privacyConsentAccepted
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNPRIVACYCONSENT
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.lang.Boolean getPrivacyConsentAccepted() {
		return getField("dRxjq", java.lang.Boolean.class);
	}


	/**
	 * Setter-Method for attribute: privacyConsentAccepted
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNPRIVACYCONSENT
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void setPrivacyConsentAccepted(java.lang.Boolean pPrivacyConsentAccepted) {
		setField("dRxjq", pPrivacyConsentAccepted);
	}


	/**
	 * Getter-Method for attribute: createdBy
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRCREATED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getCreatedBy() {
		return getField("dRxj2", java.lang.String.class);
	}

	/**
	 * Setter-Method for attribute: createdBy
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRCREATED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setCreatedBy(String username) {
		setField("dRxj2", username);
	}

	/**
	 * Getter-Method for attribute: passwordResetCode
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRPASSWORDRESETCODE
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getPasswordResetCode() {
		return getField("dRxjr", java.lang.String.class);
	}


	/**
	 * Setter-Method for attribute: passwordResetCode
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRPASSWORDRESETCODE
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setPasswordResetCode(java.lang.String pPasswordResetCode) {
		setField("dRxjr", pPasswordResetCode);
	}


	/**
	 * Getter-Method for attribute: changedAt
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: DATCHANGED
	 *<br>Data type: org.nuclos.common2.InternalTimestamp
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.util.Date getChangedAt() {
		return getField("dRxj3", java.util.Date.class);
	}


	/**
	 * Getter-Method for attribute: loginWithEmailAllowed
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNEMAILLOGIN
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public java.lang.Boolean getLoginWithEmailAllowed() {
		return getField("dRxjs", java.lang.Boolean.class);
	}


	/**
	 * Setter-Method for attribute: loginWithEmailAllowed
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: BLNEMAILLOGIN
	 *<br>Data type: java.lang.Boolean
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void setLoginWithEmailAllowed(java.lang.Boolean pLoginWithEmailAllowed) {
		setField("dRxjs", pLoginWithEmailAllowed);
	}


	/**
	 * Getter-Method for attribute: changedBy
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRCHANGED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public java.lang.String getChangedBy() {
		return getField("dRxj4", java.lang.String.class);
	}

	/**
	 * Setter-Method for attribute: changedBy
	 *<br>
	 *<br>Entity: nuclos_user
	 *<br>DB-Name: STRCHANGED
	 *<br>Data type: java.lang.String
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: 255
	 *<br>Precision: null
	 **/
	public void setChangedBy(String username) {
		setField("dRxj4", username);
	}

	/**
	 * Getter-Method for attribute: user
	 *<br>
	 *<br>Entity: nuclos_newsViewed
	 *<br>DB-Name: STRUID_T_MD_USER
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Reference entity: nuclos_user
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public List<org.nuclos.businessentity.NewsViewed> getNewsViewed(Flag... flags) {
		return getDependents(_NewsViewed, flags);
	}


	/**
	 * Insert-Method for attribute: user
	 *<br>
	 *<br>Entity: nuclos_newsViewed
	 *<br>DB-Name: STRUID_T_MD_USER
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Reference entity: nuclos_user
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void insertNewsViewed(org.nuclos.businessentity.NewsViewed pNewsViewed) {
		insertDependent(_NewsViewed, pNewsViewed);
	}


	/**
	 * Delete-Method for attribute: user
	 *<br>
	 *<br>Entity: nuclos_newsViewed
	 *<br>DB-Name: STRUID_T_MD_USER
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Reference entity: nuclos_user
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void deleteNewsViewed(org.nuclos.businessentity.NewsViewed pNewsViewed) {
		deleteDependent(_NewsViewed, pNewsViewed);
	}


	/**
	 * Getter-Method for attribute: user
	 *<br>
	 *<br>Entity: nuclos_newsConfirmed
	 *<br>DB-Name: STRUID_T_MD_USER
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Reference entity: nuclos_user
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public List<org.nuclos.businessentity.NewsConfirmed> getNewsConfirmed(Flag... flags) {
		return getDependents(_NewsConfirmed, flags);
	}


	/**
	 * Insert-Method for attribute: user
	 *<br>
	 *<br>Entity: nuclos_newsConfirmed
	 *<br>DB-Name: STRUID_T_MD_USER
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Reference entity: nuclos_user
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void insertNewsConfirmed(org.nuclos.businessentity.NewsConfirmed pNewsConfirmed) {
		insertDependent(_NewsConfirmed, pNewsConfirmed);
	}


	/**
	 * Delete-Method for attribute: user
	 *<br>
	 *<br>Entity: nuclos_newsConfirmed
	 *<br>DB-Name: STRUID_T_MD_USER
	 *<br>Data type: org.nuclos.common.UID
	 *<br>Reference entity: nuclos_user
	 *<br>Localized: false
	 *<br>Output format: null
	 *<br>Scale: null
	 *<br>Precision: null
	 **/
	public void deleteNewsConfirmed(org.nuclos.businessentity.NewsConfirmed pNewsConfirmed) {
		deleteDependent(_NewsConfirmed, pNewsConfirmed);
	}
	/**
	 * This method compares the current BusinessObject with an other BusinessObject.
	 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
	 **/
	public boolean compare(User boToCompareWith) {
		return super.compare(boToCompareWith);
	}
	/**
	 * This method creates a copy of the current BusinessObject
	 * and resets it by removing the primary key and setting the state flag to 'new'
	 **/
	public User copy() {
		return super.copy(User.class);
	}
	/**
	 * Save this BO. Use this instead of BusinessObjectProvider
	 */
	public void save(org.nuclos.api.context.SaveFlag ... saveFlags) throws org.nuclos.api.exception.BusinessException {
		super.save(saveFlags);
	}
	/**
	 * Delete this BO. Use this instead of BusinessObjectProvider
	 */
	public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
	}
	/**
	 * Static Delete for an Id
	 */
	public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("dRxj"), id);
	}
	/**
	 * Static Get by Id
	 */
	public static User get(org.nuclos.common.UID id) {
		return get(User.class, id);
	}
	/**
	 * Refresh this BO with data from the db layer, interface or similar
	 */
	public void refresh() throws org.nuclos.api.exception.BusinessException {
		if (this.getId() == null) {
			throw new org.nuclos.api.exception.BusinessException("Object has not yet been saved and therefore can not be refreshed!");
		}
		super.setEntityObjectVO((get(this.getId())).getEntityObjectVO());
	}

	public String getNewPassword() {
		return getField(E.USER.newPassword.getUID().toString(), String.class);
	}

	public String getNewPasswordConfirm() {
		return getField(E.USER.newPasswordConfirm.getUID().toString(), String.class);
	}

	public Boolean getSetPassword() {
		return getField(E.USER.setPassword.getUID().toString(), Boolean.class);
	}

	public Boolean getSendPassword() {
		return getField(E.USER.sendPassword.getUID().toString(), Boolean.class);
	}
}
