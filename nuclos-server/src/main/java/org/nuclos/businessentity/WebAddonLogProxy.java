//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import java.util.List; 
import java.util.Map; 
import org.nuclos.api.UID; 
import org.nuclos.api.businessobject.SearchExpression; 
import org.nuclos.api.businessobject.attribute.Attribute; 

/**
 * BusinessObject: nuclos_webAddonLog
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: [proxy]
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
@org.nuclos.api.annotation.NuclosGenerated
@javax.annotation.Generated(value = "org.nuclos.server.nbo.NuclosBusinessObjectBuilder")
public interface WebAddonLogProxy {

public void commit();
default Long count() {
	throw new java.lang.UnsupportedOperationException("You need to implement this method in order to use the proxy businessobject aside from subforms.");
}

default <SE> Long count(SearchExpression<SE> searchExpression) {
	throw new java.lang.UnsupportedOperationException("You need to implement this method in order to use the proxy businessobject aside from subforms.");
}

public void delete(java.lang.Long id) throws org.nuclos.api.exception.BusinessException;
public List<WebAddonLog> getAll();
default <SE> List<WebAddonLog> getAll(SearchExpression<SE> searchExpression, Long limit, Long offset, Map<Attribute, Boolean> sortingOrders) {
	throw new java.lang.UnsupportedOperationException("You need to implement this method in order to use the proxy businessobject aside from subforms.");
}

default List<WebAddonLog> getAll(Long limit, Long offset, Map<Attribute, Boolean> sortingOrders) {
	throw new java.lang.UnsupportedOperationException("You need to implement this method in order to use the proxy businessobject aside from subforms.");
}

public List<java.lang.Long> getAllIds();
public WebAddonLog getById(java.lang.Long id);

/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonLog
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<WebAddonLog> getByWebAddon(org.nuclos.common.UID pNucloswebAddonId);
public void insert(WebAddonLog pWebAddonLog) throws org.nuclos.api.exception.BusinessException;
default Object insertWithId(WebAddonLog pWebAddonLog) throws org.nuclos.api.exception.BusinessException {
	insert(pWebAddonLog);
	return null;
}

public void rollback();
public void setUser(org.nuclos.api.User user);
public void update(WebAddonLog pWebAddonLog) throws org.nuclos.api.exception.BusinessException; }
