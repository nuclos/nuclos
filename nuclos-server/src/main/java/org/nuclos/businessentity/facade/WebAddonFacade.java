//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.facade;

import static org.apache.tools.ant.types.resources.MultiRootFileSet.SetType.file;
import static org.nuclos.common.ParameterProvider.PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.businessobject.QueryOperation;
import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.E;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.documentfile.DocumentFileFacadeLocal;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.nbo.EOBOBridge;
import org.nuclos.server.webclient.WebAddonUtils;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.FileProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.Nuclet;
import org.nuclos.businessentity.WebAddon;
import org.nuclos.businessentity.WebAddonFile;
import org.nuclos.common.MutableBoolean;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.cluster.ClusterMessageRebuildLevel;
import org.nuclos.server.cluster.TransactionalClusterNotification;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class WebAddonFacade {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(WebAddonFacade.class);

	public static final String ENV_PARAM_DEV_WEBCLIENT_SRCDIR = "DEV_WEBCLIENT_SRCDIR";

	private static boolean checked_ENV_PARAM_DEV_WEBCLIENT_SRCDIR = false;

	private static Path webclientSrcDirFromEnv = null;

	/**
	 * environment parameter pendant to the system parameter WEBCLIENT_SRC_DIR_FOR_DEVMODE
	 */
	public static final String ENV_PARAM_ADDON_DEVMODE_WEBCLIENT_SRCDIR = "ADDON_DEVMODE_WEBCLIENT_SRCDIR";

	private static boolean checked_ENV_PARAM_ADDON_DEVMODE_WEBCLIENT_SRCDIR = false;

	private static Path webclientSrcDirFromEnvForAddonDevMode = null;

	/**
	 * optional param for npm executable
	 * for windows: ﻿"C:\Program Files\nodejs\npm.cmd"
	 */
	private static final String PARAM_WEBCLIENT_PATH_TO_NPM = "WEBCLIENT_PATH_TO_NPM";

	/**
	 * default: $NUCLOS_HOME/data/webaddons-webclient-src/
	 */
	private static final String PARAM_WEBCLIENT_SRC_DIR = "WEBCLIENT_SRC_DIR";

	/**
	 * when running nuclos from IDE set this param to your webclient sources folder
	 * something like:  /..../nuclos/nuclos-webclient2/
	 */

	private static final String WEBCLIENT_DEPLOY_DIR = "webapps/ROOT/webclient";
	private static final String WEBCLIENT_BASE_REF = "/webclient/";

	private static String NPM_VERSION;

	@Autowired
	private ServerParameterProvider parameterProv;

	@Autowired
	private SpringLocaleDelegate localeDelegate; // indirectly used from AbstractBusinessObject

	@Autowired
	private DocumentFileFacadeLocal docFacade;

	private boolean outputAndCompileAllIsRunning = true;

	private static boolean isWindows() {
		return System.getProperty("os.name").toLowerCase().contains("win");
	}

	private String getNpm() {
		final String npmFromParameter = parameterProv.getValue(PARAM_WEBCLIENT_PATH_TO_NPM);
		if (npmFromParameter != null) {
			return npmFromParameter;
		}
		return isWindows() ? "npm.cmd" : "npm";
	}

	/**
	 * addon development mode is activated when system parameter WEBCLIENT_SRC_DIR_FOR_DEVMODE is set to a valid webclient directory
	 * @return
	 */
	public boolean isAddonDevMode() {
		if (getWebclientSrcDirFromEnvForAddonDevMode() != null) {
			return true;
		}
		final String webclientSrdDirForDevMode = parameterProv.getValue(PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE);
		if (webclientSrdDirForDevMode != null) {
			final boolean srdDirForDevModeConfigured = Files.exists(getWebclientSrcDir());
			if (srdDirForDevModeConfigured && getWebclientSrcDir().resolve("package.json") == null) {
				LOG.error("No valid " + PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE + " path defined: " + webclientSrdDirForDevMode);
				return false;
			}
			return srdDirForDevModeConfigured;
		} else {
			return false;
		}
	}

	private Path getWebclientSrcDir() {
		if (!checked_ENV_PARAM_DEV_WEBCLIENT_SRCDIR) {
			final String sWebclientSrcDirFromEnv = System.getenv().get(ENV_PARAM_DEV_WEBCLIENT_SRCDIR);
			if (sWebclientSrcDirFromEnv != null) {
				File fileResult = new File(sWebclientSrcDirFromEnv);
				if (fileResult.isDirectory() && fileResult.exists()) {
					LOG.info("Using Webclient source directory [" + webclientSrcDirFromEnv + "] (from environment paramter "+ENV_PARAM_DEV_WEBCLIENT_SRCDIR+")");
					webclientSrcDirFromEnv = fileResult.toPath();
				}
			}
			checked_ENV_PARAM_DEV_WEBCLIENT_SRCDIR = true;
		}
		if (webclientSrcDirFromEnv != null) {
			return webclientSrcDirFromEnv;
		}
		Path result = getWebclientSrcDirFromEnvForAddonDevMode();
		if (result != null) {
			return result;
		}

		final String webclientSrcDirFromParam = parameterProv.getValue(PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE);
		if (webclientSrcDirFromParam != null) {
			LOG.info("Using Webclient source directory [" + webclientSrcDirFromParam + "] (from system parameter "+PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE+")");
			return Paths.get(webclientSrcDirFromParam);
		} else {
			final Path genParent = Paths.get(NuclosSystemParameters.getString(NuclosSystemParameters.GENERATOR_OUTPUT_PATH)).getParent();
			final Path webclientSrcDirPath = genParent.resolve("webaddons-webclient-src");
			LOG.debug("Webclient source directory is not configured (Systemparameter "+PARAM_WEBCLIENT_SRC_DIR_FOR_DEVMODE+"). using default: " + webclientSrcDirPath);
			return webclientSrcDirPath;
		}
	}

	private Path getWebclientSrcDirFromEnvForAddonDevMode() {
		if (!checked_ENV_PARAM_ADDON_DEVMODE_WEBCLIENT_SRCDIR) {
			final String sWebclientSrcDirFromEnvForAddonDevMode = System.getenv().get(ENV_PARAM_ADDON_DEVMODE_WEBCLIENT_SRCDIR);
			if (sWebclientSrcDirFromEnvForAddonDevMode != null) {
				File fileResult = new File(sWebclientSrcDirFromEnvForAddonDevMode);
				if (fileResult.isDirectory() && fileResult.exists()) {
					LOG.info("Using Webclient source directory [" + webclientSrcDirFromEnvForAddonDevMode + "] " +
							"(from environment paramter "+ENV_PARAM_ADDON_DEVMODE_WEBCLIENT_SRCDIR+")");
					webclientSrcDirFromEnvForAddonDevMode = fileResult.toPath();
				}
			}
			checked_ENV_PARAM_ADDON_DEVMODE_WEBCLIENT_SRCDIR = true;
		}
		return webclientSrcDirFromEnvForAddonDevMode;
	}

	/**
	 * root path where the addon sources are stored in filesystem
	 */
	private Path getAddonOutputRootPath() {
		return getWebclientSrcDir().resolve("addons");
	}

	/**
	 * path where the webAddon sources are stored in filesystem
	 */
	private Path getWebAddonOutputPath(WebAddon webAddon) {
		if (isAddonDevMode()) {
			Path addonSrc = getWebclientSrcDir().resolve("src/addons");
			if  (addonSrc != null) {
				if (!Files.exists(addonSrc)) {
					addonSrc.toFile().mkdirs();
				}
				return addonSrc.resolve(WebAddonUtils.camelCaseToFileName(webAddon.getName()));
			}
		}

		String fileName = WebAddonUtils.camelCaseToFileName(webAddon.getName());

		Path addonSrcPath = getAddonOutputRootPath();
		if  (addonSrcPath != null) {
			return addonSrcPath.resolve(fileName);
		}

		String webAddonDir = "";
		final Nuclet nucletBO = webAddon.getNucletBO();
		if (nucletBO != null) {
			webAddonDir = nucletBO.getPackage() + ".";
		}
		webAddonDir = webAddonDir + fileName;
		return getAddonOutputRootPath().resolve(webAddonDir);
	}


	/**
	 * build webclient with addons on server start
	 */
	@PostConstruct
	final void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		if (SpringLocaleDelegate.getInstance() == null) {
			throw new NuclosFatalException("Missing SpringLocaleDelegate");
		}
		StringBuilder sNpmVersion = new StringBuilder();
		if (runCommandSilentWithOutput(sNpmVersion, getNpm(), "-version")) {
			NPM_VERSION = sNpmVersion.toString().replace("\n", "").replace("\r", "");
		}
	}

	@Transactional
	public synchronized void outputAndCompileAll() {
		this.outputAndCompileAllIsRunning = true;
		try {
			final List<WebAddon> activeWebAddons = getActiveWebAddons();
			if (activeWebAddons.isEmpty() || !isNodeJsAvailable()) {
				return;
			}
			final Path outputPath = getAddonOutputRootPath();
			try {
				Files.createDirectories(outputPath);
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, outputPath, ex);
				return;
			}

			// only respect hash file check if we are in PROD mode
			// else overwrite everytime to ensure in DEV mode we
			// are having the right addon files
			if (isOutputPathUpToDate() && !isAddonDevMode()) {
				generateWebAddonModulesRegistry();
				return;
			}

			List<Path> clearProtectedPaths = new ArrayList<>();
			for (WebAddon webAddon : activeWebAddons) {
				outputWebAddon(webAddon, false); // no hash values here, write all at once
				buildWebAddon(webAddon);

				final Path addonOutputPath = getWebAddonOutputPath(webAddon);
				final Path addonLogPath = getWebAddonLogPath(webAddon);
				clearProtectedPaths.add(addonOutputPath);
				clearProtectedPaths.add(addonLogPath);
			}

			// write hash values once
			clearProtectedPaths.add(writeFileHashValuesToOutput());


			// remove inactive addons
			try {
				clearRecursive(outputPath, clearProtectedPaths.toArray(new Path[]{}));
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, outputPath, ex);
			}

			// compile webclient
			buildWebclient(activeWebAddons);

			// build was successful, notify cluster
			TransactionalClusterNotification.notifyRebuild(ClusterMessageRebuildLevel.WEBCLIENT);
		} finally {
			this.outputAndCompileAllIsRunning = false;
		}
	}

	/**
	 * write webAddon content to files
	 * @param webAddon
	 * @return true if successful
	 */
	public boolean outputWebAddon(WebAddon webAddon) {
		return outputWebAddon(webAddon, true);
	}

	private boolean outputWebAddon(WebAddon webAddon, boolean bWriteHashValues) {
		if (!isNodeJsAvailable()) {
			LOG.error("Node.js is not available.");
			return false;
		}
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		final Path addonLogPath = getWebAddonLogPath(webAddon);
		if (webAddon.isUpdate()) {
			// name updated? delete old path
			final WebAddon webAddonDb = QueryProvider.getById(WebAddon.class, webAddon.getId());
			final Path addonOutputPathDb = getWebAddonOutputPath(webAddonDb);
			if (!addonOutputPath.equals(addonOutputPathDb)) {
				try {
					LOG.info("Deleting addons: " + addonOutputPathDb);
					FileUtils.deleteDirectory(addonOutputPathDb.toFile());
				} catch (IOException ex) {
					LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPathDb, ex);
					return false;
				}
			}
		}

		// inactive? remove the addon
		if (!Boolean.TRUE.equals(webAddon.getActive())) {
			// remove only
			LOG.info("Remove inactive addon: " + addonOutputPath.toFile());
			try {
				LOG.info("Deleting addons: " + addonOutputPath);
				FileUtils.deleteDirectory(addonOutputPath.toFile());
				Files.deleteIfExists(addonLogPath);
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPath, ex);
				return false;
			}
			return false;
		}

		// remove directory contents, do not delete node_modules
		final Path nodeModulesDir = addonOutputPath.resolve("node_modules");
		try {
			Files.deleteIfExists(addonLogPath);
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPath, ex);
			return false;
		}

		LOG.info("OUTPUT web addon \"" + addonOutputPath + "\" ...");

		try {
			Files.createDirectories(addonOutputPath);
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPath, ex);
			return false;
		}
		for (WebAddonFile webAddonFile : webAddon.getWebAddonFile()) {
			byte[] fileContent = webAddonFile.getContent();
			String filename = null;
			if (fileContent == null) {
				// Only use the default way if the backup bytes of the database are not filled.
				final NuclosFile file = webAddonFile.getFile();
				if (file == null) {
					continue;
				}
				fileContent = file.getContent();
				filename = file.getName();
			} else {
				// If the file was deleted from the documents directory (multiple test runs)
				UID documentFileUID = EOBOBridge.getEO(webAddonFile).getFieldUid(E.WEBADDON_FILE.file);
				if (documentFileUID == null) {
					continue;
				}
				filename = docFacade.getFileName(documentFileUID);
			}

			final String relativePath = webAddonFile.getPath();
			final Path fileOutputPath;
			if (relativePath != null) {
				fileOutputPath = addonOutputPath.resolve(relativePath).resolve(filename);
			} else {
				fileOutputPath = addonOutputPath.resolve(filename);
			}
			try {
				Files.createDirectories(fileOutputPath.getParent());
				Files.write(fileOutputPath, fileContent);
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, fileOutputPath, ex);
			}
		}

		if (bWriteHashValues) {
			// update hash values
			writeFileHashValuesToOutput();
		}

		return true;
	}

	public boolean buildWebAddon(final WebAddon webAddon) {
		LOG.info("Building addon '{}'.", webAddon.getName());
		if (isAddonDevMode()) {
			return true;
		}
		if (!isNodeJsAvailable()) {
			LOG.error("Node.js is not available.");
			return false;
		}
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);

		LOG.info("Compile web addon to '{}'.", addonOutputPath);

		StringBuilder log = new StringBuilder();
		try {
			runCommand(log, addonOutputPath, getNpm(), "install", "--legacy-peer-deps");
			if (runCommand(log, addonOutputPath, getNpm(), "run", "build")) {
				log.append("Addon compilation completed successfully");
			}
		} finally {
			Path addonLogPath = getWebAddonLogPath(webAddon);
			try {
				Files.write(addonLogPath, log.toString().getBytes(Charset.forName("UTF-8")));
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR while writing logfile: {} on {}", ex, addonLogPath, ex);
			}
		}

		return true;
	}

	/**
	 * installs the addon module into the webclient
	 * @param webAddon
	 * @return
	 */
	private boolean installWebAddon(final WebAddon webAddon) {
		String addonFilename = WebAddonUtils.camelCaseToFileName(webAddon.getName());
		LOG.info("Installing addon: " + addonFilename);
		try {
			String sFoundVersion = getAddonVersion(webAddon);
			LOG.info("Addon version found: " + sFoundVersion);
			return runCommand(getWebclientSrcDir(), getNpm(), "install", "--legacy-peer-deps", "addons/" + addonFilename + "/dist/" + addonFilename + "-" + sFoundVersion + ".tgz");
		} catch (IOException e) {
			LOG.warn("Addon version could not be determined and therefore cannot be installed: {}", e.getMessage());
		}
		return false;
	}

	private String getAddonVersion(final WebAddon webAddon) throws IOException {
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		byte[] jsonData = Files.readAllBytes(addonOutputPath.resolve("package.json"));
		JsonFactory factory = new JsonFactory();
		ObjectMapper objectMapper = new ObjectMapper(factory);
		JsonNode rootNode = objectMapper.readTree(jsonData);
		return rootNode.get("version").asText();
	}

	public synchronized boolean buildWebclient(List<WebAddon> webAddons) {
		LOG.info("Compile webclient.");

		if (isAddonDevMode() || !isNodeJsAvailable()) {
			generateWebAddonModulesRegistry();
			return false;
		}

		final String catalinaBase = System.getProperty("catalina.base");
		final Path webclientSrcDir = getWebclientSrcDir();
		Path webclientDeployPath;
		try {
			// TODO is there a better way to get application path or contextName
			Class<?> gfWebappClassLoaderClass = getClass().getClassLoader().loadClass("org.glassfish.web.loader.WebappClassLoader");
			Field contextName = gfWebappClassLoaderClass.getDeclaredField("contextName");
			contextName.setAccessible(true);
			String hiddenContextName = ((String)contextName.get(getClass().getClassLoader()))
					.replaceAll("/", "")
					.trim();
			webclientDeployPath = Paths.get(catalinaBase,"applications", hiddenContextName, "webclient");

			LOG.info("Found GlassFish Instance with context/application-name: " + hiddenContextName);
			// TODO need to modify base href path for GF
			try {
				File packageJsonFile = Paths.get(
						webclientSrcDir.toFile().getAbsolutePath(), "package.json"
				).toFile();
				String jsonData = FileUtils.readFileToString(packageJsonFile);
				jsonData = jsonData.replaceAll("/webclient/", String.format("/%s/webclient/", hiddenContextName));
				FileUtils.writeStringToFile(packageJsonFile, jsonData);
			} catch (IOException e) {
				LOG.warn("Could not overwrite package.json for webclient build.", e);
			}
		} catch (Throwable t) {
			LOG.warn("Could not determine GlassFish Instance, if this is one inspect further: {}", t.getMessage());
			if (catalinaBase != null) {
				webclientDeployPath = Paths.get(catalinaBase, WEBCLIENT_DEPLOY_DIR);
				if (!webclientDeployPath.toFile().exists()) {
					LOG.info("Creating webclient deploy directory '" + webclientDeployPath.toFile().getAbsolutePath() + "'.");
					webclientDeployPath.toFile().mkdirs();
				}
			} else {
				LOG.error("catalina.base not set");
				return false;
			}
		}

		LOG.info("Install webclient dependencies in: " + webclientSrcDir);
		runCommand(webclientSrcDir, getNpm(), "run", "install-dependencies");
		runCommand(webclientSrcDir, getNpm(), "run", "install-addon-deps");

		LOG.info("Installing addon modules in webclient.");
		List<WebAddon> addonsForModulesRegistry = webAddons.stream()
				.filter(this::installWebAddon).collect(Collectors.toList());
		generateWebAddonModulesRegistry(addonsForModulesRegistry);

		LOG.info("Build webclient: " + webclientSrcDir);
        String buildCMD = "build:prod";
        if (NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_INSTRUMENTAL)) {
            buildCMD = "build:cov";
        }
		if (runCommand(webclientSrcDir, getNpm(), "run", buildCMD)) {

			final Path distDir = webclientSrcDir.resolve("dist");
			LOG.info("Move webclient dist from '{}' to '{}'.", distDir, webclientDeployPath);
			try {
				FileUtils.cleanDirectory(webclientDeployPath.toFile());
				webclientDeployPath.toFile().mkdirs();
				Files.move(distDir, webclientDeployPath, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, webclientDeployPath, ex);
				return false;
			}

			LOG.info("Webclient compilation completed successfully");
			return true;
		} else {
			LOG.error("Error in webclient build.");
		}

		return false;
	}

	/**
	 * @param webAddon
	 * @return true if addon was generated from templates
	 */
	public boolean isWebAddonInitialized(final WebAddon webAddon) {
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		return Files.exists(addonOutputPath) && Files.exists(addonOutputPath.resolve("package.json"));
	}

	/**
	 * generate addon from templates
	 * @param webAddon
	 */
	public void initializeWebAddonFiles(final WebAddon webAddon) {
		Path webclientSrc = getWebclientSrcDir();
		if (webclientSrc == null) {
			LOG.error("Webclient source directory is not configured. (Systemparameter WEBCLIENT_SRC_DIR)");
			return;
		}
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);

		LOG.info("Writing addon template to: " + addonOutputPath);

		WebAddonTemplate.initializeWebAddonFiles(webAddon, webclientSrc, addonOutputPath);

		webAddon.setNote(
			"Addon files are created in: " + addonOutputPath + "\n" +
			"Webclient src: " + webclientSrc+ "\n" +
			"Webclient src copied: " + getWebclientSrcDir()+ "\n" +
			""
		);

		// save generated files to db:
		syncWebAddonOutputToObject(webAddon);
	}

	public List<WebAddon> getActiveWebAddons() {
		final Query<WebAddon> query = QueryProvider.create(WebAddon.class);
		SearchExpression<WebAddon> searchExpression = new SearchExpression(WebAddon.Active, Boolean.TRUE, QueryOperation.EQUALS);
		query.where(searchExpression);
		return QueryProvider.execute(query);
	}

	public void generateWebAddonModulesRegistry() {
		// write addon.modules.ts into webclient src in devmode otherwise to webclient output
		final Path addonModulesFile = getWebclientSrcDir().resolve("src/app/addon.modules.ts");
		LOG.info("Generate: " + addonModulesFile);
		WebAddonTemplate.generateWebAddonModulesRegistry(getActiveWebAddons(), isAddonDevMode(), addonModulesFile);
	}

	public void generateWebAddonModulesRegistry(List<WebAddon> addons) {
		// write addon.modules.ts into webclient src in devmode otherwise to webclient output
		final Path addonModulesFile = getWebclientSrcDir().resolve("src/app/addon.modules.ts");
		LOG.info("Generate: " + addonModulesFile);

		Collections.sort(addons, new Comparator<WebAddon>() {
			@Override
			public int compare(WebAddon o1, WebAddon o2) {
				return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
			}
		});

		WebAddonTemplate.generateWebAddonModulesRegistry(addons, isAddonDevMode(), addonModulesFile);
	}

	public void syncActiveWebAddons() {
		boolean bSomethingChanged = false;
		for (WebAddon webAddon : getActiveWebAddons()) {
			try {
				if (syncWebAddonOutputToObject(webAddon)) {
					BusinessObjectProvider.update(webAddon);
					bSomethingChanged = true;
				}
			} catch (BusinessException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, webAddon, ex);
			}
		}
		if (bSomethingChanged) {
			writeFileHashValuesToOutput();
		}
	}

	/**
	 * save addon files to DB
	 * @param webAddon
	 */
	public boolean syncWebAddonOutputToObject(final WebAddon webAddon) {
		LOG.info("Sync webAddonOutput '{}.", webAddon.getName());
		if (!isNodeJsAvailable()) {
			return false;
		}
		final String[] excludedSubDirectories = new String[]{"dist", "node_modules", ".tmp"};
		final String[] excludedFileTypes = new String[]{".log", ".swp"};
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		final List<WebAddonFile> webAddonFileList = webAddon.getWebAddonFile();
		final Set<UID> foundFiles = new HashSet<>();
		final MutableBoolean bSomethingChanged = new MutableBoolean(false);
		try {
			if (!Files.exists(addonOutputPath)) {
				Files.createDirectories(addonOutputPath);
			}
			Files.walkFileTree(addonOutputPath, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					for (String excluded : excludedFileTypes) {
						if (file.getFileName().toString().toLowerCase().endsWith(excluded.toLowerCase())) {
							return FileVisitResult.CONTINUE;
						}
					}
					final Path relativePath = addonOutputPath.relativize(file.getParent());

					final String fileName = file.getFileName().toString();
					final byte[] bytesFromFile = Files.readAllBytes(file);
					boolean bFound = false;
					String convertedFilePath=relativePath.toString().replaceAll("\\\\","/");

					for (WebAddonFile webAddonFile : webAddonFileList) {
						if (((convertedFilePath.isEmpty() && webAddonFile.getPath() == null) ||
								convertedFilePath.equals(webAddonFile.getPath()))
								&& fileName.equals(webAddonFile.getFile().getName())) {
							bFound = true;
							foundFiles.add(webAddonFile.getId());
							// File found. Check for changes
							if (!Arrays.equals(bytesFromFile, webAddonFile.getFile().getContent())) {
								// Content changed
								try {
									webAddonFile.setFile(FileProvider.newFile(file.toFile()));
									bSomethingChanged.setValue(true);
								} catch (BusinessException ex) {
									LOG.error("Unexpected IO ERROR: {} on {}", ex, file, ex);
								}
							}
							break;
						}
					}

					if (!bFound) {
						// new file
						WebAddonFile webAddonFile = new WebAddonFile();
						if (!convertedFilePath.isEmpty()) {
							webAddonFile.setPath(convertedFilePath);
						}
						try {
							webAddonFile.setFile(FileProvider.newFile(file.toFile()));
						} catch (BusinessException ex) {
							LOG.error("Unexpected IO ERROR: {} on {}", ex, file, ex);
						}
						webAddon.insertWebAddonFile(webAddonFile);
						bSomethingChanged.setValue(true);
					}

					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
					final Path relativePath = addonOutputPath.relativize(dir);
					for (String excluded : excludedSubDirectories) {
						if (relativePath.toString().startsWith(excluded)) {
							return FileVisitResult.SKIP_SUBTREE;
						}
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					if (exc == null) {
						return FileVisitResult.CONTINUE;
					} else {
						throw exc;
					}
				}
			});

			// Check for deleted files
			for (WebAddonFile webAddonFile : webAddonFileList) {
				if (!foundFiles.contains(webAddonFile.getId())) {
					webAddon.deleteWebAddonFile(webAddonFile);
					bSomethingChanged.setValue(true);
				}
			}

		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPath, ex);
		}
		return bSomethingChanged.getValue();
	}

	/**
	 * delete addon source files
	 * @param webAddon
	 */
	public void deleteAddonFiles(WebAddon webAddon) {
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		LOG.info("Deleting addon directory: " + addonOutputPath.toString());

		try {
			LOG.info("Deleting: " + addonOutputPath);
			FileUtils.deleteDirectory(new File(addonOutputPath.toString()));
			new File(addonOutputPath.toString() + ".log").delete();
		} catch (IOException ex) {
			LOG.error("Unable to delete addon directory: {} on {}", ex, addonOutputPath, ex);
		}
	}


	public Path getWebAddonLogPath(WebAddon webAddon) {
		final Path addonOutputPath = getWebAddonOutputPath(webAddon);
		Path logFilePath = addonOutputPath.getParent().resolve(addonOutputPath.getFileName().toString() + ".log");
		return logFilePath;
	}

	public static void clearRecursive(final Path pathToClear, final Path...dirsOrFilesToIgnore) throws IOException {
		if (!pathToClear.toFile().exists()) {
			return;
		}
		Files.walkFileTree(pathToClear, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				if (dirsOrFilesToIgnore != null) {
					for (Path toIgnore : dirsOrFilesToIgnore) {
						if (file.equals(toIgnore)) {
							return FileVisitResult.CONTINUE;
						}
					}
				}
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
				if (dirsOrFilesToIgnore != null) {
					for (Path toIgnore : dirsOrFilesToIgnore) {
						if (dir.equals(toIgnore)) {
							return FileVisitResult.SKIP_SUBTREE;
						}
					}
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				if (exc == null) {
					if (!dir.equals(pathToClear)) {
						Files.delete(dir);
					}
					return FileVisitResult.CONTINUE;
				} else {
					throw exc;
				}
			}
		});
	}

	private boolean runCommand(Path workingDir, String...command) {
		return _runCommand(false, null, null, workingDir, command);
	}

	private boolean runCommand(StringBuilder log, Path workingDir, String...command) {
		return _runCommand(false, log, null, workingDir, command);
	}

	private boolean runCommandSilent(StringBuilder log, String...command) {
		return _runCommand(true, log, null, null, command);
	}

	private boolean runCommandSilent(Path workingDir, String...command) {
		return _runCommand(true, null, null, workingDir, command);
	}

	private boolean runCommandSilentWithOutput(StringBuilder processOutput, String...command) {
		return _runCommand(true, null, processOutput, null, command);
	}

	private boolean runCommandSilentWithOutput(StringBuilder processOutput, Path workingDir, String...command) {
		return _runCommand(true, null, processOutput, workingDir, command);
	}

	private static boolean _runCommand(boolean bSilent, StringBuilder log, StringBuilder processOutput, Path workingDir, String...command) {
		final String commandString = String.join(" ", command);
		_log(log, bSilent, "[exec in " + workingDir + "] '" + commandString + "'.");

		int returnValue = 0;
		try {
			ProcessBuilder pb = new ProcessBuilder(command);
			pb.redirectErrorStream(true);
			if (workingDir != null) {
				pb.directory(workingDir.toFile());
			}

			Process p = pb.start();

			new Thread(() -> {
				try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
					String line;
					while ((line = reader.readLine()) != null) {
						_log(log, bSilent, " > " + line);
						_appendLine(processOutput, line);
					}
				} catch (IOException e) {
					LOG.error("Error running '{}'.", commandString, e);
				}
			}).start();

			p.waitFor(6L, TimeUnit.MINUTES);
			p.destroy();
			returnValue = p.waitFor();
		} catch (Exception ex) {
			if (!bSilent) {
				LOG.error("Unexpected ERROR: {} on {}"+  Arrays.toString(command), ex);
			}
			if (log != null) {
				log.append(String.format("Unexpected ERROR: {} on {}"+  Arrays.toString(command)));
			}
		}
		if (returnValue != 0) {
			LOG.error("Error while running '{}'. Status code '{}'", Arrays.toString(command), returnValue);
			return false;
		}
		_log(log, bSilent, "[exec in " + workingDir + "] '" + commandString + "' successfull.");

		return true;
	}

	private static StringBuilder _appendLine(StringBuilder s, String l) {
		if (s != null && l != null) {
			if (s.length() > 0) {
				s.append("\n");
			}
			s.append(l);
		}
		return s;
	}

	private static void _log(StringBuilder log, boolean bSilent, String msg) {
		if (bSilent) {
			LOG.debug(msg);
		} else {
			LOG.info(msg);
		}
		if (log != null) {
			log.append(msg + "\n");
		}
	}


	private boolean isNodeJsAvailable() {
		return NPM_VERSION != null;
	}


	/**
	 * check against the file hash values
	 */
	private boolean isOutputPathUpToDate() {
		return getFileHashValuesFromOutput().equals(getFileHashValuesFromDb());
	}

	public Path writeFileHashValuesToOutput() {
		Path outputPath = isAddonDevMode() ? getWebclientSrcDir().resolve("src/addons") : getWebclientSrcDir();
		final Path hashListFilePath = outputPath.resolve("hashlist");
		final File hashListFile = hashListFilePath.toFile();
		LOG.info("Writing hash values to {}", hashListFile);
		try {
			Files.deleteIfExists(hashListFile.toPath());
			final SortedMap<UID, String> sortedFileHashes = new TreeMap<>();
			final Query<WebAddonFile> q = QueryProvider.create(WebAddonFile.class);
			final List<WebAddonFile> webAddonFiles = QueryProvider.execute(q);
			for (WebAddonFile webAddonFile : webAddonFiles) {
				sortedFileHashes.put(webAddonFile.getId(), StringUtils.defaultIfEmpty(webAddonFile.getHash(), "null"));
			}
			final StringBuilder result = new StringBuilder();
			for (UID fileId : sortedFileHashes.keySet()) {
				result.append(fileId.getString() + ":" + sortedFileHashes.get(fileId) + "\n");
			}
			FileUtils.writeStringToFile(hashListFile, result.toString());
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, hashListFile, ex);
		}
		return hashListFilePath;
	}

	private String getFileHashValuesFromDb() {
		final SortedMap<UID, String> sortedFileHashes = new TreeMap<>();
		final Query<WebAddonFile> q = QueryProvider.create(WebAddonFile.class);
		final List<WebAddonFile> webAddonFiles = QueryProvider.execute(q);
		for (WebAddonFile webAddonFile : webAddonFiles) {
			sortedFileHashes.put(webAddonFile.getId(), StringUtils.defaultIfEmpty(webAddonFile.getHash(), "null"));
		}
		final StringBuilder result = new StringBuilder();
		for (UID fileId : sortedFileHashes.keySet()) {
			result.append(fileId.getString() + ":" + sortedFileHashes.get(fileId) + "\n");
		}
		return result.toString();
	}

	private String getFileHashValuesFromOutput() {
		Path outputPath = isAddonDevMode() ? getWebclientSrcDir().resolve("src/addons") : getWebclientSrcDir();
		final File hashListFile = outputPath.resolve("hashlist").toFile();
		try {
			if (hashListFile.exists()) {
				final byte[] fileBytes = Files.readAllBytes(hashListFile.toPath());
				final String result = new String(fileBytes);
				return result;
			}
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, hashListFile, ex);
		}
		return "";
	}

	public boolean isOutputAndCompileAllRunning() {
		return outputAndCompileAllIsRunning;
	}

	/**
	 * removes addon from package.json and node_modules and deletes package-lock.json
	 * @param webAddon
	 */
	public void uninstallAddon(WebAddon webAddon) {
		LOG.info("Uninstalling addon '{}'.", webAddon.getName());
		final String addonFileName = WebAddonUtils.camelCaseToFileName(webAddon.getName());
		final String addonPackaggeText = "\"" + addonFileName + "\":.*$";
		Path packageJson = getWebclientSrcDir().resolve("package.json");
		try (Stream<String> lines = Files.lines(packageJson)) {
			final List<String> replaced = lines
					.map(line-> line.replaceAll(addonPackaggeText, ""))
					.collect(Collectors.toList());
			Files.write(packageJson, replaced);

			Files.deleteIfExists(getWebclientSrcDir().resolve("package-lock.json"));

			FileUtils.forceDelete(getWebclientSrcDir().resolve("node_modules").resolve(addonFileName).toFile());
		} catch (IOException e) {
			LOG.error("Unable to uninstall addon '{}'.", webAddon.getName(), e);
		}
	}

}
