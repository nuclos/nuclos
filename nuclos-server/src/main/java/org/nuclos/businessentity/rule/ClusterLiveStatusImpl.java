//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.ProcessingException;

import org.apache.commons.lang3.NotImplementedException;
import org.nuclos.api.User;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.ClusterLiveStatus;
import org.nuclos.businessentity.ClusterLiveStatusProxy;
import org.nuclos.businessentity.ClusterServer;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.schema.rest.ServerStatus;
import org.nuclos.server.cluster.ClusterRegistration;
import org.nuclos.server.cluster.RigidClusterHelper;
import org.nuclos.server.maintenance.MaintenanceConstants;
import org.nuclos.server.maintenance.MaintenanceFacadeLocal;

public class ClusterLiveStatusImpl implements ClusterLiveStatusProxy {

	@Override
	public List<ClusterLiveStatus> getAll() {
		throw new NotImplementedException("This method is not used");
	}

	@Override
	public List<UID> getAllIds() {
		throw new NotImplementedException("This method is not used");
	}

	@Override
	public ClusterLiveStatus getById(final UID id) {
		return build(ClusterServer.get(id));
	}

	private static ClusterLiveStatus build(ClusterServer cs) {
		ClusterLiveStatus result = new ClusterLiveStatus();
		result.setId(cs.getId());
		result.setCreatedBy(cs.getCreatedBy());
		result.setCreatedAt(cs.getCreatedAt());
		result.setChangedBy(cs.getChangedBy());
		result.setChangedAt(cs.getChangedAt());
		result.setType(cs.getType());
		result.setProtocol(cs.getProtocol());
		result.setHostname(cs.getHostname());
		result.setPort(cs.getPort());
		result.setContext(cs.getContext());
		result.setTester(RigidClusterHelper.getNodeId().equals(cs.getId()));
		String status = "";
		try {
			final ServerStatus restStatus;
			if (Boolean.TRUE.equals(result.getTester())) {
				restStatus = ServerStatus.builder()
					.withReady(SpringApplicationContextHolder.isNuclosReady())
					.build();
				if (!MaintenanceConstants.MAINTENANCE_MODE_OFF.equals(
						SpringApplicationContextHolder.getBean(MaintenanceFacadeLocal.class).getMaintenanceMode())) {
					restStatus.setMaintenance(true);
				}
			} else {
				restStatus = ClusterRegistration.getRestServerStatus(cs);
			}
			status = String.format("ready=\"%s\";maintenance=\"%s\"", restStatus.isReady(), Boolean.TRUE.equals(restStatus.isMaintenance()));
		} catch (ConnectException | SocketTimeoutException | NoRouteToHostException | UnknownHostException e) {
			status = String.format("not running or is not accessible: %s", e.getMessage());
		} catch (Exception e) {
			status = String.format("is running, but with an uncertain status: %s", e.getMessage());
		}
		if (status.length() > 250) {
			status = status.substring(0, 250);
		}
		result.setStatus(status);
		return result;
	}

	@Override
	public List<ClusterLiveStatus> getByServer(final UID pNuclosclusterServerId) {
		if (!RigidClusterHelper.checkNodeId()) {
			return Collections.emptyList();
		}
		Query<ClusterServer> q = QueryProvider.create(ClusterServer.class);
		return QueryProvider.execute(q).stream().map(cs -> build(cs)).collect(Collectors.toList());
	}

	@Override
	public void setUser(final User user) {

	}
}
