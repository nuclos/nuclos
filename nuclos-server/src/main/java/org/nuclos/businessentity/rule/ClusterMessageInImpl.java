//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import org.apache.commons.lang3.NotImplementedException;
import org.nuclos.api.User;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.ClusterMessage;
import org.nuclos.businessentity.ClusterMessageIn;
import org.nuclos.businessentity.ClusterMessageInProxy;
import org.nuclos.businessentity.ClusterServer;
import org.nuclos.common.UID;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.cluster.ClusterConstants;
import org.nuclos.server.cluster.ClusterMessageHelper;
import org.nuclos.server.cluster.RigidClusterHelper;

public class ClusterMessageInImpl implements ClusterMessageInProxy, ClusterConstants {

	@Override
	public List<ClusterMessageIn> getAll() {
		throw new NotImplementedException("This method is not used");
	}

	@Override
	public List<Long> getAllIds() {
		throw new NotImplementedException("This method is not used");
	}

	@Override
	public ClusterMessageIn getById(final Long id) {
		throw new NotImplementedException("This method is not used");
	}

	public static Date getExactlyOneDayAgo() {
		GregorianCalendar cal = new GregorianCalendar();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		InternalTimestamp timestamp = new InternalTimestamp(cal.getTimeInMillis());
		return timestamp;
	}

	public static String getTimestamp(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
	}

	public static ClusterMessageIn transform(ClusterMessage msg, boolean master, ConcurrentMap<UID, ClusterServer> serversById) {
		ClusterMessageIn result = new ClusterMessageIn();
		result.setId(msg.getId());
		result.setCreatedBy(msg.getCreatedBy());
		result.setCreatedAt(msg.getCreatedAt());
		result.setChangedBy(msg.getChangedBy());
		result.setChangedAt(msg.getChangedAt());
		result.setServerId(msg.getServerId());
		result.setType(msg.getType());
		result.setContent(msg.getContent());
		result.setRead(msg.getRead());
		result.setDone(msg.getDone());
		result.setTimestamp(getTimestamp(msg.getCreatedAt()));
		String sFromServer;
		if (master) {
			// getNuclosTitle geht über den Locale Mechanismus, problematisch an dieser Stelle (neue Transaktion)
			//sFromServer = serversById.get(msg.getServerId()).getNuclosTitle();
			sFromServer = ClusterMessageHelper.getNuclosTitle(serversById.get(msg.getServerId()));
		} else {
			sFromServer = "Master";
		}
		result.setFromServer(sFromServer);
		return result;
	}

	public static ConcurrentMap<UID, ClusterServer> getServersById() {
		return QueryProvider.execute(QueryProvider.create(ClusterServer.class)
			).stream().collect(Collectors.toConcurrentMap(
				cs -> cs.getId(),
				cs -> cs));
	}

	@Override
	public List<ClusterMessageIn> getByServer(final UID pNuclosclusterServerId) {
		String type = ClusterServer.get(pNuclosclusterServerId).getType();
		if (TYPE_SINGLE.equals(type) || !RigidClusterHelper.checkNodeId()) {
			return Collections.emptyList();
		}
		boolean master = TYPE_MASTER.equals(type);
		Query<ClusterMessage> q = QueryProvider.create(ClusterMessage.class);
		q.where(ClusterMessage.Master.eq(master));
		q.and(ClusterMessage.CreatedAt.Gt(getExactlyOneDayAgo()));
		if (!master) {
			q.and(ClusterMessage.ServerId.eq(pNuclosclusterServerId));
		}
		q.orderBy(ClusterMessage.Id, false);
		ConcurrentMap<UID, ClusterServer> serversById = getServersById();
		return QueryProvider.execute(q).stream().map(msg -> transform(msg, master, serversById)).collect(Collectors.toList());
	}

	@Override
	public void setUser(final User user) {

	}
}
