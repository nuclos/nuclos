//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import static org.nuclos.businessentity.rule.ClusterMessageInImpl.getExactlyOneDayAgo;
import static org.nuclos.businessentity.rule.ClusterMessageInImpl.getServersById;
import static org.nuclos.businessentity.rule.ClusterMessageInImpl.getTimestamp;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import org.apache.commons.lang3.NotImplementedException;
import org.nuclos.api.User;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.ClusterMessage;
import org.nuclos.businessentity.ClusterMessageOut;
import org.nuclos.businessentity.ClusterMessageOutProxy;
import org.nuclos.businessentity.ClusterServer;
import org.nuclos.common.UID;
import org.nuclos.server.cluster.ClusterConstants;
import org.nuclos.server.cluster.ClusterMessageHelper;
import org.nuclos.server.cluster.RigidClusterHelper;

public class ClusterMessageOutImpl implements ClusterMessageOutProxy, ClusterConstants {

	@Override
	public List<ClusterMessageOut> getAll() {
		throw new NotImplementedException("This method is not used");
	}

	@Override
	public List<Long> getAllIds() {
		throw new NotImplementedException("This method is not used");
	}

	@Override
	public ClusterMessageOut getById(final Long id) {
		throw new NotImplementedException("This method is not used");
	}

	public static ClusterMessageOut transform(ClusterMessage msg, boolean master, ConcurrentMap<UID, ClusterServer> serversById) {
		ClusterMessageOut result = new ClusterMessageOut();
		result.setId(msg.getId());
		result.setCreatedBy(msg.getCreatedBy());
		result.setCreatedAt(msg.getCreatedAt());
		result.setChangedBy(msg.getChangedBy());
		result.setChangedAt(msg.getChangedAt());
		result.setServerId(msg.getServerId());
		result.setType(msg.getType());
		result.setContent(msg.getContent());
		result.setRead(msg.getRead());
		result.setDone(msg.getDone());
		result.setTimestamp(getTimestamp(msg.getCreatedAt()));
		String sToServer;
		if (master) {
			// getNuclosTitle geht über den Locale Mechanismus, problematisch an dieser Stelle (neue Transaktion)
			//sToServer = serversById.get(msg.getServerId()).getNuclosTitle();
			sToServer = ClusterMessageHelper.getNuclosTitle(serversById.get(msg.getServerId()));
		} else {
			sToServer = "Master";
		}
		result.setToServer(sToServer);
		return result;
	}

	@Override
	public List<ClusterMessageOut> getByServer(final UID pNuclosclusterServerId) {
		String type = ClusterServer.get(pNuclosclusterServerId).getType();
		if (TYPE_SINGLE.equals(type) || !RigidClusterHelper.checkNodeId()) {
			return Collections.emptyList();
		}
		boolean master = TYPE_MASTER.equals(type);
		Query<ClusterMessage> q = QueryProvider.create(ClusterMessage.class);
		q.where(ClusterMessage.Master.eq(!master));
		q.and(ClusterMessage.CreatedAt.Gt(getExactlyOneDayAgo()));
		if (!master) {
			q.and(ClusterMessage.ServerId.eq(pNuclosclusterServerId));
		}
		q.orderBy(ClusterMessage.Id, false);
		ConcurrentMap<UID, ClusterServer> serversById = getServersById();
		return QueryProvider.execute(q).stream().map(msg -> transform(msg, master, serversById)).collect(Collectors.toList());
	}

	@Override
	public void setUser(final User user) {

	}
}
