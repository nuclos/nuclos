package org.nuclos.businessentity.rule;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.businessentity.NucletSqlConfig;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common.NuclosFatalException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
        name = "ConfigurationSaveRule",
        description = "")
@SystemRuleUsage(
        boClass = NucletSqlConfig.class,
        order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class ConfigurationSaveRule implements InsertRule, UpdateRule {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ConfigurationSaveRule.class);

    public static String calculateChecksum(String config) {
        try {
            final byte[] content = config.getBytes(StandardCharsets.UTF_8);
            final MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(content);
            final byte[] hash = digest.digest();
            return Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            throw new NuclosFatalException(e);
        }
    }

    @Override
    public void insert(final InsertContext context) throws BusinessException {

    }

    @Override
    public void update(final UpdateContext context) throws BusinessException {

    }
}
