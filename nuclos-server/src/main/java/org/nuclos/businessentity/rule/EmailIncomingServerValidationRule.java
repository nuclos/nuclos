package org.nuclos.businessentity.rule;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.mail.NuclosMail;
import org.nuclos.api.provider.MailProvider;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.businessentity.EmailIncomingServer;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.server.common.mail.AuthenticationMethod;
import org.nuclos.server.common.mail.IncomingServerType;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "EmailIncomingServerValidationRule",
		description = "")
@SystemRuleUsage(
		boClass = EmailIncomingServer.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class EmailIncomingServerValidationRule implements InsertRule, UpdateRule, CustomRule {
	@Override
	public void insert(final InsertContext context) throws BusinessException {
		validate(context.getBusinessObject(EmailIncomingServer.class));
	}

	@Override
	public void update(final UpdateContext context) throws BusinessException {
		validate(context.getBusinessObject(EmailIncomingServer.class));
	}

	private void validate(final EmailIncomingServer server) throws BusinessException {
		if (IncomingServerType.POP3.equals(IncomingServerType.valueOf(server.getType()))) {
			if (StringUtils.isNotEmpty(server.getFolderFrom()) || StringUtils.isNotEmpty(server.getFolderTo())) {
				throw new BusinessException("email.incoming.imapOnly");
			}
		}
		if (AuthenticationMethod.PASSWORD.getValue().equals(server.getAuthenticationMethod())) {
			if (StringUtils.isEmpty(server.getPassword())) {
				throw new BusinessException("email.server.passwordRequired");
			}
		}
	}

	@Override
	public void custom(final CustomContext context) throws BusinessException {
		final EmailIncomingServer server = context.getBusinessObject(EmailIncomingServer.class);

		try {
			final List<NuclosMail> received = MailProvider.receive(server.getId(), false);
			server.setTestResult(String.format("Received %d mail(s).", received.size()));
		} catch (Exception e) {
			final StringWriter sWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(sWriter));
			server.setTestResult(sWriter.toString());
		}
	}
}
