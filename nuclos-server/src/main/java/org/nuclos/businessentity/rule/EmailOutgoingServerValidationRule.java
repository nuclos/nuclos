package org.nuclos.businessentity.rule;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.mail.NuclosMail;
import org.nuclos.api.provider.MailProvider;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.businessentity.EmailOutgoingServer;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.server.common.mail.AuthenticationMethod;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "EmailOutgoingServerValidationRule",
		description = "")
@SystemRuleUsage(
		boClass = EmailOutgoingServer.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class EmailOutgoingServerValidationRule implements InsertRule, UpdateRule, CustomRule {

	public static final String KEY = "EmailOutgoingServerValidationRule.test";

	@Override
	public void insert(final InsertContext context) throws BusinessException {
		validate(context.getBusinessObject(EmailOutgoingServer.class));
	}

	@Override
	public void update(final UpdateContext context) throws BusinessException {
		validate(context.getBusinessObject(EmailOutgoingServer.class));
	}

	private void validate(final EmailOutgoingServer server) throws BusinessException {
		if (!AuthenticationMethod.NONE.getValue().equals(server.getAuthenticationMethod())) {
			if (StringUtils.isEmpty(server.getUserName())) {
				throw new BusinessException("email.server.userRequired");
			}
		}

		if (AuthenticationMethod.PASSWORD.getValue().equals(server.getAuthenticationMethod())) {
			if (StringUtils.isEmpty(server.getPassword())) {
				throw new BusinessException("email.server.passwordRequired");
			}
		}
	}

	@Override
	public void custom(final CustomContext context) throws BusinessException {
		final EmailOutgoingServer server = context.getBusinessObject(EmailOutgoingServer.class);

		String recipient = server.getTestRecipient();

		if (recipient == null) {
			throw new BusinessException("No recipient!");
		}

		final NuclosMail mail = new NuclosMail(recipient, String.format("Nuclos Outgoing Email Server %s - Test", server.getNuclosTitle()));
		mail.setMessage("TEST");
		try {
			MailProvider.send(server.getId(), mail);
			server.setTestResult(String.format("Email sent to %s", recipient));
		} catch (Exception e) {
			final StringWriter sWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(sWriter));
			server.setTestResult(sWriter.toString());
		}
	}
}
