package org.nuclos.businessentity.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.businessentity.Jobcontroller;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.job.ejb3.JobControlFacadeRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Created by Sebastian Debring on 4/14/2020.
 */

@Rule(
		name = "JobControllerDisableRule",
		description = "")
@SystemRuleUsage(
		boClass = Jobcontroller.class,
		order = 2
)
@Configurable
public class JobControllerDisableRule implements CustomRule {

	@Autowired
	JobControlFacadeRemote jobControlFacade;

	@Override
	public void custom(final CustomContext context) throws BusinessException {
		context.setUpdateAfterExecution(false);
		Jobcontroller job = context.getBusinessObject(Jobcontroller.class);
		try {
			jobControlFacade.unscheduleJob(job.getId());
		} catch (CommonBusinessException e) {
			throw new BusinessException(e);
		}
	}
}
