package org.nuclos.businessentity.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.businessentity.Jobcontroller;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common.report.ejb3.IJobKey;
import org.nuclos.common.report.ejb3.JobKeyImpl;
import org.nuclos.common.report.ejb3.SchedulerControlFacadeRemote;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.job.ejb3.JobControlFacadeRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Created by Sebastian Debring on 4/14/2020.
 */

@Rule(
		name = "JobControllerEnableRule",
		description = "")
@SystemRuleUsage(
		boClass = Jobcontroller.class,
		order = 1
)
@Configurable
public class JobControllerEnableRule implements CustomRule {

	@Autowired
	SchedulerControlFacadeRemote schedulerControlFacade;

	@Autowired
	JobControlFacadeRemote jobControlFacade;

	@Override
	public void custom(final CustomContext context) throws BusinessException {
		context.setUpdateAfterExecution(false);
		Jobcontroller job = context.getBusinessObject(Jobcontroller.class);
		checkNameWithAlreadyScheduledJobs(job);
		try {
			jobControlFacade.scheduleJob(job.getId());
		} catch (CommonBusinessException e) {
			throw new BusinessException(e);
		}
	}

	private void checkNameWithAlreadyScheduledJobs(Jobcontroller job) throws BusinessException {
		final IJobKey jk = new JobKeyImpl(job.getId(), "DEFAULT");
		if (schedulerControlFacade.isScheduled(jk)) {
			throw new BusinessException(StringUtils.getParameterizedExceptionMessage(
					"jobcontroller.error.validation.name", job.getName()));
		}
	}
}
