//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.nuclos.api.User;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.businessentity.JobQueueRun;
import org.nuclos.businessentity.JobQueueRunProxy;
import org.nuclos.common.UID;
import org.nuclos.server.job.SchedulableJob;
import org.nuclos.server.job.valueobject.JobVO;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "JobQueueRunImpl",
		description = "")
@Configurable
@SuppressWarnings("unused") // system internal usage
public class JobQueueRunImpl implements JobQueueRunProxy {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(JobQueueRunImpl.class);

	@Override
	public void setUser(final User user) {

	}

	@Override
	public List<JobQueueRun> getAll() {
		return null;
	}

	@Override
	public List<Long> getAllIds() {
		return null;
	}

	@Override
	public JobQueueRun getById(final Long id) {
		return null;
	}

	@Override
	public List<JobQueueRun> getByJob(final UID pNuclosjobcontrollerId) {
		return Collections.emptyList();
	}

	@Override
	public List<JobQueueRun> getByJobQueue(final UID pJobQueueId) {
		Collection<JobVO> jobs = SchedulableJob.getJobQueue(pJobQueueId);
		if (jobs == null) {
			return Collections.emptyList();
		}

		List<JobQueueRun> lstRuns = new ArrayList<>();
		for (JobVO job : jobs) {
			JobQueueRun jobqueueRun = new JobQueueRun();
			jobqueueRun.setJobQueueId(pJobQueueId);
			jobqueueRun.setOrder(lstRuns.size() + 1);
			jobqueueRun.setJobId(job.getId());
			jobqueueRun.setJob(job.getName());
			lstRuns.add(jobqueueRun);
		}
		return lstRuns;
	}

	@Override
	public void insert(final JobQueueRun pJobqueuerun) throws BusinessException {

	}

	@Override
	public void update(final JobQueueRun pJobqueuerun) throws BusinessException {

	}

	@Override
	public void delete(final Long id) throws BusinessException {

	}

	@Override
	public void commit() {

	}

	@Override
	public void rollback() {

	}
}
