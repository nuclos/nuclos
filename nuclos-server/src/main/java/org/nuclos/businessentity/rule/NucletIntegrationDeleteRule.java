//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.DeleteContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.DeleteRule;
import org.nuclos.businessentity.NucletIntegrationPoint;
import org.nuclos.businessentity.facade.NucletIntegrationPointFacade;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "NucletIntegrationDeleteRule",
		description = "")
@SystemRuleUsage(
		boClass = NucletIntegrationPoint.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class NucletIntegrationDeleteRule implements DeleteRule {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NucletIntegrationDeleteRule.class);

	@Autowired
	private NucletIntegrationPointFacade ipFacade;

	@Override
	public void delete(final DeleteContext context) throws BusinessException {
		NucletIntegrationPoint point = context.getBusinessObject(NucletIntegrationPoint.class);
		ipFacade.deleteIpView(point);
	}

}
