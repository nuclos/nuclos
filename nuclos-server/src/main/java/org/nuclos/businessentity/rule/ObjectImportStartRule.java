package org.nuclos.businessentity.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.businessentity.Importfile;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.fileimport.ejb3.CsvImportFacadeRemote;
import org.nuclos.server.fileimport.ejb3.ImportFacadeRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "ObjectImportStartRule",
		description = "")
@SystemRuleUsage(
		boClass = Importfile.class,
		order = 3
)
@Configurable
public class ObjectImportStartRule implements CustomRule {

	@Autowired
	CsvImportFacadeRemote importFacadeRemote;

	@Autowired
	LocaleFacadeLocal localeFacade;

	@Override
	public void custom(final CustomContext context) throws BusinessException {
		context.setUpdateAfterExecution(false);
		Importfile file = context.getBusinessObject(Importfile.class);
		try {
			importFacadeRemote.doImport(file.getId());
		} catch (CommonBusinessException e) {
			throw new BusinessException(
					ImportFacadeRemote.localize(
							e.getMessage(),
							localeFacade.getResourceBundle(localeFacade.getUserLocale())
					)
			);
		}
	}
}
