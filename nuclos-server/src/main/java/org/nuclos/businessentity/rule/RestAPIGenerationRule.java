package org.nuclos.businessentity.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.businessentity.Restapis;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Created by Falk Zilm on 9/4/2023.
 */

@Rule(
        name = "RestAPIGenerationRule",
        description = "")
@SystemRuleUsage(
        boClass = Restapis.class,
        order = 3
)
@Configurable
public class RestAPIGenerationRule implements UpdateRule {

    @Override
    public void update(final UpdateContext context) throws BusinessException {
        // delete old files
    }
}
