package org.nuclos.businessentity.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.businessentity.nuclosrole;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.statemodel.ejb3.StateFacadeRemote;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Created by Sebastian Debring on 9/26/2018.
 */

@Rule(
		name = "RoleInsertFinalRule",
		description = "")
//@SystemRuleUsage(
//		boClass = nuclosrole.class,
//		order = 1
//)
@Configurable
@SuppressWarnings("unused") // system internal usage

public class RoleInsertFinalRule implements InsertFinalRule {
	
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(RoleInsertFinalRule.class);
	public static ThreadLocal<UID> clonedUid = ThreadLocal.withInitial(() -> null);

	@Autowired
	private StateFacadeRemote stateFacade;

	@Override
	public void insertFinal(final InsertContext insertContext) throws BusinessException {
		try {
			nuclosrole role = insertContext.getBusinessObject(nuclosrole.class);
			stateFacade.copyRolePermissions(clonedUid.get(), new UID(role.getId().toString()));
			stateFacade.invalidateCache();
		} catch (Exception e) {
			LOG.error("Error copying statemodel permissions.", e);

			final String msg = SpringLocaleDelegate.getInstance().getText("RoleCollectController.error.copypermissions.details",
					"Die Statusmodell-Berechtigungen für die geklonte Rolle konnten nicht übertragen werden. Bitte tragen Sie dies manuell nach.");
			throw new BusinessException(msg);
		} finally {
			clonedUid.remove();
		}
		//TODO replace
//		try {
//			RoleRepository.getInstance().invalidate();
//		} catch(Exception e) {
//			throw new NuclosFatalException(e);
//		}
	}
}
