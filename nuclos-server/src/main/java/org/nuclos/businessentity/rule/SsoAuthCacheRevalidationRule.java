package org.nuclos.businessentity.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.DeleteContext;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.DeleteFinalRule;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.businessentity.SsoAuth;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.server.common.SecurityCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "SsoAuthCacheRevalidationRule",
		description = "")
@SystemRuleUsage(
		boClass = SsoAuth.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class SsoAuthCacheRevalidationRule implements InsertFinalRule, UpdateFinalRule, DeleteFinalRule {

	@Autowired
	private SecurityCache securityCache;

	@Override
	public void deleteFinal(final DeleteContext context) throws BusinessException {
		securityCache.revalidateSsoAuth(context.getBusinessObject(SsoAuth.class).getId());
	}

	@Override
	public void insertFinal(final InsertContext context) throws BusinessException {
		securityCache.revalidateSsoAuth(context.getBusinessObject(SsoAuth.class).getId());
	}

	@Override
	public void updateFinal(final UpdateContext context) throws BusinessException {
		securityCache.revalidateSsoAuth(context.getBusinessObject(SsoAuth.class).getId());
	}
}
