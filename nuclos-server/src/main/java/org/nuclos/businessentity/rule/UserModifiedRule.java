package org.nuclos.businessentity.rule;

import org.apache.commons.lang3.StringUtils;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.DeleteContext;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.RuleContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.DeleteRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.businessentity.User;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Created by Sebastian Debring on 4/14/2020.
 */

@Rule(
		name = "UserModifiedRule",
		description = "")
@SystemRuleUsage(
		boClass = User.class,
		order = 1
)
@Configurable
public class UserModifiedRule implements InsertRule, UpdateRule, DeleteRule {

	private static ThreadLocal<Boolean> ACTIVE = ThreadLocal.withInitial(() -> Boolean.TRUE);

	@Override
	public void insert(final InsertContext context) throws BusinessException {
		User user = context.getBusinessObject(User.class);
		validate(user, context);
	}

	@Override
	public void update(final UpdateContext context) throws BusinessException {
		User user = context.getBusinessObject(User.class);
		validate(user, context);
	}

	@Override
	public void delete(final DeleteContext context) throws BusinessException {
		User user = context.getBusinessObject(User.class);
		if (user.getUsername().equals(context.getUser().getName())) {
			throw new BusinessException("exception.user.ownuserdeletion");
		}
	}

	private void validate(User user, final RuleContext context) throws BusinessException {
		if (ACTIVE.get()) {
			if (Boolean.TRUE.equals(user.getSetPassword())) {
				if (!StringUtils.equals(user.getNewPassword(), user.getNewPasswordConfirm())) {
					throw new BusinessException("exception.password.match");
				}
			} else if (user.getNewPassword() != null || user.getNewPasswordConfirm() != null) {
				throw new BusinessException("exception.password.notset");
			}
		}
	}
}
