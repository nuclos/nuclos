//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.nuclos.api.User;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.WebAddon;
import org.nuclos.businessentity.WebAddonLog;
import org.nuclos.businessentity.WebAddonLogProxy;
import org.nuclos.businessentity.facade.WebAddonFacade;
import org.nuclos.common2.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "WebAddonLogImpl",
		description = "")
@Configurable
@SuppressWarnings("unused") // system internal usage
public class WebAddonLogImpl implements WebAddonLogProxy {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(WebAddonLogImpl.class);

	@Autowired
	private WebAddonFacade webAddonFacade;

	@Override
	public void setUser(final User user) {

	}

	@Override
	public List<WebAddonLog> getAll() {
		return null;
	}

	@Override
	public List<Long> getAllIds() {
		return null;
	}

	@Override
	public WebAddonLog getById(final Long id) {
		return null;
	}

	@Override
	public List<WebAddonLog> getByWebAddon(final org.nuclos.common.UID pNucloswebAddonId) {
		final WebAddon webAddon = QueryProvider.getById(WebAddon.class, pNucloswebAddonId);
		List<WebAddonLog> result = new ArrayList<>();
		if (webAddon == null) {
			return result;
		}
		final Path addonLogPath = webAddonFacade.getWebAddonLogPath(webAddon);
		if (!addonLogPath.toFile().exists()) {
			return result;
		}
		try {
			int i = 1;
			for (String sLogLine : Files.readAllLines(addonLogPath, Charset.forName("UTF-8"))) {
				if (StringUtils.looksEmpty(sLogLine)) {
					continue;
				}
				WebAddonLog weblog = new WebAddonLog();
				weblog.setWebAddonId(pNucloswebAddonId);
				weblog.setLogline(i);
				weblog.setLog(sLogLine);
				result.add(weblog);
				i++;
			}
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, addonLogPath, ex);
		}
		return result;
	}

	@Override
	public void insert(final WebAddonLog pWebAddonLog) throws BusinessException {

	}

	@Override
	public void update(final WebAddonLog pWebAddonLog) throws BusinessException {

	}

	@Override
	public void delete(final Long id) throws BusinessException {

	}

	@Override
	public void commit() {

	}

	@Override
	public void rollback() {

	}
}
