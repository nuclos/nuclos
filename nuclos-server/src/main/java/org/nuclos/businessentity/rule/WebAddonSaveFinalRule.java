//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import java.util.HashSet;
import java.util.Set;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.LogContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.businessentity.WebAddon;
import org.nuclos.businessentity.WebAddonProperty;
import org.nuclos.businessentity.WebAddonResultList;
import org.nuclos.businessentity.facade.WebAddonFacade;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "WebAddonSaveFinalRule",
		description = "")
@SystemRuleUsage(
		boClass = WebAddon.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class WebAddonSaveFinalRule implements UpdateFinalRule, InsertFinalRule {

	@Autowired
	private WebAddonFacade webAddonFacade;

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(WebAddonSaveFinalRule.class);

	public static final ThreadLocal<Boolean> WRITE_HASH_VALUES = new ThreadLocal<>();
	static {
		WRITE_HASH_VALUES.set(false);
	}

	public void updateFinal(final UpdateContext updateContext) throws BusinessException {
		saveFinal(updateContext.getBusinessObject(WebAddon.class), updateContext);
	}

	@Override
	public void insertFinal(final InsertContext insertContext) throws BusinessException {
		saveFinal(insertContext.getBusinessObject(WebAddon.class), insertContext);
	}

	private void saveFinal(WebAddon webAddon, LogContext log) throws BusinessException {
		try {
			// Add new result list setups for new properties
			Set<UID> entityResListSetups = new HashSet();
			for (WebAddonResultList reslist : webAddon.getWebAddonResultList()) {
				entityResListSetups.add(reslist.getEntityId());
			}
			for (UID entityId : entityResListSetups) {
				for (WebAddonProperty prop : webAddon.getWebAddonProperty()) {
					boolean bPropFound = false;
					for (WebAddonResultList reslist : webAddon.getWebAddonResultList()) {
						if (RigidUtils.equal(reslist.getEntityId(), entityId) &&
								RigidUtils.equal(reslist.getWebAddonPropertyId(), prop.getId())) {
							bPropFound = true;
							break;
						}
					}
					if (!bPropFound) {
						WebAddonResultList reslist = new WebAddonResultList();
						reslist.setEntityId(entityId);
						reslist.setWebAddonPropertyId(prop.getId());
						reslist.setWebAddonId(webAddon.getId());
						BusinessObjectProvider.insert(reslist);
					}
				}
			}


			if (Boolean.TRUE.equals(WRITE_HASH_VALUES.get())) {
				webAddonFacade.writeFileHashValuesToOutput();
			}
		} finally {
			WRITE_HASH_VALUES.set(false);
		}
	}
}
