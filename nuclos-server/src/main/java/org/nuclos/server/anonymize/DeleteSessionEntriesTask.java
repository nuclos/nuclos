package org.nuclos.server.anonymize;

import java.util.Calendar;
import java.util.TimerTask;

import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.query.DbDelete;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteSessionEntriesTask extends TimerTask {

    private static final Logger LOG = LoggerFactory.getLogger(DeleteSessionEntriesTask.class);

    public DeleteSessionEntriesTask() {
        LOG.debug("Timer for DeleteSessionEntriesTask created.");
    }

    @Override
    public void run() {
        String paramStr = ServerParameterProvider.getInstance().getValue(ParameterProvider.PRIVACY_DELETE_SESSIONLOG_AFTER_PERIOD);
        if (paramStr == null) {
            LOG.info("DeleteSessionEntriesTask will not be executed. Parameter 'PRIVACY_DELETE_SESSIONLOG_AFTER_PERIOD' is not present");
            return;
        }

        int periodDays = ServerParameterProvider.getInstance().getIntValue(ParameterProvider.PRIVACY_DELETE_SESSIONLOG_AFTER_PERIOD,-1);
        if (periodDays < 0) {
            LOG.warn(String.format("DeleteSessionEntriesTask canceled! Parameter 'PRIVACY_DELETE_SESSIONLOG_AFTER_PERIOD' is invalid ( < 0 ): %d", periodDays));
            return;
        }
        LOG.info(String.format("Start DeleteSessionEntriesTask... Parameter 'PRIVACY_DELETE_SESSIONLOG_AFTER_PERIOD': %s", paramStr));

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -periodDays);
        InternalTimestamp timestamp = new InternalTimestamp(calendar.getTimeInMillis());

        LOG.info("Delete sessions older than: " + timestamp);

        PersistentDbAccess dbAccess = new PersistentDbAccess(SpringDataBaseHelper.getInstance().getDbAccess());
        DbQueryBuilder queryBuilder = dbAccess.getQueryBuilder();
        DbDelete delete = queryBuilder.createDelete(E.SESSION);
        delete.where(queryBuilder.lessThan(delete.baseColumn(SF.CREATEDAT), queryBuilder.literal(timestamp)));
		final int countDeleted = dbAccess.executeDelete(delete);

		LOG.info(String.format("DeleteSessionEntriesTask finished, %d entries deleted", countDeleted));
    }
}
