package org.nuclos.server.anonymize;

import java.util.Calendar;
import java.util.TimerTask;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbUpdate;
import org.nuclos.server.dblayer.statements.DbMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class HistoryAnonymizerTask extends TimerTask {

    final static String USERNAME_ANON = "*****";

    private static final Logger LOG = LoggerFactory.getLogger(HistoryAnonymizerTask.class);

	@Autowired
	private MetaProvider metaProv;

    public HistoryAnonymizerTask() {
		LOG.debug("Timer for HistoryAnonymizerTask created.");
    }

    @Override
    public void run() {
		if (metaProv == null) {
			LOG.error("No metaProvider!", new NullPointerException("No metaProvider!"));
			return;
		}
        LOG.debug("Start HistoryAnonymizerTask...");

        for (EntityMeta<?> entityMeta : metaProv.getAllEntities()) {
            for (FieldMeta<?> fieldMeta : entityMeta.getFields()) {
                if (fieldMeta.isLogBookTracking() && fieldMeta.getLogbookAnonymizePeriod() != null) {
                    anonymizeFieldHistoryValues(entityMeta, fieldMeta);
                }
            }
        }

        LOG.debug("HistoryAnonymizerTask finished.");
    }

    private void anonymizeFieldHistoryValues(EntityMeta<?> entityMeta, FieldMeta<?> fieldMeta) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -fieldMeta.getLogbookAnonymizePeriod());
        InternalTimestamp timestamp = new InternalTimestamp(calendar.getTimeInMillis());

        LOG.info("Anonymize history values for " + entityMeta.getEntityName() + "." + fieldMeta.getFieldName() + " older than " + timestamp);

        PersistentDbAccess dbAccess = new PersistentDbAccess(SpringDataBaseHelper.getInstance().getDbAccess());
        DbQueryBuilder queryBuilder = dbAccess.getQueryBuilder();
        DbMap values = new DbMap();
        values.put(E.HISTORY.username, USERNAME_ANON);
        values.put(SF.CREATEDBY, USERNAME_ANON);
        values.put(SF.CHANGEDBY, USERNAME_ANON);

        DbUpdate update = queryBuilder.createUpdate(E.HISTORY, values);
        update.where(queryBuilder.and(
                queryBuilder.equalValue(update.baseColumn(E.HISTORY.entityfield),fieldMeta.getUID()),
                queryBuilder.lessThan(update.baseColumn(SF.CREATEDAT), queryBuilder.literal(timestamp))
        ));
        dbAccess.executeUpdate(update);
    }
}
