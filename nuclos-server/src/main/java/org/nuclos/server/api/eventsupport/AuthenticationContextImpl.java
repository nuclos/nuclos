package org.nuclos.server.api.eventsupport;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.nuclos.api.authentication.AuthenticationResult;
import org.nuclos.api.context.AuthenticationContext;
import org.nuclos.api.context.RefreshAuthenticationContext;

public class AuthenticationContextImpl implements AuthenticationContext, RefreshAuthenticationContext {

	private final String authenticationRuleClazz;

	private final HttpHeaders httpHeaders;

	private final UriInfo uriInfo;

	private final String sessionId;

	private final String remoteAddr;

	private final String remoteHost;

	private final String remoteUser;

	/**
	 * Only for {@code RefreshAuthenticationContext}
	 */
	private AuthenticationResult authResult;

	public AuthenticationContextImpl(
			final String authenticationRuleClazz,
			final HttpHeaders httpHeaders,
			final UriInfo uriInfo,
			final String sessionId,
			final String remoteAddr,
			final String remoteHost,
			final String remoteUser) {
		this.authenticationRuleClazz = authenticationRuleClazz;
		this.httpHeaders = httpHeaders;
		this.uriInfo = uriInfo;
		this.sessionId = sessionId;
		this.remoteAddr = remoteAddr;
		this.remoteHost = remoteHost;
		this.remoteUser = remoteUser;
	}

	public String getAuthenticationRuleClazz() {
		return authenticationRuleClazz;
	}

	@Override
	public HttpHeaders getHttpHeaders() {
		return httpHeaders;
	}

	@Override
	public UriInfo getUriInfo() {
		return uriInfo;
	}

	@Override
	public String getSessionId() {
		return sessionId;
	}

	@Override
	public String getRemoteAddr() {
		return remoteAddr;
	}

	@Override
	public String getRemoteHost() {
		return remoteHost;
	}

	@Override
	public String getRemoteUser() {
		return remoteUser;
	}

	@Override
	public AuthenticationResult getAuthenticationResult() {
		return authResult;
	}

	public void setAuthenticationResult(AuthenticationResult authResult) {
		this.authResult = authResult;
	}

	@Override
	public void log(String message) {
		Logger.getLogger(this.authenticationRuleClazz).info(message);
	}

	@Override
	public void logWarn(String message) {
		Logger.getLogger(this.authenticationRuleClazz).warn(message);
	}

	@Override
	public void logError(String message, Exception ex) {
		Logger.getLogger(this.authenticationRuleClazz).error(message, ex);
	}

}
