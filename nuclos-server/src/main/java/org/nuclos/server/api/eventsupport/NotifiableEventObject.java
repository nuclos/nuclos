//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.api.eventsupport;

import java.util.List;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.notification.Priority;
import org.nuclos.common.RuleNotification;

/**
 * notifiable event object
 * used to update state during rule execution 
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface NotifiableEventObject {

	/**
	 * send message to {@link BusinessObject}
	 * 
	 * @param message	message
	 * @param source	source {@link BusinessObject}
	 * @param target	target {@link BusinessObject}
	 */
	public void notify(String message, BusinessObject source,
			BusinessObject target);

	/**
	 * send prioritised message to {@link BusinessObject}
	 * 
	 * @param message	message 
	 * @param prio		priority {@link Priority}
	 * @param source	source {@link BusinessObject}
	 * @param target	target {@link BusinessObject}
	 */
	public void notify(String message, Priority prio, BusinessObject source,
			BusinessObject target);

	/**
	 * get notifications for business rule
	 * 
	 * @return list of {@link RuleNotification}
	 */
	public List<RuleNotification> getRuleNotifications();

}