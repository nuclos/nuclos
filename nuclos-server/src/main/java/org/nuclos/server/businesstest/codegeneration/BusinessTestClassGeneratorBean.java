package org.nuclos.server.businesstest.codegeneration;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestEntitySource;
import org.nuclos.server.customcode.codegenerator.NuclosJarGeneratorManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Generates the entity classes to be used in business test scripts.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Service
public class BusinessTestClassGeneratorBean {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestClassGeneratorBean.class);

	private final NuclosJarGeneratorManager jarGenerator;
	private final BusinessTestGenerationContextBean generationContext;

	private Date lastCodeGeneration;

	private Map<UID, BusinessTestEntitySource> entitySources = new HashMap<>();

	public BusinessTestClassGeneratorBean(
			final NuclosJarGeneratorManager jarGenerator,
			final BusinessTestGenerationContextBean generationContext
	) {
		this.jarGenerator = jarGenerator;
		this.generationContext = generationContext;
	}

	/**
	 * Creates Groovy classes for all business entities.
	 */
	public synchronized Map<UID, BusinessTestEntitySource> generateSources() throws CommonPermissionException {
		if (lastCodeGeneration != null && jarGenerator.getLastTriggered() != null
				&& lastCodeGeneration.after(jarGenerator.getLastTriggered())) {
			LOG.debug("Test entity classes are still up to date, skipping generation");
			return entitySources;
		}

		lastCodeGeneration = new Date();

		final BusinessTestGenerationContext context = generationContext.getNewContext();
		final BusinessTestClassGenerator generator = new BusinessTestClassGenerator(context);
		entitySources = generator.generateClasses();
		LOG.info("Successfully generated business test classes for {} entities", entitySources.size());

		return entitySources;
	}
}
