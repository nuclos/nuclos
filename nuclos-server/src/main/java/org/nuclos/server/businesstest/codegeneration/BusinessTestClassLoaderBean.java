package org.nuclos.server.businesstest.codegeneration;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestEntitySource;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import groovy.lang.GroovyClassLoader;

/**
 * Compiles the business test class sources.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Component
public class BusinessTestClassLoaderBean {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestClassLoaderBean.class);

	private final BusinessTestClassLoader businessTestClassLoader;

	public BusinessTestClassLoaderBean(final ServerParameterProvider parameterProvider) {
		if (!NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)
				|| !parameterProvider.isEnabled(ParameterProvider.CODEGENERATOR_ENABLED, true)) {
			businessTestClassLoader = null;
			return;
		}
		final File outputDir = NuclosCodegeneratorConstants.BUSINESS_TEST_SRC_MAIN_FOLDER;
		businessTestClassLoader = new BusinessTestClassLoader(outputDir);
	}

	public GroovyClassLoader compileSources(final Map<UID, BusinessTestEntitySource> sources) throws IOException {
		if (businessTestClassLoader != null) {
			return businessTestClassLoader.compileSources(sources);
		}
		return null;
	}
}
