package org.nuclos.server.businesstest.codegeneration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.server.businesstest.NucletVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Caches the entities for which test classes are to be generated, dependencies between those entities, etc.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public abstract class BusinessTestGenerationContext {
	/**
	 * Maps entity UIDs to the corresponding EntityMeta.
	 * Contains any kind of entities (UID entities, business entities, with or without Nuclet).
	 */
	protected final Map<UID, EntityMeta<?>> entities = new HashMap<>();

	/**
	 * Maps entity UIDs to the corresponding EntityMeta.
	 * Contains only business entities which are assigned to a Nuclet.
	 */
	private final Map<UID, EntityMeta<Long>> businessEntities = new HashMap<>();

	/**
	 * Maps field UIDs to the corresponding FieldMeta
	 */
	protected final Map<UID, FieldMeta<?>> fields = new HashMap<>();

	/**
	 * Maps entity UIDs to a List of assigned CustomRules
	 */
	protected final Map<UID, Set<CustomRule>> customRules = new HashMap<>();

	/**
	 * Holds all available EventSupportEventVOs, which model the n:m assignment of EventSupport rules to entities
	 */
	private final List<EventSupportEventVO> eventSupportEntities;

	/**
	 * Maps a target entity UID to its dependencies.
	 */
	protected final Map<UID, List<Dependency>> dependencies = new HashMap<>();

	/**
	 * Maps entity UIDs to the corresponding business test entity class name
	 */
	private final Map<UID, String> businessTestEOClassNames = new HashMap<>();

	/**
	 * Maps entity UIDs to the corresponding package name
	 */
	private final Map<UID, String> businessTestEOPackages = new HashMap<>();

	/**
	 * Maps entity UIDs to the corresponding Nuclos "processes".
	 */
	protected final Map<UID, Set<Process>> processes = new HashMap<>();


	protected abstract NucletVO getNuclet(final UID nucletUID);

	public abstract String getEntityMetaID(final UID entityUID);

	public abstract Collection<StateVO> getEntityStates(final UID entityUID);

	/**
	 * @param entities All necessary entities (including system entities)
	 */
	public BusinessTestGenerationContext(
			final Collection<EntityMeta<?>> entities,
			final List<EventSupportEventVO> eventSupportEntities,
			final Collection<Process> processes
	) {
		for (EntityMeta<?> entity : entities) {
			addEntity(entity);
		}

		this.eventSupportEntities = eventSupportEntities;

		initFields();
		initDependencies();
		initCustomRules();
		initProcesses(processes);
	}

	/**
	 * Adds the given entity to {@link #entities} and possibly to {@link #businessEntities}.
	 */
	private void addEntity(final EntityMeta<?> entity) {
		this.entities.put(entity.getUID(), entity);

		if (!entity.isUidEntity() && entity.getNuclet() != null) {
			businessEntities.put(entity.getUID(), (EntityMeta<Long>) entity);

			this.businessTestEOClassNames.put(entity.getUID(), createClassName(entity));

			final NucletVO nuclet = getNuclet(entity.getNuclet());
			this.businessTestEOPackages.put(entity.getUID(), nuclet.getPackage());
		}
	}

	/**
	 * Initializes the entity-to-process mapping.
	 */
	private void initProcesses(final Collection<Process> processes) {
		for (Process process : processes) {
			if (!this.processes.containsKey(process.getEntityUID())) {
				this.processes.put(process.getEntityUID(), new HashSet<>());
			}

			this.processes.get(process.getEntityUID()).add(process);
		}
	}

	/**
	 * Returns a Set containing all processes of the given entity.
	 */
	Set<Process> getEntityProcesses(UID entityUID) {
		Set<Process> result = new HashSet<>();

		if (processes.containsKey(entityUID)) {
			result.addAll(processes.get(entityUID));
		}

		return result;
	}

	/**
	 * Fills the mapping of entity UIDs to assigned CustomRules.
	 */
	private void initCustomRules() {
		if (eventSupportEntities == null) {
			return;
		}

		for (EventSupportEventVO event : eventSupportEntities) {
			final UID entityUID = event.getEntityUID();
			final String fullClassName = event.getEventSupportClass();
			final String type = event.getEventSupportClassType();

			// Ignore anything but CustomRules
			if (!StringUtils.equals(type, org.nuclos.api.rule.CustomRule.class.getCanonicalName())) {
				continue;
			}

			if (!customRules.containsKey(entityUID)) {
				customRules.put(entityUID, new HashSet<>());
			}

			final CustomRule customRule = new CustomRule(fullClassName);

			customRules.get(entityUID).add(customRule);
		}
	}

	private String createClassName(EntityMeta<?> entity) {
		String name = entity.getEntityName();

		name = StringUtils.capitalize(name);    // Groovy class names must be capitalized
		name = escapeIdentifier(name);

		return name;
	}

	/**
	 * Fills the fields map.
	 */
	private void initFields() {
		for (EntityMeta<?> entity : this.entities.values()) {
			for (FieldMeta<?> field : entity.getFields()) {
				fields.put(field.getUID(), field);
			}
		}
	}

	/**
	 * Finds all referencing fields and fills the dependency map.
	 */
	private void initDependencies() {
		for (EntityMeta<?> entity : this.entities.values()) {
			for (FieldMeta<?> field : entity.getFields()) {
				if (field.getForeignEntity() == null) {
					continue;
				}

				final UID foreignEntityUID = field.getForeignEntity();
				dependencies.computeIfAbsent(foreignEntityUID, k -> new ArrayList<>());

				final Dependency dependency = new Dependency(
						entity.getUID(),
						foreignEntityUID,
						field.getUID()
				);

				dependencies.get(foreignEntityUID).add(dependency);
			}
		}
	}

	/**
	 * Returns all entities known to this context.
	 */
	public Collection<EntityMeta<?>> getAllEntities() {
		return entities.values();
	}

	/**
	 * Returns only the Nuclet-assigned business entities known to this context.
	 */
	public Collection<EntityMeta<Long>> getBusinessEntities() {
		return businessEntities.values();
	}

	List<Dependency> getDependencies(UID entityUID) {
		return dependencies.get(entityUID) == null ? Collections.EMPTY_LIST : dependencies.get(entityUID);
	}

	/**
	 * Looks up the entity with the given UID.
	 *
	 * @throws IllegalArgumentException if the entity was not found.
	 */
	public EntityMeta<?> getEntity(final UID uid) {
		if (!entities.containsKey(uid)) {
			throw new IllegalArgumentException("Unknown entity " + uid);
		}

		return entities.get(uid);
	}

	public FieldMeta<?> getField(final UID uid) {
		return fields.get(uid);
	}

	String getBusinessTestEOClassName(final UID entityUID) {
		return businessTestEOClassNames.get(entityUID);
	}

	String getBusinessTestEOPackage(final UID uid) {
		return businessTestEOPackages.get(uid);
	}

	public static String escapeIdentifier(final String identifier) {
		return NuclosEntityValidator.escapeIdentifierAndQuote(identifier);
	}

	boolean isMultiDependency(Dependency dep) {
		final List<Dependency> deps = this.dependencies.get(dep.getTargetEntity());
		return deps != null && deps.size() > 1;
	}

	/**
	 * Gets a Set of the full class names of CustomRules that are assigned to the given entity UID.
	 */
	Set<CustomRule> getCustomRules(final UID entityUID) {
		if (!customRules.containsKey(entityUID)) {
			return new HashSet<>();
		}

		return customRules.get(entityUID);
	}

	/**
	 * Models a dependency of one entity to another entity via a referencing field.
	 */
	public static class Dependency {
		final UID sourceEntity;
		final UID targetEntity;
		final UID fieldUID;

		public Dependency(final UID sourceEntity, final UID targetEntity, final UID fieldUID) {
			this.sourceEntity = sourceEntity;
			this.targetEntity = targetEntity;
			this.fieldUID = fieldUID;
		}

		UID getSourceEntity() {
			return sourceEntity;
		}

		public UID getTargetEntity() {
			return targetEntity;
		}

		public UID getFieldUID() {
			return fieldUID;
		}
	}

	public static class CustomRule {
		final String fullName;
		final String simpleName;

		public CustomRule(final String fullName) {
			this.fullName = fullName;
			this.simpleName = fullName.substring(fullName.lastIndexOf('.') + 1);
		}

		String getFullName() {
			return fullName;
		}

		public String getSimpleName() {
			return simpleName;
		}
	}

	/**
	 * Represents a Nuclos "process".
	 * <p>
	 * This is not really a process and should be renamed...
	 */
	public static class Process {
		final UID uid;
		final String name;
		final UID entityUID;

		public Process(final UID uid, final String name, final UID entityUID) {
			this.uid = uid;
			this.name = name;
			this.entityUID = entityUID;
		}

		public UID getUID() {
			return uid;
		}

		public String getName() {
			return name;
		}

		public UID getEntityUID() {
			return entityUID;
		}

		@Override
		public boolean equals(final Object o) {
			if (this == o) return true;
			if (!(o instanceof Process)) return false;

			final Process process = (Process) o;

			return uid.equals(process.uid);
		}

		@Override
		public int hashCode() {
			return uid.hashCode();
		}
	}
}