package org.nuclos.server.businesstest.codegeneration.script;

import org.nuclos.common.EntityMeta;
import org.nuclos.server.businesstest.IBusinessTestLogger;
import org.nuclos.server.businesstest.codegeneration.BusinessTestGenerationContext;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptSource;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestCustomRuleScriptGenerator extends AbstractBusinessTestScriptGenerator {
	/**
	 * The simple Java class name of the CustomRule.
	 */
	private final String ruleName;

	BusinessTestCustomRuleScriptGenerator(
			final EntityMeta<?> meta,
			final IBusinessTestNucletCache nucletCache,
			final IBusinessTestLogger logger,
			final BusinessTestGenerationContext context, final String ruleName
	) {
		super(meta, nucletCache, logger, context);
		this.ruleName = ruleName;
	}

	public String getTestName() {
		return "CUSTOMRULE " + meta.getEntityName() + " " + ruleName;
	}

	@Override
	public BusinessTestScriptSource generateScriptSource() {
		final String className = getClassName();
		final BusinessTestScriptSource script = createDefaultScript();

		script.addScriptLine("List<" + className + "> bos = " + className + ".list(10)");
		script.addScriptLine("");
		script.addScriptLine("if (bos.empty) {");
		script.addScriptLine("	fail('Could not run test: no records available')");
		script.addScriptLine("}");
		script.addScriptLine("");
		script.addScriptLine("bos.each { " + className + " bo ->");
		script.addScriptLine("	attempt {");
		script.addScriptLine("		bo.execute" + ruleName + "()");
		script.addScriptLine("	}");
		script.addScriptLine("}");

		return script;
	}
}
