package org.nuclos.server.businesstest.codegeneration.source;

import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

/**
 * Defines Groovy source code, i.e. a class or a script.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
public class AbstractGroovySource {
	private final String pkg;
	private Set<String> imports = new TreeSet<>();

	AbstractGroovySource(final String pkg) {
		this.pkg = pkg;
	}

	public void addImport(Class<?> cls) {
		imports.add(cls.getCanonicalName());
	}

	public void addImport(String imp) {
		imports.add(imp);
	}

	public String getGroovySource() {
		StringBuilder sb = new StringBuilder();

		if (StringUtils.isNotBlank(pkg)) {
			sb.append("package ").append(pkg).append("\n");
			sb.append("\n");
		}

		for (String imp : imports) {
			sb.append("import ").append(imp).append("\n");
		}

		sb.append("\n");

		return sb.toString();
	}

	public String getPkg() {
		return pkg;
	}

	public Set<String> getImports() {
		return imports;
	}
}
