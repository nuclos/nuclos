package org.nuclos.server.businesstest.codegeneration.source;

/**
 * Defines a business test script.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 *
 */
public class BusinessTestScriptSource extends AbstractGroovySource {
	private String script = "";

	public BusinessTestScriptSource(final String pkg) {
		super(pkg);
	}

	public void addScriptLine(String line) {
		script += line + "\n";
	}

	public String getGroovySource() {
		String source = super.getGroovySource();

		source += script;

		return source;
	}
}
