package org.nuclos.server.businesstest.execution;

/**
 * An Exception indicating that the execution of a business test failed,
 * whereby the cause lies in the execution framework.
 * E.g. this Exception can be thrown if not enough Threads are available for a business test.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
public class BusinessTestExecutionException extends RuntimeException {
	private static final long serialVersionUID = -4158382939221366251L;

	BusinessTestExecutionException(final String message) {
		super(message);
	}
}
