package org.nuclos.server.businesstest.execution;

import java.util.concurrent.Callable;

import org.nuclos.server.common.ServerParameterProvider;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Service
public class BusinessTestRollbackBean {

	private final ServerParameterProvider parameterProvider;

	public BusinessTestRollbackBean(final ServerParameterProvider parameterProvider) {
		this.parameterProvider = parameterProvider;
	}

	void rollbackAfterTest(final Runnable runnable) throws Exception {
		rollbackAfterTest(() -> {
			runnable.run();
			return null;
		});
	}

	/**
	 * Performs a rollback after a single test, depending on system parameters.
	 */
	@Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Throwable.class)
	private <T> T rollbackAfterTest(final Callable<T> callable) throws Exception {
		return runAndRollback(
				callable,
				isRollbackBetweenTests()
		);
	}

	/**
	 * Performs a rollback after multiple tests, depending on system parameters.
	 */
	void rollbackAfterAllTests(final Runnable runnable) throws Exception {
		rollbackAfterAllTests(() -> {
			runnable.run();
			return null;
		});
	}

	/**
	 * Performs a rollback after multiple tests, depending on system parameters.
	 */
	@Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Throwable.class)
	private <T> T rollbackAfterAllTests(final Callable<T> callable) throws Exception {
		return runAndRollback(
				callable,
				isRollbackActive()
		);
	}

	private boolean isRollbackActive() {
		return parameterProvider.isEnabled("BUSINESS_TEST_ROLLBACK", true);
	}

	private boolean isRollbackBetweenTests() {
		return isRollbackActive() && parameterProvider.isEnabled("BUSINESS_TEST_ROLLBACK_BETWEEN_TESTS", true);
	}


	private <T> T runAndRollback(
			final Callable<T> callable,
			final boolean rollback
	) throws Exception {
		if (rollback) {
			return runAndRollback(callable);
		} else {
			return callable.call();
		}
	}

	private <T> T runAndRollback(final Callable<T> callable) throws Exception {
		Object savepoint = null;
		try {
			savepoint = TransactionAspectSupport.currentTransactionStatus().createSavepoint();
			return callable.call();
		} finally {
			if (savepoint != null) {
				TransactionAspectSupport.currentTransactionStatus().rollbackToSavepoint(savepoint);
			}
		}
	}
}
