package org.nuclos.server.businesstest.execution;

import java.util.concurrent.Callable;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.businesstest.BusinessTestVO;
import org.nuclos.common.businesstest.BusinessTestVO.STATE;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.businesstest.BusinessTestManagementBean;

import groovy.transform.CompileStatic;

/**
 * Base class for the scripts used in business tests. All test scripts extend this class.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@CompileStatic
public abstract class BusinessTestScript extends groovy.lang.Script {

	private int warningCount = 0;
	private int errorCount = 0;

	private Throwable lastError = null;
	private STATE state = null;

	/**
	 * Do anything within the given Callable that could cause errors but should not abort the test.
	 */
	public void attempt(Callable<?> c) {
		try {
			c.call();
		} catch (Throwable t) {
			if (t instanceof BusinessTestException) {
				BusinessTestException bte = (BusinessTestException) t;
				handleError(bte.getState(), bte);
			} else if (CommonBusinessException.class.isAssignableFrom(t.getClass())) {
				handleError(STATE.YELLOW, t);
			} else {
				handleError(STATE.RED, t);
			}
		}
	}

	/**
	 * Includes the business test with the given UID in this test.
	 */
	public Object evaluate(UID testUID) {
		final BusinessTestVO businessTestVO = BusinessTestManagementBean.getInstance().get(testUID);
		final String source = businessTestVO.getSource();

		return evaluate(source);
	}

	/**
	 * Executes the given callable as <username>.
	 */
	public <T> T withUser(
			final String user,
			final Callable<T> callable
	) throws Exception {
		BusinessTestUserCallable<T> userCallable = SpringApplicationContextHolder.getApplicationContext().getBean(BusinessTestUserCallable.class);
		userCallable.setUser(user);
		userCallable.setCallable(callable);

		return userCallable.call();
	}

	private void handleError(STATE state, Throwable t) {
		if (this.state == null || state.compareTo(this.state) > 0) {
			this.state = state;
			lastError = t;
		}

		if (state == STATE.YELLOW) {
			warningCount++;
		} else if (state == STATE.RED) {
			errorCount++;
		}
	}

	/**
	 * Fails the current test with the given message and state "yellow".
	 */
	public void fail(final String message) {
		throw new BusinessTestException(message, null, STATE.YELLOW);
	}

	int getWarningCount() {
		return warningCount;
	}

	int getErrorCount() {
		return errorCount;
	}

	Throwable getLastError() {
		return lastError;
	}

	public STATE getState() {
		return state;
	}
}
