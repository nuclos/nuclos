package org.nuclos.server.businesstest.execution;

import java.util.concurrent.Callable;

import org.nuclos.server.common.NuclosRemoteContextHolder;
import org.nuclos.server.security.NuclosLocalServerSession;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Logs in with as a given user and executes an given Callable.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Component
@Scope("prototype")
public class BusinessTestUserCallable<T> implements Callable<T> {

	private final NuclosRemoteContextHolder remoteContext;
	private final NuclosLocalServerSession session;

	private String user;
	private Callable<T> callable;

	public BusinessTestUserCallable(
			final NuclosRemoteContextHolder remoteContext,
			final NuclosLocalServerSession session
	) {
		this.remoteContext = remoteContext;
		this.session = session;
	}

	public String getUser() {
		return user;
	}

	public void setUser(final String user) {
		this.user = user;
	}

	public Callable<T> getCallable() {
		return callable;
	}

	public void setCallable(final Callable<T> callable) {
		this.callable = callable;
	}

	@Override
	public T call() throws Exception {
		remoteContext.setRemotly(true);

		try {
			return session.runAsUser(user, callable);
		} finally {
			remoteContext.pop();
		}
	}
}
