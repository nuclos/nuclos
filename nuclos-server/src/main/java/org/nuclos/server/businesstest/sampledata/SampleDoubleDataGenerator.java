package org.nuclos.server.businesstest.sampledata;

import java.util.Collection;
import java.util.Random;

/**
 * Generates a new Double value by picking a random value that lies within the range of the given sample values.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class SampleDoubleDataGenerator extends SampleDataGenerator<Double> {
	private final Random random = new Random();

	private Double min;
	private Double max;

	SampleDoubleDataGenerator() {
		super(Double.class);
	}

	@Override
	public void addSampleValues(final Collection<Double> values) {
		for (Double value : values) {
			if (min == null || value < min) {
				min = value;
			}
			if (max == null || value > max) {
				max = value;
			}
		}
	}

	@Override
	public Double newValue() {
		if (min == null || max == null) {
			return null;
		}

		return min + (max - min) * random.nextDouble();
	}
}
