package org.nuclos.server.businesstest.sampledata;

import java.util.Collection;
import java.util.Random;

/**
 * Generates a new Integer value by picking a random value that lies within the range of the given sample values.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class SampleLongDataGenerator extends SampleDataGenerator<Long> {
	private final Random random = new Random();

	private Long min;
	private Long max;

	SampleLongDataGenerator() {
		super(Long.class);
	}

	@Override
	public void addSampleValues(final Collection<Long> values) {
		for (Long value : values) {
			if (min == null || value < min) {
				min = value;
			}
			if (max == null || value > max) {
				max = value;
			}
		}
	}

	/**
	 * @return
	 */
	@Override
	public Long newValue() {
		if (min == null || max == null) {
			return null;
		}

		return min + random.nextLong();
	}
}
