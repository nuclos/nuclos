//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimerTask;

import org.nuclos.common.E;
import org.nuclos.common.SF;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.query.DbDelete;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/*
 * CLUSTERING
 * cleanup old and read messages in Db
 */
@Component
public class ClusterMessageCleanupTimerTask extends TimerTask {

	public static final Logger LOG = LoggerFactory.getLogger(ClusterMessageCleanupTimerTask.class);
	private boolean bFirstCleanup = true;

	@Override
	public void run() {
		try {
		PersistentDbAccess dbAccess = new PersistentDbAccess(SpringDataBaseHelper.getInstance().getDbAccess());
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbDelete del = builder.createDelete(E.CLUSTER_MESSAGE);
		GregorianCalendar cal = new GregorianCalendar();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		InternalTimestamp timestamp = new InternalTimestamp(cal.getTimeInMillis());
		del.where(
			builder.and(
				builder.equalValue(del.baseColumn(E.CLUSTER_MESSAGE.read), Boolean.TRUE),
				builder.lessThan(del.baseColumn(SF.CREATEDAT), builder.literal(timestamp)))
		);
		// first cleanup ? remove all read messages
		// otherwise only read and done, to track problems
		if (!bFirstCleanup) {
			del.addToWhereAsAnd(
				builder.equalValue(del.baseColumn(E.CLUSTER_MESSAGE.done), Boolean.TRUE)
			);
		}
		dbAccess.executeDelete(del);

		bFirstCleanup = false;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

}
