//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster;

import static org.nuclos.common.StreamUtils.distinctByKey;
import static org.nuclos.server.cluster.RigidClusterHelper.getNodeId;
import static org.nuclos.server.cluster.RigidClusterHelper.isMaster;
import static org.nuclos.server.cluster.RigidClusterHelper.markAsDone;
import static org.nuclos.server.cluster.RigidClusterHelper.markAsRead;
import static org.nuclos.server.cluster.TransactionalClusterNotification.getIncludes;

import java.util.Collection;
import java.util.Comparator;
import java.util.TimerTask;
import java.util.function.Consumer;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.nuclos.api.businessobject.Query;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.ClusterMessage;
import org.nuclos.businessentity.facade.WebAddonFacade;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.customcode.codegenerator.NuclosJarGeneratorManager;
import org.nuclos.server.dal.processor.DalChangeNotificationForTransactionSync;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.dbtransfer.TransferFacadeBean;
import org.nuclos.server.job.ejb3.JobControlFacadeBean;
import org.nuclos.server.maintenance.MaintenanceFacadeBean;
import org.nuclos.server.searchfilter.ejb3.SearchFilterFacadeBean;
import org.nuclos.server.security.NuclosLocalServerSession;
import org.nuclos.server.security.UserFacadeLocal;
import org.nuclos.server.statemodel.ejb3.StateFacadeLocal;
import org.nuclos.server.tasklist.TasklistFacadeBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

/*
 * CLUSTERING
 * read messages from Db and process these messages
 */
@Component
public class ClusterMessageReaderTimerTask extends TimerTask {

	public static final Logger LOG = LoggerFactory.getLogger(ClusterMessageReaderTimerTask.class);

	private static final ObjectMapper objectMapper = new ObjectMapper();

	private static final ThreadLocal<Boolean> IS_ROOT_CAUSE = new ThreadLocal() {
		@Override
		protected Object initialValue() {
			return Boolean.FALSE;
		}
	};

	@Autowired
	private NuclosLocalServerSession nuclosLocalServerSession;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	@Autowired
	private CustomCodeManager customCodeManager;

	@Autowired
	private TransferFacadeBean transferFacadeBean;

	@Autowired
	private SearchFilterFacadeBean searchFilterFacadeBean;

	@Autowired
	private UserFacadeLocal userFacadeLocal;

	@Autowired
	private StateFacadeLocal stateFacadeLocal;

	@Autowired
	private MaintenanceFacadeBean maintenanceFacadeBean;

	@Autowired
	private NuclosJarGeneratorManager nuclosJarGeneratorManager;

	@Autowired
	private WebAddonFacade webAddonFacade;

	@Autowired
	private TasklistFacadeBean tasklistFacadeBean;

	@Autowired
	private JobControlFacadeBean jobControlFacadeBean;

	private String username;
	private Collection<? extends GrantedAuthority> authorities;

	@PostConstruct
	public void postConstruct() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		this.username = ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER);
		this.authorities = nuclosLocalServerSession.getAuthorities(this.username, true);
	}

	public static boolean isRootCause() {
		return IS_ROOT_CAUSE.get();
	}

	@Override
	public void run() {
		try {
			IS_ROOT_CAUSE.set(Boolean.TRUE);
			nuclosLocalServerSession.setAuthenticationOnly(username, "", authorities);
			getUnreadMessagesAndMarkAsRead()
					.filter(distinctByKey(msg -> msg.getType() + msg.getContent()))
					.forEach(new BlockingAndExceptionLoggingConsumer() {
						@Override
						boolean processIncomingMessage(final ClusterMessage msg) throws Exception {
							ClusterMessageType type = ClusterMessageType.valueOf(msg.getType());
							switch (type) {
								case DAL_CHANGE_NOTIFICATION:
									return processDalChangeNotification(msg.getContent());
								case FREE_DAL_MEMORY:
									return freeDalMemory();
								case USER_PREFERENCES_RELATED_CHANGE:
									return invalidateUserPreferenceRelatedCaches(msg.getContent());
								case SECURITY_RELATED_CHANGE:
									return invalidateSecurityRelatedCaches(msg.getContent());
								case SSO_AUTH_RELATED_CHANGE:
									return invalidateSsoAuthRelatedCaches(msg.getContent());
								case REBUILD:
									return rebuild(ClusterMessageRebuildLevel.valueOf(msg.getContent()));
								case MAINTENANCE:
									return maintenance(Boolean.valueOf(msg.getContent()));
								case SCHEDULE_JOB:
								case UNSCHEDULE_JOB:
								case EXECUTE_JOB:
									return handleJobMessage(type, msg.getContent());
								case GENERAL_CACHE_INVALIDATION:
								default:
									return invalidateAllCaches();
							}
						}
					});
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			nuclosLocalServerSession.removeAuthenticationOnly();
			IS_ROOT_CAUSE.set(Boolean.FALSE);
		}
	}

	@Transactional(propagation= Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private boolean invalidateUserPreferenceRelatedCaches(String username) {
		if (RigidUtils.looksEmpty(username)) {
			nucletDalProvider.getWorkspaceProcessor().invalidateWorkspaceCaches();
			searchFilterFacadeBean.invalidateCaches();
		} else {
			nucletDalProvider.getWorkspaceProcessor().invalidateWorkspaceCachesByUser(username);
			searchFilterFacadeBean.invalidateCachesByUser(username);
		}
		return true;
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private boolean invalidateSecurityRelatedCaches(String username) {
		if (RigidUtils.looksEmpty(username)) {
			userFacadeLocal.evictCaches();
			SecurityCache.getInstance().invalidate(false);
			stateFacadeLocal.invalidateCache();
		} else {
			userFacadeLocal.evictCacheForUser(username);
			SecurityCache.getInstance().invalidate(username, false, null);
		}
		tasklistFacadeBean.invalidateCaches();
		// Some preferences may now be visible or have been restricted
		invalidateUserPreferenceRelatedCaches(username);
		return true;
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private boolean invalidateSsoAuthRelatedCaches(String ssoAuthId) {
		if (RigidUtils.looksEmpty(ssoAuthId)) {
			return false;
		}
		SecurityCache.getInstance().revalidateSsoAuth(UID.parseUID(ssoAuthId));
		return true;
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private boolean processDalChangeNotification(String sDalNotification) {
		try {
			final DalChangeNotificationForTransactionSync<?> notification = objectMapper.readValue(sDalNotification, DalChangeNotificationForTransactionSync.class);
			nucletDalProvider.getEntityObjectProcessor(notification.getDalEntity()).handleIncomingClusterNotification(RigidUtils.uncheckedCast(notification));
			return true;
		} catch (Exception e) {
			LOG.error(String.format("Error during dal change notification \"%s\"", sDalNotification), e);
			return false;
		}
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private boolean freeDalMemory() {
		nucletDalProvider.freeMemory(true);
		return true;
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private boolean maintenance(boolean on) throws CommonValidationException {
		if (on) {
			maintenanceFacadeBean.enterMaintenanceMode("ClusterMessage");
			maintenanceFacadeBean.forceMaintenanceMode();
		} else {
			maintenanceFacadeBean.exitMaintenanceMode();
		}
		return true;
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private boolean rebuild(ClusterMessageRebuildLevel level) throws NuclosCompileException {
		invalidateAllCaches();
		switch (level) {
			case ALL:
				return transferFacadeBean.recompileSystem(new Transfer.Result());
			case CLASSES_AND_RULES:
				return transferFacadeBean.rebuildJars(new Transfer.Result());
			case RULES_ONLY:
				return customCodeManager.forceRebuild() != null;
			case WEBCLIENT:
				webAddonFacade.outputAndCompileAll();
				return true;
		}
		return false;
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private boolean invalidateAllCaches() {
		transferFacadeBean.revalidateCaches();
		return true;
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private boolean handleJobMessage(ClusterMessageType type, String message) throws CommonBusinessException {
		Pair<UID, String> jobIdAndUsername = TransactionalClusterNotification.getObjectUidAndStringFromMessage(message);
		nuclosLocalServerSession.setAuthenticationOnly(jobIdAndUsername.getY()); // removed from timer task run later
		switch (type) {
			case SCHEDULE_JOB:
				jobControlFacadeBean.scheduleJob(jobIdAndUsername.getX());
				break;
			case UNSCHEDULE_JOB:
				jobControlFacadeBean.unscheduleJob(jobIdAndUsername.getX());
				break;
			case EXECUTE_JOB:
				jobControlFacadeBean.startJobImmediately(jobIdAndUsername.getX());
		}
		return true;
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private Stream<ClusterMessage> getUnreadMessagesAndMarkAsRead() {
		Query<ClusterMessage> q = QueryProvider.create(ClusterMessage.class);
		q.where(ClusterMessage.Read.eq(Boolean.FALSE));
		if (isMaster()) {
			q.and(ClusterMessage.Master.eq(Boolean.TRUE));
		} else {
			q.and(ClusterMessage.Master.eq(Boolean.FALSE));
			q.and(ClusterMessage.ServerId.eq(getNodeId()));
		}
		return QueryProvider.execute(q).stream()
				.sorted(new Comparator<ClusterMessage>() {
					@Override
					public int compare(final ClusterMessage msg1, final ClusterMessage msg2) {
						final ClusterMessageType type1 = ClusterMessageType.valueOf(msg1.getType());
						final ClusterMessageType type2 = ClusterMessageType.valueOf(msg2.getType());
						if (type1 != type2) {
							// DAL notifications before every other cache invalidation, rebuild & co.
							return Integer.compare(type1.order, type2.order);
						}
						return msg1.getId().compareTo(msg2.getId());
					}
				})
				.map(msg -> {
			markAsRead(msg.getId());
			msg.setRead(true);
			return msg;
		});
	}

	private static Collection<Pair<ClusterMessageType, String>> blockTransactionSynchronizations(ClusterMessage msg) {
		ClusterMessageType type = ClusterMessageType.valueOf(msg.getType());
		Collection<Pair<ClusterMessageType, String>> withIncludes = getIncludes(type, msg.getContent(), true);
		withIncludes.forEach(p -> TransactionalClusterNotification.setTransactionSynchronizationBlocked(p.getX(), p.getY(), true));
		return withIncludes;
	}

	private static abstract class BlockingAndExceptionLoggingConsumer implements Consumer<ClusterMessage> {

		abstract boolean processIncomingMessage(ClusterMessage msg) throws Exception;

		public void accept(ClusterMessage msg) {
			Collection<Pair<ClusterMessageType, String>> allBlockages = null;
			try {
				allBlockages = blockTransactionSynchronizations(msg);
				if (isMaster()) {
					if (!getNodeId().equals(msg.getServerId())) {
						// message from slave node, do the same on this master
						if (processIncomingMessage(msg)) {
							markAsDone(msg.getId());
						}
					}
					// send to slaves
					RigidClusterHelper.sendMessage(
							ClusterMessageType.valueOf(msg.getType()),
							msg.getContent(),
							msg.getServerId());
				} else {
					// this is a slave node
					if (processIncomingMessage(msg)) {
						markAsDone(msg.getId());
					}
				}
			} catch (Exception e) {
				LOG.error("Processing incoming message of type {} with content \"{}\" failed: {}",
						msg.getType(), msg.getContent(), e.getMessage(), e);
			} finally {
				if (allBlockages != null) {
					allBlockages.forEach(b -> TransactionalClusterNotification.setTransactionSynchronizationBlocked(
							b.getX(), b.getY(), false
					));
				}
			}
		}

	}

}
