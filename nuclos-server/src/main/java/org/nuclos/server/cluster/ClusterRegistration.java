//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster;

import static org.nuclos.server.cluster.RigidClusterHelper.getNodeId;
import static org.nuclos.server.cluster.RigidClusterHelper.isClusterEnabled;
import static org.nuclos.server.cluster.RigidClusterHelper.isMaster;
import static org.nuclos.server.cluster.RigidClusterHelper.writeStatus;

import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Timer;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.api.businessobject.Query;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.ClusterServer;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.schema.rest.ServerStatus;
import org.nuclos.server.rest.client.TrustAllCertsRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public final class ClusterRegistration implements ClusterConstants {

	private static final Logger LOG = LoggerFactory.getLogger(ClusterRegistration.class);

	@Autowired
	private ConfigurableApplicationContext applicationContext;

	@Autowired
	ClusterMessageReaderTimerTask clusterMessageReaderTimerTask;

	@Autowired
	ClusterMessageCleanupTimerTask clusterMessageCleanupTimerTask;

	@Autowired
	ClusterAuthStateCleanupTimerTask clusterAuthStateCleanupTimerTask;

	private boolean clusterRunning = false;

	public ClusterRegistration() {
	}

	private void register() throws BusinessException {
		if (isMaster()) {
			// get other master servers and check the status,
			// others may not be executed at the same time!
			Query<ClusterServer> q = QueryProvider.create(ClusterServer.class);
			q.where(ClusterServer.Id.neq(getNodeId()));
			q.and(ClusterServer.Type.eq(TYPE_MASTER));
			List<ClusterServer> otherMasters = QueryProvider.execute(q);
			for (ClusterServer cs : otherMasters) {
				try {
					ServerStatus serverStatus = getRestServerStatus(cs);
					if (serverStatus != null) {
						writeStatus("Startup aborted: Duplicate master");
						throw new BusinessException(String.format("Another master cluster node (%s) is already running, and reports status.ready=\"%s\". Startup aborted!",
								cs.getNuclosTitle(), serverStatus.isReady()));
					}
				} catch (ConnectException | SocketTimeoutException | NoRouteToHostException | UnknownHostException e) {
					// Other master is not running, ok!
					LOG.info("Another master cluster node ({}) is registered but not running or cannot be reached: {}",
							cs.getNuclosTitle(), e.getMessage());
					LOG.debug(e.getMessage(), e);
				} catch (ProcessingException e) {
					writeStatus("Startup aborted: Duplicate master");
					throw new BusinessException(String.format("Another master cluster node (%s) is already running, but with an uncertain status: %s. Startup aborted!",
							cs.getNuclosTitle(), e.getMessage()), e);
				}
			}
		}
	}

	public static ServerStatus getRestServerStatus(ClusterServer cs)
			throws ProcessingException, ConnectException, SocketTimeoutException, NoRouteToHostException, UnknownHostException {
		Client client = TrustAllCertsRestClient.buildClient(10000);

		if (TYPE_SINGLE.equals(cs.getType()) &&
				(NOT_AVAILABLE.equals(cs.getProtocol()) ||
				 NOT_AVAILABLE.equals(cs.getHostname()) ||
				 NOT_AVAILABLE.equals(cs.getContext()) ||
				 Integer.valueOf(-1).equals(cs.getPort()))) {
			// not configured
			throw new ConnectException(NOT_AVAILABLE);
		}

		String url = String.format("%s://%s:%d/%s/rest/serverstatus",
				cs.getProtocol(), cs.getHostname(), cs.getPort(), cs.getContext());
		WebTarget target = client.target(url);

		try {
			Response response = target.request(MediaType.APPLICATION_JSON).build("GET").invoke();
			if (response.getStatus() == Response.Status.SERVICE_UNAVAILABLE.getStatusCode()) {
				// not ready (see org.nuclos.server.rest.services.LoginService.serverstatus)
				ServerStatus serverStatus = new ServerStatus();
				serverStatus.setReady(false);
				return serverStatus;
			}
			try {
				ServerStatus serverStatus = response.readEntity(ServerStatus.class);
				return serverStatus;
			} catch (ProcessingException readEx) {
				// entity input stream is closed here. try again but read as text.
				response = target.request(MediaType.APPLICATION_JSON).build("GET").invoke();
				String unexpectedResponse = response.readEntity(String.class);
				throw new ProcessingException(String.format("Unexpected response received \"%s\"", unexpectedResponse), readEx);
			}
		} catch (ProcessingException procEx) {
			throw unwrapConnectionProblemExceptions(procEx);
		}
	}

	public static boolean isAtLeastOneSlaveOnline() {
		LOG.debug("Check the online status of the slaves...");
		Query<ClusterServer> q = QueryProvider.create(ClusterServer.class);
		q.where(ClusterServer.Type.eq(TYPE_SLAVE));
		List<ClusterServer> slaves = QueryProvider.execute(q);
		boolean bResult = false;
		for (ClusterServer slave : slaves) {
			try {
				ServerStatus slaveStatus = getRestServerStatus(slave);
				LOG.debug("Slave node ({}) is running and reports status.ready=\"{}\"", slave.getNuclosTitle(), slaveStatus.isReady());
				bResult = true;
			} catch (ConnectException | SocketTimeoutException | NoRouteToHostException | UnknownHostException e) {
				LOG.debug("Slave node ({}) is not running or is not accessible from master: {}", slave.getNuclosTitle(), e.getMessage());
			} catch (ProcessingException e) {
				LOG.debug("Slave node ({}) is running, but with an uncertain status: {}", slave.getNuclosTitle(), e.getMessage());
				bResult = true;
			}
		}
		return bResult;
	}

	private static ProcessingException unwrapConnectionProblemExceptions(ProcessingException procEx)
			throws ProcessingException, ConnectException, NoRouteToHostException, SocketTimeoutException, UnknownHostException {
		Throwable cause = procEx.getCause();
		// Throw these checked exceptions
		if (cause instanceof ConnectException) {
			throw (ConnectException) cause;
		}
		if (cause instanceof NoRouteToHostException) {
			throw (NoRouteToHostException) cause;
		}
		if (cause instanceof UnknownHostException) {
			throw (UnknownHostException) cause;
		}
        if (cause instanceof SocketTimeoutException) {
            throw (SocketTimeoutException) cause;
        }
		return procEx;
	}

	public synchronized boolean startup() {
		if (isClusterEnabled()) {
			try {
				register();
				clusterRunning = true;

				int iPeriod = 1000 * 10; // 10 sec
				Timer timer = SpringApplicationContextHolder.getBean(Timer.class);
				timer.schedule(clusterMessageReaderTimerTask, iPeriod, iPeriod);
				Runtime.getRuntime().addShutdownHook(
						new Thread(() -> timer.cancel())
				);

				if (isMaster()) {
					iPeriod = 1000 * 60 * 60 * 24; // 24h
					timer.schedule(clusterMessageCleanupTimerTask, iPeriod, iPeriod);
					timer.schedule(clusterAuthStateCleanupTimerTask, iPeriod, iPeriod);
				}

			} catch (BusinessException e) {
				LOG.error(e.getMessage(), e);
				applicationContext.close();
				return false;
			}
		}
		return true;
	}

	public synchronized void shutdown() {
		if (isClusterEnabled() && clusterRunning) {
			LOG.debug("Cluster registration shutdown received");
			clusterRunning = false;
		}
	}

}
