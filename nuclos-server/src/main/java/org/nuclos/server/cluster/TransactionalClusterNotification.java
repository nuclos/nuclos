package org.nuclos.server.cluster;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.server.common.SecurityCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class TransactionalClusterNotification {

	private static final Logger LOG = LoggerFactory.getLogger(TransactionalClusterNotification.class);

	private static final ConcurrentMap<String, TransactionSynchronization> registeredTransactionSynchronizations = new ConcurrentHashMap<>();

	private static final TransactionSynchronization BLOCKER = new TransactionSynchronizationAdapter() {
		@Override
		public void afterCompletion(int status) {
			// do nothing, this is only a blocker and not added to synchronization manager
		}
	};

	public static void notifySecurityRelatedChangeForAllUsers() {
		notifySecurityRelatedChange((String)null);
	}

	public static void notifySecurityRelatedChange(UID userId) {
		String username = null;
		if (userId != null) {
			username = SecurityCache.getInstance().getUserName(userId);
		}
		notifySecurityRelatedChange(username);
	}

	public static void notifySecurityRelatedChange(String username) {
		registerTransactionSynchronizationIfNecessary(
				ClusterMessageType.SECURITY_RELATED_CHANGE,
				username,
				false);
	}

	public static void notifySsoAuthRelatedChange(UID ssoAuthId) {
		registerTransactionSynchronizationIfNecessary(
				ClusterMessageType.SSO_AUTH_RELATED_CHANGE,
				ssoAuthId.getString(),
				false);
	}

	public static void notifyUserPreferencesRelatedChangeForAllUsers() {
		notifyUserPreferencesRelatedChange((String)null);
	}

	public static void notifyUserPreferencesRelatedChange(UID userId) {
		String username = null;
		if (userId != null) {
			username = SecurityCache.getInstance().getUserName(userId);
		}
		notifyUserPreferencesRelatedChange(username);
	}

	public static void notifyUserPreferencesRelatedChange(String username) {
		registerTransactionSynchronizationIfNecessary(
				ClusterMessageType.USER_PREFERENCES_RELATED_CHANGE,
				username,
				false);
	}

	public static void notifyGeneralCacheInvalidation() {
		registerTransactionSynchronizationIfNecessary(
				ClusterMessageType.GENERAL_CACHE_INVALIDATION,
				false);
	}

	public static void notifyFreeDalMemory() {
		registerTransactionSynchronizationIfNecessary(
				ClusterMessageType.FREE_DAL_MEMORY,
				false);
	}

	public static void notifyRebuild(ClusterMessageRebuildLevel level) {
		if (level == null) {
			throw new IllegalArgumentException("level must not be null");
		}
		registerTransactionSynchronizationIfNecessary(
				ClusterMessageType.REBUILD,
				level.name(),
				false);
	}

	public static void notifyMaintenance(boolean on) {
		registerTransactionSynchronizationIfNecessary(
				ClusterMessageType.MAINTENANCE,
				Boolean.toString(on),
				true);
	}

	public static void notifyScheduleJob(UID jobId, String username) {
		notifyJobChange(ClusterMessageType.SCHEDULE_JOB, jobId, username);
	}

	public static void notifyUnscheduleJob(UID jobId, String username) {
		notifyJobChange(ClusterMessageType.UNSCHEDULE_JOB, jobId, username);
	}

	public static void notifyExecuteJob(UID jobId, String username) {
		notifyJobChange(ClusterMessageType.EXECUTE_JOB, jobId, username);
	}

	private static void notifyJobChange(ClusterMessageType type, UID jobId, String username) {
		if (jobId == null) {
			throw new IllegalArgumentException("jobId must not be null");
		}
		registerTransactionSynchronizationIfNecessary(
				type,
				writeObjectUidAndStringToMessage(jobId, username),
				false);
	}

	public static String writeObjectUidAndStringToMessage(UID uid, String s) {
		if (uid == null) {
			uid = UID.UID_NULL;
		}
		return uid.getString().concat(";").concat(s);
	}

	public static Pair<UID, String> getObjectUidAndStringFromMessage(String message) {
		int i = message.indexOf(";");
		UID uid = null;
		String s = null;
		if (i >= 0 && i + 1 < message.length()) {
			uid = new UID(message.substring(0, i));
			s = message.substring(i + 1);
		}
		return new Pair<>(uid, s);
	}

	public static Collection<Pair<ClusterMessageType, String>> getIncludes(ClusterMessageType type, String optionalMessage, boolean addOriginAlso) {
		Set<Pair<ClusterMessageType, String>> result = new HashSet<>();
		if (addOriginAlso) {
			result.add(new Pair<>(type, optionalMessage));
		}

		addIncludesRecursive(type, optionalMessage, result);
		return result;
	}

	private static void addIncludesRecursive(ClusterMessageType type, String optionalMessage, Set<Pair<ClusterMessageType, String>> result) {
		if (!result.add(new Pair<>(type, optionalMessage))) {
			// already included
			return;
		}

		if (type == ClusterMessageType.GENERAL_CACHE_INVALIDATION) {
			// block detailed cache invalidations as well
			addIncludesRecursive(ClusterMessageType.SECURITY_RELATED_CHANGE, null, result);

		} else if (type == ClusterMessageType.REBUILD) {
			// invalidate all caches before rebuild
			addIncludesRecursive(ClusterMessageType.GENERAL_CACHE_INVALIDATION, null, result);

			ClusterMessageRebuildLevel level = ClusterMessageRebuildLevel.valueOf(optionalMessage);
			if (level == ClusterMessageRebuildLevel.ALL) {
				addIncludesRecursive(ClusterMessageType.REBUILD, ClusterMessageRebuildLevel.CLASSES_AND_RULES.name(), result);
				addIncludesRecursive(ClusterMessageType.REBUILD, ClusterMessageRebuildLevel.WEBCLIENT.name(), result);
			} else if (level == ClusterMessageRebuildLevel.CLASSES_AND_RULES) {
				addIncludesRecursive(ClusterMessageType.REBUILD, ClusterMessageRebuildLevel.RULES_ONLY.name(), result);
			}

		} else if (type == ClusterMessageType.SECURITY_RELATED_CHANGE) {
			// Some preferences may now be visible or have been restricted
			addIncludesRecursive(ClusterMessageType.USER_PREFERENCES_RELATED_CHANGE, optionalMessage, result);
		}
	}

	private static void registerTransactionSynchronizationIfNecessary(
			ClusterMessageType type,
			boolean forceToSendImmediately) {
		registerTransactionSynchronizationIfNecessary(type, null, forceToSendImmediately);
	}

	private static void registerTransactionSynchronizationIfNecessary(
			ClusterMessageType type,
			String optionalMessage,
			boolean forceToSendImmediately
			) {
		if (TransactionSynchronizationManager.isSynchronizationActive() &&
				RigidClusterHelper.isClusterEnabled() &&
				!ClusterMessageReaderTimerTask.isRootCause()) {
			final String key = createKey(type, optionalMessage);
			TransactionSynchronization ts = registeredTransactionSynchronizations.get(key);
			if (ts == null) {
				final Collection<Pair<ClusterMessageType, String>> includes = getIncludes(type, optionalMessage, false);
				ts = new TransactionSynchronizationAdapter() {
					@Override
					public int getOrder() {
						return type.order;
					}
					@Override
					public void afterCompletion(int status) {
						try {
							if (status == TransactionSynchronization.STATUS_COMMITTED) {
								RigidClusterHelper.sendMessage(type, optionalMessage);
							}
						} catch (Exception e) {
							LOG.error(e.getMessage(), e);
						} finally {
							registeredTransactionSynchronizations.remove(key, this);
							includes.forEach(p -> setTransactionSynchronizationBlocked(p.getX(), p.getY(), false));
						}
					}
				};
				registeredTransactionSynchronizations.put(key, ts);
				includes.forEach(p -> setTransactionSynchronizationBlocked(p.getX(), p.getY(), true));

				if (forceToSendImmediately) {
					ts.afterCompletion(TransactionSynchronization.STATUS_COMMITTED);
					// a new synchronization to remove the blocks only
					ts = new TransactionSynchronizationAdapter() {
						@Override
						public void afterCompletion(final int status) {
							registeredTransactionSynchronizations.remove(key, this);
							includes.forEach(p -> setTransactionSynchronizationBlocked(p.getX(), p.getY(), false));
						}
					};
				}
				TransactionSynchronizationManager.registerSynchronization(ts);
			}
		}
	}

	public static boolean setTransactionSynchronizationBlocked(
			ClusterMessageType type,
			String optionalMessage,
			boolean block
	) {
		final String key = createKey(type, optionalMessage);
		if (block) {
			return BLOCKER.equals(registeredTransactionSynchronizations.putIfAbsent(key, BLOCKER));
		} else {
			return registeredTransactionSynchronizations.remove(key, BLOCKER);
		}
	}

	private static String createKey(ClusterMessageType type,
									String optionalMessage) {
		return type.name() + optionalMessage;
	}

}
