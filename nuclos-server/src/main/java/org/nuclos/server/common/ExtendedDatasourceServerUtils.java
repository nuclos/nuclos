package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import org.nuclos.api.datasource.DatasourceColumn;
import org.nuclos.api.datasource.DatasourceResult;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.PlainSubCondition;
import org.nuclos.common.database.query.definition.Column;
import org.nuclos.common.database.query.definition.Table;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.incubator.DbExecutor;
import org.nuclos.server.eventsupport.ejb3.DatasourceRuleContext;
import org.nuclos.server.i18n.DataLanguageServerUtils;
import org.nuclos.server.report.SchemaCache;
import org.nuclos.server.report.suppliers.AccessibleMandatorsSupplier;
import org.nuclos.server.report.suppliers.DataLanguageSupplier;
import org.springframework.stereotype.Component;

@Component
public class ExtendedDatasourceServerUtils extends DatasourceServerUtils {

	public ExtendedDatasourceServerUtils(
			final IMandatorUtils mandatorUtils,
			final SessionUtils sessionUtils,
			final ISecurityCache securityCache,
			final MetaProvider metaProvider,
			final SpringDataBaseHelper dataBaseHelper) {
		super(metaProvider, mandatorUtils, sessionUtils, securityCache, dataBaseHelper);
	}

	public PlainSubCondition getConditionWithIdForInClause(
			DatasourceVO datasourcevo,
			Map<String, Object> mpParams,
			final DatasourceCache datasourceCache,
			final SchemaCache schemaCache,
			UID mandator,
			UID language) throws NuclosDatasourceException {
			return new PlainSubCondition(getSqlWithIdForInClause(datasourcevo, mpParams, datasourceCache, schemaCache, new DataLanguageSupplier(language), mandator, new AccessibleMandatorsSupplier(mandator)));
	}

	public ResultVO getDataFromRule(
			String ruleClass,
			Map<String, Object> mpParams,
			Integer iMaxRowCount,
			Supplier<UID> languageSupplier,
			final UID mandator,
			Supplier<Set<UID>> accessibleMandatorSupplier,
			Function<DatasourceRuleContext, DatasourceResult> resultFunction) throws NuclosDatasourceException {
		fillParameters(mpParams, languageSupplier, mandator, accessibleMandatorSupplier);
		try {
			DatasourceRuleContext context = new DatasourceRuleContext(ruleClass, DatasourceRuleContext.Type.GET_ALL, null, mpParams);
			DatasourceResult datasourceResult = resultFunction.apply(context);

			if (datasourceResult == null) {
				throw new NuclosDatasourceException("datasource rule returned null");
			}

			ResultVO result = new ResultVO();
			for (DatasourceColumn column : datasourceResult.getColumns()) {
				ResultColumnVO resultColumn = new ResultColumnVO();
				resultColumn.setColumnLabel(column.getName());
				resultColumn.setColumnClassName(column.getType().getName());
				result.addColumn(resultColumn);
			}

			for (Object[] row : datasourceResult.getRows()) {
				result.addRow(row);
			}

			return result;
		} catch (NuclosFatalException e) {
			throw new NuclosDatasourceException(e);
		}
	}

	protected String createLocaleTableJoinSQL(EntityMeta<?> eMeta,
											Table resultTable) {

		String tableName = resultTable.getName();

		NucletEntityMeta langEntityMeta = DataLanguageServerUtils.createEntityLanguageMeta(
				new EntityMetaVO(eMeta, true), SF.PK_ID.getMetaData(eMeta));
		UID entityRefField = DataLanguageServerUtils.extractForeignEntityReference(eMeta.getUID());
		UID entityRefDataLang = DataLanguageServerUtils.extractDataLanguageReference(eMeta.getUID());


		String tableLangName = langEntityMeta.getDbTable();

		List<String> columnsToSelect = new ArrayList<String>();
		List<String> langColumnsToSelect = new ArrayList<String>();

		for (Column c : resultTable.getColumns()) {
			columnsToSelect.add(c.getName().toLowerCase());
		}

		for (FieldMeta fm : eMeta.getFields()) {
			if (!fm.isCalculated()) {
				if (fm.isLocalized() && fm.getCalcFunction() == null) {
					String dbColumn = fm.getDbColumn().toLowerCase();
					if (columnsToSelect.contains(dbColumn)) {
						columnsToSelect.remove(dbColumn);
					}
					UID langfield = DataLanguageServerUtils.extractFieldUID(fm.getUID());
					FieldMeta<?> fieldLang = langEntityMeta.getField(langfield);
					langColumnsToSelect.add(fieldLang.getDbColumn() + " as " + dbColumn);
				}
			}
		}

		String columnsToSelectAsString = "";

		for (int idx = 0; idx < columnsToSelect.size(); idx++) {
			columnsToSelectAsString += "TNL1." + columnsToSelect.get(idx);
			if (idx != columnsToSelect.size() -1) {
				columnsToSelectAsString += ",";
			}
		}
		for (int idx = 0; idx < langColumnsToSelect.size(); idx++) {
			columnsToSelectAsString +=
					columnsToSelectAsString.length() > 0 ? ", " + "TNL2." + langColumnsToSelect.get(idx) : "TNL2." + langColumnsToSelect.get(idx);
		}

		return "(SELECT " + columnsToSelectAsString + " FROM " + tableName + " TNL1, " + tableLangName + " TNL2 " +
				" WHERE TNL1.intid = TNL2." + langEntityMeta.getField(entityRefField).getDbColumn() +
				" AND TNL2." + langEntityMeta.getField(entityRefDataLang).getDbColumn()+ " = '$locale')";
	}

	public UID getReportLanguageToUse(Collection<UID> lstModules, UID language, Long pk)
			throws NuclosReportException {

		UID retVal = language;
		EntityMeta<?> entityMeta = null;

		String pathToDataLangTable = null;

		for (UID entityUID : lstModules) {
			final EntityMeta<Object> entity = metaProv.getEntity(entityUID);
			if (entityUID != null && entity.getDataLangRefPath() != null) {
				String foundRef = entity.getDataLangRefPath();
				if (pathToDataLangTable == null) {
					pathToDataLangTable = foundRef;
					entityMeta = entity;
				} else {
					if (!foundRef.equals(pathToDataLangTable)) {
						throw new NuclosReportException("This report uses more than one business object that contains a path to the data language table. Only one is allowed");
					}
				}
			}
		}

		if (entityMeta == null) {
			return language;
		}

		EntityMeta<?> langEntityMeta = metaProv.getEntity(NucletEntityMeta.getEntityLanguageUID(entityMeta));
		UID entityRefField = DataLanguageServerUtils.extractForeignEntityReference(entityMeta.getUID());


		String dbNameReferencingEntity = entityMeta.getDbTable();
		String[] pathToDLTable = entityMeta.getDataLangRefPath().split(";");

		String sql = "SELECT T1.intid, T2.struid_t_md_data_language FROM " + dbNameReferencingEntity + " T1 JOIN " + langEntityMeta.getDbTable() + " T2 \n"
				+ "ON T1.intid = " + pk + " AND T1.intid = " + langEntityMeta.getField(entityRefField).getDbColumn() + "\n";

		if (pathToDataLangTable != null) {

			StringBuilder s = new StringBuilder();
			String upperTableName = "T1";
			String upperTableRefName = "intid";


			for(int idx=0; idx < pathToDLTable.length; idx++) {
				s.setLength(0);

				String reference = pathToDLTable[idx];
				UID referenceUID = new UID(reference);

				FieldMeta<?> referencingFieldMeta = metaProv.getEntityField(referenceUID);

				EntityMeta<?> referencingEntityMeta = metaProv.getEntity(referencingFieldMeta.getEntity());
				EntityMeta<?> referencedEntityMeta = metaProv.getEntity(referencingFieldMeta.getForeignEntity());

				String dbNameReferencedEntityPKPrefix = referencedEntityMeta.isUidEntity() ? "struid" : "intid";
				String dbNameReferencingEntityPKPrefix = referencingEntityMeta.isUidEntity() ? "struid" : "intid";

				String dbNameReferencingField = referencingFieldMeta.getDbColumn().replace("STRVALUE", dbNameReferencedEntityPKPrefix);

				String dbNameReferencedEntity = referencedEntityMeta.getDbTable();
				String joinName = "aDLJoin" + (idx + 1);


				s.append("\n\tINNER JOIN(\n");
				s.append("\tSELECT referencingEntity." + dbNameReferencingEntityPKPrefix + ", referencedEntity." + dbNameReferencedEntityPKPrefix + " as " + dbNameReferencedEntityPKPrefix + "_" + dbNameReferencedEntity + "\n");
				s.append("\tFROM " + dbNameReferencingEntity + " referencingEntity, " + dbNameReferencedEntity + " referencedEntity\n");
				s.append("\tWHERE referencingEntity." + dbNameReferencingField + " = referencedEntity." + dbNameReferencedEntityPKPrefix + ") " + joinName + "\n");
				s.append("\tON " + joinName + "." + dbNameReferencingEntityPKPrefix + " = " + upperTableName + "." + upperTableRefName + "\n");

				if (E.DATA_LANGUAGE.getUID().equals(referencedEntityMeta.getUID())) {
					s.append("\tAND " + joinName + "." + dbNameReferencedEntityPKPrefix + "_" + dbNameReferencedEntity+ "= T2.struid_t_md_data_language\n");
				}

				dbNameReferencingEntity = dbNameReferencedEntity;
				upperTableName = joinName;
				upperTableRefName = dbNameReferencedEntityPKPrefix + "_" + dbNameReferencedEntity;

				sql+= s.toString();
			}

			ResultVO result =
					dataBaseHelper.getDbAccess().executePlainQueryAsResultVO(sql, DbExecutor.LimitedResultSetRunner.MAXFETCHSIZE);

			if (result != null && result.getRows().size() > 0) {
				Object object = result.getRows().get(0)[1];
				if (object != null) retVal = new UID(object.toString());

			}

		}

		return retVal;
	}
}
