package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.cluster.TransactionalClusterNotification;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@DependsOn("metaDataProvider")
public class JobCache {

	private List<JobVO> lstAllJobs;
	
	// Spring injection
	
	@Autowired
	private NucletDalProvider nucletDalProvider;
	
	// end of Spring injection
		
	JobCache() {
	}
	
	@PostConstruct
	final void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		loadJobs();
	}

	@Transactional(propagation= Propagation.REQUIRED, noRollbackFor= {Exception.class})
	private void loadJobs() {
		lstAllJobs = new ArrayList<JobVO>();
		
		for (Object eoVO : nucletDalProvider.getEntityObjectProcessor(E.JOBCONTROLLER).getAll()) {
				lstAllJobs.add(MasterDataWrapper.getJobVO((EntityObjectVO)eoVO));
		}
	}
	
	@Cacheable(value="jobcontroller", key="#uid", condition="#uid != null")
	public JobVO getJob(UID uid) {
		JobVO retVal = null;
		
		for (JobVO curjob : getJobs()) {
			if (uid.equals(curjob.getId())) {
				retVal = curjob;
				break;
			}
		}
		
		return retVal;
	}
	
	public List<JobVO> getJobs() {
		if (lstAllJobs.isEmpty())
			loadJobs();
		
		return this.lstAllJobs;
	}

	public void invalidate(final boolean notifyCluster) {
		this.lstAllJobs.clear();

		if (notifyCluster) {
			TransactionalClusterNotification.notifyGeneralCacheInvalidation();
		}
	}
}
