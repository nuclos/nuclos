package org.nuclos.server.common;

import java.util.HashSet;
import java.util.Set;

import org.nuclos.common.UID;
import org.springframework.stereotype.Component;

/**
 * Created by Sebastian Debring on 1/27/2020.
 */

@Component
public class NuclosDocumentReferenceContextHolder {

	public static class DocumentFileLink {
		private final UID documentReference;
		private final UID entityField;
		private final Object pk;

		public DocumentFileLink(final UID documentReference, final UID entityField, final Object pk) {
			this.documentReference = documentReference;
			this.entityField = entityField;
			this.pk = pk;
		}

		public UID getDocumentReference() {
			return documentReference;
		}

		public UID getEntityField() {
			return entityField;
		}

		public Object getPk() {
			return pk;
		}
	}

	private ThreadLocal<Set<DocumentFileLink>> threadLocalRemoveReferences = ThreadLocal.withInitial(() -> new HashSet<>());

	public NuclosDocumentReferenceContextHolder() {
	}

	public void addDocumentToBeRemoved(DocumentFileLink documentLink) {
		threadLocalRemoveReferences.get().add(documentLink);
	}

	public Set<DocumentFileLink> getDocumentsToBeRemoved() {
		return threadLocalRemoveReferences.get();
	}

	public void clear() {
		threadLocalRemoveReferences.remove();
	}
}
