package org.nuclos.server.common;

import static org.nuclos.server.security.NuclosRemoteExceptionLogger.clear;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.nuclos.remoting.NuclosRemotingInterface;
import org.nuclos.remoting.NuclosRemotingInvocation;
import org.nuclos.remoting.NuclosRemotingInvocationResult;
import org.nuclos.remoting.NuclosRemotingMapperFactory;
import org.nuclos.server.security.NuclosRemoteExceptionLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;
import org.springframework.remoting.support.RemoteInvocation;
import org.springframework.remoting.support.RemoteInvocationResult;

import com.fasterxml.jackson.databind.ObjectMapper;

public class NuclosHttpInvokerServiceExporter extends HttpInvokerServiceExporter {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosHttpInvokerServiceExporter.class);

	private NuclosRemoteExceptionLogger exceptionLogger;

	private final boolean bJavaRmiEnabled;

	private static final ObjectMapper jsonMapper = NuclosRemotingMapperFactory.createObjectMapper();

	public NuclosHttpInvokerServiceExporter() {
		super();
		setContentType("application/json");
		this.bJavaRmiEnabled = NuclosSystemParameters.is(NuclosSystemParameters.REMOTING_JAVARMI_ENABELED);
	}

	/**
	 * Spring injected.
	 */
	public void setNuclosRemoteExceptionLogger(NuclosRemoteExceptionLogger exceptionLogger) {
		this.exceptionLogger = exceptionLogger;
	}

	protected RemoteInvocation readRemoteInvocation(HttpServletRequest request, InputStream is) throws IOException, ClassNotFoundException {
		try {
			if (getServiceInterface().isAnnotationPresent(NuclosRemotingInterface.class)) {
				return jsonMapper.readValue(is, NuclosRemotingInvocation.class).getRemoteInvocation();
			} else {
				if (!bJavaRmiEnabled) {
					return new RemoteInvocationRollbackOnly(clear(new RuntimeException("Java-RMI is not enabled")));
				}
				return super.readRemoteInvocation(request, is);
			}
		}
		catch (IOException | ClassNotFoundException | RuntimeException e) {
			Throwable t = exceptionLogger.logException(e, LOG);
			return new RemoteInvocationRollbackOnly(clear(new RuntimeException(t.getMessage())));
		}
	}

	@Override
	protected void writeRemoteInvocationResult(final HttpServletRequest request, final HttpServletResponse response, final RemoteInvocationResult result, final OutputStream os) throws IOException {
		try {
			final RemoteInvocationResult resultToWrite;
			if (result.getValue() instanceof Exception) {
				// special case for problems during deserialization of invocations
				resultToWrite = new RemoteInvocationResult();
				resultToWrite.setException((Exception) result.getValue());
			} else {
				resultToWrite = result;
			}
			if (getServiceInterface().isAnnotationPresent(NuclosRemotingInterface.class)) {
				jsonMapper.writeValue(os, new NuclosRemotingInvocationResult().set(resultToWrite));
			} else {
				super.writeRemoteInvocationResult(request, response, resultToWrite, os);
			}
		}
		catch (IOException | RuntimeException e) {
			exceptionLogger.logException(e, LOG);
		}
	}

}
