package org.nuclos.server.common;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebListener
public class NuclosHttpSessions implements HttpSessionListener {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosHttpSessions.class);

	final static ConcurrentMap<String, HttpSession> sessionCache = new ConcurrentHashMap<>();

	final static ThreadLocal<String> sessionIdLocal = new ThreadLocal<>();

	@Override
	public void sessionCreated(final HttpSessionEvent se) {
		final String sessionId = se.getSession().getId();
		sessionCache.put(sessionId, se.getSession());
	}

	@Override
	public void sessionDestroyed(final HttpSessionEvent se) {
		final String sessionId = se.getSession().getId();
		sessionCache.remove(sessionId);
	}

	public static boolean setCurrentSessionId(String sessionId) {
		if (sessionCache.containsKey(sessionId)) {
			sessionIdLocal.set(sessionId);
			return true;
		} else {
			LOG.warn("Session {} is invalid, please disable session persistence!", sessionId);
			return false;
		}
	}

	public static String getCurrentSessionId() {
		final String sessionId = sessionIdLocal.get();
		if (sessionId != null) {
			if (!sessionCache.containsKey(sessionId)) {
				sessionIdLocal.remove();
				return null;
			}
		}
		return sessionId;
	}

	public static void invalidateAllSessions() {
		invalidate(null);
	}

	public static void invalidateAllOtherSessions() {
		invalidate(getCurrentSessionId());
	}

	private static void invalidate(String ignoreSessionId) {
		sessionCache.forEach((s, httpSession) -> {
			try {
				if (ignoreSessionId == null || !ignoreSessionId.equals(s)) {
					httpSession.invalidate();
				}
			} catch (IllegalStateException ex) {
				// ignore, already invalidated
			}
		});
	}

}
