//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class RecordGrantRight {
	private boolean write;
	private boolean statechange;
	private boolean delete;
	
	public static final RecordGrantRight DEFAULT = new RecordGrantRight(true, true, true);
	public static final RecordGrantRight ALL_RIGHTS = new RecordGrantRight(true, true, true);
	public static final RecordGrantRight NO_RIGHTS = new RecordGrantRight(false, false, false);
	
	public RecordGrantRight(boolean write, boolean statechange, boolean delete) {
	    super();
	    this.write = write;
	    this.statechange = statechange;
	    this.delete = delete;
    }
	
	public boolean canWrite() {
    	return write;
    }
	
	public Boolean canStateChange() {
    	return statechange;
    }
	
	public boolean canDelete() {
    	return delete;
    }

	@Override
    public String toString() {
	    return "write=" + write + " statechange=" + statechange + " delete=" + delete;
    }

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		final RecordGrantRight that = (RecordGrantRight) o;

		return new EqualsBuilder()
				.append(write, that.write)
				.append(statechange, that.statechange)
				.append(delete, that.delete)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(write)
				.append(statechange)
				.append(delete)
				.toHashCode();
	}
}
