//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.SerializationUtils;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.SsoAuth;
import org.nuclos.common.AbstractDisposableRegistrationBean;
import org.nuclos.common.Actions;
import org.nuclos.common.CommonSecurityCache;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.MandatorLevelVO;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SFConstants;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common.security.AttributePermissionKey;
import org.nuclos.common.security.NotifyObject;
import org.nuclos.common.security.Permission;
import org.nuclos.common.security.SsoAuthStatus;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.valuelistprovider.UserLoginRestriction;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.autosync.XMLEntities;
import org.nuclos.server.cluster.RigidClusterHelper;
import org.nuclos.server.cluster.TransactionalClusterNotification;
import org.nuclos.server.common.ejb3.SecurityFacadeBean;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbId;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.spring.NuclosWebApplicationInitializer;
import org.nuclos.server.statemodel.valueobject.StateModelUsageVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.tasklist.TasklistFacadeLocal;
import org.nuclos.server.web.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.oauth2.sdk.pkce.CodeVerifier;
import com.nimbusds.openid.connect.sdk.Nonce;

/**
 * Singleton class for getting permissions.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * TODO: Name is misleading. This class does much more than just caching...
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 00.01.000
 */
@Component
public class SecurityCache extends AbstractDisposableRegistrationBean implements CommonSecurityCache, InitializingBean, ISecurityCache {

	private static final Logger LOG = LoggerFactory.getLogger(SecurityCache.class);

	private static SecurityCache INSTANCE;

	//

	private int masterDataPermissionsMinSize;

	private int masterDataPermissionsMaxSize;

	private SpringDataBaseHelper dataBaseHelper;
	private PersistentDbAccess dbAccessPers;

	@Autowired
	private StateCache stateCache;

	@Autowired
	private XMLEntities xmlEntities;

	@Autowired
	private ServerParameterProvider parameterProvider;

	private MetaProvider metaProvider;

	private final Map<AttributePermissionKey, Permission> mpAttributePermission
		= new ConcurrentHashMap<>();

	private final Map<UID, String> mpUserName = new ConcurrentHashMap<>();

	private final Map<String, Map<UID, UserRights>> mpUserRights = new ConcurrentHashMap<>();

	private final Map<UserAttributeGroup, Map<UID, Permission>> mpAttributeGroups
		= new ConcurrentHashMap<>();

	private final Map<UserSubForm, Map<UID, SubformPermission>> mpSubForms = new ConcurrentHashMap<>();

	private boolean mapRoleNamesFilled = false;
	private final Map<UID, String> mapRoleNames
		= new ConcurrentHashMap<>();

	/**
	 * Sorted list of mandators
	 */
	private final List<MandatorVO> lstMandators = new ArrayList<>();
	/**
	 * Key: parentUID with
	 * UID.UID_NULL for the roots
	 */
	private final Map<UID, Set<UID>> mandatorChildren = new ConcurrentHashMap<UID, Set<UID>>();
	/**
	 * the accessible mandators for the given mandator
	 * 		accessible= all childrens + direct parents up to root
	 */
	private final Map<UID, Set<UID>> mandatorAccessible = new ConcurrentHashMap<UID, Set<UID>>();
	/**
	 * mandator by pk
	 */
	private final Map<UID, MandatorVO> mapMandators = new ConcurrentHashMap<UID, MandatorVO>();
	/**
	 * mandators by level
	 */
	private final Map<UID, List<MandatorVO>> mandatorsByLevel = new ConcurrentHashMap<UID, List<MandatorVO>>();
	/**
	 * level by pk
	 */
	private final Map<UID, MandatorLevelVO> mapLevels = new ConcurrentHashMap<UID, MandatorLevelVO>();
	/**
	 * Sorted list of mandator levels
	 */
	private final List<MandatorLevelVO> lstMandatorLevels = new ArrayList<MandatorLevelVO>();

	private boolean mandatorsloaded = false;

	private Cache<String, SessionContext> sessionContextCache;
	private Cache<String, Map<String, Authentication>> fatClientContextCache;

	private final static int SSO_SECURITY_OBJECT_LIFETIME_IN_SEC = 120;
	private LoadingCache<String, NuclosSsoSecurityObject> nuclosSsoSecurityObjectCache;

	private boolean bSsoAuthsFilled = false;
	private final ConcurrentMap<UID, SsoAuth> mapSsoAuths = new ConcurrentHashMap<>();

	private final TransactionSynchronization invalidate = new TransactionSynchronizationAdapter() {
		@Override
		public synchronized void afterCommit() {
			LOG.info("afterCommit: Invalidating security cache...");

			mpUserRights.clear();
			mpAttributeGroups.clear();
			mpSubForms.clear();
			mpAttributePermission.clear();
			fatClientContextCache.invalidateAll();
			clearMapRoleNames();
			bSsoAuthsFilled = false;
			mapSsoAuths.clear();
			synchronized (lstMandators) {
				lstMandators.clear();
				lstMandatorLevels.clear();
				mandatorChildren.clear();
				mandatorAccessible.clear();
				mapMandators.clear();
				mandatorsByLevel.clear();
				mapLevels.clear();
				mandatorsloaded = false;
				cleanDbCacheAsync();
			}
		}
	};

	@Override
	public void afterPropertiesSet() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		sessionContextCache = CacheBuilder.newBuilder()
				.concurrencyLevel(Runtime.getRuntime().availableProcessors())
				.expireAfterAccess(parameterProvider.getIntValue(
						ParameterProvider.SECURITY_SESSION_TIMEOUT_IN_SECONDS,
						NuclosWebApplicationInitializer.DEFAULT_SESSION_TIMEOUT
				), TimeUnit.SECONDS)
				.build();
		fatClientContextCache = CacheBuilder.newBuilder()
				.concurrencyLevel(Runtime.getRuntime().availableProcessors())
				.expireAfterAccess(parameterProvider.getIntValue(
						ParameterProvider.SECURITY_SESSION_TIMEOUT_IN_SECONDS,
						NuclosWebApplicationInitializer.DEFAULT_SESSION_TIMEOUT
				), TimeUnit.SECONDS)
				.build();
	}

	/**
	 * Determines whether the given entity is a dynamic task list and the given user is allowed to read it.
	 */
	public boolean isReadAllowedForDynamicTaskList(
			final UID entity,
			final String user,
			final UID mandator
	) {
		EntityMeta<Object> meta = metaProvider.getEntity(entity);
		return meta != null && meta.isDynamicTasklist()
				// TODO: Ugly String comparison here, because "entity" UID has a prefix for dynamic task lists,
				// but getDynamicTasklistDatasources returns only UIDs without prefix...
				&& getDynamicTasklistDatasources(user, mandator).stream().anyMatch(
						taskListUid -> entity.toString().endsWith(taskListUid.toString())
		);
	}

	public void initSsoSecurityObjectCache() {
		if (!RigidClusterHelper.isClusterEnabled()) {
			this.nuclosSsoSecurityObjectCache = CacheBuilder.newBuilder()
					.expireAfterWrite(SSO_SECURITY_OBJECT_LIFETIME_IN_SEC, TimeUnit.SECONDS)
					.build(new CacheLoader<String, NuclosSsoSecurityObject>() {
						@Override
						public NuclosSsoSecurityObject load(final String oAuth2State) throws Exception {
							return new NuclosSsoSecurityObject(oAuth2State);
						}
					});
		}
	}

	public State getNuclosSsoAuthState() {
		String jSessionId = SpringApplicationContextHolder.getBean(Session.class).getHttpSession().getId();
		if (jSessionId == null) {
			throw new IllegalStateException("No http session");
		}
		return new State((RigidClusterHelper.isClusterEnabled() ? RigidClusterHelper.getNodeId().getString() : "")
				+ jSessionId);
	}

	public NuclosSsoSecurityObject getNuclosSsoSecurityObject() {
		return getNuclosSsoSecurityObject(getNuclosSsoAuthState());
	}

	public NuclosSsoSecurityObject getNuclosSsoSecurityObject(final State oAuth2State) {
		if (oAuth2State == null) {
			throw new IllegalStateException("oAuth2State must not be null");
		}
		if (RigidClusterHelper.isClusterEnabled()) {
			return getFromOrInitClusterAuthStateTableFor(oAuth2State);
		} else {
			if (this.nuclosSsoSecurityObjectCache == null) {
				throw new IllegalStateException("Too early");
			}
			try {
				return this.nuclosSsoSecurityObjectCache.get(oAuth2State.getValue());
			} catch (ExecutionException e) {
				throw new NuclosFatalException(e);
			}
		}
	}

	private NuclosSsoSecurityObject getFromOrInitClusterAuthStateTableFor(final State oAuth2State) {
		if (oAuth2State == null) {
			throw new IllegalArgumentException("oAuth2State must not be null");
		}
		if (this.dbAccessPers == null) {
			throw new IllegalStateException("Too early");
		}

		DbQueryBuilder builder = dbAccessPers.getQueryBuilder();
		final DbQuery<Long> qCount = builder.createQuery(Long.class);
		final DbFrom<Long> fromCount = qCount.from(E.CLUSTER_AUTHSTATE);
		qCount.select(builder.countRows());
		qCount.where(builder.equalValue(fromCount.baseColumn(E.CLUSTER_AUTHSTATE.state), oAuth2State.getValue()));
		final Long iRows = dbAccessPers.executeQuerySingleResult(qCount);
		final boolean bInsert = iRows == 0;

		if (bInsert) {
			NuclosSsoSecurityObject nsso = new NuclosSsoSecurityObject(oAuth2State.getValue());
			byte[] content = SerializationUtils.serialize(nsso);
			DbMap valuesMap = new DbMap();
			valuesMap.put(SF.PK_ID, new DbId());
			valuesMap.put(SF.VERSION, 1);
			valuesMap.put(SF.CREATEDBY, "System");
			valuesMap.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
			valuesMap.put(SF.CHANGEDBY, "System");
			valuesMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
			valuesMap.put(E.CLUSTER_AUTHSTATE.state, oAuth2State.getValue());
			valuesMap.put(E.CLUSTER_AUTHSTATE.content, content);
			dbAccessPers.execute(new DbInsertStatement(E.CLUSTER_AUTHSTATE, valuesMap));
			return nsso;
		} else {
			final DbQuery<DbTuple> qGet = builder.createTupleQuery();
			final DbFrom<Long> fromGet = qGet.from(E.CLUSTER_AUTHSTATE);
			qGet.multiselect(
					fromGet.baseColumn(SF.CHANGEDAT),
					fromGet.baseColumn(E.CLUSTER_AUTHSTATE.content));
			qGet.where(builder.equalValue(fromGet.baseColumn(E.CLUSTER_AUTHSTATE.state), oAuth2State.getValue()));
			DbTuple row = dbAccessPers.executeQuerySingleResult(qGet);
			InternalTimestamp changedAt = row.get(SF.CHANGEDAT);
			if (changedAt.before(new Date(System.currentTimeMillis() - (SSO_SECURITY_OBJECT_LIFETIME_IN_SEC * 1000)))) {
				// ignore too old cached value
				NuclosSsoSecurityObject nsso = new NuclosSsoSecurityObject(oAuth2State.getValue());
				updateClusterAuthStateTableFor(nsso);
				return nsso;
			} else {
				byte[] content = row.get(E.CLUSTER_AUTHSTATE.content);
				return SerializationUtils.deserialize(content);
			}
		}
	}

	private int updateClusterAuthStateTableFor(final NuclosSsoSecurityObject nsso) {
		if (nsso == null) {
			throw new IllegalArgumentException("NuclosSsoSecurityObject must not be null");
		}
		if (this.dbAccessPers == null) {
			throw new IllegalStateException("Too early");
		}

		byte[] content = SerializationUtils.serialize(nsso);
		DbMap valuesMap = new DbMap();
		valuesMap.put(SF.CHANGEDBY, "System");
		valuesMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		valuesMap.put(E.CLUSTER_AUTHSTATE.content, content);
		valuesMap.put(SF.VERSION, DbIncrement.INCREMENT);
		DbMap condition = new DbMap();
		condition.put(E.CLUSTER_AUTHSTATE.state, nsso.getOAuth2State().getValue());
		return dbAccessPers.execute(new DbUpdateStatement(E.CLUSTER_AUTHSTATE, valuesMap, condition));
	}

	public NuclosSsoSecurityObject setNuclosSsoAuthStatusInitiated(final SsoAuth ssoAuth) {
		return this.setNuclosSsoAuthStatus(getNuclosSsoAuthState(), SsoAuthStatus.INITIATED, ssoAuth);
	}

	private NuclosSsoSecurityObject setNuclosSsoAuthStatus(final State oAuth2State, final SsoAuthStatus nuclosSsoAuthStatus, final SsoAuth ssoAuth) {
		if (oAuth2State == null) {
			throw new IllegalStateException("oAuth2State must not be null");
		}
		try {
			final NuclosSsoSecurityObject nsso;
			if (RigidClusterHelper.isClusterEnabled()) {
				nsso = this.getFromOrInitClusterAuthStateTableFor(oAuth2State);
			} else {
				if (this.nuclosSsoSecurityObjectCache == null) {
					throw new IllegalStateException("Too early");
				}
				nsso = this.nuclosSsoSecurityObjectCache.get(oAuth2State.getValue());
			}
			final NuclosSsoSecurityObject updatedNsso = new NuclosSsoSecurityObject(nsso, nuclosSsoAuthStatus, ssoAuth.getId());
			if (RigidClusterHelper.isClusterEnabled()) {
				this.updateClusterAuthStateTableFor(updatedNsso);
			} else {
				this.nuclosSsoSecurityObjectCache.put(oAuth2State.getValue(), updatedNsso);
			}
			return updatedNsso;
		} catch (ExecutionException e) {
			throw new NuclosFatalException(e);
		}
	}

	public void setNuclosSsoSecurityObjectStatus(final NuclosSsoSecurityObject nsso, final SsoAuthStatus status) {
		if (nsso == null) {
			throw new IllegalArgumentException("NuclosSsoSecurityObject must not be null");
		}

		final NuclosSsoSecurityObject updatedNsso = new NuclosSsoSecurityObject(nsso, status);
		if (RigidClusterHelper.isClusterEnabled()) {
			this.updateClusterAuthStateTableFor(updatedNsso);
		} else {
			this.nuclosSsoSecurityObjectCache.put(updatedNsso.getOAuth2State().getValue(), updatedNsso);
		}
	}

	public void setNuclosSsoSpringAuthentication(final State oAuth2State, final Authentication auth) {
		if (oAuth2State == null) {
			throw new IllegalArgumentException("oAuth2State must not be null");
		}
		try {
			final NuclosSsoSecurityObject nsso;
			if (RigidClusterHelper.isClusterEnabled()) {
				nsso = this.getFromOrInitClusterAuthStateTableFor(oAuth2State);
			} else {
				nsso = this.nuclosSsoSecurityObjectCache.get(oAuth2State.getValue());
			}
			final NuclosSsoSecurityObject updatedNsso = new NuclosSsoSecurityObject(nsso, SsoAuthStatus.LOGGED_IN, auth);
			if (RigidClusterHelper.isClusterEnabled()) {
				this.updateClusterAuthStateTableFor(updatedNsso);
			} else {
				this.nuclosSsoSecurityObjectCache.put(oAuth2State.getValue(), updatedNsso);
			}
		} catch (ExecutionException e) {
			throw new NuclosFatalException(e);
		}
	}

	public Authentication getNuclosSsoAuthentication() {
		return getNuclosSsoSecurityObject().getSpringAuthentication();
	}

	public Session getSession() {
		return SpringApplicationContextHolder.getBean(Session.class);
	}

    public String getSessionId() {
        return Optional.ofNullable(RequestContextHolder.getRequestAttributes())
                .map(RequestAttributes::getSessionId)
                .orElse("");
    }

	public static class NuclosSsoSecurityObject implements Serializable {
		private static final long serialVersionUID = 7482016013195352568L;

		private final SsoAuthStatus nuclosAuthStatus;
		private final State oAuth2State;
		private final Nonce oidcNonce;
		private final Authentication springAuth;
		private final CodeVerifier codeVerifier;
		private final UID ssoAuthUID;

		private NuclosSsoSecurityObject(String oAuth2State) {
			this.oAuth2State = new State(oAuth2State);
			this.oidcNonce = new Nonce();
			this.nuclosAuthStatus = SsoAuthStatus.UNKNOWN;
			this.springAuth = null;
			this.codeVerifier = null;
			this.ssoAuthUID = null;
		}

		private NuclosSsoSecurityObject(final NuclosSsoSecurityObject originNsso, final SsoAuthStatus nuclosAuthStatus) {
			this(originNsso, nuclosAuthStatus, originNsso.getSsoAuthUID(), null);
		}

		private NuclosSsoSecurityObject(final NuclosSsoSecurityObject originNsso, final SsoAuthStatus nuclosAuthStatus, final UID ssoAuthUID) {
			this(originNsso, nuclosAuthStatus, ssoAuthUID, null);
		}

		private NuclosSsoSecurityObject(final NuclosSsoSecurityObject originNsso, final SsoAuthStatus nuclosAuthStatus, final Authentication springAuth) {
			this(originNsso, nuclosAuthStatus, originNsso.getSsoAuthUID(), springAuth);
		}

		private NuclosSsoSecurityObject(final NuclosSsoSecurityObject originNsso, final SsoAuthStatus nuclosAuthStatus, final UID ssoAuthUID, final Authentication springAuth) {
			this.oAuth2State = originNsso.oAuth2State;
			this.oidcNonce = originNsso.oidcNonce;
			if (nuclosAuthStatus == SsoAuthStatus.INITIATED) {
				// Generate a new random 256 bit code verifier for PKCE
				this.codeVerifier = new CodeVerifier();
			} else {
				this.codeVerifier = null;
			}
			this.nuclosAuthStatus = nuclosAuthStatus;
			this.ssoAuthUID = ssoAuthUID;
			this.springAuth = springAuth;
		}

		public SsoAuthStatus getNuclosSsoAuthStatus() {
			return nuclosAuthStatus;
		}

		public State getOAuth2State() {
			return oAuth2State;
		}

		public Nonce getOidcNonce() {
			return oidcNonce;
		}

		public Authentication getSpringAuthentication() {
			return springAuth;
		}

		public CodeVerifier getCodeVerifier() {
			return codeVerifier;
		}

		public UID getSsoAuthUID() {
			return ssoAuthUID;
		}
	}

	private class UserRights {

		private final String sUserName;
		private final UID mandator;

		private ModulePermissions modulepermissions;
		private MasterDataPermissions masterdatapermissions;
		private Collection<UID> collTransitionUids;
		private Map<ReportType,Collection<UID>> result;
		private Collection<UID> collWritableReportUids;
		private Map<UID, String> collAllowedCustomRestRules;
		private Collection<UID> collAssignedGeneratorUids;
		private Collection<UID> collAssignedRecordgrantUids;
		private Collection<UID> collReadableDataSourceUids;
		private Collection<UID> collWritableDataSourceUids;
		private Collection<CompulsorySearchFilter> collCompulsorySearchFilters;
		private Set<String> actions;
		private Set<UID> customActions;
		private Set<UID> roleUids;
		private Set<UID> dynamicTasklistUids;
		private Set<UID> dynamicTasklistDatasourceUids;
		private Set<UID> mandatorUids;

		private UID userUid;
		private Boolean isSuperUser;
		private Boolean isUserAdmin;
        private UserLoginRestriction loginRestriction;
		private UID communicationAccountPhone = UID.UID_NULL;

		UserRights(String sUserName, UID mandator) {
			this.sUserName = sUserName;
			this.mandator = mandator;
		}

		public synchronized boolean isSuperUser() {
			if (isSuperUser == null) {
				readUserData();
			}
			return isSuperUser;
		}

        public synchronized boolean isLoginRestricted(UserLoginRestriction restriction) {
            if (loginRestriction == null) {
                readUserData();
            }

            if (restriction == null) {
                return loginRestriction == null;
            } else {
                return restriction.equals(loginRestriction);
            }
        }

		public synchronized boolean isUserAdmin() {
			if (isUserAdmin == null) {
				readUserData();
			}
			return isUserAdmin;
		}

		private synchronized UID getUserUid() {
			if (userUid == null) {
				readUserData();
			}
			return userUid;
		}

		public synchronized UID getCommunicationAccountPhone() {
			if (UID.UID_NULL.equals(communicationAccountPhone)) {
				readUserData();
			}
			return communicationAccountPhone;
		}

		public synchronized Set<String> getAllowedActions() {
			if (actions == null) {
				actions = readActions();
			}
			return actions;
		}

		public synchronized Set<UID> getAllowedCustomActions() {
			if (customActions == null) {
				customActions = readCustomActions();
			}
			return customActions;
		}

		public synchronized ModulePermissions getModulePermissions() {
			if (modulepermissions == null) {
				modulepermissions = readModulePermissions();
			}
			return modulepermissions;
		}

		public synchronized MasterDataPermissions getMasterDataPermissions() {
			if (masterdatapermissions == null) {
				masterdatapermissions = readMasterDataPermissions();
				final int newSize = masterdatapermissions.size();
				masterDataPermissionsMaxSize = Math.max(masterDataPermissionsMaxSize, newSize);
				if (masterDataPermissionsMinSize <= 0) {
					masterDataPermissionsMinSize = newSize;
				} else {
					masterDataPermissionsMinSize = Math.min(masterDataPermissionsMinSize, newSize);
				}
			}
			return masterdatapermissions;
		}

		public synchronized Collection<UID> getTransitionUids() {
			if (collTransitionUids == null) {
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<UID> query = builder.createQuery(UID.class);

				if (isSuperUser()) {
					DbFrom t = query.from(E.STATETRANSITION);
					query.select(t.baseColumn(E.STATETRANSITION.getPk()));
				} else {
					DbFrom t = query.from(E.ROLETRANSITION);
					query.select(t.baseColumn(E.ROLETRANSITION.transition));
					query.where(t.baseColumn(E.ROLETRANSITION.role).in(getRoleUids()));
				}

				collTransitionUids = dataBaseHelper.getDbAccess().executeQuery(query.distinct(true));
			}
			return collTransitionUids;
		}

		public synchronized Map<ReportType,Collection<UID>> getReadableReports() {
			if (result == null) {
				result = readReports(false);
			}
			return result;
		}

		public synchronized Collection<UID> getWritableReportUids() {
			// @todo refactor: It doesn't make sense to calculate the writable and readable reports in two independent queries.
			if (collWritableReportUids == null) {
				Map<ReportType, Collection<UID>> reports = readReports(true);
				collWritableReportUids = CollectionUtils.concatAll(reports.values());
			}
			return collWritableReportUids;
		}

		public synchronized Map<UID, String> getAllowedCustomRestRules() {
			if (collAllowedCustomRestRules == null) {
				collAllowedCustomRestRules = readCustomRestRules();
			}
			return collAllowedCustomRestRules;
		}

		public synchronized Collection<UID> getAssignedGeneratorUids() {
			if (collAssignedGeneratorUids == null) {
				collAssignedGeneratorUids = readGenerations();
			}
			return collAssignedGeneratorUids;
		}

		public synchronized Collection<UID> getAssignedRecordgrantUids() {
			if (collAssignedRecordgrantUids == null) {
				collAssignedRecordgrantUids = readRecordgrants();
			}
			return collAssignedRecordgrantUids;
		}

		private synchronized Map<ReportType,Collection<UID>> readReports(boolean readWrite) {
			Map<ReportType,Collection<UID>> result = new HashMap<ReportType,Collection<UID>>();

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.REPORT);
			query.multiselect(t.baseColumn(E.REPORT.getPk()), t.baseColumn(E.REPORT.type));
			if (!isSuperUser()) {
				DbFrom rr = t.join(E.ROLEREPORT, JoinType.LEFT, "rr").on(E.REPORT.getPk(), E.ROLEREPORT.report);
				DbColumnExpression<UID> rrc = rr.baseColumn(E.ROLEREPORT.role);
				DbCondition readWriteCond = readWrite
					? builder.equalValue(rr.baseColumn(E.ROLEREPORT.readwrite), true)
					: builder.alwaysTrue();
				query.where(builder.or(
					getNameCondition(query, t, SF.CREATEDBY),
					builder.and(rrc.in(getRoleUids()), readWriteCond)));
			}

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
				UID uid = tuple.get(0, UID.class);
				ReportType type = KeyEnum.Utils.findEnum(ReportType.class, tuple.get(1, Integer.class));
				if (result.containsKey(type))
					result.get(type).add(uid);
				else {
					Collection<UID> uids = new HashSet<UID>();
					uids.add(uid);
					result.put(type, uids);
				}
			}

			return result;
		}

		private synchronized Map<UID, String> readCustomRestRules() {
			Map<UID, String> result = new HashMap<>();

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.SERVERCODE);
			query.multiselect(t.baseColumn(E.SERVERCODE.getPk()), t.baseColumn(E.SERVERCODE.name));
			DbFrom rr = t.join(E.ROLESERVERCODE, JoinType.LEFT, "rr").on(E.SERVERCODE.getPk(), E.ROLESERVERCODE.servercode);
			DbColumnExpression<UID> rrc = rr.baseColumn(E.ROLESERVERCODE.role);

			if (!isSuperUser()) {
				query.where(rrc.in(getRoleUids()));
			} else {
				// criteria to distinct CustomRestRules from other servercode rules
				query.where(rrc.isNotNull());
			}

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
				UID uid = tuple.get(0, UID.class);
				String name = tuple.get(1, String.class);
				result.put(uid, name);
			}

			return result;
		}

		private synchronized Collection<UID> readGenerations() {
			Collection<UID> result = new ArrayList<UID>();

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.GENERATION);
			query.multiselect(t.baseColumn(E.GENERATION.getPk()), t.baseColumn(E.GENERATION.name));
			if (!isSuperUser()) {
				DbFrom rr = t.join(E.ROLEGENERATION, JoinType.LEFT, "rr").on(E.GENERATION.getPk(), E.ROLEGENERATION.generation);
				DbColumnExpression<UID> rrc = rr.baseColumn(E.ROLEGENERATION.role);
				query.where(
					rrc.in(getRoleUids()));
			}

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
				UID uid = tuple.get(0, UID.class);
				result.add(uid);
			}

			return result;
		}

		private synchronized Collection<UID> readRecordgrants() {
			Collection<UID> result = new ArrayList<UID>();

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.RECORDGRANT);
			query.multiselect(t.baseColumn(E.RECORDGRANT.getPk()), t.baseColumn(E.RECORDGRANT.name));
			if (!isSuperUser()) {
				DbFrom rr = t.join(E.ROLERECORDGRANT, JoinType.LEFT, "rr").on(E.RECORDGRANT.getPk(), E.ROLERECORDGRANT.recordgrant);
				DbColumnExpression<UID> rrc = rr.baseColumn(E.ROLERECORDGRANT.role);
				query.where(
					rrc.in(getRoleUids()));
			}

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
				UID uid = tuple.get(0, UID.class);
				result.add(uid);
			}

			return result;
		}

		public synchronized Collection<UID> getReadableDataSourceUids() {
			if (collReadableDataSourceUids == null) {
				collReadableDataSourceUids = readDataSourceUids(false);
			}
			return collReadableDataSourceUids;
		}

		public synchronized Collection<UID> getWritableDataSourceUids() {
			if (collWritableDataSourceUids == null) {
				collWritableDataSourceUids = readDataSourceUids(true);
			}
			return collWritableDataSourceUids;
		}

		public synchronized Set<UID> getCompulsorySearchFilterUids(UID entityUID) {
			if (collCompulsorySearchFilters == null) {
				collCompulsorySearchFilters = readCompulsorySearchFilterUids();
			}
			Date now = DateUtils.getPureDate(DateUtils.now());
			Set<UID> result = new HashSet<UID>();
			for (CompulsorySearchFilter f : collCompulsorySearchFilters) {
				if (f.entityUID.equals(entityUID) && f.isValid(now))
					result.add(f.uid);
			}
			return result;
		}

		private synchronized Collection<UID> readDataSourceUids(boolean readWrite) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom t = query.from(E.DATASOURCE);
			query.select(t.baseColumn(E.DATASOURCE.getPk()));

			if (!isSuperUser()) {
				DbFrom ro = t.join(E.REPORTOUTPUT, JoinType.LEFT, "ro").on(E.DATASOURCE.getPk(), E.REPORTOUTPUT.datasource);
				List<UID> reportUids = CollectionUtils.concatAll(readReports(readWrite).values());
				query.where(builder.or(
					getNameCondition(query, t, SF.CREATEDBY),
					ro.baseColumn(E.REPORTOUTPUT.parent).in(reportUids)));
			}

			return dataBaseHelper.getDbAccess().executeQuery(query.distinct(true));
		}

		private DbCondition getNameCondition(DbQuery<?> q, DbFrom t, DbField<String> usercolumn) {
			DbQueryBuilder builder = q.getBuilder();
			return builder.equal(builder.upper(t.baseColumn(usercolumn)), builder.upper(builder.literal(sUserName)));
		}

		public synchronized Set<UID> getRoleUids() {
			if (roleUids == null) {
				roleUids = readUserRoleUids();
			}
			return roleUids;
		}

		public synchronized Set<UID> getDynamicTasklistUids() {
			if (dynamicTasklistUids == null) {
				dynamicTasklistUids = readDynamicTasklistUids();
			}
			return dynamicTasklistUids;
		}

		public synchronized Set<UID> getDynamicTasklistDatasourceUids() {
			if (dynamicTasklistDatasourceUids == null) {
				dynamicTasklistDatasourceUids = readDynamicTasklistDatasourceUids();
			}
			return dynamicTasklistDatasourceUids;
		}

		private ModulePermissions readModulePermissions() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

			final Map<UID, ModulePermission> mpByModuleUid
				= new ConcurrentHashMap<>();
			final Map<UID, Boolean> mpNewAllowedByModuleUid
				= new ConcurrentHashMap<>();
			final Map<UID, Set<UID>> mpNewAllowedProcessesByModuleUid
				= new ConcurrentHashMap<>();
			final Map<UID, Boolean> mpExportByEntity = new HashMap<>();

			if (isSuperUser()) {
				DbQuery<UID> query = builder.createQuery(UID.class);
				DbFrom t = query.from(E.ENTITY);
				query.select(t.baseColumn(E.ENTITY));
				query.where(builder.equalValue(t.baseColumn(E.ENTITY.usesstatemodel), true));
				for (UID entityUID : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
					ModulePermission permission = ModulePermission.DELETE_PHYSICALLY;

					mpByModuleUid.put(entityUID, permission);
					mpNewAllowedByModuleUid.put(entityUID, Boolean.TRUE);

					DbQuery<UID> processQuery = builder.createQuery(UID.class);
					DbFrom p = processQuery.from(E.PROCESS);
					processQuery.select(p.baseColumn(E.PROCESS));
					processQuery.where(builder.equalValue(p.baseColumn(E.PROCESS.module), entityUID));
					List<UID> allProcesses = dataBaseHelper.getDbAccess().executeQuery(processQuery);

					Set<UID> newAllowedProcesses = new HashSet<UID>();
					for (UID stateModelUID : stateCache.getAllModelUsagesObject().getStateModelUIDsByEntity(entityUID)) {
						StateTransitionVO initialTransitionVO = null;
						try {
							initialTransitionVO = stateCache.getInitialTransitionByModelId(stateModelUID);
						} catch (CommonFinderException e) {
							throw new CommonFatalException(e);
						}
						if (stateModelUID != null && initialTransitionVO != null) {
							for (UsageCriteria uc : stateCache.getAllModelUsagesObject().getUsageCriteriasByStateModelUID(stateModelUID)) {
								newAllowedProcesses.add(uc.getProcessUID());
								if (uc.getProcessUID() == null) {
									newAllowedProcesses.addAll(allProcesses);
								}
							}
						}
					}
					mpNewAllowedProcessesByModuleUid.put(entityUID, newAllowedProcesses);
					mpExportByEntity.put(entityUID, true);
				}
			} else {
				DbQuery<DbTuple> query = builder.createTupleQuery();
				DbFrom m = query.from(E.ENTITY, "m");
				DbFrom re = m.join(E.ROLEMODULE, JoinType.INNER, E.ROLEMODULE.module, "re");
				query.multiselect(
					m.baseColumn(E.ENTITY),
					re.baseColumn(E.ROLEMODULE.modulepermission),
					re.baseColumn(E.ROLEMODULE.export));
				query.where(builder.and(
					re.baseColumn(E.ROLEMODULE.role).in(getRoleUids()),
					builder.equalValue(m.baseColumn(E.ENTITY.usesstatemodel), true)));
				for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
					UID entityUID = tuple.get(0, UID.class);
					Integer ipermission = tuple.get(1, Integer.class);
					Boolean export = tuple.get(2, Boolean.class);
					ModulePermission permission = ModulePermission.getInstance(ipermission);
					permission = permission.max(mpByModuleUid.get(entityUID));

					mpByModuleUid.put(entityUID, permission);
					mpExportByEntity.put(entityUID, Boolean.TRUE.equals(mpExportByEntity.get(entityUID)) || export);

					/** is new allowed is defined in initial state model. */
					if (!mpNewAllowedByModuleUid.containsKey(entityUID)) {
						StateModelUsageVO smu = stateCache.getAllModelUsagesObject().getStateModelUsage(new UsageCriteria(entityUID, null, null, null));
						Boolean isNewAllowed = false;
						if (smu != null) {
							UID stateModelUid = smu.getStateModelUID();
							StateTransitionVO initialTransitionVO = null;
							try {
								initialTransitionVO = stateModelUid!=null? stateCache.getInitialTransitionByModelId(stateModelUid) : null;
							} catch (CommonFinderException e) {
								throw new CommonFatalException(e);
							}
							isNewAllowed = stateModelUid != null && initialTransitionVO != null &&
								!CollectionUtils.intersection(initialTransitionVO.getRoleUIDs(), getRoleUids()).isEmpty();
						}
						mpNewAllowedByModuleUid.put(entityUID, isNewAllowed);
					}

					DbQuery<UID> processQuery = builder.createQuery(UID.class);
					DbFrom p = processQuery.from(E.PROCESS);
					processQuery.select(p.baseColumn(E.PROCESS));
					processQuery.where(builder.equalValue(p.baseColumn(E.PROCESS.module), entityUID));
					List<UID> allProcesses = dataBaseHelper.getDbAccess().executeQuery(processQuery);

					boolean isNewAllowedForUndefinedProcess = false;

					/** is new process allowed is defined in state model. */
					if (!mpNewAllowedProcessesByModuleUid.containsKey(entityUID)) {
						Set<UID> newAllowedProcesses = new HashSet<UID>();

						for (UID stateModelUid : stateCache.getAllModelUsagesObject().getStateModelUIDsByEntity(entityUID)) {
							StateTransitionVO initialTransitionVO = null;
							try {
								initialTransitionVO = stateCache.getInitialTransitionByModelId(stateModelUid);
							} catch (CommonFinderException e) {
								throw new CommonFatalException(e);
							}
							final Boolean isNewAllowed = stateModelUid != null && initialTransitionVO != null &&
								!CollectionUtils.intersection(initialTransitionVO.getRoleUIDs(), getRoleUids()).isEmpty();
							if (isNewAllowed) {
								for (UsageCriteria uc : stateCache.getAllModelUsagesObject().getUsageCriteriasByStateModelUID(stateModelUid)) {
									newAllowedProcesses.add(uc.getProcessUID());
									if (uc.getProcessUID() == null) {
										isNewAllowedForUndefinedProcess = true;
									}
								}
							}
							else {
								for (UsageCriteria uc : stateCache.getAllModelUsagesObject().getUsageCriteriasByStateModelUID(stateModelUid)) {
									if (uc.getProcessUID() != null) {
										allProcesses.remove(uc.getProcessUID());
									}
								}
							}
						}

						if (isNewAllowedForUndefinedProcess) {
							newAllowedProcesses.addAll(allProcesses);
						}

						mpNewAllowedProcessesByModuleUid.put(entityUID, newAllowedProcesses);
					}
				}
			}

			return new ModulePermissions(mpByModuleUid, mpNewAllowedByModuleUid, mpNewAllowedProcessesByModuleUid, mpExportByEntity);
		}

		private MasterDataPermissions readMasterDataPermissions() {
			final Map<UID, MasterDataPermission> mpByEntityUid = new HashMap<UID, MasterDataPermission>();
			final Map<UID, Boolean> mpExportByEntity = new HashMap<>();

			if (isSuperUser()) {
				for (EntityMeta metaVO : metaProvider.getAllEntities()) {
					mpByEntityUid.put(metaVO.getUID(), MasterDataPermission.DELETE);
					mpExportByEntity.put(metaVO.getUID(), true);
				}
			} else {
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<DbTuple> query = builder.createTupleQuery();
				DbFrom t = query.from(E.ROLEMASTERDATA);
				query.multiselect(t.baseColumn(E.ROLEMASTERDATA.entity), t.baseColumn(E.ROLEMASTERDATA.masterdatapermission),
						t.baseColumn(E.ROLEMASTERDATA.export));
				query.where(t.baseColumn(E.ROLEMASTERDATA.role).in(getRoleUids()));
				for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
					UID entityUID = tuple.get(0, UID.class);
					MasterDataPermission mp = MasterDataPermission.getInstance(tuple.get(1, Integer.class));
					Boolean export = tuple.get(2, Boolean.class);
					mpByEntityUid.put(entityUID, mp.max(mpByEntityUid.get(entityUID)));
					mpExportByEntity.put(entityUID, Boolean.TRUE.equals(mpExportByEntity.get(entityUID)) || export);
				}
				if (isUserAdmin()) {
					mpByEntityUid.put(E.USER.getUID(), MasterDataPermission.READWRITE);
				}
			}

			return new MasterDataPermissions(mpByEntityUid, mpExportByEntity);
		}

		private Set<UID> readUserRoleUids() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom t = query.from(E.ROLEUSER);
			query.select(t.baseColumn(E.ROLEUSER.role));
			query.where(builder.equalValue(t.baseColumn(E.ROLEUSER.user), getUserUid()));
			Set<UID> result = new HashSet<UID>(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true)));

			if (mandator != null) {
				builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				query = builder.createQuery(UID.class);
				t = query.from(E.MANDATOR_ROLE_USER);
				query.select(t.baseColumn(E.MANDATOR_ROLE_USER.role));
				DbJoin mu = t.joinOnJoinedPk(E.MANDATOR_USER, JoinType.INNER, E.MANDATOR_ROLE_USER.mandatorUser, "mu");
				query.where(builder.equalValue(mu.baseColumn(E.MANDATOR_USER.user), getUserUid()));
				query.addToWhereAsAnd(builder.equalValue(mu.baseColumn(E.MANDATOR_USER.mandator), mandator));
				result.addAll(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true)));
			}

			return Collections.unmodifiableSet(result);
		}

		private Set<UID> readDynamicTasklistUids() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom<UID> t1 = query.from(E.TASKLIST);
			DbJoin<?> t2 = t1.join(E.TASKLISTROLE, JoinType.INNER, "T2").on(E.TASKLIST.getPk(), E.TASKLISTROLE.tasklist);
			//DbJoin<?> t3 = t2.join(E.ROLEUSER, JoinType.INNER, "T3").on(E.TASKLISTROLE.role, E.ROLEUSER.role);
			query.select(t1.baseColumn(E.TASKLIST));
			//query.where(builder.and(builder.equalValue(t3.baseColumn(E.ROLEUSER.user), getUserUid()),
			query.where(builder.and(t2.baseColumn(E.TASKLISTROLE.role).in(getRoleUids()),
					builder.isNotNull(t1.baseColumn(E.TASKLIST.datasource))));
			return Collections.unmodifiableSet(new HashSet<UID>(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))));
		}

		private Set<UID> readDynamicTasklistDatasourceUids() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom<UID> t1 = query.from(E.TASKLIST);
			DbJoin<?> t2 = t1.join(E.TASKLISTROLE, JoinType.INNER, "T2").on(E.TASKLIST.getPk(), E.TASKLISTROLE.tasklist);
			//DbJoin<?> t3 = t2.join(E.ROLEUSER, JoinType.INNER, "T3").on(E.TASKLISTROLE.role, E.ROLEUSER.role);
			query.select(t1.baseColumn(E.TASKLIST.datasource));
			//query.where(builder.and(builder.equalValue(t3.baseColumn(E.ROLEUSER.user), getUserUid()),
			query.where(builder.and(t2.baseColumn(E.TASKLISTROLE.role).in(getRoleUids()),
					builder.isNotNull(t1.baseColumn(E.TASKLIST.datasource))));

			return Collections.unmodifiableSet(new HashSet<UID>(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))));
		}

		private Set<String> readActions() {
			if (isSuperUser()) {
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<String> query = builder.createQuery(String.class);
				DbFrom t = query.from(E.ACTION);
				query.select(t.baseColumn(E.ACTION.action));
				Set<String> actions = new HashSet<String>(dataBaseHelper.getDbAccess().executeQuery(query));

				for(EntityObjectVO<UID> mdvo : xmlEntities.getData(E.ACTION).getAll()) {
					actions.add(mdvo.getFieldValue(E.ACTION.action));
				}
				return Collections.unmodifiableSet(actions);
			}

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<String> query = builder.createQuery(String.class);
			DbFrom t = query.from(E.ROLEACTION);
			query.select(t.baseColumn(E.ROLEACTION.action));
			query.where(t.baseColumn(E.ROLEACTION.role).in(getRoleUids()));
			Set<String> actions = new HashSet<>(dataBaseHelper.getDbAccess().executeQuery(query));
			if (isUserAdmin()) {
				actions.add(Actions.ACTION_SYSTEMSTART);
				actions.add(Actions.ACTION_RESTAPI);
			}
			return Collections.unmodifiableSet(actions);
		}

		private Set<UID> readCustomActions() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom t = query.from(E.ROLEACTION);
			t.innerJoin(E.ACTION, "taction").on(E.ROLEACTION.action, E.ACTION.action);
			query.select(t.column("taction", E.ACTION.getPk()));
			query.where(t.baseColumn(E.ROLEACTION.role).in(getRoleUids()));
			return Collections.unmodifiableSet(new HashSet<UID>(dataBaseHelper.getDbAccess().executeQuery(query)));
		}

		public synchronized Map<UID, SubformPermission> readSubFormPermissions(UID entityUID) {
			final Map<UID, SubformPermission> result = new HashMap<>();
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

			if (isSuperUser()) {
				DbQuery<UID> query = builder.createQuery(UID.class);
				DbFrom t = query.from(E.STATE);
				query.select(t.baseColumn(E.STATE));
				for (UID stateUID : dataBaseHelper.getDbAccess().executeQuery(query)) {
					result.put(stateUID, SubformPermission.ALL);
				}
				return result;
			}

			DbQuery<DbTuple> query = builder.createQuery(DbTuple.class);
			DbFrom t = query.from(E.ROLESUBFORM);
			query.multiselect(t.basePk(),
				t.baseColumn(E.ROLESUBFORM.state),
				t.baseColumn(E.ROLESUBFORM.create),
				t.baseColumn(E.ROLESUBFORM.delete));
			query.where(builder.and(
				t.baseColumn(E.ROLESUBFORM.role).in(getRoleUids()),
				builder.equalValue(t.baseColumn(E.ROLESUBFORM.entity), entityUID)));

			Map<UID, UID> mpRoleSf2StateUids = new HashMap<>();

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
				UID roleSfUID = tuple.get(0, UID.class);
				UID stateUID = tuple.get(1, UID.class);
				Boolean create = tuple.get(2, Boolean.class);
				Boolean delete = tuple.get(3, Boolean.class);

				mpRoleSf2StateUids.put(roleSfUID, stateUID);

				SubformPermission permission = new SubformPermission(create, delete);

				result.put(stateUID, permission.max(result.get(stateUID)));
			}

			query = builder.createQuery(DbTuple.class);
			t = query.from(E.ROLESUBFORMGROUP);
			query.multiselect(
					t.baseColumn(E.ROLESUBFORMGROUP.rolesubform),
					t.baseColumn(E.ROLESUBFORMGROUP.group),
					t.baseColumn(E.ROLESUBFORMGROUP.readwrite));
			query.where(t.baseColumn(E.ROLESUBFORMGROUP.rolesubform).in(mpRoleSf2StateUids.keySet()));

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
				UID roleSfUID = tuple.get(0, UID.class);
				UID groupUID = tuple.get(1, UID.class);
				Boolean readwrite = tuple.get(2, Boolean.class);

				Permission permission = Permission.get(readwrite);
				UID stateUID = mpRoleSf2StateUids.get(roleSfUID);

				SubformPermission subformPermission = result.get(stateUID);
				subformPermission.addGroupPermission(groupUID, permission);

			}

			return result;
		}

		public synchronized Map<UID, Permission> getAttributeGroupPermissions(UID attributeGroupUid) {
			final Map<UID, Permission> result = new HashMap<UID, Permission>();
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

			if (isSuperUser()) {
				DbQuery<UID> query = builder.createQuery(UID.class);
				DbFrom t = query.from(E.STATE);
				query.select(t.baseColumn(E.STATE));
				for (UID stateUID : dataBaseHelper.getDbAccess().executeQuery(query)) {
					result.put(stateUID, Permission.READWRITE);
				}
			} else {
				DbQuery<DbTuple> query = builder.createQuery(DbTuple.class);
				DbFrom t = query.from(E.ROLEATTRIBUTEGROUP);
				query.multiselect(
					t.baseColumn(E.ROLEATTRIBUTEGROUP.state),
					t.baseColumn(E.ROLEATTRIBUTEGROUP.readwrite));
				query.where(builder.and(
					t.baseColumn(E.ROLEATTRIBUTEGROUP.role).in(getRoleUids()),
					builder.equalValue(t.baseColumn(E.ROLEATTRIBUTEGROUP.attributegroup), attributeGroupUid)));
				for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
					UID stateUID = tuple.get(0, UID.class);
					Boolean readwrite = tuple.get(1, Boolean.class);
					Permission permission = Permission.get(readwrite);
					result.put(stateUID, permission.max(result.get(stateUID)));
				}
			}
			return result;
		}

		private Collection<CompulsorySearchFilter> readCompulsorySearchFilterUids() {
			if (isSuperUser())
				return Collections.emptySet();
			List<CompulsorySearchFilter> list = new ArrayList<CompulsorySearchFilter>();
			list.addAll(readCompulsorySearchFilterIdsImpl(
					E.SEARCHFILTERUSER, E.SEARCHFILTERUSER.searchfilter, E.SEARCHFILTERUSER.validFrom, E.SEARCHFILTERUSER.validUntil,
					E.SEARCHFILTERUSER.compulsoryFilter, E.SEARCHFILTERUSER.user, Collections.singleton(getUserUid())));
			list.addAll(readCompulsorySearchFilterIdsImpl(
					E.SEARCHFILTERROLE, E.SEARCHFILTERROLE.searchfilter, E.SEARCHFILTERROLE.validFrom, E.SEARCHFILTERROLE.validUntil,
					E.SEARCHFILTERROLE.compulsoryFilter, E.SEARCHFILTERROLE.role, getRoleUids()));
			return list;
		}

		private List<CompulsorySearchFilter> readCompulsorySearchFilterIdsImpl(EntityMeta<UID> subtable,
				FieldMeta<UID> refcolumn, FieldMeta<Date> fromcolumn, FieldMeta<Date> untilcolumn, FieldMeta<Boolean> compulcolumn, FieldMeta<UID> condcolumn,
				Collection<UID> conduids) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.SEARCHFILTER);
			DbFrom u = t.join(subtable, JoinType.INNER, refcolumn, "u");
			query.multiselect(
				t.baseColumn(E.SEARCHFILTER), t.baseColumn(E.SEARCHFILTER.entity),
				u.baseColumn(fromcolumn), u.baseColumn(untilcolumn));
			query.where(builder.and(
				builder.equalValue(u.baseColumn(compulcolumn), true),
				u.baseColumn(condcolumn).in(conduids)));
			query.distinct(true);
			return dataBaseHelper.getDbAccess().executeQuery(query, new Transformer<DbTuple, CompulsorySearchFilter>() {
				@Override
				public CompulsorySearchFilter transform(DbTuple t) {
					return new CompulsorySearchFilter(
						t.get(0, UID.class), t.get(1, UID.class), t.get(2, Date.class), t.get(3, Date.class));
				}
			});
		}

		private void readUserData() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.USER);
			query.multiselect(
				t.baseColumn(E.USER),
				t.baseColumn(E.USER.superuser),
				t.baseColumn(E.USER.useradmin),
				t.baseColumn(E.USER.communicationAccountPhone),
                t.baseColumn(E.USER.loginRestriction));
			query.where(getNameCondition(query, t, E.USER.username));
			DbTuple tuple = CollectionUtils.getFirst(dataBaseHelper.getDbAccess().executeQuery(query));
			if (tuple != null) {
				userUid = tuple.get(0, UID.class);
				isSuperUser = Boolean.TRUE.equals(tuple.get(1, Boolean.class));
				isUserAdmin = Boolean.TRUE.equals(tuple.get(2, Boolean.class));
				communicationAccountPhone = tuple.get(E.USER.communicationAccountPhone);
                loginRestriction = Optional.ofNullable(tuple.get(E.USER.loginRestriction))
                        .map(UserLoginRestriction::valueOf)
                        .orElse(null);
			} else {
				// No user found
				userUid = null;
				isSuperUser = false;
				isUserAdmin = false;
				communicationAccountPhone = null;
                loginRestriction = null;
			}
		}

		public synchronized Set<UID> getMandators() {
			if (mandatorUids == null) {
				mandatorUids = readMandators();
			}
			return mandatorUids;
		}

		private Set<UID> readMandators() {
			Transformer<MandatorVO, UID> toUidTransformer = new Transformer<MandatorVO, UID>() {
				@Override
				public UID transform(MandatorVO i) {
					return i.getUID();
				}
			};
			if (isSuperUser()) {
				return Collections.unmodifiableSet(CollectionUtils.transformIntoSet(getAllMandators(), toUidTransformer));
			} else {
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<UID> query = builder.createQuery(UID.class);
				DbFrom t = query.from(E.MANDATOR_USER);
				query.select(t.baseColumn(E.MANDATOR_USER.mandator));
				query.where(builder.equalValue(t.baseColumn(E.MANDATOR_USER.user), getUserUid()));
				Set<UID> result = new HashSet<UID>();
				for (UID mandatorUID : dataBaseHelper.getDbAccess().executeQuery(query)) {
					result.add(mandatorUID);
					addChildMandators(result, mandatorUID);
				}
				return Collections.unmodifiableSet(result);
			}
		}

		private void addChildMandators(Set<UID> result, UID parent) {
			for (UID child : getChildMandators(parent)) {
				result.add(child);
				addChildMandators(result, child);
			}
		}

		public Boolean canExport(UID entityUID) {
			if (getMasterDataPermissions() != null && masterdatapermissions.get(entityUID) != null) {
				return masterdatapermissions.isExport(entityUID);
			}
			return getModulePermissions() == null ? null : modulepermissions.isExport(entityUID);
		}
	}	// class UserRights


	/**
	 * defines a unique key to get the permission for an attributegroup
	 */
	private static class UserAttributeGroup {
		private String sUserName;
		private UID attributeGroupUID;
		private UID mandatorUID;

		UserAttributeGroup(String sUserName, UID attributeGroupUID, UID mandatorUID) {
			this.sUserName = sUserName;
			this.attributeGroupUID = attributeGroupUID;
			this.mandatorUID = mandatorUID;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || (this.getClass() != o.getClass())) {
				return false;
			}
			final UserAttributeGroup that = (UserAttributeGroup) o;
			return LangUtils.equal(this.sUserName, that.sUserName) && LangUtils.equal(this.attributeGroupUID, that.attributeGroupUID) && LangUtils.equal(this.mandatorUID, that.mandatorUID);
		}

		@Override
		public int hashCode() {
			return LangUtils.hashCode(this.sUserName) ^ LangUtils.hashCode(this.attributeGroupUID) ^ LangUtils.hash(this.mandatorUID);
		}

	}	// class UserAttributeGroup

	/**
	 * defines a unique key to get the permission for a subform used in an entity
	 */
	private static class UserSubForm {
		private String sUserName;
		private UID entityUID;
		private UID mandatorUID;

		UserSubForm(String sUserName, UID entityUID, UID mandatorUID) {
			this.sUserName = sUserName;
			this.entityUID = entityUID;
			this.mandatorUID = mandatorUID;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || (this.getClass() != o.getClass())) {
				return false;
			}
			final UserSubForm that = (UserSubForm) o;
			return LangUtils.equal(this.sUserName, that.sUserName) && LangUtils.equal(this.entityUID, that.entityUID) && LangUtils.equal(this.mandatorUID, that.mandatorUID);
		}

		@Override
		public int hashCode() {
			return LangUtils.hashCode(this.sUserName) ^ LangUtils.hashCode(this.entityUID) ^ LangUtils.hashCode(this.mandatorUID);
		}
	} // class UserSubForm

	private static class CompulsorySearchFilter {
		private UID uid;
		private UID entityUID;
		private Date validFrom;
		private Date validUntil;

		CompulsorySearchFilter(UID uid, UID entityUID, Date validFrom, Date validUntil) {
			this.entityUID = entityUID;
			this.uid = uid;
			this.validFrom = validFrom;
			this.validUntil = validUntil;
		}

		boolean isValid(Date date) {
			return (validFrom == null || validFrom.before(date))
				&& (validUntil == null || validUntil.after(date));
		}
	}

	public static SecurityCache getInstance() {
		return INSTANCE;
	}

	SecurityCache() {
		INSTANCE = this;
	}

	@PostConstruct
	private void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		createAsyncFlowableWithDropStrategyAndRegister(metaProvider.observeEntityMetaRefreshsAfterCommit(), entityMetas -> invalidate(false));
	}

	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
		this.dbAccessPers = new PersistentDbAccess(dataBaseHelper.getDbAccess());
	}

	@Autowired
	void setMetaProvider(MetaProvider metaProvider) {
		this.metaProvider = metaProvider;
	}

	public Set<String> getAllowedActions(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getAllowedActions();
	}

	public Set<UID> getAllowedCustomActions(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getAllowedCustomActions();
	}

	public ModulePermissions getModulePermissions(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getModulePermissions();
	}

	public MasterDataPermissions getMasterDataPermissions(String sUserName, UID mandator){
		return getUserRights(sUserName, mandator).getMasterDataPermissions();
	}

	@Override
	public boolean isActionAllowed(final String sUserName, final String action, UID mandator) {
		return getUserRights(sUserName, mandator).getAllowedActions().contains(action);
	}

	@Override
	public boolean isReadAllowedForEntity(String user, UID entityUID, UID mandator) {
		return isReadAllowedForMasterData(user, entityUID, mandator) || isReadAllowedForModule(user, entityUID, mandator);
	}

	public boolean isReadAllowedForModule(String sUserName, UID entityUID, UID mandator) {
		return getUserRights(sUserName, mandator).isSuperUser() || ModulePermission.includesReading(this.getModulePermissions(sUserName, mandator).getMaxPermissionForGenericObject(entityUID));
	}

	public boolean isReadAllowedForMasterData(String sUserName, UID entityUID, UID mandator) {
		if (E.REPORTEXECUTION.checkEntityUID(entityUID)) {
			return getUserRights(sUserName, mandator).getAllowedActions().contains(Actions.ACTION_EXECUTE_REPORTS);
		}
		return getUserRights(sUserName, null).isSuperUser()
				|| MasterDataPermission.includesReading(this.getMasterDataPermissions(sUserName, mandator).get(entityUID));
	}

	public boolean isNewAllowedForModule(String sUserName, UID entityUID, UID mandator) {
		Boolean bNewAllowed = getModulePermissions(sUserName, mandator).getNewAllowedByModuleUid().get(entityUID);
		return bNewAllowed != null ? bNewAllowed : false;
	}

	public boolean isWriteAllowedForModule(String sUserName, UID entityUID, UID mandator) {
		return ModulePermission.includesWriting(
				getModulePermissions(sUserName, mandator).getMaxPermissionForGenericObject(entityUID));
	}

	public boolean isWriteAllowedForMasterData(String sUserName, UID entityUID, UID mandator) {
		return MasterDataPermission.includesWriting(getMasterDataPermissions(sUserName, mandator).get(entityUID));
	}

	public boolean isDeleteAllowedForModule(String sUserName, UID entityUID, boolean bPhysically, UID mandator) {
		if (getUserRights(sUserName, mandator).isSuperUser())
			return true;
		final ModulePermission modulepermission = getModulePermissions(sUserName, mandator).getMaxPermissionForGenericObject(entityUID);
		return bPhysically ?
				ModulePermission.includesDeletingPhysically(modulepermission) :
				ModulePermission.includesDeletingLogically(modulepermission);

	}

	public boolean isDeleteAllowedForMasterData(String sUserName, UID entityUID, UID mandator) {
		return getUserRights(sUserName, mandator).isSuperUser()
				|| MasterDataPermission.includesDeleting(getMasterDataPermissions(sUserName, mandator).get(entityUID));
	}

	public Boolean canExportForEntity(String sUserName, UID entityUID, UID mandator) {
		return getUserRights(sUserName, mandator).canExport(entityUID);
	}

	public boolean isSuperUser(String sUserName) {
		return getUserRights(sUserName, null).isSuperUser();
	}

    public boolean isUserLoginRestricted(String sUserName, UserLoginRestriction loginRestriction) {
        return getUserRights(sUserName, null).isLoginRestricted(loginRestriction);
    }

	public boolean isUserAdmin(String sUserName) {
		return getUserRights(sUserName, null).isUserAdmin();
	}

	public boolean isMaintenanceUser(String sUserName) {
		return getAllowedActions(sUserName, null).contains(Actions.ACTION_MAINTENANCE_MODE);
	}

	public boolean isNucletExportUser(String sUserName) {
		return getAllowedActions(sUserName, null).contains(Actions.ACTION_NUCLET_EXPORT);
	}

	public boolean isNucletImportUser(String sUserName) {
		return getAllowedActions(sUserName, null).contains(Actions.ACTION_NUCLET_IMPORT);
	}

	public Boolean isNucletAssignUser(String sUserName) {
		return getAllowedActions(sUserName, null).contains(Actions.ACTION_NUCLET_ASSIGN);
	}

	/**
	 * @param sUserName
	 * @return Set&lt;UID&gt; the ids of all modules
	 * 		that can be read by the user with the given name.
	 */
	public Set<UID> getReadableModuleUids(String sUserName, UID mandator) {
		final Set<UID> result = new HashSet<UID>();
		for (Entry<UID, ModulePermission> userRights : this.getModulePermissions(sUserName, mandator).getEntries()) {
			if (ModulePermission.includesReading(userRights.getValue())) {
				result.add(userRights.getKey());
			}
		}
		return result;
	}

	public Collection<UID> getTransitionUids(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getTransitionUids();
	}

	/**
	 * Get the ids of all reports/forms the user has the right to read.
	 * Read rights are given when the user
	 * - has created the report/form himself or
	 * - has access to it via the roles he belongs to.
	 *
	 * @param sUserName
	 * @return Map&lt;ReportType,Collection&lt;UID&gt;&gt; contains a map of all reports/forms
	 * 		that may be read by the given user.
	 */
	public Map<ReportType,Collection<UID>> getReadableReports(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getReadableReports();
	}

	/**
	 * Get the ids of all custom rest rules the user is allowed to execute.
	 * - has access to it via the roles he belongs to. Superuser gets any rule that has an assignment to roles.
	 * If a rule isn't assign to a rule, it won't be delivered for anybody by this methode.
	 *
	 * @param sUserName
	 * @param mandator
	 * @return Map&lt;UID, String&gt; contains a map of all custom rest rules
	 * 		that may be allowed for the given user. UID and Name in a map
	 */
	public Map<UID, String> getAllowedCustomRestRules(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getAllowedCustomRestRules();
	}

	/**
	 * Get the ids of all generations the user has assigned.
	 * - has access to it via the roles he belongs to. Superuser gets all.
	 *
	 * @param sUserName
	 * @return Collection&lt;UID&gt; contains a map of all generations
	 * 		that may be assigned to the given user.
	 */
	public Collection<UID> getAssignedGenerations(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getAssignedGeneratorUids();
	}

	/**
	 * Get the ids of all recordgrants the user has assigned.
	 * - has access to it via the roles he belongs to. Superuser gets all.
	 *
	 * @param sUserName
	 * @return Collection&lt;UID&gt; contains a map of all recordgrants
	 * 		that may be assigned to the given user.
	 */
	public Collection<UID> getAssignedRecordgrants(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getAssignedRecordgrantUids();
	}

	/**
	 * Get the ids of all reports/forms the user has the right to write (and read).
	 * Write rights are given when the user
	 * - has created the report/form himself or
	 * - has writable access to it via the roles he belongs to.
	 *
	 * @param sUserName
	 * @return Collection&lt;UID&gt; contains the ids of all reports/forms
	 * 		that may be (read and) written by the given user.
	 */
	public Collection<UID> getWritableReportIds(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getWritableReportUids();
	}

	/**
	 * Get the ids of all datasources the user has the right to read.
	 * A user has right to read a datasource, when
	 * - the user has created it himself or
	 * - it is used in a report the user has access to (via the user's roles)
	 *
	 * @param sUserName
	 * @return Collection&lt;UID&gt; contains the ids of all datasources
	 * 		that may be read by the given user.
	 */
	public Collection<UID> getReadableDataSourceIds(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getReadableDataSourceUids();
	}

	/**
	 * Get the ids of all datasources the user has the right to read.
	 * A user has right to read a datasource, when
	 * - the user has created it himself or
	 * - it is used in a report the user has access to (via the user's roles)
	 *
	 * @param sUserName
	 * @return Collection&lt;Integer&gt; contains the ids of all datasources
	 * 		that may be read by the given user.
	 */
	public Collection<UID> getReadableDataSources(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getReadableDataSourceUids();
	}

	/**
	 * Get the ids of all datasources the user has the right to write (and read).
	 * A user has right to write a datasource when
	 * - the user created it himself or
	 * - it is used in a report the user has writable access to (via the user's roles)
	 * @param sUserName
	 * @return Collection&lt;Integer&gt; contains the ids of all datasources
	 * 		that may be (read and) written by the given user.
	 */
	public Collection<UID> getWritableDataSourceUids(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getWritableDataSourceUids();
	}

	/**
	 * @param sUserName
	 * @param entityUID
	 * @param attributeGroupUID
	 * @return maps a state id to a permission.
	 */
	public Map<UID, Permission> getAttributeGroup(String sUserName, UID entityUID, UID attributeGroupUID, UID mandator) {
		final UserAttributeGroup userattrgroup = new UserAttributeGroup(sUserName, attributeGroupUID, mandator);
		if (!mpAttributeGroups.containsKey(userattrgroup)) {
			mpAttributeGroups.put(userattrgroup, newAttributeGroup(sUserName, entityUID, attributeGroupUID, mandator));
		}

		/*
		 * todo It's not necessary to return a copy here - an unmodifiable wrapper would be better (see below).
		 * We want to go sure for now and optimize later.
		 */
		return new HashMap<UID, Permission>(mpAttributeGroups.get(userattrgroup));
	}

	/**
	 * @param sUserName
	 * @param entityUID
	 * @return maps a state id to a permission
	 */
	public Map<UID, SubformPermission> getSubForm(String sUserName, UID entityUID, UID mandator) {
		final UserSubForm usersubform = new UserSubForm(sUserName, entityUID, mandator);
		Map<UID, SubformPermission> result = mpSubForms.get(usersubform);
		if(result == null) {
			result = getUserRights(sUserName, mandator).readSubFormPermissions(entityUID);
			mpSubForms.put(usersubform, result);
		}
		return new HashMap<>(result);
	}

	private Map<UID, Permission> newAttributeGroup(String sUserName, UID entityUID, UID attributeGroupUid, UID mandator) {
		boolean groupReadOnly = SFConstants.GROUP_UID_READ.equals(attributeGroupUid);
		boolean groupReadWrite = SFConstants.GROUP_UID_WRITE.equals(attributeGroupUid);
		if (groupReadWrite || groupReadOnly) {
			Map<UID, Permission> systemFieldPermissions = new HashMap<>();

			MasterDataFacadeLocal mdFacade = SpringApplicationContextHolder.getBean(MasterDataFacadeLocal.class);
			for (MasterDataVO<UID> state : mdFacade.getMasterData(E.STATE, null)) {
				systemFieldPermissions.put(state.getPrimaryKey(), groupReadWrite ? Permission.READWRITE : Permission.READONLY);
			}

			return systemFieldPermissions;
		}

		return getUserRights(sUserName, mandator).getAttributeGroupPermissions(attributeGroupUid);
	}

	public Set<UID> getCompulsorySearchFilterUids(String userName, UID entityUID, UID mandator) {
		return getUserRights(userName, mandator).getCompulsorySearchFilterUids(entityUID);
	}

	public void invalidate() {
		invalidate(true);
		SpringApplicationContextHolder.getBean(TasklistFacadeLocal.class).invalidateCaches();
	}

	@CacheEvict(value="userData", allEntries=true)
	public void invalidate(boolean refreshMenus) {
		try {
			if (TransactionSynchronizationManager.getSynchronizations().contains(invalidate)) {
				return;
			}

			notifyUser(null, refreshMenus);
			TransactionSynchronizationManager.registerSynchronization(invalidate);
		} catch (IllegalStateException e) {
			// "Transaction synchronization is not active"
			// sometimes we are NOT in a session here, e.g. job run
		}

		TransactionalClusterNotification.notifySecurityRelatedChangeForAllUsers();
	}

	@CacheEvict(value="userData", key="#p0")
	public void invalidate(final String username, boolean refreshMenus, String customUsage) {
		if (username != null) {
			//Note that this method is explicitly called by MasterDataFacadeHelper.invalidateCaches, when the User is altered in T_MD_USER.
			//NUCLOS-3795
			final boolean removeSessionContextsForUser = !NuclosConstants.WEB_LAYOUT_CUSTOM_USAGE.equals(customUsage);
			String session2keep = null;
			try {
				session2keep = SpringApplicationContextHolder.getBean(Session.class).getHttpSession().getId();
				if (removeSessionContextsForUser && session2keep != null) {
					removeSessionContextsForUser(username, session2keep);
				}
			} catch (BeanCreationException e) {
				// sometimes we are NOT in a session here, e.g. job run
			}
			final String session2keepFinal = session2keep;


			try {
				// If we find the invalidate transaction synchronization the complete
				// cache is invalidated. Hence, there is no need to invalidate for
				// a individual user. (tp)
				if (TransactionSynchronizationManager.getSynchronizations().contains(invalidate)) {
					return;
				}

				notifyUser(username, refreshMenus);
			} catch (IllegalStateException e) {
				// "Transaction synchronization is not active"
				// sometimes we are NOT in a session here, e.g. job run
			}

			try {
				TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
					@Override
					public synchronized void afterCommit() {
						clearCachesForUser(username);

						if (removeSessionContextsForUser && session2keepFinal != null) {
							setAuthentication2NullForUser(username, session2keepFinal);
						}
					}
				});
			} catch (IllegalStateException e) {
				// "Transaction synchronization is not active"
				// sometimes we are NOT in a session here, e.g. job run
				clearCachesForUser(username);

				if (removeSessionContextsForUser && session2keepFinal != null) {
					setAuthentication2NullForUser(username, session2keepFinal);
				}
			}

			TransactionalClusterNotification.notifySecurityRelatedChange(username);
		}
		else {
			invalidate(refreshMenus);
		}
	}

	private void clearCachesForUser(String username) {
		LOG.info("afterCommit: Invalidating security cache for user {} ...", username);
		if (mpUserRights.containsKey(username)) {
			mpUserRights.remove(username);
		}
		for (UserSubForm usf : new ArrayList<UserSubForm>(mpSubForms.keySet())) {
			if (username.equals(usf.sUserName)) {
				mpSubForms.remove(usf);
			}
		}
		for (UserAttributeGroup uag : new ArrayList<UserAttributeGroup>(mpAttributeGroups.keySet())) {
			if (username.equals(uag.sUserName)) {
				mpAttributeGroups.remove(uag);
			}
		}

		fatClientContextCache.invalidate(username);
		clearMapRoleNames();
	}

	private void clearMapRoleNames() {
		synchronized (mapRoleNames) {
			mapRoleNamesFilled = false;
			mapRoleNames.clear();
		}
	}

	@Override
	public UID getUserUid(String sUserName){
		UserRights userRights = getUserRights(sUserName, null);
		return userRights != null ? userRights.getUserUid() : null;
	}

	public Set<UID> getUserRoles(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getRoleUids();
	}

	public Set<UID> getUserRolesForUid(UID userUID, UID mandator) {
		return getUserRoles(getUserName(userUID), mandator);
	}

	public String getUserName(UID userUID) {
		String sUserName = mpUserName.get(userUID);
		if (sUserName == null) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<String> query = builder.createQuery(String.class);
			DbFrom t = query.from(E.USER);
			query.select(t.baseColumn(E.USER.username));
			query.where(builder.equalValue(t.basePk(), userUID));
			sUserName = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
			mpUserName.put(userUID, sUserName);
		}
		return sUserName;
	}

	public Map<UID, String> getAllRolesWithName() {
		synchronized (mapRoleNames) {
			if (!mapRoleNamesFilled) {
				mapRoleNames.clear();
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<DbTuple> query = builder.createTupleQuery();
				DbFrom<UID> t = query.from(E.ROLE, SystemFields.BASE_ALIAS);
				query.multiselect(t.baseColumn(E.ROLE.getPk()), t.baseColumn(E.ROLE.name));
				List<DbTuple> dbRoles = dataBaseHelper.getDbAccess().executeQuery(query);
				for (DbTuple dbRole : dbRoles) {
					mapRoleNames.put(dbRole.get(0, UID.class), dbRole.get(1, String.class));
				}
				mapRoleNamesFilled = true;
			}
			return Collections.unmodifiableMap(mapRoleNames);
		}
	}

	public Map<UID, String> getUserRolesWithName(String sUserName, UID mandator) {
		Map<UID, String> result = new HashMap<UID, String>();

		Set<UID> roles = getUserRoles(sUserName, mandator);
		if (roles.isEmpty()) {
			return result;
		}

		Map<UID, String> allRolesWithName = getAllRolesWithName();
		for (UID role : roles) {
			if (allRolesWithName.containsKey(role)) {
				result.put(role, allRolesWithName.get(role));
			}
		}

		return result;
	}

	public String findOtherSimilarRoleName(UID roleUid, String roleName) {
		String roleNameEscaped = NuclosEntityValidator.escapeJavaIdentifier(roleName);
		Map<UID, String> allRoles = getAllRolesWithName();
		for (UID uid : allRoles.keySet()) {
			if (roleUid != null && roleUid.equals(uid)) {
				continue;
			}
			String otherRoleName = allRoles.get(uid);
			if (LangUtils.equal(roleNameEscaped, NuclosEntityValidator.escapeJavaIdentifier(otherRoleName))) {
				return otherRoleName;
			}
		}
		return null;
	}

	public Set<UID> getDynamicTasklists(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getDynamicTasklistUids();
	}

	public Set<UID> getDynamicTasklistDatasources(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getDynamicTasklistDatasourceUids();
	}

	private UserRights getUserRights(String sUserName, UID mandator) {
		Map<UID, UserRights> perMandator = mpUserRights.get(sUserName);
		if (perMandator == null) {
			perMandator = new ConcurrentHashMap<UID, SecurityCache.UserRights>();
			mpUserRights.put(sUserName, perMandator);
		}
		UID mandatorCacheKey = mandator == null ? UID.UID_NULL : mandator;
		UserRights result = perMandator.get(mandatorCacheKey);
		if (result == null) {
			result = new UserRights(sUserName, mandator);
			perMandator.put(mandatorCacheKey, result);
		}
		return result;
	}

	/**
	 * get the permission for an attribute within the given status numeral
	 */
	public Permission getAttributePermission(UID entityUID, UID attributeUID, UID stateUID) {
		AttributePermissionKey attributePermissionKey = new AttributePermissionKey(entityUID, attributeUID, stateUID);
		if (!mpAttributePermission.containsKey(attributePermissionKey)) {
			// TODO: Get rid of SecurityFacade here to break dependency cycle
			Permission permission = SpringApplicationContextHolder.getBean(SecurityFacadeBean.class)
					.getAttributePermission(entityUID, attributeUID, stateUID);
			if (permission == null) {
				permission = Permission.NONE;
			}
			mpAttributePermission.put(attributePermissionKey, permission);
		}
		final Permission result = mpAttributePermission.get(attributePermissionKey);
		if (Permission.NONE.equals(result)) {
			return null;
		}
		return result;
	}

	@ManagedAttribute
	public int getAttributeGroupsCount() {
		return mpAttributeGroups != null ? mpAttributeGroups.size() : 0;
	}

	@ManagedAttribute
	public int getSubFormCount() {
		return mpSubForms != null ? mpSubForms.size() : 0;
	}

	@ManagedAttribute
	public int getUserRightsCount() {
		return mpUserRights != null ? mpUserRights.size() : 0;
	}

	/**
	 * notifies clients that the leased object meta data has changed, so they can invalidate their local caches.
	 */
	private void notifyUser(String username, boolean refreshMenus) {
		LOG.debug("JMS send: notify user {} that security data has changed: {}",
		          username, this);
		NuclosJMSUtils.sendOnceAfterCommitDelayed(new NotifyObject(username, refreshMenus), JMSConstants.TOPICNAME_SECURITYCACHE);
	}

	private Collection<String> searchSessionUIDForUser(String username) {
		Collection<String> retVal = new HashSet<String>();
		for (SessionContext sc : sessionContextCache.asMap().values()) {
			if (sc != null && sc.getUsername().equals(username)) {
				retVal.add(sc.getJSessionId());
			}
		}
		return retVal;
	}

	private Collection<String> searchOtherSessions() {
		Collection<String> retVal = new HashSet<String>();
		for (SessionContext sc : sessionContextCache.asMap().values()) {
			if (sc != null && !sc.getJSessionId().equals(getSessionId())) {
				retVal.add(sc.getJSessionId());
			}
		}
		return retVal;
	}

	public SessionContext setSessionContext(
			final Authentication auth,
			final Locale locale,
			final String jSessionId,
			final Long sessionId
	) {
		final SessionContext sessionContext = new SessionContext(
				sessionId,
				jSessionId,
				auth,
				locale
		);
		return setSessionContext(jSessionId, sessionContext);
	}

	public SessionContext setSessionContext(
			final String jSessionId,
			final SessionContext sessionContext
	) {
		sessionContextCache.put(jSessionId, sessionContext);
		return sessionContext;
	}

	public SessionContext getSessionContext(String webSessionUID) {
		return sessionContextCache.getIfPresent(webSessionUID);
	}

	public void removeSessionContext(String webSessionUID) {
		sessionContextCache.invalidate(webSessionUID);
	}

	public void removeSessionContextForOthers() {
		Collection<String> sessions = searchOtherSessions();
		sessions.remove(getSessionId());
		for (String s : sessions) {
			sessionContextCache.invalidate(s);
		}
	}

	public void removeSessionContextsForUser(String username, String webSessionID2Keep) {
		Collection<String> sessions = searchSessionUIDForUser(username);
		if (!StringUtils.isNullOrEmpty(webSessionID2Keep)) {
			sessions.remove(webSessionID2Keep);
		}
		for (String s : sessions) {
			sessionContextCache.invalidate(s);
		}
	}

	public long getOtherSessionContextCount() {
		return sessionContextCache.asMap().values().stream()
				.filter(
						sessionContext ->
								!Objects.equals(sessionContext.getJSessionId(), getSessionId())
				)
				.map(SessionContext::getUsername)
				.distinct()
				.count();
	}

	public List<String> getOtherActiveUserNames() {
		return sessionContextCache.asMap().values().stream()
				.filter(
						sessionContext ->
								!Objects.equals(sessionContext.getJSessionId(), getSessionId())
				).map(SessionContext::getUsername)
				.distinct()
				.collect(Collectors.toList());
	}

	public void setAuthentication2NullForUser(String username, String webSessionID2Keep) {
		for (Map.Entry<String, SessionContext> e : sessionContextCache.asMap().entrySet()) {
			String webSessionId = e.getKey();
			SessionContext context = e.getValue();
			if (context == null || (!StringUtils.isNullOrEmpty(webSessionId) && context.getJSessionId().equals(webSessionID2Keep))) {
				continue;
			}
			if (username.equals(context.getUsername())) {
				context.setAuthentication(null);
			}
		}
	}

	public void setAuthentificationFromFatClientContext(String login, String password, Authentication auth) {
		Map<String, Authentication> mp2 = new HashMap<String, Authentication>();
		mp2.put(password, auth);
		fatClientContextCache.put(login, mp2);
	}

	public void removeAuthentificationFromFatClientContext(String username) {
		fatClientContextCache.invalidate(username);
	}

	public Authentication getAuthentificationFromFatClientContext(String login, String password) {
		Map<String, Authentication> mp2 = fatClientContextCache.getIfPresent(login);
		if (mp2 != null) {
			return mp2.get(password);
		}
		return null;
	}

	public List<MandatorVO> getAssignedMandators(String sUserName) {
		fillMandatorsIfNecessary();
		List<MandatorVO> result = new ArrayList<MandatorVO>();
		Set<UID> grantedMandators = getUserRights(sUserName, null).getMandators();
		for (MandatorVO mandator : lstMandators) {
			if (grantedMandators.contains(mandator.getUID())) {
				result.add(mandator);
			}
		}
		return Collections.unmodifiableList(result);
	}

	public boolean isMandatorPresent() {
		fillMandatorsIfNecessary();
		return !lstMandators.isEmpty();
	}

	public List<MandatorLevelVO> getAllMandatorLevels() {
		fillMandatorsIfNecessary();
		return Collections.unmodifiableList(lstMandatorLevels);
	}

	public List<MandatorVO> getAllMandators() {
		fillMandatorsIfNecessary();
		return Collections.unmodifiableList(lstMandators);
	}

	public Set<UID> getChildMandators(UID parentUID) {
		fillMandatorsIfNecessary();
		Set<UID> result = mandatorChildren.get(parentUID);
		if (result == null) {
			result = Collections.unmodifiableSet(new HashSet<UID>());
			mandatorChildren.put(parentUID, result);
		}
		return result;
	}

	public MandatorVO getMandator(UID mandatorUID) {
		fillMandatorsIfNecessary();
		return mapMandators.get(mandatorUID);
	}

	public MandatorLevelVO getMandatorLevel(UID levelUID) {
		fillMandatorsIfNecessary();
		return mapLevels.get(levelUID);
	}

	public List<MandatorVO> getMandatorsByLevel(UID levelUID) {
		fillMandatorsIfNecessary();
		List<MandatorVO> result = mandatorsByLevel.get(levelUID);
		if (result == null) {
			result = Collections.unmodifiableList(new ArrayList<MandatorVO>());
			mandatorsByLevel.put(levelUID, result);
		}
		return result;
	}

	/**
	 *
	 * @param mandatorUID
	 * @return the accessible mandators for the given mandator.
	 * 		accessible= all childrens + direct parents up to root
	 */
	public Set<UID> getAccessibleMandators(UID mandatorUID) {
		fillMandatorsIfNecessary();
		return mandatorAccessible.get(mandatorUID==null?UID.UID_NULL:mandatorUID);
	}

	private void fillMandatorsIfNecessary() {
		synchronized (lstMandators) {
			if (!mandatorsloaded) {
				lstMandators.clear();
				lstMandatorLevels.clear();
				mandatorChildren.clear();
				mandatorAccessible.clear();
				mapMandators.clear();
				mandatorsByLevel.clear();
				mapLevels.clear();
				readAllMandators(lstMandators, mandatorChildren, mapMandators, mandatorsByLevel, new HashSet<UID>(), null);
				for (Map.Entry<UID, Set<UID>> entry : mandatorChildren.entrySet()) {
					entry.setValue(Collections.unmodifiableSet(entry.getValue()));
				}
				for (Map.Entry<UID, List<MandatorVO>> entry : mandatorsByLevel.entrySet()) {
					Collections.sort(entry.getValue());
					entry.setValue(Collections.unmodifiableList(entry.getValue()));
				}
				readAllMandatorLevels(mapLevels);
				for (MandatorVO mandator : lstMandators) {
					Set<UID> set = new HashSet<UID>();
					set.add(mandator.getUID());
					addChildMandators(set, mandator.getUID(), mandatorChildren);
					addParentMandators(set, mandator.getUID(), mapMandators);
					mandatorAccessible.put(mandator.getUID(), Collections.unmodifiableSet(set));
				}
				lstMandatorLevels.addAll(mapLevels.values());
				MandatorLevelVO.sort(lstMandatorLevels);
				mandatorAccessible.put(UID.UID_NULL, Collections.unmodifiableSet(CollectionUtils.transformIntoSet(lstMandators, new MandatorVO.ToUid())));
				Collections.sort(lstMandators);
				mandatorsloaded = true;
				refillDbCacheAsync();
			}
		}
	}

	private static void addChildMandators(Set<UID> result, UID parent, Map<UID, Set<UID>> mandatorChildren) {
		if (mandatorChildren.containsKey(parent)) {
			for (UID child : mandatorChildren.get(parent)) {
				result.add(child);
				addChildMandators(result, child, mandatorChildren);
			}
		}
	}

	private static void addParentMandators(Set<UID> result, UID child, Map<UID, MandatorVO> mapMandators) {
		MandatorVO childMandator = mapMandators.get(child);
		if (childMandator.getParentUID() != null) {
			result.add(childMandator.getParentUID());
			addParentMandators(result, childMandator.getParentUID(), mapMandators);
		}
	}

	private void readAllMandatorLevels( Map<UID, MandatorLevelVO> mapLevels) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.MANDATOR_LEVEL);
		query.multiselect(
			t.baseColumn(E.MANDATOR_LEVEL),
			t.baseColumn(E.MANDATOR_LEVEL.name),
			t.baseColumn(E.MANDATOR_LEVEL.parentLevel),
			t.baseColumn(E.MANDATOR_LEVEL.showName));
		for (DbTuple child : dataBaseHelper.getDbAccess().executeQuery(query)) {
			UID pk = child.get(E.MANDATOR_LEVEL.getPk());
			String name = child.get(E.MANDATOR_LEVEL.name);
			UID parent = child.get(E.MANDATOR_LEVEL.parentLevel);
			Boolean showName = child.get(E.MANDATOR_LEVEL.showName);
			MandatorLevelVO vo = new MandatorLevelVO(pk, name, parent, showName);
			mapLevels.put(pk, vo);
		}
	}

	private void readAllMandators(
			Collection<MandatorVO> result,
			Map<UID, Set<UID>> mandatorChildren,
			Map<UID, MandatorVO> mapMandators,
			Map<UID, List<MandatorVO>> mandatorsByLevel,
			Set<UID> readPk, UID parentUID) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.MANDATOR);
		query.multiselect(
			t.baseColumn(E.MANDATOR),
			t.baseColumn(E.MANDATOR.name),
			t.baseColumn(E.MANDATOR.path),
			t.baseColumn(E.MANDATOR.parentMandator),
			t.baseColumn(E.MANDATOR.level),
			t.baseColumn(E.MANDATOR.color));
		if (parentUID == null) {
			query.where(builder.isNull(E.MANDATOR.parentMandator));
		} else {
			query.where(builder.equalValue(t.baseColumn(E.MANDATOR.parentMandator), parentUID));
		}
		for (DbTuple child : dataBaseHelper.getDbAccess().executeQuery(query)) {
			UID pk = child.get(E.MANDATOR.getPk());
			String name = child.get(E.MANDATOR.name);
			String path = child.get(E.MANDATOR.path);
			UID parent = child.get(E.MANDATOR.parentMandator);
			UID level = child.get(E.MANDATOR.level);
			String color = child.get(E.MANDATOR.color);
			if (readPk.contains(pk)) {
				continue;
			}
			readPk.add(pk);
			MandatorVO vo = new MandatorVO(pk, name, path, parent, level, color);
			result.add(vo);
			mapMandators.put(pk, vo);

			final UID key = parentUID==null?UID.UID_NULL:parentUID;
			if (!mandatorChildren.containsKey(key)) {
				mandatorChildren.put(key, new HashSet<UID>());
			}
			if (!mandatorsByLevel.containsKey(level)) {
				mandatorsByLevel.put(level, new ArrayList<MandatorVO>());
			}
			mandatorChildren.get(key).add(pk);
			mandatorsByLevel.get(level).add(vo);

			readAllMandators(result, mandatorChildren, mapMandators, mandatorsByLevel, readPk, pk);
		}
	}

	public void checkMandatorLoginPermission(String sUserName, UID mandatorUID) throws CommonPermissionException {
		if (isMandatorPresent()) {
			if (mandatorUID != null) {
				if (!getUserRights(sUserName, null).getMandators().contains(mandatorUID)) {
					MandatorVO mandator = getMandator(mandatorUID);
					if (mandator == null) {
						throw new CommonPermissionException("mandator.permission.not.found");
					} else {
						throw new CommonPermissionException("mandator.permission.not.assigned");
					}
				}
			} else {
				if (!isSuperUser(sUserName)) {
					throw new CommonPermissionException("mandator.permission.no.superuser");
				}
			}
		}
	}

	@ManagedAttribute(description="get the size (number of users/UserRights) of mpUserRights")
	public int getNumberOfUserRightsInCache() {
		return mpUserRights.size();
	}

	@ManagedAttribute(description="get the minimal size of MasterDataPermissions in mpUserRights")
	public int getMasterDataPermissionsMinSize() {
		return masterDataPermissionsMinSize;
	}

	@ManagedAttribute(description="get the maximal size of MasterDataPermissions in mpUserRights")
	public int getMasterDataPermissionsMaxSize() {
		return masterDataPermissionsMaxSize;
	}

	void cleanDbCacheAsync() {
		Thread t = new Thread("SecurityCache.cleanDbCache") {
			@Override
			public void run() {
				refillDbCache(dataBaseHelper, null, null, true);
			}
		};
		t.start();
	}

	void refillDbCacheAsync() {
		Thread t = new Thread("SecurityCache.refillDbCache") {
			@Override
			public void run() {
				refillDbCache(dataBaseHelper, new ArrayList<>(lstMandators), new HashMap<>(mandatorAccessible), false);
			}
		};
		t.start();
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	synchronized void refillDbCache(final SpringDataBaseHelper dataBaseHelper, final List<MandatorVO> lstMandators, final Map<UID, Set<UID>> mandatorAccessible, final boolean cleanOnly) {
		dataBaseHelper.execute(new DbDeleteStatement<Long>(E.MANDATOR_ACCESSIBLE, new DbMap()));
		if (cleanOnly) {
			return;
		}
		int pk = 1000;
		for (MandatorVO mandator : lstMandators) {
			for (UID accessible : mandatorAccessible.get(mandator.getUID())) {
				DbMap values = new DbMap();
				values.put(E.MANDATOR_ACCESSIBLE.getPk(), new Long(pk++));
				values.put(E.MANDATOR_ACCESSIBLE.mandator, mandator.getUID());
				values.put(E.MANDATOR_ACCESSIBLE.accessible, accessible);
				values.put(SF.CREATEDAT, InternalTimestamp.toInternalTimestamp(new Date()));
				values.put(SF.CREATEDBY, "securityCache");
				values.put(SF.CHANGEDAT, values.get(SF.CREATEDAT));
				values.put(SF.CHANGEDBY, values.get(SF.CREATEDBY));
				values.put(SF.VERSION, 1);
				dataBaseHelper.execute(new DbInsertStatement<Long>(E.MANDATOR_ACCESSIBLE, values));
			}
		}
	}

	public UID getUserCommunicationPhoneAccount(String sUserName) {
		return getUserRights(sUserName, null).getCommunicationAccountPhone();
	}

	/**
	 * TODO: Do not use flag arguments (bWrite) - split this into 2 methods instead (isReadAllowedForField / isWriteAllowedForField)
	 */
	@Deprecated
	public boolean isReadOrWriteAllowedForField(
			FieldMeta<?> fm,
			UsageCriteria usage,
			boolean bWrite,
			String user,
			UID mandator
	) {

		//TODO: For Fields without state there aren't any right checks for fields yet, so we return always true.
		if (usage.getStatusUID() == null) {
			return true;
		}

		Map<UID, Permission> mpPermission = getAttributeGroup(user, usage.getEntityUID(), fm.getFieldGroup(), mandator);
		Permission perm = mpPermission.get(usage.getStatusUID());
		if (perm == null) {
			return false;
		}
		return bWrite ? perm.includesWriting() : perm.includesReading();
	}

	/**
	 * Checks if the given field is readable either directly or indirectly via a stringified reference.
	 *
	 * TODO: Cache result
	 */
	public boolean isReadAllowedForFieldViaStringifiedReference(FieldMeta<?> fieldMeta, String user, UID mandator) {
		// Direct read allowed?
		boolean readAllowed = isReadAllowedForEntity(user, fieldMeta.getEntity(), mandator)
				&& isReadOrWriteAllowedForField(
				fieldMeta,
				new UsageCriteria(fieldMeta.getEntity(), null, null, null),
				false,
				user,
				mandator
		);

		if (readAllowed) {
			return true;
		}

		// Direct read not allowed -> check for implicit permission via stringified reference
		Collection<EntityMeta<?>> allEntities = metaProvider.getAllEntities();
		for (EntityMeta<?> entityMeta : allEntities) {

			// Find reference fields whose foreign entity is the entity of the field to check
			Collection<FieldMeta<?>> referenceFieldMetas = entityMeta.getFields().stream().filter(
					meta -> RigidUtils.equal(meta.getForeignEntity(), fieldMeta.getEntity())
			).collect(Collectors.toSet());

			for (FieldMeta<?> referenceFieldMeta : referenceFieldMetas) {
				boolean readAllowedForReferenceField = isReadOrWriteAllowedForField(
						referenceFieldMeta,
						new UsageCriteria(referenceFieldMeta.getEntity(), null, null, null),
						false,
						user,
						mandator);
				if (readAllowedForReferenceField) {
					// Check if the field to check is part of the stringified reference
					String stringifiedReference = referenceFieldMeta.getForeignEntityField();
					if (UID.parseAllUIDs(stringifiedReference).contains(fieldMeta.getUID())) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * TODO: Do not use flag arguments (bWrite) - split this into 2 methods instead (isReadAllowedForSubform / isWriteAllowedForSubform)
	 */
	@Deprecated
	public boolean isReadOrWriteAllowedForSubform(
			UID subform,
			UsageCriteria usage,
			boolean bWrite,
			String user,
			UID mandator
	) {
		SubformPermission perm = getSubformPermission(subform, usage, user, mandator);
		if (perm == null) {
			return false;
		}

		return bWrite ? perm.includesWriting() : perm.includesReading();
	}

	public SubformPermission getSubformPermission(UID subform, UsageCriteria usage, String user, UID mandator) {
		// No statemodel, all rights for subforms
		if (usage.getStatusUID() == null) {
			return SubformPermission.ALL;
		}

		Map<UID, SubformPermission> mpPermission = getSubForm(user, subform, mandator);
		return mpPermission.get(usage.getStatusUID());
	}

	private Map<UID, SsoAuth> getMapSsoAuths() {
		synchronized (mapSsoAuths) {
			if (!bSsoAuthsFilled) {
				QueryProvider.execute(QueryProvider.create(SsoAuth.class))
						.forEach(ssoAuth -> mapSsoAuths.put(ssoAuth.getId(), ssoAuth));
				bSsoAuthsFilled = true;
			}
			return mapSsoAuths;
		}
	}

	public void revalidateSsoAuth(UID ssoAuthUID) {
		SsoAuth ssoAuth = SsoAuth.get(ssoAuthUID);
		if (ssoAuth == null) {
			// removed
			mapSsoAuths.remove(ssoAuthUID);
		} else {
			mapSsoAuths.put(ssoAuthUID, ssoAuth);
		}
		TransactionalClusterNotification.notifySsoAuthRelatedChange(ssoAuthUID);
	}

	public SsoAuth getSsoAuth(UID ssoAuthUID) {
		return getMapSsoAuths().get(ssoAuthUID);
	}

	public List<SsoAuth> getActiveSsoAuths() {
		List<SsoAuth> result = new ArrayList<>(getMapSsoAuths().values()
			.stream()
			.filter(ssoAuth -> Boolean.TRUE.equals(ssoAuth.getActive()))
			.collect(Collectors.toList()));
		Collections.sort(result, new Comparator<SsoAuth>() {
			@Override
			public int compare(final SsoAuth ssoAuth1, final SsoAuth ssoAuth2) {
				return LangUtils.compare(ssoAuth1.getOrder(), ssoAuth2.getOrder());
			}
		});
		return Collections.unmodifiableList(result);
	}
}
