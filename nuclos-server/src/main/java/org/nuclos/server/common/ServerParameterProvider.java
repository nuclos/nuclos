//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.nuclos.common.AbstractParameterProvider;
import org.nuclos.common.DisposableRegistrationBean;
import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.cluster.TransactionalClusterNotification;
import org.nuclos.server.dal.ExtendedDalUtils;
import org.nuclos.server.dal.processor.DalChangeNotification;
import org.nuclos.server.dal.processor.nuclos.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.stereotype.Component;

import io.reactivex.rxjava3.disposables.Disposable;

/**
 * <code>ParameterProvider</code> for the server side.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph Radig</a>
 * @version 00.01.000
 */
@Component("parameterProvider")
@DependsOn("appContext")
public class ServerParameterProvider extends AbstractParameterProvider implements DisposableRegistrationBean {


	private static final Logger LOG = LoggerFactory.getLogger(ServerParameterProvider.class);
	
	private static final String NULL = "<null>";
	
	private static ServerParameterProvider INSTANCE;
	
	//

	/**
	 * Map<String sName, String sValue>
	 */
	private final ConcurrentMap<String, String> mpParameters = new ConcurrentHashMap<String, String>();

	/**
	 * Map<UID PK, String sValue>
	 */
	private final ConcurrentMap<UID, String> mpPK2ParameterName = new ConcurrentHashMap<UID, String>();


	//private final ClientNotifier clientnotifier = new ClientNotifier(JMSConstants.TOPICNAME_PARAMETERPROVIDER);
	
	private SpringDataBaseHelper dataBaseHelper;

	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static ServerParameterProvider getInstance() {
		return INSTANCE;
	}

	private final static Map<String, String> parameterDefaults = getParameterDefaults();

	private final Collection<Disposable> synchronizedDisposableRegistration = Collections.synchronizedCollection(new ArrayList<>());

	private final NuclosDalProvider nuclosDalProvider;

	private IEntityObjectProcessor<UID> paramProc;

	protected ServerParameterProvider(final NuclosDalProvider nuclosDalProvider) {
		this.nuclosDalProvider = nuclosDalProvider;
		INSTANCE = this;
	}

	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	@PostConstruct
	public void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		this.paramProc = nuclosDalProvider.getEntityObjectProcessor(E.PARAMETER);
 		createSyncFlowableWithoutStrategyAndRegister(paramProc.observeAllChangesImmediately(true), this::refreshParameter);
		refreshParameter(null);
	}

	/**
	 * @return Map&lt;String sName, String sValue&gt;
	 */
	@ManagedAttribute
	public Map<String, String> getAllParameters() {
		final Map<String, String> result = new HashMap<>(parameterDefaults);
		result.putAll(mpParameters);
		return Collections.unmodifiableMap(result);
	}

	@ManagedOperation(description="Get the value of the given parameter")
	@ManagedOperationParameters({
			@ManagedOperationParameter(name="sParameter", description="name of the parameter")
	})
	@Override
	public String getValue(String sParameter) {
		final String result = mpParameters.get(sParameter);
		if (NULL.equals(result)) {
			return null;
		}
		return Optional.ofNullable(result).orElseGet(() -> parameterDefaults.get(sParameter));
	}

	public String getValueForPK(UID pk) throws CommonFinderException {
		if (!mpPK2ParameterName.containsKey(pk)) {
			throw new CommonFinderException();
		}
		String sParameter = mpPK2ParameterName.get(pk);
		if (sParameter != null) {
			return getValue(sParameter);
		}
		return null;
	}

	private void refreshParameter(final DalChangeNotification<EntityObjectVO<UID>, UID> notification) {
		synchronized (mpPK2ParameterName) {
			if (notification == null || notification.hasUnidentified() || notification.isRollbackOnly()) {
				// complete refresh
				final List<EntityObjectVO<UID>> allParams = paramProc.getAll();
				final Set<UID> pks = allParams.stream().map(HasPrimaryKey::getPrimaryKey).collect(Collectors.toSet());
				final Set<UID> cachedPks = new HashSet<>(mpPK2ParameterName.keySet());
				cachedPks.stream().filter(pk -> !pks.contains(pk)).forEach(this::removeParameter);
				allParams.forEach(this::putParameter);
			} else {
				if (notification.isSingleChangeOnly()) {
					putParameter(notification.getSingleChange());
				} else {
					Optional.ofNullable(notification.getDeleted()).ifPresent(pks -> pks.forEach(this::removeParameter));
					Optional.ofNullable(notification.getInsertedAndUpdated()).ifPresent(pks -> pks.forEach(this::putParameter));
				}
			}
		}
	}

	private void removeParameter(UID pk) {
		final String sParameter = mpPK2ParameterName.get(pk);
		mpPK2ParameterName.remove(pk);
		mpParameters.remove(sParameter);
	}

	private void putParameter(UID pk) {
		putParameter(paramProc.getByPrimaryKey(pk));
	}

	private void putParameter(EntityObjectVO<UID> eoParam) {
		final UID pk = eoParam.getPrimaryKey();
		final String sParameter = eoParam.getFieldValue(E.PARAMETER.name);
		mpPK2ParameterName.put(pk, sParameter);
		final String sValue =   Optional.ofNullable(System.getProperty("override." + sParameter.replace(' ', '_')))
						.orElse(Optional.ofNullable(eoParam.getFieldValue(E.PARAMETER.value)).filter(s -> !s.isEmpty())
						.orElse(NULL));
		mpParameters.put(sParameter, sValue);
	}

	/**
	 * updates existing system parameters and insert new parameters
	 */
	public void insertOrUpdateSystemParameters(Map<String, String> parameters, String editingUser) {
		List<EntityObjectVO<UID>> parameterEVOs = paramProc.getAll();

		Set<String> setUpdated = new HashSet<>();

		for (EntityObjectVO<UID> eovo: parameterEVOs) {
			String key = eovo.getFieldValue(E.PARAMETER.name);
			String value = parameters.get(key);
			if (value != null) {
				try {
					eovo.setFieldValue(E.PARAMETER.value, value);
					eovo.flagUpdate();
					ExtendedDalUtils.updateVersionInformation(eovo, editingUser);
					paramProc.insertOrUpdate(eovo);
					setUpdated.add(key);
				} catch (Exception ex) {
					LOG.error("Error during update of system parameter '{}' (value={}): {}", key, value, ex.getMessage(), ex);
				}
			}
		}

		parameters.entrySet().stream()
				.filter(entry -> !setUpdated.contains(entry.getKey()))
				.forEach(entry -> {
					if (entry.getValue() != null) {
						try {
							EntityObjectVO<UID> eovo = new EntityObjectVO<UID>(E.PARAMETER);
							eovo.setFieldValue(E.PARAMETER.name, entry.getKey());
							eovo.setFieldValue(E.PARAMETER.description, entry.getKey());
							eovo.setFieldValue(E.PARAMETER.value, entry.getValue());
							eovo.setPrimaryKey(new UID());
							eovo.flagNew();
							ExtendedDalUtils.updateVersionInformation(eovo, editingUser);
							paramProc.insertOrUpdate(eovo);
						} catch (Exception ex) {
							LOG.error("Error during insert of system parameter '{}' (value={}): {}", entry.getKey(), entry.getValue(), ex.getMessage(), ex);
						}
					}
				});
	}
	
	private static Map<String, String> getParameterDefaults() {
		Properties defaults = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("resources/parameter-defaults.properties");
		if (is == null) {
			throw new NuclosFatalException("Missing server resource parameter-defaults.properties");
		}
		try {
			defaults.load(is);
			is.close();
		} catch (IOException ex) {
			throw new NuclosFatalException("Error loading server resource parameter-defaults.properties", ex);
		}
		return (Map) defaults;
	}

	public void notifyClients(boolean bAfterCommit) {
		LOG.debug("JMS send: notify clients that parameters changed: {}", this);
		if(bAfterCommit) {
			NuclosJMSUtils.sendOnceAfterCommitDelayed(JMS_MESSAGE_ALL_PARAMETERS_ARE_REVALIDATED, JMSConstants.TOPICNAME_PARAMETERPROVIDER);
		}
		else {
			NuclosJMSUtils.sendMessage(JMS_MESSAGE_ALL_PARAMETERS_ARE_REVALIDATED, JMSConstants.TOPICNAME_PARAMETERPROVIDER, null);
		}
	}
	
	/**
	 * @return current system environment
	 */
	public static Map<String, String> getSystemParameters() {
		return System.getenv();
	}

	@ManagedAttribute
	public Collection<String> getParameterNames() {
		// defensive copy is needed for JMX
		return new ArrayList<String>(mpParameters.keySet());
	}

	public void invalidateCache(boolean notifyClients) {
		refreshParameter(null);
		if(notifyClients) {
			notifyClients(true);
		}
	}

	@ManagedAttribute(description="get the size (number of parameters) of mpParameters")
	public int getNumberOfParameters() {
		return mpParameters.size();
	}

	@Override
	public Collection<Disposable> getDisposableRegistration() {
		return synchronizedDisposableRegistration;
	}

}	// class ServerParameterProvider
