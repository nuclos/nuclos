//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.ObjectUtils;
import org.nuclos.common.AbstractDisposableRegistrationBean;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.schema.rest.StatusInfo;
import org.nuclos.schema.rest.StatusPath;
import org.nuclos.server.cluster.TransactionalClusterNotification;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.statemodel.valueobject.AttributegroupPermissionVO;
import org.nuclos.server.statemodel.valueobject.MandatoryColumnVO;
import org.nuclos.server.statemodel.valueobject.MandatoryFieldVO;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.nuclos.server.statemodel.valueobject.StateModelUsageVO;
import org.nuclos.server.statemodel.valueobject.StateModelUsages;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StatePathElement;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.nuclos.server.statemodel.valueobject.SubformGroupPermissionVO;
import org.nuclos.server.statemodel.valueobject.SubformPermissionVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

/**
 * A cache for States.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:lars.rueckemann@novabit.de">Lars R\u00fcckemann</a>
 * @version 00.01.000
 */
@Component
public class StateCache extends AbstractDisposableRegistrationBean {
	
	private static final Logger LOG = LoggerFactory.getLogger(StateCache.class);

	private static final String GERMAN_TAG = LocaleInfo.parseTag(Locale.GERMAN).getTag();

	private static final String ENGLISH_TAG = LocaleInfo.parseTag(Locale.ENGLISH).getTag();

	private final SpringDataBaseHelper dataBaseHelper;
	private final MetaProvider metaProv;
	private final NuclosDalProvider nuclosDalProvider;
	private final NucletDalProvider nucletDalProvider;

	public StateCache(final SpringDataBaseHelper dataBaseHelper,
					  final MetaProvider metaProv,
					  final NuclosDalProvider nuclosDalProvider,
					  final NucletDalProvider nucletDalProvider) {
		this.dataBaseHelper = dataBaseHelper;
		this.metaProv = metaProv;
		this.nuclosDalProvider = nuclosDalProvider;
		this.nucletDalProvider = nucletDalProvider;
	}

	@PostConstruct
	private void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		createSyncFlowableWithDropStrategyAndRegister(metaProv.observeEntityMetaRefreshsAfterCommit(), entityMetas -> invalidate());
	}

	@Cacheable(value="allStatesMap")
	public Map<UID, StateVO> getAllStatesMap() {
		List<EntityObjectVO<UID>> allRoleAttributeGroupPermissions = nuclosDalProvider.getEntityObjectProcessor(E.ROLEATTRIBUTEGROUP).getAll();
		List<EntityObjectVO<UID>> allRoleSubformPermissions = nuclosDalProvider.getEntityObjectProcessor(E.ROLESUBFORM).getAll();
		List<EntityObjectVO<UID>> allRoleSubformGroupPermissions = nuclosDalProvider.getEntityObjectProcessor(E.ROLESUBFORMGROUP).getAll();

		List<EntityObjectVO<UID>> allMandatoryFields = nuclosDalProvider.getEntityObjectProcessor(E.STATEMANDATORYFIELD).getAll();
		List<EntityObjectVO<UID>> allMandatoryColumns = nuclosDalProvider.getEntityObjectProcessor(E.STATEMANDATORYCOLUMN).getAll();
		List<EntityObjectVO<UID>> allStates = nuclosDalProvider.getEntityObjectProcessor(E.STATE).getAll();

		Map<LocaleResourceKey, String> allStateRelevantResources = getAllStateRelevantResourcesFromDb();

		Map<UID, ConcurrentLinkedQueue<AttributegroupPermissionVO>> allRoleAttributeGroupPermissionsByStateIdMap = createInitialMapWithConcurrentList(allStates);
		Map<UID, ConcurrentLinkedQueue<SubformPermissionVO>> allSubformPermissionsByStateIdMap = createInitialMapWithConcurrentList(allStates);
		Map<UID, Set<SubformGroupPermissionVO>> allSubformGroupPermissionsBySubformPermissionIdMap = new ConcurrentHashMap<>();
		Map<UID, Set<MandatoryFieldVO>> allMandatoryFieldsByStateIdMap = createInitialMapWithConcurrentSet(allStates, true);
		Map<UID, Set<MandatoryColumnVO>> allMandatoryColumnsByStateIdMap = createInitialMapWithConcurrentSet(allStates, true);

		Map<UID, StateVO> allStatesMap = Collections.unmodifiableMap(allStates.parallelStream().collect(
				Collectors.toMap(
						k -> k.getPrimaryKey(),
						v -> {
							StateVO state = MasterDataWrapper.getStateVO(new MasterDataVO<UID>(v));

							if (state.getResourceIdForLabel() != null) {
								state.setStatename(Locale.GERMAN, getBestStateLocaleResourceFromMap(allStateRelevantResources, Locale.GERMAN, state.getResourceIdForLabel()));
								state.setStatename(Locale.ENGLISH, getBestStateLocaleResourceFromMap(allStateRelevantResources, Locale.ENGLISH, state.getResourceIdForLabel()));
							}
							if (state.getResourceIdForDescription() != null) {
								state.setDescription(Locale.GERMAN, getBestStateLocaleResourceFromMap(allStateRelevantResources, Locale.GERMAN, state.getResourceIdForDescription()));
								state.setDescription(Locale.ENGLISH, getBestStateLocaleResourceFromMap(allStateRelevantResources, Locale.ENGLISH, state.getResourceIdForDescription()));
							}
							if (state.getResourceIdForButton() != null) {
								state.setButtonLabel(Locale.GERMAN, getBestStateLocaleResourceFromMap(allStateRelevantResources, Locale.GERMAN, state.getResourceIdForButton()));
								state.setButtonLabel(Locale.ENGLISH, getBestStateLocaleResourceFromMap(allStateRelevantResources, Locale.ENGLISH, state.getResourceIdForButton()));
							}
							return state;
						})));

		allRoleAttributeGroupPermissions.parallelStream().forEach(eo -> {
					AttributegroupPermissionVO attributegroupPermissionVO = MasterDataWrapper.getAttributegroupPermissionVO(new MasterDataVO<>(eo));
					allRoleAttributeGroupPermissionsByStateIdMap.get(attributegroupPermissionVO.getStateUID()).add(attributegroupPermissionVO);
				});

		allRoleSubformPermissions.parallelStream().forEach(eo -> {
					SubformPermissionVO subformPermissionVO = MasterDataWrapper.getSubformPermissionVO(new MasterDataVO<>(eo));
					allSubformPermissionsByStateIdMap.get(subformPermissionVO.getState()).add(subformPermissionVO);
					allSubformGroupPermissionsBySubformPermissionIdMap.put(subformPermissionVO.getPrimaryKey(), ConcurrentHashMap.newKeySet());
				});

		allRoleSubformGroupPermissions.parallelStream().forEach(eo -> {
					SubformGroupPermissionVO subformGroupPermissionVO = MasterDataWrapper.getSubformColumnPermissionVO(new MasterDataVO<>(eo));
					allSubformGroupPermissionsBySubformPermissionIdMap.get(subformGroupPermissionVO.getRoleSubform()).add(subformGroupPermissionVO);
				});

		allMandatoryFields.parallelStream().forEach(eo -> {
					MandatoryFieldVO mandatoryFieldVO = MasterDataWrapper.getMandatoryFieldVO(new MasterDataVO<>(eo));
					allMandatoryFieldsByStateIdMap.get(mandatoryFieldVO.getState()).add(mandatoryFieldVO);
				});

		allMandatoryColumns.parallelStream().forEach(eo -> {
					MandatoryColumnVO mandatoryColumnVO = MasterDataWrapper.getMandatoryColumnVO(new MasterDataVO<>(eo));
					allMandatoryColumnsByStateIdMap.get(mandatoryColumnVO.getState()).add(mandatoryColumnVO);
				});

		allRoleAttributeGroupPermissionsByStateIdMap.entrySet().parallelStream().forEach(entry -> {
					StateVO.UserRights userRights = allStatesMap.get(entry.getKey()).getUserRights();
					entry.getValue().forEach(
							permission -> {
								userRights.addValue(permission.getRoleUID(), permission);
							});
				});

		allSubformPermissionsByStateIdMap.entrySet().parallelStream().forEach(entry -> {
					StateVO.UserSubformRights userSubformRights = allStatesMap.get(entry.getKey()).getUserSubformRights();
					entry.getValue().forEach(
							permission -> {
								Set<SubformGroupPermissionVO> subformGroupPermissions = allSubformGroupPermissionsBySubformPermissionIdMap.get(permission.getPrimaryKey());
								permission.setGroupPermissions(Collections.unmodifiableSet(subformGroupPermissions));
								userSubformRights.addValue(permission.getRole(), permission);
							}
					);
				});

		allMandatoryFieldsByStateIdMap.entrySet().parallelStream().forEach(entry -> {
					allStatesMap.get(entry.getKey()).setMandatoryFields(Collections.unmodifiableSet(entry.getValue()));
				});

		allMandatoryColumnsByStateIdMap.entrySet().parallelStream().forEach(entry -> {
					allStatesMap.get(entry.getKey()).setMandatoryColumns(Collections.unmodifiableSet(entry.getValue()));
				});

		return allStatesMap;
	}

	private String getBestStateLocaleResourceFromMap(Map<LocaleResourceKey, String> allStateRelevantResources, Locale locale, String sResourceId) {
		String result = "State";
		if (Locale.GERMAN.equals(locale)) {
			result = allStateRelevantResources.get(new LocaleResourceKey(sResourceId, GERMAN_TAG));
			if (result == null) {
				result = allStateRelevantResources.get(new LocaleResourceKey(sResourceId, ENGLISH_TAG));
			}
		} else {
			result = allStateRelevantResources.get(new LocaleResourceKey(sResourceId, ENGLISH_TAG));
			if (result == null) {
				result = allStateRelevantResources.get(new LocaleResourceKey(sResourceId, GERMAN_TAG));
			}
		}
		return result;
	}

	private Map<LocaleResourceKey, String> getAllStateRelevantResourcesFromDb() {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		DbQuery<String> qLabelRes = builder.createQuery(String.class);
		DbFrom<UID> fLabelRes = qLabelRes.from(E.STATE, "labres");
		qLabelRes.select(fLabelRes.baseColumn(E.STATE.labelres));
		qLabelRes.where(builder.isNotNull(fLabelRes.baseColumn(E.STATE.labelres)));

		DbQuery<String> qDescRes = builder.createQuery(String.class);
		DbFrom<UID> fDescRes = qDescRes.from(E.STATE, "descres");
		qDescRes.select(fDescRes.baseColumn(E.STATE.descriptionres));
		qDescRes.where(builder.isNotNull(fDescRes.baseColumn(E.STATE.descriptionres)));

		DbQuery<String> qButtonRes = builder.createQuery(String.class);
		DbFrom<UID> fButtonRes = qButtonRes.from(E.STATE, "buttonres");
		qButtonRes.select(fButtonRes.baseColumn(E.STATE.buttonRes));
		qButtonRes.where(builder.isNotNull(fButtonRes.baseColumn(E.STATE.buttonRes)));

		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.LOCALERESOURCE);
		query.multiselect(t.baseColumn(E.LOCALERESOURCE.resourceID), t.baseColumn(E.LOCALERESOURCE.locale), t.baseColumn(E.LOCALERESOURCE.text));
		query.where(builder.or(
				builder.in(t.baseColumn(E.LOCALERESOURCE.resourceID), qLabelRes),
				builder.in(t.baseColumn(E.LOCALERESOURCE.resourceID), qDescRes),
				builder.in(t.baseColumn(E.LOCALERESOURCE.resourceID), qButtonRes)
		));

		return dataBaseHelper.getDbAccess().executeQuery(query).parallelStream().collect(Collectors.toMap(
				tuple -> new LocaleResourceKey(tuple.get(0, String.class), tuple.get(1, String.class)),
				tuple -> tuple.get(2, String.class)
		));
	}

	private Map<LocaleResourceKey, String> getAllTransitionRelevantResourcesFromDb() {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		DbQuery<String> qLabelRes = builder.createQuery(String.class);
		DbFrom<UID> fLabelRes = qLabelRes.from(E.STATETRANSITION, "labres");
		qLabelRes.select(fLabelRes.baseColumn(E.STATETRANSITION.labelres));
		qLabelRes.where(builder.isNotNull(fLabelRes.baseColumn(E.STATETRANSITION.labelres)));

		DbQuery<String> qDescRes = builder.createQuery(String.class);
		DbFrom<UID> fDescRes = qDescRes.from(E.STATETRANSITION, "descres");
		qDescRes.select(fDescRes.baseColumn(E.STATETRANSITION.descriptionres));
		qDescRes.where(builder.isNotNull(fDescRes.baseColumn(E.STATETRANSITION.descriptionres)));

		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.LOCALERESOURCE);
		query.multiselect(t.baseColumn(E.LOCALERESOURCE.resourceID), t.baseColumn(E.LOCALERESOURCE.locale), t.baseColumn(E.LOCALERESOURCE.text));
		query.where(builder.or(
				builder.in(t.baseColumn(E.LOCALERESOURCE.resourceID), qLabelRes),
				builder.in(t.baseColumn(E.LOCALERESOURCE.resourceID), qDescRes)
		));

		return dataBaseHelper.getDbAccess().executeQuery(query).parallelStream().collect(Collectors.toMap(
				tuple -> new LocaleResourceKey(tuple.get(0, String.class), tuple.get(1, String.class)),
				tuple -> tuple.get(2, String.class)
		));
	}

	private static class LocaleResourceKey extends Pair<String, String> {

		public LocaleResourceKey(final String resourceId, final String locale) {
			super(resourceId, locale);
		}

		public String getResourceId() {
			return super.getX();
		}

		public String getLocale() {
			return super.getY();
		}
	}

	@Cacheable("allTransitionsMap")
	public Map<UID, StateTransitionVO> getAllTransitionsMap() {
		List<EntityObjectVO<UID>> allTransitions = nuclosDalProvider.getEntityObjectProcessor(E.STATETRANSITION).getAll();
		List<EntityObjectVO<UID>> allRulesTransitions = nuclosDalProvider.getEntityObjectProcessor(E.SERVERCODETRANSITION).getAll();
		List<EntityObjectVO<UID>> allRoleTransitions = nuclosDalProvider.getEntityObjectProcessor(E.ROLETRANSITION).getAll();

		Map<UID, ConcurrentLinkedQueue<EventSupportTransitionVO>> allRulesByTransitionIdMap = createInitialMapWithConcurrentList(allTransitions);
		Map<UID, ConcurrentLinkedQueue<UID>> allRolesByTransitionIdMap = createInitialMapWithConcurrentList(allTransitions);

		Map<LocaleResourceKey, String> allTransitionRelevantResources = getAllTransitionRelevantResourcesFromDb();

		Map<UID, StateTransitionVO> allTransitionsMap = Collections.unmodifiableMap(allTransitions.parallelStream().collect(
				Collectors.toMap(
						eo -> eo.getPrimaryKey(),
						eo ->  {
							StateTransitionVO stVo = MasterDataWrapper.getStateTransitionVOWithoutDependants(new MasterDataVO<>(eo));
							if (stVo.getResourceIdForLabel() != null) {
								stVo.setLabel(Locale.GERMAN, getBestStateLocaleResourceFromMap(allTransitionRelevantResources, Locale.GERMAN, stVo.getResourceIdForLabel()));
								stVo.setLabel(Locale.ENGLISH, getBestStateLocaleResourceFromMap(allTransitionRelevantResources, Locale.ENGLISH, stVo.getResourceIdForLabel()));
							}
							if (stVo.getResourceIdForDescription() != null) {
								stVo.setDescription(Locale.GERMAN, getBestStateLocaleResourceFromMap(allTransitionRelevantResources, Locale.GERMAN, stVo.getResourceIdForDescription()));
								stVo.setDescription(Locale.ENGLISH, getBestStateLocaleResourceFromMap(allTransitionRelevantResources, Locale.ENGLISH, stVo.getResourceIdForDescription()));
							}
							return stVo;
						})));

		allRulesTransitions.parallelStream().forEach(eo -> {
			EventSupportTransitionVO eventSupportTransitionVO = MasterDataWrapper.getEventSupportTransitionVO(new MasterDataVO<>(eo));
			final ConcurrentLinkedQueue<EventSupportTransitionVO> rules = allRulesByTransitionIdMap.get(eventSupportTransitionVO.getTransition());
			if (rules != null) {
				rules.add(eventSupportTransitionVO);
			}
		});

		allRoleTransitions.parallelStream().forEach(eo -> {
			final ConcurrentLinkedQueue<UID> roles = allRolesByTransitionIdMap.get(eo.getFieldUid(E.ROLETRANSITION.transition));
			if (roles != null) {
				roles.add(eo.getFieldUid(E.ROLETRANSITION.role));
			}
		});

		allRulesByTransitionIdMap.entrySet().parallelStream().forEach(entry -> {
					StateTransitionVO transitionVO = allTransitionsMap.get(entry.getKey());
					ArrayList<EventSupportTransitionVO> rulesList = Lists.newArrayList(entry.getValue().iterator());
					// Sort the rules by intorder
					Collections.sort(rulesList, new Comparator<EventSupportTransitionVO>() {
						@Override
						public int compare(EventSupportTransitionVO o1, EventSupportTransitionVO o2) {
							return o1.getOrder().compareTo(o2.getOrder());
						}
					});
					transitionVO.setRules(Collections.unmodifiableList(rulesList));
				});

		allRolesByTransitionIdMap.entrySet().parallelStream().forEach(entry -> {
					StateTransitionVO transitionVO = allTransitionsMap.get(entry.getKey());
					transitionVO.setRoleUIDs(Collections.unmodifiableList(Lists.newArrayList(entry.getValue().iterator())));
				});

		return allTransitionsMap;
	}

	public StateTransitionVO getTransitionById(@NotNull final UID transitionId) throws CommonFinderException {
		StateTransitionVO result = getAllTransitionsMap().get(transitionId);
		if (result == null) {
			throw new CommonFinderException(String.format("StateTransition with ID %s does not exist", transitionId));
		}
		return result;
	}

	/**
	 * get a single State by UID
	 * @param stateId
	 * @return StateVO with given UID
	 */
	public StateVO getStateById(final UID stateId) throws CommonFinderException {
		StateVO result = getAllStatesMap().get(stateId);
		if (result == null) {
			throw new CommonFinderException(String.format("State with ID %s does not exist", stateId));
		}
		return result;
	}

	@Cacheable(value="stateGraphById", key="#p0.getString()")
	public StateGraphVO getStateGraphById(@NotNull final UID modelId) throws CommonFinderException {
		final StateModelVO stateModel = getModelById(modelId);
		final StateGraphVO result = new StateGraphVO(stateModel);

		//get states (with attributegroup permissions) and transitions for state model
		result.setStates(Collections.unmodifiableSet(new HashSet<>(getStatesByModelId(modelId))));
		result.setTransitions(Collections.unmodifiableSet(new HashSet<>(getTransitionsByModelId(modelId))));

		if (stateModel.getLayout() == null) {
			stateModel.setLayout(StateGraphVO.newLayoutInfo(result));
		}

		return result;
	}

	@Cacheable(value="stateModels")
	public List<StateModelVO> getModels() {
		return Collections.unmodifiableList(
				nuclosDalProvider.getEntityObjectProcessor(E.STATEMODEL).getAll().parallelStream().map(eoStateModel ->
						MasterDataWrapper.getStateModelVO(new MasterDataVO<UID>(eoStateModel), null)).collect(Collectors.toList()));
	}

	@Cacheable(value="stateModelById", key="#p0.getString()")
	public StateModelVO getModelById(@NotNull final UID modelId) throws CommonFinderException {
		Optional<StateModelVO> first = getModels().parallelStream().filter(eoStateModel -> eoStateModel.getId().equals(modelId)).findFirst();
		StateModelVO modelVO = null;
		try {
			modelVO = first.get();
			if (modelVO == null) {
				throw new NoSuchElementException();
			}
		} catch (NoSuchElementException ex) {
			throw new CommonFinderException(String.format("StateModel with ID %s does not exist", modelId));
		}
		return modelVO;
	}

	public StateModelVO getModelByState(@NotNull StateVO state) throws CommonFinderException {
		return getModelById(state.getModelUID());
	}

	@Cacheable(value="statesByTransitionId", key="#p0.getString()")
	public Collection<StateVO> getStatesByTransitionId(@NotNull final UID transitionId) throws CommonFinderException {
		StateTransitionVO transition = getTransitionById(transitionId);
		return Collections.unmodifiableList(
				getAllStatesMap().values().parallelStream().filter(state ->
						state.getId().equals(transition.getStateSourceUID())
								|| state.getId().equals(transition.getStateTargetUID())).collect(Collectors.toList()));
	}

	@Cacheable(value="stateTransitionsByStateId", key="#p0.getString()")
	public List<StateTransitionVO> getTransitionsByStateId(@NotNull final UID stateId) {
		return Collections.unmodifiableList(
				getAllTransitionsMap().values().parallelStream().filter(transition ->
						stateId.equals(transition.getStateSourceUID())
								|| stateId.equals(transition.getStateTargetUID())).collect(Collectors.toList()));
	}

	@Cacheable(value="stateTransitionsBySourceStateId", key="#p0.getString()")
	public List<StateTransitionVO> getTransitionsBySourceStateId(@NotNull final UID stateId) {
		return Collections.unmodifiableList(
				getAllTransitionsMap().values().parallelStream().filter(transition ->
						stateId.equals(transition.getStateSourceUID())).collect(Collectors.toList()));
	}

	@Cacheable(value="stateTransitionsByTargetStateId", key="#p0.getString()")
	public List<StateTransitionVO> getTransitionsByTargetStateId(@NotNull final UID stateId) {
		return Collections.unmodifiableList(
				getAllTransitionsMap().values().parallelStream().filter(transition ->
						stateId.equals(transition.getStateTargetUID())).collect(Collectors.toList()));
	}

	/**
	 * get a Collection of StateVO by modelUid
	 * 
	 * @param modelId
	 * @return unmodifiable Collection&lt;StateVO&gt; of all states in model with given UID
	 */
	@Cacheable(value="statesByModelId", key="#p0.getString()")
	public Collection<StateVO> getStatesByModelId(@NotNull final UID modelId) {
		return Collections.unmodifiableList(getAllStatesMap().values().parallelStream().filter(state -> modelId.equals(state.getModelUID())).collect(Collectors.toList()));
	}

	/**
	 * get a Collection of StateTransitionVO by modelUid
	 *
	 * @param modelId
	 * @return unmodifiable Collection&lt;StateTransitionVO&gt; of all transitions in model with given UID
	 */
	@Cacheable(value="stateTransitionsByModelId", key="#p0.getString()")
	public Collection<StateTransitionVO> getTransitionsByModelId(@NotNull final UID modelId) {
		final Map<UID, StateVO> allStatesMap = getAllStatesMap();
		return Collections.unmodifiableList(getAllTransitionsMap().values().parallelStream().filter(transition ->
				transition.getStateTargetUID() != null &&
				allStatesMap.get(transition.getStateTargetUID()) != null &&
				Optional.ofNullable(allStatesMap.get(transition.getStateTargetUID()).getModelUID())
						.map(uid -> uid.equals(modelId)).orElse(false)
		).collect(Collectors.toList()));
	}

	@Cacheable(value="initialStateTransitionByModelId", key="#p0.getString()")
	public StateTransitionVO getInitialTransitionByModelId(@NotNull final UID modelId) throws CommonFinderException {
		Optional<StateTransitionVO> first = getTransitionsByModelId(modelId).stream().filter(transition -> transition.getStateSourceUID() == null).findFirst();
		if (!first.isPresent()) {
			throw new CommonFinderException(String.format("Initial StateTransition for Model with ID %s not found", modelId));
		}
		return first.get();
	}

	@Cacheable(value="orderedStateTransitionsByModelId", key="#p0.getString()")
	public List<StateTransitionVO> getOrderedTransitionsByModelId(@NotNull final UID modelId) {
		Map<UID, StateVO> allStatesMap = getAllStatesMap();
		List<StateTransitionVO> transitions = new ArrayList<>(getTransitionsByModelId(modelId));
		transitions.sort(new Comparator<StateTransitionVO>() {
			@Override
			public int compare(final StateTransitionVO o1, final StateTransitionVO o2) {
				if (o1.getStateSourceUID() == null && o2.getStateSourceUID() != null) {
					return -1;
				}
				if (o1.getStateSourceUID() != null && o2.getStateSourceUID() == null) {
					return 1;
				}
				if (RigidUtils.equal(o1.getStateSourceUID(), o2.getStateSourceUID())) {
					StateVO target1 = allStatesMap.get(o1.getStateTargetUID());
					StateVO target2 = allStatesMap.get(o2.getStateTargetUID());
					return target1.getNumeral().compareTo(target2.getNumeral());
				}
				if (o1.getStateSourceUID() != null && o2.getStateSourceUID() != null) {
					StateVO source1 = allStatesMap.get(o1.getStateSourceUID());
					StateVO source2 = allStatesMap.get(o2.getStateSourceUID());
					return source1.getNumeral().compareTo(source2.getNumeral());
				}
				return 0;
			}
		});
		return Collections.unmodifiableList(transitions);
	}

	@Cacheable(value="stateResourceIdsForStateId", key="#p0.getString()")
	public Map<FieldMeta<?>, String> getResourceIdsForStateId(@NotNull final UID stateId) throws CommonFinderException {
		StateVO state = getStateById(stateId);
		Map<FieldMeta<?>, String> mpResIds = new HashMap<FieldMeta<?>, String>();
		mpResIds.put(E.STATE.labelres, state.getResourceIdForLabel());
		mpResIds.put(E.STATE.descriptionres, state.getResourceIdForDescription());
		mpResIds.put(E.STATE.buttonRes, state.getResourceIdForButton());
		return Collections.unmodifiableMap(mpResIds);
	}

	@Cacheable(value="stateModelUsages")
	public List<StateModelUsageVO> getModelUsages() {
		// StateModelUsages is should be ordered (taken from old StateModelUsagesCache.build)
		CollectableSearchExpression usageSearchExpression = new CollectableSearchExpression();
		usageSearchExpression.setSortingOrder(Arrays.asList(
				new CollectableSorting(E.STATEMODELUSAGE.nuclos_module.getUID(), true),
				new CollectableSorting(E.STATEMODELUSAGE.process.getUID(), true)));
		List<EntityObjectVO<UID>> allStateModelUsages = nucletDalProvider.getEntityObjectProcessor(E.STATEMODELUSAGE).getBySearchExpression(usageSearchExpression);

		StateModelUsages result = new StateModelUsages();
		return Collections.unmodifiableList(allStateModelUsages.stream().map(eo -> {
			UID modelId = eo.getFieldUid(E.STATEMODELUSAGE.statemodel);
			UsageCriteria uc = new UsageCriteria(eo.getFieldUid(E.STATEMODELUSAGE.nuclos_module), eo.getFieldUid(E.STATEMODELUSAGE.process), null, null);
			UID initialStateId = null;
			try {
				initialStateId = getInitialTransitionByModelId(modelId).getStateTargetUID();
			} catch (CommonFinderException e) {
				LOG.error(e.getMessage(), e);
			}
			return new StateModelUsageVO(modelId, initialStateId, uc);
		}).collect(Collectors.toList()));
	}

	@Cacheable(value="allStateModelUsagesObject")
	public StateModelUsages getAllModelUsagesObject() {
		StateModelUsages result = new StateModelUsages();
		getModelUsages().forEach(modelUsage -> {result.add(modelUsage);});
		return result;
	}

	@Cacheable(value="statesByEntityId", key="#p0.getString()")
	public Collection<StateVO> getStatesByEntityId(@NotNull final UID entityId) {
		// special case for general search (all states for all modules)
		if (entityId == null) {
			return Collections.unmodifiableCollection(getAllStatesMap().values());
		}

		return Collections.unmodifiableList(getModelUsages().stream()
				.filter(modelUsage -> entityId.equals(modelUsage.getUsageCriteria().getEntityUID()))
				.map(modelUsage -> modelUsage.getStateModelUID())
				.distinct()
				.map(modelId -> getStatesByModelId(modelId))
				.flatMap(states -> states.stream())
				.collect(Collectors.toList()));
	}

	@Cacheable(value="hasDefaultPathByStateId", key="#p0.getString()")
	public boolean hasModelDefaultPathByStateId(@NotNull final UID stateId) throws CommonFinderException {
		final UID modelUID = getStateById(stateId).getModelUID();
		return getModelDefaultPath(modelUID) != null;
	}

	/**
	 * Determines the default path if configured.
	 * A way back is also determined if it exists.
	 * If no path exists or could not be determined, null is returned.
	 *
	 * @param modelId
	 * @return
	 * @throws CommonFinderException
	 */
	@Cacheable(value="defaultPathByModelId", key="#p0.getString()")
	public List<StatePathElement> getModelDefaultPath(@NotNull final UID modelId) throws CommonFinderException {
		final Collection<StateTransitionVO>  allTransitions = getTransitionsByModelId(modelId);
		if (allTransitions.stream().anyMatch(StateTransitionVO::isDefault)) {
			final List<StateTransitionVO> completePathUnordered = allTransitions.stream()
					.filter(StateTransitionVO::isDefault)
					.filter(t -> t.getStateSourceUID() != null && !UID.UID_NULL.equals(t.getStateSourceUID()))
					.collect(Collectors.toList());
			final Optional<StateTransitionVO> firstTransition = completePathUnordered.stream()
					.filter(t -> completePathUnordered.stream()
							.noneMatch(t2 -> t2.getStateTargetUID().equals(t.getStateSourceUID())))
					.findFirst();
			if (firstTransition.isPresent()) {
				final StateTransitionVO firstTransitionVO = firstTransition.get();
				List<StatePathElement> result = new ArrayList<>();
				result.add(new StatePathElement(
						null, // back
						getStateById(firstTransitionVO.getStateSourceUID()),
						null) // self
				);
				int i; do {
					i = result.size();
					final StateVO lastStateVO = result.get(result.size() - 1).state;
					final Optional<StateTransitionVO> nextTransition = completePathUnordered.stream()
							.filter(t -> t.getStateSourceUID().equals(lastStateVO.getId()))
							.findFirst();
					if (nextTransition.isPresent()) {
						final StateTransitionVO nextTransitionVO = nextTransition.get();
						final StateVO nextStateVO = getStateById(nextTransitionVO.getStateTargetUID());
						if (!result.contains(nextStateVO)) { // <- only to prevent endless loops
							result.add(new StatePathElement(
									allTransitions.stream().filter(t -> // find way back (if any)
											Objects.equals(nextTransitionVO.getStateSourceUID(), t.getStateTargetUID())
													&& Objects.equals(nextTransitionVO.getStateTargetUID(), t.getStateSourceUID())).findFirst().orElse(null),
									nextStateVO,
									nextTransitionVO) // self
							);
						}
					}
				} while (i < result.size());
				return result;
			}
		}
		return null;
	}

	@Cacheable(value="statusPath", key="#p0.getString() + #p1 + #p2.toString()", condition="#p0 != null && #p2 != null")
	public StatusPath getStatusPath(UID requestedStateUID, Integer maxElements, Locale locale) throws CommonFinderException {
		if (requestedStateUID == null || locale == null) {
			throw new IllegalArgumentException(String.format("requestedStateUID(%s) and locale(%s) must not be null", requestedStateUID, locale));
		}
		final int iMaxElements = ObjectUtils.defaultIfNull(maxElements, 4);
		final UID modelUID = getStateById(requestedStateUID).getModelUID();
		final List<StatePathElement> modelDefaultPath = getModelDefaultPath(modelUID);
		if (modelDefaultPath != null) {
			final List<StatePathElement> completeStatusPath = new ArrayList<>(modelDefaultPath);
			final UID focusUID;
			final boolean bEnd;
			final boolean bOutside;
			UID wayBackStateUID = null;
			if (completeStatusPath.stream().noneMatch(e -> Objects.equals(e.state.getId(), requestedStateUID))) {
				// outside the standard path, try to find next (first/best) way back ..
				final Collection<StateVO> allStatesByModel = getStatesByModelId(modelUID);
				final Map<UID, Integer> mapStateToNumeral = allStatesByModel.stream().collect(Collectors.toMap(StateVO::getId, StateVO::getNumeral));
				final Integer iReqNum = mapStateToNumeral.get(requestedStateUID);
				final Collection<StateTransitionVO> allTransitionsByModel = getTransitionsByModelId(modelUID);
				final List<StateTransitionVO> allOutgoingTransitionsByReqState = getTransitionsBySourceStateId(requestedStateUID)
						.stream().sorted(new Comparator<StateTransitionVO>() {
							@Override
							public int compare(final StateTransitionVO t1, final StateTransitionVO t2) {
								final Integer iTargetNum1 = mapStateToNumeral.get(t1.getStateTargetUID());
								final Integer iTargetNum2 = mapStateToNumeral.get(t2.getStateTargetUID());
								return iTargetNum1.compareTo(iTargetNum2);
							}
						}).collect(Collectors.toList());
				final Optional<StateTransitionVO> bestWayBack = allOutgoingTransitionsByReqState.stream()
						.filter(t -> mapStateToNumeral.get(t.getStateTargetUID()) > iReqNum)
						.filter(t -> completeStatusPath.stream().anyMatch(e -> Objects.equals(e.state.getId(), t.getStateTargetUID())))
						.findFirst();
				if (bestWayBack.isPresent()) {
					focusUID = bestWayBack.get().getStateTargetUID();
					wayBackStateUID = focusUID;
				} else {
					final Optional<StateTransitionVO> firstWayBack = allOutgoingTransitionsByReqState.stream()
							.filter(t -> completeStatusPath.stream().anyMatch(e -> Objects.equals(e.state.getId(), t.getStateTargetUID())))
							.findFirst();
					if (firstWayBack.isPresent()) {
						focusUID = firstWayBack.get().getStateTargetUID();
						wayBackStateUID = focusUID;
					} else {
						// no best/first way back, try to find an alternative path ...
						final List<StateTransitionVO> allOutgoingTransitionsWithHigherNum = allOutgoingTransitionsByReqState.stream().filter(t -> {
							try {
								return getStateById(t.getStateTargetUID()).getNumeral() > getStateById(requestedStateUID).getNumeral();
							} catch (CommonFinderException e) {
								return false;
							}
						}).collect(Collectors.toList());
						if (allOutgoingTransitionsByReqState.size() == 1
								|| allOutgoingTransitionsWithHigherNum.size() == 1) {
							// alternative path found ...
							completeStatusPath.clear();
							StateTransitionVO nextAltTransitionVO = allOutgoingTransitionsByReqState.size() == 1 ?
									allOutgoingTransitionsByReqState.get(0):
									allOutgoingTransitionsWithHigherNum.get(0);
							completeStatusPath.add(new StatePathElement(
									null, // back
									getStateById(nextAltTransitionVO.getStateSourceUID()),
									null) // self
							);
							Set<UID> alternativePathStateUIDs = new HashSet<>(); // endless loop prevention
							int i; do {
								i = alternativePathStateUIDs.size();
								final UID nextStateTargetUID = nextAltTransitionVO.getStateTargetUID();
								if (!alternativePathStateUIDs.contains(nextStateTargetUID)) {
									alternativePathStateUIDs.add(nextStateTargetUID);
									completeStatusPath.add(new StatePathElement(
											null, // back
											getStateById(nextStateTargetUID),
											nextAltTransitionVO) // self
									);
									final List<StateTransitionVO> allNextTargetTransitions = allTransitionsByModel.stream()
											.filter(t -> Objects.equals(nextStateTargetUID, t.getStateSourceUID()))
											.collect(Collectors.toList());
									final List<StateTransitionVO> allNextTargetTransitionsWithHigherNum = allNextTargetTransitions.stream().filter(t -> {
										try {
											return getStateById(t.getStateTargetUID()).getNumeral() > getStateById(nextStateTargetUID).getNumeral();
										} catch (CommonFinderException e) {
											return false;
										}
									}).collect(Collectors.toList());
									if (allNextTargetTransitions.size() == 1) {
										nextAltTransitionVO = allNextTargetTransitions.get(0);
									} else if (allNextTargetTransitionsWithHigherNum.size() == 1) {
										nextAltTransitionVO = allNextTargetTransitionsWithHigherNum.get(0);
									}
								}
							} while (i < alternativePathStateUIDs.size());
							focusUID = requestedStateUID;
						} else {
							focusUID = null;
						}
					}
				}
				bEnd = allTransitionsByModel.stream().noneMatch(t -> Objects.equals(requestedStateUID, t.getStateSourceUID()));
				bOutside = true;
			} else {
				focusUID = requestedStateUID;
				final StatePathElement lastElement = completeStatusPath.get(completeStatusPath.size() - 1);
				bEnd = Objects.equals(requestedStateUID, lastElement.state.getId()) && lastElement.back == null;
				bOutside = false;
			}
			List<StatePathElement> elementsBefore = new ArrayList<>();
			List<StatePathElement> elementsAfter = new ArrayList<>();
			StatePathElement focusState = null;
			for (StatePathElement e : completeStatusPath) {
				if (Objects.equals(focusUID, e.state.getId())) {
					focusState = e;
				} else {
					if (focusState == null) {
						elementsBefore.add(e);
					} else {
						elementsAfter.add(e);
					}
				}
			}
			final StatusPath.Builder<Void> pathBuilder = StatusPath.builder();
			pathBuilder.withOutside(bOutside)
					.withEnd(bEnd);
			if (focusState != null) {
				final UID finalWayBackStateUID = wayBackStateUID;
				if (bOutside) {
					pathBuilder.withWayBack(finalWayBackStateUID != null);
				}
				final int iMinBefore = elementsBefore.isEmpty() ? 0 : 1;
				final int iCountAfter = Math.min(elementsAfter.size(), iMaxElements - iMinBefore - 1);
				final int iCountBefore = Math.min(elementsBefore.size(), iMaxElements - iCountAfter - 1);
				final List<StatePathElement> resultElements = new ArrayList<>();
				final Map<UID, StateTransitionVO> statusInfoTransitionMap = new HashMap<>();
				resultElements.addAll(elementsBefore.subList(elementsBefore.size() - iCountBefore, elementsBefore.size()));
				resultElements.add(focusState);
				// Note: the focus element does not contain any transition values...
				// get statusInfo-transitions "back" (beforeFocus1..n + focus):
				for (int i = 1; i < resultElements.size(); i++) {
					StatePathElement e = resultElements.get(i);
					if (e.back != null) {
						statusInfoTransitionMap.put(e.back.getStateTargetUID(), e.back);
					}
				}
				final List<StatePathElement> statePathNextElements = elementsAfter.subList(0, iCountAfter);
				// get statusInfo-transitions "next" (afterFocus0..n):
				for (int i = 0; i < statePathNextElements.size(); i++) {
					StatePathElement e = statePathNextElements.get(i);
					if (e.transition != null) {
						statusInfoTransitionMap.put(e.transition.getStateTargetUID(), e.transition);
					}
				}
				resultElements.addAll(statePathNextElements);
				pathBuilder.withMoreBefore(iCountBefore < elementsBefore.size())
						.withMoreAfter(iCountAfter < elementsAfter.size())
						.addInfos(resultElements.stream()
								.map(e -> {
									StateTransitionVO transition = statusInfoTransitionMap.get(e.state.getId());
									return StatusInfo.builder()
										.withWayBack(Objects.equals(finalWayBackStateUID, e.state.getId()) ? true : null)
										.withStateId(Rest.translateUid(E.STATE, e.state.getId()))
										.withNumeral(e.state.getNumeral().longValue())
										.withStatename(e.state.getStatename(locale))
										.withDescription(StringUtils.defaultIfLooksEmpty(e.state.getDescription(locale), null))
										.withButtonLabel(StringUtils.defaultIfLooksEmpty(e.state.getButtonLabel(locale), null))
										.withButtonIcon(e.state.getButtonIcon() != null ? e.state.getButtonIcon().getId().getString() : null)
										.withColor(e.state.getColor())
										.withNonstop(Optional.ofNullable(transition).map(t -> t.isNonstop() ? true : null).orElse(null))
										.withTransitionLabel(Optional.ofNullable(transition).map(t -> StringUtils.defaultIfLooksEmpty(t.getLabel(locale), null)).orElse(null))
										.withTransitionDescription(Optional.ofNullable(transition).map(t -> StringUtils.defaultIfLooksEmpty(t.getDescription(locale), null)).orElse(null))
										.build();
								})
								.collect(Collectors.toList()));
			}
			return pathBuilder.build();
		}
		throw new CommonFinderException(String.format("Path for state %s does not exists", requestedStateUID));
	}

	private static <T> Map<UID, ConcurrentLinkedQueue<T>> createInitialMapWithConcurrentList(Collection<EntityObjectVO<UID>> colEo) {
		return Collections.unmodifiableMap(colEo.parallelStream().collect(
				Collectors.toMap(
						k -> k.getPrimaryKey(),
						v -> new ConcurrentLinkedQueue<>())));
	}

	private static <T> Map<UID, Set<T>> createInitialMapWithConcurrentSet(Collection<EntityObjectVO<UID>> colEo, boolean bUnmodifiable) {
		Map<UID, Set<T>> result = colEo.parallelStream().collect(
				Collectors.toMap(
						k -> k.getPrimaryKey(),
						v -> ConcurrentHashMap.newKeySet()));
		if (bUnmodifiable) {
			result = Collections.unmodifiableMap(result);
		}
		return result;
	}

	/**
	 * Invalidate the whole cache
	 */
	@Caching(evict = {
			@CacheEvict(value = "allStatesMap", allEntries = true),
			@CacheEvict(value = "allTransitionsMap", allEntries = true),
			@CacheEvict(value = "allStateModelUsagesObject", allEntries = true),
			@CacheEvict(value = "stateModels", allEntries = true),
			@CacheEvict(value = "stateModelUsages", allEntries = true),
			@CacheEvict(value = "statesByEntityId", allEntries = true),
			@CacheEvict(value = "statesByModelId", allEntries = true),
			@CacheEvict(value = "statesByTransitionId", allEntries = true),
			@CacheEvict(value = "stateTransitionsByStateId", allEntries = true),
			@CacheEvict(value = "stateTransitionsBySourceStateId", allEntries = true),
			@CacheEvict(value = "stateTransitionsByTargetStateId", allEntries = true),
			@CacheEvict(value = "stateTransitionsByModelId", allEntries = true),
			@CacheEvict(value = "stateResourceIdsForStateId", allEntries = true),
			@CacheEvict(value = "stateModelById", allEntries = true),
			@CacheEvict(value = "stateGraphById", allEntries = true),
			@CacheEvict(value = "initialStateTransitionByModelId", allEntries = true),
			@CacheEvict(value = "orderedStateTransitionsByModelId", allEntries = true),
			@CacheEvict(value = "defaultPathByModelId", allEntries = true),
			@CacheEvict(value = "hasDefaultPathByStateId", allEntries = true),
			@CacheEvict(value = "statusPath", allEntries = true)
	})
	protected void invalidate() {
		LOG.debug("Invalidating StateCache");
	}

	public void invalidateCache(final boolean notifyCluster) {
		invalidate();
		if (notifyCluster) {
			TransactionalClusterNotification.notifySecurityRelatedChangeForAllUsers();
		}
	}

}	// class StateCache
