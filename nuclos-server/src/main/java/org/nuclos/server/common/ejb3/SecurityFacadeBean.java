//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import static org.nuclos.common2.StringUtils.looksEmpty;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.businessentity.SsoAuth;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.LoginResult;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.RigidE;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.Version;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.security.InitialSecurityData;
import org.nuclos.common.security.Permission;
import org.nuclos.common.security.SsoAuthStatus;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.valuelistprovider.UserLoginRestriction;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.rest.SsoAuthEndpoint;
import org.nuclos.server.cluster.RigidClusterHelper;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.MasterDataPermissions;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ModulePermissions;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.communication.CommunicationInterfaceLocal;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.security.NuclosAuthenticationProvider;
import org.nuclos.server.security.NuclosSsoAuthenticationToken;
import org.nuclos.server.security.NuclosSsoUtils;
import org.nuclos.server.spring.NuclosWebApplicationInitializer;
import org.nuclos.server.web.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nimbusds.oauth2.sdk.AccessTokenResponse;
import com.nimbusds.oauth2.sdk.AuthorizationGrant;
import com.nimbusds.oauth2.sdk.AuthorizationRequest;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.RefreshTokenGrant;
import com.nimbusds.oauth2.sdk.ResponseType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.TokenRequest;
import com.nimbusds.oauth2.sdk.TokenResponse;
import com.nimbusds.oauth2.sdk.auth.ClientAuthentication;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.pkce.CodeChallengeMethod;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;
import com.nimbusds.openid.connect.sdk.LogoutRequest;

/**
 * Facade bean for searchfilter. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*/
@Transactional(noRollbackFor= {Exception.class})
@Component("securityService")
public class SecurityFacadeBean implements SecurityFacadeLocal, SecurityFacadeRemote {
	
	private static final Logger LOG = LoggerFactory.getLogger(SecurityFacadeBean.class);
	
	// Spring injection
	
	@Autowired
	private ServerParameterProvider serverParameterProvider;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private ApplicationProperties applicationProperties;
	
	@Autowired
	private NuclosUserDetailsContextHolder userCtx;
	
	@Autowired
	private SecurityCache securityCache;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	private final MetaProvider metaProv;

	// end of Spring injection

	public SecurityFacadeBean(
			final MetaProvider metaProv) {
		this.metaProv = metaProv;
	}

	@PostConstruct
	public void postConstruct() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		LOG.info("Authentication successful.");
		getCurrentApplicationInfoOnServer(); // prefill md5 info string.
		securityCache.initSsoSecurityObjectCache();

		// If there are still "active" sessions (e.g. because the server was killed), mark them as logged out
		writeLogoutProtocol();
	}

	@PreDestroy
	public void preDestroy() {
		// Server is shutting down -> mark all active sessions as logged out
		writeLogoutProtocol();
	}

	/**
	 * @deprecated Use {@link #getUserName()}
	 */
	private final String getCurrentUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
	}

	private Session getSession() {
		return SpringApplicationContextHolder.getBean(Session.class);
	}

	/**
	 * logs the current user in.
	 * @return session id for the current user
	 */
	@RolesAllowed("Login")
	public LoginResult login() {
		final String username = getUserName();
		try {
			getSession().setUsername(username);
		} catch (BeanCreationException e) {
			// sometimes we are NOT in a session here, e.g. job run
		}

		String jSessionId = null;
		try {
			jSessionId = getSession().getHttpSession().getId();
		}
		catch (Exception ex) {
			// Might not be available yet during server startup
		}

		final Long sessionId = writeLoginProtocol(username, jSessionId);

		/*
		 * If this was a remote login, save it as a web session,
		 * so the session can be also used via REST, even if the login
		 * was done via Rich Client remote invocation.
		 */
		if (jSessionId != null) {
			setWebSessionContext(jSessionId, sessionId);
		}

		if (getSession() != null) {
			try {
				final HttpSession httpSession = getSession().getHttpSession();
				if (httpSession != null) {
					int maxAge = serverParameterProvider.getIntValue(
							ParameterProvider.SECURITY_SESSION_TIMEOUT_IN_SECONDS,
							NuclosWebApplicationInitializer.DEFAULT_SESSION_TIMEOUT
					);
					httpSession.setMaxInactiveInterval(maxAge);
				}
			} catch (BeanCreationException e) {
				// Probably a local server session, no HttpSession - ignore
			}
		}

		return new LoginResult(sessionId, jSessionId);
	}

	private void setWebSessionContext(final String jSessionId, final Long sessionId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		securityCache.setSessionContext(
				auth,
				LocaleContextHolder.getLocale(),
				jSessionId,
				sessionId
		);
	}

	/**
	 * logs the current user out.
	 * @param sessionId session id for the current user
	 */
	@RolesAllowed("Login")
	public void logout(Long sessionId) {
		writeLogoutProtocol(sessionId);
		performLogout();
	}

	/**
	 * logs the current user out.
	 *
	 * @param sessionId session id of the container for the current user
	 */
	@RolesAllowed("Login")
	public void logout(final String sessionId) {
		writeLogoutProtocol(sessionId);
		performLogout();
	}

	/**
	 * Performs the logout, i.e. invalidates sessions.
	 */
	private void performLogout() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && auth.getPrincipal() != null) {
			final String username =  auth.getPrincipal().toString();
			LOG.info("User {} logged out.", username);
		}
		getSession().invalidate();
	}

	/**
	 * @return information about the current version of the application installed on the server.
	 */
    @RolesAllowed("Login")
	public Version getCurrentApplicationVersionOnServer() {
		return applicationProperties.getNuclosVersion();
	}

    private String sApplicationInfo = null;
    public String getCurrentApplicationInfoOnServer() {
    	if (sApplicationInfo == null) {
	    	try {
	        	//@todo mixup ip of current server. - not only database. @see NUCLOS-1223
	        	String info = UUID.randomUUID().toString();//SpringDataBaseHelper.getInstance().getCurrentConnectionInfo();
	        	sApplicationInfo = new String(Hex.encode(MessageDigest.getInstance("MD5").digest(info.getBytes())));
			} catch (Exception e) {
				throw new NuclosFatalException(e);
			}
    	}
    	return sApplicationInfo;
    }
    
	/**
	 * @return information about the current version of the application installed on the server.
	 */
    @RolesAllowed("Login")
	public String getUserName() {
		return getCurrentUserName();
	}

	/**
	 * Get all actions that are allowed for the current user.
	 * @return set that contains the Actions objects (no duplicates).
	 */
    @RolesAllowed("Login")
	public Set<String> getAllowedActions() {
		return securityCache.getAllowedActions(getCurrentUserName(), getCurrentMandatorUID());
	}
    
    /**
	 * Get all actions that are allowed for the current user.
	 * @return set that contains the Actions objects (no duplicates).
	 */
    @RolesAllowed("Login")
	public Set<UID> getAllowedCustomActions() {
		return securityCache.getAllowedCustomActions(getCurrentUserName(), getCurrentMandatorUID());
	}

	/**
	 * @return the module permissions for the current user.
	 */
    @RolesAllowed("Login")
	public ModulePermissions getModulePermissions() {
		return securityCache.getModulePermissions(getCurrentUserName(), getCurrentMandatorUID());
	}

	/**
	 * @return the masterdata permissions for the current user.
	 */
    @RolesAllowed("Login")
	public MasterDataPermissions getMasterDataPermissions() {
		return securityCache.getMasterDataPermissions(getCurrentUserName(), getCurrentMandatorUID());
	}
    
    /**
     * @return all accessible mandators for the current user by level.
     * @throws CommonPermissionException 
     */
    @RolesAllowed("Login")
    public Map<UID, List<MandatorVO>> getAccessibleMandatorsByLevel() throws CommonPermissionException {
    	Map<UID, List<MandatorVO>> result = new HashMap<UID, List<MandatorVO>>();
    	SecurityCache secCache = SecurityCache.getInstance();
    	secCache.checkMandatorLoginPermission(getCurrentUserName(), getCurrentMandatorUID());
    	for (UID mandatorUID : secCache.getAccessibleMandators(this.getCurrentMandatorUID())) {
    		MandatorVO mandator = secCache.getMandator(mandatorUID);
    		if (!result.containsKey(mandator.getLevelUID())) {
    			result.put(mandator.getLevelUID(), new ArrayList<MandatorVO>());
    		}
    		result.get(mandator.getLevelUID()).add(mandator);
    	}
    	for (Map.Entry<UID, List<MandatorVO>> entry : result.entrySet()) {
    		Collections.sort(entry.getValue());
    		entry.setValue(Collections.unmodifiableList(entry.getValue()));
    	}
    	return Collections.unmodifiableMap(result);
    }

	/**
	 * get a String representation of the users session context
	 */
    @RolesAllowed("Login")
	public String getSessionContextAsString() {
		return getCurrentUserName();
	}

	/**
	 * write the successful login into the protocol table
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = {Exception.class})
	private Long writeLoginProtocol(final String sUserName, final String sessionId) {
		try {
			final String sApplicationName = applicationProperties.getName();

			IExtendedEntityObjectProcessor<Long> eoProcessor = nucletDalProvider.getEntityObjectProcessor(RigidE.SESSION);

			EntityObjectVO<Long> eo = new EntityObjectVO<Long>(E.SESSION);
			eo.flagNew();
			eo.setPrimaryKey(dataBaseHelper.getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE));
			eo.setFieldValue(E.SESSION.SESSION_ID, RigidUtils.defaultIfNull(sessionId, Boolean.FALSE.toString()));
			eo.setFieldValue(E.SESSION.USER_ID, sUserName);
			eo.setFieldValue(E.SESSION.APPLICATION, sApplicationName);
			eo.setFieldValue(E.SESSION.CLUSTER_SERVER_ID, RigidClusterHelper.getNodeIdAsString(Boolean.FALSE.toString()));
			eo.setFieldValue(E.SESSION.LOGON, InternalTimestamp.toInternalTimestamp(new Date()));
			eo.setFieldValue(SF.CREATEDBY, "nuclos");
			eo.setFieldValue(SF.CHANGEDBY, "nuclos");
			eo.setFieldValue(SF.CREATEDAT, InternalTimestamp.toInternalTimestamp(new Date()));
			eo.setFieldValue(SF.CHANGEDAT, InternalTimestamp.toInternalTimestamp(new Date()));
			eo.setFieldValue(SF.VERSION, 1);

			eoProcessor.insertOrUpdate(eo);

			return eo.getPrimaryKey();
		}
		catch (Exception ex) {
			throw new CommonFatalException("Could not write login protocol", ex);//"Konnte Login nicht protokollieren."
		}
	}

	/**
	 * write the successful logout into the protocol table
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = {Exception.class})
	private void writeLogoutProtocol(final Long intid) {
		// Bean is @Transactional -> no need for PersistentDbAccess here
		dataBaseHelper.getDbAccess().execute(DbStatementUtils.updateValues(E.SESSION,
				E.SESSION.LOGOFF, InternalTimestamp.toInternalTimestamp(new Date())
		).where(E.SESSION.getPk(), intid));
	}

	/**
	 * write the successful logout into the protocol table
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = {Exception.class})
	private void writeLogoutProtocol(final String sessionId) {
		if (sessionId != null) {
			// Bean is @Transactional -> no need for PersistentDbAccess here
			dataBaseHelper.getDbAccess().execute(
					DbStatementUtils.updateValues(
							E.SESSION,
							E.SESSION.LOGOFF,
							InternalTimestamp.toInternalTimestamp(new Date())
					).where(
							E.SESSION.SESSION_ID, sessionId,
							E.SESSION.CLUSTER_SERVER_ID, RigidClusterHelper.getNodeIdAsString(Boolean.FALSE.toString()))
			);
		}
	}

	/**
	 * Writes the current time as logout time for all currently active sessions.
	 * May be cluster node specific, when activated.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = {Exception.class})
	private void writeLogoutProtocol() {
		// Bean is @Transactional -> no need for PersistentDbAccess here
		final DbAccess dbAccess = dataBaseHelper.getDbAccess();
		if (dbAccess == null) {
			// During JUnit tests
			return;
		}
		dbAccess.execute(
				DbStatementUtils.updateValues(
						E.SESSION,
						E.SESSION.LOGOFF,
						InternalTimestamp.toInternalTimestamp(new Date())
				).where(
						E.SESSION.LOGOFF, null,
						E.SESSION.CLUSTER_SERVER_ID, RigidClusterHelper.getNodeIdAsString(Boolean.FALSE.toString())));
	}

	/**
	 * get the user id from the database
	 * @param sUserName the user username in lower case characters
	 * @return
	 * @throws CommonFatalException
	 */
	public UID getUserUid(final String sUserName) throws CommonFatalException {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.USER);
		query.select(t.basePk());
		query.where(builder.equal(builder.upper(t.baseColumn(E.USER.username)), builder.upper(builder.literal(sUserName))));
		UID executeQuerySingleResult = null;
		try{
			executeQuerySingleResult = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
		} catch (DbInvalidResultSizeException e){
			throw new CommonFatalException("Could not find user "+sUserName);
		}
		return executeQuerySingleResult;
	}

	/**
	 * @return the readable subforms
	 */
    public Map<UID, SubformPermission> getSubFormPermission(final UID entityUid) {
		return securityCache.getSubForm(getCurrentUserName(), entityUid, getCurrentMandatorUID());
	}

	/**
	 * determine the permission of an attribute regarding the state of a module for the current user
	 */
    public Permission getAttributePermission(UID entityUid, UID attributeUid, UID stateUid) {
		// special behaviour for modules which have no statusnumeral (e.g. Rechnungsabschnitte)
		  if (stateUid == null) {
		   return Permission.READWRITE;
		  }
		  final UID attributeGroupUid = AttributeCache.getInstance().getAttribute(attributeUid).getAttributegroupUID();
		  final Map<UID, Permission> mpAttributePermission = securityCache.getAttributeGroup(getCurrentUserName(), entityUid, attributeGroupUid, getCurrentMandatorUID());

		  return mpAttributePermission.get(stateUid);
	}

	public Map<UID, Permission> getAttributePermissionsByEntity(UID entityUid, UID stateUid) {
		HashMap<UID, Permission> res = new HashMap<UID, Permission>();
		String user = getCurrentUserName();

		Map<UID, FieldMeta<?>> fields = metaProv.getAllEntityFieldsByEntity(entityUid);
		for(Map.Entry<UID, FieldMeta<?>> e : fields.entrySet()) {
			Permission p;
			if(stateUid == null) {
				p = Permission.READWRITE;
			}
			else {
				p = securityCache.getAttributeGroup(
						user,
						entityUid,
						e.getValue().getFieldGroup(), getCurrentMandatorUID())
					.get(stateUid);
			}
			res.put(e.getKey(), p);
		}
		return res;
	}

    @RolesAllowed("Login")
	public Boolean isSuperUser() {
		return securityCache.isSuperUser(getCurrentUserName());
	}

    @RolesAllowed("Login")
    public Boolean isLoginRestricted(UserLoginRestriction userLoginRestriction) {
        return securityCache.isUserLoginRestricted(getCurrentUserName(), userLoginRestriction);
    }

    @RolesAllowed("Login")
	public void invalidateCache(){
		SecurityCache.getInstance().invalidate(true);
	}

    @RolesAllowed("Login")
    public InitialSecurityData getInitialSecurityData() {
		InitialSecurityData iniData = new InitialSecurityData();
		String userName = getCurrentUserName();
		UID mandatorUid = getCurrentMandatorUID();
		iniData.setUserName(userName);
		iniData.setUserUid(securityCache.getUserUid(userName));
		iniData.setSuperUser(isSuperUser());
		iniData.setWithCommunicationAccount(Boolean.valueOf(securityCache.getUserCommunicationPhoneAccount(userName) != null));
		iniData.setAllowedActions(getAllowedActions());
		iniData.setAllowedCustomActions(getAllowedCustomActions());
		iniData.setModulePermissions(getModulePermissions());
		iniData.setMasterDataPermissions(getMasterDataPermissions());
		iniData.setDynamicTasklists(securityCache.getDynamicTasklists(userName, mandatorUid));
		iniData.setUserRoles(securityCache.getUserRolesWithName(userName, mandatorUid));
		try {
			iniData.setMandatoryByLevel(getAccessibleMandatorsByLevel());
		} catch (CommonPermissionException e) {
			throw new NuclosFatalException(e);
		}
		return iniData;
    }

    @Override
	public boolean isSsoAuthenticationActive() {
		return SecurityContextHolder.getContext().getAuthentication() instanceof NuclosSsoAuthenticationToken;
	}

	public Boolean isLdapAuthenticationActive() {
    	return SpringApplicationContextHolder.getBean(NuclosAuthenticationProvider.class).hasActiveLdapBindAuthenticator();
	}

	public Boolean isLdapSynchronizationActive() {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.LDAPSERVER);
		query.select(t.basePk());
		query.where(builder.and(builder.equalValue(t.baseColumn(E.LDAPSERVER.active), true), builder.isNotNull(t.baseColumn(E.LDAPSERVER.serversearchfilter))));

		return dataBaseHelper.getDbAccess().executeQuery(query).size() > 0;
	}

	public Date getPasswordExpiration() {
		Integer interval = 0;
		if (serverParameterProvider.getValue(ParameterProvider.KEY_SECURITY_PASSWORD_INTERVAL) != null) {
			interval = serverParameterProvider.getIntValue(ParameterProvider.KEY_SECURITY_PASSWORD_INTERVAL, 0);
		}

		if (interval == 0) {
			return null;
		}

		String username = getCurrentUserName();

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.USER);
		query.multiselect(t.baseColumn(E.USER.lastPasswordChange), t.baseColumn(E.USER.superuser));
		query.where(builder.equal(builder.upper(t.baseColumn(E.USER.username)), builder.upper(builder.literal(username))));
		DbTuple tuple = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);

		Date d = tuple.get(0, Date.class);
		Boolean isSuperUser = Boolean.TRUE.equals(tuple.get(1, Boolean.class));
		if (d != null && (!isLdapAuthenticationActive() || isSuperUser)) {
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			c.add(Calendar.DAY_OF_MONTH, interval);
			return c.getTime();
		}
		return null;
	}

	private final UID getCurrentMandatorUID() {
		return userCtx.getMandatorUID();
	}
	
	@Override
	public List<MandatorVO> getMandators() {
		return SecurityCache.getInstance().getAssignedMandators(getCurrentUserName());
	}
	
	@Override
	public void checkMandatorPermission() throws CommonPermissionException {
		try {
			SecurityCache.getInstance().checkMandatorLoginPermission(getCurrentUserName(), getCurrentMandatorUID());
		} catch (CommonPermissionException permex) {
			// too early for injection
			String localizedError = SpringLocaleDelegate.getInstance().getText(permex.getMessage());
			throw new CommonPermissionException(localizedError);
		}
	}
	
	@Override
	public List<MandatorVO> getMandatorsByLevel(UID levelUID) {
		return SecurityCache.getInstance().getMandatorsByLevel(levelUID);
	}

	@Override
	public List<CollectableField> getUserCommunicationAccounts(UID userUID, String requestContextClassName) {
		List<CollectableField> result = new ArrayList<>();

		Class<?> requestContextClass = null;
		try {
			requestContextClass = Class.forName(requestContextClassName);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}

		if(securityCache.isWriteAllowedForMasterData(getUserName(), E.USER.getUID(), userCtx.getMandatorUID())) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom<UID> t = query.from(E.USER_COMMUNICATION_ACCOUNT);
			query.multiselect(t.basePk(), t.baseColumn(E.USER_COMMUNICATION_ACCOUNT.account), t.baseColumn(E.USER_COMMUNICATION_ACCOUNT.communicationPort));
			query.where(builder.equalValue(t.baseColumn(E.USER_COMMUNICATION_ACCOUNT.user), userUID));
			
			CommunicationInterfaceLocal interfaces = SpringApplicationContextHolder.getBean(CommunicationInterfaceLocal.class);
			
			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
				UID accountUID = tuple.get(E.USER_COMMUNICATION_ACCOUNT.getPk());
				String accountLogin = tuple.get(E.USER_COMMUNICATION_ACCOUNT.account);
				UID portUID = tuple.get(E.USER_COMMUNICATION_ACCOUNT.communicationPort);
				for (Class<? extends RequestContext<?>> supportedClass : interfaces.getSupportedRequestContextClasses(portUID)) {
					if (supportedClass.equals(requestContextClass)) {
						
						DbQuery<String> q2 = builder.createQuery(String.class);
						DbFrom<UID> t2 = q2.from(E.COMMUNICATION_PORT);
						q2.select(t2.baseColumn(E.COMMUNICATION_PORT.portName));
						q2.where(builder.equalValue(t2.basePk(), portUID));
						String portName = dataBaseHelper.getDbAccess().executeQuerySingleResult(q2);
						
						CollectableValueIdField clctValue = new CollectableValueIdField(accountUID, portName + " (" + accountLogin + ")");
						result.add(clctValue);
					}
				}
			}
		}
		
		return result;
	}

	@Override
	public List<SsoAuthEndpoint> getSsoAuthEndpoints() {
		String jSessionId = getSession().getHttpSession().getId();
		if (jSessionId == null) {
			throw new IllegalStateException("No http session");
		}
		return securityCache.getActiveSsoAuths().stream().map(ssoAuth ->
				SsoAuthEndpoint.builder()
					.withSsoAuthId(ssoAuth.getId().getString())
					.withLabel(ssoAuth.getName())
					.withColor(ssoAuth.getColor())
					.withImageIcon(ssoAuth.getImageIcon() == null ? null : Base64.encodeBase64String(ssoAuth.getImageIcon().getContent()))
					.withFontAwesomeIcon(ssoAuth.getFontAwesomeIcon())
					.build())
			.collect(Collectors.toList());
	}

	@Override
	public URI startNuclosSsoAuthentication(UID ssoAuthUID) {
		String jSessionId = getSession().getHttpSession().getId();
		if (jSessionId == null) {
			throw new IllegalStateException("No http session");
		}
    	if (ssoAuthUID == null) {
    		throw new IllegalArgumentException("ssoAuthUID must not be null");
		}
		SsoAuth ssoAuth = securityCache.getSsoAuth(ssoAuthUID);
		if (ssoAuth == null || !Boolean.TRUE.equals(ssoAuth.getActive())) {
			throw new NuclosWebException(Response.Status.UNAUTHORIZED, "SSO Authentication with ID \"" + ssoAuthUID + "\" not found or disabled");
		}
		if (ssoAuth.getOAuth2Endpoint() == null ||
			ssoAuth.getOAuth2TokenEndpoint() == null ||
			ssoAuth.getOAuth2ClientId() == null) {
			throw new IllegalArgumentException("Not all required SSO setting have been configured (Endpoint(s), Client ID)");
		}

		ClientID clientID = new ClientID(ssoAuth.getOAuth2ClientId());
		URI callback = null;
		if (!looksEmpty(ssoAuth.getOAuth2Callback())) {
			try {
				callback = new URI(ssoAuth.getOAuth2Callback());
			} catch (URISyntaxException ex) {
				NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, ex, "OAuth2Callback is not a malformed URL");
			}
		}
		Scope scope = null;
		if (!looksEmpty(ssoAuth.getOAuth2Scopes())) {
			scope = new Scope(ssoAuth.getOAuth2Scopes().split(","));
		}
		SecurityCache.NuclosSsoSecurityObject nsso = securityCache.setNuclosSsoAuthStatusInitiated(ssoAuth);

		AuthorizationRequest request = null;
		try {
			AuthorizationRequest.Builder builder = new AuthorizationRequest.Builder(new ResponseType(ResponseType.Value.CODE), clientID)
					.scope(scope)
					.state(nsso.getOAuth2State())
					.codeChallenge(nsso.getCodeVerifier(), CodeChallengeMethod.S256)
					.redirectionURI(callback)
					.endpointURI(new URI(ssoAuth.getOAuth2Endpoint()));

			if (Boolean.TRUE.equals(ssoAuth.getOidcIdTokenNonce())) {
				//builder.nonce(nsso.getOidcNonce()) <--- Missing?!
				builder.customParameter("nonce", nsso.getOidcNonce().getValue());
			}

			if (!looksEmpty(ssoAuth.getAcrValues())) {
				builder.customParameter("acr_values", ssoAuth.getAcrValues());
			}

			request = builder.build();
		} catch (URISyntaxException ex) {
			NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, ex, "OAuth2Endpoint is not a malformed URL");
		}

		return request.toURI();
	}

	@Override
	public SsoAuthStatus checkNuclosSsoAuthenticationStatus() {
		SecurityCache.NuclosSsoSecurityObject nsso = securityCache.getNuclosSsoSecurityObject();
		if (nsso.getNuclosSsoAuthStatus() == SsoAuthStatus.LOGGED_IN) {
			Authentication auth = nsso.getSpringAuthentication();
			if (auth != null) {
				SecurityContextHolder.getContext().setAuthentication(auth);
			}
		}
		return nsso.getNuclosSsoAuthStatus();
	}

	@Override
	public void refreshSsoAuthentication(@NotNull NuclosSsoAuthenticationToken authToken) {
		String jSessionId = getSession().getHttpSession().getId();
		if (jSessionId == null) {
			throw new IllegalStateException("No http session");
		}
		if (authToken.getRefreshToken() == null) {
			throw new IllegalArgumentException("Session is not refreshable. Missing refresh token!");
		}
		SsoAuth ssoAuth = securityCache.getSsoAuth(authToken.getSsoAuthUID());
		if (ssoAuth == null || !Boolean.TRUE.equals(ssoAuth.getActive())) {
			throw new NuclosWebException(Response.Status.UNAUTHORIZED, "SSO Authentication with ID \"" + authToken.getSsoAuthUID() + "\" not found or disabled");
		}
		if (ssoAuth.getOAuth2TokenEndpoint() == null || ssoAuth.getOAuth2ClientId() == null) {
			throw new NuclosWebException(Response.Status.UNAUTHORIZED, "SSO service is missing a token endpoint and/or client ID");
		}
		synchronized (authToken) {
			if (!authToken.isAccessTokenValid()) { // check again, in case of parallel token refreshs.
				try {
					AuthorizationGrant refreshTokenGrant = new RefreshTokenGrant(authToken.getRefreshToken());

					URI tokenEndpoint = NuclosSsoUtils.getTokenEndpoint(LOG, ssoAuth, jSessionId);

					// The credentials to authenticate the client at the token endpoint
					ClientID clientID = new ClientID(ssoAuth.getOAuth2ClientId());
					String sClientSecret = ssoAuth.getOAuth2ClientSecret();

					// Make the token request
					TokenRequest request = null;
					if (looksEmpty(sClientSecret)) {
						request = new TokenRequest(tokenEndpoint, clientID, refreshTokenGrant);
					} else {
						Secret clientSecret = new Secret(sClientSecret);
						ClientAuthentication clientAuth = new ClientSecretBasic(clientID, clientSecret);
						request = new TokenRequest(tokenEndpoint, clientAuth, refreshTokenGrant);
					}

					TokenResponse response = null;
					try {
						response = TokenResponse.parse(request.toHTTPRequest().send());
					} catch (ParseException ex) {
						throw NuclosSsoUtils.logAndGetException(LOG, ssoAuth, jSessionId, ex, "Refresh token response is not parsable");
					} catch (IOException ex) {
						throw NuclosSsoUtils.logAndGetException(LOG, ssoAuth, jSessionId, ex, "Refresh token response could not be send");
					}

					if (!response.indicatesSuccess()) {
						// The tokenRequest failed, e.g. due to invalid or expired token
						NuclosSsoUtils.throwException(response.toErrorResponse(), String.format("token request against \"%s\"", tokenEndpoint));
					}

					AccessTokenResponse successResponse = response.toSuccessResponse();

					// Get the access token, the refresh token may be updated
					AccessToken accessToken = successResponse.getTokens().getAccessToken();
					RefreshToken refreshToken = successResponse.getTokens().getRefreshToken();

					authToken.updateTokens(accessToken, refreshToken);

				} catch (RuntimeException rex) {
					NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, rex, "");
				}
			}
		}
	}

	@Override
	public String getSsoLogoutUri() {
		String jSessionId = getSession().getHttpSession().getId();
		if (jSessionId == null) {
			throw new IllegalStateException("No http session");
		}
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth instanceof NuclosSsoAuthenticationToken) {
			NuclosSsoAuthenticationToken authToken = (NuclosSsoAuthenticationToken) auth;
			SsoAuth ssoAuth = securityCache.getSsoAuth(authToken.getSsoAuthUID());
			if (ssoAuth == null || !Boolean.TRUE.equals(ssoAuth.getActive())) {
				throw new NuclosWebException(Response.Status.UNAUTHORIZED, "SSO Authentication with ID \"" + authToken.getSsoAuthUID() + "\" not found or disabled");
			}
			if (authToken.getIdToken() != null && !looksEmpty(ssoAuth.getOidcEndSessionEndpoint())) {
				URI endSessionEndpoint = NuclosSsoUtils.getEndSessionEndpoint(LOG, ssoAuth, jSessionId);
				URI callback = null;
				try {
					if (ssoAuth.getOAuth2Callback().endsWith("/")) {
						callback = new URI(ssoAuth.getOAuth2Callback() + "logout");
					} else {
						callback = new URI(ssoAuth.getOAuth2Callback() + "/logout");
					}
				} catch (URISyntaxException ex) {
					NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, ex, "OAuth2Callback is not a malformed URL");
				}
				LogoutRequest request = new LogoutRequest(endSessionEndpoint, authToken.getIdToken(), callback, authToken.getLogoutState());
				return request.toURI().toString();
			}
		}
		return null;
	}

}
