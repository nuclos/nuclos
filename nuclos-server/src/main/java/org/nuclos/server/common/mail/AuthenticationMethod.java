package org.nuclos.server.common.mail;

public enum AuthenticationMethod {
	NONE("None"),
	PASSWORD("Password"),
	OAUTH2("OAuth2");


	private final String value;

	AuthenticationMethod(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
