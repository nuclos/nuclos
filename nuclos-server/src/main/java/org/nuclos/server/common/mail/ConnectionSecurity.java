package org.nuclos.server.common.mail;

public enum ConnectionSecurity {
	NONE("None"),
	STARTTLS("STARTTLS"),
	SSL("SSL/TLS");

	private final String value;

	ConnectionSecurity(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
