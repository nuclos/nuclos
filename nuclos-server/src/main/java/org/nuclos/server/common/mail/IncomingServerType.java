package org.nuclos.server.common.mail;

public enum IncomingServerType {
	IMAP,
	POP3
}
