package org.nuclos.server.common.mail;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.mail.NuclosMail;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.service.MailService;
import org.nuclos.businessentity.EmailIncomingServer;
import org.nuclos.businessentity.EmailOutgoingServer;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common2.exception.MailException;
import org.nuclos.common2.exception.MailReceiveException;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.mail.properties.IMAPConnectionProperties;
import org.nuclos.server.common.mail.properties.MailConnectionProperties;
import org.nuclos.server.common.mail.properties.POP3ConnectionProperties;
import org.nuclos.server.common.mail.properties.SMTPConnectionProperties;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.security.NuclosSsoUtils;
import org.springframework.stereotype.Component;

@Component("mailServiceProvider")
public class NuclosMailServiceProvider implements MailService {

	private final ServerParameterProvider serverParameterProvider;
	private final NucletDalProvider dalProvider;
	private final NuclosUserDetailsContextHolder userCtx;

	public NuclosMailServiceProvider(
			final ServerParameterProvider serverParameterProvider,
			final NucletDalProvider dalProvider,
			final NuclosUserDetailsContextHolder userCtx) {
		this.serverParameterProvider = serverParameterProvider;
		this.dalProvider = dalProvider;
		this.userCtx = userCtx;
	}

	private MailConnectionProperties getConnectionPropertiesForReceiving(UID uidIncomingServer) {
		final IExtendedEntityObjectProcessor<UID> eopEmailIncoming = this.dalProvider.getEntityObjectProcessor(E.EMAILINCOMINGSERVER);

		MailConnectionProperties properties;
		final EmailIncomingServer server;

		if (uidIncomingServer == null) {
			List<UID> incomingServers = new ArrayList<>();

			CollectableSearchCondition cndActive = SearchConditionUtils.newComparison(E.EMAILINCOMINGSERVER.active, ComparisonOperator.EQUAL, true);

			if (userCtx.getMandatorUID() != null) {
				final CollectableSearchCondition searchCondition = new CollectableSubCondition(
					E.EMAILINCOMINGMANDATOR.getUID(),
					E.EMAILINCOMINGMANDATOR.incomingServer.getUID(),
					SearchConditionUtils.newUidComparison(E.EMAILINCOMINGMANDATOR.mandator, ComparisonOperator.EQUAL, userCtx.getMandatorUID()));
				incomingServers = eopEmailIncoming.getIdsBySearchExpression(new CollectableSearchExpression(SearchConditionUtils.and(searchCondition, cndActive), CollectableSorting.createSorting(E.EMAILINCOMINGSERVER.order.getUID(), true)));
			}

			if (incomingServers.isEmpty()) {
				final CollectableSearchCondition scHasMandators = new CollectableSubCondition(
						E.EMAILINCOMINGMANDATOR.getUID(),
						E.EMAILINCOMINGMANDATOR.incomingServer.getUID(),
						TrueCondition.TRUE);
				incomingServers = eopEmailIncoming.getIdsBySearchExpression(new CollectableSearchExpression(SearchConditionUtils.and(cndActive, SearchConditionUtils.not(scHasMandators)), CollectableSorting.createSorting(E.EMAILINCOMINGSERVER.order.getUID(), true)));
			}

			if (!incomingServers.isEmpty()) {
				server = EmailIncomingServer.get(incomingServers.get(0));
			} else {
				server = null;
			}
		} else {
			server = EmailIncomingServer.get(uidIncomingServer);
		}

		if (server != null) {
			boolean usingIMAP = IncomingServerType.IMAP.equals(IncomingServerType.valueOf(server.getType()));


			if (!usingIMAP) {
				properties = new POP3ConnectionProperties();
			} else {
				properties = new IMAPConnectionProperties();
				properties.setFolderFrom(server.getFolderFrom());
				properties.setFolderTo(server.getFolderTo());
			}
			properties.setHost(server.getServerName());
			properties.setPort(server.getPort());

			properties.setUser(server.getUserName());
			if (AuthenticationMethod.PASSWORD.getValue().equals(server.getAuthenticationMethod())) {
				properties.setPassword(server.getPassword());
			} else if (AuthenticationMethod.OAUTH2.getValue().equals(server.getAuthenticationMethod())) {
				properties.setUseOAuth2(true);
				properties.setPassword(getOAuth2AccessToken(server));
			}

			final String connectionSecurity = server.getConnectionSecurity();
			if (ConnectionSecurity.STARTTLS.getValue().equals(connectionSecurity)) {
				properties.setStartTls(true);
			} else if (ConnectionSecurity.SSL.getValue().equals(connectionSecurity)) {
				properties.setSsl(true);
			}
		} else {
			// use of system parameters is deprecated
			String authIMAP = serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_PROTOCOL);
			boolean usingIMAP = (authIMAP != null && (authIMAP.equalsIgnoreCase("Y") || authIMAP.equalsIgnoreCase("Yes"))) ? true : false;

			if (!usingIMAP) {
				properties = new POP3ConnectionProperties();
				properties.setUser(serverParameterProvider.getValue(ParameterProvider.KEY_POP3_USERNAME));
				properties.setPassword(serverParameterProvider.getValue(ParameterProvider.KEY_POP3_PASSWORD));
				properties.setHost(serverParameterProvider.getValue(ParameterProvider.KEY_POP3_SERVER));
				properties.setPort(serverParameterProvider.getIntValue(ParameterProvider.KEY_POP3_PORT, 110));
			} else {
				properties = new IMAPConnectionProperties();
				properties.setUser(serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_USERNAME));
				properties.setPassword(serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_PASSWORD));
				properties.setHost(serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_SERVER));
				properties.setPort(serverParameterProvider.getIntValue(ParameterProvider.KEY_IMAP_PORT, 143));
				properties.setFolderFrom(serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_FOLDER_FROM));
				properties.setFolderTo(serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_FOLDER_TO));
			}

			properties.setSsl(serverParameterProvider.isEnabled(ParameterProvider.KEY_MAILGET_SSL));
		}

		return properties;
	}

	private SMTPConnectionProperties getConnectionPropertiesForSending(final UID uidOutgoingServer) {
		final IExtendedEntityObjectProcessor<UID> eopEmailOutgoing = this.dalProvider.getEntityObjectProcessor(E.EMAILOUTGOINGSERVER);

		SMTPConnectionProperties properties = new SMTPConnectionProperties();
		final EmailOutgoingServer server;

		if (uidOutgoingServer == null) {
			final Query<EmailOutgoingServer> query = QueryProvider.create(EmailOutgoingServer.class).where(EmailOutgoingServer.Active.eq(true));
			List<UID> outgoingServers = new ArrayList<>();

			CollectableSearchCondition cndActive = SearchConditionUtils.newComparison(E.EMAILOUTGOINGSERVER.active, ComparisonOperator.EQUAL, true);

			if (userCtx.getMandatorUID() != null) {
				final CollectableSearchCondition searchCondition = new CollectableSubCondition(
					E.EMAILOUTGOINGMANDATOR.getUID(),
					E.EMAILOUTGOINGMANDATOR.outgoingServer.getUID(),
					SearchConditionUtils.newUidComparison(E.EMAILOUTGOINGMANDATOR.mandator, ComparisonOperator.EQUAL, userCtx.getMandatorUID()));
				outgoingServers = eopEmailOutgoing.getIdsBySearchExpression(new CollectableSearchExpression(SearchConditionUtils.and(searchCondition, cndActive), CollectableSorting.createSorting(E.EMAILOUTGOINGSERVER.order.getUID(), true)));
			}

			if (outgoingServers.isEmpty()) {
				final CollectableSearchCondition scHasMandators = new CollectableSubCondition(
						E.EMAILOUTGOINGMANDATOR.getUID(),
						E.EMAILOUTGOINGMANDATOR.outgoingServer.getUID(),
						TrueCondition.TRUE);
				outgoingServers = eopEmailOutgoing.getIdsBySearchExpression(new CollectableSearchExpression(SearchConditionUtils.and(cndActive, SearchConditionUtils.not(scHasMandators)), CollectableSorting.createSorting(E.EMAILOUTGOINGSERVER.order.getUID(), true)));
			}

			if (!outgoingServers.isEmpty()) {
				server = EmailOutgoingServer.get(outgoingServers.get(0));
			} else {
				server = null;
			}
		} else {
			server = EmailOutgoingServer.get(uidOutgoingServer);
		}

		if (server != null) {
			properties.setUser(server.getUserName());
			if (AuthenticationMethod.PASSWORD.getValue().equals(server.getAuthenticationMethod())) {
				properties.setPassword(server.getPassword());
			} else if (AuthenticationMethod.OAUTH2.getValue().equals(server.getAuthenticationMethod())) {
				properties.setUseOAuth2(true);
				properties.setPassword(getOAuth2AccessToken(server));
			}

			final String connectionSecurity = server.getConnectionSecurity();
			if (ConnectionSecurity.STARTTLS.getValue().equals(connectionSecurity)) {
				properties.setStartTls(true);
			} else if (ConnectionSecurity.SSL.getValue().equals(connectionSecurity)) {
				properties.setSsl(true);
			}
			properties.setHost(server.getServerName());
			properties.setPort(server.getPort());
			properties.setSender(server.getSender());
		} else {
			// use of system parameters is deprecated
			boolean auth = serverParameterProvider.isEnabled(ParameterProvider.KEY_SMTP_AUTHENTICATION);
			properties.setStartTls(serverParameterProvider.isEnabled(ParameterProvider.KEY_SMTP_STARTTLS));
			properties.setSsl(serverParameterProvider.isEnabled(ParameterProvider.KEY_SMTP_SSL));
			properties.setHost(serverParameterProvider.getValue(ParameterProvider.KEY_SMTP_SERVER));
			properties.setPort(serverParameterProvider.getIntValue(ParameterProvider.KEY_SMTP_PORT, 25));
			properties.setSender(serverParameterProvider.getValue(ParameterProvider.KEY_SMTP_SENDER));

			if (auth) {
				properties.setUser(serverParameterProvider.getValue(ParameterProvider.KEY_SMTP_USERNAME));
				properties.setPassword(serverParameterProvider.getValue(ParameterProvider.KEY_SMTP_PASSWORD));
			}
		}

		return properties;
	}

	@Override
	public void send(NuclosMail mail) throws BusinessException {
		send(null, mail);
	}

	@Override
	public void send(org.nuclos.api.UID uidOutgoingServer, NuclosMail mail) throws BusinessException {
		try {
			send((UID) uidOutgoingServer, new org.nuclos.common.mail.NuclosMail(mail));
		} catch (MailException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void send(InputStream is) throws BusinessException {
		send(null, is);
	}

	@Override
	public void send(org.nuclos.api.UID uidOutgoingServer, InputStream is) throws BusinessException {
		final NuclosMailSender nuclosMailSender = new NuclosMailSender(getConnectionPropertiesForSending((UID) uidOutgoingServer));
		try {
			nuclosMailSender.sendMail(is);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void transformToEml(NuclosMail mail, OutputStream outputStream) throws BusinessException {
		//TODO signature
		final SMTPConnectionProperties properties = getConnectionPropertiesForSending(null);

		final String sender = properties.getSender();
		final String emailSignature = ServerParameterProvider.getInstance().getValue(ParameterProvider.EMAIL_SIGNATURE);

		try {
			new NuclosMailTransformer().transformToEml(new org.nuclos.common.mail.NuclosMail(mail), outputStream, sender, emailSignature);
		} catch (IOException | MessagingException e) {
			throw new BusinessException(e);
		}
	}

	public void send(org.nuclos.common.mail.NuclosMail mail) throws MailException {
		send(null, mail);
	}

	public void send(UID uidOutgoingServer,  org.nuclos.common.mail.NuclosMail mail) throws MailException {
		final SMTPConnectionProperties properties = getConnectionPropertiesForSending(uidOutgoingServer);
		final NuclosMailSender nuclosMailSender = new NuclosMailSender(properties);
		final String sender = properties.getSender();
		// TODO signature
		final String emailSignature = ServerParameterProvider.getInstance().getValue(ParameterProvider.EMAIL_SIGNATURE);

		nuclosMailSender.sendMail(mail, sender, emailSignature);
	}

	@Override
	public List<NuclosMail> receive(boolean bDeleteMails)
			throws BusinessException {
		return receive(null, null, bDeleteMails);
	}

	@Override
	public List<NuclosMail> receive(org.nuclos.api.UID uidIncomingServer, boolean bDeleteMails)
			throws BusinessException {
		return receive(uidIncomingServer, null, bDeleteMails);
	}

	@Override
	public List<NuclosMail> receive(String folderFrom, boolean bDeleteMails) throws BusinessException {
		return receive(null, folderFrom, bDeleteMails);
	}

	@Override
	public List<NuclosMail> receive(org.nuclos.api.UID uidIncomingServer, String folderFrom, boolean bDeleteMails) throws BusinessException {
		try {
			final NuclosMailHandler nuclosMailHandler = new NuclosMailHandler(getConnectionPropertiesForReceiving((UID) uidIncomingServer));
			return nuclosMailHandler.receiveMails(folderFrom, bDeleteMails);
		} catch (MailReceiveException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Ideally there should be a real connection test whenever the relevant parameters are changed.
	 *
	 * @return true, if the necessary SMTP parameters for sending emails are set.
	 */
	public boolean isConfiguredForSending() {
		final SMTPConnectionProperties properties = getConnectionPropertiesForSending(null);
		return StringUtils.isNotBlank(properties.getHost()) && StringUtils.isNotBlank(properties.getSender());
	}

	private String getOAuth2AccessToken(EmailOutgoingServer server) {
		return NuclosSsoUtils.getOAuth2TokenForOutgoingEmail(server);
	}

	private String getOAuth2AccessToken(EmailIncomingServer server) {
		return NuclosSsoUtils.getOAuth2TokenForIncomingEmail(server);
	}
}
