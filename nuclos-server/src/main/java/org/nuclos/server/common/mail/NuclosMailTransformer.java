package org.nuclos.server.common.mail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.mail.NuclosMail;

public class NuclosMailTransformer {

	public NuclosMailTransformer() {

	}

	public void transformToEml(final NuclosMail mail, final OutputStream outputStream, final String sender, final String emailSignature) throws MessagingException, IOException {
		if (StringUtils.isBlank(mail.getFrom())) {
			mail.setFrom(sender);
		}

		MimeMultipart content = new MimeMultipart();
		MimeBodyPart text = getBodyPart(mail.getMessage(), mail);
		content.addBodyPart(text);

		if (!StringUtils.isBlank(emailSignature)) {
			MimeBodyPart sig = getBodyPart(emailSignature, mail);
			content.addBodyPart(sig);
		}

		Message msg = new MimeMessage((Session) null);
		msg.setFrom(new InternetAddress(mail.getFrom()));

		List<String> to = mail.getRecipients();
		InternetAddress[] recipients = new InternetAddress[to.size()];
		for (int i = 0; i < to.size(); i++) {
			recipients[i] = new InternetAddress(to.get(i));
		}
		msg.setRecipients(Message.RecipientType.TO, recipients);

		List<String> toCC = mail.getRecipientsCC();
		InternetAddress[] recipientsCC = new InternetAddress[toCC.size()];
		for (int i = 0; i < toCC.size(); i++) {
			recipientsCC[i] = new InternetAddress(toCC.get(i));
		}
		msg.setRecipients(Message.RecipientType.CC, recipientsCC);

		List<String> toBCC = mail.getRecipientsBCC();
		InternetAddress[] recipientsBCC = new InternetAddress[toBCC.size()];
		for (int i = 0; i < toBCC.size(); i++) {
			recipientsBCC[i] = new InternetAddress(toBCC.get(i));
		}
		msg.setRecipients(Message.RecipientType.BCC, recipientsBCC);

		if (mail.getReplyTo() != null) {
			InternetAddress[] replies = new InternetAddress[1];
			replies[0] = new InternetAddress(mail.getReplyTo());
			msg.setReplyTo(replies);
		}
		else {
			InternetAddress[] replies = new InternetAddress[1];
			replies[0] = new InternetAddress(mail.getFrom());
			msg.setReplyTo(replies);
		}

		msg.setSubject(removeLineBreaks(mail.getSubject()));
		msg.setContent(content);
		msg.setHeader("MIME-Version", "1.0");
		msg.setHeader("Content-Type", content.getContentType());

		for (Map.Entry<String, String> header : mail.getHeaders().entrySet()) {
			msg.setHeader(header.getKey(), header.getValue());
		}

		msg.setSentDate(new Date(System.currentTimeMillis()));

		for (org.nuclos.api.common.NuclosFile attachment : mail.getAttachments()) {
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			DataSource dataSource = new ByteArrayDataSource(attachment.getContent(), "application/octet-stream");
			messageBodyPart.setDataHandler(new DataHandler(dataSource));
			messageBodyPart.addHeader("Content-Type", "application/octet-stream ; charset= UTF-8");
			messageBodyPart.setFileName(attachment.getName());
			content.addBodyPart(messageBodyPart);
		}
		msg.writeTo(outputStream);
	}

	private static MimeBodyPart getBodyPart(String body, NuclosMail mail) throws MessagingException {
		MimeBodyPart text = new MimeBodyPart();

		text.setText(body, mail.getCharset() != null ? mail.getCharset() : "UTF-8");
		text.setHeader("MIME-Version","1.0");

		// set Content-Type
		String contentType = mail.getHeader("Content-Type");
		if (null == contentType) {
			// default text/plain
			contentType = "text/plain; charset=UTF-8";
		}
		if (mail.getCharset() != null && mail.getType() != null) {
			contentType = mail.getType() + "; charset=" + mail.getCharset();
		}

		text.setHeader("Content-Type", contentType);

		return text;
	}

	private static String removeLineBreaks(String input) {
		if (input == null) {
			return null;
		}
		return input.replace("\r","").replace("\n","");
	}

}
