package org.nuclos.server.common.mail.properties;

import java.util.Properties;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class IMAPConnectionProperties extends MailConnectionProperties {
	/**  */
	private static final long serialVersionUID = 1L;

	public IMAPConnectionProperties() {
		super("imap");

		setSocketFactoryClass("mail.imap.socketFactory.class");
	}

	public boolean isUsingIMAP() {
		return true;
	}

	@Override
	public Properties toProperties() {
		Properties p = new Properties();

		p.put("mail.imap.host", getHost());
		p.put("mail.imap.port", getPort());
		p.put("mail.imap.auth", "true");

		if (isStartTls()) {
			p.put("mail.imap.starttls.enable", "true");
			p.put("mail.imap.starttls.required", "true");
		}

		if (useOAuth2()) {
			p.put("mail.imap.auth.mechanisms", "XOAUTH2");
		}

		p.put("mail.imap.socketFactory.class", getSocketFactoryClass());
		p.put("mail.imap.connectiontimeout", getConnectionTimeout());
		p.put("mail.imap.timeout", getSocketTimeout());

		return p;
	}

	public void setUser(String user) {
		this.username = user;
	}
}
