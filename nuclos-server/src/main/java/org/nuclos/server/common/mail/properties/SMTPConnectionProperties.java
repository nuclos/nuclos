package org.nuclos.server.common.mail.properties;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import org.nuclos.common2.exception.MailException;
import org.nuclos.common2.exception.MailSendException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SMTPConnectionProperties extends MailConnectionProperties {
	/**  */
	private static final long serialVersionUID = 1L;

	private String sender;

	public SMTPConnectionProperties() {
		super("smtp");
	}

	public boolean isUsingIMAP() {
		return true;
	}

	@Override
	public Properties toProperties() throws MailException {
		final String smtpHost = getHost();
		final int smtpPort = getPort();
		final boolean isStartTls = isStartTls();
		final boolean isSsl = isSsl();
		final String login = getUser();
		final String password = getPassword();

		if (smtpHost == null) {
			throw new MailSendException("mailsender.error.3");
		}
		Properties properties = new Properties();
		properties.put("mail.smtp.host", smtpHost);
		properties.put("mail.smtp.port", smtpPort);

		if (isSsl) {
			properties.put("mail.smtp.ssl.enable", "true");
		}

		if (isStartTls) {
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.starttls.required", "true");
		}

		if (login != null) {
			properties.put("mail.smtp.user", login);
			properties.put("mail.smtp.auth", "true");

			if (useOAuth2()) {
				properties.put("mail.imap.auth.mechanisms", "XOAUTH2");
			}

			Authenticator auth = new Authenticator() {

				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(login, password);
				}
			};
			properties.put("auth", auth);
		}

		properties.put("mail.smtp.connectiontimeout", getConnectionTimeout());
		properties.put("mail.smtp.timeout", getSocketTimeout());

		return properties;
	}

	public void setUser(String user) {
		this.username = user;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(final String sender) {
		this.sender = sender;
	}
}
