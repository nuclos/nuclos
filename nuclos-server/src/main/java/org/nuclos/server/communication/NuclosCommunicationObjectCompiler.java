package org.nuclos.server.communication;

import org.nuclos.server.eventsupport.ejb3.SourceType;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler;
import org.springframework.stereotype.Component;

@Component
public class NuclosCommunicationObjectCompiler extends AbstractNuclosObjectCompiler {

	public NuclosCommunicationObjectCompiler() {
		super(SourceType.COMMUNICATION,
		      NuclosCodegeneratorConstants.COMMUNICATIONJARFILE,
		      NuclosCodegeneratorConstants.COMMUNICATION_SRC_DIR_NAME);
	}


}
