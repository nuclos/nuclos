//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.communication.context.impl;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.api.communication.CommunicationPort;
import org.nuclos.api.context.communication.GenericNotificationContext;

public class GenericNotificationContextImpl extends AbstractCommunicationContext implements GenericNotificationContext {
	
	private final Map<String, String> contentMap = new HashMap<>();
	
	public GenericNotificationContextImpl(CommunicationPort port, String logClassname) {
		super(port, logClassname);
	}
	
	/**
	 * 
	 * @return
	 */
	@Override
	public Map<String, String> getContentMap() {
		return contentMap;
	}

}
