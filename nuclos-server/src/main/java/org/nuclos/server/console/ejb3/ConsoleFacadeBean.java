//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.console.ejb3;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.ArithmeticUtils;
import org.nuclos.businessentity.DocumentDir;
import org.nuclos.businessentity.facade.WebAddonFacade;
import org.nuclos.common.Actions;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.ByteUnit;
import org.nuclos.common.CommandMessage;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.MandatorLevelVO;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.Priority;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.RuleNotification;
import org.nuclos.common.SF;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.PreferenceVO;
import org.nuclos.common.preferences.Preference;
import org.nuclos.common.querybuilder.DatasourceXMLParser;
import org.nuclos.common.querybuilder.DatasourceXMLTransformer;
import org.nuclos.common.report.JRPatchParameters;
import org.nuclos.common.report.JRUtils;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportParameterVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.remoting.NuclosRemotingInterface;
import org.nuclos.server.cluster.TransactionalClusterNotification;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.common.ejb3.PreferencesFacadeLocal;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.dblayer.query.DbUpdate;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.dblayer.structure.DbConstraint;
import org.nuclos.server.dblayer.structure.DbIndex;
import org.nuclos.server.dbtransfer.TransferFacadeBean;
import org.nuclos.server.documentfile.DocumentFileFacadeLocal;
import org.nuclos.server.documentfile.DocumentFileUtils;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.livesearch.ejb3.LiveSearchFacadeLocal;
import org.nuclos.server.maintenance.MaintenanceFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.masterdata.ejb3.MetaDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.report.ejb3.ExtendedDatasourceFacadeBean;
import org.nuclos.server.report.ejb3.ReportFacadeBean;
import org.nuclos.server.report.migration.JRBaseInfo;
import org.nuclos.server.report.migration.JRFilePatcher;
import org.nuclos.server.report.migration.JRMigrationInfo;
import org.nuclos.server.report.migration.JRMigrationResult;
import org.nuclos.server.resource.ejb3.ResourceFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for all NuclosConsole functions.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor = {Exception.class})
@RolesAllowed("UseManagementConsole")
@NuclosRemotingInterface
public class ConsoleFacadeBean extends NuclosFacadeBean implements ConsoleFacadeLocal, ConsoleFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(ConsoleFacadeBean.class);

	private SpringDataBaseHelper dataBaseHelper;
	private PersistentDbAccess dbAccessPers;

	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
		this.dbAccessPers = new PersistentDbAccess(dataBaseHelper.getDbAccess());
	}

	@Autowired
	private DocumentFileFacadeLocal documentFileFacade;

	@Autowired
	private TransferFacadeBean nucletTransferFacade;

	@Autowired
	private EventSupportFacadeRemote eventSupportFacade;

	@Autowired
	private SecurityCache securityCache;

	@Autowired
	private NucletDalProvider nucletDal;

	@Autowired
	private LiveSearchFacadeLocal liveSearchFacade;

	@Autowired
	private WebAddonFacade webAddonFacade;

	@Autowired
	private MaintenanceFacadeBean maintenanceFacade;

	@Autowired
	private PreferencesFacadeLocal preferencesFacade;

	@Autowired
	private ReportFacadeBean reportFacade;

	@Autowired
	private ResourceFacadeLocal resourceFacade;

	@Autowired
	private ExtendedDatasourceFacadeBean datasourceFacade;

	@Autowired
	private MasterDataFacadeBean masterDataService;

	@Autowired
	private MasterDataFacadeHelper masterDataFacadeHelper;

	@Autowired
	private ServerParameterProvider paramProv;

	public ConsoleFacadeBean() {
	}

	/**
	 * @param sMessage the message to send
	 * @param sUser    the receiver of this message (all users if null)
	 * @param priority
	 * @param sAuthor  the author of the message
	 */
	public void sendClientNotification(String sMessage, String sUser, Priority priority, String sAuthor) {
		LOG.info("JMS send client notification to user {}: {}: {}", sUser, sMessage, this);
		final RuleNotification rn = new RuleNotification(priority, sMessage, sAuthor);
		if (sUser != null) {
			NuclosJMSUtils.sendObjectMessageAfterCommit(
					rn, JMSConstants.TOPICNAME_RULENOTIFICATION, sUser);
		} else {
			NuclosJMSUtils.sendOnceAfterCommitDelayed(rn, JMSConstants.TOPICNAME_RULENOTIFICATION);
		}
	}

	/**
	 * end all the clients of sUser
	 *
	 * @param sUser if null for all users
	 */
	public void killSession(String sUser) {
		LOG.info("JMS send killSession {}: {}", sUser, this);
		final CommandMessage cm = new CommandMessage(CommandMessage.CMD_SHUTDOWN_NOW);
		if (sUser != null) {
			NuclosJMSUtils.sendObjectMessageAfterCommit(
					cm, JMSConstants.TOPICNAME_RULENOTIFICATION, sUser);
		} else {
			NuclosJMSUtils.sendOnceAfterCommitDelayed(
					cm, JMSConstants.TOPICNAME_RULENOTIFICATION);
		}
	}

	/**
	 * check for VIEWS and FUNCTIONS which are invalid and compile them
	 *
	 * @throws SQLException
	 */
	public void compileInvalidDbObjects() throws SQLException {
		LOG.info("compiling invalid db objects (views and functions)");
		dataBaseHelper.getDbAccess().validateObjects();
	}

	/**
	 * invalidateAllServerSide Caches
	 */
	public void invalidateAllCaches() {
		nucletTransferFacade.revalidateCaches();
	}

	@Override
	public String[] freeDalMemory() {
		final long totalMemoryUsageBefore = nucletDal.getTotalMemoryUsage(true);
		nucletDal.freeMemory(true);
		final long totalMemoryUsageAfter = nucletDal.getTotalMemoryUsage(true);
		TransactionalClusterNotification.notifyFreeDalMemory();
		return new String[]{String.format("%s freed memory",
				RigidUtils.humanReadableBytes(totalMemoryUsageBefore - totalMemoryUsageAfter, ByteUnit.MEGABYTE))};
	}

	@Override
	public String[] gatDalMemoryLoad() {
		final List<Pair<String, Long>> pairs = nucletDal.getMemoryUsageByEntities(true, 128L).stream()
				.map(pair -> new Pair<String, Long>(
						String.format("%s.%s",
								Optional.ofNullable(pair.getX().getNuclet())
										.map(metaProvider::getNuclet)
										.map(eoNuclet -> eoNuclet.getFieldValue(E.NUCLET.packagefield))
										.orElse(""),
								pair.getX().getEntityName()),
						pair.getY()))
				.collect(Collectors.toList());
		pairs.add(0, new Pair<>("DalMemoryLoad(total)", pairs.stream().map(Pair::getY).reduce(0L, ArithmeticUtils::addAndCheck)));
		pairs.forEach(p -> LOG.info(String.format("%64s: %16s", p.getX(), RigidUtils.humanReadableBytes(p.getY(), ByteUnit.BYTE))));
		return pairs.stream().map(p -> String.format("%s: %s", p.getX(), RigidUtils.humanReadableBytes(p.getY(), ByteUnit.MEGABYTE)))
				.toArray(String[]::new);
	}

	@Override
	@RolesAllowed("Super-User")
	public void setDevelopmentEnvironment(boolean bDevelopment) {
		if (ApplicationProperties.getInstance().isFunctionBlockDev()) {
			NuclosSystemParameters.setDevelopmentEnvironment(bDevelopment);
		} else {
			throw new IllegalStateException("no Functionblock.Dev");
		}
	}

	@Override
	@RolesAllowed("Super-User")
	public void setProductionEnvironment(boolean bProduction) {
		if (ApplicationProperties.getInstance().isFunctionBlockDev()) {
			NuclosSystemParameters.setProductionEnvironment(bProduction);
		} else {
			throw new IllegalStateException("no Functionblock.Dev");
		}
	}

	/**
	 * get Infomation about the database in use
	 */
	public String getDatabaseInformationAsHtml() {
		final StringBuilder sb = new StringBuilder("<b>Database Meta Information</b><br>");
		sb.append("<HTML><table border=\"1\">");

		for (Map.Entry<String, Object> e : dataBaseHelper.getDbAccess().getMetaDataInfo().entrySet()) {
			sb.append(String.format("<tr><td><b>%s</b></td><td>%s</td></tr>", e.getKey(), e.getValue()));
		}

		sb.append("</table><br><b>Vendor specific parameters:<br><table border=\"1\">");
		final Map<String, String> mpDbParameters = dataBaseHelper.getDbAccess().getDatabaseParameters();
		for (String sParameter : mpDbParameters.keySet()) {
			sb.append("<tr><td><b>").append(sParameter).append("</b></td><td>")
					.append(mpDbParameters.get(sParameter)).append("</td></tr>");
		}
		sb.append("</table></html>");
		return sb.toString();
	}

	/**
	 * get the system properties of the server
	 */
	public String getSystemPropertiesAsHtml() {
		final StringBuilder sbClient = new StringBuilder();
		sbClient.append("<html><b>Java System Properties (Server):</b>");
		sbClient.append("<table border=\"1\">");
		for (final Object key : System.getProperties().keySet()) {
			sbClient.append("<tr><td><b>").append((String) key).append("</b></td><td>")
					.append(System.getProperty((String) key)).append("</td></tr>");
		}
		sbClient.append("</table></html>\n");
		return sbClient.toString();
	}

	@Override
	public String[] getConstraints(final Set<String> tables) {
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		return getConstraints(dbAccess).stream()
				.filter(c -> tables.isEmpty() || tables.contains(c.getTable().getName()))
				.map(DbConstraint::toString)
				.collect(Collectors.toList())
				.toArray(new String[]{});
	}

	@Override
	public String[] dropConstraints(final Set<String> constraintsToDrop) {
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		List<String> result = new ArrayList<String>();
		for (DbConstraint c : getConstraints(dbAccess)) {
			if (constraintsToDrop.contains(c.getConstraintName())) {
				createOrDropConstraints(Collections.singletonList(c), false, dbAccess, result);
			}
		}
		return result.toArray(new String[]{});
	}

	@Override
	public String[] rebuildConstraints() {
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		List<String> result = new ArrayList<String>();
		for (DbConstraint c : getConstraints(dbAccess)) {
			createOrDropConstraints(Collections.singletonList(c), false, dbAccess, result);
			createOrDropConstraints(Collections.singletonList(c), true, dbAccess, result);

		}
		return result.toArray(new String[]{});
	}

	@Override
	public String[] rebuildIndexes() {
		PersistentDbAccess dbAccess = new PersistentDbAccess(dataBaseHelper.getDbAccess());
		List<String> result = new ArrayList<String>();
		for (DbIndex c : getIndexes(dataBaseHelper.getDbAccess())) {
			createOrDropIndexes(Collections.singletonList(c), false, dbAccess, result);
			createOrDropIndexes(Collections.singletonList(c), true, dbAccess, result);

		}
		return result.toArray(new String[]{});
	}

	/**
	 * Performs an analysis on all DbSources and all data sources.
	 * The user is informed about following possible problems:
	 * - the database object for a database source is no longer existing
	 * - a database source gives a sql warning
	 * - a datasource could not be validated (Calculated Attribute, RecordGrant, Diagramm, Dynamic BusinessObject,
	 * 					 					  Dynamic TaskList, Report & Form, ValueList Provider)
	 * - the sql of a datasource could not be validated
	 * @return - String array containing the output for the management console
	 * @throws CommonPermissionException - if the user does not have permission to read some DbSources or datasources
	 */
	@Override
	public String[] checkDatasources() throws CommonPermissionException {
		int numMissingDbObjects = 0;
		int numDatasourceWarnings = 0;
		int numDatasourceExceptions = 0;
		int numValidationExceptions;
		List<String> result = new ArrayList<>();

		result.add("Validating dbobject sources...");
		List<Object> data = masterDataService.getMasterDataIds(E.DBSOURCE.getUID(), CollectableSearchExpression.TRUE_SEARCH_EXPR);
		for(Object sourceUID : data) {
			try {
				MasterDataVO<UID> source = masterDataService.get(E.DBSOURCE.getUID(), (UID)sourceUID);
				if (validateDbObjectExistsForSource(UID.parseUID(source.getFieldValue(E.DBSOURCE.dbobject.getUID()).toString()))) {
					result.add("Problem detected: DbObject not existing for DbSource: " + sourceUID);
					numMissingDbObjects++;
				}
				// Trigger the update routine with no changes, so all database objects get dropped and reinitialized
				// If a NuclosBusinessException is thrown, the user will get informed, that something is wrong
				StringBuffer warnings = masterDataFacadeHelper.updateDbObject(source.getEntityObject(), source.getEntityObject(), 0, false);
				if (warnings == null) {
					// if warnings is null, that means the DbSource is not active
					// so we inform the user, that the datasource was ignored and not sql checked
					result.add("Ignored Datasource \"" + source.getFieldValue(E.DBSOURCE.dbobject.getUID()).toString() + "\" because database type is not matching...");
				} else {
					if (warnings.length() > 0) {
						result.add("Warning while validating datasource \"" + source.getFieldValue(E.DBSOURCE.dbobject.getUID()).toString() + "\":");
						result.add("\t" + warnings.toString().replace("\n", "\n\t"));
						numDatasourceWarnings++;
					}
				}
			} catch (CommonFinderException | CommonPermissionException e) {
				LOG.error(e.getMessage(), e);
			} catch (NuclosBusinessException e) {
				LOG.error(e.getMessage(), e);
				result.add("Database Source Problem detected.");
				result.add("\tException: " + e.getClass());
				result.add("Stacktrace: ");
				result.add("\t" + e.getMessage().replace("\n", "\n\t\t"));
				result.add("");
				numDatasourceExceptions++;
			}
		}

		// Calculated Attribute
		Collection<CalcAttributeVO> calcAttributes = datasourceFacade.getCalcAttributes();
		result.add("Validating calculated attributes...");
		int calcAttributeExceptions = validateDatasourceCollection(calcAttributes, result);

		// RecordGrant
		Collection<RecordGrantVO> recordGrants = datasourceFacade.getRecordGrant();
		result.add("Validating record grants...");
		int recordGrantExceptions = validateDatasourceCollection(recordGrants, result);

		// Diagramm
		Collection<ChartVO> charts = datasourceFacade.getCharts();
		result.add("Validating charts...");
		int chartExceptions = validateDatasourceCollection(charts, result);

		// Dynamic BusinessObject
		Collection<DynamicEntityVO> dynamicEntities = datasourceFacade.getDynamicEntities();
		result.add("Validating dynamic entities...");
		int dynamicEntityExceptions = validateDatasourceCollection(dynamicEntities, result);

		// Dynamic TaskList
		Collection<DynamicTasklistVO> tasklists = datasourceFacade.getDynamicTasklists();
		result.add("Validating dynamic tasklists...");
		int tasklistExceptions = validateDatasourceCollection(tasklists, result);

		// Report & Form
		Collection<DatasourceVO> datasources = datasourceFacade.getDatasources();
		result.add("Validating reports & forms...");
		int datasourceExceptions = validateDatasourceCollection(datasources, result);

		// Valuelist Provider
		Collection<ValuelistProviderVO> valuelistProviders =  datasourceFacade.getValuelistProvider();
		result.add("Validating valuelist providers...");
		int valuelistExceptions = validateDatasourceCollection(valuelistProviders, result);

		numValidationExceptions = calcAttributeExceptions + recordGrantExceptions + chartExceptions + dynamicEntityExceptions
				+ tasklistExceptions + datasourceExceptions + valuelistExceptions;

		if (numValidationExceptions == 0 && numDatasourceExceptions == 0 && numDatasourceWarnings == 0 && numMissingDbObjects == 0) {
			result.add("Success: All datasources & dataobjects are valid! (" + numValidationExceptions + " tested, no warnings occurred)");
		} else {
			if (numMissingDbObjects > 0) {
				result.add(numMissingDbObjects + " DbSource(s), is/are missing their DbObject(s).");
			}
			if (numDatasourceWarnings > 0) {
				result.add(numDatasourceWarnings + " Warning(s) occurred, while attempting to validate the source for the dbobjects.");
			}
			if (numDatasourceExceptions > 0) {
				result.add(numDatasourceExceptions + " Exception(s) was/were thrown, while attempting to validate the source for the dbobjects.");
			}
			if (numValidationExceptions > 0) {
				result.add(numValidationExceptions + " Exception(s) was/were thrown, while attempting to validate the datasources.");
			}
		}

		return result.toArray(new String[]{});
	}

	/**
	 * Checks if a DbObject exists for the given UID.
	 * It is used to find out, if the DbObject referenced by a DbSource still exists.
	 * @param dbObject - input a UID e.g. the dboject UID from a datasource
	 * @return true, if a dbobject exists for the given UID
	 */
	public boolean validateDbObjectExistsForSource(UID dbObject) {
		try {
			masterDataService.get(E.DBOBJECT, dbObject);
		} catch (CommonFinderException cfe) {
			return false;
		} catch (CommonPermissionException ignored) { }
		return true;
	}

	/**
	 * Attempts to validate all DatasourceVOs of the specified Collection and their Sql.
	 * @param datasources - the Collection of DatasourceVOs that should be validated
	 * @param output - a list, where the output for the management console is added to
	 * @return - number of DatasourceVOs, which failed with an exception during the validation process
	 */
	public int validateDatasourceCollection(Collection<? extends DatasourceVO> datasources, List<String> output) {
		int numExceptions = 0;
		for (DatasourceVO datasourceVO : datasources) {
			try {
				datasourceVO.validate();
				datasourceFacade.validateSqlFromXML(datasourceVO);
			} catch (Exception e) {
				output.add("Exception during datasource validation: " + datasourceVO.getName());
				numExceptions++;
				if (e.getCause() instanceof DbException && e.getCause().getCause() instanceof SQLException) {
					// provide special console output for SQL Errors
					String errorMessage = e.getCause().getCause().getMessage();
					String sqlCode = e.getCause().getMessage();
					output.add("" + errorMessage);
					output.add("" + sqlCode);
					try {
						dataBaseHelper.getDbAccess().getDbExecutor().commit();
					} catch (SQLException sqle) {
						output.add("");
						output.add("[WARNING] Could not commit, the next statement might fail...");
					}
				} else {
					// provide standard exception message for non SQL exceptions
					output.add("" + e.getMessage());
				}
				output.add("");
				output.add("");
			}
		}
		return numExceptions;
	}

	@Override
	public String[] migrateJasperReports(String [] args) throws CommonPermissionException {
		List<String> result = new ArrayList<>();
		JRPatchParameters patchParameters = new JRPatchParameters();

		for(int i = 0; i < args.length; i++) {
			if(args[i].equals("-patch")) {
				patchParameters.patch = true;
			}
			if(args[i].equals("-removeDeprecatedPdfFonts")) {
				patchParameters.replaceDeprecatedPdfFonts = true;
			}
			if(args[i].equals("-overwriteReportFonts")) {
				patchParameters.overwriteReportFonts = true;
				if(i + 1 < args.length && !args[i + 1].startsWith("-")) {
					patchParameters.replacementFont = args[i + 1];
				}
			}
			if(args[i].equals("-forceAll")) {
				patchParameters.forceAll = true;
			}
		}

		ArrayList<DefaultReportVO> allReportsAndForms = new ArrayList<>();
		allReportsAndForms.addAll(reportFacade.getReports());
		allReportsAndForms.addAll(reportFacade.getForms());

		//Keep track of JRMigrationStatus of the reports
		JRMigrationInfo migrationInfo = new JRMigrationInfo();
		//If true inform the user, that there are fonts in the report that are not present in the jvm
		boolean hasJvmMissingFont = false;

		//call the migration process for each report with the parsed parameters
		for(DefaultReportVO reportVO : allReportsAndForms) {
			for(DefaultReportOutputVO outputVO : reportFacade.getReportOutputs(reportVO.getId())) {
				if(outputVO.getFormat() != ReportOutputVO.Format.PDF) {
					//skip non-jasper Reports
					continue;
				}
				String rvoName = reportVO.getName();
				String description = outputVO.getDescription();
				migrateJasperReport(migrationInfo, patchParameters, outputVO, null, rvoName, description);
				if(!hasJvmMissingFont) {
					hasJvmMissingFont = reportFacade.hasJvmMissingFont(outputVO);
				}
				for(ReportParameterVO reportParameterVO : reportFacade.getReportParameters(E.SUBREPORT, outputVO.getId())) {
					migrateJasperReport(migrationInfo, patchParameters, null, reportParameterVO, rvoName, description);
				}
			}
		}

		result.add("");
		result.add("The current Jasper Version of Nuclos is \"" + JRUtils.NUCLOS_JASPER_VERSION + "\"");
		if(hasJvmMissingFont) {
			result.add("");
			result.add("Note: There are one/multiple (sub)reports with fonts that are missing in the JVM.");
			result.add("          Missing fonts will be replaced with a system default.");
			result.add("");
		}

		ArrayList<String> matchingReportList = migrationInfo.filterForResult(JRMigrationResult.TYPE_MATCHING_ALREADY);
		result.add("---------------------------------------------------------------------------------------------------");
		result.add("Found " + matchingReportList.size() + " report(s) matching the current Jasper Version of Nuclos. ");
		result.add("No patching required for following reports: ");
		result.add("---------------------------------------------------------------------------------------------------");
		result.addAll(matchingReportList);
		result.add("\n\n");


		ArrayList<String> newerReportList = migrationInfo.filterForResult(JRMigrationResult.TYPE_TOO_NEW);
		if(newerReportList.size() > 0) {
			result.add("---------------------------------------------------------------------------------------------------");
			result.add("Found " + newerReportList.size() + " reports with a newer version than used in Nuclos. ");
			result.add("Consider exporting the following reports for a lower Jasper Version: ");
			result.add("---------------------------------------------------------------------------------------------------");
			result.addAll(newerReportList);
			result.add("\n\n");
		}

		if(patchParameters.patch) {
			ArrayList<String> outdatedReportsList = migrationInfo.filterForResult(JRMigrationResult.TYPE_SUCCESSFUL);
			result.add("---------------------------------------------------------------------------------------------------");
			result.add("Successfully patched " + outdatedReportsList.size() + " outdated report(s): ");
			result.add("---------------------------------------------------------------------------------------------------");
			result.addAll(outdatedReportsList);
			result.add("\n\n");

			ArrayList<String> failedReportsList = migrationInfo.filterForResult(JRMigrationResult.TYPE_ERROR);
			result.add("---------------------------------------------------------------------------------------------------");
			result.add("Patching failed for " + failedReportsList.size() + " report(s). See the following errors: ");
			result.add("---------------------------------------------------------------------------------------------------");
			result.addAll(failedReportsList);
			result.add("\n\n");
		} else {
			ArrayList<String> outdatedReportsList = migrationInfo.filterForResult(JRMigrationResult.TYPE_OUTDATED);
			result.add("---------------------------------------------------------------------------------------------------");
			result.add("Found " + outdatedReportsList.size() + " report(s) with a older version than used in Nuclos.");
			result.add("---------------------------------------------------------------------------------------------------");
			result.addAll(outdatedReportsList);
			result.add("\n\n");

			ArrayList<String> outdatedPdfTagReportsList = migrationInfo.filterForResult(JRMigrationResult.TYPE_OUTDATED_WITH_PDF_TAGS);
			if(outdatedPdfTagReportsList.size() > 0) {
				result.add("---------------------------------------------------------------------------------------------------");
				result.add("Found " + outdatedPdfTagReportsList.size() + " outdated report(s) containing the deprecated 'pdfFontName'-tag.");
				result.add("---------------------------------------------------------------------------------------------------");
				result.addAll(outdatedPdfTagReportsList);
				result.add("\n\n");
			}
		}
		return result.toArray(new String[]{});
	}

	public String[] setMandatorLevel(String[] args) {
		Map<String, UID> mapNucletByPackage = getMapNucletByPackage();
		List<String> result = new ArrayList<String>();

		List<String> lstArgs = new ArrayList<String>();
		for (String arg : args) {
			arg = arg.trim();
			if (arg.contains("\n")) {
				for (String cut : arg.split("\n")) {
					lstArgs.add(cut.trim());
				}
			} else {
				lstArgs.add(arg);
			}
		}

		Iterator<String> itArgs = lstArgs.iterator();
		boolean nextPackage = false;

		try {
			while (itArgs.hasNext() || nextPackage) {

				String arg = itArgs.next();
				if ("-package".equals(arg) || nextPackage) {
					Map<String, String> mapArgValues = new HashMap<String, String>();
					if (nextPackage) {
						nextPackage = false;
						mapArgValues.put("-package", arg);
					} else {
						mapArgValues.put("-package", itArgs.next());
					}
					mapArgValues.put(itArgs.next(), itArgs.next());
					mapArgValues.put(itArgs.next(), itArgs.next());
					while (itArgs.hasNext()) {
						String sNext = itArgs.next();
						if ("-package".equals(sNext)) {
							nextPackage = true;
							break;
						} else {
							if ("-bo".equals(sNext) ||
									"-level".equals(sNext) ||
									"-initial".equals(sNext)) {
								mapArgValues.put(sNext, itArgs.next());
							}
							else if("-uniqueMandator".equals(sNext)){
								mapArgValues.put(sNext, "true");
							}
						}
					}

					String sPackage = mapArgValues.get("-package");
					UID nucletUID = mapNucletByPackage.get(sPackage);
					if (nucletUID == null) {
						result.add("Nuclet with package \"" + sPackage + "\" does not exist!");
						continue;
					}

					String sBusinessObjectName = mapArgValues.get("-bo");
					EntityMeta<?> eMeta = null;
					for (UID eUID : metaProvider.getEntities(nucletUID)) {
						EntityMeta<?> em = metaProvider.getEntity(eUID);
						if (em.getEntityName().equals(sBusinessObjectName)) {
							eMeta = em;
						}
					}
					if (eMeta == null) {
						result.add("Business object \"" + sBusinessObjectName + "\" in nuclet \"" + sPackage + "\" does not exist!");
						continue;
					}

					String sLevel = mapArgValues.get("-level");
					int level = 0;
					try {
						level = Integer.parseInt(sLevel);
					} catch (Exception ex) {
						result.add("Level \"" + sLevel + "\" is not a number. Error: " + ex.getMessage());
						continue;
					}
					List<MandatorLevelVO> allMandatorLevelsSorted = securityCache.getAllMandatorLevels();
					if (level > allMandatorLevelsSorted.size()) {
						result.add("Level \"" + sLevel + "\" does not exist!");
						continue;
					}
					MandatorLevelVO mandatorLevelVO = null;
					if (level > 0) {
						mandatorLevelVO = allMandatorLevelsSorted.get(level - 1);
					}

					String sInitialMandatorPath = mapArgValues.get("-initial");
					MandatorVO initialMandatorVO = null;
					if (sInitialMandatorPath != null) {
						for (MandatorVO mandatorVO : securityCache.getAllMandators()) {
							if (sInitialMandatorPath.equals(mandatorVO.getPath())) {
								if (initialMandatorVO != null) {
									result.add("Mandator with path \"" + sInitialMandatorPath + "\" is not unique!");
									continue;
								}
								initialMandatorVO = mandatorVO;
							}
						}
						if (initialMandatorVO == null) {
							result.add("Initial mandator with path \"" + sInitialMandatorPath + "\" does not exist!");
							continue;
						}
					}

					boolean mandatorUnique = mapArgValues.containsKey("-uniqueMandator");
					result.add(setMandatorLevel(sPackage, eMeta, sLevel, mandatorLevelVO, initialMandatorVO, mandatorUnique));
				}
			}
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			result.add("ERROR: " + ex.getMessage());
		}

		return result.toArray(new String[]{});
	}

	private BidiMap<String, UID> getMapNucletByPackage() {
		BidiMap<String, UID> result = new DualHashBidiMap<String, UID>();

		for (EntityObjectVO<UID> nucletEO : nucletDal.getEntityObjectProcessor(E.NUCLET).getAll()) {
			result.put(nucletEO.getFieldValue(E.NUCLET.packagefield), nucletEO.getPrimaryKey());
		}

		return result;
	}

	/**
	 * for testing only! (only Superusers)
	 */
	@Override
	public String[] resetCompleteMandatorSetup() {
		if (!securityCache.isSuperUser(getCurrentUserName())) {
			throw new NuclosFatalException("Not allowed (only Superusers)");
		}
		List<String> result = new ArrayList<String>();
		try {
			BidiMap<String, UID> mapNucletByPackage = getMapNucletByPackage();

			Collection<EntityMeta<?>> allEntities = metaProvider.getAllEntities();
			for (EntityMeta<?> eMeta : allEntities) {
				if (!eMeta.isMandator()) {
					continue;
				}
				String sPackage = NuclosBusinessObjectBuilder.DEFFAULT_PACKAGE_NUCLET;
				if (eMeta.getNuclet() != null) {
					sPackage = mapNucletByPackage.getKey(eMeta.getNuclet());
				}
				result.add(setMandatorLevel(sPackage, eMeta, "-1", null, null, false));
			}

			result.add(truncateTable(E.MANDATOR_ROLE_USER));
			result.add(truncateTable(E.MANDATOR_USER));
			result.add(truncateTable(E.MANDATOR_PARAMETERVALUE));
			result.add(truncateTable(E.MANDATOR_RESOURCE));
			result.add(truncateTable(E.MANDATOR_ACCESSIBLE));
			result.add(truncateTable(E.MANDATOR, E.MANDATOR.parentMandator, UID.class));
			result.add(truncateTable(E.MANDATOR_LEVEL, E.MANDATOR_LEVEL.parentLevel, UID.class));

		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			result.add("ERROR: " + ex.getMessage());
		}
		return result.toArray(new String[]{});
	}

	/**
	 * for testing only! (only Superusers)
	 * switch lucene indexing on or off
	 *
	 * @param enabled
	 */
	public void setIndexerEnabled(boolean enabled, boolean synchronous) {
		if (!securityCache.isSuperUser(getCurrentUserName())) {
			throw new NuclosFatalException("Not allowed (only Superusers)");
		}
		liveSearchFacade.setIndexerEnabled(enabled, synchronous);
	}

	/**
	 * for testing only! (only Superusers)
	 * rebuild Lucene Index
	 */
	public void rebuildLuceneIndex() {
		if (!securityCache.isSuperUser(getCurrentUserName())) {
			throw new NuclosFatalException("Not allowed (only Superusers)");
		}
		liveSearchFacade.rebuildLuceneIndex();
	}

	public String[] fixRecordGrantPkAlias() throws CommonPermissionException {
		List<String> results = new ArrayList<>();
		datasourceFacade.getRecordGrant().forEach(recordGrantVO -> {
			try {
				final DatasourceXMLParser.Result parsedRecordGrant = DatasourceXMLParser.parse(recordGrantVO.getSource());
				if (parsedRecordGrant.isModelUsed() && !parsedRecordGrant.isEntityOptionDynamic()) {
					final String updatedSource = DatasourceXMLTransformer.setDynamicEntity(recordGrantVO.getSource(), true);
					recordGrantVO.setSource(updatedSource);

					datasourceFacade.modify(recordGrantVO);
					results.add("Altered record grant datasource \"" + recordGrantVO.getId() + "\"");
				}
			} catch (CommonBusinessException e) {
				results.add(e.getMessage());
			}
		});

		if (results.isEmpty()) {
			results.add("There are no record grant datasource that need to be fixed.");
		}

		return results.toArray(new String[results.size()]);
	}

	private <PK> String truncateTable(EntityMeta<PK> systemEntity) {
		return truncateTable(systemEntity, null, null);
	}

	@SuppressWarnings("unchecked")
	private <PK> String truncateTable(EntityMeta<PK> systemEntity, FieldMeta<PK> recursivParentForeignKeyField, Class<PK> pkClass) {
		IExtendedEntityObjectProcessor<PK> proc = nucletDal.getEntityObjectProcessor(systemEntity);
		List<EntityObjectVO<PK>> all = new ArrayList<>(proc.getAll());
		while (!all.isEmpty()) {
			Set<PK> parentPKs = new HashSet<>();
			if (recursivParentForeignKeyField != null) {
				for (EntityObjectVO<PK> eo : all) {
					PK parent = null;
					if (pkClass.equals(UID.class)) {
						parent = (PK) eo.getFieldUid((FieldMeta<UID>) recursivParentForeignKeyField);
					} else {
						parent = (PK) eo.getFieldId((FieldMeta<Long>) recursivParentForeignKeyField);
					}
					if (parent != null) {
						parentPKs.add(parent);
					}
				}
			}
			Iterator<EntityObjectVO<PK>> it = all.iterator();
			while (it.hasNext()) {
				EntityObjectVO<PK> eo = it.next();
				PK pk = eo.getPrimaryKey();
				if (parentPKs.isEmpty() || !parentPKs.contains(pk)) {
					it.remove();
					Delete<PK> del = new Delete<>(pk);
					proc.delete(del);
				}
			}
		}
		return "truncate table " + systemEntity.getDbTable() + ": " + all.size() + " rows";
	}

	private void migrateJasperReport(JRMigrationInfo migrationInfo, JRPatchParameters parameters, DefaultReportOutputVO reportOutputVO, ReportParameterVO reportParameterVO, String reportVOName, String outputVOdescription) {
		JRBaseInfo baseInfo;
		reportVOName = "(" + reportVOName + ")" + " " + "(" + outputVOdescription + ")";
		if(reportOutputVO != null && reportParameterVO == null) {
			baseInfo = new JRBaseInfo(reportOutputVO, resourceFacade);
		} else if(reportParameterVO != null && reportOutputVO == null) {
			if(reportParameterVO.getParameterName() != null) {
				reportVOName += " (Parameter = " + reportParameterVO.getParameterName() + ")";
			}
			baseInfo = new JRBaseInfo(reportParameterVO, resourceFacade);
		} else {
			throw new IllegalArgumentException("Exactly one parameter out of DefaultReportOutputVO and ReportParameterVO should be not null.");
		}
		if (baseInfo == null || !baseInfo.hasValidSource()) {
			return;
		}
		JRMigrationResult migrationResult;
		if(baseInfo.isPatchRequired() || parameters.forceAll) {
			boolean containsDeprecatedPdfFonts = baseInfo.containsDeprecatedPdfFonts();
			if(parameters.patch) {
				try {
					if(reportOutputVO != null) {
						JRFilePatcher.migrateJasperReport(dataBaseHelper, masterDataService, reportOutputVO, parameters);
					} else {
						JRFilePatcher.migrateJasperSubreport(dataBaseHelper, masterDataService, reportParameterVO, parameters);
					}
					migrationResult = new JRMigrationResult(baseInfo, reportVOName, JRMigrationResult.TYPE_SUCCESSFUL);
				} catch(NuclosReportException e) {
					String indentedMsg = e.getMessage().replaceAll("\n", "\n\t") + "\n";
					migrationResult = new JRMigrationResult(baseInfo, reportVOName, JRMigrationResult.TYPE_ERROR, indentedMsg);
				}
			} else {
				int resultType = containsDeprecatedPdfFonts ? JRMigrationResult.TYPE_OUTDATED_WITH_PDF_TAGS : JRMigrationResult.TYPE_OUTDATED;
				migrationResult = new JRMigrationResult(baseInfo, reportVOName, resultType);
			}
		} else {
			int resultType = baseInfo.isVersionToRecent() ? JRMigrationResult.TYPE_TOO_NEW : JRMigrationResult.TYPE_MATCHING_ALREADY;
			migrationResult = new JRMigrationResult(baseInfo, reportVOName, resultType);
		}
		if(migrationResult != null) {
			migrationInfo.AddMigrationResult(migrationResult);
		}
	}

	private String setMandatorLevel(String sPackage, EntityMeta<?> eMeta, String sLevel, MandatorLevelVO mandatorLevelVO, MandatorVO initialMandatorVO, boolean mandatorUnique) throws NuclosCompileException, InterruptedException, CommonPermissionException {
		String result = sPackage + "." + eMeta.getEntityName() + " -> ";
		if (mandatorLevelVO != null) {
			if (mandatorLevelVO.getName() != null) {
				sLevel = sLevel + ":" + mandatorLevelVO.getName();
			}
			result = result + sLevel;
			if (initialMandatorVO != null) {
				result = result + " with initial mandator " + initialMandatorVO.getPath();
			}
		} else {
			result = result + "NO mandator (level)";
		}

		MetaDataFacadeLocal metaFacade = SpringApplicationContextHolder.getBean(MetaDataFacadeLocal.class);
		try {
			metaFacade.setMandatorLevel(eMeta.getUID(),
					mandatorLevelVO == null ? null : mandatorLevelVO.getUID(),
					initialMandatorVO == null ? null : initialMandatorVO.getUID(), mandatorUnique);
			result = result + " DONE";
		} catch (NuclosBusinessException e) {
			result = result + " ERROR: " + e.getMessage();
		}

		invalidateAllCaches();
		rebuildAllClasses();

		return result;
	}

	@Override
	public String[] migrateDocumentAttachmentsIntoSubDirs(long iChunkSize, long iMaxChunksToMigrate, String...ignoredExtensions) {
		if (!securityCache.isActionAllowed(getCurrentUserName(), Actions.ACTION_MANAGEMENT_CONSOLE,null)) {
			throw new NuclosFatalException("Management console not allowed");
		}
		final String guidelineTotals = paramProv.getValue(ParameterProvider.DOCUMENTS_GUIDELINE_FOR_TOTAL_NUMBER_OF_FILES_PER_DIR);
		if (org.apache.commons.lang3.StringUtils.isEmpty(guidelineTotals)) {
			return new String[]{String.format("Parameter %s is not set", ParameterProvider.DOCUMENTS_GUIDELINE_FOR_TOTAL_NUMBER_OF_FILES_PER_DIR)};
		}
		final Set<String> setIgnoredExtensions = Arrays.stream(ignoredExtensions)
				.map(String::toLowerCase)
				.collect(Collectors.toSet());
		final LocalDateTime startTime = LocalDateTime.now();
		final StringBuilder sMigrationParameter = new StringBuilder();
		sMigrationParameter.append("ChunkSize=");
		sMigrationParameter.append(iChunkSize);
		final List<String> result = new ArrayList<String>();

		DbQueryBuilder builder = dbAccessPers.getQueryBuilder();
		final DbQuery<Long> query = builder.createQuery(Long.class);
		final DbFrom<UID> t = query.from(E.DOCUMENTFILE);
		query.select(builder.countRows());
		query.where(builder.isNull(t.baseColumn(E.DOCUMENTFILE.documentDir)));
		final long iCountDocumentAttachmentsToMigrate = dbAccessPers.executeQuerySingleResult(query);
		LOG.info(String.format("migrateDocumentAttachmentsIntoSubDirs found %d attachments to migrate", iCountDocumentAttachmentsToMigrate));
		long iChunksToExecute = (iCountDocumentAttachmentsToMigrate / iChunkSize) + 1;
		if (iMaxChunksToMigrate > 0L) {
			sMigrationParameter.append("; MaxChunksToMigrate=");
			sMigrationParameter.append(iMaxChunksToMigrate);
			LOG.info(String.format("migrateDocumentAttachmentsIntoSubDirs: maxChunksToMigrate is set to %d", iMaxChunksToMigrate));
			iChunksToExecute = Math.min(iChunksToExecute, iMaxChunksToMigrate);
		}

		// Reset last errors and ignores to try again:
		DbMap valuesMap = new DbMap();
		valuesMap.put(SF.CHANGEDBY, "System");
		DbMap condition = new DbMap();
		condition.put(SF.CHANGEDBY, "MigError");
		dbAccessPers.execute(new DbUpdateStatement<>(E.DOCUMENTFILE, valuesMap, condition));
		condition.put(SF.CHANGEDBY, "MigIgnore");
		dbAccessPers.execute(new DbUpdateStatement<>(E.DOCUMENTFILE, valuesMap, condition));

		AtomicInteger iSuccessCounter = new AtomicInteger(0);
		AtomicInteger iIgnoreCounter = new AtomicInteger(0);
		AtomicInteger iErrorCounter = new AtomicInteger(0);
		for (long i = 1; i <= iChunksToExecute; i++) {
			LOG.info(String.format("migrateDocumentAttachmentsIntoSubDirs.startChunk(%d/%d) - Counter[success=%d;ignored=%d,error=%d]",
					i, iChunksToExecute, iSuccessCounter.get(), iIgnoreCounter.get(), iErrorCounter.get()));
			migrateDocumentAttachmentsIntoSubDirsChunk(iChunkSize, iSuccessCounter, iIgnoreCounter, iErrorCounter, setIgnoredExtensions);
		}

		result.add(String.format("Migration parameter: %s", sMigrationParameter));
		result.add(String.format("Found document attachments to migrate: %d", iCountDocumentAttachmentsToMigrate));
		result.add(String.format("Successful migrated: %d", iSuccessCounter.get()));
		result.add(String.format("Ignored: %d", iIgnoreCounter.get()));
		result.add(String.format("With error (see server.log for details): %d", iErrorCounter.get()));
		LOG.info(String.format("migrateDocumentAttachmentsIntoSubDirs finished in %s - Counter[success=%d;ignored=%d,error=%d]",
				RigidUtils.humanReadableDuration(Duration.between(startTime, LocalDateTime.now())), iSuccessCounter.get(), iIgnoreCounter.get(), iErrorCounter.get()));

		return result.toArray(new String[]{});
	}

	private void migrateDocumentAttachmentsIntoSubDirsChunk(long iChunkSize, AtomicInteger iSuccessCounter, AtomicInteger iIgnoreCounter, AtomicInteger iErrorCounter, Set<String> setIgnoredExtensions) {
		DbQueryBuilder builder = dbAccessPers.getQueryBuilder();
		final DbQuery<DbTuple> query = builder.createTupleQuery();
		final DbFrom<UID> t = query.from(E.DOCUMENTFILE);
		query.multiselect(t.basePk(),
				t.baseColumn(SF.CREATEDAT),
				t.baseColumn(E.DOCUMENTFILE.filename));
		query.where(builder.isNull(t.baseColumn(E.DOCUMENTFILE.documentDir)))
				.addToWhereAsAnd(builder.not(builder.equalValue(t.baseColumn(SF.CHANGEDBY), "MigError")))
				.addToWhereAsAnd(builder.not(builder.equalValue(t.baseColumn(SF.CHANGEDBY), "MigIgnore")));
		query.limit(iChunkSize);
		final List<DbTuple> foundDocs = dbAccessPers.executeQuery(query);
		final Path basePath = NuclosSystemParameters.getFile(NuclosSystemParameters.DOCUMENT_PATH).toPath();
		for (DbTuple tuple : foundDocs) {
			final UID documentFileUID = tuple.get(E.DOCUMENTFILE.getPk());
			final InternalTimestamp createdAt = tuple.get(SF.CREATEDAT);
			final String fileName = tuple.get(E.DOCUMENTFILE.filename);
			final String documentFileName = DocumentFileUtils.getDocumentFileName(documentFileUID, fileName);
			final String fileExtension = Optional.ofNullable(org.nuclos.common2.File.getExtension(documentFileName))
					.map(String::toLowerCase)
					.orElse(null);
			final Path resolvedFilePath = basePath.resolve(documentFileName);
			final File resolvedFile = resolvedFilePath.toFile();
			if (resolvedFile.exists()) {
				if (fileExtension != null && setIgnoredExtensions.contains(fileExtension)) {
					LOG.debug(String.format("Ignored document attachment %s", documentFileName));
					iIgnoreCounter.incrementAndGet();
					updateDocumentFile(documentFileUID, "MigIgnore", null);
					continue;
				}
				final DocumentDir documentDir = documentFileFacade.incrementOrCreateDocumentDir(createdAt);
				if (documentDir == null) {
					throw new RuntimeException("documentDir not created!");
				}
				final Path targetDocumentDir = basePath.resolve(documentDir.getParent()).resolve(documentDir.getId().getString());
				final File targetDocumentDirFile = targetDocumentDir.toFile();
				try {
					LOG.debug(String.format("Moving document attachment %s to %s...", documentFileName, targetDocumentDirFile));
					// move file first
					FileUtils.moveFileToDirectory(resolvedFile, targetDocumentDirFile, true);
					// update the database record
					updateDocumentFile(documentFileUID, "System", documentDir.getId());
					iSuccessCounter.incrementAndGet();
				} catch (IOException e) {
					iErrorCounter.incrementAndGet();
					LOG.error(String.format("An error occurred while moving file %s to %s: %s", documentFileName, targetDocumentDirFile, e.getMessage()));
					updateDocumentFile(documentFileUID, "MigError", null);
				}
			} else {
				iErrorCounter.incrementAndGet();
				LOG.error(String.format("Document attachment does not exist: %s", resolvedFile.toString()));
				updateDocumentFile(documentFileUID, "MigError", null);
			}
		}
	}

	private void updateDocumentFile(UID documentFileUID, String changedBy, UID documentDirUID) {
		DbMap valuesMap = new DbMap();
		valuesMap.put(SF.VERSION, DbIncrement.INCREMENT);
		valuesMap.put(SF.CHANGEDBY, changedBy);
		valuesMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		if (documentDirUID == null) {
			valuesMap.putNull(E.DOCUMENTFILE.documentDir);
		} else {
			valuesMap.put(E.DOCUMENTFILE.documentDir, documentDirUID);
		}
		DbMap condition = new DbMap();
		condition.put(E.DOCUMENTFILE.getPk(), documentFileUID);
		dbAccessPers.execute(new DbUpdateStatement<>(E.DOCUMENTFILE, valuesMap, condition));
	}

	public String[] checkDocumentAttachments(boolean deleteUnusedFiles, Set<String> deleteEntityObjects) {
		final List<String> result = new ArrayList<String>();
		final Set<File> foundFiles = new HashSet<File>();
		final File baseDir = NuclosSystemParameters.getFile(NuclosSystemParameters.DOCUMENT_PATH);
		if (!baseDir.exists() || !baseDir.isDirectory()) {
			throw new NuclosFatalException("Document folder does not exist: " + baseDir);
		}
		final List<File> documentsDirContents = addAllFiles(new ArrayList<>(), baseDir.toPath());
		MetaProvider mdprov = SpringApplicationContextHolder.getBean(MetaProvider.class);
		MasterDataRestFqnCache fqnCache = SpringApplicationContextHolder.getBean(MasterDataRestFqnCache.class);
		for (EntityMeta<?> entityMeta : mdprov.getAllEntities()) {
			if (entityMeta.isProxy() || entityMeta.isGeneric()) {
				continue; // a proxy could not be queried via database
			}
			String fqn = fqnCache.translateUid(E.ENTITY, entityMeta.getUID());
			Set<FieldMeta<?>> docFields = new HashSet<FieldMeta<?>>();
			for (FieldMeta<?> fieldMeta : mdprov.getAllEntityFieldsByEntity(entityMeta.getUID()).values()) {
				if (fieldMeta.getDataType().equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile")) {
					docFields.add(fieldMeta);
				}
			}
			if (!docFields.isEmpty()) {
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<DbTuple> query = builder.createTupleQuery();
				DbFrom<?> from = query.from(entityMeta);
				List<DbSelection<?>> select = new ArrayList<DbSelection<?>>();
				select.add(from.basePk());
				boolean where = false;
				for (FieldMeta<?> fieldMeta : docFields) {
					DbField<?> dbRefColumn = SimpleDbField.createRef(fieldMeta.getDbColumn(), true);
					select.add(from.baseColumn(dbRefColumn));
					if (where) {
						query.addToWhereAsOr(builder.isNotNull(from.baseColumn(dbRefColumn)));
					} else {
						query.where(builder.isNotNull(from.baseColumn(dbRefColumn)));
						where = true;
					}
				}
				query.multiselect(select);

				dataBaseHelper.getDbAccess().executeQuery(query, new CheckDocumentAttachmentTransformer(entityMeta, fqn, docFields, deleteEntityObjects, foundFiles, result, documentFileFacade));
			}
		}

		checkDocumentAttachmentDir(documentsDirContents, foundFiles, deleteUnusedFiles, result);

		return result.toArray(new String[]{});
	}

	private void checkDocumentAttachmentDir(final List<File> documentsDirContents, final Set<File> foundFiles, boolean deleteUnusedFiles, final List<String> result) {
		for (File file : documentsDirContents) {
			if (file.isHidden()) {
				// ignore hidden "system" files
				continue;
			}
			if (file.isFile()) {
				if (!foundFiles.contains(file)) {
					String r = "NOT USED: " + file.getAbsolutePath();
					if (deleteUnusedFiles) {
						try {
							if (file.delete()) {
								r = r + " FILE DELETED!";
							} else {
								r = r + " FILE NOT DELETED!";
							}
						} catch (Exception ex) {
							r = r + " FILE NOT DELETED: " + ex.getMessage();
							LOG.error(r, ex);
						}
					}
					result.add(r);
					LOG.info(r);

				}
			}
		}
	}

	private static List<File> addAllFiles(List<File> fileList, Path path) {
		final File[] files = path.toFile().listFiles();
		if (files != null) {
			fileList.addAll(Arrays.stream(files)
					.filter(File::isFile)
					.collect(Collectors.toList()));
			Arrays.stream(files)
					.filter(File::isDirectory)
					.forEach(dir -> addAllFiles(fileList, dir.toPath()));
		}
		return fileList;
	}

	/**
	 * Find and remove duplicate document files
	 */
	public String cleanupDuplicateDocuments() {
		return DocumentFileUtils.cleanupDuplicateDocuments();
	}

	private static class CheckDocumentAttachmentTransformer implements Transformer<DbTuple, Void> {

		private final EntityMeta<?> entityMeta;
		private final String fqn;
		private final Set<FieldMeta<?>> docFields;
		private final Set<String> deleteEntityObjects;
		private final Set<File> foundFiles;
		private final List<String> result;
		private DocumentFileFacadeLocal documentFileFacade;

		public CheckDocumentAttachmentTransformer(EntityMeta<?> entityMeta, String fqn, Set<FieldMeta<?>> docFields, Set<String> deleteEntityObjects, Set<File> foundFiles,
												  List<String> result, DocumentFileFacadeLocal documentFileFacade) {
			super();
			this.entityMeta = entityMeta;
			this.fqn = fqn;
			this.docFields = docFields;
			this.deleteEntityObjects = deleteEntityObjects;
			this.foundFiles = foundFiles;
			this.result = result;
			this.documentFileFacade = documentFileFacade;
		}

		@Override
		public Void transform(DbTuple tuple) {
			Object pk = tuple.get(entityMeta.getPk());

			for (FieldMeta<?> fieldMeta : docFields) {
				DbField<?> dbRefColumn = SimpleDbField.createRef(fieldMeta.getDbColumn(), true);
				UID documentFileUID = (UID) tuple.get(dbRefColumn);
				if (documentFileUID != null) {
					File documentFile = documentFileFacade.getFile(documentFileUID);
					if (documentFile.exists()) {
						foundFiles.add(documentFile);

						// test for a valid link
						final UID docFileUIDfromLink = documentFileFacade.findDocumentFileUID(fieldMeta.getUID(), pk);
						if (docFileUIDfromLink == null) {
							documentFileFacade.createDocumentFileLink(documentFileUID, fieldMeta.getUID(), pk);
							String r = fqn + "(" + pk + ") - LINK DOES NOT EXIST, CREATED A NEW ONE!";
							result.add(r);
							LOG.info(r);
						}

					} else {
						Pair<String, String> fileNameAndDir = documentFileFacade.getFileNameAndDir(documentFileUID);
						String r = String.format("%s(%s) - NOT FOUND: %s in ./%s", fqn, pk, fileNameAndDir.getX(), StringUtils.defaultIfNull(fileNameAndDir.getY(), ""));
						if (deleteEntityObjects.contains(fqn)) {
							try {
								EntityObjectFacadeLocal eoFacade = SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class);
								eoFacade.delete(entityMeta.getUID(), pk, false, false);
								r = r + " RECORD " + pk + " DELETED!";
							} catch (Exception ex) {
								r = r + " RECORD " + pk + " NOT DELETED: " + ex.getMessage();
								LOG.error(r, ex);
							}
						}
						result.add(r);
						LOG.info(r);
					}
				}
			}

			return null;
		}
	}

	@Override
	public void rebuildAllClasses() throws NuclosCompileException, InterruptedException, CommonPermissionException {
		eventSupportFacade.createBusinessObjects();
	}

	@Override
	public void syncActiveWebAddons() {
		webAddonFacade.syncActiveWebAddons();
	}

	public String getMaintenanceMode() {
		return maintenanceFacade.getMaintenanceMode();
	}

	public String startMaintenance(final String[] args) {
		boolean force = args.length > 1
				&& (RigidUtils.parseBoolean(args[1]) || args[1].equalsIgnoreCase("force"));
		final String result = maintenanceFacade.enterMaintenanceMode(getCurrentUserName());
		if (force) {
			maintenanceFacade.forceMaintenanceMode();
		}
		return result;
	}

	public void stopMaintenance() throws CommonValidationException {
		maintenanceFacade.exitMaintenanceMode();
	}

	public void clearUserPreferences(String userName) throws CommonFinderException {
		EntityObjectFacadeLocal eoFacade = SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class);
		eoFacade.clearUserPreferences(userName);
	}

	@Override
	public String[] migrateSubformToOnDeleteCascadeConstraint(final boolean bPretend) {
		ArrayList<String> result = new ArrayList<>();
		Set<UID> setFieldsToUpdate = new HashSet<>();
		Set<UID> setFieldsUpdated = new HashSet<>();

		for (UID entityUid : metaProvider.getAllEntityUids()) {
			if (!metaProvider.isNuclosEntity(entityUid)) {
				for (UID layoutUid : getLayoutFacade().getAllLayoutUidsForEntity(entityUid)) {
					try {
						Map<EntityAndField, UID> mpEntityAndParentEntityName = getLayoutFacade().getSubFormEntityAndParentSubFormEntityNamesByLayoutId(layoutUid).stream()
								.collect(HashMap::new, (m, v)->m.put(v.entityAndField, v.parent), HashMap::putAll);
						for (EntityAndField parentEntityForeignKeyField : mpEntityAndParentEntityName.keySet()) {
							FieldMeta<?> foreignKeyField = metaProvider.getEntityField(parentEntityForeignKeyField.getField());
							if (!setFieldsToUpdate.contains(foreignKeyField.getUID()) && !foreignKeyField.isOnDeleteCascade()) {
								if (!metaProvider.getEntity(foreignKeyField.getEntity()).isDynamic()) {
									setFieldsToUpdate.add(foreignKeyField.getUID());
								}
							}
						}
					} catch (Exception e) {
						LOG.warn(e.getMessage(), e);
						result.add("An error occurred: " + e.getMessage());
					}
				}
			}
		}
		for (UID uidField : setFieldsToUpdate) {
			try {
				if (!bPretend) {
					DbMap dbValues = new DbMap();
					dbValues.put(E.ENTITYFIELD.ondeletecascade, true);
					DbQueryBuilder queryBuilder = dataBaseHelper.getDbAccess().getQueryBuilder();
					DbUpdate fieldUpdate = queryBuilder.createUpdate(metaProvider.getEntity(E.ENTITYFIELD.UID)
							, dbValues);
					DbCondition fieldCondition =
							queryBuilder.equalValue(fieldUpdate.baseColumn(E.ENTITYFIELD.getPk()), uidField);
					fieldUpdate.where(fieldCondition);
					dataBaseHelper.getDbAccess().executeUpdate(fieldUpdate);
				}
				setFieldsUpdated.add(uidField);
			} catch (DbException e) {
				result.add("An error occurred: " + e.getMessage());
			}
		}
		for (UID uidField : setFieldsUpdated) {
			FieldMeta<?> fieldMeta = metaProvider.getEntityField(uidField);
			result.add((bPretend ? "Pretend: " : "") + "Updated flag \"on delete cascade\" for field \""
					+ metaProvider.getEntity(fieldMeta.getEntity()).getEntityName() + "."
					+ fieldMeta.getFieldName() +"\" (" + fieldMeta.getUID() + ")");
		}
		return result.toArray(new String[result.size()]);
	}

	@Override
	public String[] setSideviewMenuWidth(final int width, final boolean sharedOnly, final boolean privateOnly, UID... prefs) {
		ArrayList<String> result = new ArrayList<>();
		List<Preference> lstPrefs;
		if (prefs == null) {
			lstPrefs = preferencesFacade.getPreferences(null, NuclosPreferenceType.TABLE.getType(), null, null, false, securityCache.getUserUid(getCurrentUserName()), false, false);
			if (privateOnly) {
				lstPrefs = lstPrefs.stream().filter(preference -> !preference.isShared()).collect(Collectors.toList());
			} else if (sharedOnly) {
				lstPrefs = lstPrefs.stream().filter(preference -> preference.isShared()).collect(Collectors.toList());
			}
		} else {
			lstPrefs = preferencesFacade.getPreferences(prefs)
					.stream()
					.filter(preference ->
						NuclosPreferenceType.TABLE.getType().equals(preference.getType())
							&& PreferenceVO.APP_NUCLOS.equals(preference.getApp())
					)
					.collect(Collectors.toList());
		}
		lstPrefs.forEach(preference -> {
			JsonObject json = preference.getJson();
			JsonObjectBuilder job = Json.createObjectBuilder();
			json.forEach(job::add);
			job.add("sideviewMenuWidth", width);
			Preference.WritablePreference writablePreference = preference.createWriteableObject();
			writablePreference.setJson(job.build());
			try {
				if (!preference.isShared()) {
					preferencesFacade.updatePreference(writablePreference);
				} else {
					preferencesFacade.updatePreferenceShare(writablePreference);
				}
			} catch (CommonPermissionException | CommonFinderException e) {
				result.add("An error occurred: " + e.getMessage());
			}
		});

		return result.toArray(new String[result.size()]);
	}
}
