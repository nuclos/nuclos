//Copyright (C) 2025  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AbstractFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.nuclos.api.annotation.Rule;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.PostConstructManager;
import org.nuclos.server.customcode.codegenerator.RuleClassLoader;
import org.nuclos.server.spring.AutoDbSetupComplete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;

/**
 * Support class to load extensions and search for Nuclos Rules and Spring {@link Component} in order to add them
 * to the extension application context or the rule application context via the {@link RuleClassLoader},
 * depending on the requirements.
 * An extension is only assigned to the RuleClassLoader if it contains rules and NO system rules are present.
 * A system rule is defined by the presence of the annotation {@link SystemRuleUsage}
 */
@Component
public class CustomClassLoaderSupport implements ApplicationContextAware {

	private static final Logger LOG = LoggerFactory.getLogger(CustomClassLoaderSupport.class);

	private static final Object LOAD_LOCK = new Object();

	public static final Set<String> ALL_RULE_TYPES = Collections.unmodifiableSet(
			RuleClassLoader.REGISTERED_EVENT_TYPES.stream()
			.map(Class::getCanonicalName)
			.collect(Collectors.toSet()));

	private enum Provision {SYSTEMCL,RULECL,BOTH}

	private static boolean extensionLoaded = false;
	private static AnnotationConfigApplicationContext ctx;
	private static URLClassLoader cl;
	private static File serverLibDir;
	private static File extensionsLibDir;
	private static final ConcurrentMap<Integer, ConcurrentMap<String, RuleMetaData>> mpRuleMetaData = new ConcurrentHashMap<>();
	private static final ConcurrentMap<File, Set<String>> mpPackagesToScanFor = new ConcurrentHashMap<>();
	private static Collection<File> extensionsWithoutRules = null;
	private static Collection<File> extensionsWithSystemRules = null;
	private static Collection<File> extensionsForRuleClassLoader = null;

	@Override
	public void setApplicationContext(@NonNull ApplicationContext applicationCtx) throws BeansException {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		cl = (URLClassLoader) Thread.currentThread().getContextClassLoader();
		final ServletContext servletCtx = applicationCtx.getBean(ServletContext.class);
		String sLibDir = servletCtx.getRealPath("/WEB-INF/lib/");
		if (sLibDir == null) {
			throw new NuclosFatalException("CustomClassLoaderSupport could not be initialized: WEB-INF/lib not found");
		}
		serverLibDir = new File(sLibDir);
		String sExtensionsLibDir = servletCtx.getRealPath("/WEB-INF/extensions/");
		extensionsLibDir = sExtensionsLibDir != null ? new File(sExtensionsLibDir) : null;
		ctx = ((AutoDbSetupComplete.CompleteWebApplicationContext) applicationCtx).getExtensionContext();
	}

	private CustomClassLoaderSupport() {
	}

	/**
	 * Loads extensions without rules or with system rules into the system class loader
	 * and the extension application context. Scans for {@link Component}.
	 * Only executed once at system startup.
	 */
	public void loadExtensions() {
		if (extensionLoaded) {
			throw new IllegalStateException("Extension loading has already been called");
		}
		extensionLoaded = true;
		try {
			final List<File> toLoad = Stream.concat(getExtensionsWithoutRules().stream(), getExtensionsWithSystemRules().stream())
					.sorted().collect(Collectors.toList());
			if (!toLoad.isEmpty()) {
				final Environment env = new StandardEnvironment();
				final ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver(createScanClassLoader(toLoad));
				final MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);
				LOG.info("Loading extensions to <System> class path: [{}]", toLoad.stream().map(File::getAbsolutePath).collect(Collectors.joining(", ")));
				final TreeSet<String> basePackagesToScan = getBasePackagesOnly(toLoad.parallelStream().map(ext -> {
						if (loadExtension(ext, cl)) {
							return getPackagesWithAnnotationsToScanFor(ext, env, resourcePatternResolver, metadataReaderFactory);
						}
						return Collections.<String>emptySet();
					})
					.flatMap(Collection::stream)
					.collect(Collectors.toCollection(TreeSet::new)));
				if (!basePackagesToScan.isEmpty()) {
					LOG.info("Spring annotation <Extension> context scan for base packages: [{}]", String.join(", ", basePackagesToScan));
					ctx.scan(basePackagesToScan.toArray(new String[0]));
				}
			}
		} finally {
			ctx.refresh();
			ctx.start();
		}
	}

	/**
	 * Used by the {@link CustomCodeManager} to search for components, functions and more
	 * @param ruleClassLoader For the application context.
	 * @return Context for rule operations. Parent is the extension context.
	 */
	public AnnotationConfigApplicationContext createRuleApplicationContext(RuleClassLoader ruleClassLoader) {
		final AnnotationConfigApplicationContext result = new AnnotationConfigApplicationContext();
		result.setParent(ctx);
		result.setClassLoader(ruleClassLoader);
		return result;
	}

	/**
	 * Adds all extensions with rules to the given {@link RuleClassLoader} and returns the packages that need to be searched.
	 * Extensions without rules or with a system rule have already been loaded via the {@link CustomClassLoaderSupport#loadExtensions)
	 * @param ruleClassLoader For adding and package searching
	 * @param ruleJar Also added and searched
	 * @return Packages that need to be scanned
	 */
	public Set<String> addRulesFromExtensions(RuleClassLoader ruleClassLoader, File ruleJar) {
		if (ruleClassLoader == null) {
			throw new IllegalArgumentException("ruleClassLoader cannot be null");
		}
		final List<File> toLoad = Stream.concat(Stream.of(ruleJar), getExtensionsForRuleClassLoader().stream())
				.sorted().collect(Collectors.toList());
		if (!toLoad.isEmpty()) {
			LOG.info("Loading extensions to <Rule> class path: [{}]", toLoad.stream().map(File::getAbsolutePath).collect(Collectors.joining(", ")));
			final Environment env = new StandardEnvironment();
			final ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver(createScanClassLoader(toLoad));
			final MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);
			TreeSet<String> basePackagesToScan = getBasePackagesOnly(toLoad.parallelStream()
					.map(ext -> {
						if (loadExtension(ext, ruleClassLoader)) {
							if (ext == ruleJar) { // no caching
								return getPackagesWithAnnotationsToScanFor(ext, env, resourcePatternResolver, metadataReaderFactory);
							}
							return mpPackagesToScanFor.computeIfAbsent(ext, k ->
									getPackagesWithAnnotationsToScanFor(ext, env, resourcePatternResolver, metadataReaderFactory));
						}
						return Collections.<String>emptySet();})
					.flatMap(Collection::stream)
					.collect(Collectors.toCollection(TreeSet::new)));
			if (!basePackagesToScan.isEmpty()) {
				LOG.info("Spring annotation <Rule> context scan for base packages: [{}]", String.join(", ", basePackagesToScan));
				return basePackagesToScan;
			}
		}
		return Collections.emptySet();
	}

	/**
	 *
	 * @param ext Extension zo be loaded
	 * @param cl ClassLoader for loading, must be an instance of {@link URLClassLoader}
	 * @return Added to the given class loader?
	 */
	private static boolean loadExtension(File ext, ClassLoader cl) {
		if (ext == null) { // rule jar
			return false;
		}
		synchronized (LOAD_LOCK) {
			try {
				final Method addURLMethod = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
				addURLMethod.setAccessible(true);
				addURLMethod.invoke(cl, ext.toPath().toUri().toURL());
				return true;
			} catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
				LOG.error("Extension could not be loaded: {} on {}", ex, ext, ex);
			}
			return false;
		}
	}

	/**
	 * @return Cached collection of extension jars without rules
	 */
	private Collection<File> getExtensionsWithoutRules() {
		Collection<File> result = extensionsWithoutRules;
		if (result == null) {
			result = new ArrayList<>(getAllExtensions());
			final List<File> allRuleJars = getAllRulesFromClasspath().values().stream()
					.map(RuleMetaData::getJarFile).distinct().collect(Collectors.toList());
			result.removeIf(allRuleJars::contains);
			extensionsWithoutRules = Collections.unmodifiableCollection(result);
		}
		return Collections.unmodifiableCollection(result);
	}

	/**
	 * @return Cached collection of extension jars with system rules
	 * A system rule is defined by the presence of the annotation {@link SystemRuleUsage}
	 */
	private Collection<File> getExtensionsWithSystemRules() {
		Collection<File> result = extensionsWithSystemRules;
		if (result == null) {
			result = new ArrayList<>(getAllExtensions());
			result.retainAll(getSystemRulesFromClasspath().values().stream()
					.map(RuleMetaData::getJarFile).distinct().collect(Collectors.toList()));
			extensionsWithSystemRules = Collections.unmodifiableCollection(result);
		}
		return Collections.unmodifiableCollection(result);
	}

	/**
	 * Extensions with rules (No system rules) only!
	 * Attention, an extension with rules installed via installer is also return for backward compatibility.
	 * In rare cases this can lead to problems, because these classes are loaded by the system and the RuleClassLoader.
	 * RuleClassLoader would be the only correct one, as it supplies the {@link org.nuclos.api.businessobject.BusinessObject}s
	 * If problems occur, it is better to install such extensions with a Nuclet.
	 * @return Cached collection of extension jars for the {@link RuleClassLoader}
	 */
	private Collection<File> getExtensionsForRuleClassLoader() {
		Collection<File> result = extensionsForRuleClassLoader;
		if (result == null) {
			final Collection<File> allExtensions = getAllExtensions();
			result = new ArrayList<>(allExtensions);
			final Collection<File> extensionsWithoutRules = getExtensionsWithoutRules();
			final Collection<File> extensionsWithSystemRules = getExtensionsWithSystemRules();
			result.removeIf(extensionsWithoutRules::contains);
			result.removeIf(extensionsWithSystemRules::contains);
			// Attention, an extension installed via installer exists in both ClassLoaders! Necessary for backward compatibility
			final List<File> extensionsFromInstaller = getRulesFromClasspathForRuleClassLoader().values().stream()
					.map(RuleMetaData::getJarFile).distinct().collect(Collectors.toList());
			extensionsFromInstaller.removeIf(allExtensions::contains);
			result.addAll(extensionsFromInstaller);
			extensionsForRuleClassLoader = Collections.unmodifiableCollection(result);
		}
		return Collections.unmodifiableCollection(result);
	}

	/**
	 * @return All rules (regular + system) also Nuclos internal
	 */
	public ConcurrentMap<String, RuleMetaData> getAllRulesFromClasspath() {
		return getRulesFromClasspath(ALL_RULE_TYPES, Provision.BOTH, true);
	}

	/**
	 * @return Only system rules, also Nuclos internal
	 */
	public ConcurrentMap<String, RuleMetaData> getSystemRulesFromClasspath() {
		return getRulesFromClasspath(ALL_RULE_TYPES, Provision.SYSTEMCL, true);
	}

	/**
	 * @return Regular rules from db and extensions only, no system rules (no Nuclos internal)
	 */
	public ConcurrentMap<String, RuleMetaData> getRulesFromClasspathForRuleClassLoader() {
		return getRulesFromClasspath(ALL_RULE_TYPES, Provision.RULECL, false);
	}

	/**
	 * @return Rules (regular + system) also Nuclos internal, which implement the given interfaces
	 */
	public ConcurrentMap<String, RuleMetaData> getRulesFromClasspath(Set<String> interfaces) {
		return getRulesFromClasspath(interfaces, Provision.BOTH, true);
	}

	/**
	 * Analyzes the class files of the libs found in the system that may contain possible rules.
	 * This includes all Nuclet extensions and extensions installed via an installer that follow the convention:
	 * *-server-*.jar, *-rule-*.jar or *-rules-*.jar
	 * @param interfaces For filtering, at leased the rule must one of these interfaces
	 * @param provision (System-ClassLoader, Rule-ClassLoader or Both)
	 * @param withNuclosInternal (nuclos-server-*.jar)
	 * @return Map of found rule name and metadata
	 * Results are cached
	 */
	private ConcurrentMap<String, RuleMetaData> getRulesFromClasspath(Set<String> interfaces, Provision provision, boolean withNuclosInternal) {
		final ConcurrentMap<String, RuleMetaData> ruleClasses = new ConcurrentHashMap<>();
		final Collection<File> jarsToScan = getLibsToScanForRules(withNuclosInternal);
		final ClassLoader scanCl = createScanClassLoaderWithAllExtensions();
		int key = interfaces.hashCode() ^ jarsToScan.hashCode() ^ provision.hashCode();
		if (mpRuleMetaData.containsKey(key)) {
			return mpRuleMetaData.get(key);
		}

		final Environment env = new StandardEnvironment();
		final ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver(scanCl);
		final MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);

		jarsToScan.parallelStream().forEach(jar -> {
			// Search for classes within the current ClassLoader-URLs for loading classes and resources
			ArrayList<File> searchFile = getClassFiles(jar, ".class");

			for (File curFile : searchFile) {
				// ignore the java 9+ module dependency class
				if ("module-info.class".equals(curFile.getName()) || "package-info.class".equals(curFile.getName())) {
					continue;
				}

				// Extract filename and filepath to retrieve resource information
				String path = curFile.getPath().substring(0, curFile.getPath().indexOf(curFile.getName()));
				String convertedResourcePath =
						ClassUtils.convertClassNameToResourcePath(env.resolveRequiredPlaceholders(path));

				// get the resource
				Resource resource = resourcePatternResolver.getResource(convertedResourcePath + curFile.getName());

				// Get the metainformation of the resource
				MetadataReader metadataReader = null;
				try {
					metadataReader = metadataReaderFactory.getMetadataReader(resource);
				} catch (IOException e) {
					if (!convertedResourcePath.startsWith("1/0/")) {
						LOG.info("{} not found: {}: {}", jar, resource, e.toString());
					}
				// } catch (Exception e) { ... is not enough: NoClassDefFoundError is an Error (Throwable)
				} catch (Throwable e) {
					LOG.error(e.getMessage(), e);
				}

				if (metadataReader != null) {
					metadataReader.getClassMetadata();
					for (String inter : interfaces) {
						boolean contains =
								Arrays.asList(metadataReader.getClassMetadata().getInterfaceNames()).contains(inter);

						// If the resource implements one of the ruleInterfaces the resource will
						// be listed as an executable event support
						if (contains) {
							ClassMetadata classMetadata = metadataReader.getClassMetadata();
							AnnotationMetadata anno = metadataReader.getAnnotationMetadata();
							final boolean systemRule = anno.hasAnnotation(SystemRuleUsage.class.getCanonicalName());
							if ((provision == Provision.SYSTEMCL && !systemRule)
									|| (provision == Provision.RULECL && systemRule)) {
								break;
							}

							String ruleName = resource.getFilename();
							String ruleDescription = classMetadata.getClassName();
							String ruleClassName = classMetadata.getClassName();

							Date ruleCompilationDate = null;
							long lastModified = 0;
							try {
								lastModified = resource.lastModified();
								if (lastModified != 0) {
									ruleCompilationDate = new Date(lastModified);
								}
							} catch (Exception e) {
								LOG.trace("Last modified {} could not be parsed to a compilation date: {}", lastModified, e.getMessage(), e);
							}

							String rulePackagePath = ClassUtils.convertResourcePathToClassName(
									env.resolveRequiredPlaceholders(env.resolveRequiredPlaceholders(path)));

							if (!rulePackagePath.trim().isEmpty() && rulePackagePath.lastIndexOf(".") == rulePackagePath.length() - 1)
								rulePackagePath = rulePackagePath.substring(0, rulePackagePath.length() - 1);

							if (anno.hasAnnotation(Rule.class.getCanonicalName())) {
								Map<String, Object> annotationAttributes = anno.getAnnotationAttributes(Rule.class.getCanonicalName());
								if (annotationAttributes != null) {
									if (annotationAttributes.containsKey("name")) {
										ruleName = (String) annotationAttributes.get("name");
									}
									if (annotationAttributes.containsKey("description")) {
										ruleDescription = (String) annotationAttributes.get("description");
									}
								}
							}

							if (!ruleClasses.containsKey(ruleClassName)) {
								RuleMetaData meta = new RuleMetaData(
										ruleName, ruleDescription, ruleClassName, rulePackagePath,
										ruleCompilationDate, systemRule, jar);
								meta.addRuleInterface(inter);
								ruleClasses.put(ruleClassName, meta);
							} else {
								ruleClasses.get(ruleClassName).addRuleInterface(inter);
							}
						}
					}
				}
			}
		});
		mpRuleMetaData.put(key, ruleClasses);
		return ruleClasses;
	}

	/**
	 * Extensions are only added if they are not contained in the parent (cl)
	 * @return ClassLoader for scanning.
	 */
	private ClassLoader createScanClassLoaderWithAllExtensions() {
		return createScanClassLoader(getAllExtensions());
	}

	/**
	 * Extensions are only added if they are not contained in the parent (cl)
	 * @return ClassLoader for scanning.
	 */
	private ClassLoader createScanClassLoader(Collection<File> jarsToScan) {
		final List<URL> urls = new ArrayList<>(Arrays.asList(cl.getURLs()));
		jarsToScan.forEach(jar -> {
			try {
				URL url = jar.toPath().toUri().toURL();
				if (!urls.contains(url)) {
					urls.add(url);
				}
			} catch (Exception e) {
				LOG.warn("addJar {} failed with: {}", jar, e.getMessage());
			}
		});
		return new URLClassLoader(urls.toArray(new URL[0]), null);
	}

	/**
	 * @param withNuclosJar Include nuclos-server-*.jar
	 * @return List of found libs
	 * This includes all Nuclet extensions and extensions installed via an installer that follow the convention:
	 * 	 * *-server-*.jar, *-rule-*.jar or *-rules-*.jar
	 */
	private List<File> getLibsToScanForRules(boolean withNuclosJar) {
		List<File> result = new ArrayList<>();
		result.addAll(FileUtils.listFiles(serverLibDir, new AbstractFileFilter() {
			@Override
			public boolean accept(final File file) {
				String name = file.getName().toLowerCase();
				boolean returnLib = name.endsWith(".jar") &&
						// include nuclos (system rules) here
						//!name.startsWith("nuclos-") &&
						((name.contains("-server-") &&
								!name.startsWith("jersey-server-") &&
								!name.startsWith("jetty-server-")) ||
								name.contains("-rule-") ||
								name.contains("-rules-"));
				if (!withNuclosJar &&
						name.startsWith("nuclos-") && name.contains("-server-")) {
					returnLib = false;
				}
				return returnLib;
			}
		}, TrueFileFilter.INSTANCE));

		result.addAll(getAllExtensions());
		return result;
	}

	/**
	 * @return Collection of libs found in the extension dir (/WEB-INF/extensions/)
	 */
	private Collection<File> getAllExtensions() {
		if (extensionsLibDir != null && extensionsLibDir.exists() && extensionsLibDir.isDirectory()) {
			return FileUtils.listFiles(extensionsLibDir, new AbstractFileFilter() {
				@Override
				public boolean accept(final File file) {
					return file.getName().toLowerCase().endsWith(".jar");
				}
			}, TrueFileFilter.INSTANCE);
		}
		return Collections.emptyList();
	}

	/**
	 * This method scans the given directory, subdirectories and containing jars files
	 * and returns a list of all files that correspond with the given filePattern
	 * @param root - Directory oder File to search in
	 * @param filenamePattern - value to search for
	 * @return List of all files that correspond with the filenamePattern within the given directory
	 */
	private static ArrayList<File> getClassFiles(File root, String filenamePattern) {
		ArrayList<File> matches = new ArrayList<File> ();
		if (root.isFile())
		{
			// Does the found file correspond with the file pattern
			if (root.getName().endsWith(filenamePattern)) {
				matches.add(root);
			}
			else if (root.getName().endsWith(".jar"))
			{
				try (JarFile jarFile = new JarFile(root)) {
					Enumeration<JarEntry> jarEntries = jarFile.entries();
					while(jarEntries.hasMoreElements())
					{
						JarEntry foundElement = jarEntries.nextElement();
						if (foundElement.getName().contains(filenamePattern))
						{
							File myFile = new File(foundElement.getName());
							matches.add(myFile);
						}
					}
				} catch (IOException e) {
					LOG.warn("{} could not be opened for scanning: {}", root.getName(), e.getMessage(), e);
				}
			}
		}
		else if (root.isDirectory())
		{
			// Recursive call for subdirectories
			matches.addAll(getClassFiles(root, filenamePattern));
		}
		return matches;
	}

	/**
	 * Analyzes the class files of the given lib
	 * @return Packages which should be scanned for {@link Component}s
	 */
	private static Set<String> getPackagesWithAnnotationsToScanFor(
			final File jarContainingRulesOrCodeGen,
			final Environment env,
			final ResourcePatternResolver resourcePatternResolver,
			final MetadataReaderFactory metadataReaderFactory) {
		final Set<String> result = ConcurrentHashMap.newKeySet();

		// final boolean bIsNucletExtension = jarContainingRulesOrCodeGen.getName().startsWith(NucletExtensionInstaller.EXTENSION_FILE_IDENTIFIER);
		getClassFiles(jarContainingRulesOrCodeGen, ".class")
			.forEach(clazz -> {
				// ignore the java 9+ module dependency class
				if ("module-info.class".equals(clazz.getName())) {
					return;
				}
				// Extract filename and filepath to retrieve resource information
				String path = clazz.getPath().substring(0, clazz.getPath().indexOf(clazz.getName()));
				String convertedResourcePath =
						ClassUtils.convertClassNameToResourcePath(env.resolveRequiredPlaceholders(path));
				// get the resource
				Resource resource = resourcePatternResolver.getResource(convertedResourcePath + clazz.getName());
				try {
					final MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);
					final ClassMetadata classMetadata = metadataReader.getClassMetadata();
					final AnnotationMetadata annotationMetadata = metadataReader.getAnnotationMetadata();
					if (classMetadata.getClassName().lastIndexOf('.') == -1) {
						return;
					}
					final String sPackage = classMetadata.getClassName().substring(0, classMetadata.getClassName().lastIndexOf('.'));
					if (annotationMetadata.hasAnnotation(Component.class.getCanonicalName())
							|| annotationMetadata.hasAnnotation(Service.class.getCanonicalName())
							|| annotationMetadata.hasAnnotation(Controller.class.getCanonicalName())
							|| annotationMetadata.hasAnnotation(Repository.class.getCanonicalName())) {
						result.add(sPackage);
					}
				} catch (IOException e) {
					if (!convertedResourcePath.startsWith("1/0/")) {
						LOG.warn("{} Class metadata could not be read from {}: {}", resource, jarContainingRulesOrCodeGen, e.getMessage());
					}
				}
			});
		return result;
	}

	/**
	 * Remove subpackages from given list and return a new sorted set of base packages only.
	 * @return {@link TreeSet} of base packages only.
	 */
	private static TreeSet<String> getBasePackagesOnly(TreeSet<String> packages) {
		final TreeSet<String> basePackages = new TreeSet<>();
		for (String sPackage : packages) {
			boolean isSubPackage = false;
			for (String basePackage : basePackages) {
				if (sPackage.startsWith(basePackage + ".")) {
					isSubPackage = true;
					break;
				}
			}
			if (!isSubPackage) {
				basePackages.add(sPackage);
			}
		}
		return basePackages;
	}

	/**
	 * Analysis result of a found rule class
	 */
	public static final class RuleMetaData {
		final String ruleName;
		final String ruleDescription;
		final String ruleClassName;
		final String rulePackagePath;
		final Date ruleCompilationDate;
		final boolean systemRule;
		final File jarFile;
		final Set<String> ruleInterfaces = ConcurrentHashMap.newKeySet();

		public RuleMetaData(final String ruleName, final String ruleDescription, final String ruleClassName,
							final String rulePackagePath, final Date ruleCompilationDate, final boolean systemRule,
							final File jarFile) {
			this.ruleName = ruleName;
			this.ruleDescription = ruleDescription;
			this.ruleClassName = ruleClassName;
			this.rulePackagePath = rulePackagePath;
			this.ruleCompilationDate = ruleCompilationDate;
			this.systemRule = systemRule;
			this.jarFile = jarFile;
		}

		public String getRuleName() {
			return ruleName;
		}

		public String getRuleDescription() {
			return ruleDescription;
		}

		public String getRuleClassName() {
			return ruleClassName;
		}

		public String getRulePackagePath() {
			return rulePackagePath;
		}

		public Date getRuleCompilationDate() {
			return ruleCompilationDate;
		}

		public boolean isSystemRule() {
			return systemRule;
		}

		public File getJarFile() {
			return jarFile;
		}

		public void addRuleInterface(final String ruleInterface) {
			ruleInterfaces.add(ruleInterface);
		}

		public Set<String> getRuleInterfaces() {
			return Collections.unmodifiableSet(ruleInterfaces);
		}
	}

}
