//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.HasCryptoHash;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.HasPrimaryKey;

/**
 * A representation of a 'new rule'/event object (java code compiled by Nuclos)
 * with information on its source on the filesystem and the db object backing it.
 *
 * @author Thomas Pasch
 */
public class GeneratedFile implements HasPrimaryKey<UID>, HasCryptoHash {

	static final String COMMENT_START = "// BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)";
	static final String COMMENT_END = "// END NUCLOS CODEGENERATOR COMMENT";

	/**
	 * Flag indicating if additional information about this file should be appended to
	 * the source as a comment.
	 * <p>
	 * In real life the comment looks like this (when written to filesystem):
	 * <pre><code>
	 * // BEGIN NUCLOS CODEGENERATOR COMMENT (DO NOT REMOVE)
	 * // class=org.nuclos.server.customcode.codegenerator.PlainCodeGenerator
	 * // type=org.nuclos.server.customcode.valueobject.CodeVO
	 * // entity=nuclos_servercode
	 * // name=de.bos.nuclos.hrdb.HRUtils
	 * // id=77013214
	 * // version=399
	 * // modified=1395676536000
	 * // date=2014-03-24 16:55:36.0
	 * // END NUCLOS CODEGENERATOR COMMENT
	 * </code></pre>
	 */
	private final boolean writeProperties;

	/**
	 * File on disk.
	 */
	private File file;

	/**
	 * Fully qualified (java) class name.
	 */
	private String name;

	private String targetClassName;

	private String type;

	private EntityMeta<?> entity;

	private String generatorClass;

	private UID uid;

	private char[] content;

	private String hashValue;

	GeneratedFile(boolean writeProperties) {
		this.writeProperties = writeProperties;
	}

	/**
	 * Return the fully qualified (java) class name.
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	void setType(String type) {
		this.type = type;
	}

	private String getGeneratorClass() {
		return generatorClass;
	}

	void setGeneratorClass(String generatorClass) {
		this.generatorClass = generatorClass;
	}

	@Override
	public UID getPrimaryKey() {
		return uid;
	}

	void setPrimaryKey(UID uid) {
		this.uid = uid;
	}

	public char[] getContent() {
		return content;
	}

	void setContent(char[] content) {
		this.content = content;
	}

	public File getFile() {
		return file;
	}

	void setFile(File file) {
		this.file = file;
	}

	private String getTargetClassName() {
		return targetClassName;
	}

	void setTargetClassName(String targetClassName) {
		this.targetClassName = targetClassName;
	}

	public EntityMeta getEntity() {
		return entity;
	}

	public void setEntity(EntityMeta<?> entity) {
		this.entity = entity;
	}

	public String getHashValue() {
		return hashValue;
	}

	void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}

	Map<String, String> getProperties() {
		final Map<String, String> result = new TreeMap<>();

		if (writeProperties) {
			result.put("class", getGeneratorClass());
			result.put("type", getType());
			result.put("entity", "" + getEntity());
			result.put("name", getName());
			if (getTargetClassName() != null) {
				result.put("classname", getTargetClassName());
			}
			result.put("uid", "" + getPrimaryKey());
		}

		return result;
	}

	String getPropertiesAsComment() {
		if (writeProperties) {
			return CodeGenerator.propertiesToComment(getProperties());
		}

		return "";
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("GeneratedFile[");
		if (name != null) {
			result.append("name=").append(name).append(", ");
		}
		if (file != null) {
			result.append("file=").append(file).append(", ");
		}
		result.append("uid=").append(uid).append(", ");
		if (hashValue != null) {
			result.append("hashValue=").append(hashValue).append(", ");
		}
		if (type != null) {
			result.append("type=").append(type).append(", ");
		}
		if (targetClassName != null) {
			result.append("target=").append(targetClassName).append(", ");
		}
		result.append("]");
		return result.toString();
	}

}
