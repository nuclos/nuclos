package org.nuclos.server.customcode.codegenerator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PostConstructManager;
import org.nuclos.server.common.ServerParameterProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class GeneratorClasspathComponent implements ApplicationContextAware {
	
	private static final Logger LOG = LoggerFactory.getLogger(GeneratorClasspathComponent.class);
	
	//

	@Autowired
	private ServerParameterProvider parameterProvider;

	private List<File> expandedGeneratorClassPath;
	
	private List<File> expandedGeneratorClassPathApiOnly;
	
	private ApplicationContext applicationContext;
	
	GeneratorClasspathComponent() {
	}
	
	@PostConstruct
	void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		if (NuclosCodegeneratorConstants.DEV_EXTENSION_ENABLED
				|| !parameterProvider.isEnabled(ParameterProvider.CODEGENERATOR_ENABLED, true)) {
			return;
		}
		expandedGeneratorClassPath = getExpandedGeneratorClassPathFactory(false);
		expandedGeneratorClassPathApiOnly = getExpandedGeneratorClassPathFactory(true);
	}
	
	private List<File> getExpandedGeneratorClassPathFactory(final boolean apiOnly) {
		final List<File> classPath = new ArrayList<File>();
		final Resource r = applicationContext.getResource("WEB-INF/lib/");
		try {
			classPath.addAll(getLibs(r.getFile(), apiOnly));
		}
		catch (IOException e) {
			throw new NuclosFatalException(e);
		}
		return classPath;
	}

	/**
	 * Returns the expanded class path for system parameter {@code nuclos.codegenerator.class.path}.
	 * Note: WSDL libraries are not included.
	 */
	public List<File> getExpandedSystemParameterClassPath(boolean apiOnly) {
		if (apiOnly) {
			return expandedGeneratorClassPathApiOnly;
		} else {
			return expandedGeneratorClassPath;
		}
	}

	public List<File> getLibs(File folder, boolean apiOnly) {
		final List<File> files = new ArrayList<File>();
		if(!folder.isDirectory()) {
			// just return empty list, compiler will give notice if classes are missing
			return files;
		}
		for(File file : folder.listFiles()) {
			final String filename = file.getName().toLowerCase();
			if (!filename.endsWith(".jar")) {
				continue;
			}
			boolean add = true;
			if (apiOnly && filename.startsWith("nuclos-")
					&& !filename.contains("-api-")
					&& !filename.contains("-ccce-")
					// for the 'nuclos-integration-test-rules' module:
					&& !filename.contains("-rules-")) {
				add = false;
			}
			if (add) {
				files.add(file);
			}
		}		
		return files;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}	
	
}
