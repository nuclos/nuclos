//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.PostConstructManager;
import org.nuclos.server.common.NuclosSystemParameters;
import org.slf4j.LoggerFactory;

public class NuclosCodegeneratorConstants {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NuclosCodegeneratorConstants.class);

	public static final String JAVA_SRC_ENCODING = "UTF-8";

	/*
		Set this environment variable in your IDE to your rules maven module,
		 to generate business object classes and more in src/nuclos-generated/java
		 e.g. '/home/myUser/Nuclos/workspaces/project-fdm/fdm-rules/'
		A default pom.xml is generated once if it does not already exist.
	 */
	public static final String DEV_EXTENSION_GENERATOR_OUTPUTDIR = "DEV_EXTENSION_GENERATOR_OUTPUTDIR";

	public static final File DEV_EXTENSION_GENERATOR_SRCDIR;

	public static final File SYSTEM_PARAMETERS_GENERATOR_FOLDER;

	/** Base directory where the generated code is based on (e.g. data/codegenerator/) */
	public static final File GENERATOR_FOLDER;

	public static final boolean DEV_EXTENSION_ENABLED;

	public static final String DEV_EXTENSION_RELATIVE_FILE_SYSTEM_PATH = "src" + File.separatorChar + "nuclos-generated" + File.separatorChar + "java";
	public static final String DEV_EXTENSION_RELATIVE_POM_PATH = "src/nuclos-generated/java";
	
	static {
		if (PostConstructManager.isDisabled()) {
			// JUnit test only
			DEV_EXTENSION_GENERATOR_SRCDIR = null;
			SYSTEM_PARAMETERS_GENERATOR_FOLDER = null;
			GENERATOR_FOLDER = null;
			DEV_EXTENSION_ENABLED = false;
		} else { try {
			SYSTEM_PARAMETERS_GENERATOR_FOLDER = NuclosSystemParameters.getDirectory(NuclosSystemParameters.GENERATOR_OUTPUT_PATH).getCanonicalFile();

			final String sDevExtDir = System.getenv().get(DEV_EXTENSION_GENERATOR_OUTPUTDIR);
			File devExtDir = null;
			File devExtSrcDir = null;
			if (sDevExtDir != null) {
				devExtDir = new File(sDevExtDir);
				if (devExtDir.exists() && !devExtDir.isDirectory()) {
					throw new NuclosFatalException("Environment variable "+DEV_EXTENSION_GENERATOR_OUTPUTDIR+" is not a directory!");
				}
				devExtDir = createDirIfNecessary(devExtDir);
				if (devExtDir != null) {
					devExtSrcDir = new File(sDevExtDir + File.separatorChar + DEV_EXTENSION_RELATIVE_FILE_SYSTEM_PATH)
							.getCanonicalFile();
					devExtSrcDir = createDirIfNecessary(devExtSrcDir);
					if (devExtSrcDir == null) {
						devExtDir = null;
					}
				}
			}
			DEV_EXTENSION_ENABLED = devExtDir != null;
			DEV_EXTENSION_GENERATOR_SRCDIR = DEV_EXTENSION_ENABLED ? devExtSrcDir : null;
			GENERATOR_FOLDER = DEV_EXTENSION_ENABLED ? devExtDir : SYSTEM_PARAMETERS_GENERATOR_FOLDER;
		} catch (IOException e) {
			throw new ExceptionInInitializerError(e);
		}}
	}

	private static File source(File defaultSourceFolder) {
		if (DEV_EXTENSION_ENABLED) {
			return DEV_EXTENSION_GENERATOR_SRCDIR;
		}
		return defaultSourceFolder;
	}

	public static final File   BUILD_OUTPUT_PATH = new File(GENERATOR_FOLDER, ".build");

	public static final String CCCE_NAME = "nuclos-ccce.jar";
	// moved to nuclos-ccce maven module
	//public static final File   CCCEJARFILE = new File(GENERATOR_FOLDER, CCCE_NAME);

	public static final String SRC_DIR_NAME = "src-rule";
	public static final File   SOURCE_OUTPUT_PATH = source(new File(GENERATOR_FOLDER, SRC_DIR_NAME));

	public static final String JAR_NAME = "rule.jar";
	// TODO remove duplicate constant
	public static final File   JAR_SRC_FOLDER = SOURCE_OUTPUT_PATH;
	public static final File   JARFILE = new File(GENERATOR_FOLDER, JAR_NAME);
	public static final String JAR_OLD_NAME = "rule.jar.old";
	public static final File   JARFILE_OLD = new File(GENERATOR_FOLDER, JAR_OLD_NAME);
	
	public static final String BO_NAME =  "conf-business-object.jar";
	public static final String BO_SRC_DIR_NAME = "src-conf-business-object";
	public static final File   BO_SRC_FOLDER = source(new File(GENERATOR_FOLDER, BO_SRC_DIR_NAME));
	public static final File   BOJARFILE = new File(GENERATOR_FOLDER, BO_NAME);
	
	public static final String DATASOURCEREPORT_NAME = "conf-datasource.jar";
	public static final String DATASOURCEREPORT_SRC_DIR_NAME = "src-conf-datasource";
	public static final File   DATASOURCEREPORT_SRC_FOLDER = source(new File(GENERATOR_FOLDER, DATASOURCEREPORT_SRC_DIR_NAME));
	public static final File   DATASOURCEREPORTJARFILE = new File(GENERATOR_FOLDER, DATASOURCEREPORT_NAME);

	public static final String REPORT_NAME = "conf-report.jar";
	public static final String REPORT_SRC_DIR_NAME = "src-conf-report";
	public static final File   REPORT_SRC_FOLDER = source(new File(GENERATOR_FOLDER, REPORT_SRC_DIR_NAME));
	public static final File   REPORTJARFILE = new File(GENERATOR_FOLDER, REPORT_NAME);

    public static final File REST_OUTPUT_PATH = new File(GENERATOR_FOLDER, "apis");
	public static final String REST_NAME = "conf-rest.jar";
	public static final String REST_SRC_DIR_NAME = "src-conf-rest";
	public static final File   REST_SRC_FOLDER = source(new File(GENERATOR_FOLDER, REST_SRC_DIR_NAME));
	public static final File   RESTJARFILE = new File(GENERATOR_FOLDER, REST_NAME);

	public static final File   WSDL_OUTPUT_PATH = new File(GENERATOR_FOLDER, "wsdl");
	public static final String WEBSERVICE_NAME = "conf-webservice.jar";
	public static final String WEBSERVICE_SRC_DIR_NAME = "src-conf-webservice";
	public static final File   WEBSERVICE_SRC_FOLDER = source(new File(GENERATOR_FOLDER, WEBSERVICE_SRC_DIR_NAME));
	public static final File   WEBSERVICEJARFILE = new File(GENERATOR_FOLDER, WEBSERVICE_NAME);
	
	public static final String IMPORTSTRUCTUREDEFS_NAME = "conf-import-structure-definition.jar";
	public static final String IMPORTSTRUCTUREDEFS_SRC_DIR_NAME = "src-conf-import-structure-definition";
	public static final File   IMPORTSTRUCTUREDEFS_SRC_FOLDER = source(new File(GENERATOR_FOLDER, IMPORTSTRUCTUREDEFS_SRC_DIR_NAME));
	public static final File   IMPORTSTRUCTUREDEFSJARFILE = new File(GENERATOR_FOLDER, IMPORTSTRUCTUREDEFS_NAME);
	
	public static final String PRINTOUT_NAME = "conf-printout.jar";
	public static final String PRINTOUT_SRC_DIR_NAME = "src-conf-printout";
	public static final File   PRINTOUT_SRC_FOLDER = source(new File(GENERATOR_FOLDER, PRINTOUT_SRC_DIR_NAME));
	public static final File   PRINTOUTJARFILE = new File(GENERATOR_FOLDER, PRINTOUT_NAME);

	public static final String USERROLE_NAME = "conf-role.jar";
	public static final String USERROLE_SRC_DIR_NAME = "src-conf-role";
	public static final File   USERROLE_SRC_FOLDER = source(new File(GENERATOR_FOLDER, USERROLE_SRC_DIR_NAME));
	public static final File   USERROLEJARFILE = new File(GENERATOR_FOLDER, USERROLE_NAME);
	
	public static final String GENERATION_NAME = "conf-generation.jar";
	public static final String GENERATION_SRC_DIR_NAME = "src-conf-generation";
	public static final File   GENERATION_SRC_FOLDER = source(new File(GENERATOR_FOLDER, GENERATION_SRC_DIR_NAME));
	public static final File   GENERATIONJARFILE = new File(GENERATOR_FOLDER, GENERATION_NAME);
	
	public static final String PARAMETER_NAME = "conf-parameter.jar";
	public static final String PARAMETER_SRC_DIR_NAME = "src-conf-parameter";
	public static final File   PARAMETER_SRC_FOLDER = source(new File(GENERATOR_FOLDER, PARAMETER_SRC_DIR_NAME));
	public static final File   PARAMETERJARFILE = new File(GENERATOR_FOLDER, PARAMETER_NAME);
	
	public static final String COMMUNICATION_NAME = "conf-communication-port.jar";
	public static final String COMMUNICATION_SRC_DIR_NAME = "src-conf-communication-port";
	public static final File   COMMUNICATION_SRC_FOLDER = source(new File(GENERATOR_FOLDER, COMMUNICATION_SRC_DIR_NAME));
	public static final File   COMMUNICATIONJARFILE = new File(GENERATOR_FOLDER, COMMUNICATION_NAME);

	public static final String STATEMODEL_SRC_DIR_NAME = "src-conf-statemodel";
	public static final File   STATEMODEL_SRC_FOLDER = source(new File(GENERATOR_FOLDER, STATEMODEL_SRC_DIR_NAME));
	public static final String STATEMODEL_NAME = "conf-statemodel.jar";
	public static final File   STATEMODELJARFILE = new File(GENERATOR_FOLDER, STATEMODEL_NAME);

	public static final String TEMP_SRC_DIR_NAME = "temp";
	public static final File   TEMP_FOLDER = source(new File(GENERATOR_FOLDER, TEMP_SRC_DIR_NAME));

	public static final String BUSINESS_TEST_SRC_DIR_NAME = "business-test";
	public static final File   BUSINESS_TEST_SRC_FOLDER = new File(SYSTEM_PARAMETERS_GENERATOR_FOLDER, BUSINESS_TEST_SRC_DIR_NAME);
	// Source folder for business test entity classes etc.
	public static final File   BUSINESS_TEST_SRC_MAIN_FOLDER = new File(BUSINESS_TEST_SRC_FOLDER, "main");
	// Source folder for the actual test scripts
	public static final File   BUSINESS_TEST_SRC_TEST_FOLDER = new File(BUSINESS_TEST_SRC_FOLDER, "test");

	public static final File[] SRC_FOLDERS = DEV_EXTENSION_ENABLED ?
			new File[] {
		DEV_EXTENSION_GENERATOR_SRCDIR
	} :
			new File[] {
		JAR_SRC_FOLDER, BO_SRC_FOLDER, IMPORTSTRUCTUREDEFS_SRC_FOLDER, PARAMETER_SRC_FOLDER,
		COMMUNICATION_SRC_FOLDER, DATASOURCEREPORT_SRC_FOLDER, PRINTOUT_SRC_FOLDER,
		USERROLE_SRC_FOLDER, REPORT_SRC_FOLDER, GENERATION_SRC_FOLDER, STATEMODEL_SRC_FOLDER,
		WEBSERVICE_SRC_FOLDER, REST_SRC_FOLDER
	};
	
	public static final List<File> SRC_FOLDERS_LIST;
	
	public static final File[] JAR_FILES = new File[] {
		JARFILE, BOJARFILE, IMPORTSTRUCTUREDEFSJARFILE, PARAMETERJARFILE, COMMUNICATIONJARFILE,
		USERROLEJARFILE, DATASOURCEREPORTJARFILE, GENERATIONJARFILE, PRINTOUTJARFILE, REPORTJARFILE,
		WEBSERVICEJARFILE, STATEMODELJARFILE, RESTJARFILE
	};
	
	public static final List<File> JAR_FILES_LIST;
	
	public static final String[] JAR_NAMES = new String[] {
		JAR_NAME, BO_NAME, IMPORTSTRUCTUREDEFS_NAME, DATASOURCEREPORT_NAME, PRINTOUT_NAME,
		PARAMETER_NAME, COMMUNICATION_NAME, USERROLE_NAME, GENERATION_NAME, REPORT_NAME,
		WEBSERVICE_NAME, STATEMODEL_NAME, REST_NAME
	};
	
	public static final List<String> JAR_NAMES_LIST;

	// moved to nuclos-ccce maven module
	/*static final Class<?>[] CODE_COMPILER_CLASSPATH_EXTENSION = new Class<?>[] {
		AbstractBusinessObject.class, AbstractBusinessObject.NuclosMandatorProvider.class, UID.class, 
		AbstractOutputFormat.class, AbstractPrintout.class,
		nuclosmandator.class, nuclosprintservice.class, nuclosprintservicetray.class, nuclosprocess.class,
		nuclosrole.class, nuclosroleuser.class, nuclosstate.class, nuclosuser.class
	};*/
	
	static {
		try {
			if (SYSTEM_PARAMETERS_GENERATOR_FOLDER == null) {
				SRC_FOLDERS_LIST = null;
				JAR_FILES_LIST = null;
				JAR_NAMES_LIST = null;
			} else {
				SYSTEM_PARAMETERS_GENERATOR_FOLDER.mkdirs();
				if (!SYSTEM_PARAMETERS_GENERATOR_FOLDER.isDirectory()) {
					throw new IOException(SYSTEM_PARAMETERS_GENERATOR_FOLDER.toString());
				}

				SRC_FOLDERS_LIST = Arrays.asList(SRC_FOLDERS);
				JAR_FILES_LIST = Arrays.asList(JAR_FILES);
				JAR_NAMES_LIST = Arrays.asList(JAR_NAMES);
			}
		} catch (IOException e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	private static File createDirIfNecessary(File dir) {
		if (!dir.exists()) {
			try {
				if (!dir.mkdirs()) {
					LOG.warn(dir.getAbsolutePath() + " does not exists and could not be created!");
					return null;
				}
			} catch (SecurityException ex) {
				LOG.error(dir.getAbsolutePath() + " does not exists and could not be created: " + ex.getMessage(), ex);
				return null;
			}
		}
		return dir;
	}

}
