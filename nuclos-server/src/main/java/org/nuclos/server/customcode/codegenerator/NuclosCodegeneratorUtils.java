//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import java.io.File;

public class NuclosCodegeneratorUtils {
	
	private NuclosCodegeneratorUtils() {
	}
	
	public static File eventSupportSource(String qname) {
		return new File(NuclosCodegeneratorConstants.JAR_SRC_FOLDER, qname.replace(".", "/") + ".java");
	}
	
	private static File source(File folder, String packageName, String entity) {
		return new File(folder, packageName.replace(".", File.separator) + File.separatorChar + entity + ".java");
	}
	
	public static File eventSupportSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.JAR_SRC_FOLDER, packageName, entity);
	}

	public static File businessObjectSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.BO_SRC_FOLDER, packageName, entity); 
	}

	public static File datasourceReportSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.DATASOURCEREPORT_SRC_FOLDER, packageName, entity);
	}
	
	public static File statemodelSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.STATEMODEL_SRC_FOLDER, packageName, entity); 
	}

	public static File generatorSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.GENERATION_SRC_FOLDER, packageName, entity);
	}
	
	public static File parameterSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.PARAMETER_SRC_FOLDER, packageName, entity);
	}
	
	public static File communicationSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.COMMUNICATION_SRC_FOLDER, packageName, entity);
	}
	
	public static File reportSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.REPORT_SRC_FOLDER, packageName, entity);
	}
	
	public static File webserviceSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.WEBSERVICE_SRC_FOLDER, packageName, entity);
	}

	public static File restSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.REST_SRC_FOLDER, packageName, entity);
	}
	
	public static File importStructureDefinitionSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFS_SRC_FOLDER, packageName, entity);
	}
	
	public static File printoutSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.PRINTOUT_SRC_FOLDER, packageName, entity);
	}
	
	public static File userRoleSource(String packageName, String entity) {
		return source(NuclosCodegeneratorConstants.USERROLE_SRC_FOLDER, packageName, entity);
	}
	
}
