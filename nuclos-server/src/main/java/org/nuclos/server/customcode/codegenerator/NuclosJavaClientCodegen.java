package org.nuclos.server.customcode.codegenerator;

import org.openapitools.codegen.languages.JavaClientCodegen;

public class NuclosJavaClientCodegen extends JavaClientCodegen {

	@Override
	public void postProcess() {
		//do nothing to prevent output of following message
		/*
		  ################################################################################
		  # Thanks for using OpenAPI Generator.                                          #
		  # Please consider donation to help us maintain this project 🙏                 #
		  # https://opencollective.com/openapi_generator/donate                          #
		  ################################################################################
		 */
	}
}
