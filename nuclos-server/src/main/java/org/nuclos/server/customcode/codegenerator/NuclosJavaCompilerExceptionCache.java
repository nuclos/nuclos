package org.nuclos.server.customcode.codegenerator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;
import org.springframework.stereotype.Component;

@Component
public class NuclosJavaCompilerExceptionCache {

	private Map<String, ErrorMessage> msgs = new HashMap<String, ErrorMessage> ();

	public boolean hasErrors() {
		return !msgs.isEmpty();
	}
	
	public void add(List<ErrorMessage> lstMsgs) {
		for (ErrorMessage msg: lstMsgs) {
			String sourceName = msg.getSource().contains(".java") ?  msg.getSource().substring(0, msg.getSource().indexOf(".java")) : msg.getSource();
			this.msgs.put(msg.getPackage() != null ? msg.getPackage() + "." + sourceName : sourceName, msg);
		}
	}
	
	public void clear() {
		this.msgs.clear();
	}
	
	public Map<String, ErrorMessage> getMessages() {
		return this.msgs;
	}

	public List<ErrorMessage> getMessagesList() {
		return this.msgs.entrySet().stream()
			.sorted((e1, e2) -> StringUtils.compare(e1.getKey(), e2.getKey()))
			.map(Map.Entry::getValue)
			.collect(Collectors.toList());
	}
}
