//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.customcode.codegenerator;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.nuclos.api.rule.AuthenticationRule;
import org.nuclos.api.rule.CommunicationRule;
import org.nuclos.api.rule.CustomRestRule;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.DatasourceRule;
import org.nuclos.api.rule.DeleteFinalRule;
import org.nuclos.api.rule.DeleteRule;
import org.nuclos.api.rule.GenerateFinalRule;
import org.nuclos.api.rule.GenerateRule;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.JobRule;
import org.nuclos.api.rule.PrintFinalRule;
import org.nuclos.api.rule.PrintRule;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.api.rule.TransactionalJobRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.api.rule.UpdateRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

import com.google.common.collect.Lists;

/**
 * ClassLoader for loading business and timelimit rules.
 * Common generated code like common code artifacts and webservice proxies are loaded if present (and active).
 * Each compilation unit is validated by comparing the actual manifest with the artifacts's generated one.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Configurable
public class RuleClassLoader extends OpenURLClassLoader {

	public static final List<Class<?>> REGISTERED_EVENT_TYPES = Lists.newArrayList(
			CustomRule.class,
			CustomRestRule.class,
			DeleteFinalRule.class, DeleteRule.class,
			GenerateFinalRule.class, GenerateRule.class,
			InsertFinalRule.class, InsertRule.class,
			StateChangeFinalRule.class, StateChangeRule.class,
			UpdateFinalRule.class, UpdateRule.class,
			TransactionalJobRule.class, JobRule.class,
			PrintRule.class, PrintFinalRule.class,
			CommunicationRule.class,
			AuthenticationRule.class,
			DatasourceRule.class
	);

	private static final Logger LOG = LoggerFactory.getLogger(RuleClassLoader.class);
	
	//

	private final ClassLoader systemClassLoader;

	public RuleClassLoader(ClassLoader parent) {
		super(new URL[]{}, parent);
		this.systemClassLoader = getSystemClassLoader();
	}

	@Override
	public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
		synchronized(getClassLoadingLock(name)) {
			Class<?> result = findLoadedClass(name);
			if (result == null) {
				try {
					if (systemClassLoader != null) {
						result = systemClassLoader.loadClass(name);
					}
				} catch (ClassNotFoundException ex) {
					// only a first try
				}

				// load javax.* from container. Is required for the Glassfish injection of CustomRestContext.
				if (name.startsWith("javax")) {
					result = super.loadClass(name, resolve);
				}

				try {
					// Reverse the default class loading order, like Spring does.
					// Otherwise, rules from the extension have problems loading the required BusinessObject classes from this RuleClassLoader.
					if (result == null) {
						result = findClass(name);
					}
				} catch (ClassNotFoundException e) {
					// class is not found, let's try it in parent classloader.
					// If class is still not found, then this method will throw ClassNotFoundException.
					result = super.loadClass(name, resolve);
				}
			}

			if (resolve) {
				resolveClass(result);
			}
			return result;
		}
	}

	@Override
	public Enumeration<URL> getResources(String name) throws IOException {
		List<URL> allResources = new LinkedList<>();

		// load resources from sys class loader
		Enumeration<URL> sysResources = systemClassLoader.getResources(name);
		if (sysResources != null) {
			while (sysResources.hasMoreElements()) {
				allResources.add(sysResources.nextElement());
			}
		}

		// load resource from this classloader
		Enumeration<URL> ruleResources = findResources(name);
		if (ruleResources != null) {
			while (ruleResources.hasMoreElements()) {
				allResources.add(ruleResources.nextElement());
			}
		}

		// then try finding resources from parent classloaders
		Enumeration<URL> parentResources = super.findResources(name);
		if (parentResources != null) {
			while (parentResources.hasMoreElements()) {
				allResources.add(parentResources.nextElement());
			}
		}

		return new Enumeration<URL>() {
			Iterator<URL> it = allResources.iterator();

			@Override
			public boolean hasMoreElements() {
				return it.hasNext();
			}

			@Override
			public URL nextElement() {
				return it.next();
			}
		};
	}

	@Override
	public URL getResource(String name) {
		URL result = null;
		if (systemClassLoader != null) {
			result = systemClassLoader.getResource(name);
		}
		if (result == null) {
			result = findResource(name);
		}
		if (result == null) {
			result = super.getResource(name);
		}
		return result;
	}

	public void addJarsToClassPath(File folder) {
		try {
			if(folder.exists()) {
				File[] jarFiles = folder.listFiles(new FileFilter() {
					@Override
					public boolean accept(File arg0) {
						if(arg0.toString().lastIndexOf(".jar") != -1)
							return true;
						else
							return false;
					}
				});
				for(int i = 0; i < jarFiles.length; i++) {
					LOG.debug("{}", jarFiles[i]);
					addURL(new URL(jarFiles[i].toURI().toString()));
				}
			}
		}
		catch(Exception e) {
			LOG.warn("addJarsToClassPath", e);
		}
	}
}
