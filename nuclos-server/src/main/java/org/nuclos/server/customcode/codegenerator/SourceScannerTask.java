//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static org.nuclos.common.AbstractParameterProvider.isValueEnabled;
import static org.nuclos.server.customcode.codegenerator.GeneratedFile.COMMENT_END;
import static org.nuclos.server.customcode.codegenerator.GeneratedFile.COMMENT_START;

import java.io.BufferedReader;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.security.MessageDigestInputStream;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.customcode.ejb3.CodeFacadeLocal;
import org.nuclos.server.eventsupport.ejb3.SourceCache;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.security.NuclosLocalServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

/**
 * @author Thomas Pasch
 */
@Configurable
public class SourceScannerTask implements ISourceScannerTask {

	private static final Logger LOG = LoggerFactory.getLogger(SourceScannerTask.class);

	private static final Pattern PROP_PAT = Pattern.compile("^//\\s*(\\p{Alnum}+)=(.*)$");

	private static final String NUCLOS_WEBSERVICE_SRC_PATH = WsdlCodeGenerator.DEFAULT_PACKAGE_WEBSERVICES.replace('.', File.separatorChar);

	private MetaProvider metaProv;
	private ServerParameterProvider parameterProvider;
	private NuclosLocalServerSession nuclosLocalServerSession;
	private SourceCache sourceCache;
	private CodeFacadeLocal codeFacade;

	private Disposable watchSubscription;

	public SourceScannerTask() {
		LOG.debug("Scanner created");
	}

	@Autowired
	private void inject(
			final MetaProvider metaProv,
			final ServerParameterProvider parameterProvider,
			final NuclosLocalServerSession nuclosLocalServerSession,
			final SourceCache sourceCache,
			final CodeFacadeLocal codeFacade) {
		this.metaProv = metaProv;
		this.parameterProvider = parameterProvider;
		this.nuclosLocalServerSession = nuclosLocalServerSession;
		this.sourceCache = sourceCache;
		this.codeFacade = codeFacade;
	}

	@Override
	public void run() {
		if (NuclosCodegeneratorConstants.DEV_EXTENSION_ENABLED
				|| !isCodeGeneratorEnabled()
		) {
			return;
		}
		try {
			startWatcher();
			LOG.info("Scanner started");
		} catch (Exception e) {
			LOG.warn("Scanner failed: ", e);
		}
	}

	@Override
	public synchronized void cancel() {
		if (watchSubscription != null) {
			try {
				watchSubscription.dispose();
				LOG.info("Scanner canceled");
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	@Override
	public boolean isRunning() {
		return watchSubscription != null;
	}

	private boolean isCodeGeneratorEnabled() throws NuclosFatalException {
		return Optional.ofNullable(parameterProvider)
			.map((paramProv) ->
				paramProv.isEnabled(ParameterProvider.CODEGENERATOR_ENABLED, true) &&
				paramProv.isEnabled(ParameterProvider.CODEGENERATOR_SOURCE_SCANNER_ENABLED,
						  isValueEnabled(System.getenv(ParameterProvider.CODEGENERATOR_SOURCE_SCANNER_ENABLED), true)))
			.orElseThrow(() -> new NuclosFatalException("ParameterProvider was not initialized correctly"));
	}

	// Watch for modifications on disk
	private WatchService createWatchService() {
		if (!NuclosCodegeneratorConstants.SOURCE_OUTPUT_PATH.exists()) {
			LOG.info("No scanner task, source dir does not exist");
			return null;
		}

		WatchService watchService = null;
		try {
			watchService = FileSystems.getDefault().newWatchService();
			registerPathAndAllSubdirectories(watchService, NuclosCodegeneratorConstants.SOURCE_OUTPUT_PATH.toPath());
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			watchService = null;
		}
		return watchService;
	}

	private synchronized void startWatcher() {
		final WatchService watchService = createWatchService();
		if (watchService == null) {
			return;
		}

		// Create an observable that monitors file changes
		Observable<Pair<File, WatchEvent.Kind<?>>> watchStream = Observable
				.create((ObservableEmitter<Pair<File, WatchEvent.Kind<?>>> emitter) -> {
			try {
				while (!Thread.currentThread().isInterrupted()) {
					WatchKey key = watchService.take();
					boolean bIgnoreThisWatchEvent = !SpringApplicationContextHolder.isSpringReady(true);
					if (bIgnoreThisWatchEvent) {
						key.reset();
						return;
					}

					try {
						// wait a few ms before polling events and distinct should works as expected
						Thread.sleep(100);
					} catch (Exception e) {
						// ignore...
					}

					final Path watchPath = (Path) key.watchable();
					List<WatchEvent<?>> pollEvents = distinctPollEvents(key.pollEvents(), watchPath);
					for (WatchEvent<?> event : pollEvents) {

						if (event.context() instanceof Path) {
							final File f = getFile(event, watchPath);

							if (f.isDirectory()) {
								if (!event.kind().equals(ENTRY_DELETE)) {
									registerPathAndAllSubdirectories(watchService, f.toPath());
								}
								continue;
							}

							if (!f.getName().endsWith(".java")) {
								// Ignore JetBrains tmp files for example
								continue;
							}
							if (f.getCanonicalPath().startsWith(NuclosCodegeneratorConstants.SOURCE_OUTPUT_PATH.getCanonicalPath()
									+ File.separator + NUCLOS_WEBSERVICE_SRC_PATH)) {
								// ignore webservice sources completely
								continue;
							}
							emitter.onNext(new Pair<>(f, event.kind()));
						}
					}

					// watch again...
					key.reset();
				}
			} catch (ClosedWatchServiceException e) {
				// Task canceled
				LOG.debug("Watch service closed: {} ", e.getMessage());
				emitter.onComplete();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				LOG.error(e.getMessage(), e);
				emitter.onError(e);
			}
		})
		.subscribeOn(Schedulers.io()) // Executes monitoring on a separate IO thread
		.observeOn(Schedulers.io())  // Also processed in the IO thread
		.doOnDispose(() -> {
			watchService.close(); // Cleanup: Stop the WatchService when the observable is stopped
			LOG.debug("WatchService closed");
		});

		watchSubscription = watchStream
			.doOnNext(event -> LOG.info("Event received: {} {}", event.getY(),
				NuclosCodegeneratorConstants.SOURCE_OUTPUT_PATH.toPath().relativize(event.getX().toPath())))
			.publish(sharedStream -> sharedStream
				.switchMap(event -> sharedStream
					.startWithItem(event) // Start with the current event
					.buffer(Observable.timer( // 3s inactivity -> processing
							parameterProvider.getIntValue(ParameterProvider.CODEGENERATOR_SOURCE_SCANNER_DELAY_TIME,
							NumberUtils.toInt(System.getenv(ParameterProvider.CODEGENERATOR_SOURCE_SCANNER_DELAY_TIME), 3)), TimeUnit.SECONDS))
				)
			)
			.filter(events -> !events.isEmpty()) // Ignore empty buffers
			.subscribe(events -> runOnce()); // We collect the events, but ignore them and start a central sync
					// that is also used in other situations. In the past, single code changes were too error-prone
	}

	/**
	 * Some IDE's (IDEA) generate too many events (delete -> create -> edit), we are only interested in the last event.
	 * @param pollEvents
	 * @return
	 */
	private static List<WatchEvent<?>> distinctPollEvents(final List<WatchEvent<?>> pollEvents, final Path watchPath) throws IOException {
		final List<WatchEvent<?>> result = new ArrayList<>();
		final Set<String> alreadyProcessedFiles = new HashSet<>();
		for (WatchEvent<?> event : pollEvents) {
			final String canonicalPath = getCanonicalPath(event, watchPath);
			if (alreadyProcessedFiles.contains(canonicalPath)) {
				// remove previous event for the same file in the result
				Iterator<WatchEvent<?>> itResult = result.iterator();
				while (itResult.hasNext()) {
					WatchEvent<?> resultEvent = itResult.next();
					final String resultCanonicalPath = getCanonicalPath(resultEvent, watchPath);
					if (RigidUtils.equal(canonicalPath, resultCanonicalPath)) {
						itResult.remove();
					}
				}
			} else {
				alreadyProcessedFiles.add(canonicalPath);
			}
			result.add(event);
		}
		return result;
	}

	private static File getFile(WatchEvent<?> pollEvent, Path watchPath) {
		final Path contextPath = (Path) pollEvent.context();
		final Path fullPath = watchPath.resolve(contextPath);
		return fullPath.toFile();
	}

	private static String getCanonicalPath(WatchEvent<?> pollEvent, Path watchPath) throws IOException {
		return getFile(pollEvent, watchPath).getCanonicalPath();
	}

	private void registerPathAndAllSubdirectories(final WatchService watchService, final Path start) throws IOException {
		if (watchService != null) {
			// register directory and sub-directories
			Files.walkFileTree(start, new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
						throws IOException {
					dir.register(watchService, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
					return FileVisitResult.CONTINUE;
				}

			});
		}
	}

	@Override
	public synchronized void runOnce() {
		final Date startRun = new Date();
		int result = 0;
		nuclosLocalServerSession.loginAsUser(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER));
		try {
			final List<MasterDataVO<UID>> activeRules = codeFacade.getAll().stream()
				.filter(rule -> Boolean.TRUE.equals(rule.getEntityObject().getFieldValue(E.SERVERCODE.active)))
				.collect(Collectors.toList());
			final File srcDir = NuclosCodegeneratorConstants.SOURCE_OUTPUT_PATH;

			// Find modified files on disk
			final List<File> javaSrc = new ArrayList<>();
			scanDir(javaSrc, srcDir, ".java");

			if (javaSrc.isEmpty()) {
				return;
			}

			final Collection<MasterDataVO<UID>> newAndUpdatedVOs = new ArrayList<>();
			final Set<UID> processedIds = new HashSet<>();
			boolean saveSrc = false;
			for (File f : javaSrc) {
				GeneratedFile gf = null;
				try {
					if (f.getCanonicalPath().startsWith(srcDir.getCanonicalPath() + File.separator + NUCLOS_WEBSERVICE_SRC_PATH)) {
						// ignore webservice sources completely
						continue;
					}
					// Parse files on disk (to get type and id)
					gf = parseFile(f, false);
					if (gf.getPrimaryKey() != null) {
						String hashValue = sourceCache.getHashValue(gf.getName());
						if (hashValue == null) {
							hashValue = codeFacade.updateHashValueInCache(gf.getPrimaryKey());
						}
						if (gf.getEntity().checkEntityUID(E.SERVERCODE.getUID())) {
							final MasterDataVO<UID> updated = getUpdatedVoAsMd(activeRules, gf);
							if (updated != null) {
								processedIds.add(updated.getPrimaryKey());
							}
							if (!hashValue.equals(gf.getHashValue())) {
								// Change data in DB based on file changes on disk
								if (updated != null) {
									if ("org.nuclos.server.customcode.valueobject.CodeVO".equals(gf.getType())) {
										newAndUpdatedVOs.add(updated);
									}
								} else {
									LOG.warn("Can't find rule for {}", gf.getFile().getCanonicalPath());
								}
							}
						}
					} else {
						newAndUpdatedVOs.add(createCodeVoAsMd(E.SERVERCODE, f, gf.getContent()));
						saveSrc = true;
					}
				} catch (IOException | CommonFinderException | CommonPermissionException e) {
					LOG.warn("Can't parse file {}: {}", f, e.getMessage(), e);
				}
			}

			List<MasterDataVO<UID>> batchList = new ArrayList<>();
			batchList.addAll(activeRules.stream()
				.filter(rule -> !processedIds.contains(rule.getPrimaryKey())) // deleted rule
				.peek(rule -> {
					rule.setFieldValue(E.SERVERCODE.active, Boolean.FALSE);
					rule.getEntityObject().flagUpdate();
				})
				.collect(Collectors.toList()));
			batchList.addAll(newAndUpdatedVOs);
			codeFacade.syncChanges(batchList, saveSrc);
			result = batchList.size();
		} catch (CommonBusinessException e) {
			LOG.warn("Synchronize code changes resulted in error: {}", e.getMessage(), e);
		} finally {
			nuclosLocalServerSession.logout();
			LOG.info("Source scanner took {} seconds. ({} changes)", ((new Date()).getTime() - startRun.getTime()) / 1000, result);
		}
	}

	private static MasterDataVO<UID> getUpdatedVoAsMd(List<MasterDataVO<UID>> activeRules, GeneratedFile gf) {
		final MasterDataVO<UID> vo = activeRules.stream()
			.filter(codeVO -> Objects.equals(codeVO.getPrimaryKey(), gf.getPrimaryKey()))
			.findAny().orElse(null);
		if (vo == null) {
			return null;
		}
		if (!gf.getName().equals(vo.getFieldValue(E.SERVERCODE.name))) {
			throw new IllegalStateException();
		}
		vo.setFieldValue(E.SERVERCODE.source, new String(gf.getContent()));
		return vo;
	}

	private MasterDataVO<UID> createCodeVoAsMd(EntityMeta sourceEntityMeta, File sourceFile, char[] content) throws IOException {
		if (!E.SERVERCODE.equals(sourceEntityMeta)) {
			throw new IllegalStateException();
		}

		// build code name
		final File srcDir = NuclosCodegeneratorConstants.SOURCE_OUTPUT_PATH;
		final String codeName = sourceFile.getCanonicalPath().substring(srcDir.getCanonicalPath().length()+1).replace(File.separatorChar, '.').replace(".java", "");

		final MasterDataVO<UID> vo = new MasterDataVO<UID>(new EntityObjectVO<UID>(E.SERVERCODE.getUID()));
		vo.setFieldValue(E.SERVERCODE.source, new String(content));
		vo.setFieldValue(E.SERVERCODE.name, codeName);
		vo.setFieldValue(E.SERVERCODE.description, "Created from an external source");
		vo.setFieldValue(E.SERVERCODE.debug, false);
		vo.setFieldValue(E.SERVERCODE.active, true);

		// search for nuclet
		UID nucletUID = null;
		String foundNucletPackage = null;
		for (EntityObjectVO<UID> nucletVO : metaProv.getNuclets()) {
			String sNucletPackage = nucletVO.getFieldValue(E.NUCLET.packagefield);
			String sNucletPackageAsPath = nucletVO.getFieldValue(E.NUCLET.packagefield).replace('.', File.separatorChar);
			if (sourceFile.getName().startsWith(sNucletPackage) ||
					sourceFile.getCanonicalPath().endsWith(sNucletPackageAsPath + File.separatorChar + sourceFile.getName())) {
				if (nucletUID == null) {
					nucletUID = nucletVO.getPrimaryKey();
					foundNucletPackage = sNucletPackage;
				} else {
					if (foundNucletPackage.length() < sNucletPackage.length()) {
						nucletUID = nucletVO.getPrimaryKey();
						foundNucletPackage = sNucletPackage;
					}
				}
			}
		}
		vo.setFieldUid(E.SERVERCODE.nuclet, nucletUID);

		return vo;
	}

	private void scanDir(List<File> result, File dir, String ext) {
		final File[] files = dir.listFiles();
		for (File f : files) {
			if (f.isDirectory()) {
				scanDir(result, f, ext);
			} else if (f.isFile() && f.getName().endsWith(ext)) {
				result.add(f);
			}
		}
	}

	GeneratedFile parseFile(File file, boolean bThrowIllegalStateIfNoComment) throws IOException {
		final GeneratedFile result = new GeneratedFile(NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT));
		result.setFile(file);
		final MessageDigestInputStream in = new MessageDigestInputStream(new FileInputStream(file), SourceCache.DIGEST);

		try (
				final BufferedReader reader = new BufferedReader(
						new InputStreamReader(in, NuclosCodegeneratorConstants.JAVA_SRC_ENCODING)
				);
				final CharArrayWriter out = new CharArrayWriter()
		) {
			Map<String, String> properties = null;
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.equals(COMMENT_START)) {
					properties = readPropertiesComment(reader);
					properties.forEach((field, value) -> {
						if ("name".equals(field)) {
							result.setName(value);
						} else if ("classname".equals(field)) {
							result.setTargetClassName(value);
						} else if ("type".equals(field)) {
							result.setType(value);
						} else if ("entity".equals(field)) {
							result.setEntity(E.getByName(value));
						} else if ("class".equals(field)) {
							result.setGeneratorClass(value);
						} else if ("uid".equals(field)) {
							result.setPrimaryKey(new UID(value));
						} else {
							LOG.info("Unknown field '{}' with value '{}' in {}", field, value, file);
						}
					});
				} else {
					out.append(line);
					out.append("\n");
				}
			}
			if ((properties == null || properties.isEmpty()) && bThrowIllegalStateIfNoComment) {
				throw new IllegalStateException("Parse code: Can't find CodeGenerator properties in " + file);
			}

			if ("org.nuclos.server.ruleengine.valueobject.RuleVO".equals(result.getType())) {
				copyCode(reader, out);
			} else {
				copy(reader, out);
			}

			if (out.size() <= 0) {
				throw new IllegalStateException();
			}

			// Trim additional empty lines, leave only 1 line break at the end.
			final String trimmedContent = StringUtils.trimToEmpty(out.toString()) + "\n";

			result.setContent(trimmedContent.toCharArray());
		}
		result.setHashValue(in.digestAsBase64());
		return result;
	}

	private Map<String, String> readPropertiesComment(BufferedReader reader) throws IOException {
		Map<String, String> result = new TreeMap<>();

		String line = reader.readLine();
		while (StringUtils.startsWith(line, "//") && !StringUtils.startsWith(line, COMMENT_END)) {
			final Matcher m = PROP_PAT.matcher(line);
			if (m.matches()) {
				String field = m.group(1);
				String value = m.group(2);
				result.put(field, value);
			}
			line = reader.readLine();
		}

		return result;
	}

	private void copy(Reader r, Writer w) throws IOException {
		final char[] buffer = new char[4092];
		int size;
		while ((size = r.read(buffer)) >= 0) {
			w.write(buffer, 0, size);
		}
	}

	private void copyCode(BufferedReader r, Writer w) throws IOException {
		String line;
		boolean begin = false;
		// only copy the 'rule' part of the source
		while ((line = r.readLine()) != null) {
			if (line.trim().equals("// BEGIN RULE")) {
				begin = true;
				break;
			}
		}
		boolean end = false;
		while ((line = r.readLine()) != null) {
			if (line.trim().equals("// END RULE")) {
				end = true;
				break;
			}
			w.write(line, 0, line.length());
			w.write("\n");
		}
		if (!(begin && end)) {
			throw new IllegalStateException("Parse code: Can't find begin and end");
		}
	}

}
