package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a webservice must cause a recompile.
 */
@Component
public class ApisRecompileChecker extends CompositeRecompileChecker {

    public ApisRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.RESTAPIS.name),
              new FieldRecompileChecker<>(E.RESTAPIS.invokerpackage),
              new FieldRecompileChecker<>(E.RESTAPIS.apipackage),
              new FieldRecompileChecker<>(E.RESTAPIS.modelPackage),
              new FieldRecompileChecker<>(E.RESTAPIS.httplib),
              new FieldRecompileChecker<>(E.RESTAPIS.datelib),
              new FieldRecompileChecker<>(E.RESTAPIS.bigdecimalasstring),
              new FieldRecompileChecker<>(E.RESTAPIS.serializablemodel),
              new UIDFieldRecompileChecker(E.RESTAPIS.apifile),
              new UIDFieldRecompileChecker(E.RESTAPIS.nuclet));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new or deleted api services force a recompile
        return true;
    }
}
