package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a (form or report) datasource must cause a recompile.
 */
@Component
public class DataSourceRecompileChecker extends CompositeRecompileChecker {

    public DataSourceRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.DATASOURCE.name),
              new UIDFieldRecompileChecker(E.DATASOURCE.nuclet),
              new FieldRecompileChecker<>(E.DATASOURCE.description),
			  new FieldRecompileChecker<>(E.DATASOURCE.withRuleClass));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
		if (mdvo.getFieldValue(E.DATASOURCE.withRuleClass)) {
			return true;
		} else {
			return false;
		}
    }

	@Override
	public boolean isRecompileRequiredOnUpdate(final MasterDataVO<?> oldMdvo, final MasterDataVO<?> updatedMdvo) {
		if (!oldMdvo.getFieldValue(E.DATASOURCE.withRuleClass) && !updatedMdvo.getFieldValue(E.DATASOURCE.withRuleClass)) {
			return false;
		}
		return super.isRecompileRequiredOnUpdate(oldMdvo, updatedMdvo);
	}
}
