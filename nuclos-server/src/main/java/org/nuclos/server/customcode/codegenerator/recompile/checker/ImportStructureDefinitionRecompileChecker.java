package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a import must cause a recompile.
 */
@Component
public class ImportStructureDefinitionRecompileChecker extends CompositeRecompileChecker {

    public ImportStructureDefinitionRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.IMPORT.name),
              new UIDFieldRecompileChecker(E.IMPORT.nuclet));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new or deleted imports force a recompile
        return true;
    }

}
