package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * No recompile required.
 */
@Component
public class NoRecompileRequiredChecker implements IRecompileChecker {

    @Override
    public void loadRequiredDependentsOf(MasterDataVO<?> mdvoOld) {
        // no dependents required for comparison
    }

    /**
     * @return Always returns {@code false}.
     */
    @Override
    public boolean isRecompileRequiredOnUpdate(
        MasterDataVO<?> oldMdvo,
        MasterDataVO<?> updatedMdvo) {
        return false;
    }

    /**
     * @return Always returns {@code false}.
     */
    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        return false;
    }

}
