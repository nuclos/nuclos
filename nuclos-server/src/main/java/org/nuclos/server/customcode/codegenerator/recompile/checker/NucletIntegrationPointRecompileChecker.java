package org.nuclos.server.customcode.codegenerator.recompile.checker;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Checks if the integration point require a recompile.
 */
@Component
public class NucletIntegrationPointRecompileChecker extends CompositeRecompileChecker {

	@Autowired
	private MasterDataFacadeHelper masterDataFacadeHelper;

    public NucletIntegrationPointRecompileChecker() {
        super(new PKRecompileChecker(),
              new UIDFieldRecompileChecker(E.NUCLET_INTEGRATION_POINT.nuclet),
              new FieldRecompileChecker(E.NUCLET_INTEGRATION_POINT.name),
			  new FieldRecompileChecker(E.NUCLET_INTEGRATION_POINT.readonly),
			  new FieldRecompileChecker(E.NUCLET_INTEGRATION_POINT.stateful));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new or deleted force a recompile
        return true;
    }

	@Override
	public boolean isRecompileRequiredOnUpdate(
			MasterDataVO<?> oldPoint,
			MasterDataVO<?> updatedPoint) {

		if (super.isRecompileRequiredOnUpdate(oldPoint, updatedPoint)) {
			return true;
		}

		Map<UID, EntityObjectVO<UID>> oldFieldsByUID = new HashMap<>();
		Map<UID, EntityObjectVO<UID>> newFieldsByUID = new HashMap<>();

		Collection<EntityObjectVO<UID>> newFields =
				updatedPoint.getDependents().getDataPk(E.NUCLET_INTEGRATION_FIELD.integrationPoint);
		Collection<EntityObjectVO<UID>> oldFields =
				oldPoint.getDependents().getDataPk(E.NUCLET_INTEGRATION_FIELD.integrationPoint);
		for (EntityObjectVO<UID> oldField : oldFields) {
			oldFieldsByUID.put(oldField.getPrimaryKey(), oldField);
		}
		for (EntityObjectVO<UID> newField : newFields) {
			newFieldsByUID.put(newField.getPrimaryKey(), newField);
		}

		if (oldFieldsByUID.size() != newFields.size()) {
			return true;
		} else {
			for (Map.Entry<UID, EntityObjectVO<UID>> entry : newFieldsByUID.entrySet()) {
				UID newKey = entry.getKey();
				EntityObjectVO<UID> newField = entry.getValue();
				EntityObjectVO<UID> oldField = oldFieldsByUID.get(newKey);
				if (oldField == null) {
					return true;
				}
				final String oldName = oldField.getFieldValue(E.NUCLET_INTEGRATION_FIELD.name);
				final String newName = newField.getFieldValue(E.NUCLET_INTEGRATION_FIELD.name);
				final String newType = newField.getFieldValue(E.NUCLET_INTEGRATION_FIELD.datatype);
				final String oldType = oldField.getFieldValue(E.NUCLET_INTEGRATION_FIELD.datatype);
				final UID oldTarget = oldField.getFieldUid(E.NUCLET_INTEGRATION_FIELD.targetField);
				final UID newTarget = newField.getFieldUid(E.NUCLET_INTEGRATION_FIELD.targetField);
				final Boolean newReadonly = newField.getFieldValue(E.NUCLET_INTEGRATION_FIELD.readonly);
				final Boolean oldReadonly = oldField.getFieldValue(E.NUCLET_INTEGRATION_FIELD.readonly);
				if (ObjectUtils.notEqual(oldName, newName) ||
					ObjectUtils.notEqual(oldType, newType) ||
					ObjectUtils.notEqual(oldTarget, newTarget) ||
					ObjectUtils.notEqual(oldReadonly, newReadonly)) {
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public void loadRequiredDependentsOf(MasterDataVO<?> mdvo) {
		super.loadRequiredDependentsOf(mdvo);

		// load nuclet params:
		String username =
				SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		Collection<EntityObjectVO<UID>> nucletParamEOs =
				masterDataFacadeHelper.getDependantMasterData(E.NUCLET_INTEGRATION_FIELD.integrationPoint.getUID(),
						username, null, (UID)mdvo.getPrimaryKey());
		mdvo.getDependents().addAllData(E.NUCLET_INTEGRATION_FIELD.integrationPoint, nucletParamEOs);
	}

}
