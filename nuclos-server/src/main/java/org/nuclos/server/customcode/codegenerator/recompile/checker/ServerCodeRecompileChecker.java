package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a rule must cause a recompile.
 */
@Component
public class ServerCodeRecompileChecker extends CompositeRecompileChecker {

    public ServerCodeRecompileChecker() {
        super(
            new FieldRecompileChecker<>(E.SERVERCODE.name),
            new FieldRecompileChecker<>(E.SERVERCODE.active),
			// Changes are already tracked by the CodeFacade and are more efficient,
			// because only the rules are recompiled
            //new FieldRecompileChecker<>(E.SERVERCODE.source),
            new FieldRecompileChecker<>(E.SERVERCODE.debug),
            new FieldRecompileChecker<>(E.SERVERCODE.extensionRule)
        );
    }

	@Override
	public boolean isRecompileRequiredOnUpdate(final MasterDataVO<?> oldMdvo, final MasterDataVO<?> updatedMdvo) {
		if (!oldMdvo.getFieldValue(E.SERVERCODE.active) && !updatedMdvo.getFieldValue(E.SERVERCODE.active)) {
			return false;
		}
		return super.isRecompileRequiredOnUpdate(oldMdvo, updatedMdvo);
	}

	@Override
    public boolean isRecompileRequiredOnInsertOrDelete(MasterDataVO<?> rule) {
    	if (Boolean.TRUE.equals(rule.getFieldValue(E.SERVERCODE.active))) {
			// new/removed active rules must trigger recompile
    		return true;
		} else {
    		return false;
		}
    }

}
