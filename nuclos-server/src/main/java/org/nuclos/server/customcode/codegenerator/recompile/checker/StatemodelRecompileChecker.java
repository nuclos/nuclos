package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a state model must cause a recompile. <p/> Only checks the state
 * model, not the dependents (e.g. E.STATE).<br/> This is because the {@link
 * org.nuclos.server.common.ejb3.SecurityFacadeBean} does not fill the statemodel MDVO with the
 * dependents when it passes it to the {@link org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean}.
 */
@Component
public class StatemodelRecompileChecker extends CompositeRecompileChecker {


    public StatemodelRecompileChecker() {
        super(
            //statemodel java file does not contain PK
            // but its states contain the PK of the statemodel
            new PKRecompileChecker(),
            new FieldRecompileChecker<>(E.STATEMODEL.name),
            new UIDFieldRecompileChecker(E.STATEMODEL.nuclet)
        );
    }

    @Override
    public void loadRequiredDependentsOf(MasterDataVO<?> mdvo) {
        // see javadoc for details about dependents
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // when a state is added/removed we must recompile
        return true;
    }

}
