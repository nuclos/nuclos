package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a webservice must cause a recompile.
 */
@Component
public class WebServiceRecompileChecker extends CompositeRecompileChecker {

    public WebServiceRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.WEBSERVICE.name),
              new UIDFieldRecompileChecker(E.WEBSERVICE.nuclet));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new or deleted web services force a recompile
        return true;
    }
}
