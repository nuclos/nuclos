package org.nuclos.server.customcode.codegenerator.recompile.checker.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.nuclos.server.customcode.codegenerator.recompile.checker.IRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * A composite recompile checker.
 */
public class CompositeRecompileChecker implements IRecompileChecker {

    private final List<IRecompileChecker> recompileCheckers;

    public CompositeRecompileChecker(List<IRecompileChecker> recompileCheckers) {
        this.recompileCheckers = new ArrayList<>(recompileCheckers);
    }

    public CompositeRecompileChecker(IRecompileChecker... recompileCheckers) {
        this.recompileCheckers = new ArrayList<>();
        if (recompileCheckers != null) {
            Collections.addAll(this.recompileCheckers, recompileCheckers);
        }
    }

    protected void addRecompileChecker(IRecompileChecker recompileChecker) {
    	this.recompileCheckers.add(recompileChecker);
	}

    @Override
    public void loadRequiredDependentsOf(MasterDataVO<?> mdvo) {
        for (IRecompileChecker recompileChecker : recompileCheckers) {
            recompileChecker.loadRequiredDependentsOf(mdvo);
        }
    }

    @Override
    public boolean isRecompileRequiredOnUpdate(MasterDataVO<?> oldMdvo,
                                               MasterDataVO<?> updatedMdvo) {
        for (IRecompileChecker recompileChecker : recompileCheckers) {
            if (recompileChecker.isRecompileRequiredOnUpdate(oldMdvo, updatedMdvo)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        for (IRecompileChecker recompileChecker : recompileCheckers) {
            if (recompileChecker.isRecompileRequiredOnInsertOrDelete(mdvo)) {
                return true;
            }
        }
        return false;
    }
}
