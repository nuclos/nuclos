//Copyright (C) 2018  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator.recompile.checker.composite;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.server.customcode.codegenerator.recompile.checker.IRecompileChecker;
import org.nuclos.server.datasource.DatasourceMetaParser;
import org.nuclos.server.datasource.DatasourceMetaVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Checks if the datasource meta value was modified (by using {@code equals}).
 */
public class DatasourceMetaRecompileChecker implements IRecompileChecker {

	private final DatasourceMetaParser dsMetaParser;

	private final FieldMeta.Valueable<String> fieldMeta;

	public DatasourceMetaRecompileChecker(final DatasourceMetaParser dsMetaParser, final FieldMeta.Valueable<String> fieldMeta) {
		this.dsMetaParser = dsMetaParser;
		this.fieldMeta = fieldMeta;
	}

	@Override
	public void loadRequiredDependentsOf(final MasterDataVO<?> mdvo) {
	}

	@Override
	public boolean isRecompileRequiredOnUpdate(final MasterDataVO<?> oldMdvo, final MasterDataVO<?> updatedMdvo) {
		final String sMetaOld = oldMdvo.getFieldValue(fieldMeta);
		final String sMetaNew = updatedMdvo.getFieldValue(fieldMeta);
		final DatasourceMetaVO metaOld = getMetaVO(sMetaOld);
		final DatasourceMetaVO metaNew = getMetaVO(sMetaNew);
		return !RigidUtils.equal(metaOld, metaNew);
	}

	private DatasourceMetaVO getMetaVO(String sMetaXML) {
		return dsMetaParser.parse(sMetaXML, null);
	}

	@Override
	public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
		return true;
	}
}
