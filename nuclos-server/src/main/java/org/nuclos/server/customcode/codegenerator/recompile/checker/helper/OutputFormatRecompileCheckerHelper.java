package org.nuclos.server.customcode.codegenerator.recompile.checker.helper;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Checks if a modification to an output format must cause a recompile. <p/>
 */
public class OutputFormatRecompileCheckerHelper {

    private final EntityMeta<UID> outputEntityMeta;
    private final FieldMeta.Valueable<java.lang.String> descriptionFieldMeta;
    private final FieldMeta<?> outputReferencingField;

    public OutputFormatRecompileCheckerHelper(
        EntityMeta<UID> outputEntityMeta,
        FieldMeta.Valueable<String> descriptionFieldMeta,
        FieldMeta<?> outputReferencingField) {
        this.outputEntityMeta = outputEntityMeta;
        this.descriptionFieldMeta = descriptionFieldMeta;
        this.outputReferencingField = outputReferencingField;
    }


    public boolean isRecompileRequired(
        MasterDataVO<?> oldReportOrForm,
        MasterDataVO<?> updatedReportOrForm) {

        // OutputFormat*
        // 	- name (Bezeichnung) (description)
        // 	- UID(PK)

        // iterate over output formats (dependants...)

        Map<UID, String> oldOutputNamesByUID = new HashMap<>();
        Map<UID, String> newOutputNamesByUID = new HashMap<>();
        // E.REPORTOUTPUT
        Collection<EntityObjectVO<?>> newReportOutputs =
            updatedReportOrForm.getDependents().getData(outputReferencingField);
        Collection<EntityObjectVO<?>> oldReportOutputs =
            oldReportOrForm.getDependents().getData(outputReferencingField);
        for (EntityObjectVO<?> oldReportOutput : oldReportOutputs) {
            UID outputReportPK = (UID) oldReportOutput.getPrimaryKey();
            String outputName = oldReportOutput.getFieldValue(descriptionFieldMeta);
            oldOutputNamesByUID.put(outputReportPK, outputName);
        }
        for (EntityObjectVO<?> newReportOutput : newReportOutputs) {
            UID outputReportPK = (UID) newReportOutput.getPrimaryKey();
            String outputName = newReportOutput.getFieldValue(descriptionFieldMeta);
            newOutputNamesByUID.put(outputReportPK, outputName);
        }

        if (oldOutputNamesByUID.size() != newOutputNamesByUID.size()) {
            return true;
        } else {
            for (Map.Entry<UID, String> entry : newOutputNamesByUID.entrySet()) {
                UID newKey = entry.getKey();
                String newOutputName = entry.getValue();
                String oldOutputName = oldOutputNamesByUID.get(newKey);
                if (oldOutputName == null) {
                    return true;
                }
                if (!newOutputName.equals(oldOutputName)) {
                    return true;
                }
            }
        }

        return false;
    }
}
