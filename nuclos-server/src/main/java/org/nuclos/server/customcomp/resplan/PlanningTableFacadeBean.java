package org.nuclos.server.customcomp.resplan;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.ReferencingCollectableSearchCondition;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common.customcomp.resplan.PlanElementLocaleVO;
import org.nuclos.common.customcomp.resplan.ResPlanConfigVO;
import org.nuclos.common.customcomp.resplan.ResourceLocaleVO;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.preferences.Preference;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.interval.Interval;
import org.nuclos.schema.rest.BackgroundEvent;
import org.nuclos.schema.rest.Booking;
import org.nuclos.schema.rest.BookingDef;
import org.nuclos.schema.rest.BusinessHour;
import org.nuclos.schema.rest.Milestone;
import org.nuclos.schema.rest.MilestoneDef;
import org.nuclos.schema.rest.PlanElementDef;
import org.nuclos.schema.rest.PlanElementDefList;
import org.nuclos.schema.rest.PlanningTable;
import org.nuclos.schema.rest.Relation;
import org.nuclos.schema.rest.RelationDef;
import org.nuclos.schema.rest.SearchfilterInfo;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.common.ejb3.PreferencesFacadeBean;
import org.nuclos.server.common.ejb3.SecurityFacadeLocal;
import org.nuclos.server.customcomp.ejb3.CustomComponentFacadeBean;
import org.nuclos.server.customcomp.valueobject.CustomComponentVO;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.i18n.DataLanguageCache;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean;
import org.nuclos.server.masterdata.ejb3.MetaDataFacadeBean;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.searchfilter.ejb3.SearchFilterFacadeBean;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional(noRollbackFor = { Exception.class })
@Component("planningTableService")
public class PlanningTableFacadeBean extends NuclosFacadeBean implements PlanningTableFacadeLocal {

	private final static Logger LOG = LoggerFactory.getLogger(PlanningTableFacadeBean.class);

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	@Qualifier("masterDataService")
	private MasterDataFacadeBean masterDataFacade;

	@Autowired
	private MetaDataFacadeBean metaDataFacade;

	@Autowired
	private CustomComponentFacadeBean customComponentFacade;

	@Autowired
	private SecurityFacadeLocal securityFacade;

	@Autowired
	private LocaleFacadeLocal localeFacade;

	@Autowired
	private SearchFilterFacadeBean searchFilterFacade;

	@Autowired
	private PreferencesFacadeBean preferencesFacade;

	public PlanningTableFacadeBean() { }

	public void checkReadAllowed(ResPlanConfigVO resPlanConfigVO) throws CommonPermissionException {
		List<PlanElement> planElements = resPlanConfigVO.getPlanElements();
		List<UID> allAccessedElements = new ArrayList<>();
		allAccessedElements.add(resPlanConfigVO.getResourceEntity());
		allAccessedElements.addAll(planElements.stream().map(p -> p.getEntity()).collect(Collectors.toList()));
		if (resPlanConfigVO.getHolidaysEntity() != null) {
			allAccessedElements.add(resPlanConfigVO.getHolidaysEntity());
		}
		this.checkReadAllowed(allAccessedElements.toArray(new UID[0]));
	}

	@Override
	public ResPlanConfigVO getResPlanConfig(final UID uid) throws CommonPermissionException, CommonFinderException {
		CustomComponentVO result = customComponentFacade.getAll().stream()
				.filter(config -> uid.equals(config.getId()))
				.findAny()
				.orElse(null);
		if(result == null) {
			throw new CommonFinderException();
		}
		final Jaxb2Marshaller marshaller = SpringApplicationContextHolder.getBean(Jaxb2Marshaller.class);
		ResPlanConfigVO resPlanConfigVO = ResPlanConfigVO.fromBytes(result.getData(), marshaller);

		this.checkReadAllowed(resPlanConfigVO);

		return resPlanConfigVO;
	}

	public void updateEvent(UID planningTableId, UID metaId, String entityId, String resource, Date dateFrom,
							Date dateUntil, String timeFrom, String timeUntil) throws CommonFinderException, CommonPermissionException, CommonRemoveException, CommonCreateException, CommonValidationException, CommonStaleVersionException, NuclosBusinessRuleException {
		ResPlanConfigVO resPlanConfigVO = getResPlanConfig(planningTableId);
		List<PlanElement> planElements = resPlanConfigVO.getPlanElements();
		PlanElement targetElement = planElements.stream()
				.filter(x -> x.getEntity().equals(metaId))
				.findAny()
				.orElse(null);
		if (targetElement != null) {
			switch (targetElement.getType()) {
				case PlanElement.ENTRY:
					updateBooking(targetElement, entityId, resource, dateFrom, dateUntil, timeFrom, timeUntil);
					break;
				case PlanElement.MILESTONE:
					updateMilestone(targetElement, entityId, resource, dateFrom);
					break;
				default:
					LOG.error("The specified planelement can not be updated by this method.");
					break;
			}
		}
	}

	public PlanElementDefList getAllPlanElementDefs(UID planTableDef) throws CommonBusinessException {
		ResPlanConfigVO configVO = getResPlanConfig(planTableDef);
		ArrayList<PlanElementDef> planElementDefList = new ArrayList<>();
		if (configVO != null) {
			List<PlanElement> planElements = configVO.getPlanElements();
			for (PlanElement defHolder : planElements) {
				PlanElementDef planElementDef = null;
				switch (defHolder.getType()) {
					case PlanElement.ENTRY:
						planElementDef = PlanElementDef.builder().withElementType("booking").withBookingDef(getBookingDef(defHolder)).build();
						break;
					case PlanElement.MILESTONE:
						planElementDef = PlanElementDef.builder().withElementType("milestone").withMilestoneDef(getMilestoneDef(defHolder)).build();
						break;
					case PlanElement.RELATION:
						planElementDef = PlanElementDef.builder().withElementType("relation").withRelationDef(getRelationDef(defHolder)).build();
						break;
				}
				if (planElementDef != null) {
					planElementDefList.add(planElementDef);
				}
			}
		}

		return PlanElementDefList.builder().addPlanElementDefs(planElementDefList).build();
	}

	public PlanElementDef getPlanElementDef(UID planTableDef, UID planElementMeta) throws CommonBusinessException {
		ResPlanConfigVO configVO = getResPlanConfig(planTableDef);
		PlanElementDef planElementDef = null;
		if (configVO != null) {
			List<PlanElement> planElements = configVO.getPlanElements();
			PlanElement defHolder = planElements.stream().filter(e -> e.getEntity().equals(planElementMeta)).findFirst().orElse(null);
			switch (defHolder.getType()) {
				case PlanElement.ENTRY:
					planElementDef = PlanElementDef.builder().withElementType("booking").withBookingDef(getBookingDef(defHolder)).build();
					break;
				case PlanElement.MILESTONE:
					planElementDef = PlanElementDef.builder().withElementType("milestone").withMilestoneDef(getMilestoneDef(defHolder)).build();
					break;
				case PlanElement.RELATION:
					planElementDef = PlanElementDef.builder().withElementType("relation").withRelationDef(getRelationDef(defHolder)).build();
					break;
			}
		}

		return planElementDef;
	}

	private BookingDef getBookingDef(PlanElement e) {
		BookingDef bookingDef = BookingDef.builder()
				.withEntity(e.getEntity().toString())
				.withDateFromField(e.getDateFromField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD))
				.withDateUntilField(e.getDateUntilField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD))
				.withWithTime(e.hasTime())
				.withBusinessHours(getBusinessHours(e))
				.withColor(e.getColor())
				.withResourceRefField(e.getPrimaryField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD)).build();
		if (e.hasTime()) {
			bookingDef.setTimeFromField(e.getTimeFromField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD));
			bookingDef.setTimeUntilField(e.getTimeUntilField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD));
		}
		return bookingDef;
	}

	private MilestoneDef getMilestoneDef(PlanElement e) {
		MilestoneDef milestoneDef = MilestoneDef.builder()
				.withEntity(e.getEntity().toString())
				.withResourceRefField(e.getPrimaryField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD))
				.withDateFromField(e.getDateFromField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD))
				.build();
		if (e.getFromIcon() != null) {
			milestoneDef.setIcon(e.getFromIcon());
		}
		return milestoneDef;
	}

	private RelationDef getRelationDef(PlanElement e) {
		RelationDef relationDef = RelationDef.builder()
				.withEntity(e.getEntity().toString())
				.withFromField(e.getPrimaryField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD))
				.withToField(e.getSecondaryField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD))
				.withFromIcon(e.getFromIcon())
				.withToIcon(e.getToIcon())
				.withFromLineCap("" + e.getFromPresentation())
				.withToLineCap("" + e.getToPresentation()).build();
		return relationDef;
	}

	private void updateBooking(PlanElement booking, String id, String resource, Date dateFrom, Date dateUntil, String timeFrom, String timeUntil)
			throws CommonFinderException, CommonPermissionException, CommonRemoveException, CommonCreateException, CommonValidationException, CommonStaleVersionException, NuclosBusinessRuleException {
		Long parsedId = Long.parseLong(id);
		MasterDataVO dataVO = this.masterDataFacade.get(booking.getEntity(), parsedId);
		if (resource != null) {
			Long parsedResourceId = Long.parseLong(resource);
			dataVO.setFieldId(booking.getPrimaryField(), parsedResourceId);
		}
		if (dateFrom != null) {
			dataVO.setFieldValue(booking.getDateFromField(), dateFrom);
		}
		if (dateUntil != null) {
			dataVO.setFieldValue(booking.getDateUntilField(), dateUntil);
		}
		if (timeFrom != null) {
			dataVO.setFieldValue(booking.getTimeFromField(), timeFrom);
		}
		if (timeUntil != null) {
			dataVO.setFieldValue(booking.getTimeUntilField(), timeUntil);
		}
		this.masterDataFacade.modify(dataVO, null);
	}

	private void updateMilestone(PlanElement milestone, String id, String resource, Date dateFrom)
			throws CommonRemoveException, CommonFinderException, CommonCreateException, CommonValidationException, CommonPermissionException, CommonStaleVersionException, NuclosBusinessRuleException {
		MasterDataVO dataVO = this.masterDataFacade.get(milestone.getEntity(), new UID(id));
		if (resource != null) {
			long parsedResourceId = Long.parseLong(resource);
			dataVO.setFieldId(milestone.getPrimaryField(), parsedResourceId);
		}

		if (dateFrom != null) {
			dataVO.setFieldValue(milestone.getDateFromField(), dateFrom);
		}

		this.masterDataFacade.modify(dataVO, null);
	}

	public PlanningTable getPlanningTable(UID planningTableId, Interval<Date> interval, boolean ownBookings,
										  CollectableSearchCondition resourceSearchFilter, String layoutEoId, String resourceForeignKey) throws CommonBusinessException {
		ResPlanConfigVO resPlanConfigVO = getResPlanConfig(planningTableId);
		CustomComponentVO componentWrapper = customComponentFacade.get(planningTableId);
		ResourceLocaleVO resLocaleVO = getCurrentUserResourceLocaleVO(resPlanConfigVO);
		TranslationVO baseTranslation = getTranslationForCurrentUser(planningTableId);

		String componentLabel = "";
		String componentMenupath = null;
		String legendLabel = null;
		String legendTooltip = null;

		if (baseTranslation != null) {
			if (componentWrapper.getLabelResourceId() != null
					&& baseTranslation.getLabels().get(TranslationVO.LABELS_CUSTOM_COMPONENT[0]) != null) {
				componentLabel = baseTranslation.getLabels().get(TranslationVO.LABELS_CUSTOM_COMPONENT[0]);
			}
			if (componentWrapper.getMenupathResourceId() != null
					&& baseTranslation.getLabels().get(TranslationVO.LABELS_CUSTOM_COMPONENT[1]) != null) {
				componentMenupath = baseTranslation.getLabels().get(TranslationVO.LABELS_CUSTOM_COMPONENT[1]);
			}
		}
		if (resLocaleVO != null) {
			if (resLocaleVO.getLegendLabel() != null) {
				legendLabel = resLocaleVO.getLegendLabel();
			}
			if (resLocaleVO.getLegendTooltip() != null) {
				legendTooltip = resLocaleVO.getLegendTooltip();
			}
		}
		return PlanningTable.builder()
				.withResources(getResources(resPlanConfigVO, resourceSearchFilter, layoutEoId, resourceForeignKey))
				.withBookings(getBookings(resPlanConfigVO, interval, ownBookings, layoutEoId, resourceForeignKey))
				.withMilestones(getMilestones(resPlanConfigVO, interval, ownBookings, layoutEoId, resourceForeignKey))
				.withRelations(getRelations(resPlanConfigVO, interval, ownBookings))
				.withBackgroundEvents(getBackgroundEvents(resPlanConfigVO))
				.withBusinessHours(getBusinessHours(resPlanConfigVO))
				.withSearchFilters(getWebclientFilterInfos(resPlanConfigVO))
				.withDateFrom(DATE_FORMAT.format(interval.getStart()))
				.withDateUntil(DATE_FORMAT.format(interval.getEnd()))
				.withComponentLabel(componentLabel)
				.withComponentMenupath(componentMenupath)
				.withLegendLabel(legendLabel)
				.withLegendTooltip(legendTooltip)
				.withShowBackgroundEvents("" + resPlanConfigVO.getWithHolidays())
				.withUsePeriodicRefresh(resPlanConfigVO.getUsePeriodicRefreshTimer())
				.withPeriodicRefreshInterval(resPlanConfigVO.getRefreshInterval() != null ? resPlanConfigVO.getRefreshInterval().intValue() : -1)
				.withDateChooserPolicy(resPlanConfigVO.getDateChooserPolicy())
				.build();
	}

	public List<BackgroundEvent> getBackgroundEvents(ResPlanConfigVO resPlanConfigVO) {
		if(resPlanConfigVO.getHolidaysEntity() == null) {
			return Collections.EMPTY_LIST;
		}
		EntityMeta<?> meta = metaDataFacade.getEntityMeta(resPlanConfigVO.getHolidaysEntity());
		Collection<? extends MasterDataVO<?>> result = masterDataFacade.getMasterData(meta, null);
		ArrayList<BackgroundEvent> backgroundEvents = new ArrayList<>();
		for(MasterDataVO entry : result) {
			BackgroundEvent backgroundEvent = BackgroundEvent.builder()
					.withEventEntity(meta.getEntityName())
					.withEventName((String) entry.getFieldValue(resPlanConfigVO.getHolidaysName()))
					.withEventDate(entry.getFieldValue(resPlanConfigVO.getHolidaysDate()).toString())
					.withEventTip((String) entry.getFieldValue(resPlanConfigVO.getHolidaysTip()))
					.build();

			backgroundEvents.add(backgroundEvent);
		}
		return backgroundEvents;
	}

	public TranslationVO getTranslationForCurrentUser(UID uid) throws CommonBusinessException {
		String languageCode = Rest.facade().getCurrentLocale().getLanguage();
		List<TranslationVO> translations = customComponentFacade.getTranslations(uid);
		return translations.stream().filter(t -> t.getLanguage().equals(languageCode)).findAny().orElse(null);
	}

	public ResourceLocaleVO getCurrentUserResourceLocaleVO(ResPlanConfigVO resPlanConfigVO) {
		LocaleInfo localeInfo = localeFacade.getAllLocales(false).stream().filter(x -> x.getLanguage().equals(Rest.facade().getCurrentLocale().getLanguage()))
				.findAny().orElse(null);
		List<ResourceLocaleVO> list = resPlanConfigVO.getOrMigrateResourceLocales();

		return list.stream().filter(x -> localeInfo == null || x.getLocaleId().equals(localeInfo.getLocale()))
				.findAny().orElse(null);
	}

	public PlanElementLocaleVO getCurrentUserPlanElementLocaleVO(PlanElement planElement) {
		LocaleInfo localeInfo = localeFacade.getAllLocales(false).stream().filter(x -> x.getLanguage().equals(Rest.facade().getCurrentLocale().getLanguage()))
				.findAny().orElse(null);
		List<PlanElementLocaleVO> list = planElement.getPlanElementLocaleVO();

		return list.stream().filter(x -> localeInfo == null || x.getLocaleId().equals(localeInfo.getLocale()))
				.findAny().orElse(null);
	}

	public Collection<org.nuclos.schema.rest.PlanningResource> getResources(ResPlanConfigVO resPlanConfigVO, CollectableSearchCondition searchFilter, String layoutEoId, String resourceForeignKey) throws CommonBusinessException {
		CompositeCollectableSearchCondition compositeCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND);

		EntityMeta<?> meta = metaDataFacade.getEntityMeta(resPlanConfigVO.getResourceEntity());
		if (searchFilter != null) {
			compositeCondition.addOperand(searchFilter);
		}
		if (layoutEoId != null && resourceForeignKey != null) {
			FieldMeta<?> fm = meta.getFields().stream().filter(
					x -> resourceForeignKey.equals(metaDataFacade.getAllEntityFQNs().get(meta.getUID()) + "_" + x.getFieldName().replace("_", ""))
			).findFirst().orElse(null);
			EntityMeta<?> layoutMeta = metaDataFacade.getEntityMeta(fm.getForeignEntity());
			FieldMeta<?> layoutIdField = layoutMeta.getFields().stream().filter(x -> x.isPrimaryKey()).findFirst().orElse(null);
			long layoutEoIdLong = Long.parseLong(layoutEoId);
			CollectableComparison cmp = SearchConditionUtils.newComparison(layoutIdField, ComparisonOperator.EQUAL, layoutEoIdLong);

			CollectableEntity resourceEntity = new CollectableMasterDataEntity(meta);
			ReferencingCollectableSearchCondition rcsc = new ReferencingCollectableSearchCondition(resourceEntity.getEntityField(fm.getUID()), cmp);

			compositeCondition.addOperand(rcsc);
		}
		List<CollectableSorting> sortingList = new ArrayList<>();
		if (resPlanConfigVO.getResourceSortField() != null) {
			sortingList.add(new CollectableSorting(resPlanConfigVO.getResourceSortField(), true));
		}
		CollectableSearchExpression searchExpression = new CollectableSearchExpression(compositeCondition.getOperandCount()  > 0 ? compositeCondition : null);
		searchExpression.setSortingOrder(sortingList);
		Collection<? extends MasterDataVO<?>> result = masterDataFacade.getMasterDataImpl(meta.getUID(), searchExpression, true);
		ResourceLocaleVO resourceLocaleVO = getCurrentUserResourceLocaleVO(resPlanConfigVO);
		ArrayList<org.nuclos.schema.rest.PlanningResource> resources = new ArrayList<>();

		for(MasterDataVO item : result) {
			String resourceLabel = null;
			String resourceTooltip = null;
			if(resourceLocaleVO != null) {
				resourceLabel = formatTemplateText(resourceLocaleVO.getResourceLabel(), item);
				resourceTooltip = formatTemplateText(resourceLocaleVO.getResourceTooltip(), item);
			}
			org.nuclos.schema.rest.PlanningResource pr = org.nuclos.schema.rest.PlanningResource.builder()
					.withEntityMeta(resPlanConfigVO.getResourceEntity().toString())
					.withResourceId(item.getPrimaryKey().toString())
					.withLabel(resourceLabel)
					.withTooltip(resourceTooltip)
					.build();

			resources.add(pr);
		}

		return resources;
	}

	public static String formatTemplateText(String templateText, MasterDataVO item) {
		if (templateText == null)
			return null;
		final boolean html = templateText.startsWith("<html>");
		ForeignEntityFieldUIDParser uidParser = new ForeignEntityFieldUIDParser(templateText, null, null);
		Iterator<IFieldUIDRef> itParser =  uidParser.iterator();

		final StringBuffer result = new StringBuffer(templateText.length());
		while (itParser.hasNext()) {
			IFieldUIDRef part = itParser.next();
			if (part.isConstant()) {
				result.append(part.getConstant());
			} else {
				try {
					UID fieldUID = part.getUID();
					Object fieldValue = item.getFieldValue(fieldUID);
					if (fieldValue != null) {
						String stringFieldValue = fieldValue.toString();
						result.append(html ? StringUtils.xmlEncode(stringFieldValue) : stringFieldValue);
					}
				} catch (CommonFatalException e) {
					LOG.warn("Planning table configuration probably changed. Please reopen planning table. If" +
							" this warning persists, please update your planning table configuration.");
				}
			}

		}
		return result.toString();
	}

	public Collection<Preference> getWebclientSearchfilterPrefs(ResPlanConfigVO resPlanConfigVO) throws CommonFinderException, CommonPermissionException {
		List<Preference> allPreferences = preferencesFacade.getPreferences(null, NuclosPreferenceType.SEARCHTEMPLATE.getType(), null, null, true, securityFacade.getUserUid(securityFacade.getUserName()), null, null);
		List<Preference> relevantFilters = allPreferences.stream().filter(filter -> filter.getEntity().equals(resPlanConfigVO.getResourceEntity())).collect(Collectors.toList());

		return relevantFilters;
	}

	public Collection<SearchfilterInfo> getRichclientFilterInfos(ResPlanConfigVO resPlanConfigVO) throws CommonFinderException, CommonPermissionException {
		ArrayList<SearchfilterInfo> filterInfos = new ArrayList<>();
		getAllSearchFilters(resPlanConfigVO).stream().forEach(
								x -> filterInfos.add(SearchfilterInfo.builder()
												.withId("" + x.getId())
												.withType("filter-richclient")
												.withFilterName(x.getFilterName())
												.withIcon("" + x.getFastSelectIcon())
										.build())
		);
		return filterInfos;
	}
	public Collection<SearchfilterInfo> getWebclientFilterInfos(ResPlanConfigVO resPlanConfigVO) throws CommonFinderException, CommonPermissionException {
		ArrayList<SearchfilterInfo> filterInfos = new ArrayList<>();
		getWebclientSearchfilterPrefs(resPlanConfigVO).stream().forEach(
				x -> filterInfos.add(SearchfilterInfo.builder()
								.withId("" + x.getUID())
								.withType("filter-webclient")
								.withFilterName(x.getName())
						.build())
		);
		return filterInfos;
	}

	private Collection<SearchFilterVO> getAllSearchFilters(ResPlanConfigVO resPlanConfigVO) throws CommonFinderException, CommonPermissionException {
		ArrayList<SearchFilterVO> filters = searchFilterFacade.getAllSearchFilterByCurrentUser().stream()
				.filter(f -> f.getEntity().equals(resPlanConfigVO.getResourceEntity())).collect(Collectors.toCollection(ArrayList::new));
		return filters;
	}

	public Collection<Booking> getBookings(ResPlanConfigVO resPlanConfigVO, Interval<Date> interval, boolean ownBookings, String layoutEoId, String resourceForeignKey) {
		ArrayList<Booking> result = new ArrayList<>();
		List<PlanElement> planElements = resPlanConfigVO.getPlanElements();
		planElements = planElements.stream().filter(x -> x.getType() == PlanElement.ENTRY).collect(Collectors.toCollection(ArrayList::new));
		for(PlanElement pe : planElements) {
			result.addAll(getBookingsForPlanElement(pe, interval, ownBookings, resPlanConfigVO.getResourceEntity(), layoutEoId, resourceForeignKey));
		}
		return result;
	}

	public Collection<BusinessHour> getBusinessHours(ResPlanConfigVO resPlanConfigVO) {
		ArrayList<BusinessHour> result = new ArrayList<>();
		List<PlanElement> planElements = resPlanConfigVO.getPlanElements();
		planElements = planElements.stream().filter(x -> x.getType() == PlanElement.ENTRY).collect(Collectors.toCollection(ArrayList::new));
		if (planElements.size() > 0) {
			String timePeriodsString = planElements.stream()
					.filter(pe -> !Boolean.FALSE.equals(pe.getUseTimePeriods()))
					.map(pe -> pe.getTimePeriodsString())
					.filter(string -> !StringUtils.isNullOrEmpty(string))
					.findFirst()
					.orElse(null);
			getBusinessHoursFromString(result, timePeriodsString);
		}
		return result;
	}

	public Collection<BusinessHour> getBusinessHours(PlanElement planElement) {
		ArrayList<BusinessHour> result = new ArrayList<>();
		if (planElement != null) {
			String timePeriodsString = !Boolean.FALSE.equals(planElement.getUseTimePeriods()) ? planElement.getTimePeriodsString() : null;
			getBusinessHoursFromString(result, timePeriodsString);
		}
		return result;
	}

	private void getBusinessHoursFromString(Collection<BusinessHour> resultCollection, String timePeriodsString) {
		if (timePeriodsString != null) {
			String[] splitSemicolon = timePeriodsString.split(";");
			for (int i = 0; i < splitSemicolon.length; i++) {
				String[] splitDash = splitSemicolon[i].split("-");
				resultCollection.add(BusinessHour.builder().withFrom(splitDash[0]).withUntil(splitDash[1]).build());
			}
		}
	}

	public ArrayList<Booking> getBookingsForPlanElement(final PlanElement planElement, Interval<Date> interval, boolean ownBookings, UID resourceEntity, String layoutEoId, String resourceForeignKey) {
		EntityMeta<?> meta = metaDataFacade.getEntityMeta(planElement.getEntity());
		CollectableSearchCondition filterByDateAndUser = getInternalEntrySearchCondition(interval, ownBookings, planElement, resourceEntity, layoutEoId, resourceForeignKey);
		Collection<? extends MasterDataVO<?>> result = masterDataFacade.getMasterData(meta, filterByDateAndUser);
		ArrayList<Booking> bookings = new ArrayList<>();
		PlanElementLocaleVO planElementLocale = getCurrentUserPlanElementLocaleVO(planElement);
		for(MasterDataVO item : result) {
			Date dateFrom = (Date) item.getFieldValue(planElement.getDateFromField());
			Date dateUntil = (Date) item.getFieldValue(planElement.getDateUntilField());

			String bookingLabel = formatTemplateText(planElementLocale.getBookingLabel(), item);
			String bookingTooltip = formatTemplateText(planElementLocale.getBookingTooltip(), item);
			String nuclosRowColor = "";
			FieldMeta colorField = meta.getFields().stream().filter(x -> x.isNuclosRowColor()).findFirst().orElse(null);
			if (colorField != null) {
				nuclosRowColor = (String) item.getFieldValue(colorField.getUID());
			}
			if (StringUtils.isNullOrEmpty(nuclosRowColor)) {
				if (planElement.getColor() != null) {
					nuclosRowColor = planElement.getColor();
					if (nuclosRowColor.startsWith("0x")) {
						nuclosRowColor = nuclosRowColor.replaceFirst("0x", "#");
					}
				}
			}

			Booking booking = Booking.builder()
					.withId("" + item.getPrimaryKey())
					.withEntityMeta(planElement.getEntity().toString())
					.withBooker((String) item.getFieldValue(planElement.getBookerField()))
					.withResource("" + item.getFieldId(planElement.getPrimaryField()))
					.withFromTime((String) item.getFieldValue(planElement.getTimeFromField()))
					.withUntilTime((String) item.getFieldValue(planElement.getTimeUntilField()))
					.withFromDate(DATE_FORMAT.format(dateFrom))
					.withUntilDate(DATE_FORMAT.format(dateUntil))
					.withBackgroundColor(nuclosRowColor)
					.withLabel(bookingLabel)
					.withToolTip(bookingTooltip)
					.build();

			bookings.add(booking);
		}

		return bookings;
	}

	public Collection<Milestone> getMilestones(ResPlanConfigVO resPlanConfigVO, Interval<Date> interval, boolean ownBookings, String layoutEoId, String resourceForeignKey) {
		ArrayList<Milestone> result = new ArrayList<>();
		List<PlanElement> planElements = resPlanConfigVO.getPlanElements();
		planElements = (List<PlanElement>) planElements.stream().filter(x -> x.getType() == PlanElement.MILESTONE).collect(Collectors.toCollection(ArrayList::new));
		for(PlanElement pe : planElements) {
			result.addAll(getMilestonesForPlanElement(pe, interval, ownBookings, resPlanConfigVO.getResourceEntity(), layoutEoId, resourceForeignKey));
		}
		return result;
	}

	public ArrayList<Milestone> getMilestonesForPlanElement(final PlanElement planElement, Interval<Date> interval, boolean ownBookings, UID resourceEntity, String layoutEoId, String resourceForeignKey) {
		EntityMeta<?> meta = metaDataFacade.getEntityMeta(planElement.getEntity());
		CollectableSearchCondition filterByDateAndUser = getInternalMilestoneSearchCondition(interval, ownBookings, planElement, resourceEntity, layoutEoId, resourceForeignKey);
		Collection<? extends MasterDataVO<?>> result = masterDataFacade.getMasterData(meta, filterByDateAndUser);
		ArrayList<Milestone> milestones = new ArrayList<>();
		for(MasterDataVO item : result) {
			Date dateFrom = (Date) item.getFieldValue(planElement.getDateFromField());

			String milestoneBackground = "";
			FieldMeta colorField = meta.getFields().stream().filter(x -> x.isNuclosRowColor()).findFirst().orElse(null);
			if (colorField != null) {
				milestoneBackground = (String) item.getFieldValue(colorField.getUID());
			}
			if (StringUtils.isNullOrEmpty(milestoneBackground)) {
				milestoneBackground = planElement.getColor();
			}

			Milestone milestone = Milestone.builder()
					.withId("" + item.getPrimaryKey())
					.withEntityMeta(planElement.getEntity().toString())
					.withResource("" + item.getFieldId(planElement.getPrimaryField()))
					.withFromDate(DATE_FORMAT.format(dateFrom))
					.withBackgroundColor(milestoneBackground)
					.build();

			milestones.add(milestone);
		}

		return milestones;
	}

	public Collection<Relation> getRelations(ResPlanConfigVO resPlanConfigVO, Interval<Date> interval, boolean ownBookings) {
		ArrayList<Relation> result = new ArrayList<>();
		List<PlanElement> planElements = resPlanConfigVO.getPlanElements();
		planElements = (List<PlanElement>) planElements.stream().filter(x -> x.getType() == PlanElement.RELATION).collect(Collectors.toCollection(ArrayList::new));
		for(PlanElement pe : planElements) {
			result.addAll(getRelationsForPlanElement(pe, interval, ownBookings));
		}
		return result;
	}

	public ArrayList<Relation> getRelationsForPlanElement(final PlanElement planElement, Interval<Date> interval, boolean ownBookings) {
		EntityMeta<?> meta = metaDataFacade.getEntityMeta(planElement.getEntity());
		Collection<? extends MasterDataVO<?>> result = masterDataFacade.getMasterData(meta, null);
		ArrayList<Relation> relations = new ArrayList<>();
		for(MasterDataVO item : result) {
			Relation milestone = Relation.builder()
					.withId("" + item.getPrimaryKey())
					.withEntityMeta(planElement.getEntity().toString())
					.withTo("" + item.getFieldId(planElement.getPrimaryField()))
					.withFrom("" + item.getFieldId(planElement.getSecondaryField()))
					.withStartDecoration("" + planElement.getFromPresentation())
					.withEndDecoration("" + planElement.getToPresentation())
					.withPresentationMode("" + planElement.getPresentation())
					.withColor(planElement.getColor())
					.build();

			relations.add(milestone);
		}

		return relations;
	}

	private CollectableSearchCondition getInternalEntrySearchCondition(Interval<Date> interval, boolean ownBookings, PlanElement rpEntry, UID resource, String layoutEoId, String resourceForeignKey) {
		CollectableEntity collectableEntity = new CollectableMasterDataEntity(metaDataFacade.getEntityMeta(rpEntry.getEntity()));
		CollectableEntityField dateFromField = collectableEntity.getEntityField(rpEntry.getDateFromField());

		CollectableEntityField dateUntilField = rpEntry.isMileStone() ? dateFromField
				: collectableEntity.getEntityField(rpEntry.getDateUntilField());
		CollectableEntityField bookerField = rpEntry.getBookerField() != null ?
				collectableEntity.getEntityField(rpEntry.getBookerField()) : null;

		CompositeCollectableSearchCondition searchCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND, Arrays.asList(
				new CollectableComparison(dateFromField, ComparisonOperator.LESS_OR_EQUAL, new CollectableValueField(interval.getEnd())),
				new CollectableComparison(dateUntilField, ComparisonOperator.GREATER_OR_EQUAL, new CollectableValueField(interval.getStart()))));

		if (!StringUtils.isNullOrEmpty(layoutEoId) && !StringUtils.isNullOrEmpty(resourceForeignKey)) {
			CollectableEntity resourceEntity = new CollectableMasterDataEntity(metaDataFacade.getEntityMeta(resource));
			CollectableEntityField primaryField =
					collectableEntity.getEntityField(rpEntry.getPrimaryField());
			EntityMeta<?> meta = metaDataFacade.getEntityMeta(resource);
			FieldMeta<?> fm = meta.getFields().stream().filter(
					x -> resourceForeignKey.equals(metaDataFacade.getAllEntityFQNs().get(meta.getUID()) + "_" + x.getFieldName().replace("_", ""))
			).findFirst().orElse(null);

			long layoutEoIdLong = Long.parseLong(layoutEoId);
			EntityMeta<?> layoutMeta = metaDataFacade.getEntityMeta(fm.getForeignEntity());
			FieldMeta<?> layoutIdField = layoutMeta.getFields().stream().filter(x -> x.isPrimaryKey()).findFirst().orElse(null);

			CollectableComparison cmp = SearchConditionUtils.newComparison(layoutIdField, ComparisonOperator.EQUAL, layoutEoIdLong);
			ReferencingCollectableSearchCondition rcsc = new ReferencingCollectableSearchCondition(resourceEntity.getEntityField(fm.getUID()), cmp);

			CollectableSubCondition csc = new CollectableSubCondition(primaryField.getUID(), resourceEntity.getUID(), null, rcsc);


			searchCondition.addOperand(csc);
		}

		if (bookerField != null && ownBookings) {
			searchCondition.addOperand(new CollectableInIdCondition<UID>(bookerField, Arrays.asList(securityFacade.getUserUid(securityFacade.getUserName()))));
		}
		EntityMeta<?> eMeta = metaDataFacade.getEntityMeta(rpEntry.getEntity());
		if (eMeta.isStateModel()) {
			searchCondition.addOperand(new CollectableComparison(new CollectableEOEntityField(metaDataFacade.getEntityField(SF.LOGICALDELETED.getUID(eMeta))), ComparisonOperator.EQUAL, new CollectableValueField(Boolean.FALSE)));
		}
		return searchCondition;
	}

	private CollectableSearchCondition getInternalMilestoneSearchCondition(Interval<Date> interval, boolean ownBookings, PlanElement rpEntry, UID resource, String layoutEoId, String resourceForeignKey) {
		CollectableEntity collectableEntity = new CollectableMasterDataEntity(metaDataFacade.getEntityMeta(rpEntry.getEntity()));
		CollectableEntityField dateFromField = collectableEntity.getEntityField(rpEntry.getDateFromField());

		CollectableEntityField bookerField = rpEntry.getBookerField() != null ?
				collectableEntity.getEntityField(rpEntry.getBookerField()) : null;

		CompositeCollectableSearchCondition searchCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND, Arrays.asList(
				new CollectableComparison(dateFromField, ComparisonOperator.LESS_OR_EQUAL, new CollectableValueField(interval.getEnd())),
				new CollectableComparison(dateFromField, ComparisonOperator.GREATER_OR_EQUAL, new CollectableValueField(interval.getStart()))));

		if (!StringUtils.isNullOrEmpty(layoutEoId) && !StringUtils.isNullOrEmpty(resourceForeignKey)) {
			CollectableEntity resourceEntity = new CollectableMasterDataEntity(metaDataFacade.getEntityMeta(resource));
			CollectableEntityField primaryField =
					collectableEntity.getEntityField(rpEntry.getPrimaryField());
			CollectableEntityField foreignKeyField =
					resourceEntity.getEntityField(this.parseFQN(metaDataFacade.getEntityMeta(resource), resourceForeignKey));

			long layoutEoIdLong = Long.parseLong(layoutEoId);
			EntityMeta<?> meta = metaDataFacade.getEntityMeta(resource);
			FieldMeta<?> fm = meta.getFields().stream().filter(
					x -> resourceForeignKey.equals(metaDataFacade.getAllEntityFQNs().get(meta.getUID()) + "_" + x.getFieldName().replace("_", ""))
			).findFirst().orElse(null);

			EntityMeta<?> layoutMeta = metaDataFacade.getEntityMeta(fm.getForeignEntity());
			FieldMeta<?> layoutIdField = layoutMeta.getFields().stream().filter(x -> x.isPrimaryKey()).findFirst().orElse(null);

			CollectableComparison cmp = SearchConditionUtils.newComparison(layoutIdField, ComparisonOperator.EQUAL, layoutEoIdLong);
			ReferencingCollectableSearchCondition rcsc = new ReferencingCollectableSearchCondition(resourceEntity.getEntityField(fm.getUID()), cmp);

			CollectableSubCondition csc = new CollectableSubCondition(primaryField.getUID(), resourceEntity.getUID(), null, rcsc);

			searchCondition.addOperand(csc);
		}

		if (bookerField != null && ownBookings) {
			searchCondition.addOperand(new CollectableInIdCondition<UID>(bookerField, Arrays.asList(securityFacade.getUserUid(securityFacade.getUserName()))));
		}
		EntityMeta<?> eMeta = metaDataFacade.getEntityMeta(rpEntry.getEntity());
		if (eMeta.isStateModel()) {
			searchCondition.addOperand(new CollectableComparison(new CollectableEOEntityField(metaDataFacade.getEntityField(SF.LOGICALDELETED.getUID(eMeta))), ComparisonOperator.EQUAL, new CollectableValueField(Boolean.FALSE)));
		}
		return searchCondition;
	}

	private CollectableSearchCondition getInternalRelationSearchCondition(Interval<Date> interval, boolean ownBookings, PlanElement rpEntry) {
		CollectableEntity collectableEntity = new CollectableMasterDataEntity(metaDataFacade.getEntityMeta(rpEntry.getEntity()));
		CollectableEntityField dateFromField = collectableEntity.getEntityField(rpEntry.getDateFromField());

		CollectableEntityField bookerField = rpEntry.getBookerField() != null ?
				collectableEntity.getEntityField(rpEntry.getBookerField()) : null;

		CompositeCollectableSearchCondition searchCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND, Arrays.asList());
		if (bookerField != null && ownBookings) {
			searchCondition.addOperand(new CollectableInIdCondition<UID>(bookerField, Arrays.asList(securityFacade.getUserUid(securityFacade.getUserName()))));
		}
		EntityMeta<?> eMeta = metaDataFacade.getEntityMeta(rpEntry.getEntity());
		if (eMeta.isStateModel()) {
			searchCondition.addOperand(new CollectableComparison(new CollectableEOEntityField(metaDataFacade.getEntityField(SF.LOGICALDELETED.getUID(eMeta))), ComparisonOperator.EQUAL, new CollectableValueField(Boolean.FALSE)));
		}
		return searchCondition;
	}

	private UID parseFQN(EntityMeta<?> meta, String fqn) {
		FieldMeta<?> fm = meta.getFields().stream().filter(
				x -> fqn.equals(metaDataFacade.getAllEntityFQNs().get(meta.getUID()) + "_" + x.getFieldName().replace("_", ""))
		).findFirst().orElse(null);
		return fm.getUID();
	}

}
