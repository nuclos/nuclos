package org.nuclos.server.customcomp.resplan;

import java.util.Date;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.customcomp.resplan.ResPlanConfigVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.interval.Interval;
import org.nuclos.schema.rest.PlanElementDef;
import org.nuclos.schema.rest.PlanElementDefList;
import org.nuclos.schema.rest.PlanningTable;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

public interface PlanningTableFacadeLocal {

	ResPlanConfigVO getResPlanConfig(UID uid) throws CommonPermissionException, CommonFinderException;

	PlanningTable getPlanningTable(UID configUID, Interval<Date> interval, boolean ownBookingsOnly, CollectableSearchCondition resourceSearchFilter, String layoutEoId, String resourceForeignKey) throws CommonBusinessException;

	PlanElementDef getPlanElementDef(UID planTableDef, UID planElementMeta) throws CommonBusinessException;

	PlanElementDefList getAllPlanElementDefs(UID planTableDef) throws CommonBusinessException;

	void updateEvent(UID planningTableId, UID metaId, String entityId, String resource, Date dateFrom,
					 Date dateUntil, String timeFrom, String timeUntil) throws CommonFinderException, CommonPermissionException, CommonRemoveException, CommonCreateException, CommonValidationException, CommonStaleVersionException, NuclosBusinessRuleException;

}
