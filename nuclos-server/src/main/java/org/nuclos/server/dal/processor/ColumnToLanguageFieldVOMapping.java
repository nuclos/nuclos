package org.nuclos.server.dal.processor;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;

public class ColumnToLanguageFieldVOMapping<T, PK> extends
		ColumnToFieldVOMapping<T, PK> {

	private UID langUID;
	private boolean forDataMap;
	
	public ColumnToLanguageFieldVOMapping(String alias, FieldMeta<?> field, UID langUID,
			boolean readonly, boolean forDataMap) throws ClassNotFoundException {
		super(alias, field, readonly, false);
		this.langUID = langUID;
		this.forDataMap = forDataMap;
	}
	
	public UID getLanguageUID() {
		return this.langUID;
	}
	
	public boolean isForDataMap() {
		return this.forDataMap;
	}
	
	@Override
	public final String getTableAlias() {
		return super.getTableAlias();
	}

	@Override
	public String toString() {
		return generateString("langUID", langUID);
	}
}
