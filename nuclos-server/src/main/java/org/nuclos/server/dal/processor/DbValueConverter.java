//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import java.awt.Dimension;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.INuclosImageProducer;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.RigidFile;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.util.ServerCryptUtil;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.resource.valueobject.ResourceFile;

public class DbValueConverter implements IDbValueConverter {
	@Override
	public <S, PK> S convertFromDbValue(final Object value, final String column, final Class<S> dataType, final PK recordPk, final FromDbValueConversionParams params) {
		if (dataType == ResourceFile.class) {
			if (value == null) {
				return null;
			}
			RigidFile rf = (RigidFile) value;
			return (S) new ResourceFile(rf.getFilename(), recordPk);
		} else if (dataType == GenericObjectDocumentFile.class) {
			if (value == null) {
				return null;
			}
			RigidFile rf = (RigidFile) value;
			return (S) new GenericObjectDocumentFile(rf.getFilename(), recordPk);
		} else if (dataType == NuclosPassword.class) {
			try {
				return (S) new NuclosPassword(ServerCryptUtil.decrypt((String) value));
			} catch (SQLException e) {
				throw new CommonFatalException(e);
			}
		} else if (dataType == NuclosImage.class && params.includeThumbnailsOnly()) {
			if (value == null) {
				return null;
			}
			NuclosImage image = (NuclosImage) value;

			ServerParameterProvider parameterProvider = SpringApplicationContextHolder.getBean(ServerParameterProvider.class);
			String sParamThumbnailSize = parameterProvider.getValue(ServerParameterProvider.KEY_THUMBAIL_SIZE);
			int iWidth;
			int iHeight;
			if (StringUtils.isNotBlank(sParamThumbnailSize)) {
				String s[] = sParamThumbnailSize.split("\\*");
				iWidth = Integer.parseInt(s[0]);
				iHeight = Integer.parseInt(s[1]);
			} else {
				iWidth = INuclosImageProducer.DEFAULTTHUMBSIZE;
				iHeight = INuclosImageProducer.DEFAULTTHUMBSIZE;
			}
			Object result = new NuclosImage(image.getName(), image.getContent(), new Dimension(iWidth, iHeight));
			return dataType.cast(result);

		}
		return null;
	}

	@Override
	public Object convertToDbValue(final Class<?> javaType, final Object value) {
		if (value instanceof ResourceFile) {
			return ((ResourceFile) value).getFilename();
		} else if (value instanceof GenericObjectDocumentFile) {
			return ((GenericObjectDocumentFile) value).getFilename();
		} else if (value instanceof DateTime) {
			return new InternalTimestamp(((DateTime) value).getTime());
		} else if (value instanceof NuclosPassword) {
			try {
				String encrypted = ServerCryptUtil.encrypt(((NuclosPassword) value).getValue());
				if (encrypted == null) {
					return DbNull.forType(java.lang.String.class);
				} else {
					return encrypted;
				}
			} catch (SQLException e) {
				throw new CommonFatalException(e);
			}
		}
		return null;
	}
}
