//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import java.util.Collection;

import javax.inject.Inject;

import org.nuclos.businessentity.utils.BusinessObjectBuilderForInternalUse;
import org.nuclos.common.ConstructorParameter;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationHelper;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dal.processor.jdbc.impl.ChartEntityObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DynamicEntityObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DynamicTasklistEntityObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.EOWithLangSupportProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.EOWithStateModelProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.ExtendedEntityObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.ImportObjectProcessor;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dal.processor.proxy.impl.ProxyEntityObjectProcessor;
import org.nuclos.server.dal.processor.proxy.impl.WriteProxyEntityObjectProcessor;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.datasource.DatasourceMetaParser;
import org.nuclos.server.fileimport.ImportStructure;
import org.nuclos.server.i18n.DataLanguageCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ProcessorFactorySingleton extends AbstractProcessorFactory {

	private static ProcessorFactorySingleton INSTANCE;
	
	// Spring injection

	@Inject
	private ApplicationContext applicationContext;

	private TableAliasSingleton tableAliasSingleton;

	private SpringDataBaseHelper dataBaseHelper;
	
	private SessionUtils utils;
	
	private DataLanguageCache dataLangCache;
	
	private NuclosUserDetailsContextHolder userContext;

	private DatasourceMetaParser datasourceMetaParser;
	

	// end of Spring injection
	
	private ProcessorFactorySingleton() {
		INSTANCE = this;
	}

	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static ProcessorFactorySingleton getInstance() {
		return INSTANCE;
	}
	
	@Autowired
	void setTableAliasSingleton(TableAliasSingleton tableAliasSingleton) {
		this.tableAliasSingleton = tableAliasSingleton;
	}
	
	@Autowired
	void setDataLanguageCache(DataLanguageCache dataLangCache) {
		this.dataLangCache = dataLangCache;
	}
	
	@Autowired
	void setSpringDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}
	
	@Autowired
	void setNuclosUserDetailsContextHolder(NuclosUserDetailsContextHolder userContext) {
		this.userContext = userContext;
	}
	
	@Autowired
	void setSessionUtils(SessionUtils utils) {
		this.utils = utils;
	}

	@Autowired
	void setDatasourceMetaParser(DatasourceMetaParser datasourceMetaParser) {
		this.datasourceMetaParser = datasourceMetaParser;
	}

	public static <S extends Object, PK> IColumnToVOMapping<S, PK> createLanguageFieldMapping(String alias, UID langUID, FieldMeta<?> field, boolean readonly, boolean forDataMap) {
		try {
			return new ColumnToLanguageFieldVOMapping<S, PK>(alias, field, langUID, readonly, forDataMap);
		} catch (ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}

	public <PK> IExtendedEntityObjectProcessor<PK> newEntityObjectProcessor(EntityMeta<PK> eMeta, Collection<FieldMeta<?>> colEfMeta, boolean addSystemColumns) {
		final Class<EntityObjectVO<PK>> type = LangUtils.getGenericClass(EntityObjectVO.class);
		
		if (eMeta.isProxy()) {
			final boolean bForInternalUseOnly = E.isNuclosEntity(eMeta.getUID()) && BusinessObjectBuilderForInternalUse.getEntityMetas().contains(eMeta);
			final IExtendedEntityObjectProcessor<PK> result = new ProxyEntityObjectProcessor<PK>(
					eMeta, type, (Class<PK>) (eMeta.isUidEntity() ?
					(bForInternalUseOnly ? org.nuclos.common.UID.class : org.nuclos.api.UID.class) :
					Long.class));
			return result;
		} else {
			final ProcessorConfiguration<EntityObjectVO<PK>, PK> config = newProcessorConfiguration(
					type, eMeta, colEfMeta, addSystemColumns,
					NuclosDalProvider.createCachedObjectBuilder(eMeta));

			IExtendedEntityObjectProcessor<PK> result = null;
			if (eMeta.isStateModel()) {
				result = (IExtendedEntityObjectProcessor<PK>) SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, EOWithStateModelProcessor.class, config);
			} else if (eMeta.IsLocalized()) {
				result = (IExtendedEntityObjectProcessor<PK>) SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, EOWithLangSupportProcessor.class, config);
			} else {
				// use default spring injection, SpringApplicationContextHolder is "too early" here
				result = (IExtendedEntityObjectProcessor<PK>) SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, ExtendedEntityObjectProcessor.class, config);
			}

			ExtendedEntityObjectProcessor<PK> eoProc = (ExtendedEntityObjectProcessor<PK>) result;

			if (eMeta.isWriteProxy()) {
				result = (IExtendedEntityObjectProcessor<PK>) SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, WriteProxyEntityObjectProcessor.class, eoProc);
			}

			return result;
		}		
	}

	public <PK> DynamicEntityObjectProcessor<PK> newDynamicEntityObjectProcessor(EntityMeta<PK> eMeta, Collection<FieldMeta<?>> colEfMeta) {
		final Class<EntityObjectVO<PK>> type = LangUtils.getGenericClass(EntityObjectVO.class);
		final ProcessorConfiguration<EntityObjectVO<PK>, PK> config = newProcessorConfiguration(
				type, eMeta, colEfMeta, false, null);
		
		final DynamicEntityObjectProcessor<PK> result = SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, DynamicEntityObjectProcessor.class, config);
		
		return result;
	}

	public <PK> ChartEntityObjectProcessor<PK> newChartEntityObjectProcessor(EntityMeta<PK> eMeta, Collection<FieldMeta<?>> colEfMeta) {
		final Class<EntityObjectVO<PK>> type = LangUtils.getGenericClass(EntityObjectVO.class);
		final ProcessorConfiguration<EntityObjectVO<PK>, PK> config = newProcessorConfiguration(
				type, eMeta, colEfMeta, false, null);
		
		final ChartEntityObjectProcessor<PK> result = SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, ChartEntityObjectProcessor.class, config);
		
		return result;
	}

	public <PK> DynamicTasklistEntityObjectProcessor<PK> newDynamicTasklistEntityObjectProcessor(EntityMeta<PK> eMeta, Collection<FieldMeta<?>> colEfMeta) {
		final Class<EntityObjectVO<PK>> type = LangUtils.getGenericClass(EntityObjectVO.class);
		final ProcessorConfiguration<EntityObjectVO<PK>, PK> config = newProcessorConfiguration(
				type, eMeta, colEfMeta, false, null);

		final DynamicTasklistEntityObjectProcessor<PK> result = SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, DynamicTasklistEntityObjectProcessor.class, config);

		return result;
	}

	public <PK> RuleBasedDynamicTasklistEntityObjectProcessor<PK> newRuleBasedDynamicTasklistEntityObjectProcessor(EntityMeta<PK> eMeta, Collection<FieldMeta<?>> colEfMeta) {
		final Class<IDalVO<PK>> eov = LangUtils.getGenericClass(EntityObjectVO.class);

		final RuleBasedDynamicTasklistEntityObjectProcessor<PK> result = SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, RuleBasedDynamicTasklistEntityObjectProcessor.class, eMeta.getUID(), eov, Long.class);

		return result;
	}

	public <PK> ImportObjectProcessor<PK> newImportObjectProcessor(
			EntityMeta<PK> eMeta, Collection<FieldMeta<?>> colEfMeta, ImportStructure structure) {

		final Class<EntityObjectVO<PK>> type = LangUtils.getGenericClass(EntityObjectVO.class);
		final ProcessorConfiguration<EntityObjectVO<PK>, PK> config = newProcessorConfiguration(
				type, eMeta, colEfMeta, true, null);
		final ImportObjectProcessor<PK> result = SpringApplicationHelper.newInstanceAutowiredAndInitialized(applicationContext, ImportObjectProcessor.class, config,
				new ConstructorParameter(structure, ImportStructure.class));
		
		return result;
	}

	@Override
	protected TableAliasSingleton getTableAliasSingleton() {
		return tableAliasSingleton;
	}
}
