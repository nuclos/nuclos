package org.nuclos.server.dal.processor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.api.datasource.DatasourceColumn;
import org.nuclos.api.datasource.DatasourceResult;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.NuclosRuleCompileException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.RecordGrantRight;
import org.nuclos.server.dal.processor.nuclos.IEOChunkableProcessor;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.eventsupport.ejb3.DatasourceRuleContext;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeBean;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;

public class RuleBasedDynamicTasklistEntityObjectProcessor<PK> extends AbstractDalProcessor<EntityObjectVO<PK>, PK> implements IExtendedEntityObjectProcessor<PK>, IEOChunkableProcessor<PK> {

	@Autowired
	MetaProvider metaProvider;

	@Autowired
	EventSupportFacadeBean eventSupportFacadeBean;

	protected RuleBasedDynamicTasklistEntityObjectProcessor(final UID entity, final Class<EntityObjectVO<PK>> type, final Class<PK> pkType) {
		super(entity, type, pkType);
	}

	@Override
	public List<EntityObjectVO<PK>> getChunkBySearchExpressionImpl(final CollectableSearchExpression clctexpr, final ResultParams resultParams) {
		final Map<String, Object> mpParams = new HashMap<>();
		mpParams.put("limit", resultParams.getLimit());
		mpParams.put("offset", resultParams.getOffset());

		//class of DatasourceRule is stored in DbTable of entity meta data
		try {
			DatasourceRuleContext context = new DatasourceRuleContext(getDatasourceRule(), DatasourceRuleContext.Type.GET_ALL, getSearchExpression(clctexpr), mpParams);
			final DatasourceResult datasourceResult = (DatasourceResult) eventSupportFacadeBean.executeDatasourceRuleCall(context);

			final List<EntityObjectVO<PK>> result = transformDatasourceResult(metaProvider.getEntity(getEntityUID()), datasourceResult);

			return result;
		} catch (NuclosBusinessRuleException | NuclosRuleCompileException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public UID getOwner(final PK id) {
		return null;
	}

	@Override
	public void lock(final PK id, final UID userUID) {
		throw new UnsupportedOperationException("\"lock\" is not supported by rule based dynamic tasklist entity");
	}

	@Override
	public void unlock(final PK id) {
		throw new UnsupportedOperationException("\"unlock\" is not supported by rule based dynamic tasklist entity");
	}

	@Override
	public void setIgnoreRecordGrantsAndOthers(final boolean ignore) {
		//do nothing
	}

	@Override
	public boolean getIgnoreRecordGrantsAndOthers() {
		return false;
	}

	@Override
	public void setThinReadEnabled(final boolean enabled) {
		//do nothing
	}

	@Override
	public boolean isThinReadEnabled() {
		return false;
	}

	@Override
	public void cancelRunningStatements() {
		//do nothing
	}

	@Override
	public void destroy() {
	}

	@Override
	public Long count(final CollectableSearchExpression clctexpr) {
		try {
			DatasourceRuleContext context = new DatasourceRuleContext(getDatasourceRule(), DatasourceRuleContext.Type.COUNT, getSearchExpression(clctexpr), new HashMap<>());
			final Long count = (Long) eventSupportFacadeBean.executeDatasourceRuleCall(context);

			return count;
		} catch (NuclosBusinessRuleException | NuclosRuleCompileException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public List<EntityObjectVO<PK>> getAll() {
		final EntityMeta<PK> entityMeta = metaProvider.getEntity(getEntityUID());

		try {
			DatasourceRuleContext context = new DatasourceRuleContext(getDatasourceRule(), DatasourceRuleContext.Type.GET_ALL, null, new HashMap<>());
			final DatasourceResult datasourceResult = (DatasourceResult) eventSupportFacadeBean.executeDatasourceRuleCall(context);

			final List<EntityObjectVO<PK>> result = transformDatasourceResult(entityMeta, datasourceResult);

			return result;
		} catch (NuclosBusinessRuleException | NuclosRuleCompileException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public List<PK> getAllIds() {
		try {
			DatasourceRuleContext context = new DatasourceRuleContext(getDatasourceRule(), DatasourceRuleContext.Type.GET_ALL_IDS, null, new HashMap<>());
			final List<PK> ids = (List<PK>) eventSupportFacadeBean.executeDatasourceRuleCall(context);

			return ids;
		} catch (NuclosBusinessRuleException | NuclosRuleCompileException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public EntityObjectVO<PK> getByPrimaryKey(final PK id) {
		final EntityMeta<PK> entityMeta = metaProvider.getEntity(getEntityUID());

		try {
			final HashMap<String, Object> mpParams = new HashMap<>();
			mpParams.put("INTID", id);
			DatasourceRuleContext context = new DatasourceRuleContext(getDatasourceRule(), DatasourceRuleContext.Type.GET_BY_ID, null, mpParams);
			final DatasourceResult datasourceResult = (DatasourceResult) eventSupportFacadeBean.executeDatasourceRuleCall(context);

			final List<EntityObjectVO<PK>> result = transformDatasourceResult(entityMeta, datasourceResult);

			return !result.isEmpty() ? result.get(0) : null;
		} catch (NuclosBusinessRuleException | NuclosRuleCompileException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public EntityObjectVO<PK> getByPrimaryKey(final PK id, final Collection<FieldMeta<?>> fields) {
		//fields are not supported
		return getByPrimaryKey(id);
	}

	@Override
	public List<EntityObjectVO<PK>> getByPrimaryKeys(final List<PK> ids) {
		final EntityMeta<PK> entityMeta = metaProvider.getEntity(getEntityUID());

		try {
			final HashMap<String, Object> mpParams = new HashMap<>();
			mpParams.put("INTID", ids);
			DatasourceRuleContext context = new DatasourceRuleContext(getDatasourceRule(), DatasourceRuleContext.Type.GET_BY_IDS, null, mpParams);
			final DatasourceResult datasourceResult = (DatasourceResult) eventSupportFacadeBean.executeDatasourceRuleCall(context);

			final List<EntityObjectVO<PK>> result = transformDatasourceResult(entityMeta, datasourceResult);

			return result;
		} catch (NuclosBusinessRuleException | NuclosRuleCompileException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public List<EntityObjectVO<PK>> getBySearchExpression(final CollectableSearchExpression clctexpr) {
		final Map<String, Object> mpParams = new HashMap<>();

		//class of DatasourceRule is stored in DbTable of entity meta data
		try {
			DatasourceRuleContext context = new DatasourceRuleContext(getDatasourceRule(), DatasourceRuleContext.Type.GET_ALL, getSearchExpression(clctexpr), mpParams);
			final DatasourceResult datasourceResult = (DatasourceResult) eventSupportFacadeBean.executeDatasourceRuleCall(context);

			final List<EntityObjectVO<PK>> result = transformDatasourceResult(metaProvider.getEntity(getEntityUID()), datasourceResult);

			return result;
		} catch (NuclosBusinessRuleException | NuclosRuleCompileException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public List<EntityObjectVO<PK>> getBySearchExprResultParams(final CollectableSearchExpression clctexpr, final ResultParams resultParams) {
		return getChunkBySearchExpressionImpl(clctexpr, resultParams);
	}

	@Override
	public List<PK> getIdsBySearchExpression(final CollectableSearchExpression clctexpr) {
		final Map<String, Object> mpParams = new HashMap<>();

		//class of DatasourceRule is stored in DbTable of entity meta data
		try {
			DatasourceRuleContext context = new DatasourceRuleContext(getDatasourceRule(), DatasourceRuleContext.Type.GET_ALL, getSearchExpression(clctexpr), mpParams);
			final DatasourceResult datasourceResult = (DatasourceResult) eventSupportFacadeBean.executeDatasourceRuleCall(context);

			final EntityMeta<PK> entityMeta = metaProvider.getEntity(getEntityUID());
			final List<EntityObjectVO<PK>> result = transformDatasourceResult(entityMeta, datasourceResult);

			return (List<PK>) result.stream()
					.map(eovo ->
							eovo.getFieldValue(entityMeta.getFields().stream()
									.filter(field -> "INTID".equals(field.getFieldName()))
									.findFirst().get().getUID()))
					.collect(Collectors.toList());
		} catch (NuclosBusinessRuleException | NuclosRuleCompileException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public Set<UsageCriteria> getUsageCriteriaBySearchExpression(final CollectableSearchExpression clctexpr) {
		return null;
	}

	@Override
	public Set<RecordGrantRight> getRecordGrantRightBySearchExpression(final CollectableSearchExpression clctexpr) {
		return null;
	}

	@Override
	public Set<UID> getNuclosOwnerBySearchExpression(final CollectableSearchExpression clctexpr) {
		return null;
	}

	@Override
	public Integer getVersion(final PK pk) {
		return null;
	}

	@Override
	public Object insertOrUpdate(final EntityObjectVO<PK> dalVO) throws DbException {
		throw new UnsupportedOperationException("insert/update is not supported by rule based dynamic tasklist entity");
	}

	@Override
	public void checkLogicalUniqueConstraint(final EntityObjectVO<PK> dalVO) throws DbException {
		//do nothing
	}

	@Override
	public void delete(final Delete<PK> del) throws DbException {
		throw new UnsupportedOperationException("delete is not supported by rule based dynamic tasklist entity");
	}

	private SearchExpression<?> getSearchExpression(final CollectableSearchExpression clctexpr) {
		if (clctexpr.getSearchCondition() != null) {
			return clctexpr.getSearchCondition().accept(new ToApiSearchExpressionVisitor(this.metaProvider, getEntityUID()));
		}
		return null;
	}

	private String getDatasourceRule() {
		final EntityMeta<Object> entityMeta = metaProvider.getEntity(getEntityUID());
		return entityMeta.getDbTable();
	}

	private List<EntityObjectVO<PK>> transformDatasourceResult(final EntityMeta<PK> entityMeta,
															   final DatasourceResult datasourceResult) {
		final List<EntityObjectVO<PK>> result = new ArrayList<>();

		for (Object[] row : datasourceResult.getRows()) {
			EntityObjectVO<PK> eovo = new EntityObjectVO<>(entityMeta);

			for (int i = 0; i < datasourceResult.getColumns().size(); i++) {
				DatasourceColumn column = datasourceResult.getColumns().get(i);
				Object value = row[i];

				if (entityMeta.getPk().getDbColumn().toUpperCase().equals(column.getName().toUpperCase())) {
					eovo.setPrimaryKey((PK) value);

					continue;
				}

				for (FieldMeta<?> field : entityMeta.getFields()) {
					if (field.getFieldName().equals(column.getName())) {
						eovo.setFieldValue(field.getUID(), value);
						break;
					}
				}
			}

			result.add(eovo);
		}

		return result;
	}
}
