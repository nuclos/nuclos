package org.nuclos.server.dal.processor;

import java.math.BigDecimal;
import java.util.Date;

import org.nuclos.api.businessobject.QueryOperation;
import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.NumericAttribute;
import org.nuclos.api.businessobject.attribute.StringAttribute;
import org.nuclos.businessentity.NucletIntegrationPoint;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonDateValues;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithOtherField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithParameter;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIsNullCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.visit.AtomicVisitor;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.RelativeDate;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.SessionUtils;

public class ToApiSearchExpressionAtomicVisitor implements AtomicVisitor<SearchExpression<?>, NuclosFatalException> {


	private final MetaProvider metaProvider;
	private final UID entityUid;

	public ToApiSearchExpressionAtomicVisitor(final MetaProvider metaProvider, final UID entityUid) {
		this.metaProvider = metaProvider;
		this.entityUid = entityUid;
	}

	@Override
	public SearchExpression<?> visitComparison(final CollectableComparison comparison) throws NuclosFatalException {
		FieldMeta<?> entityField = metaProvider.getEntityField(comparison.getFieldUID());
		Attribute<?> attribute = createAttribute(null, null, entityUid.getString(),
				comparison.getFieldUID().getString(), entityField.getJavaClass());
		Object value;
		try {
			value = comparison.getComparand().getValueId();
			if (value == null) {
				value = comparison.getComparand().getValue();
			}
		} catch (UnsupportedOperationException e) {
			//no id field
			value = comparison.getComparand().getValue();
		}
		return new SearchExpression(attribute, value, getQueryOperation(comparison.getComparisonOperator()));
	}

	@Override
	public SearchExpression<?> visitComparisonWithOtherField(final CollectableComparisonWithOtherField comparisonwf) throws NuclosFatalException {
		FieldMeta<?> entityField = metaProvider.getEntityField(comparisonwf.getFieldUID());
		Attribute<?> sourceAttribute = createAttribute(null, null, entityUid.getString(),
				comparisonwf.getFieldUID().getString(), entityField.getJavaClass());
		Attribute<?> targetAttribute = createAttribute(null, null, entityUid.getString(),
				comparisonwf.getFieldUID().getString(), entityField.getJavaClass());
		return new SearchExpression(sourceAttribute, targetAttribute, getQueryOperation(comparisonwf.getComparisonOperator()));
	}

	@Override
	public SearchExpression<?> visitComparisonWithParameter(final CollectableComparisonWithParameter comparisonwp) throws NuclosFatalException {
		final ComparisonOperator compop = comparisonwp.getComparisonOperator();
		final CollectableEntityField field = comparisonwp.getEntityField();
		final CollectableField comparand;
		// Reduce parameter to a regular comparand value...
		switch (comparisonwp.getParameter()) {
			case TODAY:
				comparand = new CollectableValueField(RelativeDate.today());
				break;
			case USER:
				SessionUtils utils = SpringApplicationContextHolder.getBean(SessionUtils.class);
				String userName = utils.getCurrentUserName();
				if (field.isIdField()) {
					UID userUID = SecurityCache.getInstance().getUserUid(userName);
					comparand = new CollectableValueIdField(userUID, userName);
				} else {
					comparand = new CollectableValueField(userName);
				}
				break;
			default:
				throw new UnsupportedOperationException("Unsupported parameter " + comparisonwp.getParameter());
		}
		// ...and delegate to that handler method
		return visitComparison(new CollectableComparison(field, compop, comparand));
	}

	@Override
	public SearchExpression<?> visitComparisonDateValues(final CollectableComparisonDateValues comparisondv) throws NuclosFatalException {
		FieldMeta<?> entityField = metaProvider.getEntityField(comparisondv.getFieldUID());
		Attribute<?> attribute = createAttribute(null, null, entityUid.getString(),
				comparisondv.getFieldUID().getString(), entityField.getJavaClass());
		if (Date.class.equals(entityField.getJavaClass())) {
			return ((NumericAttribute) attribute).Gte(comparisondv.getDateValues().getFromDate()).and(((NumericAttribute) attribute).Lte(comparisondv.getDateValues().getToDate()));
		}
		throw new IllegalArgumentException("CollectableComparisonDateValues is not supported for anything else than NumericAttribute");
	}

	@Override
	public SearchExpression<?> visitLikeCondition(final CollectableLikeCondition likecond) throws NuclosFatalException {
		FieldMeta<?> entityField = metaProvider.getEntityField(likecond.getFieldUID());
		if (String.class.isAssignableFrom(entityField.getJavaClass())
				|| Date.class.isAssignableFrom(entityField.getJavaClass())
				|| InternalTimestamp.class.isAssignableFrom(entityField.getJavaClass())) {
			Attribute<?> attribute = createAttribute(null, null, entityUid.getString(),
					likecond.getFieldUID().getString(), String.class);
			return ((StringAttribute) attribute).like(likecond.getLikeComparand());
		} else if (Integer.class.isAssignableFrom(entityField.getJavaClass()) || Long.class.isAssignableFrom(entityField.getJavaClass())) {
			return visitComparison(SearchConditionUtils.newComparison(entityField, ComparisonOperator.EQUAL, Long.valueOf(likecond.getLikeComparand().replaceAll("[*%_?]",""))));
		}
		throw new IllegalArgumentException("CollectableLikeCondition is not supported for anything else than StringAttribute");
	}

	@Override
	public SearchExpression<?> visitIsNullCondition(final CollectableIsNullCondition isnullcond) throws NuclosFatalException {
		FieldMeta<?> entityField = metaProvider.getEntityField(isnullcond.getFieldUID());
		Attribute<?> attribute = createAttribute(null, null, entityUid.getString(),
				isnullcond.getFieldUID().getString(), entityField.getJavaClass());
		return isnullcond.isPositive() ? attribute.isNull() : attribute.notNull();
	}



	@Override
	public <T> SearchExpression<?> visitInCondition(final CollectableInCondition<T> incond) throws NuclosFatalException {
		FieldMeta<?> entityField = metaProvider.getEntityField(incond.getFieldUID());
		final Attribute<?> attribute = createAttribute(null, null, entityUid.getString(),
				entityField.getUID().getString(), entityField.getJavaClass());
		return new SearchExpression(attribute, incond.getInComparands(), QueryOperation.IN);
		//throw new IllegalArgumentException("CollectableInCondition is not supported");
	}

	private Attribute<?> createAttribute(String attributeName, String packageName, String entityUID, String attributeUID, Class<?> type) {
		Attribute<?> attribute;
		if ( Date.class.equals(type) ||
				Long.class.equals(type) ||
				Double.class.equals(type) ||
				BigDecimal.class.equals(type) ||
				Integer.class.equals(type)) {
			attribute = new NumericAttribute<>(attributeName, packageName, entityUID, attributeUID, type);
		} else if (InternalTimestamp.class.equals(type)) {
			attribute = new NumericAttribute<>(attributeName, packageName, entityUID, attributeUID, Date.class);
		} else if (String.class.equals(type)) {
			attribute = new StringAttribute<>(attributeName, packageName, entityUID, attributeUID, type);
		} else {
			attribute = new Attribute<>(attributeName, packageName, entityUID, attributeUID, type);
		}

		return attribute;
	}

	private QueryOperation getQueryOperation(final ComparisonOperator comparisonOperator) {
		switch (comparisonOperator) {
			case EQUAL:
				return QueryOperation.EQUALS;
			case LESS:
				return QueryOperation.LT;
			case GREATER:
				return QueryOperation.GT;
			case LESS_OR_EQUAL:
				return QueryOperation.LTE;
			case GREATER_OR_EQUAL:
				return QueryOperation.GTE;
			case IN:
				return QueryOperation.IN;
			case NOT_EQUAL:
				return QueryOperation.UNEQUALS;
			case LIKE:
				return QueryOperation.LIKE;
			case IS_NULL:
				return QueryOperation.NULL;
			case IS_NOT_NULL:
				return QueryOperation.NOTNULL;
			default:
				throw new IllegalArgumentException("ComparisonOperator " + comparisonOperator + " is not supported");
		}
	}
}
