package org.nuclos.server.dal.processor;

import java.util.stream.Collectors;

import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.GeneralJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.RefJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.ReferencingCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.collect.collectable.searchcondition.visit.Visitor;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.server.common.MetaProvider;

public class ToApiSearchExpressionVisitor implements Visitor<SearchExpression<?>, NuclosFatalException> {

	private final MetaProvider metaProvider;
	private final UID entityUid;

	public ToApiSearchExpressionVisitor(final MetaProvider metaProvider, final UID entityUid) {
		this.metaProvider = metaProvider;
		this.entityUid = entityUid;
	}

	@Override
	public SearchExpression<?> visitTrueCondition(final TrueCondition truecond) throws NuclosFatalException {
		return null;
	}

	@Override
	public SearchExpression<?> visitAtomicCondition(final AtomicCollectableSearchCondition atomiccond) throws NuclosFatalException {
		return atomiccond.accept(new ToApiSearchExpressionAtomicVisitor(this.metaProvider, this.entityUid));
	}

	@Override
	public SearchExpression<?> visitCompositeCondition(final CompositeCollectableSearchCondition compositecond) throws NuclosFatalException {
		SearchExpression<?> result = null;
		for (CollectableSearchCondition operandCondition : compositecond.getOperands()) {
			if (compositecond.getLogicalOperator() == LogicalOperator.AND || compositecond.getLogicalOperator() == LogicalOperator.OR) {
				SearchExpression<?> se = operandCondition.accept(this);
				if (result == null) {
					result = se;
				} else {
					switch (compositecond.getLogicalOperator()) {
						case AND:
							result.and(se);
							break;
						case OR:
							result.or(se);
							break;
						default:
							throw new IllegalArgumentException("LogicalOperator " + compositecond.getLogicalOperator() + " not supported in CompositeCollectableSearchCondition.");
					}
				}
			} else {
				throw new IllegalArgumentException("LogicalOperator " + compositecond.getLogicalOperator() + " not supported by SearchExpression.");
			}
		}

		return result;
	}

	@Override
	public SearchExpression<?> visitIdCondition(final CollectableIdCondition idcond) throws NuclosFatalException {
		throw new IllegalArgumentException("CollectableIdCondition is not supported");
	}

	@Override
	public SearchExpression<?> visitIdListCondition(final CollectableIdListCondition collectableIdListCondition) throws NuclosFatalException {
		CollectableEntityField pkField = new CollectableEOEntityField(metaProvider.getEntityField(SF.PK_ID.getUID(entityUid)));
		CollectableInCondition<Long> collectableInCondition = new CollectableInCondition<>(pkField, collectableIdListCondition.getLongIds().stream().collect(Collectors.toList()));
		return visitInCondition(collectableInCondition);
	}

	@Override
	public <T> SearchExpression<?> visitInCondition(final CollectableInCondition<T> collectableInCondition) throws NuclosFatalException {
		return collectableInCondition.accept(new ToApiSearchExpressionAtomicVisitor(this.metaProvider, this.entityUid));
	}

	@Override
	public SearchExpression<?> visitSubCondition(final CollectableSubCondition subcond) throws NuclosFatalException {
		return null;
	}

	@Override
	public SearchExpression<?> visitRefJoinCondition(final RefJoinCondition joincond) throws NuclosFatalException {
		throw new IllegalArgumentException("RefJoinCondition is not supported");
	}

	@Override
	public SearchExpression<?> visitGeneralJoinCondition(final GeneralJoinCondition joincond) throws NuclosFatalException {
		throw new IllegalArgumentException("GeneralJoinCondition is not supported");
	}

	@Override
	public SearchExpression<?> visitReferencingCondition(final ReferencingCollectableSearchCondition refcond) throws NuclosFatalException {
		throw new IllegalArgumentException("ReferencingCollectableSearchCondition is not supported");
	}
}
