//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.WhitelistedString;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.dal.MetaDalUtils;
import org.nuclos.server.dal.processor.IDataLanguageLocator;
import org.nuclos.server.dal.processor.jdbc.ICalcAttributeBuilder;
import org.nuclos.server.dal.processor.nuclos.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.report.SchemaCache;
import org.nuclos.server.report.suppliers.AccessibleMandatorsSupplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CalcAttributeBuilder implements ICalcAttributeBuilder {

	private final UID calcAttributeDatasourceUID;

	private final String calcAttributeParamValues;

	private final boolean fieldBelongsToMandatorEnabledEntity;

	private final DatasourceCache dsCache;
	private final SchemaCache schemaCache;

	private final DatasourceServerUtils dsUtils;

	private final NuclosDalProvider dalProvider;

	private final LocaleFacadeLocal locFacade;

	private final MetaProvider metaProv;

	private NuclosUserDetailsContextHolder userCtx;

	public CalcAttributeBuilder(final UID calcAttributeDatasourceUID,
								final String calcAttributeParamValues,
								final boolean fieldBelongsToMandatorEnabledEntity,
								final DatasourceCache dsCache,
								final SchemaCache schemaCache, final DatasourceServerUtils dsUtils,
								final NuclosDalProvider dalProvider,
								final LocaleFacadeLocal locFacade,
								final MetaProvider metaProv) {
		this.calcAttributeDatasourceUID = calcAttributeDatasourceUID;
		this.calcAttributeParamValues = calcAttributeParamValues;
		this.fieldBelongsToMandatorEnabledEntity = fieldBelongsToMandatorEnabledEntity;
		this.dsCache = dsCache;
		this.schemaCache = schemaCache;
		this.dsUtils = dsUtils;
		this.dalProvider = dalProvider;
		this.locFacade = locFacade;
		this.metaProv = metaProv;
	}

	@Autowired
	public void inject(NuclosUserDetailsContextHolder userCtx) {
		this.userCtx = userCtx;
	}

	@Override
	public String buildCalcAttributeSQL(final DbFrom<?> from) {
		Map<String, Object> mpParams = CalcAttributeUtils.stringToMap(calcAttributeParamValues);

		if (mpParams.containsKey("field_dbcolumn")) {
			if (mpParams.containsKey("struidfield")) {
				final UID baseFieldUid = UID.parseUID((String) mpParams.get("struidfield"));
				final FieldMeta<?> baseField = metaProv.getEntityField(baseFieldUid);
				if (!baseField.isCalculated()) {
					mpParams.put("fieldvalue", new WhitelistedString(from.getAlias() + "." + mpParams.get("field_dbcolumn")));
				} else {
					if (baseField.getCalcAttributeDS() != null) {
						final Object calcAttributeBuilder = FieldMeta.getCalcAttributeBuilder(baseField, metaProv);
						if (calcAttributeBuilder != null) {
							final String sSQL = ((ICalcAttributeBuilder) calcAttributeBuilder).buildCalcAttributeSQL(from);
							mpParams.put("fieldvalue", "(" + sSQL + ")");
						}
					} else if (baseField.getCalcFunction() != null) {
						String localeAdd = baseField.isLocalized() ? " , '" + ((IDataLanguageLocator) FieldMeta.getDataLanguageLocator(baseField, metaProv)).getDataLanguageToUse() + "'"  : "";

						final String sSQL = baseField.getCalcFunction() + "(" + from.basePk().getSqlColumnExpr() + localeAdd + ")";
						mpParams.put("fieldvalue", "(" + sSQL + ")");
					}
				}
			}
		}

		mpParams.put("intid", new WhitelistedString(from.getAlias() + "." + from.basePk().getAlias()));
		UID dataMandator = null;
		if (fieldBelongsToMandatorEnabledEntity) {
			dataMandator = SystemFields.getMandatorColumnUID(from.getAlias());
		}
		try {
			CalcAttributeVO calcAttributeDS = dsCache.getCalcAttribute(calcAttributeDatasourceUID);
			String dsSQL = dsUtils.createSQL(calcAttributeDS, mpParams, from, dsCache, schemaCache, userCtx::getDataLocal, dataMandator, new AccessibleMandatorsSupplier(dataMandator));
			return dsSQL;
		} catch (NuclosDatasourceException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public FieldMeta<?> buildCalcAttributeCustomization(UID fieldUID, String paramValues) {
		if (paramValues == null) {
			throw new IllegalArgumentException("paramValues must not be null");
		}
		if (fieldUID == null) {
			throw new IllegalArgumentException("fieldUID must not be null");
		}

		FieldMeta<?> baseField = metaProv.getEntityField(fieldUID);
		if (baseField.getCalcAttributeDS() == null) {
			throw new IllegalArgumentException("Field " + fieldUID + " is not calculated");
		}

		final IEntityObjectProcessor<UID> eoProc = dalProvider.getEntityObjectProcessor(E.ENTITYFIELDCUSTOMIZATION);
		List<EntityObjectVO<UID>> existing = eoProc.getAll().parallelStream()
				.filter(eo ->
						Objects.equals(fieldUID, eo.getFieldUid(E.ENTITYFIELDCUSTOMIZATION.entityfield)) &&
						Objects.equals(baseField.getCalcAttributeDS(), eo.getFieldUid(E.ENTITYFIELDCUSTOMIZATION.calcAttributeDS)) &&
						Objects.equals(paramValues, eo.getFieldValue(E.ENTITYFIELDCUSTOMIZATION.calcAttributeParamValues)))
				.collect(Collectors.toList());

		UID calcFieldUID = null;
		String resIdLabel = null;
		String resIdDesc = null;

		if (!existing.isEmpty()) {
			EntityObjectVO<UID> found = existing.get(0);
			calcFieldUID = found.getPrimaryKey();
			resIdLabel = found.getFieldValue(E.ENTITYFIELDCUSTOMIZATION.localeresourcel);
			resIdDesc = found.getFieldValue(E.ENTITYFIELDCUSTOMIZATION.localeresourced);
		}

		if (calcFieldUID == null) {
			calcFieldUID = CalcAttributeUtils.createNewCalcAttributeCustomizationUID();
			if (baseField.getLocaleResourceIdForLabel() != null) {
				resIdLabel = locFacade.createResource(baseField.getLocaleResourceIdForLabel());
			}
			if (baseField.getLocaleResourceIdForDescription() != null) {
				resIdDesc = locFacade.createResource(baseField.getLocaleResourceIdForDescription());
			}
			EntityObjectVO<UID> newCustomization = new EntityObjectVO<>(E.ENTITYFIELDCUSTOMIZATION);
			newCustomization.setPrimaryKey(calcFieldUID);
			newCustomization.setFieldUid(E.ENTITYFIELDCUSTOMIZATION.entityfield, fieldUID);
			newCustomization.setFieldUid(E.ENTITYFIELDCUSTOMIZATION.calcAttributeDS, baseField.getCalcAttributeDS());
			newCustomization.setFieldValue(E.ENTITYFIELDCUSTOMIZATION.calcAttributeParamValues, paramValues);
			if (resIdLabel != null) {
				newCustomization.setFieldValue(E.ENTITYFIELDCUSTOMIZATION.localeresourcel, resIdLabel);
			}
			if (resIdDesc != null) {
				newCustomization.setFieldValue(E.ENTITYFIELDCUSTOMIZATION.localeresourced, resIdDesc);
			}
			MetaDalUtils.updateVersionInformation(newCustomization, "metaProvider");
			newCustomization.flagNew();
			eoProc.insertOrUpdate(newCustomization);
		}

		return metaProv.getEntityField(calcFieldUID);
	}
}
