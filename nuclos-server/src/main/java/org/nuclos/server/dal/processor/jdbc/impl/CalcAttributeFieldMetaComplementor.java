//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.IFieldMetaComplementor;
import org.nuclos.common.Mutable;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.ExtendedDatasourceServerUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.report.SchemaCache;
import org.springframework.stereotype.Component;

@Component
public class CalcAttributeFieldMetaComplementor implements IFieldMetaComplementor {

	private final DatasourceCache dsCache;
	private final SchemaCache schemaCache;
	private final DatasourceServerUtils dsUtils;
	private final NuclosDalProvider dalProvider;
	private final LocaleFacadeLocal locFacade;
	private final MetaProvider metaProv;

	public CalcAttributeFieldMetaComplementor(
			final DatasourceCache dsCache,
			final SchemaCache schemaCache,
			final ExtendedDatasourceServerUtils dsUtils,
			final NuclosDalProvider dalProvider,
			final LocaleFacadeLocal locFacade,
			final MetaProvider metaProv) {
		this.dsCache = dsCache;
		this.schemaCache = schemaCache;
		this.dsUtils = dsUtils;
		this.dalProvider = dalProvider;
		this.locFacade = locFacade;
		this.metaProv = metaProv;
	}

	@PostConstruct
	private void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		metaProv.registerFieldMetaComplementor(this, false);
	}

	@Override
	public void complementFieldMeta(final FieldMetaVO<?> fieldMeta) {
		if (fieldMeta.getCalcAttributeDS() != null && fieldMeta.getCalcAttributeBuilder() == null) {
			EntityMeta<?> entityMeta = metaProv.getEntity(fieldMeta.getEntity());
			fieldMeta.setCalcAttributeBuilder(new CalcAttributeBuilder(
					fieldMeta.getCalcAttributeDS(),
					fieldMeta.getCalcAttributeParamValues(),
					entityMeta.isMandator(),
					dsCache,
					schemaCache,
					dsUtils,
					dalProvider,
					locFacade,
					metaProv));
		}
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public FieldMetaVO<Object> createFieldMeta(final UID fieldUID, final Mutable<Boolean> refreshCache) {
		if (CalcAttributeUtils.isCalcAttributeCustomization(fieldUID)) {
			EntityObjectVO<UID> calcFieldEO = dalProvider.getEntityObjectProcessor(E.ENTITYFIELDCUSTOMIZATION).getByPrimaryKey(fieldUID);
			if (calcFieldEO == null) {
				throw new CommonFatalException("customized entity field " + fieldUID + " does not exist any more.");
			}
			UID calcAttributeDS = calcFieldEO.getFieldUid(E.ENTITYFIELDCUSTOMIZATION.calcAttributeDS);
			UID baseFieldUID = calcFieldEO.getFieldUid(E.ENTITYFIELDCUSTOMIZATION.entityfield);
			String paramValues = calcFieldEO.getFieldValue(E.ENTITYFIELDCUSTOMIZATION.calcAttributeParamValues);
			String resIdLabel = calcFieldEO.getFieldValue(E.ENTITYFIELDCUSTOMIZATION.localeresourcel);
			String resIdDesc = calcFieldEO.getFieldValue(E.ENTITYFIELDCUSTOMIZATION.localeresourced);

			FieldMeta<?> baseField = metaProv.getEntityField(baseFieldUID);
			EntityMeta<?> entityMeta = metaProv.getEntity(baseField.getEntity());

			if (!Objects.equals(calcAttributeDS, baseField.getCalcAttributeDS())) {
				dalProvider.getEntityObjectProcessor(E.ENTITYFIELDCUSTOMIZATION).delete(new Delete<UID>(fieldUID));
				throw new CommonFatalException("customized entity field " + fieldUID + " is not up to date.");
			}

			FieldMetaVO result = new FieldMetaVO(baseField);
			result.setUID(fieldUID);
			result.setCalcBaseFieldUID(baseFieldUID);
			result.setDbColumn(CalcAttributeUtils.getColumnName(fieldUID));
			result.setCalcAttributeParamValues(paramValues);
			result.setCalcAttributeBuilder(new CalcAttributeBuilder(
					result.getCalcAttributeDS(),
					paramValues,
					entityMeta.isMandator(),
					dsCache,
					schemaCache,
					dsUtils,
					dalProvider,
					locFacade,
					metaProv));
			Set<Pair<String, String>> resToExtend = new HashSet<Pair<String, String>>();
			if (resIdLabel != null) {
				result.setLocaleResourceIdForLabel(resIdLabel);
				resToExtend.add(new Pair<String, String>(resIdLabel, baseField.getLocaleResourceIdForLabel()));
			}
			if (resIdDesc != null) {
				result.setLocaleResourceIdForDescription(resIdDesc);
				resToExtend.add(new Pair<String, String>(resIdDesc, baseField.getLocaleResourceIdForDescription()));
			}
			for (Pair<String, String> resPair : resToExtend) {
				for (LocaleInfo li : locFacade.getAllLocales(false)) {
					String res = locFacade.getResourceById(li, resPair.getY());
					Map<String, Object> paramValuesMap = CalcAttributeUtils.stringToMap(paramValues);
					Set<String> keys = new TreeSet<String>(paramValuesMap.keySet());
					StringBuffer resCalc = new StringBuffer();
					for (String key : keys) {
						Object nextValue = paramValuesMap.get(key);
						if (nextValue != null) {
							if (key.endsWith("Id")) {
								if (keys.contains(key.substring(0, key.length() - 2))) {
									// id from from a value list provider, ignore
									continue;
								}
							}
							if (nextValue instanceof Date) {
								nextValue = DateFormat.getDateInstance(DateFormat.SHORT, li.toLocale()).format(nextValue);
							}
							if (nextValue instanceof Double) {
								nextValue = NumberFormat.getNumberInstance(li.toLocale()).format(nextValue);
							}
							if (resCalc.length() > 0) {
								resCalc.append(", ");
							}
							resCalc.append(nextValue);
						}
					}
					res = resCalc.length() == 0 ? res : resCalc.toString();
					locFacade.setResourceForLocale(resPair.getX(), li, res);
				}
			}
			return result;
		}
		return null;
	}
}
