//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EOGenericObjectVO;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.processor.AbstractProcessorFactory;
import org.nuclos.server.dal.processor.DbValueConverter;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IDbValueConverter;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclos.IEOGenericObjectProcessor;
import org.nuclos.server.dblayer.DbException;
import org.springframework.stereotype.Component;

@Component
public class EOGenericObjectProcessor extends AbstractJdbcDalProcessor<EOGenericObjectVO, Long>
	implements IEOGenericObjectProcessor {

	private static final IDbValueConverter DB_VALUE_CONVERTER = new DbValueConverter();

	private static final Class<? extends IDalVO<Long>> type = EOGenericObjectVO.class;
	private static final IColumnToVOMapping<Long, Long> idColumn = AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_ID, E.GENERICOBJECT.getUID());
	private static final IColumnToVOMapping<UID, Long> moduleColumn = AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, E.GENERICOBJECT.module, "entityUID");

	private static List<IColumnToVOMapping<?, Long>> createAllColumns() {
		final List<IColumnToVOMapping<?, Long>> allColumns = new ArrayList<>();
		allColumns.add(idColumn);
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.GENERICOBJECT.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.GENERICOBJECT.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.GENERICOBJECT.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.GENERICOBJECT.getUID()));
		allColumns.add(AbstractProcessorFactory.createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.GENERICOBJECT.getUID()));
		allColumns.add(moduleColumn);
		return allColumns;
	}

	private final MetaProvider metaProv;

	public EOGenericObjectProcessor(
			final MetaProvider metaProv) {
		super(E.GENERICOBJECT.getUID(), type, Long.class, createAllColumns());
		this.metaProv = metaProv;
	}

	@Override
	public EntityMeta<Long> getMetaData() {
		return E.GENERICOBJECT;
	}

	@Override
	protected IRigidMetaProvider getMetaProvider() {
		return metaProv;
	}

	@Override
	protected IDbValueConverter getDbValueConverter() {
		return DB_VALUE_CONVERTER;
	}

	@Override
	protected IColumnToVOMapping<Long, Long> getPrimaryKeyColumn() {
		return idColumn;
	}

	@Override
	public void delete(Delete<Long> id) throws DbException {
		super.delete(id);
	}

	@Override
	public Object insertOrUpdate(EOGenericObjectVO dalVO) {
		return super.insertOrUpdate(dalVO);
	}

	@Override
	public List<EOGenericObjectVO> getAll() {
		return super.getAll();
	}
	
	@Override
	public EOGenericObjectVO getByPrimaryKey(Long id) {
		return super.getByPrimaryKey(id);
	}

	@Override
	public List<EOGenericObjectVO> getByPrimaryKeys(List<Long> ids) {
		return super.getByPrimaryKeys(allColumns, ids);
	}

	@Override
	public List<EOGenericObjectVO> getByParent(UID parentId) {
		return super.getByColumn(allColumns, moduleColumn, parentId);
	}
	
	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<Long> dalVO) throws DbException {
		throw new NotImplementedException();
	}
	
}
