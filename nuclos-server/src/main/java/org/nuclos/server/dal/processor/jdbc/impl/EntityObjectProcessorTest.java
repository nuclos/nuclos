package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.Collections;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.test.IntegrationTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class EntityObjectProcessorTest implements IntegrationTest {

	@Autowired
	protected NucletDalProvider nucletDalProvider;

	@Override
	public Map<String, Object> run(final String sTestcase, final Map<String, Object> mapParams) throws Exception {
		if ("runAddJoinsForSortingOrder_RefOverRefWithIdSelectionOnly".equals(sTestcase)) {
			UID orderUID = UID.parseUID((String) mapParams.get("orderUid"));
			UID rechnungAttrUID = UID.parseUID((String) mapParams.get("rechnungAttrUid"));
			CollectableSearchExpression cse = new CollectableSearchExpression(TrueCondition.TRUE);
			cse.setSortingOrder(Collections.singletonList(new CollectableSorting(rechnungAttrUID, true)));
			mapParams.put("result", nucletDalProvider.getEntityObjectProcessor(orderUID).getIdsBySearchExpression(cse));
		}
		return mapParams;
	}

}
