//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.nuclos.api.context.SaveFlag;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.Mutable;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RecordGrantMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SFE;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.RefJoinCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.JdbcTransformerParams;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalJoin;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.common.dal.vo.IDalReadVO;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.IDataLanguageLocalizedEntityEntry;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common.format.FormattingUIDTransformer;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.ExtendedDatasourceServerUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.RecordGrantRight;
import org.nuclos.server.common.RecordGrantUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.dal.processor.ColumnToBeanVOMapping;
import org.nuclos.server.dal.processor.ColumnToFieldIdVOMapping;
import org.nuclos.server.dal.processor.ColumnToFieldVOMapping;
import org.nuclos.server.dal.processor.ColumnToLanguageFieldVOMapping;
import org.nuclos.server.dal.processor.DbValueConverter;
import org.nuclos.server.dal.processor.FromDbValueConversionParams;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IColumnWithMdToVOMapping;
import org.nuclos.server.dal.processor.IDbValueConverter;
import org.nuclos.server.dal.processor.ProcessorConfiguration;
import org.nuclos.server.dal.processor.ProcessorFactorySingleton;
import org.nuclos.server.dal.processor.jdbc.DalJoinUtils;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclos.JdbcExtendedEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.datasource.DatasourceMetaParser;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbBusinessException;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbId;
import org.nuclos.server.dblayer.incubator.DbExecutor.LimitedResultSetRunner;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.genericobject.searchcondition.CollectableGenericObjectSearchExpression;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.i18n.DataLanguageCache;
import org.nuclos.server.i18n.DataLanguageServerUtils;
import org.nuclos.server.livesearch.ejb3.LiveSearchFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.report.SchemaCache;
import org.nuclos.server.report.SchemaCache.NamedTableConstraintCacheEntry;
import org.nuclos.server.report.suppliers.AccessibleMandatorsSupplier;
import org.nuclos.server.report.suppliers.DataLanguageSupplier;
import org.nuclos.server.security.NuclosLocalServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

@Configurable
public class ExtendedEntityObjectProcessor<PK>
		extends EntityObjectProcessor<PK>
		implements JdbcExtendedEntityObjectProcessor<PK> {

	// static variables

	private static final Logger LOG = LoggerFactory.getLogger(ExtendedEntityObjectProcessor.class);

	private static final IDbValueConverter DB_VALUE_CONVERTER = new DbValueConverter();

	public static final String QUERY_HINT_ALL_LANGUAGES = "AllLanguages";

	private static final Set<String> QUERY_HINTS_DEFAULT = Collections.unmodifiableSet(Collections.emptySet());
	private static final Set<String> QUERY_HINTS_WITH_ALL_LANGUAGES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
			QUERY_HINT_ALL_LANGUAGES)));

	protected NuclosUserDetailsContextHolder userCtx;
	protected SessionUtils utils;
	private ParameterProvider parameterProvider;
	private MetaProvider metaProvider;
	private SchemaCache schemaCache;
	private DatasourceCache datasourceCache;
	private RecordGrantUtils grantUtils;
	private ExtendedDatasourceServerUtils datasourceServerUtils;
	private DatasourceMetaParser datasourceMetaParser;
	private LiveSearchFacadeLocal searchFacade;
	private TableAliasSingleton tableAliasSingleton;

	private final ThreadLocal<Boolean> ignoreRecordGrantsAndOthers = new ThreadLocal<>();

	private final ThreadLocal<String> transactionId = new ThreadLocal<>();

	public ExtendedEntityObjectProcessor(ProcessorConfiguration<EntityObjectVO<PK>, PK> config) {
		super(config);
	}

	@Autowired
	private void inject(final NuclosUserDetailsContextHolder userCtx,
						final SessionUtils utils,
						final ParameterProvider parameterProvider,
						final MetaProvider metaProvider,
						final SchemaCache schemaCache,
						final DatasourceCache datasourceCache,
						final RecordGrantUtils grantUtils,
						final ExtendedDatasourceServerUtils datasourceServerUtils,
						final DatasourceMetaParser datasourceMetaParser,
						final LiveSearchFacadeLocal searchFacade,
						final TableAliasSingleton tableAliasSingleton) {
		this.userCtx = userCtx;
		this.utils = utils;
		this.parameterProvider = parameterProvider;
		this.metaProvider = metaProvider;
		this.schemaCache = schemaCache;
		this.datasourceCache = datasourceCache;
		this.grantUtils = grantUtils;
		this.datasourceServerUtils = datasourceServerUtils;
		this.datasourceMetaParser = datasourceMetaParser;
		this.searchFacade = searchFacade;
		this.tableAliasSingleton = tableAliasSingleton;
	}

	//This is pessimistic, means default = false
	protected final boolean usePrimaryKeySubSelect() {
		if (parameterProvider != null) {
			return "true".equals(parameterProvider.getValue(ParameterProvider.USE_PK_SUBSELECT));
		}
		return false;
	}

	//This is optimistic, means default = true
	protected final boolean useReferenceSubSelect() {
		if (parameterProvider != null) {
			return !"false".equals(parameterProvider.getValue(ParameterProvider.USE_REF_SUBSELECT));
		}
		return true;
	}

	private boolean doSortCalculatedAttribues() {
		if (parameterProvider != null) {
			return "true".equalsIgnoreCase(parameterProvider.getValue(ParameterProvider.SORT_CALCULATED_ATTRIBUTES));
		}
		return false;
	}

	private Integer getQueryTimeout() {
		if (parameterProvider != null) {
			String value = parameterProvider.getValue(ParameterProvider.QUERY_TIMEOUT);
			if (value != null) {
				try {
					return Integer.parseInt(value);
				} catch (NumberFormatException nfe) {
					LOG.warn(nfe.getMessage(), nfe);
				}
			}
		}
		return null;
	}

	private int getIntiatorId() {
		return LangUtils.hashCode(utils.getCurrentUserName()) ^ getEntityUID().hashCode();
	}

	public void cancelRunningStatements() {
		dataBaseHelper.getDbAccess().cancelRunningStatements(getIntiatorId());
	}

	@Override
	protected MetaProvider getMetaProvider() {
		return metaProvider;
	}

	@Override
	protected IDbValueConverter getDbValueConverter() {
		return DB_VALUE_CONVERTER;
	}

	@Override
	protected final <T> DbQuery<T> createQueryImpl(Class<T> clazz) {
		DbQuery<T> query = super.createQueryImpl(clazz);
		query.setSortCalculatedAttributes(doSortCalculatedAttribues());
		query.setTimeout(getQueryTimeout());
		query.setInitiatorId(getIntiatorId());
		return query;
	}

	@Override
	protected <S> Transformer<Object[], EntityObjectVO<PK>> getResultTransformer(final JdbcTransformerParams params, final IColumnToVOMapping<Object, PK>... columns) {
		return new Transformer<Object[], EntityObjectVO<PK>>() {
			@Override
			public EntityObjectVO<PK> transform(Object[] result) {
				try {
					final EntityObjectVO<PK> dalVO = newDalVOInstance();
					for (int i = 0, n = columns.length; i < n; i++) {
						final IColumnToVOMapping<Object, PK> column = columns[i];
						Object value = result[i];
						if (column instanceof ColumnToLanguageFieldVOMapping) {
							ColumnToLanguageFieldVOMapping col = (ColumnToLanguageFieldVOMapping) column;
							if (col.isForDataMap()) {
								// not here ...
							} else {
								column.convertFromDbValueToDalField(dalVO, value, new FromDbValueConversionParams(params.includeThumbnailsOnly()),
										getMetaProvider(), DB_VALUE_CONVERTER);
							}
						} else {
							value = transformValue(result, i, column, columns);
							column.convertFromDbValueToDalField(dalVO, value, new FromDbValueConversionParams(params.includeThumbnailsOnly()),
									getMetaProvider(), DB_VALUE_CONVERTER);
						}
					}

					dalVO.processor(getProcessor());
					setDebugInfo(dalVO);
					return dalVO;
				} catch (Exception e) {
					throw new CommonFatalException(e);
				}
			}
		};
	}

	@Override
	protected DbQuery<Object[]> createQuery(final List<IColumnToVOMapping<?, PK>> columns,
											final Collection<IColumnWithMdToVOMapping<?, PK>> referenceColumnsAsSubselect,
											boolean bAddJoins,
											final Set<String> queryHints) {
		//joins will be added later from createOptimalQueryWithJoinsOrSubselects
		assert metaProvider != null;
		assert schemaCache != null;
		bAddJoins = false;
		return super.createQuery(columns, referenceColumnsAsSubselect, bAddJoins, queryHints);
	}

	private void addToIndexQueue(final IDalReadVO<PK> idrVO) {
		if (!MetaProvider.isDataEntity(getMetaData(), false)) {
			return;
		}

		synchronized (transactionId) {
			if (transactionId.get() == null) {
				String transActionNameShort = TransactionSynchronizationManager.getCurrentTransactionName().replaceFirst("org\\.nuclos\\.server\\.", "");
				transactionId.set(transActionNameShort + Thread.currentThread().hashCode());
				TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
					@Override
					public void afterCompletion(int status) {
						synchronized (transactionId) {
							searchFacade.transactionComplete(transactionId.get(), status == TransactionSynchronization.STATUS_COMMITTED, true);

							//In any case, remove this Transaction from ThreadLocal
							transactionId.set(null);
						}
					}
				});
			}

			searchFacade.sendVOintoQueue(idrVO, transactionId.get());
		}
	}

	/**
	 * @param dalVO
	 * @param force true: do not check logical unique constraints
	 *              false: check!
	 */
	@Override
	public Object insertOrUpdateWithOrWithoutForce(EntityObjectVO<PK> dalVO, boolean force, SaveFlag... saveFlags) {
		Object pk = super.insertOrUpdateWithOrWithoutForce(dalVO, force, saveFlags);

		insertOrUpdateLanguageData(dalVO);
		addToIndexQueue(dalVO);
		return pk;
	}

	@Override
	public void delete(Delete<PK> id) throws DbException {
		deleteLanguageData(id);
		super.delete(id);
		addToIndexQueue(id);
		if (eMeta.isOwner()) {
			this.unlock(id.getPrimaryKey());
		}
	}

	private void insertOrUpdateLanguageData(EntityObjectVO<PK> dalVO) {
		if (dalVO.getDataLanguageMap() == null) {
			return;
		}

		Map<UID, IDataLanguageLocalizedEntityEntry> dlMapField = dalVO.getDataLanguageMap().getLanguageMap();
		for (UID language : dlMapField.keySet()) {
			IDataLanguageLocalizedEntityEntry value = dlMapField.get(language);
			if (value != null) {
				if (value.getEntityEntryPrimaryKey() == null) {
					value.setEntityEntryPrimaryKey((Long) dalVO.getPrimaryKey());
				}

				if (value.getPrimaryKey() != null) {
					value.flagUpdate();
				}

				if (value.isFlagNew()) {
					value.setCreatedAt(InternalTimestamp.toInternalTimestamp(new Date()));
					value.setChangedAt(InternalTimestamp.toInternalTimestamp(new Date()));
					value.setCreatedBy("nuclos");
					value.setChangedBy("nuclos");
					value.setVersion(1);
					value.setPrimaryKey(dataBaseHelper.getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE));
				}

				NucletDalProvider.getInstance().getEntityObjectProcessor(value.getDalEntity()).insertOrUpdate((EntityObjectVO<Object>) value);
			}
		}
	}

	private void deleteLanguageData(Delete<PK> d) {
		if (d.getDalEntity() != null) {
			UID langDataUID = NucletEntityMeta.getEntityLanguageUID(d.getDalEntity());
			UID langDataRefFieldUID = DataLanguageServerUtils.extractForeignEntityReference(d.getDalEntity());

			if (getMetaProvider().hasEntity(langDataUID)) {
				IExtendedEntityObjectProcessor<PK> dlprocessor = NucletDalProvider.getInstance().getEntityObjectProcessor(langDataUID);
				CollectableSearchExpression cse = new CollectableSearchExpression(SearchConditionUtils.newComparison(langDataRefFieldUID, ComparisonOperator.EQUAL, d.getPrimaryKey()));

				for (PK foundDataLangEntry : dlprocessor.getIdsBySearchExpression(cse)) {
					dlprocessor.delete(new Delete<PK>(foundDataLangEntry, langDataUID));
				}
			}
		}
	}

	/* End BLOCK A: Insert/Update/Delete - Block */

	/* BLOCK B: Select - Block */

	/**
	 * Includes joins for (former) views.
	 */
	@Override
	public List<EntityObjectVO<PK>> getAll() {
		return getBySearchExpression(CollectableSearchExpression.TRUE_SEARCH_EXPR);
	}

	@Override
	public EntityObjectVO<PK> getByPrimaryKey(PK id) {
		return getByPrimaryKey(id, null);
	}

	/**
	 * Includes joins for (former) views.
	 */
	@Override
	public EntityObjectVO<PK> getByPrimaryKey(PK id, Collection<FieldMeta<?>> fields) {
		return CollectionUtils.getSingleIfExist(getBySearchExpressionAndPrimaryKeysImpl(CollectableSearchExpression.TRUE_SEARCH_EXPR, Collections.singletonList(id), fields));
	}

	/**
	 * Includes joins for (former) views.
	 */
	@Override
	public List<EntityObjectVO<PK>> getByPrimaryKeys(List<PK> pks) {
		return getBySearchExpressionAndPrimaryKeysImpl(CollectableSearchExpression.TRUE_SEARCH_EXPR, pks, null);
	}

	/**
	 * Includes joins for (former) views.
	 * <p>
	 * Attention: In contrast to getChunkBySearchExcpression here is no automatic addition for systemfields except "id"
	 * When using "fields" the caller is responsible for the any field
	 * //TODO: Find a consistent way for both methods and implement
	 */
	public List<EntityObjectVO<PK>> getBySearchExpressionAndPrimaryKeysImpl(CollectableSearchExpression clctexpr, List<PK> pks, Collection<FieldMeta<?>> fields) {
		final boolean bWithDefaultColumns = fields == null;
		List<IColumnToVOMapping<?, PK>> columns = new ArrayList<>(bWithDefaultColumns ? getDefaultColumns() : getColumnsByMeta(fields));

		if (fields != null) {
			Set<UID> sfields = new HashSet<>();
			for (FieldMeta<?> efmdv : fields) sfields.add(efmdv.getUID());
			columns = new ArrayList<>();
			IColumnToVOMapping<?, PK> primaryCol = getPrimaryKeyColumn();

			for (IColumnToVOMapping<?, PK> col : allColumns) {
				if (col == primaryCol || sfields.contains(col.getUID())) {
					columns.add(col);
				}
			}
		}

		final boolean bHasAllColumns = hasAllColumns(columns);
		DbQuery<Object[]> query = createOptimalQueryWithJoinsOrSubselects(clctexpr, columns, true, false, null);
		final Integer iCache = getCache(bWithDefaultColumns, columns);

		DbFrom<PK> from = (DbFrom<PK>) CollectionUtils.getFirst(query.getRoots());
		DbExpression<?> pkExpr = getPrimaryKeyColumn().getDbColumn(from, getMetaProvider());
		Transformer<Object[], EntityObjectVO<PK>> transformer = createResultTransformer(columns);

		final boolean isTrueExpression = CollectableSearchExpression.TRUE_SEARCH_EXPR.equals(clctexpr);
		List<EntityObjectVO<PK>> result = null;
		if (isTrueExpression) {
			result = super.getByPrimaryKeys(iCache, pks);
			if (result != null) {
				final Set<PK> cachedPks = result.parallelStream()
						.map(HasPrimaryKey::getPrimaryKey)
						.collect(Collectors.toSet());
				pks = pks.parallelStream()
						.filter(pk -> !cachedPks.contains(pk))
						.collect(Collectors.toList());
			}
		}

		if (result == null || result.isEmpty()) {
			result = new ArrayList<>(pks.size());
		} else {
			result = new ArrayList<>(result);
		}

		List<List<PK>> splittedList = CollectionUtils.splitEvery(pks, query.getBuilder().getInLimit());
		for (List<PK> pkSubList : splittedList) {
			query.addToWhereAsAnd(pkExpr.as(getPkType()).in(pkSubList));
			final List<EntityObjectVO<PK>> eosFromDb = dataBaseHelper.getDbAccess().executeQuery(query, transformer);
			if (isTrueExpression) {
				result.addAll(storeInCache(iCache, eosFromDb));
			} else {
				result.addAll(eosFromDb);
			}
			if (splittedList.indexOf(pkSubList) == splittedList.size() - 1) {
				break;
			}
			query = createOptimalQueryWithJoinsOrSubselects(clctexpr, columns, true, false, null);
		}

		result.parallelStream().forEach(eo -> eo.setComplete(bHasAllColumns));
		return result;
	}

	@Override
	public List<EntityObjectVO<PK>> getChunkBySearchExpressionImpl(CollectableSearchExpression clctexpr, ResultParams resultParams) {

		Collection<UID> fields = resultParams.getFields();
		List<IColumnToVOMapping<?, PK>> columns;

		if (resultParams.isIdOnlySelection()) {
			Collection<UID> colFields = new HashSet<>();
			colFields.add(SF.PK_ID.getUID(getEntityUID()));
			columns = getColumns(colFields);

		} else if (fields != null) {
			Collection<UID> colFields = new HashSet<>(fields);

			for (IColumnToVOMapping<?, PK> col : allColumns) {

				if (colFields.contains(col.getUID())) {
					continue;
				}

				// This adds the most basic system fields like PK, DATCREATED and INTVERSION, if there are not included yet
				if (col.getClass() == ColumnToBeanVOMapping.class) {
					colFields.add(col.getUID());
					continue;
				}

				FieldMeta<?> fMeta;
				if (col instanceof IColumnWithMdToVOMapping) {
					fMeta = ((IColumnWithMdToVOMapping<?, PK>) col).getMeta();
				} else if (col instanceof ColumnToFieldIdVOMapping) {
					fMeta = ((ColumnToFieldIdVOMapping<?, PK>) col).getMeta();
				} else {
					continue;
				}

				// This add any other system field and nuclosRowColor, if available (NUCLOS-6068 Part 1)
				if (fMeta.isSystemField() || fMeta.isNuclosRowColor()) {
					colFields.add(col.getUID());
				}
			}

			columns = getColumns(colFields);

		} else {
			columns = new ArrayList<>(getDefaultColumns());
		}

		return getChunk(columns, clctexpr, resultParams);
	}

	private List<EntityObjectVO<PK>> getChunk(List<IColumnToVOMapping<?, PK>> columns, CollectableSearchExpression clctexpr, ResultParams resultParams) {
		boolean bHasAllColumns = hasAllColumns(columns);

		DbQuery<Object[]> query = createOptimalQueryWithJoinsOrSubselects(clctexpr, columns, false, resultParams.isForceDistinct(), null);

		if (!E.isNuclosEntity(getEntityUID()) && usePrimaryKeySubSelect()) {
			// NUCLOS-5779 Gain performance by using a sub-query for fetching the primary keys
			DbQuery<PK> subquery = getIdQuery(clctexpr, ignoreRecordGrantsAndOthers.get());
			setLimitAndOffset(subquery, resultParams);

			DbFrom<?> from = query.getRoots().iterator().next();
			DbCondition subcondition = query.getBuilder().in(from.baseColumn(getPrimaryKeyColumn()), subquery);
			query.replaceWhere(subcondition);

		} else {
			setLimitAndOffset(query, resultParams);
		}

		Transformer<Object[], EntityObjectVO<PK>> transformer = createResultTransformer(new JdbcTransformerParams(resultParams.loadThumbnailsOnly()), columns);
		List<EntityObjectVO<PK>> result = new ArrayList<>();

		result.addAll(dataBaseHelper.getDbAccess().executeQuery(query, transformer));

		boolean resetNextFireTime = E.JOBCONTROLLER.getUID().equals(getEntityUID()) &&
				NuclosLocalServerSession.getInstance().isAutomaticJobStartOff();

		for (EntityObjectVO<PK> eo : result) {
			eo.setComplete(bHasAllColumns && !resultParams.loadThumbnailsOnly());
			// NUCLOS-6708
			if (resetNextFireTime && "Aktiviert".equals(eo.getFieldValue(E.JOBCONTROLLER.laststate))) {
				eo.setFieldValue(E.JOBCONTROLLER.nextfiretime, "AutoStart=OFF");
			}
		}

		return result;
	}

	private <T> void setLimitAndOffset(DbQuery<T> query, ResultParams resultParams) {
		Long lLimit = resultParams.getLimit();
		if (lLimit == null && utils.isCalledRemotely()) {
			lLimit = LimitedResultSetRunner.MAXFETCHSIZE.longValue();
		}

		if (lLimit != null) {
			query.limit(lLimit);
		}

		Long lOffset = resultParams.getOffset();
		if (lOffset != null && lOffset >= 0) {

			//NUCLOS-5302 for now only if there is no sorting.
			if (lOffset > 0 && resultParams.getAnchor().size() == 1) {
				CollectableIdCondition cic = (CollectableIdCondition) resultParams.getAnchor().get(0);
				String sPkCompare = getMetaData().isUidEntity() ? "t.STRUID > '$1'" : "t.INTID > $1";
				DbCondition cond = dataBaseHelper.getDbAccess().getQueryBuilder().plainCondition(sPkCompare.replace("$1", cic.getId().toString()));
				query.addToWhereAsAnd(cond);
			} else if (lOffset > 0 || dataBaseHelper.getDbAccess().withOffsetZero()) {
				query.offset(lOffset);
			}
		}
	}

	/**
	 * getSlimChunk, a method to get only the requested columns of a DB-Table
	 *
	 * @param clctexpr SearchExpression, same as else
	 * @param resultParams   ResultParams
	 * @return List of EntityObjectVO
	 */
	public List<EntityObjectVO<PK>> getSlimChunk(CollectableSearchExpression clctexpr, ResultParams resultParams) {
		return getChunk(getColumns(resultParams.getFields()), clctexpr, resultParams.forceDistinct());
	}

	/**
	 * Includes joins for (former) views.
	 */
	@Override
	public List<EntityObjectVO<PK>> getBySearchExpression(CollectableSearchExpression clctexpr) {
		return getBySearchExprResultParams(clctexpr, ResultParams.DEFAULT_RESULT_PARAMS);
	}

	/**
	 * Includes joins for (former) views.
	 */

	@Override
	public List<EntityObjectVO<PK>> getBySearchExprResultParams(CollectableSearchExpression clctexpr, ResultParams resultParams) {
		Collection<UID> fields = resultParams.getFields();
		final boolean bWithDefaultColumns = fields == null;
		List<IColumnToVOMapping<?, PK>> columns = bWithDefaultColumns ? getDefaultColumns() : getColumns(fields);

		if (fields != null && !columns.contains(getPrimaryKeyColumn())) {
			columns.add(getPrimaryKeyColumn());
		}
		boolean bHasAllColumns = hasAllColumns(columns);
		final Mutable<RecordGrantMeta> rgMetaIfAny = new Mutable<>();
		DbQuery<Object[]> query = createOptimalQueryWithJoinsOrSubselects(clctexpr, columns, true, resultParams.isForceDistinct(), rgMetaIfAny);
		final Integer iCache = rgMetaIfAny.getValue() == null ?
				getCache(bWithDefaultColumns, columns) : null; // no "all" caching when record grants are involved

		if (iCache != null &&
				CollectableSearchExpression.TRUE_SEARCH_EXPR.equals(clctexpr) &&
				ResultParams.DEFAULT_RESULT_PARAMS.equals(resultParams)) {
			List<EntityObjectVO<PK>> allCached = super.getAll(iCache);
			if (allCached != null) {
				allCached.parallelStream().forEach(eo -> eo.setComplete(bHasAllColumns));
				return new ArrayList<>(allCached);
			}
		}

		setLimitAndOffset(query, resultParams);

		if (query.hasLimit() && !query.hasOffset() && dataBaseHelper.getDbAccess().withOffsetZero()) {
			query.offset(0L);
		}
		List<EntityObjectVO<PK>> result = dataBaseHelper.getDbAccess().executeQuery(query, createResultTransformer(columns));

		if (iCache != null &&
				CollectableSearchExpression.TRUE_SEARCH_EXPR.equals(clctexpr) &&
				ResultParams.DEFAULT_RESULT_PARAMS.equals(resultParams)) {
			result = new ArrayList<>(storeAllInCache(iCache, result));
		}

		result.parallelStream().forEach(eo -> eo.setComplete(bHasAllColumns));
		return result;
	}

	/**
	 * Includes joins for (former) views (needed for sorting order).
	 */
	@Override
	public List<PK> getIdsBySearchExpression(CollectableSearchExpression clctexpr) {
		return dataBaseHelper.getDbAccess().executeQuery(getIdQuery(clctexpr, ignoreRecordGrantsAndOthers.get()));
	}

	@Override
	public Set<UsageCriteria> getUsageCriteriaBySearchExpression(final CollectableSearchExpression clctexpr) {
		return Collections.singleton(new UsageCriteria(getEntityUID(), null, null, null));
	}

	@Override
	public Long count(CollectableSearchExpression clctexpr) {
		CollectableSearchCondition condition = getSearchConditionWithDeletedAndVLP(clctexpr);
		final RecordGrantMeta rgMeta = getAssignedRecordgrantMeta(null);

		//NUCLOS-5241 Estimated count doesn't work for dynamic entities and charts and are not necessary either
		//NUCLOS-5947 Estimated count is now optional and disabled by default
		if (getMetaData().isDynamic() || getMetaData().isChart() ||
				!ServerParameterProvider.getInstance().isEnabled(ParameterProvider.KEY_ESTIMATE_COUNT)) {
			return countExact(rgMeta, condition);
		}

		boolean bNoCondition = condition == null && rgMeta == null;
		DbQuery<PK> query2Estimate = bNoCondition ? null : getSimpleIdQuery(rgMeta, condition);

		Long estimatedCount = dataBaseHelper.getDbAccess().estimateCount(eMeta, query2Estimate);

		if (estimatedCount != null) {
			return estimatedCount;
		}

		return countExact(rgMeta, condition);
	}

	public Long countWithLimit(final CollectableSearchExpression clctexpr, final Long limit) {
		CollectableSearchCondition condition = getSearchConditionWithDeletedAndVLP(clctexpr);
		return countWithLimit(condition, limit);
	}

	private Long countWithLimit(CollectableSearchCondition condition, final Long limit) {
		Map<String, Object> mpParameters = new HashMap<>();
		mpParameters.put("condition", condition);
		DbQuery<Long> query = createCountWithLimitQuery(getPrimaryKeyColumn(), limit, mpParameters);
		if (query == null) {
			query = createCountQuery(getPrimaryKeyColumn());
		}

		return dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
	}

	@Override
	protected void adjustCountWithLimitSubQuery(final DbQuery<PK> subQuery, final Map<String, Object> mpParameters) {

		addJoinsForRecordgrant(subQuery, getAssignedRecordgrantMeta(null));

		EOSearchExpressionUnparser unparser = createEOSearchExpressionUnparser(subQuery);
		unparser.unparseSearchCondition((CollectableSearchCondition) mpParameters.get("condition"));
	}

	private Long countExact(final RecordGrantMeta rgMeta, CollectableSearchCondition condition) {
		DbQuery<Long> query = createCountQuery(getPrimaryKeyColumn());
		addJoinsForRecordgrant(query, rgMeta);

		EOSearchExpressionUnparser unparser = createEOSearchExpressionUnparser(query);
		unparser.unparseSearchCondition(condition);

		return dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
	}

	private DbQuery<PK> getSimpleIdQuery(final RecordGrantMeta rgMeta, CollectableSearchCondition condition) {
		DbQuery<PK> query = createSingleColumnQuery(getPrimaryKeyColumn(), true);
		addJoinsForRecordgrant(query, rgMeta);

		EOSearchExpressionUnparser unparser = createEOSearchExpressionUnparser(query);
		unparser.unparseSearchCondition(condition);

		return query;
	}

	/* End BLOCK B: Select - Block */

	private DbQuery<PK> getIdQuery(CollectableSearchExpression clctexpr, Boolean bIgnoreRecordGrants) {
		DbQuery<PK> query = createSingleColumnQuery(getPrimaryKeyColumn(), false);
		if (clctexpr.getLimit() != null) {
			query.limit(clctexpr.getLimit());
		}

		RecordGrantMeta rgMeta = null;

		if (!Boolean.TRUE.equals(bIgnoreRecordGrants)) {

			List<IColumnToVOMapping<?, PK>> pkColumnsList = new ArrayList<>();
			pkColumnsList.add(getPrimaryKeyColumn());

			rgMeta = getAssignedRecordgrantMeta(pkColumnsList);

		}

		// we modify the search expr here, too.
		clctexpr = composeSearchCondition(clctexpr, new ArrayList<>());

		addSearchExpressionAndGrantsToQuery(clctexpr, query, rgMeta, allColumns);

		return query;
	}

	protected EOSearchExpressionUnparser createEOSearchExpressionUnparser(DbQuery<?> query) {
		EOSearchExpressionUnparser result = new EOSearchExpressionUnparser(query, eMeta, utils, metaProvider);
		if (!E.isNuclosEntity(getEntityUID())) {
			// This is a hack for ref join conditions on localized entities :-(
			// How can we handle this better?
			result.setDataLanguageCache(SpringApplicationContextHolder.getBean(DataLanguageCache.class));
		}
		return result;
	}

	protected boolean addSearchExpressionAndGrantsToQuery(CollectableSearchExpression clctexpr, DbQuery<?> query,
														RecordGrantMeta rgMeta, List<IColumnToVOMapping<?, PK>> columns) {

		final boolean bRecordGrantsJoined = addJoinsForRecordgrant(query, rgMeta);

		EOSearchExpressionUnparser unparser = createEOSearchExpressionUnparser(query);
		unparser.unparseSearchCondition(getSearchConditionWithDeletedAndVLP(clctexpr));

		if (columns != null) {
			for (Pair<UID, FieldMeta> column : unparser.getAdditionalColumns()) {
				columns.add(ProcessorFactorySingleton.createLanguageFieldMapping(
						SystemFields.BASE_ALIAS, column.getX(), column.getY(), false, false));
			}
		}

		final DbFrom<PK> from = query.getDbFrom();
		DbExpression<PK> primaryCol = (columns != null && columns.contains(getPrimaryKeyColumn())) || !query.isDistinct() ? getPrimaryKeyColumn()
				.getDbColumn(from, getMetaProvider()) : null;
		unparser.unparseSortingOrder(clctexpr.getSortingOrder(), primaryCol);
		return bRecordGrantsJoined;
	}

	private DbQuery<Object[]> createOptimalQueryWithJoinsOrSubselects(CollectableSearchExpression clctexpr,
																	  List<IColumnToVOMapping<?, PK>> columns,
																	  boolean bAllLanguages, boolean forceDistinct,
																	  Mutable<RecordGrantMeta> rgMetaIfAny) {
		final RecordGrantMeta rgMeta = getAssignedRecordgrantMeta(columns);
		Optional.ofNullable(rgMetaIfAny).ifPresent(m -> m.setValue(rgMeta));

		List<IColumnToVOMapping<?, PK>> referenceColumns = getReferenceColumns(columns);
		Collection<IColumnWithMdToVOMapping<?, PK>> subSelectableReferenceColumns = getSubselectableReferenceColumns(referenceColumns, clctexpr);

		DbQuery<Object[]> query = createQuery(columns, subSelectableReferenceColumns, false,
				bAllLanguages ? QUERY_HINTS_WITH_ALL_LANGUAGES : QUERY_HINTS_DEFAULT);
		if (forceDistinct) {
			query.distinct(true);
		}

		referenceColumns.removeAll(subSelectableReferenceColumns);

		List<DalJoin> joins = getJoinsForReferenceColumns(referenceColumns);
		// search expression will be modified
		clctexpr = composeSearchCondition(clctexpr, joins);

		addSearchExpressionAndGrantsToQuery(clctexpr, query, rgMeta, columns);

		return query;
	}

	private Collection<IColumnWithMdToVOMapping<?, PK>> getSubselectableReferenceColumns(
			List<IColumnToVOMapping<?, PK>> referenceColumns, CollectableSearchExpression clctexpr) {
		Collection<IColumnWithMdToVOMapping<?, PK>> ret = new HashSet<>();

		if (referenceColumns.isEmpty() || !useReferenceSubSelect()) {
			return ret;
		}

		Collection<UID> fieldsNotToBeSubselected = new HashSet<>();

		if (clctexpr != null && !CollectableSearchExpression.TRUE_SEARCH_EXPR.equals(clctexpr)) {
			//if (clctexpr.getValueListProviderDatasource() != null) {
			//	return ret; //TODO: Check whether to accept or not
			//}

			for (AtomicCollectableSearchCondition atomic : clctexpr.getAllAtomicConditions()) {
				boolean bSameEntity = atomic.compareEntity(getMetaData().getUID());

				if (bSameEntity && atomic.isComparandAnId()) {
					//An Id comparison within the same entity will not interfere with the stringified reference
					continue;
				}
				final FieldMeta<?> atomicFieldMeta = getMetaProvider().getEntityField(atomic.getFieldUID());
				if (SFE.OWNER.checkField(atomicFieldMeta.getEntity(), atomicFieldMeta.getUID())) {
					// No join for owner (first calculated ref field!)... use subselect...
					continue;
				}
				fieldsNotToBeSubselected.add(atomic.getFieldUID());
				//TODO: There are quite some kinds of comparisons where the field still can be subselected
			}

			for (CollectableSorting sorting : clctexpr.getSortingOrder()) {
				final FieldMeta<?> sortingFieldMeta = getMetaProvider().getEntityField(sorting.getField());
				if (SFE.OWNER.checkField(sortingFieldMeta.getEntity(), sortingFieldMeta.getUID())) {
					// No join for owner (first calculated ref field!)... use subselect...
					continue;
				}
				fieldsNotToBeSubselected.add(sorting.getField());
			}

		}

		for (IColumnToVOMapping<?, PK> column : referenceColumns) {

			if (column.getUID() != null && getMetaProvider().checkEntityField(column.getUID())) {
				final FieldMeta<?> fieldMeta = getMetaProvider().getEntityField(column.getUID());
				// Do not subselect localized columns!
				if (fieldMeta.getForeignEntity() != null && fieldMeta.getForeignEntityField() != null) {
					boolean bContinue = false;
					for (UID stringifiedFieldUID : RefValueExtractor.getAttributes(fieldMeta.getForeignEntityField())) {
						final FieldMeta<?> stringifiedFieldMeta = getMetaProvider().getEntityField(stringifiedFieldUID);
						if (stringifiedFieldMeta.isLocalized()) {
							bContinue = true;
							break;
						}
					}
					if (bContinue) {
						continue;
					}
				}
			}

			if (column instanceof IColumnWithMdToVOMapping) {
				IColumnWithMdToVOMapping<?, PK> col = (IColumnWithMdToVOMapping<?, PK>) column;

				if (fieldsNotToBeSubselected.contains(col.getUID())) {
					continue;
				}

				if (DalJoinUtils.getJoins(col.getMeta(), getMetaProvider()).size() == 1) {
					ret.add(col);
				}
			}
		}

		return ret;
	}

	private CollectableSearchCondition getSearchConditionWithDeletedAndVLP(CollectableSearchExpression clctexpr) {
		CollectableSearchCondition result = null;
		if (clctexpr.getValueListProviderDatasource() != null) {
			try {
				if (clctexpr.getValueListProviderDatasource().getValid())
					result = datasourceServerUtils.getConditionWithIdForInClause(
							clctexpr.getValueListProviderDatasource(),
							clctexpr.getValueListProviderDatasourceParameter(),
							datasourceCache, schemaCache, clctexpr.getMandator(),
							userCtx.getDataLocal());
			} catch (NuclosDatasourceException e) {
				throw new NuclosFatalException("datasource.error.valuelistprovider.invalid", e);
			}
		}

		if (clctexpr.getDynamicTasklistDatasource() != null) {
			try {
				if (clctexpr.getDynamicTasklistDatasource().getValid()) {
					CollectableSearchCondition condTasklist = datasourceServerUtils.getConditionWithIdForInClause(
							clctexpr.getDynamicTasklistDatasource(),
							new HashMap<>(),
							datasourceCache, schemaCache, clctexpr.getMandator(),
							userCtx.getDataLocal());
					result = result != null ? SearchConditionUtils.and(condTasklist, result) : condTasklist;
				}
			} catch (NuclosDatasourceException e) {
				throw new NuclosFatalException("datasource.error.dynamictasklist.invalid", e);
			}
		}

		UID entityUID = getEntityUID();

		if (clctexpr instanceof CollectableGenericObjectSearchExpression) {
			final CollectableGenericObjectSearchExpression clctGOexpr = (CollectableGenericObjectSearchExpression) clctexpr;
			if (!clctGOexpr.getSearchDeleted().equals(CollectableGenericObjectSearchExpression.SEARCH_BOTH)) {
				final Boolean bSearchDeleted = CollectableGenericObjectSearchExpression.SEARCH_DELETED == clctGOexpr.getSearchDeleted();
				final CollectableSearchCondition condSearchDeleted = org.nuclos.common.SearchConditionUtils.newComparison(SF.LOGICALDELETED.getUID(entityUID), ComparisonOperator.EQUAL, bSearchDeleted);
				result = result != null ? SearchConditionUtils.and(condSearchDeleted, result) : condSearchDeleted;
			}
		}

		NucletDalProvider dalProvider = null;
		try {
			dalProvider = NucletDalProvider.getInstance();
		} catch (Exception ex) {
			// too early .. ignore here
			// internal use of processor (e.g. building class sources)
		}
		if (dalProvider != null) {
			if (dalProvider.getAccessibleMandators() != null
					&& getMetaProvider().getEntity(entityUID).isMandator()
					&& SecurityCache.getInstance().isMandatorPresent()) {

				Set<UID> accessibleMandators = dalProvider.getAccessibleMandators();
				final CollectableSearchCondition condMandators;
				if (accessibleMandators.isEmpty()) {
					// could it be?
					condMandators = SearchConditionUtils.newIsNullCondition(SF.MANDATOR_UID.getMetaData(entityUID));
				} else {
					condMandators = new CollectableInIdCondition<>(
							SearchConditionUtils.newEntityField(SF.MANDATOR_UID.getMetaData(entityUID)),
							new ArrayList<>(accessibleMandators));
				}
				result = result != null ? SearchConditionUtils.and(condMandators, result) : condMandators;
			}
		}

		if (result == null) {
			result = clctexpr.getSearchCondition() == null ? null : setLocalizationSearchConditions(clctexpr.getSearchCondition());
		} else {
			result = clctexpr.getSearchCondition() == null ? result : SearchConditionUtils.and(setLocalizationSearchConditions(clctexpr.getSearchCondition()), result);
		}

		if (!E.isNuclosEntity(entityUID)) {
			if (!Boolean.TRUE.equals(ignoreRecordGrantsAndOthers.get())) {
				result = grantUtils.appendCompulsorySearchFilters(result, entityUID);
			}
		}

		return result;
	}

	private CollectableSearchCondition setLocalizationSearchConditions(CollectableSearchCondition sc) {
		AtomicCollectableSearchCondition localizableAtomicCondition = null;
		if (sc instanceof CompositeCollectableSearchCondition) {
			CompositeCollectableSearchCondition csc = (CompositeCollectableSearchCondition) sc;
			for (CollectableSearchCondition curCS : csc.getOperands()) {
				setLocalizationSearchConditions(curCS);
			}
		} else if (sc instanceof CollectableLikeCondition) {
			localizableAtomicCondition = (AtomicCollectableSearchCondition) sc;
		} else if (sc instanceof CollectableComparison) {
			localizableAtomicCondition = (AtomicCollectableSearchCondition) sc;
		}
		if (localizableAtomicCondition != null) {
			if (localizableAtomicCondition.getEntityField().isLocalized() && !localizableAtomicCondition.getEntityField().isCalculated()) {
				UID extractFieldUID = DataLanguageServerUtils.extractFieldUID(localizableAtomicCondition.getFieldUID());
				CollectableEntityField clctef = SearchConditionUtils.newEntityField(getMetaProvider().getEntityField(extractFieldUID));
				localizableAtomicCondition.setCollectableEntityField(clctef);
			}
		}
		return sc;
	}

	@Override
	protected List<IColumnToVOMapping<?, PK>> getColumns(final Collection<UID> fields) {
		final Set<UID> notFound = new HashSet<>(fields);
		List<IColumnToVOMapping<?, PK>> result =
				CollectionUtils.select(new ArrayList<>(allColumns), (IColumnToVOMapping<?, PK> column) -> {
					notFound.remove(column.getUID());
					return fields.contains(column.getUID());
				});
		for (UID nfUID : notFound) {
			if (CalcAttributeUtils.isCalcAttributeCustomization(nfUID)) {
				try {
					result.add(new ColumnToFieldVOMapping<>(SystemFields.BASE_ALIAS, getMetaProvider().getEntityField(nfUID), true, false));
				} catch (ClassNotFoundException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
		return result;
	}

	private CollectableSearchExpression composeSearchCondition(CollectableSearchExpression clctexpr, List<DalJoin> joins) {
		List<CollectableSorting> sortingOrder = addJoinsForSortingOrder(clctexpr, joins);
		CollectableSearchCondition searchCondition = clctexpr.getSearchCondition();

		if (joins != null && !joins.isEmpty()) {
			List<CollectableSearchCondition> refJoins = joins.stream()
					.map(RefJoinCondition::new)
					.collect(Collectors.toList());
			if (searchCondition != null) {
				refJoins.add(searchCondition);
			}

			searchCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND, refJoins);
		}

		//NUCLOS-4716: Make a copy, because a new sorting would alter the original SearchExpression		
		CollectableSearchExpression result;

		if (clctexpr instanceof CollectableGenericObjectSearchExpression) {
			// we need this. because others will check instance and add SEARCH_DELETED vlp or something like that.
			result = new CollectableGenericObjectSearchExpression(searchCondition, sortingOrder, ((CollectableGenericObjectSearchExpression) clctexpr).getSearchDeleted());

		} else {
			result = new CollectableSearchExpression(searchCondition, sortingOrder);

		}

		// reset vlps, mandator, etc.
		result.setIncludingSystemData(clctexpr.isIncludingSystemData());
		result.setValueListProviderDatasource(clctexpr.getValueListProviderDatasource());
		result.setValueListProviderDatasourceParameter(clctexpr.getValueListProviderDatasourceParameter());
		result.setDynamicTasklistDatasource(clctexpr.getDynamicTasklistDatasource());
		result.setMandator(clctexpr.getMandator());
		return result;
	}

	private List<IColumnToVOMapping<?, PK>> getReferenceColumns(final List<IColumnToVOMapping<?, PK>> columns) {
		final List<IColumnToVOMapping<?, PK>> result = new ArrayList<>();

		for (IColumnToVOMapping<?, PK> m : columns) {
			boolean add = false;
			final String f = m.getColumn();
			if (m instanceof IColumnWithMdToVOMapping) {
				final IColumnWithMdToVOMapping<?, PK> mapping = (IColumnWithMdToVOMapping<?, PK>) m;

				final FieldMeta<?> meta = mapping.getMeta();
				final UID fentity = LangUtils.firstNonNull(meta.getForeignEntity(), meta.getUnreferencedForeignEntity());
				if (fentity != null) {
					add = true;
				}
			}
			// ???
			else if (f.startsWith("STRVALUE_") || f.startsWith("INTVALUE_") || f.startsWith("OBJVALUE_")) {
				add = true;
			}

			if (add) {
				result.add(m);
			}
		}
		return result;
	}

	private List<CollectableSorting> addJoinsForSortingOrder(CollectableSearchExpression searchExpression, List<DalJoin> result) {
		final List<CollectableSorting> sortingOrder = searchExpression.getSortingOrder();

		if (sortingOrder == null) {
			return null;
		}

		// 
		// Also add join conditions needed by sorting order. (tp)
		final List<CollectableSorting> newSortingOrder = new ArrayList<>(sortingOrder.size());
		final UID baseEntity = eMeta.getUID();
		for (CollectableSorting cs : sortingOrder) {
			UID sortingEntity = cs.getEntity();

			// This could happen only for the base entity, for foreign entity fields ('STRVALUE_...'),
			// because sorting is only available for the base entity.

			// In case there is no entity given we can safely assume the it's the baseEntity
			if (sortingEntity == null || sortingEntity.equals(baseEntity)) {

				// not needed. otherwise we can not sort fields referencing baseEntity. references to entity it self... @see NUCLOS-1472
				//assert cs.getTableAlias().equals(SystemFields.BASE_ALIAS); 
				final FieldMeta<?> mdField = getMetaProvider().getEntityField(cs.getField());
				final UID fentity = mdField.getForeignEntity();
				if (fentity == null || mdField.isCalculated()) {
					// copy sorting order
					newSortingOrder.add(cs);
					continue;
				}

				final List<DalJoin> joins = DalJoinUtils.getJoins(mdField, getMetaProvider());
				final DalJoin join = joins.get(0);

				if (!result.contains(join)) {
					result.add(join);
				}

				// retrieve sorting order
				for (IFieldUIDRef ref : new ForeignEntityFieldUIDParser(mdField, getMetaProvider())) {
					if (ref.isUID()) {
						FieldMeta<?> refFieldPart = getMetaProvider().getEntityField(ref.getUID());
						if (refFieldPart.getForeignEntity() != null) {
							for (IFieldUIDRef ref2 : new ForeignEntityFieldUIDParser(refFieldPart, getMetaProvider())) {
								if (ref2.isUID()) {
									FieldMeta<?> refFieldPart2 = getMetaProvider().getEntityField(ref2.getUID());
									if (refFieldPart2.getForeignEntity() == null) {
										DalJoin join2 = null;
										for (int i = 1; i < joins.size(); i++) {
											DalJoin iJoin = joins.get(i);
											if (ref.getUID().equals(iJoin.getField().getUID())) {
												join2 = iJoin;
											}
										}
										if (join2 != null) {
											if (!result.contains(join2)) {
												result.add(join2);
											}
											newSortingOrder.add(new CollectableSorting(
													join2.getTableAliasRight(), refFieldPart.getForeignEntity(), false, ref2.getUID(), cs.isAscending()));
										}
									}
								}
							}
						} else {
							if (refFieldPart.isLocalized() && refFieldPart.getCalcFunction() == null) {
								EntityMeta<?> emRefFieldEntity = getMetaProvider().getEntity(refFieldPart.getEntity());
								EntityMeta<?> emRefEntityLang = getMetaProvider().getEntity(NucletEntityMeta.getEntityLanguageUID(refFieldPart.getEntity()));

								FieldMeta<?> refForeignRefLangMeta = emRefEntityLang.getField(
										DataLanguageServerUtils.extractForeignEntityReference(emRefFieldEntity.getUID()));
								FieldMeta<?> refForeignRefFieldLangMeta =
										emRefEntityLang.getField(DataLanguageServerUtils.extractFieldUID(ref.getUID()));

								newSortingOrder.add(new CollectableSorting(
										TableAliasSingleton.getInstance().getAlias(refForeignRefLangMeta, join.getTableAliasRight()), fentity, false, refForeignRefFieldLangMeta.getUID(), cs.isAscending()));

							} else {
								newSortingOrder.add(new CollectableSorting(
										join.getTableAliasRight(), fentity, false, ref.getUID(), cs.isAscending()));
							}
						}
					}
				}
			} else {
				// copy sorting order
				newSortingOrder.add(cs);
			}
		}

		return newSortingOrder;

	}

	private boolean addJoinsForRecordgrant(DbQuery<?> query, final RecordGrantMeta rgMeta) {
		if (rgMeta != null) {
			final DbFrom<?> from = query.getDbFrom();
			final String dbColumn = from.basePk().getColumn().getDbColumn();
			if (Long.class.equals(from.basePk().getColumn().getJavaClass())) {
				from.joinOnJoinedPk(rgMeta, JoinType.INNER, new DbField<Long>() {
					@Override
					public String getDbColumn() {
						return dbColumn;
					}

					@Override
					public Class<Long> getJavaClass() {
						return Long.class;
					}
				}, rgMeta.getEntityName());
			} else if (UID.class.equals(rgMeta.getPkClass())) {
				from.joinOnJoinedPk(rgMeta, JoinType.INNER, new DbField<UID>() {
					@Override
					public String getDbColumn() {
						return dbColumn;
					}

					@Override
					public Class<UID> getJavaClass() {
						return UID.class;
					}
				}, rgMeta.getEntityName());
			} else {
				throw new CommonFatalException("unknown pk class " + from.basePk().getColumn().getJavaClass());
			}

			return true;
		}
		return false;
	}

	/**
	 * @param clctexpr
	 * @return the distinct owner UIDs of the records. Non-owned records results in a UID_NULL
	 */
	@Override
	public Set<UID> getNuclosOwnerBySearchExpression(final CollectableSearchExpression clctexpr) {
		if (getMetaData().isOwner()) {
			final IColumnToVOMapping<?, PK> ownerColumn = allColumns.stream().filter(col -> col.getUID().equals(SFE.OWNER.getUID(getEntityUID()))).findFirst().orElse(null);
			if (ownerColumn == null) {
				throw new NuclosFatalException("Nuclos owner column not registered");
			}
			final DbQuery<UID> query = createQueryImpl(UID.class);
			final DbFrom<PK> from = query.from(getMetaData(), false);
			adjustFrom(from);

			DbExpression<?> ownerSelection = ownerColumn.getDbColumn(from, getMetaProvider());
			query.selectLiberate(ownerSelection);
			query.distinct(true);

			RecordGrantMeta rgMeta = getAssignedRecordgrantMeta(null);
			addSearchExpressionAndGrantsToQuery(clctexpr, query, rgMeta, null);

			/* Sometimes ordering throws an exception
				SQL query failed with org.h2.jdbc.JdbcSQLException: Column "T.INTID" must be in the GROUP BY list; SQL statement:
					SELECT t.STRUID_NUCLOSPROCESS, t.STRUID_NUCLOSSTATE FROM V594_ORDER AS t WHERE (t.INTID IN (?, ?) AND t.BLNNUCLOSDELETED = ?)
					GROUP BY t.STRUID_NUCLOSPROCESS, t.STRUID_NUCLOSSTATE ORDER BY t.INTID DESC [90016-196]:
			 */
			// ... and ordering is unnecessary:
			query.orderBy(Collections.emptyList());

			return dataBaseHelper.getDbAccess().executeQuery(query).stream()
					.map(ownerId -> ownerId != null ? ownerId : UID.UID_NULL)
					.collect(Collectors.toSet());
		}

		return Collections.emptySet();
	}

	@Override
	public Set<RecordGrantRight> getRecordGrantRightBySearchExpression(final CollectableSearchExpression clctexpr) {
		List<IColumnToVOMapping<?, PK>> columnsList = new ArrayList<>();
		RecordGrantMeta rgMeta = getAssignedRecordgrantMeta(columnsList);

		if (rgMeta != null && rgMeta.hasCanSomething() && !columnsList.isEmpty()) {
			final DbQuery<Object[]> query = createQueryImpl(Object[].class);
			final DbFrom<PK> from = query.from(getMetaData(), false);
			adjustFrom(from);

			final int iWriteIndex = IntStream.range(0, columnsList.size()).filter(i -> columnsList.get(i).getDbColumn().equals(SF.CANWRITE.getDbColumn())).findFirst().orElse(-1);
			final int iStateChangeIndex = IntStream.range(0, columnsList.size()).filter(i -> columnsList.get(i).getDbColumn().equals(SF.CANSTATECHANGE.getDbColumn())).findFirst().orElse(-1);
			final int iDeleteIndex = IntStream.range(0, columnsList.size()).filter(i -> columnsList.get(i).getDbColumn().equals(SF.CANDELETE.getDbColumn())).findFirst().orElse(-1);
			final List<DbExpression<?>> selection = columnsList.stream().map(column -> column.getDbColumn(from, getMetaProvider())).collect(Collectors.toList());
			query.multiselect(selection);
			query.groupBy(selection);

			addSearchExpressionAndGrantsToQuery(clctexpr, query, rgMeta, null);

			/* Sometimes ordering throws an exception
				SQL query failed with org.h2.jdbc.JdbcSQLException: Column "T.INTID" must be in the GROUP BY list; SQL statement:
					SELECT t.STRUID_NUCLOSPROCESS, t.STRUID_NUCLOSSTATE FROM V594_ORDER AS t WHERE (t.INTID IN (?, ?) AND t.BLNNUCLOSDELETED = ?)
					GROUP BY t.STRUID_NUCLOSPROCESS, t.STRUID_NUCLOSSTATE ORDER BY t.INTID DESC [90016-196]:
			 */
			// ... and ordering is unnecessary:
			query.orderBy(Collections.emptyList());

			return new HashSet<>(dataBaseHelper.getDbAccess().executeQuery(query, new Transformer<Object[], RecordGrantRight>() {
				@Override
				public RecordGrantRight transform(final Object[] object) {
					return new RecordGrantRight(
							iWriteIndex < 0 || RecordGrantUtils.isTrue(object[iWriteIndex]),
							iStateChangeIndex < 0 || RecordGrantUtils.isTrue(object[iStateChangeIndex]),
							iDeleteIndex < 0 || RecordGrantUtils.isTrue(object[iDeleteIndex]));
				}
			}));
		}
		return Collections.emptySet();
	}

	protected RecordGrantMeta getAssignedRecordgrantMeta(List<IColumnToVOMapping<?, PK>> columns) {
		//BMWFDM-696: Only Remote Calls should use the Record-Grants. Else ignore them.
		if (!utils.isCalledRemotely()) {
			return null;
		}
		if (Boolean.TRUE.equals(ignoreRecordGrantsAndOthers.get())) {
			return null;
		}

		RecordGrantMeta rgMeta = null;
		try {
			final Set<RecordGrantVO> recordGrant = grantUtils.getByEntity(getEntityUID());
			if (utils.getCurrentUserName() != null && !SecurityCache.getInstance().isSuperUser(utils.getCurrentUserName()) && !recordGrant.isEmpty()) {
				final RecordGrantVO rgVO = recordGrant.iterator().next();
				if (rgVO.getValid() && SecurityCache.getInstance().getAssignedRecordgrants(utils.getCurrentUserName(), userCtx.getMandatorUID()).contains(rgVO.getId())) {
					final String sql;
					List<String> rgColumns = null;
					try {
						sql = datasourceServerUtils.createSQL(rgVO, new HashMap<>(), null, datasourceCache, schemaCache, new DataLanguageSupplier(null), userCtx.getMandatorUID(), new AccessibleMandatorsSupplier(userCtx.getMandatorUID()));

						if (rgVO.getMeta() != null) {
							rgColumns = CollectionUtils.transform(datasourceMetaParser.parse(rgVO.getMeta(), "E.RECORDGRANT.meta").getColumns(), c -> c.getColumnName().toUpperCase());
						} else {
							rgColumns = DatasourceUtils.getColumnsWithoutQuotes(DatasourceUtils.getColumns(sql));
							rgColumns = CollectionUtils.transform(rgColumns, (String i) -> i.toUpperCase());

						}
					} catch (NuclosDatasourceException e) {
						throw new NuclosFatalException("unable to create sql from record grant datasource.", e);
					}
					// add fields for record grants here.
					boolean bHasCanSomething = false;

					if (columns != null) {
						final Class<IDalVO<PK>> eov = LangUtils.getGenericClass(EntityObjectVO.class);

						if (rgColumns.contains(SF.CANWRITE.getDbColumn())) {
							bHasCanSomething = true;

							final FieldMeta<?> efCanwrite = SF.CANWRITE.getMetaData(getEntityUID());
							columns.add(createBeanMapping(tableAliasSingleton.getRecordGrantAlias(), eov, efCanwrite, efCanwrite.getFieldName()));

						}
						if (rgColumns.contains(SF.CANSTATECHANGE.getDbColumn())) {
							bHasCanSomething = true;

							final FieldMeta<?> efCanstatechange = SF.CANSTATECHANGE.getMetaData(getEntityUID());
							columns.add(createBeanMapping(tableAliasSingleton.getRecordGrantAlias(), eov, efCanstatechange, efCanstatechange.getFieldName()));

						}
						if (rgColumns.contains(SF.CANDELETE.getDbColumn())) {
							bHasCanSomething = true;

							final FieldMeta<?> efCandelete = SF.CANDELETE.getMetaData(getEntityUID());
							columns.add(createBeanMapping(tableAliasSingleton.getRecordGrantAlias(), eov, efCandelete, efCandelete.getFieldName()));
						}
					}

					rgMeta = new RecordGrantMeta(sql, tableAliasSingleton.getRecordGrantAlias(), bHasCanSomething, getMetaProvider().getEntity(getEntityUID()).getPkClass());

				}
			}
		} catch (IllegalStateException e) {
			// we are to early. called from datasource cache. so retun null here.
			LOG.info("catching IllegalStateException getting recordgrants.", e);
		}
		return rgMeta;
	}

	/*
	 * we have to return our own BeanMapping here. cause "Object.class" will be mapped as "byte[].class".
	 * But we want to access resultset with rs.getObject() to get valid entries for canwrite, canstatechange or candelete.
	 */
	private static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, FieldMeta<S> ef, String methodRadical) {
		try {
			return (IColumnToVOMapping<S, PK>) createBeanMapping(alias, type, ef.getDbColumn(), methodRadical, ef.getUID(), Class.forName(ef.getDataType()), false);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}

	private static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, String column, String methodRadical, UID fieldUID, Class<S> dataType, boolean isReadonly) {
		final String xetterSuffix = methodRadical.substring(0, 1).toUpperCase() + methodRadical.substring(1);
		// final Class<?> clazz = getDalVOClass();
		Class<?> methodParameterType = dataType;
		if ("primaryKey".equals(methodRadical)) {
			methodParameterType = Object.class;
		}
		try {
			return new ColumnToBeanVOMapping<S, PK>(alias, column, fieldUID, type.getMethod("set" + xetterSuffix, methodParameterType),
					type.getMethod((DT_BOOLEAN.equals(dataType) ? "is" : "get") + xetterSuffix), dataType, isReadonly, false) {
				@Override
				public Class getJavaClass() {
					return Object.class;
				}
			};
		} catch (Exception e) {
			throw new CommonFatalException("On " + type + ": " + e);
		}
	}

	@Override
	protected DbException transformInsertOrUpdateException(DbException ex, final EntityObjectVO<PK> eo) {
		try {
			NamedTableConstraintCacheEntry constraint = schemaCache.getTableConstraintFromDbExceptionIfAny(ex);
			if (constraint != null) {
				SpringLocaleDelegate localDelegate = SpringLocaleDelegate.getInstance();
				String message;
				if (constraint.getType() == NamedTableConstraintCacheEntry.TYPE_UNIQUE_KEY) {
					String bo = localDelegate.getText(constraint.getEntityMeta().getLocaleResourceIdForLabel());
					StringBuffer fields = new StringBuffer();
					final FormattingUIDTransformer formatter = new FormattingUIDTransformer(true, getMetaProvider()) {
						@Override
						protected Object getValue(UID field) {
							return eo.getFieldValue(field);
						}
					};
					for (FieldMeta<?> fMeta : constraint.getFieldMetas()) {
						if (fields.length() > 0) {
							fields.append(", ");
						}
						fields.append(localDelegate.getText(fMeta.getLocaleResourceIdForLabel()));
						fields.append("=");
						Object val = formatter.transform(fMeta.getUID());
						val = val instanceof String && StringUtils.isNullOrEmpty((String)val) ?
								DbAccess.getMaskedId(eo.getFieldId(fMeta.getUID())) : val;
						fields.append(val);
					}

					//Das Businessobjekt {0} enthält bereits einen Datensatz mit {1}
					message = localDelegate.getMsg("db.unique.constraint.violation", bo, fields.toString());

				} else if (constraint.getType() == NamedTableConstraintCacheEntry.TYPE_FOREIGN_KEY) {
					String bo = localDelegate.getText(constraint.getEntityMeta().getLocaleResourceIdForLabel());
					FieldMeta<?> fMeta = constraint.getFieldMetas().get(0);
					String reffield = localDelegate.getText(fMeta.getLocaleResourceIdForLabel());
					String refbo = localDelegate.getText(getMetaProvider().getEntity(fMeta.getForeignEntity()).getLocaleResourceIdForLabel());
					String refValue = eo.getFieldValue(fMeta.getUID(), String.class);
					Object refId = eo.getFieldId(fMeta.getUID());
					if (refId == null) {
						if (eo.getFieldUid(fMeta.getUID()) != null) {
							refId = eo.getFieldUid(fMeta.getUID()).getString();
						}
					} else {
						refId = refId.toString();
					}

					//Der Datensatz \"{0}\" (ID={1}) im Businessobjekt {2} existiert nicht, wird aber verwendet: {3}.{4}
					message = localDelegate.getMsg("db.foreign.constraint.violation.during.insertupdate", refValue, refId, refbo, bo, reffield);

				} else {
					throw new IllegalArgumentException("Constraint type " + constraint.getType());
				}

				return new DbBusinessException(ex.getPk(), message, ex.getSqlCause());
			}
		} catch (Exception e1) {
			String message = "Exception during transformation of insert or update exception: " + e1.getMessage();
			LOG.warn(message);
			LOG.debug(message, e1);
		}
		return super.transformInsertOrUpdateException(ex, eo);
	}

	@Override
	protected DbException transformDeleteException(DbException ex, PK pk) {
		try {
			NamedTableConstraintCacheEntry constraint = schemaCache.getTableConstraintFromDbExceptionIfAny(ex);
			if (constraint != null) {
				String message;
				if (constraint.getType() == NamedTableConstraintCacheEntry.TYPE_FOREIGN_KEY) {
					SpringLocaleDelegate localDelegate = SpringLocaleDelegate.getInstance();
					String bo = localDelegate.getText(getMetaData().getLocaleResourceIdForLabel());
					String refbo = localDelegate.getText(constraint.getEntityMeta().getLocaleResourceIdForLabel());
					FieldMeta<?> fMeta = constraint.getFieldMetas().get(0);
					String reffield = localDelegate.getText(fMeta.getLocaleResourceIdForLabel());

					EntityObjectVO<PK> eo = getByPrimaryKey(pk);
					String refValue = RefValueExtractor.get(eo, fMeta.getUID(), null, true, getMetaProvider());

					//Der Datensatz \"{0}\" ({1}) kann nicht gelöscht werden, da noch Datensätze aus dem Businessobjekt {2} im Attribut {3} auf diesen verweisen.
					message = localDelegate.getMsg("db.foreign.constraint.violation.during.delete", refValue, bo, refbo, reffield);

				} else {
					throw new IllegalArgumentException("Constraint type " + constraint.getType());
				}

				return new DbBusinessException(ex.getPk(), message, ex.getSqlCause());
			}
		} catch (Exception e1) {
			String message = "Exception during transformation of delete exception: " + e1.getMessage();
			LOG.warn(message);
			LOG.debug(message, e1);
		}
		return super.transformDeleteException(ex, pk);
	}

	@Override
	public void setIgnoreRecordGrantsAndOthers(boolean ignore) {
		synchronized (ignoreRecordGrantsAndOthers) {
			if (ignore && !Boolean.TRUE.equals(ignoreRecordGrantsAndOthers.get())
					&& TransactionSynchronizationManager.isSynchronizationActive()) {
				// Security fallback only...
				TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
					@Override
					public void afterCompletion(int status) {
						synchronized (ignoreRecordGrantsAndOthers) {
							ignoreRecordGrantsAndOthers.set(false);
						}
					}
				});
			}
			ignoreRecordGrantsAndOthers.set(ignore);
		}
	}

	@Override
	public boolean getIgnoreRecordGrantsAndOthers() {
		return Boolean.TRUE.equals(ignoreRecordGrantsAndOthers.get());
	}

	@Override
	protected Transformer<Object[], EntityObjectVO<PK>> createResultTransformer(final JdbcTransformerParams params, List<IColumnToVOMapping<?, PK>> columns) {
		final Transformer<Object[], EntityObjectVO<PK>> jdbcTransformer = super.createResultTransformer(params, columns);
		if (eMeta.isOwner()) {
			return (Object[] o) -> {
				EntityObjectVO<PK> result = jdbcTransformer.transform(o);
				UID ownerUID = result.getFieldUid(SFE.OWNER_UID);
				if (ownerUID != null) {
					String userName = getCurrentUserName();
					final UID currentUserUID = userName != null ? SecurityCache.getInstance().getUserUid(userName) : null;
					if (!LangUtils.equal(ownerUID, currentUserUID)) {
						result.setCanWrite(false);
						result.setCanDelete(false);
						result.setCanStateChange(false);
					}
				}
				return result;
			};
		} else {
			return jdbcTransformer;
		}
	}

	public final String getCurrentUserName() {
		return utils.getCurrentUserName();
	}

	@Override
	public UID getOwner(PK id) {
		if (!eMeta.isOwner()) {
			throw new NuclosFatalException("Entity " + eMeta + " is not an owner enabled entity!");
		}
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<UID> q = builder.createQuery(UID.class);
		DbFrom<Long> from = q.from(E.ENTITYOBJECTLOCK);
		q.select(from.baseColumn(E.ENTITYOBJECTLOCK.owner));
		q.where(builder.equalValue(from.baseColumn(E.ENTITYOBJECTLOCK.objectId), (Long) id));
		q.addToWhereAsAnd(builder.equalValue(from.baseColumn(E.ENTITYOBJECTLOCK.entity), eMeta.getUID()));
		try {
			return dbAccess.executeQuerySingleResult(q);
		} catch (DbInvalidResultSizeException ex) {
			// no lock
			return null;
		}
	}

	@Override
	public void lock(PK id, UID userUID) {
		if (!eMeta.isOwner()) {
			throw new NuclosFatalException("Entity " + eMeta + " is not an owner enabled entity!");
		}
		this.unlock(id);
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbMap values = new DbMap();
		values.put(SF.PK_ID, new DbId()); //DalUtils.getNextId());
		values.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		values.put(SF.CREATEDBY, getCurrentUserName());
		values.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		values.put(SF.CHANGEDBY, getCurrentUserName());
		values.put(SF.VERSION, 1);
		values.put(E.ENTITYOBJECTLOCK.owner, userUID);
		values.putUnsafe(E.ENTITYOBJECTLOCK.objectId, id);
		values.put(E.ENTITYOBJECTLOCK.entity, eMeta.getUID());
		DbInsertStatement<?> insert = new DbInsertStatement<>(E.ENTITYOBJECTLOCK, values);
		dbAccess.execute(insert);
	}

	@Override
	public void unlock(PK id) {
		if (!eMeta.isOwner()) {
			throw new NuclosFatalException("Entity " + eMeta + " is not an owner enabled entity!");
		}
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbMap conditions = new DbMap();
		conditions.putUnsafe(E.ENTITYOBJECTLOCK.objectId, id);
		conditions.put(E.ENTITYOBJECTLOCK.entity, eMeta.getUID());
		DbDeleteStatement<?> delete = new DbDeleteStatement<>(E.ENTITYOBJECTLOCK, conditions);
		dbAccess.execute(delete);
	}

	@Override
	protected void adjustFrom(final DbFrom<PK> from) {
		from.setUsername(utils.getCurrentUserName());

		if (userCtx != null) {
			if (userCtx.getDataLocal() != null) {
				from.setLocale(userCtx.getDataLocal().getString());
			}
			if (SecurityCache.getInstance() != null && SecurityCache.getInstance().isMandatorPresent()) {
				from.setMandator(
						SecurityCache.getInstance().getAccessibleMandators(userCtx.getMandatorUID())
								.stream()
								.map(UID::getString)
								.collect(Collectors.joining("', '"))
				);
			}
		}
	}

	@Override
	protected boolean isClusterWideNotification() {
		return super.isClusterWideNotification() ||
				MasterDataRestFqnCache.isEntityCached(getEntityUID());
	}
}
