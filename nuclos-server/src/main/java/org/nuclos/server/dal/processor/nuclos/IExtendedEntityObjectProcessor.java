//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.nuclos;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.dal.specification.IDalCountSpecification;
import org.nuclos.server.dal.specification.IDalSearchExpressionSpecification;

public interface IExtendedEntityObjectProcessor<PK> extends
		IEntityObjectProcessor<PK>,
	IDalSearchExpressionSpecification<EntityObjectVO<PK>, PK>,
	IDalCountSpecification {

	UID getOwner(PK id);
	void lock(PK id, UID userUID);
	void unlock(PK id);

	/**
	 * Ignore record grants and others (compulsory search filters), but only for current thread and transaction!
	 * @param ignore
	 */
	public void setIgnoreRecordGrantsAndOthers(boolean ignore);
	public boolean getIgnoreRecordGrantsAndOthers();
	
	void cancelRunningStatements();

}
