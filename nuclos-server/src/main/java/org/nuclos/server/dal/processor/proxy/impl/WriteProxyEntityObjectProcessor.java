package org.nuclos.server.dal.processor.proxy.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.nuclos.api.context.InputRequiredException;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.RecordGrantRight;
import org.nuclos.server.dal.processor.AbstractDalProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.ExtendedEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclos.IEOChunkableProcessor;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.eventsupport.ejb3.ProxyContext;
import org.nuclos.server.eventsupport.ejb3.ProxyContext.Type;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.ruleengine.NuclosInputRequiredException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 *
 */
public class WriteProxyEntityObjectProcessor<PK> extends AbstractDalProcessor<EntityObjectVO<PK>, PK>
		implements IExtendedEntityObjectProcessor<PK>, IEOChunkableProcessor<PK> {

	private static final Logger LOG = LoggerFactory.getLogger(ProxyEntityObjectProcessor.class);

	private EventSupportFacadeLocal _evsuFacade;

	private final ExtendedEntityObjectProcessor<PK> delegate;

	public WriteProxyEntityObjectProcessor(ExtendedEntityObjectProcessor<PK> delegate) {
		super(delegate.getEntityUID(), delegate.getDalType(), delegate.getPkType());
		this.delegate = delegate;
	}

	public EventSupportFacadeLocal getEventSupportFacade() {
		if (_evsuFacade == null) {
			_evsuFacade = SpringApplicationContextHolder.getBean(EventSupportFacadeLocal.class);
		}
		return _evsuFacade;
	}

	/**
	 * 
	 * @param pc
	 * @return proxy object (for rollback)
	 */
	private Object executeCall(final ProxyContext pc) {
		try {
			return getEventSupportFacade().executeProxyCall(pc);
		} catch (NuclosBusinessRuleException | NuclosCompileException e) {
			if (e.getCause() != null && e.getCause() instanceof BusinessException && e.getCause().getCause() != null && e.getCause().getCause() instanceof InputRequiredException) {
				throw (InputRequiredException) e.getCause().getCause();
			}
			if (e.getCause() != null && e.getCause() instanceof BusinessException && e.getCause().getCause() != null && e.getCause().getCause() instanceof NuclosInputRequiredException) {
				throw (NuclosInputRequiredException) e.getCause().getCause();
			}
			String msg = e.getCause() != null ? e.getCause().toString() : e.getMessage();
			String error = String.format("Proxy call " + pc.getType() + " for entity %s had an error: %s", getEntityUID(), msg);
			LOG.error(error, e);
			throw new NuclosFatalException(error, e);
		}
	}

	@Override
	public Object insertOrUpdate(EntityObjectVO<PK> dalVO)  throws DbException{
		final ProxyContext pc = new ProxyContext(getEntityUID(), getPkType(), Type.INSERT_OR_UPDATE);
		pc.setParamObject(dalVO);
		final boolean flagNew = dalVO.isFlagNew();
		final boolean flagUpdated = dalVO.isFlagUpdated();
		Object proxy = executeCall(pc);
		if (flagNew) {
			notifyInsert(dalVO, true);
		}
		if (flagUpdated) {
			notifyUpdate(dalVO, true);
		}
		registerTransactionSynchronization(getEntityUID(), proxy);
		return pc.getPKResult();
	}

	@Override
	public void checkLogicalUniqueConstraint(final EntityObjectVO<PK> dalVO) throws DbException {

	}

	@Override
	public void delete(Delete del) throws DbException {
		final ProxyContext pc = new ProxyContext(getEntityUID(), getPkType(), Type.DELETE);
		pc.setParamId(del.getPrimaryKey());
		Object proxy = executeCall(pc);
		notifyDelete(del, true);
		registerTransactionSynchronization(getEntityUID(), proxy);
	}

	private void registerTransactionSynchronization(final UID entity, final Object proxy) {
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCompletion(int status) {
				final String sStatus = (status == TransactionSynchronization.STATUS_COMMITTED) ? "Commit" : "Rollback";
				try {
					if (TransactionSynchronization.STATUS_COMMITTED == status) {
						getEventSupportFacade().finishProxyCall(entity, proxy, true);
					} else {
						getEventSupportFacade().finishProxyCall(entity, proxy, false);
					}
				} catch (NuclosBusinessRuleException e) {
					LOG.error("{} of proxy {} failed [entity={}]", sStatus, proxy, entity, e);
				} catch (NuclosCompileException e) {
					LOG.error("{} of proxy {} failed [entity={}]", sStatus, proxy, entity, e);
				}
			}
		});
	}

	@Override
	public UID getOwner(final PK id) {
		return delegate.getOwner(id);
	}

	@Override
	public void lock(final PK id, final UID userUID) {
		delegate.lock(id, userUID);
	}

	@Override
	public void unlock(final PK id) {
		delegate.unlock(id);
	}

	@Override
	public void setIgnoreRecordGrantsAndOthers(boolean ignore) {
		delegate.setIgnoreRecordGrantsAndOthers(ignore);
	}

	@Override
	public boolean getIgnoreRecordGrantsAndOthers() {
		return delegate.getIgnoreRecordGrantsAndOthers();
	}

	@Override
	public void setThinReadEnabled(boolean enabled) {
		delegate.setThinReadEnabled(enabled);
	}

	@Override
	public boolean isThinReadEnabled() {
		return delegate.isThinReadEnabled();
	}

	@Override
	public void cancelRunningStatements() {
		delegate.cancelRunningStatements();
	}

	@Override
	public void destroy() throws Exception {
		delegate.destroy();
	}

	@Override
	public Long count(final CollectableSearchExpression clctexpr) {
		return delegate.count(clctexpr);
	}

	@Override
	public List<EntityObjectVO<PK>> getAll() {
		return delegate.getAll();
	}

	@Override
	public List<PK> getAllIds() {
		return delegate.getAllIds();
	}

	@Override
	public EntityObjectVO<PK> getByPrimaryKey(final PK id) {
		return delegate.getByPrimaryKey(id);
	}

	@Override
	public EntityObjectVO<PK> getByPrimaryKey(final PK id, final Collection<FieldMeta<?>> fields) {
		return delegate.getByPrimaryKey(id, fields);
	}

	@Override
	public List<EntityObjectVO<PK>> getByPrimaryKeys(final List<PK> ids) {
		return delegate.getByPrimaryKeys(ids);
	}

	@Override
	public List<EntityObjectVO<PK>> getBySearchExpression(final CollectableSearchExpression clctexpr) {
		return delegate.getBySearchExpression(clctexpr);
	}

	@Override
	public List<EntityObjectVO<PK>> getBySearchExprResultParams(final CollectableSearchExpression clctexpr, final ResultParams resultParams) {
		return delegate.getBySearchExprResultParams(clctexpr, resultParams);
	}

	@Override
	public List<PK> getIdsBySearchExpression(final CollectableSearchExpression clctexpr) {
		return delegate.getIdsBySearchExpression(clctexpr);
	}

	@Override
	public Set<UsageCriteria> getUsageCriteriaBySearchExpression(final CollectableSearchExpression clctexpr) {
		return delegate.getUsageCriteriaBySearchExpression(clctexpr);
	}

	@Override
	public Set<RecordGrantRight> getRecordGrantRightBySearchExpression(final CollectableSearchExpression clctexpr) {
		return delegate.getRecordGrantRightBySearchExpression(clctexpr);
	}

	@Override
	public Set<UID> getNuclosOwnerBySearchExpression(final CollectableSearchExpression clctexpr) {
		return delegate.getNuclosOwnerBySearchExpression(clctexpr);
	}

	@Override
	public Integer getVersion(final PK pk) {
		return delegate.getVersion(pk);
	}

	@Override
	public List<EntityObjectVO<PK>> getChunkBySearchExpressionImpl(final CollectableSearchExpression clctexpr, final ResultParams resultParams) {
		return delegate.getChunkBySearchExpressionImpl(clctexpr, resultParams);
	}
}
