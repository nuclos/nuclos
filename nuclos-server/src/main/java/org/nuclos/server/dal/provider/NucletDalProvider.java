//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.provider;

import java.time.temporal.Temporal;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.apache.commons.math3.util.ArithmeticUtils;
import org.nuclos.common.AbstractDisposableRegistrationBean;
import org.nuclos.common.ByteUnit;
import org.nuclos.common.CommonConsole;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.cluster.ClusterMessageType;
import org.nuclos.server.cluster.RigidClusterHelper;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.processor.DalChangeNotification;
import org.nuclos.server.dal.processor.ProcessorFactorySingleton;
import org.nuclos.server.dal.processor.jdbc.impl.EOGenericObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.WorkspaceProcessor;
import org.nuclos.server.dal.processor.nuclos.IEOGenericObjectProcessor;
import org.nuclos.server.dal.processor.nuclos.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclos.IWorkspaceProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * TODO Replace with pure Spring solution.
 */
@Component
public class NucletDalProvider extends AbstractDisposableRegistrationBean {

	private static final Logger LOG = LoggerFactory.getLogger(NucletDalProvider.class);

	private static final ConcurrentMap<UID, Object> BLOCKER = new ConcurrentHashMap<>();

	private static final ObjectMapper objectMapper = new ObjectMapper();

	private enum ProcessorFactoryType {
		ENTITY_OBJECT,
		DYNAMIC,
		CHART,
		DYNAMICTASKLIST,
		REPORT,
		REPORTOUTPUT
	}

	/**
	 * Singleton der auch in einer MultiThreading-Umgebung Threadsafe ist...
	 */
	private static NucletDalProvider INSTANCE;

	// Spring injection

	@Autowired
	private CacheManager cacheManager;

	private final EOGenericObjectProcessor eoGenericObjectProcessor;

	private final WorkspaceProcessor workspaceProcessor;

	private final ProcessorFactorySingleton processorFac;


	// end of Spring injection

	private ThreadLocal<Stack<Set<UID>>> accessibleMandators = new ThreadLocal<Stack<Set<UID>>>() {
		@Override
		protected Stack<Set<UID>> initialValue() {
			return new Stack<Set<UID>>();
		}
	};

	private ThreadLocal<Stack<UID>> accessibleMandatorsOrigin = new ThreadLocal<Stack<UID>>() {
		@Override
		protected Stack<UID> initialValue() {
			return new Stack<UID>();
		}
	};

	private ThreadLocal<Stack<UID>> runningJob = new ThreadLocal<Stack<UID>>() {
		@Override
		protected Stack<UID> initialValue() {
			return new Stack<UID>();
		}
	};

	private final ThreadLocal<TransactionSynchronization> transactionSyncForInvalidation = new ThreadLocal<>();

	private final MetaProvider metaProv;

	private final NuclosDalProvider nuclosDalProvider;

	private Collection<EntityMeta<?>> entityMetas;


	public NucletDalProvider(
			final MetaProvider metaProv,
			final NuclosDalProvider nuclosDalProvider,
			final ProcessorFactorySingleton processorFac,
			final EOGenericObjectProcessor eoGenericObjectProcessor,
			final WorkspaceProcessor workspaceProcessor
	) {
		this.metaProv = metaProv;
		this.nuclosDalProvider = nuclosDalProvider;
		this.processorFac = processorFac;
		this.eoGenericObjectProcessor = eoGenericObjectProcessor;
		this.workspaceProcessor = workspaceProcessor;
		INSTANCE = this;
	}

	@PostConstruct
	public void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		workspaceProcessor.setNucletDalProvider(this);
		createSyncFlowableWithDropStrategyAndRegister(metaProv.observeEntityMetaRefreshsAfterCommit(), this::forceInvalidateCaches);
	}

	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static NucletDalProvider getInstance() {
		if (INSTANCE == null || INSTANCE.eoGenericObjectProcessor == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	public Collection<EntityMeta<?>> getAllEntities() {
		return Collections.unmodifiableCollection(getAllEntityMetasMap().values().stream().map(Pair::getX).collect(Collectors.toList()));
	}

	//@Cacheable annotation does not work during startup. Workaround via cacheManager...
	//@Cacheable(value = "nucletDalProviderAllEntityMetas")
	public Map<UID, Pair<EntityMeta<?>, ProcessorFactoryType>> getAllEntityMetasMap() {
		Map<UID, Pair<EntityMeta<?>, ProcessorFactoryType>> result = null;
		Cache.ValueWrapper valueWrapper = cacheManager.getCache("nucletDalProviderAllEntityMetas").get("");
		if (valueWrapper != null) {
			result = (Map<UID, Pair<EntityMeta<?>, ProcessorFactoryType>>) valueWrapper.get();
		}
		if (result == null) {
			result = createAllEntityMetasMap();
			cacheManager.getCache("nucletDalProviderAllEntityMetas").put("", result);
		}
		return result;
	}

	private Map<UID, Pair<EntityMeta<?>, ProcessorFactoryType>> createAllEntityMetasMap() {
		final Map<UID, Pair<EntityMeta<?>, ProcessorFactoryType>> result = new HashMap<>();

		Optional.ofNullable(entityMetas).orElseGet(metaProv::getAllEntities)
				.forEach(entityMeta -> {
			if (!entityMeta.isGeneric()) {
				final ProcessorFactoryType procType;
				if (entityMeta.isDynamic()) {
					procType = ProcessorFactoryType.DYNAMIC;
				} else if (entityMeta.isChart()) {
					procType = ProcessorFactoryType.CHART;
				} else if (entityMeta.isDynamicTasklist()) {
					procType = ProcessorFactoryType.DYNAMICTASKLIST;
				} else if (E.REPORT.getUID().equals(entityMeta.getUID())) {
					procType = ProcessorFactoryType.REPORT;
				} else if (E.REPORTOUTPUT.getUID().equals(entityMeta.getUID())) {
					procType = ProcessorFactoryType.REPORTOUTPUT;
				} else {
					procType = ProcessorFactoryType.ENTITY_OBJECT;
				}
				result.put(entityMeta.getUID(), Pair.makePair(entityMeta, procType));
			}
		});

		return Collections.unmodifiableMap(result);
	}

	public <PK> IExtendedEntityObjectProcessor<PK> getEntityObjectProcessor(@NotNull EntityMeta<PK> entity) {
		return getEntityObjectProcessor(entity.getUID());
	}

	//@Cacheable annotation does not work during startup. Workaround via cacheManager...
	//@Cacheable(value = "nucletDalProviderEntityObjectProcessor", key = "#entity.getString()")
	public <PK> IExtendedEntityObjectProcessor<PK> getEntityObjectProcessor(@NotNull UID entity) {
		IExtendedEntityObjectProcessor<PK> result = RigidUtils.uncheckedCast(Optional.ofNullable(cacheManager
						.getCache("nucletDalProviderEntityObjectProcessor")
						.get(entity.getString())).map(Cache.ValueWrapper::get)
						.orElse(null));
		if (result == null) {
			synchronized (syncObject(entity)) { // only sync creation and check if created already...
				result = RigidUtils.uncheckedCast(Optional.ofNullable(cacheManager
								.getCache("nucletDalProviderEntityObjectProcessor")
								.get(entity.getString())).map(Cache.ValueWrapper::get)
								.orElse(null));
				if (result == null) {
					result = createEntityObjectProcessor(entity);
					cacheManager.getCache("nucletDalProviderEntityObjectProcessor").put(entity.getString(), result);
					freeMemoryLinking(result);
				}
			}
		}
		return result;
	}

	private <PK> void freeMemoryLinking(IExtendedEntityObjectProcessor<PK> createdProc) {
		if (createdProc == null) {
			return;
		}
		if (E.FORM.getUID().equals(createdProc.getEntityUID())) {
			observeChangesAndFreeMemoryOfLinkedEntity(createdProc, E.REPORT);
		} else if (E.REPORT.getUID().equals(createdProc.getEntityUID())) {
			observeChangesAndFreeMemoryOfLinkedEntity(createdProc, E.FORM);
		} else if (E.FORMOUTPUT.getUID().equals(createdProc.getEntityUID())) {
			observeChangesAndFreeMemoryOfLinkedEntity(createdProc, E.REPORTOUTPUT);
		} else if (E.REPORTOUTPUT.getUID().equals(createdProc.getEntityUID())) {
			observeChangesAndFreeMemoryOfLinkedEntity(createdProc, E.FORMOUTPUT);
		} else if (E.RESOURCE.getUID().equals(createdProc.getEntityUID())) {
			observeChangesAndFreeMemoryOfLinkedEntity(createdProc, E.REPORTOUTPUT);
			observeChangesAndFreeMemoryOfLinkedEntity(createdProc, E.FORMOUTPUT);
		}
	}

	private <PK> void observeChangesAndFreeMemoryOfLinkedEntity(IExtendedEntityObjectProcessor<PK> createdProc, EntityMeta<?> linkedEntityMeta) {
		((AbstractDisposableRegistrationBean) createdProc).createSyncFlowableWithoutStrategyAndRegister(
				createdProc.observeChangesNotifyAfterCommit(true), notification -> {
					getEntityObjectProcessor(linkedEntityMeta).freeMemory(true);
					if (RigidClusterHelper.isClusterEnabled()) {
						try {
							RigidClusterHelper.sendMessage(
									ClusterMessageType.DAL_CHANGE_NOTIFICATION,
									objectMapper.writeValueAsString(DalChangeNotification.withUnidentifiedDeletes(linkedEntityMeta.getUID(), linkedEntityMeta.isUidEntity()).compressForClusterMessage()));
						} catch (Exception e) {
							LOG.error(String.format("Cluster message could not be send for entity %s!", linkedEntityMeta.getEntityName()), e);
						}
					}
				});
	}

	private <PK> IExtendedEntityObjectProcessor<PK> createEntityObjectProcessor(@NotNull UID entity) {
		Pair<EntityMeta<?>, ProcessorFactoryType> metaProcessorFactoryTypePair = getAllEntityMetasMap().get(entity);
		if (metaProcessorFactoryTypePair == null) {
			throw new CommonFatalException(String.format("Entity for UID \"%s\" not found", entity));
		}
		try {
			EntityMeta<PK> eMeta = RigidUtils.uncheckedCast(metaProcessorFactoryTypePair.getX());
			switch (metaProcessorFactoryTypePair.getY()) {
				case ENTITY_OBJECT:
					return processorFac.newEntityObjectProcessor(eMeta, eMeta.getFields(), true);
				case DYNAMIC:
					return processorFac.newDynamicEntityObjectProcessor(eMeta, eMeta.getFields());
				case CHART:
					return processorFac.newChartEntityObjectProcessor(eMeta, eMeta.getFields());
				case DYNAMICTASKLIST:
					if (eMeta.isProxy()) {
						return processorFac.newRuleBasedDynamicTasklistEntityObjectProcessor(eMeta, eMeta.getFields());
					} else {
						return processorFac.newDynamicTasklistEntityObjectProcessor(eMeta, eMeta.getFields());
					}
				default:
					return processorFac.newEntityObjectProcessor(eMeta, eMeta.getFields(), true);
			}
		} catch (Exception ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	public IEOGenericObjectProcessor getEOGenericObjectProcessor() {
		return eoGenericObjectProcessor;
	}
	
	public IWorkspaceProcessor getWorkspaceProcessor() {
		return workspaceProcessor;
	}

	public <PK> void invalidate() {
		if (TransactionSynchronizationManager.isSynchronizationActive()) {
			TransactionSynchronization ts = transactionSyncForInvalidation.get();
			if (ts == null) {
				ts = new TransactionSynchronizationAdapter() {
					@Override
					public void afterCompletion(final int status) {
						try {
							if (status == TransactionSynchronization.STATUS_COMMITTED) {
								forceInvalidateCaches(null);
							}
						} catch (Exception ex) {
							LOG.error(String.format("Error during %s.invalidate after commit: %s", NucletDalProvider.class.getCanonicalName(), ex.getMessage()), ex);
						} finally {
							transactionSyncForInvalidation.remove();
						}
					}
				};
			}
			TransactionSynchronizationManager.registerSynchronization(ts);
		} else {
			forceInvalidateCaches(null);
		}
	}

	@Caching(evict = {
			@CacheEvict(value = "nucletDalProviderAllEntityMetas", allEntries = true),
			@CacheEvict(value = "nucletDalProviderEntityObjectProcessor", allEntries = true)
	})
	public void forceInvalidateCaches(final Collection<EntityMeta<?>> entityMetas) {
		if (entityMetas != null) {
			this.entityMetas = entityMetas;
		}
		LOG.debug("Invalidating NucletDalProvider");
		ConcurrentMapCache cache = RigidUtils.uncheckedCast(cacheManager.getCache("nucletDalProviderEntityObjectProcessor"));
		Map<String, IExtendedEntityObjectProcessor<?>> nativeCache = RigidUtils.uncheckedCast(cache.getNativeCache());
		for (IEntityObjectProcessor<?> eoProc : nativeCache.values()) {
			try {
				eoProc.destroy();
			} catch (Exception e) {
				LOG.error(String.format("Error destruction of EntityObjectProcessor for entity \"%s\": %s", eoProc.getEntityUID(), e.getMessage()), e);
			}
		}
	}

	/**
	 * Free the memory / clear caches
	 * @param includeLinkedProcessors
	 * 			Free also all other processors for this entity, e.g. the Extended
	 */
	public void freeMemory(final boolean includeLinkedProcessors) {
		final Temporal startTime = RigidUtils.now();
		final long totalMemoryUsageBefore = getTotalMemoryUsage(includeLinkedProcessors);
		getAllEntities().parallelStream()
				.map(this::getEntityObjectProcessor)
				.forEach(proc -> proc.freeMemory(includeLinkedProcessors));
		final long totalMemoryUsageAfter = getTotalMemoryUsage(includeLinkedProcessors);
		LOG.info(String.format("%s freed memory in %s",
				RigidUtils.humanReadableBytes(totalMemoryUsageBefore - totalMemoryUsageAfter, ByteUnit.MEGABYTE),
				RigidUtils.humanReadableDurationToNow(startTime)));
	}

	/**
	 * Answer the total instance footprint
	 * @param includeLinkedProcessors
	 * 			Query also all other processors for this entity, e.g. the Extended
	 * @return total instance footprint, bytes
	 */
	public long getTotalMemoryUsage(boolean includeLinkedProcessors) {
		return new CommonConsole.SysOutSuppressor("# WARNING: Unable to attach Serviceability Agent.")
				.suppressSysOut(() -> getAllEntities().parallelStream()
				.map(this::getEntityObjectProcessor)
				.map(proc -> proc.getTotalMemoryUsage(includeLinkedProcessors))
				.reduce(0L, ArithmeticUtils::addAndCheck));
	}

	/**
	 * Detailed view of the used memory (in memory stored data) of all processors
	 * @param includeLinkedProcessors
	 * 			Query also all other processors for this entity, e.g. the Extended
	 * @param filterUsageGreaterThan
	 * 			Return only those entities whose memory usage is greater than this value
	 * @return Sorted (the biggest usage first) list of entities and there memory usage
	 */
	public List<Pair<EntityMeta<?>, Long>> getMemoryUsageByEntities(final boolean includeLinkedProcessors, final long filterUsageGreaterThan) {
		return new CommonConsole.SysOutSuppressor("# WARNING: Unable to attach Serviceability Agent.")
				.suppressSysOut(() -> getAllEntities().parallelStream()
				.map(entityMeta -> new Pair<EntityMeta<?>, Long>(
						entityMeta,
						getEntityObjectProcessor(entityMeta.getUID()).getTotalMemoryUsage(includeLinkedProcessors)))
				.filter(pair -> pair.getY() > filterUsageGreaterThan)
				.sorted(new Comparator<Pair<EntityMeta<?>, Long>>() {
					@Override
					public int compare(final Pair<EntityMeta<?>, Long> p1, final Pair<EntityMeta<?>, Long> p2) {
						if (Objects.equals(p1.getY(), p2.getY())) {
							return String.CASE_INSENSITIVE_ORDER.compare(p1.getX().getEntityName(), p2.getX().getEntityName());
						} else {
							return Long.compare(p2.getY(), p1.getY()); // bigger first
						}
					}
				}).collect(Collectors.toList()));
	}

	private static Object syncObject(UID entityUID) {
		return BLOCKER.computeIfAbsent(entityUID, key -> new UID());
	}
	
	/**
	 * only available during rule execution
	 * @return 
	 */
	public Set<UID> getAccessibleMandators() {
		try {
			return accessibleMandators.get().peek();
		} catch (EmptyStackException ese) {
			return null;
		}
	}
	
	/**
	 * only available during rule execution
	 * @return 
	 */
	public UID getAccessibleMandatorsOrigin() {
		try {
			return accessibleMandatorsOrigin.get().peek();
		} catch (EmptyStackException ese) {
			return null;
		}
	}
	
	public boolean isAccessibleMandatorsSet() {
		try {
			return accessibleMandators.get().peek() != null;
		} catch (EmptyStackException ese) {
			return false;
		}
	}
	
	/** 
	 * set from rule engine only
	 * @param mandators
	 * @param mandatorOrigin
	 */
	public void setAccessibleMandators(Set<UID> mandators, UID mandatorOrigin) {
		accessibleMandators.get().push(mandators);
		accessibleMandatorsOrigin.get().push(mandatorOrigin);
	}
	
	/**
	 * called from rule engine only
	 */
	public void removeAccessibleMandators() {
		accessibleMandators.get().pop();
		accessibleMandatorsOrigin.get().pop();
	}
	
	/**
	 * only available during rule execution
	 * @return 
	 */
	public UID getRunningJob() {
		try {
			return runningJob.get().peek();
		} catch (EmptyStackException ese) {
			return null;
		}
	}
	
	public boolean isRunningJobSet() {
		try {
			return runningJob.get().peek() != null;
		} catch (EmptyStackException ese) {
			return false;
		}
	}
	
	/** 
	 * set from rule engine only
	 * @param job
	 */
	public void setRunningJob(UID job) {
		runningJob.get().push(job);
	}
	
	/**
	 * called from rule engine only
	 */
	public void removeRunningJob() {
		runningJob.get().pop();
	}
}
