package org.nuclos.server.dbtransfer;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.nuclos.businessentity.facade.NucletFacade;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.maintenance.MaintenanceFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a class for automatically importing nuclets.
 * Manages the process of enabling maintenance mode, resolving nuclet files in a specified path,
 * and performing auto-import of nuclets. Provides methods to handle maintenance mode, exit maintenance mode,
 * and synchronize parameters for the transfer. Also includes a method to scan for nuclets and initiate the auto-import process.
 */
public class AutoNucletImport {
    protected static final Logger LOG = LoggerFactory.getLogger(AutoNucletImport.class);

	private final ParameterProvider parameterProvider;
    private final MaintenanceFacadeLocal maintenanceFacade;
    private final TransferFacadeRemote transferFacade;
    private final MasterDataFacadeLocal masterDataFacadeLocal;
	private final NucletFacade nucletFacade;
	private final CustomCodeManager customCodeManager;
    private final File pathToNuclets;
    private boolean maintenanceModeAlready = false;
	private boolean maintenanceModeChecked = false;

    public AutoNucletImport(ParameterProvider parameterProvider,
							MaintenanceFacadeLocal maintenanceFacade,
							TransferFacadeRemote transferFacade,
							MasterDataFacadeLocal masterDataFacadeLocal,
							NucletFacade nucletFacade,
							CustomCodeManager customCodeManager,
							final File pathToNuclets) {
		this.parameterProvider = parameterProvider;
        this.maintenanceFacade = maintenanceFacade;
        this.transferFacade = transferFacade;
        this.masterDataFacadeLocal = masterDataFacadeLocal;
		this.nucletFacade = nucletFacade;
		this.customCodeManager = customCodeManager;
        this.pathToNuclets = pathToNuclets;
    }

    public void enableMaintenanceModeIfNecessary() {
		if (maintenanceModeChecked) {
			return;
		}
		this.maintenanceModeChecked = true;

        if (maintenanceFacade == null) return;

        if (maintenanceFacade.isMaintenanceOff()) {
			maintenanceFacade.forceMaintenanceMode();
            maintenanceFacade.enterMaintenanceMode(parameterProvider.getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER));
        } else {
            this.maintenanceModeAlready = true;
        }
    }

    public void exitMaintenanceModeIfNecessary() throws CommonValidationException {
        if (maintenanceFacade == null) return;
        if (!this.maintenanceModeAlready) {
            maintenanceFacade.exitMaintenanceMode();
        }
    }

    public List<File> resolveNucletFilesInPath() {
        if (pathToNuclets != null && pathToNuclets.exists() && pathToNuclets.isDirectory()) {
            File[] files = pathToNuclets.listFiles();
            if (files != null) {
                // create zip from directories
                List<File> createdZIPFiles =
                        Arrays.stream(files)
                                .filter(File::isDirectory)
                                .map(AutoNucletImport::compressFolder)
                                .collect(Collectors.toList());
                List<File> foundZIPFiles =
                        Arrays.stream(files)
                                .filter(AutoNucletImport::isNucletFile)
                                .collect(Collectors.toList());

                List<File> combined = new ArrayList<>();
                combined.addAll(foundZIPFiles);
                combined.addAll(createdZIPFiles);

                combined.sort(Comparator.comparing(File::getName));

                return combined;
            }
        }
        return new ArrayList<>();
    }

    public List<Transfer.Result> autoImport() {
		boolean leaveMaintenance = true;
        try {
            LOG.info("AutoImport for nuclets started.");

            List<Transfer.Result> importResults = new ArrayList<>();
            for (File file : resolveNucletFilesInPath()) {
                LOG.info("AutoImport nuclet at {}", file.getAbsolutePath());
				Instant start = Instant.now();
                try {
                    byte[] bytes = Files.readAllBytes(file.toPath());
                    String fileName = file.getName();
                    Transfer.Result result = null;

                    if (!transferFacade.checkIfFileNeedsToBeTransfered(bytes, fileName)) {
                        LOG.info("AutoImport for nuclet {} skipped due to not changed bytes", fileName);
                        continue;
                    }

					enableMaintenanceModeIfNecessary();
                    try {
                        Transfer importTransferObject = transferFacade.prepareTransfer(bytes, fileName, false, ConstraintAction.CLEAN);
                        if (importTransferObject.result.hasCriticals()) {
							leaveMaintenance = false;
                            throw new NuclosBusinessException("Error importing nuclet: " + importTransferObject.result.getCriticals());
                        }
                        Transfer transfer = new Transfer(importTransferObject);
                        transfer.setParameter(syncParameters(transfer));
                        result = transferFacade.runTransfer(transfer, ConstraintAction.REBUILD);
                        importResults.add(result);
						writeResult(importTransferObject.result, start, file);
                    } catch (NuclosBusinessException e) {
						leaveMaintenance = false;
                        LOG.error("Unable to import nuclet '{}'.", fileName, e);
						writeResult(e, start, file);
                    }
                    if (result != null && result.hasCriticals()) {
						leaveMaintenance = false;
                        LOG.warn("nucletimport with criticals: {}", result.getCriticals());
                    }
                } catch (Exception e) {
					leaveMaintenance = false;
                    LOG.warn("nucletimport failed: {}", e.getMessage(), e);
					writeResult(e, start, file);
                }
            }

            LOG.info("AutoImport for nuclets finished.");
            return importResults;
        } catch (Exception e) {
            LOG.error("AutoImport failed", e);
        } finally {
			if (maintenanceModeChecked && !this.maintenanceModeAlready && leaveMaintenance) {
				SpringApplicationContextHolder.addSpringReadyListener(() -> {
					try {
						if (nucletFacade != null && customCodeManager != null && !maintenanceFacade.isMaintenanceOff()) {
							if (!nucletFacade.areExtensionsUpToDate()) {
								LOG.info("Extensions are not up to date, stay in maintenance mode.");
							} else if (!customCodeManager.hasClassLoader()) {
								LOG.info("CustomCodeManager not ready, stay in maintenance mode.");
							} else {
								exitMaintenanceModeIfNecessary();
							}
						}
					} catch (Exception e) {
						LOG.error("Error while exiting maintenance mode: {}", e.getMessage(), e);
					}
				});
			}
		}

        return Collections.emptyList();
    }

	protected void writeResult(final Object oRes, final Instant start, final File nucletFile) {
		final Path parent = nucletFile.toPath().getParent();
		if (isNucletFile(nucletFile)) {
			final long minutes = (long) Math.ceil(Duration.between(start, Instant.now()).getSeconds() / 60.0);
			final String logtime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmm"));
			final String baseName = FilenameUtils.getBaseName(nucletFile.getName());
			final Path resultFile = parent.resolve(baseName + ".latest.result");
			try {
				Files.deleteIfExists(resultFile);
				final String resCategory;
				if (oRes instanceof Transfer.Result) {
					final Transfer.Result result = (Transfer.Result) oRes;
					if (result.hasCriticals()) {
						resCategory = "critical";
					} else if (result.hasWarnings()) {
						resCategory = "warn";
					} else {
						resCategory = "ok";
					}
					Files.write(resultFile, String.format("# Import with %s at %s completed (took %d minutes)\n\n", resCategory.toUpperCase(), logtime, minutes)
							.getBytes(StandardCharsets.UTF_8));
					if (result.hasCriticals()) {
						writeResultBlock(resultFile, "Critical", result.getCriticals());
					}
					if (result.hasWarnings()) {
						writeResultBlock(resultFile, "Warn", result.getWarnings());
					}
					if (result.hasInfos()) {
						writeResultBlock(resultFile, "Info", result.getInfos());
					}
					if (!result.getScript().isEmpty()) {
						Files.write(resultFile, "## Script\n".getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
						Files.write(resultFile, result.getScript(), StandardOpenOption.APPEND);
					}
				} else if (oRes instanceof Exception) {
					final Exception e = (Exception) oRes;
					resCategory = "exception";
					Files.write(resultFile, String.format("# Import with %s at %s completed (took %d minutes)\n\n", resCategory.toUpperCase(), logtime, minutes)
							.getBytes(StandardCharsets.UTF_8));
					Files.write(resultFile, ExceptionUtils.getStackTrace(e).getBytes(), StandardOpenOption.APPEND);
				} else {
					throw new IllegalStateException("Unexpected result type: " + oRes.getClass());
				}
				final Path logFile = parent.resolve(baseName + "." + logtime + "." + resCategory + ".result");
				Files.copy(resultFile, logFile, StandardCopyOption.REPLACE_EXISTING);
			} catch (Exception e) {
				LOG.error("Unable to write import result file '{}'.", resultFile, e);
			}
		}
	}

	private void writeResultBlock(Path resultFile, String title, String block) throws IOException {
		Files.write(resultFile, String.format("## %s\n", title).getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
		Files.write(resultFile, block
				.replaceAll("<br />","\n")
				.replaceAll("<br/>","\n")
				.replaceAll("<br>","\n")
				.replaceAll("(?m)^", "\t")
				.getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
		Files.write(resultFile, "\n\n".getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
	}

    protected Collection<EntityObjectVO<UID>> syncParameters(final Transfer transfer) {
        Collection<EntityObjectVO<UID>> oldParams = new ArrayList<EntityObjectVO<UID>>();
        Collection<MasterDataVO<UID>> params = this.masterDataFacadeLocal.getMasterData(E.NUCLETPARAMETER, TrueCondition.TRUE);
        for (MasterDataVO<UID> param : params) {
            oldParams.add(param.getEntityObject());
        }

        Collection<EntityObjectVO<UID>> newParams = transfer.getParameter();

        return TransferFacadeBean.synchronizeParameters(oldParams, newParams, "null");
    }

    public static boolean isNucletFile(File f) {
        return f != null && f.isFile() && (f.getName().endsWith(".nuclet") || f.getName().endsWith(".zip"));
    }

    public static List<Transfer.Result> scanForNuclets(ParameterProvider parameterProvider,
													   MaintenanceFacadeLocal maintenanceFacade,
                                      TransferFacadeRemote transferFacade,
                                      MasterDataFacadeLocal masterDataFacadeLocal,
									  NucletFacade nucletFacade,
									  CustomCodeManager customCodeManager,
                                      final File pathToNuclets) {
        AutoNucletImport autoNucletImport = new AutoNucletImport(parameterProvider, maintenanceFacade,
				transferFacade, masterDataFacadeLocal, nucletFacade, customCodeManager, pathToNuclets);
        return autoNucletImport.autoImport();
    }

    protected static File compressFolder(File sourceDir) {
        File outputFile = new File(sourceDir.getAbsolutePath() + ".nuclet");
        try {
			Files.deleteIfExists(outputFile.toPath());
            try (ZipOutputStream zipOutputStream = new ZipOutputStream(Files.newOutputStream(outputFile.toPath()))) {
                compressDirectoryToZipFile(sourceDir.toPath(), sourceDir.toPath(), zipOutputStream);
            }
        } catch (IOException e) {
            LOG.warn("Creation of ZIP file from {} failed: {}", sourceDir.getAbsolutePath(), e.getMessage(), e);
        }
        return outputFile;
    }

    protected static void compressDirectoryToZipFile(Path basePath, Path dir, ZipOutputStream out) throws IOException {
		try (Stream<Path> walk = Files.walk(dir).sorted()) {
			for (Path path : walk.collect(Collectors.toList())) {
				if (basePath.equals(path)) continue;
				String entryName = basePath.relativize(path).toString().replace("\\", "/");
				ZipEntry entry = new ZipEntry(entryName);
				entry.setTime(0);
				entry.setMethod(ZipEntry.DEFLATED);
				out.putNextEntry(entry);
				if (!Files.isDirectory(path)) {
					Files.copy(path, out);
				}
				out.closeEntry();
			}
		}
    }
}
