//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.nuclos.common.Version;
import org.nuclos.common.dbtransfer.TransferOption;

public class MetaDataRoot {

	public final Integer transferVersion;
	public final String nucletUID;
	public final Version version;
	public final String database;
	public final Date exportDate;
	
	public final Map<TransferOption, Serializable> exportOptions;

	public MetaDataRoot(
		Integer transferVersion,
		String nucletUID,
		Version version,
		String database,
		Date exportDate,
		Map<TransferOption, Serializable> exportOptions)
	{
		this.transferVersion = transferVersion;
		this.nucletUID = nucletUID;
		this.version = version;
		this.database = database;
		this.exportDate = exportDate;
		this.exportOptions = exportOptions;
	}
}
