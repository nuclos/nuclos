package org.nuclos.server.dbtransfer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nuclos.businessentity.rule.ConfigurationSaveRule;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class SqlConfigurationService {
    protected static final Logger LOG = LoggerFactory.getLogger(SqlConfigurationService.class);

    public enum RunType {
        BEFORE_SCHEME, AFTER_SCHEME
    }

    private final PersistentDbAccess dataBaseAccess;
    private final List<String> includes = new ArrayList<>();
    private final List<String> excludes = new ArrayList<>();

    SqlConfigurationService(
            DbAccess dataBaseAccess,
            String tagFilter
    ) {
        this.dataBaseAccess = new PersistentDbAccess(dataBaseAccess);
        if (dataBaseAccess != null && dataBaseAccess.getDbType() != null) {
            this.includes.add(dataBaseAccess.getDbType().toString());
        }
        this.includes.addAll(
                collectTagContent(tagFilter, "include")
        );
        this.excludes.addAll(
                collectTagContent(tagFilter, "exclude")
        );
    }

    public Optional<String> executeSQL(String sql) {
        try {
            if (sql != null && !sql.isEmpty()) {
                this.dataBaseAccess.executePlainUpdate(sql);
                return Optional.empty();
            } else {
                return Optional.of("SQL Statement is empty/wrong");
            }
        } catch (DbException e) {
            LOG.debug("SQL Configuration failed to execute", e.getSqlCause());
            return Optional.of(
                    e.getSqlCause().getMessage()
            );
        }
    }

    public void updateConfigurationInDatabase(UID primaryKey, String runCheckSum, Boolean hadErrors, String runOut) {
        if (this.dataBaseAccess != null) {
            try {
                this.dataBaseAccess.execute(
                        DbStatementUtils.updateValues(
                                E.NUCLETSQLCONFIGURAITON,
                                E.NUCLETSQLCONFIGURAITON.lastRunCkSum,
                                runCheckSum,
                                E.NUCLETSQLCONFIGURAITON.lastRunError,
                                hadErrors,
                                E.NUCLETSQLCONFIGURAITON.lastRunResult,
                                runOut,
                                E.NUCLETSQLCONFIGURAITON.lastRun,
                                InternalTimestamp.toInternalTimestamp(new Date())
                        ).where(
                                E.NUCLETSQLCONFIGURAITON.getPk(), primaryKey
                        )
                );
            } catch (DbException e) {
                LOG.error("Could not update SQLConfigurations in database");
                LOG.debug(e.getMessage(), e);
            }
        }
    }

    public boolean tagMatch(String configurationTags) {
        List<String> tags = Optional
                .ofNullable(configurationTags)
                .map(
                        tagList -> tagList.isEmpty() ? includes : Arrays.asList(tagList.split(" "))
                )
                .orElse(
                        includes
                );

        boolean matchesInclude = includes.stream()
                .map(Pattern::compile)
                .anyMatch(pattern ->
                        tags.stream()
                            .map((t) -> pattern.matcher(t).matches())
                            .filter(Boolean.TRUE::equals)
                            .findFirst()
                            .orElse(Boolean.FALSE)
                );
        if (matchesInclude) {
            boolean matchesExclude = excludes.stream()
                    .map(Pattern::compile)
                    .anyMatch(pattern ->
                            tags.stream()
                                    .map((t) -> pattern.matcher(t).matches())
                                    .filter(Boolean.TRUE::equals)
                                    .findFirst()
                                    .orElse(Boolean.FALSE)
                    );
            return !matchesExclude;
        }
        return false;
    }

    protected List<String> collectTagContent(String input, String tagName) {
        List<String> contents = new ArrayList<>();

        if (input == null) {
            return contents;
        }

        String regex = "<" + tagName + ">(.*?)</" + tagName + ">";
        Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            contents.add(matcher.group(1).trim());
        }

        return contents;
    }

    public static void runSqlConfigurations(DbAccess dbAccess, String tagFilter, RunType runType) {
        SqlConfigurationService service = new SqlConfigurationService(dbAccess, tagFilter);
        try {
            service.getSqlConfigurations(
                    runType.equals(RunType.AFTER_SCHEME)
            ).stream().filter(
                    skipUnwantedConfigurations(service)
            ).sorted(
                    Comparator.comparingInt(t -> t.get(E.NUCLETSQLCONFIGURAITON.order))
            ).forEach(dbTuple -> {
                String configSQL = dbTuple.get(E.NUCLETSQLCONFIGURAITON.sql);
                String lastRunCkSum = ConfigurationSaveRule.calculateChecksum(configSQL);

                // split configSQL so that multiple statements could be executed one after another
                AtomicBoolean hadErrors = new AtomicBoolean(false);
                StringBuilder runOutputs = new StringBuilder();
                Optional<String> sqlExecute = service.executeSQL(configSQL);
                if (sqlExecute.isPresent()) {
                    hadErrors.set(true);
                    runOutputs.append(sqlExecute.get()).append("\n");
                }

                service.updateConfigurationInDatabase(
                        dbTuple.get(E.NUCLETSQLCONFIGURAITON.getPk()),
                        lastRunCkSum,
                        hadErrors.get(),
                        runOutputs.toString()
                );
            });
        } catch (DbException e) {
            LOG.error("Could not read SQLConfigurations from database");
            LOG.debug(e.getMessage(), e);
        }
    }

    protected static Predicate<DbTuple> skipUnwantedConfigurations(SqlConfigurationService service) {
        return dbTuple ->
                dbTuple.get(E.NUCLETSQLCONFIGURAITON.nucletVersion) <= dbTuple.get(E.NUCLET.nucletVersion) &&
                !ConfigurationSaveRule.calculateChecksum(dbTuple.get(E.NUCLETSQLCONFIGURAITON.sql))
                        .equals(dbTuple.get(E.NUCLETSQLCONFIGURAITON.lastRunCkSum)) &&
                service.tagMatch(dbTuple.get(E.NUCLETSQLCONFIGURAITON.tags));
    }

    private List<DbTuple> getSqlConfigurations(Boolean afterImport) throws DbException {
        if (this.dataBaseAccess != null) {
            DbQueryBuilder builder = dataBaseAccess.getQueryBuilder();
            DbQuery<DbTuple> query = builder.createTupleQuery();
            DbFrom<UID> t = query.from(E.NUCLETSQLCONFIGURAITON);
            DbJoin<UID> j = t.joinOnJoinedPk(E.NUCLET, JoinType.INNER, E.NUCLETSQLCONFIGURAITON.nuclet, "nucletD");
            query.multiselect(
                    t.basePk(),
                    t.baseColumn(E.NUCLETSQLCONFIGURAITON.order),
                    t.baseColumn(E.NUCLETSQLCONFIGURAITON.sql),
                    t.baseColumn(E.NUCLETSQLCONFIGURAITON.tags),
                    t.baseColumn(E.NUCLETSQLCONFIGURAITON.lastRunCkSum),
                    t.baseColumn(E.NUCLETSQLCONFIGURAITON.nucletVersion), // INTNUCVERSION
                    j.baseColumn(E.NUCLET.nucletVersion) //INTNUCLETVERSION
            ).where(
                    builder.equalValue(
                            t.baseColumn(E.NUCLETSQLCONFIGURAITON.afterImport), afterImport
                    )
            );
            return dataBaseAccess.executeQuery(query);
        }
        return new ArrayList<>();
    }
}
