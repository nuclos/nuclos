//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer;

import java.io.InputStream;
import java.util.List;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.Mutable;
import org.nuclos.common.UID;
import org.nuclos.common.dbtransfer.TransferNuclet;
import org.nuclos.common2.BoDataSet;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;

// @Local
public interface TransferFacadeLocal {
	
	void checkCircularReference(UID nucletUID) throws CommonValidationException;
	
	BoDataSet importOrExportDatabase(InputStream is)  throws CommonPermissionException;
	
	List<TransferNuclet> getAvailableNuclets();

	void validateEntityFieldDefaultMandatory(Mutable<String> defaultMandatoryId,
											 Mutable<String> defaultMandatoryValue,
											 FieldMeta<?> fieldMeta,
											 StringBuffer sbWarning);

	void validateEntityFieldDefaultValue(Mutable<Long> idDefault,
												Mutable<UID> uidDefault,
												Mutable<String> valueDefault,
										 		FieldMeta<?> fieldMeta);
}
