package org.nuclos.server.dbtransfer.content;

import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class EmailIncomingServerNucletContent extends DefaultNucletContent {

	public EmailIncomingServerNucletContent(final List<INucletContent> contentTypes, final IRigidMetaProvider metaProv) {
		super(E.EMAILINCOMINGSERVER, contentTypes, metaProv);
	}

	@Override
	public EntityObjectVO<UID> readNc(final EntityObjectVO<UID> ncObject) {
		ncObject.removeFieldValue(E.EMAILINCOMINGSERVER.testResult.getUID());
		return super.readNc(ncObject);
	}
}
