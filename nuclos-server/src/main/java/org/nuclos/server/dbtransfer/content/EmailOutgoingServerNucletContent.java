package org.nuclos.server.dbtransfer.content;

import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class EmailOutgoingServerNucletContent extends DefaultNucletContent {

	public EmailOutgoingServerNucletContent(final List<INucletContent> contentTypes, final IRigidMetaProvider metaProv) {
		super(E.EMAILOUTGOINGSERVER, contentTypes, metaProv);
	}

	@Override
	public EntityObjectVO<UID> readNc(final EntityObjectVO<UID> ncObject) {
		ncObject.removeFieldValue(E.EMAILOUTGOINGSERVER.testResult.getUID());
		ncObject.removeFieldValue(E.EMAILOUTGOINGSERVER.testRecipient.getUID());
		return super.readNc(ncObject);
	}
}
