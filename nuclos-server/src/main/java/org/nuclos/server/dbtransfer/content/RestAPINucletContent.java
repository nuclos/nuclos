package org.nuclos.server.dbtransfer.content;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.documentfile.DocumentFileUtils;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

public class RestAPINucletContent extends DefaultNucletContent implements IExternalizeBytes {

	private final static Set<UID> ignoredFieldsUsedInEqualityCheck = Collections.singleton(E.RESTAPIS.content.getUID());

	public RestAPINucletContent(List<INucletContent> contentTypes, IRigidMetaProvider metaProv) {
		super(E.RESTAPIS, contentTypes, metaProv);
	}

	@Override
	protected Set<UID> getIgnoredFieldsUsedInEqualityCheck() {
		return ignoredFieldsUsedInEqualityCheck;
	}

	@Override
	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject) {
		loadDocumentFileContent(ncObject, E.RESTAPIS.apifile.getUID());
		// we do not need the backup here (for the change detection)
		ncObject.removeFieldValue(E.RESTAPIS.content.getUID());
		return ncObject;
	}

	@Override
	public EntityObjectVO<UID> readNcOriginal(final EntityObjectVO<UID> ncObject) {
		loadDocumentFileContent(ncObject, E.RESTAPIS.apifile.getUID());
		return ncObject;
	}

	@Override
	public void deleteNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly) {
		if (!testOnly) {
			DocumentFileBase docfile = (DocumentFileBase) NucletDalProvider.getInstance().getEntityObjectProcessor(getEntity()).getByPrimaryKey(ncObject.getPrimaryKey()).getFieldValue(E.RESTAPIS.apifile.getUID());
			if (docfile != null) {
				DocumentFileUtils.removeDocumentFiles(ncObject, metaProv);
			}
		}
		super.deleteNcObject(result, ncObject, testOnly);
	}

	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly, final Transfer transfer, final LogEntry log) {
		if (ncObject.isFlagUpdated()) {
			// remove backup from cache also (for the change detection)
			EntityObjectVO<UID> eoFromCache = getEOOriginal(getEntity().getUID(), ncObject.getPrimaryKey());
			eoFromCache.removeFieldValue(E.RESTAPIS.content.getUID());
		}
		if (!testOnly) {
			Map<UID, UID> existingDocumentFileMap = null;
			if (!ncObject.isFlagNew()) {
				existingDocumentFileMap = DocumentFileUtils.getExistingDocumentFiles(ncObject, metaProv);
			}
			DocumentFileUtils.storeDocumentFiles(ncObject, existingDocumentFileMap, metaProv);
		}
		return super.insertOrUpdateNcObject(result, ncObject, testOnly, transfer, log);
	}
	
	@Override
	public Map<FieldMeta<?>, Pair<String, byte[]>> externalize(EntityObjectVO<UID> ncObject) {
		DocumentFileBase docfile = (DocumentFileBase) ncObject.getFieldValue(E.RESTAPIS.apifile.getUID());
		if (docfile != null) {
				String name = docfile.getFilename();
			if (name != null && name.contains(".")) {
				String fileType = name.substring(name.lastIndexOf(".")+1);
				if (fileType != null && fileType.length() > 0) {
					byte[] bytes = docfile.getContents();
					if (bytes != null) {
						Map<FieldMeta<?>, Pair<String, byte[]>> result = new HashMap<>();
						result.put(E.RESTAPIS.apifile, new Pair<>(fileType.toLowerCase(), bytes));
						ncObject.setFieldValue(E.RESTAPIS.apifile.getUID(), new GenericObjectDocumentFile(docfile.getFilename(), docfile.getDocumentFilePk()));
						return result;
					}
				}
			}
		}
		return null;
	}

	@Override
	public void importBytes(EntityObjectVO<UID> ncObject, FieldMeta<?> efMeta, byte[] bytes) {
		if (E.RESTAPIS.apifile.equals(efMeta)) {
			DocumentFileBase docfile = (DocumentFileBase) ncObject.getFieldValue(E.RESTAPIS.apifile.getUID());
			if (docfile != null) {
				ncObject.setFieldValue(E.RESTAPIS.apifile.getUID(), new GenericObjectDocumentFile(docfile.getFilename(), docfile.getDocumentFilePk(), bytes));
			}
		}
	}
}
