//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.Date;
import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.springframework.security.core.context.SecurityContextHolder;


public class SqlConfigurationNucletContent extends DefaultNucletContent {

	public SqlConfigurationNucletContent(List<INucletContent> contentTypes, IRigidMetaProvider metaProv) {
		super(E.NUCLETSQLCONFIGURAITON.nuclet, contentTypes, metaProv);
	}

	@Override
	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject) {
		removeStatusFields(ncObject);
		ncObject.setVersion(1);
		return super.readNc(ncObject);
	}

	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly, final Transfer transfer, final LogEntry log) {
		if (ncObject.isFlagUpdated()) {
			EntityObjectVO<UID> existing = NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLETSQLCONFIGURAITON).getByPrimaryKey(ncObject.getPrimaryKey());
			Date sysdate = new Date();
            existing.setFieldValue(E.NUCLETSQLCONFIGURAITON.order, ncObject.getFieldValue(E.NUCLETSQLCONFIGURAITON.order));
			existing.setFieldValue(E.NUCLETSQLCONFIGURAITON.name, ncObject.getFieldValue(E.NUCLETSQLCONFIGURAITON.name));
			existing.setFieldValue(E.NUCLETSQLCONFIGURAITON.description, ncObject.getFieldValue(E.NUCLETSQLCONFIGURAITON.description));
			existing.setFieldValue(E.NUCLETSQLCONFIGURAITON.sql, ncObject.getFieldValue(E.NUCLETSQLCONFIGURAITON.sql));
            existing.setFieldValue(E.NUCLETSQLCONFIGURAITON.nucletVersion, ncObject.getFieldValue(E.NUCLETSQLCONFIGURAITON.nucletVersion));
            existing.setFieldValue(E.NUCLETSQLCONFIGURAITON.isBreaking, ncObject.getFieldValue(E.NUCLETSQLCONFIGURAITON.isBreaking));
            existing.setFieldValue(E.NUCLETSQLCONFIGURAITON.tags, ncObject.getFieldValue(E.NUCLETSQLCONFIGURAITON.tags));
			existing.setFieldUid(E.NUCLETSQLCONFIGURAITON.nuclet, ncObject.getFieldUid(E.NUCLETSQLCONFIGURAITON.nuclet));
			existing.setVersion(ncObject.getVersion());
			existing.setChangedAt(InternalTimestamp.toInternalTimestamp(sysdate));
			existing.setChangedBy(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
			ncObject = existing;
			ncObject.flagUpdate();
		} else {
			removeStatusFields(ncObject);
		}

		return super.insertOrUpdateNcObject(result, ncObject, testOnly, transfer, log);
	}

    private void removeStatusFields(EntityObjectVO<UID> ncObject) {
		ncObject.setFieldValue(E.NUCLETSQLCONFIGURAITON.lastRun, null);
		ncObject.setFieldValue(E.NUCLETSQLCONFIGURAITON.lastRunCkSum, null);
        ncObject.setFieldValue(E.NUCLETSQLCONFIGURAITON.lastRunResult, null);
        ncObject.setFieldValue(E.NUCLETSQLCONFIGURAITON.lastRunError, null);
	}

}
