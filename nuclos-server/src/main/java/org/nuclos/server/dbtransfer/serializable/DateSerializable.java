//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.serializable;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.nuclos.common2.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class DateSerializable implements Converter {
	
	private static final Logger LOG = LoggerFactory.getLogger(DateSerializable.class);
	
	public static void register(XStream xstream) {
		xstream.registerConverter(new DateSerializable());
	}

	@Override
	public boolean canConvert(Class type) {
		return java.util.Date.class.equals(type);
	}

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer,	MarshallingContext context) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		writer.setValue(sdf.format(source));
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		String sValue = reader.getValue();
		if (StringUtils.looksEmpty(sValue)) {
			return null;
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				return sdf.parse(sValue);
			} catch (ParseException e) {
				LOG.error("Could not unmarshal date: {}", sValue, e);
				return null;
			}
		}
	}
	
}
