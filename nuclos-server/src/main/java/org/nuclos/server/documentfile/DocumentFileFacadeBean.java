//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.documentfile;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.nuclos.businessentity.DocumentDir;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.IOUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.common.utils.NuclosFileUtils;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.dal.ExtendedDalUtils;
import org.nuclos.server.dal.processor.nuclos.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbDecrement;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import jonelo.jacksum.JacksumAPI;
import jonelo.jacksum.algorithm.AbstractChecksum;

/**
 * Facade for managing DocumentFile objects.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:kai.fibr@nuclos.de">Kai Fibr</a>
 * @version 01.00.00
 */
@Component("documentFileService")
public class DocumentFileFacadeBean extends NuclosFacadeBean implements DocumentFileFacadeLocal, DocumentFileFacadeRemote {

	@Autowired
	private NucletDalProvider dalProv;

	@Autowired
	private NuclosDalProvider nuclosDalProv;

	@Autowired
	private ServerParameterProvider paramProv;

	private SpringDataBaseHelper dataBaseHelper;
	private PersistentDbAccess dbAccessPers;

	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
		this.dbAccessPers = new PersistentDbAccess(dataBaseHelper.getDbAccess());
	}

	@PostConstruct
	private void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		this.restoreMissingInternalDocumentFilesFromDbBackup();
	}

	@Autowired
	void setMetaProvider(MetaProvider metaProv) {
		// meta provider is a dependency for the restore to work
	}

	private IExtendedEntityObjectProcessor<UID> getDocFileProcessor() {
		return dalProv.getEntityObjectProcessor(E.DOCUMENTFILE);
	}

	private IExtendedEntityObjectProcessor<UID> getDocLinkProcessor() {
		return dalProv.getEntityObjectProcessor(E.DOCUMENTFILELINK);
	}

	/**
	 * Get name of uploaded file by documentFileUID
	 * @param documentFileUID UID of DocumentFile
	 * @return name of uploaded file
	 */
	@Override
	public String getFileName(UID documentFileUID) {
		return getFileNameAndDir(documentFileUID).getX();
	}

	/**
	 * Get name and subdir (if any) of uploaded file by documentFileUID
	 * @param documentFileUID UID of DocumentFile
	 * @return name of uploaded file
	 */
	@Override
	public Pair<String, String> getFileNameAndDir(UID documentFileUID) {
		return getFileNameAndDir(documentFileUID, null);
	}

	public Pair<String, String> getFileNameAndDir(UID documentFileUID, IEntityObjectProcessor<UID> docFileProcessor) {
		if (docFileProcessor == null) {
			docFileProcessor = getDocFileProcessor();
		}
		try {
			EntityObjectVO<UID> eoVO = docFileProcessor.getByPrimaryKey(documentFileUID);
			if (eoVO == null) {
				return new Pair<>();
			}
			final String fileName = eoVO.getFieldValue(E.DOCUMENTFILE.filename.getUID(), String.class);
			final String dir = eoVO.getFieldValue(E.DOCUMENTFILE.documentDir.getUID(), String.class);
			return new Pair<>(fileName, (dir == null || File.separatorChar == '/') ? dir : dir.replace('/', File.separatorChar));
		} catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	/**
	 * Load file content and return as NuclosFile
	 * @param documentFileUID UID of DocumentFile
	 * @return NuclosFile 
	 */
	@Override
	public NuclosFile loadContentAsNuclosFile(UID documentFileUID) {
		Pair<String, String> fileNameAndDir = getFileNameAndDir(documentFileUID);
		byte[] content = loadContent(documentFileUID, fileNameAndDir.getX(), fileNameAndDir.getY());
		return new NuclosFile(documentFileUID, fileNameAndDir.getX(), content);
	}

	/**
	 *
	 * @param documentFileUID
	 * @return
	 */
	@Override
	public java.io.File getFile(UID documentFileUID) {
		final Pair<String, String> fileNameAndDir = getFileNameAndDir(documentFileUID);
		if (fileNameAndDir.getX() == null) {
			// return not existing
			return NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH).toPath().resolve("dev/null/file.does.not.exist").toFile();
		}
		return getFile(documentFileUID, fileNameAndDir.getX(), fileNameAndDir.getY());
	}

	private java.io.File getFile(UID documentFileUID, String fileName, String documentDir) {
		if (documentFileUID == null) {
			throw new NuclosFatalException("godocumentfile.invalid.id");// "Die Id des Dokumentanhangs darf nicht null sein");
		}
		File dir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH);
		if (documentDir != null) {
			dir = dir.toPath().resolve(documentDir).toFile();
		}
		java.io.File file = new java.io.File(dir, DocumentFileUtils.getDocumentFileName(documentFileUID, fileName));

		return file;
	}


	
	/**
	 * Load file content and return as byte array
	 * @param documentFileUID UID of DocumentFile
	 * @return content as byte array
	 */
	@Override
	public byte[] loadContent(UID documentFileUID) {
		Pair<String, String> fileNameAndDir = getFileNameAndDir(documentFileUID);
		return loadContent(documentFileUID, fileNameAndDir.getX(), fileNameAndDir.getY());
	}


	/**
	 * Load file content and return as byte array
	 * @param documentFileUID UID of DocumentFile
	 * @param fileName Name of the uploaded file
	 * @param documentDir Subdir if any
	 * @return content as byte array
	 */
	private byte[] loadContent(UID documentFileUID, String fileName, String documentDir) {
		java.io.File file = getFile(documentFileUID, fileName, documentDir);

		try {
			return IOUtils.readFromBinaryFile(file);
		}
		catch(IOException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	/**
	 * Count all document file entries
	 * @return number of document file entries
	 */
	public Long countDocumentFiles() {
		CollectableSearchExpression searchExpression = new CollectableSearchExpression();
		return getDocFileProcessor().count(searchExpression);
	}
	
	/**
	 * Get a chunk of DocumentFileChecksum objects, that contain
	 * meta information about document files
	 * @param startIndex start index of chunk
	 * @param endIndex end index of chunk
	 * @return a chunk of DocumentFileChecksum object
	 */
	public List<DocumentFileChecksum> getDocumentFileChecksums(long startIndex, long endIndex) {
		List<DocumentFileChecksum> result = new ArrayList<DocumentFileChecksum>();
		
		AbstractChecksum checksumObject = null;
		try {
			checksumObject = JacksumAPI.getChecksumInstance("md5");
			checksumObject.setEncoding(AbstractChecksum.HEX_UPPERCASE);
		} catch (NoSuchAlgorithmException nsae) {
			throw new NuclosFatalException(nsae);
		}
		
		Map<UID, FieldMeta<?>> mpFields = metaProvider.getAllEntityFieldsByEntity(E.DOCUMENTFILE.getUID());
		CollectableSearchExpression searchExpression = new CollectableSearchExpression();
		ResultParams resultParams = new ResultParams(mpFields.keySet(), startIndex, endIndex - startIndex + 1, true);
		for (EntityObjectVO<UID> eo : getDocFileProcessor().getBySearchExprResultParams(searchExpression, resultParams)) {
			try {
				UID documentUID = eo.getPrimaryKey();
				String fileName = (String) eo.getFieldValue(E.DOCUMENTFILE.filename.getUID());
				File file = getFile(documentUID);
				if (file.exists()) {
					// determine checksum of file
					checksumObject.readFile(file.getAbsolutePath());
					String checksum = checksumObject.format("#CHECKSUM");
					result.add(new DocumentFileChecksum(documentUID, fileName, checksum, file.length()));
					checksumObject.reset();
				}
				// not existing files are ignored
			} catch (Exception ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return result;
	}
	
	/**
	 * Create DocumentFile entry
	 *
	 * @param documentFileUID UID of DocumentFile
	 * @param fileName        name of uploaded file
	 * @return special document dir if any
	 */
	@Override
	public String createDocumentFile(UID documentFileUID, String fileName) {
		try {
			EntityObjectVO<UID> eoVO = new EntityObjectVO<>(E.DOCUMENTFILE);
			eoVO.setPrimaryKey(documentFileUID);
			eoVO.setFieldValue(E.DOCUMENTFILE.filename.getUID(), org.nuclos.common2.StringUtils.trimInvalidCharactersInFilename(fileName, false));
			eoVO.flagNew();
			ExtendedDalUtils.updateVersionInformation(eoVO, getCurrentUserName());
			final DocumentDir docDir = incrementOrCreateDocumentDir(eoVO.getCreatedAt());
			if (docDir != null) {
				eoVO.setFieldUid(E.DOCUMENTFILE.documentDir, docDir.getId());
				eoVO.setFieldValue(E.DOCUMENTFILE.documentDir.getUID(), docDir.getParent() + docDir.getId().getString());
			}
			getDocFileProcessor().insertOrUpdate(eoVO);
			return eoVO.getFieldValue(E.DOCUMENTFILE.documentDir.getUID(), String.class);
		} catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}

	/**
	 *
	 * @param date
	 * @return
	 */
	@Override
	public DocumentDir incrementOrCreateDocumentDir(Date date) {
		if (date == null) {
			return null;
		}
		final String guidelineTotals = paramProv.getValue(ParameterProvider.DOCUMENTS_GUIDELINE_FOR_TOTAL_NUMBER_OF_FILES_PER_DIR);
		if (StringUtils.isNotEmpty(guidelineTotals)) {
			final Integer iGuidelineTotals;
			try {
				iGuidelineTotals = Integer.valueOf(guidelineTotals);
			} catch (NumberFormatException ex) {
				LOG.error(String.format("Systemparameter %s value \"%s\" not a number: %s", ParameterProvider.DOCUMENTS_GUIDELINE_FOR_TOTAL_NUMBER_OF_FILES_PER_DIR, guidelineTotals, ex.getMessage()));
				return null;
			}
			String subDirStructure = paramProv.getValue(ParameterProvider.DOCUMENTS_SUBDIR_STRUCTURE_DATE_FORMAT);
			if (StringUtils.isEmpty(subDirStructure)) {
				subDirStructure = "yyyy";
			}
			subDirStructure = subDirStructure.replace('\\', '/');
			String parent = new SimpleDateFormat(subDirStructure).format(date);
			if (!parent.endsWith("/")) {
				parent += "/";
			}
			DbQueryBuilder builder = dbAccessPers.getQueryBuilder();
			final DbQuery<UID> query = builder.createQuery(UID.class);
			final DbFrom<UID> t = query.from(E.DOCUMENTDIR);
			query.select(t.basePk());
			query.where(builder.equalValue(t.baseColumn(E.DOCUMENTDIR.parent), parent));
			query.addToWhereAsAnd(builder.lessThan(t.baseColumn(E.DOCUMENTDIR.count), builder.literal(iGuidelineTotals)));
			query.limit(1L);
			final List<UID> foundDirs = dbAccessPers.executeQuery(query);
			DocumentDir result;
			if (foundDirs.isEmpty()) {
				// insert
				result = new DocumentDir();
				result.setId(createDocumentDir(parent));
				result.setParent(parent);
			} else {
				result = new DocumentDir();
				result.setId(foundDirs.get(0));
				result.setParent(parent);
				incrementDocumentDirCount(result.getId(), true);
			}
			return result;
		}
		return null;
	}

	private UID createDocumentDir(String parent) {
		final UID id = DocumentFileBase.newFileUID(8);
		DbMap valuesMap = new DbMap();
		// insert
		valuesMap.put(SF.VERSION, 1);
		valuesMap.put(SF.CREATEDBY, "System");
		valuesMap.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		valuesMap.put(SF.CHANGEDBY, "System");
		valuesMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		valuesMap.put(E.DOCUMENTDIR.getPk(), id);
		valuesMap.put(E.DOCUMENTDIR.parent, parent);
		valuesMap.put(E.DOCUMENTDIR.count, 1);
		try {
			dbAccessPers.execute(new DbInsertStatement<>(E.DOCUMENTDIR, valuesMap));
		} catch (DbException dbEx) {
			// check unique constraint
			DbQueryBuilder builder = dbAccessPers.getQueryBuilder();
			final DbQuery<Long> query = builder.createQuery(Long.class);
			final DbFrom<UID> t = query.from(E.DOCUMENTDIR);
			query.select(builder.countRows());
			query.where(builder.equalValue(t.basePk(), id));
			try {
				if (dbAccessPers.executeQuerySingleResult(query).equals(1L)) {
					// id exist, try next
					return createDocumentDir(parent);
				}
			} catch (Exception ex) {
				// throw new first exception...
			}
			throw dbEx;
		}
		return id;
	}

	private void incrementDocumentDirCount(UID docDirUID, boolean increment) {
		if (docDirUID == null) {
			return;
		}
		DbMap valuesMap = new DbMap();
		// update
		valuesMap.put(SF.VERSION, DbIncrement.INCREMENT);
		valuesMap.put(SF.CHANGEDBY, "System");
		valuesMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		valuesMap.put(E.DOCUMENTDIR.count, increment ? DbIncrement.INCREMENT : DbDecrement.DECREMENT);
		DbMap condition = new DbMap();
		condition.put(E.DOCUMENTDIR.getPk(), docDirUID);
		dbAccessPers.execute(new DbUpdateStatement<>(E.DOCUMENTDIR, valuesMap, condition));
	}
	
	/**
	 * Update file name and version of DocumentFile entry
	 * @param documentFileUID UID of DocumentFile
	 * @param fileName name of uploaded file
	 */
	public void updateDocumentFile(UID documentFileUID, String fileName) {
		try {
			EntityObjectVO<UID> eoVO = getDocFileProcessor().getByPrimaryKey(documentFileUID);
			eoVO.setFieldValue(E.DOCUMENTFILE.filename.getUID(), org.nuclos.common2.StringUtils.trimInvalidCharactersInFilename(fileName, false));
			eoVO.flagUpdate();
			ExtendedDalUtils.updateVersionInformation(eoVO, getCurrentUserName());
			getDocFileProcessor().insertOrUpdate(eoVO);
		} catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	/**
	 * Updates document file links from an old document UID to an new one.
	 * This is used in the "cleanup duplicate documents" console command.
	 * @param oldDocumentFileUID UID of old DocumentFile
	 * @param newDocumentFileUID UID of new DocumentFile 
	 */
	public void updateDocumentFileLinks(UID oldDocumentFileUID, UID newDocumentFileUID) {
		try {
			CollectableComparison compEntityField = SearchConditionUtils.newUidComparison(E.DOCUMENTFILELINK.documentfile, ComparisonOperator.EQUAL, oldDocumentFileUID);
			CollectableSearchExpression searchExpression = new CollectableSearchExpression(compEntityField);
			List<EntityObjectVO<UID>> lst = getDocLinkProcessor().getBySearchExpression(searchExpression);
			if (lst != null && lst.size() == 1) {
				EntityObjectVO<UID> documentLinkEO = lst.get(0);
				UID entityFieldUID = documentLinkEO.getFieldUid(E.DOCUMENTFILELINK.entityfield);
				Object pk = documentLinkEO.getFieldValue(E.DOCUMENTFILELINK.entityobjectuid);
				if (pk == null) {
					pk = documentLinkEO.getFieldValue(E.DOCUMENTFILELINK.entityobjectid);
				}
				FieldMeta<?> fieldMeta = metaProvider.getEntityField(entityFieldUID);
				UID entityUID = fieldMeta.getEntity();
				final IExtendedEntityObjectProcessor<Object> eoProc = dalProv.getEntityObjectProcessor(entityUID);
				EntityObjectVO<Object> eo = eoProc.getByPrimaryKey(pk);
				eo.setFieldUid(entityFieldUID, newDocumentFileUID);
				eo.flagUpdate();
				ExtendedDalUtils.updateVersionInformation(eo, getCurrentUserName());
				eoProc.insertOrUpdate(eo);
				
				documentLinkEO.setFieldUid(E.DOCUMENTFILELINK.documentfile, newDocumentFileUID);
				documentLinkEO.flagUpdate();
				ExtendedDalUtils.updateVersionInformation(eo, getCurrentUserName());
				getDocLinkProcessor().insertOrUpdate(documentLinkEO);
			}
		} catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	/**
	 * Create a DocumentFile link entry.
	 * 
	 * For every reference to a document, a DocumentFileLink entry is created,
	 * which contains the DocumentFile UID, entity field UID and the business objects primary key
	 * @param documentFileUID UID of DocumentFile
	 * @param entityFieldUID UID of entity field witch contains a DocumentFile
	 * @param pk primary key of business object
	 */
	public void createDocumentFileLink(UID documentFileUID, UID entityFieldUID, Object pk) {
		try {
			EntityObjectVO<UID> eoVO = new EntityObjectVO<>(E.DOCUMENTFILELINK);
			eoVO.setPrimaryKey(new UID());
			eoVO.setFieldUid(E.DOCUMENTFILELINK.documentfile.getUID(), documentFileUID);
			eoVO.setFieldUid(E.DOCUMENTFILELINK.entityfield.getUID(), entityFieldUID);
			if (pk instanceof UID) {
				eoVO.setFieldValue(E.DOCUMENTFILELINK.entityobjectuid.getUID(), pk);
			} else {
				eoVO.setFieldValue(E.DOCUMENTFILELINK.entityobjectid.getUID(), pk);
			}
			eoVO.flagNew();
			ExtendedDalUtils.updateVersionInformation(eoVO, getCurrentUserName());
			getDocLinkProcessor().insertOrUpdate(eoVO);
		} catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	/**
	 * Find DocumentFile UID for a specific entity field of a business object
	 * @param entityFieldUID UID of entity field witch contains a DocumentFile
	 * @param pk primary key of business object
	 * @return UID of DocumentFile or null if no DocumentFile exist for the field
	 */
	public UID findDocumentFileUID(UID entityFieldUID, Object pk) {
		try {
			CollectableComparison compEntityField = SearchConditionUtils.newUidComparison(E.DOCUMENTFILELINK.entityfield, ComparisonOperator.EQUAL, entityFieldUID);
			CollectableComparison compObjectId;
			if (pk instanceof UID) {
				compObjectId = SearchConditionUtils.newComparison(E.DOCUMENTFILELINK.entityobjectuid, ComparisonOperator.EQUAL, (UID)pk);
			} else {
				compObjectId = SearchConditionUtils.newComparison(E.DOCUMENTFILELINK.entityobjectid, ComparisonOperator.EQUAL, (Long)pk);
			}
			CompositeCollectableSearchCondition condition = SearchConditionUtils.and(compEntityField, compObjectId);
			List<EntityObjectVO<UID>> eoList = getDocLinkProcessor().getBySearchExpression(new CollectableSearchExpression(condition));
			if (eoList == null || eoList.isEmpty()) {
				return null;
			}
			EntityObjectVO<UID> eoVO = eoList.get(0);
			return eoVO.getFieldUid(E.DOCUMENTFILELINK.documentfile.getUID());
			
		} catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	/**
	 * Delete an document file entry and the physical file (transactional)
	 * @param documentFileUID UID of DocumentFile
	 */
	public void deleteDocumentFile(final UID documentFileUID) {
		try {
			EntityObjectVO<UID> documentFile = getDocFileProcessor().getByPrimaryKey(documentFileUID);
			final UID documentDirUID = documentFile.getFieldUid(E.DOCUMENTFILE.documentDir);
			final File file = getFile(documentFileUID);
			getDocFileProcessor().delete(new Delete<UID>(documentFileUID));
			// register commit handler
			TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
				@Override
				public void afterCompletion(int status) {
					if (status == TransactionSynchronization.STATUS_COMMITTED) {
						// remove file
						if (file.exists()) {
							file.delete();
							incrementDocumentDirCount(documentDirUID, false);
						}
					}
				}
			});
		} catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	/**
	 * Delete an DocumentFileLink entry.
	 * 
	 * If that entry was the last reference, DocumentFile entry and the physical file are deleted as well
	 * @param documentFileUID UID of DocumentFile
	 * @param entityFieldUID UID of entity field witch contains a DocumentFile
	 * @param pk primary key of business object
	 */
	public void deleteDocumentFileLink(final UID documentFileUID, UID entityFieldUID, Object pk) {
		try {
			// find and remove DocumentFileLink
			CollectableComparison compDocumentFile = SearchConditionUtils.newUidComparison(E.DOCUMENTFILELINK.documentfile, ComparisonOperator.EQUAL, documentFileUID);
			CollectableComparison compEntityField = SearchConditionUtils.newUidComparison(E.DOCUMENTFILELINK.entityfield, ComparisonOperator.EQUAL, entityFieldUID);
			CollectableComparison compObjectId;
			if (pk instanceof UID) {
				compObjectId = SearchConditionUtils.newComparison(E.DOCUMENTFILELINK.entityobjectuid, ComparisonOperator.EQUAL, (UID)pk);
			} else {
				compObjectId = SearchConditionUtils.newComparison(E.DOCUMENTFILELINK.entityobjectid, ComparisonOperator.EQUAL, (Long)pk);
			}
			CompositeCollectableSearchCondition condition = SearchConditionUtils.and(compDocumentFile, compEntityField, compObjectId);
			List<UID> pkList = getDocLinkProcessor().getIdsBySearchExpression(new CollectableSearchExpression(condition));
			if (pkList == null || pkList.size() != 1) {
				//throw new IllegalStateException();
				// ignore here. Migration of missing documentfile writes 'null' without a link. 
				return;
			}
			getDocLinkProcessor().delete(new Delete<UID>(pkList.get(0)));
			
			// count remaining links
			Long entries = getDocLinkProcessor().count(new CollectableSearchExpression(compDocumentFile));
			// was last link? -> remove DocumentFile
			if (entries == 0) {
				deleteDocumentFile(documentFileUID);
			}
		} catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}

	@Transactional(propagation= Propagation.REQUIRED, noRollbackFor= {Exception.class})
	void restoreMissingInternalDocumentFilesFromDbBackup() {
		MultiListMap<EntityMeta, FieldMeta> fieldMetasToCheck = new MultiListHashMap<>();
		for (EntityMeta<?> eMeta : E.getAllEntities()) {
			for (FieldMeta<?> efMeta : eMeta.getFields()) {
				if (efMeta.getDocumentFileDbBackupContentField() != null) {
					fieldMetasToCheck.addValue(eMeta, efMeta);
				}
			}
		}
		for (EntityMeta eMeta : fieldMetasToCheck.keySet()) {
			final List<FieldMeta> efMetas = fieldMetasToCheck.getValues(eMeta);
			final IEntityObjectProcessor<UID> proc = nuclosDalProv.getEntityObjectProcessor(eMeta);
			for (EntityObjectVO<UID> eo : proc.getAll()) {
				for (FieldMeta efMeta : efMetas) {
					final UID docUID = eo.getFieldUid(efMeta.getUID());
					GenericObjectDocumentFile doc = (GenericObjectDocumentFile) eo.getFieldValue(efMeta.getUID());
					if (doc != null) {
						try {
							final Pair<String, String> fileNameAndDir = getFileNameAndDir(docUID, nuclosDalProv.getEntityObjectProcessor(E.DOCUMENTFILE));
							final File file = getFile(docUID, fileNameAndDir.getX(), fileNameAndDir.getY());
							if (!file.exists()) {
								// File does not exist... repair from DB
								final byte[] backupContent = eo.getFieldValue(efMeta.getDocumentFileDbBackupContentField(), byte[].class);
								if (backupContent != null) {
									doc = new GenericObjectDocumentFile(doc.getFilename(), doc.getDocumentFilePk(), backupContent);
										final String filePath = NuclosFileUtils.getPathNameForDocument(doc, docUID, fileNameAndDir.getY());
										IOUtils.writeToBinaryFile(new File(filePath), backupContent);
										LOG.warn("DocumentFile {} (Field {}.{}) restored from db backup",
												doc.getFilename(), eMeta.getEntityName(), efMeta.getFieldName());
								}
							}
						} catch (IOException e) {
							LOG.error("Error restoring DocumentFile from db backup for EO {} (Field {}.{}) ",
									eo.getPrimaryKey(), eMeta.getEntityName(), efMeta.getFieldName(), e.getMessage(), e);
						}
					}
				}
			}
		}
	}
	
}
