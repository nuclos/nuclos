//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.eventsupport.ejb3;

import org.nuclos.common.HasCryptoHash;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.server.customcode.codegenerator.GeneratedFile;
import org.nuclos.server.customcode.valueobject.CodeVO;

/**
 * Information about java code compiled by Nuclos
 * (rules, business objects, business support objects) derived from 
 * {@link CodeVO} and/or {@link GeneratedFile}.
 *  
 * @author Thomas Pasch
 * @since Nuclos 3.14.18, 3.15.18, 4.0.15
 */
class CodeInfo implements HasPrimaryKey<UID>, HasCryptoHash {
	
	private final UID uid;
	
	private final String qualifiedName;
	
	private String hashValue;
	
	CodeInfo(CodeVO vo) {
		if (vo == null) {
			throw new NullPointerException();
		}
		this.uid = vo.getPrimaryKey();
		this.qualifiedName = vo.getName();
		if (this.qualifiedName == null) {
			throw new NullPointerException();
		}
	}
	
	CodeInfo(GeneratedFile gf) {
		if (gf == null) {
			throw new NullPointerException();
		}
		this.uid = gf.getPrimaryKey();
		this.qualifiedName = gf.getName();
		this.hashValue = gf.getHashValue();
		if (this.qualifiedName == null) {
			throw new NullPointerException();
		}
	}
	
	@Override
	public UID getPrimaryKey() {
		return uid;
	}

	String getQualifiedName() {
		return qualifiedName;
	}

	public String getHashValue() {
		return hashValue;
	}

	void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}
	
	/**
	 * Equals is based on {@link #getQualifiedName()}.
	 */
	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		if (!(other instanceof CodeInfo)) return false;
		final CodeInfo o = (CodeInfo) other;
		return qualifiedName.equals(o.qualifiedName);
	}
	
	/**
	 * Hashcode is based on {@link #getQualifiedName()}.
	 */
	@Override
	public int hashCode() {
		return 9331 + 17 * qualifiedName.hashCode();
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder("CodeInfo[");
		result.append(qualifiedName);
		if (uid != null) {
			result.append(",id=").append(uid);
		}
		if (hashValue != null) {
			result.append(",hv=").append(hashValue);
		}
		result.append("]");
		return result.toString();
	}

}
