package org.nuclos.server.eventsupport.ejb3;

import java.util.Map;

import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

public class DatasourceRuleContext {

	public enum Type {
		GET_ALL,
		GET_ALL_IDS,
		GET_BY_ID,
		GET_BY_IDS,
		COUNT
	}

	private final String sClzDatasourceRule;
	private final Type type;
	private final SearchExpression<?> searchExpression;
	private final Map<String, Object> mpParams;

	public DatasourceRuleContext(final String sClzDatasourceRule, final Type type,
								 final SearchExpression<?> searchExpression,
								 final Map<String, Object> mpParams) {
		this.sClzDatasourceRule = sClzDatasourceRule;
		this.type = type;
		this.searchExpression = searchExpression;
		this.mpParams = mpParams;
	}

	public String getRuleClass() {
		return sClzDatasourceRule;
	}

	public Type getType() {
		return type;
	}

	public SearchExpression<?> getSearchExpression() {
		return searchExpression;
	}

	public Map<String, Object> getParams() {
		return mpParams;
	}
}
