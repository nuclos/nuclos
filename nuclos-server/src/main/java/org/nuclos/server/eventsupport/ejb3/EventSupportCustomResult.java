package org.nuclos.server.eventsupport.ejb3;

import org.nuclos.common.dal.vo.EntityObjectVO;

public class EventSupportCustomResult<T> {

	private EntityObjectVO<T> eovo;
	private boolean bUpdateAfterExecution;
	
	public boolean getUpdateAfterExecution() {
		return bUpdateAfterExecution;
	}
	
	public void setUpdateAfterExecution(boolean bUpdateAfterExecution) {
		this.bUpdateAfterExecution = bUpdateAfterExecution;
	}
	
	public EntityObjectVO<T> getEntityObjectVO() {
		return eovo;
	}
	
	public void setEntityObjectVO(EntityObjectVO<T> eovo) {
		this.eovo = eovo;
	}
	
	
}
