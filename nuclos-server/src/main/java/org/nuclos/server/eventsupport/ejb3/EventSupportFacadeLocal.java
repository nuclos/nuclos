package org.nuclos.server.eventsupport.ejb3;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import org.nuclos.api.authentication.AuthenticationResult;
import org.nuclos.api.context.communication.CommunicationContext;
import org.nuclos.common.DependentSelection;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.PrintoutTO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosRuleCompileException;
import org.nuclos.server.api.eventsupport.AuthenticationContextImpl;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

public interface EventSupportFacadeLocal {
	
	Collection<EventSupportTransitionVO> getEventSupportsByTransitionUid(UID transUID) 
			throws CommonFinderException, CommonPermissionException  ;
	
	void fireTimelimitEventSupport(UID jobUID, UID mandatorUID, Long iSessionId)
			throws CommonPermissionException, NuclosCompileException, NuclosFatalException;

	<T> void fireInitMandator(EntityObjectVO<T> eoVO) throws NuclosBusinessRuleException, NuclosCompileException;
	
	<T> EntityObjectVO<T> fireSaveEventSupport(EntityObjectVO<T> eoVO,
			String sEventSupportType, boolean isCollectiveProcessing) throws NuclosBusinessRuleException, NuclosCompileException;
	
	<T> void fireDeleteEventSupport(EntityObjectVO<T> eoVO,
			String sEventSupportType, UsageCriteria usagecriteria, boolean isLogical) throws NuclosBusinessRuleException, NuclosCompileException ;
	
	<T> EventSupportCustomResult<T> fireCustomEventSupport(EntityObjectVO<T> eoVO, DependentSelection dependentSelection,
			EventSupportSourceVO eseVO, boolean bIgnoreExceptions) throws NuclosBusinessRuleException, NuclosCompileException;
	
	<T> EntityObjectVO<T> fireStateTransitionEventSupport(
			UID sourceStateUID, UID targetStateUID,
			EntityObjectVO<T> eoVOBefore, String eventClassType)
			throws NuclosBusinessRuleException, CommonFinderException, CommonPermissionException, NuclosCompileException;
	
	<T> EntityObjectVO<T> fireGenerationEventSupport(UID genUid,
			EntityObjectVO<T> target,
			Collection<EntityObjectVO<T>> sources,
			DependentSelection dependentSelection,
			EntityObjectVO paramEoVO, List<String> lstActions,
			Boolean after)  throws NuclosBusinessRuleException, NuclosCompileException, CommonPermissionException;
	
	List<PrintoutTO> firePrintEventSupport(EntityObjectVO<Long> eoVO,
			String sEventSupportType) throws NuclosBusinessRuleException,
			NuclosCompileException;

	<T> void fireCommunicationEventSupport(CommunicationContext context)
			throws NuclosBusinessRuleException, NuclosCompileException;

	void createBusinessObjects(final ForkJoinPool builderThreadPool) throws NuclosCompileException, InterruptedException;

	List<EventSupportEventVO> getEventSupportsByEntityUid(UID entityname, boolean bIncludeIntegrationPointsInResult) throws CommonPermissionException;

	boolean getUsesEventSupportForEntity(UID entityUID, String eventType);
	
	Collection<EntityObjectVO<UID>> getAllServerCode();

	AuthenticationResult executeAuthentication(AuthenticationContextImpl context, final boolean system) throws NuclosBusinessRuleException, NuclosRuleCompileException;

	boolean refreshAuthentication(AuthenticationContextImpl context, final boolean system) throws NuclosBusinessRuleException, NuclosRuleCompileException;

	Object executeDatasourceRuleCall(final DatasourceRuleContext context) throws NuclosBusinessRuleException, NuclosRuleCompileException;

	<PK> Object executeProxyCall(ProxyContext<PK> context) throws NuclosBusinessRuleException, NuclosCompileException;
	
	List<EventSupportSourceVO> findEventSupportsByUsageAndEntity(String sEventclass, UsageCriteria usagecriteria);
	
	/**
	 * commit or rollback a writing proxy call.
	 * runs in autonomous transaction!
	 * 
	 * @param entity
	 * @param proxy
	 * @param commit (true==commit; false=rollback)
	 * @throws NuclosCompileException
	 * @throws NuclosBusinessRuleException
	 */
	void finishProxyCall(UID entity, Object proxy, boolean commit)
			throws NuclosCompileException, NuclosBusinessRuleException;
	
	void invalidateCaches();
	void invalidateCaches(EntityMeta<UID> entity);
}
