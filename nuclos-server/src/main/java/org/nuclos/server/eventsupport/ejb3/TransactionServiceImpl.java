package org.nuclos.server.eventsupport.ejb3;

import org.nuclos.api.service.TransactionService;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.dal.DalUtils;
import org.springframework.stereotype.Component;

@Component("transactionServiceImpl")
public class TransactionServiceImpl implements TransactionService {

    @Override
    public void disableRuleExecutionsForCurrentTransactionOnly() {
		EventSupportCache.disableRuleExecutions();
    }

    @Override
    public void disableBusinessObjectMetaUpdatesForCurrentTransactionOnly() {
        DalUtils.disableVersionUpdates();
    }
}
