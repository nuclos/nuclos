//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.Map;
import java.util.Set;

import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;

public interface ImportStructure {

	String getName();

	String getEncoding();

	Map<UID, ? extends Item> getItems();

	Set<UID> getIdentifiers();

	boolean isInsert();

	boolean isUpdate();

	boolean isDelete();

	UID getEntityUid();
	
	UID getImportStructureUid();

	Set<String> getForbiddenAttributeNames(ParameterProvider paramProvider);

	int getImportSettings();

	String getMode();

}
