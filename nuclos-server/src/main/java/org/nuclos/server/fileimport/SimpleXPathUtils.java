//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.activemq.util.ByteArrayInputStream;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.XMLUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class SimpleXPathUtils {
	
	private static final Pattern WHITESPACE_PAT = Pattern.compile("^\\s*$", Pattern.MULTILINE);
	
	private SimpleXPathUtils() {
		// Never invoked.
	}
	
	public static boolean isAttribute(String elem) {
		return elem.startsWith("@") && elem.indexOf("/") < 0;
	}
	
	public static boolean isElement(String elem) {
		return !elem.startsWith("@") && elem.indexOf("/") < 0;
	}
	
	public static boolean matches(StringBuilder sx, String match) {
		boolean result = false;
		final int idx = sx.lastIndexOf(match); 
		if (idx >= 0) {
			result = idx + match.length() == sx.length();
			if (result && idx > 0) {
				// only match full names
				result = sx.charAt(idx - 1) == '/';
			}
		}
		return result;
	}
	
	public static String removeLastElement(StringBuilder sx) {
		if (sx.length() == 0) {
			throw new IllegalArgumentException("Can't strip empty path");
		}
		final int idx = sx.lastIndexOf("/");
		final int aIdx = sx.lastIndexOf("/@");
		if (idx == aIdx) {
			if (idx < 0) {
				// only one element
				sx.setLength(0);
				return sx.toString();
			} else {
				throw new IllegalArgumentException("Can't strip element on path ending with attribute: " + 
						sx.toString());
			}
		}
		String result = null;
		if (idx >= 0) {
			result = sx.substring(idx + 1);
			sx.setLength(idx);
		}
		return result;
	}
	
	public static String getLast(StringBuilder sx) {
		String result = null;
		final int idx = sx.lastIndexOf("/");
		if (idx >= 0) {
			result = sx.substring(idx + 1);
		}
		return result;
	}
	
	/*
	public static String getLast(StringBuilder sx, int n) {
		if (n < 0) {
			throw new IllegalArgumentException();
		}
		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < n; ++i) {
			if (i + 1 < n) {
				result.insert(0, '/');
			}
			result.insert(0, getLast(sx));
		}
		return result.toString();
	}
	 */
	
	public static StringBuilder removeElements(StringBuilder sx, int level) {
		if (level < 0) {
			throw new IllegalArgumentException();
		}
		final StringBuilder result = new StringBuilder(sx);
		for (int i = 0; i < level; ++i) {
			final String stripped = removeLastElement(result);
			if (stripped.startsWith("@") && i != 0) {
				throw new IllegalStateException(stripped);
			}
		}
		return result;
	}
	
	public static StringBuilder combine(StringBuilder sx, String relativePath) {
		final Pair<Integer,String> levelAndPath = getParentLevelAndPath(relativePath);
		if (levelAndPath == null) {
			throw new IllegalArgumentException(relativePath);
		}
		final StringBuilder result = removeElements(sx, levelAndPath.getX());
		result.append("/").append(levelAndPath.getY());
		return result;
	}
	
	public static Pair<Integer,String> getParentLevelAndPath(String path) {
		int level = 0;
		int idx = 0;
		for (; path.indexOf("../", idx) >= 0; idx += 3) {
			++level;
		}
		
		if (idx > 0) {
			return new Pair<Integer,String>(level, path.substring(idx));
		}
		return null;
	}
	
	public static boolean equal(StringBuilder sx, String s) {
		return sx.indexOf(s) == 0 && sx.length() == s.length();
	}
	
	public static Iterable<XmlParseObject> xml(final byte[] content, final String entityMatch, 
			final Set<String> innerMatch, final String outerMatch) {
		return new Iterable<XmlParseObject>() {
			
			@Override
			public Iterator<XmlParseObject> iterator() {
				return xmlIterator(content, entityMatch, innerMatch, outerMatch);
			}
		};
	}
	
	public static Iterator<XmlParseObject> xmlIterator(byte[] content, String entityMatch, 
			Set<String> innerMatch, String outerMatch) {
		try {
			return new SimpleXPathIterator(content, entityMatch, innerMatch, outerMatch);
		} catch (XMLStreamException e) {
			throw new IllegalStateException(e);
		}
	}
	
	private static class SimpleXPathIterator implements Iterator<XmlParseObject> {
		
		private final String entityMatch;
		private final Set<String> innerMatch;
		private final String outerMatch;
		
		/**
		 * {@link SimpleXPathUtils#getParentLevelAndPath(String)} of <code>outerMatch</code>.
		 */
		private final Pair<Integer,String> levelAndPath;
		
		//
		
		/**
		 * Absolute path of the current processed entityMatch.
		 */
		private String record = null;
		
		/**
		 * One iterator 'result' in case that there is no <code>outerMatch</code>, delivered on end tag 
		 * of <code>entityMatch</code>.
		 * <p>
		 * Several iterator 'result's in case that there is a <code>outerMatch</code>, delivered one
		 * after another on end tag of <code>outerMatch</code>.
		 * </p><p>
		 * (String) absolute path (of trigger) -multi-> ((Map<String,String>) relative XPath -> value)
		 * </p>
		 */
		private final Multimap<String,Map<String, String>> resultMap = ArrayListMultimap.create();
		
		/**
		 * Iterator to <code>resultMap</code>
		 */
		private Iterator<Map<String,String>> currentResult;
		
		private XMLEvent event;
		
		/**
		 * Are we after the end of an tag, but before the beginning of the next?
		 */
		private boolean afterTag = false;
		
		private final XMLEventReader reader;
		
		/**
		 * Current path in parsed XML. 
		 */
		private final StringBuilder path = new StringBuilder();
		
		/**
		 * The relative path of (XPath) properties in respect to the current processed entityMatch.
		 */
		private final StringBuilder entityRelativePath = new StringBuilder();
		
		// contains only attributes and DIRECT content tags
		private final List<Map<String,String>> parentStack = new LinkedList<Map<String,String>>();
		
		//

		SimpleXPathIterator(byte[] content, String entityMatch, 
				Set<String> innerMatch, String outerMatch) 
				throws XMLStreamException {
			
			this.entityMatch = entityMatch;
			this.innerMatch = innerMatch;
			this.outerMatch = outerMatch;
			if (outerMatch != null) {
				this.levelAndPath = getParentLevelAndPath(outerMatch);
				if (this.levelAndPath == null) {
					throw new IllegalArgumentException(outerMatch);
				}
			} else {
				this.levelAndPath = null;
			}
			
			final XMLInputFactory xif = XMLUtils.newSecuredXMLInputFactory();
			xif.setProperty(XMLInputFactory.IS_COALESCING, true);
			reader = XMLUtils.newXmlEventReader(new ByteArrayInputStream(content));
		}
		
		public void xml() throws XMLStreamException {
			final Map<String, String> entityMap = new HashMap<String, String>();
			while (reader.hasNext()) {
				event = reader.nextEvent();
				switch (event.getEventType()) {
				
				case XMLEvent.START_DOCUMENT:
					path.setLength(0);
					break;
					
				case XMLEvent.END_DOCUMENT:
					break;
					
				case XMLEvent.START_ELEMENT:
					afterTag = false;
					final StartElement se = (StartElement) event;
					final String current = se.getName().getLocalPart();
					path.append("/").append(current);
					
					// push new parent
					Map<String,String> parentNode = null;
					if (!parentStack.isEmpty()) {
						parentNode = parentStack.get(parentStack.size() - 1);
					}
					final Map<String,String> myNode = new HashMap<String, String>();
					parentStack.add(myNode);
					
					final int alen = path.length();
					if (record != null) {
						if (entityRelativePath.length() > 0) {
							entityRelativePath.append("/");
						}
						entityRelativePath.append(current);
					}
					if (SimpleXPathUtils.matches(path, entityMatch)) {
						record = path.toString();
						entityRelativePath.setLength(0);
					}
					final int elen = entityRelativePath.length();
					for (Iterator<Attribute> it = se.getAttributes(); it.hasNext();) {
						final Attribute a = it.next();
						final String an = a.getName().getLocalPart();
						final String av = a.getValue();
						
						// add attribute to parent node
						if (parentNode != null) {
							parentNode.put(current + "/@" + an, av);
						}
						// add attributes to myNode
						myNode.put("@" + an, av);
						
						path.append("/@").append(an);
						if (record != null) {
							if (entityRelativePath.length() > 0) {
								entityRelativePath.append("/");
							}
							entityRelativePath.append("@").append(an);
							entityMap.put(entityRelativePath.toString(), av);
						}
						// Can't happen as there could be no BO attributes here! (tp)
						/*
						if (SimpleXPathUtils.matches(path, importstructure.getMatch())) {								
						}
						 */
						path.setLength(alen);
						entityRelativePath.setLength(elen);
					}
					break;
					
				case XMLEvent.END_ELEMENT:
					afterTag = true;
					boolean endRecord = false;
					if (record != null) {
						// Support for empty tags:
						// If there is nothing in the entityMap so far,
						// we store a null value. (tp)
						final String erp = entityRelativePath.toString();
						if (!entityMap.containsKey(erp)) {
							entityMap.put(erp, null);
						}
						
						if (entityRelativePath.length() > 0) {
							SimpleXPathUtils.removeLastElement(entityRelativePath);
						}
						if (SimpleXPathUtils.equal(path, record)) {
							endRecord = true;
							final Map<String,String> singleResult = new HashMap<String, String>(entityMap);
							
							if (outerMatch == null) {
								final String ap = path.toString();
								resultMap.put(ap, singleResult);
								currentResult = resultMap.get(ap).iterator();
								entityMap.clear();
								record = null;
								return;
							} else {
								// we have to wait until we reach the outerMatch
								final StringBuilder ap = removeElements(path, levelAndPath.getX());
								resultMap.put(ap.toString(), singleResult);
								
								// reset entity
								entityMap.clear();
								record = null;
								
								// do NOT return
							}
						}
					}
					
					// process entities with parent ref
					if (outerMatch != null && !resultMap.isEmpty() && record == null) {
						final String pathString = path.toString();
						// for all matching results
						final Collection<Map<String,String>> col = resultMap.get(pathString);
						if (!col.isEmpty()) {
							final Map<String,String> parent = parentStack.get(parentStack.size() - 1);
							final String refValue = parent.get(levelAndPath.getY());
							final Collection<Map<String,String>> em = resultMap.get(pathString);
							for (Map<String,String> m: em) {
								m.put(outerMatch, refValue);
							}
							// return results, if any
							if (!em.isEmpty()) {
								currentResult = em.iterator();
								entityMap.clear();
								return;								
							}
						}
					}
					
					// pop parent
					parentStack.remove(parentStack.size() - 1);
					
					SimpleXPathUtils.removeLastElement(path);
					break;
					
				case XMLEvent.CHARACTERS:
				case XMLEvent.CDATA:
					final Characters ch = (Characters) event;
					final String content = ch.getData();
					final boolean onlyWs = WHITESPACE_PAT.matcher(content).matches();
					
					if (afterTag) {
						// we expect only white spaces here
						if (!onlyWs) {
							throw new IllegalStateException("After a tag: content found, but handle of mixed content is not implemented: '" + 
									content + "'");
						}
					} else {
						// add content to current node
						final int pss = parentStack.size();
						final Map<String, String> mn = parentStack.get(pss - 1);
						mn.put("", content);
						// also add content to parent node
						if (pss >= 2) {
							final Map<String, String> pn = parentStack.get(pss - 2);
							final String currentTag = getLast(path);
							pn.put(currentTag, content);
						}
					}
					
					// We don't support mixed content, only tag OR text within tags.
					if (record != null) {
						if (!afterTag) {
							final String erp = entityRelativePath.toString();
							if (onlyWs) continue;
							// Double entries are only an error if used in matches. (tp)
							if (!entityMap.containsKey(erp) || !innerMatch.contains(erp)) {
								entityMap.put(erp, ch.getData());
							} else {
								throw new IllegalStateException("Two relative XPaths '" + erp + "' within '" + record + "'");
							}
						}
					}
					break;
					
				default:
					// do nothing
				}
			}
		}

		@Override
		public boolean hasNext() {
			boolean result = !resultMap.isEmpty();
			if (!result) {
				try {
					xml();
					result = !resultMap.isEmpty();
				} catch (XMLStreamException e) {
					throw new IllegalStateException(e);
				}
			}
			return result;
		}

		@Override
		public XmlParseObject next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			if (currentResult == null || !currentResult.hasNext()) {
				throw new IllegalStateException();
			}
			final XmlParseObject result = new XmlParseObject(currentResult.next(), event.getLocation());
			if (!currentResult.hasNext()) {
				currentResult = null;
				resultMap.removeAll(path.toString());
			}
			return result;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}
	
	public static Iterable<String> path(final StringBuilder xpath) {
		return new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				return pathIterator(xpath);
			}
		};
	}
	
	public static Iterable<String> path(final String xpath) {
		return new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				return pathIterator(new StringBuilder(xpath));
			}
		};
	}
	
	public static Iterator<String> pathIterator(final StringBuilder xpath) {
		return new Iterator<String>() {
			
			private int idx = 0;
			
			@Override
			public boolean hasNext() {
				return idx < xpath.length();
			}

			@Override
			public String next() {
				int i = xpath.indexOf("/", idx);
				if (i == -1) {
					i = xpath.length();
				}
				final String result = xpath.substring(idx, i);
				idx = i + 1;
				return result;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

}
