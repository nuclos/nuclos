//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport.ejb3;

import java.util.Map;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

import com.google.common.collect.Multimap;

class XmlExportMetaData {

	private Multimap<UID,EntityObjectVO<UID>> e2XmlImport;
	
	private Multimap<UID,EntityObjectVO<UID>> e2XmlImportAttr;
	
	private Multimap<UID,UID> e2f;

	private Multimap<UID,FieldMeta<?>> esReferredByField;
	
	private Multimap<UID,EntityObjectVO<UID>> f2XmlImportFieldIdentifier;
	
	private Map<UID,EntityObjectVO<UID>> f2XmlImportAttr;
	
	XmlExportMetaData() {
	}

	Multimap<UID, EntityObjectVO<UID>> getE2XmlImport() {
		return e2XmlImport;
	}

	void setE2XmlImport(Multimap<UID, EntityObjectVO<UID>> e2XmlImport) {
		this.e2XmlImport = e2XmlImport;
	}

	Multimap<UID, EntityObjectVO<UID>> getE2XmlImportAttr() {
		return e2XmlImportAttr;
	}

	void setE2XmlImportAttr(Multimap<UID, EntityObjectVO<UID>> e2XmlImportAttr) {
		this.e2XmlImportAttr = e2XmlImportAttr;
	}

	Multimap<UID, UID> getE2f() {
		return e2f;
	}

	void setE2f(Multimap<UID, UID> e2f) {
		this.e2f = e2f;
	}

	Multimap<UID, EntityObjectVO<UID>> getF2XmlImportFieldIdentifier() {
		return f2XmlImportFieldIdentifier;
	}

	void setF2XmlImportFieldIdentifier(Multimap<UID, EntityObjectVO<UID>> f2XmlImportFieldIdentifier) {
		this.f2XmlImportFieldIdentifier = f2XmlImportFieldIdentifier;
	}

	Map<UID, EntityObjectVO<UID>> getF2XmlImportAttr() {
		return f2XmlImportAttr;
	}

	void setF2XmlImportAttr(Map<UID, EntityObjectVO<UID>> f2XmlImportAttr) {
		this.f2XmlImportAttr = f2XmlImportAttr;
	}

	Multimap<UID, FieldMeta<?>> getEsReferredByField() {
		return esReferredByField;
	}

	void setEsReferredByField(Multimap<UID, FieldMeta<?>> esReferredByField) {
		this.esReferredByField = esReferredByField;
	}

}
