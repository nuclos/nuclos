package org.nuclos.server.genericobject.ejb3;

import org.nuclos.server.eventsupport.ejb3.SourceType;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler;
import org.springframework.stereotype.Component;

@Component
public class GeneratorObjectCompiler extends AbstractNuclosObjectCompiler {

	public GeneratorObjectCompiler() {
		super(SourceType.GENERATION,
		      NuclosCodegeneratorConstants.GENERATIONJARFILE,
		      NuclosCodegeneratorConstants.GENERATION_SRC_DIR_NAME);
	}

}
