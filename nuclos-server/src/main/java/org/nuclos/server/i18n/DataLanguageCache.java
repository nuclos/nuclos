package org.nuclos.server.i18n;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.nuclos.common.E;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.server.cluster.TransactionalClusterNotification;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataLanguageCache {

	private static final Logger LOG = LoggerFactory.getLogger(DataLanguageCache.class);
	
	@Autowired
	private NuclosUserDetailsContextHolder userCtx;
	
	private List<DataLanguageVO> allDataLanguages;
	private UID primaryLanguage;
	
	public Collection<DataLanguageVO> getDataLanguages() {
		
		if (allDataLanguages == null) {
			ArrayList<DataLanguageVO> allDataLanguages = new ArrayList<>();
			
			NucletDalProvider nucletDalProvider = NucletDalProvider.getInstance();
			nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE)
					.getAll()
					.forEach(eovo -> allDataLanguages.add(new DataLanguageVO(eovo)));

			Collections.sort(allDataLanguages, (o1, o2) -> RigidUtils.compare(o1.getOrder(), o2.getOrder()));
			this.allDataLanguages = allDataLanguages;
		}
		
		return allDataLanguages;
	}
	
	public List<org.nuclos.api.UID> getDataLanguageUIDs() {		
		return CollectionUtils.transform(getDataLanguages(), new Transformer<DataLanguageVO, org.nuclos.api.UID>() {
			@Override
			public org.nuclos.api.UID transform(DataLanguageVO i) {
				return i.getPrimaryKey();
			}
		});
	}
	
	public org.nuclos.api.UID getNuclosPrimaryDataLanguage() {
		if (primaryLanguage == null) {
			for (DataLanguageVO dlvo : getDataLanguages()) {
				if (dlvo.isPrimary()) {
					return dlvo.getPrimaryKey();
				}
			}

            // if it is still null there was no primary data language set - log warning and use first one
            getDataLanguages().stream().findFirst().ifPresent((dataLanguageVO -> {
                primaryLanguage = dataLanguageVO.getPrimaryKey();
                LOG.warn("!!! There is no primary data language set, using the first from database: " + dataLanguageVO.getLanguage());
            }));
		}
		
		return primaryLanguage;
	}
	
	public DataLanguageVO getNuclosPrimaryDataLanguageObject() {
        final AtomicReference<DataLanguageVO> retVal = new AtomicReference<>();
		for (DataLanguageVO dlvo : getDataLanguages()) {
			if (dlvo.isPrimary()) {
				return dlvo;
			}
		}

        // if it is still null there was no primary data language set - log warning and use first one
        getDataLanguages().stream().findFirst().ifPresent((dataLanguageVO -> {
            retVal.set(dataLanguageVO);
            LOG.warn("!!! There is no primary data language set, using the first from database: " + dataLanguageVO.getLanguage());
        }));

		return retVal.get();
	}
	
	public UID getLanguageToUse() {
		return getCurrentUserDataLanguage() != null ? 
				(UID)getCurrentUserDataLanguage() : (UID)getNuclosPrimaryDataLanguage();
	}

	public void invalidate() {
		this.primaryLanguage = null;
		this.allDataLanguages = null;

		TransactionalClusterNotification.notifyGeneralCacheInvalidation();
	}

	public org.nuclos.api.UID getCurrentUserDataLanguage() {
		return this.userCtx.getDataLocal();
	}

	public DataLanguageVO getCurrentUserDataLanguageObject() {
		DataLanguageVO retVal = null;
		
		org.nuclos.api.UID currentUserDataLanguage = getLanguageToUse();
		
		for (DataLanguageVO dlvo : getDataLanguages()) {
			if (currentUserDataLanguage.equals(dlvo.getPrimaryKey())) {
				retVal = dlvo;
				break;
			}
		}
		
		return retVal;
	}

}
