//Copyright (C) 2023  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.i18n;

import javax.annotation.PostConstruct;

import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.IFieldMetaComplementor;
import org.nuclos.common.Mutable;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.UID;
import org.nuclos.server.common.MetaProvider;
import org.springframework.stereotype.Component;

@Component
public class DataLanguageFieldMetaComplementor implements IFieldMetaComplementor {

	private final DataLanguageFacadeRemote dlFacade;

	private final MetaProvider metaProv;

	public DataLanguageFieldMetaComplementor(
			final DataLanguageFacadeRemote dlFacade,
			final MetaProvider metaProv) {
		this.dlFacade = dlFacade;
		this.metaProv = metaProv;
	}

	@PostConstruct
	private void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		metaProv.registerFieldMetaComplementor(this, false);
	}

	@Override
	public void complementFieldMeta(final FieldMetaVO<?> fieldMeta) {
		if (fieldMeta != null && fieldMeta.isLocalized() && fieldMeta.getDataLanguageLocator() == null) {
			fieldMeta.setDataLanguageLocator(new DataLanguageLocator(dlFacade));
		}
	}

	@Override
	public FieldMetaVO<Object> createFieldMeta(final UID fieldUID, final Mutable<Boolean> refreshCache) {
		return null;
	}
}
