package org.nuclos.server.jnlp;

import java.io.File;

import org.nuclos.common2.StringUtils;

public class JarDownloadResource implements Comparable<JarDownloadResource> {
	
	private File file;
	private String jarName;
	private String version;
	private String contentEncoding;
	private String hash;
	private boolean extension;
	private boolean theme;
	private boolean bNative;
	
	public JarDownloadResource() {
	}

	public String getContentType() {
		return "application/x-java-archive";
	}
	
	public File getFile() {
		return file;
	}
	
	public void setFile(File file) {
		this.file = file;
	}

	public String getJarName() {
		return jarName;
	}
	
	public void setJarName(String jarName) {
		this.jarName = jarName;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}

	public String getContentEncoding() {
		return contentEncoding;
	}
	
	public void setContentEncoding(String encoding) {
		this.contentEncoding = encoding;
	}
	
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public boolean isExtension() {
		return extension;
	}

	public void setExtension(boolean extension) {
		this.extension = extension;
	}

	public boolean isTheme() {
		return theme;
	}

	public void setTheme(boolean theme) {
		this.theme = theme;
	}

	public boolean isNative() {
		return bNative;
	}

	public void setNative(boolean bNative) {
		this.bNative = bNative;
	}

	public String getJnlpVersionInfo() {
		if (getVersion() == null && getHash() == null) {
			return null;
		}
		return getVersion() + "_" + getHash();
	}

	public long getLastModified() {
		if (file != null) {
			return file.lastModified();
		}
		return 0;
	}
	
	public int getContentLength() {
		if (file != null) {
			return new Long(file.length()).intValue();
		}
		return 0;
	}

	@Override
	public int compareTo(JarDownloadResource o) {
		return StringUtils.compareIgnoreCase(getJarName(), o.getJarName());
	}
	
}
