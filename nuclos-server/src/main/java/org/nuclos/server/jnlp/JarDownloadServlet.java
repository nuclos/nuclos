package org.nuclos.server.jnlp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.nuclos.common.startup.NuclosEnviromentConstants;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet for webstart jar downloads
 * 
 * For more information visit <a href="http://docs.oracle.com/javase/7/docs/technotes/guides/jweb/networking/compression_formats.html">
 * 									   http://docs.oracle.com/javase/7/docs/technotes/guides/jweb/networking/compression_formats.html</a>
 * 
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class JarDownloadServlet extends AbstractWebstartServlet {
	
	private static final Logger LOG = LoggerFactory.getLogger(JarDownloadServlet.class);
	
	@Override
	protected void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String requestURI = request.getRequestURI();
		LOG.debug("Handling request head: " + requestURI);
		
		JarDownloadResource resource = getResource(requestURI);
		
		response.setContentType(Encode.forJava(resource.getContentType()));
        response.setContentLength(resource.getContentLength());
        if (resource.getJnlpVersionInfo() != null) {
            response.setHeader(JNLP_VERSION_ID, Encode.forJava(resource.getJnlpVersionInfo()));
        }
        if (resource.getLastModified() != 0) {
            response.setDateHeader(LAST_MODIFIED, resource.getLastModified());
        }
        response.sendError(HttpServletResponse.SC_OK);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String requestURI = request.getRequestURI();
		LOG.debug("Handling request: " + requestURI);
		
		JarDownloadResource resource = getResource(requestURI);
		if (resource == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		long ifModifiedSince = request.getDateHeader("If-Modified-Since");
		
		if (ifModifiedSince != -1 &&
                (ifModifiedSince / 1000) >=
                (resource.getLastModified() / 1000)) {
            // see Sample Jnlp Download Servlet for more information (/1000)
            response.sendError(HttpServletResponse.SC_NOT_MODIFIED);
		} else {
			
			response.setContentType(Encode.forJava(resource.getContentType()));
	        response.setContentLength(resource.getContentLength());
	        if (resource.getJnlpVersionInfo() != null) {
	            response.setHeader(JNLP_VERSION_ID, Encode.forJava(resource.getJnlpVersionInfo()));
	        }
	        if (resource.getLastModified() != 0) {
	            response.setDateHeader(LAST_MODIFIED, resource.getLastModified());
	        }
	        response.setHeader(CONTENT_ENCODING, Encode.forJava(resource.getContentEncoding()));
	        FileUtils.copyFile(resource.getFile(), response.getOutputStream());
		}
	}
	
	private JarDownloadResource getResource(String requestURI) throws ServletException {
		final int indexCodebaseUriStart = requestURI.indexOf(NuclosEnviromentConstants.CODEBASE_ENDING);
		final String codebaseUri = requestURI.substring(indexCodebaseUriStart);
		
		if (!isJarFile(codebaseUri)) {
			throw new ServletException("Wrong Servlet for " + requestURI);
		}
		
		JarDownloadResource result = getCodebaseUriToResource().get(codebaseUri);
		if (result != null) {
			LOG.debug("Found jar file: " + result.getFile().getAbsolutePath() + " for " + requestURI);
			return result;
		}
		
		LOG.warn("Jar File '" + requestURI + "' not found!");
		return null;
	}

}
