//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.job;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.nuclos.common.E;
import org.nuclos.common.StackTraceParser;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.job.JobType;
import org.nuclos.common2.KeyEnum;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.job.ejb3.JobControlFacadeLocal;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.NuclosQuartzJob;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains the necessary logic to control quartz job.
 * <br>
 * <b>Responsibilities:</b>
 * <ul>
 *   <li>Prepare a job execution - set job state to running, delete old log data, create a data record in protocol table (calls <code>JobControlFacadeBean</code> for separate transaction)</li>
 *   <li>Execute a job - this must be implemented in specific job classes</li>
 *   <li>Set job execution result (calls <code>JobControlFacadeBean</code> for separate transaction)</li>
 * </ul>
 * All Nuclos Quartz Job must extend this class.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version 01.00.00
 *
 */
public class SchedulableJob extends NuclosQuartzJob {

	private static final Logger LOG = LoggerFactory.getLogger(SchedulableJob.class);

	private static JobControlFacadeLocal jobfacade;

	private static Map<UID, Deque<JobVO>> mpJobQueue = new ConcurrentHashMap<>();

	public SchedulableJob() {
		super(new SchedulableJobImpl());
	}

	public static void process(UID oId, Date dFireTime, Date dNextFireTime) {
		Pair<JobVO, MasterDataVO<Long>> pair = new Pair<>();
		Long lSessionId = null;
		try {
			//prepare job execution: clean protocol table, set 'running' true, create jobrun data record
			pair = getJobFacade().prepare(oId);
			lSessionId = pair.getY().getId().longValue();

			if (pair.getX().getQueue() != null) {
				// NUCLOS-8563 This job is assigned to a queue, thus let it wait in case another queue job is running
				mpJobQueue.putIfAbsent(pair.getX().getQueue(), new LinkedList<>());
				Deque<JobVO> jobQueue = mpJobQueue.get(pair.getX().getQueue());
				jobQueue.add(pair.getX());

				if (jobQueue.size() > 1) {
					synchronized (pair.getX()) {
						pair.getX().wait();
					}
				}
			}

			//result of job execution
			String sResult = null;

			JobType type = KeyEnum.Utils.findEnum(JobType.class, pair.getX().getType());

			//execute TimelimitJob or HealthCheckJob
			if (JobType.TIMELEIMIT.equals(type)) {
				Collection<EntityObjectVO<UID>> mandatorDeps = pair.getX().getDependants().getDataPk(E.JOBMANDATOR.parent);
				if (mandatorDeps != null && !mandatorDeps.isEmpty()) {
					for (EntityObjectVO<UID> mandatorDep : mandatorDeps) {
						UID mandator = mandatorDep.getFieldUid(E.JOBMANDATOR.mandator);
						getJobFacade().setMandatorInUserContext(mandator);
						String sResultMore = new TimelimitJob().execute(pair.getX(), mandator, lSessionId);
						if (sResult == null) {
							sResult = sResultMore;
						} else {
							sResult = sResult + ", " + sResultMore;
						}
					}
				} else {
					sResult = new TimelimitJob().execute(pair.getX(), null, lSessionId);
				}
			} else if (JobType.HEALTHCHECK.equals(type)) {
				sResult = new HealthCheckJob().execute(pair.getX(), null, lSessionId);
			} else {
				throw new IllegalArgumentException("Unknown job type: " + type);
			}
			//set 'running' false, get execution result from protocol table
			getJobFacade().setJobExecutionResult(sResult, dFireTime, dNextFireTime, pair.getX(), pair.getY());
		}
		catch (Exception e) {
			try {
				getJobFacade().setJobExecutionResultError(oId, dFireTime, dNextFireTime, lSessionId, e);
				logStackTrace(e, lSessionId);
			}
			catch (Exception ex) {
				// do not throw exception to quartz, just log it instead and finish the job execution normally
				// throw new NuclosFatalException(e);
				LOG.warn("An error occurred while setting the job execution error result.", ex);
			}
		} finally {
			if (pair.getX() != null && pair.getX().getQueue() != null) {
				// When assigned to a queue, the job must be removed from the queue and start the next one
				mpJobQueue.putIfAbsent(pair.getX().getQueue(), new LinkedList<>());
				Deque<JobVO> jobQueue = mpJobQueue.get(pair.getX().getQueue());
				jobQueue.remove(pair.getX());
				if (!jobQueue.isEmpty()) {
					JobVO jobNext = jobQueue.getFirst();
					synchronized (jobNext) {
						jobNext.notify();
					}
				}
			}
		}
	}

	/**
	 * Writes the complete stacktrace of the given Throwable to the log of the jobrun with the given session-ID.
	 */
	private static void logStackTrace(final Throwable t, final Long sessionId) {
		// Write the full StackTrace to the server log
		LOG.error("Job " + sessionId + " failed with Exception", t);

		// Write the shortended StackTrace to the job lob
		List<String> shortenedStackFrames = new StackTraceParser(t).getShortenedStackFrames();
		for (String frame : shortenedStackFrames) {
			getJobFacade().writeToJobRunMessages(sessionId, "ERROR", frame, null);
		}
	}

	private static class SchedulableJobImpl implements Job {
		
		@Override
		public void execute(JobExecutionContext context) throws JobExecutionException {
			JobDetail jobDetail = context.getJobDetail();
			LOG.info("BEGIN executing job {}", jobDetail.getKey());

			//get the id of data record from jobcontroller
			Object oId = jobDetail.getJobDataMap().get(jobDetail.getKey().getName());

			if (oId instanceof UID)
				process((UID)oId, context.getFireTime(), context.getNextFireTime());
			else
				process(UID.parseUID(oId.toString()), context.getFireTime(), context.getNextFireTime());

			LOG.info("END executing Job {}", jobDetail.getKey());
		}
		
	}

	private static JobControlFacadeLocal getJobFacade() {
		if (jobfacade == null)
			jobfacade = ServerServiceLocator.getInstance().getFacade(JobControlFacadeLocal.class);
		return jobfacade;
	}

	public static Collection<JobVO> getJobQueue(UID jobQueue) {
		Deque<JobVO> jobs = mpJobQueue.get(jobQueue);
		return jobs != null ? Collections.unmodifiableCollection(jobs) : null;
	}
}
