//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.maintenance;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;

import org.nuclos.api.businessobject.Query;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.ClusterServer;
import org.nuclos.businessentity.NucletIntegrationPoint;
import org.nuclos.common.Actions;
import org.nuclos.common.CommandMessage;
import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.Mutable;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.schema.rest.ServerStatus;
import org.nuclos.server.cluster.ClusterConstants;
import org.nuclos.server.cluster.ClusterRegistration;
import org.nuclos.server.cluster.RigidClusterHelper;
import org.nuclos.server.cluster.TransactionalClusterNotification;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.web.activemq.NuclosJMSBrokerTunnelServlet;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


// @Transactional(noRollbackFor = {Exception.class}) <-- Das verbraucht einfach viel zu viele
// Transaktionen (blockUserLogin, getMaintenanceMode, ...),
// Deadlocks weil keine Connections mehr zur DB aufgemacht werden können ist die Folge!
@Component("maintenanceService")
public class MaintenanceFacadeBean extends NuclosFacadeBean implements MaintenanceFacadeLocal, MaintenanceFacadeRemote {

	private static final String MAINTENANCE_MODE_MARKER_FILE_NAME = "maintenance-mode-on";

	private String maintenanceMode = MaintenanceConstants.MAINTENANCE_MODE_OFF;

	@Autowired
	private ServerParameterProvider serverParameterProvider;

	@Autowired
	private ParameterProvider paramProvider;

	private final NuclosDalProvider dalProv;

	private final SecurityCache securityCache;

	private String maintenanceSuperUserName;

	private boolean forceMaintenanceMode;

	private Scheduler jobScheduler;

	public MaintenanceFacadeBean(
			final NuclosDalProvider dalProv,
			final SecurityCache securityCache
	) {
		this.dalProv = dalProv;
		this.securityCache = securityCache;
	}

	private Scheduler getJobScheduler() {
		if (jobScheduler == null) {
			jobScheduler = (Scheduler) SpringApplicationContextHolder.getBean("nuclosScheduler");
		}
		return jobScheduler;
	}

	@PostConstruct
	@Transactional
	private void checkMaintenanceModeMarkerFile() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		final Mutable<Boolean> forceMaintenanceOn = new Mutable<>(Boolean.FALSE);
		if (RigidClusterHelper.isClusterEnabled() &&
			!RigidClusterHelper.isMaster()) // This is a slave
		{
			// Check the master(s), too
			if (dalProv != null) { // During tests null
				dalProv.getEntityObjectProcessor(E.CLUSTER_SERVER)
						.getAll().stream()
						.filter(server -> ClusterConstants.TYPE_MASTER.equals(server.getFieldValue(E.CLUSTER_SERVER.type)))
						.findAny().ifPresent(master -> forceMaintenanceOn.setValue(
								      ClusterConstants.STATUS_MAINTENANCE.equals(master.getFieldValue(E.CLUSTER_SERVER.status))));
			}
		}
		if (forceMaintenanceOn.getValue() ||
				(paramProvider.isEnabled(ParameterProvider.KEY_MAINTENANCE_MODE_SURVIVE_RESTART) && existMaintenanceModeMarkerFile())) {
			maintenanceMode = MAINTENANCE_MODE_ON;
			RigidClusterHelper.writeStatus(ClusterConstants.STATUS_MAINTENANCE);
			// cluster notification is not necessary here
		}
	}

	@Override
	public String getMaintenanceSuperUserName() {
		return maintenanceSuperUserName;
	}



	private boolean checkUserAllowed() {
		String user = getCurrentUserName();
		boolean isMaintenanceAllowed = SecurityCache.getInstance().getAllowedActions(user, null).contains(Actions.ACTION_MAINTENANCE_MODE);
		boolean isSuperuser = SecurityCache.getInstance().isSuperUser(user);
		if(isMaintenanceAllowed || isSuperuser) {
			return true;
		} else {
			throw new AccessDeniedException("user not have the super user flag or the access for the action maintenance mode");
		}
	}

	@RolesAllowed("Login")
	@Override
	public void forceMaintenanceMode() {
		this.forceMaintenanceMode = true;
	}

	@RolesAllowed("Login")
	@Override
	@Transactional(noRollbackFor = {Exception.class})
	public String enterMaintenanceMode(final String maintenanceSuperUserName) {
		checkUserAllowed();

		this.maintenanceSuperUserName = maintenanceSuperUserName;

		if (!MAINTENANCE_MODE_OFF.equals(maintenanceMode)) {
			return maintenanceMode;
		}

		maintenanceMode = MAINTENANCE_MODE_INITIALIZED;
		maintenanceModeInitializeRequestedAt = new Date();

		RigidClusterHelper.writeStatus("Maintenance initialized");
		TransactionalClusterNotification.notifyMaintenance(true);

		new Thread(this::initShutdownSessions).start();

		pauseJobs();

		try {
			enterMaintenanceModeAfterWaitingPeriod(getCompleteWaittimeInSeconds());
			securityCache.removeSessionContextForOthers();
			if (paramProvider != null && paramProvider.isEnabled(ParameterProvider.KEY_MAINTENANCE_MODE_SURVIVE_RESTART)) {
				writeMaintenanceModeMarkerFile();
			}
			RigidClusterHelper.writeStatus(ClusterConstants.STATUS_MAINTENANCE);
			waitForCluster(true);

		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			maintenanceMode = MAINTENANCE_MODE_OFF;
			this.maintenanceSuperUserName = null;
			TransactionalClusterNotification.notifyMaintenance(false);
			resumeJobs();
		}

		String result = getMaintenanceMode();
		LOG.info("enterMaintenanceMode={}", result);
		return result;
	}

	private void waitForCluster(boolean waitForOn) {
		if (RigidClusterHelper.isClusterEnabled() &&
			RigidClusterHelper.isMaster()) {
			// wait for slaves here if responding via rest
			LOG.info("Wait for all running slave nodes...");

			Query<ClusterServer> q = QueryProvider.create(ClusterServer.class);
			q.where(ClusterServer.Type.eq(ClusterConstants.TYPE_SLAVE));
			QueryProvider.execute(q).stream().allMatch(slave -> {
				while (true) {
					if (MaintenanceConstants.MAINTENANCE_MODE_OFF.equals(getMaintenanceMode())) {
						// cancel wait if the maintenance mode has already been exited
						break;
					}
					try {
						final ServerStatus slaveStatus = ClusterRegistration.getRestServerStatus(slave);
						final boolean isMaintenanceOn = Boolean.TRUE.equals(slaveStatus.isMaintenance());
						LOG.info("Slave node ({}) reports server.ready=\"{}\";maintenance=\"{}\"",
								slave.getNuclosTitle(),
								slaveStatus.isReady(),
								isMaintenanceOn);
						if (isMaintenanceOn && waitForOn) {
							break;
						} else if (!isMaintenanceOn && !waitForOn) {
							break;
						}
					} catch (ConnectException | SocketTimeoutException | NoRouteToHostException | UnknownHostException e) {
						LOG.info("Slave node ({}) is not running or is not accessible from master: {}", slave.getNuclosTitle(), e.getMessage());
						break;
					} catch (Exception e) {
						LOG.debug("Slave node ({}) is running, but with an uncertain status: {}. Will check now the cluster server table.", slave.getNuclosTitle(), e.getMessage(), e);
						// check status in db
						final ClusterServer slaveFromDb = ClusterServer.get(slave.getId());
						final boolean isMaintenanceOn = ClusterConstants.STATUS_MAINTENANCE.equals(slaveFromDb.getStatus());
						final boolean isShutdown = ClusterConstants.STATUS_SHUTDOWN.equals(slaveFromDb.getStatus());
						LOG.info("Slave node ({}) last (db) status: {} ({})", slave.getNuclosTitle(), slaveFromDb.getStatus(), slaveFromDb.getLastStatusUpdate());
						if ((isMaintenanceOn || isShutdown) && waitForOn) {
							break;
						} else if (!isMaintenanceOn && !waitForOn) {
							break;
						}
					}
					try {
						Thread.currentThread().sleep(5000);
					} catch (InterruptedException e) {
						LOG.error("Slave node ({}) maintenance mode check failed: {}", slave.getNuclosTitle(), e.getMessage(), e);
					}
				}
				return true;
			});
		}
	}

	private File getSystemPathDir() {
		final File systemPathDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.SYSTEM_PATH);
		if (systemPathDir == null) {
			LOG.error("System parameter '{}' not set. ", NuclosSystemParameters.SYSTEM_PATH);
			return null;
		}
		try {
			return systemPathDir.getCanonicalFile();
		} catch (IOException e) {
			LOG.error("Unable to get system path dir '{}'. ", NuclosSystemParameters.getString(NuclosSystemParameters.SYSTEM_PATH));
			return null;
		}
	}

	private synchronized boolean existMaintenanceModeMarkerFile() {
		try {
			final File systemPath = getSystemPathDir();
			if (systemPath != null && systemPath.exists()) {
				File maintenanceModeMarkerFile = new File(systemPath, MAINTENANCE_MODE_MARKER_FILE_NAME);
				if (maintenanceModeMarkerFile.exists()) {
					return true;
				}
			}
		} catch (Exception e) {
			LOG.error("Error while checking maintenance mode marker file.", e);
		}
		return false;
	}
	
	private synchronized void writeMaintenanceModeMarkerFile() {
		try {
			File systemPath = getSystemPathDir();
			if (systemPath != null) {
				if (!systemPath.exists()) {
					systemPath.mkdirs();
				}
				Path maintenanceModeMarkerFile = Paths.get(systemPath.getAbsolutePath(), MAINTENANCE_MODE_MARKER_FILE_NAME);
				Files.createFile(maintenanceModeMarkerFile);
			}
		} catch (Exception e) {
			LOG.error("Error while writing maintenance mode marker file.", e);
		}
	}
	
	private synchronized  void deleteMaintenanceModeMarkerFile() {
		try {
			File systemPath = getSystemPathDir();
			if (systemPath != null && systemPath.exists()) {
				Path maintenanceModeMarkerFile = Paths.get(systemPath.getAbsolutePath(), MAINTENANCE_MODE_MARKER_FILE_NAME);
				Files.deleteIfExists(maintenanceModeMarkerFile);
			}
		} catch (Exception e) {
			LOG.error("Error while deleting maintenance mode marker file.", e);
		}
	}

	/**
	 * wait until all jobs are done and all users are logged out
	 *
	 * @param waitMaxSeconds max time to wait in minutes
	 */
	private void enterMaintenanceModeAfterWaitingPeriod(final int waitMaxSeconds) {

		long timestamp = new Date().getTime();
		long endTimestamp = timestamp + waitMaxSeconds * 1000;
		try {

			while (
					maintenanceMode.equals(MaintenanceConstants.MAINTENANCE_MODE_INITIALIZED)
					&& (!getJobScheduler().getCurrentlyExecutingJobs().isEmpty() || getNumberOfRecentActiveUsers(maintenanceSuperUserName) > 0)
					&& new Date().getTime() < endTimestamp
					&& !forceMaintenanceMode
			) {
				LOG.info(
						"Waiting to start maintenance mode. {} executing jobs. {} open sessions. Wait: {}",
						getJobScheduler().getCurrentlyExecutingJobs().size(),
						getNumberOfRecentActiveUsers(maintenanceSuperUserName),
						endTimestamp - new Date().getTime()
				);
				Thread.sleep(1000);

				LOG.info("Running jobs: {}, open sessions: {}",
						getJobScheduler().getCurrentlyExecutingJobs().size(),
						getNumberOfRecentActiveUsers(maintenanceSuperUserName));
			}

			this.forceMaintenanceMode = false;

			if (maintenanceMode.equals(MAINTENANCE_MODE_INITIALIZED)) {
				maintenanceMode = MAINTENANCE_MODE_ON;
				this.maintenanceModeInitializeRequestedAt = null;
				LOG.info("Maintenance mode started.");
			}

			if (!getJobScheduler().getCurrentlyExecutingJobs().isEmpty()) {
				throw new IllegalStateException("There are still " + getJobScheduler().getCurrentlyExecutingJobs().size() + " running job(s).");
			}

		} catch (InterruptedException e) {
			LOG.error("Unable to sleep.", e);
			Thread.currentThread().interrupt();
		} catch (SchedulerException e) {
			LOG.error("Unable to get JobScheduler information.", e);
		}
	}


	private Date maintenanceModeInitializeRequestedAt = null;

	/**
	 * @return the remaining time until the maintenance mode will entered in seconds
	 */
	@Override
	public synchronized Integer getWaittimeInSeconds() {
		if (MaintenanceConstants.MAINTENANCE_MODE_ON.equals(maintenanceMode)) {
			return 0;
		}
		if (maintenanceModeInitializeRequestedAt == null) {
			return null;
		}
		Calendar waitUntilCal = Calendar.getInstance();
		waitUntilCal.setTime(maintenanceModeInitializeRequestedAt);
		waitUntilCal.add(Calendar.SECOND, getCompleteWaittimeInSeconds());
		return ((int) (waitUntilCal.getTime().getTime() - new Date().getTime())) / 1000;
	}

	@Override
	public void throwRecompileOutsideMaintenanceIfNecessary() throws CommonPermissionException {
		final boolean isProductionEnvironment = NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_PRODUCTION);
		if (isProductionEnvironment && !MaintenanceConstants.MAINTENANCE_MODE_ON.equals(getMaintenanceMode())) {
			throw new CommonPermissionException("recompile.outside.maintenance");
		}
	}

	@Override
	@RolesAllowed("Login")
	@Transactional(noRollbackFor = {Exception.class})
	public String exitMaintenanceMode() throws CommonValidationException {
		checkUserAllowed();
		final boolean isProductionEnvironment = NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_PRODUCTION);
		if (isProductionEnvironment) {
			final Query<NucletIntegrationPoint> qIp = QueryProvider.create(NucletIntegrationPoint.class);
			qIp.where(NucletIntegrationPoint.Problem.eq(true));
			if (!QueryProvider.execute(qIp).isEmpty()) {
				throw new CommonValidationException("integration.point.with.problem");
			}
		}
		maintenanceMode = MAINTENANCE_MODE_OFF;
		maintenanceSuperUserName = null;
		cancelShutdownSessions();
		if (paramProvider != null && paramProvider.isEnabled(ParameterProvider.KEY_MAINTENANCE_MODE_SURVIVE_RESTART)) {
			deleteMaintenanceModeMarkerFile();
		}
		resumeJobs();
		LOG.info("exitMaintenanceMode");

		String result = getMaintenanceMode();
		LOG.info("exitMaintenanceMode={}", result);

		RigidClusterHelper.writeStatus(ClusterConstants.STATUS_RUNNING);
		TransactionalClusterNotification.notifyMaintenance(false);
		waitForCluster(false);

		return result;
	}


	@Override
	public String getMaintenanceMode() {
		return maintenanceMode;
	}

	private void resumeJobs() {
		try {
			final Scheduler scheduler = getJobScheduler();
			if (scheduler.isInStandbyMode()) {
			    scheduler.start();
            }
		} catch (SchedulerException e) {
			LOG.error("Error while trying to resume jobs.", e);
		}
	}

	private void pauseJobs() {
		try {
			getJobScheduler().standby();
		} catch (SchedulerException e) {
			LOG.error("Unable to pause JobScheduler.", e);
		}
	}


	private void initShutdownSessions() {
		LOG.info("JMS send killSession to all users.");
		final CommandMessage cm = new CommandMessage(CommandMessage.CMD_SHUTDOWN);
		cm.setShutdownWaitTimeInSeconds(getCompleteWaittimeInSeconds());

		try {
			NuclosJMSUtils.sendObjectMessage(cm, JMSConstants.TOPICNAME_RULENOTIFICATION, null);
		} catch (IllegalStateException e) {
			LOG.warn("Error while calling initShutdownSessions.");
		}
	}

	private void cancelShutdownSessions() {
		LOG.info("cancel JMS send killSession to all users.");
		final CommandMessage cm = new CommandMessage(CommandMessage.CMD_CANCEL_SHUTDOWN);
		try {
			NuclosJMSUtils.sendObjectMessage(cm, JMSConstants.TOPICNAME_RULENOTIFICATION, null);
		} catch (IllegalStateException e) {
			LOG.warn("Error while calling cancelShutdownSessions.");
		}
	}


	/**
	 * @return true if the server is in maintenance mode
	 * and the given user is not a superuser
	 * or another superuser has already access to the system
	 */
	@Override
	public boolean blockUserLogin(String username) {
		// block login if username is null for security purposes
		if (username == null) {
			return true;
		}

		if ("anonymousUser".equals(username)) { // before client login
			return false;
		}

		if (MAINTENANCE_MODE_OFF.equals(maintenanceMode)) {
			return false;
		}

		if (SecurityCache.getInstance().isSuperUser(username) || SecurityCache.getInstance().isMaintenanceUser(username)) {
			return false;
		}

		return !LangUtils.equal(maintenanceSuperUserName, username);
	}

	@Override
	public long getNumberOfRecentActiveSessions() {
		return securityCache.getOtherSessionContextCount();
	}

	@Override
	public long getNumberOfRecentActiveUsers(final String excludeUsername) {
		return NuclosJMSBrokerTunnelServlet.getNrOfRecentActiveUsers(excludeUsername);
	}

	@Override
	public List<String> getRecentActiveUsers() {
		return securityCache.getOtherActiveUserNames();
	}

	@Override
	public Integer getNumberOfRunningJobs() {
		try {
			return getJobScheduler().getCurrentlyExecutingJobs().size();
		} catch (SchedulerException e) {
			LOG.error("Unable to get JobScheduler information.", e);
		}
		return null;
	}

	public boolean isMaintenanceOff() {
		return MaintenanceConstants.MAINTENANCE_MODE_OFF.equals(getMaintenanceMode());
	}

	@Override
	public boolean isProductionEnvironment() {
		return NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_PRODUCTION);
	}

	@Override
	public boolean isDevelopmentEnvironment() {
		return NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT);
	}

	@Override
	public int getMaintenaceInitWaitTimeInMinutes() {
		int waitTimeInMinutes = MAINTENANCE_INIT_WAIT_TIME_IN_MINUTES;
		if (serverParameterProvider != null && serverParameterProvider.getValue(ParameterProvider.KEY_MAINTENANCE_INIT_WAIT_TIME_IN_MINUTES) != null) {
			waitTimeInMinutes = serverParameterProvider.getIntValue(ParameterProvider.KEY_MAINTENANCE_INIT_WAIT_TIME_IN_MINUTES, MAINTENANCE_INIT_WAIT_TIME_IN_MINUTES);
		}
		return waitTimeInMinutes;
	}
}
