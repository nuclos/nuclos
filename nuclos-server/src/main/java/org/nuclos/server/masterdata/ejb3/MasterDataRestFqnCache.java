package org.nuclos.server.masterdata.ejb3;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.commons.collections15.bidimap.UnmodifiableBidiMap;
import org.apache.commons.lang.NotImplementedException;
import org.nuclos.cache.IFqnCache;
import org.nuclos.common.AbstractDisposableRegistrationBean;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.attribute.ejb3.LayoutObjectBuilder;
import org.nuclos.server.cluster.TransactionalClusterNotification;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.genericobject.ejb3.GeneratorObjectBuilder;
import org.nuclos.server.job.JobObjectBuilder;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.printservice.printout.PrintoutObjectBuilder;
import org.nuclos.server.printservice.printout.PrintoutObjectBuilder.OutputFormatVisitor;
import org.nuclos.server.report.ChartDatasourceObjectBuilder;
import org.nuclos.server.report.ReportDatasourceObjectBuilder;
import org.nuclos.server.statemodel.StatemodelObjectBuilder;
import org.nuclos.server.user.UserRoleObjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class MasterDataRestFqnCache extends AbstractDisposableRegistrationBean implements INucletCache, IFqnCache {

	private static final Logger LOG = LoggerFactory.getLogger(MasterDataRestFqnCache.class);
	
	private static final Set<UID> FQN_ENABLED = new HashSet<UID>();
	static {
		FQN_ENABLED.add(E.NUCLET.getUID());
		FQN_ENABLED.add(E.ENTITY.getUID());
		FQN_ENABLED.add(E.ENTITYFIELD.getUID());
		FQN_ENABLED.add(E.PROCESS.getUID());
		FQN_ENABLED.add(E.STATEMODEL.getUID());
		FQN_ENABLED.add(E.STATE.getUID());
		FQN_ENABLED.add(E.LAYOUT.getUID());
		FQN_ENABLED.add(E.GENERATION.getUID());
		FQN_ENABLED.add(E.REPORT.getUID());
		FQN_ENABLED.add(E.REPORTOUTPUT.getUID());
		FQN_ENABLED.add(E.DATASOURCE.getUID());
		FQN_ENABLED.add(E.CHART.getUID());
		FQN_ENABLED.add(E.JOBCONTROLLER.getUID());
		FQN_ENABLED.add(E.ROLE.getUID());
	}

	private Collection<EntityMeta<?>> entityMetas = null;

	private final NuclosDalProvider nuclosDalProvider;
	private final MetaProvider metaprovider;
	private final MasterDataFacadeLocal mdfacade;

	public MasterDataRestFqnCache(final NuclosDalProvider nuclosDalProvider,
								  final MetaProvider metaprovider,
								  final MasterDataFacadeLocal mdfacade) {
		this.nuclosDalProvider = nuclosDalProvider;
		this.metaprovider = metaprovider;
		this.mdfacade = mdfacade;
	}

	@PostConstruct
	private void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		createSyncFlowableWithDropStrategyAndRegister(metaprovider.observeEntityMetaRefreshsAfterCommit(), this::invalidateCache);
		FQN_ENABLED.forEach(entityUID ->
				createSyncFlowableWithDropStrategyAndRegister(nuclosDalProvider.getEntityObjectProcessor(entityUID)
								.observeAllChangesImmediately(true)
								.debounce(500, TimeUnit.MILLISECONDS),
						notification -> {
							try {
								invalidateCacheForEntity(entityUID);
							} catch (Exception ex) {
								LOG.warn("FQN cache invalidation for entity " + entityUID + " failed:" + ex.getMessage());
								LOG.debug("Error: ", ex);
							}
						}));
	}

	@Override
	public String getFullQualifiedNucletName(UID nucletUID) {
		String result = getRestFqnUidMap(E.NUCLET).get(nucletUID);
		if (result != null) {
			result = validateFqn(result);
		}
		return result;
	}
	
	@Override
	public UID translateFqn(EntityMeta<?> eMeta, String fqn) {
		if (fqn == null) {
			throw new IllegalArgumentException("fqn must not be null");
		}
		BidiMap<UID, String> bidiMap = eMeta==null?null:getRestFqnUidMap(eMeta);
		final UID result;
		if (bidiMap == null) {
			// not enabled or eMeta is null
			result = UID.parseUID(fqn);
		} else {
			result = bidiMap.getKey(fqn);
		}
		if (result == null) {
			// special case like calc attribute...
			return UID.parseUID(fqn);
		}
		return result;
	}

	/**
	 * Tries to guess the EntityMeta and translate the given UID.
	 *
	 * TODO: If the cache was not splitted by entity, we could use it directly.
	 *
	 * @param uid
	 * @return
	 */
	@Override
	public String translateUid(
			@NotNull final UID uid
	) {
		for (UID enabledEntityUID: FQN_ENABLED) {
			EntityMeta<Object> entityMeta = E.getByUID(enabledEntityUID);
			String result = translateUid(entityMeta, uid, null);
			if (result != null) {
				return result;
			}
		}
		return uid.toString();
	}

	/**
	 * Tries to translate the given UID for the given EntityMeta.
	 * Returns the stringified UID if the translation was not successful.
	 *
	 * @param eMeta
	 * @param uid
	 * @return
	 */
	@Override
	public String translateUid(
			final EntityMeta<?> eMeta,
			final UID uid
	) {
		if (uid == null) {
			throw new IllegalArgumentException("uid must not be null");
		}

		final String defaultValue = uid.toString();
		return translateUid(eMeta, uid, defaultValue);
	}

	/**
	 * Tries to translate the given UID for the given EntityMeta.
	 * Returns the given default value if the translation was not successful.
	 *
	 * @param eMeta
	 * @param uid
	 * @param defaultValue
	 * @return
	 */
	private String translateUid(
			final EntityMeta<?> eMeta,
			final UID uid,
			final String defaultValue
	) {
		String result = null;
		BidiMap<UID, String> bidiMap = eMeta == null ? null : getRestFqnUidMap(eMeta);
		if (bidiMap != null) {
			result = bidiMap.get(uid);
		}

		if (result == null) {
			// special case like calc attribute...
			result = defaultValue;
		}

		return result;
	}

	@CacheEvict(value="masterDataRestFqnCache", allEntries=true)
	public void evictCompleteCache() {
	}
	
	@CacheEvict(value="masterDataRestFqnCache", key="#p0")
	public void evictCacheForEntity(UID entityUID) {
	}

	@Override
	public synchronized <PK> void invalidateCacheForEntityObjectIfNecessary(final EntityMeta<?> entityMeta, EntityObjectVO<PK> entityObject) {
		if (entityObject == null) {
			// fallback
			invalidateCacheForEntity(entityMeta.getUID());
			return;
		}
		if (!entityMeta.getUID().equals(entityObject.getDalEntity())) {
			throw new IllegalArgumentException(String.format("entityMeta.getUID[%s] != entityObject.getDalEntity[%s]", entityMeta.getUID(), entityObject.getDalEntity()));
		}
		if (!FQN_ENABLED.contains(entityMeta.getUID())) {
			return;
		}
		boolean invalidateCacheForEntity = false;
		try {
			final String fqn = buildFqn(entityObject);
			final String translateUid = translateUid(entityMeta, (UID) entityObject.getPrimaryKey());
			if (!RigidUtils.equal(fqn, translateUid)) {
				// fqn changed -> invalidate cache for complete entity
				invalidateCacheForEntity = true;
			}
		} catch (NotImplementedException e) {
			// ignore here -> entity, entityfield special cases
			invalidateCacheForEntity = true;
		}
		if (invalidateCacheForEntity) {
			invalidateCacheForEntity(entityMeta.getUID());
		}
	}

	public static boolean isEntityCached(UID entityUID) {
		return FQN_ENABLED.contains(entityUID);
	}

	private void invalidateCache(Collection<EntityMeta<?>> entityMetas) {
		this.entityMetas = entityMetas;
		evictCompleteCache();
	}

	@Override
	public synchronized void invalidateCacheForEntity(UID entityUID) {
		if (!FQN_ENABLED.contains(entityUID)) {
			return;
		}
		if (E.NUCLET.checkEntityUID(entityUID)) {
			evictCompleteCache();
			return;
		}
		if (E.ENTITY.checkEntityUID(entityUID)) {
			invalidateCacheForEntity(E.ENTITYFIELD.getUID());
			invalidateCacheForEntity(E.PROCESS.getUID());
		} else if (E.STATEMODEL.checkEntityUID(entityUID)) {
			invalidateCacheForEntity(E.STATE.getUID());
		} else if (E.REPORT.checkEntityUID(entityUID)) {
			invalidateCacheForEntity(E.REPORTOUTPUT.getUID());
		} else if (E.FORM.checkEntityUID(entityUID)) {
			invalidateCacheForEntity(E.REPORT.getUID());
		}
		evictCacheForEntity(entityUID);

		if (E.ROLE.checkEntityUID(entityUID)) {
			TransactionalClusterNotification.notifySecurityRelatedChangeForAllUsers();
		} else {
			TransactionalClusterNotification.notifyGeneralCacheInvalidation();
		}
	}

	/**
	 * TODO: Do a proper generic lookup here instead of cumbersome if-else switches over all entities!
	 * <p>
	 * TODO: Why do we have separate Maps per entity?
	 * The point of UIDs is to have globally unique IDs, so 1 Map should be enough.
	 * The only use of separate maps seems to be cache invalidation for specific entities.
	 *
	 * @param eMeta
	 * @return
	 */
	@Cacheable(value="masterDataRestFqnCache", key="#p0.getUID()")
	public BidiMap<UID, String> getRestFqnUidMap(EntityMeta<?> eMeta) {
		return createRestFqnUidMap(eMeta);
	}

	@Transactional(propagation= Propagation.REQUIRED, noRollbackFor= {Exception.class})
	public synchronized BidiMap<UID, String> createRestFqnUidMap(EntityMeta<?> eMeta) {
		if (!FQN_ENABLED.contains(eMeta.getUID())) {
			return null;
		}
		if (!eMeta.isUidEntity()) {
			throw new IllegalArgumentException("Entity " + eMeta.getEntityName() + " has to be a UID entity");
		}
		final BidiMap<UID, String> result = new DualHashBidiMap<UID, String>();

		final Collection<EntityMeta<?>> allEntityMetas = Optional.ofNullable(entityMetas).orElseGet(metaprovider::getAllEntities);

		// use special case for entity and entityfield. provider contains more entities, like dynamic.
		if (E.ENTITY.equals(eMeta)) {
			for (EntityMeta<?> entity : allEntityMetas) {
				String fqn = NuclosBusinessObjectBuilder.getNucletPackageStatic(entity.getUID(), 
						entity.getNuclet(), this, false) +
						"_" + NuclosBusinessObjectBuilder.getNameForFqn(NuclosEntityValidator.getFormattedEntityNameForSystemEntity(entity.getBusinessObjectClassName()));
				fqn = validateFqn(fqn);
				result.put(entity.getUID(), fqn);
			}
		} else if (E.ENTITYFIELD.equals(eMeta)) {
			BidiMap<UID, String> entityMap = getRestFqnUidMap(E.ENTITY);
			for (EntityMeta<?> entity : allEntityMetas) {
				for (FieldMeta<?> field : entity.getFields()) {
					try {
						String fqnEntity = entityMap.get(entity.getUID());
						String fqn = fqnEntity + "_" + NuclosBusinessObjectBuilder.getFieldNameForFqn(field);
						fqn = validateFqn(fqn);
						result.put(field.getUID(), fqn);
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
						//continue building
					}
				}
			}
		} 
		
		// special case for report output formats.
		else if (E.REPORTOUTPUT.equals(eMeta)) {
			BidiMap<UID, String> reportMap = getRestFqnUidMap(E.REPORT);
			for (final UID reportUID : reportMap.keySet()) {
				final String fqnReport = reportMap.get(reportUID); 
				PrintoutObjectBuilder.visitOutputFormats(reportUID, new OutputFormatVisitor() {
					@Override
					public void visitOutputFormat(UID outputFormatUID, String fullyQualifiedName) {
						String fqn = fqnReport + "_" + fullyQualifiedName;
						fqn = validateFqn(fqn);
						result.put(outputFormatUID, fqn);
					}
				});
			}
		} 
		
		// default masterdata way for all other entities, including the XML Entities (very important)
		else {
			for (MasterDataVO<?> mdvo : mdfacade.getMasterData(eMeta, null)) {
				final UID pk = (UID) mdvo.getPrimaryKey();
				result.put(pk, buildFqn(mdvo.getEntityObject()));
			}
		}
		return UnmodifiableBidiMap.decorate(result);
	}

	private <PK> String buildFqn(EntityObjectVO<PK> eo) {
		final UID entityUID = eo.getDalEntity();
		if (!FQN_ENABLED.contains(eo.getDalEntity())) {
			return null;
		}
		if (!(eo.getPrimaryKey() instanceof UID)) {
			throw new IllegalArgumentException("Entity " + eo.getDalEntity() + " has to be a UID entity");
		}
		final UID pk = (UID) eo.getPrimaryKey();
		final String fqn;

		if (E.NUCLET.checkEntityUID(entityUID)) {
			fqn = eo.getFieldValue(E.NUCLET.packagefield);
		} else if (E.PROCESS.checkEntityUID(entityUID)) {
			BidiMap<UID, String> entityMap = getRestFqnUidMap(E.ENTITY);
			String fqnEntity = entityMap.get(eo.getFieldUid(E.PROCESS.module));
			fqn = fqnEntity + "_" + NuclosBusinessObjectBuilder.getProcessNameForFqn(eo.getFieldValue(E.PROCESS.name));
		} else if (E.STATEMODEL.checkEntityUID(entityUID)) {
			fqn = StatemodelObjectBuilder.getNucletPackageStatic(eo.getFieldUid(E.STATEMODEL.nuclet), this) +
					"_" + StatemodelObjectBuilder.getNameForFqn(eo.getFieldValue(E.STATEMODEL.name));
		} else if (E.STATE.checkEntityUID(entityUID)) {
			BidiMap<UID, String> statemodelMap = getRestFqnUidMap(E.STATEMODEL);
			String fqnStatemodel = statemodelMap.get(eo.getFieldUid(E.STATE.model));
			fqn = fqnStatemodel + "_" + StatemodelObjectBuilder.getStateNameForFqn(eo.getFieldValue(E.STATE.numeral));
		} else if (E.LAYOUT.checkEntityUID(entityUID)) {
			fqn = LayoutObjectBuilder.getNucletPackageStatic(eo.getFieldUid(E.LAYOUT.nuclet), this) +
					"_" + LayoutObjectBuilder.getNameForFqn(eo.getFieldValue(E.LAYOUT.name));
		} else if (E.GENERATION.checkEntityUID(entityUID)) {
			fqn = GeneratorObjectBuilder.getNucletPackageStatic(eo.getFieldUid(E.GENERATION.nuclet), this) +
					"_" + GeneratorObjectBuilder.getNameForFqn(eo.getFieldValue(E.GENERATION.name));
		} else if (E.REPORT.checkEntityUID(entityUID)) {
			fqn = PrintoutObjectBuilder.getNucletPackageStatic(eo.getFieldUid(E.REPORT.nuclet), this) +
					"_" + PrintoutObjectBuilder.getNameForFqn(eo.getFieldValue(E.REPORT.name));
		} else if (E.DATASOURCE.checkEntityUID(entityUID)) {
			fqn = ReportDatasourceObjectBuilder.getNucletPackageStatic(eo.getFieldUid(E.DATASOURCE.nuclet), this) +
					"_" + ReportDatasourceObjectBuilder.getNameForFqn(eo.getFieldValue(E.DATASOURCE.name));
		} else if (E.CHART.checkEntityUID(entityUID)) {
			fqn = ChartDatasourceObjectBuilder.getNucletPackageStatic(eo.getFieldUid(E.CHART.nuclet), this) +
					"_" + ChartDatasourceObjectBuilder.getNameForFqn(eo.getFieldValue(E.CHART.name));
		} else if (E.JOBCONTROLLER.checkEntityUID(entityUID)) {
			fqn = JobObjectBuilder.getNucletPackageStatic(eo.getFieldUid(E.JOBCONTROLLER.nuclet), this) +
					"_" + JobObjectBuilder.getNameForFqn(eo.getFieldValue(E.JOBCONTROLLER.name));
		} else if (E.ROLE.checkEntityUID(entityUID)) {
			fqn = UserRoleObjectBuilder.getNucletPackageStatic(eo.getFieldUid(E.ROLE.nuclet), this) +
					"_" + UserRoleObjectBuilder.getNameForFqn(eo.getFieldValue(E.ROLE.name));
		} else {
			throw new NotImplementedException("Default FQN to UID transformation for entity " + entityUID + " not implemented yet");
		}
		String validatedFqn = validateFqn(fqn);
		return validatedFqn;
	}

	/**
	 * TODO: A simple string replacement is no validation!
	 *
	 * @param sName
	 * @return
	 */
	private static String validateFqn(String sName) {
		return sName.replace('.', '_');
	}
}
