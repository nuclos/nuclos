package org.nuclos.server.nbo;

import org.nuclos.server.eventsupport.ejb3.SourceType;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.springframework.stereotype.Component;

@Component
public class NuclosBusinessObjectCompiler extends AbstractNuclosObjectCompiler {

	public NuclosBusinessObjectCompiler() {
		super(SourceType.BO_ENTITIES,
		      NuclosCodegeneratorConstants.BOJARFILE,
		      NuclosCodegeneratorConstants.BO_SRC_DIR_NAME);
	}

}
