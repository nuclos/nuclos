package org.nuclos.server.nbo;

import org.nuclos.common.UID;

public class NuclosBusinessObjectDepConstantMetaData extends
		NuclosBusinessObjectConstantMetaData {

	public NuclosBusinessObjectDepConstantMetaData(String pName, String pEntityName, UID entityUid, 
				String pForeignKeyFieldName, UID pForeignKeyFieldId, String pType) {
		super(pName, null, pEntityName, entityUid, pForeignKeyFieldName, pForeignKeyFieldId, pType, null, null, null);
	}

	public boolean equals(Object obj) {
		 boolean retVal = false;
		
		 if (obj instanceof NuclosBusinessObjectDepConstantMetaData) {
			 NuclosBusinessObjectDepConstantMetaData metaData = (NuclosBusinessObjectDepConstantMetaData) obj;
			 
			 if (metaData.getAttributeName().equals(this.getAttributeName()))
				 	retVal = true;
		 }
		 return retVal;
	}
	
}
