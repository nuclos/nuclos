//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.print.DocFlavor;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;

import org.fusesource.hawtbuf.ByteArrayInputStream;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.context.PrintResult;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.printservice.PrintServiceLocator;
import org.nuclos.common.report.print.PDFPrintJob;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.common2.exception.CommonPrintException;
import org.nuclos.common2.exception.PrintoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * {@link MultiPrintResultJob} sends {@link PrintResult} to 
 * ths system printservice
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class MultiPrintResultJob {
	private final static Logger LOG = LoggerFactory
			.getLogger(MultiPrintResultJob.class);
	private static final PDFPrintJob DEFAUL_PDF_PRINT_JOB = new PDFPrintJob(false);
	private final List<PrintResult> printResults;
	private final PrintService printService;
	
	
	public MultiPrintResultJob(final List<PrintResult> printResults, final PrintService printService) {
		this.printService = printService;
		
		
		this.printResults = new ArrayList<PrintResult>();
		if (null != printResults) {
			this.printResults.addAll(printResults);
		} else {
			LOG.warn("create PrintResultJob without any printresult");
		}
	}
	
	public void execute()
			throws PrintoutException {
		for (final PrintResult printresult : printResults) {
			final OutputFormatTO outputFormat = printresult.getOutputFormat();
			InputStream is = null;
			try {

				final PrintProperties properties = outputFormat
						.getProperties();

				final PrintRequestAttributeSet attributes = createAttributeSet(
						printService, properties);
				if (LOG.isDebugEnabled()) {
					LOG.debug("send {} to printservice {}",
							outputFormat.getDescription(),
							printService.getName());
				}
				
				NuclosFile toPrint = PrintUtils.toPdf(printresult.getOutput());
				is = new ByteArrayInputStream(toPrint.getContent());

				// set job name
				final String sJobname = new Date() + "_" + printresult.getOutput().getName();
				final JobName jobname = new JobName(sJobname, Locale.getDefault()); 
				attributes.add(jobname);

				final PDFPrintJob printJob = DEFAUL_PDF_PRINT_JOB;
				if (isNativePdfPrintSupported(printService) &&
					useNativePdfPrintSupport(properties)) {
					printJob.print(
						printService, // print service
						is, 		  // content
						attributes 	  // print attributes obtained by properties
						); 
				} else {
					printJob.printViaPdfbox(printService, is, attributes);
				}

			} catch (final CommonPrintException ex) {
				LOG.error(ex.getMessage(), ex);
				throw new PrintoutException(null, outputFormat, ex);
			} catch (final PrintException ex) {
				LOG.error(ex.getMessage(), ex);
				throw new PrintoutException(null, outputFormat, ex);
			} finally {
				if (null != is) {
					try {
						is.close();
					} catch (final IOException ex) {
						LOG.error(ex.getMessage(), ex);
						throw new PrintoutException(null, outputFormat, ex);
					}
				}
			}

		}

	}

	private static PrintRequestAttributeSet createAttributeSet(final PrintService ps,
			final PrintProperties properties) throws CommonPrintException {
		
		Integer trayNumber = null;
		if (properties.getPrintServiceId() != null) {
			PrintServiceTO printServiceTO = SpringApplicationContextHolder.getBean(PrintServiceRepository.class).printService((UID) properties.getPrintServiceId());
			if (properties.getTrayId() == null) {
				if (printServiceTO.getDefaultTray() != null) {
					trayNumber = printServiceTO.getDefaultTray().getNumber();
				}
			} else {
				for (PrintServiceTO.Tray tray : printServiceTO.getTrays()) {
					if (tray.getId().equals(properties.getTrayId())) {
						trayNumber = tray.getNumber();
					}
				}
			}
		}
		
		
		final Integer copies = properties.getCopies();
		final boolean isDuplex = properties.isDuplex();
		
		return PrintServiceLocator.createAttributeSet(ps, trayNumber, copies, isDuplex);
	}
	
	private boolean isNativePdfPrintSupported(PrintService ps) {
		for (DocFlavor df : ps.getSupportedDocFlavors()) {
			if (df.equals(DocFlavor.INPUT_STREAM.PDF)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean useNativePdfPrintSupport(PrintProperties printProperties) {
		if (printProperties != null && printProperties.getPrintServiceId() != null) {
			PrintServiceTO printService = SpringApplicationContextHolder.getBean(PrintServiceRepository.class).printService((UID) printProperties.getPrintServiceId());
			return Boolean.TRUE.equals(printService.isUseNativePdfSupport());
		}
		return false;
	}

}
