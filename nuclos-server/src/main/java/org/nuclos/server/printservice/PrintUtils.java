package org.nuclos.server.printservice;

import static fr.opensagres.xdocreport.converter.ConverterTypeTo.PDF;
import static fr.opensagres.xdocreport.converter.ConverterTypeVia.ODFDOM;
import static fr.opensagres.xdocreport.converter.ConverterTypeVia.XWPF;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.fusesource.hawtbuf.ByteArrayInputStream;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.common2.exception.CommonPrintException;

import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;

public class PrintUtils {

	/**
	 * Transforms the given file to PDF.
	 * Supports DOCX and ODT.
	 * PDFs are returned without manipulation.
	 * 
	 * Only checks the file name to find out what to do.
	 * 
	 * @param file
	 * @return
	 * @throws CommonPrintException
	 */
	public static NuclosFile toPdf(NuclosFile file) throws CommonPrintException {
		final String filenameLC = file.getName().toLowerCase();
		if (filenameLC.endsWith(".pdf")) {
			return file;
		}
		final String pdfFilename;
		byte[] bytes = file.getContent();
		try {
			Options options;
			if (filenameLC.endsWith(".docx")) {
				options = Options.getTo(PDF).via(XWPF);
			} else if (filenameLC.endsWith(".odt")) {
				options = Options.getTo(PDF).via(ODFDOM);
			} else {
				throw new CommonPrintException("Print of " + file.getName() + " is not supported (only .pdf, .docx and .odt)!");
			}

			IXDocReport report = XDocReportRegistry.getRegistry().loadReport(new ByteArrayInputStream(bytes), TemplateEngineKind.Freemarker);
			IContext context = report.createContext();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			report.convert(context, options, out);

			pdfFilename = filenameLC.substring(0, filenameLC.lastIndexOf(".")) + ".pdf";
			return new org.nuclos.common.NuclosFile(pdfFilename, out.toByteArray());
		} catch (FileNotFoundException e) {
			throw new CommonPrintException("Print of " + file.getName() + " to pdf failed: " + e.getMessage(), e);
		} catch (IOException e) {
			throw new CommonPrintException("Print of " + file.getName() + " to pdf failed: " + e.getMessage(), e);
		} catch (Exception e) {
			throw new CommonPrintException("Print of " + file.getName() + " to pdf failed: " + e.getMessage(), e);
		}
	}
	
}
