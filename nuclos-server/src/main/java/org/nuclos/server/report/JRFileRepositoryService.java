package org.nuclos.server.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.resource.ResourceCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.repo.RepositoryContext;
import net.sf.jasperreports.repo.Resource;
import net.sf.jasperreports.repo.ResourceInfo;
import net.sf.jasperreports.repo.StandardResourceInfo;
import net.sf.jasperreports.repo.StreamRepositoryService;

/**
 * By transitioning from JasperReports Library v. 3.5.3 to v. 6.17.0 the FileResolver interface got deprecated.
 * This class is a replacement for the old JRFileResolver class, which implemented the deprecated FileResolver interface.
 * It still uses the same mechanism to check, if a mandator has access to a resource, in the getFile() method.
 */
public class JRFileRepositoryService implements StreamRepositoryService {

	private static final Logger LOG = LoggerFactory.getLogger(JRFileRepositoryService.class);

	private final UID nucletUID;
	private final UID mandatorUID;

	public JRFileRepositoryService(UID nucletUID, UID mandatorUID) {
		this.nucletUID = nucletUID;
		this.mandatorUID = mandatorUID;
	}

	/**
	 * As the FileResolver-Interface is deprecated for the new Version 6.17.0 of Jasper Reports,
	 * we use the old base Method within the new StreamRepositoryService interface.
	 * @param uri
	 * @return
	 */
	private File getFile(final String uri) {
		String filename = uri.substring(LangUtils.max(uri.lastIndexOf('\\'), uri.lastIndexOf('/')) + 1);
		try {
			if (SecurityCache.getInstance().isMandatorPresent() && mandatorUID != null) {
				UID resourceUID = ResourceCache.getInstance().getResourceUID(nucletUID, filename);
				if (resourceUID != null) {
					CollectableComparison cond1 = SearchConditionUtils.newComparison(E.MANDATOR_RESOURCE.resource, ComparisonOperator.EQUAL, resourceUID);
					List<EntityObjectVO<UID>> mandatorResources = NucletDalProvider.getInstance().getEntityObjectProcessor(E.MANDATOR_RESOURCE).
							getBySearchExpression(new CollectableSearchExpression(cond1));
					Set<UID> accessibleMandators = SecurityCache.getInstance().getAccessibleMandators(mandatorUID);
					resourceUID = MandatorUtils.getByAccessibleMandators(mandatorResources, E.MANDATOR_RESOURCE.mandatorResource, E.MANDATOR_RESOURCE.mandator, accessibleMandators);
					if (resourceUID != null) {
						File file = ResourceCache.getInstance().getResourceFile(resourceUID);
						return file;
					}
				}
			}

			File file = ResourceCache.getInstance().getResourceFile(nucletUID, filename);
			return file;
		} catch (Exception ex) {
			String error = StringUtils.getParameterizedExceptionMessage("report.exception.filenotresolved", filename);
			LOG.error(error, ex);
			throw new NuclosFatalException(error);
		}
	}

	@Override
	public InputStream getInputStream(final String uri) {
		try {
			return new FileInputStream(getFile(uri));
		} catch (FileNotFoundException e) {
			throw new JRRuntimeException(e);
		}
	}

	@Override
	public OutputStream getOutputStream(final String uri) {
		try {
			return new FileOutputStream(getFile(uri));
		} catch (FileNotFoundException e) {
			throw new JRRuntimeException(e);
		}
	}

	@Override
	public ResourceInfo getResourceInfo(final RepositoryContext context, final String location) {
		File file = getFile(location);
		if (file != null)
		{
			try
			{
				Path path = file.toPath().toRealPath();
				return StandardResourceInfo.from(path);
			}
			catch (IOException e)
			{
				return StandardResourceInfo.from(file);
			}
		}
		return null;
	}

	/**
	 * This method is never used, so we just give it an empty implementation.
	 * @param uri
	 * @return
	 */
	@Override
	public Resource getResource(final String uri) {
		return null;
	}

	/**
	 * This method is never used, so we just give it an empty implementation.
	 * @param uri
	 * @param resource
	 */
	@Override
	public void saveResource(final String uri, final Resource resource) {

	}

	/**
	 * Loading the resource here is not needed.
	 * We just return null and the resource will be resolved by the FileRepositoryPersistenceServiceFactory
	 * that we set in the JasperExport.java anyway.
	 * Maybe this method has to be implemented in future versions of the JasperReports Library.
	 * @param uri
	 * @param resourceType
	 * @param <K>
	 * @return
	 */
	@Override
	public <K extends Resource> K getResource(final String uri, final Class<K> resourceType) {
		return null;
	}
}
