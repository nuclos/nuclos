//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
/**
 *
 */
package org.nuclos.server.report;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import org.nuclos.common.NucletConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.report.api.JRNuclosDataSource;
import org.nuclos.server.report.ejb3.ExtendedDatasourceFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.HashMap;
import java.util.Map;

@Configurable
public class JRRuleBasedNuclosDataSource implements JRNuclosDataSource {

	private static final Logger LOG = LoggerFactory.getLogger(JRRuleBasedNuclosDataSource.class);

	private ExtendedDatasourceFacadeLocal datasourcefacade;

	private final UID dataSourceUid;
	private final Map<String, Object> params;
	private final JRRuleBasedNuclosDataSource parent;
	private final UID nucletUid;
	private final UID language;
	private final Authentication authentication;

	private Map<String, Integer> fields = new HashMap<String, Integer>();

	private int iCurrentRow;
	private ResultVO data;

	public JRRuleBasedNuclosDataSource(final DatasourceVO datasourceVO, Map<String, Object> params, UID nucletUid, UID language, Authentication authentication) {
		this(datasourceVO, params, null, nucletUid, language, authentication);
	}

	public JRRuleBasedNuclosDataSource(final DatasourceVO datasourceVO, Map<String, Object> params, JRRuleBasedNuclosDataSource parent, UID nucletUid, UID language, Authentication authentication) {
		if (datasourceVO == null) {
			throw new IllegalArgumentException("no datasource found with name " + datasourceVO + " and nuclet " + (nucletUid == null ? NucletConstants.DEFAULT_LOCALIDENTIFIER : nucletUid.getStringifiedDefinition()));
		}

		this.dataSourceUid = datasourceVO.getId();
		this.nucletUid = nucletUid;
		this.params = params;
		this.parent = parent;
		this.language = language;
		this.authentication = authentication;
	}

	@Autowired
	private void inject(ExtendedDatasourceFacadeLocal datasourceFacade) {
		this.datasourcefacade = datasourceFacade;
	}
	
	private UID getNucletUid() {
		return nucletUid;
	}

	private void getData() throws NuclosFatalException {
		Map<String, Object> currentparams = new HashMap<String, Object>();
		currentparams.putAll(this.params);

		//boolean asSuperUser = false;
		try {
			if (SecurityContextHolder.getContext().getAuthentication() == null
				|| !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
				SecurityContextHolder.getContext().setAuthentication(authentication);
				if (SecurityContextHolder.getContext().getAuthentication() == null
						|| !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
					throw new NuclosFatalException("Not authenticated");
				}
			}
			DatasourceVO datasource = datasourcefacade.getDatasource(dataSourceUid);

			if (datasource == null) {
				throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("jrnuclosdatasource.exception.datasourcedoesnotexist", dataSourceUid));
			}

			data = datasourcefacade.executeQuery(dataSourceUid, currentparams, -1, language, null);
		} catch (NuclosDatasourceException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public boolean next() throws JRException {
		if (data == null) {
			getData();
		}
		if (iCurrentRow++ < data.getRowCount()) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public Object getFieldValue(JRField field) throws JRException {
		Object objValue = null;
		if (field != null && this.data != null) {
			Integer columnIndex = -1;
			for (int i = 0; i < data.getColumnCount(); i++) {
				ResultColumnVO column = data.getColumns().get(i);
				if (field.getName().equals(column.getColumnLabel())) {
					columnIndex = i;
					break;
				}
			}

			if (columnIndex >= 0) {
				objValue = data.getRows().get(iCurrentRow - 1)[columnIndex];
			}
		}

		return objValue;
	}

	@Override
	public JRNuclosDataSource getDataSource(final String sDataSource) {
		return new JRRuleBasedNuclosDataSource(datasourcefacade.getDatasource(sDataSource, this.getNucletUid()), params, this, this.getNucletUid(), this.language, authentication);
	}

	@Override
	public JRNuclosDataSource getDataSource(final String sDataSource, Object[][] paramArray) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.putAll(this.params);
		for (Object[] param : paramArray) {
			if (param.length < 3) {
				throw new NuclosFatalException("jrnuclosdatasource.exception.parameterarraylength");
			}
			if (!(param[0] instanceof String)) {
				throw new NuclosFatalException("jrnuclosdatasource.exception.parameterkeytype");
			}

			params.put((String)param[0], param[1]);
		}

		return new JRRuleBasedNuclosDataSource(datasourcefacade.getDatasource(sDataSource, this.getNucletUid()), params, this, this.getNucletUid(), language, authentication);
	}

	/**
	 * Get the total size of the main report set (after the datasource has been processed completely)
	 * @return Number of rows in result set.
	 */
	public int getSize() {
		return data.getRowCount();
	}

	@Override
	public void moveFirst() throws JRException {
		this.iCurrentRow = -1;
	}
}
