package org.nuclos.server.report;

import java.util.List;

import org.nuclos.api.datasource.DatasourceColumn;
import org.nuclos.api.datasource.DatasourceResult;

public class NuclosResultImpl implements DatasourceResult{

	List<Object[]> results;
	List<DatasourceColumn> columns;
	
	public NuclosResultImpl(List<Object[]> pResults, List<DatasourceColumn> pColumns) {
		this.results = pResults;	
		this.columns = pColumns;
	}
	
	@Override
	public List<Object[]> getRows() {
		return this.results;
	}

	@Override
	public List<DatasourceColumn> getColumns() {
		return this.columns;
	}

}
