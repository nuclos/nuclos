package org.nuclos.server.report.ejb3;

import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang3.BooleanUtils;
import org.nuclos.api.datasource.DatasourceResult;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.database.query.definition.ISchema;
import org.nuclos.common.database.query.definition.Table;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.querybuilder.DatasourceXMLParser;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceQueryExecutionParameters;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoteException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosRuleCompileException;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.DatasourceWrapper;
import org.nuclos.server.common.ExtendedDatasourceServerUtils;
import org.nuclos.server.common.ISecurityCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.datasource.DatasourceMetaParser;
import org.nuclos.server.datasource.DatasourceMetaVO;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.ResultSetMetaDataTransformer;
import org.nuclos.server.dblayer.incubator.DbExecutor;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.IResultParams;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.SchemaCache;
import org.nuclos.server.report.suppliers.AccessibleMandatorsSupplier;
import org.nuclos.server.report.suppliers.DataLanguageSupplier;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Datasource facade encapsulating datasource management. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor = { Exception.class })
@Component("datasourceService")
public class ExtendedDatasourceFacadeBean extends DatasourceFacadeBean implements ExtendedDatasourceFacadeLocal, DatasourceFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(DatasourceFacadeBean.class);

	private final NuclosFacadeBean nuclosFacade;
	private final MasterDataFacadeLocal masterDataFacade;
	private final LocaleFacadeLocal localeFacade;
	private final DatasourceMetaParser dsMetaParser;
	private final MetaProvider metaProvider;
	private final ExtendedDatasourceServerUtils datasourceServerUtils;
	private final ServerParameterProvider serverParameterProvider;
	private final EventSupportFacadeLocal eventSupport;
	private final SpringLocaleDelegate localeDelegate;

	public ExtendedDatasourceFacadeBean(
			final SpringDataBaseHelper dataBaseHelper,
			final ExtendedDatasourceServerUtils datasourceServerUtils,
			final SessionUtils sessionUtils,
			@Qualifier("nuclosService") final NuclosFacadeBean nuclosFacade,
			final MasterDataFacadeLocal masterDataFacade,
			final LocaleFacadeLocal localeFacade,
			final DatasourceMetaParser dsMetaParser,
			final MetaProvider metaProvider,
			final ISecurityCache securityCache,
			final DatasourceCache datasourceCache,
			final SchemaCache schemaCache,
			final ServerParameterProvider serverParameterProvider, final EventSupportFacadeLocal eventSupport, final SpringLocaleDelegate localeDelegate) {
		super(dataBaseHelper, datasourceServerUtils, sessionUtils, securityCache, datasourceCache, schemaCache, metaProvider);
		this.nuclosFacade = nuclosFacade;
		this.masterDataFacade = masterDataFacade;
		this.localeFacade = localeFacade;
		this.dsMetaParser = dsMetaParser;
		this.metaProvider = metaProvider;
		this.datasourceServerUtils = datasourceServerUtils;
		this.serverParameterProvider = serverParameterProvider;
		this.eventSupport = eventSupport;
		this.localeDelegate = localeDelegate;
	}

	@PostConstruct
	private void postInject() {
		this.datasourceCache.setUtils(this.datasourceServerUtils);
	}

	/**
	 * get datasource value object
	 *
	 * @param dataSourceUID
	 *            UID of datasource
	 * @return datasource value object
	 */
	@Override
	@RolesAllowed("Login")
	public DatasourceVO get(final UID dataSourceUID) throws CommonPermissionException {
		final DatasourceVO datasourceVO = datasourceCache.getDatasourcesByUser(dataSourceUID, nuclosFacade.getCurrentUserName(), nuclosFacade.getCurrentMandatorUID());

		if (datasourceCache.getPermission(dataSourceUID, nuclosFacade.getCurrentUserName(), nuclosFacade.getCurrentMandatorUID()) == DatasourceVO.Permission.PERMISSION_NONE) {
			throw new CommonPermissionException("datasource.cach.missing.permission");
		}
		return datasourceVO;

		/*
		 * todo how should we handle permissions here? - It is necessary, that
		 * // all users have the right to execute inline datasources if
		 * (result.getPermission() == DatasourceVO.PERMISSION_NONE) { throw new
		 * CommonPermissionException(); } return result;
		 */
	}

	/**
	 * get a list of DatasourceVO which uses the datasource
	 *
	 * @param datasourceVO
	 *            could also be an instance of <code>DynamicEntityVO</code> or
	 *            <code>ValuelistProviderVO</code>
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@Override
	@RolesAllowed("Login")
	public List<DatasourceVO> getUsagesForDatasource(final DatasourceVO datasourceVO) throws CommonFinderException, CommonPermissionException {
		final DataSourceType datasourceType = DataSourceType.getFromDatasourceVO(datasourceVO);

		final List<DatasourceVO> result = new ArrayList<DatasourceVO>();

		CollectableComparison cond = SearchConditionUtils.newKeyComparison(datasourceType.fieldEntityUsedUID, ComparisonOperator.EQUAL, datasourceVO.getPrimaryKey());

		//CollectableComparison cond = SearchConditionUtils.newMDReferenceComparison(MasterDataMetaCache.getInstance().getMetaData(datasourceType.entityUsage), datasourceType.fieldEntityUsedUID, datasourceVO.getPrimaryKey());

		for (MasterDataVO<UID> usageVO : getMasterDataFacade().getMasterData(datasourceType.entityUsage, cond)) {
			MasterDataVO<UID> deVO = getMasterDataFacade().get(datasourceType.entity, usageVO.getFieldUid(datasourceType.fieldEntityUsedUID));
			if (!datasourceVO.getId().equals(deVO.getPrimaryKey())) {
				result.add(setLocaleResouces(wrap(deVO.getEntityObject(), datasourceType, nuclosFacade.getCurrentUserName(), nuclosFacade.getCurrentMandatorUID())));
			}
		}

		return result;
	}

	/**
	 * get a list of DatasourceCVO which are used by the datasource with the
	 * given UID
	 *
	 * @param dataSourceUID data source UID
	 * @return
	 * @throws CommonFinderException
	 * @throws CommonPermissionException
	 */
	@Override
	public List<DatasourceVO> getUsingByForDatasource(final UID dataSourceUID) throws CommonFinderException, CommonPermissionException {

		final DbQueryBuilder builder = getDataBaseHelper().getDbAccess().getQueryBuilder();
		final DbQuery<DbTuple> query = builder.createTupleQuery();
		final DbFrom<UID> t = query.from(E.DATASOURCEUSAGE);
		query.multiselect(t.baseColumn(SF.PK_UID), t.baseColumn(E.DATASOURCEUSAGE.datasource), t.baseColumn(E.DATASOURCEUSAGE.datasourceUsed));
		query.where(builder.equalValue(t.baseColumn(E.DATASOURCEUSAGE.datasource), dataSourceUID));

		final List<DatasourceVO> result = new ArrayList<DatasourceVO>();
		// TODO MULTINUCLET use DbTuple<UID>
		for (DbTuple tuple : getDataBaseHelper().getDbAccess().executeQuery(query)) {
			try {
				final UID dataSourceUsedUID = tuple.get(2, UID.class);
				final MasterDataVO<UID> mdVO = masterDataFacade.get(E.DATASOURCE, dataSourceUsedUID);
				result.add(DatasourceWrapper.getDatasourceVO(mdVO.getEntityObject(), nuclosFacade.getCurrentUserName(), nuclosFacade.getCurrentMandatorUID(), securityCache));
			}
			catch (CommonFinderException ex) {
				throw new NuclosFatalException(ex);
			}
			catch (CommonPermissionException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return result;
	}

	/**
	 * get all datasources
	 *
	 * @return set of datasources
	 * @throws CommonPermissionException
	 */
	@Override
	public Collection<DatasourceVO> getDatasources() throws CommonPermissionException {
		nuclosFacade.checkReadAllowed(E.DATASOURCE);
		return datasourceCache.getAllDatasources(nuclosFacade.getCurrentUserName(), nuclosFacade.getCurrentMandatorUID());
	}

	@Override
	public DatasourceVO getDatasourceByName(String sDataSource) throws CommonBusinessException {
		try {
			for (DatasourceVO datasourceVO : getDatasources()) {
				if (datasourceVO.getName().equals(sDataSource))
					return datasourceVO;
			}
			return null;
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * get all datasources
	 *
	 * @return set of datasources
	 */
	public Collection<DatasourceVO> getDatasourcesForCurrentUser() {
		return datasourceCache.getDatasourcesByCreator(nuclosFacade.getCurrentUserName(), nuclosFacade.getCurrentMandatorUID());
	}

	/**
	 * get valuelist provider value object
	 *
	 * @param valuelistProviderUID
	 *            UID of valuelist provider
	 * @return valuelist provider value object
	 */
	@RolesAllowed("Login")
	@Override
	public ValuelistProviderVO getValuelistProvider(final UID valuelistProviderUID) {
		final ValuelistProviderVO vlpvo = super.getValuelistProvider(valuelistProviderUID);
		setLocaleResources(vlpvo);
		return vlpvo;
	}

	private boolean isEditSQLAllowed() {
		return SecurityCache.getInstance().isActionAllowed(nuclosFacade.getCurrentUserName(), Actions.ACTION_EDIT_DATASOURCE_SQL, nuclosFacade.getCurrentMandatorUID());
	}

	private void checkWriteAllowed(final DatasourceVO datasourcevo) throws CommonPermissionException {
		final boolean bEditSQLAllowed = isEditSQLAllowed();
		try {
			final DatasourceXMLParser.Result parseresult = datasourceServerUtils.parseDatasourceXML(datasourcevo.getSource());
			if(!parseresult.isModelUsed() && !bEditSQLAllowed) {
				throw new CommonPermissionException("edit.datasource.sql.not.allowed");
			}
			for (DatasourceParameterVO parameterVO : parseresult.getLstParameters()) {
				if ("SQL".equals(parameterVO.getDatatype()) && !bEditSQLAllowed) {
					throw new CommonPermissionException("edit.datasource.sql.param.not.allowed");
				}
			}
		} catch (NuclosDatasourceException e) {
			// Usually no problem here: The validation takes place before the check
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * create new datasource
	 *
	 * @param datasourcevo
	 *            value object
	 * @return new datasource
	 * @throws CommonFinderException
	 */
	@Override
	public DatasourceVO create(final DatasourceVO datasourcevo, final IDependentDataMap dependants, final List<UID> lstUsedDataSourceUIDs) throws CommonBusinessException {
		final DataSourceType type = DataSourceType.getFromDatasourceVO(datasourcevo);
		EntityMeta<UID> entity = type.entity;
		nuclosFacade.checkWriteAllowed(entity);

		datasourcevo.setPrimaryKey(new UID());

		try {
			datasourcevo.validate();
		} catch (Exception e) {
			throw new CommonValidationException(e.getMessage(), e.getCause());
		}
		updateValidFlag(datasourcevo);
		this.checkWriteAllowed(datasourcevo);

		if (E.RECORDGRANT.equals(entity)) {
			processChangingRecordGrant((RecordGrantVO) datasourcevo, null);
		}

		if (E.DYNAMICENTITY.equals(entity)) {
			processChangingDynamicEntity((DynamicEntityVO) datasourcevo, null);
		}

		if (E.CHART.equals(entity)) {
			processChangingChartEntity((ChartVO) datasourcevo, null);
		}

		if (E.DYNAMICTASKLIST.equals(entity)) {
			processChangingDynamicTasklist((DynamicTasklistVO) datasourcevo, null);
		}

		final MasterDataVO<UID> mdVO = getMasterDataFacade().create(new MasterDataVO<UID>(unwrap(datasourcevo, type, entity)), null);
		getMasterDataFacade().notifyClients(entity.getUID());

		final DatasourceVO dbDataSourceVO = DatasourceWrapper.getDatasourceVO(mdVO.getEntityObject(), nuclosFacade.getCurrentUserName(), nuclosFacade.getCurrentMandatorUID(), securityCache);

		try {
			replaceUsedDatasourceList(dbDataSourceVO, lstUsedDataSourceUIDs);
		}
		catch (CommonFinderException | CommonRemoveException | CommonStaleVersionException e) {
			throw new CommonFatalException(e);
		}
		invalidateCaches(type);
		return setLocaleResources(DatasourceWrapper.wrap(mdVO.getEntityObject(), type, nuclosFacade.getCurrentUserName(), nuclosFacade.getCurrentMandatorUID(), securityCache, localeDelegate, metaProvider));
	}

	private EntityObjectVO<UID> unwrap(final DatasourceVO datasourcevo, final DataSourceType type, final EntityMeta<UID> entity) throws CommonPermissionException {
		EntityObjectVO<UID> eovo = DatasourceWrapper.unwrap(datasourcevo, type, entity.getUID());

		if (datasourcevo instanceof ValuelistProviderVO) {
			eovo = updateResourceForVLP(eovo, (ValuelistProviderVO) datasourcevo);
		}

		return eovo;
	}

	public DatasourceVO wrap(EntityObjectVO<UID> eovo, final DataSourceType type, final String sUser, UID mandator) {
		switch (type) {
			case DYNAMICENTITY:
				return DatasourceWrapper.getDynamicEntityVO(eovo);
			case VALUELISTPROVIDER:
				return DatasourceWrapper.getValuelistProviderVO(eovo, localeDelegate);
			case RECORDGRANT:
				return DatasourceWrapper.getRecordGrantVO(eovo, metaProvider);
			case DYNAMICTASKLIST:
				return DatasourceWrapper.getDynamicTasklistVO(eovo);
			case CHART:
				return DatasourceWrapper.getChartVO(eovo);
			case CALCATTRIBUTE:
				return DatasourceWrapper.getCalcAttributeVO(eovo);
			default:
				return DatasourceWrapper.getDatasourceVO(eovo, sUser, mandator, securityCache);
		}
	}

	public EntityObjectVO<UID> updateResourceForVLP(final EntityObjectVO<UID> eovo, final ValuelistProviderVO vlpVO) throws CommonPermissionException {
		MasterDataVO<UID> mdvoDb = null;
		try {
			mdvoDb = masterDataFacade.get(E.VALUELISTPROVIDER, vlpVO.getId());
		} catch (CommonFinderException ex) {
			// do nothing
		}
		String resourceId = mdvoDb != null ? mdvoDb.getFieldValue(E.VALUELISTPROVIDER.detailsearchdescription) : null;

		LocaleFacadeLocal localeFacade = ServerServiceLocator.getInstance().getFacade(LocaleFacadeLocal.class);
		if (resourceId != null) {
			localeFacade.update(resourceId, LocaleInfo.parseTag(Locale.GERMAN), vlpVO.getDetailsearchdescription(Locale.GERMAN));
			localeFacade.update(resourceId, LocaleInfo.parseTag(Locale.ENGLISH), vlpVO.getDetailsearchdescription(Locale.ENGLISH));
		} else {
			resourceId = localeFacade.insert(null, LocaleInfo.parseTag(Locale.GERMAN), vlpVO.getDetailsearchdescription(Locale.GERMAN));
			localeFacade.insert(resourceId, LocaleInfo.parseTag(Locale.ENGLISH), vlpVO.getDetailsearchdescription(Locale.ENGLISH));
		}


		eovo.setFieldValue(DatasourceWrapper.getDsValueableField(E.VALUELISTPROVIDER.getUID(), E.VALUELISTPROVIDER.detailsearchdescription), resourceId);
		return eovo;
	}


	private void processChangingRecordGrant(final RecordGrantVO newRGVO, RecordGrantVO oldRGVO) throws CommonValidationException {
		if (newRGVO != null) {
			try {
				EntityMeta<?> entityToGrant = null;
				if (newRGVO.getEntityUID() != null) {
					entityToGrant = metaProvider.getEntity(newRGVO.getEntityUID());
				}

				String sqlSelect = createSQL(newRGVO);
				newRGVO.setQuery(sqlSelect);
				LOG.debug("validate dynamic entity sql <SQL>{}</SQL>", sqlSelect);

				final DatasourceMetaVO dsmetaOld = Optional.ofNullable(oldRGVO).map(DatasourceVO::getMeta).map(dsMetaParser::parseRecordGrant).orElse(null);
				final DatasourceMetaTransformer transformer = new DatasourceMetaTransformer(newRGVO, dsmetaOld, E.RECORDGRANT);
				final DatasourceMetaVO dsmetaNew = validateRecordGrantSQL(entityToGrant, sqlSelect, transformer);
				if (dsmetaNew != null) {
					newRGVO.setMeta(dsmetaNew.toString());
				}
			}
			catch (NuclosDatasourceException e) {
				throw new CommonFatalException(e);
			}
		}
	}

	/**
	 * modify an existing datasource without usages
	 *
	 * @param datasourcevo
	 * @throws CommonFinderException
	 * @throws CommonPermissionException
	 * @throws CommonStaleVersionException
	 * @throws CommonValidationException
	 */
	@Override
	public void modify(DatasourceVO datasourcevo) throws CommonBusinessException {
		modify(datasourcevo, null, null, false);
	}

	/**
	 * modify an existing datasource
	 *
	 * @param datasourcevo
	 *            value object
	 * @return modified datasource
	 */
	@Override
	public DatasourceVO modify(final DatasourceVO datasourcevo, final IDependentDataMap dependants, final List<UID> lstUsedDataSourceUIDs) throws CommonBusinessException {

		return modify(datasourcevo, dependants, lstUsedDataSourceUIDs, true);
	}

	private DatasourceVO modify(final DatasourceVO datasourcevo, final IDependentDataMap dependants, final List<UID> lstUsedDataSourceUIDs, boolean modifyUsedDatasources) throws CommonBusinessException {
		final DataSourceType type = DataSourceType.getFromDatasourceVO(datasourcevo);
		final EntityMeta<UID> entity = type.entity;
		nuclosFacade.checkWriteAllowed(entity);

		try {
			datasourcevo.validate();
		} catch (Exception e) {
			throw new CommonValidationException(e.getMessage(), e.getCause());
		}

		// validate used data sources first to avoid mistaken java.lang.StackOverflowError: alreadyProcessed
		if (lstUsedDataSourceUIDs != null && !lstUsedDataSourceUIDs.isEmpty()) {
			for(UID uid : lstUsedDataSourceUIDs) {
				if (datasourcevo.getId().equals(uid)) {
					throw new CommonValidationException("datasource.error.selfreference");
				}
				DatasourceVO usedDS = datasourceCache.get(type, uid);
				try {
					validateSqlFromXML(usedDS);
				} catch (NuclosDatasourceException ex) {
					throw new CommonRemoteException(ex);
				}
			}
		}

		if (E.DYNAMICENTITY.equals(entity)) {
			try { //validation before updateValidFlag for better validation messages
				String sqlSelect = createSQL(datasourcevo);
				validateDynEntitySQL(sqlSelect, null, getDataBaseHelper().getDbAccess().getTableAliasing());
			} catch (CommonValidationException ex) {
				throw ex;
			} catch (Exception ex) {
				// ignore here
			}
		}

		updateValidFlag(datasourcevo);
		this.checkWriteAllowed(datasourcevo);

		try {
			validateUniqueConstraint(datasourcevo);

			final String sUser = nuclosFacade.getCurrentUserName();
			MasterDataVO<UID> dsAsMd = getMasterDataFacade().get(entity, datasourcevo.getPrimaryKey());

			if (E.DATASOURCE.equals(entity)) {
				if (datasourceCache.getPermission(dsAsMd.getPrimaryKey(), sUser, nuclosFacade.getCurrentMandatorUID()) != DatasourceVO.Permission.PERMISSION_READWRITE) {
					throw new CommonPermissionException();
				}
			}

			if (E.RECORDGRANT.equals(entity)) {
				processChangingRecordGrant((RecordGrantVO) datasourcevo, (RecordGrantVO) DatasourceWrapper.wrap(dsAsMd.getEntityObject(), type, sUser, nuclosFacade.getCurrentMandatorUID(), securityCache, localeDelegate, metaProvider));
			}

			if (E.DYNAMICENTITY.equals(entity)) {
				processChangingDynamicEntity((DynamicEntityVO) datasourcevo, (DynamicEntityVO) DatasourceWrapper.wrap(dsAsMd.getEntityObject(), type, sUser, nuclosFacade.getCurrentMandatorUID(), securityCache, localeDelegate, metaProvider));
			}

			if (E.CHART.equals(entity)) {
				processChangingChartEntity((ChartVO) datasourcevo, (ChartVO) DatasourceWrapper.wrap(dsAsMd.getEntityObject(), type, sUser, nuclosFacade.getCurrentMandatorUID(), securityCache, localeDelegate, metaProvider));
			}

			if (E.DYNAMICTASKLIST.equals(entity)) {
				processChangingDynamicTasklist((DynamicTasklistVO) datasourcevo, (DynamicTasklistVO) DatasourceWrapper.wrap(dsAsMd.getEntityObject(), type, sUser, nuclosFacade.getCurrentMandatorUID(), securityCache, localeDelegate, metaProvider));
			}

			MasterDataVO<UID> unwrapped = new MasterDataVO<>(unwrap(datasourcevo, type, entity));

			if (dsAsMd.getVersion() != datasourcevo.getVersion()) {
				throw new CommonStaleVersionException(nuclosFacade.getVersionConflictMessages("datasource", unwrapped.getEntityObject(), dsAsMd.getEntityObject()));
			}
			getMasterDataFacade().modify(unwrapped, null);

			if (modifyUsedDatasources) {
				// store the list of used datasources
				replaceUsedDatasourceList(datasourcevo, lstUsedDataSourceUIDs);
			}

			invalidateCaches(type);

			dsAsMd = getMasterDataFacade().get(entity, datasourcevo.getPrimaryKey());
			return setLocaleResources(DatasourceWrapper.wrap(dsAsMd.getEntityObject(), type, sUser, nuclosFacade.getCurrentMandatorUID(), securityCache, localeDelegate, metaProvider));
		}
		catch (CommonRemoveException e) {
			throw new NuclosFatalException(e);
		}
		catch (CommonCreateException e) {
			throw new NuclosFatalException(e);
		}
		catch (CommonFatalException e) {
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * delete an existing datasource
	 *
	 * @param datasourcevo
	 *            value object
	 */
	@Override
	public void remove(DatasourceVO datasourcevo)
			throws CommonValidationException, CommonFinderException, CommonRemoveException, CommonPermissionException,
			CommonStaleVersionException, NuclosBusinessRuleException {

		DataSourceType type = DataSourceType.getFromDatasourceVO(datasourcevo);
		EntityMeta<UID> entity = type.entity;
		nuclosFacade.checkDeleteAllowed(entity);

		MasterDataVO<UID> dsAsMd = getMasterDataFacade().get(entity, datasourcevo.getId());

		if (E.DATASOURCE.equals(entity)) {
			if (datasourceCache.getPermission(dsAsMd.getPrimaryKey(), nuclosFacade.getCurrentUserName(), nuclosFacade.getCurrentMandatorUID()) != DatasourceVO.Permission.PERMISSION_READWRITE) {
				throw new CommonPermissionException();
			}
		}

		if (dsAsMd.getVersion() != datasourcevo.getVersion()) {
			throw new CommonStaleVersionException(dsAsMd.getEntityObject(), datasourcevo.getVersion());
		}

		getMasterDataFacade().remove(entity.getUID(), datasourcevo.getPrimaryKey(), null);
		getMasterDataFacade().notifyClients(entity.getUID());

		try {
			replaceUsedDatasourceList(datasourcevo, Collections.<UID> emptyList());
		}
		catch (CommonCreateException e) {
			throw new CommonFatalException(e);
		}

		invalidateCaches(type);
	}

	/**
	 * @param datasourcevo
	 */
	private void updateValidFlag(final DatasourceVO datasourcevo) throws NuclosDatasourceException {
		try {
			this.validateSqlFromXML(datasourcevo);
			datasourcevo.setValid(Boolean.TRUE);
		}
		catch (NuclosDatasourceException e) {
			datasourcevo.setValid(Boolean.FALSE);
			throw e;
		}
	}

	/**
	 *
	 * @param newDEVO
	 * @param oldDEVO
	 * @throws CommonValidationException
	 *             (only if validate is true)
	 */
	private void processChangingDynamicEntity(DynamicEntityVO newDEVO, DynamicEntityVO oldDEVO) throws CommonValidationException {
		String sqlSelect;
		if (newDEVO != null) {
			try {
				sqlSelect = createSQL(newDEVO);
				newDEVO.setQuery(sqlSelect);
				LOG.debug("validate dynamic entity sql <SQL>{}</SQL>", sqlSelect);

				final DatasourceMetaVO dsmetaOld = Optional.ofNullable(oldDEVO).map(DatasourceVO::getMeta).map(dsMetaParser::parseDynamicEntity).orElse(null);
				final DatasourceMetaTransformer transformer = new DatasourceMetaTransformer(newDEVO, dsmetaOld, E.DYNAMICENTITY);
				final DatasourceMetaVO dsmetaNew = validateDynEntitySQL(sqlSelect, transformer, getDataBaseHelper().getDbAccess().getTableAliasing());
				if (dsmetaNew != null) {
					newDEVO.setMeta(dsmetaNew.toString());
				}
			}
			catch (NuclosDatasourceException e) {
				throw new CommonFatalException(e);
			}
		}
	}

	/**
	 *
	 * @param newDEVO
	 * @param oldDEVO
	 * @throws CommonValidationException
	 *             (only if validate is true)
	 */
	private void processChangingChartEntity(ChartVO newDEVO, ChartVO oldDEVO) throws CommonValidationException {
		String sqlSelect;
		if (newDEVO != null) {
			try {
				LOG.debug("validate chart entity name \"{}\"", newDEVO.getName());
				sqlSelect = createSQL(newDEVO);
				newDEVO.setQuery(sqlSelect);
				LOG.debug("validate chart entity sql <SQL>{}</SQL>",  sqlSelect);

				final DatasourceMetaVO dsmetaOld = Optional.ofNullable(oldDEVO).map(DatasourceVO::getMeta).map(dsMetaParser::parseChart).orElse(null);
				final DatasourceMetaTransformer transformer = new DatasourceMetaTransformer(newDEVO, dsmetaOld, E.CHART);
				DatasourceMetaVO dsmetaNew;
				if (newDEVO.getParentEntityUID() != null) {
					dsmetaNew = validateDynEntitySQL(sqlSelect, transformer, getDataBaseHelper().getDbAccess().getTableAliasing());
				} else {
					dsmetaNew = validateChartEntitySQL(sqlSelect,
							getColumns(createSQL(newDEVO, getTestParameters(newDEVO.getSource()))), transformer);
					dsmetaNew = validateChartEntitySQLWithParameters(
							createSQL(newDEVO, getTestParameters(newDEVO.getSource())), transformer);
				}

				if (dsmetaNew != null) {
					newDEVO.setMeta(dsmetaNew.toString());
				}
			}
			catch (NuclosDatasourceException e) {
				throw new CommonFatalException(e);
			}
		}
	}

	/**
	 *
	 * @param newDTVO
	 * @param oldDTVO
	 * @throws CommonValidationException
	 *             (only if validate is true)
	 */
	private void processChangingDynamicTasklist(DynamicTasklistVO newDTVO, DynamicTasklistVO oldDTVO) throws CommonValidationException {
		String sqlSelect;
		if (newDTVO != null && newDTVO.getDatasourceRule() == null) {
			try {
				sqlSelect = createSQL(newDTVO);
				newDTVO.setQuery(sqlSelect);
				LOG.debug("validate dynamic task sql <SQL>{}</SQL>", sqlSelect);

				final DatasourceMetaVO dsmetaOld = Optional.ofNullable(oldDTVO).map(DatasourceVO::getMeta).map(dsMetaParser::parseDynamictasklist).orElse(null);
				final DatasourceMetaTransformer transformer = new DatasourceMetaTransformer(newDTVO, dsmetaOld, E.DYNAMICTASKLIST);
				DatasourceMetaVO dsmetaNew = null;
				try {
					dsmetaNew = validateDynTasklistSQL(sqlSelect, transformer, getDataBaseHelper().getDbAccess().getTableAliasing());
				} catch (Exception e) {
					LOG.warn("Metadata creation for SQL SELECT {} failed:\n{}", sqlSelect, e);
					throw e;
				}
				if (dsmetaNew != null) {
					newDTVO.setMeta(dsmetaNew.toString());
				}
			}
			catch (NuclosDatasourceException e) {
				throw new CommonFatalException(e);
			}
		} else if (newDTVO != null && newDTVO.getDatasourceRule() != null) {
			try {
				final DatasourceMetaVO dsmetaOld = dsMetaParser.parseDynamictasklist(oldDTVO.getMeta());
				DatasourceMetaVO dsmetaNew = validateDynTasklistRule(
						newDTVO.getDatasourceRule(),
						(Transformer<List<ResultColumnVO>, DatasourceMetaVO>) columns -> {
							final DatasourceMetaVO result = new DatasourceMetaVO();

							result.setEntityUID(new UID(E.DYNAMICTASKLIST.getUID().getString() + "-" + newDTVO.getId().getString()));

							final List<DatasourceMetaVO.ColumnMeta> columnMetas = new ArrayList<>();

							for (ResultColumnVO column : columns) {
								final DatasourceMetaVO.ColumnMeta columnMeta = new DatasourceMetaVO.ColumnMeta();
								columnMeta.setColumnName(column.getColumnLabel());

								UID fieldUID = null;
								if (dsmetaOld != null && dsmetaOld.getColumns() != null) {
									for (DatasourceMetaVO.ColumnMeta existingcol : dsmetaOld.getColumns()) {
										if (column.getColumnLabel().equals(existingcol.getColumnName())) {
											fieldUID = existingcol.getFieldUID();
										}
									}
								}
								if (fieldUID == null) {
									fieldUID = new UID(E.DYNAMICTASKLIST.getUID().getString() + "f-" + new UID().getString());
								}

								columnMeta.setFieldUID(fieldUID);
								columnMeta.setJavaType(column.getColumnClassName());
								columnMeta.setReadOnly(true);

								columnMetas.add(columnMeta);
							}

							result.setColumns(columnMetas);

							return result;
						});

				if (dsmetaNew != null) {
					newDTVO.setMeta(dsmetaNew.toString());
				}

			} catch (Exception e) {
				throw new CommonFatalException(e);
			}
		}
	}

	private void checkReadPermissionForDSEntity(UID dsEntity)  throws CommonPermissionException {
		try {
			nuclosFacade.checkReadAllowed(dsEntity);
		} catch (CommonPermissionException e) {
			throw new CommonPermissionException("No permission to read SchemaData. Cannot read " + dsEntity);
		}

	}

	@Override
	public ISchema getSchemaTables(UID dsEntity) throws CommonPermissionException {
		checkReadPermissionForDSEntity(dsEntity);

		return schemaCache.getCurrentSchema();
	}

	@Override
	public Table getSchemaColumns(Table table, UID dsEntity)  throws CommonPermissionException {
		checkReadPermissionForDSEntity(dsEntity);

		schemaCache.fillTableColumnsAndConstraints(table);
		return table;
	}

	public String createSQL(final DatasourceVO datasourcevo, final Map<String, Object> mpParams, UID mandatorUID, UID language) throws NuclosDatasourceException {
		return datasourceServerUtils.createSQL(datasourcevo, mpParams, null, datasourceCache, schemaCache, new DataLanguageSupplier(language), mandatorUID, new AccessibleMandatorsSupplier(mandatorUID));
	}

	@Override
	public String createSQLOriginalParameter(DatasourceVO datasourcevo) throws NuclosDatasourceException {
		return datasourceServerUtils.createSQLOriginalParameter(datasourcevo, datasourceCache);
	}

	@Override
	@RolesAllowed("Login")
	public DynamicTasklistVO getDynamicTasklist(final UID dynamicTaskListUID) {
		return datasourceCache.getDynamicTasklist(dynamicTaskListUID);
	}

	@Override
	public Collection<DynamicTasklistVO> getDynamicTasklists() throws CommonPermissionException {
		nuclosFacade.checkReadAllowed(E.TASKLIST);
		return datasourceCache.getAllDynamicTasklists();
	}
	//TODO MULTINUCLET Set<String> attributes?
	@Override
	public Set<String> getDynamicTasklistAttributes(final UID dynamicTaskListUID) throws CommonPermissionException, NuclosDatasourceException {
		nuclosFacade.checkReadAllowed(E.TASKLIST);
		final DynamicTasklistVO dtl = datasourceCache.getDynamicTasklist(dynamicTaskListUID);

		Set<String> attributes = new HashSet<>();

		ResultVO result;
		if (dtl.getDatasourceRule() != null) {
			result = getDataFromRule(dtl, new HashMap<>(), -1, null, null);
		} else {
			final String sQuery = createSQL(dtl, new HashMap<>());

			result = executePlainQueryAsResultVO(sQuery, 1);
			//TODO MULTINUCLET attributes?
		}
		for (ResultColumnVO col : result.getColumns()) {
			attributes.add(col.getColumnLabel());
		}
		return attributes;
	}

	public ResultVO getDynamicTasklistData(final UID dynamicTaskListUID, final IResultParams resultParams) throws CommonPermissionException, NuclosDatasourceException {
		if (!SecurityCache.getInstance().isSuperUser(nuclosFacade.getCurrentUserName()) && !SecurityCache.getInstance().getDynamicTasklistDatasources(nuclosFacade.getCurrentUserName(),nuclosFacade.getCurrentMandatorUID()).contains(dynamicTaskListUID)) {
			throw new CommonPermissionException("Permission denied for dynamic task list with id " + dynamicTaskListUID);
		}

		final DynamicTasklistVO dtl = datasourceCache.getDynamicTasklist(dynamicTaskListUID);
		Map<String, Object> mpParams = new HashMap<>();
		if (resultParams != null) {
			if (resultParams.getLimit() != null) {
				mpParams.put("limit", resultParams.getLimit());
				mpParams.put("offset", resultParams.getOffset() != null ? resultParams.getOffset() : 0l);
			}
		}
		if (dtl.getDatasourceRule() != null) {
			return getDataFromRule(dtl, mpParams, null, null, null);
		} else {
			String sQuery = createSQL(dtl, mpParams, new DataLanguageSupplier(null), null, new AccessibleMandatorsSupplier(null));
			return executePlainQueryAsResultVO(sQuery, null);
		}
	}

	@Override
	public UID getReportLanguageToUse(UID report, UID language, Long pk) throws NuclosReportException {

		final List<UID> lstModules = masterDataFacade.getMasterData(E.REPORTUSAGE,
						SearchConditionUtils.newUidComparison(E.REPORTUSAGE.form, ComparisonOperator.EQUAL, report))
				.stream()
				.map(reportUsage -> reportUsage.getFieldUid(E.REPORTUSAGE.module))
				.collect(Collectors.toList());

		return datasourceServerUtils.getReportLanguageToUse(lstModules, language, pk);
	}

	@Override
	public CollectableField getDefaultValue(final UID dataSourceUID, final String valuefield, String idfield, String defaultfield, Map<String, Object> params, UID baseEntityUID, final UID entityFieldUID, UID mandatorUID, UID languageUID) throws CommonBusinessException {
		final DatasourceVO dsvo = getValuelistProvider(dataSourceUID);
		final List<DatasourceParameterVO> collParameters = getParametersFromXML(dsvo.getSource());
		final Map<String, Object> queryParams = new HashMap<String, Object>(params);

		for (final DatasourceParameterVO dpvo : collParameters) {
			if (queryParams.get(dpvo.getParameter()) == null) {
				queryParams.put(dpvo.getParameter(), null);
			}
		}

		final List<? extends CollectableField> result = executeQueryForVLP(
				new VLPQuery(dataSourceUID)
						.setQueryParams(queryParams)
						.setNameField(valuefield)
						.setIdFieldName(idfield)
						.setDefaultField(defaultfield)
						.setBaseEntityUID(baseEntityUID)
						.setMandatorUID(mandatorUID)
						.setLanguageUID(languageUID)
						.setEntityFieldUID(entityFieldUID)
		);

		for (CollectableField field : result) {
			boolean selected = false;
			if (field instanceof CollectableValueIdField) {
				selected = ((CollectableValueIdField) field).isSelected();
			} else if (field instanceof CollectableValueField) {
				selected = ((CollectableValueField) field).isSelected();
			}
			if (selected) {
				if (idfield != null) {
					return field;
				} else {
					return new CollectableValueField(field.getValue());
				}
			}
		}
		return idfield != null ? CollectableValueIdField.NULL : CollectableValueField.NULL;
	}

	/**
	 * boolean representation from object
	 * 
	 * @param obj	object
	 * @return	
	 */
	private Boolean booleanFromObject(final Object obj) {
		if (null == obj) {
			return Boolean.FALSE;
		}
		
		final Object value = obj;
		
		Boolean isDefault = null;
		if (value instanceof String) {
			isDefault = BooleanUtils.toBooleanObject((String)value);
		}
		if (value instanceof Integer) {
			isDefault = BooleanUtils.toBooleanObject((Integer)value);
		}
		if (value instanceof Boolean) {
			isDefault = (Boolean)value;
		} 
		
		if (null == isDefault) {
			throw new IllegalArgumentException("no boolean representation for unknown class " + obj.getClass());
		}
		
		return isDefault;
	}
	
	@Override
	public List<String> getColumnsFromVLP(UID valuelistProviderUid, Class clazz) throws CommonBusinessException {
		ValuelistProviderVO vlpVO = getValuelistProvider(valuelistProviderUid);
		if (vlpVO == null) {
			throw new CommonFinderException("Could Not Find VLP with UID=" + valuelistProviderUid);
		}

		return getColumns(createSQL(vlpVO, getTestParameters(vlpVO.getSource())), clazz);
	}
	
	@Override
	public List<String> getColumns(String sql) {
		return getColumns(sql, null);
	}
	
	public List<String> getColumns(String sql, Class clazz) {
		List<String> result = new ArrayList<String>();
		try {
			if (StringUtils.isNullOrEmpty(sql))
				return result;
			
			ResultVO resultVO = executePlainQueryAsResultVO(sql, 0);
			for (ResultColumnVO column : resultVO.getColumns()) {
				if (clazz == null || clazz.isAssignableFrom(column.getColumnClass()))
				result.add(column.getColumnLabel());
			}
			
			// fallback 
			// beware. we cannot respect the column clazz here.
			if (result.isEmpty() && clazz == null) {
				result.addAll(DatasourceUtils.getColumns(sql));
			}
			return result;
		} catch (Exception e) {
			// fallback
			// beware. we cannot respect the column clazz here.
			if (result.isEmpty() && clazz == null) {
				result.addAll(DatasourceUtils.getColumns(sql));
			}
		}
		return result;
	}

	public static class DatasourceMetaTransformer implements ResultSetMetaDataTransformer<DatasourceMetaVO> {
		
		private final DatasourceVO dsvo;
		
		private final DatasourceMetaVO current;
		
		private final EntityMeta<UID> datasourceMeta;

		public DatasourceMetaTransformer(DatasourceVO dsvo, DatasourceMetaVO current, EntityMeta<UID> datasourceMeta) {
			super();
			this.dsvo = dsvo;
			this.current = current;
			this.datasourceMeta = datasourceMeta;
		}

		public static boolean checkEntityUID(UID entityUID, EntityMeta<UID> datasourceMeta) {
			final String sTest = entityUID.getString();
			return sTest.startsWith(getEntityPrefix(datasourceMeta));
		}

		private static String getEntityPrefix(EntityMeta<UID> datasourceMeta) {
			return datasourceMeta.getUID().getString() + "-";
		}

		private static String getEntityFieldPrefix(EntityMeta<UID> datasourceMeta) {
			return datasourceMeta.getUID().getString() + "f-";
		}

		@Override
		public DatasourceMetaVO transform(ResultSetMetaData rsmeta) {
			if (rsmeta == null) {
				return null;
			}
			
			DatasourceMetaVO dsmeta = new DatasourceMetaVO();
			
			try {
				dsmeta.setEntityUID(new UID(getEntityPrefix(datasourceMeta)+dsvo.getId().getString()));
				
				List<DatasourceMetaVO.ColumnMeta> columns = new ArrayList<>();
				for (int i = 1; i <= rsmeta.getColumnCount(); i++) {
					String columnname = rsmeta.getColumnLabel(i);
					
					boolean forceTimestampFormat = false;
					final String tsHint = "_as_timestamp";
					if (columnname.toLowerCase().endsWith(tsHint)) {
						forceTimestampFormat = true;
					}
					
					UID fieldUID = null;
					if (current != null && current.getColumns() != null) {
						for (DatasourceMetaVO.ColumnMeta existingcol : current.getColumns()) {
							if (columnname.equals(existingcol.getColumnName())) {
								fieldUID = existingcol.getFieldUID();
							}
						}
					}
					if (fieldUID == null) {
						fieldUID = new UID(getEntityFieldPrefix(datasourceMeta)+new UID().getString());
					}
					
					Integer scale = rsmeta.getScale(i);
					Integer precision = rsmeta.getPrecision(i);
					String classname = rsmeta.getColumnClassName(i);
					Class<?> cls = Class.forName(classname);
					if (Number.class.isAssignableFrom(cls)) {
						if (LangUtils.defaultIfNull(scale, new Integer(0)).intValue() > 0) {
							classname = Double.class.getName();
						} else {
							if (columnname.toUpperCase().startsWith("BLN") && LangUtils.defaultIfNull(precision, new Integer(0)).intValue() == 1) {
								classname = Boolean.class.getName();
								scale = null;
							} else {
								classname = Integer.class.getName();
							}
						}
						if (precision == null) {
							precision = 9;
						}
					} else if (cls == java.sql.Date.class || Date.class.isAssignableFrom(Class.forName(classname))) {
						cls = Date.class;
						classname = cls.getName();
					}
					if (forceTimestampFormat && Date.class.isAssignableFrom(Class.forName(classname))) {
						classname = InternalTimestamp.class.getName();
					}
					Boolean readonly = rsmeta.isReadOnly(i);
					
					DatasourceMetaVO.ColumnMeta colmeta = new DatasourceMetaVO.ColumnMeta();
					colmeta.setFieldUID(fieldUID);
					colmeta.setColumnName(columnname);
					colmeta.setJavaType(classname);
					colmeta.setReadOnly(readonly);
					// switch scale <-> precision for nuclos
					colmeta.setPrecision(scale);
					colmeta.setScale(precision);
					columns.add(colmeta);
				}
				
				dsmeta.setColumns(columns);
			} catch (Exception ex) {
				LOG.error("Unable to generate meta data for datasource {} uid={}: {}",
				          dsvo.getName(), dsvo.getId(), ex.getMessage(), ex);
			}
			
			return dsmeta;
		}
		
	}
	
	public int cacheExpiration() {
		return serverParameterProvider.getIntValue(ParameterProvider.KEY_VLP_RESULTCACHE_EXPIRATION, 4000);
	}
	
	public Object getBooleanRepresentationInSQL(Boolean b) {
		return dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL(b);
	}

	private DatasourceVO setLocaleResouces(DatasourceVO dsVO) {
		if (dsVO instanceof ValuelistProviderVO) {
			ValuelistProviderVO vlp = (ValuelistProviderVO) dsVO;
			String resId = vlp.getDetailsearchdescriptionResourceId();
			if (resId != null) {
				String sDetailsearchdescriptionDe = localeFacade.getResourceById(LocaleInfo.parseTag(Locale.GERMAN), resId);
				String sDetailsearchdescriptionEn = localeFacade.getResourceById(LocaleInfo.parseTag(Locale.ENGLISH), resId);
				vlp.setDetailsearchdescription(sDetailsearchdescriptionDe, Locale.GERMAN);
				vlp.setDetailsearchdescription(sDetailsearchdescriptionEn, Locale.ENGLISH);
			}
		}
		return dsVO;
	}

	public DatasourceMetaVO validateDynEntitySQL(String dssql, ResultSetMetaDataTransformer<DatasourceMetaVO> transformer, String tableAliasing) throws CommonValidationException {
		final String refCol;
		if (new DataSourceCaseSensivity(dssql).isIntidGenericObjectCaseInsensitive()) {
			refCol = DataSourceCaseSensivity.REF_ENTITY;
		} else {
			refCol = "\"" + DataSourceCaseSensivity.REF_ENTITY + "\"";
		}

		String sql = "SELECT * FROM (" + RigidUtils.cleanSql(dssql) + ") " + tableAliasing + " validateDynEntitySQL WHERE validateDynEntitySQL." + refCol + " = 123";

		final DatasourceMetaVO datasourceMetaVO;
		try {
			datasourceMetaVO = validateSql(sql, transformer);
		} catch (Exception e) {
			throw new CommonValidationException("datasource.validation.dynamic.entity.sql.5");
		}

		List<String> columns = datasourceMetaVO.getColumns().stream().map(DatasourceMetaVO.ColumnMeta::getColumnName).collect(Collectors.toList());

		if (StringUtils.looksEmpty(dssql)) {
			throw new CommonValidationException("datasource.validation.dynamic.entity.sql.1");
		}

		boolean foundIntid1 = false;
		boolean foundIntid2 = false;

		if (columns != null) {
			for (String column : columns) {
				if (NuclosEntityValidator.isEntirelyInvalidIdentifier(column)) {
					throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("datasource.validation.dynamic.entity.sql.6", column));
				}
				if (column.toUpperCase().equals(DataSourceCaseSensivity.PRIMARY_KEY)
						|| column.toUpperCase().equals("\"" + DataSourceCaseSensivity.PRIMARY_KEY + "\""))
					foundIntid1 = true;
				if (column.toUpperCase().equals(DataSourceCaseSensivity.REF_ENTITY)
						|| column.toUpperCase().equals("\"" + DataSourceCaseSensivity.REF_ENTITY + "\""))
					foundIntid2 = true;
			}
		}

		if (!foundIntid1) {
			throw new CommonValidationException("datasource.validation.dynamic.entity.sql.2");
		}
		if (!foundIntid2) {
			throw new CommonValidationException("datasource.validation.dynamic.entity.sql.3");
		}
		final Collection<String> parametersFromSql = DatasourceUtils.getParametersFromString(sql);
		for (String param : parametersFromSql) {
			if (!"username".equals(param) && !"mandator".equals(param) && !"locale".equals(param)) {
				throw new CommonValidationException("datasource.validation.dynamic.entity.sql.4");
			}
		}

		return datasourceMetaVO;
	}

	public DatasourceMetaVO validateDynTasklistSQL(String dssql, ResultSetMetaDataTransformer<DatasourceMetaVO> transformer, String tableAliasing) throws CommonValidationException {
		final DatasourceMetaVO datasourceMetaVO;
		try {
			datasourceMetaVO = validateSql(dssql, transformer);
		} catch (Exception e) {
			throw new CommonValidationException("datasource.validation.dynamic.tasklist.sql.3");
		}

		List<String> columns = datasourceMetaVO.getColumns().stream().map(DatasourceMetaVO.ColumnMeta::getColumnName).collect(Collectors.toList());
		if (StringUtils.looksEmpty(dssql)) {
			throw new CommonValidationException("datasource.validation.dynamic.tasklist.sql.1");
		}

		boolean foundIntid1 = false;

		for (String column : columns) {
			if (column.equalsIgnoreCase(DataSourceCaseSensivity.PRIMARY_KEY)
					|| column.equalsIgnoreCase("\"" + DataSourceCaseSensivity.PRIMARY_KEY + "\"")) {
				foundIntid1 = true;
			}
		}

		if (!foundIntid1) {
			throw new CommonValidationException("datasource.validation.dynamic.tasklist.sql.2");
		}

		return datasourceMetaVO;
	}

	public <I, O> O validateDynTasklistRule(String datasourceRule, Transformer<I, O> transformer) throws CommonValidationException {
		try {
			return validateRule(datasourceRule, transformer);
		} catch (Exception e) {
			throw new CommonValidationException("datasource.validation.dynamic.tasklist.sql.3");
		}
	}

	public static <T> T validateChartEntitySQL(String sql, List<String> columns, ResultSetMetaDataTransformer<T> transformer) throws CommonValidationException {
		if (StringUtils.looksEmpty(sql)) {
			throw new CommonValidationException("datasource.validation.chart.entity.sql.1");
		}

		boolean foundIntid1 = false;
		boolean foundIntid2 = false;

		if (columns != null) {
			for (String column : columns) {
				if (column.toUpperCase().equals(DataSourceCaseSensivity.PRIMARY_KEY)
						|| column.toUpperCase().equals("\"" + DataSourceCaseSensivity.PRIMARY_KEY + "\""))
					foundIntid1 = true;
				if (column.toUpperCase().equals(DataSourceCaseSensivity.REF_ENTITY)
						|| column.toUpperCase().equals("\"" + DataSourceCaseSensivity.REF_ENTITY + "\""))
					foundIntid2 = true;
			}
		}

		boolean foundParam1 = false;

		for (String param : DatasourceUtils.getParametersFromString(sql)) {
			if (param.equals("genericObject"))
				foundParam1 = true;
		}

		if (!foundIntid1) {
			throw new CommonValidationException("datasource.validation.chart.entity.sql.2");
		}
		if (!foundIntid2) {
			throw new CommonValidationException("datasource.validation.chart.entity.sql.3");
		}
		if (!foundParam1) {
			throw new CommonValidationException("datasource.validation.chart.entity.sql.4");
		}

		return null;
	}

	public  <T> T validateChartEntitySQLWithParameters(String sql, ResultSetMetaDataTransformer<T> transformer) throws CommonValidationException {
		if (StringUtils.looksEmpty(sql)) {
			throw new CommonValidationException("datasource.validation.chart.entity.sql.1");
		}

		try {
			return validateSql(sql, transformer);
		} catch (Exception e) {
			throw new CommonValidationException("datasource.validation.chart.entity.sql.5");
		}
	}

	public DatasourceMetaVO validateRecordGrantSQL(final EntityMeta<?> entityMeta, String sql, ResultSetMetaDataTransformer<DatasourceMetaVO> transformer) throws CommonValidationException {
		final DatasourceMetaVO datasourceMetaVO;
		try {
			datasourceMetaVO = validateSql(sql, transformer);
		} catch (Exception e) {
			throw new CommonValidationException("datasource.validation.recordgrant.sql.4");
		}


		if (StringUtils.looksEmpty(sql)) {
			throw new CommonValidationException("datasource.validation.recordgrant.sql.3");
		}

		if (entityMeta != null && !StringUtils.looksEmpty(sql)) {

			List<String> columns = getColumns(sql);

			boolean foundId = false;
			boolean foundMoreId = false;

			String pkColumn = entityMeta.getPk().getDbColumn();
			if (columns != null) {
				for (String column : columns) {
					if (column.toUpperCase().equals(pkColumn) || column.toUpperCase().equals("\"" + pkColumn + "\"")) {
						if (foundId)
							foundMoreId = true;
						foundId = true;
					}
				}
			}

			if (!foundId) {
				throw new CommonValidationException("datasource.validation.recordgrant.sql.1");
			}
			if (foundMoreId) {
				throw new CommonValidationException("datasource.validation.recordgrant.sql.2");
			}
		}

		return datasourceMetaVO;
	}

	private DatasourceVO setLocaleResources(DatasourceVO dsVO) {
		if (dsVO instanceof ValuelistProviderVO) {
			ValuelistProviderVO vlp = (ValuelistProviderVO) dsVO;
			String resId = vlp.getDetailsearchdescriptionResourceId();
			if (resId != null) {
				String sDetailsearchdescriptionDe = localeFacade.getResourceById(LocaleInfo.parseTag(Locale.GERMAN), resId);
				String sDetailsearchdescriptionEn = localeFacade.getResourceById(LocaleInfo.parseTag(Locale.ENGLISH), resId);
				vlp.setDetailsearchdescription(sDetailsearchdescriptionDe, Locale.GERMAN);
				vlp.setDetailsearchdescription(sDetailsearchdescriptionEn, Locale.ENGLISH);
			}
		}
		return dsVO;
	}

	/**
	 * @param datasourceVO
	 * @param referencedDataSourceUIDs
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 * @throws CommonStaleVersionException
	 * @throws CommonRemoveException
	 * @throws NuclosBusinessRuleException
	 * @throws CommonCreateException
	 * @throws NuclosCompileException
	 */
	private void replaceUsedDatasourceList(final DatasourceVO datasourceVO, final List<UID> referencedDataSourceUIDs)
			throws CommonFinderException, CommonValidationException, CommonPermissionException, NuclosBusinessRuleException, CommonRemoveException,
			CommonStaleVersionException, CommonCreateException {
		final DataSourceType datasourceType = DataSourceType.getFromDatasourceVO(datasourceVO);


		// 1. remove all entries for this id:
		CollectableComparison condUsage = SearchConditionUtils.newKeyComparison(datasourceType.fieldEntityUID, ComparisonOperator.EQUAL, datasourceVO.getPrimaryKey());
		List<UID> lstIds = getMasterDataFacade().getMasterDataIdsNoCheck(datasourceType.entityUsage.getUID(), new CollectableSearchExpression(condUsage));
		for (UID id : lstIds) {
			getMasterDataFacade().remove(datasourceType.entityUsage.getUID(), id, null);
		}

		// 2. insert the new entries:
		for (final UID dataSourceUID : referencedDataSourceUIDs) {
			EntityObjectVO<UID> vo = new EntityObjectVO<UID>(datasourceType.entityUsage);
			MasterDataVO<UID> newEntryVO = new MasterDataVO<UID>(vo, true);
			newEntryVO.setFieldUid(datasourceType.fieldEntityUID, datasourceVO.getPrimaryKey());
			newEntryVO.setFieldUid(datasourceType.fieldEntityUsedUID, dataSourceUID);

			getMasterDataFacade().create(newEntryVO, null);
		}
	}

	/**
	 * //TODO MULTINUCLET
	 *
	 * set test values for every parameter for sysntax check
	 *
	 * @param sDatasourceXML
	 * @return Map<String sName, String sValue>
	 */
	private Map<String, Object> getTestParameters(final String sDatasourceXML) {
		final Map<String, Object> result = new HashMap<String, Object>();

		final List<DatasourceParameterVO> lstParams;
		try {
			lstParams = this.getParametersFromXML(sDatasourceXML);
		}
		catch (NuclosDatasourceException e) {
			// No parameters defined?
			return result;
		}

		for (final DatasourceParameterVO paramvo : lstParams) {
			final String sValue = DatasourceUtils.getTestValueForParameter(paramvo, getBooleanRepresentationInSQL(Boolean.FALSE));
			result.put(paramvo.getParameter(), sValue);
			if (paramvo.getValueListProvider() != null) {
				result.put(paramvo.getParameter() + "Id", "123434");
			}
		}
		result.put("username", nuclosFacade.getCurrentUserName());

		return result;
	}

	private void validateUniqueConstraint(DatasourceVO datasourcevo) throws CommonValidationException {
		DatasourceVO foundDatasourceVO = this.getDatasource(datasourcevo.getPrimaryKey());
		if (foundDatasourceVO != null && !datasourcevo.getId().equals(foundDatasourceVO.getId())) {
			throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("validation.unique.constraint", "Name", "Data source"));
		}
	}

	@Override
	public CollectableField getDefaultValue(final UID dataSourceUID, final String valuefield, String idfield, String defaultfield, DatasourceQueryExecutionParameters qParams, UID baseEntityUID, final UID entityFieldUID, UID mandatorUID) throws CommonBusinessException {
		return getDefaultValue(dataSourceUID, valuefield, idfield, defaultfield, qParams.getParameterMap(), baseEntityUID, entityFieldUID, mandatorUID, qParams.getLanguageUID());
	}

	/**
	 * get a datasource result by datasource id
	 * used by a client
	 */
	@Override
	public ResultVO executeQuery(final UID dataSourceUID, final DatasourceQueryExecutionParameters qParams, final UID mandatorUid) throws NuclosDatasourceException {
		return executeQuery(dataSourceUID, qParams.getParameterMap(), qParams.getMaxRowCount(), qParams.getLanguageUID(), mandatorUid);
	}

	/**
	 * get a datasource result by datasource id
	 * used by server
	 */
	@Override
	public ResultVO executeQuery(final UID dataSourceUID, final Map<String, Object> mpParams, Integer iMaxRowCount, UID languageUID, UID mandatorUID) throws NuclosDatasourceException {
		final ResultVO result;
		if (iMaxRowCount == null && sessionUtils.isCalledRemotely()) {
			iMaxRowCount = DbExecutor.LimitedResultSetRunner.MAXFETCHSIZE;
		}
		DatasourceVO datasource = this.getDatasource(dataSourceUID);

		DatasourceXMLParser.Result parsedDatasource = datasourceServerUtils.parseDatasourceXML(datasource.getSource());
		if (datasource.getDatasourceRule() != null) {
			result = getDataFromRule(datasource, mpParams, iMaxRowCount, languageUID, mandatorUID);
		} else {
			result = executeQueryForDataSourceWithoutPermissionCheck(datasource, mpParams, iMaxRowCount, languageUID, mandatorUID);
		}

		return result;
	}

	/**
	 * gets a datasource result by datasource xml
	 */
	@RolesAllowed("Login")
	public ResultVO executeQueryForDataSource(DatasourceVO datasourcevo, DatasourceQueryExecutionParameters qParams) throws CommonBusinessException, NuclosDatasourceException {
		return executeQueryForDataSource(datasourcevo, qParams, nuclosFacade::getCurrentMandatorUID);
	}

	/**
	 * gets a datasource result by datasource xml
	 *
	 * @param datasourcevo     datasource vo
	 * @param qParams          all necessary parameters to execute a query
	 * @param mandatorSupplier
	 * @return report/form filled with data
	 */
	public ResultVO executeQueryForDataSource(DatasourceVO datasourcevo, DatasourceQueryExecutionParameters qParams, final Supplier<UID> mandatorSupplier) throws CommonBusinessException {
		try {
			if (datasourcevo.getDatasourceRule() != null) {
				return getDataFromRule(datasourcevo, qParams.getParameterMap(), qParams.getMaxRowCount(), qParams.getLanguageUID(), mandatorSupplier.get());
			} else {
				return executeQueryForDataSourceWithoutPermissionCheck(datasourcevo, qParams.getParameterMap(), qParams.getMaxRowCount(), qParams.getLanguageUID(), mandatorSupplier.get());
			}
		} catch (DbException e) {
			throw new NuclosDatasourceException(e.getCause().getMessage(), e);
		}
	}

	protected ResultVO executeQueryForDataSourceWithoutPermissionCheck(DatasourceVO datasourcevo, Map<String, Object> mpParams, Integer iMaxRowCount, UID languageUID, UID mandatorUID) throws NuclosDatasourceException {
		final String sQuery = createSQL(datasourcevo, mpParams, mandatorUID, languageUID);

		return executePlainQueryAsResultVO(sQuery, iMaxRowCount);
	}

	@Override
	public List<? extends CollectableField> executeQueryForVLP(VLPQuery query) throws CommonBusinessException {
		Long maxRows = 250000l;
		if (query.getMaxRowCount() != null && query.getMaxRowCount() < 250000) {
			maxRows = query.getMaxRowCount().longValue();
		}
		List<? extends CollectableField> result = nuclosFacade.getReferenceList(
				getDataBaseHelper(),
				datasourceServerUtils,
				getSessionUtils(),
				query.getEntityFieldUID(),
				StringUtils.defaultIfNull(query.getQuickSearchInput(), ""),
				query.getValuelistProviderUid(),
				query.getQueryParams(),
				query.getDefaultField(),
				maxRows,
				query.getMandatorUID(),
				!StringUtils.looksEmpty(query.getQuickSearchInput()));
		return result;
	}

	public <I, O> O validateRule(String datasourceRule, Transformer<I, O> transformer) throws CommonValidationException, NuclosDatasourceException {
		Map<String, Object> mpParams = new HashMap<>();
		mpParams.put("limit", 1);
		mpParams.put("offset", 0);
		final List<ResultColumnVO> columns = datasourceServerUtils.getDataFromRule(
				datasourceRule, mpParams, -1,
				new DataLanguageSupplier(null), null, new AccessibleMandatorsSupplier(null),
				context -> {
					try {
						return (DatasourceResult) eventSupport.executeDatasourceRuleCall(context);
					} catch (NuclosBusinessRuleException | NuclosRuleCompileException e) {
						throw new NuclosFatalException(e);
					}
				}).getColumns();

		return transformer.transform((I) columns);
	}

	@Override
	protected ResultVO getDataFromRule(DatasourceVO datasource, Map<String, Object> mpParams, Integer iMaxRowCount, UID languageUID, UID mandatorUID) throws NuclosDatasourceException {
		return datasourceServerUtils.getDataFromRule(
				datasource.getDatasourceRule(), mpParams, -1,
				new DataLanguageSupplier(null), null, new AccessibleMandatorsSupplier(null),
				context -> {
					try {
						return (DatasourceResult) eventSupport.executeDatasourceRuleCall(context);
					} catch (NuclosBusinessRuleException | NuclosRuleCompileException e) {
						throw new NuclosFatalException(e);
					}
				});
	}

	private final MasterDataFacadeLocal getMasterDataFacade() {
		return masterDataFacade;
	}

	@Override
	public boolean isMandator(UID baseEntityUID) {
		return baseEntityUID != null ? metaProvider.getEntity(baseEntityUID).isMandator() : false;
	}
}
