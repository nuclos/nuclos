package org.nuclos.server.report.ejb3;

import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

public interface ExtendedDatasourceFacadeLocal extends DatasourceFacadeLocal {

	CollectableField getDefaultValue(UID datasource, String valuefield, String idfield, String defaultfield, Map<String, Object> params, UID baseEntityUID, final UID entityFieldUID, UID mandatorUID, UID languageUID) throws CommonBusinessException;

	ResultVO executeQuery(UID dataSourceUid, Map<String, Object> params, Integer maxRows, UID language, UID mandatorUid) throws NuclosDatasourceException;
}
