//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report.ejb3;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.SerializationUtils;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common.report.JRUtils;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.common.NuclosSystemParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlDigesterFactory;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class ReportCompiler {
	private final static Logger LOG = LoggerFactory.getLogger(ReportCompiler.class);

	/**
	 * compiles a report xml definition (jasperreports)
	 *
	 * @param sourceFileContent report layout definition
	 * @return compiled jasper report
	 */
	private static JasperReport createJasperReport(byte[] sourceFileContent) {
		final String sReportXML = new String(sourceFileContent, StandardCharsets.UTF_8);

		checkCompileDir();
		try {
			final Charset charset = IOUtils.guessXmlEncoding(sReportXML);
			if (charset == null) {
				if (SpringApplicationContextHolder.isSpringReady()) {
					SpringLocaleDelegate localeDelegate = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
					String message = localeDelegate.getMessage("reportCompiler.invalidXML.encoding", "Invalid XML-declaration: encoding could not be guessed.");
					throw new NuclosFatalException(message);
				}
				throw new NuclosFatalException("Spring is not ready ...");
			}
			InputSource source = new InputSource(new ByteArrayInputStream(sReportXML.getBytes(charset)));
			source.setEncoding(charset.name());
			JasperReportsContext jpc = DefaultJasperReportsContext.getInstance();
			JasperDesign design;
			try {
				design = new JRXmlLoader(jpc, JRXmlDigesterFactory.createDigester(jpc)).loadXML(source);
			} catch (ParserConfigurationException | SAXException e) {
				throw new NuclosFatalException(e);
			}
			return JasperCompileManager.compileReport(design);
		} catch (JRException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	private static void checkCompileDir() {
		String compileDir = NuclosSystemParameters.getString(NuclosSystemParameters.JASPER_REPORTS_COMPILE_TMP);
		File file = new File(compileDir);
		if (!file.exists()) {
			if (file.mkdirs()) {
				LOG.info("Created missing reports compile directory: {}", file.getAbsolutePath());
			}
		}
	}

	public static ByteArrayCarrier compileReport(ByteArrayCarrier sourceFileContent) {
		return compileReport(sourceFileContent.getData());
	}

	private static ByteArrayCarrier compileReport(byte[] sourceFileContent) {
		return new ByteArrayCarrier(SerializationUtils.serialize(createJasperReport(sourceFileContent)));
	}
}
