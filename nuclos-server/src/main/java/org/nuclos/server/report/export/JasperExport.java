//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report.export;

import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.nuclos.api.parameter.NucletParameter;
import org.nuclos.common.E;
import org.nuclos.common.MarshalledValue;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ReportFieldDefinition;
import org.nuclos.common.report.ReportFieldDefinitionFactory;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO.PageOrientation;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ReportParameterVO;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.parameter.NuclosParameterProvider;
import org.nuclos.server.report.Export;
import org.nuclos.server.report.JRDefaultNuclosDataSource;
import org.nuclos.server.report.JREmptyNuclosDataSource;
import org.nuclos.server.report.JRFileRepositoryService;
import org.nuclos.server.report.JRRuleBasedNuclosDataSource;
import org.nuclos.server.report.ResultVODataSource;
import org.nuclos.server.report.ejb3.DatasourceFacadeLocal;
import org.nuclos.server.report.ejb3.ExtendedDatasourceFacadeLocal;
import org.nuclos.server.report.ejb3.ReportFacadeLocal;
import org.nuclos.server.report.api.JRNuclosDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRStyle;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.SimpleJasperReportsContext;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignImage;
import net.sf.jasperreports.engine.design.JRDesignSection;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.HorizontalImageAlignEnum;
import net.sf.jasperreports.engine.type.HorizontalTextAlignEnum;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.type.OrientationEnum;
import net.sf.jasperreports.engine.type.VerticalImageAlignEnum;
import net.sf.jasperreports.engine.type.VerticalTextAlignEnum;
import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;
import net.sf.jasperreports.engine.util.JRFontNotFoundException;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.repo.FileRepositoryPersistenceServiceFactory;
import net.sf.jasperreports.repo.PersistenceServiceFactory;
import net.sf.jasperreports.repo.RepositoryService;

@Configurable
public class JasperExport extends Export {

	private static final Logger LOG = LoggerFactory.getLogger(JasperExport.class);
	
	final static int DIN_A4_HEIGHT = 842;
	final static int DIN_A4_WIDTH = 595;

	private DataSource dataSource;

	private MasterDataFacadeLocal masterDatafacade;

	private ReportFacadeLocal reportfacade;

	private ExtendedDatasourceFacadeLocal datasourceFacade;
	
	@Autowired
	private NuclosParameterProvider parameterProvider;
	
	private String searchConditionText;
	private ReportOutputVO.PageOrientation pageOrientation;
	private boolean columnScaled;
	
	private static String SEARCHRESULTELEMENT = "searchresultfield";
	
	public JasperExport() {
		super();
		this.searchConditionText = "";
		this.pageOrientation = PageOrientation.PORTRAIT;
		this.columnScaled = false;
	}			
	
	public JasperExport(String searchCondition, PageOrientation pageOrientation, boolean columnScaled) {
		super();
		this.searchConditionText = searchCondition;
		this.pageOrientation = pageOrientation;
		this.columnScaled = columnScaled;
	}

	@Autowired
	private void inject(
			@Qualifier("nuclos") final DataSource dataSource,
			final MasterDataFacadeLocal masterDatafacade,
			final ReportFacadeLocal reportfacade,
			final ExtendedDatasourceFacadeLocal datasourceFacade
	) {
		this.dataSource = dataSource;
		this.masterDatafacade = masterDatafacade;
		this.reportfacade = reportfacade;
		this.datasourceFacade = datasourceFacade;
	}

	public boolean isReportFontMissing(DefaultReportOutputVO output, UID mandatorUID) {
		try {
			final JasperReport jr = deserializeJasperReportObject(output.getReportCLS());
			final JRDataSource jrdatasource = new JREmptyNuclosDataSource(1);

			final Map<String, Object> params = new HashMap<String, Object>();

			for (ReportParameterVO reportParameter : reportfacade.getReportParameters(E.SUBREPORT, output.getId())) {
				String parametername = reportParameter.getParameterName();
				if (!StringUtils.isNullOrEmpty(reportParameter.getParameterValueText())) {
					params.put(parametername, reportParameter.getParameterValueText());
				} else {
					final JasperReport jrsubreport = deserializeJasperReportObject(reportParameter.getReportCLS());
					params.put(parametername, jrsubreport);
				}
			}

			params.put("REPORT_DATA_SOURCE", jrdatasource);
			JasperReportsContext context = setDefaultsParameters(params, output, mandatorUID);
			JasperFillManager jfm = JasperFillManager.getInstance(context);
			jfm.fill(jr, params, jrdatasource);
		}
		catch(JRFontNotFoundException ex) {
			return true;
		}
		catch (Exception e) {

		}

		return false;
	}

	@Override
	public NuclosFile test(DefaultReportOutputVO output, UID mandatorUID) throws NuclosReportException {
		try {
			final JasperReport jr = deserializeJasperReportObject(output.getReportCLS());
			final JRDataSource jrdatasource = new JREmptyNuclosDataSource(1);

			final Map<String, Object> params = new HashMap<String, Object>();

			for (ReportParameterVO reportParameter : reportfacade.getReportParameters(E.SUBREPORT, output.getId())) {
				String parametername = reportParameter.getParameterName();
				if (!StringUtils.isNullOrEmpty(reportParameter.getParameterValueText())) {
					params.put(parametername, reportParameter.getParameterValueText());
				} else {
					final JasperReport jrsubreport = deserializeJasperReportObject(reportParameter.getReportCLS());
					params.put(parametername, jrsubreport);
				}
			}

			params.put("REPORT_DATA_SOURCE", jrdatasource);
			JasperReportsContext context = setDefaultsParameters(params, output, mandatorUID);
			context.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");
			JasperFillManager jfm = JasperFillManager.getInstance(context);
			JasperPrint jp = jfm.fill(jr, params, jrdatasource);
			String name = (output.getDescription() != null ? output.getDescription() : "Report") + output.getFormat().getExtension();
			return new NuclosFile(name, JasperExportManager.exportReportToPdf(jp));
		}
		catch (InternalError ex) {
			LOG.error("Unexpected JasperReport ERROR: {} on {}", ex, output, ex);
			throw new NuclosReportException("JasperExport.test(" + output + ") failed", ex);
		}
		catch (JRException ex) {
			throw new NuclosReportException("JasperExport.test(" + output + ") failed", ex);
		}
	}

	@Override
	public NuclosFile export(DefaultReportOutputVO output, Map<String, Object> params, Locale locale, int maxrows, UID language, UID mandatorUID) throws NuclosReportException {
		try {
			Map<String, Object> mpParams2 = new HashMap<>(params);
			ByteArrayCarrier reportCLS = output.getReportCLS();
			final JasperReport jr = (reportCLS) != null ? deserializeJasperReportObject(reportCLS) : null;
			final String sSourceFileName = output.getSourceFile();
			if (sSourceFileName == null && output.getSourceResource() == null) {
				throw new NuclosReportException("report.error.missing.template.2");
			}
			if (jr == null) {
				throw new NuclosReportException("report.error.invalid.template");
			}

			for (ReportParameterVO reportParameter : reportfacade.getReportParameters(E.SUBREPORT, output.getId())) {
				String parametername = reportParameter.getParameterName();

				if (!StringUtils.isNullOrEmpty(reportParameter.getParameterValueText())) {
					mpParams2.put(parametername, reportParameter.getParameterValueText());
				} else {
					ByteArrayCarrier subreportCLS = reportParameter.getReportCLS();
					final JasperReport jrsubreport = (reportCLS) != null ? deserializeJasperReportObject(subreportCLS) : null;
					final String subreportSourceFileName = reportParameter.getSourcefileName();
					final UID subreportSourceResource = reportParameter.getParameterResource();
					if (subreportSourceFileName == null && subreportSourceResource == null) {
						throw new NuclosReportException("subreport.error.missing.template");
					}
					if (jrsubreport == null) {
						throw new NuclosReportException("subreport.error.invalid.template");
					}

					mpParams2.put(parametername, jrsubreport);
				}
			}

			JasperPrint jprint = null;
			if (output.getDatasourceUID() != null) {
				// get existing connection (enlisted in current transaction)
				Connection conn = DataSourceUtils.getConnection(dataSource);
				final DatasourceVO datasourceVO = datasourceFacade.getDatasource(output.getDatasourceUID());
				final JRNuclosDataSource ds;

				if (datasourceVO.getDatasourceRule() != null) {
					ds = new JRRuleBasedNuclosDataSource(datasourceVO, params, datasourceVO.getNucletUID(), language, SecurityContextHolder.getContext().getAuthentication());
				} else {
					ds = new JRDefaultNuclosDataSource(
							datasourceVO, params, conn, datasourceVO.getNucletUID(), language, SecurityContextHolder.getContext().getAuthentication());
				}

				JasperReportsContext context = setDefaultsParameters(mpParams2, output, mandatorUID);
				context.setProperty("net.sf.jasperreports.awt.ignore.missing.font", "true");
				mpParams2.put(JRParameter.REPORT_DATA_SOURCE, ds);
				mpParams2.put(JRParameter.REPORT_LOCALE, locale);

				try {
					if (!TransactionSynchronizationManager.isActualTransactionActive()) {
						LOG.warn("AUTO COMMITTING (No active transaction!) ... Jasper.fill " + output + "\n=" + Arrays.asList(mpParams2));
					}
					JasperFillManager jfm = JasperFillManager.getInstance(context);
					jprint = jfm.fill(jr, mpParams2, ds);

					if (ds.getSize() == 0 && (WhenNoDataTypeEnum.ALL_SECTIONS_NO_DETAIL != jr.getWhenNoDataTypeValue()
							&& WhenNoDataTypeEnum.NO_DATA_SECTION != jr.getWhenNoDataTypeValue())) {
						throw new NuclosReportException("report.exception.nodata");
					}
				}
				catch (JRException e) {
					throw new NuclosReportException(e.getMessage(), e);
				}
				catch (DbException e) {
					throw new NuclosReportException(e);
				}
				catch (RuntimeException e) {
					throw new NuclosReportException(e.getMessage(), e);
				}
			}
			else {
				//While testing this else block was never reached. Not sure what purpose the person before me had in mind.
				SimpleJasperReportsContext ctx = new SimpleJasperReportsContext();

				jprint = JasperFillManager.getInstance(ctx).fill(jr, mpParams2, new JREmptyDataSource(1));
			}
			
			try {
				ResultVO resultvo = datasourceFacade.executeQuery(output.getDatasourceUID(), params, maxrows, language, mandatorUID);
				List<ReportFieldDefinition> fields = ReportFieldDefinitionFactory.getFieldDefinitions(resultvo);
				String name = getPath(!StringUtils.isNullOrEmpty(output.getFilename()) ? output.getFilename() : output.getDescription(), resultvo, fields) + output.getFormat().getExtension();
				return new NuclosFile(name, JasperExportManager.exportReportToPdf(jprint));
				
			} catch (Exception ex) {
				throw new NuclosReportException(ex);
			}
		}
		catch (JRException ex) {
			throw new NuclosReportException(ex);
		}
	}

	@Override
	public NuclosFile export(ReportOutputVO output, ResultVO result, List<ReportFieldDefinition> fields) throws NuclosReportException {
		try {
			final JasperDesign jrdesign = getJrDesignForSearchResult();
			createFields(jrdesign, fields, output, result, pageOrientation, columnScaled);

			// create Jasper Reports directory - this directory is created by the installer but not in integration tests
			NuclosSystemParameters.getDirectory(NuclosSystemParameters.JASPER_REPORTS_COMPILE_TMP).mkdirs();

			JasperPrint jp = JasperFillManager.fillReport(JasperCompileManager.compileReport(jrdesign), null, new ResultVODataSource(result, fields));

			String name = null;
		    if (null != output) {
		    	name = getPath((!StringUtils.isNullOrEmpty(output.getFilename())) ? output.getFilename() : output.getDescription(), result, fields) + output.getFormat().getExtension();
		    } else {
		    	// for resultlist export (print)
		    	name = "Export" + ReportOutputVO.Format.PDF.getExtension();
		    	name = getPath(name, result, fields); 
		    }
			return new NuclosFile(name, JasperExportManager.exportReportToPdf(jp));
		}
		catch (JRException ex) {
			throw new NuclosReportException(ex);
		}
	}

	private JasperReport deserializeJasperReportObject(ByteArrayCarrier b) throws CommonFatalException {
		Object obj;
		try {
			obj = IOUtils.fromByteArray(b.getData());
		}
		catch (IOException e) {
			throw new CommonFatalException(e);
		}
		catch (ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
		if (obj instanceof MarshalledValue) {
			obj = ((MarshalledValue) obj).get();
		}
		return (JasperReport) obj;
	}

	private JasperReportsContext setDefaultsParameters(Map<String, Object> parameters, ReportOutputVO output, UID mandatorUID) {
		SimpleJasperReportsContext context = new SimpleJasperReportsContext();
		UID nucletUID = NucletDalProvider.getInstance().getEntityObjectProcessor(E.REPORT).getByPrimaryKey(output.getReportUID()).getFieldUid(E.REPORT.nuclet);

		// get all nuclet parameter
		Map<UID, String> nucletParameterNames = parameterProvider.getNucletParameterNames();
		Collection<NucletParameter> nucletParametersByNuclet = parameterProvider.getNucletParametersByNuclet(nucletUID);
		for (NucletParameter parameter : nucletParametersByNuclet) {
			try {
				final UID parameterUID = new UID(parameter.getId());
				final String name = nucletParameterNames.get(parameterUID);
				final String value;
				if (SecurityCache.getInstance().isMandatorPresent()) {
					Set<UID> accessibleMandators = SecurityCache.getInstance().getAccessibleMandators(mandatorUID);
					value = parameterProvider.getNucletParameterByMandators(parameter, accessibleMandators);
				} else {
					value = parameterProvider.getNucletParameter(parameter);
				}
				parameters.put(name, value);
			} catch (Exception ex) {
				LOG.error(ex.getMessage(), ex);
			}
		}

		JRFileRepositoryService fileRepository = new JRFileRepositoryService(nucletUID, mandatorUID);
		context.setExtensions(RepositoryService.class, Collections.singletonList(fileRepository));
		context.setExtensions(PersistenceServiceFactory.class, Collections.singletonList(FileRepositoryPersistenceServiceFactory.getInstance()));

		String username = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		parameters.put("NUCLOS_USER_NAME", username);
		final CollectableComparison cond = SearchConditionUtils.newComparison(E.USER.username, ComparisonOperator.EQUAL, username);
		final Collection<MasterDataVO<UID>> collmdvo = masterDatafacade.getMasterData(E.USER, cond);
		if (collmdvo.size() == 1) {
			MasterDataVO<UID> user = collmdvo.iterator().next();
			parameters.put("NUCLOS_USER_EMAIL", user.getFieldValue(E.USER.email));
			parameters.put("NUCLOS_USER_FIRSTNAME", user.getFieldValue(E.USER.firstname));
			parameters.put("NUCLOS_USER_LASTNAME", user.getFieldValue(E.USER.lastname));
		}
		if (SecurityCache.getInstance().isMandatorPresent() && mandatorUID != null) {
			String name = SecurityCache.getInstance().getMandator(mandatorUID).getName();
			parameters.put("NUCLOS_MANDATOR", name);
		}
		return context;
	}

	/**
	 * This Method creates a JasperDesign, which will later be used to export a SubForm as a Print.
	 * @return
	 * @throws JRException
	 * @throws NuclosReportException
	 */
	@SuppressWarnings("deprecation")
	public JasperDesign getJrDesignForSearchResult() throws JRException, NuclosReportException {

		InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("resources/reports/JR_SearchResultTemplate.jrxml");
		if (input == null) {
			throw new NuclosFatalException("Default search result template missing");
		}

		JasperDesign jrdesignTemp = JRXmlLoader.load(input);

		final JasperDesign result = jrdesignTemp;
		result.setName(jrdesignTemp.getName());
		result.setTopMargin(jrdesignTemp.getTopMargin());
		result.setBottomMargin(jrdesignTemp.getBottomMargin());
		result.setLeftMargin(jrdesignTemp.getLeftMargin());
		result.setRightMargin(jrdesignTemp.getRightMargin());

		result.setColumnCount(jrdesignTemp.getColumnCount());
		result.setOrientation(OrientationEnum.getByValue(pageOrientation.getValue()));
		if(pageOrientation.getValue() == OrientationEnum.PORTRAIT.getValue()) {
			result.setPageWidth(DIN_A4_WIDTH);
			result.setPageHeight(DIN_A4_HEIGHT);
		}
		else {
			result.setPageWidth(DIN_A4_HEIGHT);
			result.setPageHeight(DIN_A4_WIDTH);
		}
		
		result.setTitleNewPage(jrdesignTemp.isTitleNewPage());
		result.setWhenNoDataType(jrdesignTemp.getWhenNoDataTypeValue());
		result.setTitle(jrdesignTemp.getTitle());
		result.setPageHeader(jrdesignTemp.getPageHeader());
		result.setColumnHeader(jrdesignTemp.getColumnHeader());
		result.setPageFooter(jrdesignTemp.getPageFooter());
		jrdesignTemp.getDetailSection();

		final JRStyle reportfontRegular = jrdesignTemp.getStylesMap().get("Regular");
		if (reportfontRegular == null) {
			throw new NuclosReportException("report.error.invalid.font.regular");// "Font mit dem Bezeichner 'Regular' muss in der Suchergebnisvorlage definiert sein.");
		}
		final JRStyle reportfontBold = jrdesignTemp.getStylesMap().get("Bold");
		if (reportfontBold == null) {
			throw new NuclosReportException("report.error.invalid.font.bold");// "Font mit dem Bezeichner 'Bold' muss in der Suchergebnisvorlage definiert sein.");
		}

		//For some reason one needs to set the default font size here in the new JasperVersion
		result.getStylesMap().get("Regular").setFontSize(10.f);
		result.getStylesMap().get("Bold").setFontSize(10.f);

		final JRStyle regularStyle = result.getStylesMap().get("Regular");
		final JRDesignBand footer = (JRDesignBand) result.getPageFooter();
		JRDesignStaticText staticField = new JRDesignStaticText();
		staticField.setX(20);
		staticField.setWidth(300);
		staticField.setHeight(14);
		staticField.setHorizontalTextAlign(HorizontalTextAlignEnum.LEFT);
		staticField.setText(SpringLocaleDelegate.getInstance().getMessage("ReportFormatPanel.9", "Suchergebnis") + ": " + (this.searchConditionText != null ? this.searchConditionText : ""));
		staticField.setStyle(regularStyle);
		staticField.setKey(SEARCHRESULTELEMENT);

		footer.addElement(staticField);

		return result;
	}

	@SuppressWarnings("deprecation")
	public static void createFields(JasperDesign jrdesign, List<ReportFieldDefinition> fields, ReportOutputVO output, ResultVO result, PageOrientation orientation, boolean columnScaled) {
		
		final int MAX_DATE_WIDTH = 10;
		final int MAX_STRING_WIDTH = 25; // extended = long strings width char >
											// 25
		final int MAX_BOOLEAN_WIDTH = 4;
		final int MAX_INTEGER_WIDTH = 5;
		final int MAX_DOUBLE_WIDTH = 7;

		final JRDesignBand pageHeader = (JRDesignBand) jrdesign.getPageHeader();
		final JRDesignBand columnHeader = (JRDesignBand) jrdesign.getColumnHeader();
		final JRDesignSection detail = (JRDesignSection) jrdesign.getDetailSection();
		final JRDesignBand detailBand;

		final JRStyle regularStyle = jrdesign.getStylesMap().get("Regular");
		final JRStyle boldStyle = jrdesign.getStylesMap().get("Bold");
		final Font fontPlain = new Font(regularStyle.getFontName(), Font.PLAIN, regularStyle.getFontsize().intValue());
		final Font fontBold = new Font(boldStyle.getFontName(), Font.BOLD, boldStyle.getFontsize().intValue());
		if (pageHeader == null) {
			throw new CommonFatalException(SpringLocaleDelegate.getInstance().getMessage("ReportController.5", "Bereich <PageHeader> muss in der Suchergebnisvorlage definiert sein."));
		}
		if (columnHeader == null) {
			throw new CommonFatalException(SpringLocaleDelegate.getInstance().getMessage("ReportController.3", "Bereich <ColumnHeader> muss in der Suchergebnisvorlage definiert sein."));
		}
		if (detail == null) {
			throw new CommonFatalException(SpringLocaleDelegate.getInstance().getMessage("ReportController.4", "Bereich <Detail> muss in der Suchergebnisvorlage definiert sein."));
		}
		detailBand = (detail.getBands() == null) ? null : (JRDesignBand) detail.getBands()[0];
		if(detailBand == null) {

			//Change to a different exception message than the one above to distinct
			throw new CommonFatalException(SpringLocaleDelegate.getInstance().getMessage("ReportController.4", "Bereich <Detail> muss in der Suchergebnisvorlage definiert sein."));
		}

		// set the column headers:
		final FontRenderContext fontrenderctx = new FontRenderContext(null, false, true);
		int iLabelWidth = 0;
		int iFieldWidth = 0;
		int iCurrentX = 0;
		
		int fixedFieldWidth = 0;
		
		if(columnScaled) {
			if(orientation == ReportOutputVO.PageOrientation.PORTRAIT) {
				fixedFieldWidth = (DIN_A4_WIDTH - 40 - fields.size() * 10)  / fields.size();
			}
			else { // landscape
				fixedFieldWidth = (DIN_A4_HEIGHT - 40 - fields.size() * 10) / fields.size();
			}
		}

		final double dCharWidth = fontPlain.getStringBounds("M", fontrenderctx).getWidth() + 1;
		final int iIntegerFieldWidth = (int) dCharWidth * MAX_INTEGER_WIDTH;
		final int iDoubleFieldWidth = (int) dCharWidth * MAX_DOUBLE_WIDTH;
		final int iDateFieldWidth = (int) dCharWidth * MAX_DATE_WIDTH;
		final int iBooleanFieldWidth = (int) dCharWidth * MAX_BOOLEAN_WIDTH;
		final int iStringFieldWidth = (int) dCharWidth * MAX_STRING_WIDTH;

		for (ReportFieldDefinition f : fields) {
			final String sLabel = f.getLabel();
			iLabelWidth = (int) fontBold.getStringBounds(sLabel, fontrenderctx).getWidth() + 1;

			HorizontalTextAlignEnum textAlign = HorizontalTextAlignEnum.LEFT;

			final String sClassName = f.getJavaClass().getName();
			if (Date.class.isAssignableFrom(f.getJavaClass())) {
				iFieldWidth = Math.max(iLabelWidth, iDateFieldWidth);
			}
			else if (sClassName.equals("java.lang.String")) {
				final Integer iScale = f.getMaxLength();
				if (iScale != null && iScale > MAX_STRING_WIDTH) {
					iFieldWidth = Math.max(iLabelWidth, iStringFieldWidth);
				}
				else {
					iFieldWidth = iLabelWidth + 10;
				}
			}
			else if (sClassName.equals("java.lang.Boolean")) {
				iFieldWidth = Math.max(iLabelWidth, iBooleanFieldWidth);
			}
			else if (sClassName.equals("java.lang.Integer")) {
				iFieldWidth = Math.max(iLabelWidth, iIntegerFieldWidth);
				textAlign = HorizontalTextAlignEnum.RIGHT;
			}
			else if (sClassName.equals("java.lang.Double")) {
				iFieldWidth = Math.max(iLabelWidth, iDoubleFieldWidth);
				textAlign = HorizontalTextAlignEnum.RIGHT;
			}
			else if (Timestamp.class.isAssignableFrom(f.getJavaClass())) {
				iFieldWidth = Math.max(iLabelWidth, iDateFieldWidth);
			}
			else {
				iFieldWidth = MAX_STRING_WIDTH;
			}
			
			for(ResultColumnVO voColumn : result.getColumns()) {
				String sResultColumnLabel = voColumn.getColumnLabel();
				if(sResultColumnLabel.equals(sLabel)) {
					if(voColumn.getColumnWidth() != 0) {
						iFieldWidth = voColumn.getColumnWidth();
					}
					break;
				}
			}
			
			if(columnScaled) {
				iFieldWidth = fixedFieldWidth;
			}

			final String sFieldName = f.getName();

			final JRDesignField jrdesignfield = new JRDesignField();
			jrdesignfield.setName(sFieldName);
			try {
				if (!sClassName.equals(NuclosImage.class.getName())) {
					jrdesignfield.setValueClass(String.class);
				}
				else {
					jrdesignfield.setValueClass(java.awt.Image.class);
				}
				jrdesign.addField(jrdesignfield);
			}
			catch (JRException ex) {
				throw new CommonFatalException(ex);
			}

			//This Textfield is the Bold Title Field of a Column
			JRDesignStaticText staticField = new JRDesignStaticText();
			staticField.setX(iCurrentX);
			staticField.setWidth(iFieldWidth);
			staticField.setHeight(14);
			staticField.setHorizontalTextAlign(textAlign);
			staticField.setText(sLabel);
			staticField.setStyle(boldStyle);
			columnHeader.addElement(staticField);

			JRDesignElement dataField;
			if (!sClassName.equals(NuclosImage.class.getName())) {
				dataField = new JRDesignTextField();

				((JRDesignTextField) dataField).setHorizontalTextAlign(textAlign);
				((JRDesignTextField) dataField).setVerticalTextAlign(VerticalTextAlignEnum.MIDDLE);
				((JRDesignTextField) dataField).setStyle(regularStyle);
				((JRDesignTextField) dataField).setBlankWhenNull(true);

				final String sFieldValue = "$F{" + sFieldName + "}";

				JRDesignExpression expression = new JRDesignExpression();
				expression.setText(sFieldValue);
				expression.setValueClass(String.class);
				((JRDesignTextField) dataField).setExpression(expression);
				((JRDesignTextField) dataField).setStretchWithOverflow(true);
			}
			else {
				dataField = new JRDesignImage(jrdesign);

				((JRDesignImage) dataField).setHorizontalImageAlign(HorizontalImageAlignEnum.CENTER);
				((JRDesignImage) dataField).setVerticalImageAlign(VerticalImageAlignEnum.MIDDLE);

				final String sFieldValue = "$F{" + sFieldName + "}";

				JRDesignExpression expression = new JRDesignExpression();
				expression.setText(sFieldValue);
				expression.setValueClass(java.awt.Image.class);
				((JRDesignImage) dataField).setExpression(expression);
				((JRDesignImage) dataField).setMode(ModeEnum.TRANSPARENT);
			}

			dataField.setHeight(14);
			dataField.setX(iCurrentX);
			dataField.setWidth(iFieldWidth);

			detailBand.addElement(dataField);

			iCurrentX += iFieldWidth + 10;
		}
		iCurrentX += 50;
		
		final JRDesignBand footer = (JRDesignBand) jrdesign.getPageFooter();
		JRElement el[] = footer.getElements();
		
		if(orientation == PageOrientation.PORTRAIT) {
			int counter = 0;
			for(JRElement element : el) {
				if(counter++ == 0 || SEARCHRESULTELEMENT.equals(element.getKey())) continue;				
				int x = element.getX();
				element.setX(x - DIN_A4_WIDTH + 300);
			}
		}

		if(iCurrentX > DIN_A4_WIDTH && !columnScaled) {
			jrdesign.setOrientation(OrientationEnum.LANDSCAPE);
			jrdesign.setPageWidth(iCurrentX);
			jrdesign.setPageHeight((int) (iCurrentX / 1.41));
		}
	}
}
