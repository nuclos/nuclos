package org.nuclos.server.report.migration;

import org.nuclos.common.report.JRUtils;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.ReportParameterVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.resource.ejb3.ResourceFacadeLocal;
import org.nuclos.server.resource.valueobject.ResourceVO;

public class JRBaseInfo {

	public String reportXML;
	public String reportFileName;
	public String versionString;
	public final boolean isSubreport;

	public JRBaseInfo(DefaultReportOutputVO reportOutputVO, ResourceFacadeLocal resourceFacade) {
		isSubreport = false;
		if (!StringUtils.isNullOrEmpty(reportOutputVO.getSourceFile())) {
			reportXML = new String(reportOutputVO.getSourceFileContent().getData());
			reportFileName = reportOutputVO.getSourceFile();
		} else {
			ResourceVO resource = resourceFacade.getResource(reportOutputVO.getSourceResource());
			if (resource.getFileName().endsWith(".xml") || resource.getFileName().endsWith(".jrxml")) {
				reportXML = new String(resource.getContent());
				reportFileName = resource.getFileName();
			}
		}
		if (hasValidSource()) {
			versionString = getReportVersion();
			if (versionString != null) {
				versionString = versionString.trim();
			}
		}
	}

	public JRBaseInfo(ReportParameterVO reportParameterVO, ResourceFacadeLocal resourceFacade) {
		isSubreport = true;
		if (!StringUtils.isNullOrEmpty(reportParameterVO.getSourcefileName())) {
			reportXML = new String(reportParameterVO.getSourcefileContent().getData());
			reportFileName = reportParameterVO.getSourcefileName();
		} else if (reportParameterVO.getParameterResource() != null) {
			ResourceVO resource = resourceFacade.getResource(reportParameterVO.getParameterResource());
			if (resource.getFileName().endsWith(".xml") || resource.getFileName().endsWith(".jrxml")) {
				reportXML = new String(resource.getContent());
				reportFileName = resource.getFileName();
			}
		}
		if (hasValidSource()) {
			versionString = getReportVersion();
		}
	}

	public boolean hasValidSource() {
		return !StringUtils.isNullOrEmpty(reportXML) && !StringUtils.isNullOrEmpty(reportFileName);
	}

	public String getReportVersion() {
		return JRUtils.getReportVersion(reportXML);
	}

	public boolean containsDeprecatedPdfFonts() {
		return JRUtils.containsDeprecatedPdfFontAttributes(reportXML);
	}

	public boolean isPatchRequired() {
		return JRUtils.isPatchRequired(reportXML);
	}

	public boolean isVersionToRecent() {
		return JRUtils.isVersionTooRecent(reportXML);
	}

}
