package org.nuclos.server.report.migration;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common.report.JRPatchParameters;
import org.nuclos.common.report.JRUtils;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportParameterVO;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbUpdate;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.ejb3.ReportCompiler;

public class JRFilePatcher {

	private static void checkReportFormat(DefaultReportOutputVO output) throws NuclosFatalException {
		if(output.getFormat() != ReportOutputVO.Format.PDF) {
			//skip non-jasper Reports
			throw new NuclosFatalException("");
		}
	}

	public static void migrateJasperReport(SpringDataBaseHelper dataBaseHelper, MasterDataFacadeBean masterDataFacade, DefaultReportOutputVO output, JRPatchParameters patchParameters) throws NuclosReportException {
		checkReportFormat(output);

		final byte[] reportBytes;
		MasterDataVO reportResource = null;
		String sourceFileName;
		if (output.getSourceResource() != null) {
			try {
				reportResource = masterDataFacade.get(E.RESOURCE, output.getSourceResource());
				reportBytes = (byte[]) reportResource.getFieldValue(E.RESOURCE.content);
				sourceFileName = (String) reportResource.getFieldValue(E.RESOURCE.file.getUID());
			} catch (CommonFinderException | CommonPermissionException e) {
				throw new NuclosReportException(e);
			}
		} else {
			reportBytes = output.getSourceFileContent().getData();
			sourceFileName = output.getSourceFile();
		}
		if (reportBytes == null) {
			return;
		}
		final String sReportXML = new String(reportBytes, StandardCharsets.UTF_8);

		Charset charset = IOUtils.guessXmlEncoding(sReportXML);
		if(charset == null) {
			throw new NuclosReportException("No encoding defined in " + sourceFileName + ".");
		}

		if(patchParameters.forceAll || JRUtils.isPatchRequired(new String(reportBytes, charset))) {
			String patchedXML = JRUtils.patchXML(new String(reportBytes, charset), patchParameters);
			ByteArrayCarrier patchedSourceFileContent = new ByteArrayCarrier(patchedXML.getBytes(charset));

			try {
				if (reportResource != null) {
					// save the patched report into the resource it came
					reportResource.setFieldValue(E.RESOURCE.content, patchedXML.getBytes(charset));
					masterDataFacade.modify(reportResource, null);
				}
				updateReportInDatabase(dataBaseHelper, output.getId(), patchedSourceFileContent);
			}
			catch (Exception e) {
				throw new NuclosReportException(e);
			}
		}
	}

	public static void migrateJasperSubreport(SpringDataBaseHelper dataBaseHelper, MasterDataFacadeBean masterDataFacade, ReportParameterVO output, JRPatchParameters patchParameters) throws NuclosReportException {
		final byte[] reportBytes;
		String sourceFileName;
		MasterDataVO reportResource = null;
		if (output.getParameterResource() != null) {
			try {
				reportResource = masterDataFacade.get(E.RESOURCE, output.getParameterResource());
				reportBytes = (byte[]) reportResource.getFieldValue(E.RESOURCE.content);
				sourceFileName = (String) reportResource.getFieldValue(E.RESOURCE.file.getUID());
			} catch (CommonFinderException | CommonPermissionException e) {
				throw new NuclosReportException(e);
			}
		} else {
			sourceFileName = output.getSourcefileName();
			reportBytes = output.getSourcefileContent().getData();
		}
		final String sReportXML = new String(reportBytes, StandardCharsets.UTF_8);

		Charset charset = IOUtils.guessXmlEncoding(sReportXML);
		if(charset == null) {
			throw new NuclosReportException("No encoding defined in " + sourceFileName + ".");
		}

		if(patchParameters.forceAll || JRUtils.isPatchRequired(new String(reportBytes, charset))) {
			String patchedXML = JRUtils.patchXML(new String(reportBytes, charset), patchParameters);

			ByteArrayCarrier patchedSourceFileContent = new ByteArrayCarrier(patchedXML.getBytes(charset));

			try {
				if (reportResource != null) {
					// save the patched report into the resource it came
					reportResource.setFieldValue(E.RESOURCE.content, patchedXML.getBytes(charset));
					masterDataFacade.modify(reportResource, null);
				}
				updateSubreportInDatabase(dataBaseHelper, output.getId(), patchedSourceFileContent);
			}
			catch (Exception e) {
				throw new NuclosReportException(e);
			}
		}
	}

	private static void updateReportInDatabase(SpringDataBaseHelper dataBaseHelper, UID outputId, ByteArrayCarrier patchedSourceFileContent) {
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbMap mapValues = new DbMap();
		mapValues.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		mapValues.put(E.REPORTOUTPUT.sourceFileContent, patchedSourceFileContent);
		mapValues.put(E.REPORTOUTPUT.reportCLS, ReportCompiler.compileReport(patchedSourceFileContent));
		final DbUpdate update = builder.createUpdate(E.REPORTOUTPUT, mapValues);
		update.where(builder.equalValue(update.baseColumn(E.REPORTOUTPUT.getPk()), outputId));
		dataBaseHelper.getDbAccess().executeUpdate(update);
	}

	private static void updateSubreportInDatabase(SpringDataBaseHelper dataBaseHelper, UID outputId, ByteArrayCarrier patchedSourceFileContent) throws NuclosReportException {
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbMap mapValues = new DbMap();
		mapValues.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		mapValues.put(E.SUBREPORT.sourcefileContent, patchedSourceFileContent);
		mapValues.put(E.SUBREPORT.reportCLS, ReportCompiler.compileReport(patchedSourceFileContent));
		final DbUpdate update = builder.createUpdate(E.SUBREPORT, mapValues);
		update.where(builder.equalValue(update.baseColumn(E.SUBREPORT.getPk()), outputId));
		dataBaseHelper.getDbAccess().executeUpdate(update);
	}

}
