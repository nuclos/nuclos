package org.nuclos.server.report.migration;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * This class keeps track of the changes during a jasper migration.
 * It can be used to query statistics.
 */
public class JRMigrationInfo {

	private ArrayList<JRMigrationResult>	migrationResults;

	public JRMigrationInfo() {
		migrationResults = new ArrayList<>();
	}

 	public void AddMigrationResult(JRMigrationResult result) {
		migrationResults.add(result);
	}

	public ArrayList<String> filterForResult(int resultType) {
		Predicate<JRMigrationResult> byResultType = result -> result.getResultType() == resultType;
		return migrationResults.stream().filter(byResultType).map(JRMigrationResult::toString).collect(Collectors.toCollection(ArrayList::new));
	}

}
