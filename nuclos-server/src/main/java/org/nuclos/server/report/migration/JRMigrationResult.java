package org.nuclos.server.report.migration;

import java.util.HashMap;

public class JRMigrationResult {

	public static final int TYPE_SUCCESSFUL = 0;
	public static final int TYPE_ERROR = 1;
	public static final int TYPE_TOO_NEW = 2;
	public static final int TYPE_OUTDATED = 3;
	public static final int TYPE_OUTDATED_WITH_PDF_TAGS = 4;
	public static final int TYPE_MATCHING_ALREADY = 5;

	private static HashMap<Integer, String> TYPE_MESSAGE_TEMPLATE_MAP;

	static {
		TYPE_MESSAGE_TEMPLATE_MAP = new HashMap<>();
		TYPE_MESSAGE_TEMPLATE_MAP.put(TYPE_SUCCESSFUL, "    {t}\t{n}\t{ov}\t\t{p}");
		TYPE_MESSAGE_TEMPLATE_MAP.put(TYPE_ERROR, "    {t}\t{n}\t{v}\t\t{p}\n\t");
		TYPE_MESSAGE_TEMPLATE_MAP.put(TYPE_TOO_NEW, "    {t}\t{n}\t{v}\t\t{p}");
		TYPE_MESSAGE_TEMPLATE_MAP.put(TYPE_OUTDATED, "    {t}\t{n}\t{v}\t\t{p}");
		TYPE_MESSAGE_TEMPLATE_MAP.put(TYPE_OUTDATED_WITH_PDF_TAGS, "    {t}\t{n}\t{v}\t\t{p}");
		TYPE_MESSAGE_TEMPLATE_MAP.put(TYPE_MATCHING_ALREADY, "    {t}\t{n}\t\t{p}");
	}

	private int		resultType;
	private String  consoleMessageTemplate;
	private String  reportName;

	private final String consoleMessage;

	public JRMigrationResult(JRBaseInfo reportInfo, String prefix, int resultType) {
		this(reportInfo, prefix, resultType, "");
	}

	public JRMigrationResult(JRBaseInfo reportInfo, String prefix, int resultType, String templateAddition) {
		this.reportName = "\"" + reportInfo.reportFileName + "\"";
		this.reportName = String.format("%-80s", reportName);
		this.resultType = resultType;
		consoleMessageTemplate = TYPE_MESSAGE_TEMPLATE_MAP.get(resultType);
		this.consoleMessage = String.join(" ", consoleMessageTemplate, templateAddition).replaceAll("\\{n\\}", reportName)
				.replaceAll("\\{v\\}", "(Version: " + reportInfo.versionString + ")")
				.replaceAll("\\{ov\\}", "(Old version: " + reportInfo.versionString + ")")
				.replaceAll("\\{t\\}", (reportInfo.isSubreport) ? "(Subreport)" : "(Report)    ")
				.replaceAll("\\{p\\}", prefix);
	}

	public int getResultType() { return resultType; }

	@Override
	public String toString() { return consoleMessage; }

}
