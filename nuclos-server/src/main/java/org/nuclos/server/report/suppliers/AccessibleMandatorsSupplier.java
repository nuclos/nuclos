package org.nuclos.server.report.suppliers;

import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import org.nuclos.common.UID;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class AccessibleMandatorsSupplier implements Supplier<Set<UID>> {
	private NuclosUserDetailsContextHolder userCtx;
	private final UID mandator;

	public AccessibleMandatorsSupplier(final UID mandator) {
		this.mandator = mandator;
	}

	public Set<UID> get() {
		Set<UID> mandators;
		if (mandator == null) {
			mandators = SecurityCache.getInstance().getAccessibleMandators(userCtx.getMandatorUID());
		} else {
			mandators = MandatorUtils.getAccessibleMandatorsByLogin(mandator, userCtx.getMandatorUID());
		}

		return mandators;
	}

	@Autowired
	private void inject(NuclosUserDetailsContextHolder userCtx) {
		this.userCtx = userCtx;
	}
}
