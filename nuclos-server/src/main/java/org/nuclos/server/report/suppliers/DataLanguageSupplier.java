package org.nuclos.server.report.suppliers;

import java.util.function.Supplier;

import org.nuclos.common.UID;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class DataLanguageSupplier implements Supplier<UID> {

	private LocaleFacadeLocal localeFacade;
	private NuclosUserDetailsContextHolder userCtx;
	private final UID language;

	public DataLanguageSupplier(final UID language) {
		this.language = language;
	}

	@Override
	public UID get() {
		UID language = this.language;
		if (language == null) {
			language = getDataLanguage();
			if (language == null) {
				language = new UID(this.localeFacade.getUserLocale().getLanguage());
			}
		}

		return language;
	}

	private UID getDataLanguage() {
		return this.userCtx.getDataLocal();
	}

	@Autowired
	private void inject(LocaleFacadeLocal localeFacade, NuclosUserDetailsContextHolder userCtx) {
		this.localeFacade = localeFacade;
		this.userCtx = userCtx;
	}
}
