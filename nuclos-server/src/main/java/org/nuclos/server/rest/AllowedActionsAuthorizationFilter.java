package org.nuclos.server.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.ActionsAllowed;
import org.nuclos.server.rest.services.helper.UserAction;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
@Priority(1000)
public class AllowedActionsAuthorizationFilter implements ContainerRequestFilter {
	@Context
	private ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		try {
			final Collection<UserAction> actionsAllowedClass = new ArrayList<>();
			final Collection<UserAction> actionsAllowedMethod = new ArrayList<>();

			ActionsAllowed annotation = resourceInfo.getResourceMethod().getAnnotation(ActionsAllowed.class);
			if (annotation != null) {
				actionsAllowedMethod.addAll(Arrays.asList(annotation.value()));
			}

			annotation = resourceInfo.getResourceClass().getAnnotation(ActionsAllowed.class);
			if (annotation != null) {
				actionsAllowedClass.addAll(Arrays.asList(annotation.value()));
			}

			boolean anyActionsAllowed = (actionsAllowedClass.isEmpty() && actionsAllowedMethod.isEmpty())
					|| (actionsAllowedMethod.isEmpty()
							? actionsAllowedClass.stream().anyMatch(action -> Rest.facade().isActionAllowed(action.getValue()))
							: (actionsAllowedClass.isEmpty()
								? actionsAllowedMethod.stream().anyMatch(action -> Rest.facade().isActionAllowed(action.getValue()))
								: (actionsAllowedClass.stream().anyMatch(action -> Rest.facade().isActionAllowed(action.getValue()))
											&& actionsAllowedMethod.stream().anyMatch(action -> Rest.facade().isActionAllowed(action.getValue())))
					));
			if (!anyActionsAllowed) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}
		} finally {
		}
	}
}
