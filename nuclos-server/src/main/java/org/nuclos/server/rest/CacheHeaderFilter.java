package org.nuclos.server.rest;

import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.CacheControl;

public class CacheHeaderFilter implements ContainerResponseFilter {

	@Override
	public void filter(ContainerRequestContext creq, ContainerResponseContext cresp) {
		CacheControl cc = getOrAddCacheControl(cresp);

		// If no max-age was set, we assume the resource should not be cached at all
		if (cc.getMaxAge() < 0) {
			cc.setMaxAge(0);
			cc.setSMaxAge(0);
			cc.setMustRevalidate(true);
			cc.setNoCache(true);
			cc.setNoStore(true);
			cc.setPrivate(true);
			cc.setProxyRevalidate(true);
		}
	}

	private CacheControl getOrAddCacheControl(
			final ContainerResponseContext cresp
	) {
		CacheControl result = null;

		result = findCacheControl(cresp);

		if (result == null) {
			result = new CacheControl();
			cresp.getHeaders().putSingle("cache-control", result);
		}

		return result;
	}

	private CacheControl findCacheControl(final ContainerResponseContext cresp) {
		List<Object> ccHeaders = cresp.getHeaders().get("cache-control");

		if (ccHeaders != null) {
			for (Object cc : ccHeaders) {
				if (cc instanceof CacheControl) {
					return (CacheControl) cc;
				}
			}
		}

		return null;
	}
}
