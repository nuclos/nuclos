package org.nuclos.server.rest;

import java.io.IOException;
import java.util.Date;

import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

/**
 * Sets an ETag header based on the last-modified date of a resource.
 * Checks for modification on new requests an only builds a new response if necessary.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public abstract class CacheableResponseBuilder {
	private final Request request;
	private final int maxAgeInSeconds;

	public CacheableResponseBuilder(
			final Request request
	) {
		this(request, 60*60*24);
	}

	public CacheableResponseBuilder(
			final Request request,
			final int maxAgeInSeconds
	) {
		this.request = request;
		this.maxAgeInSeconds = maxAgeInSeconds;
	}

	/**
	 * Should return the last-modified Date of the underlying resource.
	 *
	 * @return
	 */
	protected abstract Date getLastModified() throws IOException;

	/**
	 * Overwrite this function to implement custom calculation for EntityTag
	 * @return E-TAG für Response Cache
	 * @throws IOException if getLastModified() returns IOException
	 */
	protected EntityTag getEntityTag() throws IOException {
		Date lastModified = getLastModified();
		if (lastModified == null) {
			lastModified = new Date();
		}

		return new EntityTag("" + lastModified.getTime());
	}

	/**
	 * Should do all the expensive work (load the actual data etc.) to build a new reponse.
	 *
	 * @return
	 */
	protected abstract Response.ResponseBuilder newResponseBuilder() throws IOException;

	public Response build() throws IOException {
		Date lastModified = getLastModified();
		if (lastModified == null) {
			lastModified = new Date();
		}

		final EntityTag eTag = getEntityTag();

		// Check for modifications
		Response.ResponseBuilder builder = request.evaluatePreconditions(
				lastModified,
				eTag
		);

		// If builder is not null here, the resource was not modified and we don't need to build a new response.
		if (builder == null) {
			builder = newResponseBuilder();
		}

		final CacheControl cc = new CacheControl();
		cc.setMaxAge(maxAgeInSeconds);
		cc.setNoCache(true);
		cc.setMustRevalidate(true);
		cc.setPrivate(true);

		return builder.cacheControl(cc).tag(eTag).build();
	}
}
