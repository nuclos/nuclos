package org.nuclos.server.rest;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.ParameterProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.owasp.encoder.Encode;

public class CrossOriginResourceSharingFilter implements ContainerResponseFilter {

	/**
	 * Sets CORS response headers based on the referer of the request, if any.
	 * Else all origins are allowed.
	 *
	 * @param request
	 * @param response
	 */
	public static void setCORSHeadersForResponse(final HttpServletRequest request, final HttpServletResponse response) throws MalformedURLException {
		if (StringUtils.isNotBlank(request.getHeader("referer"))) {
			final URL refererURL = new URL(request.getHeader("referer"));
			final String referer = refererURL.getProtocol() + "://" + refererURL.getHost() + ":" + refererURL.getPort();
			response.setHeader("Access-Control-Allow-Origin", Encode.forJava(referer));
		} else {
			response.setHeader("Access-Control-Allow-Origin", "*");
		}

		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		response.setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept, X-Requested-With, SessionId, RESTCallLogging");
	}

	@Override
	public void filter(ContainerRequestContext creq, ContainerResponseContext cresp) {

		// allow CORS for development mode or if explicitly activated via system parameter
		if (NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)
				|| ServerParameterProvider.getInstance().isEnabled(ParameterProvider.KEY_REST_ACTIVATE_CORS)) {
			String origin = creq.getHeaderString("origin");
			cresp.getHeaders().putSingle("Access-Control-Allow-Origin", origin != null ? origin : "*");
		}

		cresp.getHeaders().putSingle("Access-Control-Allow-Credentials", "true");
		cresp.getHeaders().putSingle("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		cresp.getHeaders().putSingle("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept, X-Requested-With, SessionId, RESTCallLogging");
	}
}
