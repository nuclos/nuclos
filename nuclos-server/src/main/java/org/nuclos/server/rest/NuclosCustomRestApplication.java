//Copyright (C) 2019  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest;

import java.lang.reflect.Method;
import java.security.Principal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.LocaleUtils;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.spi.Container;
import org.glassfish.jersey.server.spi.ContainerLifecycleListener;
import org.nuclos.api.annotation.AnonymousAuthentication;
import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.rule.CustomRestRule;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.customcode.ejb3.CodeFacadeBean;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.CustomRestContextFactory;
import org.nuclos.server.rest.services.helper.CustomRestContextProducer;
import org.nuclos.server.security.SessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

// The path annotation here starts a second container (see NUCLOS-8175)
//@ApplicationPath("/rest/custom/")
public class NuclosCustomRestApplication extends NuclosRestApplication {

	public static final String APPLICATION_PATH = "/rest/custom/";

	private final static Logger LOG = LoggerFactory.getLogger(NuclosCustomRestApplication.class);
	private static List<Container> containers = new CopyOnWriteArrayList<>();

	static {
		// reload container configuration after CustomRestRule code has changed
		CodeFacadeBean.addAdListener(
				codeVO -> {
					if (codeVO != null) {
						LOG.debug("Rule compiled: {}", codeVO.getName());
					} else {
						LOG.debug("All rules compiled");
					}
					reload();
				}
		);

		SpringApplicationContextHolder.addSpringReadyListener(new SpringApplicationContextHolder.SpringReadyListener() {
			@Override
			public void springIsReady() {
				reload();
			}
		});

		// Hide Warnings like this org.glassfish.jersey.internal.inject.Providers.checkProviderRuntime:
		// A provider io.swagger.v3.jaxrs2.integration.resources.AcceptHeaderOpenApiResource registered in SERVER
		// runtime does not implement any provider interfaces applicable in the SERVER runtime.
		// Due to constraint configuration problems the provider
		// io.swagger.v3.jaxrs2.integration.resources.AcceptHeaderOpenApiResource will be ignored.
		java.util.logging.Logger.getLogger("org.glassfish.jersey.internal.inject.Providers")
				                 .setLevel(java.util.logging.Level.SEVERE);
	}

	public static void reload() {
		LOG.trace("Reloading Containers {}...", containers);
		NuclosCustomRestApplication resourceConfig = new NuclosCustomRestApplication(SpringApplicationContextHolder.isSpringReady());
		containers.forEach(it -> it.reload(resourceConfig));
		LOG.info("Reload done for Containers {}", containers);
	}


	private void registerCustomRestRules() {
		try {
			Set<String> paths = new HashSet<>();

			// TODO also check system REST paths (like '/rest/version')
			final List<Class> customRules = getCustomRestRulesForRegistration();
			for (Class customRestRule : customRules) {
				final List<String> currentClassPathConfigs = restPaths(customRestRule);
				for (String path : currentClassPathConfigs) {
					if (paths.contains(path)) {
						throw new RuntimeException("The path configuration '" + path
								+ "' in '" + customRestRule + "' conflicts with another REST path.");
					} else {
						LOG.debug("Adding path: {}", path);
						paths.add(path);
					}
					LOG.debug("Registering custom rest rule: {}", customRestRule.getName());
					// just a first try for glassfish.....
//					try {
//						// try CDI here (for glassfish)
//						BeanManager beanManager = CDI.current().getBeanManager();
//						AnnotatedType type = beanManager.createAnnotatedType(customRestRule);
//						InjectionTarget it = beanManager.createInjectionTarget(type);
//						CreationalContext<Object> ctx = beanManager.createCreationalContext(null);
//						Object instance = it.produce(ctx);
//						it.inject(instance, ctx);
//						it.postConstruct(instance);
//						register(instance);
//					} catch (Error | Exception err) {
						// must be tomcat, register class only
						register(customRestRule);
//					}
				}
			}

		} catch (Exception e) {
			LOG.warn("Unable to register CustomRestRules.", e);
		}
	}


	private List<String> restPaths(Class cl) {
		String pathForClass = cl.getAnnotation(Path.class) != null ? (((Path) cl.getAnnotation(Path.class)).value() + "/") : "";
		final Stream<List<String>> listStream = Stream.of(GET.class, POST.class, PUT.class, PATCH.class, DELETE.class)
				.map(httpMethod ->
						Arrays.stream(
								cl.getMethods()
						).filter(
								m -> m.getAnnotation(httpMethod) != null
						)
								.filter(m -> m.getAnnotation(Path.class) != null)
								.map(m -> (
										m.getAnnotation(Path.class) != null
												? (pathForClass + m.getAnnotation(Path.class).value() + " [" + httpMethod.getSimpleName() + "]")
												: (pathForClass.isEmpty() ? null : (pathForClass + " [" + httpMethod.getSimpleName() + "]"))))
								.filter(Objects::nonNull)
								.collect(Collectors.toList())
				);
		return listStream.flatMap(List::stream).collect(Collectors.toList());
	}

	@Transactional(propagation= Propagation.REQUIRED, noRollbackFor= {Exception.class})
	private List<Class> getCustomRestRulesForRegistration() throws Exception {

		EventSupportCache esCache = SpringApplicationContextHolder.getBean(EventSupportCache.class);
		CustomCodeManager customCodeManager = SpringApplicationContextHolder.getBean(CustomCodeManager.class);

		final ClassLoader classLoader = customCodeManager.getClassLoaderAndCompileIfNeeded();

		return esCache.getExecutableEventSupportFiles().stream()
				.filter(eventSupportSourceVO -> eventSupportSourceVO.isActive() &&
												eventSupportSourceVO.getInterface()
									.contains(CustomRestRule.class.getCanonicalName()))
				.map(eventSupportSourceVO -> {
					String className = eventSupportSourceVO.getClassname();
					try {
						// prioritize rules from an extension
						return Thread.currentThread().getContextClassLoader().loadClass(className);
					} catch (ClassNotFoundException | NoClassDefFoundError e) {
						// only a test here
					}
					try {
						return classLoader.loadClass(className);
					} catch (ClassNotFoundException | NoClassDefFoundError e) {
						LOG.error("Unable to load CustomRestRule '{}'.", e, className);
					}
					return null;
				})
				.filter(ruleClass -> ruleClass != null)
				.collect(Collectors.toList());
	}

	/**
	 * main init call from org.nuclos.server.spring.NuclosWebApplicationInitializer.addServletCustomRest
	 */
	public NuclosCustomRestApplication() {
		// Disable WADL generation for the REST service
		property(ServerProperties.WADL_FEATURE_DISABLE, "true");

		// container reference is needed for reloading ServletContext after CustomRestRules are changed
		register(new ContainerLifecycleListener() {

			@Override
			public void onStartup(final Container container) {
				LOG.debug("Container startup: {}", container);

				// Might be called for multiple containers, at least on Glassfish.
				NuclosCustomRestApplication.containers.add(container);
			}

			@Override
			public void onReload(final Container container) {
				LOG.debug("Container reload: {}", container);
			}

			@Override
			public void onShutdown(final Container container) {
				LOG.debug("Container shutdown: {}", container);

				NuclosCustomRestApplication.containers.remove(container);
			}
		});
	}

	/**
	 * Internal reload call (for registrations)
	 * @param springIsReady
	 */
	private NuclosCustomRestApplication(final boolean springIsReady) {
		this();

		// inject CustomRestContext into CustomRestRules
		register(new AbstractBinder() {
			@Override
			protected void configure() {
				bindFactory(CustomRestContextFactoryHk2.class)
						.to(CustomRestContext.class)
						.in(RequestScoped.class);
			}
		});
		register(new CustomRestContextProducer());

		// Registering JacksonFeature is necessary for Glassfish deployment
		register(JacksonFeature.class);

		final String sAdditionalServicesClassName = LangUtils.defaultIfNull(
				ApplicationProperties.getInstance().getAdditionalRestServices(), AdditionalRestServices.class.getName()
		);

		if (springIsReady) {
			registerCustomRestRules();
		}

		//register(CustomRestServiceAuthenticationFilter.class);
		register(CustomRestServiceAuthenticationDynamicFeature.class);

		// REGISTER SINGLETON
		registerInstances(
				new RestTransactionListener(),
				new CacheHeaderFilter(),
				new CrossOriginResourceSharingFilter(),
				new SessionValidationRequestFilter(),
				new MaintenanceModeRequestFilter(),
				new ProfilingFilter(),
				new SwaggerRequestFilter()
		);
	}

	/**
	 * The only way to determine the ResourceInfo with Payara 5.2021.7.
	 * '@Context does not work any more!
	 */
	@Provider
	public static class CustomRestServiceAuthenticationDynamicFeature implements DynamicFeature {
		@Override
		public void configure(final ResourceInfo resourceInfo, final FeatureContext featureContext) {
			featureContext.register(new CustomRestServiceAuthenticationFilter(resourceInfo), 10);
		}
	}

	/**
	 * verify access permissions of CustomRestServices
	 */
	//@Priority(10)
	public static class CustomRestServiceAuthenticationFilter
			//extends AbstractSessionIdLocator
			implements ContainerRequestFilter {

		private final static Logger log = LoggerFactory.getLogger(CustomRestServiceAuthenticationFilter.class);

		// Throws IllegalStateException: Not inside a request scope.
		// (Payara 5.2021.7)
		//@Context
		//private ResourceInfo resourceInfo;

		private final ResourceInfo resourceInfo;

		public CustomRestServiceAuthenticationFilter(final ResourceInfo resourceInfo) {
			super();
			this.resourceInfo = resourceInfo;
		}

		@Override
		public void filter(final ContainerRequestContext requestContext) {
			final Method resourceMethod = resourceInfo.getResourceMethod();
			if (resourceMethod == null) {
				return;
			}
			AnonymousAuthentication anonymousAuthentication = resourceMethod.getAnnotation(AnonymousAuthentication.class);
			if (anonymousAuthentication != null) {
				initNewAnonymousNuclosSessionIfNecessary(resourceMethod, anonymousAuthentication);
			} else {
				final Principal userPrincipal = requestContext.getSecurityContext().getUserPrincipal();
				if (userPrincipal != null && !SecurityCache.getInstance().isSuperUser(userPrincipal.getName())) {
					final Map<UID, String> allowedCustomRestRules = SecurityCache.getInstance().getAllowedCustomRestRules(userPrincipal.getName(), null);
					if (CustomRestRule.class.isAssignableFrom(resourceInfo.getResourceClass())) {
						if (!allowedCustomRestRules.containsValue(resourceInfo.getResourceClass().getName())) {
							throw new NuclosWebException(Response.Status.UNAUTHORIZED);
						}
					}
				}
			}
		}

		private void initNewAnonymousNuclosSessionIfNecessary(final Method resourceMethod, final AnonymousAuthentication anonymousAuthentication) {
			// AbstractSessionIdLocator no longer works here... javax.servlet.ServletException: java.lang.NullPointerException:
			//HttpSession httpSession = httpRequest.getSession();
			HttpSession httpSession = SecurityCache.getInstance().getSession().getHttpSession();
			String sessionId = httpSession.getId();
			Locale locale = LocaleUtils.toLocale(anonymousAuthentication.locale());
			SessionContext session = Rest.facade().initNewAnonymousNuclosSessionIfNecessary(
					sessionId, "Method["+resourceMethod.toString()+"]",
					locale, anonymousAuthentication.dataLanguage());
			if (session == null) {
				log.debug("Anonymous authentication requested, but login was not successful -> UNAUTHORIZED");
				throw new NuclosWebException(Response.Status.UNAUTHORIZED);
			}
		}
	}

	public static class CustomRestContextFactoryHk2 implements org.glassfish.hk2.api.Factory<CustomRestContext> {

		private final HttpServletRequest request;

		@Inject
		public CustomRestContextFactoryHk2(HttpServletRequest request) {
			this.request = request;
		}

		@Override
		public CustomRestContext provide() {
			final String userName = request.getUserPrincipal() != null ? request.getUserPrincipal().getName() : null;
			return CustomRestContextFactory.createCustomRestContext(userName);
		}

		@Override
		public void dispose(final CustomRestContext instance) {
		}
	}

}
