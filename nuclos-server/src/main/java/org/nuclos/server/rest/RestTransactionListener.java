package org.nuclos.server.rest;

import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ContainerResponse;
import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronizationManager;

@Configurable
public class RestTransactionListener implements ApplicationEventListener, RequestEventListener {

	private final static Logger LOG = LoggerFactory.getLogger(RestTransactionListener.class);

	private PlatformTransactionManager txManager;

	private static final ThreadLocal<TransactionStatus> txLocal = new ThreadLocal<>();

	@Override
	public void onEvent(final RequestEvent event) {
		if (event.getType() == RequestEvent.Type.REQUEST_FILTERED) {
			startTransaction(event);
		} else
		if (event.getType() == RequestEvent.Type.FINISHED) {
			completeTransaction(event);
		}
	}

	@Override
	public void onEvent(final ApplicationEvent event) {}

	@Override
	public RequestEventListener onRequest(final RequestEvent requestEvent) {
		return this;
	}

	private void startTransaction(final RequestEvent event) {
		if (SessionValidationRequestFilter.isSessionValidated() || SessionValidationRequestFilter.isAnonymousAllowed()) {
			PlatformTransactionManager txManager = getPlatformTransactionManager();
			if (txManager == null) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}
			final DefaultTransactionDefinition def = new DefaultTransactionDefinition();
			def.setName("restTxDef");
			if (TransactionSynchronizationManager.isSynchronizationActive()) {
				def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
			} else {
				def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			}
			txLocal.set(txManager.getTransaction(def));
		}
	}

	private void completeTransaction(final RequestEvent event) {
		try {
			final PlatformTransactionManager txManager = getPlatformTransactionManager();
			final TransactionStatus tx = txLocal.get();
			if (tx != null && !tx.isCompleted()) {
				final String httpMethod = event.getContainerRequest().getMethod();
				final String resourceMethod = event.getContainerRequest().getPath(true);
				final String service = SessionValidationRequestFilter.getRequestedService(event.getContainerRequest().getBaseUri());
				if (tx.isRollbackOnly()) {
					LOG.warn("Transaction is marked for rollback-only, rolling back now: {} {} ({})",
							httpMethod, resourceMethod, service);
					txManager.rollback(tx);
				} else if (!event.isSuccess()) {
					Throwable exception = event.getException();
					ContainerResponse response = event.getContainerResponse();
					Response.StatusType status = response != null ? response.getStatusInfo() : null;
					Integer iStatus = status != null ? status.getStatusCode() : null;
					String reasonPhrase = status != null ? status.getReasonPhrase() : null;
					String sException = exception != null ? exception.toString() : null;
					if (exception != null) {
						LOG.debug("", exception);
					}
					LOG.warn("Transaction has to be rolled back due to an response error or exception: {} {} ({}): {} {} / {}",
							httpMethod, resourceMethod, service, iStatus, reasonPhrase, sException);
					txManager.rollback(tx);
				} else {
					txManager.commit(tx);
				}
			}

		} finally {
			txLocal.remove();
		}
	}

	private PlatformTransactionManager getPlatformTransactionManager() {
		if (txManager == null) {
			try {
				txManager = SpringApplicationContextHolder.getBean(PlatformTransactionManager.class);
			} catch (Exception ex) {
				// too early
			}
		}
		return txManager;
	}

}
