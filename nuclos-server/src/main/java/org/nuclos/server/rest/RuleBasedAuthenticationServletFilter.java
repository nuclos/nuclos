package org.nuclos.server.rest;

import static org.nuclos.server.rest.services.helper.AbstractSessionIdLocator.cacheNewRuleBasedAuthentication;
import static org.nuclos.server.rest.services.helper.AbstractSessionIdLocator.clearRuleBasedAuthenticationCache;

import java.io.IOException;
import java.net.URI;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.startup.NuclosEnviromentConstants;
import org.nuclos.server.common.NuclosHttpSessions;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosHttpHeaders;
import org.nuclos.server.rest.misc.NuclosUriInfo;
import org.nuclos.server.security.NuclosRuleBasedAuthenticationToken;

public class RuleBasedAuthenticationServletFilter implements Filter {

	private Configuration configuration;

	public RuleBasedAuthenticationServletFilter() {
	}

	private Configuration getConfiguration() {
		synchronized (this) {
			if (configuration == null) {
				configuration = new NuclosRestApplication();
			}
		}
		return configuration;
	}

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
		if (request instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			if ("OPTIONS".equals(httpRequest.getMethod())) {
				chain.doFilter(request, response);
			} else {
				doHttpFilter(httpRequest, (HttpServletResponse) response, chain);
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	private void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
		clearRuleBasedAuthenticationCache();
		boolean isThrowAwaySession = false;
		final HttpSession httpSession = request.getSession();
		try {
			if (NuclosHttpSessions.setCurrentSessionId(httpSession.getId())
				&& !httpSession.getId().equals(request.getRequestedSessionId())
				&& SpringApplicationContextHolder.isNuclosReady()) {

				final HttpHeaders headers = new NuclosHttpHeaders(getConfiguration(), request);
				final URI restBaseUri = URI.create(String.format("%s://%s:%d%s/%s",
						request.getScheme(),
						request.getServerName(),
						request.getServerPort(),
						request.getContextPath(),
						NuclosEnviromentConstants.REST_VARIABLE));
				final URI requestUri = URI.create(request.getQueryString() != null ?
						String.format("%s?%s", request.getRequestURI(), request.getQueryString()):
						request.getRequestURI());
				final UriInfo uriInfo = new NuclosUriInfo(restBaseUri, requestUri);

				try {
					NuclosRuleBasedAuthenticationToken ruleAuth = Rest.facade().tryRuleBasedAuthentication(
							headers, uriInfo,
							httpSession.getId(),
							request.getRemoteAddr(), request.getRemoteHost(), request.getRemoteUser());

					if (ruleAuth != null) {
						isThrowAwaySession = !ruleAuth.getAuthResult().isLoginRequired(); // No login means a throw away session (an isolated request).
						// Write session id to AbstractSessionIdLocator! So it can be accessed from every rest service, even if not transmitted by the client.
						cacheNewRuleBasedAuthentication(httpSession.getId());
					}
				} catch (WebApplicationException webEx) {
					preventSessionCookie(response);
					Rest.facade().invalidateSession(httpSession.getId());
					Response errorResponse = webEx.getResponse();
					String errorMessage = errorResponse.hasEntity() ? errorResponse.getEntity().toString() : webEx.getMessage();
					response.sendError(errorResponse.getStatus(), errorMessage);
					return;
				}
			}

			if (isThrowAwaySession) {
				preventSessionCookie(response);
			}
			// process the request
			chain.doFilter(request, response);
		} finally {
			if (isThrowAwaySession) {
				Rest.facade().invalidateSession(httpSession.getId());
			}
			clearRuleBasedAuthenticationCache();
		}
	}

	/**
	 * prevents the JSESSIONID cookie in clients...
	 * @param response
	 */
	private void preventSessionCookie(HttpServletResponse response) {
		response.setHeader("Set-Cookie", "");
	}

	@Override
	public void destroy() {
	}

}
