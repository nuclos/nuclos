package org.nuclos.server.rest;

import java.lang.reflect.Method;
import java.net.URI;

import javax.annotation.Nullable;
import javax.annotation.Priority;
import javax.servlet.http.HttpSession;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.nuclos.common.Mutable;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.common.ejb3.SecurityFacadeLocal;
import org.nuclos.server.maintenance.MaintenanceFacadeLocal;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.AbstractSessionIdLocator;
import org.nuclos.server.security.NuclosRuleBasedAuthenticationToken;
import org.nuclos.server.security.NuclosSsoAuthenticationToken;
import org.nuclos.server.security.SessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.core.Authentication;

@Priority(100)
@Configurable
public class SessionValidationRequestFilter extends AbstractSessionIdLocator implements ContainerRequestFilter {

	private final static Logger log = LoggerFactory.getLogger(SessionValidationRequestFilter.class);

	private MaintenanceFacadeLocal maintenanceFacade;

	private SecurityFacadeLocal securityFacade;

	@Context
	private ResourceInfo resourceInfo;

	private static final ThreadLocal<Boolean> sessionValidated = new ThreadLocal<>();

	private static final ThreadLocal<Boolean> anonymousAllowed = new ThreadLocal<>();

	@Override
	public void filter(ContainerRequestContext creq) {
		validateSessionIfNecessary();
	}

	private void validateSessionIfNecessary() {
		sessionValidated.set(false);
		anonymousAllowed.set(false);
		final Mutable<Boolean> mutableAnonymous = new Mutable<>(false);
		if (isSessionValidationEnabledForCurrentRequest(mutableAnonymous)) {
			validateSession();
			sessionValidated.set(true);
		}
		anonymousAllowed.set(mutableAnonymous.getValue());
	}

	private boolean isSessionValidationEnabledForCurrentRequest(Mutable<Boolean> mutableAnonymous) {
		@Nullable final Method resourceMethod = resourceInfo.getResourceMethod();

		if (resourceMethod == null) {
			log.warn("Method is null - enabling session validation for resource: {}", resourceInfo);
			return true;
		}

		if (NuclosRestApplication.isSessionValidationEnabled(
				httpRequest.getMethod(),
				resourceMethod,
				getRequestedService(uriInfo.getBaseUri()),
				mutableAnonymous
		)) {
			return true;
		}
		return false;
	}

	public static String getRequestedService(URI baseUri) {
		if (baseUri != null) {
			String requestedService = baseUri.getPath();
			// remove application/root context like 'nuclos-war'
			int serviceStartIndex = requestedService.indexOf("/", 1);
			if (serviceStartIndex > 0) {
				requestedService = requestedService.substring(serviceStartIndex);
			}
			return requestedService;
		}
		return "";
	}

	private void validateSession() {
		HttpSession httpSession = httpRequest.getSession();

		log.debug("Validating session " + httpSession.getId() + " on Thread " + currentThread());

		if (!httpSession.getId().equals(httpRequest.getRequestedSessionId()) &&
			!super.isNewRuleBasedAuthentication()) {
			// tomcat session has been invalidated - remove session from SecurityCache
			Rest.facade().webLogout(httpRequest.getSession().getId());
			log.debug("Session was invalidated -> UNAUTHORIZED");
			throw new NuclosWebException(Response.Status.UNAUTHORIZED);
		} else {
			String sessionId = httpSession.getId();
			SessionContext session = Rest.facade().validateSessionAuthenticationAndInitUserContext(sessionId);
			if (session == null) {
				log.debug("Session validation enabled, but no authentication -> UNAUTHORIZED");
				throw new NuclosWebException(Response.Status.UNAUTHORIZED);
			} else if (getMaintenanceFacade() != null && maintenanceFacade.blockUserLogin(session.getUsername())) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}
			if (getSecurityFacade() == null) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}
			Authentication auth = session.getAuthentication();
			if (auth instanceof NuclosSsoAuthenticationToken) {
				NuclosSsoAuthenticationToken authToken = (NuclosSsoAuthenticationToken) auth;
				if (!authToken.isAccessTokenValid()) {
					try {
						if (authToken.getRefreshToken() == null) {
							throw new NuclosWebException(Response.Status.UNAUTHORIZED, "Access token expired");
						} else {
							securityFacade.refreshSsoAuthentication(authToken);
						}
					} catch (NuclosWebException webEx) {
						// all exceptions from refreshSession should be a NuclosWebException
						// remove session from SecurityCache
						log.debug("Session token expired or revoked -> UNAUTHORIZED");
						Rest.facade().webLogout(httpRequest.getSession().getId());
						throw webEx;
					}
				}
			} else if (auth instanceof NuclosRuleBasedAuthenticationToken
						&& !super.isNewRuleBasedAuthentication() // no need to validate a fresh session
			) {
				NuclosRuleBasedAuthenticationToken authToken = (NuclosRuleBasedAuthenticationToken) auth;
				if (!authToken.isValid()) {
					try {
						Rest.facade().refreshRuleBasedAuthentication(authToken, headers, uriInfo, httpSession.getId(),
								httpRequest.getRemoteAddr(), httpRequest.getRemoteHost(), httpRequest.getRemoteUser());
					} catch (WebApplicationException webEx) {
						log.debug("Session authorization not refreshed successfully -> UNAUTHORIZED");
						Rest.facade().webLogout(httpRequest.getSession().getId());
						throw webEx;
					}
				}
			}
		}
	}

	static boolean isAnonymousAllowed() {
		return Boolean.TRUE.equals(anonymousAllowed.get());
	}

	static boolean isSessionValidated() {
		return Boolean.TRUE.equals(sessionValidated.get());
	}

	private String currentThread() {
		return Thread.currentThread().getId() + " (" + Thread.currentThread().getName() + ")";
	}

	private MaintenanceFacadeLocal getMaintenanceFacade() {
		if (maintenanceFacade == null) {
			try {
				maintenanceFacade = SpringApplicationContextHolder.getBean(MaintenanceFacadeLocal.class);
			} catch (Exception ex) {
				// too early
			}
		}
		return maintenanceFacade;
	}

	private SecurityFacadeLocal getSecurityFacade() {
		if (securityFacade == null) {
			try {
				securityFacade = SpringApplicationContextHolder.getBean(SecurityFacadeLocal.class);
			} catch (Exception ex) {
				// too early
			}
		}
		return securityFacade;
	}

}
