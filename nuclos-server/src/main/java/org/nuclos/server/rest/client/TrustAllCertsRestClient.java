//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.client;


import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.nuclos.common.RigidUtils;

/**
 * Special thanks to this wiki article
 * https://wiki.nuclos.de/pages/viewpage.action?pageId=15990957
 */
public class TrustAllCertsRestClient {

	/**
	 * Trust manager that does not perform any checks.
	 */
	private static TrustManager[] trustAllCerts = new TrustManager[] {
			new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {return null;}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {}
		}
	};

	/**
	 * Verifier that does not perform any checks.
	 */
	private static HostnameVerifier hostnameVerifier = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

	private TrustAllCertsRestClient() {
	}

	public static Client buildClient(int timeoutInMillis) {
		ClientConfig config = new ClientConfig();
		config.property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true);
		config.property(ClientProperties.CONNECT_TIMEOUT, timeoutInMillis);
		config.property(ClientProperties.READ_TIMEOUT, timeoutInMillis);

		Client result;
		try {
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, trustAllCerts, RigidUtils.getSecureRandom());

			result = ClientBuilder.newBuilder()
				.withConfig(config)
				.hostnameVerifier(hostnameVerifier)
				.sslContext(sslContext)
				.build();
		} catch (Exception ex) {
			result = ClientBuilder.newClient(config);
		}
		result.register(JacksonFeature.class);

		return result;
	}

}
