//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.ejb3;


import static org.nuclos.common2.StringUtils.defaultIfNull;
import static org.nuclos.common2.StringUtils.looksEmpty;
import static org.nuclos.server.rest.services.helper.WebContext.DEFAULT_RESULT_LIMIT;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.Collator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.validation.constraints.NotNull;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.LocaleUtils;
import org.nuclos.api.authentication.AuthenticationResult;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.context.SpringInputContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.AuthenticationRule;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.businessentity.SsoAuth;
import org.nuclos.businessentity.SsoUserIdentifier;
import org.nuclos.common.Actions;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.CommonConsole;
import org.nuclos.common.DependentSelection;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.LoginResult;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.Mutable;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UnlockMode;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.EnumCollectableFieldsProvider;
import org.nuclos.common.collect.collectable.ValueListProviderType;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.ToHumanReadablePresentationVisitor;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.livesearch.ejb3.LiveSearchFacadeRemote;
import org.nuclos.common.lucene.LuceneSearchResult;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.ejb3.ReportFacadeRemote;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceQueryExecutionParameters;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.security.RemoteAuthenticationManager;
import org.nuclos.common.security.SsoAuthStatus;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common.textmodule.TextModule;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.common.valuelistprovider.SsoAuthOidcIdTokenJwsAlgorithmCollectableFieldsProvider;
import org.nuclos.common.valuelistprovider.UserLoginRestriction;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common.valuelistprovider.VLPUtils;
import org.nuclos.common2.BoDataSet;
import org.nuclos.common2.CollatorProvider;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosExceptions;
import org.nuclos.common2.exception.NuclosRuleCompileException;
import org.nuclos.common2.interval.Interval;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.common2.resource.ResourceResolver;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.remoting.TypePreservingObjectWrapper;
import org.nuclos.schema.rest.PlanningTable;
import org.nuclos.server.api.eventsupport.AuthenticationContextImpl;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.autosync.AutoDbSetup;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.ExtendedDatasourceServerUtils;
import org.nuclos.server.common.GenerationCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ModulePermission;
import org.nuclos.server.common.ModulePermissions;
import org.nuclos.server.common.NuclosRemoteContextHolder;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.RecordGrantRight;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.EntityObjectFacadeRemote;
import org.nuclos.server.common.ejb3.SecurityFacadeLocal;
import org.nuclos.server.customcomp.ejb3.CustomComponentFacadeBean;
import org.nuclos.server.customcomp.resplan.PlanningTableFacadeBean;
import org.nuclos.server.customcomp.valueobject.CustomComponentVO;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dbtransfer.TransferFacadeLocal;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.context.GenerationContext;
import org.nuclos.server.genericobject.context.GenerationContextBuilder;
import org.nuclos.server.genericobject.ejb3.GenerationResult;
import org.nuclos.server.genericobject.ejb3.GeneratorFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.i18n.DataLanguageCache;
import org.nuclos.server.i18n.DataLanguageVO;
import org.nuclos.server.maintenance.MaintenanceConstants;
import org.nuclos.server.maintenance.MaintenanceFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.ejb3.TreeNodeFacadeRemote;
import org.nuclos.server.navigation.treenode.AbstractStaticTreeNode;
import org.nuclos.server.navigation.treenode.AbstractTreeNode;
import org.nuclos.server.navigation.treenode.AbstractTreeNodeConfigured;
import org.nuclos.server.navigation.treenode.DefaultMasterDataTreeNode;
import org.nuclos.server.navigation.treenode.DefaultMasterDataTreeNodeParameters;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.navigation.treenode.RelationTreeNode;
import org.nuclos.server.navigation.treenode.SimpleTreeNode;
import org.nuclos.server.parameter.NuclosParameterProvider;
import org.nuclos.server.report.ejb3.ExtendedDatasourceFacadeBean;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.rvo.FilterJ;
import org.nuclos.server.rest.services.rvo.LayoutInfo;
import org.nuclos.server.rest.services.rvo.NucletInfo;
import org.nuclos.server.rest.services.rvo.RValueObject;
import org.nuclos.server.rest.services.rvo.ResultListExportRVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.searchfilter.ejb3.SearchFilterFacadeLocal;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.nuclos.server.security.NuclosAuthenticationProvider;
import org.nuclos.server.security.NuclosRemoteExceptionLogger;
import org.nuclos.server.security.NuclosRuleBasedAuthenticationToken;
import org.nuclos.server.security.NuclosSsoAuthenticationToken;
import org.nuclos.server.security.NuclosSsoUtils;
import org.nuclos.server.security.NuclosUserDetailsService;
import org.nuclos.server.security.NuclosUserDetailsWithoutCredentials;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.security.WebSessionContext;
import org.nuclos.server.statemodel.ejb3.StateFacadeLocal;
import org.nuclos.server.statemodel.ejb3.StateFacadeRemote;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.nuclos.server.tasklist.TasklistFacadeLocal;
import org.nuclos.server.textmodule.TextModuleFacadeBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.Resource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.oauth2.sdk.AccessTokenResponse;
import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.oauth2.sdk.AuthorizationCodeGrant;
import com.nimbusds.oauth2.sdk.AuthorizationGrant;
import com.nimbusds.oauth2.sdk.AuthorizationResponse;
import com.nimbusds.oauth2.sdk.AuthorizationSuccessResponse;
import com.nimbusds.oauth2.sdk.TokenRequest;
import com.nimbusds.oauth2.sdk.TokenResponse;
import com.nimbusds.oauth2.sdk.auth.ClientAuthentication;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;
import com.nimbusds.openid.connect.sdk.Nonce;
import com.nimbusds.openid.connect.sdk.UserInfoRequest;
import com.nimbusds.openid.connect.sdk.UserInfoResponse;
import com.nimbusds.openid.connect.sdk.claims.ClaimsSet;
import com.nimbusds.openid.connect.sdk.validators.IDTokenValidator;

/**
 * Server implementation of the RestFacadeLocale interface.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 *
 * @author Oliver Brausch
 * @since Nuclos 4.0
 */
@Transactional(propagation=Propagation.SUPPORTS)
@Component
@RolesAllowed("Login")
@Lazy
public class RestFacadeBean implements RestFacadeLocal {

	private static final Logger LOG = LoggerFactory.getLogger(RestFacadeBean.class);

	private static final String TEMP = "temp_";

	@Autowired
	@Qualifier("masterDataService")
	private MasterDataFacadeRemote masterDataFacade;

	@Autowired
	private SecurityCache securityCache;

	@Autowired
	private GeneratorFacadeRemote generatorFacade;

	@Autowired
	private LiveSearchFacadeRemote liveSearchFacade;

	@Autowired
	private SearchFilterFacadeLocal searchFilter;

	@Autowired
	private ResourceResolver resourceResolver;

	@Autowired
	private LayoutFacadeLocal layoutFacade;

	@Autowired
	private MetaProvider metaProvider;

	@Autowired
	private NuclosRemoteContextHolder remoteContext;

	@Autowired
	private EntityObjectFacadeRemote eoFacade;

	@Autowired
	private EntityObjectFacadeLocal eoFacadeLocal;

	@Autowired
	private StateFacadeRemote stateFacadeRemote;

	@Autowired
	private StateFacadeLocal stateFacadeLocal;

	@Autowired
	private StateCache stateCache;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	@Autowired
	private DatasourceFacadeRemote dsFacade;

	@Autowired
	private ExtendedDatasourceFacadeBean dsExtFacade;

	@Autowired
	private DatasourceCache dsCache;

	@Autowired
	private ExtendedDatasourceServerUtils dssUtils;

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	@Autowired
	private EventSupportCache esCache;

	@Autowired
	private EventSupportFacadeLocal esFacade;

	@Autowired
	private TransferFacadeLocal transferFacade;

	@Autowired
	private TreeNodeFacadeRemote treeNodeFacadeRemote;

	@Autowired
	private ServerParameterProvider serverParameter;

	@Autowired
	private NuclosParameterProvider nuclosParameterProvider;

	@Autowired
	private MasterDataRestFqnCache fqnCache;

	@Autowired
	private ReportFacadeRemote reportFacade;

	@Autowired
	private GenerationCache genCache;

	@Autowired
	private NuclosRemoteExceptionLogger remoteExceptionLogger;

	@Autowired
	private SecurityFacadeLocal securityFacade;

	@Autowired
	private NuclosUserDetailsContextHolder userCtx;

	@Autowired
	@Qualifier("tasklistService")
	private TasklistFacadeLocal tasklistFacade;

	@Autowired
	private CustomComponentFacadeBean customComponentFacade;

	@Autowired
	private PlanningTableFacadeBean planningTableFacade;

	@Autowired
	private DataLanguageCache dataLanguageCache;

	@Autowired
	private MaintenanceFacadeLocal maintenanceFacade;

	@Autowired
	private TextModuleFacadeBean textModuleFacade;

	@Transactional(noRollbackFor = {Exception.class})
	public SessionContext webLogin(
			String login,
			String password,
			Locale locale,
			String datalanguage,
			String jSessionId,
			String loginUri,
			String serverUri) {

		if (looksEmpty(login)) {
			login = null;
		}

		String username = null;

		URI serverURI = null;
		if (serverUri != null) {
			try {
				serverURI = new URI(serverUri);
			} catch (URISyntaxException e) {
				throw new NuclosWebException(Response.Status.PRECONDITION_FAILED, e.getMessage());
			}
		}

		Authentication auth = null;
		final Mutable<SecurityCache.NuclosSsoSecurityObject> mutableNsso = new Mutable<>();
		if (login == null && loginUri != null) {
			auth = trySsoAuthentication(mutableNsso, jSessionId, loginUri);
		}
		final SecurityCache.NuclosSsoSecurityObject nsso = mutableNsso.getValue();

		try {
			if (auth == null) {
				if (login == null) {
					throw new NuclosWebException(Response.Status.UNAUTHORIZED, "Missing login");
				}
				auth = new UsernamePasswordAuthenticationToken(login, password);

				// TODO: Authentication and logging of the attempt should happen in one call
				SpringApplicationContextHolder.getBean(RemoteAuthenticationManager.class).attemptAuthentication(login, password);
				auth = SpringApplicationContextHolder.getBean(NuclosAuthenticationProvider.class).authenticate(auth);
			}

			if (maintenanceFacade.blockUserLogin(auth.getName())) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}

			return authenticate(username, auth, jSessionId, locale, datalanguage, serverURI, nsso, true);
		} catch (Exception e) {
			handleAuthenticationException(e, username, nsso);
			throw e;
		}
	}

	private WebSessionContext authenticate(String username, Authentication auth, String jSessionId, Locale locale, String datalanguage,
										   URI serverURI, SecurityCache.NuclosSsoSecurityObject nsso, boolean withLogin) throws NuclosWebException {
		try {
			// login with email -> get login from authentication
			username = auth.getName();

			if (maintenanceFacade.blockUserLogin(username)) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}

			final List<MandatorVO> mandators = getMandators(username);
			if (securityCache.isMandatorPresent() && mandators.size() == 0) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}

			final boolean restApiAllowed = isRestApiAllowed(username, mandators);

			if (!restApiAllowed) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}

			SecurityContextHolder.getContext().setAuthentication(auth);

			Long sessionId = null;
			if (withLogin) {
				LoginResult loginResult = securityFacade.login();
				assert Objects.equals(loginResult.getjSessionId(), jSessionId);
				sessionId = loginResult.getSessionId();
			}
			boolean richClientAttached = nsso != null &&
					!RigidUtils.equal(nsso.getOAuth2State(), securityCache.getNuclosSsoAuthState());

			WebSessionContext webSessionContext = new WebSessionContext(
					sessionId,
					jSessionId,
					auth,
					locale,
					richClientAttached,
					serverURI);

			webSessionContext.setMandators(mandators);
			LocaleContextHolder.setLocale(webSessionContext.getLocale());
			setDataLanguage(datalanguage, webSessionContext);

			// Never add a half filled sessionContext to the cache!
			// Clients may be requesting it via 'GET:rest/login' before this method returns
			securityCache.setSessionContext(jSessionId, webSessionContext);

			if (richClientAttached) {
				securityCache.setNuclosSsoSpringAuthentication(nsso.getOAuth2State(), auth);
			}

			return webSessionContext;
		} catch (Exception e) {
			handleAuthenticationException(e, username, nsso);
			throw e;
		}
	}

	private void handleAuthenticationException(Exception e, String username, SecurityCache.NuclosSsoSecurityObject nsso) {
		if (nsso != null) {
			securityCache.setNuclosSsoSecurityObjectStatus(nsso, SsoAuthStatus.UNKNOWN);
		}
		if (username != null) {
			securityCache.removeSessionContextsForUser(username, null);
		}
		webLogout(null);
		if (e instanceof AccessDeniedException) {
			throw new NuclosWebException(Response.Status.FORBIDDEN, e.getMessage());
		}
	}

	private Authentication trySsoAuthentication(
			@NotNull final Mutable<SecurityCache.NuclosSsoSecurityObject> mutableNsso,
			@NotNull final String jSessionId,
			@NotNull String loginUri
		) {
		if (mutableNsso == null || jSessionId == null || loginUri == null) {
			throw new IllegalArgumentException(String.format("Arguments must not be null: " +
							"mutableNsso=%s, jSessionId=%s, loginUri=%s",
							mutableNsso, jSessionId, loginUri
					));
		}

		Authentication auth = null; // the result

		SecurityCache.NuclosSsoSecurityObject nsso = null;
		SsoAuth ssoAuth = null;
		try {
			if (loginUri.endsWith("#")) {
				// With a "#", Nimbus does not process any query parameters.
				loginUri = loginUri.substring(0, loginUri.length()-1);
			}

			final AuthorizationResponse authResponse;
			try {
				authResponse = AuthorizationResponse.parse(new URI(loginUri));
			} catch (com.nimbusds.oauth2.sdk.ParseException | URISyntaxException e) {
				throw NuclosSsoUtils.logAndGetException(LOG, ssoAuth, jSessionId, e, "loginUri \"{}\" is not parsable", loginUri);
			}
			if (!authResponse.indicatesSuccess()) {
				// We got an error tokenResponse...
				NuclosSsoUtils.throwException(authResponse.toErrorResponse(), String.format("authentication response parsing (%s)", loginUri));
			} else {
				// try SSO authentication
				AuthorizationSuccessResponse authSuccessResponse = authResponse.toSuccessResponse();
				AuthorizationCode code = authSuccessResponse.getAuthorizationCode();
				State state = authSuccessResponse.getState();
				// Check the returned state parameter, must match be a valid cache entry
				nsso = securityCache.getNuclosSsoSecurityObject(state);
				if (nsso.getNuclosSsoAuthStatus() != SsoAuthStatus.INITIATED) {
					NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, "Response state {} could not be validated", state);
				}
				mutableNsso.setValue(nsso);
				ssoAuth = securityCache.getSsoAuth(nsso.getSsoAuthUID());

				URI callback = null;
				if (!looksEmpty(ssoAuth.getOAuth2Callback())) {
					try {
						callback = new URI(ssoAuth.getOAuth2Callback());
					} catch (URISyntaxException ex) {
						NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, ex, "OAuth2Callback is not a malformed URL");
					}
				}
				AuthorizationGrant codeGrant = new AuthorizationCodeGrant(code, callback, nsso.getCodeVerifier());

				URI tokenEndpoint = NuclosSsoUtils.getTokenEndpoint(LOG, ssoAuth, jSessionId);

				// The credentials to authenticate the client at the token endpoint
				ClientID clientID = new ClientID(ssoAuth.getOAuth2ClientId());
				String sClientSecret = ssoAuth.getOAuth2ClientSecret();
				Secret clientSecret = null;

				// Make the token tokenRequest
				TokenRequest tokenRequest = null;
				if (looksEmpty(sClientSecret)) {
					tokenRequest = new TokenRequest(tokenEndpoint, clientID, codeGrant);
				} else {
					clientSecret = new Secret(sClientSecret);
					ClientAuthentication clientAuth = new ClientSecretBasic(clientID, clientSecret);
					tokenRequest = new TokenRequest(tokenEndpoint, clientAuth, codeGrant);
				}

				TokenResponse tokenResponse = null;
				try {
					tokenResponse = TokenResponse.parse(tokenRequest.toHTTPRequest().send());
				} catch (com.nimbusds.oauth2.sdk.ParseException ex) {
					throw NuclosSsoUtils.logAndGetException(LOG, ssoAuth, jSessionId, ex, "Token response is not parsable");
				} catch (IOException ex) {
					throw NuclosSsoUtils.logAndGetException(LOG, ssoAuth, jSessionId, ex, "Token response could not be send");
				}

				if (!tokenResponse.indicatesSuccess()) {
					// We got an error tokenResponse...
					NuclosSsoUtils.throwException(tokenResponse.toErrorResponse(), String.format("token request against \"%s\"", tokenEndpoint));
				}

				if (ssoAuth.getAuthLevel() != null && tokenResponse instanceof AccessTokenResponse) {
					AccessTokenResponse accessTokenResponse = (AccessTokenResponse)tokenResponse;
					Object oAuthLevel = accessTokenResponse.getCustomParameters().get("auth_level");
					int iAuthLevel = oAuthLevel instanceof Number ? ((Number)oAuthLevel).intValue() : 0;
					if (iAuthLevel < ssoAuth.getAuthLevel()) {
						NuclosSsoUtils.logAndThrowException(
								LOG,
								ssoAuth,
								jSessionId,
								String.format(
										"Auth Level too low (%d < %d)",
										iAuthLevel,
										ssoAuth.getAuthLevel()
								)
						);
					}
				}

				ClaimsSet claimsSet = null;
				AccessTokenResponse successResponse = tokenResponse.toSuccessResponse();
				// Get the access token, the server may also return a refresh token
				AccessToken accessToken = successResponse.getTokens().getAccessToken();
				RefreshToken refreshToken = successResponse.getTokens().getRefreshToken();

				// 1. Identity assertion: ID Token
				JWT idToken = null;
				Object idTokenObject = successResponse.getCustomParameters().get("id_token");
				if (idTokenObject != null) {
					if (looksEmpty(ssoAuth.getOidcIdTokenIssuer())) {
						NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "Id token {} received, but OidcIdTokenIssuer not configured, ignoring id token...", idTokenObject);
					} else {
						NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "Id token {} received, try to validate...", idTokenObject);

						try {
							idToken = JWTParser.parse(idTokenObject.toString());
						} catch (ParseException ex) {
							throw NuclosSsoUtils.logAndGetException(LOG, ssoAuth, jSessionId, ex, "Id token is not parsable");
						}

						String idTokenIssuer = ssoAuth.getOidcIdTokenIssuer();
						Issuer iss = new Issuer(idTokenIssuer);

						String idTokenJwsAlgorithm = ssoAuth.getOidcIdTokenJwsAlgorithm();
						if (looksEmpty(idTokenJwsAlgorithm)) {
							idTokenJwsAlgorithm = "RS256"; // default
							NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "OidcIdTokenJwsAlgorithm not configured, using default {}", idTokenJwsAlgorithm);
						}
						JWSAlgorithm jwsAlg = JWSAlgorithm.parse(idTokenJwsAlgorithm);

						String idTokenJwkSetURL = ssoAuth.getOidcIdTokenJwkSet();
						// Create validator for signed ID tokens
						IDTokenValidator validator;
						if (!looksEmpty(idTokenJwkSetURL)) {
							try {
								validator = new IDTokenValidator(iss, clientID, jwsAlg, new URL(idTokenJwkSetURL));
							} catch (MalformedURLException ex) {
								throw NuclosSsoUtils.logAndGetException(LOG, ssoAuth, jSessionId, ex, "OidcIdTokenJwkSet is not a malformed URL");
							}
						} else {
							// Fallback ID tokens with HMAC using the client_secret as a key
							if (clientSecret != null) {
								NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "OidcIdTokenJwkSet not configured, try HMAC with client secret");
								validator = new IDTokenValidator(iss, clientID, jwsAlg, clientSecret);
							} else {
								NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "OidcIdTokenJwkSet not configured, try HMAC without client secret");
								validator = new IDTokenValidator(iss, clientID);
							}
						}

						// Set the expected nonce, if enabled
						Nonce expectedNonce = null;
						if (Boolean.TRUE.equals(ssoAuth.getOidcIdTokenNonce())) {
							expectedNonce = nsso.getOidcNonce();
						}

						try {
							claimsSet = validator.validate(idToken, expectedNonce);
						} catch (BadJOSEException | JOSEException ex) {
							NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, ex, "Error during id token validation");
						}
					}
				}

				// 2. Identity assertion: UserInfo request
				if (claimsSet == null) {
					if (looksEmpty(ssoAuth.getOidcUserInfoEndpoint())) {
						NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, "Missing id token and Userinfo endpoint not configured");
					}

					URI userInfoEndpoint = null; // The UserInfoEndpoint of the OpenID provider
					try {
						userInfoEndpoint = new URI(ssoAuth.getOidcUserInfoEndpoint());
					} catch (URISyntaxException ex) {
						NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, ex, "OidcUserInfoEndpoint is not a malformed URL");
					}
					BearerAccessToken userInfoToken;
					if (accessToken instanceof BearerAccessToken) {
						userInfoToken = (BearerAccessToken) accessToken;
					} else {
						userInfoToken = new BearerAccessToken(accessToken.getValue(), accessToken.getLifetime(), accessToken.getScope());
					}

					UserInfoResponse userInfoResponse = null;
					try {
						userInfoResponse = UserInfoResponse.parse(new UserInfoRequest(userInfoEndpoint, userInfoToken)
								.toHTTPRequest()
								.send());
					} catch (com.nimbusds.oauth2.sdk.ParseException ex) {
						throw NuclosSsoUtils.logAndGetException(LOG, ssoAuth, jSessionId, ex, "Userinfo response is not parsable");
					} catch (IOException ex) {
						NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, ex, "Sending of userinfo request failed");
					}

					if (!userInfoResponse.indicatesSuccess()) {
						// The tokenRequest failed, e.g. due to invalid or expired token
						NuclosSsoUtils.throwException(userInfoResponse.toErrorResponse(), String.format("userinfo request against \"%s\"", userInfoEndpoint));
					}

					// Extract the claims
					claimsSet = userInfoResponse.toSuccessResponse().getUserInfo();
				}

				NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "ClaimsSet {} received", claimsSet.toJSONString());

				// Look for a corresponding nuclos user
				NuclosUserDetailsWithoutCredentials userDetails = null;
				if (!looksEmpty(ssoAuth.getOidcCustomClaim())) {
					String sCustomClaim = claimsSet.getStringClaim(ssoAuth.getOidcCustomClaim().trim());
					if (looksEmpty(sCustomClaim)) {
						NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "ClaimsSet does not contain any value for the configured custom claim: {}", ssoAuth.getOidcCustomClaim());
					}
					if (!looksEmpty(sCustomClaim)) {
						sCustomClaim = sCustomClaim.trim();
						Query<SsoUserIdentifier> q = QueryProvider.create(SsoUserIdentifier.class);
						q.where(SsoUserIdentifier.SsoAuthId.eq(ssoAuth.getId()))
								.and(SsoUserIdentifier.Identifier.eq(sCustomClaim));
						List<SsoUserIdentifier> ssoUserIdentifiers = QueryProvider.execute(q);
						if (ssoUserIdentifiers.size() == 1) {
							try {
								UID userId = (UID) ssoUserIdentifiers.get(0).getUserId();
								String userName = securityCache.getUserName(userId);
								userDetails = SpringApplicationContextHolder.getBean(NuclosUserDetailsService.class).findUserByUsernameCached(userName);
							} catch (UsernameNotFoundException notFoundException) {
								// ignore
							}
						} else if (ssoUserIdentifiers.size() > 1) {
							NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "More than one user found for custom claim {} and identifier \"{}\", skipping...",
									ssoAuth.getOidcCustomClaim(), sCustomClaim);
						}
					}
				}
				if (userDetails == null && !looksEmpty(ssoAuth.getOidcEmailClaim())) {
					String sEmailClaim = claimsSet.getStringClaim(ssoAuth.getOidcEmailClaim().trim());
					if (looksEmpty(sEmailClaim)) {
						NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "ClaimsSet does not contain any value for the configured email claim: {}", ssoAuth.getOidcEmailClaim());
					}
					if (!looksEmpty(sEmailClaim)) {
						sEmailClaim = sEmailClaim.trim();
						try {
							userDetails = SpringApplicationContextHolder.getBean(NuclosUserDetailsService.class).findUserByUsernameCached(sEmailClaim.trim());
							if (!sEmailClaim.equalsIgnoreCase(userDetails.getEmail())) {
								NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId,
										"NuclosUserDetails[id={},username={},email={}] found for claim {} (email) but field match is wrong, ignoring... ",
										userDetails.getUserUID().getString(), userDetails.getUsername(), userDetails.getEmail(), sEmailClaim);
								userDetails = null;
							}
						} catch (UsernameNotFoundException notFoundException) {
							// ignore
						}
					}
				}
				if (userDetails == null && !looksEmpty(ssoAuth.getOidcUsernameClaim())) {
					String sUsernameClaim = claimsSet.getStringClaim(ssoAuth.getOidcUsernameClaim().trim());
					if (looksEmpty(sUsernameClaim)) {
						NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "ClaimsSet does not contain any value for the configured username claim: {}", ssoAuth.getOidcUsernameClaim());
					}
					if (!looksEmpty(sUsernameClaim)) {
						sUsernameClaim = sUsernameClaim.trim();
						try {
							userDetails = SpringApplicationContextHolder.getBean(NuclosUserDetailsService.class).findUserByUsernameCached(sUsernameClaim);
							if (!sUsernameClaim.equalsIgnoreCase(userDetails.getUsername())) {
								NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId,
										"NuclosUserDetails[id={},username={},email={}] found for claim {} (username) but field match is wrong, ignoring... ",
										userDetails.getUserUID().getString(), userDetails.getUsername(), userDetails.getEmail(), sUsernameClaim);
								userDetails = null;
							}
						} catch (UsernameNotFoundException notFoundException) {
							// ignore
						}
					}
				}

				if (userDetails != null) {
					if (userDetails.isAccountNonExpired() && userDetails.isAccountNonLocked() && !userDetails.isLoginRestricted(UserLoginRestriction.PASSWORD)) {
						NuclosSsoAuthenticationToken authToken = new NuclosSsoAuthenticationToken(
								userDetails.getUsername(),
								ssoAuth.getId(),
								accessToken,
								refreshToken,
								idToken,
								userDetails.getAuthorities());
						authToken.setDetails(userDetails);

						auth = authToken;
						NuclosSsoUtils.logDebug(LOG, ssoAuth, jSessionId, "User {} gets SSO authenticated: {}",
								userDetails.getUsername(), auth.isAuthenticated());
						SpringApplicationContextHolder.getBean(NuclosUserDetailsService.class).logLastLogin(userDetails);
					} else {
                        if (!userDetails.isAccountNonLocked() || userDetails.isLoginRestricted(UserLoginRestriction.PASSWORD)) {
                            NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, NuclosWebException.STATUS_LOCKED, "nuclos.security.authentication.locked");
                        } else {
							NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, NuclosWebException.STATUS_LOCKED, "nuclos.security.authentication.accountexpired");
						}
					}
				} else {
					NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, Response.Status.NOT_FOUND, "Nuclos user not found");
				}
			}
		} catch (RuntimeException rex) {
			if (nsso != null) {
				securityCache.setNuclosSsoSecurityObjectStatus(nsso, SsoAuthStatus.UNKNOWN);
			}
			NuclosSsoUtils.logAndThrowException(LOG, ssoAuth, jSessionId, rex, "");
		}

		return auth;
	}

	@Override
	public UID setDataLanguage(final String datalanguage, final SessionContext webSessionContext) {
		UID datalanguageUID = null;
		if (datalanguage != null) {
			for (DataLanguageVO dataLanguageVO : dataLanguageCache.getDataLanguages()) {
				try {
					if (dataLanguageVO.getLocale().equals(LocaleUtils.toLocale(datalanguage))) {
						datalanguageUID = dataLanguageVO.getPrimaryKey();
						break;
					}
				} catch (IllegalArgumentException ex) {
					if (dataLanguageVO.getLocale().equals(Locale.forLanguageTag(datalanguage))) {
						datalanguageUID = dataLanguageVO.getPrimaryKey();
						break;
					}
				}
			}
		}
		final UID primaryDataLanguage = getPrimaryDataLanguage();
		if (primaryDataLanguage != null && datalanguageUID == null) {
			datalanguageUID = primaryDataLanguage;
		}
		webSessionContext.setDataLanguageUID(datalanguageUID);
		userCtx.setDataLocal(datalanguageUID);

		return datalanguageUID;
	}

	private List<MandatorVO> getMandators(final String username) {
		List<MandatorVO> mandators = null;
		if (securityCache.isMandatorPresent()) {
			mandators = new ArrayList<>(securityCache.getAssignedMandators(username));
			Iterator<MandatorVO> it = mandators.iterator();
			while (it.hasNext()) {
				MandatorVO mandatorVO = it.next();
				if (!securityCache.getAllowedActions(username, mandatorVO.getUID()).contains(Actions.ACTION_RESTAPI)) {
					it.remove();
				}
			}
		}
		return mandators;
	}

	public boolean isRestApiAllowed(final String username) {
		List<MandatorVO> mandators = getMandators(username);

		return isRestApiAllowed(username, mandators);
	}

	@Override
	public Set<UsageCriteria> getUsageCriteriaBySearchExpression(final UID entity, final CollectableSearchExpression clctexpr) {
		remoteContext.setRemotly(true);
		return nucletDalProvider.getEntityObjectProcessor(entity).getUsageCriteriaBySearchExpression(clctexpr);
	}

	@Override
	public Set<RecordGrantRight> getRecordGrantRightBySearchExpression(final UID entity, final CollectableSearchExpression clctexpr) {
		remoteContext.setRemotly(true);
		return nucletDalProvider.getEntityObjectProcessor(entity).getRecordGrantRightBySearchExpression(clctexpr);
	}

	/**
	 * @param clctexpr
	 * @return the distinct owner UIDs of the records. Non-owned records results in a UID_NULL
	 */
	@Override
	public Set<UID> getNuclosOwnerBySearchExpression(final UID entity, final CollectableSearchExpression clctexpr) {
		remoteContext.setRemotly(true);
		return nucletDalProvider.getEntityObjectProcessor(entity).getNuclosOwnerBySearchExpression(clctexpr);
	}

	@Override
	public StateVO getState(final UID stateUID) throws CommonFinderException {
		return stateFacadeLocal.getState(stateUID);
	}

	private boolean isRestApiAllowed(
			final String username,
			final List<MandatorVO> mandators
	) {
		final boolean result;

		// TODO: Mandators auto-activate REST-API rights?!
		if (mandators == null || mandators.isEmpty()) {
			result = securityCache.getAllowedActions(username, null).contains(Actions.ACTION_RESTAPI);
		} else {
			result = true;
		}

		return result;
	}

	@Transactional(noRollbackFor= {Exception.class})
	public void chooseMandator(UID mandatorUID) throws CommonPermissionException {
		List<MandatorVO> assignedMandators = securityCache.getAssignedMandators(getCurrentUserName());
		for (MandatorVO mandatorVO : assignedMandators) {
			if (mandatorVO.getUID().equals(mandatorUID)) {
				if (securityCache.getAllowedActions(getCurrentUserName(), mandatorUID).contains(Actions.ACTION_RESTAPI)) {
					userCtx.setMandatorUID(mandatorUID);
					return;
				}
			}
		}
		throw new CommonPermissionException("Mandator not assigned");
	}

	@Transactional(noRollbackFor= {Exception.class})
	public void changePassword(String username, String password, String newPassword, String jSessionId) throws NuclosWebException {
		String currentUser = getCurrentUserName();
		UID currentMandatorUID = getCurrentMandatorUID();
		if (!currentUser.equals("anonymousUser") && !securityCache.getAllowedActions(currentUser, currentMandatorUID).contains(Actions.ACTION_RESTAPI)) {
			throw new NuclosWebException(Response.Status.PRECONDITION_FAILED);
		}

		try {
			SpringApplicationContextHolder.getBean(RemoteAuthenticationManager.class).changePassword(username, password, newPassword, getCustomUsage());
			securityCache.setAuthentication2NullForUser(username, jSessionId);
			securityCache.removeSessionContextsForUser(username, jSessionId);
		} catch (NuclosWebException nwe) {
			if (nwe.getResponse().getStatus() == 423) {
				//User locked from now on
				//log out to apply
				securityCache.setAuthentication2NullForUser(currentUser, null);
				securityCache.removeSessionContextsForUser(currentUser, null);
				SecurityContextHolder.clearContext();
			}
			throw nwe;
		} catch (BadCredentialsException | CommonPermissionException bce) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		} catch (CommonBusinessException cbe) {
			throw new NuclosWebException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	public SessionContext getSessionContext(String webSessionID) {
		if (webSessionID == null) {
			return null;
		}
		SessionContext webSessionContext = securityCache.getSessionContext(webSessionID);
		return webSessionContext;
	}

	@Override
	@Transactional(noRollbackFor = {Exception.class})
	public NuclosRuleBasedAuthenticationToken tryRuleBasedAuthentication(
			final HttpHeaders httpHeaders,
			final UriInfo uriInfo,
			final String sessionId,
			final String remoteAddr,
			final String remoteHost,
			final String remoteUser) {

		Iterator<EventSupportSourceVO> itRules = esCache.getEventSupportsByTypeAndOrderedByPriority(AuthenticationRule.class, true).iterator();
		while (itRules.hasNext()) {
			try {
				final EventSupportSourceVO sourceVO = itRules.next();
				final AuthenticationContextImpl authContext = new AuthenticationContextImpl(
						sourceVO.getClassname(),
						httpHeaders,
						uriInfo,
						sessionId,
						remoteAddr,
						remoteHost,
						remoteUser);
				AuthenticationResult authResult = esFacade.executeAuthentication(authContext, sourceVO.isSystem());
				if (authResult == null || (authResult.getUsername() == null && authResult.getUserId() == null)) {
					// not authenticated, ignore and try next...
				} else {
					final NuclosRuleBasedAuthenticationToken auth = authenticateRuleBased(authResult, sourceVO.getClassname(), sessionId);
					return auth;
				}
			} catch (Exception ex) {
				if (ex instanceof WebApplicationException) {
					throw (WebApplicationException) ex;
				} else if (ex instanceof NuclosBusinessException) {
					throw new NuclosWebException(Response.Status.PRECONDITION_FAILED, ex.getMessage());
				} else {
					remoteExceptionLogger.logException(ex, LOG);
					throw new NuclosWebException(Response.Status.INTERNAL_SERVER_ERROR);
				}
			}
		}

		return null;
	}

	private NuclosRuleBasedAuthenticationToken authenticateRuleBased(AuthenticationResult authResult, String ruleClass, final String sessionId) throws WebApplicationException {
		final UID userId = (UID) authResult.getUserId();
		final String username;
		try {
			username = defaultIfNull(userId != null ? securityCache.getUserName(userId) : null, authResult.getUsername());
		} catch (Exception ex) {
			throw new WebApplicationException("User with id \"" + userId + "\" does not exist", Response.Status.UNAUTHORIZED);
		}
		final NuclosUserDetailsWithoutCredentials userDetails = findUserDetailsAndCheckAccountStatus(username);
		final NuclosRuleBasedAuthenticationToken auth = new NuclosRuleBasedAuthenticationToken(username, authResult, ruleClass, userDetails.getAuthorities());
		auth.setDetails(userDetails);

		Locale locale = authResult.getLocale() == null ? null : LocaleUtils.toLocale(authResult.getLocale().toString());
		String datalanguage = authResult.getDataLanguage() == null ? null : authResult.getDataLanguage().toString();
		URI serverURI = authResult.getServerUri();
		UID mandatorUID = (UID) authResult.getMandatorId();
		WebSessionContext sessionContext = authenticate(username, auth, sessionId, locale, datalanguage, serverURI, null, authResult.isLoginRequired());
		sessionContext.setMandatorUID(mandatorUID);

		return auth;
	}

	@Override
	@Transactional(noRollbackFor= {Exception.class})
	public void refreshRuleBasedAuthentication(final NuclosRuleBasedAuthenticationToken authToken,
											   final HttpHeaders httpHeaders, final UriInfo uriInfo, final String sessionId,
											   final String remoteAddr, final String remoteHost, final String remoteUser) throws WebApplicationException {
		try {
			findUserDetailsAndCheckAccountStatus(authToken.getName());
			final AuthenticationContextImpl context = new AuthenticationContextImpl(
					authToken.getAuthRule(),
					httpHeaders,
					uriInfo,
					sessionId,
					remoteAddr,
					remoteHost,
					remoteUser);
			context.setAuthenticationResult(authToken.getAuthResult());
			if (esFacade.refreshAuthentication(context, esCache.getEventSupportsByTypeAndOrderedByPriority(AuthenticationRule.class, true).stream()
					.filter(eseVO -> Objects.equals(eseVO.getClassname(), authToken.getAuthRule()))
					.findAny()
					.map(EventSupportSourceVO::isSystem)
					.orElse(false))) {
				authToken.calculateValidUntil();
			} else {
				throw new WebApplicationException("Authentication expired", Response.Status.UNAUTHORIZED);
			}
		} catch (NuclosRuleCompileException ex) {
			remoteExceptionLogger.logException(ex, LOG);
			throw new NuclosWebException(Response.Status.INTERNAL_SERVER_ERROR);
		} catch (Exception ex) {
			if (ex instanceof WebApplicationException) {
				throw (WebApplicationException) ex;
			} else {
				remoteExceptionLogger.logException(ex, LOG);
				throw new NuclosWebException(Response.Status.PRECONDITION_FAILED);
			}
		}
	}

	@Override
	public JsonArray getTextModules(TextModuleSettings textModuleSettings) throws CommonBusinessException {
		JsonArrayBuilder jabResult = Json.createArrayBuilder();
		if (textModuleSettings != null) {
			List<TextModule> textModules = textModuleFacade.findTextModules(textModuleSettings);

			for (TextModule textModule : textModules) {
				JsonObjectBuilder jobTextModule = Json.createObjectBuilder();
				jobTextModule.add("label", textModule.getLabel());
				jobTextModule.add("text", textModule.getText());
				jabResult.add(jobTextModule);
			}
		}
		return jabResult.build();
	}

	private NuclosUserDetailsWithoutCredentials findUserDetailsAndCheckAccountStatus(String username) throws WebApplicationException {
		final NuclosUserDetailsWithoutCredentials userDetails;
		try {
			userDetails = SpringApplicationContextHolder.getBean(NuclosUserDetailsService.class).findUserByUsernameCached(username);
		} catch (UsernameNotFoundException ex) {
			throw new WebApplicationException("User with name \"" + username + "\" does not exist", Response.Status.UNAUTHORIZED);
		}
		if (userDetails.isAccountNonExpired() && userDetails.isAccountNonLocked()) {
			return userDetails;
		} else {
			if (!userDetails.isAccountNonExpired()) {
				throw new WebApplicationException("Account expired", Response.Status.UNAUTHORIZED);
			} else {
				throw new WebApplicationException("Account locked", Response.Status.UNAUTHORIZED);
			}
		}
	}

	public SessionContext validateSessionAuthenticationAndInitUserContext(String webSessionID) {
		SessionContext webSessionContext = getSessionContext(webSessionID);
		if (webSessionContext == null) {
			return null;
		}
		Authentication auth = webSessionContext.getAuthentication();
		if (auth == null) {
			try {
				UserDetails userDetails = SpringApplicationContextHolder.getBean(NuclosUserDetailsService.class).loadUserByUsername(webSessionContext.getUsername());
				Authentication nAuth = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword());
				auth = SpringApplicationContextHolder.getBean(NuclosAuthenticationProvider.class).authenticate(nAuth);
				webSessionContext.setAuthentication(auth);
			} catch (Exception e) {
				LOG.warn(e.getMessage(), e);
			}
		}
		if (auth != null) {
			SecurityContextHolder.getContext().setAuthentication(auth);
			LocaleContextHolder.setLocale(webSessionContext.getLocale());
			userCtx.setMandatorUID(webSessionContext.getMandatorUID());
			userCtx.setDataLocal(webSessionContext.getDataLanguageUID());
			return webSessionContext;
		}
		webLogout(webSessionContext.getJSessionId());
		return null;
	}

	public SessionContext initNewAnonymousNuclosSessionIfNecessary(String webSessionID, final String requestor, final Locale locale, final String dataLanguage) {
		if (maintenanceFacade == null || MaintenanceConstants.MAINTENANCE_MODE_ON.equals(maintenanceFacade.getMaintenanceMode())) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
		}

		SessionContext webSessionContext = getSessionContext(webSessionID);
		if (webSessionContext == null) {
			try {
				final String anonymousUser = NuclosAuthenticationProvider.ANONYMOUS_USER_NAME;
				UserDetails userDetails;
				try {
					userDetails = SpringApplicationContextHolder.getBean(NuclosUserDetailsService.class).loadUserByUsername(anonymousUser);
				} catch (Exception ex) {
					LOG.debug("Nuclos anonymous user does not exists. Use a generic anonymous user instead.");
					userDetails = new User(anonymousUser, "", true, true, true, true, Collections.emptyList());
				}
				final UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
						userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
				auth.setDetails(userDetails);
				SecurityContextHolder.getContext().setAuthentication(auth);
				LOG.trace("Anonymous user gets authenticated. Requestor: {}", requestor);
				LoginResult loginResult = securityFacade.login();
				webSessionContext = securityCache.setSessionContext(
						auth,
						locale,
						webSessionID,
						loginResult.getSessionId()
				);
				webSessionContext.setMandators(getMandators(anonymousUser));
				SecurityContextHolder.getContext().setAuthentication(auth);
				LocaleContextHolder.setLocale(webSessionContext.getLocale());
				setDataLanguage(dataLanguage, webSessionContext);
			} catch (Exception ex) {
				webLogout(webSessionID);
				return null;
			}
		}
		return webSessionContext;
	}

	@Transactional(noRollbackFor= {Exception.class})
	public void webLogout(String webSessionID) {
		securityFacade.logout(webSessionID);

		// SecurityContextHolder.getContext().setAuthentication(null);
		SecurityContextHolder.getContext().setAuthentication(
				// auth for writing login attempts & co.
			new AnonymousAuthenticationToken("after_webLogout", "anonymousUser", AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"))
		);
		if (webSessionID != null) securityCache.removeSessionContext(webSessionID);
	}

	@Override
	@Transactional(noRollbackFor= {Exception.class})
	public void invalidateSession(String sessionId) {
		SecurityContextHolder.getContext().setAuthentication(null);
		if (sessionId != null) {
			securityCache.removeSessionContext(sessionId);
		}
		securityFacade.logout(/*no need for a session id here, no login protocol is written:*/(String)null);
	}

	public EntitySearchFilter2 getSearchFilterByPk(UID pk) throws CommonBusinessException {
		SearchFilterVO vo = searchFilter.getSearchFilterByPk(pk);
		return vo != null ? searchFilter.createEntitySearchFilterFromVO(vo) : null;
	}


	public List<SearchFilterVO> getSearchFilters() throws CommonBusinessException {
		List<SearchFilterVO> searchFilters = new ArrayList<SearchFilterVO>(searchFilter.getAllSearchFilterByCurrentUser());
		Collections.sort(searchFilters);
		return searchFilters;
	}

	public Resource getResource(UID uid) throws IOException {
		return resourceResolver.resolveDbResourceIcon(uid);
	}

	public EntityPermission getEntityPermission(UID entity) {
		EntityMeta<Object> eMeta = metaProvider.getEntity(entity);
		boolean statemodel = eMeta.isStateModel();
		String user = getCurrentUserName();
		UID mandatorUID = getCurrentMandatorUID();
		try {
			securityCache.checkMandatorLoginPermission(user, mandatorUID);
		} catch (CommonPermissionException e) {
			throw new NuclosFatalException(e);
		}
		boolean readAllowed = securityCache.isReadAllowedForEntity(user, entity, mandatorUID);
		boolean writeAllowed = eMeta.isEditable() &&
				(statemodel ? securityCache.isWriteAllowedForModule(user, entity, mandatorUID) : securityCache.isWriteAllowedForMasterData(user, entity, mandatorUID));

		boolean updateAllowed = writeAllowed;
		boolean insertAllowed = writeAllowed;
		boolean deleteAllowed = writeAllowed;

		//With state-model the initial transition must be allowed, too
		if (insertAllowed && statemodel) {
			insertAllowed = securityCache.isNewAllowedForModule(user, entity, mandatorUID);
		}

		if (deleteAllowed) {
			if (statemodel) {
				deleteAllowed = securityCache.isDeleteAllowedForModule(user, entity, true, mandatorUID);
			} else {
				// refs NUCLOS-5027
				deleteAllowed = securityCache.isDeleteAllowedForMasterData(user, entity, mandatorUID);
			}
		}

		return new EntityPermission(readAllowed, updateAllowed, insertAllowed, deleteAllowed);
	}

	@Override
	public boolean isDatasourceAllowed(final UID datasourceUID) {
		return securityCache.getReadableDataSourceIds(getCurrentUserName(), getCurrentMandatorUID()).contains(datasourceUID);
	}

	@Override
	public boolean isWriteAllowedForMD(UID entity) {
		return securityCache.isWriteAllowedForMasterData(getCurrentUserName(), entity, getCurrentMandatorUID());
	}

	@Override
	public boolean isWriteAllowedForGO(UID entity) {
		return securityCache.isWriteAllowedForModule(getCurrentUserName(), entity, getCurrentMandatorUID());
	}

	@Override
	public boolean isDeleteAllowedForGO(UID entity, boolean bPhysically) {
		return securityCache.isDeleteAllowedForModule(getCurrentUserName(), entity, bPhysically, getCurrentMandatorUID());
	}

	public Collection<FieldMeta<?>> getAllFieldsByEntity(UID entity, boolean onlyReferenceFields) {
		Collection<FieldMeta<?>> result = new HashSet<FieldMeta<?>>();
		Map<UID, FieldMeta<?>> mapFields = metaProvider.getAllEntityFieldsByEntity(entity);
		for (FieldMeta<?> fmvo : mapFields.values()) {
			if (onlyReferenceFields && fmvo.getForeignEntity() == null) {
				continue;
			}
			result.add(fmvo);
		}
		return result;
	}

	public boolean isSuperUser(){
		return securityCache.isSuperUser(getCurrentUserName());
	}

	public boolean isReadAllowedForField(FieldMeta<?> fm, UsageCriteria usage) {
		return isReadOrWriteAllowedForField(fm, usage, false);
	}

	public boolean isWriteAllowedForField(FieldMeta<?> fm, UsageCriteria usage) {
		return isReadOrWriteAllowedForField(fm, usage, true);
	}

	private boolean isReadOrWriteAllowedForField(FieldMeta<?> fm, UsageCriteria usage, boolean bWrite) {

		if (SF.CHANGEDAT.checkField(fm.getFieldName()) ||
				SF.CHANGEDBY.checkField(fm.getFieldName()) ||
				SF.CREATEDAT.checkField(fm.getFieldName()) ||
				SF.CREATEDBY.checkField(fm.getFieldName())
				) {
			return bWrite ? false : true;
		}

		return securityCache.isReadOrWriteAllowedForField(fm, usage, bWrite, getCurrentUserName(), getCurrentMandatorUID());
	}

	public boolean isReadAllowedForSubform(UID subform, UsageCriteria usage) {
		return isReadOrWriteAllowedForSubform(subform, usage, false);
	}

	public boolean isWriteAllowedForSubform(UID subform, UsageCriteria usage) {
		return isReadOrWriteAllowedForSubform(subform, usage, true);
	}

	private boolean isReadOrWriteAllowedForSubform(UID subform, UsageCriteria usage, boolean bWrite) {
		return securityCache.isReadOrWriteAllowedForSubform(subform, usage, bWrite,
				getCurrentUserName(), getCurrentMandatorUID());
	}

	public SubformPermission getSubformPermission(UID subform, UsageCriteria usage) {
		return securityCache.getSubformPermission(subform, usage, getCurrentUserName(), getCurrentMandatorUID());
	}

	@Override
	public boolean isUnlockAllowed(UID entity) {
		final EntityMeta<Object> eMeta = metaProvider.getEntity(entity);
		if (eMeta.isOwner()) {
			if (eMeta.getUnlockMode() == UnlockMode.ALL_USERS_MANUALLY || isSuperUser()) {
				return true;
			}
		}
		return false;
	}

	public <PK> void unlock(UID entity, PK pk) throws CommonPermissionException {
		eoFacade.unlock(entity, pk);
	}

	public Long countEntityObjectRows(UID entity, CollectableSearchExpression clctexpr) throws CommonPermissionException {
		remoteContext.setRemotly(true);
		return eoFacade.countEntityObjectRows(entity, clctexpr);
	}

	public <PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunk(UID entity, CollectableSearchExpression clctexpr, ResultParams resultParams)
		throws CommonPermissionException {
		remoteContext.setRemotly(true);
		return eoFacade.getEntityObjectsChunk(entity, clctexpr, resultParams, getCustomUsage());
	}

	@Override
	public <PK> Collection<PK> getEntityObjectIds(UID entity, CollectableSearchExpression clctexpr)
			throws CommonPermissionException {
		remoteContext.setRemotly(true);
		return eoFacade.getEntityObjectIds(entity, clctexpr);
	}

	@Transactional(noRollbackFor = Exception.class)
	public <PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunkWithoutRollback(
			UID entity,
			CollectableSearchExpression clctexpr,
			ResultParams resultParams
	) throws CommonPermissionException {
		return eoFacade.getEntityObjectsChunk(entity, clctexpr, resultParams, getCustomUsage());
	}

	public UID getInitialStateId(UID entity) {
		UsageCriteria usagecriteria = new UsageCriteria(entity, null, null, getCustomUsage());
		return stateFacadeLocal.getInitialState(usagecriteria).getId();
	}

	public UID getInitialStateId(UID entity, UID processId) {
		UsageCriteria usagecriteria = new UsageCriteria(entity, processId, null, getCustomUsage());
		return stateFacadeLocal.getInitialState(usagecriteria).getId();
	}

	@Override
	public <PK> EntityObjectVO<PK> getThin(UID entity, PK pk, boolean remotely) throws CommonPermissionException {
		IExtendedEntityObjectProcessor<PK> processor = null;
		try {
			processor = nucletDalProvider.getEntityObjectProcessor(entity);
			processor.setThinReadEnabled(true);
		} catch (Exception ex) {
			LOG.warn("No processor for entity " + entity.getString());
		}
		try {
			return get(entity, pk, remotely);
		} finally {
			if (processor != null) {
				processor.setThinReadEnabled(false);
			}
		}
	}

	@Override
	public <PK> EntityObjectVO<PK> get(UID entity, PK pk, boolean remotely) throws CommonPermissionException {
		if (remotely) {
			remoteContext.setRemotly(true);
			return eoFacade.get(entity, pk);
		} else {
			return eoFacadeLocal.get(entity, pk);
		}
	}

	public <PK> EntityObjectVO<PK> getDataForField(PK pk, FieldMeta<?> fm) {
		EntityMeta<PK> em = metaProvider.getEntity(fm.getEntity());
		Set<FieldMeta<?>> fields = new HashSet<FieldMeta<?>>();
		fields.add(fm);
		final IExtendedEntityObjectProcessor<PK> eop = nucletDalProvider.getEntityObjectProcessor(em);
		final boolean ignoreRecordGrantsAndOthers = eop.getIgnoreRecordGrantsAndOthers();
		try {
			eop.setIgnoreRecordGrantsAndOthers(true);
			return eop.getByPrimaryKey(pk, fields);
		} finally {
			eop.setIgnoreRecordGrantsAndOthers(ignoreRecordGrantsAndOthers);
		}
	}

	public List<CollectableField> getReferenceList(
			UID field,
			String search,
			Long lMaxRowCount,
			WebValueListProvider vlp,
			String pk,
			UID mandatorUID
	) throws CommonBusinessException {
		FieldMeta<?> efMeta = metaProvider.getEntityField(field);

		if (vlp != null && ValuelistProviderVO.Type.NAMED.getType().equals(vlp.getType())) {
			List<CollectableField> result = getVLPData(
					field,
					vlp,
					pk,
					search,
					DEFAULT_RESULT_LIMIT,
					false,
					mandatorUID
			);

			if (!looksEmpty(search)) {
				Iterator<CollectableField> it = result.iterator();
				while ( it.hasNext()) {
					CollectableField clct = it.next();
					String sValue = (String) clct.getValue();
					if (!sValue.toUpperCase().contains(search.toUpperCase())) {
						it.remove();
					}
				}
			}
			return result;
		}

		// Empty values in params produces invalid SQL. 
		// -> we replace "" with null.
		Map<String, Object> vlpParams = new HashMap<String, Object>();
		if (vlp != null && vlp.getParameters() != null) {
			for (String key : vlp.getParameters().keySet()) {
				Object value = vlp.getParameters().get(key);
				if ("".equals(value)) {
					value = null;
				}
				vlpParams.put(key, value);
			}
		}

		mandatorUID = getMandatorForReferenceList(mandatorUID);

		List<? extends CollectableField> result = eoFacadeLocal.getReferenceList(
				efMeta,
				search,
				vlp == null ? null : vlp.getValue(),
				vlp == null ? null : vlpParams,
				null,
				lMaxRowCount,
				mandatorUID
		);

		return RigidUtils.uncheckedCast(result);
	}

	public <T> List<CollectableField> getValueList(FieldMeta<T> field, List<String> sSearch, FieldMeta<Long> referencingField, Long boId) {
		final EntityMeta<Long> entity = metaProvider.<Long>getEntity(field.getEntity());
		final boolean isStringField = field.getJavaClass().equals(String.class);

		final DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
		final DbQueryBuilder queryBuilder = dbAccess.getQueryBuilder();
		final DbQuery<DbTuple> q = queryBuilder.createTupleQuery();
		final DbFrom<Long> from = q.from(entity);
		final DbColumnExpression<T> column;
		final DbColumnExpression<T> columnLowerForGroupBy;
		if ((entity.isDynamic() || entity.isChart()) && (field.isDynamic())) {
			boolean bCaseSensitive = DbFrom.useCaseSensitiveConcerningUdGenericObject(entity.getDbTable(), field.getDbColumn());
			column = from.baseColumnSensitiveOrInsensitive(field, bCaseSensitive, false);
			columnLowerForGroupBy = (DbColumnExpression<T>) from.baseColumnSensitiveOrInsensitive(field, bCaseSensitive, false).alias("nuclosLowerColumnForGroupBy");
		} else {
			column = from.baseColumn(field);
			columnLowerForGroupBy = (DbColumnExpression<T>) from.baseColumn(field).alias("nuclosLowerColumnForGroupBy");
		}
		final List selections = new ArrayList();
		selections.add(column);
		if (isStringField) {
			/* Um Postgres Fehler zu verhindern...
			 * FEHLER: bei SELECT DISTINCT müssen ORDER-BY-Ausdrücke in der Select-Liste erscheinen
			 * SQL Status:42P10
			 * Zeichen:2034
			 */
			selections.add(queryBuilder.lower((DbExpression<String>) columnLowerForGroupBy));
		}
		final DbQuery<DbTuple> select = q.multiselect(selections).distinct(true);
		if (referencingField != null) {
			q.where(queryBuilder.equalValue(from.baseColumn(referencingField), boId));
		}
		if (sSearch != null && sSearch.size() > 0 && !looksEmpty(sSearch.get(0))) {
			final DbCondition condition;
			if (isStringField) {
				condition = queryBuilder.like((DbExpression<String>) column, sSearch.get(0).replace('*', '%'));
			} else {
				condition = queryBuilder.equalValue(column, field.getJavaClass().cast(sSearch.get(0)));
			}
			if (referencingField != null) {
				q.addToWhereAsAnd(condition);
			} else {
				q.where(condition);
			}
		}
		if (entity.isMandator() && getCurrentMandatorUID() != null) {
			q.addToWhereAsAnd(queryBuilder.equalValue(from.baseColumn(SF.MANDATOR_UID), getCurrentMandatorUID()));
		}

		q.groupBy(column);
		q.orderBy(queryBuilder.asc(column));
		q.limit(1000L);
		final List<CollectableField> result = dbAccess.executeQuery(q, new Transformer<DbTuple, CollectableField>(){
			@Override
			public CollectableField transform(DbTuple tuple) {
				return new CollectableValueField(tuple.get(0));
			}});

		return result;
	}

	public List<CollectableField> getVLPData(
			UID field,
			WebValueListProvider vlp,
			String pk,
			String quickSearchInput,
			Integer maxRowCount,
			boolean bDefault,
			UID mandatorUID
	) throws CommonBusinessException {

		Map<String, Object> queryParams = new HashMap<>();
		Map<String, Object> params = vlp.getParameters();

		String sNameFieldName = null;
		String sIdFieldName = null;
		String sDefaultMarkerFieldName = null;
		Object oSearchMode = null;

		UID sessionDataLanguageUID = RigidUtils.defaultIfNull(getCurrentDataLanguage(), getPrimaryDataLanguage());
		queryParams.put("locale", sessionDataLanguageUID);

		for (String key : params.keySet()) {
			Object sValue = params.get(key);

			if (key.equals(ValuelistProviderVO.DATASOURCE_NAMEFIELD)) {
				sNameFieldName = VLPUtils.getNameField(sValue, ValuelistProviderVO.DATASOURCE_NAMEFIELD, queryParams);

			} else if(key.equals(ValuelistProviderVO.DATASOURCE_DEFAULTMARKERFIELD)) {
				sDefaultMarkerFieldName = VLPUtils.getNameField(sValue, ValuelistProviderVO.DATASOURCE_DEFAULTMARKERFIELD, queryParams);

			} else if(key.equals(ValuelistProviderVO.DATASOURCE_IDFIELD)) {
				sIdFieldName = VLPUtils.getIdField(sValue, queryParams, vlp.getValue(), dsFacade);

			} else if (key.equals(ValuelistProviderVO.DATASOURCE_SEARCHMODE)) {
				oSearchMode = sValue;
				queryParams.put(key, sValue);

			} else if (sValue instanceof String && UID.isStringifiedUID((String)sValue)) {
				queryParams.put(key, UID.parseUID((String)sValue));

			} else {
				queryParams.put(key, sValue);

			}
		}

		final boolean bSearchMode = RigidUtils.parseBoolean(oSearchMode);

		if (ValuelistProviderVO.Type.NAMED.getType().equals(vlp.getType())) {
			// TODO
			EntityMeta<?> TODO = null;
			String vlpName = translateUid(TODO, vlp.getValue());
			UID entityUID = metaProvider.getEntityField(field).getEntity();
			if (ValueListProviderType.STATUS.getType().equals(vlpName)) {

				Collection<StateVO> states;
				if (params.get("processId") != null) {
					UID processId = translateFqn(E.PROCESS, (String)params.get("processId"));
					UsageCriteria uc = new UsageCriteria(entityUID, processId, null, null);
					UID stateModelId = stateFacadeLocal.getStateModelId(uc);
					states = stateFacadeLocal.getStatesByModel(stateModelId);
				} else {
					states = stateFacadeLocal.getStatesByModule(entityUID);
				}
				// find names of states that are used in multiple state models and are thus ambiguous, when displayed without the model name
				Set<String> ambiguousStateNames = getAmbiguousStateNames(states);
				List<CollectableField> result = new ArrayList<>();
				for (StateVO state : states) {
					String postfix = "";
					// if the state belongs to the ambiguous states, we initialize a non-empty postfix with the name of the state model
					if (ambiguousStateNames.contains(state.getStatename(getCurrentLocale()))) {
						Optional<StateModelVO> stateModel = stateFacadeLocal.getStateModels().stream()
								.filter(stateModelVO -> stateModelVO.getId().equals(state.getModelUID()))
								.findFirst();
						if (stateModel.isPresent()) {
							postfix += " - [" + stateModel.get().getName() + "]";
						}
					}
					result.add(
							new CollectableValueIdField(state.getId(), state.getNumeral() + " - " + state.getStatename(getCurrentLocale()) + postfix)
					);
				}

				Collections.sort(result);
				return result;

			} else if (ValueListProviderType.PROCESS.getType().equals(vlpName)) {

				List<CollectableField> result = getAllowedProcessesByEntity(getCurrentUserName(), entityUID, pk, bSearchMode);
				Collections.sort(result);
				return result;

			} else if (ValueListProviderType.MANDATOR.getType().equals(vlpName)) {

				UID levelUID = metaProvider.getEntity(entityUID).getMandatorLevel();

				List<CollectableField> result = new ArrayList<>();
				for (MandatorVO mandator : securityCache.getMandatorsByLevel(levelUID)) {
					result.add(new CollectableValueIdField(mandator.getUID(), mandator.toString()));
				}
				Collections.sort(result);
				return result;

			} else if (ValueListProviderType.USER_COMMUNICATION_ACCOUNT.getType().equals(vlpName)) {

				String requestContextClassName = "org.nuclos.api.context.communication." + params.get("RequestContext");
				UID userUID = null;
				if (params.get("selfId") != null) {
					userUID = (UID) params.get("selfId");
				}

				return securityFacade.getUserCommunicationAccounts(userUID, requestContextClassName);
			} else if (ValueListProviderType.SYSTEM_PARAMETER.getType().equals(vlpName)) {
				return CollectionUtils.transform(ParameterProvider.listSystemParameter(), CollectableValueField::new);
			} else if (ValueListProviderType.ALL_RECORD_GRANTS.getType().equals(vlpName)) {
				ProxyList<UID, EntityObjectVO<UID>> allRecordgrants = eoFacadeLocal.getEntityObjectProxyListNoCheckLocal(E.RECORDGRANT.getUID(), new CollectableSearchExpression(), Arrays.asList(E.RECORDGRANT.name.getUID()), null);

				List<CollectableField> result = new ArrayList<>();
				for (Iterator<EntityObjectVO<UID>> it = allRecordgrants.iterator(); it.hasNext();) {
					EntityObjectVO<UID> recordgrant = it.next();
					result.add(new CollectableValueIdField(recordgrant.getPrimaryKey(), recordgrant.getFieldValue(E.RECORDGRANT.name)));
				}
				Collections.sort(result);
				return result;
			} else if (ValueListProviderType.ENUM.getType().equals(vlpName)) {
				String enumClass = (String) vlp.getParameters().get(EnumCollectableFieldsProvider.SHOW_ENUM);
				if (enumClass != null) {
					EnumCollectableFieldsProvider clctFieldsProvider = new EnumCollectableFieldsProvider();
					clctFieldsProvider.setParameter(EnumCollectableFieldsProvider.SHOW_ENUM, enumClass);
					return clctFieldsProvider.getCollectableFields();
				}
			} else if (ValueListProviderType.CSV_IMPORT_STRUCTURES.getType().equals(vlpName)) {
				final List<CollectableField> result = new ArrayList<CollectableField>();
				String mode = queryParams.get("mode").toString();
				if (mode != null) {
					final List<MasterDataVO<?>> isList = CollectionUtils.unsaveConvertToList(
							masterDataFacade.getMasterDataWithCheckTransport(E.IMPORT.getUID(), null, true)
									.getTheCollection()
					);
					for (MasterDataVO<?> is : isList) {
						final Object oFieldValue = is.getFieldValue(E.IMPORT.mode.getUID());
						if (is.getFieldValue(E.IMPORT.mode.getUID()) != null &&
								mode.equals(oFieldValue)) {
							result.add(new CollectableValueIdField(is.getId(),
									is.getFieldValue(E.IMPORT.name.getUID())));
						}
					}
				}
				Collections.sort(result);
				return result;
			} else if (ValueListProviderType.SSOAUTH_OIDC_IDTOKEN_JWS_ALGORITHMS.getType().equals(vlpName)) {
				return new SsoAuthOidcIdTokenJwsAlgorithmCollectableFieldsProvider().getCollectableFields();
			}

			throw new UnsupportedOperationException("Named VLP " + vlp.getValue() + " not supported yet!");
		}

		mandatorUID = getMandatorForReferenceList(mandatorUID);

		List<? extends CollectableField> result = dsExtFacade.executeQueryForVLP(
				new VLPQuery(vlp.getValue())
						.setQueryParams(queryParams)
						.setIdFieldName(sIdFieldName)
						.setDefaultField(sDefaultMarkerFieldName)
						.setMandatorUID(mandatorUID)
						.setNameField(sNameFieldName)
						.setEntityFieldUID(field)
						.setQuickSearchInput(quickSearchInput)
						.setMaxRowCount(maxRowCount)
		);
		return RigidUtils.uncheckedCast(result);
	}

	private Set<String> getAmbiguousStateNames(Collection<StateVO> states) {
		Set<String> stateNames = new HashSet<>();
		Set<String> ambiguousStateNames = new HashSet<>();
		for (StateVO state : states) {
			String stateName = state.getStatename(getCurrentLocale());
			if (stateNames.contains(stateName)) {
				ambiguousStateNames.add(stateName);
			} else {
				stateNames.add(stateName);
			}
		}
		return ambiguousStateNames;
	}

	private UID getMandatorForReferenceList(UID mandatorFromClient) throws CommonPermissionException {
		if (mandatorFromClient != null) {
			securityCache.checkMandatorLoginPermission(getCurrentUserName(), mandatorFromClient);
			return mandatorFromClient;
		} else {
			return getCurrentMandatorUID();
		}
	}

	/**
	 *
	 * @param user String username
	 * @param entityUID UID entity
	 * @param spk String representation of primary key of the genericc object, e.g. spk = "4000392".
	 * 		 spk = "-1"/null means the allowed processes for a new bo are requested
	 * 		 spk = "0" means the check for write permission is only on module permissions, not for a specific generic object.
	 *
	 * @param bSearchMode
	 * @return List of allowed processed
	 */
	private List<CollectableField> getAllowedProcessesByEntity(String user, UID entityUID, String spk, final boolean bSearchMode) {
		List<CollectableValueIdField> lstProcesses = eoFacadeLocal.getProcessByEntity(entityUID, bSearchMode);

		Collection<UID> allowedProcesses = null;
		if (!Boolean.TRUE.equals(bSearchMode)) {

			ModulePermissions permissions = securityCache.getModulePermissions(user, getCurrentMandatorUID());


			Long pk = null;
			if (spk != null && !spk.isEmpty()) {
				try {
					pk = Long.parseLong(spk);
				} catch (NumberFormatException nfe) {
					LOG.debug(nfe.getMessage(), nfe);
				}
			}

			if (pk == null || pk < 0) {
				allowedProcesses = permissions.getNewAllowedProcessesByModuleUid().get(entityUID);

			} else {

				boolean bAllowedToUpdate = pk == 0 ? getEntityPermission(entityUID).isUpdateAllowed()
						: ModulePermission.includesWriting(permissions.getMaxPermissionForGenericObject(entityUID));

				if (!bAllowedToUpdate) {
					return Collections.EMPTY_LIST;
				}

				allowedProcesses = new HashSet<UID>();
				for (CollectableField process : lstProcesses) {
					allowedProcesses.add((UID) process.getValueId());
				}
			}

			if (allowedProcesses == null || allowedProcesses.isEmpty()) {
				return Collections.EMPTY_LIST;
			}
		}

		List<CollectableField> result = new ArrayList<>();

		for (CollectableField processField : lstProcesses) {
			if (allowedProcesses == null || allowedProcesses.contains(processField.getValueId())) {
			    result.add(new CollectableValueIdField(translateUid(E.PROCESS, (UID)processField.getValueId()), processField.getValue()));
			}
		}

		return result;
	}

	public boolean doesLayoutChangeWithProcess(Collection<UID> entities) {
		String user = getCurrentUserName();

		for (UID entityUID : entities) {

			Collection<StateVO> collStates = stateFacadeLocal.getStatesByModule(entityUID);

			//First check if there are layout changes for a new BO:
			List<CollectableField> lstProcessesNew = getAllowedProcessesByEntity(user, entityUID, null, true);
			if (doesLayoutChangeForThoseProcesses(lstProcessesNew, collStates, entityUID)) {
				return true;
			}

			//Second check if there are layout changed for an existing BO:
			List<CollectableField> lstProcessesExisting = getAllowedProcessesByEntity(user, entityUID, "0", true);
			if (doesLayoutChangeForThoseProcesses(lstProcessesExisting, collStates, entityUID)) {
				return true;
			}

		}

		return false;
	}

	private boolean doesLayoutChangeForThoseProcesses(List<CollectableField> lstProcesses, Collection<StateVO> collStates, UID entityUID) {
		String sCustom = getCustomUsage();
		Set<UID> layoutUIDs = new HashSet<UID>();

		for (StateVO state : collStates) {
			UID statusUID = state.getId();

			for (CollectableField cf : lstProcesses) {

				if (cf instanceof CollectableValueIdField) {
					String processFQN = (String) ((CollectableValueIdField)cf).getValueId();
					UID processUID = translateFqn(E.PROCESS, processFQN);

					UsageCriteria testUsage = new UsageCriteria(entityUID, processUID, statusUID, sCustom);
					layoutUIDs.add(layoutFacade.getDetailLayoutIDForUsage(testUsage));

					// consider also layouts without process
					testUsage = new UsageCriteria(entityUID, null, statusUID, sCustom);
					layoutUIDs.add(layoutFacade.getDetailLayoutIDForUsage(testUsage));

					if (layoutUIDs.size() >= 2) {
						return true;
					}

				}
			}
		}

		return false;
	}

	public List<DatasourceParameterVO> getParameterListForDataSource(UID entity) throws NuclosDatasourceException {
		return dssUtils.getParameters(entity, dsCache);
	}

	public List<DatasourceParameterVO> getParameterListForValueListProvider(UID vlp) throws NuclosDatasourceException, CommonFinderException, CommonPermissionException {
		return dssUtils.getParameters(Rest.facade().getValuelistProvider(vlp).getSource());
	}

	public String getParamRestMenuEntities() {
		return serverParameter.getValue(ParameterProvider.REST_MENU_ENTITIES);
	}

	public String getInitialEntityEntry() {
		String user = getCurrentUserName();
		String entries = serverParameter.getValue(ParameterProvider.INITIAL_ENTRY);
		if (!looksEmpty(entries)) {
			return Arrays.stream(entries.split(","))
					.map(entry -> entry.trim())
					.filter(entry -> entry.startsWith(user) && StringUtils.countMatches(entry, ":") == 1)
					.map(entry -> entry.replaceFirst(".*:\\s*", ""))
					.findFirst()
					.orElse(null);
		}

		return null;
	}

	@Transactional(rollbackFor= {Throwable.class})
	public <PK> EntityObjectVO<PK> insert(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		return doInsert(eoVO);
	}

	@Transactional(noRollbackFor = Exception.class)
	public <PK> EntityObjectVO<PK> insertWithoutRollback(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		return doInsert(eoVO);
	}

	private <PK> EntityObjectVO<PK> doInsert(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		convertNumbers(eoVO);
		String gpoId = eoVO.getFieldValue(RValueObject.TEMPORARY_ID_FIELD, String.class);
		remoteContext.setRemotly(true);
		EntityObjectVO<PK> result = eoFacade.insert(eoVO);
		if (gpoId != null) {
			evictTemporaryObject(gpoId);
		}
		return result;
	}

	@Transactional(rollbackFor= {Throwable.class})
	public <PK> EntityObjectVO<PK> update(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		return doUpdate(eoVO);
	}

	@Transactional(noRollbackFor = Exception.class)
	public <PK> EntityObjectVO<PK> updateWithoutRollback(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		return doUpdate(eoVO);
	}


	private <PK> EntityObjectVO<PK> doUpdate(EntityObjectVO<PK> eoVO) throws CommonBusinessException {
		return doUpdate(eoVO, true);
	}

	private <PK> EntityObjectVO<PK> doUpdate(EntityObjectVO<PK> eoVO, boolean remotely) throws CommonBusinessException {
		convertNumbers(eoVO);

		// handle state change
		UID stateChangedAction = eoVO.getFieldUid(SF.STATE_CHANGED_ACTION);
		UID stateChangedActionOnly = eoVO.getFieldUid(SF.STATE_CHANGED_ACTION_ONLY);
		boolean bStateChange = stateChangedAction != null;
		boolean bStateChangeOnly = stateChangedActionOnly != null;

		remoteContext.setRemotly(remotely);
		if (bStateChangeOnly) {
			GenericObjectWithDependantsVO gowdvo = stateFacadeRemote.changeStateByUser(eoVO.getDalEntity(), (Long) eoVO.getPrimaryKey(), stateChangedActionOnly, false, getCustomUsage());
			eoVO = (EntityObjectVO<PK>)DalSupportForGO.wrapGenericObjectVO(gowdvo);
		} else if (bStateChange) {
			GenericObjectWithDependantsVO gowdvo = DalSupportForGO.getGenericObjectWithDependantsVO((EntityObjectVO<Long>) eoVO);
			gowdvo = stateFacadeRemote.changeStateAndModifyByUser(eoVO.getDalEntity(), gowdvo, stateChangedAction, getCustomUsage());
			eoVO = (EntityObjectVO<PK>)DalSupportForGO.wrapGenericObjectVO(gowdvo);
        } else {
        	eoVO = eoFacade.update(eoVO, false);
        }

		Integer version = eoVO.isFlagUnchanged() ? getVersionSafe(eoVO.getDalEntity(), eoVO.getPrimaryKey()) : null;
		if (version != null && version.equals(eoVO.getVersion())) {
			return eoVO;
		}

		return eoFacade.get(eoVO.getDalEntity(), eoVO.getPrimaryKey());
	}

	private <PK> Integer getVersionSafe(UID entity, PK pk) throws CommonPermissionException {
		try {
			return eoFacade.getVersion(entity, pk);
		} catch (org.apache.commons.lang.NotImplementedException nie) {
			LOG.warn("::getVersionSafe {}", nie.getMessage());
		}
		return null;
	}

	@Transactional(rollbackFor= {Throwable.class})
	public <PK> void delete(UID entity, PK pk, boolean logicalDeletion) throws CommonBusinessException {
		doDelete(entity, pk, logicalDeletion);
	}

	@Transactional(noRollbackFor = Exception.class)
	public <PK> void deleteWithoutRollback(UID entity, PK pk, boolean logicalDeletion) throws CommonBusinessException {
		doDelete(entity, pk, logicalDeletion);
	}

	private <PK> void doDelete(UID entity, PK pk, boolean logicalDeletion) throws CommonBusinessException {
		remoteContext.setRemotly(true);
		eoFacade.delete(entity, pk, logicalDeletion, false);
	}

	@Override
	public <PK> EntityObjectVO<PK> clone(final EntityObjectVO<PK> eoVO, final UID layout, final boolean fetchFromDb, final Long parameterObjectId, final DependentSelection dependentSelection) throws CommonBusinessException {
		EntityMeta<PK> eMeta = metaProvider.getEntity(eoVO.getDalEntity());
		final GeneratorActionVO genActionVO = generatorFacade.getGeneratorActions().getGeneratorAction(eMeta.getCloneGenerator());
		GenerationContext<PK> context = new GenerationContextBuilder(Collections.singletonList(eoVO), genActionVO)
				.parameterObjectId(parameterObjectId)
				.customUsage(getCustomUsage())
				.cloning(true)
				.dependentSelection(dependentSelection)
				.build();
		EntityObjectVO<PK> clonedEo = eoFacade.clone(eoVO, layout, fetchFromDb, context);

		prepareNewTemporaryObject(clonedEo, getCurrentUserName());
		return clonedEo;
	}

	public Collection<StateVO> getSubsequentStates(UsageCriteria usagecriteria) throws CommonBusinessException {
		//remoteContext.setRemotly(true);
		// setRemotly not working on local interfaces!
		return stateFacadeLocal.getSubsequentStates(usagecriteria, false, usagecriteria.getStatusUID());
	}

	@Override
	public Collection<StateTransitionVO> getStateTransitions(final UsageCriteria usageCriteria) {
		if (usageCriteria.getStatusUID() == null) {
			return Collections.emptyList();
		}
		return stateFacadeLocal.findStateTransitionBySourceStateNonAutomatic(usageCriteria.getStatusUID());
	}

	@Override
	public UID getTargetStateId(final UID stateTransitionUID) throws CommonFinderException {
		return stateFacadeLocal.getTargetStateId(stateTransitionUID);
	}

	@Transactional(rollbackFor= {Throwable.class})
	public void changeState(UID entity, Long pk, UID newState) throws CommonBusinessException {
		doChangeState(entity, pk, newState);
	}

	@Transactional(noRollbackFor = Exception.class)
	public void changeStateWithoutRollback(UID entity, Long pk, UID newState) throws CommonBusinessException {
		doChangeState(entity, pk, newState);
	}

	private void doChangeState(UID entity, Long pk, UID newState) throws CommonBusinessException {
		remoteContext.setRemotly(true);
		stateFacadeRemote.changeStateByUser(entity, pk, newState, false, getCustomUsage());
	}

	public <PK> Collection<EntityObjectVO<PK>> getSubformData(UID subform, UID field, PK relatedPK, CollectableSearchExpression filterExpr, ResultParams resultParams) {
		remoteContext.setRemotly(true);
		this.extendFilterExpressionForSubformDataLoad(field, relatedPK, filterExpr);
		List<EntityObjectVO<PK>> eoVOs =
				nucletDalProvider.<PK>getEntityObjectProcessor(subform).getBySearchExprResultParams(filterExpr, resultParams);
		eoVOs.forEach(eoVO -> {
			eoVO.setShadowID(masterDataFacade.getShadowID(new TypePreservingObjectWrapper(eoVO.getPrimaryKey())));
		});
		return eoVOs;
	}

	public <PK> Long getSubformDataCount(UID subform, UID field, PK relatedPK, CollectableSearchExpression filterExpr) {
		remoteContext.setRemotly(true);
		this.extendFilterExpressionForSubformDataLoad(field, relatedPK, filterExpr);
		return nucletDalProvider.<PK>getEntityObjectProcessor(subform).count(filterExpr);
	}

	private static <PK> void extendFilterExpressionForSubformDataLoad(UID field, PK relatedPK, CollectableSearchExpression filterExpr) {
		final CompositeCollectableSearchCondition cond = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		final CollectableSearchCondition cond1 = SearchConditionUtils.newPkComparison(field, ComparisonOperator.EQUAL, relatedPK);
		cond.addOperand(cond1);

		if (filterExpr.getSearchCondition() != null) {
			cond.addOperand(filterExpr.getSearchCondition());
		}
		filterExpr.setSearchCondition(cond);
	}

	public <PK> Collection<EntityObjectVO<PK>> getDependentData(UID subform, UID field, PK relatedPK) {
		return eoFacadeLocal.getDependentEntityObjectsNoCheck(subform, field, relatedPK);
	}

	public List<LuceneSearchResult> luceneIndexSearch(String searchString) {
		remoteContext.setRemotly(true);
		return liveSearchFacade.nuclosIndexSearch(searchString);
	}

	public String getFieldGroupName(UID groupID) {
		if (groupID == null) {
			return null;
		}
		return eoFacadeLocal.getFieldGroupName(groupID);
	}

	public String getDbVersion() {
		AutoDbSetup autoSetup = new AutoDbSetup(dataBaseHelper.getDbAccess(), E.getThis(), ApplicationProperties.getInstance().getNuclosVersion());
		Pair<String, Date> res = autoSetup.determineLastVersion();
		if (res == null) {
			throw new CommonFatalException("Could not determine last DB Version");
		}
		return res.getX();
	}

	public boolean isActionAllowed(String action) {
		return securityCache.getAllowedActions(getCurrentUserName(), getCurrentMandatorUID()).contains(action);
	}

	@Override
	public Collection<UID> getAllowedStateTransitions() {
		return securityCache.getTransitionUids(getCurrentUserName(), getCurrentMandatorUID());
	}

	@Override
	public List<EventSupportSourceVO> getCustomRules(UsageCriteria uc) {
		return esFacade.findEventSupportsByUsageAndEntity(CustomRule.class.getCanonicalName(), uc);
	}

	@Transactional(rollbackFor= {Throwable.class})
	public <PK> EntityObjectVO<PK> fireCustomEventSupport(EntityObjectVO<PK> eoVO, DependentSelection dependentSelection, String rule) throws NuclosCompileException, CommonBusinessException {
		return doFireCustomEventSupport(eoVO, dependentSelection, rule, true);
	}

	@Transactional(noRollbackFor = Exception.class)
	public <PK> EntityObjectVO<PK> fireCustomEventSupportWithoutRollback(EntityObjectVO<PK> eoVO, DependentSelection dependentSelection, String rule) throws NuclosCompileException, CommonBusinessException {
		return doFireCustomEventSupport(eoVO, dependentSelection, rule, false);
	}

	private <PK> EntityObjectVO<PK> doFireCustomEventSupport(EntityObjectVO<PK> eoVO, DependentSelection dependentSelection, String rule, boolean checkPermissions) throws NuclosCompileException, CommonBusinessException {
		convertNumbers(eoVO);

		EventSupportSourceVO eseVO = esCache.getEventSupportSourceByClassname(rule);
		if (eseVO == null) {
			throw new CommonFinderException("There is no such custom rule '" + rule + "'");
		}

		// NUCLOS-5995 Use this instead of esFacade, automatically solves NUCLOS-5994
		EntityObjectVO<PK> eo = eoFacade.executeBusinessRules(Collections.singletonList(eseVO), eoVO, dependentSelection, getCustomUsage(), false);

		// TODO: This is done to ensure the same behavior as the rich client. Check if this is correct
		if (eo.getPrimaryKey() != null) {
			eo = get(eo.getDalEntity(), eo.getPrimaryKey(), false);
		}

		return eo;
	}

	public Collection<UID> getEntitiesAssignedToLayoutId(UID layoutUID) throws CommonBusinessException {
		if (layoutUID == null) {
			return Collections.EMPTY_SET;
		}

		return layoutFacade.getEntitiesAssignedToLayoutId(layoutUID);
	}

	public String getLayoutML(UID pk) throws CommonBusinessException {
		if(pk == null) {
			return null;
		}
		return layoutFacade.getLayoutML(pk);
	}

	public LayoutInfo getLayoutInfo(UID layoutUID) throws CommonPermissionException {
		EntityObjectVO eo = eoFacade.get(E.LAYOUT.getUID(), layoutUID);
		return new LayoutInfo(eo);
	}

	public Set<UID> getLayoutUIDsForEntity(UID entityUID) {
		return layoutFacade.getAllLayoutUidsForEntity(entityUID);
	}

	/**
	 * Fetches thin Layout EOs and wraps them in LayoutInfo objects.
	 *
	 * @param entityUID
	 * @return
	 * @throws CommonBusinessException
	 */
	public List<LayoutInfo> getLayoutInfosForEntity(UID entityUID) {
		final List<LayoutInfo> result = new ArrayList<>();

		final ArrayList<UID> layoutUIDs = new ArrayList<>();
		layoutUIDs.addAll(layoutFacade.getAllLayoutUidsForEntity(entityUID));

		Collection<UID> fields = Arrays.<UID>asList(
				E.LAYOUT.name.getUID(),
				E.LAYOUT.description.getUID(),
				E.LAYOUT.nuclet.getUID()
		);
		ResultParams resultParams = new ResultParams(fields, 0L, 1000L, true);
		final Collection<EntityObjectVO<UID>> data = eoFacadeLocal.getEntityObjectsChunkNoCheck(
				E.LAYOUT.getUID(),
				new CollectableSearchExpression(
						SearchConditionUtils.newPkInCondition(
								E.LAYOUT,
								layoutUIDs
						)
				),
				resultParams,
				null
		);

		for (EntityObjectVO<UID> eo: data) {
			final LayoutInfo layoutInfo = new LayoutInfo(eo);

			result.add(layoutInfo);
		}

		return result;
	}

	public boolean isLayoutRestrictionsMustIgnoreGroovyRules(UID pk) {
		if(pk == null) {
			return false;
		}
		try {
			UID nucletUID = layoutFacade.getNucletUID(pk);
			String ignoreGroovyRules = getNucletParameterByName(nucletUID, NuclosParameterProvider.WEBCLIENT_RESTRICTIONS_MUST_IGNORE_GROOVY_RULES);
			if ("true".equals(ignoreGroovyRules)) {
				return true;
			}
			return false;
		} catch (CommonBusinessException e) {
			return false;
		}
	}

	public UID getDetailLayoutIDForUsage(UsageCriteria usage, boolean bSearchLayout) {
		return layoutFacade.getDetailLayoutIDForUsage(usage, bSearchLayout);
	}

	public <PK> UsageCriteria getUsageCriteriaForPK(PK pk, UID entity) throws CommonBusinessException {
		return eoFacadeLocal.getUsageCriteriaForPK(pk, entity, getCustomUsage());
	}

	@Transactional(noRollbackFor= {Exception.class})
	public BoDataSet importOrExportDatabase(InputStream is)  throws CommonPermissionException {
		return transferFacade.importOrExportDatabase(is);
	}

	private <PK> void convertNumbers(EntityObjectVO<PK> eo) {
		Map<UID,Object> mapValues = eo.getFieldValues();
		for (UID field : mapValues.keySet()) {

			Object value = mapValues.get(field);
			if (value == null) continue;
			if (value instanceof Long) {
				Long lValue = (Long)value;
				FieldMeta<?> fm = metaProvider.getEntityField(field);
				if (fm.getDataType().equals("java.lang.Integer")) {
					eo.setFieldValue(field, lValue.intValue());
				}
			} else if (value instanceof BigDecimal) {
				BigDecimal bdValue = (BigDecimal) value;
				FieldMeta<?> fm = metaProvider.getEntityField(field);
				if (fm.getDataType().equals("java.lang.Double")) {
					eo.setFieldValue(field, bdValue.doubleValue());
				}
			}
		}
		IDependentDataMap map = eo.getDependents();
		Collection<List<EntityObjectVO<?>>> depeos = map.getRoDataMap().values();
		for (List<EntityObjectVO<?>> lstDepeo : depeos) {
			for (EntityObjectVO<?> depeo : lstDepeo) {
				convertNumbers(depeo);
			}
		}
	}

	//TODO: This here is the place where a special CustomUsage for RestService and thus Webclient could be defined. So the user has the
	//chance to define a special WebLayout-Usage.
	public String getCustomUsage() {
		final String sCustomKey = serverParameter.getValue(ServerParameterProvider.KEY_LAYOUT_CUSTOM_KEY);
		return (sCustomKey != null ? sCustomKey + ":" : "") + NuclosConstants.WEB_LAYOUT_CUSTOM_USAGE;
	}

	final Map<String, AbstractTreeNode<?>> PROTOTYPE_CACHE = new HashMap<String, AbstractTreeNode<?>>();

	/**
	 * Explanation why the method has a TODO_
	 * The PROTOTYPE_CACHE in its current form should be removed.
	 * The problem is, that some treenodes can not be queried directly via the TreeNodeFacadeBean.
	 * They can only be accessed via their parents getSubNodes() method. These nodes need to be cached to be accessible via some identifier.
	 * Unfortunately the AbstractTreeNode class does not have enough attributes for a unique identifier.
	 * This is the reason, why it was decided, that treenodes are cached at a random, at runtime generated UIDs.
	 * The obvious disadvantage is, that this random key, has no meaning after a server restart.
	 *
	 * In an ideal setup each AbstractTreeNode would have a unique, persistent identifier
	 * and the TreeNodeFacadeBean would have one method to query TreeNodes of all types via this identifier without the need to cache.
	 */
	private void buildNextLevel(AbstractTreeNode treeNode, JsonArrayBuilder result) {
		List<AbstractTreeNode> subnodes = treeNode.getSubNodes();
		if(subnodes != null) {
			for(AbstractTreeNode subnode : subnodes) {

				UID newOrCachedKey = new UID();
				// TODO
				EntityMeta<?> TODO = null;
				if (PROTOTYPE_CACHE.containsValue(subnode)) {
					String cachedKey = PROTOTYPE_CACHE.entrySet()
							.stream()
							.filter(entry -> entry.getValue().equals(subnode))
							.map(entry -> entry.getKey())
							.findFirst()
							.orElse(null);
					if (cachedKey != null) {
						newOrCachedKey = new UID(cachedKey);
					}
				}
				PROTOTYPE_CACHE.put(translateUid(TODO, newOrCachedKey), subnode);

				JsonObjectBuilder json = wrapTreeNode(subnode, TODO, newOrCachedKey);

				result.add(json);
			}
		}
	}

	private JsonObjectBuilder wrapTreeNode(AbstractTreeNode treeNode, EntityMeta<?> TODO, UID cacheKey) {
		UID entityUid = treeNode.getEntityUID();
		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("node_id", translateUid(TODO, cacheKey));
		if (treeNode.getEntityUID() != null) {
			EntityMeta meta = this.metaProvider.getEntity(treeNode.getEntityUID());
			if (meta.getDetailEntity() != null) {
				meta = this.metaProvider.getEntity(meta.getDetailEntity());
				entityUid = meta.getUID();
			}
			String treeNodeEntityFqn = translateUid(meta, entityUid);
			json.add("boMetaId", treeNodeEntityFqn);
		}
		json.add("boId", treeNode.getId().toString());
		json.add("title", treeNode.getLabel());

		if (treeNode instanceof AbstractStaticTreeNode || treeNode instanceof SimpleTreeNode) {
			json.add("static", "true");
		} else if (treeNode.getEntityUID() != null && layoutFacade.getAllLayoutUidsForEntity(entityUid).isEmpty()) {
			json.add("static", "true");
		} else {
			json.add("static", "false");
		}

		if (treeNode instanceof AbstractTreeNodeConfigured) {
			json.add("leaf", !((AbstractTreeNodeConfigured) treeNode).allowsChildren());
			if (treeNode instanceof GenericObjectTreeNode) {
				if (((GenericObjectTreeNode) treeNode).getRelationId() != null) {
					json.add("relationId", ((GenericObjectTreeNode) treeNode).getRelationId());
				}
				if (getRelationIconPath((GenericObjectTreeNode) treeNode) != null) {
					json.add("relationIcon", getRelationIconPath((GenericObjectTreeNode) treeNode));
				}
				if (getRelationName((GenericObjectTreeNode) treeNode) != null) {
					json.add("relationName", getRelationName((GenericObjectTreeNode) treeNode));
				}
				if (((GenericObjectTreeNode) treeNode).getRelationType() != null) {
					json.add("relationType", ((GenericObjectTreeNode) treeNode).getRelationType().toString());
				}
				if (((GenericObjectTreeNode) treeNode).getRelationDirection() != null) {
					json.add("relationDirection", ((GenericObjectTreeNode) treeNode).getRelationDirection().toString());
				}
			}
		} else if (treeNode instanceof RelationTreeNode) {
			if (((RelationTreeNode) treeNode).getDirection() != null) {
				json.add("relationIcon", ((RelationTreeNode) treeNode).getDirection().isForward() ? "tree-parent-to-child" : "tree-child-to-parent");
			}
		}

		try {
			if(treeNode.getEntityUID() != null) {
				EntityMeta<UID> em = metaProvider.getEntity(entityUid);
				if (em.getNuclosResource() != null) {
					json.add("icon", em.getNuclosResource());
				}
				json.add("boMetaId", fqnCache.translateUid(E.ENTITY, em.getUID()));
			}
		} catch(Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return json;
	}

	public String getRelationIconPath(GenericObjectTreeNode treeNode) {
		return treeNode.isRelated() ? getRelatedNodeIcon(treeNode.getRelationType(), treeNode.getRelationDirection()) : null;
	}

	public String getRelationName(GenericObjectTreeNode treeNode) {
		if (!treeNode.isRelated()) {
			return null;
		}
		switch (treeNode.getRelationType()) {
			case PREDECESSOR_OF:
				return treeNode.getRelationDirection().isForward()
						? SpringLocaleDelegate.getInstance().getMessage("RelateGenericObjectsPanel.6", "Nachfolger")
						: SpringLocaleDelegate.getInstance().getMessage("RelateGenericObjectsPanel.5", "Vorg\u00e4nger");
			case PART_OF:
				return treeNode.getRelationDirection().isForward()
						? SpringLocaleDelegate.getInstance().getMessage("RelateGenericObjectsPanel.8", "Zusammengesetztes Objekt")
						: SpringLocaleDelegate.getInstance().getMessage("RelateGenericObjectsPanel.7", "Teilobjekt(e)");
			default: return null;
		}
	}

	private static String getRelatedNodeIcon(GenericObjectTreeNode.SystemRelationType relationtype, GenericObjectTreeNode.RelationDirection direction) {
		final String result;
		if (relationtype == null) {
			result = null;
		}
		else {
			switch (relationtype) {
				case PREDECESSOR_OF:
					result = direction.isForward() ? "tree-parent-to-child" : "tree-child-to-parent";
					break;
				case PART_OF:
					result = direction.isForward() ? "part-of" : "composite-of";
					break;
				default:
					result = null;
			}
		}
		return result;
	}


	public JsonArrayBuilder appendExpandedSubnotes(SimpleTreeNode tree, int deep){
        JsonArrayBuilder result = Json.createArrayBuilder();

        List<SimpleTreeNode> subnodes = tree.getSubNodes();
        if(subnodes != null) {
            deep++;
            for (SimpleTreeNode subnode : subnodes) {
                JsonObjectBuilder json = Json.createObjectBuilder();
                json.add("node_id", translateUid(null, new UID()));
                json.add("boId", subnode.getId().toString());
                json.add("title", subnode.getLabel());
                EntityMeta<UID> em = metaProvider.getEntity(subnode.getEntityUID());
                json.add("icon", em.getNuclosResource());
                try {
                    if(subnode.getEntityUID() != null) {
                        json.add("boMetaId", fqnCache.translateUid(E.ENTITY, subnode.getEntityUID()));
                    }
                } catch(Exception e) {
					LOG.error(e.getMessage(), e);
                }
                json.add("subNodes", appendExpandedSubnotes(subnode, deep));
                result.add(json);
            }
        }
        return result;
    }

	public JsonArrayBuilder getTreeWithSearch(String boMetaId, String boId, String search) throws CommonBusinessException {
		remoteContext.setRemotly(true);

	    DefaultMasterDataTreeNodeParameters params = new DefaultMasterDataTreeNodeParameters(Rest.translateFqn(E.ENTITY, boMetaId), new Long(boId), null, null,  "label", "description");
        DefaultMasterDataTreeNode treeNode = new DefaultMasterDataTreeNode<UID>(params);

        SimpleTreeNode expandedTreeNode = treeNodeFacadeRemote.getExpandedTreeWithSearch(treeNode, search);

	    return appendExpandedSubnotes(expandedTreeNode,0);
	}




    public JsonArrayBuilder getSubtree(String nodeId) {
		remoteContext.setRemotly(true);

		AbstractTreeNode<?> treeNode = PROTOTYPE_CACHE.get(nodeId);

		try {
			if (treeNode != null) {
				JsonArrayBuilder array = Json.createArrayBuilder();
				buildNextLevel(treeNode, array);
				return array;
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	public JsonArrayBuilder getTree(String boMetaId, String boId) {
		remoteContext.setRemotly(true);
		try {
			AbstractTreeNode treeNode;
			EntityMeta<?> eMeta = metaProvider.getAllEntities().stream()
					.filter(meta -> boMetaId.equals(fqnCache.translateUid(E.ENTITY, meta.getUID())))
					.findFirst().orElse(null);
			if (!eMeta.isStateModel() || (treeNode = treeNodeFacadeRemote.getGenericObjectTreeNode(Long.parseLong(boId), eMeta.getUID())) == null) {
				treeNode = treeNodeFacadeRemote.getMasterDataTreeNode(new Long(boId), eMeta.getUID(), null, false);
			}

			JsonArrayBuilder array = Json.createArrayBuilder();
			buildNextLevel(treeNode, array);
			return array;

		} catch (Exception e) {
			throw NuclosWebException.wrap(e);
		}
	}

	public JsonObjectBuilder getTreeRoot(String boMetaId, String boId) {
		remoteContext.setRemotly(true);
		try {
			AbstractTreeNode treeNode;
			EntityMeta<?> eMeta = metaProvider.getAllEntities().stream()
					.filter(meta -> boMetaId.equals(fqnCache.translateUid(E.ENTITY, meta.getUID())))
					.findFirst().orElse(null);
			if (!eMeta.isStateModel() || (treeNode = treeNodeFacadeRemote.getGenericObjectTreeNode(Long.parseLong(boId), eMeta.getUID())) == null) {
				treeNode = treeNodeFacadeRemote.getMasterDataTreeNode(new Long(boId), eMeta.getUID(), null, false);
			}
			if (treeNodeFacadeRemote.isTraceParents(treeNode)) {
				treeNode = treeNodeFacadeRemote.getRootNode(treeNode);
			}
			UID newOrCachedKey = new UID();
			if (PROTOTYPE_CACHE.containsValue(treeNode)) {
				final AbstractTreeNode finalTreeNode = treeNode;
				String cachedKey = PROTOTYPE_CACHE.entrySet()
						.stream()
						.filter(entry -> finalTreeNode.equals(entry.getValue()))
						.map(entry -> entry.getKey())
						.findFirst()
						.orElse(null);
				if (cachedKey != null) {
					newOrCachedKey = new UID(cachedKey);
				}
			}
			PROTOTYPE_CACHE.put(translateUid(null, newOrCachedKey), treeNode);

			return wrapTreeNode(treeNode, null, newOrCachedKey);

		} catch (Exception e) {
			throw NuclosWebException.wrap(e);
		}
	}

	public String translateUid(EntityMeta<?> eMeta, UID uid) {
		return fqnCache.translateUid(eMeta, uid);
	}

	public UID translateFqn(EntityMeta<?> eMeta, String fqn) {
		return fqnCache.translateFqn(eMeta, fqn);
	}

	public boolean hasPrintouts(UsageCriteria usagecriteria, Object boId) {
		Collection<DefaultReportVO> findReportsByUsage = reportFacade.findReportsByUsage(usagecriteria);
		if (findReportsByUsage == null || findReportsByUsage.isEmpty()) {
			return false;
		}
		return true;
	}

	public String getNucletParameterByName(UID nucletUID, String parameter) {
		return nuclosParameterProvider.getNucletParameterByName(nucletUID, parameter);
	}

	public <PK> EntityObjectVO<PK> getBoWithDefaultValues(UID entityUID) {
		EntityObjectVO<PK> eo = new EntityObjectVO<>(entityUID);
		eoFacadeLocal.setDefaultValues(eo);
		return eo;
	}

	@Transactional(rollbackFor= {Throwable.class})
	public <PK> BoGenerationResult generateBo(
			UID entityUID,
			Map<PK, Integer> eoIdsAndVersions,
			final DependentSelection dependentSelection, UID generatorUID,
			final Long parameterObjectId) throws CommonBusinessException {
		return doGenerateBo(entityUID, eoIdsAndVersions, dependentSelection, generatorUID, parameterObjectId);
	}

	@Transactional(noRollbackFor = Exception.class)
	public BoGenerationResult generateBoWithoutRollback(
			UID entityUID,
			Map<Long, Integer> eoIdsAndVersions,
			final DependentSelection dependentSelection, UID generatorUID
	) throws CommonBusinessException {
		return doGenerateBo(entityUID, eoIdsAndVersions, dependentSelection, generatorUID, null);
	}

	@Transactional(rollbackFor= {Throwable.class})
	@Override
	public <PK> Map<String, Collection<EntityObjectVO<PK>>> groupObjectsForGeneration(final Collection<PK> ids, final GeneratorActionVO generation) throws CommonPermissionException {
		remoteContext.setRemotly(true);
		return generatorFacade.groupObjects(ids, generation).getResult();
	}

	public boolean hasGroupingAttributes(UID generatoractionUid) {
		return !generatorFacade.getGroupingAttributes(generatoractionUid).isEmpty();
	}

	private <PK> BoGenerationResult doGenerateBo(
			UID entityUID,
			Map<PK, Integer> boIdsToVersion,
			final DependentSelection dependentSelection, UID generatorUID,
			final Long parameterObjectId) throws CommonBusinessException {
		//TODO NOAINT-629: parameterObjectId must no be null for Generations with ParameterEntity.
		remoteContext.setRemotly(true);
		final boolean isCloningAction = false;

		GeneratorActionVO generatorAction = genCache.getGenerationAction(generatorUID);
		EntityObjectVO<PK> eoTarget;
		try {
			List<EntityObjectVO<PK>> eoSources = new ArrayList<>();
			for (PK boId : boIdsToVersion.keySet()) {
				Integer expectedVersion = boIdsToVersion.get(boId);
				EntityObjectVO<PK> eo = eoFacade.get(entityUID, boId);
				if (eo.getVersion() > expectedVersion) {
					throw new CommonStaleVersionException(eo, expectedVersion);
				}
				eoSources.add(eo);
			}

			GenerationContext<PK> context = new GenerationContextBuilder(eoSources, generatorAction)
					.parameterObjectId(parameterObjectId)
					.customUsage(getCustomUsage())
					.cloning(isCloningAction)
					.dependentSelection(dependentSelection)
					.build();
			GenerationResult<PK> genResult = generatorFacade.generateGenericObjects(context);

			eoTarget = genResult.getGeneratedObject();

			if (generatorAction.isDoNotSave()) {
				prepareNewTemporaryObject(eoTarget, getCurrentUserName());
			}
			
			return new BoGenerationResult(generatorAction, eoTarget, null);

		} catch (GeneratorFailedException gfex) {
			// NUCLOS-6214
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			eoTarget = gfex.getGenerationResult().getGeneratedObject();

			String error = null;
			if (gfex.getCause() instanceof BusinessException) {
				error = gfex.getGenerationResult().getError();
			}

			if (error == null) {
				CommonValidationException cve = NuclosExceptions.getCause(gfex, CommonValidationException.class);
				if (cve != null) {
					//TODO: Using Rest.java at this point is bad coding because of cycles.
					error = Rest.getFullMessage(cve, entityUID);
					if (cve.getClass() != CommonValidationException.class) {
						if (cve.getCause() instanceof NuclosBusinessRuleException) {
							error = (cve.getCause()).getLocalizedMessage();

						} else if (cve.getCause() instanceof CommonValidationException) {
							error = Rest.getFullMessage((CommonValidationException)cve.getCause(), entityUID);

						} else if (cve.getCause() != null && cve.getCause().getCause() instanceof CommonValidationException) {
							error = ((CommonValidationException)cve.getCause().getCause()).getFullErrorMessage();

						}
					}
				}
			}

			eoTarget.setBusinessError(error);
			prepareNewTemporaryObject(eoTarget, getCurrentUserName());


			if (generatorAction.isCloseOnException()) {
				throw gfex;
			}
			
			return new BoGenerationResult(generatorAction, eoTarget, error);
		}
	}

	@CachePut(value="restTemporaryObjects", key="#p0")
	public <PK> EntityObjectVO<PK> cacheTemporaryObject(String temporaryId, EntityObjectVO<PK> gpo) {
		return gpo;
	}

	@Cacheable(value="restTemporaryObjects", key="#p0")
	public <PK> EntityObjectVO<PK> getTemporaryObject(PK temporaryId) throws CommonBusinessException {
		throw new NuclosBusinessException("Temporary object ID " + temporaryId + " does not exist any more. Try again.");
	}

	@CacheEvict(value="restTemporaryObjects", key="#p0")
	public void evictTemporaryObject(String temporaryId) {
	}

	public boolean isTemporaryId(String id) {
		return id != null && id.startsWith(TEMP);
	}

	@Override
	public String getTempId(String id) {
		return id.substring(TEMP.length());
	}

	private String prepareNewTemporaryObject(EntityObjectVO<?> eovo, String user) {
		String id = TEMP+getNextId();
		eovo.flagNew();
		eovo.setCreatedBy(user);
		eovo.setFieldValue(RValueObject.TEMPORARY_ID_FIELD, id);
		IDependentDataMap dependents = eovo.getDependents();
		if (dependents != null) {
			for (IDependentKey dependentKey : dependents.getKeySet()) {
				for (EntityObjectVO<?> dep : dependents.getData(dependentKey)) {
					prepareNewTemporaryObject(dep, user);
				}
			}
		} else {
			dependents = new DependentDataMap();
			eovo.setDependents(dependents);
		}
		eovo.setFieldValue(RValueObject.TEMPORARY_DEPENDENTS_ALREADY_LOADED_FIELD, new HashSet<IDependentKey>());

		cacheTemporaryObject(id, eovo);

		return id;
	}

	private Long getNextId() {
		return dataBaseHelper.getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE);
	}

	@Override
	public GeneratorActionVO getGeneration(UID generationUID) {
		return genCache.getGenerationAction(generationUID);
	}

	@Override
	public List<GeneratorActionVO> getGenerations(UsageCriteria usage, UID mandatorUID) {
//		UID mandator = getCurrentMandatorUID();
		List<GeneratorActionVO> generatorActions = new ArrayList<GeneratorActionVO>(genCache.getGeneratorActions());
		if (generatorActions == null) {
			return null;
		}

//		Collection<UID> assignedGenerations = SecurityCache.getInstance().getAssignedGenerations(getCurrentUserName(), mandator);
//		if (assignedGenerations == null) {
//			return null;
//		}
//
//		GeneratorVO generator = new GeneratorVO(generatorActions.stream()
//				.filter(ga -> assignedGenerations.contains(ga.getId()))
//				.collect(Collectors.toList()));
		generatorActions = generatorFacade.getGeneratorActions().getGeneratorActions(
				usage.getEntityUID(),
				usage.getStatusUID(),
				usage.getProcessUID(),
				mandatorUID);

		if (generatorActions.isEmpty()) {
			return null;
		}

		return generatorActions;
	}

	public void updateInputContext(Map<String, Serializable> context) {
		SpringInputContext inputContext = SpringApplicationContextHolder.getBean(SpringInputContext.class);
		inputContext.setSupported(true);
		inputContext.clear();
		if (context != null) {
			inputContext.set(context);
		}
	}

	public void clearInputContext() {
		SpringInputContext inputContext = SpringApplicationContextHolder.getBean(SpringInputContext.class);
		inputContext.setSupported(false);
		inputContext.clear();
	}

	public List<EventSupportSourceVO> findEventSupportsByUsageAndEntity(String sEventclass, UsageCriteria usagecriteria) {
		return esFacade.findEventSupportsByUsageAndEntity(sEventclass, usagecriteria);
	}

	@Override
	public ValuelistProviderVO getValuelistProvider(UID vlpUID) throws CommonFinderException, CommonPermissionException {
		return dsFacade.getValuelistProvider(vlpUID);
	}

	@Override
	public DynamicTasklistVO getDynamicTasklistDatasource(UID dynamicTaskListUID) {
		if (dynamicTaskListUID == null) {
			return null;
		}
		return dsCache.getDynamicTasklist(dynamicTaskListUID);
	}

	@Override
	public Map<String, Object> getDatasourceParameters(String dsXML, Map<String, String> datasourceParams) throws CommonBusinessException, ClassNotFoundException {
		Map<String, Object> datasourceParamsWithObjectValues = new HashMap<>();

		final List<DatasourceParameterVO> lstParameters = dsFacade.getParametersFromXML(dsXML);
		for (DatasourceParameterVO dspvo: lstParameters) {
			final String datatype = dspvo.getDatatype();
			final String paramName = dspvo.getParameter();
			if (datasourceParams.containsKey(paramName)) {
				final String sValue = datasourceParams.get(paramName);
				if (String.class.getCanonicalName().equals(datatype)) {
					datasourceParamsWithObjectValues.put(paramName, sValue);
				} else if (Boolean.class.getCanonicalName().equals(datatype)) {
					datasourceParamsWithObjectValues.put(paramName, sValue == null ? null : RigidUtils.parseBoolean(sValue));
				} else {
					if (!RigidUtils.looksEmpty(sValue)) {
						if (Integer.class.getCanonicalName().equals(datatype)) {
							datasourceParamsWithObjectValues.put(paramName, Integer.parseInt(sValue));
						} else if (Double.class.getCanonicalName().equals(datatype)) {
							datasourceParamsWithObjectValues.put(paramName, Double.parseDouble(sValue));
						} else if (Long.class.getCanonicalName().equals(datatype)) {
							datasourceParamsWithObjectValues.put(paramName, Long.parseLong(sValue));
						} else if (BigDecimal.class.getCanonicalName().equals(datatype)) {
							datasourceParamsWithObjectValues.put(paramName, BigDecimal.valueOf(Double.parseDouble(sValue)));
						} else if (Date.class.getCanonicalName().equals(datatype)) {
							// ISO-format 'yyyy-mm-dd', matches JDBC escape syntax
							try {
								Date date = new SimpleDateFormat("yyyy-MM-dd").parse(sValue);
								datasourceParamsWithObjectValues.put(paramName, date);
							} catch (ParseException e) {
								throw new NuclosFatalException("Date value " + sValue + " is not parseable with yyyy-MM-dd: " + e.getMessage());
							}
						} else if (InternalTimestamp.class.getCanonicalName().equals(datatype)) {
							try {
								// TODO: Use ISO-8601 format
								Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sValue);
								datasourceParamsWithObjectValues.put(paramName, date);
							} catch (ParseException e) {
								throw new NuclosFatalException("InternalTimestamp value " + sValue + " is not parseable with yyyy-MM-dd HH:mm:ss: " + e.getMessage());
							}
						} else {
							datasourceParamsWithObjectValues.put(paramName, CollectableFieldFormat.getInstance(
									Class.forName(datatype))
									.parse(null, sValue));
						}
					} else { // sValue == null
						datasourceParamsWithObjectValues.put(paramName, null);
					}
				}
			} else { // missing parameter
				datasourceParamsWithObjectValues.put(paramName, null);
			}

			// fill in mandatory values...
			if (datasourceParamsWithObjectValues.get(paramName) == null) {
				if (Integer.class.getCanonicalName().equals(datatype)) {
					// NUCLOS-5010 (VLPs erzeugen unterschiedliche SQLs für Webclient, RichclientComboBox und RichClientLOV)
					// Abwärtskompatibilität: Integer Parameter müssen wenn leer mit 0 befüllt sein. Das stellen wir hier sicher...
					datasourceParamsWithObjectValues.put(paramName, 0);
				} else if (Boolean.class.getCanonicalName().equals(datatype)) {
					if ("searchmode".equals(paramName)) {
						datasourceParamsWithObjectValues.put(paramName, Boolean.FALSE);
					}
				}
			}
		}

		return datasourceParamsWithObjectValues;
	}

	@Override
	public JsonArray executeDatasource(UID datasourceUID, Map<String, String> datasourceParams, Integer iMaxRowCount) throws CommonBusinessException, ClassNotFoundException {
		final DatasourceVO datasource = dsFacade.get(datasourceUID);
		Map<String, Object> datasourceParamsWithObjectValues = getDatasourceParameters(datasource.getSource(), datasourceParams);

		final ResultVO resultVO = dsFacade.executeQuery(datasourceUID, new DatasourceQueryExecutionParameters(datasourceParamsWithObjectValues, null, iMaxRowCount, null), getCurrentMandatorUID());
		final JsonArrayBuilder resultJson = Json.createArrayBuilder();
		final List<ResultColumnVO> columns = resultVO.getColumns();
		for (Object[] row : resultVO.getRows()) {
			final JsonObjectBuilder jsonObject = Json.createObjectBuilder();
			for (int i = 0; i < row.length; i++) {
				if (columns.size() > i) {
					final ResultColumnVO resultColumnVO = columns.get(i);
					final String columnLabel = resultColumnVO.getColumnLabel();
					final String columnClassName = resultColumnVO.getColumnClassName();
					final Object value = row[i];
					if (value != null) {
						// support for native json formats
						if (Boolean.class.getCanonicalName().equals(columnClassName)) {
							jsonObject.add(columnLabel, RigidUtils.parseBoolean(value));
						} else if (value instanceof Number) {
							Number nValue = ((Number) value);
							if (Integer.class.getCanonicalName().equals(columnClassName)) {
								jsonObject.add(columnLabel, nValue.intValue());
							} else if (Double.class.getCanonicalName().equals(columnClassName)) {
								jsonObject.add(columnLabel, nValue.doubleValue());
							} else if (Long.class.getCanonicalName().equals(columnClassName)) {
								jsonObject.add(columnLabel, nValue.longValue());
							} else if (BigDecimal.class.getCanonicalName().equals(columnClassName) &&
									value instanceof BigDecimal) {
								jsonObject.add(columnLabel, (BigDecimal)nValue);
							} else {
								jsonObject.add(columnLabel, resultColumnVO.format(value, getCurrentLocale()));
							}
						} else {
							if (Date.class.getCanonicalName().equals(columnClassName)) {
								// RESTful Service, we want a useful ISO-format 'yyyy-mm-dd' instead of 'Java' DateFormat.MEDIUM
								String sValue = new SimpleDateFormat("yyyy-MM-dd").format(value);
								jsonObject.add(columnLabel, sValue);
							} else if (InternalTimestamp.class.getCanonicalName().equals(columnClassName)) {
								// TODO: Use ISO-8601 format
								String sValue = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(value);
								jsonObject.add(columnLabel, sValue);
							} else {
								jsonObject.add(columnLabel, resultColumnVO.format(value, getCurrentLocale()));
							}
						}
					}
				}
			}
			resultJson.add(jsonObject);
		}
		return resultJson.build();
	}

	@Override
	public String executeManagementConsole(String sCommand, String sArguments) throws Exception {
		sArguments = sArguments==null? null: URLDecoder.decode(sArguments, "UTF-8");

		if (sCommand != null && !sCommand.startsWith("-")) {
			sCommand = "-"+sCommand;
		}

		try (PipedOutputStream out = new PipedOutputStream();
			 PipedInputStream consoleIn = new PipedInputStream(out);
			 ByteArrayOutputStream consoleOut = new ByteArrayOutputStream()) {
			Thread readerThread = new Thread(() -> {
				try {
					IOUtils.copy(consoleIn, consoleOut);
				} catch (IOException e) {
					LOG.error("Error reading console input stream", e);
				}
			});
			readerThread.setDaemon(true);
			readerThread.start();
			try {
				new CommonConsole(out).parseAndInvoke(sCommand, sArguments);
			}
			catch (Exception ex) {
				LOG.error("parseAndInvoke failed", ex);
				return ex.getMessage();
			} finally {
				out.close();
			}
			readerThread.join();
			return consoleOut.toString(StandardCharsets.UTF_8.name());
		}
	}

	@Override
	public Collection<TasklistDefinition> getDynamicTaskLists() {
		return tasklistFacade.getUsersTasklists();
	}

	@Override
	public Collection<CustomComponentVO> getCustomComponents() {
		return customComponentFacade.getAll();
	}

	@Override
	public boolean isUserPermitted(UID customComponent) {
		remoteContext.setRemotly(true);
		try {
			planningTableFacade.getResPlanConfig(customComponent);
			return true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public PlanningTable getPlanningTable(UID configUID, Interval<Date> interval, boolean ownBookingsOnly,
										  CollectableSearchCondition searchCondition, String layoutEoId, String foreignKey) throws CommonBusinessException {
		remoteContext.setRemotly(true);
		return planningTableFacade.getPlanningTable(configUID, interval, ownBookingsOnly, searchCondition, layoutEoId, foreignKey);
	}

	@Override
	public TasklistDefinition getTasklistDefinition(UID taskListUID) throws CommonFinderException, CommonPermissionException {
		return tasklistFacade.getTasklistDefinition(taskListUID);
	}

	@Override
	public List<NucletInfo> getNucletInfos() {
    	List<NucletInfo> result = new ArrayList<>();
    	for (EntityObjectVO<UID> nucletEo : metaProvider.getNuclets()) {
    		result.add(new NucletInfo(nucletEo));
		}
		Collections.sort(result, new Comparator<NucletInfo>() {
			@Override
			public int compare(final NucletInfo info1, final NucletInfo info2) {
				int result = RigidUtils.compareComparables(info1.getPackage(), info2.getPackage());
				if (result == 0) {
					RigidUtils.compareComparables(info1.getName(), info2.getName());
				}
				return result;
			}
		});
		return result;
	}

	public Boolean canExport(UID entityUID) {
		return securityCache.canExportForEntity(getCurrentUserName(), entityUID, getCurrentMandatorUID());
	}

	@Override
	public Collection<DataLanguageVO> getDataLanguages() {
		return dataLanguageCache.getDataLanguages();
	}

	@Override
	public UID getPrimaryDataLanguage() {
		return (UID) dataLanguageCache.getNuclosPrimaryDataLanguage();
	}

	@Override
	public UID getCurrentDataLanguage() {
		return userCtx.getDataLocal();
	}

	public UID getCurrentMandatorUID() {
		return userCtx.getMandatorUID();
	}

	public Collator getCurrentLocaleCollator() {
		final Collator localeCollator = userCtx.getLocaleCollator();
		if (localeCollator == null) {
			userCtx.setLocaleCollator(CollatorProvider.newDefaultCollatorForLocale(getCurrentLocale()));
		}
		return userCtx.getLocaleCollator();
	}

	public Locale getCurrentLocale() {
		return LocaleContextHolder.getLocale();
	}

	public String getCurrentUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
	}

	public UID getCurrentUserId() {
		return securityCache.getUserUid(getCurrentUserName());
	}

	public <PK> ResultListExportRVO.OutputFile exportBoList(
			String boMetaId,
			final SessionEntityContext sessionEntityContext,
			final PK fkpk,
			String format,
			boolean pageOrientationLandscape,
			boolean isColumnScaled,
			Collection<FieldMeta<?>> fields,
			CollectableSearchExpression clctexpr,
			FilterJ filter
	) throws NuclosReportException, IOException, CommonBusinessException {
		remoteContext.setRemotly(true);

		final UID entityUid = Rest.translateFqn(E.ENTITY, boMetaId);

		Boolean canExportEntity = canExport(entityUid);
		if (!Rest.facade().isActionAllowed(Actions.ACTION_EXPORT_SEARCHRESULT) || (canExportEntity != null && !canExportEntity)) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		}

		final List<CollectableEntityField> lstclctefweSelected = new ArrayList<>();
		List<Integer> selectedFieldWidth = new ArrayList<>();
		ReportOutputVO.Format outputFormat = ReportOutputVO.Format.CSV;
		if ("xls".equals(format)) {
			outputFormat = ReportOutputVO.Format.XLS;
		} else if ("xlsx".equals(format)) {
			outputFormat = ReportOutputVO.Format.XLSX;
		} else if ("pdf".equals(format)) {
			outputFormat = ReportOutputVO.Format.PDF;
		}
		String customUsage = null;
		ReportOutputVO.PageOrientation orientation = pageOrientationLandscape ? ReportOutputVO.PageOrientation.LANDSCAPE : ReportOutputVO.PageOrientation.PORTRAIT;


		if (filter.getAttributes() != null) {
			for (String attribute : filter.getAttributes().split(",")) {
				for (FieldMeta<?> fm : fields) {
					if (
							!fm.isHidden() &&
									Rest.translateFqn(E.ENTITYFIELD, attribute).equals(fm.getUID())
							) {
						lstclctefweSelected.add(SearchConditionUtils.newEntityField(fm));
					}
				}

				selectedFieldWidth.add(200);
			}
		} else {
			for (FieldMeta<?> fm : fields) {
				if (fm.isHidden()
						|| fm.getUID().equals(SF.CREATEDBY.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CREATEDAT.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CHANGEDBY.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CHANGEDAT.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.VERSION.getUID(fm.getEntity()))
						|| fm.getDbColumn().equalsIgnoreCase(SF.PK_ID.getDbColumn())
						) {
					continue;
				}
				lstclctefweSelected.add(SearchConditionUtils.newEntityField(fm));
				selectedFieldWidth.add(200);
			}
		}

		NuclosFile file;
		if (sessionEntityContext != null) {
			String searchCondition = "";
			if (clctexpr.getSearchCondition() != null) {
				searchCondition = clctexpr.getSearchCondition().accept(new ToHumanReadablePresentationVisitor());
			}

			EntityMeta<?> subformMeta = Rest.getEntity(sessionEntityContext.getReffieldMeta().getEntity());

			Boolean canExportSubEntity = canExport(subformMeta.getUID());
			if (canExportSubEntity != null && !canExportSubEntity) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}

			Long chunksize = filter.getEnd();
			if (chunksize != null && filter.getStart() != null) {
				chunksize = chunksize - filter.getStart() + 1;
			}

			ResultParams resultParams = new ResultParams(
					fields.stream().map(FieldMeta::getUID).collect(Collectors.toList()),
					filter.getStart(),
					chunksize + 1,
					true
			);


			Collection<EntityObjectVO<PK>> eos = Rest.facade().getSubformData(
					subformMeta.getUID(),
					sessionEntityContext.getReffieldMeta().getUID(),
					fkpk,
					clctexpr,
					resultParams
			);
			ResultVO resultVO = reportFacade.convertEoVoListToResultVO(subformMeta.getUID(), lstclctefweSelected, eos);

			file = reportFacade.prepareExport(searchCondition, resultVO, outputFormat, orientation, isColumnScaled);
		} else {
			file = reportFacade.prepareSearchResult(
					clctexpr,
					lstclctefweSelected,
					selectedFieldWidth,
					entityUid,
					outputFormat,
					customUsage,
					orientation,
					isColumnScaled
			);
		}

		RestPrintoutFileDownloadCache downloadCache = SpringApplicationContextHolder.getBean(RestPrintoutFileDownloadCache.class);

		String boId = null;
		String fileId = downloadCache.cacheFile(boMetaId, boId, format, file);
		RestPrintoutFileDownloadCache.CacheResult cached = downloadCache.get(boMetaId, boId, format, fileId);

		return new ResultListExportRVO.OutputFile(boMetaId, cached.fileName, format, fileId);
	}

	/**
	 * Determines the current status history for the object with Id.
	 * Only the status UIDs are returned, the order is "New before Old"
	 * @param boId
	 * @return
	 */
	@Override
	public List<UID> getStateHistoryForObject(String boId) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		DbQuery<UID> q = builder.createQuery(UID.class);
		DbFrom<Long> t = q.from(E.STATEHISTORY);
		q.select(t.baseColumn(E.STATEHISTORY.state));
		q.where(builder.equalValue(t.baseColumn(E.STATEHISTORY.genericObject), new Long(boId)));
		q.orderBy(
				builder.desc(t.baseColumn(SF.CREATEDAT)),
				builder.desc(t.baseColumn(SF.PK_ID)));

		return dataBaseHelper.getDbAccess().executeQuery(q);
	}

	/**
	 * Checks whether an object with the specified ID exists in the business object (count > 0)
	 * @param entityUID
	 * @param boId
	 * @return
	 */
	@Override
	public boolean checkObjectExists(UID entityUID, String boId) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		DbQuery<Long> q = builder.createQuery(Long.class);
		DbFrom<Long> t = q.from(metaProvider.getEntity(entityUID));
		q.select(builder.countRows());
		q.where(builder.equalValue(t.baseColumn(SF.PK_ID), new Long(boId)));

		return dataBaseHelper.getDbAccess().executeQuerySingleResult(q) > 0L;
	}

	@Override
	public MetaProvider getMetaProvider() {
		return metaProvider;
	}
}
