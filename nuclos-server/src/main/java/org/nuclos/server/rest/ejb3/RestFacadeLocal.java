package org.nuclos.server.rest.ejb3;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.Collator;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import org.nuclos.common.DependentSelection;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.lucene.LuceneSearchResult;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.common2.BoDataSet;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.interval.Interval;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.schema.rest.PlanningTable;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.RecordGrantRight;
import org.nuclos.server.customcomp.valueobject.CustomComponentVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.i18n.DataLanguageVO;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.rvo.FilterJ;
import org.nuclos.server.rest.services.rvo.LayoutInfo;
import org.nuclos.server.rest.services.rvo.NucletInfo;
import org.nuclos.server.rest.services.rvo.ResultListExportRVO;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.nuclos.server.security.NuclosRuleBasedAuthenticationToken;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.springframework.core.io.Resource;

public interface RestFacadeLocal {

	SessionContext webLogin(String username, String password, Locale locale, final String datalanguage, String jSessionId, final String loginUri, final String serverUri);

	void changePassword(String username, String password, String newPassword, String jSessionId);

	SessionContext getSessionContext(String webSessionID);

	SessionContext validateSessionAuthenticationAndInitUserContext(String webSessionID);

	SessionContext initNewAnonymousNuclosSessionIfNecessary(String webSessionID, final String requestor, final Locale locale, final String dataLanguage);

	void webLogout(String webSessionID);

	void invalidateSession(String sessionId);

	EntitySearchFilter2 getSearchFilterByPk(UID pk) throws CommonBusinessException;

	List<SearchFilterVO> getSearchFilters() throws CommonBusinessException;

	Resource getResource(UID uid) throws IOException;

	EntityPermission getEntityPermission(UID entity);

	Collection<FieldMeta<?>> getAllFieldsByEntity(UID entity, boolean onlyReferenceFields);

	boolean isSuperUser();

	boolean isReadAllowedForField(FieldMeta<?> fm, UsageCriteria usage);

	boolean isWriteAllowedForField(FieldMeta<?> fm, UsageCriteria usage);

	boolean isReadAllowedForSubform(UID subform, UsageCriteria usage);

	boolean isWriteAllowedForSubform(UID subform, UsageCriteria usage);

	SubformPermission getSubformPermission(UID subform, UsageCriteria usage);

	boolean isWriteAllowedForGO(UID entity);

	boolean isWriteAllowedForMD(UID entity);

	boolean isDeleteAllowedForGO(UID entity, boolean bPhysically);

	Long countEntityObjectRows(UID entity, CollectableSearchExpression clctexpr) throws CommonPermissionException;

	<PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunk(UID entity, CollectableSearchExpression clctexpr, ResultParams resultParams)
			throws CommonPermissionException;

	<PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunkWithoutRollback(
			UID entity,
			CollectableSearchExpression clctexpr,
			ResultParams resultParams
	) throws CommonPermissionException;

	<PK> Collection<PK> getEntityObjectIds(UID entity, CollectableSearchExpression clctexpr)
			throws CommonPermissionException;

	UID getInitialStateId(UID entity);

	UID getInitialStateId(UID entity, UID processId);

	<PK> EntityObjectVO<PK> get(UID entity, PK pk, boolean remotely) throws CommonPermissionException;

	<PK> EntityObjectVO<PK> getThin(UID entity, PK pk, boolean remotely) throws CommonPermissionException;

	<PK> EntityObjectVO<PK> getDataForField(PK pk, FieldMeta<?> fm);

	List<CollectableField> getReferenceList(UID field, String search, Long lMaxRowCount, WebValueListProvider vlp, String pk, UID mandatorUID) throws CommonBusinessException;

	List<CollectableField> getVLPData(UID field, WebValueListProvider vlp, String pk, String quickSearchInput, Integer maxRowCount, boolean bDefault, UID mandatorUID) throws CommonBusinessException;

	List<DatasourceParameterVO> getParameterListForDataSource(UID entity) throws NuclosDatasourceException;

	List<DatasourceParameterVO> getParameterListForValueListProvider(UID vlp) throws NuclosDatasourceException, CommonFinderException, CommonPermissionException;

	<PK> EntityObjectVO<PK> insert(EntityObjectVO<PK> eoVO) throws CommonBusinessException;

	<PK> EntityObjectVO<PK> update(EntityObjectVO<PK> eoVO) throws CommonBusinessException;

	<PK> void delete(UID entity, PK pk, boolean logicalDeletion) throws CommonBusinessException;

	<PK> EntityObjectVO<PK> clone(EntityObjectVO<PK> eoVO, final UID layout, final boolean fetchFromDb, final Long parameterObjectId, final DependentSelection dependentSelection) throws CommonBusinessException;

	Collection<StateVO> getSubsequentStates(UsageCriteria usagecriteria) throws CommonBusinessException;

	Collection<StateTransitionVO> getStateTransitions(UsageCriteria usageCriteria);

	UID getTargetStateId(UID stateTransitionUID) throws CommonFinderException;

	void changeState(UID entity, Long pk, UID newState) throws CommonBusinessException;

	<PK> Collection<EntityObjectVO<PK>> getSubformData(UID subform, UID field, PK relatedPK, CollectableSearchExpression filterExpr, ResultParams resultParams);

	<PK> Long getSubformDataCount(UID subform, UID field, PK relatedPK, CollectableSearchExpression filterExpr);

	<PK> Collection<EntityObjectVO<PK>> getDependentData(UID subform, UID field, PK relatedPK);

	List<LuceneSearchResult> luceneIndexSearch(String searchString);

	String getFieldGroupName(UID groupID);

	String getDbVersion();

	boolean isActionAllowed(String action);

	Collection<UID> getAllowedStateTransitions();

	List<EventSupportSourceVO> getCustomRules(UsageCriteria uc);

	<PK> EntityObjectVO<PK> fireCustomEventSupport(EntityObjectVO<PK> eoVO, DependentSelection dependentSelection, String rule) throws CommonBusinessException;

	String getCustomUsage();

	String getLayoutML(UID pk) throws CommonBusinessException;

	LayoutInfo getLayoutInfo(UID layoutUID) throws CommonPermissionException;

	List<LayoutInfo> getLayoutInfosForEntity(UID entityUID);

	boolean isLayoutRestrictionsMustIgnoreGroovyRules(UID pk);

	Collection<UID> getEntitiesAssignedToLayoutId(UID layoutUID) throws CommonBusinessException;

	UID getDetailLayoutIDForUsage(UsageCriteria usage, boolean bSearchLayout);

	<PK> UsageCriteria getUsageCriteriaForPK(PK pk, UID entity) throws CommonBusinessException;

	BoDataSet importOrExportDatabase(InputStream is) throws CommonPermissionException;

	JsonObjectBuilder getTreeRoot(String boMetaId, String boId);
	JsonArrayBuilder getTree(String boMetaId, String boId);

	JsonArrayBuilder getSubtree(String nodeId);

	JsonArrayBuilder getTreeWithSearch(String boMetaId, String boId, String search) throws CommonBusinessException;

	String getParamRestMenuEntities();

	String getInitialEntityEntry();

	String translateUid(EntityMeta<?> eMeta, UID uid);

	UID translateFqn(EntityMeta<?> eMeta, String fqn);

	boolean hasPrintouts(UsageCriteria usagecriteria, Object boId);

	String getNucletParameterByName(UID nucletUID, String parameter);

	<PK> EntityObjectVO<PK> getBoWithDefaultValues(UID entityUID);

	<PK> BoGenerationResult generateBo(
			UID entityUID,
			Map<PK, Integer> eoIdsAndVersions,
			final DependentSelection dependentSelection, UID generatorUID,
			final Long parameterObjectId) throws CommonBusinessException;

	GeneratorActionVO getGeneration(UID generationUID);

	List<GeneratorActionVO> getGenerations(UsageCriteria usage, UID mandatorUID);

	boolean doesLayoutChangeWithProcess(Collection<UID> entities);

	<PK> EntityObjectVO<PK> getTemporaryObject(PK gpoId) throws CommonBusinessException;

	boolean isTemporaryId(String id);

	String getTempId(String id);

	<T> List<CollectableField> getValueList(FieldMeta<T> field, List<String> sSearch, FieldMeta<Long> referencingField, Long boId);

	void updateInputContext(Map<String, Serializable> context);

	void clearInputContext();

	List<EventSupportSourceVO> findEventSupportsByUsageAndEntity(String sEventclass, UsageCriteria usagecriteria);

	boolean isUnlockAllowed(UID entity);

	<PK> void unlock(UID entity, PK pk) throws CommonPermissionException;

	void chooseMandator(UID mandatorUID) throws CommonPermissionException;

	UID getCurrentMandatorUID();

	Locale getCurrentLocale();

	Collator getCurrentLocaleCollator();

	String getCurrentUserName();

	UID getCurrentUserId();

	ValuelistProviderVO getValuelistProvider(UID vlpUID) throws CommonFinderException, CommonPermissionException;

	DynamicTasklistVO getDynamicTasklistDatasource(UID dynamicTaskListUID);

	Map<String, Object> getDatasourceParameters(String dsXML, Map<String, String> datasourceParams) throws CommonBusinessException, ClassNotFoundException;

	JsonArray executeDatasource(UID datasourceUID, Map<String, String> datasourceParams, Integer iMaxRowCount) throws CommonBusinessException, ClassNotFoundException;

	String executeManagementConsole(String sCommand, String sArguments) throws Exception;

	Collection<TasklistDefinition> getDynamicTaskLists();

	Collection<CustomComponentVO> getCustomComponents();

	boolean isUserPermitted(UID customComponent);

	PlanningTable getPlanningTable(UID configUID, Interval<Date> interval, boolean ownBookingsOnly,
								   CollectableSearchCondition searchCondition, String layoutEoId, String foreignKey) throws CommonBusinessException;

	TasklistDefinition getTasklistDefinition(UID taskListUID) throws CommonFinderException, CommonPermissionException;

	List<NucletInfo> getNucletInfos();

	Collection<DataLanguageVO> getDataLanguages();

	UID getPrimaryDataLanguage();

	UID getCurrentDataLanguage();

	UID setDataLanguage(String datalanguage, SessionContext webSessionContext);

	boolean isDatasourceAllowed(UID datasourceUID);

	<PK> ResultListExportRVO.OutputFile exportBoList(
			String boMetaId,
			final SessionEntityContext sessionEntityContext,
			final PK fkpk,
			String format,
			boolean pageOrientationLandscape,
			boolean isColumnScaled,
			Collection<FieldMeta<?>> fields,
			CollectableSearchExpression clctexpr,
			FilterJ filter
	) throws NuclosReportException, IOException, CommonBusinessException;

	boolean isRestApiAllowed(String username);

	Set<UsageCriteria> getUsageCriteriaBySearchExpression(final UID entity, CollectableSearchExpression clctexpr);
	Set<RecordGrantRight> getRecordGrantRightBySearchExpression(final UID entity, CollectableSearchExpression clctexpr);
	/**
	 * @param clctexpr
	 * @return the distinct owner UIDs of the records. Non-owned records results in a UID_NULL
	 */
	Set<UID> getNuclosOwnerBySearchExpression(final UID entity, CollectableSearchExpression clctexpr);

	StateVO getState(UID stateUID) throws CommonFinderException;

	<PK> Map<String, Collection<EntityObjectVO<PK>>> groupObjectsForGeneration(Collection<PK> ids, GeneratorActionVO generation) throws CommonPermissionException;

	boolean hasGroupingAttributes(UID generatoractionUid);

	Boolean canExport(UID entityUID);

	NuclosRuleBasedAuthenticationToken tryRuleBasedAuthentication(final HttpHeaders headers, final UriInfo uriInfo, final String sessionId,
																  final String remoteAddr, final String remoteHost, final String remoteUser);

	void refreshRuleBasedAuthentication(final NuclosRuleBasedAuthenticationToken authToken,
										final HttpHeaders headers, final UriInfo uriInfo, final String sessionId,
										final String remoteAddr, final String remoteHost, final String remoteUser);

	JsonArray getTextModules(TextModuleSettings textModuleSettings) throws CommonBusinessException;

	List<UID> getStateHistoryForObject(String boId);

	boolean checkObjectExists(UID entityUID, String boId);

	MetaProvider getMetaProvider();
}
