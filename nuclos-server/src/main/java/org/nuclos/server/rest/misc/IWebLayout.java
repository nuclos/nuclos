package org.nuclos.server.rest.misc;

import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common2.layout.LayoutInfo;
import org.nuclos.common2.layoutml.AbstractWebComponent;
import org.nuclos.common2.layoutml.AbstractWebDependents;
import org.nuclos.common2.layoutml.WebStaticComponent;
import org.nuclos.common2.layoutml.WebSubform;
import org.nuclos.server.rest.services.rvo.LayoutAdditions;

public interface IWebLayout {
	
	LayoutInfo getLayoutInfo();
	
	List<LayoutAdditions> getComponents(UID entity, UID reffield, boolean onlyVisibleComponents);

	Map<UID, AbstractWebDependents> getVisibleSubLists();
	
	WebSubform getWebSubform(UID subformUID);
	
	boolean hasChart(UID uid);
	
	LayoutAdditions getLayoutAdditionsForWebComponenet(AbstractWebComponent webComponent);
	
	List<WebStaticComponent> getEnabledButtons();
}
