package org.nuclos.server.rest.misc;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.api.context.InputDelegateSpecification;
import org.nuclos.api.context.InputSpecification;
import org.nuclos.schema.rest.InputRequiredSpecification;
import org.nuclos.server.rest.services.rvo.JsonFactory;

@SuppressWarnings("serial")
public class InputRequiredWebException extends WebApplicationException {

	public InputRequiredWebException(InputSpecification inputSpecification) {
		
		super(
			Response.status(Response.Status.EXPECTATION_FAILED)
				.entity(buildJson(inputSpecification).build())
				.type(MediaType.APPLICATION_JSON)
				.build()
		);
	}

	/**
	 * InputRequieredException with InputDelegateSpecification
	 * is used to open a customized InputRequired dialog in the webclient
	 */
	public InputRequiredWebException(InputDelegateSpecification inputDelegateSpecification) {

		super(
			Response.status(Response.Status.EXPECTATION_FAILED)
				.entity(buildJson(inputDelegateSpecification).build())
				.type(MediaType.APPLICATION_JSON)
				.build()
		);
	}

	private static String getType(final InputSpecification inputSpecification) {
		String type = "";
		switch (inputSpecification.getType()) {
			case InputSpecification.INPUT_OPTION:
				type = "input_option";
				break;
			case InputSpecification.INPUT_VALUE:
				type = "input_value";
				break;
			case InputSpecification.INPUT_MEMO:
				type = "input_memo";
				break;
			case InputSpecification.CONFIRM_YES_NO:
				type = "confirm_yes_no";
				break;
			case InputSpecification.CONFIRM_OK_CANCEL:
				type = "confirm_ok_cancel";
				break;
			case InputSpecification.CONFIRM_OK:
				type = "confirm_ok";
				break;
		}
		return type;
	}

	public static InputRequiredSpecification transformInputSpecification(final InputSpecification inputSpecification) {
		String defaultoption = null;
		if (inputSpecification.getDefaultOption() != null) {
			defaultoption = inputSpecification.getDefaultOption().toString();
		}

		List<String> options = null;
		if (inputSpecification.getOptions() != null) {
			options = new ArrayList<String>();
			for (Object o : inputSpecification.getOptions()) {
				options.add((String) o);
			}
		}
		return InputRequiredSpecification.builder()
				.withKey(inputSpecification.getKey())
				.withMessage(inputSpecification.getMessage())
				.withTitle(inputSpecification.getTitle())
				.withType(getType(inputSpecification))
				.withDefaultoption(defaultoption)
				.withOptions(options)
				.build();
	}

	private static JsonObjectBuilder buildJson(InputSpecification inputSpecification) {
		JsonObjectBuilder inputRequiredJson = Json.createObjectBuilder();
		JsonObjectBuilder specification = inputRequiredJson = Json.createObjectBuilder();
		specification.add("key", inputSpecification.getKey());
		specification.add("message", inputSpecification.getMessage());
		if (inputSpecification.getTitle() != null) {
			specification.add("title", inputSpecification.getTitle());
		}
		specification.add("type", getType(inputSpecification));
		
		if (inputSpecification.getOptions() != null) {
			JsonArrayBuilder options = Json.createArrayBuilder();
			for (Object o : inputSpecification.getOptions()) {
				options.add(JsonFactory.buildJsonValue(o));
			}
			specification.add("options", options);
		}

		if (inputSpecification.getDefaultOption() != null) {
			specification.add("defaultoption", inputSpecification.getDefaultOption().toString());
		}
		
		inputRequiredJson.add("specification", specification);
		
    	JsonObjectBuilder json = Json.createObjectBuilder();
    	json.add("inputrequired", inputRequiredJson);
    	
    	return json;
	}

	private static JsonObjectBuilder buildJson(InputDelegateSpecification inputDelegateSpecification) {
		JsonObjectBuilder inputRequiredJson = Json.createObjectBuilder();
		JsonObjectBuilder specification = inputRequiredJson = Json.createObjectBuilder();

		specification.add("delegateComponent", inputDelegateSpecification.getDelegateClass());

		if (inputDelegateSpecification.getData() != null) {
			JsonObjectBuilder data = Json.createObjectBuilder();
			inputDelegateSpecification.getData().entrySet().stream()
					.forEach(entry -> {
						if (entry.getValue() != null) {
							data.add(entry.getKey(), entry.getValue().toString());
						} else {
							data.addNull(entry.getKey());
						}
					});
			specification.add("data", data);
		}

		inputRequiredJson.add("specification", specification);

    	JsonObjectBuilder json = Json.createObjectBuilder();
    	json.add("inputrequired", inputRequiredJson);

    	return json;
	}

}
