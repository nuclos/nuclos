package org.nuclos.server.rest.misc;

import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

@Consumes(MediaType.APPLICATION_JSON)
public interface JsonConsumer {
}
