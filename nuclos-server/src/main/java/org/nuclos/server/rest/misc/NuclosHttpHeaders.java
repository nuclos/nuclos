package org.nuclos.server.rest.misc;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.glassfish.jersey.internal.LocalizationMessages;
import org.glassfish.jersey.internal.RuntimeDelegateDecorator;
import org.glassfish.jersey.message.internal.AcceptableLanguageTag;
import org.glassfish.jersey.message.internal.AcceptableMediaType;
import org.glassfish.jersey.message.internal.HeaderUtils;
import org.glassfish.jersey.message.internal.HeaderValueException;
import org.glassfish.jersey.message.internal.HttpHeaderReader;
import org.glassfish.jersey.message.internal.LanguageTag;
import org.glassfish.jersey.message.internal.MediaTypes;

/**
 * We need these implementation for our API.
 * Within the Jersey server they would be not necessary, but unfortunately in a servlet filter.
 * The implementation is limited to the essentials.
 * Source: org.glassfish.jersey.server.ContainerRequest
 */
public class NuclosHttpHeaders implements HttpHeaders {

	private final WeakReference<Configuration> weakConfiguration;

	private static final List<AcceptableMediaType> WILDCARD_ACCEPTABLE_TYPE_SINGLETON_LIST =
			Collections.singletonList(MediaTypes.WILDCARD_ACCEPTABLE_TYPE);

	private final MultivaluedMap<String, String> headers;

	public NuclosHttpHeaders(final Configuration config, final HttpServletRequest request) {
		this.weakConfiguration = new WeakReference<>(config);
		this.headers = HeaderUtils.createInbound();
		Collections.list(request.getHeaderNames()).forEach(name -> {
			this.headers.addAll(name, Collections.list(request.getHeaders(name)));
		});
	}

	@Override
	public List<String> getRequestHeader(final String name) {
		return getHeaders().get(name);
	}

	@Override
	public String getHeaderString(final String name) {
		List<String> values = this.headers.get(name);
		if (values == null) {
			return null;
		}
		if (values.isEmpty()) {
			return "";
		}

		final Iterator<String> valuesIterator = values.iterator();
		StringBuilder buffer = new StringBuilder(valuesIterator.next());
		while (valuesIterator.hasNext()) {
			buffer.append(',').append(valuesIterator.next());
		}

		return buffer.toString();
	}

	@Override
	public MultivaluedMap<String, String> getRequestHeaders() {
		return this.headers;
	}

	@Override
	public List<MediaType> getAcceptableMediaTypes() {
		return getQualifiedAcceptableMediaTypes().stream()
				.map((Function<AcceptableMediaType, MediaType>) input -> input)
				.collect(Collectors.toList());
	}

	@Override
	public List<Locale> getAcceptableLanguages() {
		return getQualifiedAcceptableLanguages().stream()
				.map(LanguageTag::getAsLocale)
				.collect(Collectors.toList());
	}

	private List<AcceptableLanguageTag> getQualifiedAcceptableLanguages() {
		final String value = getHeaderString(HttpHeaders.ACCEPT_LANGUAGE);

		if (value == null || value.isEmpty()) {
			return Collections.singletonList(new AcceptableLanguageTag("*", null));
		}

		try {
			return Collections.unmodifiableList(HttpHeaderReader.readAcceptLanguage(value));
		} catch (ParseException e) {
			throw exception(HttpHeaders.ACCEPT_LANGUAGE, value, e);
		}
	}

	@Override
	public MediaType getMediaType() {
		return singleHeader(HttpHeaders.CONTENT_TYPE, new Function<String, MediaType>() {
			@Override
			public MediaType apply(String input) {
				try {
					return RuntimeDelegateDecorator.configured(weakConfiguration.get())
							.createHeaderDelegate(MediaType.class)
							.fromString(input);
				} catch (IllegalArgumentException iae) {
					throw new ProcessingException(iae);
				}
			}
		}, false);
	}

	@Override
	public Locale getLanguage() {
		return singleHeader(HttpHeaders.CONTENT_LANGUAGE, new Function<String, Locale>() {
			@Override
			public Locale apply(String input) {
				try {
					return new LanguageTag(input).getAsLocale();
				} catch (ParseException e) {
					throw new ProcessingException(e);
				}
			}
		}, false);
	}

	@Override
	public Map<String, Cookie> getCookies() {
		List<String> cookies = this.headers.get(HttpHeaders.COOKIE);
		if (cookies == null || cookies.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<String, Cookie> result = new HashMap<String, Cookie>();
		for (String cookie : cookies) {
			if (cookie != null) {
				result.putAll(HttpHeaderReader.readCookies(cookie));
			}
		}
		return result;
	}

	@Override
	public Date getDate() {
		return singleHeader(HttpHeaders.DATE, new Function<String, Date>() {
			@Override
			public Date apply(String input) {
				try {
					return HttpHeaderReader.readDate(input);
				} catch (ParseException ex) {
					throw new ProcessingException(ex);
				}
			}
		}, false);
	}

	@Override
	public int getLength() {
		return singleHeader(HttpHeaders.CONTENT_LENGTH, new Function<String, Integer>() {
			@Override
			public Integer apply(String input) {
				try {
					return (input != null && !input.isEmpty()) ? Integer.parseInt(input) : -1;
				} catch (NumberFormatException ex) {
					throw new ProcessingException(ex);
				}
			}
		}, true);
	}

	public MultivaluedMap<String, String> getHeaders() {
		return this.headers;
	}

	private List<AcceptableMediaType> getQualifiedAcceptableMediaTypes() {
		final String value = getHeaderString(HttpHeaders.ACCEPT);

		if (value == null || value.isEmpty()) {
			return WILDCARD_ACCEPTABLE_TYPE_SINGLETON_LIST;
		}

		try {
			return Collections.unmodifiableList(HttpHeaderReader.readAcceptMediaType(value));
		} catch (ParseException e) {
			throw exception(HttpHeaders.ACCEPT, value, e);
		}
	}

	private static HeaderValueException exception(final String headerName, Object headerValue, Exception e) {
		return new HeaderValueException(LocalizationMessages.UNABLE_TO_PARSE_HEADER_VALUE(headerName, headerValue), e,
				HeaderValueException.Context.INBOUND);
	}

	private <T> T singleHeader(String name, Function<String, T> converter, boolean convertNull) {
		final List<String> values = this.headers.get(name);

		if (values == null || values.isEmpty()) {
			return convertNull ? converter.apply(null) : null;
		}
		if (values.size() > 1) {
			throw new HeaderValueException(LocalizationMessages.TOO_MANY_HEADER_VALUES(name, values.toString()),
					HeaderValueException.Context.INBOUND);
		}

		Object value = values.get(0);
		if (value == null) {
			return convertNull ? converter.apply(null) : null;
		}

		try {
			return converter.apply(HeaderUtils.asString(value, weakConfiguration.get()));
		} catch (ProcessingException ex) {
			throw exception(name, value, ex);
		}
	}

}
