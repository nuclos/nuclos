package org.nuclos.server.rest.misc;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.internal.util.collection.ImmutableMultivaluedMap;
import org.glassfish.jersey.uri.UriComponent;
import org.glassfish.jersey.uri.UriTemplate;
import org.glassfish.jersey.uri.internal.JerseyUriBuilder;

/**
 * We need these implementation for our API.
 * Within the Jersey server they would be not necessary, but unfortunately in a servlet filter.
 * The implementation is limited to the essentials.
 * Source: org.glassfish.jersey.server.internal.routing.UriRoutingContext
 */
public class NuclosUriInfo implements UriInfo {

	private final URI baseUri;

	private final URI requestUri;

	private String encodedRelativePath = null;

	private String decodedRelativePath = null;

	private URI absolutePathUri = null;

	private final MultivaluedHashMap<String, String> encodedTemplateValues = new MultivaluedHashMap<>();
	private final ImmutableMultivaluedMap<String, String> encodedTemplateValuesView =
			new ImmutableMultivaluedMap<>(encodedTemplateValues);

	private MultivaluedHashMap<String, String> decodedTemplateValues;
	private ImmutableMultivaluedMap<String, String> decodedTemplateValuesView;

	private ImmutableMultivaluedMap<String, String> encodedQueryParamsView;
	private ImmutableMultivaluedMap<String, String> decodedQueryParamsView;

	private static final Function<String, String> PATH_DECODER = input -> UriComponent.decode(input, UriComponent.Type.PATH);

	public NuclosUriInfo(final URI baseUri, final URI requestUri) {
		this.baseUri = baseUri;
		this.requestUri = requestUri;
	}

	@Override
	public String getPath() {
		return getPath(true);
	}

	@Override
	public String getPath(final boolean decode) {
		if (decode) {
			if (decodedRelativePath != null) {
				return decodedRelativePath;
			}

			return decodedRelativePath = UriComponent.decode(encodedRelativePath(), UriComponent.Type.PATH);
		} else {
			return encodedRelativePath();
		}
	}

	private String encodedRelativePath() {
		if (encodedRelativePath != null) {
			return encodedRelativePath;
		}

		final String requestUriRawPath = requestUri.getRawPath();

		if (baseUri == null) {
			return encodedRelativePath = requestUriRawPath;
		}

		final int baseUriRawPathLength = baseUri.getRawPath().length();
		return encodedRelativePath = baseUriRawPathLength < requestUriRawPath.length()
				? requestUriRawPath.substring(baseUriRawPathLength) : "";
	}

	@Override
	public List<PathSegment> getPathSegments() {
		return getPathSegments(true);
	}

	@Override
	public List<PathSegment> getPathSegments(final boolean decode) {
		final String requestPath = getPath(false);
		return Collections.unmodifiableList(UriComponent.decodePath(requestPath, decode));
	}

	@Override
	public URI getRequestUri() {
		return requestUri;
	}

	@Override
	public UriBuilder getRequestUriBuilder() {
		return UriBuilder.fromUri(getRequestUri());
	}

	@Override
	public URI getAbsolutePath() {
		if (absolutePathUri != null) {
			return absolutePathUri;
		}

		return absolutePathUri = new JerseyUriBuilder().uri(requestUri).replaceQuery("").fragment("").build();
	}

	@Override
	public UriBuilder getAbsolutePathBuilder() {
		return new JerseyUriBuilder().uri(getAbsolutePath());
	}

	@Override
	public URI getBaseUri() {
		return baseUri;
	}

	@Override
	public UriBuilder getBaseUriBuilder() {
		return new JerseyUriBuilder().uri(getBaseUri());
	}

	@Override
	public MultivaluedMap<String, String> getPathParameters() {
		return getPathParameters(true);
	}

	@Override
	public MultivaluedMap<String, String> getPathParameters(final boolean decode) {
		if (decode) {
			if (decodedTemplateValuesView != null) {
				return decodedTemplateValuesView;
			} else if (decodedTemplateValues == null) {
				decodedTemplateValues = new MultivaluedHashMap<>();
				for (final Map.Entry<String, List<String>> e : encodedTemplateValues.entrySet()) {
					decodedTemplateValues.put(
							UriComponent.decode(e.getKey(), UriComponent.Type.PATH_SEGMENT),
							// we need to keep the ability to add new entries
							e.getValue().stream().map(s -> UriComponent.decode(s, UriComponent.Type.PATH))
									.collect(Collectors.toCollection(ArrayList::new)));
				}
			}
			decodedTemplateValuesView = new ImmutableMultivaluedMap<>(decodedTemplateValues);

			return decodedTemplateValuesView;
		} else {
			return encodedTemplateValuesView;
		}
	}

	@Override
	public MultivaluedMap<String, String> getQueryParameters() {
		return getQueryParameters(true);
	}

	@Override
	public MultivaluedMap<String, String> getQueryParameters(final boolean decode) {
		if (decode) {
			if (decodedQueryParamsView != null) {
				return decodedQueryParamsView;
			}

			decodedQueryParamsView =
					new ImmutableMultivaluedMap<>(UriComponent.decodeQuery(getRequestUri(), true));

			return decodedQueryParamsView;
		} else {
			if (encodedQueryParamsView != null) {
				return encodedQueryParamsView;
			}

			encodedQueryParamsView =
					new ImmutableMultivaluedMap<>(UriComponent.decodeQuery(getRequestUri(), false));

			return encodedQueryParamsView;

		}
	}

	@Override
	public List<String> getMatchedURIs() {
		return getMatchedURIs(true);
	}

	@Override
	public List<String> getMatchedURIs(final boolean decode) {
		return Collections.emptyList();
	}

	@Override
	public List<Object> getMatchedResources() {
		return Collections.emptyList();
	}

	@Override
	public URI resolve(final URI uri) {
		return UriTemplate.resolve(getBaseUri(), uri);
	}

	@Override
	public URI relativize(URI uri) {
		if (!uri.isAbsolute()) {
			uri = resolve(uri);
		}

		return UriTemplate.relativize(getRequestUri(), uri);
	}

}
