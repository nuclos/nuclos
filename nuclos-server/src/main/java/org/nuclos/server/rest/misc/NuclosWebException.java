package org.nuclos.server.rest.misc;

import javax.annotation.Nonnull;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.validation.FieldValidationError;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.InternalErrorException;
import org.nuclos.common2.exception.NuclosExceptions;
import org.nuclos.common2.exception.NuclosPasswordBasedLoginForbiddenException;
import org.nuclos.common2.layoutml.exception.LayoutMLParseException;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.security.NuclosRemoteExceptionLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * TODO: Unify Exception responses - format all exceptions the same way (JSON only)
 * See also NUCLOS-6935
 */
@SuppressWarnings("serial")
public class NuclosWebException extends WebApplicationException {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosWebException.class);

	public static final Response.StatusType STATUS_LOCKED = new Response.StatusType(){
		@Override
		public int getStatusCode() {
			return 423;
		}
		@Override
		public Response.Status.Family getFamily() {
			return Response.Status.Family.familyOf(423);
		}
		@Override
		public String getReasonPhrase() {
			return "Locked";
		}
	};

	public static NuclosWebException wrap(@Nonnull final Exception e) {
		return wrap(e, null);
	}

	public static NuclosWebException wrap(@Nonnull final Exception e, final UID entity) {
		if (e instanceof NuclosWebException) {
			return (NuclosWebException) e;
		} else if (e.getCause() instanceof  NuclosWebException) {
			return (NuclosWebException) e.getCause();
		}

		Response response = getResponse(e, entity);
		return new NuclosWebException(response);
	}

	public NuclosWebException(final Response response) {
		super(response);
	}

	public NuclosWebException(Response.StatusType status) {
		// Writing the reason phrase as message here prevents the default HTML error message
		// which is automatically generated by Tomcat and can not be properly displayed by the webclient.
		this(status, status.getReasonPhrase());
	}

	public NuclosWebException(Response.StatusType status, String msg) {
		super(getResponse(status, msg));
	}

	public NuclosWebException(Exception ex, UID entity) {
		super(getResponse(ex, entity));
	}

	public NuclosWebException(Exception ex) {
		super(ex);
	}

	private static Response getResponse(Response.StatusType status, String msg) {
		LOG.error(msg);
		return Response.status(status).entity(msg).type(MediaType.TEXT_PLAIN).build();
	}

	private static Response getResponse(Throwable ex, UID entity) {
		final boolean bOriginIsCommonFatal = ex instanceof CommonFatalException;
		ex = SpringApplicationContextHolder.getBean(NuclosRemoteExceptionLogger.class).logException(ex, LOG);

		String sMessage = ex.getMessage();

		JsonObjectBuilder json = Json.createObjectBuilder();
		sMessage = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class).getMessageFromResource(sMessage);

		Status status = Response.Status.NOT_ACCEPTABLE;
		int iStatus = -1;

		if (ex instanceof NuclosBusinessException || ex instanceof BusinessException) {
			status = Response.Status.PRECONDITION_FAILED;
		} else if (ex instanceof CommonStaleVersionException) {
			status = Response.Status.CONFLICT;
		} else if (ex instanceof CommonBusinessException) {
			if (ex instanceof LayoutMLParseException) {
				status = Status.INTERNAL_SERVER_ERROR;
			} else {
				status = Response.Status.FORBIDDEN;
			}
			CommonValidationException cve = NuclosExceptions.getCause(ex, CommonValidationException.class);
			if (cve != null) {
				status = Response.Status.PRECONDITION_FAILED;
				sMessage = Rest.getFullMessage(cve, entity);

				addValidationErrorsToJson(json, cve);
			}
		} else if (ex instanceof BadCredentialsException || ex instanceof UsernameNotFoundException) {
			status = Status.UNAUTHORIZED;
		} else if (ex instanceof NuclosPasswordBasedLoginForbiddenException) {
			status = Status.FORBIDDEN;
		} else if (ex instanceof CredentialsExpiredException) {
			status = Status.PRECONDITION_FAILED;
		} else if (ex instanceof LockedException) {
			// HTTP code 423 is not available in Response.Status
			iStatus = 423;
		} else if (ex instanceof InternalErrorException) {
			status = bOriginIsCommonFatal ?
					Status.EXPECTATION_FAILED: // like the old approach (before covering of the technical information)
					Status.INTERNAL_SERVER_ERROR;
		}

		json.add("message", sMessage != null ? sMessage : ex.getClass().getCanonicalName());

		if (iStatus > 0) {
			return Response.status(iStatus).entity(json.build()).type(MediaType.APPLICATION_JSON).build();
		}
		return Response.status(status).entity(json.build()).type(MediaType.APPLICATION_JSON).build();
	}

	private static void addValidationErrorsToJson(final JsonObjectBuilder json, final CommonValidationException cve) {
		if (cve.getFieldErrors() == null) {
			return;
		}

		JsonArrayBuilder validationErrorsJSON = Json.createArrayBuilder();
		for (FieldValidationError validationError : cve.getFieldErrors()) {
			JsonObjectBuilder validationErrorJSON = Json.createObjectBuilder();
			validationErrorJSON.add("entity", Rest.translateUid(E.ENTITY, validationError.getEntity()));
			validationErrorJSON.add("field", Rest.translateUid(E.ENTITYFIELD, validationError.getField()));
			validationErrorJSON.add("errortype", validationError.getValidationErrorType().toString());
			validationErrorsJSON.add(validationErrorJSON);
		}

		json.add("validationErrors", validationErrorsJSON);
	}

}
