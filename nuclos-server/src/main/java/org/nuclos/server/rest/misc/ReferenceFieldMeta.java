package org.nuclos.server.rest.misc;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;

public class ReferenceFieldMeta {
	private final FieldMeta<?> reffield;
	private final EntityMeta<?> depMeta;
	private final EntityMeta<?> masterMeta;
	
	public ReferenceFieldMeta(FieldMeta<?> reffield, EntityMeta<?> depMeta, EntityMeta<?> masterMeta) {
		this.reffield = reffield;
		this.depMeta = depMeta;
		this.masterMeta = masterMeta;
	}

	public FieldMeta<?> getReffield() {
		return reffield;
	}

	public EntityMeta<?> getDepMeta() {
		return depMeta;
	}
	
	public EntityMeta<?> getMasterMeta() {
		return masterMeta;
	}

}
