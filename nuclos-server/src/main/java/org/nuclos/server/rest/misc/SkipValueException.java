package org.nuclos.server.rest.misc;

@SuppressWarnings("serial")
public class SkipValueException extends Exception {
	
	public SkipValueException(String message) {
		super(message);
	}

}
