package org.nuclos.server.rest.misc;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.security.EntityPermission;

public class TaskOverview extends BoMetaOverview {
	private final UID taskUid;
	private final UID taskEntity;
	private final UID dynamicEntityField;
	private final UID searchFilterId;

	/**
	 * A builder would be nice here, but the inheritance makes it difficult. :(
	 */
	public TaskOverview(
			EntityMeta<?> eMeta,
			UID taskUid,
			String label,
			EntityPermission permission,
			final UID taskEntityField,
			final UID taskEntity,
			final UID searchFilterId
	) {
		super(eMeta, label, permission, true);

		this.taskUid = taskUid;
		this.dynamicEntityField = taskEntityField;
		this.taskEntity = taskEntity;
		this.searchFilterId = searchFilterId;
	}

	public UID getTaskUID() {
		return taskUid;
	}

	public UID getDynamicEntityField() {
		return dynamicEntityField;
	}

	public UID getTaskEntity() {
		return taskEntity;
	}

	public UID getSearchFilterId() {
		return searchFilterId;
	}

	@Override
	public String toString() {
		return "TaskUid=" + taskUid + "\n" + super.toString();
	}
}
