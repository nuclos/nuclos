package org.nuclos.server.rest.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Encoding;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/boDocuments")
@Produces(MediaType.APPLICATION_JSON)
public class BoDocumentService extends DataServiceHelper {

	private static final Logger LOG = LoggerFactory.getLogger(BoDocumentService.class);

	private static File uploadPath;

	/**
	 * Try to read file from file-system with given token
	 *
	 * @param token
	 * @param deleteFile
	 * @return NuclosFile if found else null
	 */
	public static NuclosFile getUploadedFile(String token, boolean deleteFile) {
		try {
			BoDocumentTransactionSynchronization transactionSync = new BoDocumentTransactionSynchronization();
			String fileName = null;
			byte[] fileContents = null;

			BufferedReader reader = null;
			try {
				String filename = new File(token  + ".meta").getName();
				File file = new File(getUploadPath().getCanonicalPath(), filename);
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8")));
				fileName = reader.readLine();
				if (deleteFile) {
					transactionSync.filesToDelete.add(file); //... instead of file.delete();
				}
			} finally {
				if (reader != null) {
					reader.close();
				}
			}

			FileInputStream fis = null;
			try {
				String filename = new File(token  + ".data").getName();
				File file = new File(getUploadPath().getCanonicalPath(), filename);
				fis = new FileInputStream(file);
				fileContents = new byte[(int) file.length()];
				fis.read(fileContents);
				if (deleteFile) {
					transactionSync.filesToDelete.add(file); //... instead of file.delete();
				}
			} finally {
				if (fis != null) {
					fis.close();
				}
			}

			if (deleteFile) {
				TransactionSynchronizationManager.registerSynchronization(transactionSync);
			}
			return new NuclosFile(fileName, fileContents);

		} catch (IOException ex) {
			LOG.warn(ex.getMessage(), ex);
			return null;
		}
	}

	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	@Operation(
			operationId = "boDocumentUpload",
			description = "Upload a file, returns a token for use in attribute value and BO update later.",
			summary = "Upload a file",
			requestBody = @RequestBody(
					required = true,
					content = @Content(
							mediaType = "multipart/form-data",
							schema = @Schema(
									implementation = MultiRequest.class
							),
							encoding = @Encoding(
									name = "file",
									contentType = "*/*"
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											type = "string"
									)
							),
							description = "Returning token for uploaded file"
					),
					@ApiResponse(
							responseCode = "400",
							description = "Wrong parameter given for upload"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Uploading the object failed on the server"
					)
			},
			tags = "Nuclos Documents"
	)
	public String boDocumentUpload(
			@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition formData) {
		try {
			byte[] fileContents = IOUtils.toByteArray(is);
			String fileName = new String(formData.getFileName().getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);

			UID token = new UID();
			String uploadBaseFileName = getUploadPath().getCanonicalPath() + File.separatorChar + token;

			Writer out = null;
			try {
				File file = new File(uploadBaseFileName + ".meta");
				file.deleteOnExit();
				out = new BufferedWriter(new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8));
			    out.write(fileName);
			    out.write("\n");
			    out.write("Uploader: " + getUser());
			} finally {
				if (out != null) {
					out.close();
				}
			}
			
			FileOutputStream fop = null;
			try {
				File file = new File(uploadBaseFileName + ".data");
				file.deleteOnExit();
				fop = new FileOutputStream(file);
				fop.write(fileContents);
				fop.flush();
			} finally {
				if (fop != null) {
					fop.close();
				}
			}

			return token.getString();

		} catch (IOException ex) {
			throw new NuclosWebException(Status.INTERNAL_SERVER_ERROR, ex.getMessage());
		}

	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/temp/{token}")
	@Operation(
			operationId = "boTempDocValue",
			summary = "Retrieve a temporarily uploaded document",
			description = "Retrieve a temporarily uploaded document",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "application/octet-stream"
							)
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given document could not be found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Documents"
	)
	public Response boDocumentValue(
			@Context final Request request,
			@Parameter(
					name = "token",
					required = true,
					description = "String token identifier for temporarily uploaded image",
					example = "123as8f"
			)
			@PathParam("token") final String token
	) {
		NuclosFile nf = BoDocumentService.getUploadedFile(token, false);
		if (nf == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return fileResponse(nf);
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/{boId}/documents/{docAttrId}")
	@Operation(
			operationId = "boDocumentValue",
			description = "File of businessobject attribute",
			summary = "File of businessobject attribute",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "application/octet-stream"
							)
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given document could not be found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Documents"
	)
	public Response boDocumentValue(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "1000"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "docAttrId",
					required = true,
					description = "Businessobject Attribute Identifier referencing document",
					example = "example_rest_Article_sheet"
			)
			@PathParam("docAttrId") String docAttrId) {

		NuclosFile nf = byteAttributeValueWithValidation(boMetaId, boId, docAttrId);
		return fileResponse(nf);
	}

	public static File getUploadPath() {
		if (uploadPath != null) {
			return uploadPath;
		}

		File uploadPathFromParameters = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_UPLOAD_PATH);
		if (uploadPathFromParameters != null) {
			uploadPath = uploadPathFromParameters;
			return uploadPath;
		}

		// TODO: Refactor - the REST service should not be concerned about directory creation.
		File defaultUploadPath = createDefaultUploadPath();
		if (defaultUploadPath != null) {
			uploadPath = defaultUploadPath;
			return uploadPath;
		}

		throw new NuclosWebException(
				Status.INTERNAL_SERVER_ERROR,
				NuclosSystemParameters.DOCUMENT_UPLOAD_PATH + " is not set in server.properties and default directory could not be created"
		);
	}

	/**
	 * TODO: Define default path in central place.
	 * TODO: Move to appropriate place in server.
	 */
	private static File createDefaultUploadPath() {
		try {
			File documentPath = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH);
			if (documentPath != null) {
				File documentParent = documentPath.getParentFile();
				File defaultUploadPath = new File(documentParent, "documents-upload");
				defaultUploadPath.mkdir();
				return defaultUploadPath;
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return null;
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/{boId}/subBos/{refAttrId}/{subBoId}/documents/{docAttrId}")
	@Operation(
			operationId = "dependence_DocumentValue",
			description = "File of sub businessobject attribute",
			summary = "File of sub businessobject attribute",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "application/octet-stream"
							)
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given document could not be found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Documents"
	)
	public Response boDocumentValue(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "1000"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Businessobject attribute identifier referencing sub businessobject",
					example = "example_rest_Order_customer"
			)
			@PathParam("refAttrId") String refAttrId,
			@Parameter(
					name = "subBoId",
					required = true,
					description = "Sub Businessobject Identifier",
					example = "1000"
			)
			@PathParam("subBoId") String subBoId,
			@Parameter(
					name = "docAttrId",
					required = true,
					description = "Businessobject Attribute Identifier referencing document",
					example = "example_rest_Customer_sheet"
			)
			@PathParam("docAttrId") String docAttrId) {

		checkReadAllowed(boMetaId, boId);
		String subBoMetaId = getSubBoMetaId(refAttrId);
		UID subBoUid = Rest.translateFqn(E.ENTITY, subBoMetaId);
		EntityMeta<?> eMeta = Rest.getEntity(subBoUid);

		NuclosFile nf = byteAttributeValue(eMeta, subBoId, docAttrId, false);
		return fileResponse(nf);
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/{boId}/subBos/recursive/{referenceAttributeAndRecordIds:.+}/documents/{docAttrId}")
	@Operation(
			operationId = "dependence_recursive_DocumentValue",
			description = "File of sub businessobject attribute in arbitrary depth",
			summary = "File of sub businessobject attribute in arbitrary depth",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "application/octet-stream"
							)
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given document could not be found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Documents"
	)
	public Response boDocumentValueRecursive(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "1000"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "referenceAttributeAndRecordIds",
					required = true,
					description = "Businessobject attribute identifier referencing sub businessobject",
					example = "example_rest_Order_customer"
			)
			@PathParam("referenceAttributeAndRecordIds") List<PathSegment> referenceAttributeAndRecordIds,
			@Parameter(
					name = "subBoId",
					required = true,
					description = "Sub Businessobject Identifier",
					example = "1000"
			)
			@PathParam("docAttrId") String docAttrId) {

		checkReadAllowed(boMetaId, boId);

		String refAttrId = String.valueOf(referenceAttributeAndRecordIds.get(referenceAttributeAndRecordIds.size() - 2));
		String subBoId = String.valueOf(referenceAttributeAndRecordIds.get(referenceAttributeAndRecordIds.size() - 1));

		String subBoMetaId = getSubBoMetaId(refAttrId);
		UID subBoUid = Rest.translateFqn(E.ENTITY, subBoMetaId);
		EntityMeta<?> eMeta = Rest.getEntity(subBoUid);

		NuclosFile nf = byteAttributeValue(eMeta, subBoId, docAttrId, false);
		return fileResponse(nf);
	}

	private Response fileResponse(final NuclosFile nf) {
		if (nf != null) {
			final String contentDisposition = StringUtils.defaultIfBlank(parameterProvider.getValue(ParameterProvider.CONTENT_DISPOSITION), "inline");
			ResponseBuilder responseBuilder = Response.ok(nf.getContent()).type(URLConnection.guessContentTypeFromName(nf.getName()));
			responseBuilder.header("Content-Disposition", contentDisposition + "; filename=\"" + Encode.forUriComponent(nf.getName()) + "\"");
			return responseBuilder.build();
		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	// used solely in request body @Schema implementation
	// as of now you cannot tag FormDataParam correctly with SwaggerV3
	static class MultiRequest {
		@Schema(type = "string", format = "binary", description = "File object to upload")
		public String file;
	}

	private SessionEntityContext checkReadAllowed(String boMetaId, String boId) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);
		try {
			if (boId.startsWith("temp_")) {
				Rest.getTemporaryEntityObjectIfReadAllowed(boId);
			} else {
				Rest.facade().get(
						info.getEntity(),
						info.getMasterMeta().isUidEntity() ? UID.parseUID(boId) : new Long(boId),
						true
				);
			}
		} catch (Exception ex) {
			throw NuclosWebException.wrap(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
		return info;
	}
	
	private static class BoDocumentTransactionSynchronization implements TransactionSynchronization {
		
		private static final Logger LOG = LoggerFactory.getLogger(
			BoDocumentTransactionSynchronization.class);
		
		private final List<File> filesToDelete = new ArrayList<>();

		@Override
		public void suspend() {
		}

		@Override
		public void resume() {
		}

		@Override
		public void flush() {
		}

		@Override
		public void beforeCommit(boolean readOnly) {
		}

		@Override
		public void beforeCompletion() {
		}

		@Override
		public void afterCommit() {
			for (File file : filesToDelete) {
				try {
					file.delete();
				} catch (Exception ex) {
					LOG.error(ex.getMessage(), ex);
				}
			}
		}

		@Override
		public void afterCompletion(int status) {
		}
		
	}
	
}
