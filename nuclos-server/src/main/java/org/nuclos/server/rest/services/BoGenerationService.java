package org.nuclos.server.rest.services;

import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.nuclos.schema.rest.EntityObject;
import org.nuclos.server.rest.services.helper.DataServiceHelper;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.enums.ParameterStyle;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/boGenerations")
@Produces(MediaType.APPLICATION_JSON)
public class BoGenerationService extends DataServiceHelper {

	@GET
	@Path("/{boMetaId}")
	@Operation(
			summary = "Businessobject Generation with default values",
			operationId = "boWithDefaultValues",
			description = "Get a new businessobject with default values for inserting",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Businessobject with default values for given meta identifier",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given meta identifier could not be found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Generating the object failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject boWithDefaultValues(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "processMetaId",
					description = "",
					example = "example_rest_Order_Standingorder",
					in = ParameterIn.QUERY,
					style = ParameterStyle.FORM
			)
			@QueryParam("processMetaId") String processMetaId
	) {
		return getBoWithAllDefaultValues(boMetaId, processMetaId).build();
	}

	/**
	 * @return the generated BO
	 * <p>
	 * In case of an error the BO is unsaved, which means the ID is NULL.
	 * The error could be found in the BO under the key "businessError".
	 */
	@POST
	@Path("/{boMetaId}/{boId}/{boVersion}/generate/{generationId}")
	@Operation(
			operationId = "boGeneration",
			summary = "Businessobject Generation",
			description = "Run the generation and returns the BO (in case of an error the BO is unsaved)",
			requestBody = @RequestBody(
					required = true,
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(
									type = "object",
									name = "JsonObject",
									example = "{\"propertyA\": 12345, \"propertyB\": \"example\"}"
							)
					),
					description = "JSON object containing attributes to set on generated business object"
			),
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given meta identifier could not be found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Generating the object failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject boSingleGeneration(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "1000"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "boVersion",
					required = true,
					description = "Businessobject Version",
					example = "1"
			)
			@PathParam("boVersion") int boVersion,
			@Parameter(
					name = "generationId",
					required = true,
					description = "Businessobject Generator Identifier",
					example = "example"
			)
			@PathParam("generationId") String generationId,
			JsonObject data
	) {
		return generateBo(boMetaId, boId, boVersion, generationId, data).build();
	}

}
