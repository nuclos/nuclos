package org.nuclos.server.rest.services;

import java.util.List;

import javax.json.JsonValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;

import org.nuclos.schema.rest.EntityMetaBase;
import org.nuclos.server.rest.services.helper.MetaDataServiceHelper;
import org.nuclos.server.rest.services.helper.RecursiveDependency;
import org.nuclos.server.rest.services.rvo.LayoutInfo;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/boMetas")
@Produces(MediaType.APPLICATION_JSON)
public class BoMetaService<T> extends MetaDataServiceHelper {
	
	@GET
	@Path("/{boMetaId}")
	@Operation(
			operationId = "boMeta",
			summary = "Return meta information for given businessobject-meta identifier",
			description = "Return meta information for given businessobject-meta identifier",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityMetaBase.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given identifier is not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonValue bometaSelf(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId) {
		return getBOMetaSelf(boMetaId).build();
	}
	
	@GET
	@Path("/{boMetaId}/subBos/{refAttrId}")
	@Operation(
			operationId = "referencemeta_self",
			summary = "Get the meta information about one Reference",
			description = "Get the meta information about one Reference",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityMetaBase.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given identifier is not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonValue dependencemetaSelf(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Attribute identifier on businessobject referencing sub businessobject",
					example = "example_rest_Order_customer"
			)
			@PathParam("refAttrId") String refAttrId) {
		return getDependenceMetaSelf(boMetaId, new RecursiveDependency(null, refAttrId, null)).build();
	}

	@GET
	@Path("/{boMetaId}/subBos/recursive/{referenceAttributes:.+}")
	@Operation(
			operationId = "referencemeta_recursive_self",
			summary = "Get the meta information about one Reference in arbitrary depth",
			description = "Get the meta information about one Reference in arbitrary depth",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityMetaBase.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given identifier is not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonValue dependencemetaRecursiveSelf(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Attribute identifier on businessobject referencing sub businessobject",
					example = "example_rest_Order_customer"
			)
			@PathParam("referenceAttributes") List<PathSegment> referenceAttributes) {

		RecursiveDependency dependency = null;

		for (PathSegment referenceAttribute : referenceAttributes) {
			if (!String.valueOf(referenceAttribute).isEmpty()) {
				if (dependency == null) {
					dependency = new RecursiveDependency(null, String.valueOf(referenceAttribute), null);
				} else {
					dependency = dependency.dependency(String.valueOf(referenceAttribute));
				}
			}
		}

		return getDependenceMetaSelf(boMetaId, dependency).build();
	}

	@GET
	@Path("/{boMetaId}/layouts")
	@Operation(
			operationId = "entityLayouts",
			description = "Available layouts for for the given entity",
			summary = "Available layouts for for the given entity",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = LayoutInfo.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given identifier is not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Layouts"
	)
	public List<LayoutInfo> layoutInfosForEntity(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId) {
		return getLayoutInfosForEntity(boMetaId);
	}
}
