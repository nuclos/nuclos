package org.nuclos.server.rest.services;

import java.io.Serializable;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.lang3.ObjectUtils;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.printservice.PrintExecutionContext;
import org.nuclos.common.report.ejb3.ReportFacadeRemote;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintResultTO;
import org.nuclos.common.report.valueobject.PrintoutTO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.schema.rest.PrintoutList;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.printservice.PrintServiceFacadeBean;
import org.nuclos.server.printservice.PrintServiceFacadeLocal;
import org.nuclos.server.printservice.printout.PrintoutServiceProvider;
import org.nuclos.server.printservice.printout.PrintoutWrapper;
import org.nuclos.server.report.ejb3.ReportFacadeBean;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache.CacheResult;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.rvo.CommandsRVO;
import org.nuclos.server.rest.services.rvo.OutputFormatRVO;
import org.nuclos.server.rest.services.rvo.PrintoutRVO;
import org.owasp.encoder.Encode;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/boPrintouts")
@Produces(MediaType.APPLICATION_JSON)
public class BoPrintoutService extends DataServiceHelper {
	
	@GET
	@Path("/{boMetaId}/{boId}")
	@Operation(
			operationId = "boPrintoutList",
			description = "List of all executable Printouts. Send the Printouts back to execute",
			summary = "List of all executable printouts",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = PrintoutList.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given identifiers is not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonArray boPrintoutList(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "1000"
			)
			@PathParam("boId") String boId) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);

		try {
			UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(Long.parseLong(boId), info.getEntity());

			ReportFacadeBean reportFacade = SpringApplicationContextHolder.getBean(ReportFacadeBean.class);
			PrintServiceFacadeBean printService = (PrintServiceFacadeBean) SpringApplicationContextHolder.getBean("printService");
			//Collection<DefaultReportVO> reportsByUsage = reportFacadeRemote.findReportsByUsage(usage);
			List<PrintoutTO> printoutTOs = printService.printoutsForBO(usage, Long.parseLong(boId));

			JsonArrayBuilder jsonarray = Json.createArrayBuilder();
			
			//for (DefaultReportVO reportvo : reportsByUsage) {
			for (PrintoutTO printoutTO : printoutTOs) {
				//if (reportvo.getOutputType() == ReportVO.OutputType.SINGLE) {
				if (printoutTO.getOutputType() == ReportVO.OutputType.SINGLE) {
					DefaultReportVO reportvo = MasterDataWrapper.getReportVO(
							reportFacade.getFormMasterDataByReportUid((UID) printoutTO.getId()),
							Rest.facade().getCurrentUserName(),
							Rest.facade().getCurrentMandatorUID());
					PrintoutRVO printout = new PrintoutRVO(boId, reportvo);
					//for (ReportOutputVO reportoutputvo : reportFacadeRemote.getReportOutputs(reportvo.getId())) {
					for (OutputFormatTO outputFormatTO : printoutTO.getOutputFormats()) {
						//String translatedUid = Rest.translateUid(E.REPORTOUTPUT, (UID)reportoutputvo.getId());
						String translatedUid = Rest.translateUid(E.REPORTOUTPUT, (UID)outputFormatTO.getId());
						
						if (translatedUid != null) {
							// not all output-formats are runnable in server.
							// see also org.nuclos.server.report.PrintoutObjectBuilder.visitOutputFormats(UID, OutputFormatVisitor)
							// ??????????????????????????????????????????????
							//OutputFormatRVO outputformat = new OutputFormatRVO(reportoutputvo);
							OutputFormatRVO outputformat = new OutputFormatRVO(outputFormatTO,
									ObjectUtils.defaultIfNull(outputFormatTO.getDatasourceId(), (UID)printoutTO.getDatasourceId()));
							printout.addOutputFormat(outputformat);
							
						}
					}
					jsonarray.add(printout.getJSONObjectBuilder());
				}
			}
			
			return jsonarray.build();
			
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}
	
	@POST
	@Path("/{boMetaId}/{boId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "boPrintoutExecution",
			summary = "Executing printouts",
			description = "Execute the posted printouts, returns the generated file list",
			requestBody = @RequestBody(
					required = true,
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(
									implementation = PrintoutList.class
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = PrintoutList.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given identifiers is not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonArray boPrintoutExecution(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "1000"
			)
			@PathParam("boId") String boId,
			JsonArray printoutListData) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);

		try {
			Long primaryKey = Long.parseLong(boId);
			UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(primaryKey, info.getEntity());

			ReportFacadeRemote reportFacadeRemote = SpringApplicationContextHolder.getBean(ReportFacadeRemote.class);
			PrintoutServiceProvider printoutProvider = SpringApplicationContextHolder.getBean(PrintoutServiceProvider.class);
			RestPrintoutFileDownloadCache downloadCache = SpringApplicationContextHolder.getBean(RestPrintoutFileDownloadCache.class);
			PrintServiceFacadeLocal printFacade = SpringApplicationContextHolder.getBean(PrintServiceFacadeLocal.class);
			PrintoutWrapper printoutWrapper = SpringApplicationContextHolder.getBean(PrintoutWrapper.class);

			final List<PrintoutRVO> result = new ArrayList<PrintoutRVO>();
			final List<PrintoutTO> printouts = new ArrayList<PrintoutTO>(); 
			
			for (int i = 0; i < printoutListData.size(); i++) {
				final JsonObject printoutJson = printoutListData.getJsonObject(i);
				final String printoutFqn = printoutJson.getString("printoutId");
				final UID printoutUid = Rest.translateFqn(E.REPORT, printoutFqn);
				
				// query ReportVO
				final EntityObjectVO<UID> reportEO = NucletDalProvider.getInstance().getEntityObjectProcessor(E.REPORT).getByPrimaryKey(printoutUid);
				final UID mandator = Rest.facade().getCurrentMandatorUID();
				final DefaultReportVO reportVO = MasterDataWrapper.getReportVO(new MasterDataVO<>(reportEO, true),this.getUser(),mandator);
				final PrintoutRVO printoutResult = new PrintoutRVO(boId, reportVO);
				final PrintoutTO printout = new PrintoutTO(printoutUid, reportVO.getName(), reportVO.getOutputType(), reportVO.getDatasourceId());
				printout.getOutputFormats().clear();
				printout.setBusinessObjectId(primaryKey);
				
				boolean add = false;
				if (reportVO.getOutputType() == ReportVO.OutputType.SINGLE) {
					if (printoutJson.containsKey("outputFormats")) {
						JsonArray outputformatListJson = printoutJson.getJsonArray("outputFormats");
						for (int j = 0; j < outputformatListJson.size(); j++) {
							add = true;
							JsonObject outputformatJson = outputformatListJson.getJsonObject(j);
							OutputFormatRVO outputFormat = OutputFormatRVO.getOutputFormat(outputformatJson, reportVO, reportFacadeRemote);
							DefaultReportOutputVO reportOutputFormat = reportFacadeRemote.getReportOutput(outputFormat.getUID());
							OutputFormatTO wrapTO = printoutWrapper.wrapTO(reportOutputFormat);
							// TODO printout webclient
							// wrapTO.setProperties(...);
							wrapTO.getDatasourceParams().putAll(outputFormat.getParams());
							if (reportVO.getAttachDocument()) {
								// Collective report output type
								wrapTO.setAttachDocument(true);
							}
							printoutResult.addOutputFormat(outputFormat);
							printout.getOutputFormats().add(wrapTO);
						}
					}
				} else {
					for (ReportOutputVO reportoutputvo : reportFacadeRemote.getReportOutputs(printoutUid)) {
						add = true;
						printoutResult.addOutputFormat(new OutputFormatRVO(reportoutputvo, ObjectUtils.defaultIfNull(reportoutputvo.getDatasourceUID(), reportVO.getDatasourceId())));
						printout.getOutputFormats().add(printoutWrapper.wrapTO(reportoutputvo));
					}
				}
				
				if (add) {
					result.add(printoutResult);
					printouts.add(printout);
				}
			}
			
			final UID attachmentSubEntityUID = null; // Used from client extensions only
			final UID[] attachmentSubEntityAttributeUIDs = null; // Used from client extensions only
			final PrintExecutionContext context = new PrintExecutionContext(usage, primaryKey, attachmentSubEntityUID, attachmentSubEntityAttributeUIDs);
			final FieldMeta<?> fmVersion = SF.VERSION.getMetaData(Rest.translateFqn(E.ENTITY, boMetaId));
			final int versionOld = Optional.ofNullable(Rest.facade().getDataForField(primaryKey, fmVersion)).orElseThrow(CommonFinderException::new).getVersion();
			// executes print final rules...
			final List<PrintResultTO> executedPrintouts = printFacade.executePrintout(printouts, context);
			final int version = Optional.ofNullable(Rest.facade().getDataForField(primaryKey, fmVersion)).orElseThrow(CommonFinderException::new).getVersion();

			final CommandsRVO commands = getCommands();

			final JsonArrayBuilder jsonarray = Json.createArrayBuilder();
			
			for (PrintoutRVO printoutResult : result) {
				for (OutputFormatRVO outputFormatResult : printoutResult.getOutputFormats()) {
					for (PrintResultTO printed : executedPrintouts) {
						String outputFormatFqn = Rest.translateUid(E.REPORTOUTPUT, printed.getOutputFormat().getId());
						if (RigidUtils.equal(outputFormatFqn, outputFormatResult.getId())) {
							NuclosFile file = printed.getOutput();
							String fileId = downloadCache.cacheFile(boMetaId, boId, outputFormatResult.getId(), file);
							
							OutputFormatRVO.OutputFile outputFileResult = new OutputFormatRVO.OutputFile(boMetaId, boId, file.getName(), fileId, BoPrintoutService.this);
							outputFormatResult.setFile(outputFileResult);
							break;
						}						
					}
				}
				printoutResult.boChanged(versionOld != version);
				printoutResult.setCommands(commands);
				jsonarray.add(printoutResult.getJSONObjectBuilder());
			}
			
			return jsonarray.build();
			
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}

	private static CommandsRVO getCommands() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		boolean bRequestHasCommands = Collections.list(request.getAttributeNames()).stream().anyMatch("CMD"::equals);

		CommandsRVO cmds = new CommandsRVO(new ArrayList<>());

		if (bRequestHasCommands) {
			final ArrayList<Serializable> commands = (ArrayList<Serializable>) request.getAttribute("CMD");
			if (commands != null) {
				for (Serializable object : commands) {
					CommandsRVO.addCommand(cmds, object);
				}
			}
		}

		return cmds;
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/{boId}/{outputFormatId}/files/{fileId}")
	@Operation(
			operationId = "boPrintoutFile",
			description = "Get the Printout file",
			summary = "Get the Printout file",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "application/*"
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given file is not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public Response boPrintoutFile(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "outputFormatId",
					required = true,
					description = "Printout format identifier",
					example = "123olsa12o3"
			)
			@PathParam("outputFormatId") String outputFormatId,
			@Parameter(
					name = "fileId",
					required = true,
					description = "File identifier",
					example = "897123asd12jkl"
			)
			@PathParam("fileId") String fileId) {
		checkPermission(E.ENTITY, boMetaId, boId);

		RestPrintoutFileDownloadCache downloadCache = SpringApplicationContextHolder.getBean(RestPrintoutFileDownloadCache.class);
		ReportFacadeRemote reportFacadeRemote = SpringApplicationContextHolder.getBean(ReportFacadeRemote.class);
		try {
			CacheResult cached = downloadCache.get(boMetaId, boId, outputFormatId, fileId);
			ReportOutputVO.Destination destination =
					reportFacadeRemote.getReportOutput(Rest.translateFqn(E.REPORTOUTPUT, outputFormatId)).getDestination();
			String contentDisposition = ReportOutputVO.Destination.SCREEN.equals(destination) ? "inline" : "attachment";

			if (cached != null) {
				ResponseBuilder responseBuilder = Response.ok(cached.content).type(URLConnection.guessContentTypeFromName(cached.fileName));
				responseBuilder.header("Content-Disposition", contentDisposition + "; filename=\"" + Encode.forUriComponent(cached.fileName) + "\"");
				return responseBuilder.build();

			}

			return Response.status(Response.Status.NOT_FOUND).build();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}
	
}
