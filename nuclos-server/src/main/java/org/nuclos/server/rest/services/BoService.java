package org.nuclos.server.rest.services;

import java.net.URLConnection;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.nuclos.common.E;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.schema.rest.EntityMetaBase;
import org.nuclos.schema.rest.EntityObject;
import org.nuclos.schema.rest.EntityObjectResultList;
import org.nuclos.schema.rest.InputRequiredContext;
import org.nuclos.schema.rest.QueryContext;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache.CacheResult;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.rvo.ResultListExportRVO;
import org.owasp.encoder.Encode;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/bos")
@Produces(MediaType.APPLICATION_JSON)
public class BoService extends DataServiceHelper {

	@GET
	@Operation(
			operationId = "boMetas",
			summary = "Return list of available business object meta information",
			description = "Return list of available business object meta information",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = EntityMetaBase.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public List<EntityMetaBase> bometaList() {
		return getBOMetaList();
	}

	@GET
	@Path("/{boMetaId}")
	@Operation(
			operationId = "bos",
			description = "List of Data (Rows)",
			summary = "List of Data (Rows)",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObjectResultList.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject bolist(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId) {
		return getBoList(boMetaId, null).build();
	}

	@GET
	@Path("/{boMetaId}/query")
	@Operation(
			operationId = "bosQuery",
			description = "List of Data (Rows) by query object",
			summary = "List of Data (Rows) by query object",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObjectResultList.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "406",
							description = "Search clause is not parsable"
					),
					@ApiResponse(
							responseCode = "417",
							description = "Search clause contains wrong where"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject getBolist(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "where",
					in = ParameterIn.QUERY,
					required = true,
					description = "Query for searching objects",
					example = "example_rest_Article_price=30&withTitleAndInfo=false&attributes=articleNumber,name,price,active&sort=example_rest_Article_articleNumber+desc&skipStatesAndGenerations=true&offset=0&chunkSize=100&countTotal=true&searchFilterId=&taskListId="
			)
			@QueryParam("where") String where) {
		JsonObjectBuilder queryContext = Optional.ofNullable(where)
				.map(this::getQueryContextFromParam)
				.orElse(Json.createObjectBuilder());
		return getBoList(boMetaId, queryContext.build()).build();
	}

	private JsonObjectBuilder getQueryContextFromParam(final String where) {
		JsonObjectBuilder queryContext = Json.createObjectBuilder();
		if (where == null) return queryContext;

		if (where.contains("&")) {
			Arrays.stream(where.split("&"))
					.forEach((attribute) -> parseAttributeFromQuery(queryContext, attribute));
		} else {
			// maybe only one attribute defintion
			this.parseAttributeFromQuery(queryContext, where);
		}
		return queryContext;
	}

	private void parseAttributeFromQuery(final JsonObjectBuilder queryContext, final String attribute) {
		if (attribute.contains("=")) {
			int indexOfEqual = attribute.indexOf("=");
			final String key = attribute.substring(0, indexOfEqual);
			final String value = indexOfEqual == attribute.length() ? "" :
					attribute.substring(indexOfEqual + 1);
			// only inspect keys with values
			if (value.equals("true") || value.equals("false")) {
				queryContext.add(key, Boolean.parseBoolean(value));
			} else if (value.contains(",")) {
				JsonArrayBuilder array = Json.createArrayBuilder();
				Arrays.stream(value.split(",")).forEach(array::add);
				queryContext.add(key, array);
			} else if (value.matches("[0-9]+")) {
				// Long is the last known type we want
				queryContext.add(key, Long.parseLong(value));
			} else {
				queryContext.add(key, value);
			}
		}
	}

	//TODO: Both "query"-Methods have to be revalidated. First, they have the same signature with the finalized method:
	//
	//@GET
	//@Path("/{boMetaId}/{boId}")
	//@RestServiceInfo(identifier="bo", isFinalized=true, description="Full Data Row Details")
	//
	//Furthermore it is not HATEOAS Standard if a query-only service is a POST
	//
	//It's important that NOT-finalized Signatures do not have any fixed hard-coded links in the webclient as they may change in future.

	@POST
	@Path("/{boMetaId}/query")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "bosQueryPost",
			description = "List of Data (Rows) by query object",
			summary = "List of Data (Rows) by query object",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObjectResultList.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "406",
							description = "Search clause is not parsable"
					),
					@ApiResponse(
							responseCode = "417",
							description = "Search clause contains wrong where"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject bolist(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		return getBoList(boMetaId, queryContext).build();
	}


	@POST
	@Path("/{boMetaId}/boListExport/{format}/{pageOrientationLandscape}/{isColumnScaled}")
	@Operation(
			operationId = "boListExport",
			description = "Export list of data as pdf/csv/xls",
			summary = "Export list of data as pdf/csv/xls",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = ResultListExportRVO.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	@Consumes({MediaType.APPLICATION_JSON})
	public ResultListExportRVO boListExport(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "format",
					required = true,
					description = "Format of export",
					schema = @Schema(
							type = "string",
							allowableValues = {
									"pdf",
									"csv",
									"xls"
							}
					)
			)
			@PathParam("format") String format,
			@Parameter(
					name = "pageOrientationLandscape",
					required = true,
					description = "Should export be in orientation landscape",
					schema = @Schema(
							type = "bool"
					)
			)
			@PathParam("pageOrientationLandscape") boolean pageOrientationLandscape,
			@Parameter(
					name = "isColumnScaled",
					required = true,
					description = "Should export columns be scaled",
					schema = @Schema(
							type = "bool"
					)
			)
			@PathParam("isColumnScaled") boolean isColumnScaled,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		return getBoListExport(boMetaId, format, pageOrientationLandscape, isColumnScaled, queryContext);
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/boListExport/{outputFormatId}/{fileId}")
	@Operation(
			operationId = "boListExportFile",
			description = "Get the exported file",
			summary = "Get the exported file",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "application/octet-stream"
							)
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given export could not be found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public Response boListExportFile(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "outputFormatId",
					required = true,
					description = "Identifier of output format",
					example = "abcd123"
			)
			@PathParam("outputFormatId") String outputFormatId,
			@Parameter(
					name = "fileId",
					required = true,
					description = "File Identifier",
					example = "abcd123"
			)
			@PathParam("fileId") String fileId) {

		RestPrintoutFileDownloadCache downloadCache = SpringApplicationContextHolder.getBean(RestPrintoutFileDownloadCache.class);
		try {

			CacheResult cached = downloadCache.get(boMetaId, null, outputFormatId, fileId);

			if (cached != null) {
				ResponseBuilder responseBuilder = Response.ok(cached.content).type(URLConnection.guessContentTypeFromName(cached.fileName));
				responseBuilder.header("Content-Disposition", "attachment; filename=\"" + Encode.forUriComponent(cached.fileName) + "\"");
				return responseBuilder.build();
			}

			return Response.status(Response.Status.NOT_FOUND).build();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}


	@POST
	@Path("/{boMetaId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "boInsert",
			description = "Data Row Insert",
			summary = "Data Row Insert",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "412",
							description = "Business error or required input parameter is not specified"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject boinsert(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					schema = @Schema(
							implementation = EntityObject.class
					)
			) JsonObject data
	) {
		return insertBo(boMetaId, data).build();
	}

	@GET
	@Path("/{boMetaId}/{boId}")
	@Operation(
			operationId = "bo",
			description = "Full Data Row Details",
			summary = "Full Data Row Details",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "412",
							description = "Business error or required input parameter is not specified"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject bodetails(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId) {
		return getBoSelf(boMetaId, boId).build();
	}

	@PUT
	@Path("/{boMetaId}/{boId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "boUpdate",
			description = "Full Data Row Update",
			summary = "Full Data Row Update",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "412",
							description = "Business error or required input parameter is not specified"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject boupdate(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					schema = @Schema(
							implementation = EntityObject.class
					)
			) JsonObject data
	) {
		JsonObjectBuilder objectBuilder = updateBo(boId, data);
		if (objectBuilder != null) {
			return objectBuilder.build();
		}
		return null;
	}

	@DELETE
	@Path("/{boMetaId}/{boId}")
	@Operation(
			operationId = "bodelete",
			description = "Full Data Row Delete",
			summary = "Full Data Row Delete",
			tags = "Nuclos Businessobjects",
			responses = {
					@ApiResponse(
							responseCode = "204"
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "412",
							description = "Business error or required input parameter is not specified"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public Response bodelete(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId) {
		deleteBo(boMetaId, boId, null);
		return Response.status(Response.Status.NO_CONTENT).build();
	}

	@POST
	@Path("/{boMetaId}/{boId}")
	@Operation(
			operationId = "bodeleteWithInputRequired",
			description = "Full Data Row Delete, with support for InputRequiredException",
			summary = "Full Data Row Delete, with support for InputRequiredException",
			tags = "Nuclos Businessobjects",
			responses = {
					@ApiResponse(
							responseCode = "204"
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "412",
							description = "Business error or required input parameter is not specified"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	@Consumes({MediaType.APPLICATION_JSON})
	public Response bodelete(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					schema = @Schema(
							implementation = InputRequiredContext.class
					)
			) JsonObject data) {
		deleteBo(boMetaId, boId, data);
		return Response.status(Response.Status.NO_CONTENT).build();
	}

	@GET
	@Path("/{boMetaId}/{boId}/clone")
	@Operation(
			operationId = "boclone",
			description = "Full Data Row Clone",
			summary = "Full Data Row Clone",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "412",
							description = "Business error or required input parameter is not specified"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject boclone(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "layoutid",
					required = true,
					description = "Layout Identifier",
					example = "example"
			)
			@QueryParam("layoutid") String layoutid
	) {
		JsonObject emptyData = Json.createObjectBuilder().build();
		return cloneBo(boMetaId, boId, layoutid, emptyData).build();
	}

	@POST
	@Path("/{boMetaId}/{boId}/clone")
	@Operation(
			operationId = "boclone",
			description = "Full Data Row Clone",
			requestBody = @RequestBody(
					required = true,
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(
									type = "object",
									name = "JsonObject",
									example = "{\"propertyA\": 12345, \"propertyB\": \"example\"}"
							)
					),
					description = "JSON object containing attributes to set on generated business object"
			),
			summary = "Full Data Row Clone",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "412",
							description = "Business error or required input parameter is not specified"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject boclone(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "layoutid",
					required = true,
					description = "Layout Identifier",
					example = "example"
			)
			@QueryParam("layoutid") String layoutid,
			JsonObject data
	) {

		return cloneBo(boMetaId, boId, layoutid, data).build();
	}

	@DELETE
	@Path("/{boMetaId}/{boId}/lock")
	@Operation(
			operationId = "bos/{boMetaId}/{boId}/lock",
			description = "Unlocks the Row",
			summary = "Unlocks the Row",
			tags = "Nuclos Businessobjects",
			responses = {
					@ApiResponse(
							responseCode = "204"
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "412",
							description = "Business error or required input parameter is not specified"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public Response bounlock(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId) {
		unlockBo(boMetaId, boId);
		return Response.status(Response.Status.NO_CONTENT).build();
	}

	@POST
	@Path("/{boMetaId}/{boId}/execute/{ruleId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "boExecuteRule",
			description = "Executes a custom rule.",
			summary = "Executes a custom rule.",
			requestBody = @RequestBody(
					required = true,
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(
									type = "object",
									name = "JsonObject",
									example = "{\"propertyA\": 12345, \"propertyB\": \"example\"}"
							)
					),
					description = "JSON object containing attributes to set on generated bussinesobject"
			),
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "412",
							description = "Business error or required input parameter is not specified"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject executeCustomRule(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "ruleId",
					required = true,
					description = "Rule Identifier",
					example = "example_custom_Rule"
			)
			@PathParam("ruleId") String ruleId,
			@Parameter() final JsonObject data
	) {
		return executeRule(boMetaId, boId, ruleId, data).build();
	}
}
