package org.nuclos.server.rest.services;

import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.schema.rest.InputRequiredContext;
import org.nuclos.server.rest.services.helper.DataServiceHelper;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/boStateChanges")
public class BoStateService extends DataServiceHelper {

	@PUT
	@Path("/{boMetaId}/{boId}/{stateId}")
	@Operation(
			operationId = "boStateChange",
			description = "Change status of BO",
			summary = "Change status of BO",
			tags = "Nuclos Businessobjects",
			responses = {
					@ApiResponse(
							responseCode = "204"
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "412",
							description = "Business error or required input parameter is not specified"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response boStateChange(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "stateId",
					required = true,
					description = "Businessobject State Identifier",
					example = "10090"
			)
			@PathParam("stateId") String statusIdOrNumeral,
			@Parameter(
					schema = @Schema(
							implementation = InputRequiredContext.class
					)
			) JsonObject data
	) {
		stateChange(boMetaId, boId, statusIdOrNumeral, data);
		return Response.status(Response.Status.NO_CONTENT).build();
	}

}
