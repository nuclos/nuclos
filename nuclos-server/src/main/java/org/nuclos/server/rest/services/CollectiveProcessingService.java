package org.nuclos.server.rest.services;

import static org.nuclos.common.StreamUtils.not;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.rest.CollectiveProcessingAction;
import org.nuclos.schema.rest.CollectiveProcessingActionLinks;
import org.nuclos.schema.rest.CollectiveProcessingExecution;
import org.nuclos.schema.rest.CollectiveProcessingExecutionLinks;
import org.nuclos.schema.rest.CollectiveProcessingObjectInfoInputrequired;
import org.nuclos.schema.rest.CollectiveProcessingProgressInfo;
import org.nuclos.schema.rest.CollectiveProcessingSelectionOptions;
import org.nuclos.schema.rest.QueryContext;
import org.nuclos.schema.rest.RestLink;
import org.nuclos.server.common.RecordGrantRight;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.helper.ActionsAllowed;
import org.nuclos.server.rest.services.helper.CollectiveProcessingExecutionEntry;
import org.nuclos.server.rest.services.helper.CollectiveProcessingExecutionService;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.UserAction;
import org.nuclos.server.rest.services.rvo.FilterJ;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/collectiveProcessing")
@Produces(MediaType.APPLICATION_JSON)
@ActionsAllowed(UserAction.COLLECTIVE_PROCESSING)
public class CollectiveProcessingService extends DataServiceHelper {

	private final ConcurrentMap<UID, CollectiveProcessingExecutionEntry> executions = new ConcurrentHashMap<>();

	@Autowired
	private CollectiveProcessingExecutionService executionService;

	@POST
	@Path("/{boMetaId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "selectionOptions",
			description = "Selection options for a possible collection processing",
			summary = "Selection options for a possible collection processing",
			tags = "Nuclos Collective Processing",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = CollectiveProcessingSelectionOptions.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public CollectiveProcessingSelectionOptions selectionOptions(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);
		final CollectiveProcessingSelectionOptions.Builder result = CollectiveProcessingSelectionOptions.builder();

		try {
			final FilterJ filter = getFilterJ(queryContext, -1L);
			checkFilter(filter);
			final List<FieldMeta<?>> fields = Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());

			final CollectableSearchExpression clctexpr = filter.getSearchExpression(info.getEntity(), fields);
			final Set<UsageCriteria> usageCriterias = Rest.facade().getUsageCriteriaBySearchExpression(info.getEntity(), clctexpr);
			final Set<RecordGrantRight> recordGrantRights = Rest.facade().getRecordGrantRightBySearchExpression(info.getEntity(), clctexpr);
			final Set<UID> nuclosOwners = Rest.facade().getNuclosOwnerBySearchExpression(info.getEntity(), clctexpr);
			final UID currentUserId = Rest.facade().getCurrentUserId();
			final boolean onlyMyOwnRecordsOrUnOwneredSelected = nuclosOwners.stream().allMatch(userId -> UID.UID_NULL.equals(userId) || userId.equals(currentUserId));

			if (nuclosOwners.isEmpty() || onlyMyOwnRecordsOrUnOwneredSelected) {
				result.addActions(getActions(boMetaId, recordGrantRights));
				result.addStateChanges(getStateChangeActionsForCurrentUser(boMetaId, usageCriterias, recordGrantRights));
				result.addCustomRules(getCustomRulesActionsForCurrentUser(boMetaId, usageCriterias, recordGrantRights));
				result.addGenerators(getGenerationsForCurrentUser(boMetaId, usageCriterias, recordGrantRights));
			}
		} catch (CommonBusinessException e) {
			throw NuclosWebException.wrap(e, info.getEntity());
		}
		return result.build();
	}

	private List<CollectiveProcessingAction> getActions(String boMetaId, Set<RecordGrantRight> recordGrantRights) {
		final SortedMap<String, CollectiveProcessingAction> actions = new TreeMap<>();

		// TODO verify with single target
		if (recordGrantRights.stream().noneMatch(not(RecordGrantRight::canWrite))
				&& getBOMeta().isCollectiveEditEnabled() && Rest.facade().isActionAllowed(Actions.ACTION_BULKEDIT)) {
			actions.put("edit",
					CollectiveProcessingAction.builder()
							.withName("edit")
							.withLinks(CollectiveProcessingActionLinks.builder()
									.withExecute(RestLink.builder()
											.withMethods("POST")
											.withHref(getRestURI("collectiveProcessingExecuteEditAttributes",
													boMetaId))
											.build())
									.withInfo(RestLink.builder()
											.withMethods("POST")
											.withHref(getRestURI("collectiveProcessingEditAttributes",
													boMetaId))
											.build())
									.build())
							.build()
			);
		}

		if (recordGrantRights.stream().noneMatch(not(RecordGrantRight::canDelete))) {
			actions.put("delete",
					CollectiveProcessingAction.builder()
							.withName("delete")
							.withLinks(CollectiveProcessingActionLinks.builder()
									.withExecute(RestLink.builder()
											.withMethods("POST")
											.withHref(getRestURI("collectiveProcessingDelete",
													boMetaId))
											.build())
									.build())
							.build()
					);
		}

		return new ArrayList<>(actions.values());
	}

	private List<CollectiveProcessingAction> getGenerationsForCurrentUser(String boMetaId, Set<UsageCriteria> usageCriterias, Set<RecordGrantRight> recordGrantRights) {
		final SortedMap<String, CollectiveProcessingAction> sortedMap = new TreeMap<>();

		// TODO verify with single target
		if (recordGrantRights.stream().anyMatch(not(RecordGrantRight::canWrite))) {
			return Collections.emptyList();
		}

		final List<GeneratorActionVO> generators = usageCriterias
				.stream()
				.flatMap(uc ->
						Optional.ofNullable(
								Rest.facade().getGenerations(uc, getMandatorUID())
						)
						.map(Collection::stream)
						.orElseGet(Stream::empty)
				)
				.filter(GeneratorActionVO::showInCollectiveProcessing)
				.distinct()
				.collect(Collectors.toList());

		generators.forEach(gen -> {
			try {
				sortedMap.put(gen.getName(),
						CollectiveProcessingAction.builder()
								.withName(gen.getLabel() == null ? gen.getName() : gen.getLabel())
								.withSubName("Generator")
								.withLinks(CollectiveProcessingActionLinks.builder()
										.withExecute(
												RestLink.builder()
														.withMethods("POST")
														.withHref(getRestURI("collectiveProcessingGeneration",
																boMetaId,
																Rest.translateUid(E.GENERATION, gen.getId())))
														.build())
										.build())
								.build());
			} catch (Exception e) {
				throw new NuclosFatalException(e);
			}
		});

		return new ArrayList<>(sortedMap.values());
	}

	private List<CollectiveProcessingAction> getCustomRulesActionsForCurrentUser(String boMetaId, Set<UsageCriteria> usageCriterias, Set<RecordGrantRight> recordGrantRights) {
		final SortedMap<String, CollectiveProcessingAction> sortedMap = new TreeMap<>();

		// TODO verify with single target
		if (recordGrantRights.stream().anyMatch(not(RecordGrantRight::canWrite))) {
			return Collections.emptyList();
		}

		final List<EventSupportSourceVO> customRules = usageCriterias
				.stream()
				.flatMap(uc -> Rest.facade().getCustomRules(uc).stream())
				.distinct()
				.filter(EventSupportSourceVO::showInCollectiveProcessing)
				.collect(Collectors.toList());

		customRules.forEach(rule -> {
			try {
				String clazzName = rule.getClassname();
				String sNameResId = rule.getLocaleResourceName();
				String sDescrptId = rule.getLocaleResourceDescription();

				String labelName = null;
				String description = null;

				if (sNameResId != null) {
					labelName = Rest.getResource(sNameResId);
				}

				if (sDescrptId != null) {
					description = Rest.getResource(sDescrptId);
				}

				if (StringUtils.looksEmpty(labelName)) {
					labelName = rule.getName();
				}

				sortedMap.put(labelName,
						CollectiveProcessingAction.builder()
						.withName(labelName)
						.withSubName("RULE")
						.withDescription(description)
						.withLinks(CollectiveProcessingActionLinks.builder()
								.withExecute(
									RestLink.builder()
										.withMethods("POST")
										.withHref(getRestURI("collectiveProcessingCustomRuleExecute",
												boMetaId,
												clazzName))
										.build())
								.build())
						.build());
			} catch (Exception e) {
				throw new NuclosFatalException(e);
			}
		});

		return new ArrayList<>(sortedMap.values());
	}

	private List<CollectiveProcessingAction> getStateChangeActionsForCurrentUser(String boMetaId, Set<UsageCriteria> usageCriterias, Set<RecordGrantRight> recordGrantRights) {
		final SortedMap<Integer, CollectiveProcessingAction> sortedMap = new TreeMap<>();

		// Check a possible record grant CANSTATECHANGE column also. If any 'canStateChange' is false, disable all state changes for the current collective processing
		if (recordGrantRights.stream().anyMatch(not(RecordGrantRight::canStateChange))) {
			return Collections.emptyList();
		}

		final Set<UID> stateIds = usageCriterias.stream().filter(uc -> uc.getStatusUID() != null)
				.map(uc -> uc.getStatusUID()).collect(Collectors.toSet());
		// Change state support only for the same source state
		if (stateIds.size() == 1) {
			final Locale locale = getLocale();
			final Set<UID> allowed = new HashSet<>(Rest.facade().getAllowedStateTransitions());
			final Collection<StateTransitionVO> transitions = Rest.facade().getStateTransitions(
					new UsageCriteria(null, null, stateIds.iterator().next(), null))
					.stream().filter(vo -> allowed.contains(vo.getId())).collect(Collectors.toList());
			transitions.stream().forEach(transition -> {
				try {
					final StateVO state = Rest.facade().getState(transition.getStateTargetUID());
					String label = StringUtils.defaultIfNull(transition.getLabel(locale), state.getButtonLabel(locale));
					if (StringUtils.looksEmpty(label)) {
						label = state.getStatename(locale);
					}
					String description = StringUtils.defaultIfNull(transition.getDescription(locale), state.getDescription(locale));
					sortedMap.put(state.getNumeral(),
							CollectiveProcessingAction.builder()
							.withName(label)
							.withSubName(state.getNumeral().toString())
							.withColor(state.getColor())
							.withDescription(description)
							.withWithoutConfirmation(transition.isNonstop())
							.withLinks(CollectiveProcessingActionLinks.builder()
									.withExecute(
										RestLink.builder()
											.withMethods("POST")
											.withHref(getRestURI("collectiveProcessingStateChange",
													boMetaId,
													Rest.translateUid(E.STATETRANSITION, transition.getId())))
											.build())
									.build())
							.build());
				} catch (CommonFinderException e) {
					throw new NuclosFatalException(e);
				}
			});
		}

		return new ArrayList<>(sortedMap.values());
	}

	private CollectiveProcessingExecution getCollectiveProcessingExecutionResult(final String executionId) {
		return CollectiveProcessingExecution.builder()
				.withLinks(CollectiveProcessingExecutionLinks.builder()
						.withProgress(
								RestLink.builder()
										.withMethods("GET")
										.withHref(getRestURI("collectiveProcessingProgress", executionId))
										.build())
						.withAbort(
								RestLink.builder()
										.withMethods("GET")
										.withHref(getRestURI("collectiveProcessingAbort", executionId))
										.build())
						.build())
				.build();
	}

	private boolean canEditField(FieldMeta fm) {
		return !((!parameterProvider.isEnabled(ParameterProvider.MODIFIABLE_READONLY) && fm.isReadonly())
				|| !fm.isModifiable()

				|| fm.isSystemField()
				|| fm.isPrimaryKey()
				|| fm.getUID().equals(SF.CREATEDBY.getUID(fm.getEntity()))
				|| fm.getUID().equals(SF.CREATEDAT.getUID(fm.getEntity()))
				|| fm.getUID().equals(SF.CHANGEDBY.getUID(fm.getEntity()))
				|| fm.getUID().equals(SF.CHANGEDAT.getUID(fm.getEntity())));
	}

	private Stream<FieldMeta<?>> getBOWritableFields() {
		return Rest.getAllEntityFieldsByEntityIncludingVersion(getBOMeta().getUID()).values()
				.stream()
				.filter(
						f -> (
								!f.isHidden() &&
										!f.isUnique() &&
										!"Image".equals(Rest.simpleDataType(f.getDataType())) &&
										!"nuclosDeleted".equals(f.getFieldName()) &&
										!"nuclosStateIcon".equals(f.getFieldName()) &&
										!"nuclosState".equals(f.getFieldName()) &&
										canEditField(f)
						)
				);
	}

	private JsonArrayBuilder createArrayObjectFromList(final List<Object> fieldValues) {
		JsonArrayBuilder arraryBuilder = Json.createArrayBuilder();
		if (!fieldValues.isEmpty()) {
			fieldValues.stream().collect(
					Collectors.groupingBy(
							Function.identity(),
							Collectors.counting()
					)
			).forEach((r, l) -> {
				arraryBuilder.add(Json.createObjectBuilder()
						.add("value", r.toString())
						.add("count", l)
						.build());
			});
		}

		return arraryBuilder;
	}

	@POST
	@Path("/{boMetaId}/executeCustomRule/{clazzName}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "collectiveProcessingCustomRuleExecute",
			description = "Change status of selected BOs",
			summary = "Change status of selected BOs",
			tags = "Nuclos Collective Processing",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = CollectiveProcessingExecution.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public CollectiveProcessingExecution collectiveProcessingExecuteCustomRule(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "clazzName",
					required = true,
					description = "Class name of rule to execute",
					example = "example_rest_Article"
			)
			@PathParam("clazzName") String clazzName,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);

		try {
			final FilterJ filter = getFilterJ(queryContext, -1L);
			final List<FieldMeta<?>> fields = Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());
			checkFilter(filter);

			final String executionId = executionService.newExecutionId();
			final CollectiveProcessingExecutionEntry cacheEntry = executionService.createCacheEntry(executionId, getUser());
			CollectiveProcessingExecution result = getCollectiveProcessingExecutionResult(executionId);

			final CollectableSearchExpression clctexpr = filter.getSearchExpression(info.getEntity(), fields);
			executionService.executeCustomRule(cacheEntry, clctexpr, info.getEntity(), clazzName);

			return result;
		} catch (CommonBusinessException e) {
			throw NuclosWebException.wrap(e, info.getEntity());
		}
	}

	@POST
	@Path("/{boMetaId}/stateChange/{transitionId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "collectiveProcessingStateChange",
			description = "Change status of selected BOs",
			summary = "Change status of selected BOs",
			tags = "Nuclos Collective Processing",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = CollectiveProcessingExecution.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public CollectiveProcessingExecution collectiveProcessingStateChange(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "transitionId",
					required = true,
					description = "Businessobject Status Identifier",
					example = "aktiv"
			)
			@PathParam("transitionId") String transitionId,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);

		try {
			final UID transitionUID = Rest.facade().translateFqn(E.STATETRANSITION, transitionId);
			final FilterJ filter = getFilterJ(queryContext, -1L);
			final List<FieldMeta<?>> fields = Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());
			checkFilter(filter);

			final String executionId = executionService.newExecutionId();
			final CollectiveProcessingExecutionEntry cacheEntry = executionService.createCacheEntry(executionId, getUser());

			final CollectableSearchExpression clctexpr = filter.getSearchExpression(info.getEntity(), fields);
			executionService.executeStateChanges(cacheEntry, clctexpr, info.getEntity(), transitionUID);

			return getCollectiveProcessingExecutionResult(executionId);
		} catch (Exception ex) {
			throw NuclosWebException.wrap(ex, info.getEntity());
		}
	}

	@POST
	@Path("/{boMetaId}/editAttributes/{attributeId}")
	@ActionsAllowed(UserAction.BULK_EDIT)
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "collectiveProcessingEditAttributesValues",
			description = "Retrieve grouped values for specific attribute",
			summary = "Retrieve grouped values for specific attribute",
			tags = "Nuclos Collective Processing",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											type = "object",
											requiredProperties = {
													"fieldValues",
													"fieldIdValues"
											}
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public JsonValue collectiveProcessingEditAttributeValues(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "attributeId",
					required = true,
					description = "Businessobject Attribute Identifier",
					example = "example_rest_Order_customer"
			)
			@PathParam("attributeId") String attributeId,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);
		final FilterJ filter = getFilterJ(queryContext, -1L);

		try {
			UID fieldUID = Rest.facade().translateFqn(E.ENTITYFIELD, attributeId);
			FieldMeta<?> desiredField = Rest.getEntityField(fieldUID);

			if (desiredField == null) {
				throw new NuclosWebException(Response.Status.BAD_REQUEST, "No fields selected for values retrieve");
			}

			final CollectableSearchExpression clctexpr = filter.getSearchExpression(
					info.getEntity(),
					Stream.of(desiredField).collect(Collectors.toList())
			);

			final Collection<Object> ids = Rest.facade().getEntityObjectIds(info.getEntity(), clctexpr);
			List<Object> fieldValues = new ArrayList<>();
			List<Object> fieldIdValues = new ArrayList<>();

			JsonObjectBuilder obj = Json.createObjectBuilder();

			for (Object id : ids) {
				UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(id, desiredField.getEntity());
				checkPermissionForFieldGet(attributeId, desiredField, usage, null, null);

				final EntityObjectVO<Object> entityObjectVO =
						Rest.facade().getDataForField(id, desiredField);
				Object value = entityObjectVO.getFieldValue(desiredField.getUID());
				Object valueId = entityObjectVO.getFieldId(desiredField.getUID());

				if (value != null) {
					fieldValues.add(value);
				}

				if (valueId != null) {
					fieldIdValues.add(valueId);
				}
			}

			obj.add("fieldValues", createArrayObjectFromList(fieldValues).build());
			obj.add("fieldIdValues", createArrayObjectFromList(fieldIdValues).build());

			return obj.build();
		} catch (Exception ex) {
			throw NuclosWebException.wrap(ex, info.getEntity());
		}
	}

	@POST
	@Path("/{boMetaId}/editAttributes")
	@ActionsAllowed(UserAction.BULK_EDIT)
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "collectiveProcessingEditAttributes",
			description = "Retrieve list of editable attributes",
			summary = "Retrieve list of editable attributes",
			tags = "Nuclos Collective Processing",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											type = "object"
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public JsonValue collectiveProcessingEditAttributes(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);

		try {
			final FilterJ filter = getFilterJ(queryContext, -1L);
			final List<FieldMeta<?>> fields = Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());
			final CollectableSearchExpression clctexpr = filter.getSearchExpression(info.getEntity(), fields);

			JsonObjectBuilder jsonAttr = Json.createObjectBuilder();
			getBOWritableFields()
					.sorted(Comparator.comparing(FieldMeta::getFieldName))
					.forEach(j -> {
						jsonAttr.add(
								NuclosBusinessObjectBuilder.getFieldNameForFqn(j),
								jsonFactory.buildJsonObject(j, this)
						);
					});

			return jsonAttr.build();
		} catch (Exception ex) {
			throw NuclosWebException.wrap(ex, info.getEntity());
		}
	}

	@POST
	@Path("/{boMetaId}/execEditAttributes")
	@ActionsAllowed(UserAction.BULK_EDIT)
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "collectiveProcessingExecuteEditAttributes",
			description = "Execute edit attributes on bos",
			summary = "Execute edit attributes on bos",
			tags = "Nuclos Collective Processing",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = CollectiveProcessingExecution.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public CollectiveProcessingExecution collectiveProcessingExecuteEditAttributes(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);
		final FilterJ filter = getFilterJ(queryContext, -1L);
		final List<FieldMeta<?>> fields = Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());

		final String executionId = executionService.newExecutionId();
		final CollectiveProcessingExecutionEntry cacheEntry = executionService.createCacheEntry(executionId, getUser());

		try {
			final CollectableSearchExpression clctexpr = filter.getSearchExpression(info.getEntity(), fields);
			executionService.executeEdit(cacheEntry, clctexpr, info.getEntity(), filter.getEditSelectionAttributes());
		} catch (CommonBusinessException e) {
			throw NuclosWebException.wrap(e, info.getEntity());
		}

		try {
			return getCollectiveProcessingExecutionResult(executionId);
		} catch (Exception ex) {
			throw NuclosWebException.wrap(ex, info.getEntity());
		}
	}

	@POST
	@Path("/{boMetaId}/delete")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "collectiveProcessingDelete",
			description = "Delete selected BOs",
			summary = "Delete selected BOs",
			tags = "Nuclos Collective Processing",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = CollectiveProcessingExecution.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public CollectiveProcessingExecution collectiveProcessingDelete(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);

		final FilterJ filter = getFilterJ(queryContext, -1L);
		final List<FieldMeta<?>> fields = Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());

		final String executionId = executionService.newExecutionId();
		final CollectiveProcessingExecutionEntry cacheEntry = executionService.createCacheEntry(executionId, getUser());

		try {
			final CollectableSearchExpression clctexpr = filter.getSearchExpression(info.getEntity(), fields);
			executionService.executeDelete(cacheEntry, clctexpr, info.getEntity());
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}

		return getCollectiveProcessingExecutionResult(executionId);
	}

	@POST
	@Path("/{boMetaId}/generation/{generationId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "collectiveProcessingGeneration",
			description = "Generate BOs",
			summary = "Generate BOs",
			tags = "Nuclos Collective Processing",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = CollectiveProcessingExecution.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public CollectiveProcessingExecution collectiveProcessingGeneration(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "generationId",
					required = true,
					description = "Businessobject Generator Identifier",
					example = "genOrder"
			)
			@PathParam("generationId") String generationId,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);

		final UID generationUID = Rest.facade().translateFqn(E.GENERATION, generationId);
		final FilterJ filter = getFilterJ(queryContext, -1L);
		final List<FieldMeta<?>> fields = Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());
		checkFilter(filter);

		final String executionId = executionService.newExecutionId();
		final CollectiveProcessingExecutionEntry cacheEntry = executionService.createCacheEntry(executionId, getUser());

		try {
			final CollectableSearchExpression clctexpr = filter.getSearchExpression(info.getEntity(), fields);
			executionService.executeGeneration(this, cacheEntry, clctexpr, info.getEntity(), generationUID);
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
		}

		return getCollectiveProcessingExecutionResult(executionId);
	}

	@POST
	@Path("/progress/{executionId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "collectiveProcessingProgress",
			description = "Progress information of the current collective processing",
			summary = "Progress information of the current collective processing",
			tags = "Nuclos Collective Processing",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = CollectiveProcessingProgressInfo.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Execution not found"
					),
					@ApiResponse(
							responseCode = "417",
							description = "Business error or missing input required properties"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public Response progress(
			@Context final HttpServletResponse response,
			@Parameter(
					name = "executionId",
					required = true,
					description = "Execution Identifier",
					example = "213abcd"
			)
			@PathParam("executionId") String executionId,
			@Parameter(
					schema = @Schema(
							implementation = CollectiveProcessingObjectInfoInputrequired.class
					)
			) JsonObject inputRequiredData) {
		try {
			CollectiveProcessingExecutionEntry cacheEntry = executionService.getCacheEntry(executionId, getUser());
			CollectiveProcessingExecutionEntry.ProgressInfoSinceLastRequest cachedResult = cacheEntry.getProgressInfoSinceLastRequest();
			CollectiveProcessingObjectInfoInputrequired inputRequired = cacheEntry.getInputRequired();

			if (inputRequiredData != null) {
				if (inputRequired != null) {
					inputRequired = cacheEntry.handleInputRequiredFromPOST(inputRequiredData);
				}
			}

			CollectiveProcessingProgressInfo result = CollectiveProcessingProgressInfo.builder()
					.withPercent(cachedResult.getProgressInPercent())
					.withInputrequired(inputRequired)
					.addObjectInfos(cachedResult.getObjectInfos()).build();
			if (result.getPercent() >= 100L) {
				// clear the cache
				executionService.evictCache(executionId);
			}

			if (inputRequired != null) {
				return Response.status(HttpServletResponse.SC_EXPECTATION_FAILED).entity(result).build();
			} else {
				return Response.status(HttpServletResponse.SC_OK).entity(result).build();
			}
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Response.Status.FORBIDDEN, e.getMessage());
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Response.Status.NOT_FOUND, e.getMessage());
		}
	}

	@GET
	@Path("/abort/{executionId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "collectiveProcessingAbort",
			description = "Abort current collective processing",
			summary = "Abort current collective processing",
			tags = "Nuclos Collective Processing",
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "Execution aborted"
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Execution not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public Response abort(
			@Parameter(
					name = "executionId",
					required = true,
					description = "Execution Identifier",
					example = "213abcd"
			)
			@PathParam("executionId") String executionId) {
		try {
			CollectiveProcessingExecutionEntry cacheEntry = executionService.getCacheEntry(executionId, getUser());
			cacheEntry.abort();

			return Response.status(Response.Status.NO_CONTENT).build();
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Response.Status.FORBIDDEN, e.getMessage());
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Response.Status.NOT_FOUND, e.getMessage());
		}
    }

	private void checkFilter(FilterJ filter) {
		if (!filter.hasMultiSelectionCondition()) {
			throw new NuclosWebException(Response.Status.NOT_ACCEPTABLE, "QueryContext is missing the " + FilterJ.KEY_MULTI_SELECTION_CONDITION);
		}
	}

}
