package org.nuclos.server.rest.services;

import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.ObjectUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.lucene.LuceneSearchResult;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.schema.rest.EntityObject;
import org.nuclos.schema.rest.EvaluatedTitleExpression;
import org.nuclos.schema.rest.GlobalSearchResult;
import org.nuclos.schema.rest.LovEntry;
import org.nuclos.schema.rest.MatrixData;
import org.nuclos.schema.rest.MatrixRequestParameters;
import org.nuclos.schema.rest.StatusInfo;
import org.nuclos.schema.rest.StatusPath;
import org.nuclos.schema.rest.TreeNode;
import org.nuclos.server.dblayer.incubator.DbExecutor;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.rest.CacheableResponseBuilder;
import org.nuclos.server.rest.ejb3.IRValueObject.JsonBuilderConfiguration;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RDataType;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.rvo.RValueObject;
import org.nuclos.server.rest.services.rvo.ReferenceListParameterJson;
import org.nuclos.server.rest.services.rvo.UsageProperties;
import org.nuclos.server.statemodel.valueobject.StateModelUsageVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.springframework.core.io.Resource;

import com.google.common.collect.Lists;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/data")
@Produces(MediaType.APPLICATION_JSON)
public class DataService extends DataServiceHelper {

	//TODO: TO BE REFACTURED

	@GET
	@Path("/fieldget/{field}/{pk}")
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonObject fieldget(
			@Parameter(
					name = "field",
					required = true,
					description = "Field identifier",
					example = "rest_example_Test"
			)
			@PathParam("field") String field,
			@Parameter(
					name = "pk",
					required = true,
					description = "Businessobject Identifier",
					example = "213abcd"
			)
			@PathParam("pk") String pk
	) {
		return fieldget(field, pk, null, null);
	}

	@GET
	@Path("/fieldget/{field}/{pk}/{requestAttrId}")
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonObject fieldget(
			@Parameter(
					name = "field",
					required = true,
					description = "Field identifier",
					example = "rest_example_Test"
			)
			@PathParam("field") String field,
			@Parameter(
					name = "pk",
					required = true,
					description = "Businessobject Identifier",
					example = "213abcd"
			)
			@PathParam("pk") String pk,
			@Parameter(
					name = "requestAttrId",
					required = true,
					description = "Attribute Identifier",
					example = "213abcd"
			)
			@PathParam("requestAttrId") String requestAttrId
	) {
		return fieldget(field, pk, requestAttrId, null);
	}

	@GET
	@Path("/fieldget/{field}/{pk}/{requestAttrId}/{requestObjectId}")
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonObject fieldget(
			@Parameter(
					name = "field",
					required = true,
					description = "Field identifier",
					example = "rest_example_Test"
			)
			@PathParam("field") String field,
			@Parameter(
					name = "pk",
					required = true,
					description = "Businessobject Identifier",
					example = "213abcd"
			)
			@PathParam("pk") String pk,
			@Parameter(
					name = "requestAttrId",
					required = true,
					description = "Attribute Identifier",
					example = "213abcd"
			)
			@PathParam("requestAttrId") String requestAttrId,
			@Parameter(
					name = "requestObjectId",
					required = true,
					description = "Businessobject Identifier",
					example = "213abcd"
			)
			@PathParam("requestObjectId") String requestObjectId
	) {
		try {
			FieldMeta<?> fm = Rest.getEntityField(Rest.translateFqn(E.ENTITYFIELD, field));
			EntityMeta<?> eMeta = Rest.getEntity(fm.getEntity());
			final UID sessionDataLanguageUID = getSessionContext().getDataLanguageUID();

			JsonBuilderConfiguration jsonConfig = JsonBuilderConfiguration.getNoAdds();

			if (eMeta.isUidEntity()) {
				UID pkUid = Rest.translateFqn(eMeta, pk);
				UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(pkUid, eMeta.getUID());
				checkPermissionForFieldGet(field, fm, usage, requestAttrId, requestObjectId);

				EntityObjectVO<UID> eo = Rest.facade().getDataForField(pkUid, fm);
				UsageProperties up = new UsageProperties(usage, this, metaProvider, preferencesProvider, parameterProvider, false);
				return new RValueObject<UID>(eo, jsonConfig, up, null, sessionDataLanguageUID, jsonFactory, metaProvider, stateCache).getJSONObjectBuilder().build();

			} else {
				Long pkLong = Long.parseLong(pk);
				UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(pkLong, eMeta.getUID());
				checkPermissionForFieldGet(field, fm, usage, requestAttrId, requestObjectId);

				EntityObjectVO<Long> eo = Rest.facade().getDataForField(pkLong, fm);
				UsageProperties up = new UsageProperties(usage, this, metaProvider, preferencesProvider, parameterProvider, false);
				return new RValueObject<Long>(eo, jsonConfig, up, null, sessionDataLanguageUID, jsonFactory, metaProvider, stateCache).getJSONObjectBuilder().build();

			}
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITYFIELD, field));
		}
	}

	@GET
	@Path("/referencelist/{field}")
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = LovEntry.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonValue referencelist(
			@Parameter(
					name = "field",
					required = true,
					description = "Field to retrieve reference list from",
					example = "rest_example_Order_customer"
			)
			@PathParam("field") String field) {
		return super.referencelistarray(field).build();
	}

	@GET
	@Path("/vlpdata")
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = LovEntry.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			parameters = {
					@Parameter(in = ParameterIn.QUERY, name = "vlptype", description = "Type of VLP to retrieve"),
					@Parameter(in = ParameterIn.QUERY, name = "vlpvalue", description = "VLP value if any"),
					@Parameter(in = ParameterIn.QUERY, name = "intid", description = "VLP identifier if any"),
					@Parameter(in = ParameterIn.QUERY, name = "quickSearchInput", description = "Filter VLP list with given string"),
					@Parameter(in = ParameterIn.QUERY, name = "mandatorId", description = "Mandator Identifier if any"),
					@Parameter(in = ParameterIn.QUERY, name = "chunkSize", description = "Number of data to retrieve")
			},
			tags = "Nuclos Data"
	)
	public JsonValue vlpdata() {
		try {
			// TODO: ReferenceListParameterJson umbenennen in VlpParameterJson
			ReferenceListParameterJson paramJson = new ReferenceListParameterJson(getQueryParameters(), this);

			WebValueListProvider wvlp = paramJson.getWebValueListProviderIfAny(fqnCache);
			String pk = paramJson.getPK();

			UID reffield = null;
			Object oReffield = wvlp.getParameters().get("reffield");
			if (oReffield instanceof String && ((String) oReffield).trim().length() > 0) {
				reffield = Rest.translateFqn(E.ENTITYFIELD, (String) oReffield);
			}

			int chunkSize = DEFAULT_RESULT_LIMIT;
			if (paramJson.getChunkSize() != null && paramJson.getChunkSize() > 0) {
				chunkSize = paramJson.getChunkSize();
			}

			List<CollectableField> vlpData = Rest.facade().getVLPData(
					reffield,
					wvlp,
					pk,
					paramJson.getQuickSearchInput(),
					chunkSize,
					wvlp.hasDefaultMarker(),
					UID.parseUID(paramJson.getMandatorId())
			);
			return jsonFactory.buildJsonArray(vlpData, this).build();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, null);
		}
	}


	@GET
	@Path("/search/{text}")
	@Operation(
			operationId = "search",
			description = "Lucene Search",
			summary = "Lucene Search",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = GlobalSearchResult.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonArray search(
			@Parameter(
					name = "text",
					required = true,
					description = "Search query for lucene index search",
					example = "abcdef"
			)
			@PathParam("text") String text) {

		List<LuceneSearchResult> res = Rest.facade().luceneIndexSearch(text);

		return jsonFactory.buildJsonArray(res, this).build();
	}

	@GET
	@Path("/statusinfo/{uid}")
	@Operation(
			description = "Complete information of a status.",
			summary = "Complete information of a status.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = StatusInfo.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public StatusInfo statusinfo(
			@Parameter(
					name = "uid",
					required = true,
					description = "Status Identifier",
					example = "213abcd"
			)
			@PathParam("uid") String uid) {
		Locale locale = getLocale();
		UID stateId = Rest.translateFqn(E.STATE, uid);
		StateVO state;
		try {
			state = stateCache.getStateById(stateId);
		} catch (CommonFinderException ex) {
			throw new NuclosWebException(Response.Status.NOT_FOUND, String.format("Status %s(%s) not found", uid, stateId));
		}

		StatusInfo.Builder<Void> stateBuilder = StatusInfo.builder()
				.withStatename(state.getStatename(locale))
				.withNumeral(state.getNumeral().longValue())
				.withDescription(state.getDescription(locale) != null ? state.getDescription(locale) : "");

		if (state.getColor() != null) {
			stateBuilder = stateBuilder.withColor(state.getColor());
		}

		if (state.getButtonIcon() != null) {
			stateBuilder = stateBuilder.withButtonIcon( Rest.translateUid(E.RESOURCE, state.getButtonIcon().getPrimaryKey()));
		}

		String buttonLabel = state.getButtonLabel(locale);
		if (buttonLabel != null) {
			stateBuilder = stateBuilder.withButtonLabel(buttonLabel);
		}

		return stateBuilder.build();
	}


	@GET
	@Path("/path/{stateId}")
	@Operation(
			operationId = "defaultPath",
			description = "Path of the model the state belongs to (configured or automatically determined)",
			summary = "Default path of the model the state belongs to. " +
					"If the default path was left and a direct way back does not exist or no alternative path was found, " +
					"the result does not contain any infos (missing or empty infos array). " +
					"Then, with the help of the object path (/path/{stateId}/{boId}), at least the previous states can be displayed (State history).",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = StatusPath.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "A default path has not been configured"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public StatusPath defaultPath(
			@Parameter(
					name = "stateId",
					required = true,
					description = "StateId",
					example = "nuclet_test_other_TestStatesSM_State_20"
			)
			@PathParam("stateId") String stateId,
			@Parameter(
					name = "maxElements",
					description = "optional max count of returned elements (default is 4)",
					example = "4"
			)
			@QueryParam("maxElements") Integer maxElements) {
		final UID requestedStateUID = Rest.translateFqn(E.STATE, stateId);
		final int iMaxElements = ObjectUtils.defaultIfNull(maxElements, 4);
		final Locale locale = getLocale();
		try {
			return stateCache.getStatusPath(requestedStateUID, iMaxElements, locale);
		} catch (CommonFinderException ex) {
			throw new NuclosWebException(Response.Status.NOT_FOUND, ex.getMessage());
		}
	}


	@GET
	@Path("/path/{stateId}/{boId}")
	@Operation(
			operationId = "objectPath",
			description = "Path of a single object (consisting of the previous states)",
			summary = "Path of a single object (consisting of the previous states)",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = StatusPath.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "State or object not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public StatusPath objectPath(
			@Parameter(
					name = "stateId",
					required = true,
					description = "StateId",
					example = "nuclet_test_other_TestStatesSM_State_20"
			)
			@PathParam("stateId") String stateId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "maxElements",
					description = "optional max count of returned elements (default is 4)",
					example = "4"
			)
			@QueryParam("maxElements") Integer maxElements) {
		final UID requestedStateUID = Rest.translateFqn(E.STATE, stateId);
		final int iMaxElements = ObjectUtils.defaultIfNull(maxElements, 4);
		final Locale locale = getLocale();
		try {
			final StateVO stateVO = stateCache.getStateById(requestedStateUID);
			final UID modelUID = stateVO.getModelUID();
			final Set<UID> stateEntityUIDs = stateCache.getModelUsages().stream()
					.filter(usage -> Objects.equals(usage.getStateModelUID(), modelUID))
					.map(StateModelUsageVO::getUsageCriteria)
					.map(UsageCriteria::getEntityUID)
					.collect(Collectors.toSet());
			final UID entityUID;
			if (stateEntityUIDs.size() == 1) {
				// one entity
				entityUID = stateEntityUIDs.iterator().next();
			} else {
				// more than one entity, check the right one
				final List<UID> foundEntitiesWithThisId = stateEntityUIDs.stream()
						.filter(uid -> Rest.facade().checkObjectExists(uid, boId))
						.collect(Collectors.toList());
				switch (foundEntitiesWithThisId.size()) {
					case 0: entityUID = null; break;
					case 1: entityUID = foundEntitiesWithThisId.get(0); break;
					default: throw new NuclosWebException(Response.Status.fromStatusCode(418),
							String.format("Business object with Id (%s) is not unique! Found in: (%s)",
									boId,
									foundEntitiesWithThisId.stream().map(uid -> metaProvider.getEntity(uid).getEntityName())));
				}
			}
			if (entityUID == null) {
				throw new NuclosWebException(Response.Status.NOT_FOUND, String.format("Object with Id (%s) and state (%s) not found", stateId, boId));
			}
			checkPermission(E.ENTITY, Rest.translateUid(E.ENTITY, entityUID), boId);

			final StatusPath.Builder<Void> pathBuilder = StatusPath.builder();
			pathBuilder.withOutside(true)
					.withEnd(stateCache.getTransitionsByModelId(modelUID).stream().noneMatch(t -> Objects.equals(requestedStateUID, t.getStateSourceUID())));
			final List<UID> stateHistoryForObject = Rest.facade().getStateHistoryForObject(boId); // newest first
			final List<UID> resultUIDs = new ArrayList<>();
			if (stateHistoryForObject.size() > 0) {
				resultUIDs.addAll( // reverse the order because the newest are at the beginning
						Lists.reverse(stateHistoryForObject.subList(0,
								Math.min(stateHistoryForObject.size(), iMaxElements))));
			} else {
				// no state history?
				resultUIDs.add(requestedStateUID);
			}
			List<StateVO> stateVOs = new ArrayList<>();
			for (UID uid : resultUIDs) {
				stateVOs.add(stateCache.getStateById(uid));
			}
			pathBuilder.withMoreBefore(stateHistoryForObject.size() > iMaxElements)
					.withMoreAfter(false)
					.addInfos(stateVOs.stream()
							.map(state -> {
								return StatusInfo.builder()
										.withStateId(Rest.translateUid(E.STATE, state.getId()))
										.withNumeral(state.getNumeral().longValue())
										.withStatename(state.getStatename(locale))
										.withDescription(StringUtils.defaultIfLooksEmpty(state.getDescription(locale), null))
										.withButtonIcon(state.getButtonIcon() != null ? state.getButtonIcon().getId().getString() : null)
										.withColor(state.getColor())
										.build();
							})
							.collect(Collectors.toList()));
			return pathBuilder.build();
		} catch (CommonFinderException ex) {
			throw new NuclosWebException(Response.Status.NOT_FOUND, ex.getMessage());
		}
	}


	@GET
	@Path("/resource/{uid}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "application/octet-stream"
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given resource could not be found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public Response resource(
			@Context final Request request,
			@Parameter(
					name = "uid",
					required = true,
					description = "Resource Identifier",
					example = "213abcd"
			)
			@PathParam("uid") String uid
	) {
		try {
			// TODO: Load the resource only if necessary
			final Resource resource = Rest.facade().getResource(Rest.translateFqn(E.RESOURCE, uid));

			return new CacheableResponseBuilder(request) {
				@Override
				protected Date getLastModified() throws IOException {
					if (resource != null) {
						return new Date(resource.lastModified());
					}
					// resource does not exist, return now()
					return new Date();
				}

				@Override
				protected Response.ResponseBuilder newResponseBuilder() throws IOException {
					if (resource == null || resource.getFile() == null) {
						return Response.status(Response.Status.NOT_FOUND);
					}
					return Response.ok(resource.getFile())
							.type(URLConnection.guessContentTypeFromName(resource.getFile().getName()));
				}
			}.build();
		} catch (IOException e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.RESOURCE, uid));
		}
	}

	@POST
	@Path("/evaluatetitleexpression/{boMetaId}/{boId}")
	@Operation(
			operationId = "evaluatetitleexpression",
			description = "Evaluates a title expression.",
			summary = "Evaluates a title expression.",
			requestBody = @RequestBody(
					required = true,
					content = @Content(
							schema = @Schema(
									name = "expression",
									type = "string",
									description = "Expression as string to evaluate"
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EvaluatedTitleExpression.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public EvaluatedTitleExpression evaluatetitleexpression(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			String expression
	) {
		try {
			return EvaluatedTitleExpression.builder()
					.withEvaluatedExpression(evaluateExpressionString(boMetaId, boId, expression))
					.build();

		} catch (Exception e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}

	@GET
	@Path("/imageresource/{boMetaId}/{boId}/{field_id}")
	@Produces("image/png")
	@Operation(
			operationId = "imageresource",
			description = "Image Resource",
			summary = "Image Resource",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "image/png"
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Given resource could not be found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the object failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public Response imageResource(
			@Context final Request request,
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") final String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") final String boId,
			@Parameter(
					name = "field_id",
					required = true,
					description = "Field Identifier",
					example = "example_rest_Article_picture"
			)
			@PathParam("field_id") final String fieldId
	) {
		try {
			SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);

			final EntityMeta entity = info.getMasterMeta();
			final InternalTimestamp changedAt = getChangedAt(entity, boId);

			return new CacheableResponseBuilder(request) {
				@Override
				protected Date getLastModified() {
					return changedAt;
				}

				@Override
				protected Response.ResponseBuilder newResponseBuilder() {
					byte[] imageResource = getImageResource(boMetaId, boId, fieldId);
					if (imageResource == null) {
						return Response.status(Response.Status.NOT_FOUND);
					}
					return Response.ok(imageResource);
				}
			}.build();
		} catch (Exception e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.RESOURCE, boMetaId));
		}
	}

	@POST
	@Path("/data/matrix")
	@Operation(
			operationId = "matrix",
			description = "Get matrix data.",
			summary = "Get matrix data.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = MatrixData.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonObject matrix(
			@Parameter (
					schema = @Schema(
							implementation = MatrixRequestParameters.class
					)
			) JsonObject data
	) {
		String entity = data.getString("entity");
		checkPermission(E.ENTITY, entity);

		boolean bCamelCase = data.containsKey("entityMatrix");
		String valueTypeKey = bCamelCase ? "entityMatrixValueType" : "entity_matrix_value_type";
		String entity_matrix_value_type = data.get(valueTypeKey) != null ? data.getString(valueTypeKey) : null; // type of the matrix field (checkicon, number, combobox, ..)
		String refFieldKey = bCamelCase ? "entityMatrixReferenceField" : "entity_matrix_reference_field";
		String entity_matrix_reference_field = data.containsKey(refFieldKey) ? data.getString(refFieldKey) : null; // if matrix value is a reference to another BO
		String entity_matrix_value_field = data.getString(bCamelCase ? "entityMatrixValueField" : "entity_matrix_value_field");        // if matrix value is a field of the xref table itself

		String entity_x = data.getString(bCamelCase ? "entityX" : "entity_x");
		String entity_y = data.getString(bCamelCase ? "entityY" : "entity_y");
		String entity_field_matrix_parent = data.getString(bCamelCase ? "entityFieldMatrixParent" : "entity_field_matrix_parent");
		String entity_field_matrix_x_refField = data.getString(bCamelCase ? "entityFieldMatrixXRefField" : "entity_field_matrix_x_refField");

		String cellInputTypeKey = bCamelCase ? "cellInputType" : "cell_input_type";
		String cell_input_type = data.get(cellInputTypeKey) != null ? data.getString(cellInputTypeKey) : "checkicon";

		String editable = data.get("editable") != null ? data.getString("editable") : "true";

		JsonValue value = data.get("boId");

		String boId;
		if (value instanceof JsonString) {
			boId = ((JsonString) value).getString();
		} else if (value instanceof JsonNumber) {
			boId = (value).toString();
		} else {
			boId = null;
		}

		try {

			List<EntityObjectVO<Long>> lstEoRows = new ArrayList<>();
			JsonObjectBuilder json = Json.createObjectBuilder();

			if (!StringUtils.isNullOrEmpty(entity_matrix_value_type)) {
				json.add("entityMatrixValueType", entity_matrix_value_type);
			}

			// load dropdown values if matrix data is a reference to another BO
			if (!StringUtils.isNullOrEmpty(entity_matrix_reference_field)) {
				json.add("matrixValues", referencelistarray(entity_matrix_reference_field));
			}

			// load columns
			json.add("xAxisBoList", getMatrixColumns(entity_x, data, bCamelCase, boId));

			// load rows/subform data
			json.add("yAxisBoList", getMatrixRows(entity_y, data, bCamelCase, boId, lstEoRows));

			// load yMatrixColumnList
			json.add("yMatrixColumnsList", getYMatrixColumnsList(data, bCamelCase));

			List<Long> lstIDs = new ArrayList<>();
			for (EntityObjectVO<Long> eo : lstEoRows) {
				lstIDs.add(eo.getPrimaryKey());
			}

			UID refX2Y = Rest.translateFqn(E.ENTITYFIELD, entity_field_matrix_parent);
			MasterDataFacadeLocal mdLocale = SpringApplicationContextHolder.getBean(MasterDataFacadeLocal.class);

			List<EntityObjectVO<Long>> xrefObjectList = mdLocale.readDependenciesForMultiRecords(lstIDs, refX2Y);

			// load sub-subform data (xref)

			UID entityMatrixValueFieldUid = Rest.translateFqn(E.ENTITYFIELD, entity_matrix_value_field);
			FieldMeta<?> matrixValueMeta = Rest.getEntityField(entityMatrixValueFieldUid);
			RDataType dataType = new RDataType(matrixValueMeta.getDataType(), matrixValueMeta.getPrecision());
			boolean entityMatrixValueFieldIsNumber = dataType.isNumber();

			UID matrix_x_refField = Rest.translateFqn(E.ENTITYFIELD, entity_field_matrix_x_refField);
			UID matrix_parent = Rest.translateFqn(E.ENTITYFIELD, entity_field_matrix_parent);

			JsonArrayBuilder xrefData = Json.createArrayBuilder();
			JsonObjectBuilder entityMatrixBoDict = Json.createObjectBuilder();

			// WebContext.LOG.info("BEFORE TIME CONSUMING STUFF:" + xrefObjectList.size());

			for (EntityObjectVO<Long> eo : xrefObjectList) {
				// FIXME: TIME CONSUMING INSTANCIATING RVALUEOBJECT
				// TODO: Sling out Build(), Avoid instanciation of RValueObject, and: it's not thin at all
				// JsonObjectBuilder boData = getThinEoJson(eo);
				// xrefData.add(boData);

				Object entityMatrixObject = eo.getFieldValue(entityMatrixValueFieldUid);
				if (entityMatrixObject != null) {
					String entityMatrixValue = entityMatrixObject.toString().replaceAll("\"", "");

					Long xAxisBoId = eo.getFieldId(matrix_x_refField);
					Long yAxisBoId = eo.getFieldId(matrix_parent);

					String key = xAxisBoId + "_" + yAxisBoId; // key for entityMatrixBoDict

					JsonObjectBuilder dummy = Json.createObjectBuilder();
					dummy.add("boId", eo.getPrimaryKey());
					if (StringUtils.isNullOrEmpty(entity_matrix_reference_field)) {
						if (entityMatrixValueFieldIsNumber) {
							dummy.add("value", Double.parseDouble(entityMatrixValue));
						} else if ("checkicon".equals(entity_matrix_value_type)) {
							dummy.add("value", Integer.parseInt(entityMatrixValue));
						} else {
							dummy.add("value", entityMatrixValue);
						}
					} else { // matrixcell has reference value

						if (entityMatrixObject instanceof Integer) {
							dummy.add("value", (Integer) entityMatrixObject);
						} else if (entityMatrixObject instanceof String) {
							dummy.add("value", (String) entityMatrixObject);
						}
					}
					//dummy.add("attributes", boData.get("attributes"));
					entityMatrixBoDict.add(key, dummy);
				}
			}

			json.add("entityMatrixBoDict", entityMatrixBoDict);
			// FIXME: TIME CONSUMING INSTANCIATING RVALUEOBJECT
			// json.add("bos", getJsonBos(lstEoRows));
			json.add("xrefData", xrefData);

			json.add("cellInputType", cell_input_type);
			json.add("editable", editable);

			// WebContext.LOG.info("AFTER TIME CONSUMING STUFF");
			return json.build();

		} catch (Exception e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.ENTITY, entity_x));
		}
	}

	@GET
	@Path("/tree/{boMetaId}/{boId}")
	@Operation(
			operationId = "tree",
			description = "Tree",
			summary = "Tree",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = TreeNode.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonArray tree(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId) {
		return Rest.facade().getTree(boMetaId, boId).build();
	}

	@GET
	@Path("/treeroot/{boMetaId}/{boId}")
	@Operation(
			operationId = "tree",
			description = "Tree",
			summary = "Tree",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
										schema = @Schema(
												implementation = TreeNode.class
										)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonObject treeroot(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId) {
		return Rest.facade().getTreeRoot(boMetaId, boId).build();
	}

	@GET
	@Path("/tree/{boMetaId}/{boId}/{search}")
	@Operation(
			operationId = "tree",
			description = "Tree",
			summary = "Tree",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = TreeNode.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonArray treeWithSearch(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "search",
					required = true,
					description = "Filter tree data results with search query",
					example = "something"
			)
			@PathParam("search") String search) throws CommonBusinessException {
		return Rest.facade().getTreeWithSearch(boMetaId, boId, search).build();
	}

	@GET
	@Path("/subtree/{node_id}")
	@Operation(
			operationId = "subtree",
			description = "Subtree for a given node_id",
			summary = "Subtree for a given node_id",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = TreeNode.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonArray subtree(
			@Parameter(
					name = "node_id",
					required = true,
					description = "Identifier for node",
					example = "something"
			)
			@PathParam("node_id") String nodeId) {
		return Rest.facade().getSubtree(nodeId).build();
	}

	@GET
	@Path("/datasource/{datasourceId}")
	@Operation(
			operationId = "datasource",
			description = "Executes the datasource and return the plain result as JsonArray. Returns max. 250.000 rows (DbExecutor.LimitedResultSetRunner.MAXFETCHSIZE)",
			summary = "Executes the datasource and return the plain result as JsonArray.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													type = "object"
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonArray datasource(
			@Parameter(
					name = "datasourceId",
					required = true,
					description = "Identifier for datasource",
					example = "something"
			)
			@PathParam("datasourceId") String datasourceId) {
		return datasource(datasourceId, null);
	}

	@GET
	@Path("/datasource/{datasourceId}/{maxRowCount}")
	@Operation(
			operationId = "datasource",
			description = "Executes the datasource and return the plain result as JsonArray. The default for maxRowCount is 250.000 (DbExecutor.LimitedResultSetRunner.MAXFETCHSIZE)",
			summary = "Executes the datasource and return the plain result as JsonArray.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													type = "object"
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonArray datasource(
			@Parameter(
					name = "datasourceId",
					required = true,
					description = "Identifier for datasource",
					example = "something"
			)
			@PathParam("datasourceId") String datasourceId,
			@Parameter(
					name = "maxRowCount",
					description = "Number of rows to return",
					example = "1000"
			)
			@PathParam("maxRowCount") Integer maxRowCount) {
		try {
			final UID datasourceUID = Rest.facade().translateFqn(E.DATASOURCE, datasourceId);
			if (!Rest.facade().isDatasourceAllowed(datasourceUID)) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}

			final Map<String, String> datasourceParams = new HashMap<>();
			for (String key : getQueryParameters().keySet()) {
				datasourceParams.put(key, getFirstParameter(key, String.class));
			}
			final Integer iMaxRowCount = maxRowCount == null ?
					DbExecutor.LimitedResultSetRunner.MAXFETCHSIZE :
					maxRowCount;
			return Rest.facade().executeDatasource(datasourceUID, datasourceParams, iMaxRowCount);
		} catch (Exception ex) {
			throw new NuclosWebException(ex, E.DATASOURCE.getUID());
		}
	}

	@GET
	@Path("/textModule/{boMetaId}/{boId}/{attributeMetaId}")
	@Operation(
			operationId = "textModule",
			description = "Returns the text modules for a certain BO attribute if configured in the assigned layout.",
			summary = "Returns the text modules for a certain BO attribute if configured in the assigned layout.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													type = "object"
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Data"
	)
	public JsonArray textModule(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "attributeMetaId",
					required = true,
					description = "Attribute Meta Identifier",
					example = "example_rest_Order_note"
			)
			@PathParam("attributeMetaId") String attributeMetaId,
			@Parameter(
					name = "controlType",
					description = "optional control type of layout component",
					example = ""
			)
			@QueryParam("controlType") String controlType) {
		try {
			UID entityUid = Rest.facade().translateFqn(E.ENTITY, boMetaId);
			UID attributeUid = Rest.facade().translateFqn(E.ENTITYFIELD, attributeMetaId);

			Long lBoId;

			try {
				lBoId = Long.parseLong(boId);
			} catch (NumberFormatException e) {
				lBoId = null;
			}

			UsageCriteria usageCriteria = lBoId != null
					? Rest.facade().getUsageCriteriaForPK(lBoId, entityUid)
					: new UsageCriteria(entityUid, null, null, null);
			return Rest.facade().getTextModules(
					new LayoutMLParser().getTextModuleSettings(
							this.getWebLayout(usageCriteria).getLayoutInfo().getLayoutML(),
							attributeUid,
							controlType)
			);
		} catch (Exception ex) {
			throw new NuclosWebException(ex);
		}
	}
}
