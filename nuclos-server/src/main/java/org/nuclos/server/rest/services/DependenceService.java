package org.nuclos.server.rest.services;

import java.util.List;

import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.schema.rest.EntityObject;
import org.nuclos.schema.rest.EntityObjectResultList;
import org.nuclos.schema.rest.InputRequiredContext;
import org.nuclos.schema.rest.LovEntry;
import org.nuclos.schema.rest.QueryContext;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestLinks.RestLink;
import org.nuclos.server.rest.misc.RestLinks.Verbs;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RecursiveDependency;
import org.nuclos.server.rest.services.rvo.RValueObject;
import org.nuclos.server.rest.services.rvo.ResultListExportRVO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/bos/{boMetaId}/{boId}/subBos")
@Produces(MediaType.APPLICATION_JSON)
public class DependenceService extends DataServiceHelper {

	@GET
	@Path("/{refAttrId}")
	@Operation(
			operationId = "dependence_list",
			description = "List of Dependence-Data (Rows)",
			summary = "List of Dependence-Data (Rows)",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = EntityObjectResultList.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject dependencelist(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Businessobject Attribute Identifier",
					example = "example_rest_Article_price"
			)
			@PathParam("refAttrId") String refAttrId,
			@Parameter(
					name = "sort",
					required = true,
					description = "Sort fields in returning data by (asc/desc)",
					example = "example_rest_Article_price=asc,example_rest_Article_name=desc"
			)
			@QueryParam("sort") String sortString
	) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return getDependenceList(
				boMetaId,
				RecursiveDependency.forRoot(boId).dependency(refAttrId),
				linksFactory,
				sortString,
				null
		).build();
	}

	@GET
	@Path("/{refAttrId}/countOnly")
	@Operation(
			operationId = "dependence_list",
			description = "Total count of Dependence-Data (Rows)",
			summary = "Total count of Dependence-Data (Rows)",
			responses = {
					@ApiResponse(
							content = @Content(
									schema = @Schema(
											implementation = EntityObjectResultList.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject dependencelistCount(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Businessobject Attribute Identifier",
					example = "example_rest_Article_price"
			)
			@PathParam("refAttrId") String refAttrId
	) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return countDependenceList(
				boMetaId,
				RecursiveDependency.forRoot(boId).dependency(refAttrId),
				linksFactory,
				null
		).build();
	}

	//FIXME: HATEOAS Standard forbids @POST-Method for just fetching data. See also BoService @Path("/{boMetaId}/query")
	@POST
	@Path("/{refAttrId}/query")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "dependence_listQuery",
			description = "List of Dependence-Data (Rows) by query object",
			summary = "List of Dependence-Data (Rows) by query object",
			responses = {
					@ApiResponse(
							content = @Content(
									schema = @Schema(
											implementation = EntityObjectResultList.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject dependencelist(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Businessobject Attribute Identifier",
					example = "example_rest_Article_price"
			)
			@PathParam("refAttrId") String refAttrId,
			@Parameter(
					name = "sort",
					required = true,
					description = "Sort fields in returning data by (asc/desc)",
					example = "example_rest_Article_price=asc,example_rest_Article_name=desc"
			)
			@QueryParam("sort") String sortString,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return getDependenceList(
				boMetaId,
				RecursiveDependency.forRoot(boId).dependency(refAttrId),
				linksFactory,
				sortString,
				queryContext
		).build();
	}

	@GET
	@Path("/{refAttrId}/{subBoId}")
	@Operation(
			operationId = "dependence_self",
			description = "Full Data Row Details of Dependency",
			summary = "Full Data Row Details of Dependency",
			responses = {
					@ApiResponse(
							content = @Content(
									schema = @Schema(
											implementation = EntityObject.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject dependencydetails(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Businessobject Attribute Identifier",
					example = "example_rest_Article_price"
			)
			@PathParam("refAttrId") String refAttrId,
			@Parameter(
					name = "subBoId",
					required = true,
					description = "Subbusinessobject Identifier",
					example = "10090"
			)
			@PathParam("subBoId") String subBoId
	) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return getDependencyDetails(boMetaId, boId, refAttrId, subBoId, linksFactory).build();
	}

	@GET
	@Path("/recursive/{referenceAttributeAndRecordIds:.+}")
	@Operation(
			operationId = "dependents_list_recursive",
			description = "Data for subforms nested in arbitrary depth",
			summary = "Data for subforms nested in arbitrary depth",
			responses = {
					@ApiResponse(
							content = @Content(
									schema = @Schema(
											implementation = EntityObjectResultList.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject dependentsList(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "referenceAttributeAndRecordIds",
					required = true,
					description = "Businessobject Attribute or Record Identifier",
					example = "example_rest_Article_price"
			)
			@PathParam("referenceAttributeAndRecordIds") List<PathSegment> referenceAttributeAndRecordIds,
			@Parameter(
					name = "sort",
					required = true,
					description = "Sort fields in returning data by (asc/desc)",
					example = "example_rest_Article_price=asc,example_rest_Article_name=desc"
			)
			@QueryParam("sort") String sortString
	) {
		if (referenceAttributeAndRecordIds.size() % 2 == 0
				&& !String.valueOf(referenceAttributeAndRecordIds.get(referenceAttributeAndRecordIds.size() - 1)).isEmpty()) {
			RecursiveDependency dependency = RecursiveDependency.forRoot(boId);
			for (int i = 0; i < referenceAttributeAndRecordIds.size(); i += 2) {
				final String referenceAttributeId = referenceAttributeAndRecordIds.get(i).toString();

				String recordId = null;
				if (referenceAttributeAndRecordIds.size() > i + 1 && i != referenceAttributeAndRecordIds.size() - 2) {
					recordId = referenceAttributeAndRecordIds.get(i + 1).toString();
				}

				dependency = dependency.dependency(referenceAttributeId, recordId);
			}

			DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(
					boMetaId,    // TODO: Should be the Meta-ID of the immediate parent of the dependents?
					boId,        // TODO: Should be the record ID of the immediate parent of the dependents?
					dependency
			);

			return getDependencyDetails(
					boMetaId,
					dependency,
					String.valueOf(referenceAttributeAndRecordIds.get(referenceAttributeAndRecordIds.size() - 1)),
					linksFactory).build();
		} else {
			return dependentsListQueryPost(
					boMetaId,
					boId,
					referenceAttributeAndRecordIds,
					sortString,
					null
			);
		}
	}

	/**
	 * Allows to POST a query, analogous to {@link org.nuclos.server.rest.services.BoService#bolist(java.lang.String, javax.json.JsonObject)}.
	 */
	@POST
	@Path("/recursive/{referenceAttributeAndRecordIds:.+}/query")
	@Operation(
			operationId = "dependents_list_recursive_query",
			description = "Data for subforms nested in arbitrary depth with query context",
			summary = "Data for subforms nested in arbitrary depth with query context",
			responses = {
					@ApiResponse(
							content = @Content(
									schema = @Schema(
											implementation = EntityObjectResultList.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonObject dependentsListQueryPost(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Article"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "referenceAttributeAndRecordIds",
					required = true,
					description = "Businessobject Attribute or Record Identifier",
					example = "example_rest_Article_price"
			)
			@PathParam("referenceAttributeAndRecordIds") List<PathSegment> referenceAttributeAndRecordIds,
			@Parameter(
					name = "sort",
					required = true,
					description = "Sort fields in returning data by (asc/desc)",
					example = "example_rest_Article_price=asc,example_rest_Article_name=desc"
			)
			@QueryParam("sort") String sortString,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) final JsonObject queryContext
	) {
		RecursiveDependency dependency = RecursiveDependency.forRoot(boId);
		for (int i = 0; i < referenceAttributeAndRecordIds.size(); i += 2) {
			final String referenceAttributeId = referenceAttributeAndRecordIds.get(i).toString();

			String recordId = null;
			if (referenceAttributeAndRecordIds.size() > i + 1) {
				recordId = referenceAttributeAndRecordIds.get(i + 1).toString();
			}

			dependency = dependency.dependency(referenceAttributeId, recordId);
		}

		DependenceBoLinksFactory linksFactory = referenceAttributeAndRecordIds.size() == 1
				? new DependenceBoLinksFactory(
				boMetaId,    // TODO: Should be the Meta-ID of the immediate parent of the dependents?
				boId,        // TODO: Should be the record ID of the immediate parent of the dependents?
				dependency.getReferenceAttributeFqn()
		)
				: new DependenceBoLinksFactory(
				boMetaId,    // TODO: Should be the Meta-ID of the immediate parent of the dependents?
				boId,        // TODO: Should be the record ID of the immediate parent of the dependents?
				dependency);

		// TODO: Extend to allow passing params for arbitrary depth subform query
		return getDependenceList(
				boMetaId,
				dependency,
				linksFactory,
				sortString,
				queryContext
		).build();
	}

	@GET
	@Path("/{refAttrId}/valuelist/{subBoAttrId}")
	@Operation(
			operationId = "dependence_valuelist",
			description = "Value list for filtering",
			summary = "Value list for filtering",
			responses = {
					@ApiResponse(
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = LovEntry.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public JsonValue valuelist(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Businessobject Attribute Identifier",
					example = "example_rest_Order_customer"
			)
			@PathParam("refAttrId") String refAttrId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Subbusinessobject Attribute Identifier",
					example = "example_rest_Customer_name"
			)
			@PathParam("subBoAttrId") String subBoAttrId
	) {
		UID fieldUid = Rest.translateFqn(E.ENTITYFIELD, subBoAttrId);
		try {
			UID baseEntityUid = Rest.translateFqn(E.ENTITY, boMetaId);
			FieldMeta fieldMeta = Rest.getEntityField(fieldUid);
			EntityMeta<?> baseEntityMeta = Rest.getEntity(baseEntityUid);
			UID refFieldUid = Rest.translateFqn(E.ENTITYFIELD, refAttrId);
			FieldMeta refFieldMeta = Rest.getEntityField(refFieldUid);

			checkPermissionForSubform(baseEntityMeta, fieldMeta, boId, true);

			List<String> sSearch = getQueryParameters("search");

			List<CollectableField> lstCF = Rest.facade().getValueList(fieldMeta, sSearch, refFieldMeta, new Long(boId));
			return jsonFactory.buildJsonArray(lstCF, this).build();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, fieldUid);
		}
	}


	@GET
	@Deprecated // use dependenceListExportConsiderColumnConfiguration
	@Path("/{refAttrId}/export/{format}/{pageOrientationLandscape}/{isColumnScaled}")
	@Operation(
			operationId = "dependence_list_export",
			description = "Export list of Dependence-Data (subform) as pdf/csv/xls",
			summary = "Export list of Dependence-Data (subform) as pdf/csv/xls",
			responses = {
					@ApiResponse(
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = ResultListExportRVO.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public ResultListExportRVO dependenceListExport(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Businessobject Attribute Identifier",
					example = "example_rest_Order_customer"
			)
			@PathParam("refAttrId") String refAttrId,
			@Parameter(
					name = "sort",
					required = true,
					description = "Sort fields in returning data by (asc/desc)",
					example = "example_rest_Article_price=asc,example_rest_Article_name=desc"
			)
			@QueryParam("sort") String sortString,
			@Parameter(
					name = "format",
					required = true,
					description = "Format of export",
					schema = @Schema(
							type = "string",
							allowableValues = {
									"pdf",
									"csv",
									"xls"
							}
					)
			)
			@PathParam("format") String format,
			@Parameter(
					name = "pageOrientationLandscape",
					required = true,
					description = "Should export be in orientation landscape",
					schema = @Schema(
							type = "bool"
					)
			)
			@PathParam("pageOrientationLandscape") boolean pageOrientationLandscape,
			@Parameter(
					name = "isColumnScaled",
					required = true,
					description = "Should export columns be scaled",
					schema = @Schema(
							type = "bool"
					)
			)
			@PathParam("isColumnScaled") boolean isColumnScaled,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return exportDependenceList(boMetaId, refAttrId, boId, linksFactory, sortString, queryContext, format, pageOrientationLandscape, isColumnScaled);
	}

	@POST
	@Path("/{refAttrId}/export/{format}/{pageOrientationLandscape}/{isColumnScaled}")
	@Operation(
			operationId = "dependence_list_export",
			description = "Export list of Dependence-Data (subform) as pdf/csv/xls",
			summary = "Export list of Dependence-Data (subform) as pdf/csv/xls",
			responses = {
					@ApiResponse(
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = ResultListExportRVO.class
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Businessobjects"
	)
	public ResultListExportRVO dependenceListExportConsiderColumnConfiguration(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Businessobject Attribute Identifier",
					example = "example_rest_Order_customer"
			)
			@PathParam("refAttrId") String refAttrId,
			@Parameter(
					name = "format",
					required = true,
					description = "Format of export",
					schema = @Schema(
							type = "string",
							allowableValues = {
									"pdf",
									"csv",
									"xls"
							}
					)
			)
			@PathParam("format") String format,
			@Parameter(
					name = "pageOrientationLandscape",
					required = true,
					description = "Should export be in orientation landscape",
					schema = @Schema(
							type = "bool"
					)
			)
			@PathParam("pageOrientationLandscape") boolean pageOrientationLandscape,
			@Parameter(
					name = "isColumnScaled",
					required = true,
					description = "Should export columns be scaled",
					schema = @Schema(
							type = "bool"
					)
			)
			@PathParam("isColumnScaled") boolean isColumnScaled,
			@Parameter(
					schema = @Schema(
							implementation = QueryContext.class
					)
			) JsonObject queryContext
	) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return exportDependenceList(boMetaId, refAttrId, boId, linksFactory, null, queryContext, format, pageOrientationLandscape, isColumnScaled);
	}


	public static class DependenceBoLinksFactory extends RValueObject.AbstractBoLinksFactory {

		private final String boMetaId;
		private final String boId;
		private final String refAttrId;
		private final RecursiveDependency dependency;

		public DependenceBoLinksFactory(String boMetaId, String boId, String refAttrId) {
			this.boMetaId = boMetaId;
			this.boId = boId;
			this.refAttrId = refAttrId;
			this.dependency = null;
		}

		public DependenceBoLinksFactory(String boMetaId, String boId, RecursiveDependency dependency) {
			this.boMetaId = boMetaId;
			this.boId = boId;
			this.refAttrId = null;
			this.dependency = dependency;
		}

		@Override
		public RestLink addSelf(Object subBoId) {
			if (refAttrId != null) {
				return links.addLinkHref("self", "dependence_self", Verbs.GET,
						boMetaId, boId, refAttrId, subBoId).protectVerbs();
			} else if (dependency != null) {
				return links.addLinkHref("self", "dependents_list_recursive", Verbs.GET,
						boMetaId, boId, dependency.toUrlPath().substring(1) + "/" + subBoId).protectVerbs();
			}
			return null;
		}

		@Override
		public RestLink addDetail(final String detailBoMetaId, final Object subBoId) {
			return links.addLinkHref(
					"detail",
					"bo",
					Verbs.GET,
					detailBoMetaId,
					subBoId
			).protectVerbs();
		}

		@Override
		public RestLink addMeta() {
			if (refAttrId != null) {
				return links.addLinkHref("boMeta", "referencemeta_self", Verbs.GET,
						boMetaId, refAttrId);
			} else if (dependency != null) {
				return links.addLinkHref("boMeta", "referencemeta_recursive_self", Verbs.GET,
						boMetaId, dependency.toUrlPath(true).substring(1));
			}

			return null;
		}

		@Override
		public RestLink addStateIcon(String stateId) {
			return null;
		}

		@Override
		public RestLink addDefaultPath(final String stateId) {
			return null;
		}

		@Override
		public RestLink addObjectPath(final String stateId, final Object pk) {
			return null;
		}

		@Override
		public RestLink addLayout(String layoutId) {
			return links.addLinkHref("layout", "weblayoutCalculated", Verbs.GET, layoutId);
		}

		@Override
		public RestLink addDocument(String rel, String docAttrId, Object subBoId) {
			if (refAttrId != null) {
				return docLinks.addLinkHref(rel, "dependence_DocumentValue", Verbs.GET,
						boMetaId, boId, refAttrId, subBoId, docAttrId);
			} else if (dependency != null) {
				return docLinks.addLinkHref(rel, "dependence_recursive_DocumentValue", Verbs.GET,
						boMetaId, boId, dependency.toUrlPath().substring(1) + "/" + subBoId, docAttrId);
			}

			return null;
		}

		@Override
		public RestLink addImage(
				String rel,
				String imgAttrId,
				Object subBoId,
				Object version
		) {
			return addImage(
					rel,
					imgAttrId,
					subBoId,
					version,
					boMetaId
			);
		}

		@Override
		public RestLink addImage(
				String rel,
				String imgAttrId,
				Object subBoId,
				Object version,
				String boMetaId
		) {
			if (refAttrId != null) {
				return imgLinks.addLinkHref(rel, "dependence_ImageValue", Verbs.GET,
						boMetaId, boId, refAttrId, subBoId, imgAttrId, version);
			} else if (dependency != null) {
				return imgLinks.addLinkHref(rel, "dependence_recursive_ImageValue", Verbs.GET,
						boMetaId, boId, dependency.toUrlPath().substring(1) + "/" + subBoId, imgAttrId, version);
			}

			return null;
		}

		@Override
		public RestLink addPrintout(String sTranslatedEntity, Object pk) {
			return null;
		}

		@Override
		public RestLink addInsert() {
			return null;
		}

		@Override
		public RestLink addClone(final Object boId) {
			return null;
		}

		@Override
		public RestLink addSubforminfo(String layoutId, Object pk) {
			// There is currently no rest service for 'subforminfo'
			/*if (layoutId != null && pk != null) {
				return links.addLink("subforminfo", Verbs.GET, layoutId, pk.toString());
			}*/
			return null;
		}

		@Override
		public RestLink addUnlock(Object pk) {
			return null;
		}

	}


	@POST
	@Path("/{refAttrId}/{subBoId}/execute/{ruleId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "subBoRuleExecution",
			description = "Executes a custom rule in subform context.",
			summary = "Execute custom rule",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													type = "object"
											)
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Custom Rules"
	)
	public JsonObject executeCustomRule(
			@Parameter(
					name = "boMetaId",
					required = true,
					description = "Businessobject Meta Identifier",
					example = "example_rest_Order"
			)
			@PathParam("boMetaId") String boMetaId,
			@Parameter(
					name = "boId",
					required = true,
					description = "Businessobject Identifier",
					example = "10090"
			)
			@PathParam("boId") String boId,
			@Parameter(
					name = "refAttrId",
					required = true,
					description = "Businessobject Attribute Identifier",
					example = "example_rest_Order_customer"
			)
			@PathParam("refAttrId") String refAttrId,
			@Parameter(
					name = "subBoId",
					required = true,
					description = "Subbusinessobject Identifier",
					example = "10090"
			)
			@PathParam("subBoId") String subBoId,
			@Parameter(
					name = "ruleId",
					required = true,
					description = "Rule Identifier",
					example = "customRuleToExecute"
			)
			@PathParam("ruleId") String ruleId,
			@Parameter(
					schema = @Schema(
							implementation = InputRequiredContext.class
					)
			) final JsonObject data    // TODO: Only needed for input-required data, get rid of this
	) {
		return executeRuleInSubformContext(boMetaId, boId, refAttrId, subBoId, ruleId, data).build();
	}

}
