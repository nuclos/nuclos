//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.nuclos.common.E;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.nuclos.schema.layout.rule.Rules;
import org.nuclos.schema.layout.vlp.ValueListProviders;
import org.nuclos.schema.layout.web.WebLayout;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.services.base.LayoutServiceBase;
import org.nuclos.server.rest.services.rvo.LayoutInfo;
import org.xml.sax.SAXException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/layout")
@Produces(MediaType.APPLICATION_JSON)
public class LayoutService extends LayoutServiceBase {

	@GET
	@Path("/{layoutId}/calculated")
	@Operation(
			operationId = "weblayoutCalculated",
			description = "Parsed weblayout (calc()-based) for the corresponding layout-id in JSON format.",
			summary = "Parsed weblayout (calc()-based) for the corresponding layout-id in JSON format.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = WebLayout.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Layouts"
	)
	public WebLayout layoutCalculated(
			@Parameter(
					name = "layoutId",
					required = true,
					description = "Layout Identifier",
					example = "example_rest_OrderL01"
			)
			@PathParam("layoutId") String layoutId) throws JAXBException, XMLStreamException {
		return getWebLayoutCalculated(layoutId);
	}

	@GET
	@Path("/{layoutId}/fixed")
	@Operation(
			operationId = "weblayoutFixed",
			description = "Parsed weblayout (table-based) for the corresponding layout-id in JSON format.",
			summary = "Parsed weblayout (table-based) for the corresponding layout-id in JSON format.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = WebLayout.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Layouts"
	)
	public WebLayout layoutFixed(
			@Parameter(
					name = "layoutId",
					required = true,
					description = "Layout Identifier",
					example = "example_rest_OrderL01"
			)
			@PathParam("layoutId") String layoutId) throws JAXBException, XMLStreamException {
		return getWebLayoutFixed(layoutId);
	}

	@GET
	@Path("/{layoutId}/responsive")
	@Operation(
			operationId = "weblayoutResponsive",
			description = "Parsed weblayout (grid-based) for the corresponding layout-id in JSON format.",
			summary = "Parsed weblayout (grid-based) for the corresponding layout-id in JSON format.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = WebLayout.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Layouts"
	)
	public WebLayout layoutResponsive(
			@Parameter(
					name = "layoutId",
					required = true,
					description = "Layout Identifier",
					example = "example_rest_OrderL01"
			)
			@PathParam("layoutId") String layoutId) throws JAXBException, XMLStreamException {
		return getWebLayoutResponsive(layoutId);
	}

	@GET
	@Path("/{layoutId}/layoutml")
	@Operation(
			operationId = "layoutml",
			description = "Returns the raw LayoutML String.",
			summary = "Returns the raw LayoutML String.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											type = "string",
											description = "LayoutML as string representation"
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Layouts"
	)
	public String layoutml(
			@Parameter(
					name = "layoutId",
					required = true,
					description = "Layout Identifier",
					example = "example_rest_OrderL01"
			)
			@PathParam("layoutId") String layoutId) {
		return super.getLayoutML(layoutId);
	}

	@GET
	@Path("/{layoutId}/layoutml_json")
	@Operation(
			operationId = "layoutmlJson",
			description = "Returns the LayoutML object as JSON.",
			summary = "Returns the LayoutML object as JSON.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = Layoutml.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Layouts"
	)
	public Layoutml layoutmlObj(
			@Parameter(
					name = "layoutId",
					required = true,
					description = "Layout Identifier",
					example = "example_rest_OrderL01"
			)
			@PathParam("layoutId") String layoutId) throws JAXBException, XMLStreamException {
		return getLayoutMLObj(layoutId);
	}

	/**
	 * TODO: Separate rules from the layout.
	 */
	@GET
	@Path("/{layoutId}/rules")
	@Operation(
			operationId = "layoutRules",
			description = "Extracted rules of the Layout with the given ID.",
			summary = "Extracted rules of the Layout with the given ID.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = Rules.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Layouts"
	)
	public Rules layoutRules(
			@Parameter(
					name = "layoutId",
					required = true,
					description = "Layout Identifier",
					example = "example_rest_OrderL01"
			)
			@PathParam("layoutId") String layoutId) throws JAXBException, XMLStreamException {
		return getRules(layoutId);
	}

	@GET
	@Path("/{layoutId}/vlps")
	@Operation(
			operationId = "layoutVlps",
			description = "Extracted value list providers of the Layout with the given ID.",
			summary = "Extracted value list providers of the Layout with the given ID.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = ValueListProviders.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Layouts"
	)
	public ValueListProviders layoutVlps(
			@Parameter(
					name = "layoutId",
					required = true,
					description = "Layout Identifier",
					example = "example_rest_OrderL01"
			)
			@PathParam("layoutId") String layoutId) throws JAXBException, XMLStreamException {
		return getValueListProviders(layoutId);
	}

	@GET
	@Path("/{layoutId}/info")
	@Operation(
			operationId = "layoutInfo",
			description = "Returns the Layout info.",
			summary = "Returns the Layout info.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = LayoutInfo.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			},
			tags = "Nuclos Layouts"
	)
	public LayoutInfo layoutInfo(
			@Parameter(
					name = "layoutId",
					required = true,
					description = "Layout Identifier",
					example = "example_rest_OrderL01"
			)
			@PathParam("layoutId") String layoutId) throws CommonPermissionException {
		return Rest.facade().getLayoutInfo(Rest.translateFqn(E.LAYOUT, layoutId));
	}

}
