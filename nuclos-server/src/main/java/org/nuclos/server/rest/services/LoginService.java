package org.nuclos.server.rest.services;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.common.RigidUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.schema.rest.DataLanguage;
import org.nuclos.schema.rest.LegalDisclaimer;
import org.nuclos.schema.rest.LocaleInfo;
import org.nuclos.schema.rest.LoginInfo;
import org.nuclos.schema.rest.LoginParams;
import org.nuclos.schema.rest.ServerStatus;
import org.nuclos.schema.rest.SsoAuthEndpoint;
import org.nuclos.server.common.ejb3.SecurityFacadeLocal;
import org.nuclos.server.customcode.codegenerator.NuclosJarGeneratorManager;
import org.nuclos.server.maintenance.MaintenanceConstants;
import org.nuclos.server.maintenance.MaintenanceFacadeLocal;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.LoginServiceHelper;
import org.nuclos.server.rest.services.helper.SessionValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/")
public class LoginService extends LoginServiceHelper {

	@Lazy
	@Autowired
	private NuclosJarGeneratorManager nuclosJarGeneratorManager;

	@Lazy
	@Autowired
	private MaintenanceFacadeLocal maintenanceFacade;

	@Lazy
	@Autowired
	private SecurityFacadeLocal securityFacade;

	@GET
	@Path("/login")
	@Produces({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "login",
			description = "Returns JsonObject with SessionID and link for BO-Metalist.",
			summary = "Authenticate against nuclos server",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = LoginInfo.class
									)
							)
					)
			},
			tags = "Nuclos Authentication"
	)
	public LoginInfo login(@Context HttpServletRequest request) {
		return buildLoginInfo();
	}

	/**
	 * For backward compatibility only.
	 * Endpoints with same path as their parent can cause problems, see NUCLOS-7765.
	 */
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@Deprecated
	@Operation(
			tags = "Nuclos Authentication"
	)
	public LoginInfo deprecatedLogin(@Context HttpServletRequest request) {
		return login(request);
	}

	@POST
	@Path("/login")
	@SessionValidation(enabled = false)
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "login",
			description = "User-Login. Returns JsonObject with SessionID and link for BO-Metalist.",
			summary = "Authenticate again nuclos server",
			requestBody = @RequestBody(
					content = @Content(
							schema = @Schema(
									implementation = LoginParams.class,
									description = "Login information to authenticate",
									example = "{\"username\": \"nuclos\", \"password\": \"\"}"
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = LoginInfo.class
									)
							)
					)
			},
			tags = "Nuclos Authentication"
	)
	public Response login(
			@Context HttpServletRequest request,
			final LoginParams data
	) {
		LoginResult result = doLogin(data, request);

		// TODO: A LoginInfo should be returned directly, instead of a generic Response.
		//  How to set cookies then?
		return Response.ok(result.getLoginInfo(), MediaType.APPLICATION_JSON)
				.cookie(result.getCookies())
				.build();
	}

	/**
	 * For backward compatibility only.
	 * Endpoints with same path as their parent can cause problems, see NUCLOS-7765.
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@SessionValidation(enabled = false)
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = LoginInfo.class
							)
					)
			),
			tags = "Nuclos Authentication"
	)
	public Response deprecatedLogin(
			@Context final HttpServletRequest request,
			final LoginParams data
	) {
		return login(request, data);
	}

	@GET
	@Path("locale")
	@Produces({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "getLocale",
			description = "Returns the server-side locale for the current session.",
			summary = "Get server locale",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = LocaleInfo.class
									)
							)
					)
			},
			tags = "Nuclos System"
	)
	public LocaleInfo doGetLocale() {
		final Locale locale = getLocale();

		return LocaleInfo.builder()
				.withLocale(locale.toString())
				.build();
	}

	@PUT
	@Path("locale")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "setLocale",
			description = "Sets the server-side locale for the current session.",
			summary = "Set server locale",
			requestBody = @RequestBody(
					content = @Content(
							schema = @Schema(
									implementation = LocaleInfo.class,
									description = "Locale information to set",
									example = "{\"locale\": \"de\"}"
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "204"
					)
			},
			tags = "Nuclos System"
	)
	public Response setLocale(
			final LocaleInfo data
	) {
		final String localeString = data.getLocale();
		setLocale(localeString);

		return Response.status(Response.Status.NO_CONTENT).build();
	}

	@GET
	@Path("datalanguage")
	@Produces({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "getDataLanguage",
			description = "Returns the server-side data language for the current session.",
			summary = "Return server data language",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = DataLanguage.class
									)
							)
					)
			},
			tags = "Nuclos System"
	)
	public DataLanguage doGetDataLanguage() {
		final Locale dataLanguage = getDataLanguage();

		return DataLanguage.builder()
				.withDatalanguage(dataLanguage.toString())
				.withDatalanguageLabel(dataLanguage.toString())
				.build();
	}

	@GET
	@Path("datalanguages")
	@SessionValidation(enabled = false)
	@Produces({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "getDataLanguages",
			description = "Returns list of all server-side data languages.",
			summary = "Return server data languages",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = DataLanguage.class
											)
									)
							)
					)
			},
			tags = "Nuclos System"
	)
	public List<DataLanguage> getDataLanguages(
			@Parameter(
					name = "language",
					description = "language for displaying the label",
					example = "de"
			)
			@QueryParam("language") String language
	) {
		if (dataLanguageCache != null) {
			final Locale languageLocale = RigidUtils.looksEmpty(language) ? null :
				Arrays.stream(Locale.getAvailableLocales())
					.filter(l -> language.equals(l.getLanguage()))
					.findFirst()
					.orElse(null);

			return dataLanguageCache
					.getDataLanguages().stream()
					.map(dataLanguage -> DataLanguage.builder()
							.withDatalanguage(dataLanguage.getLocale().toString())
							.withDatalanguageLabel(dataLanguage.toString(languageLocale))
							.withPrimary(dataLanguage.isPrimary())
							.build())
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	@PUT
	@Path("datalanguage")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(tags = "Nuclos Authentication")
	public void setDataLanguage(
			final DataLanguage data
	) {
		final String sDataLanguage = data.getDatalanguage();
		setDataLanguage(sDataLanguage);
	}

	@POST
	@Path("chooseMandator/{mandatorId}")
	@Operation(
			operationId = "chooseMandator",
			description = "Choose the mandator after a user logs in. The Result of the login contains a list of possible mandators to choose.",
			summary = "Choose the mandator after a user logs in. The Result of the login contains a list of possible mandators to choose.", tags = "Nuclos User")
	public void chooseMandator(
			@Parameter(
					name = "mandatorId",
					required = true,
					description = "Mandator Identifier",
					example = "213sd"
			)
			@PathParam("mandatorId") String mandatorId) {
		doChooseMandator(mandatorId);
	}

	//FIXME: HATEOAS Standard dictates that @DELETE-Method must delete persistent object on the DB. See also BoService @Path("/{boMetaId}/query")
	@DELETE
	@Path("/login")
	@Operation(
			operationId = "logout",
			description = "User-Logout. Returns HTTP-Status 200, if successful",
			summary = "User-Logout. Returns HTTP-Status 200, if successful", tags = "Nuclos Authentication")
	public void logout(@Context HttpServletRequest request) {
		doLogout(request);
	}

	/**
	 * For backward compatibility only.
	 * Endpoints with same path as their parent can cause problems, see NUCLOS-7765.
	 */
	@DELETE
	@Deprecated
	@Operation(
			tags = "Nuclos Authentication"
	)
	public void deprecatedLogout(@Context HttpServletRequest request) {
		logout(request);
	}

	@GET
	@Path("serverstatus")
	@SessionValidation(enabled = false)
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			operationId = "serverstatus",
			description = "Status of the nuclos server",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = ServerStatus.class
									)
							)
					),
					@ApiResponse(
							responseCode = "503",
							description = "If server is not ready yet"
					)
			},
			summary = "Status of the nuclos server", tags = "Nuclos System")
	public ServerStatus serverstatus() {
		boolean ready =
				SpringApplicationContextHolder.isNuclosReady() &&
						!Optional.ofNullable(nuclosJarGeneratorManager)
								.map(NuclosJarGeneratorManager::isGeneratorRunningAndWaitFor)
								.orElse(true);

		if (!ready) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
		}

		ServerStatus result = ServerStatus.builder()
				.withReady(true)
				.build();
		if (!MaintenanceConstants.MAINTENANCE_MODE_OFF.equals(
				Optional.ofNullable(maintenanceFacade).map(
						MaintenanceFacadeLocal::getMaintenanceMode
				).orElse("")
			)
		) {
			result.setMaintenance(true);
		}
		return result;
	}

	@GET
	@Path("version")
	@SessionValidation(enabled = false)
	@Operation(
			operationId = "version",
			description = "Version of the nuclos server",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Returning string representation of current Nuclos version"
					)
			},
			summary = "Version of the nuclos server", tags = "Nuclos System")
	public String version() {
		return getVersion();
	}

	@GET
	@Path("version/db")
	@SessionValidation(enabled = false)
	@Operation(
			operationId = "dbversion",
			description = "Version of DB schema of the nuclos server. Actually accesses the DB.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Returning string representation of current DB schema version"
					)
			},
			summary = "Version of DB schema of the nuclos server. Actually accesses the DB.", tags = "Nuclos System")
	public String dbversion() {
		return getDbVersion();
	}

	@GET
	@Path("version/timestamp")
	@SessionValidation(enabled = false)
	@Operation(
			operationId = "versiontimestamp",
			description = "Nuclos build timestamp.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Returning string representation of Nuclos build timestamp"
					)
			},
			summary = "Nuclos build timestamp.", tags = "Nuclos System")
	public String versionDate() {
		return getVersionTimestamp();
	}

	@GET
	@Path("version/legaldisclaimer")
	@SessionValidation(enabled = false)
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			operationId = "legaldisclaimer",
			description = "Legal disclaimer about Nuclos and this application.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "List of legal disclaimer",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = LegalDisclaimer.class
											)
									)
							)
					)
			},
			summary = "Legal disclaimer about Nuclos and this application.", tags = "Nuclos System")
	public List<LegalDisclaimer> getLegalDisclaimers() {
		return super.getLegalDisclaimers();
	}

	@GET
	@Path("ssoAuthEndpoints")
	@SessionValidation(enabled = false)
	@Produces({MediaType.APPLICATION_JSON})
	@Operation(
			operationId = "ssoAuthEndpoints",
			description = "Returns the registered OAuth 2 endpoints for this instance, if any.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "List of SSO Endpoints",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = SsoAuthEndpoint.class
											)
									)
							)
					)
			},
			summary = "Returns the registered OAuth 2 endpoints for this instance, if any.", tags = "Nuclos Authentication")
	public List<SsoAuthEndpoint> getSsoAuthEndpoints() {
		if (securityFacade == null) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
		}
		return securityFacade.getSsoAuthEndpoints();
	}

	@PUT
	@Path("ssoAuthEndpoints/{ssoAuthId}")
	@SessionValidation(enabled = false)
	@Produces({MediaType.TEXT_PLAIN})
	@Operation(
			operationId = "startSsoAuthentication",
			description = "Start a authentication and returns the URI to the registered OAuth 2 endpoint.",
			summary = "Start a authentication and returns the URI to the registered OAuth 2 endpoint.", tags = "Nuclos Authentication")
	public String startSsoAuthentication(@PathParam("ssoAuthId") String ssoAuthId) {
		if (securityFacade == null) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
		}
		if (StringUtils.looksEmpty(ssoAuthId)) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST);
		}
		return securityFacade.startNuclosSsoAuthentication(UID.parseUID(ssoAuthId)).toString();
	}
}
