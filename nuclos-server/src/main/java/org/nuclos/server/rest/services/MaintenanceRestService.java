//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.apache.logging.log4j.core.config.AbstractConfiguration;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.NucletExport;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common.dbtransfer.TransferOption;
import org.nuclos.common2.BoDataSet;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.schema.rest.DebugSql;
import org.nuclos.schema.rest.JobStatus;
import org.nuclos.schema.rest.LogLevel;
import org.nuclos.schema.rest.Loggers;
import org.nuclos.server.dblayer.impl.DataSourceExecutor;
import org.nuclos.server.dbtransfer.ConstraintAction;
import org.nuclos.server.dbtransfer.TransferFacadeBean;
import org.nuclos.server.job.ejb3.JobControlFacadeBean;
import org.nuclos.server.maintenance.MaintenanceConstants;
import org.nuclos.server.maintenance.MaintenanceFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.WebContext;
import org.owasp.encoder.Encode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Encoding;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;


@Path("/maintenance")
public class MaintenanceRestService extends WebContext {

	private static final Logger LOG = LogManager.getLogger(MaintenanceRestService.class);

	@Autowired
	private MasterDataRestFqnCache masterDataRestFqnCache;

	@Autowired
	private JobControlFacadeBean jobControlFacadeBean;

	@Autowired
	MasterDataFacadeLocal masterDataFacade;

	@GET
	@Path("/mode")
	@Produces(MediaType.TEXT_PLAIN)
	@Operation(
			description = "Retrieve status of current maintenance mode",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Status of maintenance",
							content = @Content(
									schema = @Schema(
											type = "string",
											allowableValues = {
													MaintenanceConstants.MAINTENANCE_MODE_INITIALIZED,
													MaintenanceConstants.MAINTENANCE_MODE_OFF,
													MaintenanceConstants.MAINTENANCE_MODE_ON
											}
									)
							)
					)
			},
			tags = "Nuclos Maintenance"
	)
	public String mode() {
		MaintenanceFacadeBean maintenanceFacadeBean = SpringApplicationContextHolder.getBean(MaintenanceFacadeBean.class);
		return maintenanceFacadeBean.getMaintenanceMode();
	}

	@PUT
	@Path("/mode")
	@Produces(MediaType.TEXT_PLAIN)
	@Operation(
			description = "Update status of maintenance mode",
			requestBody = @RequestBody(
					content = @Content(
							mediaType = "text/plain",
							schema = @Schema(
									type = "string",
									allowableValues = {
											MaintenanceConstants.MAINTENANCE_MODE_OFF,
											MaintenanceConstants.MAINTENANCE_MODE_ON
									}
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Status of new maintenance mode",
							content = @Content(
									schema = @Schema(
											type = "string",
											allowableValues = {
													MaintenanceConstants.MAINTENANCE_MODE_INITIALIZED,
													MaintenanceConstants.MAINTENANCE_MODE_OFF,
													MaintenanceConstants.MAINTENANCE_MODE_ON
											}
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "If not authorized to change maintenance"
					),
					@ApiResponse(
							responseCode = "406",
							description = "Given maintenance mode is not supported"
					)
			},
			tags = "Nuclos Maintenance"
	)
	public String setMode(final String mode) {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_MAINTENANCE_MODE)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		final boolean force = LangUtils.defaultIfNull(getFirstParameter("force", Boolean.class), false);

		if (MaintenanceConstants.MAINTENANCE_MODE_ON.equalsIgnoreCase(mode)) {
			if (force) {
				force();
			}
			return start();
		} else if (MaintenanceConstants.MAINTENANCE_MODE_OFF.equalsIgnoreCase(mode)) {
			return end();
		}

		throw new NuclosWebException(Status.NOT_ACCEPTABLE);
	}

	private String start() {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_MAINTENANCE_MODE)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		MaintenanceFacadeBean maintenanceFacadeBean = SpringApplicationContextHolder.getBean(MaintenanceFacadeBean.class);
		String username = getUser();
		return maintenanceFacadeBean.enterMaintenanceMode(username);
	}

	private void force() {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_MAINTENANCE_MODE)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}
		SpringApplicationContextHolder.getBean(MaintenanceFacadeBean.class).forceMaintenanceMode();
	}

	private String end() {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_MAINTENANCE_MODE)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		MaintenanceFacadeBean maintenanceFacadeBean = SpringApplicationContextHolder.getBean(MaintenanceFacadeBean.class);
		try {
			return maintenanceFacadeBean.exitMaintenanceMode();
		} catch (org.nuclos.common2.exception.CommonValidationException e) {
			throw new NuclosWebException(e);
		}
	}

	/**
	 * Beispielaufruf:
	 * sessionid=`curl http://localhost:8080/nuclos-war/rest -X GET_POST -H "Content-Type: application/json" -d '{"username":"nuclos"}' | awk -v FS="\"" '{ print $4  }'`
	 * curl http://localhost:8080/nuclos-war/rest/maintenance/dbexport?sessionid=$sessionid > full.xml.gz
	 */
	@GET
	@Path("/dbexport")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Operation(
			description = "Export nuclos database",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											type = "application/octet-stream",
											implementation = GZIPOutputStream.class
									)
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "If not authorized to export data"
					),
			},
			tags = "Nuclos Maintenance"
	)
	public Response dbexport() {
		checkSuperUser();

		try {
			final BoDataSet dataSet = Rest.facade().importOrExportDatabase(null);
			StreamingOutput out = output -> {
				GZIPOutputStream gzipos = new GZIPOutputStream(output);
				dataSet.write(gzipos);
				gzipos.close();
			};
			return Response.ok(out).build();

		} catch (Exception e) {
			// TODO
			EntityMeta<?> TODO = null;
			throw new NuclosWebException(e, Rest.translateFqn(TODO, "dbexport"));
		}
	}

	/**
	 * Beispielaufruf:
	 * sessionid=`curl http://localhost:8080/nuclos-war/rest -X GET_POST -H "Content-Type: application/json" -d '{"username":"nuclos"}' | awk -v FS="\"" '{ print $4  }'`
	 * curl -F "file=@full.xml.gz" http://localhost:8080/nuclos-war/rest/maintenance/dbimport?sessionid=$sessionid
	 */
	@POST
	@Path("/dbimport")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Operation(
			description = "Import file to database",
			requestBody = @RequestBody(
					required = true,
					content = @Content(
							mediaType = "multipart/form-data",
							schema = @Schema(
									implementation = BoDocumentService.MultiRequest.class
							),
							encoding = @Encoding(
									name = "file",
									contentType = "*/*"
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "Successfully imported"
					),
					@ApiResponse(
							responseCode = "403",
							description = "If not authorized to import data"
					),
			},
			tags = "Nuclos Maintenance"
	)
	public Response dbimport(@FormDataParam("file") InputStream is) {
		checkSuperUser();

		try {
			GZIPInputStream gzipin = new GZIPInputStream(is);
			Rest.facade().importOrExportDatabase(gzipin);
			gzipin.close();

			return Response.status(Status.NO_CONTENT).build();
		} catch (Exception e) {
			// TODO
			EntityMeta<?> TODO = null;
			throw new NuclosWebException(e, Rest.translateFqn(TODO, "dbimport"));
		}
	}

	@POST
	@Path("/nucletexport/{nucletName}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Operation(
			tags = "Nuclos Maintenance"
	)
	public Response nucletexport(@PathParam("nucletName") String nucletName) {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_NUCLET_EXPORT)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		final boolean isDirectoryMode = LangUtils.defaultIfNull(getFirstParameter("isDirectoryMode", Boolean.class), true);

		TransferFacadeBean transferFacadeBean = SpringApplicationContextHolder.getBean(TransferFacadeBean.class);
		MasterDataFacadeLocal masterDataFacade = SpringApplicationContextHolder.getBean(MasterDataFacadeLocal.class);

		Collection<MasterDataVO<UID>> nuclets = masterDataFacade.getMasterData(
				E.NUCLET,
				SearchConditionUtils.newComparison(E.NUCLET.name, ComparisonOperator.EQUAL, nucletName));
		if (nuclets.size() != 1) {
			String error = "Unable to find unique nuclet with name '" + nucletName + "'.";
			LOG.error(error);
			throw new NuclosWebException(Status.EXPECTATION_FAILED, error);
		}

		UID nucletUID = nuclets.iterator().next().getPrimaryKey();

		Map<TransferOption, Serializable> transferOptions = new HashMap<>();
		if (isDirectoryMode) {
			transferOptions.put(TransferOption.IS_DIRECTORY_MODE, null);
		}

		try {
			NucletExport export = new NucletExport();
			export.setNucletUID(nucletUID);
			export.setExportOptions(transferOptions);
			byte[] result = transferFacadeBean.createTransferFile(export);
			return Response.ok(result).build();
		} catch (Exception e) {
			LOG.error("Unable to export nuclet with id='" + nucletUID + "'.", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * Beispielaufruf Powershell:
	 * Invoke-RestMethod -Verbose -Method Post -Uri "http://localhost:8080/nuclos-war/rest" -Body '{"username":"nuclos", "password":""}' -ContentType 'application/json' -SessionVariable myWebSession
	 * Invoke-RestMethod -WebSession $myWebSession -Uri "http://localhost:8080/nuclos-war/rest/maintenance/nucletimport" -Method Post -InFile test.nuclet -ContentType 'application/octet-stream'
	 */
	@POST
	@Path("/nucletimport")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	@Operation(
			tags = "Nuclos Maintenance"
	)
	public void nucletimport(InputStream is) {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_NUCLET_IMPORT)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}
		nucletimport(is, null);
	}

	/**
	 * Beispielaufruf:
	 * sessionid=`curl http://localhost:8080/nuclos-war/rest -X GET_POST -H "Content-Type: application/json" -d '{"username":"nuclos", "password":""}' | awk -v FS="\"" '{ print $4  }'`
	 * curl -F "file=@test.nuclet" "http://localhost:8080/nuclos-war/rest/maintenance/nucletimport?sessionid=$sessionid"
	 */
	@POST
	@Path("/nucletimport")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Operation(
			tags = "Nuclos Maintenance"
	)
	public void nucletimport(
			@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition formData
	) {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_NUCLET_IMPORT)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}
		try {
			byte[] bytes = IOUtils.toByteArray(is);
			String fileName = null;
			if (formData != null) fileName = formData.getFileName();
			Transfer.Result result = null;
			try {
				Transfer importTransferObject = prepareTransfer(bytes, fileName);
				if (importTransferObject.result.hasCriticals()) {
					throw new NuclosWebException(Status.NOT_ACCEPTABLE, importTransferObject.result.getCriticals());
				}
				Transfer transfer = new Transfer(importTransferObject);
				transfer.setParameter(syncParameters(transfer));
				result = runTransfer(transfer);
			} catch (NuclosBusinessException e) {
				LOG.error("Unable to import nuclet '" + fileName + "'.", e);
				throw new NuclosWebException(Status.INTERNAL_SERVER_ERROR);
			}
			if (result != null && result.hasCriticals()) {
				LOG.warn("nucletimport with criticals: " + result.getCriticals());
				throw new NuclosWebException(Status.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			LOG.warn("nucletimport failed: " + e, e);
			throw new NuclosWebException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected Transfer prepareTransfer(final byte[] bytes, final String fileName) throws NuclosBusinessException {
		return SpringApplicationContextHolder.getBean(TransferFacadeBean.class).prepareTransfer(bytes, fileName, false, ConstraintAction.CLEAN);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected Transfer.Result runTransfer(final Transfer transfer) throws NuclosBusinessException {
		return SpringApplicationContextHolder.getBean(TransferFacadeBean.class).runTransfer(transfer, ConstraintAction.REBUILD);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected Collection<EntityObjectVO<UID>> syncParameters(final Transfer transfer) {
		Collection<MasterDataVO<UID>> params = masterDataFacade.getMasterData(E.NUCLETPARAMETER, TrueCondition.TRUE);
		Collection<EntityObjectVO<UID>> oldParams = new ArrayList<EntityObjectVO<UID>>();
		for (MasterDataVO<UID> param : params) {
			oldParams.add(param.getEntityObject());
		}

		Collection<EntityObjectVO<UID>> newParams = transfer.getParameter();

		return TransferFacadeBean.synchronizeParameters(oldParams, newParams, "null");
	}

	@GET
	@Path("/logging")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = Loggers.class
							)
					)
			),
			tags = "Nuclos Maintenance"
	)
	public JsonObject getLoggerNames() {
		if (!Rest.facade().isWriteAllowedForMD(E.PARAMETER.getUID())) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		JsonObjectBuilder json = Json.createObjectBuilder();
		Enumeration<Logger> loggers = Logger.getRootLogger().getCurrentCategories();
		JsonArrayBuilder jsonarray = Json.createArrayBuilder();
		while (loggers.hasMoreElements()) {
			Logger logger = loggers.nextElement();
			if (logger.getLevel() != null) {
				JsonObjectBuilder jsonNode = Json.createObjectBuilder();
				jsonNode.add("name", logger.getName());
				jsonNode.add("level", "" + logger.getLevel());
				jsonNode.add("href", restURL("/maintenance/logging/" + logger.getName() + "/level"));
				jsonarray.add(jsonNode);
			}
		}
		json.add("logger", jsonarray);
		return json.build();
	}

	@GET
	@Path("/logging/server.log")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Operation(
			summary = "Download Server log",
			tags = "Nuclos Maintenance",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "application/octet-stream"
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Not allowed to download file"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Log File not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public Response getServerLogFile() {
		return getLogFileResponse("Logfile", "server.log");
	}

	@GET
	@Path("/logging/stacktrace-business.log")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Operation(
			summary = "Download StacktraceBusiness log",
			tags = "Nuclos Maintenance",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "application/octet-stream"
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Not allowed to download file"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Log File not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public Response getStacktraceBusinessFile() {
		return getLogFileResponse("StacktraceBusiness", "stacktrace-business.log");
	}

	@GET
	@Path("/logging/stacktrace-internal.log")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Operation(
			summary = "Download StacktraceInternal log",
			tags = "Nuclos Maintenance",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									mediaType = "application/octet-stream"
							)
					),
					@ApiResponse(
							responseCode = "403",
							description = "Not allowed to download file"
					),
					@ApiResponse(
							responseCode = "404",
							description = "Log File not found"
					),
					@ApiResponse(
							responseCode = "500",
							description = "Retrieving the objects failed on the server"
					)
			}
	)
	public Response getStacktraceInternalFile() {
		return getLogFileResponse("StacktraceInternal", "stacktrace-internal.log");
	}

	private Response getLogFileResponse(String appenderFile, String logFileName) {
		if (!Rest.facade().isWriteAllowedForMD(E.PARAMETER.getUID())) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		try {
			final String fileName;
			lookupFileName:
			{
				final LoggerContext ctx = (LoggerContext) org.apache.logging.log4j.LogManager.getContext(false);
				final AbstractConfiguration config = (AbstractConfiguration) ctx.getConfiguration();
				fileName = ((RollingFileAppender) config.getAppender(appenderFile)).getFileName();
			}

			File file = new File(fileName);
			Response.ResponseBuilder response = Response.ok(file);
			response.header("Content-Disposition", "attachment; filename=" + Encode.forJava(logFileName));
			response.header("Content-Length", FileUtils.sizeOf(file));

			return response.build();
		} catch (Exception ignored) {
			return Response.status(404).build();
		}
	}

	@GET
	@Path("/logging/{logger}/level")
	@Produces({MediaType.APPLICATION_JSON})
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = LogLevel.class
							)
					)
			),
			tags = "Nuclos Maintenance"
	)
	public JsonObject getLogLevel(@PathParam("logger") String logger) {
		if (!Rest.facade().isWriteAllowedForMD(E.PARAMETER.getUID())) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("level", "" + Logger.getLogger(logger).getLevel());
		return json.build();
	}

	@PUT
	@Path("/logging/{logger}/level")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = LogLevel.class
							)
					)
			),
			tags = "Nuclos Maintenance"
	)
	public JsonObject setLogLevel(
			@PathParam("logger") String logger,
			@Parameter(
					schema = @Schema(
							implementation = LogLevel.class
					)
			) JsonObject data
	) {
		if (!Rest.facade().isWriteAllowedForMD(E.PARAMETER.getUID())) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		Logger l = Logger.getLogger(logger);
		String level = "" + data.getString("level");
		if (level != null) {
			l.setLevel(Level.toLevel(level));
		}

		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("level", "" + l.getLevel());
		return json.build();
	}

	private final static String debugSQLKey = "debugSQL";
	private final static String minExecTimeKey = "minExecTime";

	@GET
	@Path("/logging/debugSQL")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = DebugSql.class
							)
					)
			),
			tags = "Nuclos Maintenance"
	)
	public JsonObject getDebugSQL() {
		if (!Rest.facade().isWriteAllowedForMD(E.PARAMETER.getUID())) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		JsonObjectBuilder json = Json.createObjectBuilder();
		if (DataSourceExecutor.getDebugSQL() == null) {
			json.add(debugSQLKey, JsonValue.NULL);
		} else {
			json.add(debugSQLKey, DataSourceExecutor.getDebugSQL());
		}

		if (DataSourceExecutor.getMinExecutionTimeMS() == null) {
			json.add(minExecTimeKey, JsonValue.NULL);
		} else {
			json.add(minExecTimeKey, DataSourceExecutor.getMinExecutionTimeMS());
		}

		return json.build();
	}

	@PUT
	@Path("/logging/debugSQL")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = DebugSql.class
							)
					)
			),
			tags = "Nuclos Maintenance"
	)
	public JsonObject setDebugSQL(
			@Parameter(
					schema = @Schema(
							implementation = DebugSql.class
					)
			) JsonObject data
	) {
		if (!Rest.facade().isWriteAllowedForMD(E.PARAMETER.getUID())) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add(debugSQLKey, JsonValue.NULL);
		json.add(minExecTimeKey, JsonValue.NULL);

		String debugSQL = null;
		if (data.containsKey(debugSQLKey) && data.get(debugSQLKey) instanceof JsonString) {
			debugSQL = data.getString(debugSQLKey);
			if ("null".equals(debugSQL) || StringUtils.isNullOrEmpty(debugSQL)) {
				debugSQL = null;
			} else {
				json.add(debugSQLKey, debugSQL);
			}
		}

		Integer minExecTimeMS = null;
		if (data.containsKey(minExecTimeKey) && data.get(minExecTimeKey) instanceof JsonNumber) {
			minExecTimeMS = data.getInt(minExecTimeKey);
			if (minExecTimeMS == 0) {
				minExecTimeMS = null;
			} else {
				json.add(minExecTimeKey, minExecTimeMS);
			}
		}

		DataSourceExecutor.setDebugSQL(debugSQL);
		DataSourceExecutor.setMinExecutionTimeMS(minExecTimeMS);

		return json.build();
	}

	@POST
	@Path("/jobs/{jobName}/start")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = JobStatus.class
							)
					)
			),
			tags = "Nuclos Maintenance"
	)
	public JsonObject startJob(@PathParam("jobName") String jobName) {
		if (!Rest.facade().isWriteAllowedForMD(E.JOBCONTROLLER.getUID())) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		JsonObjectBuilder json = Json.createObjectBuilder();

		if (jobName == null) {
			json.add("status", "job name missing");
		} else {
			try {
				UID jobUid = masterDataRestFqnCache.translateFqn(E.JOBCONTROLLER, jobName);
				if (jobUid == null) {
					json.add("status", "unknown job: " + jobName);
				} else {
					jobControlFacadeBean.startJobImmediately(jobUid);
					json.add("status", "ok");
				}
			} catch (Exception ex) {
				json.add("status", "Failed to start job: " + ex.getMessage());
			}
		}

		return json.build();
	}

	@GET
	@Path("/managementconsole")
	@Produces(MediaType.TEXT_PLAIN)
	@Operation(
			tags = "Nuclos Maintenance"
	)
	public String managementConsole() throws Exception {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_MANAGEMENT_CONSOLE)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}
		return Rest.facade().executeManagementConsole(null, null);
	}

	@POST
	@Path("/managementconsole/{command}")
	@Produces(MediaType.TEXT_PLAIN)
	@Operation(
			tags = "Nuclos Maintenance"
	)
	public String managementConsole(@PathParam("command") String sCommand) throws Exception {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_MANAGEMENT_CONSOLE)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}
		return Rest.facade().executeManagementConsole(sCommand, null);
	}

	@POST
	@Path("/managementconsole/{command}/{arguments}")
	@Produces(MediaType.TEXT_PLAIN)
	@Operation(
			tags = "Nuclos Maintenance"
	)
	public String managementConsole(
			@PathParam("command") String sCommand,
			@PathParam("arguments") String sArguments
	) throws Exception {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_MANAGEMENT_CONSOLE)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}
		return Rest.facade().executeManagementConsole(sCommand, sArguments);
	}

}
