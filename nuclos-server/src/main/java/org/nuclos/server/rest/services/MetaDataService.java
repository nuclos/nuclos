//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.resource.ResourceResolver;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.schema.rest.NucletExtensionsStyles;
import org.nuclos.schema.rest.RestSystemparameters;
import org.nuclos.schema.rest.SearchfilterEntityMeta;
import org.nuclos.schema.rest.SearchfilterInfo;
import org.nuclos.schema.rest.TaskEntityMeta;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.common.mail.NuclosMailServiceProvider;
import org.nuclos.server.rest.CacheableResponseBuilder;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.MetaDataServiceHelper;
import org.nuclos.server.rest.services.helper.SessionValidation;
import org.nuclos.server.rest.services.helper.TableViewLayout;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.nuclos.server.rest.services.rvo.MenuTreeNodeRVO;
import org.nuclos.server.rest.services.rvo.WebAddonUsagesRVO;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/meta")
@Produces(MediaType.APPLICATION_JSON)
public class MetaDataService extends MetaDataServiceHelper {

	private static final Logger LOG = LoggerFactory.getLogger(MetaDataService.class);

	public static final int RESOURCE_ICON_MAX_AGE_IN_SECDONS = 60 * 60 * 1;

	public static final Date CLASS_LOADING_TIME = new Date();

	private DataServiceHelper dataServiceHelper;

	@Autowired
	JsonFactory jsonFactory;

	@Autowired
	private NuclosMailServiceProvider mailServiceProvider;

	@Autowired
	private StateCache stateCache;

	@GET
	@Path("/menustructure")
	@Operation(
			operationId = "menustructure",
			description = "Menu-item structure. Includes all Businessobject-metas",
			summary = "Menu-item structure. Includes all Businessobject-metas",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = MenuTreeNodeRVO.class
									)
							)
					)
			},
			tags = "Nuclos System"
	)
	public MenuTreeNodeRVO menustructure() {
		return getMenuStructure();
	}

	@GET
	@Path("/layout/{layoutId}")
	@Operation(
			operationId = "layout",
			description = "Parsed layout for the corresponding layout-id in JSON format",
			summary = "Parsed layout for the corresponding layout-id in JSON format",
			tags = "Nuclos Layouts"
	)
	public JsonObject layout(@PathParam("layoutId") String layoutId) {
		return getLayout(layoutId).build();
	}

	/**
	 * @return A flat list of all dynamic and searchfilter based task lists
	 *
	 * TODO: Result is not structured by menu path, the individual tasklists should therefore have a corresponding attribute
	 */
	@GET
	@Path("/tasklists")
	@Operation(
			operationId = "tasklists",
			description = "List of all searchfilter based and dynamic task lists for the current user.",
			summary = "List of all searchfilter based and dynamic task lists for the current user.",
			tags = "Nuclos Meta Information"
	)
	public List<TaskEntityMeta> tasklists() {
		return getTaskLists();
	}

	@GET
	@Path("/processchange/{processId}/{boMetaId}")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = RestLinks.class
							)
					)
			),
			tags = "Nuclos Layouts"
	)
	public JsonValue processchange(
			@PathParam("processId") String processId,
			@PathParam("boMetaId") String boMetaId
	) {
		return getProcessChange(processId, boMetaId, null).build();
	}

	@GET
	@Path("/processchange/{processId}/{boMetaId}/{boId}")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = RestLinks.class
							)
					)
			),
			tags = "Nuclos Layouts"
	)
	public JsonValue processchange(
			@PathParam("processId") String processId,
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId
	) {
		return getProcessChange(processId, boMetaId, boId).build();
	}

	/**
	 * @deprecated column layout is stored in preferences
	 */
	@Deprecated
	@GET
	@Path("/tableviewlayout/{parentUid}/{dependenceUid}")
	@Operation(
			tags = "Nuclos Layouts"
	)
	public JsonValue tableviewlayout(@PathParam("parentUid") String parentUid, @PathParam("dependenceUid") String dependenceUid) {
		checkPermission(E.ENTITY, parentUid);

		TableViewLayout tableViewLayout = getDataServiceHelper().getTableViewLayout(Rest.translateFqn(E.ENTITY, parentUid), Rest.translateFqn(E.ENTITY, dependenceUid));
		return jsonFactory.buildJsonObject(tableViewLayout, this).build();
	}

	/**
	 * @deprecated column layout is stored in preferences
	 */
	@Deprecated
	@GET
	@Path("/tableviewlayout/{uid}")
	@Operation(
			tags = "Nuclos Layouts"
	)
	public JsonValue tableviewlayout(@PathParam("uid") String uid) {
		checkPermission(E.ENTITY, uid);

		TableViewLayout tableViewLayout = getDataServiceHelper().getTableViewLayout(Rest.translateFqn(E.ENTITY, uid));
		return jsonFactory.buildJsonObject(tableViewLayout, this).build();
	}

	/**
	 * @deprecated Replaced by preferences.
	 */
	@Deprecated
	@PUT
	@Path("/storetableviewlayout/{uid}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "If everything was ok"
					)
			},
			tags = "Nuclos Layouts"
	)
	public Response storetableviewlayout(@PathParam("uid") String uid, JsonObject data) {

		checkPermission(E.ENTITY, uid);

		getDataServiceHelper().storeTableViewLayout(
				uid,
				null,
				data
		);

		return Response.noContent().build();
	}

	/**
	 * @deprecated Replaced by preferences.
	 */
	@Deprecated
	@PUT
	@Path("/storetableviewlayout/{uid}/{dependenceUid}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "If everything was ok"
					)
			},
			tags = "Nuclos Layouts"
	)
	public Response storetableviewlayout(@PathParam("uid") String uid, @PathParam("dependenceUid") String dependenceUid, JsonObject data) {

		checkPermission(E.ENTITY, uid);

		getDataServiceHelper().storeTableViewLayout(
				uid,
				dependenceUid,
				data
		);

		return Response.noContent().build();
	}


	/**
	 * @deprecated Replaced by preferences.
	 */
	@Deprecated
	@POST
	@Path("/resettableviewlayout/{uid}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "If everything was ok"
					)
			},
			tags = "Nuclos Layouts"
	)
	public Response resettableviewlayout(@PathParam("uid") String uid) {

		checkPermission(E.ENTITY, uid);

		getDataServiceHelper().resetTableViewLayout(
				uid,
				null
		);

		return Response.noContent().build();
	}


	/**
	 * @deprecated Replaced by preferences.
	 */
	@Deprecated
	@POST
	@Path("/resettableviewlayout/{uid}/{dependenceUid}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "If everything was ok"
					)
			},
			tags = "Nuclos Layouts"
	)
	public Response resettableviewlayout(@PathParam("uid") String uid, @PathParam("dependenceUid") String dependenceUid) {

		checkPermission(E.ENTITY, uid);

		getDataServiceHelper().resetTableViewLayout(
				uid,
				dependenceUid
		);

		return Response.noContent().build();
	}

	@GET
	@Path("/searchfilter/{uid}")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = SearchfilterEntityMeta.class
							)
					)
			),
			tags = "Nuclos Meta Information"
	)
	public JsonObject searchfilter(@PathParam("uid") String uid) {
		try {
			EntitySearchFilter2 sf = Rest.facade().getSearchFilterByPk(Rest.translateFqn(E.SEARCHFILTER, uid));
			if (sf == null) {
				throw new WebApplicationException(Status.BAD_REQUEST);
			}
			checkPermission(E.SEARCHFILTER, Rest.translateUid(E.SEARCHFILTER, sf.getVO().getEntity()));
			//TODO: Avoid build() here
			JsonObject jsonEM = jsonFactory.buildJsonObject(getBOMeta(), this).build();
			JsonObjectBuilder b = Json.createObjectBuilder();
			for (Entry<String, JsonValue> entry : jsonEM.entrySet()) {
				b.add(entry.getKey(), entry.getValue());
			}
			b.add("searchfilter", Rest.translateUid(E.SEARCHFILTER, sf.getVO().getPrimaryKey()));
			return b.build();
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.SEARCHFILTER, uid));
		}
	}

	@GET
	@Path("/searchfilters/{uid}")
	@Operation(
			responses = @ApiResponse(
					content = @Content(
							array = @ArraySchema(
									schema = @Schema(
											implementation = SearchfilterInfo.class
									)
							)
					)
			),
			tags = "Nuclos Meta Information"
	)
	public JsonArray searchfilters(@PathParam("uid") String uid) {
		try {
			UID entityUID = Rest.translateFqn(E.ENTITY, uid);
			List<SearchFilterVO> searchFilters = Rest.facade().getSearchFilters();
			JsonArrayBuilder jsonArray = Json.createArrayBuilder();

			for (SearchFilterVO searchFilter : searchFilters) {
				if (entityUID.equals(searchFilter.getEntity()) && Boolean.TRUE.equals(searchFilter.getFastSelectInResult())) {
					jsonArray.add(
							Json.createObjectBuilder()
									.add("pk", "" + searchFilter.getPrimaryKey())
									.add("filterName", "" + searchFilter.getFilterName())
									.add("default", "" + searchFilter.getFastSelectDefault())
									.add("order", LangUtils.defaultIfNull(searchFilter.getOrder(), Integer.MAX_VALUE))
									.add("icon", searchFilter.getFastSelectIcon() != null ? searchFilter.getFastSelectIcon().toString() : "")
					);
				}
			}

			return jsonArray.build();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.SEARCHFILTER, uid));
		}
	}

	@GET
	@Path("/nucletstyles")
	@SessionValidation(enabled = false)
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Nuclet Extension Styles",
							content = @Content(
									schema = @Schema(
											implementation = NucletExtensionsStyles.class
									)
							)
					)
			},
			tags = "Nuclos System"
	)
	public NucletExtensionsStyles nucletstyles() {
		HashSet<String> nucletStyles = new HashSet<>();
		SortedSet<java.nio.file.Path> extensionPaths = NuclosSystemParameters.getNucletClientExtensionPaths();
		for (java.nio.file.Path path : extensionPaths) {
			File directory = path.toFile();
			FilenameFilter filter = (dir, name) -> name != null && name.toLowerCase().endsWith(".css");
			for (File cssFile : directory.listFiles(filter)) {
				try {
					String cssFileContent = Files.readAllLines(cssFile.toPath()).stream().collect(Collectors.joining("\n"));
					nucletStyles.add(cssFileContent);
				} catch (IOException e) {
					throw new NuclosWebException(e);
				}
			}
		}
		return NucletExtensionsStyles.builder().withCss(nucletStyles).build();
	}

	@GET
	@Path("/systemparameters")
	@SessionValidation(enabled = false)
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Systemparameters",
							content = @Content(
									schema = @Schema(
											implementation = RestSystemparameters.class
									)
							)
					)
			},
			tags = "Nuclos System"
	)
	public RestSystemparameters systemparameters() {
		return RestSystemparameters.builder()
				.withENVIRONMENTDEVELOPMENT(NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT))
				.withFULLTEXTSEARCHENABLED(!"off".equalsIgnoreCase(NuclosSystemParameters.getString(NuclosSystemParameters.INDEX_PATH)))
				.withANONYMOUSUSERACCESSENABLED(ServerParameterProvider.getInstance().isEnabled(ParameterProvider.KEY_ANONYMOUS_USER_ACCESS_ENABLED))
				.withUSERREGISTRATIONENABLED(StringUtils.isNotEmpty(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_ROLE_FOR_SELF_REGISTERED_USERS)))
				.withWEBCLIENTCSS(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_WEBCLIENT_CSS))
				.withFORGOTLOGINDETAILSENABLED(isForgotLoginDetailsEnabled())
				.withQUICKSEARCHDELAYTIME(ServerParameterProvider.getInstance().getIntValue(ParameterProvider.QUICKSEARCH_DELAY_TIME, 400))
				.withFUNCTIONBLOCKDEV(ApplicationProperties.getInstance().isFunctionBlockDev())
				.withSORTCALCULATEDATTRIBUTES(ServerParameterProvider.getInstance().isEnabled(ParameterProvider.SORT_CALCULATED_ATTRIBUTES))
				.withCONFIRMREPLACINGDOCUMENTS(ServerParameterProvider.getInstance().isEnabled(ParameterProvider.CONFIRM_REPLACING_DOCUMENTS))
				.withSECURITYPASSWORDSTRENGTHDESCRIPTION(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_SECURITY_PASSWORD_STRENGTH_DESCRIPTION))
				.withDEFAULTDASHBOARDTASKLISTOPENNEWTAB(ServerParameterProvider.getInstance().getValue(ParameterProvider.DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB))
				.withADDSTARTOMANDATORYFIELDLABEL(ServerParameterProvider.getInstance().getValue(ParameterProvider.ADD_STAR_TO_MANDATORY_FIELD_LABEL))
				.withSECURITYPASSWORDBASEDAUTHENTICATIONALLOWED(ServerParameterProvider.getInstance().isEnabled(ParameterProvider.KEY_SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED, true))
				.withROOTPANEBACKGROUNDCOLOR(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_ROOTPANE_BACKGROUND_COLOR))
				.withROOTPANESUBACKGROUNDCOLOR(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_ROOTPANE_SU_BACKGROUND_COLOR))
				.withUSERNAMEDESCRIPTION(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_USERNAME_DESCRIPTION))
				.withAPPNAME(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_APP_NAME))
				.withWEBCLIENTTITLETEMPLATE(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_WEBCLIENT_TITLE_TEMPLATE))
				.build();
	}

	/**
	 * Determines if the "forgot login details" feature is currently usable.
	 */
	private boolean isForgotLoginDetailsEnabled() {
		return mailServiceProvider.isConfiguredForSending()
			&& StringUtils.isNotEmpty(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_RESET_PW_EMAIL_MESSAGE))
			&& StringUtils.isNotEmpty(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_USERNAME_EMAIL_MESSAGE));
	}

	@PUT
	@Path("/systemparameters")
	@Transactional(rollbackFor = {Throwable.class})
	@Operation(
			requestBody = @RequestBody(
					content = @Content(
							mediaType = "application/json",
							schema = @Schema(
									implementation = RestSystemparameters.class
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "If everything was ok"
					)
			},
			tags = "Nuclos System"
	)
	public Response saveSystemparameters(Map<String, Object> parameters) {
		checkSuperUser();

		Map<String, String> stringParameters = new HashMap<>();
		for (String key : parameters.keySet()) {
			Object value = parameters.get(key);
			stringParameters.put(key, value != null ? value.toString() : null);
		}
		ServerParameterProvider.getInstance().insertOrUpdateSystemParameters(stringParameters, getUser());

		return Response.noContent().build();
	}

	@Deprecated
	@GET
	@Path("/statusicon/{uid}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Operation(
			tags = "Nuclos Meta Information"
	)
	/**
	 * Deprecated: Use org.nuclos.server.rest.services.ResourceService#stateIcon(java.lang.String)
	 */
	public Response statusicon(@PathParam("uid") String uid) {
		return ResourceService.getStateIcon(uid, stateCache);
	}

	@GET
	@Path("/icon/{resourcename}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Operation(
			operationId = "resourceicon",
			tags = "Nuclos Meta Information"
	)
	public Response icon(
			@Context final Request request,
			@PathParam("resourcename") String resourcename
	) {
		try {
			ResourceResolver rr = new ResourceResolver();
			Resource resource = rr.resolveEntityIcon(resourcename);
			if (resource == null) {
				resource = rr.resolveHiddenEntityIcon(resourcename);
			}

			if (resource == null) {
				throw new NuclosWebException(Response.Status.NOT_FOUND, String.format("Resource (%s) not found", resourcename));
			}

			final Resource finalResource = resource;

			return new CacheableResponseBuilder(request) {
				@Override
				protected Date getLastModified() throws IOException {
					try {
						return new Date(finalResource.lastModified());
					} catch (FileNotFoundException e) {
						// e.g. Unable to get icon with resourcename: org.nuclos.client.resource.icon.main-blue.earth2_a.png
						//java.io.FileNotFoundException: class path resource [org/nuclos/client/resource/icon/main-blue/earth2_a.png] cannot be resolved to URL because it does not exist
						return CLASS_LOADING_TIME;
					}
				}

				@Override
				protected Response.ResponseBuilder newResponseBuilder() throws IOException {
					byte[] imageBytes = IOUtils.toByteArray(finalResource.getInputStream());

					final Response.ResponseBuilder builder = Response.ok(imageBytes).type("image/gif");
					CacheControl cc = new CacheControl();
					cc.setMaxAge(RESOURCE_ICON_MAX_AGE_IN_SECDONS);
					builder.cacheControl(cc);
					return builder;
					// return Response.ok(imageBytes)
					//		.type("image/gif");
				}
			}.build();
		} catch (Exception e) {
			LOG.warn("Unable to get icon with resourcename: {}", resourcename, e);
			// throw new NuclosWebException(Response.Status.NOT_FOUND, String.format("Resource (%s) not found", resourcename));
			return Response.ok(Rest.EMPTY_IMAGE).type("image/gif").build();
		}
	}

	private DataServiceHelper getDataServiceHelper() {
		if (dataServiceHelper == null) {
			dataServiceHelper = new DataServiceHelper();
		}
		return dataServiceHelper;
	}

	@GET
	@Path("/addonusages")
	@SessionValidation(enabled = false)
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			description = "List WebAddons sorted after zones where they are used",
			summary = "List WebAddons",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = WebAddonUsagesRVO.class
									)
							)
					)
			},
			tags = "Nuclos System"
	)
	public WebAddonUsagesRVO addonUsages() {
		return new WebAddonUsagesRVO(true);
	}

}
