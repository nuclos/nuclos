package org.nuclos.server.rest.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.customcomp.resplan.ResPlanConfigVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.interval.Interval;
import org.nuclos.schema.rest.PlanElementDef;
import org.nuclos.schema.rest.PlanElementDefList;
import org.nuclos.schema.rest.PlanningTable;
import org.nuclos.server.customcomp.resplan.PlanningTableFacadeLocal;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.rvo.FilterJ;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/planningTables")
@Produces(MediaType.APPLICATION_JSON)
public class PlanningTableService extends DataServiceHelper {

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	private PlanningTableFacadeLocal planningTableFacade;

	private static final Logger LOG = LoggerFactory.getLogger(PlanningTableService.class);

	@GET
	@Path("/{planningTableId}")
	@Operation(
			operationId = "planningTable",
			description = "Get a single planning table.",
			summary = "Get a single planning table.",
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = PlanningTable.class
							)
					)
			),
			tags = "Nuclos Planning Tables"
	)
	public PlanningTable planningTable(@PathParam("planningTableId") String planningTableId,
									   @QueryParam("from") String eventsFromDate,
									   @QueryParam("until") String eventsUntilDate,
									   @QueryParam("showOwnEventsOnly") String showOwnEventsOnly,
									   @QueryParam("layoutEoId") String layoutId,
									   @QueryParam("resourceForeignKey") String resourceForeignKey,
									   @Parameter(
											   name = "where",
											   in = ParameterIn.QUERY,
											   required = true,
											   description = "Query for searching objects",
											   example = "example_rest_Article_price=30&withTitleAndInfo=false&attributes=articleNumber,name,price,active&sort=example_rest_Article_articleNumber+desc&skipStatesAndGenerations=true&offset=0&chunkSize=100&countTotal=true&searchFilterId=&taskListId="
									   )
									   @QueryParam("where")
									   String where) throws CommonBusinessException {
		final Interval<Date> interval;
		final Date dateFrom;
		final Date dateUntil;
		final CollectableSearchCondition searchFilterCondition;
		final boolean ownEventsOnly;

		if(showOwnEventsOnly == null) {
			ownEventsOnly = false;
		} else {
			ownEventsOnly = (showOwnEventsOnly.equals("") || Boolean.TRUE.equals(Boolean.valueOf(showOwnEventsOnly)));
		}

		if(!StringUtils.isNullOrEmpty(where)) {
			JsonObjectBuilder queryContext = Optional.ofNullable(where)
					.map(this::getQueryContextFromParam)
					.orElse(Json.createObjectBuilder());

			FilterJ filterJ = getFilterJ(queryContext.build(), -1l);

			ResPlanConfigVO test = this.planningTableFacade.getResPlanConfig(new UID(planningTableId));
			setBOMeta(metaProvider.getEntity(test.getResourceEntity()));

			final List<FieldMeta<?>> fields = Rest.getFieldsFromQueryAttributesAndSystem(test.getResourceEntity(), filterJ.getAttributes());
			final CollectableSearchExpression clctexpr = filterJ.getSearchExpression(test.getResourceEntity(), fields);
			searchFilterCondition = clctexpr.getSearchCondition();
		} else {
			searchFilterCondition = null;
		}

		try {
			Calendar c = Calendar.getInstance();
			c.setTime(DATE_FORMAT.parse(eventsUntilDate));
			c.set(Calendar.HOUR_OF_DAY, 23);
			c.set(Calendar.MINUTE, 59);
			c.set(Calendar.SECOND, 59);
			c.set(Calendar.MILLISECOND, 999);

			dateFrom = DATE_FORMAT.parse(eventsFromDate);
			dateUntil = c.getTime();
			interval = new Interval<>(dateFrom, dateUntil);
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			throw new NuclosWebException(Response.Status.BAD_REQUEST);
		}

		try {
			return Rest.getPlanningTable(new UID(planningTableId), interval, ownEventsOnly, searchFilterCondition, layoutId, resourceForeignKey);
		} catch(CommonPermissionException cpe) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Response.Status.NOT_FOUND);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NuclosWebException(e, E.CUSTOMCOMPONENT.getUID());
		}
	}

	@GET
	@Path("/{planningTableId}/plan-elements")
	@Operation(
			operationId = "planningTable",
			description = "Get all plan element definitions of a planning table.",
			summary = "Get all plan element definitions of a planning table.",
			responses = @ApiResponse(
					content = @Content(
							array = @ArraySchema(
								schema = @Schema(
										implementation = PlanElementDef.class
								)
							)
					)
			),
			tags = "Nuclos Planning Tables"
	)
	public PlanElementDefList planElementDefList(@PathParam("planningTableId") String planningTableId) throws CommonBusinessException {
		return planningTableFacade.getAllPlanElementDefs(new UID(planningTableId));
	}

	@GET
	@Path("/{planningTableId}/plan-elements/{planElementId}")
	@Operation(
			operationId = "planningTable",
			description = "Get a single plan element definition of a planning table.",
			summary = "Get a single plan element definition of a planning table.",
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = PlanElementDef.class
							)
					)
			),
			tags = "Nuclos Planning Tables"
	)
	public PlanElementDef planElement(@PathParam("planningTableId") String planningTableId, @PathParam("planElementId") String planElementId) throws CommonBusinessException {
		return planningTableFacade.getPlanElementDef(new UID(planningTableId), new UID(planElementId));
	}

	@PUT
	@Path("/{planningTableId}/events/{boMetaId}/{eventId}")
	@Operation(
			operationId = "update",
			description = "Updates a single event entry (booking/milestone) of the planning table.",
			summary = "Update a single planning table event entry.",
			responses = {
					@ApiResponse(
							responseCode = "204"
					),
					@ApiResponse(
							responseCode = "403",
							description = "Missing permission to access this information"
					),
			},
			tags = "Nuclos Planning Tables"
	)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateEvent(@PathParam("planningTableId") String planningTableId,
								  @PathParam("boMetaId") String metaId,
								  @PathParam("eventId") String eventId,
								  @QueryParam("resource") String resource,
								  @QueryParam("fromDate") String fromDate,
								  @QueryParam("untilDate") String untilDate,
								  @QueryParam("fromTime") String fromTime,
								  @QueryParam("untilTime") String untilTime) throws CommonRemoveException, CommonFinderException, CommonCreateException, CommonPermissionException, CommonValidationException, CommonStaleVersionException, NuclosBusinessRuleException {

		UID planningTableUID = new UID(planningTableId);
		UID metaUID = new UID(metaId);
		Date dFrom = null;
		Date dUntil = null;
		if (!StringUtils.isNullOrEmpty(fromDate)) {
			try {
				dFrom = DATE_FORMAT.parse(fromDate);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
		}
		if (!StringUtils.isNullOrEmpty(untilDate)) {
			try {
				dUntil = DATE_FORMAT.parse(untilDate);
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
		}

		if (Rest.hasAccessPermission(planningTableUID)) {
			planningTableFacade.updateEvent(planningTableUID, metaUID, eventId, resource, dFrom, dUntil, fromTime, untilTime);
		} else {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		return Response.status(Response.Status.NO_CONTENT).build();
	}

	private JsonObjectBuilder getQueryContextFromParam(final String where) {
		JsonObjectBuilder queryContext = Json.createObjectBuilder();
		if (where == null) return queryContext;

		if (where.contains("&")) {
			Arrays.stream(where.split("&"))
					.forEach((attribute) -> parseAttributeFromQuery(queryContext, attribute));
		} else {
			// maybe only one attribute defintion
			this.parseAttributeFromQuery(queryContext, where);
		}
		return queryContext;
	}

	private void parseAttributeFromQuery(final JsonObjectBuilder queryContext, final String attribute) {
		if (attribute.contains("=")) {
			int indexOfEqual = attribute.indexOf("=");
			final String key = attribute.substring(0, indexOfEqual);
			final String value = indexOfEqual == attribute.length() ? "" :
					attribute.substring(indexOfEqual + 1);
			// only inspect keys with values
			if (value.equals("true") || value.equals("false")) {
				queryContext.add(key, Boolean.parseBoolean(value));
			} else if (value.contains(",")) {
				JsonArrayBuilder array = Json.createArrayBuilder();
				Arrays.stream(value.split(",")).forEach(array::add);
				queryContext.add(key, array);
			} else if (value.matches("[0-9]+")) {
				// Long is the last known type we want
				queryContext.add(key, Long.parseLong(value));
			} else {
				queryContext.add(key, value);
			}
		}
	}

}
