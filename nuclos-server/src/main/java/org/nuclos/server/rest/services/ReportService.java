//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.net.URLConnection;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.ObjectUtils;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.rest.PrintoutList;
import org.nuclos.schema.rest.PrintoutOutputFormat;
import org.nuclos.schema.rest.PrintoutOutputFormatParameter;
import org.nuclos.schema.rest.RestReport;
import org.nuclos.server.report.ejb3.DatasourceFacadeLocal;
import org.nuclos.server.report.ejb3.ReportFacadeLocal;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache;
import org.nuclos.server.rest.services.helper.WebContext;
import org.nuclos.server.rest.services.rvo.OutputFormatRVO;
import org.nuclos.server.rest.services.rvo.PrintoutRVO;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/reports")
@Produces(MediaType.APPLICATION_JSON)
public class ReportService extends WebContext {

	private static final Logger LOG = LoggerFactory.getLogger(ReportService.class);

	@Autowired
	private ReportFacadeLocal reportsFacade;

	@Autowired
	private DatasourceFacadeLocal datasourceFacade;

	@Autowired
	private RestPrintoutFileDownloadCache downloadCache;

	@GET
	@Operation(
			operationId = "reports",
			description = "List of reports for the current user.",
			summary = "List of reports for the current user.",
			responses = @ApiResponse(
					content = @Content(
							array = @ArraySchema(
									schema = @Schema(
											implementation = RestReport.class
									)
							)
					)
			),
			tags = "Nuclos Reports"
	)
	public JsonArray reports() {
		try {
			Collection<DefaultReportVO> reports = reportsFacade.getReports();
			JsonArrayBuilder builder = Json.createArrayBuilder();
			for (DefaultReportVO report : reports) {
				builder.add(buildOutgoing(report));
			}
			return builder.build();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NuclosWebException(e, E.REPORT.getUID());
		}
	}

	@GET
	@Path("/{reportId}")
	@Operation(
			operationId = "report",
			description = "Get a single report.",
			summary = "Get a single report.",
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = RestReport.class
							)
					)
			),
			tags = "Nuclos Reports"
	)
	public JsonObject report(@PathParam("reportId") String reportId) {
		try {
			DefaultReportVO report = getReportVO(reportId);
			try {
				return buildOutgoing(report);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw new NuclosWebException(e, E.REPORT.getUID());
			}
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}
	}

	@GET
	@Path("/{reportId}/printout")
	@Operation(
			operationId = "report_printout",
			description = "Get a all output formats of a report as a \"printout\".",
			summary = "Get a all output formats of a report as a \"printout\".",
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = PrintoutList.class
							)
					)
			),
			tags = "Nuclos Reports"
	)
	public JsonObject reportPrintout(@PathParam("reportId") String reportId) {
		try {
			DefaultReportVO reportVO = getReportVO(reportId);
			PrintoutRVO printout = new PrintoutRVO(null, reportVO);

			if (ReportVO.OutputType.COLLECTIVE.equals(reportVO.getOutputType()) || ReportVO.OutputType.SINGLE.equals(reportVO.getOutputType())) {
				for (ReportOutputVO reportoutputvo : reportsFacade.getReportOutputs(reportVO.getId())) {
					String translatedUid = Rest.translateUid(E.REPORTOUTPUT, (UID) reportoutputvo.getId());
					if (translatedUid != null) {
						// not all output-formats are runnable in server.
						// see also org.nuclos.server.report.PrintoutObjectBuilder.visitOutputFormats(UID, OutputFormatVisitor)
						OutputFormatRVO outputformat = new OutputFormatRVO(reportoutputvo, ObjectUtils.defaultIfNull(reportoutputvo.getDatasourceUID(), reportVO.getDatasourceId()));
						printout.addOutputFormat(outputformat);
					}
					if (reportVO.getOutputType() == ReportVO.OutputType.EXCEL) {
						break;
					}
				}
				return printout.getJSONObjectBuilder().build();
			}
			throw new NuclosWebException(Status.BAD_REQUEST, "Cannot execute collective Excel reports.");
		} catch (Exception ex) {
			if (ex instanceof NuclosWebException) {
				throw (NuclosWebException) ex;
			}
			throw new NuclosWebException(ex);
		}
	}

	@POST
	@Path("/{reportId}/execute")
	@Operation(
			operationId = "report_execution",
			description = "Execute the report with the given outputformats, returns the generated file list.",
			summary = "Execute the report with the given outputformats, returns the generated file list.",
			responses = @ApiResponse(
					content = @Content(
							schema = @Schema(
									implementation = PrintoutList.class
							)
					)
			),
			tags = "Nuclos Reports"
	)
	public JsonObject reportExecution(@PathParam("reportId") String reportId, PrintoutList printoutListData) {
		try {
			final DefaultReportVO reportVO = getReportVO(reportId);
			PrintoutRVO result = new PrintoutRVO(null, reportVO);
			for (PrintoutOutputFormat outputFormat : printoutListData.getOutputFormats()) {
				NuclosFile file = reportsFacade.prepareReport(Rest.translateFqn(E.REPORTOUTPUT, outputFormat.getOutputFormatId()), getParameterMap(outputFormat.getParameters()), null, new UID(this.getLocale().toString()), this.getMandatorUID());
				OutputFormatRVO outputFormatResult = OutputFormatRVO.getOutputFormat(outputFormat, reportVO, reportsFacade);
				result.addOutputFormat(outputFormatResult);
				String fileId = downloadCache.cacheFile(null, reportId, outputFormatResult.getId(), file);
				OutputFormatRVO.OutputFile outputFileResult = new OutputFormatRVO.OutputFile(null, reportId, file.getName(), fileId, ReportService.this);
				outputFormatResult.setFile(outputFileResult);
			}
			return result.getJSONObjectBuilder().build();
		} catch (Exception ex) {
			throw new NuclosWebException(ex, null);
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{reportId}/{outputFormatId}/files/{fileId}")
	@Operation(
			operationId = "reportFile",
			description = "Get the file",
			summary = "Get the file",
			tags = "Nuclos Reports"
	)
	public Response reportFile(@PathParam("reportId") String reportId, @PathParam("outputFormatId") String outputFormatId, @PathParam("fileId") String fileId) {
		try {
			reportsFacade.checkReportReadable(UID.parseUID(reportId));

			RestPrintoutFileDownloadCache.CacheResult cached = downloadCache.get(null, reportId, outputFormatId, fileId);

			if (cached != null) {
				Response.ResponseBuilder responseBuilder = Response.ok(cached.content).type(URLConnection.guessContentTypeFromName(cached.fileName));
				responseBuilder.header("Content-Disposition", "attachment; filename=\"" + Encode.forUriComponent(cached.fileName) + "\"");
				return responseBuilder.build();
			}

			return Response.noContent().build();
		} catch (Exception ex) {
			throw new NuclosWebException(ex);
		}
	}

	private Map<String, Object> getParameterMap(final List<PrintoutOutputFormatParameter> parameters) {
		final Map<String, Object> mpParams = new HashMap<>();
		for (PrintoutOutputFormatParameter parameter : parameters) {
			mpParams.put(parameter.getParameter(), parameter.getValue());
		}
		return mpParams;
	}

	private DefaultReportVO getReportVO(final String reportId) throws CommonPermissionException, CommonFinderException {
		UID reportUid = UID.parseUID(reportId);
		Optional<DefaultReportVO> optReport =
				reportsFacade.getReports().stream().filter(report -> reportUid.equals(report.getId())).findFirst();
		return optReport.orElseThrow(CommonFinderException::new);
	}

	/**
	 * Builds a JSON object for the given Pref.
	 * <p>
	 * TODO: Use Jackson mapping instead
	 * TODO: Javadoc!
	 */
	private JsonObject buildOutgoing(DefaultReportVO report) {
		JsonObjectBuilder builder = Json.createObjectBuilder();

		builder.add("reportId", report.getId().getString());

		if (report.getName() != null) {
			builder.add("name", report.getName());
		}
		if (report.getDescription() != null) {
			builder.add("description", report.getDescription());
		}
		if (report.getOutputType() != null) {
			builder.add("reportType", report.getOutputType().getValue());
		}
		if (report.getDatasourceId() != null) {
			builder.add("datasource", datasourceFacade.getDatasource(report.getDatasourceId()).getName());
		}

		return builder.build();
	}
}
