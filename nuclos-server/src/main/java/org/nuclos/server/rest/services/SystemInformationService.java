package org.nuclos.server.rest.services;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.Version;
import org.nuclos.remoting.TypePreservingTypeIdResolver;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.services.helper.SessionValidation;
import org.nuclos.server.rest.services.helper.WebContext;
import org.nuclos.server.rest.services.rvo.NucletInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/")
public class SystemInformationService extends WebContext {
	private static final Logger LOG = LoggerFactory.getLogger(SystemInformationService.class);

	@GET
	@Path("/systeminformation")
	@SessionValidation(enabled = false)
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			operationId = "nuclos",
			description = "Returns information about nuclos version.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = SystemInformationResponse.class
									)
							)
					)
			},
			summary = "Returns information about nuclos version.", tags = "Nuclos System")
	public SystemInformationResponse nuclets() {
		SystemInformationResponse infoMap = new SystemInformationResponse();

		boolean anonymous = "anonymousUser".equals(Rest.facade().getCurrentUserName());

		infoMap.setNuclosVersion(
				getVersion(anonymous)
		);
		infoMap.setNuclets(
				getNuclets(anonymous)
		);

		return infoMap;
	}

	@GET
	@Path("/remotingclasslists")
	@SessionValidation(enabled = false)
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			operationId = "nuclos",
			description = "Returns the lists of remoting classes.",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = TypePreservingTypeIdResolver.RemotingClassLists.class
									)
							)
					)
			},
			summary = "Returns the lists of remoting classes for communication with the rich client.", tags = "Nuclos System")
	public TypePreservingTypeIdResolver.RemotingClassLists remotingClassLists() {
		return TypePreservingTypeIdResolver.getRemotingClassLists();
	}

	private List<NucletInfo> getNuclets(boolean anonymous) {
		final Stream<NucletInfo> nucletInfoStream = Rest
				.facade()
				.getNucletInfos()
				.stream();

		if (anonymous) {
			return nucletInfoStream
					.filter(NucletInfo::isMasterNuclet)
					.collect(Collectors.toList());
		}

		return nucletInfoStream
				.collect(Collectors.toList());
	}

	private Version getVersion(boolean anonymous) {
		Version version =
				ApplicationProperties
						.getInstance()
						.getNuclosVersion();

		if (anonymous) {
			version = new Version(
					"", "", version.getVersionNumber(), "1970-01-01 00:00:00"
			);
		}

		return version;
	}

	public static class SystemInformationResponse implements Serializable {
		Version nuclosVersion;
		List<NucletInfo> nuclets;

		public Version getNuclosVersion() {
			return nuclosVersion;
		}

		public void setNuclosVersion(final Version nuclosVersion) {
			this.nuclosVersion = nuclosVersion;
		}

		public List<NucletInfo> getNuclets() {
			return nuclets;
		}

		public void setNuclets(final List<NucletInfo> nuclets) {
			this.nuclets = nuclets;
		}
	}
}
