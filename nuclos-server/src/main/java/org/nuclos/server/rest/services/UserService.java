//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.mail.NuclosMail;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.mail.NuclosMailServiceProvider;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.SessionValidation;
import org.nuclos.server.rest.services.helper.UserManagementHelper;
import org.nuclos.server.rest.services.rvo.PasswordChangeRVO;
import org.nuclos.server.rest.services.rvo.PasswordResetRVO;
import org.nuclos.server.rest.services.rvo.RoleRVO;
import org.nuclos.server.rest.services.rvo.UserRVO;
import org.nuclos.server.security.UserFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService extends UserManagementHelper {

	@Autowired
	private UserFacadeLocal userFacade;

	@Autowired
	ServerParameterProvider parameterProvider;

	@Autowired
	private NuclosMailServiceProvider mailServiceProvider;

	@POST
	@Operation(
			operationId = "createUser",
			description = "User creation",
			summary = "User creation",
			requestBody = @RequestBody(
					required = true,
					content = @Content(
							schema = @Schema(
									implementation = UserRVO.class,
									description = "User to create"
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = UserRVO.class
									)
							)
					)
			},
			tags = "Nuclos User"
	)
	public UserRVO createUser(UserRVO userRVO) throws CommonBusinessException {
		checkSuperUser();
		createUserImpl(userRVO);
		return userRVO;
	}

	//DATEN ANGEBEN
	//-->NOCH EIN LETZER SCHRITT ZUR ANMELDUNG. WIR HABEN IHNEN EINE EMAIL GESCHICKT. BITTE AKTIVIEREN SIE IHREN ACCOUNT.
	@POST
	@SessionValidation(enabled = false)
	@Path("/register")
	@Operation(
			description = "Create user account",
			requestBody = @RequestBody(
					required = true,
					content = @Content(
							schema = @Schema(
									implementation = UserRVO.class,
									description = "Account to create"
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "If everything went ok"
					),
					@ApiResponse(
							responseCode = "400",
							description = "If request could not be fulfilled"
					)
			},
			tags = "Nuclos User"
	)
	public Response createAccount(UserRVO userRVO) throws CommonBusinessException {
		if (!mailServiceProvider.isConfiguredForSending()) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.email.server.not.available");
		}
		if (StringUtils.isNullOrEmpty(parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_SUBJECT))) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.email.server.not.available");
		}
		if (StringUtils.isNullOrEmpty(parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_MESSAGE))) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.email.server.not.available");
		}
		checkUserRVOIntegrety(userRVO);

		UserRVO user = new UserRVO();
		user.setEmail(userRVO.getEmail());
		user.setUsername(userRVO.getUsername());
		user.setFirstname(userRVO.getFirstname());
		user.setLastname(userRVO.getLastname());
		user.setNewPassword(userRVO.getNewPassword());

		// TODO: Activation code should expire after some time
		user.setActivationCode(generateToken());

		user.setLocked(true);
		user.setSuperuser(false);
		user.setPrivacyConsentAcceptedt(userRVO.isPrivacyConsentAccepted());

		UserVO userVO = user.toUserVO();
		String rolenamesParameter = parameterProvider.getValue(ParameterProvider.KEY_ROLE_FOR_SELF_REGISTERED_USERS);

		// Webclient-specific i18n codes are used here!
		// TODO: Define these centrally, via nuclos-common or maybe a new module
		if (StringUtils.isNullOrEmpty(rolenamesParameter)) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.self.registration.is.not.active");
		}
		String[] rolenames;
		if (rolenamesParameter.contains(",")) {
			rolenames = rolenamesParameter.split(",");
		} else {
			rolenames = new String[]{rolenamesParameter};
		}

		// Webclient-specific i18n codes are used here!
		// TODO: Define these centrally, via nuclos-common or maybe a new module
		if (userFacade.getByUserName(userRVO.getUsername()) != null) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.username.already.in.use");
		}
		// NUCLOS-8299 also throw NuclosWebException in the catch-Block to convert the IllegalArgumentsException into a sensible error message
		try {
			boolean emailAlreadyInUse = userFacade.getByUserEmail(userRVO.getEmail()) != null;
			if (emailAlreadyInUse) {
				throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.email.already.in.use");
			}
		} catch (IllegalArgumentException e) {
			// this exception means, the email address is already in use for more than 1 account
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.email.already.in.use");
		}


		// CREATE USER
		IDependentDataMap mpDependants = new DependentDataMap();
		for (String rolename : rolenames) {
			rolename = rolename.trim();
			EntityObjectVO<UID> eoRoleUser = new EntityObjectVO<>(E.ROLEUSER);
			MasterDataVO<UID> roleVO = userFacade.getRoleByName(rolename);
			if (roleVO == null) {
				throw new NuclosWebException(Response.Status.BAD_REQUEST, "Rolename '" + rolename + "' not exist.");
			}
			eoRoleUser.setFieldUid(E.ROLEUSER.role, roleVO.getPrimaryKey());
			mpDependants.addData(E.ROLEUSER.user, eoRoleUser);
		}

		UserVO mdUserVO = null;
		try {
			mdUserVO = userFacade.create(userVO, mpDependants);
		} catch (CommonValidationException e) {
			if (mdUserVO != null) {
				try {
					userFacade.remove(mdUserVO);
				} catch (CommonBusinessException ignored) {
				} // does not need any exception handling here
			}
			throw new NuclosWebException(Response.Status.BAD_REQUEST, e.getMessage());
		} catch(CommonBusinessException e) {
			//NUCLOS-9532 like mentioned above it would be better, if one exception message were defined for all clients.
			if(e.getMessage().equals("exception.user.invalidname")) {
				throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.username.invalidcharacters");
			}
			throw e;
		}

		// SEND ACTIVATION LINK
		String subject = parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_SUBJECT);
		String message = parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_MESSAGE);

		String encodedUsername = StringUtils.encodeForUrl(user.getUsername(), Collections.emptySet());

		NuclosMail mail = new NuclosMail();
		mail.addRecipient(user.getEmail());
		mail.setSubject(subject);
		message = MessageFormat.format(
				message,
				user.getFirstname(),
				user.getLastname(),
				encodedUsername,
				user.getActivationCode()
		);
		mail.setMessage(message);

		checkContentType(mail);

		try {
			mailServiceProvider.send(mail);
			return Response.noContent().build();
		} catch (BusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	@GET
	@Path("/roles")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									array = @ArraySchema(
											schema = @Schema(
													implementation = RoleRVO.class
											)
									)
							)
					)
			},
			tags = "Nuclos User"
	)
	public Set<RoleRVO> getRoles() {
		final Set<RoleRVO> result = new HashSet<>();

		final String username = getUser();
		Map<UID, String> allRolesForUser = userFacade.getAllRolesForUser(username);
		for (Map.Entry<UID, String> entry : allRolesForUser.entrySet()) {
			final RoleRVO roleRVO = new RoleRVO(
					entry.getKey(),
					entry.getValue()
			);
			result.add(roleRVO);
		}

		return result;
	}

	//FORMULAR BITTE EMAIL-ADRESSE ANGEBEN
	//--> IHNEN WURDE PER EMAIL DER USERNAME ZUGESENDET. SOLLTE SIE KEINE MAIL ERHALTEN HABEN, PRÜFEN SICH BITTE AUCH IHREN SPAM-ORDNER
	@POST
	@Path("/forgotUsername/{email}")
	@SessionValidation(enabled = false)
	@Operation(
			description = "Get email to reset password",
			summary = "Password Reset via E-Mail",
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "If user was found and mail was send"
					),
					@ApiResponse(
							responseCode = "404",
							description = "User was not found for given E-Mail"
					),
					@ApiResponse(
							responseCode = "403",
							description = "User is not allowed"
					),
					@ApiResponse(
							responseCode = "503",
							description = "E-Mail Service is not configured"
					)
			},
			tags = "Nuclos User"
	)
	public Response forgotUsername(
			@Parameter(
					name = "email",
					description = "E-Mail address from user which should get password reset"
			)
			@PathParam("email") String email
	) {
		try {
			// SEND ACTIVATION LINK
			String subject = parameterProvider.getValue(ParameterProvider.KEY_USERNAME_EMAIL_SUBJECT);
			String message = parameterProvider.getValue(ParameterProvider.KEY_USERNAME_EMAIL_MESSAGE);

			if (subject == null || message == null || !mailServiceProvider.isConfiguredForSending()) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}

			UserVO user = userFacade.getByUserEmail(email);
			if (user == null) {
				throw new NuclosWebException(Response.Status.NOT_FOUND);
			} else if (!Rest.facade().isRestApiAllowed(user.getUsername())) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			} else {
				NuclosMail mail = new NuclosMail();
				mail.addRecipient(user.getEmail());
				mail.setSubject(subject);
				mail.setMessage(
						MessageFormat.format(message, user.getFirstname(), user.getLastname(), user.getUsername())
				);

				checkContentType(mail);
				mailServiceProvider.send(mail);
			}

			return Response.noContent().build();
		} catch (Exception e) {
			throw NuclosWebException.wrap(e);
		}
	}

	@GET
	@Path("/{username}")
	@Operation(
			description = "Get user details",
			summary = "User Details",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											implementation = UserRVO.class
									)
							)
					),
					@ApiResponse(
							responseCode = "404",
							description = "User not found"
					)
			},
			tags = "Nuclos User"
	)
	public UserRVO getUser(
			@Parameter(
					name = "username",
					description = "User to receive"
			)
			@PathParam("username") String username
	) throws CommonBusinessException {
		checkSuperUser();
		UserVO userVO = userFacade.getByUserName(username);
		if (userVO == null) {
			throw new NuclosWebException(Response.Status.NOT_FOUND, "Username '" + username + "' not found.");
		}
		return new UserRVO(userVO);
	}

	//ERFOLGREICH AKTIVERT
	@POST
	@Path("/activate/{activationCode}")
	@SessionValidation(enabled = false)
	@Operation(
			description = "Activate user with code",
			summary = "User Activate",
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "User could be activated"
					),
					@ApiResponse(
							responseCode = "400",
							description = "User could not be found or activate code is wrong"
					)
			},
			tags = "Nuclos User"
	)
	public Response activateAccount(
			@PathParam("activationCode") String code
	) throws CommonBusinessException {
		// VALIDATE
		if (StringUtils.isNullOrEmpty(code))
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.activationcode.invalid");

		// CHECK EXISTING ALREADY
		UserVO user;
		try {
			user = userFacade.getByActivationCode(code);
		} catch (IllegalArgumentException i) {
			// is thrown, if multiple users match this activation code
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.activationcode.invalid");
		}

		if (user == null) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.activationcode.invalid");
		} else {
			if (user.getLocked() != true) {
				throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.already.activated");
			}
			user.setActivationCode(null);
			user.setLocked(false);
			userFacade.modify(user, null, null);
		}

		return Response.noContent().build();
	}

	@PUT
	@Path("/{username}/password")
	@SessionValidation(enabled = false)
	@Operation(
			description = "Update password of user",
			summary = "User password update",
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "Update successful"
					),
					@ApiResponse(
							responseCode = "403",
							description = "Action not allowed"
					),
					@ApiResponse(
							responseCode = "406",
							description = "Password could not be updated"
					)
			},
			tags = "Nuclos User"
	)
	public Response setPassword(
			@PathParam("username") String username,
			final PasswordChangeRVO passwordChange
	) {
		return super.setPassword(username, passwordChange);
	}

	//FORMULAR BITTE USERNAME UND EMAIL ANGEBEN...
	//--> IHNEN WURDE PER EMAIL EIN LINK ZUGESENDET, ÜBER DIESEN LINK KÖNNEN SIE IHR PASSWORT ZURÜCKSETZEN
	@POST
	@Path("/{username}/forgotPassword")
	@SessionValidation(enabled = false)
	@Operation(
			description = "Get email to reset password",
			summary = "Password Reset via username",
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "If user was found and mail was send"
					),
					@ApiResponse(
							responseCode = "404",
							description = "User was not found for given name"
					),
					@ApiResponse(
							responseCode = "403",
							description = "User is not allowed"
					),
					@ApiResponse(
							responseCode = "503",
							description = "E-Mail Service is not configured"
					)
			},
			tags = "Nuclos User"
	)
	public Response forgotPassword(@PathParam("username") String username) {
		try {
			// SEND ACTIVATION LINK
			String subject = parameterProvider.getValue(ParameterProvider.KEY_RESET_PW_EMAIL_SUBJECT);
			String message = parameterProvider.getValue(ParameterProvider.KEY_RESET_PW_EMAIL_MESSAGE);

			if (subject == null || message == null || !mailServiceProvider.isConfiguredForSending()) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}

			UserVO user = userFacade.getByUserName(username);
			if (user == null) {
				throw new NuclosWebException(Response.Status.NOT_FOUND);
			} else if (!Rest.facade().isRestApiAllowed(user.getUsername())) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			} else if (user.getEmail() == null) {
				throw new NuclosWebException(Response.Status.PRECONDITION_FAILED);
			} else {
				if (user.getLocked()) {
					// user needs to unlock his account first before resetting his password
					throw new NuclosWebException(Response.Status.FORBIDDEN, "nuclos.security.authentication.locked");
				}
				// TODO: Activation code should expire after some time
				user.setPasswordResetCode(generateToken());
				userFacade.modify(user, null, null);

				NuclosMail mail = new NuclosMail();
				mail.addRecipient(user.getEmail());
				mail.setSubject(subject);
				mail.setMessage(
						MessageFormat.format(
								message,
								user.getFirstname(),
								user.getLastname(),
								user.getUsername(),
								user.getPasswordResetCode()
						)
				);

				checkContentType(mail);
				mailServiceProvider.send(mail);
			}

			return Response.noContent().build();
		} catch (Exception e) {
			throw NuclosWebException.wrap(e);
		}
	}

	@POST
	@Path("/{username}/passwordReset")
	@SessionValidation(enabled = false)
	@Operation(
			description = "Reset password for given user",
			summary = "User Password Reset",
			requestBody = @RequestBody(
					required = true,
					content = @Content(
							schema = @Schema(
									implementation = PasswordResetRVO.class,
									description = "Password Reset Object"
							)
					)
			),
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "If everything went ok"
					),
					@ApiResponse(
							responseCode = "400",
							description = "If request could not be fulfilled"
					),
					@ApiResponse(
							responseCode = "403",
							description = "Not allowed"
					)
			},
			tags = "Nuclos User"
	)
	public Response resetPassword(
			@PathParam("username") String username,
			final PasswordResetRVO passwordReset
	) {
		if (!Rest.facade().isRestApiAllowed(username)) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		}

		return super.resetPassword(username, passwordReset);
	}

	@GET
	@Path("/{username}/passwordResetCheck/{token}")
	@SessionValidation(enabled = false)
	@Operation(
			description = "Used to check if given reset token is still valid for given user",
			summary = "Test users reset token",
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "Successful response if everything is alright"
					),
					@ApiResponse(
							responseCode = "400",
							description = "If token is expired or user not found"
					),
					@ApiResponse(
							responseCode = "403",
							description = "If given user has no permission to access REST API"
					)
			},
			tags = "Nuclos User"
	)
	public Response checkPasswordResetToken(
			@Parameter(
					required = true,
					name = "username",
					description = "User to test reset token to",
					example = "test",
					schema = @Schema(
							type = "string"
					)
			)
			@NotNull @PathParam("username") String username,
			@Parameter(
					name = "token",
					required = true,
					description = "Token to check for user password reset",
					example = "987231lksajha123",
					schema = @Schema(
							type = "string"
					)
			)
			@NotNull @PathParam("token") String resetToken
	) {

		if (!Rest.facade().isRestApiAllowed(username)) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		}

		try {
			UserVO user = userFacade.getByUserName(username);
			if (!resetToken.equals(user.getPasswordResetCode())) {
				throw new BusinessException("Token does not match");
			}
		} catch (BusinessException | CommonBusinessException e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		return Response.noContent().build();
	}

	@GET
	@Path("/{username}/privacyConsent")
	@Produces(MediaType.TEXT_PLAIN)
	@Operation(
			description = "Get state of privacy consent for user",
			summary = "Privacy Consent",
			responses = {
					@ApiResponse(
							responseCode = "200",
							content = @Content(
									schema = @Schema(
											description = "Boolean indicating if accepted (false | true)",
											example = "false"
									)
							)
					)
			},
			tags = "Nuclos User"
	)
	public Boolean getPrivacyConsent(
			@Parameter(
					name = "username",
					description = "User to get information of"
			)
			@PathParam("username") final String username
	) {
		try {
			checkUser(username);

			UserVO user = userFacade.getByUserName(username);
			return user.isPrivacyConsentAccepted();
		} catch (CommonBusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	@PUT
	@Path("/{username}/privacyConsent")
	@Operation(
			description = "Update privacy consent for user",
			summary = "Privacy Consent Update",
			responses = {
					@ApiResponse(
							responseCode = "204",
							description = "If update was successful"
					)
			},
			tags = "Nuclos User"
	)
	public Response setPrivacyConsent(
			@Parameter(
					name = "username",
					description = "User to update privacy consent"
			)
			@PathParam("username") final String username,
			@Parameter(
					name = "privacyConsent",
					required = true,
					description = "Value of privacy consent",
					schema = @Schema(
							type = "string",
							allowableValues = {
									"false",
									"true"
							}
					)
			) final Boolean privacyConsent
	) {
		try {
			checkUser(username);

			UserVO user = userFacade.getByUserName(username);
			user.setPrivacyConsentAccepted(privacyConsent);
			userFacade.modify(user, null, null);

			return Response.noContent().build();
		} catch (CommonBusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	private void createUserImpl(UserRVO userRVO) throws CommonBusinessException {
		checkUserRVOIntegrety(userRVO);

		UserVO userVO = userRVO.toUserVO();

		IDependentDataMap mpDependants = new DependentDataMap();
		List<String> rolenames = userRVO.getRoles();
		for (String rolename : rolenames) {
			MasterDataVO<UID> roleVO = userFacade.getRoleByName(rolename);
			if (roleVO == null) {
				throw new NuclosWebException(Response.Status.BAD_REQUEST, "Rolename '" + rolename + "' does not exist.");
			}
			EntityObjectVO<UID> eoRoleUser = new EntityObjectVO<>(E.ROLEUSER);
			eoRoleUser.setFieldUid(E.ROLEUSER.role, roleVO.getPrimaryKey());
			mpDependants.addData(E.ROLEUSER.user, eoRoleUser);
		}

		userFacade.create(userVO, mpDependants);
	}

	/**
	 * Determines the content type of the mail ("text/plain" or "text/html") based on the given message and signature.
	 * <p>
	 * TODO: This should be moved to the MailServiceProvider:
	 * The content type of NuclosMail should be NULL unless explicitly set.
	 * The MailServiceProvider should then determine the content type, only if it is NULL.
	 */
	private void checkContentType(final NuclosMail mail) {
		final String signature = parameterProvider.getValue(ParameterProvider.EMAIL_SIGNATURE);
		final boolean isHtml = (mail.getMessage() + signature).matches("(?s).*<[^>]+>.*");

		mail.setContentType(isHtml ? NuclosMail.HTML : NuclosMail.PLAIN);
	}

}

