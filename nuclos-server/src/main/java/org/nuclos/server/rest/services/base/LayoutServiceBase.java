package org.nuclos.server.rest.services.base;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.nuclos.common.E;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common2.JaxbMarshalUnmarshalUtil;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.layout.transformation.ILanguageLocator;
import org.nuclos.layout.transformation.rule.LayoutmlToRuleTransformer;
import org.nuclos.layout.transformation.vlp.LayoutmlToVlpTransformer;
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer;
import org.nuclos.layout.transformation.weblayout.calculated.LayoutmlToWeblayoutCalcTransformer;
import org.nuclos.layout.transformation.weblayout.fixed.LayoutmlToWeblayoutFixedTransformer;
import org.nuclos.layout.transformation.weblayout.responsive.LayoutmlToWeblayoutResponsiveTransformer;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.nuclos.schema.layout.rule.Rules;
import org.nuclos.schema.layout.vlp.ValueListProviders;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.cache.annotation.Cacheable;

/**
 * Provides base methods used by the Layout service.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Configurable
public class LayoutServiceBase {

	private IRigidMetaProvider metaProv;

	private ILanguageLocator languageLocator;

	@Autowired
	private void inject(
			final IMetaProvider metaProv,
			final SpringLocaleDelegate localeDelegate) {
		this.metaProv = metaProv;
		this.languageLocator = new ILanguageLocator() {
			@Override
			public String getLanguageToUse() {
				return SpringLocaleDelegate.getInstance().getLocale().getLanguage();
			}
		};
	}

	protected String getLayoutML(String sLayoutId) {
		try {
			UID layoutUID = Rest.translateFqn(E.LAYOUT, sLayoutId);
			return Rest.facade().getLayoutML(layoutUID);
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.LAYOUT, sLayoutId));
		}
	}

	protected Layoutml getLayoutMLObj(String layoutId) throws JAXBException, XMLStreamException {
		String layoutML = getLayoutML(layoutId);
		return JaxbMarshalUnmarshalUtil.unmarshal(layoutML, Layoutml.class);
	}

	// Caches are invalidated in LayoutFacadeBean.evictCaches() on layout changes
	protected org.nuclos.schema.layout.web.WebLayout getWebLayoutFixed(String layoutId) throws JAXBException, XMLStreamException {
		return getWebLayoutFixedCacheable(layoutId, languageLocator.getLanguageToUse());
	}

	@Cacheable(value="webLayoutFixed", key="{ #p0, #p1 }")
	public org.nuclos.schema.layout.web.WebLayout getWebLayoutFixedCacheable(String layoutId, String language) throws JAXBException, XMLStreamException {
		Layoutml layoutML = getLayoutMLObj(layoutId);
		LayoutmlToWeblayoutTransformer transformer = new LayoutmlToWeblayoutFixedTransformer(
				metaProv,
				languageLocator,
				layoutML
		);
		org.nuclos.schema.layout.web.WebLayout webLayout = transformer.transform();

		return webLayout;
	}

	protected org.nuclos.schema.layout.web.WebLayout getWebLayoutCalculated(String layoutId) throws JAXBException, XMLStreamException {
		return getWebLayoutCalculatedCacheable(layoutId, languageLocator.getLanguageToUse());
	}

	@Cacheable(value="webLayoutCalculated", key="{ #p0, #p1 }")
	public org.nuclos.schema.layout.web.WebLayout getWebLayoutCalculatedCacheable(String layoutId, String language) throws JAXBException, XMLStreamException {
		Layoutml layoutML = getLayoutMLObj(layoutId);
		LayoutmlToWeblayoutTransformer transformer = new LayoutmlToWeblayoutCalcTransformer(
				metaProv,
				languageLocator,
				layoutML
		);
		org.nuclos.schema.layout.web.WebLayout webLayout = transformer.transform();

		return webLayout;
	}

	protected org.nuclos.schema.layout.web.WebLayout getWebLayoutResponsive(String layoutId) throws JAXBException, XMLStreamException {
		return getWebLayoutResponsiveCacheable(layoutId, languageLocator.getLanguageToUse());
	}

	@Cacheable(value="webLayoutResponsive", key="{ #p0, #p1 }")
	public org.nuclos.schema.layout.web.WebLayout getWebLayoutResponsiveCacheable(String layoutId, String language) throws JAXBException, XMLStreamException {
		Layoutml layoutML = getLayoutMLObj(layoutId);
		LayoutmlToWeblayoutTransformer transformer = new LayoutmlToWeblayoutResponsiveTransformer(
				metaProv,
				languageLocator,
				layoutML
		);
		org.nuclos.schema.layout.web.WebLayout webLayout = transformer.transform();

		return webLayout;
	}

	@Cacheable(value="webLayoutRules", key="#p0")
	public Rules getRules(String layoutId) throws JAXBException, XMLStreamException {
		Layoutml layoutML = getLayoutMLObj(layoutId);
		LayoutmlToRuleTransformer transformer = new LayoutmlToRuleTransformer(metaProv, layoutML);
		Rules rules = transformer.transform();

		return rules;
	}

	@Cacheable(value="webLayoutVlps", key="#p0")
	public ValueListProviders getValueListProviders(String layoutId) throws JAXBException, XMLStreamException {
		Layoutml layoutML = getLayoutMLObj(layoutId);
		LayoutmlToVlpTransformer transformer = new LayoutmlToVlpTransformer(metaProv, layoutML);
		ValueListProviders vlps = transformer.transform();

		return vlps;
	}
}
