package org.nuclos.server.rest.services.helper;

import java.net.URI;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.nuclos.common.Mutable;
import org.nuclos.common.startup.NuclosEnviromentConstants;
import org.nuclos.server.cluster.RigidClusterHelper;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.security.WebSessionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Lazy;

@Configurable
public abstract class AbstractSessionIdLocator {

	public static final String SESSION_COOKIE_NAME = "JSESSIONID";

	@Context protected HttpHeaders headers;
	@Context protected UriInfo uriInfo;
	@Context protected HttpServletRequest httpRequest;

	@Lazy
	@Autowired private SecurityCache securityCache;

	private final static ThreadLocal<String> SESSION_ID = new ThreadLocal<>();
	private final static ThreadLocal<Boolean> IS_NEW_RULE_BASED_AUTHENTICATION = new ThreadLocal<>();

	protected final String getSessionId() {
		return getSessionId(null);
	}

	protected final String getSessionId(Mutable<String> debugInfo) {
		String localSessionId = SESSION_ID.get();
		if (localSessionId != null) {
			if (debugInfo != null) {
				debugInfo.setValue("Source[ThreadLocal.SESSION_ID]");
			}
			return localSessionId;
		}
		String result = getSessionId("sessionId", debugInfo);
		if (result != null) {
			// not used by the webclient but by other clients
			return trimInstanceName(result);
		}
		Cookie cookie = headers.getCookies().get(SESSION_COOKIE_NAME);
		if (cookie != null) {
			if (debugInfo != null) {
				debugInfo.setValue(String.format("Source[Cookie.%s]", SESSION_COOKIE_NAME));
			}
			return trimInstanceName(cookie.getValue());
		}
		return null;
	}

	public static void cacheNewRuleBasedAuthentication(final String sessionId) {
		setNewRuleBasedAuthentication(true);
		setSessionId(sessionId);
	}

	protected static void setSessionId(final String sessionId) {
		if (SESSION_ID.get() != null) {
			throw new IllegalStateException("Session id already set");
		}
		SESSION_ID.set(sessionId);
	}

	private static void setNewRuleBasedAuthentication(Boolean ruleBasedAuthenticated) {
		IS_NEW_RULE_BASED_AUTHENTICATION.set(ruleBasedAuthenticated);
	}

	protected boolean isNewRuleBasedAuthentication() {
		return Boolean.TRUE.equals(IS_NEW_RULE_BASED_AUTHENTICATION.get());
	}

	public static void clearRuleBasedAuthenticationCache() {
		IS_NEW_RULE_BASED_AUTHENTICATION.remove();
		SESSION_ID.remove();
	}

	private final String getSessionId(final String key, final Mutable<String> debugInfo) {
		List<String> sids;
		sids = getQueryParameters(key);
		if (sids != null && !sids.isEmpty()) {
			if (debugInfo != null) {
				debugInfo.setValue(String.format("Source[QueryParam.%s]", key));
			}
			return trimInstanceName(sids.get(0));
		}
		sids = headers.getRequestHeader(key);
		if (sids != null && !sids.isEmpty()) {
			if (debugInfo != null) {
				debugInfo.setValue(String.format("Source[RequestHeader.%s]", key));
			}
			return trimInstanceName(sids.get(0));
		}
		return null;
	}

	public static String trimInstanceName(final String sessionId) {
		if (sessionId != null) {
			final int dotIndex = sessionId.indexOf('.');
			if (dotIndex > 0) {
				return sessionId.substring(0, dotIndex);
			}
		}
		return sessionId;
	}

	public Set<String> getQueryParameterKeys() {
		return getQueryParameters().keySet();
	}

	protected MultivaluedMap<String, String> getQueryParameters() {
		return uriInfo.getQueryParameters();
	}

	protected List<String> getQueryParameters(String key) {
		return getQueryParameters().get(key);
	}

	private String restPath;

	/**
	 * TODO: Wrong place for this method - it has nothing to do with "SessionIdLocator"!
	 */
	private String buildRestPath() {
		if (restPath == null) {
			restPath = tryToGetRestPathFromSessionContext();
		}
		if (restPath == null) {
			if (RigidClusterHelper.isClusterEnabled()) {
				restPath = String.format("%s://%s:%d/%s%s",
						RigidClusterHelper.getBalancerProtocol(),
						RigidClusterHelper.getBalancerHostname(),
						RigidClusterHelper.getBalancerPort(),
						RigidClusterHelper.getBalancerContext(),
						httpRequest.getServletPath());
			} else {
				// Generate a protocol-relative URL, because using a specific protocol
				// does not work in situations where the connection is made via different
				// protocols because of a (reverse) proxy.
				// See NUCLOS-7818
				restPath = "//" + httpRequest.getServerName();

				// Add only non-standard ports to the URL
				int serverPort = httpRequest.getServerPort();
				if (serverPort != 80 && serverPort != 443) {
					restPath += ":" + serverPort;
				}

				restPath += httpRequest.getContextPath() + httpRequest.getServletPath();
			}
		}
		return restPath;
	}

	private String tryToGetRestPathFromSessionContext() {
		if (securityCache != null) {
			String sessionId = getSessionId();
			if (sessionId != null) {
				SessionContext sessionContext = securityCache.getSessionContext(sessionId);
				if (sessionContext instanceof WebSessionContext) {
					URI serverUri = ((WebSessionContext) sessionContext).getServerUri();
					if (serverUri != null) {
						return UriBuilder.fromUri(serverUri).path(NuclosEnviromentConstants.REST_ENDING).build().toString();
					}
				}
			}
		}
		return null;
	}

	/**
	 * TODO: Wrong place for this method - it has nothing to do with "SessionIdLocator"!
	 */
	protected final String restURL(String relativeUrl) {
		return buildRestPath() + relativeUrl;
	}

}
