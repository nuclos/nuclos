package org.nuclos.server.rest.services.helper;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ActionsAllowed {
	UserAction[] value();
}
