package org.nuclos.server.rest.services.helper;

import org.nuclos.schema.rest.CollectiveProcessingObjectInfo;

public interface CollectiveProcessingExecutionConsumer<T> {
	void apply(T t, CollectiveProcessingObjectInfo oInfo) throws Throwable;
}
