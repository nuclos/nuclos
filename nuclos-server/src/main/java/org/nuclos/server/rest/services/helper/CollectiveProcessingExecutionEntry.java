package org.nuclos.server.rest.services.helper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.validation.constraints.NotNull;

import org.nuclos.common.UID;
import org.nuclos.schema.rest.CollectiveProcessingObjectInfo;
import org.nuclos.schema.rest.CollectiveProcessingObjectInfoInputrequired;
import org.nuclos.schema.rest.InputRequiredResultMapItem;
import org.nuclos.schema.rest.InputRequiredSpecification;
import org.nuclos.schema.rest.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CollectiveProcessingExecutionEntry {
	private static final Logger LOG = LoggerFactory.getLogger(CollectiveProcessingExecutionEntry.class);

	private final String executionId;

	private final Map<Long, CollectiveProcessingObjectInfo> oInfoStore = new HashMap<>();

	private int totalCount = 0;

	private int countProcessed = 0;

	private String user;

	private long lastUpdate = System.currentTimeMillis();

	private boolean aborted = false;

	private Map<Long, CollectiveProcessingObjectInfoInputrequired> oInfoInputRequiredStore = new HashMap<>();

	private List<InputRequiredResultMapItem> applyForMultiEditStore = new ArrayList<>(1);

	public static class ProgressInfoSinceLastRequest {
		private final long progressInPercent;
		private final List<CollectiveProcessingObjectInfo> objectInfos;
		public ProgressInfoSinceLastRequest(final long progressInPercent, final List<CollectiveProcessingObjectInfo> objectInfos) {
			this.progressInPercent = progressInPercent;
			this.objectInfos = objectInfos;
		}
		public long getProgressInPercent() {
			return progressInPercent;
		}
		public List<CollectiveProcessingObjectInfo> getObjectInfos() {
			return objectInfos;
		}
	}

	protected CollectiveProcessingExecutionEntry(@NotNull final String executionId) {
		if (executionId == null) {
			throw new IllegalArgumentException("executionId must not be null");
		}
		this.executionId = executionId;
	}

	public String getExecutionId() {
		return executionId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(final String user) {
		this.user = user;
	}

	public boolean isAborted() {
        return this.aborted;
    }

    public void abort() {
	    synchronized (executionId) {
	        this.aborted = true;
	        this.oInfoInputRequiredStore.clear();
        }
    }

	public int getTotalCount() {
		return this.totalCount;
	}

	public void setTotalCount(final int totalCount) {
		synchronized (executionId) {
			this.totalCount = totalCount;
		}
	}

	public static CollectiveProcessingObjectInfo newObjectInfo(@NotNull String entityFQN, @NotNull Object id, String name, @NotNull Long infoRowNumber) {
		final ObjectId oId = new ObjectId();
		if (id instanceof UID) {
			oId.setString(((UID) id).getString());
		} else if (id instanceof Long) {
			oId.setLong((Long) id);
		} else {
			throw new IllegalArgumentException("Id class not supported: " + id.getClass().getCanonicalName());
		}
		final CollectiveProcessingObjectInfo oInfo = CollectiveProcessingObjectInfo.builder()
				.withBoMetaId(entityFQN)
				.withId(oId)
				.withInfoRowNumber(infoRowNumber)
				.withName(name)
				.build();
		return oInfo;
	}

	/**
	 * clones the CollectiveProcessingObjectInfo and stores internal for later progress requests
	 * @param oInfo
	 */
	public long storeObjectInfo(@NotNull CollectiveProcessingObjectInfo oInfo) {
		CollectiveProcessingObjectInfo oInfoToStore = (CollectiveProcessingObjectInfo) oInfo.clone();
		synchronized (executionId) {
			lastUpdate = System.currentTimeMillis();
			oInfoStore.put(oInfo.getInfoRowNumber(), oInfoToStore);
			if (!oInfo.isInProgress() && !Boolean.TRUE.equals(oInfo.isInputRequired())) {
				countProcessed++;
			}
		}
		return lastUpdate;
	}

	public CollectiveProcessingObjectInfo retrieveObjectInfo(long row) {
		synchronized (executionId) {
			return oInfoStore.get(row);
		}
	}

	public CollectiveProcessingObjectInfoInputrequired getInputRequired() {
		CollectiveProcessingObjectInfoInputrequired inputRequired = null;
		synchronized (executionId) {
			for (long row : this.oInfoStore.keySet()) {
				if (this.oInfoStore.get(row).isInProgress()) {
					inputRequired = this.getInputRequiredForObjectInfo(this.oInfoStore.get(row));
					break;
				}
			}
			if (inputRequired != null) {
				final CollectiveProcessingObjectInfoInputrequired finalInputRequired = inputRequired;
				boolean alreadyAsked =
						inputRequired.getResult()
								.stream()
								.anyMatch(r -> r.getKey().equals(finalInputRequired.getSpecification().getKey()));

				if (alreadyAsked) {
					inputRequired = null;
				}
			}
		}

		return inputRequired;
	}

	public List<InputRequiredResultMapItem> getInputRequiredResult(@NotNull CollectiveProcessingObjectInfo oInfo) {
		synchronized (executionId) {
			List<InputRequiredResultMapItem> result = new ArrayList<>(this.applyForMultiEditStore);
			CollectiveProcessingObjectInfoInputrequired inputRequired = getInputRequiredForObjectInfo(oInfo);
			if (inputRequired != null) {
				final Set<String> applyForMultiEditKeys = result.stream().map(p -> p.getKey()).collect(Collectors.toSet());
				result.addAll(inputRequired.getResult().stream()
					.filter(p -> !applyForMultiEditKeys.contains(p.getKey()))
					.collect(Collectors.toList()));
			}
			return result;
		}
	}

	public CollectiveProcessingObjectInfoInputrequired
	getInputRequiredForObjectInfo(@NotNull CollectiveProcessingObjectInfo oInfo) {
		CollectiveProcessingObjectInfoInputrequired inputRequired = null;
		synchronized (executionId) {
			if (this.oInfoInputRequiredStore.containsKey(oInfo.getInfoRowNumber())) {
				inputRequired = this.oInfoInputRequiredStore.get(oInfo.getInfoRowNumber());
			}
		}
		return inputRequired;
	}

	public long storeInputRequiredForObjectInfo(long rowNumber,
				@NotNull CollectiveProcessingObjectInfoInputrequired inputRequired) {
		CollectiveProcessingObjectInfoInputrequired inputRequiredStore = (CollectiveProcessingObjectInfoInputrequired)
					inputRequired.clone();
		synchronized (executionId) {
			if (!isAborted()) {
				lastUpdate = System.currentTimeMillis();
				this.oInfoInputRequiredStore.put(rowNumber, inputRequiredStore);
			}
		}
		return lastUpdate;
	}

	public CollectiveProcessingObjectInfoInputrequired handleInputRequiredFromPOST(JsonObject postData) {
		CollectiveProcessingObjectInfoInputrequired spec = null;

		if (isAborted()) {
			return spec;
		}

		JsonObject inputrequired = (JsonObject) postData.get("inputrequired");
		JsonNumber objectRowInfo = postData.getJsonNumber("currentRowInfoNumber");
		if (inputrequired != null && objectRowInfo != null) {
			final CollectiveProcessingObjectInfoInputrequired.Builder<Void> builder =
					CollectiveProcessingObjectInfoInputrequired.builder();
			JsonObject result = (JsonObject) inputrequired.get("result");

			if (result != null) {
				List<InputRequiredResultMapItem> resultMapItems = new ArrayList<>();
				for (String key : result.keySet()) {
					resultMapItems.add(InputRequiredResultMapItem
							.builder()
							.withKey(key)
							.withValue(result.get(key))
							.build());
				}
				builder.withResult(resultMapItems);
			}

			if (inputrequired.containsKey("applyForMultiEdit")) {
				builder.withApplyForMultiEdit(inputrequired.getBoolean("applyForMultiEdit"));
			}

			JsonObject specification = (JsonObject) inputrequired.get("specification");
			if (specification != null) {
				List<String> optionsArr = new ArrayList<>();
				JsonArray optionsJson = (JsonArray) specification.get("options");
				if (optionsJson != null) {
					optionsArr.addAll(
							optionsJson
									.stream()
									.map(jsonValue -> jsonValue.toString())
									.collect(Collectors.toList())
					);
				}
				String defaultoption = null;
				if (specification.containsKey("defaultoption")) {
					defaultoption = specification.getString("defaultoption");
				}

				builder.withSpecification(InputRequiredSpecification.builder()
						.withKey(specification.getString("key"))
						.withMessage(specification.getString("message"))
						.withType(specification.getString("type"))
						.withDefaultoption(defaultoption)
						.withOptions(optionsArr)
						.build());
			}

			spec = builder.build();
		}

		synchronized (executionId) {
			// newSpec == 'new questions' for the client
			CollectiveProcessingObjectInfoInputrequired newSpec = getInputRequired();
			if (objectRowInfo != null && spec != null) {
				if (newSpec != null) {
					// just save new answers
					final String newSpecificationKey = newSpec.getSpecification().getKey();
					InputRequiredResultMapItem resultItem = spec.getResult().stream()
							.filter(p -> p.getKey().equals(newSpecificationKey)).findFirst().orElse(null);
					if (resultItem != null
							&&
						!newSpec.getResult().stream()
								.anyMatch(p -> p.getKey().equals(newSpecificationKey))
					) {
						if (newSpec.isApplyForMultiEdit()) {
							// override old values
							this.applyForMultiEditStore.add(resultItem);
						}
						this.storeInputRequiredForObjectInfo(objectRowInfo.longValue(), spec);
						// only return new questions
						newSpec = null;
					}
				}
			}
			spec = newSpec;
		}

		return spec;
	}

	/**
	 * the stock will be cleared afterwards
	 * @return sorted list of stored object infos
	 */
	public ProgressInfoSinceLastRequest getProgressInfoSinceLastRequest() {
		List<CollectiveProcessingObjectInfo> objectInfos;
		long progressInPercent;
		synchronized (executionId) {
			progressInPercent = totalCount == 0l ? 0L : 100 * countProcessed / totalCount;
			objectInfos = new ArrayList<>(oInfoStore.values());
		}
		objectInfos.sort(new Comparator<CollectiveProcessingObjectInfo>() {
			@Override
			public int compare(final CollectiveProcessingObjectInfo o1, final CollectiveProcessingObjectInfo o2) {
				return Long.compare(o1.getInfoRowNumber(), o2.getInfoRowNumber());
			}
		});
		// 100% == collective processing finished ... Not? => 99%!
		if (progressInPercent >= 100l && countProcessed < totalCount) {
            progressInPercent = 99l;
        }
		return new ProgressInfoSinceLastRequest(progressInPercent, objectInfos);
	}

	public long getLastUpdate() {
		return lastUpdate;
	}

}
