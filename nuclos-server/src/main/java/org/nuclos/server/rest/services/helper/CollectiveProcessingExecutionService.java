package org.nuclos.server.rest.services.helper;

import static java.util.stream.Collectors.joining;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.json.Json;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;

import org.nuclos.api.Message;
import org.nuclos.api.context.InputRequiredException;
import org.nuclos.api.context.InputSpecification;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JsonUtils;
import org.nuclos.common.Mutable;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.rest.CollectiveProcessingObjectInfo;
import org.nuclos.schema.rest.CollectiveProcessingObjectInfoInputrequired;
import org.nuclos.schema.rest.EditAttributeMap;
import org.nuclos.schema.rest.InputRequiredResultMapItem;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.ejb3.ApiMessageContextService;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.rest.ejb3.BoGenerationResult;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.InputRequiredWebException;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RDataType;
import org.nuclos.server.rest.misc.SkipValueException;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.nuclos.server.ruleengine.NuclosInputRequiredException;
import org.nuclos.server.security.NuclosRemoteExceptionLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

@Component
public class CollectiveProcessingExecutionService {

	private static final Logger LOG = LoggerFactory.getLogger(CollectiveProcessingExecutionService.class);

	@Lazy
	@Autowired
	protected JsonFactory jsonFactory;

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private MetaProvider metaProv;

	@Autowired
	private SpringLocaleDelegate localeDelegate;

	@Autowired
	private NucletDalProvider dalProvider;

	@Autowired
	private NuclosUserDetailsContextHolder userDetails;

	@Autowired
	private NuclosRemoteExceptionLogger remoteExceptionLogger;

	@PostConstruct
	final void init() {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		Timer timer = new Timer(true);
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				evictForgottenCacheEntries();
			}
		}, 1000 * 60 * 60 * 24);
	}

	//private final ThreadPoolExecutor executor = Executors.newCachedThreadPool();
	private final ThreadPoolExecutor executor = new ThreadPoolExecutor(0, Integer.MAX_VALUE,
			60L, TimeUnit.SECONDS,
			new SynchronousQueue<Runnable>()) {
		@Override
		protected void beforeExecute(final Thread t, final Runnable r) {
			super.beforeExecute(t, ((ThreadLocalePropagationRunnable)r).initThreadLocales());
		}
	};

	@Transactional(propagation= Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public void executeStateChangeTransactional(UID entityUID, Object id, UID transitionUID) throws CommonBusinessException {
		UID stateUID = Rest.facade().getTargetStateId(transitionUID);
		Rest.facade().changeState(entityUID, (Long) id, stateUID);
	}

	@Transactional(propagation= Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public void executeDeleteTransactional(UID entityUID, Object id) throws CommonBusinessException {
		Rest.facade().delete(entityUID, id, false);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class})
	public void executeEditTransactional(UID entityUID, Object id, List<EditAttributeMap> fieldChanges) throws CommonBusinessException {
		EntityObjectVO<Object> eo = Rest.facade().get(entityUID, id, false);
		UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(id, entityUID);
		final EntityMeta<Object> entityMeta = metaProv.getEntity(entityUID);
		for (EditAttributeMap change : fieldChanges) {
			final Optional<FieldMeta<?>> field = entityMeta.getFields()
					.stream()
					.filter(f -> Rest.translateUid(E.ENTITYFIELD, f.getUID()).equals(change.getRefId()))
					.findFirst();
			field.ifPresent(fieldMeta -> {
				if (Rest.isReadAllowedForField(fieldMeta, usage) && Rest.isWriteAllowedForField(fieldMeta, usage)) {

					try {
						JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();

						if (change.getValue() instanceof Map) {
							JsonObjectBuilder innerObject = Json.createObjectBuilder();
							for (Object key : ((Map) change.getValue()).keySet()) {
								Object val = ((Map) change.getValue()).get(key);
								if (val != null) {
									innerObject.add(key.toString(), val.toString());
								} else {
									innerObject.addNull(key.toString());
								}
							}
							jsonBuilder.add(change.getRefId(), innerObject.build());
						} else {
							if (change.getValue() != null) {
								jsonBuilder.add(change.getRefId(), change.getValue().toString());
							} else {
								jsonBuilder.add(change.getRefId(), JsonValue.NULL);
							}
						}

						JsonObject json = jsonBuilder.build();

						RDataType rdata = new RDataType(fieldMeta.getDataType(), fieldMeta.getPrecision());

						Object content = rdata.getValue(json, change.getRefId());

						eo.setFieldValue(fieldMeta.getUID(), content);

						if (fieldMeta.isFileDataType()) {
							if (content != null) {
								GenericObjectDocumentFile godf = (GenericObjectDocumentFile) content;
								eo.setFieldUid(fieldMeta.getUID(), (UID) godf.getDocumentFilePk());
							} else {
								eo.setFieldUid(fieldMeta.getUID(), null);
							}

						} else if (fieldMeta.getForeignEntity() != null) {
							boolean bNullKey = !json.containsKey(change.getRefId()) || json.isNull(change.getRefId());
							UID foreignEntity = fieldMeta.getForeignEntity();
							boolean bUidEntity = Rest.getEntity(foreignEntity).isUidEntity();

							if (bNullKey) {
								eo.setFieldValue(fieldMeta.getUID(), null);
							} else {
								JsonObject jsonFK = json.getJsonObject(change.getRefId());
								if (jsonFK.isNull("id")) {
									eo.removeFieldId(fieldMeta.getUID());
									eo.removeFieldUid(fieldMeta.getUID());
									eo.removeFieldValue(fieldMeta.getUID());
								} else {
									if (bUidEntity) {
										EntityMeta<Object> entityMetaT = E.getByUID(foreignEntity);
										UID fk = Rest.translateFqn(entityMetaT, jsonFK.getJsonString("id").getString());
										eo.setFieldUid(fieldMeta.getUID(), fk);
									} else {
										Long fk = Long.parseLong(jsonFK.getString("id"));
										eo.setFieldId(fieldMeta.getUID(), fk);
									}

									String name = null;
									if (jsonFK.containsKey("name") && !jsonFK.isNull("name")) {
										name = jsonFK.getString("name");
									}

									eo.setFieldValue(fieldMeta.getUID(), name);
								}
							}
						}
					} catch (SkipValueException ignored) {
					}
				}

			});
		}
		Rest.facade().update(eo);
	}

	@Transactional(propagation= Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public void executeCustomRuleTransactional(UID entityUID, Object id, String rule) throws CommonBusinessException {
		EntityObjectVO<Object> eo = Rest.facade().get(entityUID, id, true);
		Rest.facade().fireCustomEventSupport(eo, null, rule);
	}

	public void executeCustomRule(
			final CollectiveProcessingExecutionEntry cacheEntry,
			final CollectableSearchExpression clctexpr,
			final UID entityUID,
			final String rule) {
		executeOn(cacheEntry, clctexpr, entityUID, (id, oInfo) -> executeCustomRuleTransactional(entityUID, id, rule));
	}

	public void executeDelete(
			final CollectiveProcessingExecutionEntry cacheEntry,
			final CollectableSearchExpression clctexpr,
			final UID entityUID) {
		executeOn(cacheEntry, clctexpr, entityUID, (id, oInfo) -> executeDeleteTransactional(entityUID, id));
	}

	public void executeEdit(
			final CollectiveProcessingExecutionEntry cacheEntry,
			final CollectableSearchExpression clctexpr,
			final UID entityUID,
			final List<EditAttributeMap> fieldChanges
	) {
		executeOn(cacheEntry, clctexpr, entityUID, (id, oInfo) -> executeEditTransactional(entityUID, id, fieldChanges));
	}

	public void executeStateChanges(
			final CollectiveProcessingExecutionEntry cacheEntry,
			final CollectableSearchExpression clctexpr,
			final UID entityUID,
			final UID transitionUID) {
		executeOn(cacheEntry, clctexpr, entityUID, (id, oInfo) -> executeStateChangeTransactional(entityUID, id, transitionUID));
	}

	interface ExecGen<PK> {
		void run(List<PK> ids, Map<PK, Integer> eoIdsAndVersions);
	}

	public <PK> void executeGeneration(
			final WebContext context,
			final CollectiveProcessingExecutionEntry cacheEntry,
			final CollectableSearchExpression clctexpr,
			final UID entityUID,
			final UID generationUID) {
		executor.execute(new ThreadLocalePropagationRunnable() {
			@Override
			public void run() {
				try {
					GeneratorActionVO generation = Rest.facade().getGeneration(generationUID);
						final Mutable<Long> infoRowNumber = new Mutable<>(0L);
						ExecGen<PK> execGen = (groupIds, eoIdsAndVersions) -> {
							BoGenerationResult genResult = null;
							String jsonGenResult = null;
							CommonBusinessException generationException = null;
							try {
								if (!cacheEntry.isAborted()) {
									genResult = Rest.facade().generateBo(entityUID, eoIdsAndVersions, null, generationUID, null);
								}
							} catch (CommonBusinessException e) {
								generationException = e;
							}
							if (genResult != null) {
								jsonGenResult = JsonUtils.objectToString(jsonFactory.buildJsonObject(genResult, context).build());
							}
							final String finalJsonGenResult = jsonGenResult;
							final CommonBusinessException finalGenerationException  = generationException;
							final BoGenerationResult finalGenResult = genResult;
							_run(cacheEntry, groupIds, infoRowNumber, entityUID, (id, oInfo) -> {
								oInfo.setBoGenerationResult(finalJsonGenResult);
								if (finalGenResult != null && finalGenResult.getBusinessError() != null) {
									throw new BusinessException(finalGenResult.getBusinessError());
								}
								if (finalGenerationException != null) {
									if (finalGenerationException instanceof GeneratorFailedException) {
										throw new BusinessException(((GeneratorFailedException) finalGenerationException).getGenerationResult().getError());
									} else {
										throw remoteExceptionLogger.logException(finalGenerationException, LOG);
									}
								}
							});
						};
					if (generation.isGroupAttributes() && Rest.facade().hasGroupingAttributes(generation.getId())) {
						final Collection<PK> ids = Rest.facade().getEntityObjectIds(entityUID, clctexpr);
						cacheEntry.setTotalCount(ids.size());
						final Map<String, Collection<EntityObjectVO<PK>>> groupedObjects = Rest.facade().groupObjectsForGeneration(ids, generation);
						groupedObjects.values().forEach(group -> {
							List<PK> groupIds = group.stream().map(EntityObjectVO::getPrimaryKey).collect(Collectors.toList());
							final Map<PK, Integer> eoIdsAndVersions = new HashMap<>();
							group.forEach(eo -> eoIdsAndVersions.put(eo.getPrimaryKey(), eo.getVersion()));
							execGen.run(groupIds, eoIdsAndVersions);
						});
					} else {
						Collection<EntityObjectVO<PK>> eos =
								Rest.facade().getEntityObjectsChunk(entityUID, clctexpr, new ResultParams(Collections.singleton(SF.VERSION.getUID(entityUID))));
						List<PK> ids = eos.stream().map(EntityObjectVO::getPrimaryKey).collect(Collectors.toList());
						cacheEntry.setTotalCount(ids.size());
						if (!generation.isGroupAttributes()) {
							eos.forEach(eo ->
								execGen.run(Collections.singletonList(eo.getPrimaryKey()), Collections.singletonMap(eo.getPrimaryKey(), eo.getVersion()))
							);
						} else {
							final Map<PK, Integer> eoIdsAndVersions = new HashMap<>();
							eos.forEach(eo -> eoIdsAndVersions.put(eo.getPrimaryKey(), eo.getVersion()));
							execGen.run(ids, eoIdsAndVersions);
						}
					}
				} catch (Exception e) {
					abortWithErrorInfoForUser(cacheEntry, e);
					throw new NuclosWebException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
				}
			}
		});
	}

	private <PK> void executeOn(final CollectiveProcessingExecutionEntry cacheEntry, final CollectableSearchExpression clctexpr, final UID entityUID, CollectiveProcessingExecutionConsumer<PK> consumer) {
		executor.execute(new ThreadLocalePropagationRunnable() {
			@Override
			public void run() {
				_run(cacheEntry, clctexpr, entityUID, consumer);
			}
		});
	}

	private <PK> void _run(final CollectiveProcessingExecutionEntry cacheEntry, final CollectableSearchExpression clctexpr, final UID entityUID, CollectiveProcessingExecutionConsumer<PK> consumer) {
		try {
			final Collection<PK> ids = Rest.facade().getEntityObjectIds(entityUID, clctexpr);
			cacheEntry.setTotalCount(ids.size());
			final Mutable<Long> infoRowNumber = new Mutable<>(0L);
			_run(cacheEntry, ids, infoRowNumber, entityUID, consumer);
		} catch (Exception e) {
			abortWithErrorInfoForUser(cacheEntry, e);
			throw new NuclosFatalException(e.getMessage(), e);
		}
	}

	private void abortWithErrorInfoForUser(final CollectiveProcessingExecutionEntry cacheEntry, Exception e) {
		cacheEntry.abort();
		final CollectiveProcessingObjectInfo oInfo = CollectiveProcessingExecutionEntry.newObjectInfo(
				"errorMessage",
				-1L,
				"Error",
				1L
		);
		oInfo.setNote(e.getMessage());
		oInfo.setInProgress(false);
		cacheEntry.storeObjectInfo(oInfo);
		cacheEntry.setTotalCount(1);
	}

	private static boolean aborted(final CollectiveProcessingExecutionEntry cacheEntry, CollectiveProcessingObjectInfo oInfo) {
		if (cacheEntry.isAborted()) {
			if (cacheEntry.getTotalCount() > 1) {
				oInfo.setSuccess(false);
				oInfo.setNote("Aborted by User."); // internationalize ?
				oInfo.setInProgress(false);
				cacheEntry.storeObjectInfo(oInfo);
				cacheEntry.setTotalCount(1);
			}

			return true;
		}
		return false;
	}

	private <PK> void handleInputRequired(final CollectiveProcessingExecutionEntry cacheEntry, final PK id, CollectiveProcessingObjectInfo oInfo, CollectiveProcessingExecutionConsumer<PK> consumer) {
		Rest.facade().updateInputContext(null); // enable user to query for InputContext.isSupported()
		oInfo.setInputRequired(false);
		boolean waitForInput = true;
		long lastInputUpdate = cacheEntry.getLastUpdate();
		try {
			while (waitForInput) {
				if (aborted(cacheEntry, oInfo)) {
					break;
				}
				final List<Message> messageStore = new ArrayList<>();
				try {
					ApiMessageContextService.setMessageStore(messageStore);
					if (oInfo.isInputRequired()) {
						if (cacheEntry.getLastUpdate() + (1000 * 60 * 30) < System.currentTimeMillis()) {
							// 30m no update? Dead client or the client has left processing
							LOG.warn("Stop waiting for input from client (execution {}). ", cacheEntry.getExecutionId());
							cacheEntry.abort();
							continue;
						}
						if (cacheEntry.getLastUpdate() == lastInputUpdate) {
							// wait and look for updates again.
							// only continue processing when a new input is available!
							try {
								Thread.sleep(500);
							} catch (InterruptedException ie) {
								LOG.error("Sleeping of CollectiveProcessingExecution failed: " + ie.getMessage(), ie);
								cacheEntry.abort();
							}
							continue;
						}
					}

					// update, even there is currently no InputRequired in ObjectInfo set (all from applyForMultiEditStore)
					Rest.facade().updateInputContext(
							cacheEntry.getInputRequiredResult(oInfo).stream()
									.filter(p -> p.getValue() != null)
									.collect(Collectors.toMap(
											InputRequiredResultMapItem::getKey,
											v -> {
												if (v.getValue() instanceof JsonNumber) {
													return ((JsonNumber) v.getValue()).intValue();
												} else if (v.getValue() instanceof JsonString) {
													return ((JsonString) v.getValue()).getString();
												} else {
													return v.getValue().toString();
												}
											}
									)));

					consumer.apply(id, oInfo);
					oInfo.setInputRequired(false);
					oInfo.setSuccess(true);
					String resultMessage = messageStore.stream()
							.map(m -> Stream.of(m.getTitle(), m.getMessage())
									.filter(s -> !StringUtils.looksEmpty(s))
									.collect(joining(": ")))
							.collect(joining(" | "));
					if (resultMessage.length() > 0) {
						oInfo.setNote(resultMessage);
					}
					waitForInput = false;

				} catch (Throwable e) {
					InputSpecification inputSpecification = null;
					if (e instanceof NuclosInputRequiredException) {
						inputSpecification = ((InputRequiredException) e.getCause()).getInputSpecification();
					} else if (e instanceof InputRequiredException) {
						inputSpecification = ((InputRequiredException) e).getInputSpecification();
					}
					if (inputSpecification != null) {
						oInfo.setInputRequired(true);
						cacheEntry.storeObjectInfo(oInfo);
						List<InputRequiredResultMapItem> result = null;
						if (cacheEntry.getInputRequiredForObjectInfo(oInfo) != null) {
							result = cacheEntry.getInputRequiredForObjectInfo(oInfo).getResult();
						}
						lastInputUpdate = cacheEntry.storeInputRequiredForObjectInfo(oInfo.getInfoRowNumber(),
								CollectiveProcessingObjectInfoInputrequired
										.builder()
										.withResult(result)
										.withApplyForMultiEdit(inputSpecification.applyForAllMultiEditObjects())
										.withSpecification(
												InputRequiredWebException.transformInputSpecification(inputSpecification)
										)
										.build()
						);

					} else {
						// inputSpecification == null
						waitForInput = false;
						LOG.error("CollectiveProcessingExecution exception: executionId=" + cacheEntry.getExecutionId() +
										", id=" + id +
										", exception=" + e.getMessage(),
								e);
						oInfo.setSuccess(false);
						oInfo.setInputRequired(false);
						oInfo.setNote(e.getMessage());
					}
				}
			}
		} finally {
			Rest.facade().clearInputContext();
			ApiMessageContextService.clearMessageStore();
			oInfo.setInProgress(false);
			cacheEntry.storeObjectInfo(oInfo);
		}
	}

	private <PK> void _run(final CollectiveProcessingExecutionEntry cacheEntry, final Collection<PK> ids, final Mutable<Long> infoRowNumber, final UID entityUID, CollectiveProcessingExecutionConsumer<PK> consumer) {
		for (PK id : ids) {
			final String eoTitle = getEoTitle(entityUID, id);
			final String entityFQN = Rest.translateUid(E.ENTITY, entityUID);
			final Long rowNumber = infoRowNumber.setValue(infoRowNumber.getValue() + 1L);
			CollectiveProcessingObjectInfo oInfo = cacheEntry.retrieveObjectInfo(rowNumber);
			if (oInfo == null) {
				oInfo = CollectiveProcessingExecutionEntry.newObjectInfo(entityFQN, id, eoTitle, rowNumber);
			}
			oInfo.setInProgress(true);
			cacheEntry.storeObjectInfo(oInfo);

			if (aborted(cacheEntry, oInfo)) {
				break;
			}

			handleInputRequired(cacheEntry, id, oInfo, consumer);
		}
	}

	private String getEoTitle(UID entityUID, Object oId) {
		final EntityObjectVO<?> eo = dalProvider.getEntityObjectProcessor(metaProv.getEntity(entityUID)).getByPrimaryKey(oId);
		return localeDelegate.getTreeViewLabel(eo, metaProv, userDetails.getDataLocal());
	}

	public String newExecutionId() {
		return new UID().getString();
	}

	public CollectiveProcessingExecutionEntry createCacheEntry(@NotNull String executionId, @NotNull String user) {
		checkExecutionParameter(executionId, user);
		final CollectiveProcessingExecutionEntry result = getCacheEntry(executionId);
		if (result.getUser() != null) {
			throw new NuclosFatalException("Cache entry exist already: executionId=" + executionId + ", userInCache=" + result.getUser() + ", userCreate=" + user);
		}
		result.setUser(user);
		return result;
	}

	public CollectiveProcessingExecutionEntry getCacheEntry(@NotNull String executionId, @NotNull String user) throws CommonPermissionException, CommonFinderException {
		checkExecutionParameter(executionId, user);
		final CollectiveProcessingExecutionEntry result = getCacheEntry(executionId);
		if (result.getUser() == null) {
			evictCache(executionId);
			throw new CommonFinderException("Execution with id " + executionId + " does not exist!");
		}
		if (!RigidUtils.equal(result.getUser(), user)) {
			throw new CommonPermissionException("User " + user + " is not authorized to access this information!");
		}
		return result;
	}

	private void checkExecutionParameter(@NotNull final String executionId, @NotNull final String user) {
		if (executionId == null) {
			throw new IllegalArgumentException("executionId must not be null");
		}
		if (user == null) {
			throw new IllegalArgumentException("user must not be null");
		}
	}

	private void evictForgottenCacheEntries() {
		Collection<Object> collectiveProcessingExecutionEntries = new ArrayList(
				((ConcurrentMapCache) cacheManager.getCache("collectiveProcessingExecutionCache")).getNativeCache().values()
		);
		collectiveProcessingExecutionEntries.stream().forEach(object -> {
			CollectiveProcessingExecutionEntry cacheEntry = (CollectiveProcessingExecutionEntry) object;
			if (cacheEntry.getLastUpdate() + (1000 * 60 * 60 * 12) < System.currentTimeMillis()) {
				// 12h no update? Dead client or client is not watching the progress
				LOG.warn("Removing progress cache entry for execution " + cacheEntry.getExecutionId() + ". " +
						"It's been more than 12 hours since the last update, and the information was not requested by any client");
				this.evictCache(cacheEntry.getExecutionId());
			}
		});
	}

	@Cacheable(value="collectiveProcessingExecutionCache", key="#p0")
	public CollectiveProcessingExecutionEntry getCacheEntry(@NotNull String executionId) {
		return new CollectiveProcessingExecutionEntry(executionId);
	}

	@Caching(evict = {
			@CacheEvict(value = "collectiveProcessingExecutionCache", key = "#p0")
	})
	public void evictCache(@NotNull String executionId) {
	}

	private abstract class ThreadLocalePropagationRunnable implements Runnable {
		private final SecurityContext securityContext;
		private final NuclosUserDetailsContextHolder.AllValues userDetailsValues;
		private final RequestAttributes requestAttributes;
		public ThreadLocalePropagationRunnable() {
			this.securityContext = SecurityContextHolder.getContext();
			this.userDetailsValues = userDetails.getAllValues();
			this.requestAttributes = RequestContextHolder.getRequestAttributes();
		}

		public ThreadLocalePropagationRunnable initThreadLocales() {
			SecurityContextHolder.setContext(this.securityContext);
			userDetails.setAllValues(this.userDetailsValues);
			RequestContextHolder.setRequestAttributes(requestAttributes);
			return this;
		}
	}

}
