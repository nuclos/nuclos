package org.nuclos.server.rest.services.helper;

import java.util.HashMap;
import java.util.Map;

import javax.json.JsonObject;

import org.nuclos.api.User;
import org.nuclos.api.context.CustomRestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomRestContextImpl implements CustomRestContext {

	private final User user;
	private final WebContext webContext;
	private final JsonObject postData;
	private final Class ruleClass;
	private final String language;
	private final Logger logger;

	public CustomRestContextImpl(
			final User user,
			final WebContext webContext,
			final JsonObject postData,
			final Class ruleClass,
			final String language
	) {
		this.user = user;
		this.webContext = webContext;
		this.postData = postData;
		this.ruleClass = ruleClass;
		this.language = language;
		this.logger = LoggerFactory.getLogger(this.ruleClass);
	}


	private final Map<String, Object> cache = new HashMap<String, Object>();

	@Deprecated
	@Override
	public String getRequestParameter(final String key) {
		if (webContext == null) {
			throw new IllegalStateException("getRequestParameter is not supported.");
		}
		return webContext.getFirstParameter(key, String.class);
	}

	@Deprecated
	@Override
	public JsonObject getRequestData() {
		return postData;
	}

	@Override
	public Object getContextCacheValue(String key) {
		return cache.get(key);
	}

	@Override
	public void addContextCacheValue(String key, Object value) {
		if (cache.containsKey(key)) {
			throw new IllegalArgumentException(String.format("Event cache contains key %s", key));
		}
		cache.put(key, value);
	}

	@Override
	public void removeContextCacheValue(String key) {
		cache.remove(key);
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public String getLanguage() {
		return language;
	}

	@Override
	public void log(final String message) {
		logger.info(message);
	}

	@Override
	public void logWarn(final String message) {
		logger.warn(message);
	}

	@Override
	public void logError(final String message, final Exception ex) {
		logger.error(message, ex);
	}
}
