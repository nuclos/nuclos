package org.nuclos.server.rest.services.helper;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Provider;

import org.nuclos.api.context.CustomRestContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Is required for the Injection from Glassfish
 */
@ApplicationScoped
public class CustomRestContextProducer implements Provider<CustomRestContext> {

	@Produces
	public CustomRestContext getCustomRestContext() {
		final String userName = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		return CustomRestContextFactory.createCustomRestContext(userName);
	}

	@Override
	public CustomRestContext get() {
		return getCustomRestContext();
	}

}
