package org.nuclos.server.rest.services.helper;

import java.io.InputStream;

import javax.json.JsonObject;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.nuclos.api.User;
import org.nuclos.api.context.CustomRestUploadContext;

public class CustomRestUploadContextImpl extends CustomRestContextImpl implements CustomRestUploadContext {

	private final InputStream inputStream;
	private final FormDataContentDisposition formData;

	public CustomRestUploadContextImpl(
			final User user,
			final WebContext webContext,
			final JsonObject postData,
			final Class ruleClass,
			final String language,
			final InputStream inputStream,
			final FormDataContentDisposition formData
	) {
		super(user, webContext, postData, ruleClass, language);
		this.inputStream = inputStream;
		this.formData = formData;
	}

	@Override
	public InputStream getInputStream() {
		return inputStream;
	}

	@Override
	public String getFilename() {
		return formData.getFileName();
	}
}
