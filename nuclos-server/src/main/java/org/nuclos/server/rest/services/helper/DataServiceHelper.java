package org.nuclos.server.rest.services.helper;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections.CollectionUtils;
import org.nuclos.api.NuclosImage;
import org.nuclos.api.context.InputDelegateSpecification;
import org.nuclos.api.context.InputRequiredException;
import org.nuclos.api.context.InputSpecification;
import org.nuclos.businessentity.rule.JobControllerDisableRule;
import org.nuclos.businessentity.rule.JobControllerEnableRule;
import org.nuclos.businessentity.rule.JobControllerExecuteRule;
import org.nuclos.common.Actions;
import org.nuclos.common.DependentSelection;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SFE;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.ValueListProviderType;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.layoutml.ButtonConstants;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.dal.processor.nuclos.IExtendedEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.rest.ejb3.BoGenerationResult;
import org.nuclos.server.rest.ejb3.IRValueObject.JsonBuilderConfiguration;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebLayout;
import org.nuclos.server.rest.misc.InputRequiredWebException;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.DependenceService.DependenceBoLinksFactory;
import org.nuclos.server.rest.services.rvo.FilterJ;
import org.nuclos.server.rest.services.rvo.LayoutAdditions;
import org.nuclos.server.rest.services.rvo.RValueObject;
import org.nuclos.server.rest.services.rvo.ReferenceListParameterJson;
import org.nuclos.server.rest.services.rvo.ResultListExportRVO;
import org.nuclos.server.rest.services.rvo.ResultRVO;
import org.nuclos.server.rest.services.rvo.UsageProperties;
import org.nuclos.server.ruleengine.NuclosInputRequiredException;
import org.nuclos.server.statemodel.NuclosSubsequentStateNotLegalException;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Configurable
public class DataServiceHelper extends MetaDataServiceHelper implements ButtonConstants {

	private static final Logger LOG = LoggerFactory.getLogger(DataServiceHelper.class);

	private static final String RECEIVER_ID = "receiverId";

	@Autowired
	protected MasterDataRestFqnCache fqnCache;

	@Autowired
	protected ParameterProvider parameterProvider;

	@Autowired
	private MetaProvider metaProv;

	protected final JsonObjectBuilder getBoSelf(String bom, @NotNull String id) {
		SessionEntityContext info = checkPermission(E.ENTITY, bom, id);

		JsonObjectBuilder row;
		try {
			if (info.isUidEntity()) {
				row = getRow(info, Rest.translateFqn(getBOMeta(), id));
			} else if (id.contains("temp_")) {
				row = getRow(info, id);
			} else {
				row = getRow(info, Long.parseLong(id));
			}

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, bom));
		}

		if (row == null) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}

		return row;
	}

	protected final <PK> void deleteBo(String bom, String id,
								  JsonObject data) {
		SessionEntityContext info = checkPermission(E.ENTITY, bom, id);

		try {
			if (data != null) {
				updateInputContext(data);
			} else {
				clearInputContext();
			}

			if (info.isUidEntity()) {
				Rest.facade().delete(info.getEntity(), Rest.translateFqn(getBOMeta(), id), false);
			} else {
				Rest.facade().delete(info.getEntity(), Long.parseLong(id), false);
			}
		} catch (Exception ex) {
			if (data != null) {
				handleInputRequiredException(ex);
			}
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, bom));
		} finally {
			clearInputContext();
		}
	}

	protected final JsonObjectBuilder cloneBo(String bom, String id, String layoutid, JsonObject data) {
		SessionEntityContext info = checkPermission(E.ENTITY, bom, id);

		UID layoutUid = StringUtils.isNullOrEmpty(layoutid) ? null : Rest.translateFqn(E.LAYOUT, layoutid);

		updateInputContext(data);
		Long parameterObjectId = null;
		if (data.containsKey("parameterObjectId")) {
			parameterObjectId = Long.parseLong(data.getString("parameterObjectId"));
		}
		DependentSelection dependentSelection = null;
		if (data.get("selection") != null) {
			dependentSelection = this.parseTopDependentSelection((JsonObject) data.get("selection"));
		}
		try {
			EntityObjectVO<?> eo = Rest.facade().get(info.getEntity(),
					info.isUidEntity() ? Rest.translateFqn(getBOMeta(), id) : Long.parseLong(id),
					true);
			eo = Rest.facade().clone(eo, layoutUid, true, parameterObjectId, dependentSelection);
			if (eo != null) {
				JsonObjectBuilder json = getJsonObjectBuilder(eo, layoutUid);
				return json;
			}
			return null;
		} catch (Exception ex) {
			handleInputRequiredException(ex);
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, bom));
		}
	}

	protected final void unlockBo(String bom, String id) {
		SessionEntityContext info = checkPermission(E.ENTITY, bom, id);

		try {
			Rest.facade().unlock(info.getEntity(), Long.parseLong(id));
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, bom));
		}
	}


	protected String getSubBoMetaId(final String refAttrId) {
		UID fieldUID = Rest.translateFqn(E.ENTITYFIELD, refAttrId);
		//Note: If the referencing Attribute is not known, but the Reference Entity, then this could be given in the Rest-Service, too.
		if (fieldUID == null) {
			return refAttrId;
		}

		FieldMeta<?> fieldMeta =  Rest.getEntityField(fieldUID);
		return Rest.translateUid(E.ENTITY, fieldMeta.getEntity());
	}


	protected JsonObjectBuilder executeRule(
			final String boMetaId,
			final String boId,
			final String ruleId,
			final JsonObject postData
	) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);

		updateInputContext(postData);

		EntityObjectVO<?> eo;
		JsonObjectBuilder result;
		try {
			if (info.isUidEntity()) {
				eo = Rest.facade().get(info.getEntity(), Rest.translateFqn(getBOMeta(), boId), true);
			} else {
				eo = Rest.facade().get(info.getEntity(), Long.parseLong(boId), true);
			}
			DependentSelection dependentSelection = this.parseTopDependentSelection(postData);
			result = executeRule(eo, dependentSelection, ruleId);

		} catch (Exception ex) {
			handleInputRequiredException(ex);
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}

		if (result == null) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}

		return result;
	}

	public DependentSelection parseTopDependentSelection(JsonObject postData) {
		DependentSelection dependentSelection = new DependentSelection();
		for (Map.Entry<String, JsonValue> entry : postData.entrySet()) {
			dependentSelection.subDependentSelection.put(entry.getKey(), this.parseDependentSelection((JsonObject) entry.getValue()));
		}
		return dependentSelection;
	}

	private DependentSelection parseDependentSelection(JsonObject postData) {
		DependentSelection dependentSelection = new DependentSelection();
		if (postData.containsKey("selectedIds")) {
			JsonArray ids = postData.getJsonArray("selectedIds");
			for (JsonValue value : ids) {
				dependentSelection.selectedIds.add(value.toString());
			}
		}
		if (postData.containsKey("selectedSubEntities")) {
			JsonArray selection = postData.getJsonArray("selectedSubEntities");
			for (JsonValue entry : selection) {
				JsonObject obj = (JsonObject) entry;
				for (Map.Entry<String, JsonValue> subData : obj.entrySet()) {
					DependentSelection ds = this.parseDependentSelection((JsonObject) subData.getValue());
					if (dependentSelection.subDependentSelection.containsKey(subData.getKey())) {
						DependentSelection ds0 = dependentSelection.subDependentSelection.get(subData.getKey());
						ds.subDependentSelection.putAll(ds0.subDependentSelection);
						ds.selectedIds.addAll(ds0.selectedIds);
					}
					dependentSelection.subDependentSelection.put(subData.getKey(), ds);
				}
			}
		}
		return dependentSelection;
	}

	protected JsonObjectBuilder executeRuleInSubformContext(
			final String boMetaId,
			final String boId,
			final String refAttrId,
			final String subBoId,
			final String ruleId,
			final JsonObject postData
	) {
		UID baseEntityUid = Rest.translateFqn(E.ENTITY, boMetaId);
		EntityMeta<?> baseEntityMeta = Rest.getEntity(baseEntityUid);

		UID refAttrUid = Rest.translateFqn(E.ENTITYFIELD, refAttrId);
		FieldMeta<?> refFieldMeta = Rest.getEntityField(refAttrUid);
		checkPermissionForSubform(baseEntityMeta, refFieldMeta, boId, true);

		String subBoMetaId = getSubBoMetaId(refAttrId);
		return executeRule(subBoMetaId, subBoId, ruleId, postData);
	}

	private <PK> JsonObjectBuilder executeRule(EntityObjectVO<PK> eo, DependentSelection dependentSelection, final String ruleId) throws CommonBusinessException {
		//allow rule execution for jobcontroller rules if user is allowed to write entity "jobcontroller"
		if (!Rest.facade().isActionAllowed(Actions.ACTION_EXECUTE_RULE_BY_USER)
			&& !(
					E.JOBCONTROLLER.getUID().equals(eo.getDalEntity())
					&& (JobControllerExecuteRule.class.getName().equals(ruleId)
						|| JobControllerEnableRule.class.getName().equals(ruleId)
						|| JobControllerDisableRule.class.getName().equals(ruleId))
					&& SecurityCache.getInstance().isWriteAllowedForMasterData(Rest.facade().getCurrentUserName(), eo.getDalEntity(), Rest.facade().getCurrentMandatorUID())
			)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		try {
			eo = Rest.facade().fireCustomEventSupport(eo, dependentSelection, ruleId);
		} finally {
			clearInputContext();
		}

		if (eo != null) {
			return getJsonObjectBuilder(eo);
		}
		return null;
	}

	protected final void stateChange(
			String boMetaId,
			String id,
			String statusIdOrNumeral,
			JsonObject data
	) {
		final SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, id);

		final long numericId = Long.parseLong(id);
		final UID statusUid = getStatusUid(info.getEntity(), numericId, statusIdOrNumeral);

		if (statusUid == null) {
			throw new NuclosWebException(Status.BAD_REQUEST);
		}

		try {
			updateInputContext(data);
			Rest.facade().changeState(
					info.getEntity(),
					numericId,
					statusUid
			);
		} catch (Exception ex) {
			handleInputRequiredException(ex);
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		} finally {
			clearInputContext();
		}
	}

	private UID getStatusUid(UID entityUid, Long id, String statusIdOrNumeral) {
		try {
			// Try to parse stateId as Integer, because it could be a status numeral
			Integer stateNumeral = Integer.parseInt(statusIdOrNumeral);
			List<StateVO> states = Rest.getSubsequentStates(entityUid, id);
			for (StateVO state : states) {
				if (state.getNumeral().equals(stateNumeral)) {
					return state.getId();
				}
			}
			return null;
		} catch (NumberFormatException ex) {
			// Ignore
		}

		return Rest.translateFqn(E.STATE, statusIdOrNumeral);
	}

	private void handleInputRequiredException(Exception ex) throws NuclosWebException {
		InputSpecification inputSpecification = null;
		InputDelegateSpecification inputDelegateSpecification = null;
		if (ex instanceof NuclosInputRequiredException) {
			inputSpecification = ((InputRequiredException) ex.getCause()).getInputSpecification();
			inputDelegateSpecification = ((InputRequiredException) ex.getCause()).getInputDelegateSpecification();
		} else if (ex instanceof InputRequiredException) {
			inputSpecification = ((InputRequiredException) ex).getInputSpecification();
		}

		if (inputSpecification != null) {
			throw new InputRequiredWebException(inputSpecification);
		}
		if (inputDelegateSpecification != null) {
			throw new InputRequiredWebException(inputDelegateSpecification);
		}
	}

	protected final <PK> JsonObjectBuilder updateBo(String id, JsonObject object) {
		SessionEntityContext info = checkPermission(object);
		try {

			extractReceiverId(object);
			updateInputContext(object);
			EntityObjectVO<PK> eo = prepareEOForModification(object, NuclosMethod.UPDATE, info);

			if (object.get(ACTION_EXECUTE_CUSTOM_RULE) != null) {
				DependentSelection dependentSelection = null;
				if (object.get("selection") != null) {
					dependentSelection = this.parseTopDependentSelection((JsonObject) object.get("selection"));
				}
				return executeRule(eo, dependentSelection, object.getString(ACTION_EXECUTE_CUSTOM_RULE));

			} else {
				return insertUpdateDelete(eo, NuclosMethod.UPDATE, info);

			}

		} catch (NuclosSubsequentStateNotLegalException ex) {
			final String sErrorMsg = Rest.getMessage("GenericObjectCollectController.34", "Der Statuswechsel konnte nicht vollzogen werden.");
			throw new NuclosWebException(Status.METHOD_NOT_ALLOWED, sErrorMsg);
		} catch (CommonStaleVersionException ex) {
			throw new NuclosWebException(Status.CONFLICT);
		} catch (CommonFinderException ex) {
			throw new NuclosWebException(Status.NOT_FOUND);
		} catch (Exception ex) {
			handleInputRequiredException(ex);
			throw new NuclosWebException(ex, info.getEntity());
		} finally {
			// reset inputContext after save
			clearInputContext();
		}
	}

	protected <PK> JsonObjectBuilder getBoWithAllDefaultValues(String bom, final String processMetaId) {
		//NUCLOS-4221: Sub-forms grant implicitly the privileges for certain entities. Bo-Generation of a subform-row is needed and thus the direct check is not possible.
		//ValidationInfo info =  validateSession(E.ENTITY, bom, null);

		//TODO: Evaluate if this means a security breach. Until now it doesn't look very dangerous as any request for real data is not possible.
		SessionEntityContext info = checkPermission(E.ENTITY, bom, null, processMetaId, false);

		try {
			EntityObjectVO<PK> eo = Rest.facade().getBoWithDefaultValues(info.getEntity());
			if (info.getUsage().getProcessUID() != null) {
				eo.setFieldUid(SFE.PROCESS_UID.getUID(info.getEntity()), info.getUsage().getProcessUID());
				eo.setFieldValue(SFE.PROCESS_UID.getUID(info.getEntity()), Rest.facade().get(E.PROCESS.getUID(), info.getUsage().getProcessUID(), false).getFieldValue(E.PROCESS.name.getUID()));
			}
			UsageCriteria criteria;
			try {
				criteria = Rest.getUsageCriteriaFromEO(eo);
			} catch (NuclosFatalException nfe) {
				//NUCLOS-5417 g)
				if (nfe.getMessage().startsWith("statemodel.usages.error.null")) {
					SpringLocaleDelegate localeDelegate = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
					String msg = localeDelegate.getMessage("NuclosCollectControllerFactory.2", "No statemodel defined");
					throw new NuclosBusinessException(msg);
				}
				throw nfe;
			}

			//NUCLOS-4324: DefaultValueFromVLPWithDefault.
			List<LayoutAdditions> lstAdditions = getComponentsFromLayoutByContext(criteria, true);
			for (LayoutAdditions addition : lstAdditions) {
				WebValueListProvider wvlp = addition.getValueListProvider();
				if (wvlp != null && wvlp.hasDefaultMarker() && !isEmptyMandatoryParameterInVLP(wvlp)) {
					Object searchmode = wvlp.getParameters().get("searchmode");
					if (searchmode == null || (searchmode instanceof String ? StringUtils.isNullOrEmpty((String) searchmode) : ((Integer) searchmode) == 0)) {
						wvlp.addParameter("searchmode", "false", fqnCache);
					}
					List<CollectableField> coll = Rest.facade().getVLPData(
							addition.getUID(),
							wvlp,
							null,
							null,
							DEFAULT_RESULT_LIMIT,
							true,
							Rest.facade().getCurrentMandatorUID()
					);

					if (!coll.isEmpty()) {
						Optional<CollectableField> optCf =
								coll.stream().filter(cf -> {
									if (cf instanceof CollectableValueIdField) {
										return ((CollectableValueIdField) cf).isSelected();
									}
									if (cf instanceof CollectableValueField) {
										return ((CollectableValueField) cf).isSelected();
									}
									return false;
								}).findFirst();
						if (optCf.isPresent()) {
							CollectableField cf = optCf.get();
							eo.setFieldValue(addition.getUID(), cf.getValue());

							if (cf instanceof CollectableValueIdField) {
								if (cf.getValueId() instanceof Long) {
									eo.setFieldId(addition.getUID(), (Long) cf.getValueId());

								} else if (cf.getValueId() instanceof UID) {
									eo.setFieldUid(addition.getUID(), (UID) cf.getValueId());

								}
							}
						}
					}
				}
			}

			UsageProperties up = new UsageProperties(criteria, this, metaProvider, preferencesProvider, parameterProvider, false);
			final UID sessionDataLanguageUID = getSessionContext().getDataLanguageUID();

			// For new records the default layout should be used
			// TODO: Having to override the layout here is ugly
			IWebLayout defaultLayout = this.getWebLayout();
			up.setWebLayout(defaultLayout);

			RValueObject<PK> rvobj = new RValueObject<>(eo, JsonBuilderConfiguration.getNoAddsAndSkipStates(), up, null, sessionDataLanguageUID, jsonFactory, metaProvider, stateCache);

			return rvobj.getJSONObjectBuilder();
		} catch (CommonBusinessException | CommonFatalException ex) {
			throw new NuclosWebException(ex, info.getEntity());
		}
	}

	public boolean isEmptyMandatoryParameterInVLP(WebValueListProvider wvlp) throws CommonFinderException, CommonPermissionException, NuclosDatasourceException {
		return Rest.facade().getParameterListForValueListProvider(wvlp.getValue()).stream()
				.filter(parameter -> Boolean.TRUE.equals(parameter.getMandatory()))
				.anyMatch(parameter -> "".equals(wvlp.getParameters().get(parameter.getLabel())));
	}

	protected JsonObjectBuilder generateBo(
			String boMetaId,
			String id,
			int version,
			String generationId,
			JsonObject data
	) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, id);

		try {

			updateInputContext(data);

			Long parameterObjectId = null;
			if (data.containsKey("parameterObjectId")) {
				parameterObjectId = Long.parseLong(data.getString("parameterObjectId"));
			}
			DependentSelection dependentSelection = null;
			if (data.get("selection") != null) {
				dependentSelection = this.parseTopDependentSelection((JsonObject) data.get("selection"));
			}

			Map<Long, Integer> eoIdsAndVersions = new HashMap<>();
			eoIdsAndVersions.put(Long.parseLong(id), version);

			BoGenerationResult genResult = Rest.facade().generateBo(
					info.getEntity(),
					eoIdsAndVersions,
					dependentSelection,
					Rest.translateFqn(E.GENERATION, generationId),
					parameterObjectId
			);
			return jsonFactory.buildJsonObject(genResult, this);
		} catch (CommonBusinessException ex) {
			throw NuclosWebException.wrap(ex, info.getEntity());
		} catch (Exception ex) {

			InputSpecification inputSpecification = null;
			if (ex instanceof NuclosInputRequiredException) {
				inputSpecification = ((InputRequiredException) ex.getCause()).getInputSpecification();
			} else if (ex instanceof InputRequiredException) {
				inputSpecification = ((InputRequiredException) ex).getInputSpecification();
			}

			if (inputSpecification != null) {
				throw new InputRequiredWebException(inputSpecification);
			} else {
				throw new NuclosWebException(ex, info.getEntity());
			}

		} finally {
			// reset inputContext after save
			clearInputContext();
		}
	}

	protected final <PK> JsonObjectBuilder insertBo(String bom, JsonObject object) {
		SessionEntityContext info = checkPermission(object);
		try {
			updateInputContext(object);
			EntityObjectVO<PK> eo = prepareEOForModification(object, NuclosMethod.INSERT, info);

			if (object.get(ACTION_EXECUTE_CUSTOM_RULE) != null) {
				return executeRule(eo, null, object.getString(ACTION_EXECUTE_CUSTOM_RULE));
			} else {
				return insertUpdateDelete(eo, NuclosMethod.INSERT, info);
			}

		} catch (Exception ex) {
			handleInputRequiredException(ex);
			throw new NuclosWebException(ex, info.getEntity());
		} finally {
			clearInputContext();
		}
	}

	protected final NuclosFile byteAttributeValueWithValidation(String boMetaId, String id, String attrId) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, id);
		return byteAttributeValue(info.getMasterMeta(), id, attrId, true);
	}

	protected final NuclosFile byteAttributeValue(EntityMeta<?> eMeta, String id, String attrId, boolean check) {
		try {
			EntityObjectVO<?> eo;
			if (id != null && id.startsWith("temp_")) {
				eo = Rest.facade().getTemporaryObject(id);
			} else {
				if (eMeta.isUidEntity()) {
					eo = Rest.facade().get(eMeta.getUID(), Rest.translateFqn(eMeta, id), check);

				} else {
					eo = Rest.facade().get(eMeta.getUID(), Long.parseLong(id), check);

				}
			}
			UID fieldUID = Rest.translateFqn(E.ENTITYFIELD, attrId);

			NuclosFile result = null;
			Object fieldValue = eo.getFieldValue(fieldUID);

			if (isImageBytes(fieldValue)) {
				fieldValue = new org.nuclos.common.NuclosImage("", (byte[]) fieldValue);
			}

			if (fieldValue instanceof NuclosImage) {
				NuclosImage ni = (NuclosImage) fieldValue;
				result = new NuclosFile(ni.getName(), ni.getContent());
			} else if (fieldValue instanceof GenericObjectDocumentFile) {
				GenericObjectDocumentFile godf = (GenericObjectDocumentFile) fieldValue;
				try {
					result = new NuclosFile(godf.getFilename(), godf.getContents());
				} catch (Exception e) {
					LOG.warn("Unable to get generic object document file:", e);
				}
			}
			return result;

		} catch (Exception ex) {
			throw new NuclosWebException(ex, eMeta.getUID());
		}
	}

	private <PK> JsonObjectBuilder getRow(SessionEntityContext info, PK pk) throws CommonBusinessException {
		EntityObjectVO<PK> eo = null;
		if (Rest.facade().isTemporaryId(pk.toString())) {
			eo = Rest.getTemporaryEntityObjectIfReadAllowed(pk);
		} else {
			eo = Rest.facade().get(info.getEntity(), pk, true);
			if (eo == null) {
				return null;
			}
		}

		UsageProperties up = new UsageProperties(Rest.getUsageCriteriaFromEO(eo), this, metaProvider, preferencesProvider, parameterProvider, false);
		final UID sessionDataLanguageUID = getSessionContext().getDataLanguageUID();
		return new RValueObject<>(eo, JsonBuilderConfiguration.getFull(), up, null, sessionDataLanguageUID, jsonFactory, metaProvider, stateCache).getJSONObjectBuilder();
	}

	protected FilterJ getFilterJ(JsonObject queryContext, Long defaultChunkSize) {
		return new FilterJ(this, queryContext, defaultChunkSize);
	}

	protected <PK> JsonObjectBuilder getBoList(String bom, JsonObject queryContext) {
		return getBoList(bom, queryContext, 40L, true);
	}

	private <PK> JsonObjectBuilder getBoList(String bom, JsonObject queryContext, Long defaultChunkSize, boolean checkPermission) {
		SessionEntityContext info = checkPermission(E.ENTITY, bom, null, null, checkPermission);
		FilterJ filter = getFilterJ(queryContext, defaultChunkSize);
		boolean bAllFields = filter.showAllFields();
		boolean idOnlySelection = filter.isIdOnlySelection();//"all".equalsIgnoreCase(filter.getFields());

		try {
			Collection<FieldMeta<?>> fields = bAllFields ? Rest.getAllEntityFieldsByEntityIncludingVersion(getBOMeta().getUID()).values() :
					Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());

			Collection<UID> fieldUids = new HashSet<>();
			for (FieldMeta<?> fm : fields) {
				fieldUids.add(fm.getUID());
				if (fm.getColorDatasource() != null) {
					FieldMeta<?> attributeColorMeta = metaProv.getEntityField(UID.parseUID(fm.getUID().toString() + FieldMeta.ATTRIBUTE_COLOR_DATASOURCE_SUFFIX));
					fieldUids.add(attributeColorMeta.getUID());
				}
			}

			Collection<EntityObjectVO<PK>> eos;
			EntityMeta<?> eMeta = getBOMeta();
			CollectableSearchExpression clctse = filter.getSearchExpression(getBOMeta().getUID(), fields);

			Long istart = filter.getStart();
			long chunksize = filter.getEnd() - istart + 1;

			Boolean all;
			Long count;

			if (!filter.countTotal()) {
				count = null;

				if (chunksize == 0) {
					eos = Collections.emptyList();
					all = null;
				} else {

					// Try to fetch chunksize + 1 to determine if there is more data than chunksize
					ResultParams resultParams = new ResultParams(fieldUids, istart, chunksize + 1, true);
					resultParams.setIdOnlySelection(idOnlySelection);

					if (checkPermission) {
						eos = Rest.facade().getEntityObjectsChunk(eMeta.getUID(), clctse, resultParams);
					} else {
						eos = Rest.facade().getEntityObjectsChunkWithoutRollback(eMeta.getUID(), clctse, resultParams);
					}

					// If we fetched more than chunksize EOs remove the additional EO
					all = eos.size() <= chunksize;
					if (!all && eos.size() > 0) {
						Object last = CollectionUtils.get(eos, eos.size() - 1);
						eos.remove(last);
					}
				}

			} else {
				count = Rest.facade().countEntityObjectRows(eMeta.getUID(), clctse);
				all = count < chunksize;
				if (count == 0) {
					eos = new ArrayList<>();

				} else {
					if (istart == 0L && all) {
						istart = -1L;
					}
					ResultParams resultParams = new ResultParams(fieldUids, istart, chunksize, true);
					resultParams.setIdOnlySelection(idOnlySelection);

					if (checkPermission) {
						eos = Rest.facade().getEntityObjectsChunk(eMeta.getUID(), clctse, resultParams);
					} else {
						eos = Rest.facade().getEntityObjectsChunkWithoutRollback(eMeta.getUID(), clctse, resultParams);
					}
				}
			}

			final JsonBuilderConfiguration jsonConfig = filter.getJsonConfig(null);
			final UID dataLanguageUID = getSessionContext().getDataLanguageUID();
			final ResultRVO resultRVO = new ResultRVO();
			applicationContext.getAutowireCapableBeanFactory().autowireBean(resultRVO);
			resultRVO.fill(eos, all, count, jsonConfig, this, dataLanguageUID);
			final JsonObjectBuilder json = resultRVO.getJSONObjectBuilder();

			EntityPermission permission = Rest.getEntityPermission(info.getEntity());
			boolean canCreateBo = info.getMasterMeta().isEditable() && permission.isInsertAllowed() && Rest.canAccessEntityWithLayout(info.getEntity());
			json.add("canCreateBo", canCreateBo);

			return json;

		} catch (Exception ex) {
			throw new NuclosWebException(ex, info.getEntity());
		}
	}


	/**
	 * see ReportExportController.export(...)
	 */
	protected ResultListExportRVO getBoListExport(
			String boMetaId,
			String format,
			boolean pageOrientationLandscape,
			boolean isColumnScaled,
			JsonObject queryContext
	) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);
		FilterJ filter = getFilterJ(queryContext, Long.MAX_VALUE);
		boolean bAllFields = filter.showAllFields(); //"all".equalsIgnoreCase(filter.getFields());

		try {
			Collection<FieldMeta<?>> fields = bAllFields ? Rest.getAllEntityFieldsByEntityIncludingVersion(getBOMeta().getUID()).values() :
					Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());

			CollectableSearchExpression clctexpr = filter.getSearchExpression(getBOMeta().getUID(), fields);

			ResultListExportRVO.OutputFile outputFile = Rest.facade().exportBoList(
					boMetaId,
					null,
					null, format,
					pageOrientationLandscape,
					isColumnScaled,
					fields,
					clctexpr,
					filter
			);

			ResultListExportRVO result = new ResultListExportRVO(this);
			result.setFile(outputFile);

			return result;
		} catch (Exception ex) {
			if (ex instanceof NuclosWebException) {
				throw (NuclosWebException) ex;
			}
			throw new NuclosWebException(ex, info.getEntity());
		}
	}

	protected JsonObjectBuilder getDependenceList(
			final String boMetaId,
			final RecursiveDependency dependency,
			final DependenceBoLinksFactory linksFactory,
			final String sortString,
			final JsonObject queryContext) {
		final UID parentEntityUid = Rest.translateFqn(E.ENTITY, boMetaId);

		try {
			final EntityMeta<?> parentEntityMeta = Rest.getEntity(parentEntityUid);

			if (boMetaId.equals(dependency.getReferenceAttributeFqn())) {
				return getBoList(boMetaId, queryContext);
			}

			final SessionEntityContext info = checkPermissionForSubform(
					parentEntityMeta,
					dependency
			);

			return subloadPK(
					info,
					dependency,
					linksFactory,
					sortString,
					queryContext
			);

		} catch (Exception cbe) {
			throw NuclosWebException.wrap(cbe, parentEntityUid);
		}
	}

	protected JsonObjectBuilder countDependenceList(
			final String boMetaId,
			final RecursiveDependency dependency,
			final DependenceBoLinksFactory linksFactory,
			final JsonObject queryContext) {
		try {
			final EntityMeta<?> parentEntityMeta = Rest.getEntity(Rest.translateFqn(E.ENTITY, boMetaId));

			final SessionEntityContext info = checkPermissionForSubform(
					parentEntityMeta,
					dependency
			);

			return subcountPK(
					info,
					dependency,
					linksFactory,
					queryContext
			);

		} catch (Exception cbe) {
			throw NuclosWebException.wrap(cbe, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}

	protected ResultListExportRVO exportDependenceList(
			String boMetaId,
			String reffield,
			String fk,
			DependenceBoLinksFactory linksFactory,
			String sortString,
			JsonObject queryContext,
			String outputFormat,
			boolean pageOrientationLandscape,
			boolean isColumnScaled
	) {

		UID refFieldUid = Rest.translateFqn(E.ENTITYFIELD, reffield);
		UID parentEntityUid = Rest.translateFqn(E.ENTITY, boMetaId);
		try {
			FieldMeta<?> refFieldMeta = Rest.getEntityField(refFieldUid);
			EntityMeta<?> subformMeta = Rest.getEntity(refFieldMeta.getEntity());
			EntityMeta<?> parentEntityMeta = Rest.getEntity(parentEntityUid);

			SessionEntityContext sessionEntityContext = checkPermissionForSubform(parentEntityMeta, refFieldMeta, fk, true);
			// TODO checkPermissionForSubform sets parentEntityMeta to WebContext (which per se is suboptimal) but subformMeta is needed in FilterJ
			this.setBOMeta(subformMeta);

			FilterJ filter = getFilterJ(queryContext, 5000L);

			boolean bAllFields = filter.showAllFields();
			Collection<FieldMeta<?>> fields = bAllFields ? Rest.getAllEntityFieldsByEntityIncludingVersion(subformMeta.getUID()).values() :
					Rest.getFieldsFromQueryAttributesAndSystem(subformMeta.getUID(), filter.getAttributes());
			Collection<UID> fieldUids = new ArrayList<>();

			List<FieldMeta<?>> selectedFields = new ArrayList<>();

			if (filter.getAttributes() != null) {
				for (String attribute : filter.getAttributes().split(",")) {
					for (FieldMeta<?> fm : fields) {
						if (!fm.isHidden() && Rest.translateFqn(E.ENTITYFIELD, attribute).equals(fm.getUID())) {
							selectedFields.add(fm);
						}
					}
				}
			} else {
				for (FieldMeta<?> fm : fields) {
					if (fm.isHidden()
							|| fm.getUID().equals(SF.CREATEDBY.getUID(fm.getEntity()))
							|| fm.getUID().equals(SF.CREATEDAT.getUID(fm.getEntity()))
							|| fm.getUID().equals(SF.CHANGEDBY.getUID(fm.getEntity()))
							|| fm.getUID().equals(SF.CHANGEDAT.getUID(fm.getEntity()))
							|| fm.getUID().equals(SF.VERSION.getUID(fm.getEntity()))
							|| fm.getUID().equals(SF.STATEICON.getUID(fm.getEntity()))
							|| fm.getDbColumn().equalsIgnoreCase(SF.PK_ID.getDbColumn())
							) {
						continue;
					}

					selectedFields.add(fm);
					fieldUids.add(fm.getUID());
				}
			}

			UID subform = subformMeta.getUID();
			final CollectableSearchExpression clctexpr = filter.getSearchExpression(subform, metaProvider.getAllEntityFieldsByEntity(subform).values());

			Object relatedPK = subformMeta.isUidEntity() ? UID.parseUID(fk) : Long.valueOf(fk);
//			final CollectableSearchCondition cond = SearchConditionUtils.newPkComparison(refFieldUid, ComparisonOperator.EQUAL, relatedPK);
//			clctexpr.setSearchCondition(cond);
			ResultListExportRVO.OutputFile outputFile = Rest.facade().exportBoList(boMetaId,
					sessionEntityContext,
					relatedPK,
					outputFormat,
					pageOrientationLandscape,
					isColumnScaled,
					selectedFields,
					clctexpr,
					filter);

			ResultListExportRVO result = new ResultListExportRVO(this);
			result.setFile(outputFile);

			return result;
		} catch (Exception cbe) {
			throw NuclosWebException.wrap(cbe, refFieldUid);
		}
	}

	private <PK> JsonObjectBuilder subcountPK(
			final SessionEntityContext info,
			final RecursiveDependency dependency,
			final DependenceBoLinksFactory linksFactory,
			final JsonObject queryContext) throws CommonBusinessException {

		EntityMeta<?> subformMeta = Rest.getEntity(info.getReffieldMeta().getEntity());
		EntityMeta<Object> parentMeta = Rest.getEntity(info.getReffieldMeta().getForeignEntity());
		String boType = null;
		if (subformMeta instanceof E._Generalsearchdocument) {
			boType = "document";
		}
		final String parentRecordId = dependency.getParent().getRecordId();

		FilterJ filter = getFilterJ(queryContext, 5000L);

		PK fkpk = (PK) (parentMeta.isUidEntity()
				? Rest.translateFqn(parentMeta, parentRecordId)
				: Long.parseLong(parentRecordId));

		final CollectableSearchExpression filterExpr = filter.getSearchExpression(
				subformMeta.getUID(),
				metaProvider.getAllEntityFieldsByEntity(subformMeta.getUID()).values()
		);

		Long total = Rest.facade().getSubformDataCount(
				subformMeta.getUID(),
				info.getReffieldMeta().getUID(),
				fkpk,
				filterExpr
		);

		final UID dataLanguageUID = getSessionContext().getDataLanguageUID();
		final ResultRVO resultRVO = new ResultRVO();
		applicationContext.getAutowireCapableBeanFactory().autowireBean(resultRVO);

		resultRVO.fill(Collections.emptyList(), true, total, filter.getJsonConfig(true), this, dataLanguageUID, boType, linksFactory, true);

		return resultRVO.getJSONObjectBuilder();
	}

	private <PK> JsonObjectBuilder subloadPK(
			final SessionEntityContext info,
			final RecursiveDependency dependency,
			final DependenceBoLinksFactory linksFactory,
			final String sortString,
			final JsonObject queryContext) throws CommonBusinessException {

		EntityMeta<?> subformMeta = Rest.getEntity(info.getReffieldMeta().getEntity());
		EntityMeta<Object> parentMeta = Rest.getEntity(info.getReffieldMeta().getForeignEntity());
		String boType = null;
		if (subformMeta instanceof E._Generalsearchdocument) {
			boType = "document";
		}
		final String parentRecordId = dependency.getParent().getRecordId();

		FilterJ filter = getFilterJ(queryContext, 5000L);

		boolean bAllFields = filter.showAllFields();
		Collection<FieldMeta<?>> fields = bAllFields ? Rest.getAllEntityFieldsByEntityIncludingVersion(subformMeta.getUID()).values() :
				Rest.getFieldsFromQueryAttributesAndSystem(subformMeta.getUID(), filter.getAttributes());

		Collection<UID> notAllowed = getNotReadAllowedFieldsForSubform(subformMeta, info.getUsage());

		Collection<UID> fieldUids = new ArrayList<>();
		for (FieldMeta<?> fMeta : fields) {
			if (!notAllowed.contains(fMeta.getUID())) {
				fieldUids.add(fMeta.getUID());
			}
		}

		Boolean all;
		Collection<EntityObjectVO<PK>> eos = null;
		Long total = null;
		if (Rest.facade().isTemporaryId(dependency.getParent().getRecordId())) {
			EntityObjectVO<?> temporaryObject = Rest.facade().getTemporaryObject(dependency.getParent().getRecordId());
			@SuppressWarnings("unchecked")
			Set<IDependentKey> alreadyLoaded = (Set<IDependentKey>) temporaryObject.getFieldValue(RValueObject.TEMPORARY_DEPENDENTS_ALREADY_LOADED_FIELD);
			IDependentKey dependentKey = DependentDataMap.createDependentKey(info.getReffieldMeta());
			if (alreadyLoaded.contains(dependentKey)) {
				throw new NuclosWebException(Response.Status.NOT_FOUND);
			}
			eos = temporaryObject.getDependents().getDataPk(dependentKey);
			alreadyLoaded.add(dependentKey);
			all = true;

		} else {
			PK fkpk = (PK) (parentMeta.isUidEntity()
					? Rest.translateFqn(parentMeta, parentRecordId)
					: Long.parseLong(parentRecordId));

			final CollectableSearchExpression filterExpr = filter.getSearchExpression(
					subformMeta.getUID(),
					metaProvider.getAllEntityFieldsByEntity(subformMeta.getUID()).values()
			);

			Long chunksize = filter.getEnd();
			if (chunksize != null && filter.getStart() != null) {
				chunksize = chunksize - filter.getStart() + 1;
			}

			ResultParams resultParams = new ResultParams(
					fieldUids,
					filter.getStart(),
					chunksize + 1,
					true
			);

			if (filter.countTotal()) {
				total = Rest.facade().getSubformDataCount(
						subformMeta.getUID(),
						info.getReffieldMeta().getUID(),
						fkpk,
						filterExpr
				);
				if (total.longValue() == 0l) {
					eos = Collections.emptyList();
				}
			}

			if (eos == null) {
				eos = Rest.facade().getSubformData(
						subformMeta.getUID(),
						info.getReffieldMeta().getUID(),
						fkpk,
						filterExpr,
						resultParams
				);
			}

			all = eos.size() <= chunksize;
			if (!all && eos.size() > 0) {
				Object last = CollectionUtils.get(eos, eos.size() - 1);
				eos.remove(last);
			}
		}

		List<EntityObjectVO<PK>> eol = new ArrayList<>(eos);
		if (sortString != null && sortString.length() != 0) {
			List<String> sortFields;
			sortFields = new ArrayList<>();
			Collections.addAll(sortFields, sortString.split(","));

			sortResultList(eol, sortFields);
		}

		JsonBuilderConfiguration jsonConfig = filter.getJsonConfig(true);
		boolean canOpenDetail;
		if (subformMeta.isDynamic()) {
			UID detailEntity = subformMeta.getDetailEntity();
			canOpenDetail = detailEntity != null && isReadAllowedForEntity(subformMeta.getDetailEntity());
		} else {
			canOpenDetail = isReadAllowedForEntity(info.getReffieldMeta().getEntity());
		}
		jsonConfig.setWithDetailLink(canOpenDetail);

		final UID dataLanguageUID = getSessionContext().getDataLanguageUID();
		final ResultRVO resultRVO = new ResultRVO();
		applicationContext.getAutowireCapableBeanFactory().autowireBean(resultRVO);

		if (filter.countTotal() && total == null) {
			total = Integer.valueOf(eol.size()).longValue();
		}

		resultRVO.fill(eol, all, total, jsonConfig, this, dataLanguageUID, boType, linksFactory, true);

		return resultRVO.getJSONObjectBuilder();
	}

	protected JsonObjectBuilder getDependencyDetails(
			String bometa,
			String parentBoId,
			String reffield,
			String subBoId,
			DependenceBoLinksFactory linksFactory
	) {
		UID entityUid = Rest.translateFqn(E.ENTITY, bometa);
		UID reffieldUid = Rest.translateFqn(E.ENTITYFIELD, reffield);

		try {
			IDependentKey dependentKey = DependentDataMap.createDependentKey(reffieldUid);

			EntityObjectVO<?> eo;

			if (Rest.facade().isTemporaryId(parentBoId)) {
				EntityObjectVO<?> temporaryObject = Rest.getTemporaryEntityObjectIfReadAllowed(parentBoId);

				Collection<EntityObjectVO<?>> tempEos = temporaryObject.getDependents().getData(dependentKey);
				for (EntityObjectVO<?> tempEo : tempEos) {
					String temporaryId = tempEo.getFieldValue(RValueObject.TEMPORARY_ID_FIELD, String.class);
					if (subBoId.equals(temporaryId)) {
						break;
					}
				}
			} else {
				// NUCLOS-5983 2)
				checkPermissionForSubform(Rest.getEntity(entityUid), Rest.getEntityField(reffieldUid), parentBoId, true);
			}

			eo = subGetEoAndSetMasterUsage(entityUid, parentBoId, reffieldUid, subBoId);
			UsageProperties up = new UsageProperties(Rest.getUsageCriteriaFromEO(eo), this, metaProvider, preferencesProvider, parameterProvider, false);
			final UID sessionDataLanguageUID = getSessionContext().getDataLanguageUID();
			return new RValueObject(eo, JsonBuilderConfiguration.getFull(), up, linksFactory, sessionDataLanguageUID, jsonFactory, metaProvider, stateCache).getJSONObjectBuilder();

		} catch (CommonBusinessException cbe) {
			throw new NuclosWebException(cbe, reffieldUid);
		}
	}

	protected JsonObjectBuilder getDependencyDetails(
			String boMetaId,
			RecursiveDependency dependency,
			String subBoId,
			DependenceBoLinksFactory linksFactory
	) {
		final UID parentEntityUid = Rest.translateFqn(E.ENTITY, boMetaId);

		try {
			final EntityMeta<?> parentEntityMeta = Rest.getEntity(parentEntityUid);

			final SessionEntityContext info = checkPermissionForSubform(
					parentEntityMeta,
					dependency
			);

			return getDependencyDetails(
					dependency.getParent().getReferenceAttributeFqn()
							.substring(0, dependency.getParent().getReferenceAttributeFqn().lastIndexOf("_")),
					dependency.getParent().getRecordId(),
					dependency.getReferenceAttributeFqn(),
					subBoId,
					linksFactory
			);

		} catch (Exception cbe) {
			throw NuclosWebException.wrap(cbe, parentEntityUid);
		}
	}

	private EntityObjectVO<?> subGetEoAndSetMasterUsage(
			UID parentUid,
			String parentBoId,
			UID reffieldUid,
			String pk
	) throws CommonBusinessException {
		SessionEntityContext subInfo = checkPermissionForReference(parentUid, parentBoId, reffieldUid);
		EntityMeta<?> subformMeta = Rest.getEntity(subInfo.getReffieldMeta().getEntity());
		EntityObjectVO<?> eo;
		if (subformMeta.isUidEntity()) {
			// TODO
			EntityMeta<?> TODO = null;
			eo = Rest.facade().get(subformMeta.getUID(), Rest.translateFqn(TODO, pk), false);
		} else {
			eo = Rest.facade().get(subformMeta.getUID(), Long.parseLong(pk), false);
		}

		if (subInfo.isUidEntity()) {
			UID masterPK = eo.getFieldUid(subInfo.getReffieldMeta().getUID());
			pk = Rest.translateUid(E.ENTITYFIELD, masterPK);
		} else {
			Long masterPK = eo.getFieldId(subInfo.getReffieldMeta().getUID());
			pk = masterPK.toString();
		}
		SessionEntityContext info = checkPermission(E.ENTITY, Rest.translateUid(E.ENTITY, subInfo.getEntity()), pk);

		Collection<UID> fields2Clear = getNotReadAllowedFieldsForSubform(subformMeta, info.getUsage());
		eo.clearFields(fields2Clear);

		return eo;
	}

	// TODO: NUCLOS-6140 Centralize
	// NUCLOS-6134
	private Collection<UID> getNotReadAllowedFieldsForSubform(EntityMeta<?> subformMeta, UsageCriteria uc) {
		Collection<UID> notAllowedFields = new HashSet<>();
		SubformPermission subformPermission = Rest.facade().getSubformPermission(subformMeta.getUID(), uc);
		for (FieldMeta<?> fMeta : subformMeta.getFields()) {
			if (subformPermission == null) {
				notAllowedFields.add(fMeta.getUID());
			}
			// NUCLOS-6183 Do not add Systemfields
			else if (!fMeta.isSystemField() && !subformPermission.getGroupPermission(fMeta.getFieldGroup()).includesReading()) {
				notAllowedFields.add(fMeta.getUID());
			}
		}
		return notAllowedFields;
	}

	/**
	 * evaluates a field expression like '<b>uid{Li4MRC8s0YNQkrVPW10E}</b> uid{CgOmkIMnskz4iYXYHpRT}'
	 */
	private <PK> String evaluateExpressionString(UID boMetaId, EntityObjectVO<PK> eo, String expression, List<Pair<String, Object>> attrbs) {

		ForeignEntityFieldUIDParser uidParser = new ForeignEntityFieldUIDParser(expression, null, null);
		Iterator<IFieldUIDRef> itParser = uidParser.iterator();

		final StringBuilder result = new StringBuilder(expression.length());
		while (itParser.hasNext()) {
			IFieldUIDRef part = itParser.next();
			if (part.isConstant()) {
				result.append(part.getConstant());
			} else {
				UID fieldUID = part.getUID();
				Object fieldValue = eo != null ? eo.getFieldValue(fieldUID) : "dummy";
				if (fieldValue == null) {
					fieldUID = Rest.translateFqn(E.ENTITYFIELD, fieldUID.getString());//
					fieldValue = fieldUID != null ? eo.getFieldValue(fieldUID) : null;
				}

				if (fieldUID != null && attrbs != null) {
					attrbs.add(Pair.makePair(Rest.translateUid(E.ENTITYFIELD, fieldUID), fieldValue));
				}

				if (fieldValue != null) {
					if (fieldValue instanceof NuclosImage) {

						String imageString = "<img src='" + this.getRestURI("imageresource", Rest.translateUid(E.ENTITY, boMetaId), eo.getPrimaryKey().toString(), Rest.translateUid(E.ENTITYFIELD, fieldUID)) + "?sessionid=" + getSessionId() + "'>";
						result.append(imageString);

					} else {
						result.append(StringUtils.xmlEncode(fieldValue.toString()));
					}
				}
			}
		}

		return result.toString();
	}

	private JsonArrayBuilder vlpdata(ReferenceListParameterJson paramJson) {
		try {
			WebValueListProvider wvlp = paramJson.getWebValueListProviderIfAny(fqnCache);
			String pk = paramJson.getPK();

			List<CollectableField> lstCF = Rest.facade().getVLPData(
					null,
					wvlp,
					pk,
					paramJson.getQuickSearchInput(),
					DEFAULT_RESULT_LIMIT,
					false,
					UID.parseUID(paramJson.getMandatorId())
			);
			return jsonFactory.buildJsonArray(lstCF, this);

		} catch (Exception ex) {
			throw new NuclosWebException(ex, null);
		}
	}

	protected final String evaluateExpressionString(String boMetaId, String boId, String expression) {

		try {

			SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);
			EntityObjectVO eo = Rest.facade().get(info.getEntity(), Long.parseLong(boId), true);

			ForeignEntityFieldUIDParser uidParser = new ForeignEntityFieldUIDParser(expression, null, null);
			Iterator<IFieldUIDRef> itParser = uidParser.iterator();

			final StringBuilder result = new StringBuilder(expression.length());
			while (itParser.hasNext()) {
				IFieldUIDRef part = itParser.next();
				if (part.isConstant()) {
					result.append(part.getConstant());
				} else {
					UID fieldUID = part.getUID();
					Object fieldValue = eo.getFieldValue(fieldUID);
					if (fieldValue != null) {
						if (fieldValue instanceof NuclosImage) {

							/*
							NuclosImage nuclosImage = (NuclosImage)fieldValue;
							byte[] imageBytes = nuclosImage.getContent();
							if (imageBytes != null)  {
								String base64Image =  new String(Base64.encodeBase64(imageBytes));
								result.append(base64Image);
							}
							*/

							String imageString = "<img src='" + this.getRestURI("imageresource", boMetaId, boId, fieldUID.toString()) + "?sessionid=" + getSessionId() + "'>";
							result.append(imageString);

						} else {
							result.append(StringUtils.xmlEncode(fieldValue.toString()));
						}
					}
				}
			}

			return result.toString();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}


	protected final byte[] getImageResource(String boMetaId, String boId, String fieldUID) {
		try {

			SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);
			EntityObjectVO eo = Rest.facade().get(info.getEntity(), Long.parseLong(boId), true);

			Object fieldValue = eo.getFieldValue(Rest.translateFqn(E.ENTITYFIELD, fieldUID));
			if (fieldValue != null) {
				if (fieldValue instanceof NuclosImage) {
					NuclosImage nuclosImage = (NuclosImage) fieldValue;
					return nuclosImage.getContent();
				}
			}

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
		return null;
	}

	/**
	 * Queries only the changedAt timestamp for the given entity object from the DB.
	 */
	protected <PK> InternalTimestamp getChangedAt(final EntityMeta<?> entity, final String boId) {
		Object id;
		EntityObjectVO<?> data = null;

		try {
			if (boId.startsWith("temp_")) {
				data = Rest.facade().getTemporaryObject(boId);
			} else {
				if (entity.isUidEntity()) {
					id = Rest.translateFqn(entity, boId);
				} else {
					id = Long.parseLong(boId);
				}

				FieldMeta<InternalTimestamp> fieldMeta = SF.CHANGEDAT.getMetaData(entity);
				data = Rest.facade().getDataForField(id, fieldMeta);
			}
		} catch (Exception e) {
			throw new NuclosWebException(e, entity.getDetailEntity());
		}

		return Optional.ofNullable(data)
				.map((d) -> d.getFieldValue(SF.CHANGEDAT))
				.orElse(new InternalTimestamp(System.currentTimeMillis()));
	}

	private void extractReceiverId(JsonObject object) {
		if (object.containsKey(RECEIVER_ID)) {
			Integer receiverId = object.getInt(RECEIVER_ID, 0);
			LOG.debug("RequestContext-->receiverId={}", receiverId);
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
			request.setAttribute(RECEIVER_ID, receiverId);
		} else {
			LOG.debug("RequestContext-->receiverId=NULL");
		}
	}

	protected JsonArrayBuilder referencelistarray(String field) {
		UID fieldUid = checkPermissionForReferenceField(Rest.translateFqn(E.ENTITYFIELD, field));
		try {
			ReferenceListParameterJson paramJson = new ReferenceListParameterJson(getQueryParameters(), this);
			WebValueListProvider wvlp = paramJson.getWebValueListProviderIfAny(fqnCache);
			String pk = paramJson.getPK();

			long chunkSize = DEFAULT_RESULT_LIMIT;
			if (paramJson.getChunkSize() != null && paramJson.getChunkSize() > 0) {
				chunkSize = paramJson.getChunkSize();
			}

			UID entityUID = metaProvider.getEntityField(fieldUid).getEntity();
			// NUCLOS-8827 Check for process and use the system-named-vlp "process", no matter what.
			if (fieldUid.equals(SFE.PROCESS.getUID(entityUID))) {
				wvlp = new WebValueListProvider(ValuelistProviderVO.Type.NAMED.getType(), ValueListProviderType.PROCESS.getType());
			}

			List<CollectableField> lstCVIF = Rest.facade().getReferenceList(
					fieldUid,
					paramJson.getQuickSearchInput(),
					chunkSize,
					wvlp,
					pk,
					UID.parseUID(paramJson.getMandatorId())
			);
			return jsonFactory.buildJsonArray(lstCVIF, this);

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITYFIELD, field));
		}
	}

	private static <PK> void sortResultList(List<EntityObjectVO<PK>> resultList, final List<String> sortFieldList) {
		if (sortFieldList.isEmpty()) {
			return;
		}

		resultList.sort((o1, o2) -> {
			for (String sortFieldString : sortFieldList) {
				String[] sfArray = sortFieldString.trim().split(":");

				UID sortField = Rest.translateFqn(E.ENTITYFIELD, sfArray[0].trim());
				boolean asc = sfArray.length <= 1 || "asc".equals(sfArray[1].trim().toLowerCase());
				String s1 = o1.getFieldValue(sortField) != null ? o1.getFieldValue(sortField).toString() : "";
				String s2 = o2.getFieldValue(sortField) != null ? o2.getFieldValue(sortField).toString() : "";
				if (s1.compareTo(s2) != 0) {
					return asc ? s1.compareTo(s2) : s2.compareTo(s1);
				}
			}
			return 0;
		});
	}

	// NUCLOS-8974, NUCLOS-8983
	// In most cases the title is nothing than a single attribute (e.g. "name").
	private UID getSimpleTitleField(UID entity) {
		final EntityMeta<?> meta = metaProvider.getEntity(entity);
		String resource = springLocaleDelegate.getResourceById(springLocaleDelegate.getUserLocaleInfo(),
				meta.getLocaleResourceIdForTreeView());
		if (resource != null && resource.startsWith("uid{") && resource.endsWith("}")) {
			resource = resource.substring(4, resource.length() - 1);
			if (!org.apache.commons.lang.StringUtils.containsAny(resource, "{}()!")) {
				for (FieldMeta<?> field : meta.getFields()) {
					if (field.getUID().getString().equals(resource)) {
						if (field.getDataType().equals("java.lang.String")) {
							return field.getUID();
						}
						return null;
					}
				}
			}
		}
		return null;
	}

	protected JsonArrayBuilder getMatrixColumns(String entity_x, JsonObject data, boolean bCamelCase, String boId) throws CommonBusinessException {
		String fieldVlp = bCamelCase ? "entityXVlpId" : "entity_x_vlp_id";
		String entity_x_vlp_id = data.containsKey(fieldVlp) ? data.getString(fieldVlp) : null;

		String fieldSorting = bCamelCase ? "entityXSortingFields" : "entity_x_sorting_fields";
		String entity_x_sorting_fields = data.containsKey(fieldSorting) ? data.getString(fieldSorting) : null;

		String fieldHidden = bCamelCase ? "entityXHiddenField" : "entity_x_hidden_field";
		String entity_x_hidden_field = data.containsKey(fieldHidden) ? data.getString(fieldHidden) : null;

		String fieldXHeader = bCamelCase ? "entityXHeader" : "entity_x_header";
		String entity_x_header = data.containsKey(fieldXHeader) ? data.getString(fieldXHeader) : null;

		String fieldX = bCamelCase ? "entityFieldX" : "entity_field_X";
		String entity_field_x = data.containsKey(fieldX) ? data.getString(fieldX) : null;

		String fieldCategorie = bCamelCase ? "entityFieldCategorie" : "entity_field_categorie";
		String entity_field_categorie = data.containsKey(fieldCategorie) ? data.getString(fieldCategorie) : null;

		UID entityX = Rest.translateFqn(E.ENTITY, entity_x);
		List<EntityObjectVO<Long>> xAxisBoList = new ArrayList<>();
		IExtendedEntityObjectProcessor<Long> proc = NucletDalProvider.getInstance().getEntityObjectProcessor(entityX);
		// filter columns by VLP

		if (entity_x_vlp_id != null && entity_x_vlp_id.length() != 0) { // with VLP

			String entity_x_vlp_idfieldname = data.getString(bCamelCase ? "entityXVlpIdfieldname" : "entity_x_vlp_idfieldname");
			String entity_x_vlp_fieldname = data.getString(bCamelCase ? "entityXVlpFieldname" : "entity_x_vlp_fieldname");
			String entity_x_vlp_reference_param_name = data.getString(bCamelCase ? "entityXVlpReferenceParamName" : "entity_x_vlp_reference_param_name");

			MultivaluedMap<String, String> vlpInputParameter = new MultivaluedHashMap<>();
			vlpInputParameter.add("vlpvalue", "uid{" + entity_x_vlp_id + "}");
			vlpInputParameter.add("vlptype", "ds");
			vlpInputParameter.add(ValuelistProviderVO.DATASOURCE_IDFIELD, entity_x_vlp_idfieldname);
			vlpInputParameter.add(ValuelistProviderVO.DATASOURCE_NAMEFIELD, entity_x_vlp_fieldname);
			vlpInputParameter.add(entity_x_vlp_reference_param_name, boId);

			ReferenceListParameterJson paramJson = new ReferenceListParameterJson(vlpInputParameter);
			//TODO: Sling out Build()
			JsonArray xArray = vlpdata(paramJson).build();

			Set<Long> vlpIds = new HashSet<>();
			List<Long> boIds = new ArrayList<>();

			for (JsonValue boJsonValue : xArray) {
				JsonObject jo = (JsonObject) boJsonValue;
				Long id = jo.containsKey("id") ? jo.getJsonNumber("id").longValue()
						: jo.getJsonNumber("boId").longValue();
				if (!vlpIds.contains(id)) {
					vlpIds.add(id);
					boIds.add(id);
				}
			}

			xAxisBoList.addAll(proc.getByPrimaryKeys(boIds));

		} else { // without VLP

			xAxisBoList.addAll(proc.getBySearchExpression(CollectableSearchExpression.TRUE_SEARCH_EXPR));
		}

		final List<String> sortFieldList = getSortedFieldListFqn(entity_x_sorting_fields, entity_x);

		sortResultList(xAxisBoList, sortFieldList);

		boolean evaluateHeader = !StringUtils.isNullOrEmpty(entity_x_header);

		JsonArrayBuilder xAxisJsonBoList = Json.createArrayBuilder();

		UID simpleTitleField = getSimpleTitleField(entityX);
		for (EntityObjectVO<Long> eovo : xAxisBoList) {

			JsonObjectBuilder dummy = Json.createObjectBuilder();
			dummy.add("boId", eovo.getPrimaryKey());
			if (evaluateHeader) {
				String title = evaluateExpressionString(Rest.translateFqn(E.ENTITY, entity_x), eovo, entity_x_header, null);
				dummy.add("_title", title);
			} else {
				Object title;
				if (!StringUtils.isNullOrEmpty(entity_field_categorie) && !StringUtils.isNullOrEmpty(entity_field_x)) {
					title = eovo.getFieldValue(Rest.translateFqn(E.ENTITYFIELD, entity_field_x));
				} else if (simpleTitleField != null) {
					title = eovo.getFieldValue(simpleTitleField);
				} else {
					title = Rest.getTreeViewLabel(eovo.getFieldValues(), eovo.getDalEntity());
				}
				dummy.add("_title", Objects.toString(title, ""));
				UID hiddenUID = null;
				if (!StringUtils.isNullOrEmpty(entity_x_hidden_field)) {
					for (FieldMeta<?> fm : metaProvider.getAllEntityFieldsByEntity(entityX).values()) {
						if (fm.toString().equals(entity_x_hidden_field)) {
							hiddenUID = fm.getUID();
							break;
						}
					}
					Object bHidden = hiddenUID != null ? eovo.getFieldValue(hiddenUID) : null;
					if (bHidden instanceof Boolean) {
						dummy.add("_hidden", (Boolean)bHidden);
					} else if (bHidden instanceof Number) {
						dummy.add("_hidden", ((Number)bHidden).intValue() != 0);
					}
				}
			}

			if (!StringUtils.isNullOrEmpty(entity_field_categorie)) {
				Object group = eovo.getFieldValue(Rest.translateFqn(E.ENTITYFIELD, entity_field_categorie));;
				dummy.add("_group", Objects.toString(group, ""));
			}

			xAxisJsonBoList.add(dummy);
		}

		return xAxisJsonBoList;
	}

	private static List<String> getSortedFieldListFqn(String sortingFields, String entity) {
		final List<String> sortFieldList = new ArrayList<>();

		// make full qualified name of x sorting fields
		if (!StringUtils.isNullOrEmpty(sortingFields)) {
			String[] sa = sortingFields.split(",");
			for (final String aSa : sa) {
				sortFieldList.add(entity + "_" + aSa);
			}
		}

		return sortFieldList;
	}

	protected JsonArrayBuilder getYMatrixColumnsList(JsonObject data, boolean bCamelCase) {
		String fieldYHeader = bCamelCase ? "entityYHeader" : "entity_y_header";
		String entity_y_header = data.containsKey(fieldYHeader) ? data.getString(fieldYHeader) : null;

		JsonArrayBuilder yMatrixColumnsList = Json.createArrayBuilder();

		if (StringUtils.isNullOrEmpty(entity_y_header)) {
			return yMatrixColumnsList;
		}

		List<Pair<String, Object>> attrs = new ArrayList<>();
		evaluateExpressionString(null, null, entity_y_header, attrs);
		for (Pair<String, Object> attr : attrs) {
			FieldMeta<?> f = metaProvider.getEntityField(Rest.translateFqn(E.ENTITYFIELD, attr.getX()));
			JsonObjectBuilder json = Json.createObjectBuilder();
			json.add("boAttrId", attr.getX());
			json.add("value", springLocaleDelegate.getLabelFromMetaFieldDataVO(f));
			json.add("type", f.getJavaClass().getSimpleName());
			yMatrixColumnsList.add(json);
		}

		return yMatrixColumnsList;
	}

	protected JsonArrayBuilder getMatrixRows(String entity_y, JsonObject data, boolean bCamelCase, String boId, List<EntityObjectVO<Long>> lstEos) throws CommonBusinessException {
		JsonArrayBuilder yAxisBoList = Json.createArrayBuilder();
		if (StringUtils.isNullOrEmpty(boId) || "null".equals(boId)) {
			return yAxisBoList;
		}

		String entity_y_parent_field = data.getString(bCamelCase ? "entityYParentField" : "entity_y_parent_field");

		String fieldYSorting = bCamelCase ? "entityYSortingFields" : "entity_y_sorting_fields";
		String entity_y_sorting_fields = data.containsKey(fieldYSorting) ? data.getString(fieldYSorting) : null;

		String fieldYHidden = bCamelCase ? "entityYHiddenField" : "entity_y_hidden_field";
		String entity_y_hidden_field = data.containsKey(fieldYHidden) ? data.getString(fieldYHidden) : null;

		String fieldYHeader = bCamelCase ? "entityYHeader" : "entity_y_header";
		String entity_y_header = data.containsKey(fieldYHeader) ? data.getString(fieldYHeader) : null;

		UID entityY = Rest.translateFqn(E.ENTITY, entity_y);
		UID entityYRefField = Rest.translateFqn(E.ENTITYFIELD, entity_y_parent_field);
		Long lBoId = Long.parseLong(boId);

		Collection<EntityObjectVO<Long>> eos = Rest.facade().getDependentData(entityY, entityYRefField, lBoId);
		lstEos.clear();
		lstEos.addAll(eos);

		List<String> lstSorting = getSortedFieldListFqn(entity_y_sorting_fields, entity_y);
		sortResultList(lstEos, lstSorting);

		UID simpleTitleField = getSimpleTitleField(entityY);

		UID nuclosRowColorUID = null, hiddenUID = null;
		for (FieldMeta<?> fm : metaProvider.getAllEntityFieldsByEntity(entityY).values()) {
			if (fm.isNuclosRowColor()) {
				nuclosRowColorUID = fm.getUID();
				if (hiddenUID != null) {
					break;
				}
			}
			if (fm.toString().equals(entity_y_hidden_field)) {
				hiddenUID = fm.getUID();
				if (nuclosRowColorUID != null) {
					break;
				}
			}
		}

		for (EntityObjectVO<Long> eo : lstEos) {
			Long id = eo.getPrimaryKey();

			JsonObjectBuilder dummy = Json.createObjectBuilder();
			dummy.add("boId", id);

			if (nuclosRowColorUID != null && eo.getFieldValue(nuclosRowColorUID) instanceof String) {
				dummy.add("rowColor", (String)eo.getFieldValue(nuclosRowColorUID));
			}

			if (hiddenUID != null && eo.getFieldValue(hiddenUID) instanceof Boolean) {
				dummy.add("rowHidden", (Boolean)eo.getFieldValue(hiddenUID));
			}

			if (!StringUtils.isNullOrEmpty(entity_y_header)) {
				List<Pair<String, Object>> attrs = new ArrayList<>();
				dummy.add("evaluatedHeaderTitle", evaluateExpressionString(entityY, eo, entity_y_header, attrs));
				JsonArrayBuilder xAxisColumns = Json.createArrayBuilder();
				for (Pair<String, Object> attr : attrs) {
					JsonObjectBuilder json = Json.createObjectBuilder();
					json.add("boAttrId", attr.getX());
					if (attr.getY() == null) {
						json.addNull("value");
					} else if (attr.getY() instanceof Boolean) {
						json.add("value", (Boolean)attr.getY());
					} else if (attr.getY() instanceof Double) {
						json.add("value", (Double) attr.getY());
					} else if (attr.getY() instanceof Number) {
						json.add("value", ((Number) attr.getY()).longValue());
					} else {
						json.add("value", attr.getY().toString());
					}
					xAxisColumns.add(json);
				}
				dummy.add("yAxisColumns", xAxisColumns);
			} else {
				String title;
				if (simpleTitleField != null) {
					title = (String)eo.getFieldValue(simpleTitleField);
				} else {
					title = Rest.getTreeViewLabel(eo.getFieldValues(), eo.getDalEntity());
				}
				dummy.add("evaluatedHeaderTitle", title);
			}

			yAxisBoList.add(dummy);
		}

		return yAxisBoList;
	}

	public static boolean isImageBytes(final Object data) {
		try {
			if (data instanceof byte[]) {
				BufferedImage image = ImageIO.read(new ByteArrayInputStream((byte[]) data));
				return image != null;
			}
		} catch (IOException e) {
			LOG.trace("Not an image: {}", data);
		}

		return false;
	}

	@Override
	public IRigidMetaProvider getMetaProvider() {
		return metaProv;
	}
}
