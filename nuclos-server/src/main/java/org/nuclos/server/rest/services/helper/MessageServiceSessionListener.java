//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.helper;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageServiceSessionListener implements HttpSessionListener {

	private static final Logger LOG = LoggerFactory.getLogger(MessageServiceSessionListener.class);
	
	//SessionID-ReceiverId-MessageServiceThread
	private static Map<String, Map<Integer, MessageServiceThread>> sessionMessageServiceThreads = new ConcurrentHashMap<String, Map<Integer, MessageServiceThread>>();
	
	@Override
	public void sessionCreated(HttpSessionEvent session) {
		String sessionId = session.getSession().getId();
		LOG.debug("sessionCreated with sessionId={}", sessionId);
	}
	
	@Override
	public void sessionDestroyed(HttpSessionEvent session) {
		String sessionId = session.getSession().getId();
		LOG.debug("sessionDestroyed with sessionId={}", sessionId);
		Map<Integer, MessageServiceThread> threads = sessionMessageServiceThreads.get(sessionId);
		if(threads!=null){
			Set<Entry<Integer, MessageServiceThread>> receiverIds = threads.entrySet();
			for (Entry<Integer, MessageServiceThread> entry : receiverIds) {
				MessageServiceThread thread = entry.getValue();
				LOG.debug("terminate receiverId={}", thread.getReceiverId());
				thread.terminate();
			}
			for (Entry<Integer, MessageServiceThread> entry : receiverIds) {
				Integer receiverId = entry.getKey();
				threads.remove(receiverId);
			}
		}
		sessionMessageServiceThreads.remove(sessionId);
	}

	public static void addMessageServiceThread(String sessionId, MessageServiceThread thread){
		LOG.debug("session addMessageServiceThread with sessionId={} and receiverId={}",
		          sessionId, thread.getReceiverId());
		if(sessionMessageServiceThreads.get(sessionId)==null){
			sessionMessageServiceThreads.put(sessionId, new ConcurrentHashMap<Integer, MessageServiceThread>());
		}
		sessionMessageServiceThreads.get(sessionId).put(thread.getReceiverId(), thread);
	}
	
}
