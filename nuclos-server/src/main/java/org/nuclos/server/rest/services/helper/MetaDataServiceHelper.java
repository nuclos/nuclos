package org.nuclos.server.rest.services.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.ProfileUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.PreferenceVO;
import org.nuclos.common.preferences.ColumnPreferences;
import org.nuclos.common.preferences.ColumnSortingPreferences;
import org.nuclos.common.preferences.IPreferencesProvider;
import org.nuclos.common.preferences.Preference;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.resource.ResourceResolver;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.schema.rest.EntityMetaBase;
import org.nuclos.schema.rest.EntityMetaBaseLinks;
import org.nuclos.schema.rest.RestLink;
import org.nuclos.schema.rest.TaskEntityMeta;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ejb3.PreferencesFacadeLocal;
import org.nuclos.server.customcomp.valueobject.CustomComponentVO;
import org.nuclos.server.dal.processor.nuclos.IPreferenceProcessor;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.BoMetaOverview;
import org.nuclos.server.rest.misc.IMenuEntry;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.ReferenceFieldMeta;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.rest.misc.RestLinks.Verbs;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.misc.TaskOverview;
import org.nuclos.server.rest.services.rvo.MenuPreferenceRVO;
import org.nuclos.server.rest.services.rvo.MenuPreferenceRVO.MenuItemRVO;
import org.nuclos.server.rest.services.rvo.MenuTreeNodeRVO;
import org.nuclos.server.rest.services.rvo.MenuTreeNodeRVO.TreeNodeBuilder;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;

@Configurable
public class MetaDataServiceHelper extends WebContext {

	@Autowired
	protected ApplicationContext applicationContext;

	@Autowired
	private IPreferenceProcessor prefsProc;

	@Autowired
	private IPreferencesProvider prefsProv;

	@Autowired
	private PreferencesFacadeLocal prefsFacade;

	private static final Logger LOG = LoggerFactory.getLogger(MetaDataServiceHelper.class);

	private Collection<EntityMeta<?>> restMenuSystemEntityWhitelist = Arrays.asList(
			E.USER,
			E.PARAMETER,
			E.NEWS,
			E.IMPORTFILE,
			E.JOBCONTROLLER,
			E.WEBADDON,
			E.CLUSTER_SERVER,
			E.SSOAUTH,
			E.RESTAPIS,
			E.EMAILOUTGOINGSERVER,
			E.EMAILINCOMINGSERVER
	);

	protected List<EntityMetaBase> getBOMetaList() {
		try {
			return getReadableBOMetasAsFlatList(null, false);
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(e, null);
		}
	}


	private PreferenceVO getMenuPreferenceItem() {
		UID user = SecurityCache.getInstance().getUserUid(getUser());
		final List<PreferenceVO> prefs = prefsProc.getByAppTypeUserAndEntity("nuclos", NuclosPreferenceType.MENU.getType(), user, null, null, false);
		if (prefs.size() == 0) {
			return null;
		}
		if (prefs.size() > 1) {
			LOG.warn("Found more than 1 menu preference (" + prefs.size() + ") for user '" + user + "'.");
		}
		return prefs.get(0);
	}


	protected JsonArrayBuilder getMenu() {
		try {
			PreferenceVO pref = getMenuPreferenceItem();

			Set<String> reducedStartmenus = new HashSet<String>();
			if (pref != null) {
				MenuPreferenceRVO mrvo = MenuPreferenceRVO.parse(pref.getJson().toString());
				for (MenuItemRVO mirvo : mrvo.getMenuItems()) {
					if (!mirvo.getIsExpanded()) {
						reducedStartmenus.add(mirvo.getName());
					}
				}
			}
			return getReadableBOMetas(reducedStartmenus, true);
		} catch (Exception e) {
			throw new NuclosWebException(e, null);
		}
	}

	protected MenuTreeNodeRVO getMenuStructure() {
		try {
			PreferenceVO pref = getMenuPreferenceItem();

			Set<String> reducedStartmenus = new HashSet<>();
			if (pref != null) {
				MenuPreferenceRVO mrvo = MenuPreferenceRVO.parse(pref.getJson().toString());
				for (MenuItemRVO mirvo : mrvo.getMenuItems()) {
					if (!mirvo.getIsExpanded()) {
						reducedStartmenus.add(mirvo.getName());
					}
				}
			}
			return getReadableBOMetasMenuStructure(reducedStartmenus);
		} catch (Exception e) {
			throw new NuclosWebException(e, null);
		}
	}

	protected List<TaskEntityMeta> getTaskLists() {
		try {
			return getReadableTasklistMetas();
		} catch (CommonBusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	protected JsonObjectBuilder getBOMetaSelf(String id) {
		//NUCLOS-4221: Sub-forms grant implicitly the privileges for certain entities. Meta-Data is needed and thus the direct check is not possible.
		//validateSession(E.ENTITY, id);

		//TODO: Evaluate if this means a security breach. Until now it doesn't look very dangerous as any request for real data is not possible.
		checkPermission(E.ENTITY, id, null, null, false);
		return jsonFactory.buildJsonObject(getBOMeta(), this);
	}

	protected final JsonObjectBuilder getDependenceMetaSelf(String boMetaId, RecursiveDependency dependency) {
		String reffield = dependency.getReferenceAttributeFqn();

		UID fieldUid = Rest.translateFqn(E.ENTITYFIELD, reffield);

		try {
			FieldMeta<?> fieldMeta = Rest.getEntityField(fieldUid);
			EntityMeta<?> subformMeta = Rest.getEntity(fieldMeta.getEntity());

			EntityMeta<?> baseEntityMeta;
			if (dependency.getParent() != null) {
				boMetaId = dependency.getParent().getReferenceAttributeFqn().substring(0, dependency.getParent().getReferenceAttributeFqn().lastIndexOf("_"));
			}

			UID baseEntityUid = Rest.translateFqn(E.ENTITY, boMetaId);
			baseEntityMeta = Rest.getEntity(baseEntityUid);

			SessionEntityContext info = checkPermissionForSubform(baseEntityMeta, fieldMeta, null, false);

			ReferenceFieldMeta rfMeta = new ReferenceFieldMeta(fieldMeta, subformMeta, info.getMasterMeta());
			return jsonFactory.buildJsonObject(rfMeta, this);

		} catch (Exception cbe) {
			throw new NuclosWebException(cbe, fieldUid);
		}
	}

	/**
	 * TODO consolidate with getReadableBOMetasMenu
	 * TODO: Duplicated code
	 * <p>
	 * at the moment there is
	 * one REST service for the header menu (which is also responsible for tasks - but tasks are also shown in the dashboard)
	 * one REST service for the dashboard menu
	 * - this needs to be refactored
	 *
	 * @deprecated One big mess, see comments!
	 */
	@Deprecated
	private MenuTreeNodeRVO getReadableBOMetasMenuStructure(
			Set<String> reducedStartmenus
	) {
		Set<UID> restMenuEntites = Rest.getRestMenuEntities();

		TreeNodeBuilder treeNodeList = new TreeNodeBuilder(this);
		Set<UID> menuEntities = new HashSet<>();

		final Collection<TasklistDefinition> dynamicTaskLists = Rest.getDynamicTaskLists();
		if (dynamicTaskLists != null && !dynamicTaskLists.isEmpty()) {
			Map<UID, EntityMeta<?>> mapDynamicTasklistEntityMeta = getDynamicTaskListEntityMetaMap();

			for (TasklistDefinition tlDef : dynamicTaskLists) {
				final EntityMeta<?> tasklistEntityMeta = mapDynamicTasklistEntityMeta.get(tlDef.getId());
				if (tasklistEntityMeta != null) {
					final String menu = Rest.getResource(tlDef.getMenupathResourceId());
					final String label = Rest.getResource(tlDef.getLabelResourceId());

					if (StringUtils.isBlank(menu) || StringUtils.isBlank(label)) {
						continue;
					}

					treeNodeList.add(
							Rest.translateUid(E.ENTITY, tasklistEntityMeta.getUID()),
							menu + "\\" + label,
							false,
							// TODO: org.nuclos.common.E._Searchfilter.getNuclosResource could be used here, if the icon wasn't blue.
							"org.nuclos.client.resource.icon.glyphish.06-magnify.png",
							false,
							null,
							null,
							null,
							null);
				}
			}
		}

		final Collection<CustomComponentVO> customComponents = Rest.getCustomComponents();
		for(CustomComponentVO customComponent : customComponents) {
			if (restMenuEntites != null && !restMenuEntites.contains(customComponent.getId())) {
				continue;
			}
			EntityMeta<?> meta = Rest.getAllEntities().stream()
					.filter(m -> m.getUID().equals(E.CUSTOMCOMPONENT.getUID())).findAny().orElse(null);

			final String menu = Rest.getResource(customComponent.getMenupathResourceId());
			final String label = Rest.getResource(customComponent.getLabelResourceId());
			String icon = meta.getNuclosResource() != null ? meta.getNuclosResource() : ResourceResolver.DEFAULT_ICON;

			if (StringUtils.isBlank(menu) || StringUtils.isBlank(label)) {
				continue;
			}

			if (!Rest.hasAccessPermission(customComponent.getId())) {
				continue;
			}

			treeNodeList.add(
					"/planningtable/" + Rest.translateUid(E.ENTITY, customComponent.getId()),
					menu + "\\" + label,
					false,
					icon,
					null,
					null,
					null);
		}

		for (EntityObjectVO<?> conf : Rest.getAllEntityMenus()) {
			EntityMeta<UID> meta = metaProvider.getEntity(conf.getFieldUid(E.ENTITYMENU.entity.getUID()));
			UID entity = meta.getUID();

			if (shouldEntryBeSkipped(restMenuEntites, menuEntities, entity)) {
				continue;
			}

			String menuRootPath = null;
			String[] menuPath = splitMenuPath(
					SpringLocaleDelegate.getInstance().getResource(
							conf.getFieldValue(E.ENTITYMENU.menupath.getUID(), String.class),
							null
					)
			);

			if (menuPath != null && menuPath.length > 0) {
				menuRootPath = menuPath[0];
			}

			if (menuRootPath != null) {
				EntityPermission permission = Rest.getEntityPermission(meta.getUID());

				if (permission.isReadAllowed()) {
					String boMetaId = meta.getUID() != null ? Rest.translateUid(E.ENTITY, meta.getUID()) : null;
					UID processUid = conf.getFieldUid(E.ENTITYMENU.process.getUID());
					boolean hidden = reducedStartmenus != null && reducedStartmenus.contains(menuRootPath);
					String icon = meta.getNuclosResource() != null ? meta.getNuclosResource() : ResourceResolver.DEFAULT_ICON;
					String path = "";
					for (String part : menuPath) {
						path += part + "\\";
					}
					path = path.substring(0, path.length() - 1);
					treeNodeList.add(
							boMetaId,
							path,
							hidden,
							icon,
							conf.getFieldValue(E.ENTITYMENU.newfield.getUID(), Boolean.class),
							processUid != null ? Rest.translateUid(E.PROCESS, processUid) : null,
							null,
							meta.getAccelerator(),
							meta.getAcceleratorModifier());
				}
				menuEntities.add(entity);
			}
		}


		Rest.getAllEntities().stream()
				.filter(entityMeta -> !treeNodeList.contains(Rest.translateUid(E.ENTITY, entityMeta.getUID())))
				.filter(entityMeta -> !shouldEntryBeSkipped(restMenuEntites, menuEntities, entityMeta.getUID()))
				.filter(entityMeta -> entityMeta.getLocaleResourceIdForMenuPath() != null)
				.filter(entityMeta -> Rest.getEntityPermission(entityMeta.getUID()).isReadAllowed())
				.forEach(entityMeta -> {
					String boMetaId = entityMeta.getUID() != null ? Rest.translateUid(E.ENTITY, entityMeta.getUID()) : null;
					String icon = entityMeta.getNuclosResource() != null ? entityMeta.getNuclosResource() : ResourceResolver.DEFAULT_ICON;
					treeNodeList.add(
							boMetaId,
							Rest.getResource(entityMeta.getLocaleResourceIdForMenuPath()) + "\\" + Rest.getResource(entityMeta.getLocaleResourceIdForLabel()),
							false,
							icon,
							false,
							null,
							entityMeta.getMenuPathOrder(),
							entityMeta.getAccelerator(),
							entityMeta.getAcceleratorModifier());
				});

		return treeNodeList.build();
	}

	private Map<UID, EntityMeta<?>> getDynamicTaskListEntityMetaMap() {
		Map<UID, EntityMeta<?>> mapDynamicTasklistEntityMeta = new HashMap<>();

		for (EntityMeta<?> eMeta : Rest.getAllEntities()) {
			if (eMeta.isDynamicTasklist()) {
				final TasklistDefinition tlDef = (TasklistDefinition) eMeta.getProperty(EntityMeta.PROPERTY.TASKLIST_DEFINITION);
				if (tlDef != null) {
					mapDynamicTasklistEntityMeta.put(tlDef.getId(), eMeta);
				}
			}
		}
		return mapDynamicTasklistEntityMeta;
	}

	/**
	 * check if entry needs to be added to menu
	 *
	 * @return true if it should not be added to menu
	 */
	private boolean shouldEntryBeSkipped(
			final Set<UID> restMenuEntites,
			final Set<UID> menuEntities,
			final UID entity
	) {
		if (E.isNuclosEntity(entity)
				&& !restMenuSystemEntityWhitelist
					.stream()
					.map(EntityMeta::getUID)
					.collect(Collectors.toList())
					.contains(entity)) {
			return true;
		}

		boolean bReadAllowedForEntity = isReadAllowedForEntity(entity);
		if (!bReadAllowedForEntity) {
			return true;
		}

		if (restMenuEntites != null && !restMenuEntites.contains(entity)) {
			return true;
		}

		return false;

	}

	/**
	 * Returns a properly typed task meta list, instead of only JSON builders.
	 * <p>
	 * TODO: Currently still uses the old JSON builder methods, which is not very performant.
	 */
	protected List<TaskEntityMeta> getReadableTasklistMetas() throws CommonBusinessException {
		List<TaskEntityMeta> result = new ArrayList<>();

		final Map<String, List<JsonValue>> tasklistMenus = getTasklistMenus();

		final JsonArray jsonArray = buildReadableBoMetaJsonArray(
				null,
				false,
				tasklistMenus
		).build();

		for (int i = 0; i < jsonArray.size(); i++) {
			JsonObject object = jsonArray.getJsonObject(i);
			JsonObject links = object.getJsonObject("links");

			TaskEntityMeta.Builder<Void> builder = TaskEntityMeta.builder()
					.withDynamicEntityFieldName(object.getString("dynamicEntityFieldName", null))
					.withTaskMetaId(object.getString("taskMetaId", null))
					.withTaskEntity(object.getString("taskEntity", null))
					.withSearchfilter(object.getString("searchfilter", null))
					.withBoMetaId(object.getString("boMetaId", null))
					.withCreateNew(object.getBoolean("createNew", false))
					.withName(object.getString("name", null))
					.withLinks(
							EntityMetaBaseLinks.builder()
									.withBoMeta(restLinkFromJson(links.getJsonObject("boMeta")))
									.withBos(restLinkFromJson(links.getJsonObject("bos")))
									.build()
					);

			result.add(builder.build());
		}

		final Collection<Preference> searchTemplates = prefsFacade.getPreferences(
				null,
				NuclosPreferenceType.SEARCHTEMPLATE.getType(),
				null,
				null,
				true,
				SecurityCache.getInstance().getUserUid(getSessionContext().getUsername()),
				false,
				false);

		if (searchTemplates != null && !searchTemplates.isEmpty()) {
			for (Preference searchTemplate : searchTemplates) {
				String boMetaId = Rest.translateUid(E.ENTITY,
						searchTemplate.getEntity());
				TaskEntityMeta.Builder<Void> builder = TaskEntityMeta.builder()
						.withTaskEntity(boMetaId)
						.withSearchfilter(searchTemplate.getUID().getString())
						.withBoMetaId(boMetaId)
						.withCreateNew(false)
						.withName(String.format("[%s] %s",
								springLocaleDelegate.getLabelFromMetaDataVO(
										metaProvider.getEntity(searchTemplate.getEntity())),
								searchTemplate.getName()));

				result.add(builder.build());
			}
		}

		return result;
	}

	/**
	 * Returns a properly typed list, instead of only JSON builders.
	 * <p>
	 * TODO: Currently still uses the old JSON builder methods, which is not very performant.
	 */
	private List<EntityMetaBase> getReadableBOMetasAsFlatList(
			Set<String> reducedStartmenus,
			boolean asMenuTree
	) throws CommonBusinessException {
		List<EntityMetaBase> result = new ArrayList<>();

		final Map<String, List<JsonValue>> rootMenus = getRootMenus(
				asMenuTree
		);

		final JsonArray jsonArray = buildReadableBoMetaJsonArray(
				reducedStartmenus,
				asMenuTree,
				rootMenus
		).build();

		for (int i = 0; i < jsonArray.size(); i++) {
			JsonObject object = jsonArray.getJsonObject(i);
			JsonObject links = object.getJsonObject("links");

			EntityMetaBase.Builder<Void> builder = EntityMetaBase.builder()
					.withBoMetaId(object.getString("boMetaId"))
					.withCreateNew(object.getBoolean("createNew"))
					.withName(object.getString("name"))
					.withLinks(
							EntityMetaBaseLinks.builder()
									.withBoMeta(restLinkFromJson(links.getJsonObject("boMeta")))
									.withBos(restLinkFromJson(links.getJsonObject("bos")))
									.build()
					);

			result.add(builder.build());
		}

		return result;
	}

	/**
	 * @deprecated This is just a workaround. RestLink should be constructed directly instead of being converted from JSON.
	 */
	@Deprecated
	RestLink restLinkFromJson(JsonObject object) {
		if (object == null || !object.containsKey("methods") || !object.containsKey("href")) {
			return null;
		}

		RestLink.Builder<Void> builder = RestLink.builder();

		JsonArray methods = object.getJsonArray("methods");
		for (int i = 0; i < methods.size(); i++) {
			builder.addMethods(methods.getString(i));
		}

		builder.withHref(object.getString("href"));

		return builder.build();
	}

	/**
	 * @deprecated See {@link MetaDataServiceHelper#getRootMenus(boolean)}
	 */
	@Deprecated
	private JsonArrayBuilder getReadableBOMetas(
			Set<String> reducedStartmenus,
			boolean bAsMenuTree
	) throws CommonBusinessException {
		Map<String, List<JsonValue>> rootMenus = getRootMenus(bAsMenuTree);

		return buildReadableBoMetaJsonArray(
				reducedStartmenus,
				bAsMenuTree,
				rootMenus
		);
	}

	private Map<String, List<JsonValue>> getTasklistMenus() {
		Map<String, List<JsonValue>> tasklistMenus = new HashMap<>();

		final Collection<TasklistDefinition> dynamicTaskLists = Rest.getDynamicTaskLists();
		if (dynamicTaskLists != null && !dynamicTaskLists.isEmpty()) {
			Map<UID, EntityMeta<?>> mapDynamicTasklistEntityMeta = getDynamicTaskListEntityMetaMap();

			for (TasklistDefinition tlDef : dynamicTaskLists) {
				final EntityMeta<?> tasklistEntityMeta = mapDynamicTasklistEntityMeta.get(tlDef.getId());
				if (tasklistEntityMeta != null) {
					EntityPermission permission = new EntityPermission(true, false, false, false);

					final String label = getLabelForTasklist(tlDef, tasklistEntityMeta);

					IMenuEntry entry = new TaskOverview(
							tasklistEntityMeta,
							tlDef.getId(),
							label,
							permission,
							tlDef.getDynamicTasklistEntityFieldUid(),
							tlDef.getTaskEntityUID(),
							null
					);

					createMenuItem(tasklistMenus, "", entry);
				}
			}
		}

		return tasklistMenus;
	}

	/**
	 * TODO: Method is too complex and has too many responsibilities.
	 * TODO: Duplicated code.
	 * TODO: Return a proper entity, instead of only a JsonArrayBuilder.
	 *
	 * @deprecated See TODOs!
	 */
	@Deprecated
	private Map<String, List<JsonValue>> getRootMenus(
			final boolean bAsMenuTree
	) throws CommonBusinessException {
		Set<UID> restMenuEntites = null;
		if (bAsMenuTree) {
			restMenuEntites = Rest.getRestMenuEntities();
		}
		Map<String, List<JsonValue>> rootMenus = new HashMap<>();
		Set<UID> menuEntities = new HashSet<>();

		for (EntityObjectVO<?> conf : Rest.getAllEntityMenus()) {
			EntityMeta<UID> meta = metaProvider.getEntity(conf.getFieldUid(E.ENTITYMENU.entity.getUID()));
			UID entity = meta.getUID();

			if (shouldEntryBeSkipped(null, Collections.EMPTY_SET, entity)) {
				continue;
			}

			boolean bReadAllowedForEntity = isReadAllowedForEntity(entity);
			if (bAsMenuTree && !bReadAllowedForEntity) {
				continue;
			}

			String menuroot = null;
			String menuLabel = null;

			if (!bAsMenuTree) {
				menuroot = "";
			} else {
				String[] menuPath = splitMenuPath(SpringLocaleDelegate.getInstance().getResource(
						conf.getFieldValue(E.ENTITYMENU.menupath.getUID(), String.class), null));

				if (menuPath != null && menuPath.length > 0) {
					menuroot = menuPath[0];
					menuLabel = menuPath.length > 1 ? menuPath[menuPath.length - 1] : null;
				}
			}

			if (menuroot != null) {
				EntityPermission permission = Rest.getEntityPermission(meta.getUID());

				String label;
				if (menuLabel == null) {
					label = menuroot;
					menuroot = "";
				} else {
					label = menuLabel;
				}
				IMenuEntry entry = new BoMetaOverview(
						meta,
						label,
						permission,
						bAsMenuTree,
						conf.getFieldUid(E.ENTITYMENU.process.getUID()),
						conf.getFieldValue(E.ENTITYMENU.newfield.getUID(), Boolean.class)
				);
				createMenuItem(rootMenus, menuroot, entry);
				menuEntities.add(entity);
			}
		}

		for (final EntityMeta<?> meta : Rest.getAllEntities()) {
			UID entity = meta.getUID();

			if (shouldEntryBeSkipped(restMenuEntites, menuEntities, entity)) {
				continue;
			}

			String menuroot = null;

			if (!bAsMenuTree) {
				menuroot = "";

			} else {
				String[] menuPath = splitMenuPath(Rest.getResource(meta.getLocaleResourceIdForMenuPath()));

				if (menuPath != null && menuPath.length > 0) {
					menuroot = menuPath[0];
				}
			}

			if (menuroot != null) {
				EntityPermission permission = Rest.getEntityPermission(meta.getUID());

				String label = Rest.getLabelFromMetaDataVO(meta);
				IMenuEntry entry = new BoMetaOverview(meta, label, permission, bAsMenuTree);
				createMenuItem(rootMenus, menuroot, entry);
			}
		}

		return rootMenus;
	}

	/**
	 * @deprecated Do not build JSON manually!
	 */
	@Deprecated
	private JsonArrayBuilder buildReadableBoMetaJsonArray(
			final Set<String> reducedStartmenus,
			final boolean bAsMenuTree,
			final Map<String, List<JsonValue>> rootMenus
	) {
		JsonArrayBuilder jsonarray = Json.createArrayBuilder();
		for (String rootMenu : rootMenus.keySet()) {
			List<JsonValue> lstMenuItems = rootMenus.get(rootMenu);

			if (bAsMenuTree) {
				JsonArrayBuilder miArray = Json.createArrayBuilder();

				for (JsonValue menuItem : lstMenuItems) {
					miArray.add(menuItem);
				}


				JsonObjectBuilder job = Json.createObjectBuilder();
				job.add("path", rootMenu);
				job.add("entries", miArray.build());

				if (reducedStartmenus != null && reducedStartmenus.contains(rootMenu)) {
					job.add("hidden", true);
				}

				JsonObject menu = job.build();

				jsonarray.add(menu);

			} else {
				for (JsonValue menuItem : lstMenuItems) {
					jsonarray.add(menuItem);
				}
			}
		}
		return jsonarray;
	}

	/**
	 * Returns the configured label or a fallback.
	 */
	private String getLabelForTasklist(final TasklistDefinition tlDef, final EntityMeta<?> tasklistEntityMeta) {
		String label = Rest.getResource(tlDef.getLabelResourceId());

		if (label == null) {
			label = tlDef.getName();
		}

		return label;
	}

	private Map<UID, List<SearchFilterVO>> getSearchFiltersByEntityForTaskLists(final Set<UID> setTasks) throws CommonBusinessException {
		final Map<UID, List<SearchFilterVO>> result = new HashMap<>();

		for (UID task : setTasks) {
			EntitySearchFilter2 searchFilter2 = Rest.facade().getSearchFilterByPk(task);
			if (searchFilter2 != null) {
				UID sfEntity = searchFilter2.getVO().getEntity();
				if (!result.containsKey(sfEntity)) {
					result.put(sfEntity, new ArrayList<>());
				}
				result.get(sfEntity).add(searchFilter2.getVO());
			}
		}

		return result;
	}

	private void createMenuItem(
			@Nonnull final Map<String, List<JsonValue>> rootMenus,
			@Nonnull final String menuroot,
			@Nonnull final IMenuEntry menuEntry
	) {
		JsonValue menuItem = jsonFactory.buildJsonObject(menuEntry, this).build();

		if (!rootMenus.containsKey(menuroot)) {
			rootMenus.put(menuroot, new ArrayList<>());
		}
		rootMenus.get(menuroot).add(menuItem);
	}

	private static String[] splitMenuPath(String menuPath) {
		if (menuPath == null || menuPath.isEmpty()) {
			return null;
		}
		return menuPath.split("\\\\");
	}

	protected JsonObjectBuilder getProcessChange(String processId, String boMetaId, String boId) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);

		try {
			JsonObjectBuilder builder = Json.createObjectBuilder();

			UID entityUID = Rest.translateFqn(E.ENTITY, boMetaId);
			UID processUID = Rest.translateFqn(E.PROCESS, processId);
			UID statusUID;
			String customUsage;

			if (boId != null && !boId.isEmpty()) {
				UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(Long.parseLong(boId), entityUID);
				statusUID = usage.getStatusUID();
				customUsage = usage.getCustom();

			} else {
				statusUID = Rest.facade().getInitialStateId(entityUID, processUID);
				customUsage = Rest.facade().getCustomUsage();

			}

			UsageCriteria newUsage = new UsageCriteria(entityUID, processUID, statusUID, customUsage);

			UID newLayoutUID = Rest.facade().getDetailLayoutIDForUsage(newUsage, false);
			RestLinks links = new RestLinks(builder);
			if (newLayoutUID != null) {
				links.addLinkHref("layout", "weblayoutCalculated", Verbs.GET, Rest.translateUid(E.LAYOUT, newLayoutUID));
			}

			if (statusUID != null && stateCache.hasModelDefaultPathByStateId(statusUID)) {
				if (boId == null) {
					links.addLink("defaultPath", Verbs.GET, Rest.translateUid(E.STATE, statusUID));
				}
			}
			if (links.count() > 0) {
				links.buildJson(this);
			}

			return builder;
		} catch (Exception e) {
			throw new NuclosWebException(e, info.getEntity());
		}
	}

	public TableViewLayout getTableViewLayout(UID entityUID) {
		return getTableViewLayout(entityUID, null);
	}

	public TableViewLayout getTableViewLayout(UID entityUID, UID dependenceUID) {

		List<UID> sortedColumns = new ArrayList<UID>();
		List<Integer> columnWidths = new ArrayList<Integer>();
		List<ColumnSortingPreferences> columnSortings = null;

		try {
			final String userName = Rest.facade().getCurrentUserName();
			final UID mandatorUID = Rest.facade().getCurrentMandatorUID();
			final TablePreferencesManager tblprefManager;
			if (dependenceUID != null) {
				// TODO layout should be a parameter!
				UID layoutUID = null;
				// layout null? try to get default layout for main entity...
				if (layoutUID == null && entityUID != null) {
					layoutUID = Rest.facade().getDetailLayoutIDForUsage(new UsageCriteria(entityUID, null, null, Rest.facade().getCustomUsage()), false);
				}
				tblprefManager = prefsProv.getTablePreferencesManagerForSubformEntity(dependenceUID, layoutUID,
						userName, mandatorUID);
			} else {
				tblprefManager = prefsProv.getTablePreferencesManagerForEntity(entityUID,
						userName, mandatorUID);
			}

			if (tblprefManager != null) {

				TablePreferences tp = tblprefManager.getSelected();

				if (tp != null) {

					columnSortings = tp.getColumnSortings();

					for (ColumnPreferences cp : tp.getSelectedColumnPreferences()) {
						sortedColumns.add(cp.getColumn());
						columnWidths.add(cp.getWidth());
					}
				}

				// if no layout is available (e.g. layout was not saved in rich client workspace) create one
				if (sortedColumns.size() == 0) {

					Collection<FieldMeta<?>> entityFields = Rest.getAllEntityFieldsByEntityIncludingVersion(dependenceUID != null ? dependenceUID : entityUID).values();
					List<FieldMeta<?>> fields = new ArrayList<FieldMeta<?>>(entityFields);
					Collections.sort(fields);

					for (FieldMeta<?> field : fields) {
						// don't add system fields
						if (
								!field.getUID().equals(SF.CREATEDBY.getUID(field.getEntity())) &&
										!field.getUID().equals(SF.CREATEDAT.getUID(field.getEntity())) &&
										!field.getUID().equals(SF.CHANGEDBY.getUID(field.getEntity())) &&
										!field.getUID().equals(SF.CHANGEDAT.getUID(field.getEntity())) &&
										!field.getUID().equals(SF.VERSION.getUID(field.getEntity()))
						) {
							sortedColumns.add(field.getUID());
							int width = ProfileUtils.getMinimumColumnWidth(field.getJavaClass());
							columnWidths.add(width);
						}
					}
				}

			}
		} catch (Exception e) {
			LOG.error("Unable to get list layout from preferences.", e);
		}

		return new TableViewLayout(sortedColumns, columnWidths, columnSortings);
	}


	public void resetTableViewLayout(String entityFQN, String dependenceFQN) {

		UID entityUID = Rest.translateFqn(E.ENTITY, entityFQN);
		UID dependenceUID = dependenceFQN != null ? Rest.translateFqn(E.ENTITY, dependenceFQN) : null;

		try {
			final String userName = Rest.facade().getCurrentUserName();
			final UID mandatorUID = Rest.facade().getCurrentMandatorUID();
			final TablePreferencesManager tblprefManager;
			if (dependenceUID != null) {
				tblprefManager = prefsProv.getTablePreferencesManagerForSubformEntity(dependenceUID, null,
						userName, mandatorUID);
			} else {
				tblprefManager = prefsProv.getTablePreferencesManagerForEntity(dependenceUID,
						userName, mandatorUID);
			}

			if (tblprefManager != null) {
				TablePreferences tp = tblprefManager.getSelected();
				tblprefManager.reset(tp);
			}

		} catch (Exception e) {
			LOG.error("Unable to store table layout in prefs.", e);
		}
	}

	public void storeTableViewLayout(String entityFQN, String dependenceFQN, JsonObject data) {


		List<ColumnSortingPreferences> columnSortings = new ArrayList<ColumnSortingPreferences>();

		List<ColumnPreferences> columns = new ArrayList<ColumnPreferences>();
		JsonArray jsonColumns = data.getJsonArray("columns");
		for (JsonValue jsonValue : jsonColumns) {
			JsonObject jsonObject = (JsonObject) jsonValue;

			if (jsonObject.get("fieldName") != null) {

				ColumnPreferences columnPreferences = new ColumnPreferences();

				UID columnUid = Rest.translateFqn(E.ENTITYFIELD, (dependenceFQN != null ? dependenceFQN : entityFQN) + "_" + jsonObject.getString("fieldName"));
				columnPreferences.setColumn(columnUid);

				columnPreferences.setWidth(jsonObject.getInt("width"));

				if (jsonObject.get("sort") != null) {
					JsonObject sort = jsonObject.getJsonObject("sort");
					if (!sort.isEmpty()) {
						boolean isAscending = sort.getString("direction").equals("asc");
						int priority = sort.getInt("priority"); // TODO
						ColumnSortingPreferences columnSorting = new ColumnSortingPreferences();
						columnSorting.setAsc(isAscending);
						columnSorting.setColumn(columnUid);
						columnSortings.add(columnSorting);
					}
				}

				columns.add(columnPreferences);
			}
		}


		UID entityUID = Rest.translateFqn(E.ENTITY, entityFQN);
		UID dependenceUID = dependenceFQN != null ? Rest.translateFqn(E.ENTITY, dependenceFQN) : null;

		try {
			final String userName = Rest.facade().getCurrentUserName();
			final UID mandatorUID = Rest.facade().getCurrentMandatorUID();
			final TablePreferencesManager tblprefManager;
			if (dependenceUID != null) {
				tblprefManager = prefsProv.getTablePreferencesManagerForSubformEntity(dependenceUID, null,
						userName, mandatorUID);
			} else {
				tblprefManager = prefsProv.getTablePreferencesManagerForEntity(dependenceUID,
						userName, mandatorUID);
			}

			if (tblprefManager != null) {

				TablePreferences tp = tblprefManager.getSelected();

				List<ColumnPreferences> orderedColumnPreferences = new ArrayList<ColumnPreferences>();
				for (ColumnPreferences c : columns) {
					if (tp != null && tp.getSelectedColumnPreferences() != null) {
						for (ColumnPreferences cp : tp.getSelectedColumnPreferences()) {

							if (cp.getColumn().equals(c.getColumn())) {
								cp.setWidth(c.getWidth());
								orderedColumnPreferences.add(cp);
							}
						}
					}
				}

				if (orderedColumnPreferences.size() == 0) {
					// no column definition found in workspace - add them
					orderedColumnPreferences.addAll(columns);
				}
				if (tp != null) {
					tp.removeAllSelectedColumnPreferences();
					tp.addAllSelectedColumnPreferences(orderedColumnPreferences);

					tp.removeAllColumnSortings();
					tp.addAllColumnSortings(columnSortings);

					tblprefManager.update(tp);
				}
			}

		} catch (Exception e) {
			LOG.error("Unable to store table layout in prefs.", e);
		}
	}

}
