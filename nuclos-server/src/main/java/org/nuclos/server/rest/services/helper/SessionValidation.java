package org.nuclos.server.rest.services.helper;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface SessionValidation {
	/**
	 * Determines if the current session should validated on called REST path
	 *
	 * @return boolean
	 */
	boolean enabled() default true;
}
