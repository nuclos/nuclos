package org.nuclos.server.rest.services.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.preferences.ColumnSortingPreferences;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.rest.ejb3.Rest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TableViewLayout {
	
	private static final Logger LOG = LoggerFactory.getLogger(TableViewLayout.class);

	private final List<UID> sortedColumns;
	private final List<Integer> columnWidths;
	private final List<ColumnInfo> columns;
	private int tableWidth;

	public TableViewLayout(List<UID> sortedColumns, List<Integer> columnWidths) {
		this(sortedColumns, columnWidths, null);
	}

	public TableViewLayout(List<UID> sortedColumns, List<Integer> columnWidths, List<ColumnSortingPreferences> columnSortings) {
		this.sortedColumns = sortedColumns;
		this.columnWidths = columnWidths;
		this.columns = new ArrayList<ColumnInfo>();
		
		int index = 0;
		this.tableWidth = 0;

		Hashtable<UID, Integer> sorting = new Hashtable<UID, Integer>();
		if (columnSortings != null) {
			int i = 0;
			for(ColumnSortingPreferences columnSorting : columnSortings) {
				sorting.put(columnSorting.getColumn(), i++);
			}			
		}
		
   		for (UID columnUID : sortedColumns) {

   			Integer columnWidth = columnWidths.size() > index ? columnWidths.get(index) : null;
			try {
				FieldMeta<?> fieldMeta = Rest.getEntityField(columnUID);
				Integer currentSortColumnOrder = null;				
				String sortColumn = null;
				if(columnSortings != null) {
					for(ColumnSortingPreferences columnSorting : columnSortings) {
						if(columnSorting.getColumn().equals(columnUID)) {
							sortColumn = columnSorting.isAsc() ? "asc" : "desc";
							currentSortColumnOrder = sorting.get(columnUID);
							break;
						}
					}
				}
				columns.add(new ColumnInfo(fieldMeta, columnWidth, sortColumn, currentSortColumnOrder));
				
				tableWidth += columnWidth;
			} catch (CommonFatalException e) {
				LOG.error("Unable to get entity field.", e);
			}
   			index ++;
   		}
	}
	
	public List<ColumnInfo> getColumns() {
		return Collections.unmodifiableList(columns);
	}
	
	public int getTableWidth() {
		return tableWidth;
	}

	public List<Integer> getPercentWidths() {
		Integer maxWidth = 0;
		for (Integer width : columnWidths) {
			maxWidth += width;
		}
		List<Integer> result = new ArrayList<Integer>();
		for (Integer width : columnWidths) {
			result.add(
					new BigDecimal(100.0)
					.multiply(new BigDecimal(width))
					.divide(new BigDecimal(maxWidth), BigDecimal.ROUND_HALF_EVEN)
					.intValue()
					);
		}

		return result;
	}

	public List<UID> getSortedColumns() {
		return Collections.unmodifiableList(sortedColumns);
	}

}
