package org.nuclos.server.rest.services.helper;

public enum UserAction {
	COLLECTIVE_PROCESSING("CollectiveProcessing"),
	BULK_EDIT("BulkEdit");

	private final String value;

	UserAction(final String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
