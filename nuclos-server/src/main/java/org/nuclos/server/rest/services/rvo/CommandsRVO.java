//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

import org.nuclos.api.Direction;
import org.nuclos.api.Message;
import org.nuclos.api.command.CloseCommand;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.api.ApiCommandImpl;
import org.nuclos.common.api.ApiDirectionImpl;
import org.nuclos.common.api.ApiMessageImpl;
import org.nuclos.server.rest.ejb3.Rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CommandsRVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	final private List<CommandRVO> list;
	
	@JsonCreator
	public CommandsRVO(@JsonProperty("list") List<CommandRVO> list) {
		this.list = list;
	}

	public List<CommandRVO> getList() {
		return list;
	}

	@JsonIgnore
	public JsonObjectBuilder toJsonBuilderObject(){
		JsonObjectBuilder json = Json.createObjectBuilder();
		JsonArrayBuilder alist = Json.createArrayBuilder();
		for (CommandRVO c : list) {
			alist.add(c.toJsonBuilderObject());
		}
		json.add("list", alist);
		return json;
	}

	/**
	 * Creates a new RVO for the given command object and adds it to the commands RVO.
	 * <p>
	 * TODO: Get rid of ugly if-else instanceof switches!
	 */
	public static void addCommand(final CommandsRVO cmds, final Serializable cmd) {
		if (cmd instanceof ApiDirectionImpl) {
			final ApiDirectionImpl apiDirection = (ApiDirectionImpl) cmd;
			final Direction<Long> direction = apiDirection.getDirection();

			if (direction.getAdditionalNavigation() != null) {
				final DirectionRVO directionRVO = new DirectionRVO(null, null, null, direction.isNewTab(), direction.getAdditionalNavigation(), null);
				cmds.getList().add(directionRVO);
			} else if (direction.getUrl() != null) {
				final DirectionRVO directionRVO = new DirectionRVO(null, null, null, direction.isNewTab(), null, direction.getUrl());
				cmds.getList().add(directionRVO);
			} else {
				final Collection<EntityMeta<?>> entities = Rest.getAllEntities();
				for (EntityMeta<?> entityMeta : entities) {
					String boMetaId = Rest.translateUid(E.ENTITY, entityMeta.getUID());

					if (direction.getEntityUid() != null && entityMeta.getUID() != null && direction.getEntityUid().equals(entityMeta.getUID())) {
						final DirectionRVO directionRVO = new DirectionRVO(direction.getId(), boMetaId, direction.getSearchFilterUid(), direction.isNewTab(), null, null);
						cmds.getList().add(directionRVO);
					}
				}
			}
		} else if (cmd instanceof ApiMessageImpl) {
			final ApiMessageImpl apiMessage = (ApiMessageImpl) cmd;
			final Message message = apiMessage.getMessage();
			final MessageRVO messageRVO = new MessageRVO(message.getMessage(), message.getTitle());
			cmds.getList().add(messageRVO);
		} else if (cmd instanceof ApiCommandImpl) {
			Object command = ((ApiCommandImpl) cmd).getCommand();
			if (command instanceof CloseCommand) {
				cmds.getList().add(new CloseCommandRVO());
			}
		}
	}
}
