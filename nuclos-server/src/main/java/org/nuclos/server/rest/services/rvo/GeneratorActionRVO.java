package org.nuclos.server.rest.services.rvo;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.rest.misc.RestLinks.Verbs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
class GeneratorActionRVO {
	private final String generationId;
	private final String name;
	private final EntityMeta<?> targetEntity;
	private final String target;
	private final boolean nonstop;
	private final boolean internal;
	private final boolean hideInCollectiveProcessing;
	private final boolean openInOverlay;
	private final String boMetaId;
	private final boolean dialogMode;
	private final int dialogWidth;
	private final int dialogHeight;
	private final UID parameterEntity;
	private final UID parameterEntityVLP;

	private DatasourceServerUtils datasourceServerUtils;
	private DatasourceCache datasourceCache;

	GeneratorActionRVO(GeneratorActionVO generation, String boMetaId) {
		generationId = Rest.translateUid(E.GENERATION, generation.getId());
		name = StringUtils.defaultIfNull(generation.getLabel(), generation.getName());
		targetEntity = Rest.getEntity(generation.getTargetModule());
		target = targetEntity != null ? Rest.getLabelFromMetaDataVO(targetEntity) : "";
		nonstop = generation.isNonstop();
		internal = generation.isRuleOnly();
		hideInCollectiveProcessing = generation.isHideInCollectiveProcessing();
		openInOverlay = generation.isOpenInOverlay();
		this.boMetaId = boMetaId;
		dialogMode = generation.isDialogMode();
		dialogWidth = generation.getDialogWidth();
		dialogHeight = generation.getDialogHeight();
		parameterEntity = generation.getParameterEntity();
		parameterEntityVLP = generation.getValuelistProvider();
	}

	@Autowired
	private void inject(DatasourceServerUtils datasourceServerUtils, DatasourceCache datasourceCache) {
		this.datasourceServerUtils = datasourceServerUtils;
		this.datasourceCache = datasourceCache;
	}

	//Note: It's not trivial to put this into JsonFactory, as the result is dependent of boId
	JsonObjectBuilder getJsonObjectBuilder(
			final String boId,
			final int version,
			final IWebContext context
	) {
		JsonObjectBuilder json = Json.createObjectBuilder();

		json.add("generationId", generationId)
				.add("name", name)
				.add("target", target)
				.add("nonstop", nonstop)
				.add("internal", internal)
				.add("hideInCollectiveProcessing", hideInCollectiveProcessing)
				.add("openInOverlay", openInOverlay)
				.add("dialogMode", dialogMode);

		if (dialogMode) {
			json.add("dialogWidth", dialogWidth)
					.add("dialogHeight", dialogHeight);
		}

		if (parameterEntity != null) {
			json.add("parameterEntity", Rest.translateUid(E.ENTITY, parameterEntity));
			if (parameterEntityVLP != null) {
				json.add("parameterEntityVLPId", parameterEntityVLP.toString());
				String vlpName;
				vlpName = datasourceServerUtils.getValuelistProvider(parameterEntityVLP, datasourceCache).getName();
				json.add("parameterEntityVLPName", vlpName);
			}
		}

		RestLinks links = new RestLinks(json);
		links.addLinkHref(
				"generate",
				"boGeneration",
				Verbs.POST,
				boMetaId,
				boId,
				version,
				generationId
		);
		links.buildJson(context);

		return json;
	}


}
