package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.rest.ejb3.Rest;

/**
 * Wrapper class for Layout EOs to access just some basic infos.
 *
 * TODO: Should also contain the layout URL.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class LayoutInfo implements Serializable {
	private final EntityObjectVO<UID> wrappedEO;

	public LayoutInfo(final EntityObjectVO<UID> wrappedEO) {
		this.wrappedEO = wrappedEO;
	}

	public UID getUid() {
		return wrappedEO.getPrimaryKey();
	}

	public String getName() {
		return wrappedEO.getFieldValue(E.LAYOUT.name);
	}

	public String getDescription() {
		return wrappedEO.getFieldValue(E.LAYOUT.description);
	}

	public UID getNucletUID() {
		return wrappedEO.getFieldUid(E.LAYOUT.nuclet);
	}

	/**
	 * Returns the fully qualified name to be used by the webclient.
	 */
	public String getFQN() {
		return Rest.translateUid(E.LAYOUT, wrappedEO.getPrimaryKey());
	}
}
