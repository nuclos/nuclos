package org.nuclos.server.rest.services.rvo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.nuclos.server.rest.misc.NuclosWebException;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuPreferenceRVO {

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class MenuItemRVO {
		
		private String name;
		private boolean isExpanded;
		
		public boolean getIsExpanded() {
			return isExpanded;
		}
		public void setIsExpanded(boolean expand) {
			this.isExpanded = expand;
		}
		public MenuItemRVO() {
			
		}
		public MenuItemRVO(String name, boolean expand) {
			this.name = name;
			this.isExpanded = expand;
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		try {
			return mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			throw new NuclosWebException(e, null);
		}
	}

	public static MenuPreferenceRVO parse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		return  mapper.readValue(json, MenuPreferenceRVO.class);
	}
	
	List<MenuItemRVO> menuItems = new ArrayList<MenuItemRVO>();
	
	public List<MenuItemRVO> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(List<MenuItemRVO> menuItems) {
		this.menuItems = menuItems;
	}

}
