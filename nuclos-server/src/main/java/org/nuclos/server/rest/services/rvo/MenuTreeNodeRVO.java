package org.nuclos.server.rest.services.rvo;

import java.awt.event.InputEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common2.CollatorProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.common.E;
import org.nuclos.server.rest.misc.IWebContext;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuTreeNodeRVO implements Comparable<MenuTreeNodeRVO>, Serializable {

	private static final long serialVersionUID = 1L;
	public static final String ROOT_MENU_ID = "ROOT_MENU";
	private static final String PATH_SEPARATOR = "\\";
	private static final String PATH_SEPARATOR_FOR_REGEX = "\\\\";
	
	private String id;
	private String parentId;
	private List<MenuTreeNodeRVO> entries = new ArrayList<MenuTreeNodeRVO>();

	private String boMetaId;
	private String name;
	private String href;
	private String icon;
	private boolean hidden;
	private boolean createNew;
	private String processMetaId;
	private Integer order;
	private String accelerator;
	private String acceleratorModifier;

	private IWebContext webContext;

	/**
	 * create root node
	 */
	private MenuTreeNodeRVO() {
		this.id = ROOT_MENU_ID;
		this.name = ROOT_MENU_ID;
	}

	public MenuTreeNodeRVO(String boMetaId, String path, boolean hidden, String icon, boolean createNew, String processMetaId, final Integer order, IWebContext webContext,
						   String accelerator, Integer acceleratorModifer) {
		String[] pathArray = path.split(PATH_SEPARATOR_FOR_REGEX);
		if (pathArray.length > 1) {
			this.name = pathArray[pathArray.length - 1];
			this.id = StringUtils.join(pathArray, PATH_SEPARATOR);
			this.parentId = StringUtils.join(pathArray, PATH_SEPARATOR, 0, pathArray.length - 1);
		} else if (pathArray.length == 1) {
			this.name = path;
			this.id = path;
			this.parentId = ROOT_MENU_ID;
		}

		this.boMetaId = boMetaId;
		this.hidden = hidden;
		this.icon = icon;
		this.createNew = createNew;
		this.processMetaId = processMetaId;
		this.order = order != null ? order : Integer.MAX_VALUE;

		this.accelerator = accelerator;

		if (acceleratorModifer != null) {
			switch (acceleratorModifer) {
				case InputEvent.ALT_MASK:
				case InputEvent.ALT_DOWN_MASK:
					this.acceleratorModifier = "ALT";
					break;
				case InputEvent.CTRL_MASK:
				case InputEvent.CTRL_DOWN_MASK:
					this.acceleratorModifier = "CTRL";
					break;
				case InputEvent.CTRL_DOWN_MASK + InputEvent.SHIFT_DOWN_MASK:
				case InputEvent.CTRL_MASK + InputEvent.SHIFT_MASK:
					this.acceleratorModifier = "CTRL|SHIFT";
					break;
			}
		}

		this.webContext = webContext;
	}
	
	private MenuTreeNodeRVO findTreeNode(String id) {
		if (this.id.equals(id)) {
			return this;
		}
		for (MenuTreeNodeRVO tn : entries) {
			MenuTreeNodeRVO node = tn.findTreeNode(id);
			if (node != null) {
				return node;
			}
		}
		return null;
	}
	
	private MenuTreeNodeRVO addTreeNode(MenuTreeNodeRVO treeNode) {
		if (this.id.equals(treeNode.parentId)) {
			this.entries.add(treeNode);
			return this;
		}
		for (MenuTreeNodeRVO tn : entries) {
			return tn.addTreeNode(treeNode);
		}
		return null;
	}
	
	/**
	 * for debugging
	 */
	public void print() {
		print(0);
	}

	private void print(int level) {
		if (entries != null) {
			for (MenuTreeNodeRVO childNode : entries) {
				childNode.print(level + 1);
			}
		}
	}

	@JsonInclude(Include.NON_NULL)
	public String getKeyStroke() {
		return accelerator;
	}

	public static class RestLinks {
		private RestLink bos;
		private RestLink resourceicon;

		@JsonInclude(Include.NON_NULL)
		public RestLink getResourceicon() {
			return resourceicon;
		}

		@JsonInclude(Include.NON_NULL)
		public RestLink getBos() {
			return bos;
		}
	}

	public static class RestLink {
		private String href;
		
		public RestLink(String href) {
			super();
			this.href = href;
		}

		public String getHref() {
			return href;
		}
	}
	
	public RestLinks getLinks() {
		RestLinks restLinks = new RestLinks();
		if (icon != null) {
			restLinks.resourceicon = new RestLink(webContext.getRestURI("resourceicon", "") + icon);
		}
		return restLinks;
	}
	
	
	@Override
	public int compareTo(MenuTreeNodeRVO treeNode) {
		if (this.id == null || treeNode.id == null) {
			return 0;
		}
		if (this.parentId != null || treeNode.parentId != null) {
			if (StringUtils.equals(this.parentId, treeNode.parentId)) {
				if (this.boMetaId == null && treeNode.boMetaId != null) {
					return 1;
				}
				if (this.boMetaId != null && treeNode.boMetaId == null) {
					return -1;
				}
				int cmp = this.order.compareTo(treeNode.order);
				if (cmp == 0) {
					return compareIds(this.id, treeNode.id);
				}
				return cmp;
			} else {
				if (this.parentId == ROOT_MENU_ID && treeNode.parentId != ROOT_MENU_ID) {
					return -1;
				}
				if (this.parentId != ROOT_MENU_ID && treeNode.parentId == ROOT_MENU_ID) {
					return 1;
				}
				return compareIds(this.parentId, treeNode.parentId);
			}
		} else {
			return compareIds(this.id, treeNode.id);
		}
	}

	private int compareIds(final String id1, final String id2) {
		return Rest.facade().getCurrentLocaleCollator().compare(id1, id2);
	}


	@JsonInclude(Include.NON_NULL)
	public String getBoMetaId() {
		return boMetaId;
	}

	public String getName() {
		return name;
	}

	@JsonInclude(Include.NON_NULL)
	public String getIcon() {
		return icon;
	}

	@JsonInclude(Include.NON_NULL)
	public String getHref() {
		return href;
	}

	@JsonInclude(Include.NON_EMPTY)
	public List<MenuTreeNodeRVO> getEntries() {
		return entries;
	}
	
	@JsonInclude(Include.NON_NULL)
	public String getProcessMetaId() {
		return processMetaId;
	}

	public boolean getCreateNew() {
		return createNew;
	}

	public Integer getOrder() {
		return order;
	}

	@JsonInclude(Include.NON_NULL)
	public String getKeyStrokeModifier() {
		return acceleratorModifier;
	}

	public static class TreeNodeBuilder {

		private IWebContext webContext;

		public TreeNodeBuilder(IWebContext webContext) {
			this.webContext = webContext;
		}

		List<MenuTreeNodeRVO> list = new ArrayList<MenuTreeNodeRVO>();

		public MenuTreeNodeRVO add(String boMetaId, String path, boolean hidden, String icon, boolean createNew, String processMetaId, final Integer order,
								   String accelerator, Integer acceleratorModifier) {
			MenuTreeNodeRVO tn = new MenuTreeNodeRVO(boMetaId, path, hidden, icon, createNew, processMetaId, order, webContext,
					accelerator, acceleratorModifier);
			addOrRedistribute(tn);
			addMissingParentNodes(tn);
			return tn;
		}

		public MenuTreeNodeRVO add(String href, String path, boolean hidden, String icon, final Integer order,
								   String accelerator, Integer acceleratorModifier) {
			MenuTreeNodeRVO tn = new MenuTreeNodeRVO(null, path, hidden, icon, false, null, order, webContext,
					accelerator, acceleratorModifier);
			tn.href = href;
			addOrRedistribute(tn);
			addMissingParentNodes(tn);
			return tn;
		}

		/**
		 * Checks, if there is already a node with the specified boMetaId in the list.
		 * @param boMetaId the boMetaId, to check for
		 * @return true, if there is already a node with the specified boMetaId and false otherwise.
		 */
		public boolean contains(String boMetaId) {
			return list.stream()
					.anyMatch(element -> StringUtils.equals(element.boMetaId, boMetaId));
		}

		/**
		 * Inserts a MenuTreeNodeRVO into the tree structure.
		 * If there are 2 identical nodes, then it will be checked, if there is a fix for the misconfiguration by redistributing.
		 * Redistribution resolves duplicate node names, by adding the duplicate nodes to a new parent, and renaming the nodes.
		 * @param tn the tree node to insert into the structure
		 * @see <a href="https://support.nuclos.de/browse/NUCLOS-10464">NUCLOS-10464</a>
		 */
		private void addOrRedistribute(MenuTreeNodeRVO tn) {
			Optional<MenuTreeNodeRVO> duplicate = list.stream()
					.filter(node -> StringUtils.equals(node.name, tn.name) && StringUtils.equals(node.parentId, tn.parentId))
					.findFirst();
			boolean isLeafNode = tn.boMetaId != null;
			if (duplicate.isPresent()) {
				redistribute(tn, duplicate.get());
			}
			// prevent non-leaf duplicates, by not adding them
			if (!duplicate.isPresent() || isLeafNode) {
				list.add(tn);
			}
		}

		public void redistribute(MenuTreeNodeRVO tn, MenuTreeNodeRVO duplicateNode) {
			boolean isLeafNode = tn.boMetaId != null;
			if (isLeafNode && isPushDownNecessary(tn, duplicateNode)) {
				boolean duplicateHasMetaId = duplicateNode.boMetaId != null;
				boolean notPerfectlyMatching = !simpleEquals(tn, duplicateNode);
				// Only push the duplicate down, if it has a metaId, otherwise it was never meant to be a leaf node
				if (duplicateHasMetaId && notPerfectlyMatching) {
					pushDownNode(duplicateNode);
					add(null, duplicateNode.parentId + "\\" + duplicateNode.name, false, null, false, null, null, null, null);
				}
				// always push down tn to make it a leaf again
				pushDownNode(tn);
			}
		}

		/**
		 * Checks, if the boMetaId, processMetaId and the name of the MenuTreeNodeRVO match.
		 * It is not meant as a full implementation of {@link Object#equals(Object)}.
		 * @param tn
		 * @param duplicateNode
		 * @return true, if boMetaId, processMetaId and name are equal. False, otherwise.
		 */
		public boolean simpleEquals(MenuTreeNodeRVO tn, MenuTreeNodeRVO duplicateNode) {
			return StringUtils.equals(duplicateNode.boMetaId, tn.boMetaId)
					&& StringUtils.equals(duplicateNode.processMetaId, tn.processMetaId)
					&& StringUtils.equals(duplicateNode.name, tn.name);
		}

		/**
		 * Checks, if the 2 tree nodes are completely identical and thus need to be pushed down to resolve the misconfiguration.
		 * If the pushdown would not result in unique label names, then it is not necessary.
		 * @param tn the new tree node to insert
		 * @param duplicateNode the node that matches the metaId of tn and possibly is a complete duplicate
		 * @return true, if a pushdown is necessary and false otherwise.
		 */
		private boolean isPushDownNecessary(MenuTreeNodeRVO tn, MenuTreeNodeRVO duplicateNode) {
			boolean noNullMeta = tn.boMetaId != null && duplicateNode.boMetaId != null;
			// if pushing down does not result in unique labels, then it should not be done
			if (noNullMeta && StringUtils.equals(getPushDownLabel(tn), getPushDownLabel(duplicateNode)))  {
				return false;
			}
			// at this point, we know pushing down would resolve the name collision
			return (!StringUtils.equals(tn.boMetaId, duplicateNode.boMetaId)
					|| !StringUtils.equals(tn.processMetaId, duplicateNode.processMetaId)
					|| !StringUtils.equals(getPushDownLabel(tn), getPushDownLabel(duplicateNode)));
		}

		/**
		 * Retrieves the localized label resource of the nodes entity for a menu entry, when it will be pushed down.
		 * @param pushDownNode
		 * @return
		 */
		private String getPushDownLabel(MenuTreeNodeRVO pushDownNode) {
			if (pushDownNode.getBoMetaId() == null) {
				return null;
			}
			return Rest.getAllEntities().stream()
					.filter(meta -> Rest.translateUid(E.ENTITY, meta.getUID()).equals(pushDownNode.getBoMetaId()))
					.map(meta -> Rest.getResource(meta.getLocaleResourceIdForLabel()))
					.findFirst()
					.orElse(pushDownNode.getBoMetaId().replaceAll("_[^_]+$", ""));
		}

		/**
		 * Pushes down the specified node.
		 * This means, the path of the node was not fully specified in the bo editor.
		 * Thus, the localized label resource of the entity has to be appended to the path.
		 * @param pushDownNode
		 */
		private void pushDownNode(MenuTreeNodeRVO pushDownNode) {
			String pushDownLabel = getPushDownLabel(pushDownNode);

			if (pushDownNode.parentId.equals(ROOT_MENU_ID)) {
				pushDownNode.parentId = pushDownNode.name;
			} else {
				pushDownNode.parentId += "\\" + pushDownNode.name;
			}
			pushDownNode.id = pushDownNode.parentId + "\\" + pushDownLabel;
			pushDownNode.name = pushDownLabel;
		}

		private void addMissingParentNodes(MenuTreeNodeRVO treeNode) {
			for (MenuTreeNodeRVO tn : list) {
				if (tn.id.equals(treeNode.parentId)) {
					return;
				}
			}

			if (!treeNode.parentId.equals(ROOT_MENU_ID)) {
				MenuTreeNodeRVO newNode = add(null, treeNode.parentId, false, null, false, null, null, null, null);
				if (newNode.parentId != null) {
					addMissingParentNodes(newNode);
				}
			}
		}

		public void sort() {
			Collections.sort(this.list);
		}

		public MenuTreeNodeRVO build() {
			this.sort();

			MenuTreeNodeRVO root = new MenuTreeNodeRVO();
			list.add(0, root);
			for (MenuTreeNodeRVO tn : list) {
				MenuTreeNodeRVO parentNode = root.findTreeNode(tn.parentId);
				if (parentNode != null) {
					parentNode.entries.add(tn);
				} else {
					root.addTreeNode(tn);
				}
			}
			return root;
		}
	}

	/* for testing
	public static void main(String[] args) {
		TreeNodeBuilder treeNodeList = new TreeNodeBuilder(null);
		treeNodeList.add("de_nuclos_example_X-Achse", "Matrix\\Submenu\\X-Achse", false, null);
		treeNodeList.add("de_nuclos_example_Y-Achse", "Matrix\\Submenu\\Y-Achse", false, null);

		treeNodeList.add(null, "Example", false, null);
		treeNodeList.add("de_nuclos_example_Article", "Example\\Article", false, null);
		treeNodeList.add("de_nuclos_example_Category", "Example\\Category", false, null);
		treeNodeList.add("de_nuclos_example_Customer", "Example\\Customer", false, null);

		MenuTreeNode root = treeNodeList.build();
		
		System.out.println("menu:");
		root.print();
		
		System.out.println("menu (JSON):");
		System.out.println(root.buildJson().build());
	}
	*/

	
}
