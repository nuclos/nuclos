package org.nuclos.server.rest.services.rvo;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.json.JsonNumber;

final class NuclosJsonNumber implements JsonNumber {
    private final BigDecimal bigDecimal;
    
    public NuclosJsonNumber(Number n) {
    	
    	if (n instanceof Integer) {
    		bigDecimal = new BigDecimal((Integer)n);
    		
    	} else if (n instanceof Long) {
    		bigDecimal = new BigDecimal((Long)n);
    		
    	} else if (n instanceof BigInteger) {
    		bigDecimal = new BigDecimal((BigInteger)n);
    		
    	} else if (n instanceof Double) {
    		bigDecimal = BigDecimal.valueOf((Double)n);
    		
    	} else if (n instanceof BigDecimal) {
    		bigDecimal = (BigDecimal)n;
    		
    	} else {
    		bigDecimal = new BigDecimal(0);
    	}
    }
    
    @Override
    public boolean isIntegral() {
        return bigDecimal.scale() == 0;
    }

    @Override
    public int intValue() {
        return bigDecimal.intValue();
    }

    @Override
    public int intValueExact() {
        return bigDecimal.intValueExact();
    }

    @Override
    public long longValue() {
        return bigDecimal.longValue();
    }

    @Override
    public long longValueExact() {
        return bigDecimal.longValueExact();
    }

    @Override
    public BigInteger bigIntegerValue() {
        return bigDecimal.toBigInteger();
    }

    @Override
    public BigInteger bigIntegerValueExact() {
        return bigDecimal.toBigIntegerExact();
    }

    @Override
    public double doubleValue() {
        return bigDecimal.doubleValue();
    }

    @Override
    public BigDecimal bigDecimalValue() {
        return bigDecimal;
    }

    @Override
    public ValueType getValueType() {
        return ValueType.NUMBER;
    }

    @Override
    public int hashCode() {
        return bigDecimalValue().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof JsonNumber)) {
            return false;
        }
        JsonNumber other = (JsonNumber)obj;
        return bigDecimalValue().equals(other.bigDecimalValue());
    }

    @Override
    public String toString() {
        return bigDecimal.toString();
    }

}