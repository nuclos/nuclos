package org.nuclos.server.rest.services.rvo;

import javax.json.JsonString;

final class NuclosJsonString implements JsonString {

    private final String value;

    public NuclosJsonString(String value) {
        this.value = value;
    }

    @Override
    public String getString() {
        return value;
    }

    @Override
    public CharSequence getChars() {
        return value;
    }

    @Override
    public ValueType getValueType() {
        return ValueType.STRING;
    }

    @Override
    public int hashCode() {
        return getString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof JsonString)) {
            return false;
        }
        JsonString other = (JsonString)obj;
        return getString().equals(other.getString());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('"').append(value).append('"');
        return sb.toString();
    }
}

