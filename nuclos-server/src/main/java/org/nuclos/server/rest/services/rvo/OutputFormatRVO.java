package org.nuclos.server.rest.services.rvo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.commons.lang3.ObjectUtils;
import org.nuclos.common.E;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.report.ejb3.ReportFacadeRemote;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceParameterValuelistproviderVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.schema.rest.PrintoutOutputFormat;
import org.nuclos.schema.rest.PrintoutOutputFormatParameter;
import org.nuclos.server.report.ejb3.ReportFacadeLocal;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.misc.RDataType;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.rest.misc.RestLinks.Verbs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class OutputFormatRVO extends AbstractJsonRVO<UID> {

	@Autowired
	JsonFactory jsonFactory;
	
	private final static Logger LOG = LoggerFactory.getLogger(OutputFormatRVO.class);
	
	private final ReportOutputVO reportOutputVO;
	private Map<String, Object> params;
	private final UID datasourceId;
	private final OutputFormatTO outputFormatTO;
	
	private String boId;
	
	private OutputFile file;
	
	public OutputFormatRVO(final ReportOutputVO reportOutputVO, final UID datasourceId) {
		super(E.REPORTOUTPUT);
		if (reportOutputVO == null) {
			throw new IllegalArgumentException("reportOutputVO must not be null");
		}
		this.reportOutputVO = reportOutputVO;
		this.datasourceId = datasourceId;
		this.outputFormatTO = null;
	}

	public OutputFormatRVO(final OutputFormatTO outputFormatTO, final UID datasourceId) {
		super(E.REPORTOUTPUT);
		if (outputFormatTO == null) {
			throw new IllegalArgumentException("outputFormatTO must not be null");
		}
		this.outputFormatTO = outputFormatTO;
		this.datasourceId = datasourceId;
		this.reportOutputVO = null;
	}

	public void setFile(OutputFile file) {
		this.file = file;
	}

	public String getId() {
		UID outputUID;
		if (reportOutputVO != null) {
			outputUID = (UID) reportOutputVO.getId();
		} else if (outputFormatTO != null) {
			outputUID = outputFormatTO.getId();
		} else {
			return null;
		}
		return (String) getPKForJson(outputUID);
	}

	public String getName() {
		if (reportOutputVO != null) {
			return reportOutputVO.getDescription();
		} else if(outputFormatTO != null) {
			return outputFormatTO.getDescription();
		}
		return null;
	}

	public UID getDatasourceId() {
		return this.datasourceId;
	}

	@Override
	public JsonObjectBuilder getJSONObjectBuilder() {
		NuclosJsonObjectBuilder json = new NuclosJsonObjectBuilder();
		
		json.add("outputFormatId", getId());
		json.add("name", getName());

		List<DatasourceParameterVO> dsParams = null;
		try {
			dsParams = Rest.facade().getParameterListForDataSource(getDatasourceId());
			
		} catch (Exception e) {
			LOG.warn("Exception during parameter determination: {}", e.getMessage());
			LOG.debug(e.getMessage(), e);
		}
		
		List<JsonObjectBuilder> params = new ArrayList<JsonObjectBuilder>();
		
		if (dsParams != null && !dsParams.isEmpty()) {
			Map<String, Object> transportParams = null;
			if (outputFormatTO != null) {
				transportParams = outputFormatTO.getDatasourceParams();
			}
			for (DatasourceParameterVO paramvo : dsParams) {
				if ("intid".equalsIgnoreCase(paramvo.getParameter())) {
					continue;
				}
				if (paramvo.getValueListProvider() != null) {
					DatasourceParameterValuelistproviderVO dsParamVlp = paramvo.getValueListProvider();
					Map<String, String> dsParamVlpParams = dsParamVlp.getParameters();
					if (dsParamVlpParams != null && dsParamVlpParams.containsKey("intid")) {
						dsParamVlpParams.put("intid", boId);
					}
				}
				if (transportParams != null && transportParams.containsKey(paramvo.getParameter())) {
					final Object transportValue = transportParams.get(paramvo.getParameter());
					if (transportValue != null) {
						paramvo = new DatasourceParameterVO.DatasourceParameterWithValueVO(paramvo, transportValue);
					}
				}
				params.add(jsonFactory.buildJsonObject(paramvo, null));
			}
		}
		
		if (!params.isEmpty()) {
			addJsonObjectArray(json, "parameters", params);
		}
		
		if (file != null) {
			json.add("fileName", file.fileName);
			
			RestLinks links = new RestLinks(json);
			links.addLinkHref("file", file.boMetaId != null ? "boPrintoutFile" : "reportFile", Verbs.GET, file.boMetaId, file.boId, getId(), file.fileId);
			links.buildJson(file.webContext);
		}
		
		return json;
	}

	public static OutputFormatRVO getOutputFormat(JsonObject outputformatJson,
												  DefaultReportVO reportVO,
												  ReportFacadeRemote reportFacadeRemote) {
		
		String outputFormatFqn = outputformatJson.getString("outputFormatId");
		UID outputFormatUid = Rest.translateFqn(E.REPORTOUTPUT, outputFormatFqn);
		try {
			ReportOutputVO reportoutputvo = reportFacadeRemote.getReportOutput(outputFormatUid);
			if (reportoutputvo != null) {

				OutputFormatRVO result = new OutputFormatRVO(reportoutputvo, ObjectUtils.defaultIfNull(reportoutputvo.getDatasourceUID(), reportVO.getDatasourceId()));
				
				Map<String, Object> params = new HashMap<String, Object>();
				if (outputformatJson.containsKey("parameters")) {
					JsonArray parameterListJson = outputformatJson.getJsonArray("parameters");
					
					List<DatasourceParameterVO> dsParams = Rest.facade().getParameterListForDataSource(result.getDatasourceId());
					
					for (DatasourceParameterVO dsParam : dsParams) {
						RDataType rdatatype = new RDataType(dsParam.getDatatype(), 2);
						
						for (int i = 0; i < parameterListJson.size(); i++) {
							JsonObject parameterJson = parameterListJson.getJsonObject(i);
							
							if (parameterJson.containsKey("parameter") &&
									RigidUtils.equal(parameterJson.getString("parameter"), dsParam.getParameter())) {
								
								Object value = rdatatype.getValue(parameterJson, "value");
								params.put(dsParam.getParameter(), value);

								// NUCLOS-4807
								if (parameterJson.get("value") instanceof JsonObject) {
									JsonObject obj = parameterJson.getJsonObject("value");
									if (obj.containsKey("id") && !obj.isNull("id") && !obj.isEmpty()) { // json contains name and id
										params.put(dsParam.getParameter() + "Id", obj.getJsonNumber("id").longValue());
									} else if (obj.containsKey("id") && obj.isNull("id")) {
										params.put(dsParam.getParameter() + "Id", null);
									}
								}
								
							}
						}
					}
				}
				result.setParams(params);
				return result;
			}
		} catch (Exception ex) {
			LOG.warn("OutputFormat is not available: {}", ex.getMessage());
			LOG.debug(ex.getMessage(), ex);
		}
		
		return null;
	}

	public static OutputFormatRVO getOutputFormat(final PrintoutOutputFormat outputFormat, final DefaultReportVO reportVO, ReportFacadeLocal reportFacade) {
		String outputFormatFqn = outputFormat.getOutputFormatId();
		UID outputFormatUid = Rest.translateFqn(E.REPORTOUTPUT, outputFormatFqn);
		try {
			ReportOutputVO reportoutputvo = reportFacade.getReportOutput(outputFormatUid);
			if (reportoutputvo != null) {
				OutputFormatRVO result = new OutputFormatRVO(reportoutputvo, ObjectUtils.defaultIfNull(reportoutputvo.getDatasourceUID(), reportVO.getDatasourceId()));

				Map<String, Object> params = new HashMap<String, Object>();
				List<PrintoutOutputFormatParameter> parameters = outputFormat.getParameters();
				if (parameters != null && !parameters.isEmpty()) {
					List<DatasourceParameterVO> dsParams = Rest.facade().getParameterListForDataSource(reportoutputvo.getDatasourceUID());

					for (DatasourceParameterVO dsParam : dsParams) {
						RDataType rdatatype = new RDataType(dsParam.getDatatype(), 2);

						for (PrintoutOutputFormatParameter parameter : parameters) {
							if (RigidUtils.equal(parameter.getParameter(), dsParam.getParameter())) {

								Object value = parameter.getValue();
								params.put(dsParam.getParameter(), value);

								// NUCLOS-4807
								//TODO
							}
						}
					}
				}
				result.setParams(params);
				return result;
			}
		} catch (Exception ex) {
			LOG.warn("OutputFormat is not available: {}", ex.getMessage());
			LOG.debug(ex.getMessage(), ex);
		}

		return null;
	}
	
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	
	public Map<String, Object> getParams() {
		return params;
	}
	
	public UID getUID() {
		return (UID)reportOutputVO.getId();
	}
	
	public static class OutputFile {
		
		private final String boMetaId;
		private final String boId;
		private final String fileName;
		private final String fileId;
		private final IWebContext webContext;
		
		public OutputFile(String boMetaId, String boId, String fileName, String fileId, IWebContext webContext) {
			super();
			this.boMetaId = boMetaId;
			this.boId = boId;
			this.fileName = fileName;
			this.fileId = fileId;
			this.webContext = webContext;
		}
		
	}

	public void setBoId(String boId) {
		this.boId = boId;
	}

}
