package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordResetRVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String passwordResetCode;
	private String newPassword;

	public String getNewPassword() {
		return newPassword;
	}

	public String getPasswordResetCode() {
		return passwordResetCode;
	}
}
