package org.nuclos.server.rest.services.rvo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.json.JsonObjectBuilder;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.DefaultReportVO;

public class PrintoutRVO extends AbstractJsonRVO<UID> {
	
	private final DefaultReportVO reportVO;
	private List<OutputFormatRVO> outputFormats;
	
	private final String boId;
	private boolean boChanged = false;
	private CommandsRVO commands = null;

	public PrintoutRVO(String boId, DefaultReportVO reportVO) {
		super(E.REPORT);
		this.reportVO = reportVO;
		this.boId = boId;
	}

	public void addOutputFormat(OutputFormatRVO outputformat) {
		if (outputFormats == null) {
			 outputFormats = new ArrayList<>();
		}
		outputFormats.add(outputformat);
		outputFormats.sort(Comparator.comparing(OutputFormatRVO::getName));
		outputformat.setBoId(boId);
	}
	
	public List<OutputFormatRVO> getOutputFormats() {
		List<OutputFormatRVO> result = new ArrayList<OutputFormatRVO>();
		if (outputFormats != null) {
			for (JsonRVO format : outputFormats) {
				result.add((OutputFormatRVO) format);
			}
		}
		return result;
	}
	
	@Override
	public JsonObjectBuilder getJSONObjectBuilder() {
		NuclosJsonObjectBuilder json = new NuclosJsonObjectBuilder();
		
		Object pk = getPKForJson((UID)reportVO.getId());
		json.add("printoutId", pk);
		json.add("name", reportVO.getName());
		json.add("boChanged", hasBoChanged());
		if (commands != null) {
			json.add("commands", commands.toJsonBuilderObject().build());
		}

		addArray(json, "outputFormats", outputFormats);
		
		return json;
	}

	public boolean hasBoChanged() {
		return boChanged;
	}

	public void boChanged(final boolean boChanged) {
		this.boChanged = boChanged;
	}

	public CommandsRVO getCommands() {
		return commands;
	}

	public void setCommands(final CommandsRVO commands) {
		this.commands = commands;
	}
}
