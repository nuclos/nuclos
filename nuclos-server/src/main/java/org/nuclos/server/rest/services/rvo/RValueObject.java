//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuclos.api.NuclosImage;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.SF;
import org.nuclos.common.SFE;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDataLanguageLocalizedEntityEntry;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.File;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.schema.rest.StatusInfo;
import org.nuclos.schema.rest.StatusPath;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.i18n.DataLanguageUtils;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.rest.ejb3.IRValueObject;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.rest.misc.RestLinks.RestLink;
import org.nuclos.server.rest.misc.RestLinks.Verbs;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.common.collect.Lists;

public class RValueObject<PK> extends AbstractJsonRVO<PK> implements IRValueObject {

	/**
	 * temporary ID of the stateful cached EntityObjectVO in RestFacadeBean
	 */
	public static final UID TEMPORARY_ID_FIELD = new UID("nuclosTemporaryId");

	public static final UID TEMPORARY_DEPENDENTS_ALREADY_LOADED_FIELD = new UID("nuclosTemporaryDependentsAlreadyLoaded");

	public static final String TEMPORARY_ID_JSON_KEY = "temporaryId";

	public static final String SHADOW_ID_JSON_KEY = "shadowID";

	private static final Logger LOG = LoggerFactory.getLogger(RValueObject.class);

	private final StateCache stateCache;

	private final MetaProvider metaProvider;

	private final JsonFactory jsonFactory;

	private final EntityObjectVO<PK> eo;
	private final LinkedHashMap<String, Object> data;
	private LinkedHashMap<String, Object> values;
	private final LinkedHashMap<String, String> attributeColors = new LinkedHashMap<>();
	private Map<String, JsonArrayBuilder> arrays = null;
	private Set<String> imageAttributes;
	private Set<String> documentAttributes;
	private final UID sessionDataLanguageUID;

	private final JsonBuilderConfiguration jsonConfig;
	private final UsageProperties usageProperties;
	private final BoLinksFactory boLinksFactory;

	public RValueObject(
			EntityObjectVO<PK> eo,
			JsonBuilderConfiguration jsonConfig,
			UsageProperties usageProperties,
			BoLinksFactory boLinksFactory,
			UID sessionDataLanguageUID,
			JsonFactory jsonFactory,
			MetaProvider metaProvider,
			StateCache stateCache
	) throws CommonBusinessException {
		super(metaProvider.getEntity(eo.getDalEntity()));
		this.jsonFactory = jsonFactory;
		this.metaProvider = metaProvider;
		this.stateCache = stateCache;
		this.jsonConfig = jsonConfig;
		this.eo = eo;
		this.usageProperties = usageProperties;
		this.sessionDataLanguageUID = sessionDataLanguageUID;
		if (boLinksFactory != null) {
			this.boLinksFactory = boLinksFactory;
		} else {
			EntityMeta<?> eMeta = getEntityMeta();
			if (eMeta.isDynamicTasklist()) {
				final TasklistDefinition tlDef = (TasklistDefinition) eMeta.getProperty(EntityMeta.PROPERTY.TASKLIST_DEFINITION);
				if (tlDef == null) {
					throw new NuclosFatalException("No TASKLIST_DEFINITION property set");
				}
				UID detailEntityUID = tlDef.getTaskEntityUID();
				if (detailEntityUID == null) {
					final UID refToEntityMeta = tlDef.getDynamicTasklistEntityFieldUid();
					if (refToEntityMeta == null) {
						throw new NuclosFatalException("TasklistDefinition without information to show details?");
					}
					FieldMeta<?> refToEntityMetaField = (FieldMeta<?>) eMeta.getProperty(EntityMeta.PROPERTY.TASKLIST_REF_TO_ENTITYMETA_FIELD);
					if (refToEntityMetaField == null) {
						throw new NuclosFatalException("No TASKLIST_REF_TO_ENTITYMETA_FIELD property set");
					}
					final Object refToEntityMetaValue = eo.getFieldValue(refToEntityMetaField.getUID());
					if (refToEntityMetaValue == null) {
						throw new NuclosFatalException("Tasklist eo with ID " + eo.getPrimaryKey() + " contains no reference to detail entity meta information");
					}
					detailEntityUID = UID.parseUID(refToEntityMetaValue.toString());
				}
				final String detailEntityFQN = Rest.translateUid(E.ENTITY, detailEntityUID);
				this.boLinksFactory = new DefaultBoLinksFactory(detailEntityFQN);
			} else {
				this.boLinksFactory = new DefaultBoLinksFactory(this.usageProperties.sTranslatedEntity);
			}
		}

		this.data = new LinkedHashMap<>();

		this.data.put("version", eo.getVersion());

		createObject();

		Object tempId = eo.getFieldValue(TEMPORARY_ID_FIELD);
		if (tempId != null) {
			data.put(TEMPORARY_ID_JSON_KEY, tempId);
		} else {
			addStates();
			addGenerations();
			addCommands();
		}
	}

	/**
	 * Adds all commands which are to be sent to the client to this value object.
	 */
	private void addCommands() {
		if (jsonConfig.skipStatesAndGenerations) {
			// NUCLOS-6317 1)
			return;
		}
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		boolean bRequestHasCommands = Collections.list(request.getAttributeNames()).stream().anyMatch("CMD"::equals);

		CommandsRVO cmds = new CommandsRVO(new ArrayList<>());

		if (bRequestHasCommands) {
			final ArrayList<Serializable> commands = (ArrayList<Serializable>) request.getAttribute("CMD");
			if (commands != null) {
				for (Serializable object : commands) {
					CommandsRVO.addCommand(cmds, object);
				}
			}
		}

		this.data.put("commands", cmds.toJsonBuilderObject());
	}

	private void addStates() {
		if (jsonConfig.skipStatesAndGenerations) {
			//NUCLOS-5417 h)
			return;
		}
		if (usageProperties.bHasStateModel && eo.canStateChange()) {
			List<StateVO> states = usageProperties.getStatesSorted();

			if (!states.isEmpty()) {
				JsonArrayBuilder array = Json.createArrayBuilder();

				final UID stateUID = eo.getFieldUid(SF.STATE_UID.getUID(eo.getDalEntity()));
				final Locale currentLocale = Rest.facade().getCurrentLocale();

				List<UID> lstNextAllowedPathStateUIDs = null;
				List<UID> lstBackAllowedPathStateUIDs = null;
				if (stateUID != null && currentLocale != null) {
					lstNextAllowedPathStateUIDs = getAllowedPathStateUIDs(stateUID, PathDirection.NEXT, currentLocale);
					lstBackAllowedPathStateUIDs = getAllowedPathStateUIDs(stateUID, PathDirection.BACK, currentLocale);
				}
				UID firstNextAllowedUID = getFirstUID(lstNextAllowedPathStateUIDs);
				UID firstBackAllowedUID = getFirstUID(lstBackAllowedPathStateUIDs);
				for (StateVO svo : states) {
					StateRVO srvo = new StateRVO(svo, usageProperties.sTranslatedEntity, StringUtils.defaultIfEmpty(eo.getPKStringRepresentation(),
							eo.getFieldValue(TEMPORARY_ID_FIELD, String.class)), usageProperties.usage.getStatusUID(),
							Objects.equals(svo.getId(), firstNextAllowedUID) ? removeUID(lstNextAllowedPathStateUIDs, firstNextAllowedUID) : null,
							Objects.equals(svo.getId(), firstBackAllowedUID) ? removeUID(lstBackAllowedPathStateUIDs, firstBackAllowedUID) : null
					);
					array.add(jsonFactory.buildJsonObject(srvo, usageProperties.webContext));
				}

				if (arrays == null) {
					arrays = new HashMap<>();
				}

				arrays.put("nextStates", array);
			}
		}
	}

	private static List<UID> removeUID(List<UID> lst, UID uid) {
		List<UID> result = null;
		if (lst != null) {
			result = new ArrayList<>(lst);
			result.remove(uid);
		}
		return result;
	}

	private static UID getFirstUID(List<UID> lst) {
		if (lst != null && !lst.isEmpty()) {
			return lst.get(0);
		}
		return null;
	}

	private enum PathDirection {NEXT, BACK}

	private List<UID> getAllowedPathStateUIDs(UID currentStateUID, PathDirection pathDirection, Locale currentLocale) {
		List<UID> result = new ArrayList<>();
		try {
			final StatusPath statusPath = stateCache.getStatusPath(currentStateUID, 64, currentLocale);
			List<StatusInfo> infos = statusPath.getInfos();
			if (pathDirection == PathDirection.BACK) {
				infos = Lists.reverse(infos);
			}
			UID checkPathUID = null;
			for (StatusInfo i : infos) {
				final UID pathItemUID = Rest.translateFqn(E.STATE, i.getStateId());
				if (checkPathUID != null) {
					final UsageCriteria ucCheckPathItem = new UsageCriteria(usageProperties.usage.getEntityUID(), usageProperties.usage.getProcessUID(), checkPathUID, usageProperties.usage.getCustom());
					boolean bStateChangeAllowed = false;
					// getSubsequentStates contains only granted state changes
					for (StateVO subsequentState : Rest.facade().getSubsequentStates(ucCheckPathItem)) {
						if (subsequentState.getId().equals(pathItemUID)) {
							bStateChangeAllowed = true;
							checkPathUID = pathItemUID;
							result.add(pathItemUID);
							break;
						}
					}
					if (!bStateChangeAllowed) {
						// do not add any following path elements
						checkPathUID = null;
					}
				} else {
					// the current state -> start with the allowed check
					if (currentStateUID.equals(pathItemUID)) {
						checkPathUID = pathItemUID;
					}
				}
			}
		} catch (Exception e) {
			// no path
		}
		return result;
	}

	private void addGenerations() {
		if (jsonConfig.skipStatesAndGenerations) {
			//NUCLOS-6317 1)
			return;
		}

		UID mandatorUID = null;
		if (getEntityMeta().isMandator()) {
			mandatorUID = eo.getFieldUid(SF.MANDATOR_UID);
		}
		List<GeneratorActionRVO> generations = usageProperties.getGenerationsSorted(mandatorUID);

		if (!generations.isEmpty()) {
			JsonArrayBuilder array = Json.createArrayBuilder();

			for (GeneratorActionRVO generation : generations) {
				array.add(
						generation.getJsonObjectBuilder(
								eo.getPKStringRepresentation(),
								eo.getVersion(),
								usageProperties.webContext
						)
				);
			}

			if (arrays == null) {
				arrays = new HashMap<>();
			}

			arrays.put("generations", array);
		}
	}

	static final SimpleDateFormat FORMATTS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	static final SimpleDateFormat FORMATDATE = new SimpleDateFormat("yyyy-MM-dd");
	private void addAttributeValue(FieldMeta<?> fm) {
		UID field = fm.getUID();
		Object obj = eo.getFieldValue(field);
		Object pk = getPKForJson(eo.getPrimaryKey());

		boolean isReference = fm.getForeignEntity() != null;
		if (isReference && SF.STATENUMBER.checkField(fm.getFieldName())) {
			isReference = false;
		}

		boolean isAttributeColor = fm.getUID().toString().endsWith(FieldMeta.ATTRIBUTE_COLOR_DATASOURCE_SUFFIX);

		// i18n basic support
		final Object i18nValue;
		if (isReference && metaProvider.checkEntityField(DataLanguageUtils.extractForeignEntityReference(fm.getForeignEntity()))) {
			// first look up, if there is a translation specific to the exact field (NUCLOS-10841)
			if (eo.getFieldValue(DataLanguageUtils.extractForeignEntityReference(fm.getForeignEntity(), fm.getUID())) != null) {
				i18nValue = eo.getFieldValue(DataLanguageUtils.extractForeignEntityReference(fm.getForeignEntity(), fm.getUID()));
			} else {
				i18nValue = eo.getFieldValue(DataLanguageUtils.extractForeignEntityReference(fm.getForeignEntity()));
			}
			if (i18nValue != null) {
				obj = i18nValue;
			}
		} else if (eo.getDataLanguageMap() != null && sessionDataLanguageUID != null && eo.getDataLanguageMap().getDataLanguage(sessionDataLanguageUID) != null) {
			i18nValue = eo.getDataLanguageMap().getDataLanguage(sessionDataLanguageUID).getValue(DataLanguageUtils.extractFieldUID(field));
			if (i18nValue != null) {
				obj = i18nValue;
			}
		}

		if (obj instanceof NuclosImage) {
			return;
		}

		if (obj instanceof Double) {
			String sDouble = String.valueOf(obj);
			obj = new BigDecimal(sDouble).setScale(fm.getPrecision(), RoundingMode.HALF_UP);
		}

		String jsonPropertyName = getJsonPropertyName(fm);

		if (!isReference) {
			if (obj == null) {
				if (pk == null) {
					// bo with defaults only
					if (SF.STATENUMBER.checkField(fm.getFieldName()) ||
							SF.STATE.checkField(fm.getFieldName()) ||
							SF.STATEICON.checkField(fm.getFieldName()) ||
							SF.CREATEDAT.checkField(fm.getFieldName()) ||
							SF.CREATEDBY.checkField(fm.getFieldName()) ||
							SF.CHANGEDBY.checkField(fm.getFieldName()) ||
							SF.VERSION.checkField(fm.getFieldName()) ||
							SF.CHANGEDAT.checkField(fm.getFieldName())) {
						return;
					}
				} else {
					//TODO: Evaluate whether to put the NULL-Value or skip this field completely
					return;
//					obj = JsonValue.NULL;
				}
			}

			if (!isAttributeColor) {

				if (obj instanceof Timestamp || obj instanceof DateTime) {
					String stringValue = FORMATTS.format(obj instanceof DateTime ? ((DateTime) obj).getDate() : obj);
					values.put(jsonPropertyName, stringValue);
				} else if (obj instanceof Date) {
					String stringValue = FORMATDATE.format(obj);
					values.put(jsonPropertyName, stringValue);
				} else if (obj instanceof NuclosPassword) {
					values.put(jsonPropertyName, ((NuclosPassword) obj).getValue());
				} else {
					values.put(jsonPropertyName, obj);
				}
			} else if (obj instanceof String) {
				final FieldMeta<?> baseField = metaProvider.getEntityField(UID.parseUID(StringUtils.substring(fm.getUID().toString(), 0, -1 * (FieldMeta.ATTRIBUTE_COLOR_DATASOURCE_SUFFIX.length()))));
				attributeColors.put(getJsonPropertyName(baseField), (String) obj);
			}


		} else {
			Object fieldid = eo.getFieldId(field);
			JsonObjectBuilder json = Json.createObjectBuilder();

			if (obj instanceof File) {
				final String filetype = ((File) obj).getFiletype();
				if (!filetype.equals(File.TYPE_UNKNOWN)) {
					json.add("fileType", filetype);
				}
			}

			if (fieldid instanceof Long) {
				json.add("id", (Long) fieldid);
				json.add("name", obj != null ? obj.toString() : "");
				obj = json;

			} else {
				fieldid = eo.getFieldUid(field);
				if (fieldid == null) {
					json.add("id", JsonValue.NULL);
					json.add("name", JsonValue.NULL);
					obj = json;
				}
				if (fieldid instanceof UID) {
					EntityMeta<?> translationEntity = metaProvider.getEntity(fm.getForeignEntity());
					json.add("id", Rest.translateUid(translationEntity, (UID) fieldid));
					json.add("name", obj != null ? obj.toString() : "");
					obj = json;

				}
			}

			values.put(jsonPropertyName, obj);
		}
	}

	private void addAttributeValues(List<FieldMeta<?>> fields) {
		this.values = new LinkedHashMap<>();

		for (FieldMeta<?> fm : fields) {
			addAttributeValue(fm);
		}
	}

	private void createObject() {

		AttributesAndRestrictions attrsAndRestrictions = usageProperties.getAttributesAndRestrictions(jsonConfig.getAttributes());
		if (jsonConfig.noRestrictions) {
			this.values = new LinkedHashMap<>();
			Map<UID, Object> mapFields = eo.getFieldValues();
			for (UID fieldUid : mapFields.keySet()) {
				try {
					FieldMeta<?> fm = Rest.getEntityField(fieldUid);
					addAttributeValue(fm);
				} catch (CommonFatalException cfe) {
					//Happens typically with system fields like "version"
					LOG.debug(cfe.getMessage(), cfe);
				}
			}
		} else {
			List<FieldMeta<?>> eoFieldsMeta = eo.getFieldValues().keySet().stream()
					.map(uid -> {
						if (metaProvider.checkEntityField(uid)) {
							return metaProvider.getEntityField(uid);
						} else {
							return null;
						}
					})
					.filter(metafield -> metafield != null)
					.collect(Collectors.toList());

			addAttributeValues(ListUtils.intersection(attrsAndRestrictions.lstAttributesReadAllowed, eoFieldsMeta));
		}
		addRowColorIfThere(attrsAndRestrictions);
		addImages(attrsAndRestrictions);

		if (arrays == null) {
			arrays = new HashMap<>();
		}

		if (jsonConfig.withTitleAndInfo) {
			String title = Rest.getTreeViewLabel(eo.getFieldValues(), eo.getDalEntity());
			String info = Rest.getTreeViewDescription(eo.getFieldValues(), eo.getDalEntity());

			if (title.equals(info)) {
				info = "";
			}
			data.put("title", title);
			data.put("info", info);
		}

		for (FieldMeta<?> fm : attrsAndRestrictions.lstAttributesReadAllowed) {
			Object obj = eo.getFieldValue(fm.getUID());
			if (obj == null) {
				continue;
			}

			if (obj instanceof NuclosImage) {
				if (imageAttributes == null) {
					imageAttributes = new HashSet<>(1);
				}
				imageAttributes.add(Rest.translateUid(E.ENTITYFIELD, fm.getUID()));
				continue;
			}

			if (obj instanceof GenericObjectDocumentFile) {
				if (documentAttributes == null) {
					documentAttributes = new HashSet<>(1);
				}
				documentAttributes.add(Rest.translateUid(E.ENTITYFIELD, fm.getUID()));
			}
		}
	}

	private String getJsonPropertyName(FieldMeta<?> fm) {
		return NuclosBusinessObjectBuilder.getFieldNameForFqn(fm);
	}

	private void addRowColorIfThere(AttributesAndRestrictions attrsAndRestrictions) {
		if (attrsAndRestrictions != null && attrsAndRestrictions.attributeRowColor != null) {
			Object o = eo.getFieldValue(attrsAndRestrictions.attributeRowColor.getUID());
			if (o instanceof String) {
				data.put("rowcolor", o);
			}
		}
	}

	private void addImages(AttributesAndRestrictions attrsAndRestrictions) {
		for (FieldMeta<?> fm : attrsAndRestrictions.lstAttributesReadAllowed) {
			UID field = fm.getUID();
			Object obj = eo.getFieldValue(field);


			if (obj instanceof NuclosImage || DataServiceHelper.isImageBytes(obj)) {
				if (imageAttributes == null) {
					imageAttributes = new HashSet<>(1);
				}
				imageAttributes.add(Rest.translateUid(E.ENTITYFIELD, fm.getUID()));
			}
		}
	}

	/**
	 * Some types of BO are not 'detailable' like dynamic or chart BOs.
	 * The ID is not unique and therefore a precise select is not possible.
	 */
	private boolean isDetailableBo() {
		EntityMeta<?> eMeta = metaProvider.getEntity(usageProperties.usage.getEntityUID());
		return !eMeta.isDynamic() && !eMeta.isChart();
	}

	@Override
	public JsonObjectBuilder getJSONObjectBuilder() {
		NuclosJsonObjectBuilder json = new NuclosJsonObjectBuilder();

		Object pk = getPKForJson(eo.getPrimaryKey());
		Object tempId = eo.getFieldValue(TEMPORARY_ID_FIELD);

		json.add("boId", pk);
		json.add("boMetaId", usageProperties.sTranslatedEntity);
		//canStateChange not needed: Links are only generated if allowed
		//json.add("canStateChange", eo.canStateChange());

		boolean canWrite = usageProperties.isUpdateAllowed && eo.canWrite();
		boolean canDelete = usageProperties.isDeleteAllowed && eo.canDelete();

		this.boLinksFactory.clear();
		if (pk == null) {
			if (usageProperties.isInsertAllowed) {
				boLinksFactory.addInsert();
			}
		} else {
			if (jsonConfig.isWithDetailLink() && isDetailableBo()) {
				RestLink link = boLinksFactory.addSelf(pk);
				if (link != null && !link.isProtectVerbs()) {
					if (canWrite && usageProperties.isWriteAllowed()) {
						link.setVerbPut();
					} else {
						canWrite = false;
					}
					if (canDelete && usageProperties.isDeleteAllowed()) {
						link.setVerbDelete();
					} else {
						canDelete = false;
					}
				}
			}
		}

		json.add("canWrite", canWrite);
		json.add("canDelete", canDelete);

		if (usageProperties.usage.getStatusUID() != null) {
			StateVO state;
			StateModelVO stateModel;
			try {
				state = stateCache.getStateById(usageProperties.usage.getStatusUID());
				stateModel = stateCache.getModelByState(state);
			} catch (CommonFinderException e) {
				throw new CommonFatalException(e);
			}
			json.add("showStateNumerals", stateModel.getShowNumeral());
			json.add("showStateIcons", stateModel.getShowIcon());
		}

		if (jsonConfig.withMetaLink || pk == null) {
			boLinksFactory.addMeta();
		}

		for (String key : data.keySet()) {
			json.add(key, data.get(key));
		}

		if (values != null) {
			NuclosJsonObjectBuilder jsonValues = new NuclosJsonObjectBuilder();
			for (String key : values.keySet()) {
				Object value = values.get(key);
				if (value instanceof Date) {
					String stringValue = Rest.getDateFormat().format(value);
					jsonValues.add(key, stringValue);

				} else if (value instanceof GenericObjectDocumentFile) {
					GenericObjectDocumentFile godf = (GenericObjectDocumentFile) value;
					JsonObjectBuilder jsonb = Json.createObjectBuilder();
					jsonb.add("name", godf.getFilename());
					jsonValues.add(key, jsonb);

				} else {
					jsonValues.add(key, value);
				}
			}
			json.add("attributes", jsonValues);

			AttributesAndRestrictions fieldsAndRestrictions = usageProperties.getAttributesAndRestrictions(jsonConfig.getAttributes());
			if (fieldsAndRestrictions.restrictions != null) {
				json.add("attrRestrictions", fieldsAndRestrictions.restrictions);
			}

			if (!attributeColors.isEmpty()) {
				final JsonObjectBuilder jobAttributeColors = Json.createObjectBuilder();
				for (String attribute : attributeColors.keySet()) {
					jobAttributeColors.add(attribute, attributeColors.get(attribute));
				}
				json.add("attrColors", jobAttributeColors.build());
			}
		}
		if (eo.getDataLanguageMap() != null) {
			NuclosJsonObjectBuilder jsonLanguagesMap = new NuclosJsonObjectBuilder();
			for (Map.Entry<UID, IDataLanguageLocalizedEntityEntry> e : eo.getDataLanguageMap().getLanguageMap().entrySet()) {
				NuclosJsonObjectBuilder jsonLanguageBuilder = new NuclosJsonObjectBuilder();
				String languageId = Rest.facade().translateUid(getEntityMeta(), e.getKey());
				Map<UID, String> valueMap = e.getValue().getValueMap();
				for (Map.Entry<UID, String> translationEntry : valueMap.entrySet()) {
					UID originalFieldUID = DataLanguageUtils.extractOriginalFieldUID(translationEntry.getKey());
					UID originalFieldUIDmod = DataLanguageUtils.extractFlaggedFieldFromLangField(translationEntry.getKey());
					if (metaProvider.checkEntityField(originalFieldUID) && metaProvider.checkEntityField(originalFieldUIDmod)) {
						String attributeFqn = metaProvider.getEntityField(originalFieldUID).getFieldName();

						Object value = translationEntry.getValue();
						jsonLanguageBuilder.add(attributeFqn, value);
					}
				}
				jsonLanguagesMap.add(languageId, jsonLanguageBuilder);
			}
			json.add("attrDatalanguages", jsonLanguagesMap);
		}

		if (imageAttributes != null) {
			JsonObjectBuilder images = Json.createObjectBuilder();
			for (String imageAttrId : imageAttributes) {
				FieldMeta<?> fm = metaProvider.getEntityField(Rest.translateFqn(E.ENTITYFIELD, imageAttrId));
				String rel = getJsonPropertyName(fm);
				/*
				Version Information will be a sha1 hex value representing the actual value.
				If it changes the webclient will request the correct new file, for null values it will create a cache from empty bytes.
				Like Git, we only use the first 8 characters.
				 */
				String versionInformation = Optional.ofNullable(eo.getFieldValue(fm.getUID())).map(nuclosFileImage -> {
					if (nuclosFileImage instanceof NuclosImage) {
						return DigestUtils.sha1Hex(((NuclosImage) nuclosFileImage).getContent());
					} else if (nuclosFileImage instanceof byte[]) {
						return DigestUtils.sha1Hex((byte[]) nuclosFileImage);
					}
					return DigestUtils.sha1Hex("");
				}).orElse(DigestUtils.sha1Hex("")).substring(0, 8);

				if (metaProvider.getEntity(fm.getEntity()).isDatasourceBased()) {
					boLinksFactory.addImage(rel, imageAttrId, pk == null ? tempId : pk, versionInformation, usageProperties.sTranslatedEntity);
				} else {
					boLinksFactory.addImage(rel, imageAttrId, pk == null ? tempId : pk, versionInformation);
				}
			}

			boLinksFactory.buildImageJSON(images, usageProperties.webContext);
			json.add("attrImages", images);
		}

		if (documentAttributes != null) {
			JsonObjectBuilder documents = Json.createObjectBuilder();
			for (String docAttrId : documentAttributes) {
				FieldMeta<?> fm = metaProvider.getEntityField(Rest.translateFqn(E.ENTITYFIELD, docAttrId));
				String rel = getJsonPropertyName(fm);
				boLinksFactory.addDocument(rel, docAttrId, pk == null ? tempId : pk);
			}

			boLinksFactory.buildDocumetJSON(documents, usageProperties.webContext);
			json.add("attrDocuments", documents);
		}

		if (pk != null) {
			if (Rest.facade().hasPrintouts(usageProperties.usage, pk)) {
				boLinksFactory.addPrintout(usageProperties.sTranslatedEntity, pk);
			}
			boLinksFactory.addClone(pk);
		}

		if ((jsonConfig.withLayoutLink || pk == null) && usageProperties.webLayout != null) {

			if (usageProperties.webLayout.getLayoutInfo().getLayoutUID() != null) {
				boLinksFactory.addLayout(Rest.translateUid(E.LAYOUT, usageProperties.webLayout.getLayoutInfo().getLayoutUID()));
				boLinksFactory.addSubforminfo(Rest.translateUid(E.LAYOUT, usageProperties.webLayout.getLayoutInfo().getLayoutUID()), pk);
			}

			Boolean bWriteAllowedForPk = null;
			Object pkForSubs = pk;
			if (pkForSubs == null) {
				pkForSubs = data.get(TEMPORARY_ID_JSON_KEY);
				bWriteAllowedForPk = Boolean.TRUE;
			}

			JsonObjectBuilder subBos = usageProperties.getSubBos(pkForSubs, bWriteAllowedForPk);
			if (subBos != null) {
				json.add("subBos", subBos);
			}

		}

		if (usageProperties.usage.getStatusUID() != null) {
			StateVO state = null;
			try {
				state = stateCache.getStateById(usageProperties.usage.getStatusUID());
			} catch (CommonFinderException e) {
				throw new CommonFatalException(e);
			}
			final String stateId = Rest.translateUid(E.STATE, usageProperties.usage.getStatusUID());

			NuclosImage image = state.getIcon();
			if (image != null) {
				boLinksFactory.addStateIcon(stateId);
			}

			if (pk != null) {
				boLinksFactory.addObjectPath(stateId, pk);
			}
			try {
				if (stateCache.hasModelDefaultPathByStateId(state.getId())) {
					boLinksFactory.addDefaultPath(stateId);
				}
			} catch (CommonFinderException e) {
				throw new CommonFatalException(e);
			}

			if (state.getTabbedPaneName() != null) {
				json.add("defaultTabbedPaneName", state.getTabbedPaneName());
			}
		}

		if (arrays != null) {
			for (String key : arrays.keySet()) {
				json.add(key, arrays.get(key));
			}
		}

		if (eo.getShadowID() != null) {
			json.add("shadowID", eo.getShadowID());
		}

		if (eo.getBusinessError() != null) {
			json.add("businessError", eo.getBusinessError());
		}

		if (jsonConfig.withLock && pk != null) {
			if (eo.getFieldUid(SFE.OWNER_UID) != null) {
				if (Rest.facade().isUnlockAllowed(usageProperties.webContext.getBOMeta().getUID())) {
					boLinksFactory.addUnlock(pk);
				}
			}
		}

		if (jsonConfig.isWithDetailLink()) {
			String detailBoMetaId = Rest.translateUid(E.ENTITY, eo.getDalEntity());
			boLinksFactory.addDetail(detailBoMetaId, pk);
		}

		boLinksFactory.buildJSON(json, usageProperties.webContext);

		return json;
	}

	static class AttributesAndRestrictions {
		private final List<FieldMeta<?>> lstAttributesReadAllowed;
		private final JsonObject restrictions;
		private final FieldMeta<?> attributeRowColor;

		AttributesAndRestrictions(List<FieldMeta<?>> lstAttributesReadAllowed, JsonObject jsonObject) {
			this.lstAttributesReadAllowed = lstAttributesReadAllowed;
			this.restrictions = jsonObject;
			FieldMeta<?> arc = null;
			for (FieldMeta<?> fm : lstAttributesReadAllowed) {
				if (fm.isNuclosRowColor()) {
					arc = fm;
					break;
				}
			}
			this.attributeRowColor = arc;

			//Note: NUCLOS-4099 requests that the NuclosRowColor is not displayed in the web-client.
			//TODO: Evaluate if this (within the REST-Service) is the right place to filter this attribute out of the data
			if (arc != null) {
				this.lstAttributesReadAllowed.remove(arc);
			}
		}
	}

	public interface BoLinksFactory {

		void buildJSON(JsonObjectBuilder boJson, IWebContext context);

		void buildImageJSON(JsonObjectBuilder imgJson, IWebContext context);

		void buildDocumetJSON(JsonObjectBuilder docJson, IWebContext context);

		RestLink addDocument(String rel, String docAttrId, Object boId);

		RestLink addImage(
				String rel,
				String imgAttrId,
				Object boId,
				Object version
		);

		RestLink addImage(
				String rel,
				String imgAttrId,
				Object boId,
				Object version,
				String boMetaId
		);

		RestLink addStateIcon(String stateId);

		RestLink addDefaultPath(String stateId);

		RestLink addObjectPath(String stateId, Object pk);

		RestLink addLayout(String layoutId);

		RestLink addSubforminfo(String layoutId, Object pk);

		RestLink addPrintout(String sTranslatedEntity, Object pk);

		RestLink addUnlock(Object pk);

		RestLink addSelf(Object selfId);

		RestLink addDetail(String detailBoMetaId, Object selfId);

		RestLink addInsert();

		RestLink addClone(final Object boId);

		RestLink addMeta();

		void clear();
	}

	public static abstract class AbstractBoLinksFactory implements BoLinksFactory {

		protected final RestLinks links = new RestLinks();
		protected final RestLinks imgLinks = new RestLinks();
		protected final RestLinks docLinks = new RestLinks();

		@Override
		public void buildJSON(JsonObjectBuilder boJson, IWebContext context) {
			links.setParent(boJson);
			links.buildJson(context);
		}

		@Override
		public void buildImageJSON(JsonObjectBuilder imgJson, IWebContext context) {
			imgLinks.setParent(imgJson);
			imgLinks.buildJson(context);
		}

		@Override
		public void buildDocumetJSON(JsonObjectBuilder docJson, IWebContext context) {
			docLinks.setParent(docJson);
			docLinks.buildJson(context);
		}

		@Override
		public void clear() {
			links.clear();
			imgLinks.clear();
			docLinks.clear();
		}
	}

	private static class DefaultBoLinksFactory extends AbstractBoLinksFactory {

		private final String boMetaId;

		DefaultBoLinksFactory(String boMetaId) {
			this.boMetaId = boMetaId;
		}

		@Override
		public RestLink addSelf(Object selfId) {
			return links.addLinkHref("self", "bo", Verbs.GET, boMetaId, selfId);
		}

		@Override
		public RestLink addDetail(final String detailBoMetaId, final Object selfId) {
			return null;
		}

		@Override
		public RestLink addMeta() {
			return links.addLink("boMeta", Verbs.GET, boMetaId);
		}

		@Override
		public RestLink addLayout(String layoutId) {
			return links.addLinkHref("layout", "weblayoutCalculated", Verbs.GET, layoutId);
		}

		@Override
		public RestLink addSubforminfo(String layoutId, Object pk) {
			// There is currently no rest service for 'subforminfo'
			/*if (layoutId != null && pk != null) {
				return links.addLink("subforminfo", Verbs.GET, layoutId, pk.toString());
			}*/
			return null;
		}

		@Override
		public RestLink addStateIcon(String stateId) {
			return links.addLink("stateIcon", Verbs.GET, stateId);
		}

		@Override
		public RestLink addDefaultPath(String stateId) {
			return links.addLink("defaultPath", Verbs.GET, stateId);
		}

		@Override
		public RestLink addObjectPath(String stateId, Object pk) {
			return links.addLink("objectPath", Verbs.GET, stateId, pk.toString());
		}

		@Override
		public RestLink addImage(
				final String rel,
				final String imgAttrId,
				final Object boId,
				final Object version
		) {
			return addImage(
					rel,
					imgAttrId,
					boId,
					version,
					boMetaId
			);
		}

		@Override
		public RestLink addImage(
				final String rel,
				final String imgAttrId,
				final Object boId,
				final Object version,
				final String boMetaId
		) {
			return imgLinks.addLinkHref(
					rel,
					"boImageValue",
					Verbs.GET,
					boMetaId,
					boId,
					imgAttrId,
					version
			);
		}

		@Override
		public RestLink addDocument(String rel, String docAttrId, Object boId) {
			return docLinks.addLinkHref(rel, "boDocumentValue", Verbs.GET, boMetaId, boId, docAttrId);
		}

		@Override
		public RestLink addPrintout(String sTranslatedEntity, Object pk) {
			return links.addLinkHref("printouts", "boPrintoutList", Verbs.GET, sTranslatedEntity, pk);
		}

		@Override
		public RestLink addClone(final Object boId) {
			return links.addLinkHref("clone", "boclone", Verbs.GET, boMetaId, boId);
		}

		@Override
		public RestLink addInsert() {
			return links.addLinkHref("insert", "bos", Verbs.POST, boMetaId);
		}

		@Override
		public RestLink addUnlock(Object boId) {
			return links.addLinkHref("lock", "bos/{boMetaId}/{boId}/lock", Verbs.DELETE, boMetaId, boId);
		}

	}
}
