package org.nuclos.server.rest.services.rvo;

import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.statemodel.valueobject.StateVO;

public class StateRVO {
	private final StateVO stateVO;
	private final String boMetaId;
	private final String boId;
	private final boolean bNonStop;
	private final String transitionname;

	private final List<UID> lstNextAllowedPathStateUIDs;

	private final List<UID> lstBackAllowedPathStateUIDs;

	public StateRVO(StateVO stateVO, String boMetaId, String boId, UID sourceStateUid, List<UID> lstNextAllowedPathStateUIDs, List<UID> lstBackAllowedPathStateUIDs) {
		this.stateVO = stateVO;
		this.boMetaId = boMetaId;
		this.boId = boId;
		this.bNonStop = stateVO.isTransitionNonstop(sourceStateUid);
		this.transitionname = stateVO.getTransitionName(Rest.facade().getCurrentLocale());
		this.lstNextAllowedPathStateUIDs = lstNextAllowedPathStateUIDs;
		this.lstBackAllowedPathStateUIDs = lstBackAllowedPathStateUIDs;
	}

	public StateVO getStateVO() {
		return stateVO;
	}

	public String getBoMetaId() {
		return boMetaId;
	}

	public String getBoId() {
		return boId;
	}
	
	public boolean isNonStop() {
		return bNonStop;
	}

	public String getTransitionname() {
		return transitionname;
	}

	public List<UID> getNextAllowedPathStateUIDs() {
		return lstNextAllowedPathStateUIDs;
	}

	public List<UID> getBackAllowedPathStateUIDs() {
		return lstBackAllowedPathStateUIDs;
	}
}
