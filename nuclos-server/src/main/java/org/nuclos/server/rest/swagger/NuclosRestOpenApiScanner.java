//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.swagger;

import static org.nuclos.common.StreamUtils.not;

import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.nuclos.api.rule.CustomRestRule;

import io.swagger.v3.jaxrs2.integration.JaxrsApplicationAndAnnotationScanner;

public class NuclosRestOpenApiScanner extends JaxrsApplicationAndAnnotationScanner {

	@Override
	public Set<Class<?>> classes() {
		// filter CustomRestRules here (added later from org.nuclos.server.rest.swagger.NuclosRestReader)
		return super.classes()
				.parallelStream()
				.filter(not(CustomRestRule.class::isAssignableFrom))
				.collect(Collectors.toSet());
	}

}
