//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.restapis;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;

import org.apache.commons.io.FileUtils;
import org.nuclos.businessentity.Nuclet;
import org.nuclos.common.CryptUtil;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerComponent;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.openapitools.codegen.ClientOptInput;
import org.openapitools.codegen.DefaultGenerator;
import org.openapitools.codegen.config.CodegenConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RestObjectBuilder extends NuclosObjectBuilder {

	private static final Logger LOG = LoggerFactory.getLogger(RestObjectBuilder.class);

	public static final String DEFAULT_PACKAGE_WEBSERVICES = "org.nuclos.restapis";

	private final RestServiceObjectCompiler compiler;
	private final NucletDalProvider nucletDalProvider;

	private final NuclosJavaCompilerComponent nuclosJavaCompilerComponent;
	private final Map<UID, RestApiBuilderConfig> mpRestApiBuilderConfig = new HashMap<>();

	public RestObjectBuilder(final RestServiceObjectCompiler compiler, final NucletDalProvider nucletDalProvider, NuclosJavaCompilerComponent nuclosJavaCompilerComponent) {
		this.compiler = compiler;
		this.nucletDalProvider = nucletDalProvider;
		this.nuclosJavaCompilerComponent = nuclosJavaCompilerComponent;
	}

	public void createObjects(final ForkJoinPool builderThreadPool) throws CommonBusinessException, InterruptedException {
		List<NuclosBusinessJavaSource> sources = new ArrayList<>();

		List<EntityObjectVO<UID>> restapis = nucletDalProvider.getEntityObjectProcessor(E.RESTAPIS).getAll();

		for (EntityObjectVO<UID> restapi : restapis) {
			sources.addAll(generateRestAPIClasses(restapi));
		}

		if (sources.size() > 0) {
			compiler.compileSourcesAndJar(builderThreadPool, sources);
		}

	}

	private List<NuclosBusinessJavaSource> generateRestAPIClasses(final EntityObjectVO<UID> restapi) {
		try {

			checkJSONApi(restapi);

			String restapiName = restapi.getFieldValue(E.RESTAPIS.name);
			String modelPackage = getPackage(
					restapi,
					restapi.getFieldValue(E.RESTAPIS.modelPackage),
					restapiName + ".models"
			);
			String invokerPackage = getPackage(
					restapi,
					restapi.getFieldValue(E.RESTAPIS.invokerpackage),
					restapiName + ".invoker"
			);
			String apiPackage = getPackage(
					restapi,
					restapi.getFieldValue(E.RESTAPIS.apipackage),
					restapiName + ".api"
			);

			// call OpenAPI codegen and generate source files
			CodegenConfigurator codegenConfigurator = new CodegenConfigurator() {};
			codegenConfigurator.setInputSpec(mpRestApiBuilderConfig.get(restapi.getPrimaryKey()).getJson().getAbsolutePath());
			codegenConfigurator.setOutputDir(nuclosJavaCompilerComponent.getCodeGeneratorOutputPath().getAbsolutePath());
			codegenConfigurator.setLibrary(restapi.getFieldValue(E.RESTAPIS.httplib));
			codegenConfigurator.setModelPackage(modelPackage);
			codegenConfigurator.setInvokerPackage(invokerPackage);
			codegenConfigurator.setApiPackage(apiPackage);
			codegenConfigurator.addAdditionalProperty("bigDecimalAsString", Boolean.TRUE.equals(restapi.getFieldValue(E.RESTAPIS.bigdecimalasstring.getUID())));
			codegenConfigurator.addAdditionalProperty("serializableModel", Boolean.TRUE.equals(restapi.getFieldValue(E.RESTAPIS.serializablemodel.getUID())));
			codegenConfigurator.addAdditionalProperty("dateLibrary", restapi.getFieldValue(E.RESTAPIS.datelib));
			codegenConfigurator.addAdditionalProperty("localVariablePrefix", restapi.getFieldValue(E.RESTAPIS.variableprefix));
			codegenConfigurator.setGeneratorName("org.nuclos.server.customcode.codegenerator.NuclosJavaClientCodegen");

			ClientOptInput clientOptInput = codegenConfigurator.toClientOptInput();
			// add ignore file from resources to just get java files
			clientOptInput.getConfig().setIgnoreFilePathOverride(mpRestApiBuilderConfig.get(restapi.getPrimaryKey()).getSwaggerCgIgnore().getAbsolutePath());

			List<NuclosBusinessJavaSource> result = new ArrayList<>();
			Set<String> controllerApis = new HashSet<>();
			for (File sourcefile : new DefaultGenerator().opts(clientOptInput).generate()) {
				if (sourcefile.getAbsolutePath().endsWith(".java") && !sourcefile.getAbsolutePath().contains("src" + File.separator + "test" + File.separator +"java")) {
					// get package name from path
					String filePackage = sourcefile.getAbsolutePath().substring(
							sourcefile.getAbsolutePath().indexOf("java" + File.separator) + 5,
							sourcefile.getAbsolutePath().lastIndexOf(File.separator)
				).replaceAll(File.separator.equals("\\") ? "\\\\" : File.separator, ".");
					String className = sourcefile.getName().substring(0, sourcefile.getName().lastIndexOf('.'));
					String name = filePackage + "." + className;
					result.add(new NuclosBusinessJavaSource(
							name,
							NuclosCodegeneratorUtils.restSource(filePackage, className).toString(),
							readFile(sourcefile),
							false
					));

					// collect api controller names
					if (apiPackage.equals(filePackage)) {
						controllerApis.add(className);
					}
				}
				// remove generated file
				sourcefile.delete();
			}

			final File mainDir = new File(nuclosJavaCompilerComponent.getCodeGeneratorOutputPath().getAbsolutePath(), "src/main");
			FileUtils.deleteDirectory(mainDir);

			final File testDir = new File(nuclosJavaCompilerComponent.getCodeGeneratorOutputPath().getAbsolutePath(), "src/test");
			FileUtils.deleteDirectory(testDir);

			// generate adapter class
			result.add(
					generateAdapterClass(restapi, modelPackage, apiPackage, invokerPackage, controllerApis)
			);

			return result;

		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}
	}

	private NuclosBusinessJavaSource generateAdapterClass(EntityObjectVO<UID> restapi, String modelPackage, String apiPackage, String invokerPackage, Set<String> controllerApis) {
		String serviceName = getServiceName((String) restapi.getFieldValue(E.RESTAPIS.name.getUID()));
		serviceName = serviceName.substring(0, 1).toUpperCase() + serviceName.substring(1);

		StringBuilder sourceBuilder = new StringBuilder();

		sourceBuilder.append(
				String.format("package %s;\n\n", getMainClassPackage(restapi))
		);

		// imports
		sourceBuilder.append(
				String.format("import %s.ApiClient;\n", invokerPackage)
		);
		sourceBuilder.append(
				String.format("import %s.Configuration;\n", invokerPackage)
		);

		for (String apiCtrl : controllerApis) {
			sourceBuilder.append(
					String.format("import %s.%s;\n", apiPackage, apiCtrl)
			);
		}

		// class start
		sourceBuilder.append(
				String.format("\n\n/** Nuclos generated adapter class for RESTApi  \"%s\" **/\n", serviceName)
		);
		sourceBuilder.append(
				String.format("public class %sRESTAdapter {\n\n", serviceName)
		);

		// class members
		sourceBuilder.append("\tprivate ApiClient apiClient;\n");
		for (String apiCtrl : controllerApis) {
			sourceBuilder.append(
					String.format("\tprivate %s %s;\n", apiCtrl, apiCtrl.toLowerCase())
			);
		}

		// functions
		sourceBuilder.append("\n\n");

		// constructor
		sourceBuilder.append(
				String.format("\tpublic %sRESTAdapter(String newBasePath) {\n", serviceName)
		);

		sourceBuilder.append(
				"\t\tthis.apiClient = new ApiClient();\n\n\t\tif (newBasePath != null && !newBasePath.isEmpty()) {\n\t\t\tthis.apiClient.setBasePath(newBasePath);\n\t\t}\n\n"
		);

		sourceBuilder.append(
				"\t\tthis.updateClientInfo(this.apiClient);\n"
		);

		sourceBuilder.append(
				"\t}\n\n"
		);

		// updateClientInfo
		sourceBuilder.append(
				String.format("\tpublic %sRESTAdapter updateClientInfo(ApiClient client) {\n", serviceName)
		);

		sourceBuilder.append(
				"\t\tConfiguration.setDefaultApiClient(client);\n"
		);

		for (String apiCtrl : controllerApis) {
			sourceBuilder.append(
					String.format("\t\tthis.%s = new %s();\n", apiCtrl.toLowerCase(), apiCtrl)
			);
		}

		sourceBuilder.append("\n\t\treturn this;\n");

		sourceBuilder.append(
				"\t}\n\n"
		);

		// getApiClient
		sourceBuilder.append(
				"\tpublic ApiClient getApiClient() {\n"
		);

		sourceBuilder.append(
				"\t\treturn this.apiClient;\n"
		);

		sourceBuilder.append(
				"\t}\n\n"
		);

		// newInstance
		sourceBuilder.append(
				String.format("\tpublic static %sRESTAdapter newInstance(String optionalNewBasePath) {\n", serviceName)
		);

		sourceBuilder.append(
				String.format("\t\t%sRESTAdapter adapter = new %sRESTAdapter(optionalNewBasePath);\n", serviceName, serviceName)
		);

		sourceBuilder.append("\n\t\treturn adapter;\n");

		sourceBuilder.append(
				"\t}\n\n"
		);

		for (String apiCtrl : controllerApis) {
			// getApiClient
			sourceBuilder.append(
					String.format("\tpublic %s get%s() {\n", apiCtrl, apiCtrl)
			);

			sourceBuilder.append(
					String.format("\t\treturn this.%s;\n", apiCtrl.toLowerCase())
			);

			sourceBuilder.append(
					"\t}\n\n"
			);
		}

		// class end
		sourceBuilder.append(
				"}"
		);

		final String className = serviceName + "RESTAdapter";
		final String packageName = getMainClassPackage(restapi);

		return new NuclosBusinessJavaSource(
				packageName + "." + className,
				NuclosCodegeneratorUtils.restSource(packageName, className).toString(),
				sourceBuilder.toString(),
				false
		);
	}

	private void checkJSONApi(final EntityObjectVO<UID> restapi) throws IOException {
		RestApiBuilderConfig config = mpRestApiBuilderConfig.computeIfAbsent(restapi.getPrimaryKey(), RestApiBuilderConfig::new);
		DocumentFileBase gofile =
				(DocumentFileBase) restapi.getFieldValue(E.RESTAPIS.apifile.getUID());
		if (gofile == null) {
			throw new IOException(String.format("Webservice file for \"%s\" not found.", restapi.getFieldValue(E.RESTAPIS.name)));
		}
		byte[] apiContent = restapi.getFieldValue(E.RESTAPIS.content);
		if (apiContent == null) {
			apiContent = gofile.getContents();
		}
		config.setJson(new File(
				new File(nuclosJavaCompilerComponent.getRestOutputPath(), getServiceName(restapi.getFieldValue(E.RESTAPIS.name))),
				gofile.getFilename()
		));
		config.setRealDigest(CryptUtil.digestStringOf(apiContent));

		final File jsonDigest = new File(config.getJson().getParent(), config.getJson().getName() + ".sha1");
		if (jsonDigest.canRead()) {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(
					Files.newInputStream(jsonDigest.toPath()), StandardCharsets.UTF_8))) {
				final String oldDigest = reader.readLine();
				if (oldDigest != null) {
					LOG.debug("API digest: old: {} new: {}", oldDigest, config.getRealDigest());
					config.setRecompileIsNecessary(restapi.isFlagNew() || (!oldDigest.equals(config.getRealDigest()) || !restapi.isFlagUnchanged()) && !restapi.isFlagRemoved());

				}
			}
		}
		LOG.debug("recompileIsNecessary: {}", config.isRecompileIsNecessary());

		config.setRecompileIsNecessary(true);

		config.setPackageName(getMainClassPackage(restapi));
		// SourceFolder for
		config.setGeneratedSourceFolder(new File(nuclosJavaCompilerComponent.getRestOutputPath(), getServiceName(restapi.getFieldValue(E.RESTAPIS.name))));
		config.setSwaggerCgIgnore(new File(config.getGeneratedSourceFolder(), ".openapi-codegen-ignore"));
		if (config.isRecompileIsNecessary()) {
			deleteAndNew(config.getSwaggerCgIgnore());
			deleteAndNew(config.getJson());
			deleteAndNew(jsonDigest);


			try (OutputStream jsonOut = new BufferedOutputStream(Files.newOutputStream(config.getJson().toPath()));
				 BufferedWriter jsonDigestOut = new BufferedWriter(
						 new OutputStreamWriter(Files.newOutputStream(jsonDigest.toPath()), StandardCharsets.UTF_8)
				 );
				 BufferedWriter ignoreOut = new BufferedWriter(
						 new OutputStreamWriter(Files.newOutputStream(config.getSwaggerCgIgnore().toPath()), StandardCharsets.UTF_8)
				 );
			) {
				jsonOut.write(apiContent);
				jsonDigestOut.write(config.getRealDigest());
				ignoreOut.write(
						"**/*.xml\n" +
						"**/*.yaml\n" +
						"gradle\n" +
						"**/*.jar\n" +
						"build\n" +
						".gitignore\n" +
						".openapi-generator\n" +
						"gradlew\n" +
						"**/*.yml\n" +
						"build.sbt\n" +
						"**/*.gradle\n" +
						"**/*.properties\n" +
						"**/*.md\n" +
						"**/*.sh\n" +
						"**/*.bat\n" +
						"docs\n" +
						"!**/*.java");
			}

			config.getGeneratedSourceFolder().mkdirs();
		}
	}

	private static String getServiceName(String serviceName) {
		serviceName = serviceName.replaceAll(" ", "");
		serviceName = serviceName.toLowerCase();
		serviceName = serviceName.replaceAll("[^a-z,0-9]", "");

		return serviceName;
	}

	private String getPackage(EntityObjectVO<UID> restapi, String boSetPackage, String defaultName) {
		return Optional.ofNullable(boSetPackage)
				.orElse(getMainClassPackage(restapi) + "." + defaultName);
	}

	private String getMainClassPackage(EntityObjectVO<UID> restapi) {
		return Optional.ofNullable(restapi.getFieldUid(E.RESTAPIS.nuclet))
				.map(Nuclet::get)
				.map(Nuclet::getPackage)
				.orElse(DEFAULT_PACKAGE_WEBSERVICES);
	}

	private String readFile(File f) throws IOException {
		final StringBuilder text = new StringBuilder();
		final String newline = System.getProperty("line.separator");
		try (Scanner scanner = new Scanner(new BufferedInputStream(Files.newInputStream(f.toPath())),
				NuclosCodegeneratorConstants.JAVA_SRC_ENCODING)) {
			while (scanner.hasNextLine()) {
				text.append(scanner.nextLine()).append(newline);
			}
		}
		return text.toString();
	}

	private void deleteAndNew(File file) throws IOException {
		if (file.exists()) {
			file.delete();
		}
		file.getParentFile().mkdirs();
		if (!file.createNewFile()) {
			throw new IOException("cannot delete and re-create " + file);
		}
	}

	private static class RestApiBuilderConfig {

		private boolean apiChecked;
		private File json;
		private String realDigest;
		private boolean recompileIsNecessary;
		private String packageName;
		private File generatedSourceFolder;
		private File swaggerCgIgnore;

		public RestApiBuilderConfig(final UID uid) {

		}

		public boolean isApiChecked() {
			return apiChecked;
		}

		public void setApiChecked(final boolean apiChecked) {
			this.apiChecked = apiChecked;
		}

		public void setJson(final File json) {
			this.json = json;
		}

		public File getJson() {
			return json;
		}

		public void setRealDigest(final String realDigest) {
			this.realDigest = realDigest;
		}

		public String getRealDigest() {
			return realDigest;
		}

		public void setRecompileIsNecessary(final boolean recompileIsNecessary) {
			this.recompileIsNecessary = recompileIsNecessary;
		}

		public boolean isRecompileIsNecessary() {
			return recompileIsNecessary;
		}

		public void setPackageName(final String packageName) {
			this.packageName = packageName;
		}

		public String getPackageName() {
			return packageName;
		}

		public void setGeneratedSourceFolder(final File generatedSourceFolder) {
			this.generatedSourceFolder = generatedSourceFolder;
		}

		public File getGeneratedSourceFolder() {
			return generatedSourceFolder;
		}

		public void setSwaggerCgIgnore(final File swaggerCgIgnore) {
			this.swaggerCgIgnore = swaggerCgIgnore;
		}

		public File getSwaggerCgIgnore() {
			return swaggerCgIgnore;
		}
	}
}
