package org.nuclos.server.rule.client;

import java.util.Map;

import org.nuclos.api.UID;
import org.nuclos.api.exception.BusinessException;

public abstract class AbstractClientRuleBusinessObject {
	
	private UID boUid;
	private Map<UID, Object> fieldmap;
	
	protected AbstractClientRuleBusinessObject(UID boUid, Map<UID, Object> fieldmap) {
		this.boUid = boUid;
		this.fieldmap = fieldmap;
	}
	
	protected UID getBoUID() {
		return this.boUid;
	}
	
	protected <T> T getAttribute(String field, Class<T> type) throws BusinessException {
		
		T castedValue = null;
		
		if (!fieldmap.containsKey(new org.nuclos.common.UID(field)))
			return null;
		
		try {
			castedValue = type.cast(this.fieldmap.get(new org.nuclos.common.UID(field)));
			
		} catch (ClassCastException e) {
			throw new BusinessException("Value of field '" + field + "' is not of type '" + type.getCanonicalName() + "'");
		}
		
		return castedValue;		
	}
	
	
}
