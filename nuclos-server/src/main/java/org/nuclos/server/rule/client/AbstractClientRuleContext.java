package org.nuclos.server.rule.client;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.nuclos.api.UID;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.server.rule.client.vo.ClientRuleContextParameter;

public class AbstractClientRuleContext {

	private UID boUid;
	private ClientRuleContextParameter parameter;
	
	protected AbstractClientRuleContext(UID boUid, ClientRuleContextParameter parameter) {
		this.boUid = boUid;
		this.parameter = parameter;
	}
	
	protected UID getBoUID() {
		return this.boUid;
	}
	
	protected <T> T getAttribute(UID field, Class<T> type) throws BusinessException {
		
		T castedValue = null;
		
		if (!this.parameter.getAttributes().containsKey(field))
			return null;
		
		try {
			castedValue = type.cast(this.parameter.getAttributes().get(field));
			
		} catch (ClassCastException e) {
			throw new BusinessException("Value of field '" + field + "' is not of type '" + type.getCanonicalName() + "'");
		}
		
		return castedValue;		
	}
	
	protected <T extends AbstractClientRuleBusinessObject> T getReference(String refFieldUID, String refBOUID, Class<T> type) 
			throws BusinessException {
		
		T retVal = null;
		
		Map<UID, Object> reference = this.parameter.getReference(
				new org.nuclos.common.UID(refFieldUID), new org.nuclos.common.UID(refBOUID));
		
		if (reference != null) {
			try {
				Constructor ctor = type.getDeclaredConstructor(UID.class, Map.class);
				ctor.setAccessible(true);
				retVal = (T) ctor.newInstance(refBOUID, reference);
			} catch (Exception e) {
				throw new BusinessException(e);
			}
		}
		
		return retVal;
	}
	
	protected <T extends AbstractClientRuleBusinessObject> List<T> getDependent(String refFieldUID, String refBOUID, Class<T> type) 
			throws BusinessException {
		
		List<T> retVal = null;
		
		List<Map<UID, Object>> dependent = this.parameter.getDependent(new org.nuclos.common.UID(refFieldUID), new org.nuclos.common.UID(refBOUID));
		
		if (dependent != null) {
			retVal = new ArrayList<T>();
			
			try {
				for (Map<UID, Object> row : dependent) {				
					Constructor ctor = type.getDeclaredConstructor(UID.class, Map.class);
					ctor.setAccessible(true);
					retVal.add( (T) ctor.newInstance(refBOUID, row));
				}
				
			} catch (Exception e) {
				throw new BusinessException(e);
			}
		}
		
		return retVal;
	}
	
}
