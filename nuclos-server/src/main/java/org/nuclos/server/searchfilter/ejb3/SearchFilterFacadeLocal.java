package org.nuclos.server.searchfilter.ejb3;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

public interface SearchFilterFacadeLocal extends CommonSearchFilterFacade {

	SearchFilterVO getSearchFilterByPk(UID pk) throws CommonBusinessException;

	EntitySearchFilter2 createEntitySearchFilterFromVO(SearchFilterVO vo);

}
