//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.util.concurrent.atomic.AtomicReference;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class FailDuringStartupAuthenticationProvider implements AuthenticationProvider {

	private AtomicReference<NuclosAuthenticationProvider> realProvider = new AtomicReference<NuclosAuthenticationProvider>();

	FailDuringStartupAuthenticationProvider() {
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		final NuclosAuthenticationProvider nap = realProvider.get();
		if (nap != null) {
			return nap.authenticate(authentication);
		}
		else {
			throw new ProviderNotFoundException("Server is not ready, please retry in an instant ...");
		}
	}

	@Override
	public boolean supports(Class<?> clazz) {
		final NuclosAuthenticationProvider nap = realProvider.get();
		if (nap != null) {
			return nap.supports(clazz);
		}
		else {
			if (UsernamePasswordAuthenticationToken.class.isAssignableFrom(clazz)) {
				return true;
			}
			else if (clazz == NuclosLocalServerAuthenticationToken.class) {
				return true;
			}
			return false;
		}
	}

	public void setNuclosAuthenticationProvider(NuclosAuthenticationProvider nuclosAuthenticationProvider) {
		if (nuclosAuthenticationProvider == null) {
			throw new NullPointerException();
		}
		if (realProvider.get() == null) {
			realProvider.set(nuclosAuthenticationProvider);
		}
	}

}
