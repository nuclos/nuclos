//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.nuclos.common2.exception.NuclosUsernameNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Implementation for Spring's <code>LdapUserSearch</code> that uses Spring LDAP's <code>LdapTemplate</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class LdapUserSearch implements org.springframework.security.ldap.search.LdapUserSearch {

	private static final Logger LOG = LoggerFactory.getLogger(LdapUserSearch.class);

	private final ContextSource contextSource;
	private final String searchBase;
	private final String searchFilter;
	private final int scope;

	public LdapUserSearch(String searchBase, String searchFilter, BaseLdapPathContextSource contextSource, int scope) {
		this.contextSource = contextSource;
		this.searchBase = searchBase;
		this.searchFilter = searchFilter;
		this.scope = scope;
	}

	@Override
	public DirContextOperations searchForUser(String username) throws UsernameNotFoundException {
		LOG.debug("Searching for user '{}'.", username);

		LdapTemplate template = new LdapTemplate(contextSource);
		template.setIgnorePartialResultException(true);

		ContextMapper mapper = (Object ctx) -> ctx;

		List<DirContextOperations> l = template.search("", MessageFormat.format(searchFilter, username), scope, mapper);
		if (l.size() == 0) {
			throw new NuclosUsernameNotFoundException("User " + username + " not found in directory.");
		}

		for (LdapSearchListener listener : LDAPSEARCHLISTENERS) {
			listener.found(l);
		}

		if (l.size() != 1) {
			throw new IncorrectResultSizeDataAccessException(1, l.size());
		}

		return l.get(0);
	}

	// NUCLOS-7938 The following is used by an extension can can't be removed.

	private static final List<LdapSearchListener> LDAPSEARCHLISTENERS = new ArrayList<>();

	public static void registerLdapSearchListener(LdapSearchListener listener) {
		if (!LDAPSEARCHLISTENERS.contains(listener)) {
			LDAPSEARCHLISTENERS.add(listener);
		}
	}

	public static boolean unregisterLdapSearchListener(LdapSearchListener listener) {
		return LDAPSEARCHLISTENERS.remove(listener);
	}

	static void bound(DirContextOperations operations) {
		if (operations != null) {
			for (LdapSearchListener listener : LDAPSEARCHLISTENERS) {
				if (listener instanceof  LdapActionListener) {
					((LdapActionListener)listener).bound(operations);
				}
			}
		}
	}

	public interface LdapSearchListener {

		void found(List<DirContextOperations> lstOperations);
	}

	public interface LdapActionListener extends LdapSearchListener {

		void bound(DirContextOperations operation);
	}


}
