//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.util.Arrays;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.nuclos.api.context.InputRequiredException;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.InternalErrorException;
import org.nuclos.common2.exception.NuclosExceptions;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 * A logger for exceptions that are sent to a client.
 * Removes any technical information from the exception. Only the type and the message remain.
 */
@Component
public class NuclosRemoteExceptionLogger {

	private static final Logger log = (Logger) LogManager.getLogger(NuclosRemoteExceptionLogger.class);
	private static final Logger logBusiness = getLogger("StacktraceBusiness");
	private static final Logger logInternalError = getLogger("StacktraceInternal");

	@Autowired
	private SpringLocaleDelegate localeDelegate;

	public Throwable logException(Throwable ex, final org.slf4j.Logger serverLogger) {
		return logException(ex, serverLogger, false, false);
	}

	public Throwable logException(final Throwable ex, final org.slf4j.Logger serverLogger,
								  final boolean bForceReturnBusinessRuleExceptions,
								  final boolean bForceReturnGeneratorFailedException) {
		return logException(ex, serverLogger,
				bForceReturnBusinessRuleExceptions,
				bForceReturnGeneratorFailedException,
				localeDelegate.getText("internal.error"));
	}

	public static Throwable logException(final Throwable ex, final org.slf4j.Logger serverLogger,
										 final boolean bForceReturnBusinessRuleExceptions,
										 final boolean bForceReturnGeneratorFailedException,
										 final String sInternalErrorMessage) {
		Throwable busiEx = null;
		Throwable ruleBusiEx = null;

		Throwable result = ex;
		try {
			if (ex != null && !isCleared(ex)) {
				// AuthenticationException is always business!
				busiEx = NuclosExceptions.findRootCause(
						ex,
						it -> it instanceof AuthenticationException ||
							  it instanceof AccessDeniedException
				);

				// Handle GeneratorFailedException before any other CommonBusinessExceptions or BusinessExceptions
				if (ex instanceof GeneratorFailedException && bForceReturnGeneratorFailedException) {
					busiEx = ex;
				}

				if (busiEx == null && bForceReturnBusinessRuleExceptions) {
					// Return a BusinessRuleException if exists (for bubbles etc.), but we log the stacktrace from cause
					ruleBusiEx = NuclosExceptions.findRootCause(
							ex,
							it -> it instanceof NuclosBusinessRuleException
					);
				}

				// Find InputRequiredException before BusinessException
				if (busiEx == null) {
					busiEx = NuclosExceptions.findRootCause(
							ex,
							it -> it instanceof InputRequiredException
					);
				}

				// Find root BusinessException or ValidationException with field errors
				if (busiEx == null) {
					busiEx = NuclosExceptions.findRootCause(
							ex,
							it -> it instanceof BusinessException
									|| (it instanceof CommonValidationException && ((CommonValidationException) it).getFieldErrors() != null)
					);
				}

				// Find other CommonBusinessException
				if (busiEx == null) {
					busiEx = NuclosExceptions.findRootCause(
							ex,
							it -> it instanceof CommonBusinessException
					);
				}

				if (busiEx != null) {
					// remove infrastructure stack trace elements (these are rarely relevant)
					Throwable t = busiEx;
					while (t != null) {
						if (t instanceof BusinessException || t instanceof CommonBusinessException) {
							removeInfrastructureStackTraceElements(t);
						}
						t = t.getCause();
					}
					logBusiness.error(busiEx.getMessage(), busiEx);
					// remove stacktrace, cause and suppressedExceptions before returning
					if (busiEx instanceof GeneratorFailedException && bForceReturnGeneratorFailedException) {
						GeneratorFailedException genFailed = (GeneratorFailedException) busiEx;
						Throwable cause = genFailed.getCause();
						if (cause instanceof CommonBusinessException) {
							genFailed.setRemoteReadyCommonBusinessException((CommonBusinessException) clear(cause));
						}
						if (cause instanceof BusinessException) {
							genFailed.setRemoteReadyBusinessException((BusinessException) clear(cause));
						}
					}
					clear(busiEx);
					if (ruleBusiEx != null) {
						return clear(ruleBusiEx);
					}
					result = busiEx;
				} else {
					int hashCode = ExceptionUtils.getStackTrace(ex).hashCode();
					InternalErrorException iee = new InternalErrorException(hashCode, sInternalErrorMessage);
					logInternalError.error(iee.getIdentifier(), ex);
					result = iee;
				}
			}
			return result;
		} finally {
			if (serverLogger != null) {
				if (result instanceof InternalErrorException) {
					serverLogger.warn("{}: {}", result, clear(ex));
				} else {
					serverLogger.info("{}", (Object) result);
				}
			}
		}
	}

	public static boolean isCleared(Throwable ex) {
		return ex.getCause() == null && ex.getStackTrace().length == 0 && ex.getSuppressed().length == 0;
	}

	public static <T extends Throwable> T clear(T ex) {
		try {
			FieldUtils.writeField(Throwable.class.getDeclaredField("cause"), ex, null, true);
			FieldUtils.writeField(Throwable.class.getDeclaredField("suppressedExceptions"), ex, null, true);
		} catch (IllegalAccessException | NoSuchFieldException ex2) {
			log.error(ex2.getMessage(), ex2);
		}
		ex.setStackTrace(new StackTraceElement[0]);
		return ex;
	}

	public static void removeInfrastructureStackTraceElements(Throwable ex) {
		if (ex == null) {
			return;
		}
		StackTraceElement[] stackTraceElements = Arrays.stream(ex.getStackTrace())
				.filter(element ->
						!element.isNativeMethod() &&
								!element.getClassName().startsWith("sun.reflect") &&
								!element.getClassName().startsWith("jdk.internal.reflect") &&
								!element.getClassName().startsWith("com.sun.proxy") &&
								!element.getClassName().startsWith("javax.servlet.http") &&
								!element.getClassName().startsWith("java.lang.reflect") &&
								!element.getClassName().startsWith("java.lang.Thread") &&
								!element.getClassName().startsWith("java.util.concurrent.ThreadPoolExecutor") &&
								!element.getClassName().startsWith("org.nuclos") &&
								!element.getClassName().startsWith("org.glassfish") &&
								!element.getClassName().startsWith("org.springframework") &&
								!element.getClassName().startsWith("org.apache.logging") &&
								!element.getClassName().startsWith("org.apache.catalina") &&
								!element.getClassName().startsWith("org.apache.coyote") &&
								!element.getClassName().startsWith("org.apache.tomcat"))
				.toArray(StackTraceElement[]::new);
		ex.setStackTrace(stackTraceElements);
	}

	private static Logger getLogger(String sLoggername) {
		Logger result = (Logger) LogManager.getLogger(sLoggername);
		if (result == null) {
			result = log;
		}
		return result;
	}

}
