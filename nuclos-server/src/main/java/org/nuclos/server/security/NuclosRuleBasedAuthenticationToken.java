package org.nuclos.server.security;

import static org.nuclos.api.authentication.AuthenticationResult.UNSET;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.nuclos.api.authentication.AuthenticationResult;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class NuclosRuleBasedAuthenticationToken extends AbstractAuthenticationToken {

	private final String username;
	private final AuthenticationResult authResult;
	private final String authRule;
	private long validUntil = UNSET;

	public NuclosRuleBasedAuthenticationToken(final String username,
											  final AuthenticationResult authResult,
											  final String authRule,
											  final Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		this.username = username;
		this.authResult = authResult;
		this.authRule = authRule;
		calculateValidUntil();
		setAuthenticated(true);
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return username;
	}

	public AuthenticationResult getAuthResult() {
		return authResult;
	}

	public String getAuthRule() {
		return authRule;
	}

	public void calculateValidUntil() {
		final Long lifetime = authResult.getLifetime();
		final TimeUnit lifetimeUnit = authResult.getLifetimeUnit();
		if (lifetime != null && lifetimeUnit != null) {
			this.validUntil = System.currentTimeMillis() + lifetimeUnit.toMillis(lifetime);
		} else {
			this.validUntil = UNSET;
		}
	}

	public boolean isValid() {
		return this.validUntil == UNSET
				|| System.currentTimeMillis() < this.validUntil;
	}

}
