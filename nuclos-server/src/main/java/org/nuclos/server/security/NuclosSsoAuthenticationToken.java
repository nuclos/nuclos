//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.security;

import java.util.Collection;

import org.nuclos.common.UID;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.nimbusds.jwt.JWT;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.RefreshToken;

public class NuclosSsoAuthenticationToken extends AbstractAuthenticationToken {

	private final State logoutState;
	private final Object principal;
	private final UID ssoAuthUID;
	private transient RefreshToken refreshToken;
	private transient AccessToken accessToken;
	private transient JWT idToken;
	private long accessTokenValidUntil = 0l;

	public NuclosSsoAuthenticationToken(Object principal, UID ssoAuthUID, AccessToken accessToken, RefreshToken refreshToken, JWT idToken, Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		this.logoutState = new State();
		this.principal = principal;
		this.ssoAuthUID = ssoAuthUID;
		this.idToken = idToken;
		updateTokens(accessToken, refreshToken);
		setAuthenticated(true);
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return this.principal;
	}

	public UID getSsoAuthUID() {
		return this.ssoAuthUID;
	}

	public AccessToken getAccessToken() {
		return accessToken;
	}

	public void updateTokens(AccessToken accessToken, final RefreshToken refreshToken) {
		this.accessToken = accessToken;
		if (accessToken.getLifetime() > 0l) {
			this.accessTokenValidUntil = System.currentTimeMillis() + (accessToken.getLifetime() * 1000);
		}
		this.refreshToken = refreshToken;
	}

	public boolean isAccessTokenValid() {
		return this.accessTokenValidUntil == 0l
				|| System.currentTimeMillis() < this.accessTokenValidUntil;
	}

	public RefreshToken getRefreshToken() {
		return refreshToken;
	}

	public JWT getIdToken() {
		return idToken;
	}

	public State getLogoutState() {
		return logoutState;
	}
}
