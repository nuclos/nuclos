package org.nuclos.server.security;

import static org.nuclos.common2.StringUtils.looksEmpty;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.Response;

import org.nuclos.businessentity.EmailIncomingServer;
import org.nuclos.businessentity.EmailOutgoingServer;
import org.nuclos.businessentity.SsoAuth;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

import com.nimbusds.oauth2.sdk.ClientCredentialsGrant;
import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.ErrorResponse;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.TokenRequest;
import com.nimbusds.oauth2.sdk.TokenResponse;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.ClientID;

public class NuclosSsoUtils {

	public static URI getTokenEndpoint(final Logger LOG,
									   final SsoAuth ssoAuth,
									   final String jSessionId) {
		try {
			return new URI(ssoAuth.getOAuth2TokenEndpoint());
		} catch (URISyntaxException ex) {
			throw NuclosSsoUtils.logAndGetException(LOG, ssoAuth, jSessionId, ex, "OAuth2TokenEndpoint is not a malformed URL");
		}
	}

	public static URI getEndSessionEndpoint(final Logger LOG,
									   final SsoAuth ssoAuth,
									   final String jSessionId) {
		try {
			return new URI(ssoAuth.getOidcEndSessionEndpoint());
		} catch (URISyntaxException ex) {
			throw NuclosSsoUtils.logAndGetException(LOG, ssoAuth, jSessionId, ex, "OidcEndSessionEndpoint is not a malformed URL");
		}
	}

	public static void logDebug(final Logger LOG,
								final SsoAuth ssoAuth,
								final String jSessionId,
								final String sLogline,
								final Object...values) {
		if (Boolean.TRUE.equals(ssoAuth.getDebug())) {
			Object[] extendedArray = new Object[values.length + 1];
			System.arraycopy(values, 0, extendedArray, 1, values.length);
			extendedArray[0] = jSessionId;
			LOG.info("Debug SSO auth for session {}: " + sLogline, extendedArray);
		}
	}

	public static void logAndThrowException(final Logger LOG,
											final SsoAuth ssoAuth,
											final String jSessionId,
											final String sLogline,
											final Object...values) {
		logAndThrowException(LOG, ssoAuth, jSessionId, null, sLogline, values);
	}

	public static void logAndThrowException(final Logger LOG,
										  final SsoAuth ssoAuth,
										  final String jSessionId,
										  final Throwable ex,
										  final String sLogline,
										  final Object...values) {
		throw logAndGetException(LOG, ssoAuth, jSessionId, ex, sLogline, values);
	}

	public static void logAndThrowException(final Logger LOG,
											final SsoAuth ssoAuth,
											final String jSessionId,
											final Response.StatusType responseStatus,
											final String responseError) {
		logAndThrowException(LOG, ssoAuth, jSessionId, responseStatus, responseError, null, null);
	}

	public static void logAndThrowException(final Logger LOG,
											final SsoAuth ssoAuth,
											final String jSessionId,
											final Response.StatusType responseStatus,
											final String responseError,
											final String sLogline,
											final Object...values) {
		throw logAndGetException(LOG, ssoAuth, jSessionId, null, responseStatus, responseError, sLogline, values);
	}

	public static NuclosWebException logAndGetException(final Logger LOG,
														final SsoAuth ssoAuth,
														final String jSessionId,
														final Throwable ex,
														final String sLogline,
														final Object...values) {
		throw logAndGetException(LOG, ssoAuth, jSessionId, ex, null, null, sLogline, values);
	}

	private static NuclosWebException logAndGetException(final Logger LOG,
											   final SsoAuth ssoAuth,
											   final String jSessionId,
											   final Throwable ex,
											   final Response.StatusType responseStatus,
											   final String responseError,
											   final String sLogline,
											   final Object...values) {
		if (ex instanceof NuclosWebException) {
			return (NuclosWebException) ex;
		} else {
			NuclosWebException webEx = new NuclosWebException(
					RigidUtils.defaultIfNull(responseStatus, Response.Status.UNAUTHORIZED),
					RigidUtils.defaultIfNull(responseError, jSessionId));
			final Throwable exToLog = (responseStatus != null && ex == null) ?
					webEx // log responseStatus and error
					: ex; // log exception, if any

			int extElementCount = 1;
			if (exToLog != null) {
				extElementCount++;
			}
			Object[] extendedArray = new Object[(values==null?0:values.length) + extElementCount];
			if (values != null && values.length > 0) {
				System.arraycopy(values, 0, extendedArray, extElementCount, values.length);
			}
			extendedArray[0] = jSessionId;
			String sDefault = "SSO auth failed for session {}";
			String s = sDefault;
			if (exToLog != null) {
				extendedArray[1] = RigidUtils.defaultIfNull(responseError, exToLog.getMessage());
				s += (looksEmpty(sLogline) ?
						" Exception: \"{}\"" :
						(" [Exception \"{}\"]: " + sLogline));
			} else if (!looksEmpty(sLogline)) {
				s += ": " + sLogline;
			}
			LOG.warn(s, extendedArray);
			if (exToLog != null && ssoAuth != null && Boolean.TRUE.equals(ssoAuth.getDebug())) {
				LOG.error(sDefault + ": {}", jSessionId, exToLog.getMessage(), exToLog);
			}
			return webEx;
		}
	}

	public static void throwException(ErrorResponse errorResponse, String authPart) {
		int httpStatusCode = 0;
		ErrorObject errorObject = errorResponse.getErrorObject();
		if (errorObject != null) {
			httpStatusCode = errorObject.getHTTPStatusCode();
		}
		String description = errorObject.getDescription();
		if (description == null) {
			description = "";
			if (httpStatusCode != 0) {
				try {
					description = HttpStatus.valueOf(httpStatusCode).getReasonPhrase();
				} catch (IllegalArgumentException notKnown) {
					// ignore
				}
			}
		}
		String code = errorObject.getCode();
		if (code == null) {
			code = "";
		} else {
			code = " " + code;
		}
		throw new SessionAuthenticationException(String.format("%s (%d%s) during %s", description, httpStatusCode, code, authPart));
	}

	public static String getOAuth2TokenForOutgoingEmail(EmailOutgoingServer emailOutgoingServer) {
		if (emailOutgoingServer == null) {
			throw new IllegalArgumentException("emailOutgoingServer must not be null");
		}

		OAuth2Config config = new OAuth2Config(
				emailOutgoingServer.getOAuth2TokenEndpoint().trim(),
				emailOutgoingServer.getOAuth2ClientId(),
				emailOutgoingServer.getOAuth2ClientSecret(),
				emailOutgoingServer.getOAuth2Scopes());

		return getOAuth2AccessTokenWithClientCredentials(config);
	}

	public static String getOAuth2TokenForIncomingEmail(EmailIncomingServer emailIncomingServer) {
		if (emailIncomingServer == null) {
			throw new IllegalArgumentException("emailOutgoingServer must not be null");
		}

		OAuth2Config config = new OAuth2Config(
				emailIncomingServer.getOAuth2TokenEndpoint().trim(),
				emailIncomingServer.getOAuth2ClientId(),
				emailIncomingServer.getOAuth2ClientSecret(),
				emailIncomingServer.getOAuth2Scopes());

		return getOAuth2AccessTokenWithClientCredentials(config);
	}

	private static String getOAuth2AccessTokenWithClientCredentials(OAuth2Config config) {
		if (config.getTokenEndpoint() == null
				|| config.getClientId() == null
				|| config.getClientSecret() == null) {
			throw new IllegalArgumentException("Not all required OAuth2 settings have been configured (Token Endpoint, Client ID, Client Secret)");
		}

		ClientID clientID = new ClientID(config.getClientId());
		Scope scope = null;
		if (!looksEmpty(config.getScopes())) {
			scope = new Scope(config.getScopes().split(","));
		}

		TokenRequest request;
		try {
			request = new TokenRequest(new URI(config.getTokenEndpoint()), new ClientSecretBasic(clientID, new Secret(config.getClientSecret())), new ClientCredentialsGrant(), scope);

			final HTTPResponse response = request.toHTTPRequest().send();
			TokenResponse tokenResponse = TokenResponse.parse(response);
			if (!tokenResponse.indicatesSuccess()) {
				throwException(tokenResponse.toErrorResponse(), String.format("token request against \"%s\"", config.getTokenEndpoint()));
			} else {
				return tokenResponse.toSuccessResponse().getTokens().getBearerAccessToken().getValue();
			}
		} catch (URISyntaxException | IOException | ParseException e) {
			throw new NuclosFatalException(e);
		}

		return null;
	}

	private static class OAuth2Config {
		private final String tokenEndpoint;
		private final String clientId;
		private final String clientSecret;
		private final String scopes;

		public OAuth2Config(
				String tokenEndpoint,
				String clientId,
				String clientSecret,
				String scopes
		) {
			this.tokenEndpoint = tokenEndpoint;
			this.clientId = clientId;
			this.clientSecret = clientSecret;
			this.scopes = scopes;
		}

		public String getTokenEndpoint() {
			return tokenEndpoint;
		}

		public String getClientId() {
			return clientId;
		}

		public String getScopes() {
			return scopes;
		}

		public String getClientSecret() {
			return clientSecret;
		}
	}
}
