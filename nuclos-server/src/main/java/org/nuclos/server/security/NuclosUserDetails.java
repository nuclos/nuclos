package org.nuclos.server.security;

import java.util.Collection;

import org.nuclos.common.UID;
import org.nuclos.common.security.UserDetails;
import org.nuclos.common.valuelistprovider.UserLoginRestriction;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class NuclosUserDetails extends User implements UserDetails {

	private final UID userUID;
	private final String email;
	private final String salt;
	private final Integer hashAlgorithm;
    private final UserLoginRestriction loginRestriction;

	public NuclosUserDetails(final String username, final String password, final String salt, final Integer hashAlgorithm, final boolean enabled,
                             final boolean accountNonExpired, final boolean credentialsNonExpired, final boolean accountNonLocked,
                             final Collection<? extends GrantedAuthority> authorities, final UID userUID, final String email,
                             UserLoginRestriction loginRestriction) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.userUID = userUID;
		this.email = email;
		this.salt = salt;
		this.hashAlgorithm = hashAlgorithm;
        this.loginRestriction = loginRestriction;
	}

	public UID getUserUID() {
		return userUID;
	}

	public String getEmail() {
		return email;
	}

	public Integer getHashAlgorithm() { return this.hashAlgorithm; }

	public String getSalt() { return salt; }

    public Boolean isLoginRestricted(UserLoginRestriction restriction) {
        if (restriction == null) {
            return loginRestriction == null;
        } else {
            return restriction.equals(loginRestriction);
        }
    }
}
