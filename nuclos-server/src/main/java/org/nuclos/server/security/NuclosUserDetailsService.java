//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.valuelistprovider.UserLoginRestriction;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.NuclosUsernameNotFoundException;
import org.nuclos.server.autosync.XMLEntities;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

public class NuclosUserDetailsService implements org.nuclos.server.security.UserDetailsService {

	private static final Logger LOG = LoggerFactory.getLogger(MasterDataFacadeHelper.class);

	public static final GrantedAuthority SUPER_USER_AUTHORITY = new SimpleGrantedAuthority("SuperUser");

	private ParameterProvider paramprovider;

	private SpringDataBaseHelper dataBaseHelper;

	private Cache<String, NuclosUserDetailsWithoutCredentials> userDetailsCache;

	NuclosUserDetailsService() {
		userDetailsCache = CacheBuilder.newBuilder()
				.concurrencyLevel(1)
				.maximumSize(1000l)
				.build();
	}

	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	@Autowired
	XMLEntities xmlEntities;

	public void setParameterProvider(ParameterProvider paramprovider) {
		this.paramprovider = paramprovider;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = {Exception.class})
	public static List<DbTuple> loadUsersDataByLoginFromDb(String login, DbAccess dbAccess) {
		return loadUsersDataByLoginFromDbWithoutNewTransaction(login, dbAccess);
	}

	private static List<DbTuple> loadUsersDataByLoginFromDbWithoutNewTransaction(String login, DbAccess dbAccess) {
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.USER);
		query.multiselect(
				t.basePk(),
				t.baseColumn(E.USER.password),
				t.baseColumn(E.USER.superuser),
				t.baseColumn(E.USER.lastLogin),
				t.baseColumn(E.USER.locked),
				t.baseColumn(E.USER.expirationDate),
				t.baseColumn(E.USER.lastPasswordChange),
				t.baseColumn(E.USER.passwordChangeRequired),
				t.baseColumn(E.USER.username),
				t.baseColumn(E.USER.email),
				t.baseColumn(E.USER.hashAlgorithm),
				t.baseColumn(E.USER.passwordSalt),
                t.baseColumn(E.USER.loginRestriction));

		addLoginWhereExpressionToQuery(login, builder, query, t);

		return dbAccess.executeQuery(query);
	}

	public void evictCaches() {
		userDetailsCache.invalidateAll();
	}

	public void evictCacheForUser(String login) {
		if (login == null) {
			throw new IllegalArgumentException("login must not be null");
		}
		NuclosUserDetailsWithoutCredentials cachedValue = userDetailsCache.getIfPresent(login.toUpperCase());
		if (cachedValue != null) {
			if (cachedValue.getEmail() != null) {
				userDetailsCache.invalidate(cachedValue.getUsername().toUpperCase());
				userDetailsCache.invalidate(cachedValue.getEmail().toUpperCase());
			} else {
				userDetailsCache.invalidate(login.toUpperCase());
			}
		}
	}

	/**
	 * Load user including roles (actions) from nuclos database
	 * (T_MD_USER, T_MD_ROLE_USER, T_MD_ROLE, T_MD_ROLE_ACTION, T_AD_ACTION) and JSON.
	 */
	public NuclosUserDetailsWithoutCredentials findUserByUsernameCached(final String login) throws UsernameNotFoundException {
		return findUserByUsernameCached(login, true);
	}

	@Override
	public NuclosUserDetailsWithoutCredentials findUserByUsernameCached(final String login, final boolean withinNewTransaction) throws UsernameNotFoundException {
		if (login == null) {
			throw new IllegalArgumentException("login must not be null");
		}
		try {
			NuclosUserDetailsWithoutCredentials result = userDetailsCache.get(login.toUpperCase(), ()->{
				if (withinNewTransaction) {
					return (NuclosUserDetailsWithoutCredentials) loadUserByUsername(login, true);
				} else {
					return (NuclosUserDetailsWithoutCredentials) loadUserByUsernameWithoutNewTransaction(login, true);
				}
			});
			if (result.getEmail() != null) {
				if (result.getEmail().equalsIgnoreCase(login)) {
					userDetailsCache.put(result.getUsername().toUpperCase(), result);
				} else {
					userDetailsCache.put(result.getEmail().toUpperCase(), result);
				}
			}
			return result;
		} catch (Exception e) {
			throw new NuclosUsernameNotFoundException("User " + login + " not found");
		}
	}

	/**
	 * Load user including roles (actions) from nuclos database
	 * (T_MD_USER, T_MD_ROLE_USER, T_MD_ROLE, T_MD_ROLE_ACTION, T_AD_ACTION) and JSON.
	 */
	@Override
	public NuclosUserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		return loadUserByUsername(login, false);
	}

	/**
	 * Load user including roles (actions) from nuclos database
	 * (T_MD_USER, T_MD_ROLE_USER, T_MD_ROLE, T_MD_ROLE_ACTION, T_AD_ACTION) and JSON.
	 */
	@Transactional(propagation= Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private NuclosUserDetails loadUserByUsername(String login, boolean bWithoutCredentials) throws UsernameNotFoundException {
		return loadUserByUsernameWithoutNewTransaction(login, bWithoutCredentials);
	}

	private NuclosUserDetails loadUserByUsernameWithoutNewTransaction(String login, boolean bWithoutCredentials) throws UsernameNotFoundException {
		final DbTuple tuple = CollectionUtils.getSingleIfExist(loadUsersDataByLoginFromDbWithoutNewTransaction(login, dataBaseHelper.getDbAccess()));
		final UID uid;
		final boolean isSuperUser;
		final String password;
		final Date lastlogin;
		boolean locked;
		final Date expiration;
		final Date passwordchanged;
		final boolean requirechange;
		final String user;
		final String email;
		final String salt;
		final Integer hashAlgorithm;
        final UserLoginRestriction loginRestriction;

		if (tuple != null) {
			uid = tuple.get(0, UID.class);
			password = tuple.get(1, String.class);
			isSuperUser = Boolean.TRUE.equals(tuple.get(2, Boolean.class));
			lastlogin = tuple.get(3, Date.class);
			locked = Boolean.TRUE.equals(tuple.get(4, Boolean.class));
			expiration = tuple.get(5, Date.class);
			passwordchanged = tuple.get(6, Date.class);
			requirechange = Boolean.TRUE.equals(tuple.get(7, Boolean.class));
			user = tuple.get(8, String.class);
			email = tuple.get(9, String.class);
			hashAlgorithm = tuple.get(10, Integer.class);
			salt = tuple.get(11, String.class);
            loginRestriction = Optional.ofNullable(tuple.get(12, String.class)).map(UserLoginRestriction::valueOf).orElse(null);
		} else {
			throw new NuclosUsernameNotFoundException("User " + login + " not found");
		}

		boolean expired = false;
		if (expiration != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(expiration);
			expired = c.before(Calendar.getInstance());
		}

		String lockDays = paramprovider.getValue(ParameterProvider.KEY_SECURITY_LOCK_DAYS);
		if (!locked && lastlogin != null && !StringUtils.isNullOrEmpty(lockDays)) {
			try {
				int days = Integer.parseInt(lockDays);
				Calendar c = Calendar.getInstance();
				c.setTime(lastlogin);
				c.add(Calendar.DAY_OF_MONTH, days);
				if (Calendar.getInstance().after(c)) {
					lockUser(uid);
					locked = true;
				}
			} catch (NumberFormatException ex) {
				LOG.error("Cannot parse parameter value for key {}",
						ParameterProvider.KEY_SECURITY_LOCK_DAYS, ex);
			}
		}

		boolean credentialsExpired = false;
		String passwordInterval = paramprovider.getValue(ParameterProvider.KEY_SECURITY_PASSWORD_INTERVAL);
		if (!StringUtils.isNullOrEmpty(passwordInterval)) {
			if (passwordchanged != null) {
				try {
					int days = Integer.parseInt(passwordInterval);
					Calendar c = Calendar.getInstance();
					c.setTime(passwordchanged);
					c.add(Calendar.DAY_OF_MONTH, days);
					credentialsExpired = c.before(Calendar.getInstance());
				} catch (NumberFormatException ex) {
					LOG.error("Cannot parse parameter value for key {}",
							ParameterProvider.KEY_SECURITY_PASSWORD_INTERVAL, ex);
				}
			} else {
				setPasswordChanged(uid);
			}
		}

		List<GrantedAuthority> authorities = getAuthoritiesWithoutNewTransaction(login, isSuperUser);
		authorities.add(new SimpleGrantedAuthority("real.nuclos.username=" + user));

		if (bWithoutCredentials) {
			return new NuclosUserDetailsWithoutCredentials(user,
					true,
					!expired,
					!credentialsExpired && !requirechange,
					!locked,
					authorities,
					uid,
					email,
                    loginRestriction);
		} else {
			return new NuclosUserDetails(user,
					password == null ? "" : password,
					salt,
					hashAlgorithm,
					true,
					!expired,
					!credentialsExpired && !requirechange,
					!locked,
					authorities,
					uid,
					email,
                    loginRestriction);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = {Exception.class})
	public List<GrantedAuthority> getAuthorities(String username, boolean isSuperUser) {
		return getAuthoritiesWithoutNewTransaction(username, isSuperUser);
	}

	/**
	 * private method = no new transaction!
	 */
	private List<GrantedAuthority> getAuthoritiesWithoutNewTransaction(String username, boolean isSuperUser) {
		Set<String> actions;

		if (isSuperUser) {
			actions = new HashSet<>(getSuperUserActionsWithoutNewTransaction());
		} else {
			actions = new HashSet<>(SecurityCache.getInstance().getAllowedActions(username, null));
		}

		List<GrantedAuthority> authorities = new ArrayList<>(CollectionUtils.transform(actions, SimpleGrantedAuthority::new));
		if (isSuperUser) {
			authorities.add(SUPER_USER_AUTHORITY);
		}

		return authorities;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = {Exception.class})
	public void logAttempt(String login, boolean authenticated) {
		int maxattempts = 0;
		String sMaxattempts = paramprovider.getValue(ParameterProvider.KEY_SECURITY_LOCK_ATTEMPTS);
		if (!StringUtils.isNullOrEmpty(sMaxattempts)) {
			try {
				maxattempts = Integer.parseInt(sMaxattempts);
			} catch (NumberFormatException ex) {
				LOG.error("Cannot parse parameter value for key {}",
						ParameterProvider.KEY_SECURITY_LOCK_ATTEMPTS, ex);
				return;
			}
		}

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.USER);
		query.multiselect(t.basePk(), t.baseColumn(E.USER.loginAttempts));

		addLoginWhereExpressionToQuery(login, builder, query, t);

		List<DbTuple> tuples = dataBaseHelper.getDbAccess().executeQuery(query);

		if (tuples.isEmpty()) {
			throw new NuclosUsernameNotFoundException("User " + login + " not found");
		}

		DbTuple tuple = tuples.get(0);

		UID uid = tuple.get(0, UID.class);
		Integer loginattempts = tuple.get(1, Integer.class);

		final DbMap values = new DbMap(2);
		if (!authenticated) {
			if (loginattempts == null) {
				loginattempts = 1;
			} else {
				loginattempts++;
			}
			values.put(E.USER.loginAttempts, loginattempts);
			if (maxattempts > 0 && loginattempts.compareTo(maxattempts) >= 0) {
				values.put(E.USER.locked, true);
			}
		} else {
			values.put(E.USER.loginAttempts, 0);
			values.putUnsafe(E.USER.lastLogin, DbCurrentDateTime.CURRENT_DATETIME);
		}
		final DbMap conditions = new DbMap(1);
		// conditions.put("INTID", id);
		conditions.put(E.USER.getPk(), uid);
		dataBaseHelper.getDbAccess().execute(new DbUpdateStatement<>(E.USER, values, conditions));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = {Exception.class})
	public void logLastLogin(final NuclosUserDetailsWithoutCredentials userDetails) {
		if (userDetails == null) {
			throw new IllegalArgumentException("userDetails must not be null");
		}
		final DbMap values = new DbMap(1);
		values.putUnsafe(E.USER.lastLogin, DbCurrentDateTime.CURRENT_DATETIME);
		final DbMap conditions = new DbMap(1);
		conditions.put(E.USER.getPk(), userDetails.getUserUID());
		dataBaseHelper.getDbAccess().execute(new DbUpdateStatement<>(E.USER, values, conditions));
	}

	private static void addLoginWhereExpressionToQuery(final String login, final DbQueryBuilder builder, final DbQuery<DbTuple> query, final DbFrom<UID> t) {
		DbExpression<String> upperLogin = builder.upper(builder.literal(login));
		query.where(
				builder.or(
						builder.equal(builder.upper(t.baseColumn(E.USER.username)), upperLogin),
						builder.and(
								builder.equalValue(t.baseColumn(E.USER.loginWithEmailAllowed), true),
								builder.equal(builder.upper(t.baseColumn(E.USER.email)), upperLogin)
						)
				));
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = {Exception.class})
	Set<String> getSuperUserActions() {
		return getSuperUserActionsWithoutNewTransaction();
	}

	Set<String> getSuperUserActionsWithoutNewTransaction() {
		DbQueryBuilder builder = SpringDataBaseHelper.getInstance().getDbAccess().getQueryBuilder();
		DbQuery<String> rolesQuery = builder.createQuery(String.class);
		DbFrom<UID> action = rolesQuery.from(E.ACTION);
		rolesQuery.select(action.baseColumn(E.ACTION.action));
		Set<String> actions = new HashSet<>(SpringDataBaseHelper.getInstance().getDbAccess().executeQuery(rolesQuery));

		for (EntityObjectVO<UID> mdvo : xmlEntities.getData(E.ACTION).getAll()) {
			actions.add(mdvo.getFieldValue(E.ACTION.action));
		}

		return actions;
	}

	private void lockUser(UID uid) {
		final DbMap values = new DbMap(1);
		values.put(E.USER.locked, Boolean.TRUE);
		final DbMap conditions = new DbMap(1);
		// conditions.put("INTID", user);
		conditions.put(E.USER.getPk(), uid);
		dataBaseHelper.getDbAccess().execute(new DbUpdateStatement<>(E.USER, values, conditions));
	}

	private void setPasswordChanged(UID uid) {
		final DbMap values = new DbMap(1);
		values.put(E.USER.lastPasswordChange, Calendar.getInstance().getTime());
		final DbMap conditions = new DbMap(1);
		// conditions.put("INTID", user);
		conditions.put(E.USER.getPk(), uid);
		dataBaseHelper.getDbAccess().execute(new DbUpdateStatement<>(E.USER, values, conditions));
	}
}
