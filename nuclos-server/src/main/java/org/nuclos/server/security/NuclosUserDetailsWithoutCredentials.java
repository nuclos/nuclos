package org.nuclos.server.security;

import java.util.Collection;

import org.nuclos.common.UID;
import org.nuclos.common.valuelistprovider.UserLoginRestriction;
import org.springframework.security.core.GrantedAuthority;

public class NuclosUserDetailsWithoutCredentials extends NuclosUserDetails {

	public NuclosUserDetailsWithoutCredentials(
            final String username, final boolean enabled, final boolean accountNonExpired,
            final boolean credentialsNonExpired, final boolean accountNonLocked,
            final Collection<? extends GrantedAuthority> authorities, final UID userUID, final String email,
            UserLoginRestriction loginRestriction) {
		super(
                username, "", null, -1, enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, authorities, userUID, email, loginRestriction
        );
		super.eraseCredentials();
	}

	@Override
	public String getPassword() {
		throw new RuntimeException("Wrong class instance used!");
	}
}
