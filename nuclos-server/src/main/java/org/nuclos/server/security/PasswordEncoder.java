package org.nuclos.server.security;

public interface PasswordEncoder {
	/**
	 * This will encode the given bytes with the implemented
	 * Crypto-Algorithm
	 * @param input information to encode
	 * @param salt additional random bytes to encode with, can be null
	 * @return encoded bytes
	 * @throws Exception if any problem occurred during encoding
	 */
	byte[] encode(byte[] input, byte[] salt) throws Exception;

	/**
	 * This will encode the given string with the implemented
	 * Crypto-Algorithm
	 * @param input information to encode
	 * @param salt additional random bytes to encode with, can be null
	 * @return encoded string representation
	 * @throws Exception if any problem occurred during encoding
	 */
	String encode(String input, String salt) throws Exception;

	/**
	 * Compute if un-encoded information matches encoded information
	 * @param unencoded information to test
	 * @param salt additional random bytes to encode with, can be null
	 * @param encoded encoded information to test against
	 * @return matching result
	 */
	boolean test(byte[] unencoded, byte[] salt, byte[] encoded);

	/**
	 * Compute if un-encoded information matches encoded information
	 * @param unencoded information to test
	 * @param salt additional random bytes to encode with, can be null
	 * @param encoded encoded information to test against
	 * @return matching result
	 */
	boolean test(String unencoded, String salt, String encoded);
}
