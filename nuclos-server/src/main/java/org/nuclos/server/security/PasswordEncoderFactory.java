package org.nuclos.server.security;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Base64;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;

import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.prng.DigestRandomGenerator;
import org.springframework.security.core.userdetails.UserDetails;

public class PasswordEncoderFactory {
	static final Map<Encoder, Supplier<PasswordEncoder>> factoryMap = new HashMap<>();

	// Encoder supplier prefill
	static {
		factoryMap.put(Encoder.SHA1, SHA1Encoder::new);
		factoryMap.put(Encoder.SCRYPT, SCRYPTEncoder::new);
	}

	static final Encoder DEFAULT_ENCODER = Encoder.SCRYPT;
	static final int DEFAULT_SALT_SIZE = 16;
	private static final DigestRandomGenerator generator = new DigestRandomGenerator(new SHA512Digest());

	enum Encoder {
		SHA1(0),
		SCRYPT(1);

		public static Encoder get(int index) {
			return Arrays.stream(Encoder.values()).filter(v -> index == v.getDbOrdinal())
					.findFirst().orElse(DEFAULT_ENCODER);
		}

		private int dbOrdinal = -1;

		Encoder(int dbOrdinal) {
			this.dbOrdinal = dbOrdinal;
		}

		public int getDbOrdinal() {
			return dbOrdinal;
		}
	}

	/**
	 * Return PasswordEncoder for given crypto scheme.
	 * If not supported throws IllegalArgumentException.
	 * @param crypto scheme to encode information
	 * @return PasswordEncoder for scheme
	 * @throws IllegalArgumentException if not found
	 */
	public static PasswordEncoder getPasswordEncoder(Encoder crypto) throws IllegalArgumentException {
		if (!factoryMap.containsKey(crypto)) {
			throw new IllegalArgumentException("Not supported crypto scheme");
		}
		return factoryMap.get(crypto).get();
	}

	/**
	 * Get current set DEFAULT_ENCODER for PW encoding
	 *  - Nuclos 4.50 - SCrypt
	 * @return Instance of PasswordEncoder
	 * @throws IllegalArgumentException if not found
	 */
	public static PasswordEncoder getDefaultEncoder() throws IllegalArgumentException {
		return getPasswordEncoder(DEFAULT_ENCODER);
	}

	/**
	 * Get PasswordEncoder ordinal number for saving in database
	 * @param encoder -
	 * @return Integer of ordinal
	 * @throws IllegalArgumentException if not found
	 */
	public static Integer getEncoderOrdinal(PasswordEncoder encoder) throws IllegalArgumentException {
		return factoryMap.keySet().stream()
				// We need to look up our map to find the key
				// corresponding to our instance,
				// as we use supplier syntax we need to instantiate it to
				// check with isInstance if given encoder matches.
				.filter((k) -> Optional.ofNullable(encoder)
						.map((enc) -> factoryMap.get(k).get().getClass().isInstance(enc)).orElse(false)
				)
				.findFirst()
				.orElseThrow(() -> new IllegalArgumentException("Not supported crypto scheme"))
				.getDbOrdinal();
	}

	/**
	 * Used to test NuclosUserDetails with passwordToTest using
	 * Encoder given on NuclosUserDetails or SHA1
	 * @param ud instanceof NuclosUserDetails
	 * @param passwordToTest Input to test
	 * @return true if passwordToTest matches encrypted PW from UserDetails
	 */
	public static boolean testUserDetails(UserDetails ud, String passwordToTest) {
		if (ud == null || passwordToTest == null) {
			return false;
		}

		PasswordEncoderFactory.Encoder encoder = PasswordEncoderFactory.Encoder.SHA1;
		String salt = null;
		if (ud instanceof NuclosUserDetails) {
			encoder = PasswordEncoderFactory.Encoder.get(
					((NuclosUserDetails)ud).getHashAlgorithm()
			);
			salt = ((NuclosUserDetails)ud).getSalt();
		}
		final PasswordEncoder passwordEncoder = PasswordEncoderFactory.getPasswordEncoder(encoder);

		return passwordEncoder.test(
				passwordToTest,
				salt,
				ud.getPassword()
		);
	}

	/**
	 * Generates a byte array of n bytes with SHA-512
	 * @param n number of bytes to generate (in range of 0..512)
	 * @return String representation of salt
	 */
	public static String generateSalt(int n) {
		byte[] salt = n < 0 || n > 512 ? new byte[0] : new byte[n];
		generator.addSeedMaterial(ThreadLocalRandom.current().nextLong());
		generator.nextBytes(salt);
		return Base64.getEncoder().encodeToString(salt);
	}
}
