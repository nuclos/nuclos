//Copyright (C) 2022  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.common.ServerParameterProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

public class RemotingServletFilter implements Filter {

	private static final Logger LOG = LoggerFactory.getLogger(RemotingServletFilter.class);

	private ServerParameterProvider _parameterProvider;

	private long _lastParameterCheck = 0L;

	private String _proxyList = null;

	private String _allowRemoting = null;

	private boolean _allowRemotingLogEnabled = false;

	private List<IpAddressMatcher> _lstIpAddressMatcher = Collections.emptyList();

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
		if (request instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			if (!SpringApplicationContextHolder.isNuclosReady()) {
				((HttpServletResponse) response).sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Nuclos remoting interface is not ready");
			}
			final String remoteAddr = TemporaryBlockService.getRemoteAddr(getProxyList(), httpRequest);
			final List<IpAddressMatcher> lstIpAddressMatcher = getIpAddressMatcher();
			if (response.isCommitted()) {
				return; // org.apache.catalina.core.StandardWrapperValve.invoke Servlet.service() for servlet [jmsbroker] in context with path [/nuclos-war] threw exception
						// java.lang.IllegalStateException: Cannot create a session after the response has been committed
			}
			if (lstIpAddressMatcher.isEmpty() || lstIpAddressMatcher.stream().anyMatch(matcher -> matcher.matches(remoteAddr))) {
				chain.doFilter(request, response);
			} else {
				if (isAllowRemotingLogEnabled()) {
					final String requestRemoteAddr = httpRequest.getRemoteAddr();
					final String requestURI = ((HttpServletRequest) request).getRequestURI();
					if (Objects.equals(requestRemoteAddr, remoteAddr)) {
						LOG.warn("Request blocked [RemoteAddr={}, RequestUri={}]", requestRemoteAddr, requestURI);
					} else {
						LOG.warn("Request blocked [RemoteAddr={}, ForwardedFor={}, RequestUri={}]", requestRemoteAddr, remoteAddr, requestURI);
					}
				}
				((HttpServletResponse) response).sendError(HttpServletResponse.SC_FORBIDDEN, "Nuclos remoting interface is forbidden");
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {
	}

	private List<IpAddressMatcher> getIpAddressMatcher() {
		checkParameterChanges();
		return _lstIpAddressMatcher;
	}

	private String getProxyList() {
		checkParameterChanges();
		return _proxyList;
	}

	private boolean isAllowRemotingLogEnabled() {
		checkParameterChanges();
		return _allowRemotingLogEnabled;
	}

	private void checkParameterChanges() {
		final long now = System.currentTimeMillis();
		if (_lastParameterCheck + 60000L < now) {
			if (_lastParameterCheck > 0L) {
				// first check(s) must guarantee a value -> only set _lastParameterCheck after parameters are determined.
				_lastParameterCheck = now;
			}

			final String sParameterProxyList = TemporaryBlockService.getProxyList(getParameterProvider());
			final String sParameterAllowRemoting = getParameterProvider().getValue(ParameterProvider.SECURITY_IP_ALLOW_REMOTING);
			_allowRemotingLogEnabled = getParameterProvider().isEnabled(ParameterProvider.SECURITY_IP_ALLOW_REMOTING_LOG_ENABLED, true);
			if (!Objects.equals(sParameterProxyList, _proxyList)) {
				_proxyList = sParameterProxyList;
			}
			if (!Objects.equals(sParameterAllowRemoting, _allowRemoting)) {
				final String[] ipAddresses = StringUtils.split(sParameterAllowRemoting, ",;\n ");
				_lstIpAddressMatcher = Optional.ofNullable(ipAddresses).map(ips ->
						Arrays.stream(ips)
						.map(IpAddressMatcher::new)
						.collect(Collectors.toList()))
					.orElse(Collections.emptyList());
				_allowRemoting = sParameterAllowRemoting;
			}

			_lastParameterCheck = now;
		}
	}

	private ParameterProvider getParameterProvider() {
		if (_parameterProvider == null) {
			_parameterProvider = SpringApplicationContextHolder.getBean(ServerParameterProvider.class);
		}
		return _parameterProvider;
	}

}
