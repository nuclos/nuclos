package org.nuclos.server.security;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.crypto.generators.SCrypt;
import org.nuclos.common2.StringUtils;

public class SCRYPTEncoder implements PasswordEncoder{
	private final Charset defaultCharset = StandardCharsets.UTF_8;

	@Override
	public byte[] encode(final byte[] input, final byte[] salt) throws Exception {
		return Base64.encodeBase64(
				SCrypt.generate(input, salt, (int) Math.pow(2, 18), 4, 1, 60)
		);
	}

	@Override
	public String encode(final String input, final String salt) throws Exception {
		return new String(
				this.encode(
					StringUtils.getSafeBytes(input, defaultCharset),
					StringUtils.getSafeBytes(salt, defaultCharset)
				),
				defaultCharset
		);
	}

	@Override
	public boolean test(final byte[] unencoded, final byte[] salt, final byte[] encoded) {
		try {
			return Arrays.equals(encoded, this.encode(unencoded, salt));
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean test(final String unencoded, final String salt, final String encoded) {
		return this.test(
				StringUtils.getSafeBytes(unencoded, defaultCharset),
				StringUtils.getSafeBytes(salt, defaultCharset),
				StringUtils.getSafeBytes(encoded, defaultCharset)
		);
	}
}
