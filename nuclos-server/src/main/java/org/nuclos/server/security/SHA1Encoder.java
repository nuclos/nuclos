package org.nuclos.server.security;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;

public class SHA1Encoder implements PasswordEncoder{

	private final Charset defaultCharset = StandardCharsets.ISO_8859_1;

	@Override
	public byte[] encode(byte[] input, byte[] salt) throws Exception {
		try {
			byte[] concatedInput = new byte[
					(input != null ? input.length : 0) +
							(salt != null ? salt.length : 0)
					];
			if (input != null) {
				System.arraycopy(input, 0, concatedInput, 0, input.length);
			}
			if (salt != null) {
				System.arraycopy(salt, 0, concatedInput, (input != null ? input.length : 0), salt.length);
			}
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			digest.reset();
			digest.update(concatedInput);
			// Nuclos StringUtils did some weird stuff here with encoding
			// we need to keep this for backward compatibility
			return Base64.encodeBase64(
					new String(
							digest.digest()
					).getBytes(defaultCharset)
			);
		} catch (NoSuchAlgorithmException e) {
			throw new CommonFatalException(e.getMessage(), e);
		}
	}

	@Override
	public String encode(String input, String salt) throws Exception {
		return new String(
				this.encode(
						StringUtils.getSafeBytes(input, defaultCharset),
						StringUtils.getSafeBytes(salt, defaultCharset)
				),
				defaultCharset
		);
	}

	@Override
	public boolean test(byte[] unencoded, byte[] salt, byte[] encoded) {
		try {
			return Arrays.equals(encoded, this.encode(unencoded, salt));
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean test(String unencoded, String salt, String encoded) {
		return this.test(
				StringUtils.getSafeBytes(unencoded, defaultCharset),
				StringUtils.getSafeBytes(salt, defaultCharset),
				StringUtils.getSafeBytes(encoded, defaultCharset)
		);
	}
}
