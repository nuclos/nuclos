//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import static org.nuclos.common.SearchConditionUtils.newComparison;
import static org.nuclos.common.SearchConditionUtils.newIsNotNullComparison;
import static org.nuclos.common.SearchConditionUtils.newIsNullCondition;
import static org.nuclos.common.SearchConditionUtils.newUidComparison;
import static org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator.EQUAL;
import static org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator.GREATER;
import static org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils.and;
import static org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils.not;
import static org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils.or;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.annotation.security.RolesAllowed;

import org.apache.log4j.Logger;
import org.nuclos.api.common.NuclosUserCommunicationAccount;
import org.nuclos.api.communication.CommunicationPort;
import org.nuclos.api.context.communication.PhoneCallRequestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.service.CommunicationService;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.mail.NuclosMail;
import org.nuclos.common.security.PasswordHistoryVO;
import org.nuclos.common.security.UserFacadeRemote;
import org.nuclos.common.security.UserVO;
import org.nuclos.common.valueobject.MandatorUserVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.MailException;
import org.nuclos.common2.exception.MailSendException;
import org.nuclos.server.cluster.TransactionalClusterNotification;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.common.ejb3.PreferencesFacadeLocal;
import org.nuclos.server.common.mail.NuclosMailServiceProvider;
import org.nuclos.server.common.valueobject.PreferencesVO;
import org.nuclos.server.communication.CommunicationInterfaceLocal;
import org.nuclos.server.communication.context.impl.PhoneCallRequestContextImpl;
import org.nuclos.server.dal.ExtendedDalUtils;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Transactional(propagation=Propagation.REQUIRED, noRollbackFor= {Exception.class})
@RolesAllowed("Login")
@Component("userService")
public class UserFacadeBean extends NuclosFacadeBean implements UserFacadeLocal, UserFacadeRemote {

	private static final Logger LOG = Logger.getLogger(UserFacadeBean.class);

	@Autowired
	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private PreferencesFacadeLocal preferenceFacade;

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	@Autowired
	private ServerParameterProvider serverParameter;

	@Autowired
	private NuclosMailServiceProvider mailServiceProvider;

	public UserFacadeBean() {
	}

	public UserVO getByUID(UID id) throws CommonBusinessException {
		return new UserVO(masterDataFacade.get(E.USER, id));
	}

	@Override
	@CacheEvict(value="userVOByUserName", allEntries=true)
	public void evictCaches() {
		SpringApplicationContextHolder.getBean(NuclosUserDetailsService.class).evictCaches();
		TransactionalClusterNotification.notifySecurityRelatedChangeForAllUsers();
	}

	@CacheEvict(value="userVOByUserName", key="#p0")
	public void evictCacheForUser(String username) {
		SpringApplicationContextHolder.getBean(NuclosUserDetailsService.class).evictCacheForUser(username);
		TransactionalClusterNotification.notifySecurityRelatedChange(username);
	}

	@Cacheable(value="userVOByUserName", key="#p0", unless="#result == null")
	public UserVO getByUserName(String username) throws CommonBusinessException {
		return getUserByFieldValue(E.USER.username, username, "Username");
	}

	public UserVO getByUserEmail(String email) throws CommonBusinessException {
		return getUserByFieldValue(E.USER.email, email, "Email");
	}

	public UserVO getByActivationCode(String code) throws CommonBusinessException {
		return getUserByFieldValue(E.USER.activationCode, code, "ActivationCode");
	}

	private UserVO getUserByFieldValue(FieldMeta<?> fieldMeta, Object value, String errorMsg) throws CommonBusinessException {
		CollectableComparison cond = newComparison(fieldMeta, EQUAL, value);
		Collection<UID> col = masterDataFacade.getMasterDataIdsNoCheck(E.USER.getUID(), new CollectableSearchExpression(cond));

		if (col.isEmpty()) {
			return null;
		}
		else if (col.size() > 1) {
			throw new IllegalArgumentException(errorMsg + ": " + value + " unkown!");
		}

		return new UserVO(masterDataFacade.get(E.USER, col.iterator().next()));
	}

	public MasterDataVO<UID> getRoleByName(String rolename) throws CommonBusinessException {
		CollectableComparison cond = newComparison(E.ROLE.name, EQUAL, rolename);
		Collection<UID> col = masterDataFacade.getMasterDataIdsNoCheck(E.ROLE.getUID(), new CollectableSearchExpression(cond));

		if (col.isEmpty()) {
			return null;
		}
		else if (col.size() > 1) {
			throw new IllegalArgumentException("Rolename: " + rolename + " unkown!");
		}

		return masterDataFacade.get(E.ROLE, col.iterator().next());
	}

	/**
	 * Returns the direct and indirect (inherited via role hierarchy) roles of the given user.
	 *
	 * @param user
	 * @return
	 */
	public Map<UID, String> getAllRolesForUser(final String user) {
		final Map<UID, String> result = new HashMap<>();

		final Map<UID, String> allRoles = securityCache.getAllRolesWithName();
		Set<UID> rolesForUser = masterDataFacade.getRolesHierarchyForUser(user);
		for (Map.Entry<UID, String> entry : allRoles.entrySet()) {
			if (rolesForUser.contains(entry.getKey())) {
				result.put(entry.getKey(), entry.getValue());
			}
		}

		return result;
	}

	public UserVO create(UserVO vo, IDependentDataMap mpDependants) throws CommonBusinessException {
		checkWriteAllowed(E.USER);

		checkUsernameValid(vo);
		checkUsernameSqlRisk(vo);
		checkUniqueLogin(vo);
		checkUserAdmin(vo);

		MasterDataVO<UID> mdvo = vo.toMasterDataVO();

		if (vo.getSetPassword()) {
			mdvo = setPassword(vo);
		} else {
			mdvo.setFieldValue(E.USER.hashAlgorithm,
					PasswordEncoderFactory.getEncoderOrdinal(
							PasswordEncoderFactory.getDefaultEncoder()
					)
			);
		}

		if (mpDependants != null) {
			mdvo.setDependents(mpDependants);
		}
		mdvo = masterDataFacade.create(mdvo, null);

		boolean suppressNotification = ServerParameterProvider.getInstance().isEnabled(ParameterProvider.KEY_SUPPRESS_USER_NOTIFICATION_EMAIL);

		if (vo.getSetPassword() && vo.getNotifyUser() && !suppressNotification) {
			notifyUser(vo, "Nuclos - account created", "The password for your new Nuclos account ({0}) is: {1}");
		}

		if (vo.getSuperuser() != null && vo.getSuperuser()) {
			LOG.info("Superuser permission for user " + vo.getUsername() + " granted by user " + getCurrentUserName());
		}
		return new UserVO(mdvo);
	}

	public UserVO modify(UserVO vo, IDependentDataMap mpDependants, String customUsage) throws CommonBusinessException {
		checkWriteAllowed(E.USER);

		checkUsernameSqlRisk(vo);
		checkUniqueLogin(vo);
		checkUserAdmin(vo);

		// get preferences first, because they are deleted by masterDataFacade.modify
		PreferencesVO prefs = null;
		try {
			prefs = preferenceFacade.getPreferencesForUser(vo.getUsername());
		} catch (CommonFinderException cfe) {
			LOG.debug(cfe.getMessage(), cfe);
		}

		MasterDataVO<UID> mdvo = vo.toMasterDataVO();

		if (vo.getSetPassword()) {
			mdvo = setPassword(vo);
		} else {
			String oldpasswordencrypted = getEncryptedPassword(vo.getId());
			mdvo.setFieldValue(E.USER.password, oldpasswordencrypted);
			mdvo.setFieldValue(E.USER.hashAlgorithm, getHashAlgorithm(vo.getId()));
			mdvo.setFieldValue(E.USER.passwordSalt, getSalt(vo.getId()));
		}

		mdvo.setDependents(mpDependants);

		UID id = vo.getId();
		UserVO userDB = new UserVO(masterDataFacade.get(E.USER, id));
		boolean superUserFlagChanged = !ObjectUtils.nullSafeEquals(userDB.getSuperuser(), vo.getSuperuser());

		evictCacheForUser(userDB.getUsername());
		masterDataFacade.modify(mdvo, customUsage);

		// set preferences again
		if (prefs != null) {
			preferenceFacade.setPreferencesForUser(vo.getUsername(), prefs);
		}

		if (vo.getSetPassword() && vo.getNotifyUser()) {
			notifyUser(vo, "Nuclos - password notification", "The password for your Nuclos account ({0}) has been reset to: {1}");
		}

		if (superUserFlagChanged) {
			LOG.info("Superuser permission for user " + vo.getUsername() + " " + (vo.getSuperuser() != null && vo.getSuperuser() ? "granted" : "revoked") + " by user " + getCurrentUserName());
		}

		return new UserVO(masterDataFacade.get(E.USER, id));
	}

	public void remove(UserVO vo) throws CommonBusinessException {
		checkWriteAllowed(E.USER);
		// clear password history
		masterDataFacade.remove(E.USER.getUID(), vo.getPrimaryKey(), null);
	}

	/**
	 * Checks whether the username matches the (optional) regular expression pattern from parameter USERNAME_DESCRIPTION
	 * @param vo
	 * @throws CommonBusinessException
	 */
	public void checkUsernameValid(UserVO vo) throws CommonBusinessException {
		String sRegExp = Optional
				.ofNullable(serverParameter.getValue(ParameterProvider.KEY_USERNAME_REGEXP))
				.orElse("[a-zA-Z0-9\\.\\-öäüß\\+_]+");

		String errorDescription = Optional
				.ofNullable(serverParameter.getValue(ParameterProvider.KEY_USERNAME_DESCRIPTION))
				.orElse("exception.username.regexp.error.description");

		if (!StringUtils.isNullOrEmpty(sRegExp)) {
			try {
				if (!Pattern.matches(sRegExp, vo.getUsername())) {
					throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("exception.username.regexp", errorDescription));
				}
			}
			catch (PatternSyntaxException ex) {
				throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("exception.parameter.invalid.regexp", sRegExp), ex);
			}
		}
	}

	/**
	 * Checks whether the username contains characters with a potential risk for a sql injection.
	 * @param vo
	 * @throws CommonBusinessException
	 */
	public void checkUsernameSqlRisk(UserVO vo) throws CommonBusinessException {
		if(vo.getUsername().contains("%") || vo.getUsername().contains("'")) {
			throw new NuclosBusinessException("exception.user.invalidname");
		}
	}

	/**
	 * eMail with login option und usernames must be unique
	 * @param vo
	 */
	public void checkUniqueLogin(UserVO vo) throws CommonBusinessException {
		if (vo.getExpirationDate() == null || vo.getExpirationDate().after(new Date())) {
			// not same id
			Collection<CollectableSearchCondition> colMainAndConditions = new ArrayList<>();
			if (vo.getId() != null) {
				colMainAndConditions.add(not(newComparison(E.USER.getPk().getMetaData(E.USER), EQUAL, vo.getId())));
			}
			// active user only
			colMainAndConditions.add(or(
					newComparison(E.USER.expirationDate, GREATER, new Date()),
					newIsNullCondition(E.USER.expirationDate)));
			// username conditions
			Collection<CollectableSearchCondition> colNameOrConditions = new ArrayList<>();
			colNameOrConditions.add(newComparison(E.USER.username, EQUAL, vo.getUsername()));
			colNameOrConditions.add(and(newComparison(E.USER.loginWithEmailAllowed, EQUAL, true),
										newIsNotNullComparison(E.USER.email),
										newComparison(E.USER.email, EQUAL, vo.getUsername())));
			if (vo.getEmail() != null && Boolean.TRUE.equals(vo.isLoginWithEmailAllowed())) {
				colNameOrConditions.add(newComparison(E.USER.username, EQUAL, vo.getEmail()));
				colNameOrConditions.add(and(newComparison(E.USER.loginWithEmailAllowed, EQUAL, true),
											newIsNotNullComparison(E.USER.email),
											newComparison(E.USER.email, EQUAL, vo.getEmail())));
			}
			colMainAndConditions.add(or(colNameOrConditions.toArray(new CollectableSearchCondition[]{})));
			Collection<MasterDataVO<UID>> col = masterDataFacade.getMasterData(E.USER, and(colMainAndConditions.toArray(new CollectableSearchCondition[]{})));
			if (col.size() > 0) {
				UserVO other = new UserVO(col.iterator().next());
				throw new NuclosBusinessException("Username or email are not unique for login." +
						"\nNew/modified user with username \"" + vo.getUsername() + "\"" +
						(vo.getEmail() != null && vo.isLoginWithEmailAllowed() ? (" and email-login \"" + vo.getEmail()) + "\"": "") + "."
						+ "\nFound user with username \"" + other.getUsername() + "\"" +
						(other.getEmail() != null && other.isLoginWithEmailAllowed() ? (" and email-login \"" + other.getEmail()) + "\"" : "") + ".");
			}
		}
	}

	private static void checkUserAdmin(UserVO userVO) {
		if (userVO.isUseradmin() == null) {
			userVO.setUseradmin(false);
		}
	}

	public void setPassword(String username, String password, String customUsage) throws CommonBusinessException {
		UserVO user = getUserWithNewPassword(username, password);
		modify(user, null, customUsage);
	}

	public void setPasswordAndResetCode(String username, String password) throws CommonBusinessException {
		UserVO user = getUserWithNewPassword(username, password);
		user.setPasswordResetCode(null);
		modify(user, null, null);
	}

	private UserVO getUserWithNewPassword(final String username, final String password) throws CommonFinderException, CommonPermissionException {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.USER);
		query.select(t.basePk());
		query.where(builder.equal(builder.upper(t.baseColumn(E.USER.username)), builder.upper(builder.literal(username))));
		UID userId = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);

		UserVO user = new UserVO(masterDataFacade.get(E.USER, userId));
		user.setSetPassword(true);
		user.setNewPassword(password);
		user.setLastPasswordChange(Calendar.getInstance().getTime());
		user.setPasswordChangeRequired(false);
		return user;
	}

	private MasterDataVO<UID> setPassword(UserVO user) throws CommonValidationException {
		final MasterDataVO<UID> result = user.toMasterDataVO();

		String passwd = user.getNewPassword();
		Integer algorithm = PasswordEncoderFactory.getEncoderOrdinal(
				PasswordEncoderFactory.getDefaultEncoder()
		);
		result.setFieldValue(E.USER.hashAlgorithm, algorithm);

		if (StringUtils.isNullOrEmpty(passwd)) {
			//NUCLOS-4259
			if (SpringApplicationContextHolder.getBean(NuclosAuthenticationProvider.class).hasActiveLdapBindAuthenticator()) {
				return result;
			}

			throw new CommonValidationException("exception.password.empty");

		} else {
			String passwdEncrypted;
			String salt = PasswordEncoderFactory.generateSalt(
					PasswordEncoderFactory.DEFAULT_SALT_SIZE
			);
			try {
				passwdEncrypted = encryptPassword(
						algorithm,
						(user.getUsername().toLowerCase() + passwd),
						salt
				);
			} catch (Exception e) {
				throw new CommonValidationException(
						StringUtils.getParameterizedExceptionMessage(
								"exception.password.crypto.failed",
								PasswordEncoderFactory.DEFAULT_ENCODER)
				);
			}
			String errorDescription = serverParameter.getValue(ParameterProvider.KEY_SECURITY_PASSWORD_STRENGTH_DESCRIPTION);

			// check min length first
			String sMinLength = serverParameter.getValue(ParameterProvider.KEY_SECURITY_PASSWORD_STRENGTH_LENGTH);
			if (!StringUtils.isNullOrEmpty(sMinLength)) {
				try {
					if (passwd.length() < Integer.parseInt(sMinLength)) {
						throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("exception.password.length", errorDescription));
					}
				}
				catch (NumberFormatException ex) {
					throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("exception.parameter.type", sMinLength), ex);
				}
			}

			// check regular expression
			String sRegExp = serverParameter.getValue(ParameterProvider.KEY_SECURITY_PASSWORD_STRENGTH_REGEXP);
			if (!StringUtils.isNullOrEmpty(sRegExp)) {
				try {
					if (!Pattern.matches(sRegExp, passwd)) {
						throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("exception.password.regexp", errorDescription));
					}
				}
				catch (PatternSyntaxException ex) {
					throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("exception.parameter.invalid.regexp", sRegExp), ex);
				}
			}

			UID userUid = user.getId();

			if (userUid != null) {
				String oldpasswordencrypted = getEncryptedPassword(userUid);
				String currentPasswdWithOldSalt = "";
				try {
					currentPasswdWithOldSalt = encryptPassword(
							getHashAlgorithm(userUid),
							(user.getUsername().toLowerCase() + passwd),
							getCurrentSalt(userUid)
					);
				} catch (Exception ignored) { }

				if (LangUtils.equal(currentPasswdWithOldSalt, oldpasswordencrypted)) {
					throw new CommonValidationException("exception.password.equals.previous");
				}

				// check password history
				List<PasswordHistoryVO> history = getPasswordHistory(userUid);
				for (PasswordHistoryVO pwd : history) {
					try {
						currentPasswdWithOldSalt = encryptPassword(
								pwd.getHashAlgorithm(),
								(user.getUsername().toLowerCase() + passwd),
								pwd.getSalt()
						);
					} catch (Exception ignored) { }

					if (LangUtils.equal(currentPasswdWithOldSalt, pwd.getEncryptedPassword())) {
						Integer numOfPasswords = getIntegerParameter(ParameterProvider.KEY_SECURITY_PASSWORD_HISTORY_NUMBER, 0);
						throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("exception.password.history", numOfPasswords));
					}
				}

			}

			result.setFieldValue(E.USER.password, passwdEncrypted);
			result.setFieldValue(E.USER.passwordSalt, salt);
			result.setFieldValue(E.USER.lastPasswordChange, Calendar.getInstance().getTime());

			if (userUid != null) {
				updatePasswordHistory(userUid, passwdEncrypted, algorithm, salt);
			}

			return result;
		}
	}

	private List<PasswordHistoryVO> getPasswordHistory(UID user) {
		Integer number = getIntegerParameter(ParameterProvider.KEY_SECURITY_PASSWORD_HISTORY_NUMBER, 0);
		Integer days = getIntegerParameter(ParameterProvider.KEY_SECURITY_PASSWORD_HISTORY_DAYS, 0);
		if (number == 0 && days == 0) {
			// no validation
			return new ArrayList<>();
		}

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<Long> t = query.from(E.PASSWORDHISTORY);

		query.multiselect(
				t.basePk(),
				t.baseColumn(SF.CREATEDAT), // 1
				t.baseColumn(E.PASSWORDHISTORY.password), // 2
				t.baseColumn(E.PASSWORDHISTORY.passwordSalt), // 3
				t.baseColumn(E.PASSWORDHISTORY.hashAlgorithm) // 4
		);
		query.where(builder.equalValue(t.baseColumn(E.PASSWORDHISTORY.user), user));
		query.orderBy(builder.desc(t.baseColumn(SF.CREATEDAT)), builder.desc(t.basePk()));

		List<DbTuple> result = dataBaseHelper.getDbAccess().executeQuery(query);

		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -1 * days);
		int counter = 0;

		List<PasswordHistoryVO> history = new ArrayList<>();

		for (DbTuple tuple : result) {
			Date created = tuple.get(1, Date.class);
			boolean relevant = false;
			if (days != 0 && created.after(c.getTime())) {
				relevant = true;
			}

			if (number != 0 && counter < number ) {
				relevant = true;
			}

			if (relevant) {
				history.add(
						new PasswordHistoryVO(
								tuple.get(2, String.class),
								tuple.get(3, String.class),
								tuple.get(4, Integer.class)
						)
				);
			}
			counter++;
		}
		return history;
	}

	private String encryptPassword(Integer algorithm, String input, String salt) throws Exception {
		return PasswordEncoderFactory
				.getPasswordEncoder(
						PasswordEncoderFactory.Encoder.get(algorithm)
				)
				.encode(
						input,
						salt
				);
	}

	private String getEncryptedPassword(UID user) {
		return this.getDBField(String.class, E.USER.password, user);
	}

	private String getCurrentSalt(UID user) {
		return this.getDBField(String.class, E.USER.passwordSalt, user);
	}

	private Integer getHashAlgorithm(UID user) {
		return this.getDBField(Integer.class, E.USER.hashAlgorithm, user);
	}

	private String getSalt(UID user) {
		return this.getDBField(String.class, E.USER.passwordSalt, user);
	}

	private <T> T getDBField(Class<T> fieldClass, DbField<T> column, UID user) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<T> query = builder.createQuery(fieldClass);
		DbFrom<UID> t = query.from(E.USER);

		query.select(t.baseColumn(column));
		query.where(builder.equalValue(t.basePk(), user));

		return dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
	}

	private void updatePasswordHistory(UID user, String encryptedpassword, Integer algorithm, String salt) {
		Integer number = getIntegerParameter(ParameterProvider.KEY_SECURITY_PASSWORD_HISTORY_NUMBER, 0);
		Integer days = getIntegerParameter(ParameterProvider.KEY_SECURITY_PASSWORD_HISTORY_DAYS, 0);

		if (days != 0 || number != 0) {
			DbMap m = new DbMap();
			m.put(SF.PK_ID, ExtendedDalUtils.getNextId());
			m.put(E.PASSWORDHISTORY.user, user);
			m.put(E.PASSWORDHISTORY.password, encryptedpassword);
			m.put(E.PASSWORDHISTORY.hashAlgorithm, algorithm);
			m.put(E.PASSWORDHISTORY.passwordSalt, salt);
			m.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
			m.put(SF.CREATEDBY, getCurrentUserName());
			m.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
			m.put(SF.CHANGEDBY, getCurrentUserName());
			m.put(SF.VERSION, 1);
			dataBaseHelper.getDbAccess().execute(new DbInsertStatement<Long>(E.PASSWORDHISTORY, m));
		}

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<Long> t = query.from(E.PASSWORDHISTORY);
		query.multiselect(t.basePk(), t.baseColumn(SF.CREATEDAT));
		query.where(builder.equalValue(t.baseColumn(E.PASSWORDHISTORY.user), user));
		query.orderBy(builder.desc(t.baseColumn(SF.CREATEDAT)), builder.desc(t.basePk()));
		List<DbTuple> result = dataBaseHelper.getDbAccess().executeQuery(query);

		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -1 * days);
		int counter = 0;

		for (DbTuple tuple : result) {
			Date created = tuple.get(1, Date.class);
			boolean remove = true;
			if (days != 0 && created.after(c.getTime())) {
				remove = false;
			}

			if (number != 0 && counter < number) {
				remove = false;
			}

			if (remove) {
				Long id = tuple.get(0, Long.class);
				DbMap condition = new DbMap(1);
				// condition.put("INTID", id);
				condition.put(SF.PK_ID, id);
				dataBaseHelper.getDbAccess().execute(new DbDeleteStatement<Long>(E.PASSWORDHISTORY, condition));
			}
			counter++;
		}
	}

	private void notifyUser(UserVO user, String subject, String message) throws MailException {
		if (StringUtils.isNullOrEmpty(user.getEmail())) {
			throw new MailSendException("exception.user.email.empty");
		}

		if (StringUtils.isNullOrEmpty(serverParameter.getValue(ParameterProvider.KEY_SMTP_SERVER))) {
			throw new MailSendException("exception.parameter.email.configuration");
		}

		NuclosMail mail = new NuclosMail(user.getEmail(), subject, MessageFormat.format(message, user.getUsername(), user.getNewPassword()));
		mailServiceProvider.send(mail);
	}

	private Integer getIntegerParameter(String name,Integer defaultValue) {
		String value = serverParameter.getValue(name);
		if (!StringUtils.isNullOrEmpty(value)) {
			try {
				return Integer.parseInt(value);
			}
			catch (NumberFormatException ex) {
				throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("exception.parameter.type", value), ex);
			}
		}
		return defaultValue;
	}

	@Override
	public void requestPhoneCall(UID entityUID, Long id, String toNumber) throws CommonBusinessException {
		if (StringUtils.looksEmpty(toNumber)) {
			throw new IllegalArgumentException("toNumber looks empty");
		}
		if (entityUID == null) {
			throw new IllegalArgumentException("entityUID must not be null");
		}
		if (id == null) {
			throw new IllegalArgumentException("id must not be null");
		}
		String sUserName = getCurrentUserName();
		UID userAccountUID = SecurityCache.getInstance().getUserCommunicationPhoneAccount(sUserName);
		if (userAccountUID == null) {
			throw new NuclosFatalException("User could not request a phone call. Phone account is missing.");
		}
		try {
			NuclosUserCommunicationAccount userAccount = QueryProvider.executeSingleResult(QueryProvider.create(NuclosUserCommunicationAccount.class).where(NuclosUserCommunicationAccount.Id.eq(userAccountUID)));
			UID portUID = (UID) userAccount.getCommunicationPortId();
			CommunicationPort port = SpringApplicationContextHolder.getBean(CommunicationInterfaceLocal.class).getPort(portUID);
			CommunicationService comService = SpringApplicationContextHolder.getBean(CommunicationService.class);

			EntityObjectVO<?> eo = nucletDalProvider.getEntityObjectProcessor(entityUID).getByPrimaryKey(id);

			PhoneCallRequestContextImpl phoneCallRequestContext = (PhoneCallRequestContextImpl) comService.newContextInstance(port, PhoneCallRequestContext.class);
			phoneCallRequestContext.setToNumber(toNumber);
			phoneCallRequestContext.setNuclosUserAccount(userAccount);
			phoneCallRequestContext.setEntityObject(eo);
			comService.executeRequest(phoneCallRequestContext);
		} catch (BusinessException e) {
			throw new NuclosBusinessRuleException(e);
		}
	}


	public boolean isMandatorGranted(UserVO user, MandatorVO mandator) throws CommonBusinessException {
		CollectableComparison cond1 = newUidComparison(E.MANDATOR_USER.mandator, EQUAL, mandator.getPrimaryKey());
		CollectableComparison cond2 = newUidComparison(E.MANDATOR_USER.user, EQUAL, user.getPrimaryKey());

		CompositeCollectableSearchCondition searchCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND, Arrays.asList(cond1, cond2));

		final Collection<MasterDataVO<UID>> col = masterDataFacade.getMasterData(E.MANDATOR_USER, searchCondition);

		if (col.isEmpty()) {
			return false;
		} else if (col.size() > 0) {
			return true;
		}
		return false;
	}

	public org.nuclos.api.UID grantMandator(MandatorUserVO mandatorUserVO) throws CommonValidationException, CommonCreateException, NuclosBusinessRuleException, CommonPermissionException {

		MasterDataVO<UID> mdvo = mandatorUserVO.toMasterDataVO();

		mdvo = masterDataFacade.create(mdvo, null);

		return mdvo.getPrimaryKey();
	}

	@Override
	public void revokeMandator(final UserVO user, final MandatorVO mandator) throws NuclosBusinessRuleException {
		CollectableComparison cond1 = newUidComparison(E.MANDATOR_USER.mandator, EQUAL, mandator.getPrimaryKey());
		CollectableComparison cond2 = newUidComparison(E.MANDATOR_USER.user, EQUAL, user.getPrimaryKey());

		CompositeCollectableSearchCondition searchCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND, Arrays.asList(cond1, cond2));

		final Collection<MasterDataVO<UID>> col = masterDataFacade.getMasterData(E.MANDATOR_USER, searchCondition);

		if(col.size()>0)
		{
			try {
				MasterDataVO vo=col.iterator().next();
				masterDataFacade.remove(E.MANDATOR_USER.getUID(),vo.getPrimaryKey());
			} catch (CommonFinderException e) {
				LOG.error(e.getMessage(), e);
			} catch (Exception e) {
				throw new NuclosBusinessRuleException("not able to delete MANDATOR_USER");
			}
		}
	}
}
