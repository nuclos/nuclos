package org.nuclos.server.security;

import java.util.Date;

import org.nuclos.api.UID;
import org.nuclos.api.User;
import org.nuclos.api.locale.NuclosLocale;

public class UserImpl implements User {
	
	private UID uid;
	private String name;
	private String lastname;
	private String firstname;
	private String email;
	private boolean isLocked;
	private boolean isSuperUser;
	private Date passwordChangedDate;
	private Date expireDate;
	private boolean passwordChangeRequired;
	private NuclosLocale userDataLangLocale;
	
	public UserImpl(UID uid, String name, String lastname, String firstname,
			String email, boolean isLocked, boolean isSuperUser,
			Date passwordChangedDate, Date expireDate,
			boolean passwordChangeRequired, NuclosLocale userDataLangLocale) {
		super();
		this.uid = uid;
		this.name = name;
		this.lastname = lastname;
		this.firstname = firstname;
		this.email = email;
		this.isLocked = isLocked;
		this.isSuperUser = isSuperUser;
		this.passwordChangedDate = passwordChangedDate;
		this.expireDate = expireDate;
		this.passwordChangeRequired = passwordChangeRequired;
		this.userDataLangLocale = userDataLangLocale;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getEmail() {
		return this.email;
	}
	
	@Override
	public String getLastname() {
		return this.lastname;
	}

	@Override
	public String getFirstname() {
		return this.firstname;
	}

	@Override
	public Boolean isSuperuser() {
		return this.isSuperUser;
	}

	@Override
	public Boolean isLocked() {
		return this.isLocked;
	}

	@Override
	public Date getPasswordChanged() {
		return this.passwordChangedDate;
	}

	@Override
	public Date getExpirationDate() {
		return this.expireDate;
	}

	@Override
	public Boolean isRequirePasswordChange() {
		return this.passwordChangeRequired;
	}

	@Override
	public UID getId() {
		return this.uid;
	}

	@Override
	public NuclosLocale getDataLocale() {
		return this.userDataLangLocale;
	}
	
}
