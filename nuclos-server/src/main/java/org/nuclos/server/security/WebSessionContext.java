package org.nuclos.server.security;

import java.net.URI;
import java.util.Locale;

import javax.annotation.Nullable;

import org.springframework.security.core.Authentication;

public class WebSessionContext extends SessionContext {

	private final boolean richClientAttached;
	private final URI serverUri;

	/**
	 * @param id             The DB-ID of this session (from t_ad_session)
	 * @param jSessionId     The container session ID
	 * @param authentication
	 * @param locale
	 * @param richClientAttached
	 * @param serverUri
	 */
	public WebSessionContext(final Long id, final String jSessionId, final Authentication authentication,
							 final Locale locale, final boolean richClientAttached, final URI serverUri) {
		super(id, jSessionId, authentication, locale);
		this.richClientAttached = richClientAttached;
		this.serverUri = serverUri;
	}

	public boolean isRichClientAttached() {
		return richClientAttached;
	}

	@Nullable
	public URI getServerUri() {
		return this.serverUri;
	}
}
