//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.servermeta.ejb3;

import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.dal.provider.NuclosDalProvider;
import org.nuclos.server.i18n.DataLanguageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Special "no-login"-service class. Keep small and secure ;)
 */
@Component("serverMetaService")
public class ServerMetaFacadeBean implements ServerMetaFacadeRemote {
	
	private ServerParameterProvider serverParameterProvider;

	private final NuclosDalProvider nuclosDalProvider;
	
	public ServerMetaFacadeBean(final NuclosDalProvider nuclosDalProvider) {
		this.nuclosDalProvider = nuclosDalProvider;
	}
	
	@Autowired
	void setServerParameterProvider(ServerParameterProvider serverParameterProvider) {
		this.serverParameterProvider = serverParameterProvider;
	}
	
	public String getServerProperty(String key) {
		if(key.startsWith("application.settings.client."))
			return serverParameterProvider.getValue(key);
		return "<no access>";
	}

	public String getServerDefaultTimeZone() {
		return TimeZone.getDefault().getID();
	}
	
	public String getDefaultNuclosTheme() {
		return serverParameterProvider.getValue(ParameterProvider.KEY_DEFAULT_NUCLOS_THEME);
	}

	@Override
	public boolean isMandatorPresent() {
		return SecurityCache.getInstance().isMandatorPresent();
	}

	@Override
	public Collection<DataLanguageVO> getDataLanguages() {
		// moved from ServerParameterProvider...
		/*
		List<DataLanguageVO> retVal = new ArrayList<DataLanguageVO>();

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.DATA_LANGUAGE);
		query.multiselect(t.baseColumn(E.DATA_LANGUAGE.language),
				t.baseColumn(E.DATA_LANGUAGE.country),
				t.baseColumn(E.DATA_LANGUAGE.primaryLanguage),
				t.baseColumn(E.DATA_LANGUAGE.order));

		query.orderBy(
				builder.desc(t.baseColumn(E.DATA_LANGUAGE.primaryLanguage)),
				builder.asc(t.baseColumn(E.DATA_LANGUAGE.order)));

		for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
			DataLanguageVO part = new DataLanguageVO(
					tuple.get(0, String.class),
					tuple.get(1, String.class),
					tuple.get(2, Boolean.class),
					tuple.get(3, Integer.class));
			retVal.add(part);
		}
		return retVal;
		 */
		return nuclosDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).getAll().stream()
				.map(eo -> new DataLanguageVO(
						eo.getFieldValue(E.DATA_LANGUAGE.language),
						eo.getFieldValue(E.DATA_LANGUAGE.country),
						eo.getFieldValue(E.DATA_LANGUAGE.primaryLanguage),
						eo.getFieldValue(E.DATA_LANGUAGE.order)
				))
				.sorted(new Comparator<DataLanguageVO>() {
					@Override
					public int compare(final DataLanguageVO l1, final DataLanguageVO l2) {
						if (Objects.equals(l1.isPrimary(), l2.isPrimary())) {
							return Integer.compare(Optional.ofNullable(l1.getOrder()).orElse(0),
												   Optional.ofNullable(l2.getOrder()).orElse(0));
						} else {
							return Boolean.TRUE.equals(l1.isPrimary()) ? -1 : 1;
						}
					}
				})
				.collect(Collectors.toList());
	}

	@Override
	public String getNuclosFontFamily() {
		return serverParameterProvider.getValue(ParameterProvider.KEY_NUCLOS_FONT_FAMILY);
	}

	@Override
	public boolean isPasswordBasedAuthenticationAllowed() {
		return serverParameterProvider.isEnabled(ParameterProvider.KEY_SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED, true);
	}

}
