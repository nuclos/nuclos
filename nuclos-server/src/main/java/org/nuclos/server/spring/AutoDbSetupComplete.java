//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.spring;

import java.io.File;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.businessentity.facade.NucletFacade;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PostConstructManager;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.spring.AnnotationJaxb2Marshaller;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.anonymize.DeleteSessionEntriesTask;
import org.nuclos.server.anonymize.HistoryAnonymizerTask;
import org.nuclos.server.cluster.ClusterConstants;
import org.nuclos.server.cluster.ClusterRegistration;
import org.nuclos.server.cluster.RigidClusterHelper;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.customcode.CustomClassLoaderSupport;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.dbtransfer.AutoNucletImport;
import org.nuclos.server.dbtransfer.TransferFacadeBean;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.job.NuclosSchedulerFactoryBean;
import org.nuclos.server.maintenance.MaintenanceFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean;
import org.nuclos.server.security.FailDuringStartupAuthenticationProvider;
import org.nuclos.server.security.NuclosAuthenticationProvider;
import org.nuclos.server.validation.DefaultSchemaValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.web.context.WebApplicationContext;

/**
 * In the server, the Spring startup context is minimized to (only) allow AutoDbSetup (and migration).
 * This Spring Bean will be called to trigger the initialization of the 'real' big spring server context.
 * 
 * @see DispatcherApplicationContextInitializer
 * @author Thomas Pasch
 * @since Nuclos 4.0
 */
public class AutoDbSetupComplete implements InitializingBean, ApplicationContextAware {

	private static final Logger LOG = LoggerFactory.getLogger(AutoDbSetupComplete.class);
	
	private static final String[] SERVER_SPRING_BEANS = new String[] {
		"classpath*:META-INF/nuclos/*-beans.xml"
	};
	
	public static class CompleteWebApplicationContext extends ClassPathXmlApplicationContext implements WebApplicationContext {
		
		private WebApplicationContext parentContext;

		private AnnotationConfigApplicationContext extensionContext;
		
		private CompleteWebApplicationContext(String[] configLocations, boolean refresh, ApplicationContext parent)
				throws BeansException {
			super(configLocations, refresh, parent);
			this.parentContext = (WebApplicationContext) parent;
		}

		@Override
		public ServletContext getServletContext() {
			return parentContext.getServletContext();
		}
		
		@Override
		public Resource getResource(String location) {
			return parentContext.getResource(location);
		}

		/**
		 * @return Returns the application context which is used to load and execute extensions.
		 * Regular rules are added by the {@link org.nuclos.server.customcode.CustomCodeManager} to a temporary
		 * application context similar to the temporary {@link org.nuclos.server.customcode.codegenerator.RuleClassLoader},
		 * this one is only for system rules or extensions without rules.
		 */
		public synchronized AnnotationConfigApplicationContext getExtensionContext() {
			if (extensionContext == null) {
				extensionContext = new AnnotationConfigApplicationContext();
				extensionContext.setParent(this);
			}
			return extensionContext;
		}

		@Override
		public void close() {
			try {
				if (this.extensionContext != null) {
					this.extensionContext.close();
					this.extensionContext = null;
				}
			} finally {
				super.close();
			}
		}

		/*
		@Override
		public ClassLoader getClassLoader() {
			return parentContext.getClassLoader();
		}
		*/

	}
	
	private static final AtomicReference<WebApplicationContext> MAIN_CONTEXT = new AtomicReference<WebApplicationContext>();
	
	//

	private long testDelay;
	
	private WebApplicationContext startupContext;
	
	// Spring injection
	
	@Autowired
	Timer timer;
	
	@Autowired
	FailDuringStartupAuthenticationProvider authenticationProvider;
	
	// end of Spring injection

	AutoDbSetupComplete() {
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (PostConstructManager.isDisabled()) {
			return;
		}
		if (testDelay > 0) {
			// test if the 'real' spring context will start
			final TimerTask task = new TimerTask() {
				@Override
				public void run() {
					try {
						afterAutoDbSetup();
					} catch (Exception e) {
						LOG.error("afterAutoDbSetup failed: {}", e, e);
					}
				}
			};
			timer.schedule(task, testDelay);
		}
	}

	public boolean afterAutoDbSetup() {
		RigidClusterHelper.writeStatus("Starting");
		final WebApplicationContext mc = createMainContext();

		try {
			if (!setupCluster()) {
				// cluster could not startup, abort afterAutoDbSetup
				return false;
			}
		} catch (BusinessException e) {
			throw new NuclosFatalException("Failed to setup cluster: " + e.getMessage(), e);
		}
		final ServerParameterProvider parameterProvider = mc.getBean(ServerParameterProvider.class);
		final MaintenanceFacadeBean maintenanceFacade = mc.getBean(MaintenanceFacadeBean.class);
		final NucletFacade nucletFacade = mc.getBean(NucletFacade.class);
		final CustomCodeManager customCodeManager = mc.getBean(CustomCodeManager.class);

		// load extensions but without regular rules (these require the generated bo classes and could therefore
		// only be loaded later by the RuleClassLoader)
		mc.getBean(CustomClassLoaderSupport.class).loadExtensions();
		mc.getBean(EventSupportCache.class).loadExternalOrSystemEventSupportFiles();

		// auto nuclet imports
		boolean nucletsImported = false;
		if (!RigidClusterHelper.isClusterEnabled()  || RigidClusterHelper.isMaster()) {
			nucletsImported = !AutoNucletImport.scanForNuclets(
					parameterProvider, maintenanceFacade,
					mc.getBean(TransferFacadeBean.class),
					mc.getBean(MasterDataFacadeBean.class),
					nucletFacade, customCodeManager,
					new File(NuclosSystemParameters.getDirectory(NuclosSystemParameters.HOME), "/data/nucletimport")
			).isEmpty();
		}

		if (!nucletsImported) {
			try {
				nucletFacade.asyncUpdateExtensionsIfNecessary().join();
				if (nucletFacade.areExtensionsUpToDate()) {
					final EventSupportFacadeLocal eventSupportFacadeLocal = mc.getBean(EventSupportFacadeLocal.class);
					// create and load BusinessObjects on startup
					int cores = Runtime.getRuntime().availableProcessors();
					ForkJoinPool builderThreadPool = new ForkJoinPool(cores);
					try {
						eventSupportFacadeLocal.createBusinessObjects(builderThreadPool);
					} catch (NuclosCompileException e) {
						LOG.warn("Compile errors while creating business objects: {}", e, e);
					} catch (Exception e) {
						Marker fatal = MarkerFactory.getMarker("FATAL");
						LOG.error(fatal, "Failed to create business objects: {}", e, e);
					} finally {
						builderThreadPool.shutdown();
					}
				}
			} catch (InterruptedException e) {
				LOG.error("Failed to update extensions: {}", e, e);
			}
		}

		// set the 'real' authentication provider (tp)
		final NuclosAuthenticationProvider nuclosAuthenticationProvider = mc.getBean(NuclosAuthenticationProvider.class);
		authenticationProvider.setNuclosAuthenticationProvider(nuclosAuthenticationProvider);
		
		// set the 'real' jaxb2Marshaller in SchemaHelper (tp)
		final AnnotationJaxb2Marshaller jaxb2Marshaller = mc.getBean(AnnotationJaxb2Marshaller.class);
		
		final boolean setContext = MAIN_CONTEXT.compareAndSet(null, mc);
		assert setContext;
		synchronized (MAIN_CONTEXT) {
			MAIN_CONTEXT.notify();
		}

		if (!customCodeManager.hasClassLoader()
				&& maintenanceFacade.isMaintenanceOff()) {
			LOG.info("CustomCodeManager is not ready, starting maintenance mode!");
			maintenanceFacade.forceMaintenanceMode();
			maintenanceFacade.enterMaintenanceMode(parameterProvider.getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER));
		}

		// The final 3rd step...
		SpringApplicationContextHolder.setSpringReady();

		if (nucletFacade.areExtensionsUpToDate()
				&& customCodeManager.hasClassLoader()
				&& maintenanceFacade.isMaintenanceOff()) {
			// only a fallback here, a SpringReady listener might have already started the scheduler
			mc.getBean(NuclosSchedulerFactoryBean.class).startNuclosScheduler();
		}
		
		if (NuclosSystemParameters.is(NuclosSystemParameters.AUTOSETUP_VALIDATE_SCHEMA_ENABLED, false) &&
				RigidClusterHelper.isMaster()) {
			try {
				DefaultSchemaValidation.removeSysContraints(NuclosSystemParameters.AUTOSETUP_VALIDATE_SCHEMA_ENABLED);
				DefaultSchemaValidation.validate(true, jaxb2Marshaller, true, null);
			} catch (Exception e) {
				LOG.error("remove sys constraints before {} failed: {}",
				          NuclosSystemParameters.AUTOSETUP_VALIDATE_SCHEMA_ENABLED, e.getMessage(),
				          e);
			} finally {
				try {
					DefaultSchemaValidation.createSysConstraints(NuclosSystemParameters.AUTOSETUP_VALIDATE_SCHEMA_ENABLED);
				} catch (Exception e) {
					LOG.error("recreate sys constraints after {} failed: {}",
					          NuclosSystemParameters.AUTOSETUP_VALIDATE_SCHEMA_ENABLED, e.getMessage(), e);
					throw new NuclosFatalException("recreate sys constraints after entity remove failed: " + e.getMessage(), e);
				} 
			}
		}
		
		File docUploadDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_UPLOAD_PATH);
		if (docUploadDir != null) {
			if (docUploadDir.exists()) {
				if (!docUploadDir.isDirectory()) {
					LOG.error("{} must be a directory!", docUploadDir);
				} else {
					try {
						FileUtils.cleanDirectory(docUploadDir);
					} catch (Exception ex) {
						LOG.error("Error during cleanup of document upload directory: {}", ex.getMessage(), ex);
					}
				}
			} else {
				try {
					if (!docUploadDir.mkdirs()) {
						LOG.error("Directory {} could not be created!", docUploadDir);
					}
				} catch (Exception ex) {
					LOG.error("Error during creation of document upload directory: {}", ex.getMessage(), ex);
				}
			}

		}

		RigidClusterHelper.writeStatus(ClusterConstants.STATUS_RUNNING);

		setupAnonymizerTasks();

		return true;
	}

	private void setupAnonymizerTasks() {
		// only on master cluster node
		if (RigidClusterHelper.isClusterEnabled() && !RigidClusterHelper.isMaster()) {
			return;
		}

		// Anonymize history task
		// next sunday @ 00:00:00
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY,0);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);

		TimerTask task = new HistoryAnonymizerTask();
		timer.schedule(task, cal.getTime(), TimeUnit.MILLISECONDS.convert(7, TimeUnit.DAYS));

		// Delete sessions task
		// every day @ 00:00:00
		cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY,0);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);

		task = new DeleteSessionEntriesTask();
		timer.schedule(task, cal.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
	}

	/*
	 * CLUSTERING
	 * check if Nuclos run in Cluster Mode
	 * if true ? register && ping server && check all other registered Servers : do nothing
	 */
	private boolean setupCluster() throws BusinessException {
		if (RigidClusterHelper.isClusterEnabled()) {
			ClusterRegistration clusterRegistration = SpringApplicationContextHolder.getBean(ClusterRegistration.class);
			return clusterRegistration.startup();
		}
		return true;
	}
	
	private WebApplicationContext createMainContext() {
		final CompleteWebApplicationContext result = new CompleteWebApplicationContext(SERVER_SPRING_BEANS, false, startupContext);
		result.setClassLoader(startupContext.getClassLoader());
		result.refresh();
		return result;
	}

	public long getTestDelay() {
		return testDelay;
	}

	public void setTestDelay(long testDelay) {
		this.testDelay = testDelay;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		startupContext = (WebApplicationContext) applicationContext;
	}
	
	public static WebApplicationContext getMainContext() throws InterruptedException {
		WebApplicationContext result = null;
		do {
			result = MAIN_CONTEXT.get();
			if (result != null) break;
			synchronized (MAIN_CONTEXT) {
				LOG.warn("Spring main context not initialized, waiting ...");
				MAIN_CONTEXT.wait(500);				
			}
		} while (result == null);
		return result;
	}

}
