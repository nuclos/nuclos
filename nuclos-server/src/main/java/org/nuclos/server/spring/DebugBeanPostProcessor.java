//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.spring;

import java.util.Arrays;

import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class DebugBeanPostProcessor implements BeanPostProcessor {
	
	private static final Logger LOG = LoggerFactory.getLogger(DebugBeanPostProcessor.class);

	DebugBeanPostProcessor() {
		if (LOG.isInfoEnabled()) {
			// reduce the log level to warn for the BeanPostProcessorChecker, otherwise the log will be spammed with:
			// 			[org.springframework.context.support.PostProcessorRegistrationDelegate$BeanPostProcessorChecker] -
			// 			Bean 'nuclosJavaCompilerComponent' of type [org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerComponent]
			// 			is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
			org.apache.log4j.Logger.getLogger("org.springframework.context.support.PostProcessorRegistrationDelegate$BeanPostProcessorChecker").setLevel(Level.WARN);
		}
	}
	
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("init: {}", beanName);
		}

		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (LOG.isDebugEnabled() && shouldLog(beanName)) {
			final Class<?> beanClass = bean.getClass();
			LOG.debug("after init: {} -> {} ({}) direct interfaces: {}",
			         beanName, beanClass.getName(), bean, Arrays.asList(beanClass.getInterfaces()));
		}
		return bean;
	}
	
	private boolean shouldLog(String beanName) {
		return beanName.endsWith("Local") || beanName.startsWith("org.nuclos.server.spring.");
	}
}
