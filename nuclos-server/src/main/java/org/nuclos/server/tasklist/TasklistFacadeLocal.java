package org.nuclos.server.tasklist;

import java.util.Collection;

import org.nuclos.common.UID;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

public interface TasklistFacadeLocal {

	Collection<TasklistDefinition> getUsersTasklists();

	void invalidateCaches();

	TasklistDefinition getTasklistDefinition(UID taskListUID) throws CommonFinderException, CommonPermissionException;
}
