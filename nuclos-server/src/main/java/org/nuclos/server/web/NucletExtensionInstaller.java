package org.nuclos.server.web;

import static org.nuclos.server.extension.RigidExtensionUtils.getNucletExtensionPathEntries;
import static org.nuclos.server.extension.RigidExtensionUtils.getNucletExtensionsServerHomePath;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletContext;

import org.slf4j.LoggerFactory;

public class NucletExtensionInstaller {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NucletExtensionInstaller.class);

	public static Path getServerExtensionPathFromContext(final ServletContext ctx) {
		return getServerExtensionPathFromContext(ctx, false);
	}

	private static Path getServerExtensionPathFromContext(final ServletContext ctx, boolean bCreate) {
		String sExtensionsLibDir = ctx.getRealPath("/WEB-INF/extensions/");
		Path extensionsLibDir = sExtensionsLibDir != null ? Paths.get(sExtensionsLibDir) : null;
		if (extensionsLibDir != null) {
			if (!extensionsLibDir.toFile().exists() && bCreate) {
				try {
					Files.createDirectory(extensionsLibDir);
				} catch (IOException ex) {
					LOG.error("Unexpected IO ERROR: {} on {}", ex, extensionsLibDir, ex);
					return null;
				}
			}
			if (extensionsLibDir.toFile().isDirectory()) {
				return extensionsLibDir;
			}
		}
		return null;
	}

	static void updateAndInstallServerExtensions(final ServletContext ctx) {
		Path extensionsLibDir = getServerExtensionPathFromContext(ctx, true);
		// 1. delete all nuclet extensions from extensionsLibDir
		if (extensionsLibDir != null) {
			LOG.info("Nuclet server extensions lib dir: {}", extensionsLibDir.toString());
			try {
				getNucletExtensionPathEntries(extensionsLibDir).forEach(NucletExtensionInstaller::uninstallExtension);
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, extensionsLibDir, ex);
			}
		}
		// 2. copy all nuclet extensions to extensionsLibDir
		final Path homeExtensionsServer = getNucletExtensionsServerHomePath();
		if (homeExtensionsServer != null && homeExtensionsServer.toFile().exists() && homeExtensionsServer.toFile().isDirectory() &&
			extensionsLibDir != null) {
			try {
				getNucletExtensionPathEntries(homeExtensionsServer).forEach(ext -> installExtension(ext, extensionsLibDir));
			} catch (IOException ex) {
				LOG.error("Unexpected IO ERROR: {} on {}", ex, extensionsLibDir, ex);
			}
		}
	}

	private static void installExtension(Path ext, Path toDir) {
		try {
			final Path newFile = toDir.resolve(ext.getFileName());
			Files.copy(ext, newFile);
		} catch (IOException ex) {
			LOG.error("Extension could not be installed: {} on {} -> {}", ex, ext, toDir, ex);
		}
	}

	private static void uninstallExtension(Path file) {
		try {
			Files.delete(file);
		} catch (IOException ex) {
			LOG.error("Extension could not be uninstalled: {} on {}", ex, file, ex);
		}
	}

}
