//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * A (http) request scoped spring bean for having easy support for request within Nuclos server.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.0.10
 */
//startup context: @Component does NOT work (tp)
public class Request {
	
	// Spring injection
	
	@Autowired
	private HttpServletRequest httpRequest;
	
	// end of Spring injection
	
	public Request() {	
	}
	
	public HttpServletRequest getHttpRequest() {
		return httpRequest;
	}

}
