//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.web;

import java.io.Serializable;
import java.text.DateFormat;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * A (http) session scoped spring bean for having easy support for session within Nuclos server.
 * <p>
 * This class MUST be {@link Serializable} because session-scoped beans life in the HttpSession 
 * and hence might be serialized to disk.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.0.10
 */
//startup context: @Component does NOT work (tp)
public class Session implements Serializable {
	
	private static final DateFormat DATE_FORMAT = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
	
	// Spring injection
	
	@Autowired
	private HttpSession httpSession;
	
	// end of Spring injection
	
	private String username;
	
	private String remoteAddress;
	
	private int remotePort = -9999;
	
	public Session() {	
	}
	
	static DateFormat getDateFormat() {
		return (DateFormat) DATE_FORMAT.clone();
	}
	
	public HttpSession getHttpSession() {
		return httpSession;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	public int getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(int remotePort) {
		this.remotePort = remotePort;
	}

	public void invalidate() {
		httpSession.invalidate();
		username = null;
		remoteAddress = null;
		remotePort = -9999;
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		if (!(other instanceof Session)) return false;
		final Session o = (Session) other;
		return httpSession.getId().equals(o.httpSession.getId());
	}
	
	@Override
	public int hashCode() {
		int result = 111 + 17 * httpSession.getId().hashCode();
		return result;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("Session[");
		if (httpSession != null) {
			result.append("id=").append(httpSession.getId());
			result.append(",last=").append(
					getDateFormat().format(httpSession.getLastAccessedTime()));
		}
		if (username != null) {
			result.append(",user=").append(username);
		}
		if (remoteAddress != null) {
			result.append(",from=").append(remoteAddress).append(":").append(remotePort);
		}
		result.append("]");
		return result.toString();
	}

}
