package org.nuclos.server.webclient;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.WebclientService;
import org.nuclos.api.webclient.WebclientUrl;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.startup.NuclosEnviromentConstants;
import org.nuclos.server.cluster.RigidClusterHelper;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.security.WebSessionContext;
import org.nuclos.server.web.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component("webclientServiceProvider")
public class WebclientServerServiceProvider implements WebclientService {

	@Autowired
	ServerParameterProvider parameterProvider;

	@Autowired
	MasterDataRestFqnCache fqnCache;

	@Autowired private SecurityCache securityCache;

	@Override
	public URL buildUrl(final WebclientUrl webclientUrl) throws BusinessException {
		try {
			URL mainUrl = getWebclientMainUrl();
			StringBuilder detailPart = new StringBuilder();
			if (webclientUrl != null) {
				if (webclientUrl.getEntityId() != null) {
					detailPart.append("/view/");
					detailPart.append(fqnCache.translateUid(E.ENTITY, (UID) webclientUrl.getEntityId()));
				}
				if (webclientUrl.getBusinessObjectId() != null) {
					detailPart.append("/");
					if (webclientUrl.getBusinessObjectId() instanceof UID) {
						detailPart.append(URLEncoder.encode(((UID) webclientUrl.getBusinessObjectId()).getString(), "UTF-8"));
					} else if (webclientUrl.getBusinessObjectId() instanceof Number) {
						detailPart.append(String.format("%d", (Number) webclientUrl.getBusinessObjectId()));
					} else {
						detailPart.append(URLEncoder.encode(webclientUrl.getBusinessObjectId().toString(), "UTF-8"));
					}
				}
				Map<String, String> queryParams = new HashMap<>();
				if (webclientUrl.getSearchFilterId() != null) {
					queryParams.put("searchFilterId", URLEncoder.encode(webclientUrl.getSearchFilterId(), "UTF-8"));
				}
				if (webclientUrl.getTaskListId() != null) {
					queryParams.put("taskListId", URLEncoder.encode(webclientUrl.getTaskListId(), "UTF-8"));
				}
				if (webclientUrl.getProcessId() != null) {
					queryParams.put("searchFilterId", URLEncoder.encode(((UID)webclientUrl.getProcessId()).getString(), "UTF-8"));
				}
				if (!queryParams.isEmpty()) {
					detailPart.append('?');
					detailPart.append(queryParams.keySet().stream()
							.sorted()
							.map(key -> String.format("%s=%s",
											key, queryParams.get(key)))
							.collect(Collectors.joining("&")));
				}
			}

			return new URL(mainUrl.toExternalForm() // toExternalForm preserves the hash "#" in the main url,
														  // otherwise it will be accidentally removed!
					+ detailPart.toString());
		} catch (MalformedURLException | UnsupportedEncodingException e) {
			throw new BusinessException(e.getMessage());
		}
	}

	private URL getWebclientMainUrl() throws BusinessException, MalformedURLException {
		String webclient = NuclosEnviromentConstants.WEBCLIENT_DEFAULT_URL;

		String webclientBaseFromSystemparameter = parameterProvider.getValue(ParameterProvider.KEY_WEBCLIENT_BASEURL);
		if (!RigidUtils.looksEmpty(webclientBaseFromSystemparameter)) {
			if (webclientBaseFromSystemparameter.startsWith("http://")
					|| webclientBaseFromSystemparameter.startsWith("https://")) {
				return new URL(webclientBaseFromSystemparameter);
			} else {
				webclient = webclientBaseFromSystemparameter;
			}
		}

		String server = "";
		if (RigidClusterHelper.isClusterEnabled()) {
			String protocol = RigidClusterHelper.getBalancerProtocol();
			Integer port = RigidClusterHelper.getBalancerPort();
			if (("https".equals(protocol) && 443 == port) ||
					("http".equals(protocol) && 80 == port)) {
				server = String.format("%s://%s", protocol, port);
			} else {
				server = String.format("%s://%s:%d", protocol, RigidClusterHelper.getBalancerHostname(), port);
			}
		}

		// try to get server from session context
		if (RigidUtils.looksEmpty(server)) {
			Session session = securityCache.getSession();
			if (session != null) {
				HttpSession httpSession = null;
				try {
					httpSession = session.getHttpSession();
				} catch (Exception e) {
					// No thread-bound request found.. ignore! (probably called by a job)
				}
				if (httpSession != null) {
					String sessionId = httpSession.getId();
					if (sessionId != null) {
						SessionContext sessionContext = securityCache.getSessionContext(sessionId);
						if (sessionContext instanceof WebSessionContext) {
							URI serverUri = ((WebSessionContext) sessionContext).getServerUri();
							if (serverUri != null) {
								String s = serverUri.toString();
								if (!s.startsWith("//")) {
									// protocol-relative URLs are not allowed,
									// and will produce a MalformedURLException
									server = s;
								}
							}
						}
					}
				}
			}
		}

		// try to get server from request
		if (RigidUtils.looksEmpty(server)) {
			RequestAttributes requestAttributes = null;
			try {
				requestAttributes = RequestContextHolder.currentRequestAttributes();
			} catch (Exception e) {
				// No thread-bound request found.. ignore! (probably called by a job)
			}
			if (requestAttributes instanceof ServletRequestAttributes) {
				HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
				server = request.getRequestURL().toString();
			}
		}

		if (RigidUtils.looksEmpty(server)) {
			throw new BusinessException("Webclient URL could not be determined. Set an absolut value in the system parameter WEBCLIENT_BASEURL.");
		}

		// remove context(/rest) part from request or server URI, we only need the pure servername(:port)
		Pattern pattern = Pattern.compile("[^/]/[^/]");
		Matcher matcher = pattern.matcher(server);
		if (matcher.find()) {
			server = server.substring(0, matcher.start() + 1);
		}

		return new URL(new URL(server), webclient);
	}

}
