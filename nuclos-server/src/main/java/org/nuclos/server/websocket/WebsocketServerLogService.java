package org.nuclos.server.websocket;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.AbstractConfiguration;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Message;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.Broadcaster;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.security.SessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@ManagedService(path = "/websocket/serverlog")
public class WebsocketServerLogService {
	private static final Logger log = LoggerFactory.getLogger(WebsocketServerLogService.class);

	@Inject
	@Named("NuclosServerLogBroadcaster")
	private Broadcaster broadcaster;

	public WebsocketServerLogService() {
		final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		final AbstractConfiguration config = (AbstractConfiguration) ctx.getConfiguration();

		Layout layout = getLayout(config);

		// Create and add the appender
		CustomAppender appender = new CustomAppender(this, "Custom", null, layout);
		appender.start();

		config.getRootLogger().addAppender(appender, null, null);

		ctx.updateLoggers();
	}

	/**
	 * Tries to lookup the Console Appender and returns its Layout.
	 * Returns a default Layout if there is no Console Appender.
	 *
	 * @param config
	 * @return
	 */
	private Layout getLayout(final AbstractConfiguration config) {
		Layout layout = null;
		Appender consoleAppender = config.getRootLogger().getAppenders().get("Console");
		if (consoleAppender != null) {
			layout = consoleAppender.getLayout();
		} else {
			layout = PatternLayout.createDefaultLayout();
		}
		return layout;
	}

	/**
	 * Invoked when the connection as been fully established and suspended, e.g
	 * ready for receiving messages.
	 *
	 * @param r the atmosphere resource
	 * @throws IOException
	 */
	@Ready
	public final void onReady(final AtmosphereResource r) throws IOException {
		log.info("Client {} connected.", r.uuid());

		validateSession(r);

		// Add the resource to the log broadcaster only after the superuser session was successfully validated
		broadcaster.addAtmosphereResource(r);
	}

	/**
	 * Invoked when the client disconnect or when an unexpected closing of the
	 * underlying connection happens.
	 *
	 * @param event the event
	 * @throws IOException
	 */
	@Disconnect
	public final void onDisconnect(final AtmosphereResourceEvent event) throws IOException {
		final AtmosphereResource resource = event.getResource();
		if (event.isCancelled()) {
			log.info("Client {} unexpectedly disconnected", resource.uuid());
		} else if (event.isClosedByClient()) {
			log.info("Client {} closed the connection", resource.uuid());
		}
	}

	/**
	 * Validates the current session and returns the corresponding SessionContext.
	 *
	 * @param r
	 * @return
	 * @throws IOException
	 */
	private SessionContext validateSession(final AtmosphereResource r) throws IOException {
		final String sessionId = r.getRequest().getSession().getId();
		final SessionContext session = Rest.facade().validateSessionAuthenticationAndInitUserContext(sessionId);
		if (session == null || !session.getAuthentication().isAuthenticated() || !Rest.facade().isSuperUser()) {
			r.write(r.uuid() + " UNAUTHORIZED - connection will be closed now");
			r.close();
			throw new NuclosWebException(Response.Status.UNAUTHORIZED);
		}
		return session;
	}

	@Message(encoders = {DefaultCodec.class}, decoders = {DefaultCodec.class})
	public final ServerLogMessage onMessage(final ServerLogMessage message) throws IOException {
		return message;
	}

	private synchronized void broadcast(ServerLogMessage message) {
		// Broadcaster might not have been initialized yet
		if (broadcaster != null) {
			broadcaster.broadcast(message);
		}
	}

	public class ServerLogMessage {
		private final Date date;
		private final String level;
		private final String loggerName;
		private final String message;

		public ServerLogMessage(final Date date, final String level, final String loggerName, final String message) {
			this.date = date;
			this.level = level;
			this.loggerName = loggerName;
			this.message = message;
		}

		public Date getDate() {
			return date;
		}

		public String getLevel() {
			return level;
		}

		public String getLoggerName() {
			return loggerName;
		}
		
		public String getMessage() {
			return message;
		}
	}

	public class CustomAppender extends AbstractAppender {

		private WebsocketServerLogService websocketServerLogService;

		public CustomAppender(
				WebsocketServerLogService websocketServerLogService,
				String name,
				Filter filter,
				Layout<? extends Serializable> layout
		) {
			super(name, filter, layout);

			this.websocketServerLogService = websocketServerLogService;
		}

		@Override
		public void append(LogEvent event) {
			ServerLogMessage serverLogMessage = new ServerLogMessage(
					new Date(event.getTimeMillis()),
					event.getLevel().name(),
					event.getLoggerName(),
					event.getMessage().getFormattedMessage()
			);

			websocketServerLogService.broadcast(serverLogMessage);
		}

		@Override
		public void stop() {
		}

	}
}

