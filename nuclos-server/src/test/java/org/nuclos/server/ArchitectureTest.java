package org.nuclos.server;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;
import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

import org.junit.runner.RunWith;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchIgnore;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.thirdparty.com.google.common.collect.Iterators;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
		packages = {
				"org.nuclos.server",
				"org.nuclos.businessentity",
		},
		importOptions = ImportOption.DontIncludeTests.class
)
public class ArchitectureTest {
	@ArchTest
	public static void checkClassCount(JavaClasses classes) {
		// There are ~10k Nuclos server/businessentity classes at the moment
		assert Iterators.size(classes.iterator()) > 9000 : "Only " + Iterators.size(classes.iterator()) + " classes found";
	}

	@ArchTest
	@ArchIgnore    // TODO: Currently impossible to execute this test, because of too much complexity (too many cycles?!)
	public static final ArchRule noCycles = slices().matching("org.nuclos.server.(*)..").should().beFreeOfCycles();

	/**
	 * TODO: There is a corresponding ArchRule in the global ArchitectureTest, see org.nuclos.test.architecture.ArchitectureTest#noSun
	 * 	Once there are no more usages of sun.* in the desktop client, remove this ArchRule here
	 * 	and activate the global one instead.
	 */
	@ArchTest
	public static final ArchRule noSun = noClasses().should().accessClassesThat().resideInAPackage("sun.*");
}
