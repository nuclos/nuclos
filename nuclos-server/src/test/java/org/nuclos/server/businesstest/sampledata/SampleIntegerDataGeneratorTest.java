package org.nuclos.server.businesstest.sampledata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SampleIntegerDataGeneratorTest {

	@Test
	public void testDataGeneration() {
		SampleIntegerDataGenerator generator = new SampleIntegerDataGenerator();

		generator.addSampleValues(Arrays.asList(42));

		assert generator.newValue() == 42;

		generator.addSampleValues(Arrays.asList(45));

		List<Date> values = new ArrayList(Arrays.asList(42, 43, 44, 45));
		while (!values.isEmpty()) {
			Integer newValue = generator.newValue();

			assert newValue >= 42;
			assert newValue <= 45;

			values.remove(newValue);
		}
	}
}