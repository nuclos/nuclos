package org.nuclos.server.businesstest.sampledata;

import java.util.Arrays;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SampleTextDataGeneratorTest {

	@Test
	public void testDataGeneration() {
		SampleTextDataGenerator generator = new SampleTextDataGenerator();

		assert generator.newValue().equals("");

		generator.addSampleValues(Arrays.asList("", "AbC123$'#.", null));

		assert generator.newValue().equals("AbC123$'#.");
		assert generator.newValue().equals("AbC123$'#.");

		generator.addSampleValues(Arrays.asList("X", "Y", "Z"));

		assert generator.newValue().matches("^[AXYZ]bC123\\$'#\\.$");
	}
}