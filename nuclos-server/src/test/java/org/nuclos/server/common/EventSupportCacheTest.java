package org.nuclos.server.common;

import org.junit.Test;
import org.nuclos.common.Mutable;
import org.nuclos.common.UID;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Marvin Vitcu <marvin.vitcu@nuclos.de>
 */

public class EventSupportCacheTest {

    /**
     * Test for NUCLOS-7496
     */

    @Test
    @SuppressWarnings("unchecked")
    public void testInterfaceExtractor() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        String textNoComment = "package nuclet.test.rules;\n" +
                "\n" +
                "import org.nuclos.api.annotation.Rule; \n" +
                "\n" +
                "import org.nuclos.api.exception.BusinessException; \n" +
                "import org.nuclos.api.provider.BusinessObjectProvider;\n" +
                "\n" +
                "import org.nuclos.api.rule.GenerateFinalRule;\n" +
                "import org.nuclos.api.context.GenerateContext; \n" +
                "\n" +
                "import org.nuclos.api.rule.InsertRule; \n" +
                "import org.nuclos.api.context.InsertContext;\n" +
                "\n" +
                "import org.nuclos.api.rule.DeleteRule; \n" +
                "import org.nuclos.api.context.DeleteContext; \n" +
                "\n" +
                "@Rule(name = \"TestRule\", description = \"rule for testing\") \n" +
                "public class TestRule implements GenerateFinalRule, DeleteRule, InsertRule\n" +
                "{\n" +
                " \n" +
                "\tpublic void insert(InsertContext context) throws BusinessException {\n" +
                "\t\n" +
                "\t}\n" +
                " \n" +
                "\tpublic void generateFinal(GenerateContext context) throws BusinessException {\n" +
                "\t\t\n" +
                "\t}\n" +
                "\n" +
                "\tpublic void delete(DeleteContext context) throws BusinessException {\n" +
                "\t\n" +
                "\t}\n" +
                "}";

        String textOneLineComment = "package nuclet.test.rules;\n" +
                "\n" +
                "import org.nuclos.api.annotation.Rule; \n" +
                "\n" +
                "import org.nuclos.api.exception.BusinessException; \n" +
                "import org.nuclos.api.provider.BusinessObjectProvider;\n" +
                "\n" +
                "import org.nuclos.api.rule.GenerateFinalRule;\n" +
                "import org.nuclos.api.context.GenerateContext; \n" +
                "\n" +
                "import org.nuclos.api.rule.InsertRule; \n" +
                "import org.nuclos.api.context.InsertContext;\n" +
                "\n" +
                "import org.nuclos.api.rule.DeleteRule; \n" +
                "import org.nuclos.api.context.DeleteContext; \n" +
                "\n" +
                "@Rule(name = \"TestRule\", description = \"rule for testing\") \n" +
                "public class TestRule implements GenerateFinalRule, DeleteRule, InsertRule //TEST one line comment\n" +
                "{\n" +
                " \n" +
                "\tpublic void insert(InsertContext context) throws BusinessException {\n" +
                "\t\n" +
                "\t}\n" +
                " \n" +
                "\tpublic void generateFinal(GenerateContext context) throws BusinessException {\n" +
                "\t\t\n" +
                "\t}\n" +
                "\n" +
                "\tpublic void delete(DeleteContext context) throws BusinessException {\n" +
                "\t\n" +
                "\t}\n" +
                "}";

        String textMultiLineComment = "package nuclet.test.rules;\n" +
                "\n" +
                "import org.nuclos.api.annotation.Rule; \n" +
                "\n" +
                "import org.nuclos.api.exception.BusinessException; \n" +
                "import org.nuclos.api.provider.BusinessObjectProvider;\n" +
                "\n" +
                "import org.nuclos.api.rule.GenerateFinalRule;\n" +
                "import org.nuclos.api.context.GenerateContext; \n" +
                "\n" +
                "import org.nuclos.api.rule.InsertRule; \n" +
                "import org.nuclos.api.context.InsertContext;\n" +
                "\n" +
                "import org.nuclos.api.rule.DeleteRule; \n" +
                "import org.nuclos.api.context.DeleteContext; \n" +
                "\n" +
                "@Rule(name = \"TestRule\", description = \"rule for testing\") \n" +
                "public class TestRule implements GenerateFinalRule, DeleteRule, InsertRule\n" +
                "/*TEST multi\n" +
                "line comment*/\n" +
                "{\n" +
                " \n" +
                "\tpublic void insert(InsertContext context) throws BusinessException {\n" +
                "\t\n" +
                "\t}\n" +
                " \n" +
                "\tpublic void generateFinal(GenerateContext context) throws BusinessException {\n" +
                "\t\t\n" +
                "\t}\n" +
                "\n" +
                "\tpublic void delete(DeleteContext context) throws BusinessException {\n" +
                "\t\n" +
                "\t}\n" +
                "}";

        String textOneAndMultiLineComment = "package nuclet.test.rules;\n" +
                "\n" +
                "import org.nuclos.api.annotation.Rule; \n" +
                "\n" +
                "import org.nuclos.api.exception.BusinessException; \n" +
                "import org.nuclos.api.provider.BusinessObjectProvider;\n" +
                "\n" +
                "import org.nuclos.api.rule.GenerateFinalRule;\n" +
                "import org.nuclos.api.context.GenerateContext; \n" +
                "\n" +
                "import org.nuclos.api.rule.InsertRule; \n" +
                "import org.nuclos.api.context.InsertContext;\n" +
                "\n" +
                "import org.nuclos.api.rule.DeleteRule; \n" +
                "import org.nuclos.api.context.DeleteContext; \n" +
                "\n" +
                "//TEST one line comment with the word implements\n" +
                "/*TEST multi line comment\n" +
                "with the word implements */{\n" +
                " \n" +
                "@Rule(name = \"TestRule\", description = \"rule for testing\") \n" +
                "public class TestRule implements GenerateFinalRule, DeleteRule, InsertRule\n" +
                "//TEST one line comment with bracket {\n" +
                "/*TEST multi line comment\n" +
                "with bracket {*/\n" +
                "{ \n" +
                " \n" +
                "\tpublic void insert(InsertContext context) throws BusinessException {\n" +
                "\t\n" +
                "\t}\n" +
                " \n" +
                "\tpublic void generateFinal(GenerateContext context) throws BusinessException {\n" +
                "\t\t\n" +
                "\t}\n" +
                "\n" +
                "\tpublic void delete(DeleteContext context) throws BusinessException {\n" +
                "\t\n" +
                "\t}\n" +
                "}";

            //using reflection because method is private
            String methodName = "getInterfacesForEventSupportSource";
            Class[] parameterTypes = new Class[5];
            parameterTypes[0] = String.class;
            parameterTypes[1] = Map.class;
            parameterTypes[2] = Mutable.class;
            parameterTypes[3] = Map.class;
            parameterTypes[4] = Mutable.class;

            Method privateStringInterfaceMethod = EventSupportCache.class.getDeclaredMethod(methodName, parameterTypes);

            privateStringInterfaceMethod.setAccessible(true);

            //test: no comment
            List<String> interfacesNoComment = (List<String>) privateStringInterfaceMethod.invoke(new EventSupportCache(),
                    textNoComment, new HashMap<String, UID>(), null, new HashMap<String, UID>(), null);

            assert interfacesNoComment.contains("org.nuclos.api.rule.GenerateFinalRule");
            assert interfacesNoComment.contains("org.nuclos.api.rule.DeleteRule");
            assert interfacesNoComment.contains("org.nuclos.api.rule.InsertRule");
            assert interfacesNoComment.size() == 3;

            //test: one line comment
            List<String> interfacesOneLineComment = (List<String>) privateStringInterfaceMethod.invoke(new EventSupportCache(),
                    textOneLineComment, new HashMap<String, UID>(), null, new HashMap<String, UID>(), null);

            assert interfacesOneLineComment.contains("org.nuclos.api.rule.GenerateFinalRule");
            assert interfacesOneLineComment.contains("org.nuclos.api.rule.DeleteRule");
            assert interfacesOneLineComment.contains("org.nuclos.api.rule.InsertRule");
            assert interfacesOneLineComment.size() == 3;

            //test: multi line comment
            List<String> interfacesMultiLineComment = (List<String>) privateStringInterfaceMethod.invoke(new EventSupportCache(),
                    textMultiLineComment, new HashMap<String, UID>(), null, new HashMap<String, UID>(), null);

            assert interfacesMultiLineComment.contains("org.nuclos.api.rule.GenerateFinalRule");
            assert interfacesMultiLineComment.contains("org.nuclos.api.rule.DeleteRule");
            assert interfacesMultiLineComment.contains("org.nuclos.api.rule.InsertRule");
            assert interfacesMultiLineComment.size() == 3;

            //test: one and multi line comment
            List<String> interfacesOneAndMultiLineComment = (List<String>) privateStringInterfaceMethod.invoke(new EventSupportCache(),
                    textOneAndMultiLineComment, new HashMap<String, UID>(), null, new HashMap<String, UID>(), null);

            assert interfacesOneAndMultiLineComment.contains("org.nuclos.api.rule.GenerateFinalRule");
            assert interfacesOneAndMultiLineComment.contains("org.nuclos.api.rule.DeleteRule");
            assert interfacesOneAndMultiLineComment.contains("org.nuclos.api.rule.InsertRule");
            assert interfacesOneAndMultiLineComment.size() == 3;
    }
}
