package org.nuclos.server.database;

import org.nuclos.server.dblayer.DbAccess;
import org.springframework.stereotype.Component;

@Component("dataBaseHelper")
public class SpringDataBaseHelper {
	private DbAccess dbAccess;

	public void setDbAccess(final DbAccess dbAccess) {
		this.dbAccess = dbAccess;
	}

	public DbAccess getDbAccess() {
		return dbAccess;
	}

	public static SpringDataBaseHelper getInstance() {
		return new SpringDataBaseHelper();
	}
}
