package org.nuclos.server.dbtransfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.stubbing.Answer;
import org.nuclos.businessentity.facade.NucletFacade;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.maintenance.MaintenanceConstants;
import org.nuclos.server.maintenance.MaintenanceFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;

public class AutoNucletImportTests {

    @Test
    public void nullPointerSafeInvocations() {
        AutoNucletImport.scanForNuclets(null, null, null, null, null, null, null);
    }

    @Test
    public void verify_check_for_import_was_called() throws NuclosBusinessException {
		ParameterProvider parameterProvider = mock(ParameterProvider.class);
        MaintenanceFacadeLocal maintenanceFacade = mock(MaintenanceFacadeLocal.class);
        TransferFacadeRemote transferFacade = mock(TransferFacadeRemote.class);
        MasterDataFacadeLocal masterDataFacadeLocal = mock(MasterDataFacadeLocal.class);
        when(masterDataFacadeLocal.getMasterData(any(), any())).thenReturn(new ArrayList<>());
		NucletFacade nucletFacade = mock(NucletFacade.class);
		CustomCodeManager customCodeManager = mock(CustomCodeManager.class);

        Transfer transfer = mock(Transfer.class);
        transfer.setParameter(Collections.emptyList());
        when(transfer.getParameter()).thenReturn(Collections.emptyList());
        Transfer.Result result = mock(Transfer.Result.class);
        transfer.result = result;
        when(result.hasCriticals()).thenReturn(false);
		when(result.getScript()).thenReturn(Collections.emptyList());
        when(transferFacade.prepareTransfer(any(byte[].class), anyString(), anyBoolean(), any(ConstraintAction.class))).thenReturn(transfer);
        when(transferFacade.runTransfer(any(Transfer.class), any(ConstraintAction.class))).thenReturn(result);
        when(transferFacade.checkIfFileNeedsToBeTransfered(any(byte[].class), anyString())).thenReturn(true);

        File nuclet = mock(File.class);
        when(nuclet.exists()).thenReturn(true);
        when(nuclet.isFile()).thenReturn(true);
        when(nuclet.getName()).thenReturn("nuclet1.nuclet");
        when(nuclet.getAbsoluteFile()).thenReturn(nuclet);
		Path nucletPath = mock(Path.class);
		Path parent = mock(Path.class);
		when(nuclet.toPath()).thenReturn(nucletPath);
		when(nucletPath.getParent()).thenReturn(parent);

        File nucletImportDir = mock(File.class);
        when(nucletImportDir.exists()).thenReturn(true);
        when(nucletImportDir.isDirectory()).thenReturn(true);
        when(nucletImportDir.listFiles()).thenReturn(new File[] { nuclet });

        try (MockedStatic<Files> mockedFiles = mockStatic(Files.class)) {
            mockedFiles.when(() -> Files.readAllBytes(any())).thenReturn(new byte[0]);
            AutoNucletImport.scanForNuclets(parameterProvider, maintenanceFacade, transferFacade, masterDataFacadeLocal, nucletFacade, customCodeManager, nucletImportDir);
            verify(transferFacade, times(1)).checkIfFileNeedsToBeTransfered(any(byte[].class), anyString());
        }

    }

    // No nuclet files found in the specified path
    @Test
    public void test_no_nuclet_files_found_in_path() {
		ParameterProvider parameterProvider = mock(ParameterProvider.class);
        MaintenanceFacadeLocal maintenanceFacade = mock(MaintenanceFacadeLocal.class);
        TransferFacadeRemote transferFacade = mock(TransferFacadeRemote.class);
        MasterDataFacadeLocal masterDataFacadeLocal = mock(MasterDataFacadeLocal.class);
        when(masterDataFacadeLocal.getMasterData(any(), any())).thenReturn(new ArrayList<>());
		NucletFacade nucletFacade = mock(NucletFacade.class);
		CustomCodeManager customCodeManager = mock(CustomCodeManager.class);

        File nucletImportDir = mock(File.class);
        when(nucletImportDir.exists()).thenReturn(true);
        when(nucletImportDir.isDirectory()).thenReturn(true);
        when(nucletImportDir.listFiles()).thenReturn(null);

        AutoNucletImport autoNucletImport = new AutoNucletImport(parameterProvider, maintenanceFacade, transferFacade, masterDataFacadeLocal, nucletFacade, customCodeManager, nucletImportDir);


        List<File> result = autoNucletImport.resolveNucletFilesInPath();

        assertTrue(result.isEmpty());
    }

	// Successfully enable maintenance mode if not already enabled
    // Successfully exit maintenance mode if it was enabled by the process
    @Test
    public void test_enable_maintenance_mode_if_not_already_enabled_and_exit_maintenance_mode() throws CommonValidationException, NuclosBusinessException {

		try (MockedStatic<Files> mockedFiles = mockStatic(Files.class);
		     MockedStatic<SpringApplicationContextHolder> mockedContextHolder = mockStatic(SpringApplicationContextHolder.class)) {
			ParameterProvider parameterProvider = mock(ParameterProvider.class);
			when(parameterProvider.getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER)).thenReturn("nuclos");
			MaintenanceFacadeLocal maintenanceFacade = mock(MaintenanceFacadeLocal.class);
			final MutableBoolean isMaintenanceOff = new MutableBoolean(true);
			when(maintenanceFacade.isMaintenanceOff()).then((Answer<Boolean>) invocation -> isMaintenanceOff.isTrue());
			when(maintenanceFacade.enterMaintenanceMode(anyString())).then((Answer<String>) invocation -> {
				isMaintenanceOff.setValue(false);
				return MaintenanceConstants.MAINTENANCE_MODE_ON;
			});
			when(maintenanceFacade.exitMaintenanceMode()).then((Answer<String>) invocation -> {
				isMaintenanceOff.setValue(true);
				return MaintenanceConstants.MAINTENANCE_MODE_OFF;
			});
			TransferFacadeRemote transferFacade = mock(TransferFacadeRemote.class);
			MasterDataFacadeLocal masterDataFacadeLocal = mock(MasterDataFacadeLocal.class);
			when(masterDataFacadeLocal.getMasterData(any(), any())).thenReturn(new ArrayList<>());
			NucletFacade nucletFacade = mock(NucletFacade.class);
			when(nucletFacade.areExtensionsUpToDate()).thenReturn(true);
			CustomCodeManager customCodeManager = mock(CustomCodeManager.class);
			when(customCodeManager.hasClassLoader()).thenReturn(true);

			Transfer transfer = mock(Transfer.class);
			transfer.setParameter(Collections.emptyList());
			when(transfer.getParameter()).thenReturn(Collections.emptyList());
			Transfer.Result result = mock(Transfer.Result.class);
			transfer.result = result;
			when(result.hasCriticals()).thenReturn(false);
			when(result.getScript()).thenReturn(Collections.emptyList());
			when(transferFacade.prepareTransfer(any(byte[].class), anyString(), anyBoolean(), any(ConstraintAction.class))).thenReturn(transfer);
			when(transferFacade.runTransfer(any(Transfer.class), any(ConstraintAction.class))).thenReturn(result);
			when(transferFacade.checkIfFileNeedsToBeTransfered(any(byte[].class), anyString())).thenReturn(true);

			File nuclet = mock(File.class);
			when(nuclet.exists()).thenReturn(true);
			when(nuclet.isFile()).thenReturn(true);
			when(nuclet.getName()).thenReturn("nuclet1.nuclet");
			when(nuclet.getAbsoluteFile()).thenReturn(nuclet);
			Path nucletPath = mock(Path.class);
			Path parent = mock(Path.class);
			when(nuclet.toPath()).thenReturn(nucletPath);
			when(nucletPath.getParent()).thenReturn(parent);

			File nucletImportDir = mock(File.class);
			when(nucletImportDir.exists()).thenReturn(true);
			when(nucletImportDir.isDirectory()).thenReturn(true);
			when(nucletImportDir.listFiles()).thenReturn(new File[]{nuclet});

			mockedFiles.when(() -> Files.readAllBytes(any())).thenReturn(new byte[0]);
			mockedContextHolder.when(() -> SpringApplicationContextHolder.addSpringReadyListener(any())).then((Answer<Void>) invocation -> {
				final SpringApplicationContextHolder.SpringReadyListener listener = invocation.getArgument(0);
				listener.springIsReady();
				return null;
			});
			AutoNucletImport.scanForNuclets(parameterProvider, maintenanceFacade, transferFacade, masterDataFacadeLocal, nucletFacade, customCodeManager, nucletImportDir);
			verify(maintenanceFacade).enterMaintenanceMode("nuclos");
			verify(maintenanceFacade).exitMaintenanceMode();
		}
    }

    // Correctly resolve nuclet files in the specified path
    @Test
    public void test_resolve_nuclet_files_in_path() {
        File nucletFile = mock(File.class);
        when(nucletFile.exists()).thenReturn(true);
        when(nucletFile.isFile()).thenReturn(true);
        when(nucletFile.getName()).thenReturn("nuclet1.nuclet");
        File nucletFile2 = mock(File.class);
        when(nucletFile2.exists()).thenReturn(true);
        when(nucletFile2.isFile()).thenReturn(true);
        when(nucletFile2.getName()).thenReturn("nuclet1.zip");
        File nucletFolder = mock(File.class);
        when(nucletFolder.exists()).thenReturn(true);
        when(nucletFolder.isFile()).thenReturn(false);
        when(nucletFolder.getAbsolutePath()).thenReturn("/nuclet2");
        when(nucletFolder.isDirectory()).thenReturn(true);
        when(nucletFolder.getName()).thenReturn("nuclet2");
        File nucletImportDir = mock(File.class);
        when(nucletImportDir.exists()).thenReturn(true);
        when(nucletImportDir.isDirectory()).thenReturn(true);
        when(nucletImportDir.listFiles()).thenReturn(new File[] { nucletFile, nucletFile2, nucletFolder });

        AutoNucletImport autoNucletImport = new AutoNucletImport(null, null, null, null, null, null, nucletImportDir);

        List<File> resolvedFiles = autoNucletImport.resolveNucletFilesInPath();

        assertNotNull(resolvedFiles);
        assertEquals(3, resolvedFiles.size());
        assertEquals(resolvedFiles.get(0), nucletFile);
        assertEquals(resolvedFiles.get(1), nucletFile2);
        assertEquals(resolvedFiles.get(2).getName(), "nuclet2.nuclet");
    }

    // Successfully log the import of each nuclet file
    @Test
    public void test_log_import_of_each_nuclet_file() {
		ParameterProvider parameterProvider = mock(ParameterProvider.class);
        // Mock MaintenanceFacadeLocal
        MaintenanceFacadeLocal maintenanceFacade = mock(MaintenanceFacadeLocal.class);
        when(maintenanceFacade.getMaintenanceMode()).thenReturn(MaintenanceConstants.MAINTENANCE_MODE_OFF);

        MasterDataFacadeLocal masterDataFacadeLocal = mock(MasterDataFacadeLocal.class);
        when(masterDataFacadeLocal.getMasterData(any(), any())).thenReturn(new ArrayList<>());
		NucletFacade nucletFacade = mock(NucletFacade.class);
		CustomCodeManager customCodeManager = mock(CustomCodeManager.class);

        // Mock TransferFacadeRemote
        TransferFacadeRemote transferFacade = mock(TransferFacadeRemote.class);

        File nucletImportDir = mock(File.class);
        when(nucletImportDir.exists()).thenReturn(true);
        when(nucletImportDir.isDirectory()).thenReturn(true);
        when(nucletImportDir.listFiles()).thenReturn(null);

        // Create AutoNucletImport instance
        AutoNucletImport autoNucletImport = new AutoNucletImport(parameterProvider, maintenanceFacade, transferFacade, masterDataFacadeLocal, nucletFacade, customCodeManager, nucletImportDir);

        // Test resolveNucletFilesInPath method
        List<File> nucletFiles = autoNucletImport.resolveNucletFilesInPath();

        // Verify log messages for each nuclet file
        for (File file : nucletFiles) {
            verify(AutoNucletImport.LOG).debug("AutoImport nuclet at {}", file.getAbsolutePath());
        }
    }

    // Maintenance mode is already enabled before starting the process
    @Test
    public void test_enable_maintenance_mode_if_already_enabled() {
		ParameterProvider parameterProvider = mock(ParameterProvider.class);
        MaintenanceFacadeLocal maintenanceFacade = mock(MaintenanceFacadeLocal.class);
        TransferFacadeRemote transferFacade = mock(TransferFacadeRemote.class);
        MasterDataFacadeLocal masterDataFacadeLocal = mock(MasterDataFacadeLocal.class);
        when(masterDataFacadeLocal.getMasterData(any(), any())).thenReturn(new ArrayList<>());
		NucletFacade nucletFacade = mock(NucletFacade.class);
		CustomCodeManager customCodeManager = mock(CustomCodeManager.class);

        when(maintenanceFacade.getMaintenanceMode()).thenReturn(MaintenanceConstants.MAINTENANCE_MODE_ON);

        AutoNucletImport.scanForNuclets(parameterProvider, maintenanceFacade, transferFacade, masterDataFacadeLocal, nucletFacade, customCodeManager, mock(File.class));

        verify(maintenanceFacade, never()).enterMaintenanceMode("nuclos");
    }

    // Transfer result contains critical issues
    @Test
    public void test_transfer_result_critical_issues() throws NuclosBusinessException {
		ParameterProvider parameterProvider = mock(ParameterProvider.class);
        // Mock MaintenanceFacadeLocal
        MaintenanceFacadeLocal maintenanceFacade = mock(MaintenanceFacadeLocal.class);
        when(maintenanceFacade.getMaintenanceMode()).thenReturn(MaintenanceConstants.MAINTENANCE_MODE_OFF);

        MasterDataFacadeLocal masterDataFacadeLocal = mock(MasterDataFacadeLocal.class);
        when(masterDataFacadeLocal.getMasterData(any(), any())).thenReturn(new ArrayList<>());
		NucletFacade nucletFacade = mock(NucletFacade.class);
		CustomCodeManager customCodeManager = mock(CustomCodeManager.class);

        // Mock TransferFacadeRemote
        TransferFacadeRemote transferFacade = mock(TransferFacadeRemote.class);
        Transfer transfer = mock(Transfer.class);
        transfer.setParameter(Collections.emptyList());
        when(transfer.getParameter()).thenReturn(Collections.emptyList());
        Transfer.Result result = mock(Transfer.Result.class);
        transfer.result = result;
        when(result.hasCriticals()).thenReturn(true);
		when(result.getCriticals()).thenReturn("Critical issue detected");
		when(result.getScript()).thenReturn(Collections.emptyList());
        when(transferFacade.prepareTransfer(any(byte[].class), anyString(), anyBoolean(), any(ConstraintAction.class))).thenReturn(transfer);
        when(transferFacade.runTransfer(any(Transfer.class), any(ConstraintAction.class))).thenReturn(result);
        when(transferFacade.checkIfFileNeedsToBeTransfered(any(byte[].class), anyString())).thenReturn(true);

        File nuclet = mock(File.class);
        when(nuclet.exists()).thenReturn(true);
        when(nuclet.isFile()).thenReturn(true);
        when(nuclet.getName()).thenReturn("nuclet1.nuclet");
        when(nuclet.getAbsoluteFile()).thenReturn(nuclet);
		Path nucletPath = mock(Path.class);
		Path parent = mock(Path.class);
		when(nuclet.toPath()).thenReturn(nucletPath);
		when(nucletPath.getParent()).thenReturn(parent);

        File nucletImportDir = mock(File.class);
        when(nucletImportDir.exists()).thenReturn(true);
        when(nucletImportDir.isDirectory()).thenReturn(true);
        when(nucletImportDir.listFiles()).thenReturn(new File[] { nuclet });

        // Perform the test
        try (MockedStatic<Files> mockedFiles = mockStatic(Files.class)) {
            mockedFiles.when(() -> Files.readAllBytes(any())).thenReturn(new byte[0]);
            List<Transfer.Result> results =
                    AutoNucletImport.scanForNuclets(
                            parameterProvider, maintenanceFacade, transferFacade, masterDataFacadeLocal, nucletFacade, customCodeManager, nucletImportDir
                    );
            assertEquals(0, results.size());
        }
    }

    // Exception occurs while preparing or running a transfer
    @Test
    public void test_exception_occurs_while_transfer() throws NuclosBusinessException {
		ParameterProvider parameterProvider = mock(ParameterProvider.class);
        // Mock MaintenanceFacadeLocal
        MaintenanceFacadeLocal maintenanceFacade = mock(MaintenanceFacadeLocal.class);
        when(maintenanceFacade.getMaintenanceMode()).thenReturn(MaintenanceConstants.MAINTENANCE_MODE_OFF);

        MasterDataFacadeLocal masterDataFacadeLocal = mock(MasterDataFacadeLocal.class);
        when(masterDataFacadeLocal.getMasterData(any(), any())).thenReturn(new ArrayList<>());
		NucletFacade nucletFacade = mock(NucletFacade.class);
		CustomCodeManager customCodeManager = mock(CustomCodeManager.class);

        // Mock TransferFacadeRemote
        TransferFacadeRemote transferFacade = mock(TransferFacadeRemote.class);
        when(transferFacade.prepareTransfer(any(byte[].class), anyString(), anyBoolean(), any(ConstraintAction.class)))
                .thenThrow(new NuclosBusinessException());
        when(transferFacade.runTransfer(any(Transfer.class), any(ConstraintAction.class)))
                .thenThrow(new NuclosBusinessException());

        // Create AutoNucletImport instance
        List<Transfer.Result> results =
                AutoNucletImport.scanForNuclets(
                        parameterProvider, maintenanceFacade, transferFacade, masterDataFacadeLocal, nucletFacade, customCodeManager, mock(File.class)
                );
        assertEquals(0, results.size());
    }
}
