package org.nuclos.server.dbtransfer;

import static org.mockito.ArgumentMatchers.any;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.nuclos.businessentity.rule.ConfigurationSaveRule;
import org.nuclos.common.E;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.DbType;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbBuildableStatement;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;

public class SqlConfigurationServiceTests {

    // Executes SQL statements successfully without errors
    @Test
    public void test_execute_sql_success() {
        DbAccess mockDbAccess = Mockito.mock(DbAccess.class);
        SqlConfigurationService service = new SqlConfigurationService(mockDbAccess, "<include>test</include>");
        String sql = "UPDATE table SET column = value";

        Optional<String> result = service.executeSQL(sql);

        Assertions.assertFalse(result.isPresent());
        Mockito.verify(mockDbAccess).executePlainUpdate(sql);
    }

    // Handles null or empty SQL statements gracefully
    @Test
    public void test_execute_sql_with_null_or_empty() {
        DbAccess mockDbAccess = Mockito.mock(DbAccess.class);
        SqlConfigurationService service = new SqlConfigurationService(mockDbAccess, "<include>test</include>");

        Optional<String> resultNull = service.executeSQL(null);
        Optional<String> resultEmpty = service.executeSQL("");

        Assertions.assertTrue(resultNull.isPresent());
        Assertions.assertTrue(resultEmpty.isPresent());
        Mockito.verify(mockDbAccess, Mockito.never()).executePlainUpdate(Mockito.anyString());
    }

    // Correctly matches tags when includes and excludes are specified
    @Test
    public void test_tag_match_with_includes_and_excludes() {
        DbAccess mockDbAccess = Mockito.mock(DbAccess.class);
        Mockito.when(mockDbAccess.getDbType()).thenReturn(DbType.POSTGRESQL);
        String tagFilter = "<include>test</include><exclude>ignore</exclude>";
        SqlConfigurationService service = new SqlConfigurationService(mockDbAccess, tagFilter);

        String configurationTags = "test other";
        boolean result = service.tagMatch(configurationTags);

        Assertions.assertTrue(result);

        configurationTags = "";
        result = service.tagMatch(configurationTags);

        Assertions.assertTrue(result);

        configurationTags = "ignore other";
        result = service.tagMatch(configurationTags);

        Assertions.assertFalse(result);

        configurationTags = "POSTGRESQL other";
        result = service.tagMatch(configurationTags);

        Assertions.assertTrue(result);

        configurationTags = "test ignore other";
        result = service.tagMatch(configurationTags);

        Assertions.assertFalse(result);

        configurationTags = "nomatch";
        result = service.tagMatch(configurationTags);

        Assertions.assertFalse(result);
    }

    // Deals with malformed tag content in the input
    @Test
    public void test_collect_tag_content_with_malformed_input() {
        SqlConfigurationService service = new SqlConfigurationService(null, "");
        String malformedInput = "<include>valid</include><include>malformed<include>include>qwe</include>includetestinclude";

        List<String> result = service.collectTagContent(malformedInput, "include");

        Assertions.assertEquals(Arrays.asList("valid", "malformed<include>include>qwe"), result);
    }

    // Collects tag content accurately from given input
    @Test
    public void test_collect_tag_content() {
        String input = "<include>tag1</include><include>tag2</include><exclude>tag3</exclude>";
        SqlConfigurationService service = new SqlConfigurationService(null, input);

        List<String> expectedIncludes = Arrays.asList("tag1", "tag2");
        List<String> expectedExcludes = Collections.singletonList("tag3");

        Assertions.assertEquals(expectedIncludes, service.collectTagContent(input, "include"));
        Assertions.assertEquals(expectedExcludes, service.collectTagContent(input, "exclude"));
        Assertions.assertEquals(0, service.collectTagContent(null, "include").size());
    }

    // Runs SQL configurations in the correct order based on RunType
    @Test
    public void test_run_sql_configurations_in_correct_order() {
        DbAccess mockDbAccess = Mockito.mock(DbAccess.class);
        DbQueryBuilder mockQueryBuilder = Mockito.mock(DbQueryBuilder.class);
        DbQuery<DbTuple> mockQuery = Mockito.mock(DbQuery.class);
        DbTuple mockDbTuple = Mockito.mock(DbTuple.class);
        DbTuple mockDbTuple2 = Mockito.mock(DbTuple.class);
        DbFrom mockDbFrom = Mockito.mock(DbFrom.class);
        DbJoin mockDbJoin = Mockito.mock(DbJoin.class);

        Mockito.when(mockDbFrom.joinOnJoinedPk(any(), any(), any(), any())).thenReturn(mockDbJoin);
        Mockito.when(mockQuery.from(any())).thenReturn(mockDbFrom);
        Mockito.when(mockQuery.where(any())).thenReturn(mockQuery);
        Mockito.when(mockQuery.multiselect(any(), any(), any(), any(), any(), any(), any())).thenReturn(mockQuery);
        Mockito.when(mockDbAccess.getQueryBuilder()).thenReturn(mockQueryBuilder);
        Mockito.when(mockQueryBuilder.createTupleQuery()).thenReturn(mockQuery);
        Mockito.when(mockDbAccess.executeQuery(mockQuery)).thenReturn(
                Arrays.asList(mockDbTuple2, mockDbTuple)
        );

        Mockito.when(mockDbTuple2.get(E.NUCLETSQLCONFIGURAITON.sql)).thenReturn("SELECT * FROM table2;");
        Mockito.when(mockDbTuple2.get(E.NUCLETSQLCONFIGURAITON.order)).thenReturn(2);
        Mockito.when(mockDbTuple2.get(E.NUCLETSQLCONFIGURAITON.tags)).thenReturn("test");
        Mockito.when(mockDbTuple2.get(E.NUCLETSQLCONFIGURAITON.nucletVersion)).thenReturn(1);
        Mockito.when(mockDbTuple2.get(E.NUCLET.nucletVersion)).thenReturn(1);
        Mockito.when(mockDbTuple2.get(E.NUCLETSQLCONFIGURAITON.lastRunCkSum)).thenReturn("checksum");

        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.sql)).thenReturn("SELECT * FROM table;");
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.order)).thenReturn(1);
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.tags)).thenReturn("test");
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.nucletVersion)).thenReturn(1);
        Mockito.when(mockDbTuple.get(E.NUCLET.nucletVersion)).thenReturn(1);
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.lastRunCkSum)).thenReturn("checksum");

        SqlConfigurationService.runSqlConfigurations(mockDbAccess, "<include>test</include>", SqlConfigurationService.RunType.BEFORE_SCHEME);

        InOrder inOrder = Mockito.inOrder(mockDbAccess);
        inOrder.verify(mockDbAccess).getDbType();
        inOrder.verify(mockDbAccess).getQueryBuilder();
        inOrder.verify(mockDbAccess).executeQuery(any());
        inOrder.verify(mockDbAccess).executePlainUpdate("SELECT * FROM table;");
        inOrder.verify(mockDbAccess).execute((DbBuildableStatement) any());
        inOrder.verify(mockDbAccess).executePlainUpdate("SELECT * FROM table2;");
        inOrder.verify(mockDbAccess).execute((DbBuildableStatement) any());
    }

    // Correctly handles SQL execution failures and logs appropriate messages
    @Test
    public void test_execute_sql_failure() {
        DbAccess mockDbAccess = Mockito.mock(DbAccess.class);
        SqlConfigurationService service = new SqlConfigurationService(mockDbAccess, "<include>test</include>");
        String sql = "UPDATE table SET column = value";
        DbException dbException = new DbException(null, "SQL error", new SQLException("Syntax error", "42000"));

        Mockito.doThrow(dbException).when(mockDbAccess).executePlainUpdate(sql);

        Optional<String> result = service.executeSQL(sql);

        Assertions.assertTrue(result.isPresent());
        Assertions.assertEquals("Syntax error", result.get());
        Mockito.verify(mockDbAccess).executePlainUpdate(sql);
    }

    // Correctly filters out unwanted configurations based on conditions
    @Test
    public void test_skip_unwanted_configurations() {
        DbAccess mockDbAccess = Mockito.mock(DbAccess.class);
        SqlConfigurationService service = new SqlConfigurationService(mockDbAccess, "<include>test</include>");

        DbTuple mockDbTuple = Mockito.mock(DbTuple.class);
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.nucletVersion)).thenReturn(1);
        Mockito.when(mockDbTuple.get(E.NUCLET.nucletVersion)).thenReturn(2);
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.sql)).thenReturn("");
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.lastRunCkSum)).thenReturn("checksum2");
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.tags)).thenReturn("test");

        DbTuple mockDbTuple2 = Mockito.mock(DbTuple.class);
        Mockito.when(mockDbTuple2.get(E.NUCLETSQLCONFIGURAITON.nucletVersion)).thenReturn(3);
        Mockito.when(mockDbTuple2.get(E.NUCLET.nucletVersion)).thenReturn(2);
        Mockito.when(mockDbTuple2.get(E.NUCLETSQLCONFIGURAITON.sql)).thenReturn("");
        Mockito.when(mockDbTuple2.get(E.NUCLETSQLCONFIGURAITON.lastRunCkSum)).thenReturn("checksum2");
        Mockito.when(mockDbTuple2.get(E.NUCLETSQLCONFIGURAITON.tags)).thenReturn("test");

        DbTuple mockDbTuple3 = Mockito.mock(DbTuple.class);
        Mockito.when(mockDbTuple3.get(E.NUCLETSQLCONFIGURAITON.nucletVersion)).thenReturn(2);
        Mockito.when(mockDbTuple3.get(E.NUCLET.nucletVersion)).thenReturn(2);
        Mockito.when(mockDbTuple3.get(E.NUCLETSQLCONFIGURAITON.sql)).thenReturn("");
        Mockito.when(mockDbTuple3.get(E.NUCLETSQLCONFIGURAITON.lastRunCkSum)).thenReturn(ConfigurationSaveRule.calculateChecksum(""));
        Mockito.when(mockDbTuple3.get(E.NUCLETSQLCONFIGURAITON.tags)).thenReturn("test");

        Predicate<DbTuple> predicate = SqlConfigurationService.skipUnwantedConfigurations(service);
        boolean result = predicate.test(mockDbTuple);

        Assertions.assertTrue(result);

        predicate = SqlConfigurationService.skipUnwantedConfigurations(service);
        result = predicate.test(mockDbTuple2);

        Assertions.assertFalse(result);

        predicate = SqlConfigurationService.skipUnwantedConfigurations(service);
        result = predicate.test(mockDbTuple3);

        Assertions.assertFalse(result);
    }

    // Handles multiple SQL statements in a single configuration
    @Test
    public void test_run_sql_configurations_with_multiple_statements() {
        DbAccess mockDbAccess = Mockito.mock(DbAccess.class);
        DbQueryBuilder mockQueryBuilder = Mockito.mock(DbQueryBuilder.class);
        DbQuery<DbTuple> mockQuery = Mockito.mock(DbQuery.class);
        DbFrom mockDbFrom = Mockito.mock(DbFrom.class);
        DbJoin mockDbJoin = Mockito.mock(DbJoin.class);

        Mockito.when(mockDbFrom.joinOnJoinedPk(any(), any(), any(), any())).thenReturn(mockDbJoin);
        Mockito.when(mockQuery.from(any())).thenReturn(mockDbFrom);
        Mockito.when(mockQuery.where(any())).thenReturn(mockQuery);
        Mockito.when(mockQuery.multiselect(any(), any(), any(), any(), any(), any(), any())).thenReturn(mockQuery);
        Mockito.when(mockDbAccess.getQueryBuilder()).thenReturn(mockQueryBuilder);
        Mockito.when(mockQueryBuilder.createTupleQuery()).thenReturn(mockQuery);

        DbTuple mockDbTuple = Mockito.mock(DbTuple.class);
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.sql)).thenReturn("INSERT INTO table1 VALUES (1); INSERT INTO table2 VALUES (2)");
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.order)).thenReturn(1);
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.lastRunCkSum)).thenReturn("checksum2");
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.tags)).thenReturn("includeTag");
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.nucletVersion)).thenReturn(1);
        Mockito.when(mockDbTuple.get(E.NUCLET.nucletVersion)).thenReturn(1);

        List<DbTuple> mockDbTuples = Arrays.asList(mockDbTuple);

        Mockito.doReturn(mockDbTuples).when(mockDbAccess).executeQuery(any());

        SqlConfigurationService.runSqlConfigurations(mockDbAccess, "<include>includeTag</include>", SqlConfigurationService.RunType.AFTER_SCHEME);

        Mockito.verify(mockDbAccess, Mockito.times(1)).executePlainUpdate(Mockito.anyString());
    }

    // Validates checksum calculation for SQL configurations
    @Test
    public void test_calculate_checksum_for_sql_configurations() {
        DbAccess mockDbAccess = Mockito.mock(DbAccess.class);
        DbQueryBuilder mockQueryBuilder = Mockito.mock(DbQueryBuilder.class);
        DbQuery<DbTuple> mockQuery = Mockito.mock(DbQuery.class);
        DbFrom mockDbFrom = Mockito.mock(DbFrom.class);
        DbJoin mockDbJoin = Mockito.mock(DbJoin.class);

        Mockito.when(mockDbFrom.joinOnJoinedPk(any(), any(), any(), any())).thenReturn(mockDbJoin);
        Mockito.when(mockQuery.from(any())).thenReturn(mockDbFrom);
        Mockito.when(mockQuery.where(any())).thenReturn(mockQuery);
        Mockito.when(mockQuery.multiselect(any(), any(), any(), any(), any(), any(), any())).thenReturn(mockQuery);
        Mockito.when(mockDbAccess.getQueryBuilder()).thenReturn(mockQueryBuilder);
        Mockito.when(mockQueryBuilder.createTupleQuery()).thenReturn(mockQuery);

        String sql = "SELECT * FROM table";
        String expectedChecksum = ConfigurationSaveRule.calculateChecksum(sql);

        DbTuple mockDbTuple = Mockito.mock(DbTuple.class);
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.order)).thenReturn(1);
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.sql)).thenReturn(sql);
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.tags)).thenReturn("test");
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.nucletVersion)).thenReturn(1);
        Mockito.when(mockDbTuple.get(E.NUCLETSQLCONFIGURAITON.lastRunCkSum)).thenReturn("checksum2");
        Mockito.when(mockDbTuple.get(E.NUCLET.nucletVersion)).thenReturn(1);
        Mockito.when(mockDbAccess.executeQuery(any())).thenReturn(Arrays.asList(mockDbTuple));

        SqlConfigurationService.runSqlConfigurations(mockDbAccess, "<include>test</include>", SqlConfigurationService.RunType.BEFORE_SCHEME);

        Mockito.verify(mockDbAccess).executePlainUpdate(sql);
        Mockito.verify(mockDbAccess).execute(
                (DbBuildableStatement) Mockito.argThat(statement ->
                        ((DbUpdateStatement) statement)
                                .getColumnValues()
                                .get(E.NUCLETSQLCONFIGURAITON.lastRunCkSum)
                                .equals(expectedChecksum)
                )
        );
    }
}
