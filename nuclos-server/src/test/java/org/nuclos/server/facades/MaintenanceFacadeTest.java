package org.nuclos.server.facades;

import java.util.Collections;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerProperties;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.maintenance.MaintenanceFacadeBean;
import org.quartz.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class MaintenanceFacadeTest {

	private static final Logger LOG = LoggerFactory.getLogger(MaintenanceFacadeTest.class);

	static MockedStatic<ServerProperties> serverPropertiesMockedStatic;
	static MockedStatic<SpringApplicationContextHolder> mockContextHolder;
	static MockedStatic<SecurityCache> mockSecurityCache;
	static MockedStatic<SpringDataBaseHelper> mockSpringDataBaseHelper;
	MaintenanceFacadeBean maintenanceFacadeBean = null;
	Authentication nuclosAuthMock;

	@BeforeClass
	public static void oneTimeSetup() {
		SecurityCache securityCache = Mockito.mock(SecurityCache.class);
		SpringDataBaseHelper springDataBaseHelper = Mockito.mock(SpringDataBaseHelper.class);
		Scheduler scheduler = Mockito.mock(Scheduler.class);
		DbAccess dbAccess = Mockito.mock(DbAccess.class);

		Mockito.when(securityCache.getAllowedActions("nuclos", null))
				.thenReturn(Collections.singleton("Maintenance Mode"));

		Mockito.when(securityCache.isSuperUser("nuclos"))
				.thenReturn(true);

		Mockito.when(springDataBaseHelper.getDbAccess())
				.thenReturn(dbAccess);

		serverPropertiesMockedStatic = Mockito.mockStatic(ServerProperties.class);

		serverPropertiesMockedStatic
				.when(() -> ServerProperties.loadProperties("java:comp/env/nuclos-conf-server"))
				.thenReturn(new Properties());

		serverPropertiesMockedStatic
				.when(() -> ServerProperties.loadProperties(new Properties(), "java:comp/env/nuclos-conf-server", true))
				.thenReturn(new Properties());

		mockContextHolder = Mockito.mockStatic(SpringApplicationContextHolder.class);
		mockContextHolder
				.when(() -> SpringApplicationContextHolder.getBean("nuclosScheduler"))
				.thenReturn(scheduler);

		mockSecurityCache = Mockito.mockStatic(SecurityCache.class);
		mockSecurityCache
				.when(SecurityCache::getInstance)
				.thenReturn(securityCache);

		mockSpringDataBaseHelper = Mockito.mockStatic(SpringDataBaseHelper.class);
		mockSpringDataBaseHelper
				.when(SpringDataBaseHelper::getInstance)
				.thenReturn(springDataBaseHelper);
	}

	@AfterClass
	public static void deregisterMock() {
		serverPropertiesMockedStatic.close();
		mockContextHolder.close();
		mockSecurityCache.close();
		mockSpringDataBaseHelper.close();
	}

	@Before
	public void setup() {
		maintenanceFacadeBean = Mockito.spy(new MaintenanceFacadeBean(null, SecurityCache.getInstance()));
		assert maintenanceFacadeBean != null : "MaintenanceFacadeBean should me mocked";
		Mockito.doReturn(5L).when(maintenanceFacadeBean).getNumberOfRecentActiveUsers("nuclos");
		Mockito.doReturn(2).when(maintenanceFacadeBean).getNumberOfRunningJobs();
		Mockito.doReturn(true).when(maintenanceFacadeBean).isDevelopmentEnvironment();

		nuclosAuthMock = Mockito.mock(Authentication.class);
		Mockito.when(nuclosAuthMock.getPrincipal()).thenReturn("nuclos");
		SecurityContextHolder.getContext().setAuthentication(nuclosAuthMock);
	}

	@Test
	public void maintenanceFacadeWorks() {
		assert maintenanceFacadeBean.isMaintenanceOff();
		assert maintenanceFacadeBean.getNumberOfRecentActiveUsers("nuclos") == 5L;
		assert maintenanceFacadeBean.getNumberOfRunningJobs() == 2;
		assert !maintenanceFacadeBean.blockUserLogin("nuclos");
	}

	@Test
	public void enterMaintenanceModeWorks() {
		long timeTestStart = System.currentTimeMillis();
		new Thread(() -> {
			try {
				Thread.sleep(10000);
				Mockito.doReturn(0L).when(maintenanceFacadeBean).getNumberOfRecentActiveUsers("nuclos");
				Mockito.doReturn(0).when(maintenanceFacadeBean).getNumberOfRunningJobs();
			} catch (InterruptedException e) {
				LOG.error(e.getMessage());
			}
		}).start();
		maintenanceFacadeBean.enterMaintenanceMode("nuclos");
		assert System.currentTimeMillis() - timeTestStart >= 10000L;
		assert !maintenanceFacadeBean.isMaintenanceOff();
		assert !maintenanceFacadeBean.blockUserLogin("nuclos");
		assert maintenanceFacadeBean.blockUserLogin("test");
	}

	@Test
	public void exitingMaintenanceModeWorks() throws Exception {
		enterMaintenanceModeWorks();
		maintenanceFacadeBean.exitMaintenanceMode();
		assert maintenanceFacadeBean.isMaintenanceOff();
		assert !maintenanceFacadeBean.blockUserLogin("test");
	}

	@Test
	public void forcingMaintenanceModeWorks() {
		long timeTestStart = System.currentTimeMillis();
		new Thread(() -> {
			try {
				Thread.sleep(5000);
				maintenanceFacadeBean.forceMaintenanceMode();
			} catch (InterruptedException e) {
				LOG.error(e.getMessage());
			}
		}).start();
		maintenanceFacadeBean.enterMaintenanceMode("nuclos");
		assert System.currentTimeMillis() - timeTestStart >= 5000L;
		assert !maintenanceFacadeBean.isMaintenanceOff();
		assert !maintenanceFacadeBean.blockUserLogin("nuclos");
		assert maintenanceFacadeBean.blockUserLogin("test");
	}
}
