package org.nuclos.server.report;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common.report.JRPatchParameters;
import org.nuclos.common.report.JRUtils;
import org.nuclos.common.report.JRVersion;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ReportFieldDefinition;
import org.nuclos.common.report.ReportFieldDefinitionFactory;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerProperties;
import org.nuclos.server.report.ejb3.ReportCompiler;
import org.nuclos.server.report.export.JasperExport;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.google.common.io.ByteStreams;

public class ReportTest {

	static byte [] searchResultTemplateBytes;
	static String searchResultTemplateXML;
	static byte [] oldSearchResultTemplateBytes;
	static String oldSearchResultTemplateXML;
	static String testReportOutdatedVersionXML;
	static MockedStatic<ServerProperties> serverPropertiesMockedStatic;
	static MockedStatic<NuclosSystemParameters> nuclosSystemParametersMockedStatic;

	@BeforeClass
	public static void oneTimeSetup() throws IOException {
		Properties properties = new Properties();
		serverPropertiesMockedStatic = Mockito.mockStatic(ServerProperties.class);
		serverPropertiesMockedStatic
				.when(() -> ServerProperties.loadProperties("java:comp/env/nuclos-conf-server"))
				.thenReturn(properties);
		serverPropertiesMockedStatic
				.when(() ->
					ServerProperties.loadProperties(new Properties(), "java:comp/env/nuclos-conf-server", true))
				.thenReturn(properties);

		nuclosSystemParametersMockedStatic = Mockito.mockStatic(NuclosSystemParameters.class);
		nuclosSystemParametersMockedStatic
				.when(() ->
					NuclosSystemParameters.getString(NuclosSystemParameters.JASPER_REPORTS_COMPILE_TMP))
				.thenReturn(ReportTest.class.getResource("").getPath() + "reportCompileDir" + File.separator);
		nuclosSystemParametersMockedStatic
				.when(() ->
					NuclosSystemParameters.getDirectory(NuclosSystemParameters.JASPER_REPORTS_COMPILE_TMP))
				.thenReturn(new File(ReportTest.class.getResource("").getPath() + "reportCompileDir" + File.separator));

		SpringLocaleDelegate localeMock = Mockito.mock(SpringLocaleDelegate.class);
		Mockito.when(localeMock.getMessage("ReportFormatPanel.9", "Suchergebnis"))
				.thenReturn("Suchergebnis:");

		Mockito.mockStatic(SpringLocaleDelegate.class)
				.when(() ->
					SpringLocaleDelegate.getInstance())
			.thenReturn(localeMock);

		properties.put(NuclosSystemParameters.JASPER_REPORTS_COMPILE_TMP, ReportTest.class.getResource("").getPath());

		Resource resource = new ClassPathResource("resources/reports/JR_SearchResultTemplate.jrxml");
		searchResultTemplateBytes = ByteStreams.toByteArray(resource.getInputStream());
		searchResultTemplateXML = new String(searchResultTemplateBytes, "UTF-8");

		Resource oldTemplate = new ClassPathResource("resources/reports/JR_SearchResultTemplate.xml");
		oldSearchResultTemplateBytes = ByteStreams.toByteArray(oldTemplate.getInputStream());
		oldSearchResultTemplateXML = new String(oldSearchResultTemplateBytes, "UTF-8");

		Resource outdatedVersionReport = new ClassPathResource("resources/reports/JR_TestReportVersionOutdated.jrxml");
		byte [] outdatedVersionBytes = ByteStreams.toByteArray(outdatedVersionReport.getInputStream());
		testReportOutdatedVersionXML = new String(outdatedVersionBytes, "UTF-8");
	}

	@AfterClass
	public static void deregisterMock() {
		serverPropertiesMockedStatic.close();
		nuclosSystemParametersMockedStatic.close();
	}

	@Test
	public void _01_testCompileSearchResultTemplate() throws NuclosReportException {
		ByteArrayCarrier byteContent = new ByteArrayCarrier(searchResultTemplateBytes);
		ReportCompiler.compileReport(byteContent);
		JasperExport export = new JasperExport();
		ResultVO resultVO = new ResultVO();

		ResultColumnVO column1 = new ResultColumnVO();
		column1.setColumnLabel("Column 1");
		column1.setColumnClassName("java.lang.Object");
		column1.setColumnWidth(100);

		ResultColumnVO column2 = new ResultColumnVO();
		column2.setColumnLabel("Column 2");
		column2.setColumnClassName("java.lang.Object");
		column2.setColumnWidth(100);

		resultVO.addColumn(column1);
		resultVO.addColumn(column2);

		resultVO.addRow(new String[]{"1. ", "Zeile"});
		resultVO.addRow(new String[]{"2. ", "Line"});

		List<ReportFieldDefinition> fields = ReportFieldDefinitionFactory.getFieldDefinitions(resultVO);

		export.export(null, resultVO, fields);
	}

	@Test
	public void _02_testExportSearchResultTemplate() throws NuclosReportException {
		JasperExport export = new JasperExport();
		ResultVO resultVO = new ResultVO();

		ResultColumnVO column1 = new ResultColumnVO();
		column1.setColumnLabel("Column 1");
		column1.setColumnClassName("java.lang.Object");
		column1.setColumnWidth(100);

		ResultColumnVO column2 = new ResultColumnVO();
		column2.setColumnLabel("Column 2");
		column2.setColumnClassName("java.lang.Object");
		column2.setColumnWidth(100);

		resultVO.addColumn(column1);
		resultVO.addColumn(column2);

		resultVO.addRow(new String[]{"1. ", "Zeile"});
		resultVO.addRow(new String[]{"2. ", "Line"});

		List<ReportFieldDefinition> fields = ReportFieldDefinitionFactory.getFieldDefinitions(resultVO);

		export.export(null, resultVO, fields);
	}

	@Test
	public void _03_testPatchOldReport() {
		JRPatchParameters patchParameters = new JRPatchParameters();
		patchParameters.patch = true;
		patchParameters.replaceDeprecatedPdfFonts = true;

		String patchedXML = JRUtils.patchXML(oldSearchResultTemplateXML, patchParameters);

		assert (patchedXML.contains(JRUtils.NUCLOS_JASPER_VERSION.toString()));
		Assert.assertFalse(JRUtils.isPatchRequired(patchedXML));
	}

	/**
	 * Check if the Version gets parsed correctly by the JRUtils class.
	 */
	@Test
	public void _04_testReportVersionDetection() {
		String outdatedVersion             = JRUtils.getReportVersion(testReportOutdatedVersionXML);

		assert (outdatedVersion.equals("3.5.3"));
		//For old reports patching should not be required
		assert (JRUtils.isPatchRequired(testReportOutdatedVersionXML));
	}

	@Test
	public void _05_testVersionComparison() {
		JRVersion oldest = new JRVersion("3.5.3", null);
		JRVersion old    = new JRVersion("4.5.0", null);
		JRVersion newVersion = new JRVersion("6.17.0", null);
		JRVersion latest = new JRVersion("6.17.0", "6d93193241dd8cc42629e188b94f9e0bc5722efd");

		assert (oldest.compareProjectVersions(old) < 0);
		assert (old.compareProjectVersions(newVersion) < 0);
		assert(old.compareProjectVersions(latest) < 0);

		assert (oldest.compareTo(old) < 0);
		assert (old.compareTo(newVersion) < 0);
	}

}
