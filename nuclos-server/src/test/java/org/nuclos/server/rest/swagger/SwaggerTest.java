package org.nuclos.server.rest.swagger;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.PathItem;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.nuclos.server.rest.services.CustomRestComponent;

import javax.ws.rs.*;
import javax.xml.ws.Response;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

@Path("swaggertest")
public class SwaggerTest {
    @Test
    public void testAddOperationsManually() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(this.getClass());

        NuclosRestReader reader = new NuclosRestReader();
        OpenAPI api = reader.read(classes);

        Set<CustomRestComponent.EndpointInfo> customRestRules = new HashSet<>();
        customRestRules.add(createEndpointInfo("get1", GET.class));
        customRestRules.add(createEndpointInfo("put1", PUT.class));
        customRestRules.add(createEndpointInfo("post1", POST.class));
        customRestRules.add(createEndpointInfo("delete1", DELETE.class));

        reader.addOperationsManually(api, customRestRules);

        PathItem item = api.getPaths().get("/rest/custom/swaggertest/all1");
        Assert.assertNotNull(item);
        Assert.assertNotNull(item.getGet());
        Assert.assertNotNull(item.getPut());
        Assert.assertNotNull(item.getPost());
        Assert.assertNotNull(item.getDelete());

    }

    private CustomRestComponent.EndpointInfo createEndpointInfo(String methodName, Class methodType) throws NoSuchMethodException {
        CustomRestComponent.EndpointInfo info = Mockito.mock(CustomRestComponent.EndpointInfo.class);
        Mockito.doReturn(SwaggerTest.class).when(info).getCustomRestClass();
        Mockito.doReturn(SwaggerTest.class.getMethod(methodName)).when(info).getCustomRestMethod();
        Mockito.doReturn(methodType).when(info).getMethodType();
        return info;
    }

    @GET
    @Path("all1")
    public Response get1() {
        return null;
    }

    @PUT
    @Path("all1")
    public Response put1() {
        return null;
    }

    @DELETE
    @Path("all1")
    public Response delete1() {
        return null;
    }

    @POST
    @Path("all1")
    public Response post1() {
        return null;
    }

}
