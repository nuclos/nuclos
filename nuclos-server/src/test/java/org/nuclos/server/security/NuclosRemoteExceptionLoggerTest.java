//Copyright (C) 2020  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.util.Collections;

import org.junit.Test;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.Mutable;
import org.nuclos.common2.exception.InternalErrorException;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.genericobject.ejb3.GenerationResult;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.Marker;

public class NuclosRemoteExceptionLoggerTest {

	private static final BusinessException bexSolo = new BusinessException("a test BusinessException");
	private static final BusinessException bexWithBex = new BusinessException(bexSolo);
	private static final RuntimeException rexSolo = new RuntimeException("a test RuntimeException");
	private static final RuntimeException rexWithBex = new RuntimeException(bexWithBex);
	private static final RuntimeException rexWithRex = new RuntimeException(rexSolo);
	private static final RuntimeException outOfBoundsEx = new ArrayIndexOutOfBoundsException("007");
	private static final NuclosBusinessRuleException ruleBex = new NuclosBusinessRuleException(bexSolo);
	private static final GeneratorFailedException genBex = new GeneratorFailedException("a test GeneratorFailedException",
			new GenerationResult(Collections.emptyList(), null, "a Gen-Error"),
			bexSolo);
	private static final String interr = "Internal-Error-Message";

	@Test
	public void testClear() {
		assert bexWithBex.getCause() != null;
		assert bexWithBex.getStackTrace().length > 0;
		NuclosRemoteExceptionLogger.clear(bexWithBex);
		assert bexWithBex.getCause() == null;
		assert bexWithBex.getStackTrace().length == 0;
		assert NuclosRemoteExceptionLogger.isCleared(bexWithBex);
	}

	@Test
	public void testSoloBusinessException() {
		Throwable result = NuclosRemoteExceptionLogger.logException(bexWithBex, null, false, false, interr);
		assert NuclosRemoteExceptionLogger.isCleared(result);
		assert result == bexWithBex;
		assert result.getMessage().equals(bexSolo.toString());
	}

	@Test
	public void testBusinessExceptionWrappedWithRuntime() {
		Throwable result = NuclosRemoteExceptionLogger.logException(rexWithBex, null, false, false, interr);
		assert NuclosRemoteExceptionLogger.isCleared(result);
		assert result == bexWithBex;
		assert result.getMessage().equals(bexSolo.toString());
	}

	@Test
	public void testRuleBusinessException() {
		Throwable result = NuclosRemoteExceptionLogger.logException(ruleBex, null, false, false, interr);
		assert NuclosRemoteExceptionLogger.isCleared(result);
		assert result == bexSolo;
		assert result.getMessage().equals(bexSolo.getMessage());

		result = NuclosRemoteExceptionLogger.logException(ruleBex, null, true, false, interr);
		assert NuclosRemoteExceptionLogger.isCleared(result);
		assert result == ruleBex;
		assert result.getMessage().equals(bexSolo.getMessage());
	}

	@Test
	public void testGeneratorFailedException() {
		Throwable result = NuclosRemoteExceptionLogger.logException(genBex, null, false, false, interr);
		assert NuclosRemoteExceptionLogger.isCleared(result);
		assert result == bexSolo;
		assert result.getMessage().equals(bexSolo.getMessage());

		result = NuclosRemoteExceptionLogger.logException(genBex, null, false, true, interr);
		// Not cleared is necessary here for the Java Client
		assert !NuclosRemoteExceptionLogger.isCleared(result);
		assert NuclosRemoteExceptionLogger.isCleared(result.getCause());
		assert result == genBex;
		assert result.getCause() == bexSolo;
		assert result.getMessage().equals(genBex.getMessage());
		assert result.getCause().getMessage().equals(bexSolo.getMessage());
	}

	@Test
	public void testRuntimeException() {
		Throwable result = NuclosRemoteExceptionLogger.logException(rexSolo, null, false, false, interr);
		assert NuclosRemoteExceptionLogger.isCleared(result);
		assert result != rexSolo;
		assert result instanceof InternalErrorException;
		assert result.getMessage().equals(String.format("%s (%s)",interr, ((InternalErrorException) result).getIdentifier()));
	}

	@Test
	public void testRuntimeExceptionWrappedWithRuntime() {
		Throwable result = NuclosRemoteExceptionLogger.logException(rexWithRex, null, false, false, interr);
		assert NuclosRemoteExceptionLogger.isCleared(result);
		assert result != rexWithRex;
		assert result instanceof InternalErrorException;
		assert result.getMessage().equals(String.format("%s (%s)",interr, ((InternalErrorException) result).getIdentifier()));
	}

	@Test
	public void testWithServerLogger() {
		Mutable<String> serverInfo = new Mutable<>();
		NuclosRemoteExceptionLogger.logException(bexSolo, new TestLogger() {
			@Override
			public void info(final String format, final Object arg) {
				serverInfo.setValue(arg.toString());
			}
		}, false, false, interr);
		assert bexSolo.toString().equals(serverInfo.getValue());

		Mutable<String> serverWarn1 = new Mutable<>();
		Mutable<String> serverWarn2 = new Mutable<>();
		Throwable iee = NuclosRemoteExceptionLogger.logException(outOfBoundsEx, new TestLogger() {
			@Override
			public void warn(final String format, final Object arg1, final Object arg2) {
				serverWarn1.setValue(arg1.toString());
				serverWarn2.setValue(arg2.toString());
			}
		}, false, false, interr);
		assert iee.toString().equals(serverWarn1.getValue());
		assert outOfBoundsEx.toString().equals(serverWarn2.getValue());
	}

	private static abstract class TestLogger implements Logger {
		@Override
		public String getName() {
			return null;
		}

		@Override
		public boolean isTraceEnabled() {
			return false;
		}

		@Override
		public void trace(final String msg) {

		}

		@Override
		public void trace(final String format, final Object arg) {

		}

		@Override
		public void trace(final String format, final Object arg1, final Object arg2) {

		}

		@Override
		public void trace(final String format, final Object... arguments) {

		}

		@Override
		public void trace(final String msg, final Throwable t) {

		}

		@Override
		public boolean isTraceEnabled(final Marker marker) {
			return false;
		}

		@Override
		public void trace(final Marker marker, final String msg) {

		}

		@Override
		public void trace(final Marker marker, final String format, final Object arg) {

		}

		@Override
		public void trace(final Marker marker, final String format, final Object arg1, final Object arg2) {

		}

		@Override
		public void trace(final Marker marker, final String format, final Object... argArray) {

		}

		@Override
		public void trace(final Marker marker, final String msg, final Throwable t) {

		}

		@Override
		public boolean isDebugEnabled() {
			return false;
		}

		@Override
		public void debug(final String msg) {

		}

		@Override
		public void debug(final String format, final Object arg) {

		}

		@Override
		public void debug(final String format, final Object arg1, final Object arg2) {

		}

		@Override
		public void debug(final String format, final Object... arguments) {

		}

		@Override
		public void debug(final String msg, final Throwable t) {

		}

		@Override
		public boolean isDebugEnabled(final Marker marker) {
			return false;
		}

		@Override
		public void debug(final Marker marker, final String msg) {

		}

		@Override
		public void debug(final Marker marker, final String format, final Object arg) {

		}

		@Override
		public void debug(final Marker marker, final String format, final Object arg1, final Object arg2) {

		}

		@Override
		public void debug(final Marker marker, final String format, final Object... arguments) {

		}

		@Override
		public void debug(final Marker marker, final String msg, final Throwable t) {

		}

		@Override
		public boolean isInfoEnabled() {
			return false;
		}

		@Override
		public void info(final String msg) {

		}

		@Override
		public void info(final String format, final Object arg) {

		}

		@Override
		public void info(final String format, final Object arg1, final Object arg2) {

		}

		@Override
		public void info(final String format, final Object... arguments) {

		}

		@Override
		public void info(final String msg, final Throwable t) {

		}

		@Override
		public boolean isInfoEnabled(final Marker marker) {
			return false;
		}

		@Override
		public void info(final Marker marker, final String msg) {

		}

		@Override
		public void info(final Marker marker, final String format, final Object arg) {

		}

		@Override
		public void info(final Marker marker, final String format, final Object arg1, final Object arg2) {

		}

		@Override
		public void info(final Marker marker, final String format, final Object... arguments) {

		}

		@Override
		public void info(final Marker marker, final String msg, final Throwable t) {

		}

		@Override
		public boolean isWarnEnabled() {
			return false;
		}

		@Override
		public void warn(final String msg) {

		}

		@Override
		public void warn(final String format, final Object arg) {

		}

		@Override
		public void warn(final String format, final Object... arguments) {

		}

		@Override
		public void warn(final String format, final Object arg1, final Object arg2) {

		}

		@Override
		public void warn(final String msg, final Throwable t) {

		}

		@Override
		public boolean isWarnEnabled(final Marker marker) {
			return false;
		}

		@Override
		public void warn(final Marker marker, final String msg) {

		}

		@Override
		public void warn(final Marker marker, final String format, final Object arg) {

		}

		@Override
		public void warn(final Marker marker, final String format, final Object arg1, final Object arg2) {

		}

		@Override
		public void warn(final Marker marker, final String format, final Object... arguments) {

		}

		@Override
		public void warn(final Marker marker, final String msg, final Throwable t) {

		}

		@Override
		public boolean isErrorEnabled() {
			return false;
		}

		@Override
		public void error(final String msg) {

		}

		@Override
		public void error(final String format, final Object arg) {

		}

		@Override
		public void error(final String format, final Object arg1, final Object arg2) {

		}

		@Override
		public void error(final String format, final Object... arguments) {

		}

		@Override
		public void error(final String msg, final Throwable t) {

		}

		@Override
		public boolean isErrorEnabled(final Marker marker) {
			return false;
		}

		@Override
		public void error(final Marker marker, final String msg) {

		}

		@Override
		public void error(final Marker marker, final String format, final Object arg) {

		}

		@Override
		public void error(final Marker marker, final String format, final Object arg1, final Object arg2) {

		}

		@Override
		public void error(final Marker marker, final String format, final Object... arguments) {

		}

		@Override
		public void error(final Marker marker, final String msg, final Throwable t) {

		}
	}

}
