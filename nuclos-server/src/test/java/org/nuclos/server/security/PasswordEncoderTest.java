package org.nuclos.server.security;
import java.util.ArrayList;
import java.util.Objects;

import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordEncoderTest {

	@Test
	public void testEncoderFactory() {
		PasswordEncoder getEncoder= PasswordEncoderFactory.getPasswordEncoder(PasswordEncoderFactory.Encoder.SHA1);
		assert getEncoder instanceof SHA1Encoder;
		assert PasswordEncoderFactory.getDefaultEncoder() instanceof SCRYPTEncoder;
		assert PasswordEncoderFactory.getEncoderOrdinal(getEncoder) == 0;
		assert PasswordEncoderFactory.getEncoderOrdinal(
				PasswordEncoderFactory.getDefaultEncoder()
		) == 1;
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullEncoderGet() {
		PasswordEncoderFactory.getPasswordEncoder(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullEncoderGetOrdinal() {
		PasswordEncoderFactory.getEncoderOrdinal(null);
	}

	@Test
	public void testSaltGeneration() {
		String salt = PasswordEncoderFactory.generateSalt(16);
		assert !salt.isEmpty();

		assert PasswordEncoderFactory.generateSalt(-1) != null;
		assert PasswordEncoderFactory.generateSalt(512) != null;
		assert PasswordEncoderFactory.generateSalt(Integer.MAX_VALUE) != null;
	}

	@Test
	public void testUserDetails() {
		assert PasswordEncoderFactory.testUserDetails(
				new NuclosUserDetails(
						"test",
						"UT8/Y2B4Pz8/Pz9Fenx2P1w/EUw=",
						"",
						0,
						false,
						false,
						false,
						false,
						new ArrayList<>(),
						null,
						"test@test",
                        null
				),
				"testtest"
		);
		assert PasswordEncoderFactory.testUserDetails(
				new NuclosUserDetails(
						"test",
						"krN+b904OF1GiwT7fQcmKRZcGJjscsfrmFTxipIkiJ6z33MnOpHeHsevAOkHBAcwMS6SZpeYG9aZXCs0",
						"B+jxOCeP+IoD2UyPGMWdew==",
						1,
						false,
						false,
						false,
						false,
						new ArrayList<>(),
						null,
						"test@test",
                        null
				),
				"testtest"
		);

		assert !PasswordEncoderFactory.testUserDetails(null, null);
		assert !PasswordEncoderFactory.testUserDetails(
				new NuclosUserDetails(
						"test",
						"UT8/Y2B4Pz8/Pz9Fenx2P1w/EUw=",
						"",
						0,
						false,
						false,
						false,
						false,
						new ArrayList<>(),
						null,
						"test@test",
                        null
				),
				null
		);
		assert !PasswordEncoderFactory.testUserDetails(
				null,
				"testtest"
		);
	}

	@Test
	public void testSHA1Encoding() throws Exception {
		PasswordEncoder sha1 = PasswordEncoderFactory.getPasswordEncoder(PasswordEncoderFactory.Encoder.SHA1);
		String encoded = sha1.encode("testtest", null);
		assert sha1.test("testtest", null, encoded): "Encoded should be testable";

		encoded  = sha1.encode(null, "");
		assert !encoded.isEmpty(): "Null encoding should return encoded data of empty";

		encoded = sha1.encode("testtest", "");
		assert sha1.test("testtest", null, encoded): "Encoded should be testable";

		assert !sha1.test("", null, null): "Test should return false for ambiguous data";
		assert !sha1.test(null, "", null): "Test should return false for ambiguous data";
		assert !sha1.test(null, null, ""): "Test should return false for ambiguous data";

		assert Objects.equals(sha1.encode("testtest", ""), sha1.encode("testtest", null)) : "Salt null or empty string should return same data";
		assert Objects.equals("UT8/Y2B4Pz8/Pz9Fenx2P1w/EUw=", sha1.encode("testtest", "")): "Encryption should work as before";
		assert Objects.equals("UT8/Y2B4Pz8/Pz9Fenx2P1w/EUw=", sha1.encode("testtest", null)): "Encryption should work as before";
	}

	@Test
	public void testSCRYPTEncoding() throws Exception {
		PasswordEncoder scrypt = PasswordEncoderFactory.getPasswordEncoder(PasswordEncoderFactory.Encoder.SCRYPT);
		String salt = PasswordEncoderFactory.generateSalt(16);
		String encoded = scrypt.encode("testtest", salt);
		assert scrypt.test("testtest", salt, encoded): "Encoded should be testable";

		encoded  = scrypt.encode(null, "");
		assert !encoded.isEmpty(): "Null encoding should return encoded data of empty";

		encoded = scrypt.encode("testtest", "");
		assert scrypt.test("testtest", null, encoded): "Encoded should be testable";

		String password = "qmPc&^a45#RHsCze4KGZWh46Ck!S2xqGN9HiHGkc8$s9$^rVCh8Q7oN!AC53yq%9U2LC42mqNijjHE$$s";
		encoded = scrypt.encode("test" + password, salt);
		assert encoded.length() == 80: "SCrypt key should at maximum 80 characters despite the password length";

		assert !scrypt.test("", null, null): "Test should return false for ambiguous data";
		assert !scrypt.test(null, "", null): "Test should return false for ambiguous data";
		assert !scrypt.test(null, null, ""): "Test should return false for ambiguous data";

		assert Objects.equals(scrypt.encode("testtest", ""), scrypt.encode("testtest", null)) : "Salt null or empty string should return same data";
	}
}
