package org.nuclos.server.websocket;

import java.util.Date;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class DefaultCodecTest {
	private DefaultCodec codec = new DefaultCodec();

	@Test
	public void testEncodeDecodeDate() throws Exception {
		Date date = new Date();
		String encoded = codec.encode(date);
		Date decoded = codec.decodeDate(encoded);

		assert date.equals(decoded);
	}
}