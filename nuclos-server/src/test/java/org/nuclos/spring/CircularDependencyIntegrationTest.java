package org.nuclos.spring;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nuclos.common.PostConstructManager;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class, CacheManager.class })
public class CircularDependencyIntegrationTest {

	@BeforeClass
	public static void setup() {
		PostConstructManager.setPostConstructDisabled();
	}

	@AfterClass
	public static void cleanup() {
		PostConstructManager.setPostConstructEnabled();
	}

	@Test
	public void givenCircularDependency_whenConstructorInjection_thenItFails() {
		// Empty test; we just want the context to load
	}

}
