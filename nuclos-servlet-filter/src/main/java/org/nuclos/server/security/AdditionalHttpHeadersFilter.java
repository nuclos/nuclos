package org.nuclos.server.security;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class AdditionalHttpHeadersFilter implements Filter {

	private static final String ANTI_CLICK_JACKING_ENABLED = "antiClickJackingEnabled";
	private static final String ANTI_CLICK_JACKING_OPTIONS = "antiClickJackingOptions";
	private static final String ADDITIONAL_HTTP_HEADERS = "additionalHttpHeaders";
	private FilterConfig filterConfig;
	private Map<String, String> mpHeaders = new HashMap<>();
	
	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;

		if (preventClickJacking()) {
			String antiClickJackingOptions = getAntiClickJackingOptions();
			setHeader("Content-Security-Policy", "frame-ancestors " + antiClickJackingOptions + ";");

			if ("'self'".equals(antiClickJackingOptions)) {
				setHeader("X-Frame-Options", "SAMEORIGIN");
			} else if (antiClickJackingOptions != null && antiClickJackingOptions.length() > 0) {
				setHeader("X-Frame-Options", "DENY");
			}
		}

		final String additionalHttpHeaders = getAdditionalHttpHeaders();
		if (additionalHttpHeaders != null && additionalHttpHeaders.length() > 0) {
			for (String line : additionalHttpHeaders.split("\n")) {
				final String[] headerValue = line.split(":");
				if (headerValue.length == 2) {
					String header = headerValue[0];
					String value = headerValue[1];

					setHeader(header, value);
				}
			}

		}

	}

	/**
	 * Add CSP policies on each HTTP response.
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		for (Map.Entry<String, String> header : mpHeaders.entrySet()) {
			httpResponse.setHeader(header.getKey(), header.getValue());
		}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

	private boolean preventClickJacking() {
		return Boolean.parseBoolean(this.filterConfig.getInitParameter(ANTI_CLICK_JACKING_ENABLED));
	}

	private String getAntiClickJackingOptions() {
		final String value = getTrimmedStringParameter(ANTI_CLICK_JACKING_OPTIONS);
		return value == null || value.equals("") ? "'none'" : value;
	}

	private String getAdditionalHttpHeaders() {
		return getTrimmedStringParameter(ADDITIONAL_HTTP_HEADERS);
	}

	private String getTrimmedStringParameter(String sParameter) {
		final String parameter = this.filterConfig.getInitParameter(sParameter);
		return parameter != null ? parameter.trim() : null;
	}

	private void setHeader(final String header, final String value) {
		String values = mpHeaders.computeIfAbsent(header, k -> "");
		mpHeaders.put(header, values + (values.length() == 0 ? "" : "; ") + value);
	}
}
