# Nuclos Webclient v2
## Prerequisites for local testing
You need a running Nuclos server which is accessible via the REST interface.

## Preparation
Install necessary Webclient dependencies via:
```
npm install
```

## Start webclient server
```
npm start
```
Angular CLI starts the Webclient locally on: `http://localhost:4200/`.

The default REST URL is: `//<host>:8080/nuclos-war`

You can override the REST URL via the "url" parameter:
```
npm start -- --url='//<host>:12345/nuclos-war'
```

## Code style

Proper formatting is enforced via linter checks and TypeScript compiler configuration.

For IntelliJ import the [Novabit profile](../codestyle/intellij/Novabit.xml). The auto-formatting should
then format your code correctly.
