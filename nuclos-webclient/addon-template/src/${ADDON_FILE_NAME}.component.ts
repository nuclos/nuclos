import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { ${ADDON_NAME_CAMEL_CASE}Service } from './${ADDON_FILE_NAME}.service';

@Component({
	selector: 'nuc-addon-${ADDON_FILE_NAME}',
	styles: [`
		:host {
			display: block;
			width: 100%;
			height: 100%;
			border: 1px dashed lightskyblue;
			text-align: center;
		}
	`],
	template: `
		<div>
			Addon ${ADDON_NAME_CAMEL_CASE} works.
		</div>
	`
})
export class ${ADDON_NAME_CAMEL_CASE}Component implements OnInit, OnDestroy {

	// Shows component in modal if true otherwise it will be instantiated invisible
	@Input()
	showAddonUI = true;

	constructor(
		private service: ${ADDON_NAME_CAMEL_CASE}Service
	) {
	}

	ngOnInit() {

	}

	ngOnDestroy(): void {
	}

}
