import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ${ADDON_NAME_CAMEL_CASE}Component } from './${ADDON_FILE_NAME}.component';

import { ${ADDON_NAME_CAMEL_CASE}Service } from './${ADDON_FILE_NAME}.service';

@NgModule({
	imports: [
		CommonModule
	],
	providers: [
		${ADDON_NAME_CAMEL_CASE}Service,
		{
			provide: '${ADDON_NAME_CAMEL_CASE}Component',
			useValue: ${ADDON_NAME_CAMEL_CASE}Component
		}
	],
	declarations: [
		${ADDON_NAME_CAMEL_CASE}Component
	],
	exports: [
		${ADDON_NAME_CAMEL_CASE}Component
	],
	entryComponents: [
		${ADDON_NAME_CAMEL_CASE}Component
	],
	id: '${ADDON_NAME_CAMEL_CASE}'
})
export class ${ADDON_NAME_CAMEL_CASE}Module {
}
