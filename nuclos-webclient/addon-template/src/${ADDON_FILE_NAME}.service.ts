import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class ${ADDON_NAME_CAMEL_CASE}Service {

	constructor() {
	}

}
