// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

var isDocker = require('is-docker')();

module.exports = function (config) {
	config.set({
		basePath: './',
		frameworks: ['jasmine', '@angular-devkit/build-angular'],
		plugins: [
			require('karma-jasmine'),
			require('karma-chrome-launcher'),
			require('karma-jasmine-html-reporter'),
			require('karma-coverage-istanbul-reporter'),
			require('@angular-devkit/build-angular/plugins/karma')
		],
		client:{
			clearContext: false // leave Jasmine Spec Runner output visible in browser
		},
		coverageIstanbulReporter: {
			dir: require('path').join(__dirname, 'coverage'), reports: [ 'html', 'lcovonly' ],
			fixWebpackSourcePaths: true
		},
		
		reporters: ['progress', 'kjhtml'],
		port: 9876,
		colors: true,
		logLevel: config.LOG_INFO,
		autoWatch: true,
		browsers: ['Chrome'],
		singleRun: false,
		files: [
			// This should not be necessary. But for some reason Zone was not available in "ng test"
			'node_modules/zone.js/dist/zone.js',
			'node_modules/zone.js/dist/long-stack-trace-zone.js'
		],
		customLaunchers: {
			ChromeCustom: {
				base: 'ChromeHeadless',
				// We must disable the Chrome sandbox when running Chrome inside Docker (Chrome's sandbox needs
				// more permissions than Docker allows by default)
				flags: isDocker ? ['--no-sandbox'] : []
			}
		},
		browserNoActivityTimeout: 100000
	});
};
