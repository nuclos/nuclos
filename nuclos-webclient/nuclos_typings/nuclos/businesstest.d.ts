export interface BusinesstestOverallResult {
	testsTotal: number;
	testsGreen: number;
	testsYellow: number;
	testsRed: number;
	duration: number;
	state: 'GREE' | 'YELLOW' | 'RED';
}

export interface IBusinessTestVO {
	state: any;
	duration: number;
	description: string;
	source: string;
	entity: any;
	nuclet: any;
	startdate: any;
	result: string;
	stacktrace: string;
	warningCount: number;
	errorCount: number;
	enddate: any;
	masterDataVO: any;
	name: string;
	primaryKey: any;
	id: any;
	version: number;
	createdBy: string;
	createdAt: number;
	changedAt: number;
	changedBy: string;
	removed: boolean;
	warningLine: number;
	errorLine: number;
	log: string;
}
