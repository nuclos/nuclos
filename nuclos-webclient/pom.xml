<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.nuclos</groupId>
		<artifactId>nuclos</artifactId>
		<version>4.2025.9-SNAPSHOT</version>
	</parent>
	<artifactId>nuclos-webclient</artifactId>
	<name>nuclos-webclient</name>

	<properties>
		<webclient.base.href>/webclient/</webclient.base.href>
		<!-- Changes to this variable are not recognized by regex.property goal, when it is declared -->
		<!--webclient.build.timestamp></webclient.build.timestamp-->
		<webclient.build.conf>prod</webclient.build.conf>

		<skip.test>true</skip.test>

		<coverage.force.delivery.to.war.skip>true</coverage.force.delivery.to.war.skip>
		<sonar.moduleKey>${project.artifactId}</sonar.moduleKey>
		<sonar.language>ts</sonar.language>
		<sonar.sources>${project.basedir}/src</sonar.sources>
		<sonar.javascript.lcov.reportPaths>target/coverage-output/lcov.info</sonar.javascript.lcov.reportPaths>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.nuclos</groupId>
			<artifactId>nuclos-schema</artifactId>
			<version>${project.version}</version>
			<scope>compile</scope>
		</dependency>
	</dependencies>

	<build>
		<sourceDirectory>src</sourceDirectory>
		<plugins>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>build-helper-maven-plugin</artifactId>
				<version>3.0.0</version>
				<executions>
					<execution>
						<id>timestamp-property</id>
						<phase>validate</phase>
						<goals>
							<goal>timestamp-property</goal>
						</goals>
						<configuration>
							<name>webclient.build.timestamp</name>
							<pattern>yyyyMMddHHmmssSSS</pattern>
							<unit>millisecond</unit>
							<locale>de,DE</locale>
						</configuration>
					</execution>

					<!--
					Special way to make Nuclos versioning compatible with SemVer
					-->
					<execution>
						<id>regex-property-semver</id>
						<phase>validate</phase>
						<goals>
							<goal>regex-property</goal>
						</goals>
						<configuration>
							<name>webclient.npm.version</name>
							<value>${project.version}</value>
							<regex>(\d+)\.(\d+)\.(\d+)\.(\d+)</regex>
							<replacement>$1.$2.$3-$4</replacement>
							<failIfNoMatch>false</failIfNoMatch>
						</configuration>
					</execution>

					<execution>
						<id>regex-property</id>
						<phase>validate</phase>
						<goals>
							<goal>regex-property</goal>
						</goals>
						<configuration>
							<name>webclient.npm.version</name>
							<value>${webclient.npm.version}</value>
							<regex>-SNAPSHOT</regex>
							<replacement>-SNAPSHOT.${webclient.build.timestamp}</replacement>
							<failIfNoMatch>false</failIfNoMatch>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>com.github.eirslett</groupId>
				<artifactId>frontend-maven-plugin</artifactId>
				<version>1.7.6</version>
				<configuration>
					<workingDirectory>${project.basedir}</workingDirectory>
					<installDirectory>${project.build.directory}</installDirectory>
				</configuration>
				<executions>
					<!-- It will install nodejs and npm -->
					<execution>
						<id>install-node-and-npm</id>
						<phase>initialize</phase>
						<goals>
							<goal>install-node-and-npm</goal>
						</goals>
						<configuration>
							<nodeVersion>${node.version}</nodeVersion>
							<npmVersion>${npm.version}</npmVersion>
						</configuration>
					</execution>

					<!-- It will execute command "npm install @nuclos/nuclos-addon-api + npm ci" -->
					<execution>
						<id>npm-install-dependencies</id>
						<phase>initialize</phase>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<arguments>run install-dependencies:prod</arguments>
						</configuration>
					</execution>

					<!-- It will execute command "npm run lint"-->
					<execution>
						<id>npm-lint</id>
						<phase>process-sources</phase>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<arguments>run lint</arguments>
						</configuration>
					</execution>

					<!-- It will execute command "npm version ..."-->
					<execution>
						<id>npm-version</id>
						<phase>process-sources</phase>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<!-- The following will set a SNAPSHOT working webclient version for our public nexus repository -->
							<arguments>version ${webclient.npm.version} --allow-same-version</arguments>
						</configuration>
					</execution>

					<!-- It will execute command "npm run build:prod ..." inside the prepared directory to clean and create "/dist" directory-->
					<execution>
						<id>npm-build</id>
						<phase>compile</phase>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<arguments>run build:${webclient.build.conf} --@nuclos/nuclos-webclient:build-base-href="${webclient.base.href}"</arguments>
						</configuration>
					</execution>

					<!-- It will execute command "npm publish"-->
					<execution>
						<id>npm-publish</id>
						<phase>deploy</phase>
						<goals>
							<goal>npm</goal>
						</goals>
						<configuration>
							<arguments>publish</arguments>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>aspectj-maven-plugin</artifactId>
				<executions>
					<execution>
						<phase>none</phase>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-antrun-plugin</artifactId>
				<version>3.0.0</version>
				<executions>
					<!-- Build webclient.zip -->
					<execution>
						<id>build-webclient-zip-dist</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>run</goal>
						</goals>
						<configuration>
							<target>
								<property name="compile_classpath"
								          refid="maven.compile.classpath" />
								<property name="outputDir"
								          value="${basedir}" />
								<property name="sourceDir"
								          value="${basedir}" />
								<zip destfile="${project.build.directory}/webclient-dist.zip">
									<zipfileset dir="${project.basedir}/dist" />
								</zip>
								<zip destfile="${project.build.directory}/webclient-src.zip">
									<zipfileset dir="${project.basedir}/src" prefix="src/">
										<exclude name="src/assets/config.json" />
										<exclude name="**/*/config.json" />
										<exclude name="**/config.json" />
									</zipfileset>
									<zipfileset dir="${project.basedir}/scripts" prefix="scripts/" />
									<zipfileset dir="${project.basedir}/addon-template" prefix="addon-template/" />
									<zipfileset dir="${project.basedir}/docs" prefix="docs/" />
									<zipfileset dir="${project.basedir}/nuclos_typings" prefix="nuclos_typings/" />
									<zipfileset file="${project.basedir}/package.json" />
									<zipfileset file="${project.basedir}/angular.json" />
									<zipfileset file="${project.basedir}/karma.conf.js" />
									<zipfileset file="${project.basedir}/protractor.conf.js" />
									<zipfileset file="${project.basedir}/tslint.json" />
									<zipfileset file="${project.basedir}/tsconfig.json" />
									<zipfileset file="${project.basedir}/npm-shrinkwrap.json" />
									<zipfileset file="${project.basedir}/README.md" />
								</zip>
							</target>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>cz.habarta.typescript-generator</groupId>
				<artifactId>typescript-generator-maven-plugin</artifactId>
				<version>2.12.476</version>
				<executions>
					<execution>
						<id>generate-weblayout</id>
						<goals>
							<goal>generate</goal>
						</goals>
						<phase>generate-sources</phase>
						<configuration>
							<jsonLibrary>jackson2</jsonLibrary>
							<classPatterns>
								<classPattern>org.nuclos.schema.layout.web.**</classPattern>
							</classPatterns>
							<outputFile>${project.basedir}/src/app/layout/shared/weblayout.d.ts</outputFile>
							<outputKind>global</outputKind>
							<noFileComment>true</noFileComment>
							<!-- Plugin seems to ignore \t as indentString :( -->
							<indentString>&#9;</indentString>
							<excludeClassPatterns>
								<excludeClassPattern>**$Builder</excludeClassPattern>
								<excludeClassPattern>**$Select</excludeClassPattern>
								<excludeClassPattern>**$Selector</excludeClassPattern>
							</excludeClassPatterns>
						</configuration>
					</execution>
					<execution>
						<id>generate-rules</id>
						<goals>
							<goal>generate</goal>
						</goals>
						<phase>generate-sources</phase>
						<configuration>
							<jsonLibrary>jackson2</jsonLibrary>
							<classPatterns>
								<classPattern>org.nuclos.schema.layout.rule.**</classPattern>
							</classPatterns>
							<outputFile>src/app/modules/rule/shared/rules.d.ts</outputFile>
							<outputKind>global</outputKind>
							<noFileComment>true</noFileComment>
							<!-- Plugin seems to ignore \t as indentString :( -->
							<indentString>&#9;</indentString>
							<excludeClassPatterns>
								<excludeClassPattern>**$Builder</excludeClassPattern>
								<excludeClassPattern>**$Select</excludeClassPattern>
								<excludeClassPattern>**$Selector</excludeClassPattern>
							</excludeClassPatterns>
						</configuration>
					</execution>
					<execution>
						<id>generate-vlps</id>
						<goals>
							<goal>generate</goal>
						</goals>
						<phase>generate-sources</phase>
						<configuration>
							<jsonLibrary>jackson2</jsonLibrary>
							<classPatterns>
								<classPattern>org.nuclos.schema.layout.vlp.**</classPattern>
							</classPatterns>
							<outputFile>src/app/modules/vlp/shared/vlps.d.ts</outputFile>
							<outputKind>global</outputKind>
							<noFileComment>true</noFileComment>
							<!-- Plugin seems to ignore \t as indentString :( -->
							<indentString>&#9;</indentString>
							<excludeClassPatterns>
								<excludeClassPattern>**$Builder</excludeClassPattern>
								<excludeClassPattern>**$Select</excludeClassPattern>
								<excludeClassPattern>**$Selector</excludeClassPattern>
							</excludeClassPatterns>
						</configuration>
					</execution>
					<execution>
						<id>generate-entities</id>
						<goals>
							<goal>generate</goal>
						</goals>
						<phase>generate-sources</phase>
						<configuration>
							<!-- we use jaxb for the xml annotation @XmlAttribute(required = true) to be processed correctly -->
							<jsonLibrary>jaxb</jsonLibrary>
							<optionalProperties>useLibraryDefinition</optionalProperties>
							<classPatterns>
								<classPattern>org.nuclos.schema.rest.**</classPattern>
							</classPatterns>
							<outputFile>${project.basedir}/src/app/shared/entities.d.ts</outputFile>
							<outputKind>global</outputKind>
							<noFileComment>true</noFileComment>
							<!-- Plugin seems to ignore \t as indentString :( -->
							<indentString>&#9;</indentString>
							<excludeClassPatterns>
								<excludeClassPattern>**$Builder</excludeClassPattern>
								<excludeClassPattern>**$Select</excludeClassPattern>
								<excludeClassPattern>**$Selector</excludeClassPattern>
								<excludeClassPattern>**$Links</excludeClassPattern>
							</excludeClassPatterns>
						</configuration>
					</execution>
					<execution>
						<id>generate-meta-entity</id>
						<goals>
							<goal>generate</goal>
						</goals>
						<phase>generate-sources</phase>
						<configuration>
							<!-- we use jaxb for the xml annotation @XmlAttribute(required = true) to be processed correctly -->
							<jsonLibrary>jaxb</jsonLibrary>
							<optionalProperties>useLibraryDefinition</optionalProperties>
							<classPatterns>
								<classPattern>org.nuclos.schema.meta.entity.**</classPattern>
							</classPatterns>
							<outputFile>${project.basedir}/src/app/shared/meta/entity.d.ts</outputFile>
							<outputKind>global</outputKind>
							<noFileComment>true</noFileComment>
							<!-- Plugin seems to ignore \t as indentString :( -->
							<indentString>&#9;</indentString>
							<excludeClassPatterns>
								<excludeClassPattern>**$Builder</excludeClassPattern>
								<excludeClassPattern>**$Select</excludeClassPattern>
								<excludeClassPattern>**$Selector</excludeClassPattern>
								<excludeClassPattern>**$Links</excludeClassPattern>
							</excludeClassPatterns>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.owasp</groupId>
				<artifactId>dependency-check-maven</artifactId>
				<configuration>
					<scanSet>
						<fileSet>
							<directory>${project.basedir}</directory>
							<includes>
								<include>package.json</include>
								<include>package-lock.json</include>
							</includes>
						</fileSet>
					</scanSet>
				</configuration>
			</plugin>

			<plugin>
				<artifactId>maven-clean-plugin</artifactId>
				<version>3.1.0</version>
				<configuration>
					<filesets>
						<fileset>
							<directory>${project.basedir}/dist</directory>
						</fileset>
					</filesets>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>test</id>
			<properties>
				<skip.test>false</skip.test>
				<webclient.base.href>/</webclient.base.href>
				<webclient.build.conf>prod</webclient.build.conf>
			</properties>
		</profile>
		<profile>
			<id>test2</id>
			<properties>
				<skip.test>false</skip.test>
				<webclient.base.href>/</webclient.base.href>
			</properties>
		</profile>
	</profiles>
</project>
