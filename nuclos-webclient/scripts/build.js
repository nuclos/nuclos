#!/usr/bin/env node

const buildMode = process.argv[2];
const baseHref = process.env.npm_package_config_build_base_href;
const outputPath = process.env.npm_package_config_build_output_path;
var cmd = "node ./node_modules/@angular/cli/bin/ng build --base-href \""+baseHref+"\" --output-path \""+outputPath+"\"";
if ( buildMode == "prod" ) {
	cmd += " --configuration production --build-optimizer --vendor-chunk --output-hashing all --named-chunks false"
} else {
	cmd += " --configuration development"
}
console.log('Building: ', cmd);
child = require('child_process').execSync(cmd, {
	cwd: '.',
	stdio: [ 'ignore', 1, 2 ]
});
