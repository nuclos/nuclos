console.log("Checking dependencies in package.json...");

let fs = require("fs");
let content = fs.readFileSync("package.json");
let json = JSON.parse(content);

/**
 * Makes sure that there are no dynamic semver package versions used in the package.json of the Webclient,
 * but only fixed version numbers.
 * Thereby the Webclient build is always reproducible, see NUCLOS-7601
 */
let check = deps => {
	for (let packageName in deps) {
		let version = deps[packageName];
		if (version === 'lates' ||
			version.startsWith('^') ||
			version.startsWith('~') ||
			version.startsWith('>') ||
			version.startsWith('<')
		) {
			throw new Error('Invalid version \'' + version + '\' for package \'' + packageName + '\' - only fixed versions are allowed!')
		}
	}
};

check(json.dependencies);
check(json.devDependencies);
