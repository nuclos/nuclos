const path_to_replace = process.env.npm_config_coverage_path_replace;
const destPath = process.env.npm_config_coverage_dest_path;

if (path_to_replace !== '' && path_to_replace !== undefined && destPath !== '' && destPath !== undefined) {
	console.log('Replacing paths and run nyc reporter...')
	const sed_cmd = `sed -i 's:${path_to_replace}:${destPath}:g' ./target/coverage-output/coverage_* `;
	console.log('Executing: ' + sed_cmd);
	child = require('child_process').execSync(sed_cmd, {
		cwd: '.',
		stdio: [ 'ignore', 1, 2 ]
	});
}
child = require('child_process').execSync(`node ./node_modules/nyc/bin/nyc report --reporter=lcov --reporter=text-summary -t target/coverage-output --report-dir=target/coverage-output`, {
	cwd: '.',
	stdio: [ 'ignore', 1, 2 ]
});

child = require('child_process').execSync(`sed -i 's|SF:dist/webpack:/|SF:|g' ./target/coverage-output/lcov.info`, {
	cwd: '.',
	stdio: [ 'ignore', 1, 2 ]
});
