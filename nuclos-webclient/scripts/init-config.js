let fs = require('fs');

// change server value by adding --@nuclos/nuclos-webclient:nuclos-server="172.17.0.1" (npm v6)
// change server-port value by adding --@nuclos/nuclos-webclient:nuclos-server-port=8090 (npm v6)
// change server-context value by adding --@nuclos/nuclos-webclient:nuclos-server-context=extension-war (npm v6)

// change server value by adding --nuclos-server="172.17.0.1" (npm v7+)
// change server-port value by adding --nuclos-server-port=8090 (npm v7+)
// change server-context value by adding --nuclos-server-context=extension-war (npm v7+)

let server = process.env.npm_config_nuclos_server ?? process.env.npm_package_config_nuclos_server;
let port = process.env.npm_config_nuclos_server_port ?? process.env.npm_package_config_nuclos_server_port ?? process.env.HOST_PORT;
let appcontext = process.env.npm_config_nuclos_server_context ?? process.env.npm_package_config_nuclos_server_context
let conf = {
	"nuclosURL": "//" + server + ":" + port + "/" + appcontext
};
fs.writeFile(
	'src/assets/config.json',
	JSON.stringify(conf),
	error => {console.error(error);}
);
