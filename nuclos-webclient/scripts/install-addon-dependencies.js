#!/usr/bin/env node

/**
 * prints the command to install addon dependencies
 *
 * this is useful when using the addon development mode
 *
 * usage: $(./install-addon-dependencies.js)
 */

const fs = require('fs');
const path = require('path');

let runCmd = cmd => {
	console.log('Installing: ', cmd);
	require('child_process').execSync(cmd, {
		cwd: '.',
		stdio: ['ignore', 1, 2]
	});
};

const npmRegistry = process.env.npm_package_config_npm_registry;
const nodeSassBinarySite = process.env.npm_package_config_node_sass_binary_site;

const isDirectory = source => fs.lstatSync(source).isDirectory();
const getDirectories = source => fs.existsSync(source)
	? fs.readdirSync(source)
		// Must not be a hidden file which typically start with a dot, e.g. .git
		.filter(name => !name.startsWith('.'))
		.map(name => path.join(source, name))
		.filter(isDirectory)
		.filter(dir => fs.existsSync(dir + '/package.json'))
	: [];

// Shall we add styles/scripts for 3rd party deps to angular.json
const import3rdPartyItems = process.argv.findIndex((value) => value === "--ignore-angular-json-extensions") === -1;
let angular3rdPartyStyles = new Set();
let angular3rdPartyScripts = new Set();
let angular3rdPartyAssets = new Set();

let dependencies = new Set();
let devDependencies = new Set();
[
	...getDirectories('addons'),
	...getDirectories('src/addons') // addon-dev-mode
]
	.forEach(dir => {
		const content = fs.readFileSync(dir + '/package.json');
		const jsonContent = JSON.parse(content);
		for (const key in jsonContent.dependencies) {
			const value = jsonContent.dependencies[key];
			if (key.indexOf('@angular') === -1) {
				dependencies.add(key + '@' + value);
			}
		}

		for (const key in jsonContent.devDependencies) {
			const value = jsonContent.devDependencies[key];
			if (key.indexOf('@angular') === -1) {
				devDependencies.add(key + '@' + value);
			}
		}

		if (import3rdPartyItems) {
			if (fs.existsSync(dir + '/angular-ext.json')) {
				const angularExtJsonContent = JSON.parse(fs.readFileSync(dir + '/angular-ext.json'));
				angularExtJsonContent.styles.forEach(style => angular3rdPartyStyles.add(style));
				angularExtJsonContent.scripts.forEach(script => angular3rdPartyScripts.add(script));
				angularExtJsonContent.assets.forEach(asset => angular3rdPartyAssets.add(asset))
			}
		}
	});

if (import3rdPartyItems) {
	if (fs.existsSync('angular.json')) {
		console.log('Adding to angular.json: styles[' + Array.from(angular3rdPartyStyles.values()).join(', ') + '], scripts['
			+ Array.from(angular3rdPartyScripts.values()).join(', ') + '], assets[' + Array.from(angular3rdPartyAssets.values()) + ']');
		const angularJson = JSON.parse(fs.readFileSync('angular.json'));
		angular3rdPartyStyles.forEach(style => {
			if (!angularJson.projects["nuclos-webclient"].architect.build.options.styles.includes(style)) {
				angularJson.projects["nuclos-webclient"].architect.build.options.styles.push(style);
			}
		});

		angular3rdPartyScripts.forEach(script => {
			if (!angularJson.projects["nuclos-webclient"].architect.build.options.scripts.includes(script)) {
				angularJson.projects["nuclos-webclient"].architect.build.options.scripts.push(script);
			}
		});

		angular3rdPartyAssets.forEach(asset => {
			if (!angularJson.projects["nuclos-webclient"].architect.build.options.assets.includes(asset)) {
				angularJson.projects["nuclos-webclient"].architect.build.options.assets.push(asset);
			}
		});

		fs.copyFileSync('angular.json', 'angular.json.bak');
		fs.writeFileSync('angular.json', JSON.stringify(angularJson, null, 4));
	}
}

// To skip the compilation of node-sass (which can be time-consuming and resource-intensive) we set the "SASS_BINARY_SITE":
runCmd('SASS_BINARY_SITE='+nodeSassBinarySite+' npm install --registry=' + npmRegistry + ' --strict-ssl=false  --legacy-peer-deps --save-exact ' + Array.from(dependencies.values()).join(' '));
runCmd('SASS_BINARY_SITE='+nodeSassBinarySite+' npm install --registry=' + npmRegistry + ' --strict-ssl=false  --legacy-peer-deps --save-dev ' + Array.from(devDependencies.values()).join(' '));
