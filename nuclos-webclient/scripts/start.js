#!/usr/bin/env node

// change host value by adding --@nuclos/nuclos-webclient:webclient-host="172.17.0.1" (npm v6)
// change port value by adding --@nuclos/nuclos-webclient:webclient-port=4300 (npm v6)

// change host value by adding --webclient-host="172.17.0.1" (npm v7+)
// change port value by adding --webclient-port=4300 (npm v7+)

const sslMode = process.argv[2];

const host = process.env.npm_config_webclient_host ?? process.env.npm_package_config_webclient_host;
const port = process.env.npm_config_webclient_port ?? process.env.npm_package_config_webclient_port;
let cmd = "node ./node_modules/@angular/cli/bin/ng serve --host="+host+" --port="+port+" --configuration=development --disable-host-check true";

const publicHost = process.env.npm_package_config_webclient_public_host;
if ( publicHost !== undefined ) {
	cmd += " --public-host="+publicHost;
}

if ( sslMode === "ssl" ) {
	const key = process.env.npm_package_config_webclient_ssl_key;
	const cert = process.env.npm_package_config_webclient_ssl_cert;
	cmd += " --ssl --ssl-key="+key+" --ssl-cert="+cert;
}

console.log('Starting: ', cmd);
child = require('child_process').execSync(cmd, {
	cwd: '.',
	stdio: [ 'ignore', 1, 2 ]
});
