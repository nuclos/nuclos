import { DOCUMENT } from '@angular/common';
import { ChangeDetectorRef, Component, ElementRef, Inject, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { Locale } from '@modules/i18n/shared/locale';
import { Logger } from '@modules/log/shared/logger';
import { NuclosTitleService } from '@shared/service/nuclos-title.service';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { BrowserDetectionService } from './shared/service/browser-detection.service';
import { BusyService } from './shared/service/busy.service';

@Component({
	selector: 'nuc-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
	title = 'Nuclos Webclient v2';

	busy = false;
	loadingAnimationType = ngxLoadingAnimationTypes.threeBounce;

	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private elementRef: ElementRef,
		private browserDetectionService: BrowserDetectionService,
		private busyService: BusyService,
		private ngZone: NgZone,
		private changeDetectorRef: ChangeDetectorRef,
		private $log: Logger,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private titleService: NuclosTitleService,
		private i18nService: NuclosI18nService,
		@Inject(DOCUMENT) private document: Document
	) {
		window['NgZone'] = this.ngZone;

		this.i18nService.getLocale().pipe(
			takeUntil(this.unsubscribe$)
		).subscribe((locale: Locale) => {
			this.document.documentElement.lang = locale.key;
		});
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnInit(): void {
		let canonicalUrl = window.location.href.replace(/([^:]\/)\/+/g, '$1');
		if (canonicalUrl !== window.location.href) {
			window.location.href = canonicalUrl;
		}

		if (this.browserDetectionService.isSafari()) {
			$(this.elementRef.nativeElement).addClass('browser-safari');
		}

		this.busyService.isBusy().pipe(takeUntil(this.unsubscribe$)).subscribe(busy => {
			this.busy = busy;

			// https://github.com/angular/angular/issues/17572#issuecomment-323465737
			this.changeDetectorRef.detectChanges();
		});

		this.router.events.pipe(
			filter(event => event instanceof NavigationEnd)
		).subscribe(() => {
			const route: ActivatedRoute = this.getChild(this.activatedRoute);
			route.data.subscribe(data => {
				if (data.localizedTitle) {
					this.titleService.setLocalizedTitle(data.localizedTitle);
				} else if (data.title) {
					this.titleService.setTitle(data.title);
				} else if (data.setDefaultTitle !== false) {
					this.titleService.setDefaultTitle();
				}
			});
		});
	}

	private getChild(activatedRoute: ActivatedRoute) {
		if (activatedRoute.firstChild) {
			return this.getChild(activatedRoute.firstChild);
		} else {
			return activatedRoute;
		}

	}
}


