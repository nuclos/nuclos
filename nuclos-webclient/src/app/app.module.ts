import { LOCATION_INITIALIZED } from '@angular/common';
import { APP_INITIALIZER, Injector, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DndModule } from 'ng2-dnd';
import { NgxLoadingModule } from 'ngx-loading';
import { TreeModule } from 'primeng/tree';
import { AddonModules } from './addon.modules';
import { AppComponent } from './app.component';
import { AppRoutesModule } from './app.routes';
import { CoreModule } from './core/core.module';
import { ChartModule } from './modules/chart/chart.module';
import { I18nModule } from './modules/i18n/i18n.module';
import { LogModule } from './modules/log/log.module';
import { MenuModule } from './modules/menu/menu.module';
import { NewsRouteModule } from './modules/news/news-route/news-route.module';
import { UiComponentsModule } from './modules/ui-components/ui-components.module';
import { CanNeverActivateGuard } from './core/guard/can-never-activate-guard';
import { BrowserDetectionService } from './shared/service/browser-detection.service';
import { BrowserRefreshService } from './shared/service/browser-refresh.service';
import { BusyService } from './shared/service/busy.service';
import { DatasourceService } from './shared/service/datasource.service';
import { DatetimeService } from './shared/service/datetime.service';
import { FqnService } from './shared/service/fqn.service';
import { HyperlinkService } from './shared/service/hyperlink.service';
import { IdFactoryService } from './shared/service/id-factory.service';
import { NuclosConfigService } from './shared/service/nuclos-config.service';
import { NumberService } from './shared/service/number.service';
import { SharedModule } from './shared/shared.module';

export function configFactory(config: NuclosConfigService, injector: Injector) {
	// fix for https://github.com/angular/angular-cli/issues/5762
	return () => new Promise<any>((resolve: any) => {
		const locationInitialized = injector.get(LOCATION_INITIALIZED, Promise.resolve(null));
		locationInitialized.then(() => {
			config.load().then(() => resolve(null));
		});
	});
}

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		AddonModules,

		// Angular modules
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,

		DndModule.forRoot(),

		// Nuclos modules
		CoreModule,
		SharedModule,

		ChartModule,
		I18nModule,
		NgxLoadingModule,
		LogModule,
		MenuModule,
		NewsRouteModule,
		UiComponentsModule,

		// App Routes (contains Wildcard-Routes and must therefor be defined last)
		AppRoutesModule,
		TreeModule
	],
	providers: [
		BrowserRefreshService,
		BrowserDetectionService,
		BusyService,
		DatasourceService,
		FqnService,
		DatetimeService,
		NumberService,
		NuclosConfigService,
		HyperlinkService,
		{
			provide: APP_INITIALIZER,
			useFactory: configFactory,
			deps: [NuclosConfigService, Injector],
			multi: true
		},
		CanNeverActivateGuard,
		IdFactoryService
	],
	exports: [
		AppComponent,
	],
	bootstrap: [
		AppComponent
	]
})
export class AppModule {
}
