import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ServerConnectivityGuard } from '@app/guard/server-connectivity-guard';
import { SetLocaleComponent } from '@modules/i18n/set-locale/set-locale.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'about',
		data: {
			localizedTitle: 'webclient.user.about'
		},
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/system-information/system-information.module').then(m => m.SystemInformationModule)
	},
	{
		path: 'auth',
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/authentication/authentication.module').then(m => m.AuthenticationModule)
	},
	{
		path: 'account',
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/account/account.module').then(m => m.AccountModule)
	},
	{
		path: 'maintenance',
		data: {
			localizedTitle: 'webclient.admin.maintenance.mode'
		},
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/admin/maintenance/maintenance.module').then(m => m.MaintenanceModule)
	},
	{
		path: 'businesstests',
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/businesstest/businesstest.module').then(m => m.BusinesstestModule)
	},
	{
		path: 'cache',
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/cache/cache.module').then(m => m.CacheModule)
	},
	{
		path: 'dashboard',
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
	},
	{
		path: '',
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/entity-object/entity-object.module').then(m => m.EntityObjectModule)
	},
	{
		path: 'error',
		loadChildren: () => import('./modules/error/error.module').then(m => m.ErrorModule)
	},
	{
		path: 'news',
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/news/news.module').then(m => m.NewsModule)
	},
	{
		path: 'preferences',
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/preferences/preferences.module').then(m => m.PreferencesModule)
	},
	{
		path: 'reports',
		data: {
			localizedTitle: 'webclient.reports.execute'
		},
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/reports/reports.module').then(m => m.ReportsModule)
	},
	{
		path: '',
		data: {
			localizedTitle: 'webclient.planningTable.name'
		},
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('@modules/planning-table/planning-table.module').then(m => m.PlanningTableModule)
	},
	{
		path: 'serverinfo',
		data: {
			localizedTitle: 'webclient.serverInfo.name'
		},
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/server-info/server-info.module').then(m => m.ServerInfoModule)
	},
	{
		path: 'swagger-ui',
		data: {
			localizedTitle: 'webclient.swaggerUI'
		},
		canActivate: [ServerConnectivityGuard],
		canActivateChild: [ServerConnectivityGuard],
		loadChildren: () => import('./modules/swagger/swagger.module').then(m => m.SwaggerModule)
	},
	{
		path: 'login',
		pathMatch: 'full',
		redirectTo: '/auth/login'
	},
	{
		path: 'logout',
		pathMatch: 'full',
		redirectTo: '/auth/logout'
	},
	{
		path: 'session-info',
		pathMatch: 'full',
		redirectTo: '/auth/session-info'
	},
	{
		path: '',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: 'index.html',
		pathMatch: 'full',
		redirectTo: 'login'
	},
	{
		path: 'locale/:locale',
		component: SetLocaleComponent
	},
	{
		path: '**',
		redirectTo: 'error/404'
	}
];

export const AppRoutesModule = RouterModule.forRoot(ROUTE_CONFIG, {
	useHash: true,
	initialNavigation: 'enabledNonBlocking',
	preloadingStrategy: PreloadAllModules
});
