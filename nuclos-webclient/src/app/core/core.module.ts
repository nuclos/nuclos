import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule, Optional, SkipSelf } from '@angular/core';
import { NavigationGuard } from '@app/guard/navigation-guard';
import { ServerConnectivityGuard } from '@app/guard/server-connectivity-guard';
import { CookieService } from 'ngx-cookie-service';

import { throwIfAlreadyLoaded } from './guard/module-import.guard';
import { NuclosHttpInterceptor } from './interceptors/nuclos-http-interceptor';
import { GlobalErrorHandlerService } from './service/global-error-handler.service';

@NgModule({
	imports: [
		HttpClientModule
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: NuclosHttpInterceptor,
			multi: true
		},
		{
			provide: ErrorHandler,
			useClass: GlobalErrorHandlerService
		},
		CookieService,
		NavigationGuard,
		ServerConnectivityGuard
	]
})
export class CoreModule {
	constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
		throwIfAlreadyLoaded(parentModule, 'CoreModule');
	}
}
