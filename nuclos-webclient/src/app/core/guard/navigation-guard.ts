import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { take } from 'rxjs/operators';
import { EntityObjectResultService } from '@modules/entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { EntityObjectComponent } from '@modules/entity-object/entity-object.component';

@Injectable()
export class NavigationGuard implements CanDeactivate<EntityObjectComponent> {

	constructor(
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService,
	) {
	}

	canDeactivate(
		component: EntityObjectComponent,
		route: ActivatedRouteSnapshot,
		_state: RouterStateSnapshot
	): boolean {
		return this.canDeactivateSelectedEo(component);
	}

	canDeactivateSelectedEo(
		component: EntityObjectComponent | undefined,
		newIdFromSelectionEvent: any = undefined,
		triggerWarnChange = true
	): boolean {
		let selectedEO = this.eoResultService.getSelectedEo() as EntityObject;

		if (selectedEO && selectedEO.getId() === newIdFromSelectionEvent) {
			// object is already in result service selected. nothing to check any more...
			return true;
		}

		if (selectedEO) {
			// allow navigation if new eo was not modified
			if (selectedEO.isNew() && !selectedEO.isDirty()) {
				// TODO: This class is a guard. It should not actively interfere with EO services.
				this.eoService.delete(selectedEO).pipe(take(1)).subscribe(
					() => {
						this.eoResultService.selectEo(undefined);
					}
				);
			}
		}

		if (selectedEO && selectedEO.isDirty()) {
			if (triggerWarnChange) {
				selectedEO.triggerWarnChange();
			}

			// Throwing an error is necessary here, because we must know about it in the sidebar component - only true/false is not enough.
			throw new Error('Navigation failed - there are unsaved changes');
		}

		if (this.eoResultService.forceCollectiveProcessingView || (component?.collectiveProcessingExecuting)) {
			throw new Error('Navigation failed - collective processing is executing or result must be confirmed');
		}

		return true;
	}

}
