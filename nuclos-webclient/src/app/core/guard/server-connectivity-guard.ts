import { Inject, Injectable } from '@angular/core';
import {
	ActivatedRouteSnapshot,
	CanActivate,
	CanActivateChild,
	Router,
	RouterStateSnapshot,
	UrlTree
} from '@angular/router';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { iif, Observable, of } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';

@Injectable()
export class ServerConnectivityGuard implements CanActivate, CanActivateChild {

	constructor(
		private nuclosConfigServer: NuclosConfigService,
		private router: Router
	) { }

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.checkServerConnectivityAndReturnErrorURLIfNecessary();
	}

	canActivateChild(
		childRoute: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return this.checkServerConnectivityAndReturnErrorURLIfNecessary();
	}

	/**
	 * Test server REST if available
	 * If route (children) can activate and return is true, if not return error url
	 * @private
	 */
	private checkServerConnectivityAndReturnErrorURLIfNecessary(): Observable<UrlTree | boolean> {
		return this.nuclosConfigServer.getServerStatus().pipe(
			take(1),
			switchMap((statusOfServer: boolean) => iif(() => statusOfServer, of(true), of(this.router.createUrlTree(['error/503']))))
		);
	}
}
