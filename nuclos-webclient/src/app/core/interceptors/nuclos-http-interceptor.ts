import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Logger } from '@modules/log/shared/logger';
import { NOT_ACCEPTABLE, SERVICE_UNAVAILABLE, UNAUTHORIZED } from 'http-status-codes';

import { Observable, Subject } from 'rxjs';
import { throwError as observableThrowError } from 'rxjs/internal/observable/throwError';
import { catchError, finalize } from 'rxjs/operators';

/**
 * NuclosHttpInterceptor
 *
 * Used to intercept all HTTP requests from application to handle errors and authentication.
 */
@Injectable({
	providedIn: 'root'
})
export class NuclosHttpInterceptor implements HttpInterceptor {
	private unauthorizedRequests = new Subject();
	private pendingRequests = new Map<number, string>();
	private requestsPendingSince: Date | undefined;
	private lastRequestID = 0;

	constructor(
		private $log: Logger,
		private router: Router,
	) {
		this.$log.debug('NuclosHttpInterceptor initialized');
	}

	intercept(req: HttpRequest<any>, next: HttpHandler):
		Observable<HttpEvent<any>> {

		const sendReq = req.clone({
			withCredentials: true,
			headers: this.updatedHeaders(req)
		});
		const requestID = this.nextRequestID();

		this.$log.debug('%s %s with headers %o and body %o',
			sendReq.method, sendReq.url, sendReq.headers, sendReq.body);
		this.requestStarted(requestID, sendReq.url);
		return next.handle(sendReq).pipe(
			catchError(err => {
				this.handleError(err, sendReq);
				return observableThrowError(err);
			}),
			finalize(() => this.requestFinished(requestID))
		);
	}

	getUnauthorizedRequests(): Observable<any> {
		return this.unauthorizedRequests;
	}

	nextRequestID() {
		return ++this.lastRequestID;
	}

	hasPendingRequest() {
		return this.pendingRequests.size > 0;
	}

	/**
	 * How long since there were no pending requests (in ms).
	 */
	getPendingRequestTime() {
		if (!this.requestsPendingSince) {
			return -1;
		}

		return Date.now() - this.requestsPendingSince.getTime();
	}

	requestStarted(requestID: number, url: string) {
		if (!this.requestsPendingSince) {
			this.requestsPendingSince = new Date();
		}

		this.pendingRequests.set(requestID, url);
	}

	requestFinished(requestID: number) {
		this.pendingRequests.delete(requestID);

		if (!this.hasPendingRequest()) {
			this.requestsPendingSince = undefined;
		}
	}

	private handleError(err, req: HttpRequest<any>) {
		if (err?.status === NOT_ACCEPTABLE || err?.status === 0 || err?.status === SERVICE_UNAVAILABLE) {
			this.$log.error(`Request ${req.url} failed`, err);
		} else if (err?.status === UNAUTHORIZED) {
			this.unauthorizedRequests.next(true);
		}
	}

	private updatedHeaders(req: HttpRequest<any>): HttpHeaders {
		if (req.headers.get('Content-Type') === undefined) {
			return req.body instanceof FormData ? req.headers.delete('Content-Type') : req.headers.set('Content-Type', 'application/json');
		}
		return req.headers;
	}
}
