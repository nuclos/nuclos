import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { FORBIDDEN, NOT_FOUND } from 'http-status-codes';
import * as StackTrace from 'stacktrace-js';
import { Logger } from '@modules/log/shared/logger';

@Injectable({
	providedIn: 'root'
})
export class GlobalErrorHandlerService implements ErrorHandler {

	private static IGNORE_ERRORS_FOR = ['/subBos/', '/user/forgot', '/forgotPassword'];

	constructor(
		private injector: Injector,
		private router: Router,
		private $log: Logger,
	) {
	}

	handleError(error?: any): void {
		this._handleError(error).catch(e => this.$log.error(e));
	}

	private async _handleError(error?: any) {
		if (!error) {
			this.$log.warn('Caught undefined error: %o', error);
			return;
		}

		if (this.isErrorIgnoredForUrl(error.url)) {
			return;
		}

		if (error.status === FORBIDDEN) {
			this.router.navigate(['/error', FORBIDDEN]);
		} else if (error.status === NOT_FOUND) {
			this.router.navigate(['/error', NOT_FOUND]);
		} else if (error.message && error.message.indexOf('Navigation failed') >= 0) {
			this.$log.debug('Navigation error - ignoring');
		} else if (error.status >= 500 || error.status === 0) {
			this.$log.error('There was a huge failure on server side', error);
		} else if (error.message && error.message.indexOf('ExpressionChangedAfterItHasBeenCheckedError') >= 0) {
			let stringifiedError = await this.getStackTrace(error);
			this.$log.error(error.message + '\n' + stringifiedError);
		} else if (error.alreadyHandled) {
			return;
		} else {
			// Unhandled error

			let stacktrace = await this.getStackTrace(error);

			this.$log.error(error);
			if (stacktrace) {
				this.$log.error(stacktrace);
			}
			this.writeErrorToBody(error);
		}
	}

	private async errorToString(error) {
		let stacktrace = await this.getStackTrace(error);

		if (typeof error === 'object') {
			error = JSON.stringify(error, () => {
				// handler for circular references
				// default is to discard this information
				const seen = new WeakSet();
				return (key, value) => {
					if (typeof value === 'object' && value !== null) {
						if (seen.has(value)) {
							return;
						}
						seen.add(value);
					}
					return value;
				};
			});
		}

		if (stacktrace) {
			error += '\n' + stacktrace;
		}

		return '' + error;
	}

	private writeErrorToBody(error: any) {
		// Write it to the property "error" of the body, so the tests can check it
		document.body.setAttribute('error', error);
	}

	private async getStackTrace(error: any) {
		// get the stack trace, lets grab the last 10 stacks only
		try {
			let stackframes = await StackTrace.fromError(error);

			return stackframes
				.map(sf => sf.toString())
				.join('\n');
		} catch (e) {
			this.$log.warn('Could not get StackTrace for error %o: %o', error, e);
		}

		return undefined;
	}

	/**
	 * TODO: Find a better way than a hard-coded list to ignore errors for certain URLs.
	 */
	private isErrorIgnoredForUrl(url: string) {
		return url && GlobalErrorHandlerService.IGNORE_ERRORS_FOR.find(
			value => url.indexOf(value) >= 0
		) !== undefined;
	}
}
