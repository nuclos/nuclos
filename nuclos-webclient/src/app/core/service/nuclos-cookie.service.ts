import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { StringUtils } from '@shared/string-utils';

@Injectable({
	providedIn: 'root'
})
export class NuclosCookieService {

	private cache = new Map<string, string>();

	constructor(
		private cookieService: CookieService
	) {
	}

	get(key: string) {
		return this.cookieService.get(key);
	}

	/**
	 * Uses a cache to not query Cookies every time.
	 */
	getCached(key: string) {
		let result;

		if (!this.cache.has(key)) {
			result = this.get(key);
			this.cache.set(key, result);
		}

		return this.cache.get(key)!;
	}

	getObject(key: string) {
		return JSON.parse(
			this.get(key)
		);
	}

	getObjectCached(key: string) {
		return JSON.parse(
			this.getCached(key)
		);
	}

	put(key: string, value: string, expires?: Date) {
		this.cookieService.set(
			key,
			value,
			expires,
			this.getPathDirectory()
		);

		this.cache.set(key, value);
	}

	putObject(key: string, value: object) {
		this.put(
			key,
			JSON.stringify(value)
		);
	}

	remove(key: string, path?: string) {
		this.cookieService.delete(key, path ? path : this.getPathDirectory());

		this.cache.delete(key);
	}

	private getPathDirectory() {
		let path = window.location.pathname;
		let dir = path.substring(0, path.lastIndexOf('/')) + '/';

		dir = StringUtils.regexReplaceAll(dir, '/{2,}', '/');

		return dir;
	}
}
