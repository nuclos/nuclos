import { Injectable } from '@angular/core';
import { EntityObjectEventService } from '@modules/entity-object-data/shared/entity-object-event.service';
import { EntityObject, SubEntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { DetailDialogComponent } from '@modules/entity-object/detail-dialog/detail-dialog.component';
import { DetailModalComponent } from '@modules/entity-object/detail-modal/detail-modal.component';
import {
	InputRequiredDialogComponent
} from '@modules/input-required/input-required-dialog/input-required-dialog.component';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DialogButton, IEntityObject, Logger } from '@nuclos/nuclos-addon-api';
import { IdFactoryService } from '@shared/service/id-factory.service';
import { from, Observable, of, Subject } from 'rxjs';
import { catchError, finalize, take } from 'rxjs/operators';
import { DialogOptions } from '../../data/schema/dialog.model';
import { DialogComponent } from '@shared/components/dialog/dialog.component';

@Injectable({
	providedIn: 'root'
})
export class NuclosDialogService {

	private lastDialogClosed: Subject<boolean>;

	/**
	 * open modalRefs
	 */
	private modalRefs: Map<string, NgbModalRef> = new Map();

	private activeElement: Element | null | undefined;

	constructor(
		private modalService: NgbModal,
		private idFactory: IdFactoryService,
		private eventService: EntityObjectEventService,
		private $log: Logger
	) {
		this.lastDialogClosed = new Subject<boolean>();
	}

	updateActiveElement(element: Element | null | undefined) {
		this.activeElement = element;
	}

	/**
	 * opens a alert modal
	 * @param options the options for the modal (title and message)
	 * @returns {Observable<>} an observable that is fulfilled when the user clicks ok button
	 */
	alert(options: DialogOptions): Observable<any | undefined> {
		const id = this.idFactory.getNextId();
		const buttonOptions = [
			new DialogButton(
				'button-ok',
				'webclient.button.ok',
				'btn-primary',
				() => {
					this.closeDialog(id);
				}),
		];

		options.ariaRole = 'alertdialog';
		options.zIndex = 2000 +  this.modalRefs.size;

		return this.open(
			id,
			DialogComponent,
			{
				'options': options,
				'buttonOptions': buttonOptions
			},
			{
				size: 'lg',
			}
		);
	}

	/**
	 * opens a display modal (similar to alert modal, but larger)
	 * @param options the options for the modal (title and message)
	 * @returns {Observable<any>} an observable that is fulfilled when the user clicks ok button
	 */
	display(options: DialogOptions): Observable<any | undefined> {
		const id = this.idFactory.getNextId();
		const buttonOptions = [
			new DialogButton(
				'button-ok',
				'webclient.button.ok',
				'btn-primary',
				() => {
					this.closeDialog(id);
				}),
		];

		return this.open(
			id,
			DialogComponent,
			{
				'options': options,
				'buttonOptions': buttonOptions
			},
			{
				size: 'lg'
			}
		);
	}


	/**
	 * opens a custom modal/dialog with given content in templateRefOrType
	 * @param templateRefOrType - Content to display in modal
	 * @param context - context variables you may need in your template
	 * @param options - options for displaying modal
	 * @param sourceEvent - Event which triggered this modal
	 */
	custom(
		templateRefOrType: any,
		context?: any,
		options?: NgbModalOptions,
		sourceEvent?: EventTarget | null
	): Observable<any | undefined> {
		const id = this.idFactory.getNextId();

		return this.open(
			id,
			templateRefOrType,
			{
				...context,
				'dialogId': id
			},
			options,
			sourceEvent
		);
	}

	/**
	 * opens a confirm modal
	 * @param options the options for the modal (title and message)
	 * @param sourceEvent the event element triggering this dialog
	 * @returns {Observable<any>} an observable that is fulfilled when the user chooses to confirm, and rejected when
	 * the user chooses not to confirm, or closes the modal
	 */
	confirm(
		options: DialogOptions,
		sourceEvent?: EventTarget | null
	): Observable<boolean | undefined> {
		const id = this.idFactory.getNextId();
		options.dialogId = id;
		const buttonOptions = [
			new DialogButton(
				'button-ok',
				'webclient.button.yes',
				'btn-primary',
				() => {
					this.closeDialog(id, true);
				}),

			new DialogButton(
				'button-cancel',
				'webclient.button.no',
				'btn-warning',
				() => {
					this.closeDialog(id, false);
				}),
		];

		return this.open(
			id,
			DialogComponent,
			{
				'options': options,
				'buttonOptions': buttonOptions
			},
			{
				size: 'lg'
			},
			sourceEvent
		);
	}


	/*
	 * opens a modal dialog
	 * interaction should be handled via ButtonOption.callback
	 * @param {string} title
	 * @param {string} message
	 * @param {DialogButton[]} buttonOptions
	 * @param {NgbModalOptions} modalOptions
	 */
	openDialog(
		title: string,
		message: string,
		buttonOptions: DialogButton[],
		modalOptions?: NgbModalOptions
	): Observable<any | undefined> {

		const id = this.idFactory.getNextId();
		let options: DialogOptions = {
			title: title,
			message: message,
			dialogId: id
		};

		return this.open(
			id,
			DialogComponent,
			{
				'options': options,
				'buttonOptions': buttonOptions,
			},
			modalOptions
		);

	}

	hasOpenDialog() {
		return this.modalService.hasOpenModals();
	}

	/**
	 * close dialog if not already happened
	 * @param dialogId
	 * @param result
	 */
	closeDialog(dialogId: any, result?: any) {
		const modalRef = this.getDialog(dialogId);
		if (modalRef && dialogId) {
			modalRef.close(result);
			this.modalRefs.delete(dialogId);
			if (this.modalRefs.size === 0) {
				this.lastDialogClosed.next(true);
			}

			this.eventService.emitEoInDialog(undefined);
		}
	}

	getDialog(dialogId: any): NgbModalRef | undefined {
		if (dialogId && this.modalRefs.has(dialogId)) {
			return this.modalRefs.get(dialogId);
		}
		return undefined;
	}

	closeCurrentModal(result?: any) {
		const modalsWithDialogId = Array.from(this.modalRefs.values()).map((value) => this.mapToDialogId(value));
		const lastModal = modalsWithDialogId.pop();
		if (lastModal) {
			this.closeDialog(lastModal, result);
		}
	}

	openEoInDialog(
		eo: IEntityObject,
		sourceEo: IEntityObject | undefined = undefined,
		dialogTitle: string,
		dialogWidth: number,
		dialogHeight: number,
		sourceEvent?: EventTarget | null
	): Observable<any> {

		return this.custom(
			DetailDialogComponent,
			{
				'sourceEo': sourceEo as EntityObject,
				'eo': eo as EntityObject,
				'title': dialogTitle,
				'width': dialogWidth,
				'height': dialogHeight
			},
			{
				size: 'sm',
				windowClass: 'custom-modal-window'
			},
			sourceEvent
		);
	}

	openEoInModal(
		eo: IEntityObject,
		sourceEo: IEntityObject | undefined = undefined,
		independentContext = false,
		sourceEvent?: EventTarget | null
	): Observable<any> {
		return this.custom(
			DetailModalComponent,
			{
				'independentContext': independentContext,
				'sourceEo': sourceEo as unknown as EntityObject,
				'eo': eo
			},
			{
				size: 'lg',
				windowClass: 'fullsize-modal-window'
			},
			sourceEvent
		);
	}

	observeLastDialogClosed(): Subject<boolean> {
		return this.lastDialogClosed;
	}

	/**
	 * opens ngbmodal service window
	 * @private
	 */
	private open(
		id: any,
		templateRefOrType: any,
		context?: any,
		modalOptions?: NgbModalOptions,
		sourceEvent?: EventTarget | null
	): Observable<any | undefined> {

		const modalRef = this.modalService.open(templateRefOrType,
			{
				...modalOptions,
				centered: true
			}
		);

		for (let x in context) {
			if (context.hasOwnProperty(x)) {
				if (modalRef.componentInstance) {
					modalRef.componentInstance[x] = context[x];
				}
			}
		}

		this.modalRefs.set(id, modalRef);
		this.setOptionalARIARole(context?.options);
		modalRef.shown.pipe(
			take(1)
		).subscribe(() => {
			if (templateRefOrType !== InputRequiredDialogComponent) {
				// input required has its own logic
				this.focusConfirmationButton();
			}
		});

		if (context.hasOwnProperty('eo')) {
			this.eventService.emitEoInDialog(context['eo']);
		}

		return from(modalRef.result).pipe(
			catchError(() => {
				return of(void 0);
			}),
			finalize(() => {
				this.closeDialog(id);
				this.putFocusBackTo(sourceEvent ?? this.activeElement);
				this.activeElement = undefined;
			})
		);
	}

	private focusConfirmationButton() {
		setTimeout(() => {
			$('ngb-modal-window')
				.last()
				.find('#button-ok')
				.first()
				.trigger('focus');
		});
	}

	private setOptionalARIARole(options: DialogOptions | undefined) {
		if (options?.ariaRole !== undefined) {
			// next GUI tick so DOM was already rendered
			setTimeout(() => {
				$('ngb-modal-window').last().get(0)?.setAttribute('role', options.ariaRole!);
			});
		}
	}

	private mapToDialogId(value: NgbModalRef): number | undefined {
		let dialogId: any;
		if (value.componentInstance?.hasOwnProperty('dialogId')) {
			dialogId = value.componentInstance.dialogId;
		} else {
			dialogId = value.componentInstance?.options.dialogId;
		}
		if (!dialogId) {
			for (const [id, modalRef] of this.modalRefs) {
				if (value === modalRef) {
					this.modalRefs.delete(id);
					break;
				}
			}
		}

		return dialogId;
	}

	private putFocusBackTo(element: EventTarget | null | undefined) {
		setTimeout(() => {
			if (element !== null && element !== undefined) {
				$(element).first().trigger('focus');
			}
		});
	}
}
