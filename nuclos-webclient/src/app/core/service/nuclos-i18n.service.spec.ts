/* tslint:disable:no-unused-variable */

import { inject, TestBed } from '@angular/core/testing';
import { NuclosI18nService } from './nuclos-i18n.service';

xdescribe('Service: NuclosI18n', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [NuclosI18nService]
		});
	});

	it('should ...', inject([NuclosI18nService], (service: NuclosI18nService) => {
		expect(service).toBeTruthy();
	}));
});
