import { Injectable } from '@angular/core';
import { ISecurityService, UserAction } from '@nuclos/nuclos-addon-api';
import { AuthenticationService } from '../../modules/authentication';

@Injectable({
	providedIn: 'root'
})
export class SecurityService implements ISecurityService {

	constructor(
		private authService: AuthenticationService,
	) {
	}

	isUserActionAllowed(action: UserAction): boolean {
		return this.authService.isActionAllowed(action);
	}
}
