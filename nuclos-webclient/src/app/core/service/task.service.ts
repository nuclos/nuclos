import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPreference, IPreferenceContent } from '@nuclos/nuclos-addon-api';
import { FqnService } from '@shared/service/fqn.service';
import { EMPTY, forkJoin, Observable, of, of as observableOf } from 'rxjs';

import { catchError, flatMap, map, mergeMap, take } from 'rxjs/operators';
import { TaskList } from '../../data/schema/task-list';
import { NuclosCacheService } from '../../modules/cache/shared/nuclos-cache.service';
import { EntityMeta, EntityMetaData } from '../../modules/entity-object-data/shared/bo-view.model';
import { DataService, LoadContext, LoadInnerContext } from '../../modules/entity-object-data/shared/data.service';
import { EntityObjectService } from '../../modules/entity-object-data/shared/entity-object.service';
import { MetaService } from '../../modules/entity-object-data/shared/meta.service';
import { ResultParams } from '../../modules/entity-object-data/shared/result-params';
import { Logger } from '../../modules/log/shared/logger';
import {
	AttributeSelectionContent,
	Preference, SearchtemplatePreferenceContent
} from '../../modules/preferences/preferences.model';
import { PreferencesService } from '../../modules/preferences/preferences.service';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';

@Injectable({
	providedIn: 'root'
})
export class TaskService {

	constructor(
		private config: NuclosConfigService,
		private cache: NuclosCacheService,
		private httpService: HttpClient,
		private $log: Logger,
		private metaService: MetaService,
		private prefService: PreferencesService,
		// TODO: Refactor to some EO-data service
		private dataService: DataService,
		// TODO: Only injected here to make sure it is instantiated
		private eoService: EntityObjectService,
		private fqnService: FqnService,
	) {
	}

	/**
	 * Does not return complete entity meta data!
	 */
	getTaskListDefinitions(): Observable<EntityMetaData[]> {
		const restUri = this.config.getRestHost() + '/meta/tasklists';
		return this.httpService.get<EntityMetaData[]>(restUri);
	}

	getTaskListDefinitionByTaskMetaId(taskMetaId: string): Observable<EntityMeta> {
		return this.getTaskListDefinitions().pipe(map(
			metas => {
				let taskListMeta = metas.find(
					meta => meta.taskMetaId === taskMetaId
				);

				if (!taskListMeta) {
					throw new Error('Could not find task list meta for taskMetaId=' + taskMetaId);
				}

				return new EntityMeta(taskListMeta, this.fqnService);
			}
		));
	}

	getTaskListDefinition(taskListFqn: string): Observable<EntityMeta> {
		return this.getTaskListDefinitions().pipe(map(
			metas => {
				let taskListMeta = metas.find(
					meta => meta.boMetaId === taskListFqn
				);

				if (!taskListMeta) {
					throw new Error('Could not find task list meta for ' + taskListFqn);
				}

				return new EntityMeta(taskListMeta, this.fqnService);
			}
		));
	}

	getTaskData(
		taskList: TaskList,
		attributeSelection?: Preference<AttributeSelectionContent>,
	) {
		return forkJoin(
			[
				this.getSearchtemplatePreferenceIfAny(taskList.searchfilter),
				this.getTaskListMeta(taskList)
			]
		).pipe(
			mergeMap(observedValues => {
				let searchtemplatePreference = observedValues[0] as IPreference<SearchtemplatePreferenceContent>;
				let meta = observedValues[1] as EntityMeta;
				let params: ResultParams = {
					offset: 0,
					chunkSize: 500,
					countTotal: false
				};

				let notEmptyAttributeSelection: Preference<AttributeSelectionContent>;
				if (attributeSelection === undefined
					|| attributeSelection.content.columns === undefined
					|| attributeSelection.content.columns.length === 0) {
					notEmptyAttributeSelection = new Preference(
						'tasklist-table',
						meta.getEntityClassId());
					notEmptyAttributeSelection.content.columns = [];
					// load all columns from meta data
					meta.getAttributes().forEach(attributeMeta => {
						notEmptyAttributeSelection.content.columns.push({
							name: attributeMeta.getName(),
							boAttrId: attributeMeta.getAttributeID(),
							system: attributeMeta.isSystemAttribute(),
							selected: true,
						});
					});
				} else {
					notEmptyAttributeSelection = attributeSelection;
				}

				let context: LoadContext = {
					meta: meta,
					vlpId: undefined,
					vlpParams: undefined,
					attributeSelection: attributeSelection,
					searchtemplatePreference: searchtemplatePreference,
					resultParams: params
				};
				return this.dataService.loadEoList(context, this.buildInnerContext(context));
			})
		);
	}

	/**
	 * Returns a complete entity meta for the task list entity.
	 */
	getTaskListMeta(taskList: TaskList) {
		return this.getTaskListMetaByFqn(taskList.entityClassId);
	}

	getTaskListMetaByFqn(taskListFqn: string) {
		return this.metaService.getBoMeta(taskListFqn);
	}

	/**
	 * TODO: Should be refactored later to some kind of push notification from the server (Websocket).
	 */
	getTaskCountSince(taskList: TaskList, since: Date) {
		return this.getTaskListMeta(taskList).pipe(
			mergeMap(meta => {
				// TODO: This method only works for search filter based task lists at the moment
				if (!taskList.searchfilter) {
					return observableOf(0);
				}

				let params: ResultParams = {
					offset: 0,
					chunkSize: 0,
					countTotal: true,
					searchFilterId: taskList.searchfilter
				};

				return this.metaService.getEntityMeta(meta.getEntityClassId()).pipe(
					mergeMap(entityMeta =>
						this.dataService.loadEoData(
							entityMeta,
							params,
							{
								where: {
									aliases: {},
									clause: entityMeta.getAttribute('changedAt').boAttrId + ' > \'' + since.toISOString() + '\''
								}
							}
						).pipe(
							map(
								data => data?.total ?? 0
							),
							catchError(e => {
								this.$log.warn('Failed to get count for task list: %o', e);
								return EMPTY;
							}),
						)
					)
				);
			})
		);
	}

	private buildInnerContext(context: LoadContext): LoadInnerContext {
		let columns = this.dataService.buildColumns(context.meta, context.attributeSelection, context.resultParams?.sidebarView);

		let sort = '';
		if (context.attributeSelection && context.attributeSelection.content.columns) {
			sort = this.dataService.getSortString(context.attributeSelection.content.columns);
		}

		let innerContext: LoadInnerContext = {
			...context,
			columns: columns,
			sort: sort,
			search: '',
			resultParams:
				context.resultParams === undefined ? ResultParams.DEFAULT : context.resultParams
		};

		return innerContext;
	}

	private getSearchtemplatePreferenceIfAny(prefId: string | undefined): Observable<IPreference<IPreferenceContent> | null> {
		if (prefId) {
			return this.prefService.getPreference(prefId);
		}
		return of(null);
	}
}
