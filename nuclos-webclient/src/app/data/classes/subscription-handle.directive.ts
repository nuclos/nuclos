import { Directive, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { UnsubscribeNotifier } from '../interfaces/unsubscribe-notifier';

@Directive()
export class SubscriptionHandleDirective implements OnDestroy, UnsubscribeNotifier {
	public unsubscribe$ = new Subject<boolean>();

	public ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}
}
