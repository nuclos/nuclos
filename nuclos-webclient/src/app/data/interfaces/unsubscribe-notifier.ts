import { Subject } from 'rxjs';

export interface UnsubscribeNotifier {
	unsubscribe$: Subject<boolean>;
}
