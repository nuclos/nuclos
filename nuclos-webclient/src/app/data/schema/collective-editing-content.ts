import { EntityAttrMeta } from '../../modules/entity-object-data/shared/bo-view.model';

export class CollectiveEditingContent {

	private attributes: Array<EntityAttrMeta> = [];
	private editAttributes: Array<EntityAttrMeta> = [];
	private deletedEditAttributes: Array<EntityAttrMeta> = [];
	private result: EditSelectionResult = {attributes: []};
	private resultAttributes: EditAttributeMap[] = [];
	private deletedResultAttributes: EditAttributeMap[] = [];
	private links: CollectiveProcessingActionLinks;

	public getAvailableAttributes(): Array<EntityAttrMeta> {
		return this.attributes;
	}

	public updateAvailableAttributes(attributesUpdate: Array<EntityAttrMeta>) {
		if (this.attributes.length === 0) {
			this.attributes = attributesUpdate;
		} else {
			// possibly multi selection result got updated so we need to modify the whole attributes
			let resultingAttrsForSelection = attributesUpdate;

			for (let i = 0; i < this.editAttributes.length; i++) {
				let attr = this.editAttributes[i];
				let element = resultingAttrsForSelection.find(v => v.getAttributeID() === attr.getAttributeID());
				if (element === undefined) {
					// edited attributes is not present in current update list anymore
					// save it in deletedEdit for maybe later reAdd
					this.removeEditAttribute(attr.getName()!);
				} else {
					resultingAttrsForSelection.splice(resultingAttrsForSelection.indexOf(element), 1);
				}
			}

			this.attributes = resultingAttrsForSelection;

			let deleted = Array.from(this.deletedEditAttributes);
			for (let j = 0; j < deleted.length; j++) {
				let attr = deleted[j];
				if (resultingAttrsForSelection.find(v => v.getAttributeID() === attr.getAttributeID())) {
					// edited attributes is not present in current update list anymore
					// save it in deletedEdit for maybe later reAdd
					this.addEditAttribute(attr.getName()!);
				}
			}
		}
	}

	public hasEditedValues(): boolean {
		return this.editAttributes.length > 0 || this.deletedEditAttributes.length > 0;
	}

	public getEditingAttributes(): Array<EntityAttrMeta> {
		return this.editAttributes;
	}

	public getDeletedEditingAttributes(): Array<EntityAttrMeta> {
		return this.deletedEditAttributes;
	}

	public addEditAttribute(addAttribute: string) {
		let attrMeta = this.attributes.find(v => addAttribute === v.getName());
		if (attrMeta) {
			// push attribute to editing fields and remove it from initial selection dropdown
			let deleted = this.deletedEditAttributes.find(v => addAttribute === v.getName());
			if (deleted) {
				let deletedValue = this.deletedResultAttributes.find(r => r.refId === deleted!.getAttributeID());
				if (deletedValue) {
					this.resultAttributes.push(deletedValue);
					this.deletedResultAttributes.splice(this.deletedResultAttributes.indexOf(deletedValue), 1);
				}
				this.deletedEditAttributes.splice(this.deletedEditAttributes.indexOf(deleted), 1);
			}
			if (!this.resultAttributes.find(r => r.refId! === attrMeta?.getAttributeID())) {
				this.resultAttributes.push({refId: attrMeta?.getAttributeID(), value: null});
			}
			this.editAttributes.push(attrMeta);
			this.attributes.splice(this.attributes.indexOf(attrMeta), 1);
		}
	}

	public removeEditAttribute(removeAttribute: string) {
		let attrMeta = this.editAttributes.find(v => removeAttribute === v.getName());
		if (attrMeta) {
			let currentValue = this.resultAttributes.find(r => r.refId! === attrMeta!.getAttributeID());
			if (currentValue) {
				this.resultAttributes.splice(this.resultAttributes.indexOf(currentValue), 1);
				this.deletedResultAttributes.push(currentValue);
			}

			this.deletedEditAttributes.push(attrMeta);
			this.attributes.push(attrMeta);
			this.editAttributes.splice(this.editAttributes.indexOf(attrMeta), 1);
		}
	}

	public updateAttributeValue(attributeRef: string, value: any) {
		let currentValue = this.resultAttributes.find(r => r.refId! === attributeRef);
		if (currentValue) {
			currentValue.value = value;
		} else {
			this.resultAttributes.push({refId: attributeRef, value: value});
		}
	}

	public getAttributeValue(attributeRef: string): any {
		let currentValue = this.resultAttributes.find(r => r.refId! === attributeRef);
		if (currentValue) {
			return currentValue.value;
		}
		return '';
	}

	public getDeletedAttributeValue(attributeRef: string): any {
		let currentValue = this.deletedResultAttributes.find(r => r.refId! === attributeRef);
		if (currentValue) {
			return currentValue.value;
		}
		return '';
	}

	public setLinks(links: CollectiveProcessingActionLinks) {
		this.links = links;
	}

	public getLinks(): CollectiveProcessingActionLinks {
		return this.links;
	}

	public getResult(): EditSelectionResult {
		this.result.attributes = this.resultAttributes;
		return this.result;
	}
}
