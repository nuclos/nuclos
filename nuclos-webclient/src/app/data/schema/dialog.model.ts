export class DialogOptions {

	/**
	 * id used for getting modal reference
	 */
	dialogId?: any;

	/**
	 * The title of the confirmation modalRef
	 */
	title: string;

	/**
	 * The message in the confirmation modalRef
	 */
	message: string;

	/**
	 * Optional ARIA role for dialog modal
	 */
	ariaRole?: string;

	zIndex?: number;
}
