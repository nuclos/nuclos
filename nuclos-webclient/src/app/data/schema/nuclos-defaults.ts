export namespace NuclosDefaults {
	/**
	 * Maximum number of results to load for dropdowns (combobox, LOV).
	 * For comboboxes more results must be loaded than are actually shown,
	 * because comboboxes are not refreshed every time and must therefore
	 * hold more results in the background.
	 */
	export const DROPDOWN_LOAD_RESULT_LIMIT = 10000;

	/**
	 * Maximum number of results to be shown when a dropdown is expanded.
	 */
	export const DROPDOWN_SHOW_RESULT_LIMIT = 100;
}
