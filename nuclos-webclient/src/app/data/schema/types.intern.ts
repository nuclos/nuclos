export type Options = {
	description?: string;
	keys: string;
	modifier?: 'CTRL' | 'ALT' | 'CTRL_SHIFT' | 'ALT_SHIFT' | 'CTRL_ALT' | 'CTRL_ALT_SHIFT';
	target?: Element;
	webComponent?: WebComponent;
};
