import { HttpParams } from '@angular/common/http';
import {
	AfterContentInit,
	Directive,
	ElementRef,
	Injector,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { ClickOutsideService } from '@modules/click-outside/click-outside.service';
import { EntityAttrMeta, LovEntry } from '@modules/entity-object-data/shared/bo-view.model';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { LovDataService } from '@modules/entity-object-data/shared/lov-data.service';
import { LovSearchConfig } from '@modules/entity-object-data/shared/lov-search-config';
import { ReferenceTargetService } from '@modules/entity-object-data/shared/reference-target.service';
import { Logger } from '@modules/log/shared/logger';
import { IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { ObjectUtils } from '@shared/object-utils';
import { FqnService } from '@shared/service/fqn.service';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { AutoComplete } from 'primeng/autocomplete';
import { iif, Observable, of, Subject, Subscription } from 'rxjs';
import { finalize, switchMap, take, takeUntil } from 'rxjs/operators';
import { NuclosDefaults } from '../../data/schema/nuclos-defaults';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';
import { ComboboxUtils } from '../shared/combobox-utils';
import { LovHandler } from './lov-handler';

/**
 * TODO: Use DropdownComponent here instead of PrimeNG Autocomplete.
 */
@Directive()
export abstract class AbstractLovComponentDirective<T extends WebInputComponent>
	extends AbstractInputComponentDirective<T>
	implements OnInit, OnDestroy, OnChanges, AfterContentInit {
	@Input() handler: LovHandler = LovHandler.DUMMY;

	@ViewChild('autoComplete') autoComplete: AutoComplete | undefined;

	lovEntries: LovEntry[] = [];
	hover;
	displayValue?: LovEntry;

	protected attributeMeta: EntityAttrMeta | undefined;

	protected unsubscribe$ = new Subject<boolean>();

	private inputModified = false;

	private keydownEventRegistered = false;

	/**
	 * Fake dropdown entry to display the current search string.
	 */
	private searchValue?: LovEntry;

	private resultSubscription: Subscription;

	private eoListener: IEntityObjectEventListener = {
		refreshVlp: (
			entityObject: EntityObject,
			attributeName: string,
			onlyIfDefaultField: boolean
		) => this.refreshEntries(attributeName, onlyIfDefaultField),
		afterAttributeChange: (
			entityObject: EntityObject,
			attributeName: string,
			oldValue: any,
			newValue: any
		) => {
			this.initDisplayValue();
			if (attributeName === this.getName() && !this.isTextfieldCombobox()) {
				this.selectDropdownOption(newValue);
			}
		},
		afterValidationChange: () => this.blur(),
		afterSave: () => this.blur(),
	};

	constructor(
		protected lovDataService: LovDataService,
		protected injector: Injector,
		private fqnService: FqnService,
		protected elementRef: ElementRef,
		private referenceTargetService: ReferenceTargetService,
		protected nuclosHotkeysService: NuclosHotkeysService,
		protected i18nService: NuclosI18nService,
		private $log: Logger
	) {
		super(injector);
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngAfterContentInit() {
		// workaround for ARIA role set NUCLOS-9835
		setTimeout(() => {
			this.autoComplete?.inputEL?.nativeElement?.setAttribute('role', 'combobox');
		});
	}

	ngOnInit() {
		let name = this.getName();
		if (name) {
			this.eo.getAttributeMeta(name).pipe(take(1)).subscribe(meta => {
				this.attributeMeta = meta;
			});
		}

		this.eo.addListener(this.eoListener);

		// when using the dropdown in the primeng p-autocomplete component the input element will be focused when clicking on the dropdown
		// this behaviour will always set back the focus to the input when trying to leave the input with a keyboard tab
		// disabling the dropdown button fixes this
		setTimeout(() => {
			let dropdownButton = this.elementRef.nativeElement.querySelector('button');
			if (dropdownButton) {
				$(dropdownButton).prop('tabindex', -1);
			}
		}, 0);

		// use timeout to let shortcut registration run in next GUI Loop
		setTimeout(() => {
			this.registerShortCuts();
		});
		this.initDisplayValue();
	}

	getStyle(): any {
		let style = super.getStyle();

		delete style['height'];
		delete style['width'];
		delete style['min-height'];
		delete style['min-width'];

		return style;
	}

	setFocus() {
		if (this.autoComplete) {
			$(this.autoComplete.inputEL.nativeElement).focus();
		}
	}

	ngOnChanges(changes: SimpleChanges) {
		super.ngOnChanges(changes);
		let eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				eoChange.previousValue.removeListener(this.eoListener);
			}
			if (eoChange.currentValue) {
				eoChange.currentValue.addListener(this.eoListener);
			}
			if (eoChange.previousValue && eoChange.currentValue) {
				this.selectDropdownOption(this.getValue());
			}
		}
	}

	isWritable(): boolean {
		if (this.handler.isEnabled) {
			return this.handler.isEnabled();
		}

		return super.isWritable();
	}

	getLovSearchConfig(search: string): LovSearchConfig {
		return {
			attributeMeta: this.attributeMeta!,
			eo: this.eo,
			quickSearchInput: search,
			vlp: this.webComponent.valuelistProvider,
			resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT,
			mandatorId: this.getEntityObject()
				.getRootEo()
				.getMandatorId()
		};
	}

	getVlpId(): string | undefined {
		if (this.getVlp()) {
			return this.getVlp()!.value;
		}
		return undefined;
	}

	getVlp(): WebValuelistProvider | undefined {
		return this.webComponent.valuelistProvider;
	}

	getVlpParams(): HttpParams {
		return this.lovDataService.getLovParams(this.getLovSearchConfig(''));
	}

	getLovHandler(): LovHandler {
		return this.handler;
	}

	isLoading(): boolean {
		return this.resultSubscription && !this.resultSubscription.closed;
	}

	/**
	 * fix for https://github.com/primefaces/primeng/issues/745
	 */
	showAllEntries() {
		if (!this.isWritable()) {
			return;
		}

		this.subscribeToNewEntries(
			this.handler.loadEntries().pipe(finalize(() => this.highlightSelectedOption()))
		);
	}

	autocompleteSearch(search: string) {
		search = this.getInputValue();

		this.inputModified = true;
		this.searchValue = {name: search, id: null};
		if (this.autoComplete) {
			this.autoComplete.highlightOption = undefined;
		}

		this.subscribeToNewEntries(
			this.filter(search).pipe(finalize(() => this.highlightUniqueMatch()))
		);
	}

	initDisplayValue() {
		this.displayValue = this.getValue();
	}

	/**
	 * The current search value should be always visible in the input field,
	 * therefore it is returned first.
	 */
	getValue(): any {
		if (this.attributeMeta && !this.attributeMeta.isReference()) {
			return this.getValueForTextfield();
		}

		if (this.searchValue) {
			return this.searchValue;
		}

		let result = super.getValue();

		if (!result || !result.name) {
			if (this.inputModified && this.hasFocus()) {
				return this.getInputValue();
			}
		}

		return result;
	}

	openReference(createNewEntry = false) {
		let field = this.eo.getAttribute(this.webComponent.name);
		if (!field && this.attributeMeta === undefined) {
			return;
		}

		let fieldName = this.fqnService.getShortAttributeName(
			this.eo.getEntityClassId(),
			this.attributeMeta!.getAttributeID()
		);
		if (fieldName) {
			let referenceBo = createNewEntry ? {id: 'new'} : this.eo.getAttribute(fieldName);

			if (referenceBo && referenceBo.id) {
				let href =
					location.href.substring(0, location.href.indexOf('/#/')) +
					'/#/view/' +
					this.attributeMeta!.getReferencedEntityClassId() +
					'/' +
					referenceBo.id +
					'?expand=true';
				if (field.properties && field.properties.popupparameter) {
					window.open(href, '', field.properties.popupparameter);
				} else {
					window.open(href);
				}
			}
		}
	}

	selectDropdownOption(value) {
		this.searchValue = undefined;
		this.displayValue = value;

		this.setValue(value);

		// TODO: Input value should be updated automatically by the autocomplete
		if (value && this.autoComplete) {
			$(this.autoComplete.inputEL.nativeElement).val(value.name);
		}

		if (this.handler.entrySelected) {
			this.handler.entrySelected(value);
		}

		if (this.autoComplete && this.autoComplete.overlayVisible) {
			// sometimes it seems to remain open (during testing)
			this.autoComplete.hide();
		}
	}

	/**
	 * Validates the given value and writes it to the model.
	 */
	setValue(value: any): any {
		this.inputModified = false;
		if (this.attributeMeta && !this.attributeMeta.isReference()) {
			if (value && value.name) {
				value = value.name;
			}
		}

		return super.setValue(value);
	}

	/**
	 * Only for non-reference fields should the text input be written to the model immediately.
	 * For reference field, the model is only written after an entry is selected from the dropdown.
	 */
	setValueImmediatelyIfNoReference(value: any) {
		if (this.isTextfieldCombobox() && this.isInsertable()) {
			this.setValue(value);

			if (this.handler.setValue) {
				this.handler.setValue(value);
			}
		}
	}

	toggleResultPanelAndLoad($event?) {
		if ($event) {
			// We handle the dropdown panel. Prevent the event from reaching the autocomplete component:

			let eventObject = $event;

			// The Autocomplete component wraps the original MouseEvent
			if (typeof eventObject.originalEvent === 'object') {
				eventObject = eventObject.originalEvent;
			}

			if (typeof eventObject.preventDefault === 'function') {
				eventObject.preventDefault();
			}
			if (typeof eventObject.stopPropagation === 'function') {
				eventObject.stopPropagation();
			}
		}

		if (this.autoComplete && this.autoComplete.overlayVisible) {
			this.autoComplete.hide();
		} else {
			this.showAllEntries();
		}

		// select lov input text
		if (this.autoComplete) {
			let textInput = this.autoComplete.inputEL.nativeElement;
			if (textInput.selectionEnd - textInput.selectionStart === 0) {
				textInput.setSelectionRange(0, textInput.value.length);
			}
		}
	}

	dropdownClick($event) {
		if (this.autoComplete && this.autoComplete.overlayVisible) {
			this.autoComplete.hide();
			return;
		}

		this.toggleResultPanelAndLoad($event);
		if (this.autoComplete) {
			this.autoComplete.handleDropdownClick($event);
		}
	}

	keydown($event: KeyboardEvent) {
		if ($event.key === 'Enter' && !this.autoComplete?.overlayVisible) {
			this.toggleResultPanelAndLoad($event);
		}
	}

	keyup($event: KeyboardEvent) {
		if ($event.key === 'ArrowUp' || $event.key === 'ArrowDown') {
			return;
		}

		if ($event.shiftKey && $event.altKey && ($event.key === 'a' || $event.key === 'e' || $event.key === 'q')) {
			return;
		}

		// The autocomplete component does not trigger a search, if there is no input
		if (this.autoComplete && $(this.autoComplete.inputEL.nativeElement).val() === '' && $event.key !== 'Escape') {
			this.clearModelValue();
		}

		if ($event.key === 'Escape') {
			this.autoComplete?.hide();
			$event.stopPropagation();
			$event.preventDefault();
		}
	}

	addReferenceTarget() {
		if (this.getLovHandler().stopEditing) {
			// @ts-ignore
			this.getLovHandler().stopEditing();
		}
		this.referenceTargetService.canAddReference(this.eo, this.attributeMeta)
			.pipe(
				take(1),
				switchMap((canAdd) =>
					iif(
						() => canAdd,
						this.referenceTargetService.targetReference(
							this.eo,
							this.attributeMeta,
							'new',
							true,
							this.getVlpId(),
							undefined,
							false,
							this.getAdvancedProperty('popupparameter')),
						of(void 0)
					)
				)
			)
			.subscribe();
	}

	editReferenceTarget() {
		if (this.getLovHandler().stopEditing) {
			// @ts-ignore
			this.getLovHandler().stopEditing();
		}
		this.referenceTargetService.canEditReference(this.eo, this.attributeMeta, this.getValue())
			.pipe(
				take(1),
				switchMap((canEdit) =>
					iif(
						() => canEdit,
						this.referenceTargetService.targetReference(
							this.eo,
							this.attributeMeta,
							this.getValue().id,
							true,
							this.getVlpId(),
							undefined,
							true,
							this.getAdvancedProperty('popupparameter')),
						of(void 0)
					)
				)
			)
			.subscribe(() => this.initDisplayValue());
	}

	searchReferenceTarget() {
		if (this.getLovHandler().stopEditing) {
			// @ts-ignore
			this.getLovHandler().stopEditing();
		}
		this.referenceTargetService.canOpenReference(this.eo, this.attributeMeta)
			.pipe(
				take(1),
				switchMap((canOpen) =>
					iif(
						() => canOpen,
						this.referenceTargetService.targetReference(
							this.eo,
							this.attributeMeta,
							undefined,
							false,
							this.getVlpId(),
							// @ts-ignore
							this.getLovHandler().getVlpParams ? this.getLovHandler().getVlpParams() : undefined,
							false,
							this.getAdvancedProperty('popupparameter')),
						of(void 0)
					)
				)
			)
			.subscribe();
	}

	refreshReferenceTarget() {
		if (this.getLovHandler().stopEditing) {
			// @ts-ignore
			this.getLovHandler().stopEditing();
		}
		if (this.getLovHandler().refreshEntries) {
			// @ts-ignore
			this.getLovHandler().refreshEntries();
		}
	}


	getAttributeMeta(): EntityAttrMeta | undefined {
		return this.attributeMeta;
	}

	getAutocomplete() {
		return this.autoComplete;
	}

	outsideClick(target: EventTarget) {
		if (
			target !== this.elementRef.nativeElement &&
			this.autoComplete && !this.autoComplete.el.nativeElement.contains(target)
		) {
			this.blur();
			this.autoComplete?.hide();
		}
	}

	blur() {
		this.searchValue = undefined;
		if (this.inputModified && !this.autoComplete?.overlayVisible) {
			// if we typed something restore old value
			this.saveInputEl();
		}
		this.initDisplayValue();
		const isNotInsertable = !this.isInsertable();
		const isReferencingAttribute = this.attributeMeta && this.attributeMeta.isReference();
		if (isNotInsertable || isReferencingAttribute) {
			this.resetDisplayValue();
		}
	}

	resetDisplayValue() {
		if (!this.lovEntries.some(entry => ObjectUtils.isEqual(entry, this.displayValue))) {
			this.displayValue = this.getValue();
			this.autoComplete?.writeValue(this.displayValue);
		}
	}

	getWidth() {
		return this.handler.getWidth && this.handler.getWidth();
	}

	getHeight() {
		return this.handler.getHeight && this.handler.getHeight();
	}

	refreshReferenceClick() {
		if (this.handler.refreshReferenceClick) {
			this.handler.refreshReferenceClick();
		}
	}

	searchReferenceClick() {
		if (this.handler.searchReferenceClick) {
			this.handler.searchReferenceClick();
		}
	}

	addReferenceClick() {
		if (this.handler.addReferenceClick) {
			this.handler.addReferenceClick();
		}
	}

	setupPanelAndInput() {
		if (this.autoComplete) {
			let element = this.autoComplete.overlay;
			element.setAttribute('for-id', this.getId());

			let inputEl = this.autoComplete.inputEL.nativeElement;
			let inputWidth = $(inputEl).outerWidth();
			$(element).outerWidth(inputWidth);

			if (!this.keydownEventRegistered) {
				this.keydownEventRegistered = true;

				inputEl.addEventListener('keydown', event => {
					if (this.handler.keydownHandler) {
						this.handler.keydownHandler(event);
					}
				});
			}
		}
		this.highlightSelectedOption();
		this.highlightUniqueMatch();
	}

	doRefreshEntries() {
		let eoId = this.eo.getId();
		if (this.attributeMeta) {
			this.eo.refreshVlp(
				this.attributeMeta.getAttributeID(),
				this.webComponent.valuelistProvider,
				false
			).pipe(take(1)).subscribe(entries => {
				// if the entity object has changed in between (e.g. after "cancel") do not reset the reference value
				if (this.eo.getId() === eoId) {
					if (this.getModelValue() !== null && !entries.some(entry => entry.id === this.getModelValue().id)) {
						this.clearModelValue();
						if (entries.length > 0) {
							this.selectDropdownOption(entries[0]);
						}
					}
					if (this.isVisible()) {
						this.showAllEntries();
					}
				}
			});
		}
	}

	protected showResultPanel() {
		if (!this.isWritable()) {
			return;
		}

		this.autoComplete?.show();
	}

	private refreshEntries(attributeId: string, onlyIfDefaultField: boolean) {
		if (this.attributeMeta && this.attributeMeta.getAttributeID() === attributeId) {
			let hasDefaultField = false;
			let vlp = this.getVlp();
			if (vlp !== undefined) {
				hasDefaultField = vlp.parameter.filter(parameter => parameter.name === 'default-fieldname').length > 0;
			}
			if (!onlyIfDefaultField || (hasDefaultField)) {
				if (this.handler.refreshEntries) {
					this.handler.refreshEntries();
				}
			}
		}
	}

	private getInputValue() {
		let inputValue = this.autoComplete ? $(this.autoComplete.inputEL.nativeElement).val() : '';
		inputValue = inputValue.replace('\u200b', '');
		return inputValue;
	}

	/**
	 * Loads new LOV entries from the given Observable,
	 * cancels previous Subscription.
	 */
	private subscribeToNewEntries(observable: Observable<LovEntry[]>) {
		if (this.resultSubscription && !this.resultSubscription.closed) {
			this.resultSubscription.unsubscribe();
		}

		this.resultSubscription = observable
			.pipe(finalize(() => this.updateMessage()), take(1))
			.subscribe(data => this.setLovEntries(data));

		this.loadingStarted();
	}

	private highlightSelectedOption() {
		const value = this.getValue();
		// highlight selected option
		if (this.autoComplete) {
			this.autoComplete.highlightOption = this.lovEntries.find(
				entry => value
					? (!this.isTextfieldCombobox() && entry.id === value.id || entry.name === value.name)
					: (entry.id === null || entry.name === '')
			);
		}

		this.lovEntries.forEach(
			option => value
				? (option.selected = (!!option.id && option.id === value.id) || (option.name === value.name))
				: option.selected = (option.id === null || option.name === '')
		);
	}

	private highlightUniqueMatch() {
		// if only one item is suggested preselect it
		if (this.lovEntries) {
			let nonEmptyResults = this.lovEntries.filter(entry => entry.id !== null);
			if (nonEmptyResults.length === 1) {
				let uniqueMatch = nonEmptyResults[0];
				if (this.autoComplete) {
					this.autoComplete.highlightOption = uniqueMatch;
				}
			}
		}
	}

	private setLovEntries(results: LovEntry[]) {
		this.lovEntries = results.slice(0, NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT);
		if (this.autoComplete) {
			this.autoComplete.loading = false;
		}
		this.updateMessage();
	}

	private getValueForTextfield() {
		let value = super.getValue();

		if (typeof value === 'string') {
			if (this.searchValue) {
				this.searchValue.name = value;
			} else {
				this.searchValue = { name: value, id: null };
			}
			value = this.searchValue;
		}

		return value;
	}

	private hasFocus() {
		let result =
			this.autoComplete &&
			this.autoComplete.inputEL &&
			this.autoComplete.inputEL.nativeElement === document.activeElement;

		return result;
	}

	private clearModelValue() {
		if (this.attributeMeta && this.attributeMeta.isReference()) {
			this.setValue(LovEntry.EMPTY);
		} else {
			this.setValue(this.isTextfieldCombobox() ? '' : undefined);
		}

		if (this.handler.clearValue && !this.isTextfieldCombobox()) {
			this.handler.clearValue();
		}

		// With PrimeNG 9 somehow the behaviour got updated
		// to match our compliments i had to add some delay in opening the autocomplete
		// cause the PrimeNG logic seems to destruct the overlay on empty value
		setTimeout(() => {
			this.showAllEntries();
		}, 250);
	}

	private filter(search: string) {
		// TODO: It should not be necessary to filter again here,
		// but filtering does not work reliably in LOVs yet.
		return ComboboxUtils.filter(this.handler.loadFilteredEntries(search), search);
	}

	private notifyClickOutside() {
		let clickOutsideService = this.injector.get(ClickOutsideService);
		if (clickOutsideService) {
			clickOutsideService.outsideClick(this.elementRef.nativeElement);
		}
	}

	private loadingStarted() {
		// Make sure we are really still loading something
		if (!this.isLoading()) {
			return;
		}

		this.lovEntries = [];
		this.updateMessage();
	}

	private saveInputEl() {
		if (this.autoComplete && this.autoComplete.highlightOption) {
			this.selectDropdownOption(this.autoComplete.highlightOption);
		} else if (!this.isTextfieldCombobox()) {
			this.selectDropdownOption(
				this.eo.getAttribute(this.getName())
			);
		}
	}

	/**
	 * Abuses the empty-message for displaying a "loading" text, since the normal loading indicator of
	 * the component can not be used because it overlaps with Nuclos reference buttons.
	 */
	private updateMessage() {
		this.showResultPanel();
		if (this.autoComplete) {
			if (this.isLoading()) {
				this.autoComplete.emptyMessage = this.getI18nService().getI18n('common.loading');
				this.autoComplete.loading = true;
			} else {
				this.autoComplete.loading = false;
				if (typeof this.lovEntries !== 'undefined' && this.lovEntries.length === 0) {
					this.autoComplete.emptyMessage = this.getI18nService().getI18n('common.empty');
				} else {
					this.autoComplete.emptyMessage = '';
				}

				let element = this.autoComplete.overlay;
				let inputEl = this.autoComplete.inputEL.nativeElement;

				// NUCLOS-8397 open Dropdown topside if size below input is not enough
				// 200 is the default dropdown max-height
				const realSize = (this.lovEntries.length * 21.5) > 200 ? 200 : (this.lovEntries.length * 21.5);
				const sizeNeeded = $(inputEl).offset().top + $(inputEl).innerHeight() + realSize;
				if (sizeNeeded > window.innerHeight) {
					const newTopOffset = $(inputEl).offset().top - realSize;
					$(element).offset({top: newTopOffset, left: $(inputEl).offset().left});
				}
			}
		}
	}

	private isTextfieldCombobox(): boolean {
		return (this.attributeMeta && !this.attributeMeta.isReference()) ?? false;
	}

	private getI18nService() {
		return this.injector.get(NuclosI18nService);
	}

	private registerShortCuts() {
		this.unsubscribe$ = new Subject<boolean>();

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT',
			keys: 'r',
			description: this.i18nService.getI18n('webclient.shortcut.reference-target.refresh'),
			target: this.elementRef.nativeElement
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.refreshReferenceTarget();
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT',
			keys: 'a',
			description: this.i18nService.getI18n('webclient.shortcut.reference-target.add'),
			target: this.elementRef.nativeElement
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.addReferenceTarget();
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT',
			keys: 'e',
			description: this.i18nService.getI18n('webclient.shortcut.reference-target.edit'),
			target: this.elementRef.nativeElement
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.editReferenceTarget();
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT',
			keys: 'q',
			description: this.i18nService.getI18n('webclient.shortcut.reference-target.search'),
			target: this.elementRef.nativeElement
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.searchReferenceTarget();
		});
	}
}
