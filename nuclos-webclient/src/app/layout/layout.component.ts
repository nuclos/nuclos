import {
	AfterViewChecked,
	Component,
	ElementRef,
	EventEmitter,
	HostListener,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { ClickOutsideService } from '@modules/click-outside/click-outside.service';
import { EntityObjectEventService } from '@modules/entity-object-data/shared/entity-object-event.service';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { Logger } from '@modules/log/shared/logger';
import { RuleService } from '@modules/rule/shared/rule.service';
import { IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { BrowserDetectionService } from '@shared/service/browser-detection.service';
import { BusyService } from '@shared/service/busy.service';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { Observable, of, Subject, Subscriber, Subscription } from 'rxjs';
import { catchError, distinctUntilChanged, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { LayoutService } from './shared/layout.service';

@Component({
	selector: 'nuc-layout',
	templateUrl: './layout.component.html',
	styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit, OnChanges, OnDestroy, AfterViewChecked {
	@Input() eo: EntityObject | undefined;
	@Output() onSubmit = new EventEmitter<any>();
	@Output() onLayoutLoaded = new EventEmitter<any>();

	@ViewChild('layoutContainer') layoutContainer: ElementRef;

	webLayout: WebLayout | undefined;
	rules: Rules | undefined;

	private eoListener: IEntityObjectEventListener;
	private subscriptions: Subscription = new Subscription();
	private isInitialFocusSet = false;
	private isBusy = false;
	private unsubscribe$ = new Subject<boolean>();
	private listOfElements: Array<{ type: string; name: string | null; elementRef: HTMLElement }> = [];
	private $refreshTabList: Subject<any> = new Subject<any>();

	constructor(
		private layoutService: LayoutService,
		private eoService: EntityObjectService,
		private entityObjectEventService: EntityObjectEventService,
		private busyService: BusyService,
		private ruleService: RuleService,
		private clickOutsideServer: ClickOutsideService,
		private browserDetectionService: BrowserDetectionService,
		private elementRef: ElementRef,
		private hotkeysService: NuclosHotkeysService,
		private i18n: NuclosI18nService,
		private dialogService: NuclosDialogService,
		private $log: Logger
	) {
		this.subscriptions = this.$refreshTabList
			.pipe(
				switchMap(() => this.buildTabFocusList())
			)
			.subscribe((tabList: Array<any>) => {
				this.$log.debug('Collecting tab focus list', tabList);
				this.listOfElements = tabList;
			});
	}

	ngOnInit() {
		this.registerHotkeys();

		this.subscriptions.add(this.dialogService.observeLastDialogClosed().subscribe(
			() => this.selectFirstAttributeEntry()
		));

		this.subscriptions.add(this.busyService.isBusy().pipe(distinctUntilChanged()).subscribe(
			isBusy => this.isBusy = isBusy
		));

		this.subscriptions.add(
			this.entityObjectEventService.observeEoLayoutUpdated().subscribe(() => {
				setTimeout(() => {
					this.$refreshTabList.next(new Date().getMilliseconds());
				});
			})
		);
	}

	ngAfterViewChecked(): void {
		if (this.eo && this.eo.isNew() && !this.isInitialFocusSet && !this.isBusy) {
			// this specific code path lead to ExpressionHasChangedAfterViewError
			// to avoid this and because it is necessary it is called with setTimeout
			// in the next GUI Tick
			setTimeout(() => {
				this.isInitialFocusSet = this.selectFirstAttributeEntry();
			});
		}
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChanges = changes['eo'];
		if (eoChanges) {
			let previousEO = changes['eo'].previousValue;

			if (previousEO && this.eoListener) {
				previousEO.removeListener(this.eoListener);
			}

			let eo: EntityObject = changes['eo'].currentValue;
			if (eo) {
				this.createListener();
				eo.addListener(this.eoListener);
				this.updateLayout(eo).pipe(
					take(1)
				).subscribe();
			}
		}
	}

	submit() {
		// TODO: Submitting the form somehow also triggers adding of a new row in the subform
		this.onSubmit.next(true);
	}

	@HostListener('window:resize')
	refreshFocusOnResize(): void {
		setTimeout(() => {
			this.$refreshTabList.next(new Date().getMilliseconds());
		}, 1000);
	}

	keydown($event: KeyboardEvent) {
		if ($event.key === 'Tab') {
			let srcElement = $event.target;

			if (this.isSubformChildFocused(srcElement)) {
				return;
			}

			if (srcElement === this.layoutContainer?.nativeElement) {
				// set initial focus
				this.selectFirstAttributeEntry();

				$event.preventDefault();
				$event.stopPropagation();
				return;
			}

			if (!srcElement || !this.isElementInFocusList(srcElement)) {
				if (this.hasNextFocusAndCanFocusOrIgnore($event, $event.shiftKey)) {
					return;
				}
			}

			$event.preventDefault();
			$event.stopPropagation();

			if (this.listOfElements.length > 0) {
				const srcAttrName = $(srcElement!).closest('[next-focus-field],[next-focus-comp]').attr('name');
				let currentFocusIndex: number = [...this.listOfElements]
					.reverse()
					.findIndex(
						(el) =>
							srcElement instanceof Node && el.elementRef.isEqualNode(srcElement)
					);
				if (currentFocusIndex === -1) {
					currentFocusIndex = [...this.listOfElements]
						.reverse()
						.findIndex((el) => el.name !== null && el.name === srcAttrName);
				}

				if (currentFocusIndex !== -1) {
					currentFocusIndex = this.listOfElements.length - (currentFocusIndex + 1);
				}

				if ($event.shiftKey) {
					this.focusingIndexInDirection(-1, currentFocusIndex);
				} else {
					this.focusingIndexInDirection(1, currentFocusIndex);
				}
			}
		}
	}

	// where direction is 1 for next and -1 for previous
	private focusingIndexInDirection(direction: number, currentFocusIndex: number) {
		let nextFocusIndex;
		if (direction === -1) {
			if (currentFocusIndex - 1 < 0) {
				nextFocusIndex = this.listOfElements.length - 1;
			} else {
				nextFocusIndex = currentFocusIndex - 1;
			}
		} else {
			if (currentFocusIndex + 1 > (this.listOfElements.length - 1)) {
				nextFocusIndex = 0;
			} else {
				nextFocusIndex = currentFocusIndex + 1;
			}
		}

		if (nextFocusIndex !== -1) {
			const element = this.listOfElements[nextFocusIndex];
			this.$log.debug('Focusing next element from layout elements', element);
			if ($(element.elementRef).length > 0) {
				this.focus(element.elementRef);
			} else {
				this.focusingIndexInDirection(direction, nextFocusIndex);
			}
		}
	}

	private createListener() {
		// Create an Observable for the busy service
		let subscriber: Subscriber<any>;
		let saving: Observable<any> = new Observable(internalSubscriber => {
			subscriber = internalSubscriber;
		});

		this.eoListener = {
			afterLayoutChange: (entityObject: EntityObject, _layoutURL: string) => {
				this.busyService.busy(
						this.updateLayout(entityObject).pipe(
							take(1),
							switchMap(() => this.ruleService.updateRuleExecutor(entityObject))
						)
					)
					.pipe(
						take(1)
					)
					.subscribe();
			},
			isSaving: (_eo, inProgress) => {
				if (inProgress) {
					this.busyService.busy(saving).pipe(takeUntil(this.unsubscribe$)).subscribe();
					subscriber.next(true);
				} else {
					subscriber.next(false);
					subscriber.complete();
				}
				Logger.instance.debug('Saving: %o', inProgress);
			},
			afterSave: (_eo) => {
				this.isInitialFocusSet = this.selectFirstAttributeEntry();
				setTimeout(() => {
					this.$refreshTabList.next(new Date().getMilliseconds());
				});
			}
		};
	}

	private updateLayout(eo: EntityObject): Observable<any> {
		return this.layoutService.getWebLayout(eo).pipe(
			catchError((err) => {
				return this.dialogService.alert({
					message: this.i18n.getI18n('webclient.error.unknown.message', err.message ? err.message : err.toString()),
					title: this.i18n.getI18n('webclient.error.title')
				});
			}),
			tap(weblayout => {
				this.webLayout = weblayout;
				this.isInitialFocusSet = false;
				this.onLayoutLoaded.next(true);
				setTimeout(() => {
					this.$refreshTabList.next(new Date().getMilliseconds());
				});
			})
		);
	}

	private focus(element) {
		this.unfocusCurrent();
		this.$log.debug('Call focus on element: ', element);
		element.focus();
	}

	private canFocus(element): boolean {
		let $el = $(element);

		if ($el.attr('contenteditable') === 'true') {
			return true;
		}

		if (
			(element.type === 'checkbox' && $el.is(':disabled')) ||
			(element.type !== 'checkbox' &&
				($el.is(':hidden') ||
					$el.is(':disabled')))
		) {
			return false;
		}

		let tabIndex = +$el.attr('tabindex');
		tabIndex = isNaN(tabIndex) ? -1 : tabIndex;
		return $el.is(':input, a[href], area[href], iframe') || tabIndex > -1;
	}

	private selectFirstAttributeEntry(force?: boolean): boolean {
		return this.eoService.setFocusOnInitialFocusElementForEntityObject(force);
	}

	private registerHotkeys() {
		const isMainLayout = $(this.elementRef.nativeElement).parent().get(0)?.tagName === 'NUC-DETAIL';
		// so we do only register this hotkeys once and not for overlays again
		if (isMainLayout) {
			this.subscriptions.add(this.hotkeysService.addShortcut({
				'modifier': 'ALT', 'keys': '0', 'description':
					this.i18n.getI18n('webclient.shortcut.detail.selectFirst')
			})
				.subscribe(
					() => {
						this.selectFirstAttributeEntry(true);
					}));

			this.subscriptions.add(this.hotkeysService.addShortcut({
				'modifier': 'ALT', 'keys': 'ESC', 'description':
					this.i18n.getI18n('webclient.shortcut.detail.unfocusActive')
			})
				.subscribe(() => {
					// if there is an active element blur it to unfocus
					this.unfocusCurrent();
				}));
		}
	}

	private unfocusCurrent() {
		// Simulate an outside click for previous element, because "blur" is not enough in some cases (dropdowns).
		this.clickOutsideServer.outsideClick(this.elementRef.nativeElement);

		if (document.activeElement) {
			$(document.activeElement).blur();
		}
	}

	private buildTabFocusList(): Observable<Array<any>> {
		let listOfElements: Array<{ type: string; name: string | null; elementRef: HTMLElement }> = [];
		let processedElements: Array<{ type: string; name: string | null; elementRef: HTMLElement }> = [];

		// collect elements starting from initial focus element
		let startElement = this.eoService.getInitialFocusElement();
		this.collectFocusableElements(startElement, listOfElements, processedElements);
		return of(listOfElements);
	}

	private collectFocusableElements(
		startElement: JQuery | undefined,
		listOfElements: Array<{ type: string; name: string | null; elementRef: HTMLElement }>,
		processedElements: Array<{ type: string; name: string | null; elementRef: HTMLElement }>
	) {
		while (startElement !== undefined) {
			if (startElement.length ?? 0 > 0) {
				// handling when initial focus is input of lov
				if (startElement.first().attr('class')?.includes('p-autocomplete-input')) {
					startElement = startElement.parentsUntil('nuc-web-component').find('div.lov-container');
				}
				// handling when initial focus is input of multi-selection
				if (startElement.first().attr('class')?.includes('multi-selection-container')) {
					startElement = startElement.parentsUntil('nuc-web-component').find('div.multi-selection-container');
				}
				const inputID = startElement?.first().attr('id') ?? startElement?.first().attr('for-id');
				const attrName = startElement?.first().attr('name');
				let nextFocusFieldName = startElement?.first().attr('next-focus-field');
				let nextFocusComponent = startElement?.first().attr('next-focus-comp');

				if (nextFocusComponent !== undefined) {
					// this overwrites nextFocusFieldName
					nextFocusFieldName = nextFocusComponent;
				}

				if (!this.isSubformChild(startElement)) {
					// look if label has toggle tip before
					const labelForElement = $('label[for="' + inputID + '"]');
					if (labelForElement.length > 0) {
						let toggletipLabel = labelForElement.parentsUntil('nuc-web-component').find('nuc-toggletip');
						if (toggletipLabel.length > 0) {
							this.addToListIfNotAvailable(
								this.getFirstFocusableElement(toggletipLabel),
								listOfElements
							);
						}
					}

					// put current input element as focusable in
					if (startElement.find('.p-editor-content').length > 0) {
						this.addToListIfNotAvailable(
							this.getFirstFocusableElement(startElement.find('.p-editor-content').first()),
							listOfElements
						);
					} else if (startElement.first().get(0).tagName !== 'NUC-TAB-BAR') {
						let focusELInput = this.getFirstFocusableElement(startElement);
						if (focusELInput !== undefined &&
							(focusELInput.elementRef.getAttribute('disabled') ?? 'false') === 'false') {
							this.addToListIfNotAvailable(
								focusELInput,
								listOfElements
							);
						}
					}

					// look if there are any links in this element
					let startContainer = startElement.parentsUntil('nuc-web-component');
					if (startElement.get(0).tagName === 'NUC-TAB-BAR') {
						startContainer = startElement.find('div.p-tabview-nav-content');
					}

					if (startElement.get(0).tagName !== 'NUC-WEB-SUBFORM') {
						const linksElements = startContainer.find('a,[tabindex]');
						for (let i = 0; i < linksElements.length; i++) {
							let focusLinkEL = this.getFirstFocusableElement($(linksElements.get(i)));
							if (focusLinkEL !== undefined &&
								// skip lov links
								!$(focusLinkEL.elementRef).attr('class')?.includes('reference') &&
								// skip dropdown btn
								!$(focusLinkEL.elementRef).attr('class')?.includes('p-autocomplete-dropdown')) {
								this.addToListIfNotAvailable(
									focusLinkEL,
									listOfElements
								);
							}
						}
					}
					// look if toggle tip is present after
					let toggletipInput = startContainer.find('nuc-toggletip');
					if (toggletipInput.length > 0) {
						this.addToListIfNotAvailable(
							this.getFirstFocusableElement(toggletipInput),
							listOfElements
						);
					}
				}

				processedElements.push({
					type: startElement.get()[0].tagName,
					name: attrName,
					elementRef: startElement.get()[0]
				});

				if (nextFocusFieldName !== undefined &&
					nextFocusFieldName !== attrName &&
					listOfElements.find((el) => el.name === nextFocusFieldName) === undefined &&
					processedElements.find((processedElement) => processedElement.name === nextFocusFieldName) === undefined) {
					startElement = startElement.parentsUntil('.layout-container').find('[name="' + nextFocusFieldName + '"]');
				} else {
					startElement = undefined;
				}
			} else if (processedElements.length > 0) {
				let remainingElements = $(processedElements[0].elementRef).parentsUntil('.layout-container').find('[name]');
				if (remainingElements.length > processedElements.length) {
					// set next field as focus seems to be interrupted
					startElement = remainingElements.eq(processedElements.length);
				} else {
					// we reached end
					startElement = undefined;
				}
			} else {
				startElement = undefined;
			}
		}
		const modalButtons = $('ngb-modal-window div.modal-footer').children();
		for (let j = 0; j < modalButtons.length; j++) {
			this.addToListIfNotAvailable(
				this.getFirstFocusableElement($(modalButtons.get(j))),
				listOfElements
			);
		}
	}

	private addToListIfNotAvailable(
		item: {type: string, name: string | null, elementRef: HTMLElement} | undefined,
		array: Array<{ type: string; name: string | null; elementRef: HTMLElement }>): void {

		if (item &&
			array.find(
				(aItem) =>
					aItem.elementRef.isEqualNode(item.elementRef) ||
					(aItem.name !== null && aItem.name === item.name)
			) === undefined) {
			array.push(item);
		}
	}

	private getFirstFocusableElement(element: JQuery):
		{type: string, name: string | null, elementRef: HTMLElement} | undefined {
		for (let i = 0; i < element.length; i++) {
			if (this.canFocus(element.get(i))) {
				return {
					type: element.get(i).tagName,
					name: element.get(i).getAttribute('name'),
					elementRef: element.get(i)
				};
			} else {
				// look up children
				let focusableElement = this.getFirstFocusableElement(element.children());
				if (focusableElement !== undefined) {
					return focusableElement;
				}
			}
		}
		return undefined;
	}

	private isSubformChildFocused(srcElement: EventTarget | null): boolean {
		return srcElement !== null && this.isSubformChild($(srcElement));
	}

	private isSubformChild(element: JQuery | undefined): boolean {
		return !!element && element.tagName !== 'NUC-WEB-SUBFORM' &&
			element.parentsUntil('nuc-web-subform').parent().first().get(0).tagName
			=== 'NUC-WEB-SUBFORM';
	}

	private isElementInFocusList(srcElement: EventTarget) {
		return this.listOfElements.find(
			(focusEl) => focusEl.elementRef === srcElement || focusEl.elementRef.contains(srcElement as Node)
		) !== undefined;
	}

	private hasNextFocusAndCanFocusOrIgnore(event: KeyboardEvent, reverse = false): boolean {
		let srcElement = event.target;
		if (srcElement !== null) {
			const nextFocusAvailable = $(srcElement)
				.closest('[next-focus-field]')
				.attr('next-focus-field');

			const attributeName = $(srcElement)
				.closest('[next-focus-field]')
				.attr('name');

			let element;
			if (reverse) {
				if (attributeName !== undefined) {
					element = $('[next-focus-field="' + attributeName + '"');
				}
			} else {
				if (nextFocusAvailable !== undefined) {
					element = $('[name="' + nextFocusAvailable + '"');
				}
			}
			if (element && element.length > 0 && this.canFocus(element.get(0))) {
				this.focus(element.get(0));

				event.preventDefault();
				event.stopPropagation();

				return true;
			}
		}

		return false;
	}
}
