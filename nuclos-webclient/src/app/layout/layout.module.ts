import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PlanningTableModule } from '@modules/planning-table/planning-table.module';
import { SearchModule } from '@modules/search/search.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgGridModule } from 'ag-grid-angular';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ChipModule } from 'primeng/chip';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { SplitButtonModule } from 'primeng/splitbutton';
import { VirtualScrollerModule } from 'primeng/virtualscroller';
import { ClickOutsideModule } from '../modules/click-outside/click-outside.module';
import {
	ViewPreferencesConfigModule
} from '../modules/entity-object/view-preferences-config/view-preferences-config.module';
import { GridModule } from '../modules/grid/grid.module';
import { I18nModule } from '../modules/i18n/i18n.module';
import { UiComponentsModule } from '../modules/ui-components/ui-components.module';
import { SharedModule } from '../shared/shared.module';
import { GridLayoutComponent } from './grid-layout/grid-layout.component';
import { LayoutComponent } from './layout.component';
import { ImageService } from './shared/image.service';
import { LayoutService } from './shared/layout.service';
import { TextModuleComponent } from './text-module/text-module.component';
import { WebAddonComponent } from './web-addon/web-addon.component';
import { WebButtonAddonComponent } from './web-button/web-button-addon/web-button-addon.component';
import { WebButtonChangeStateComponent } from './web-button/web-button-change-state/web-button-change-state.component';
import { WebButtonDummyComponent } from './web-button/web-button-dummy/web-button-dummy.component';
import { WebButtonExecuteRuleComponent } from './web-button/web-button-execute-rule/web-button-execute-rule.component';
import {
	WebButtonGenerateObjectComponent
} from './web-button/web-button-generate-object/web-button-generate-object.component';
import { WebButtonHyperlinkComponent } from './web-button/web-button-hyperlink/web-button-hyperlink.component';
import { WebCheckboxComponent } from './web-checkbox/web-checkbox.component';
import { WebColorchooserComponent } from './web-colorchooser/web-colorchooser.component';
import { WebComboboxComponent } from './web-combobox/web-combobox.component';
import { WebComponentComponent } from './web-component/web-component.component';
import { WebContainerComponent } from './web-container/web-container.component';
import { WebDatechooserComponent } from './web-datechooser/web-datechooser.component';
import { WebEmailComponent } from './web-email/web-email.component';
import { WebFileComponent } from './web-file/web-file.component';
import { WebGridCalculatedComponent } from './web-grid-calculated/web-grid-calculated.component';
import { WebHtmlEditorComponent } from './web-html-editor/web-html-editor.component';
import { WebHyperlinkComponent } from './web-hyperlink/web-hyperlink.component';
import { WebImageComponent } from './web-image/web-image.component';
import { WebLabelStaticComponent } from './web-label-static/web-label-static.component';
import { WebLabelComponent } from './web-label/web-label.component';
import { AddReferenceTargetComponent } from './web-listofvalues/add-reference-target/add-reference-target.component';
import { EditReferenceTargetComponent } from './web-listofvalues/edit-reference-target/edit-reference-target.component';
import {
	RefreshReferenceTargetComponent
} from './web-listofvalues/refresh-reference-target/refresh-reference-target.component';
import {
	SearchReferenceTargetComponent
} from './web-listofvalues/search-reference-target/search-reference-target.component';
import { WebListofvaluesComponent } from './web-listofvalues/web-listofvalues.component';
import { MatrixButtonsComponent } from './web-matrix/matrix-buttons/matrix-buttons.component';
import { WebMatrixComponent } from './web-matrix/web-matrix.component';
import {
	WebMultiselectionComboboxComponent
} from './web-multiselection-combobox/web-multiselection-combobox.component';
import { WebOptiongroupComponent } from './web-optiongroup/web-optiongroup.component';
import { WebPanelComponent } from './web-panel/web-panel.component';
import { WebPasswordComponent } from './web-password/web-password.component';
import { WebPhonenumberComponent } from './web-phonenumber/web-phonenumber.component';
import { WebScrollableBoxComponent } from './web-scrollable-box/web-scrollable-box.component';
import { WebScrollpaneComponent } from './web-scrollpane/web-scrollpane.component';
import { WebSeparatorComponent } from './web-separator/web-separator.component';
import {
	SubformBooleanEditorComponent
} from './web-subform/cell-editors/subform-boolean-editor/subform-boolean-editor.component';
import {
	SubformComboboxEditorComponent
} from './web-subform/cell-editors/subform-combobox-editor/subform-combobox-editor.component';
import {
	SubformDateEditorComponent
} from './web-subform/cell-editors/subform-date-editor/subform-date-editor.component';
import {
	SubformEmailEditorComponent
} from './web-subform/cell-editors/subform-email-editor/subform-email-editor.component';
import {
	SubformHyperlinkEditorComponent
} from './web-subform/cell-editors/subform-hyperlink-editor/subform-hyperlink-editor.component';
import { SubformLovEditorComponent } from './web-subform/cell-editors/subform-lov-editor/subform-lov-editor.component';
import {
	SubformMultilineEditorComponent
} from './web-subform/cell-editors/subform-multiline-editor/subform-multiline-editor.component';
import {
	SubformNumberEditorComponent
} from './web-subform/cell-editors/subform-number-editor/subform-number-editor.component';
import {
	SubformStringEditorComponent
} from './web-subform/cell-editors/subform-string-editor/subform-string-editor.component';
import { FloatingFilterDateComponent } from './web-subform/cell-filters/floating-filter-date/floating-filter-date.component';
import { FloatingFilterNumberComponent } from './web-subform/cell-filters/floating-filter-number/floating-filter-number.component';
import { FloatingFilterTextComponent } from './web-subform/cell-filters/floating-filter-text/floating-filter-text.component';
import { FilterBooleanComponent } from './web-subform/cell-filters/filter-boolean/filter-boolean.component';
import { FloatingFilterLovComponent } from './web-subform/cell-filters/floating-filter-lov/floating-filter-lov.component';
import {
	FloatingFilterBooleanComponent
} from './web-subform/cell-filters/floating-filter-boolean/floating-filter-boolean.component';
import {
	FloatingFilterEditorComponent
} from './web-subform/cell-filters/floating-filter-editor/floating-filter-editor.component';
import {
	SubformBooleanRendererComponent,
	SubformDateRendererComponent,
	SubformDocumentRendererComponent,
	SubformEditRowRendererComponent,
	SubformEmailRendererComponent,
	SubformHiddenRendererComponent,
	SubformHyperlinkRendererComponent,
	SubformNumberRendererComponent,
	SubformReferenceRendererComponent,
	SubformStateIconRendererComponent
} from './web-subform/cell-renderer';
import { FilterEditorOverlayComponent } from './web-subform/filter-editor-overlay/filter-editor-overlay.component';
import {
	SubformSearchFilterSelectorComponent
} from './web-subform/filter-editor-overlay/subform-search-filter-selector/subform-search-filter-selector.component';
import {
	FavoriteFilterHamburger
} from './web-subform/filter-favorite-header-group/favorite-filter-hamburger/favorite-filter-hamburger';
import {
	FavoriteFilterSelector
} from './web-subform/filter-favorite-header-group/favorite-filter-selector/favorite-filter-selector';
import {
	FavoriteFiltersRow
} from './web-subform/filter-favorite-header-group/favorite-filters-row/favorite-filters-row';
import { FilterFavoriteHeaderGroup } from './web-subform/filter-favorite-header-group/filter-favorite-header-group';
import { NoRowsOverlayComponent } from './web-subform/no-rows-overlay/no-rows-overlay.component';
import {
	MoreSubformActionsPopup
} from './web-subform/subform-buttons/./more-subform-actions-popup/more-subform-actions-popup';
import { SubformButtonsComponent } from './web-subform/subform-buttons/subform-buttons.component';
import { WebSubformComponent } from './web-subform/web-subform.component';
import { WebSubformService } from './web-subform/web-subform.service';
import { WebTabcontainerComponent } from './web-tabcontainer/web-tabcontainer.component';
import { WebTableComponent } from './web-table/web-table.component';
import { WebTextareaComponent } from './web-textarea/web-textarea.component';
import { WebTextfieldComponent } from './web-textfield/web-textfield.component';
import { WebTitledSeparatorComponent } from './web-titled-separator/web-titled-separator.component';
import { WebSplitpaneComponent } from './web-splitpane/web-splitpane.component';
import { WebPlanningTableComponent } from './web-planning-table/web-planning-table.component';
import { WebHtmlFieldComponent } from './web-html-field/web-html-field.component';

@NgModule({
	imports: [
		AgGridModule.withComponents([
			SubformBooleanRendererComponent,
			SubformDateRendererComponent,
			SubformDocumentRendererComponent,
			SubformStateIconRendererComponent,
			SubformNumberRendererComponent,
			SubformReferenceRendererComponent,
			SubformEditRowRendererComponent,
			SubformHiddenRendererComponent,
			SubformEmailRendererComponent,
			SubformHyperlinkRendererComponent,
			SubformComboboxEditorComponent,
			SubformDateEditorComponent,
			SubformLovEditorComponent,
			SubformNumberEditorComponent,
			SubformBooleanEditorComponent,
			SubformStringEditorComponent,
			SubformMultilineEditorComponent,
			SubformEmailEditorComponent,
			SubformHyperlinkEditorComponent,
			FloatingFilterDateComponent,
			FloatingFilterNumberComponent,
			FloatingFilterTextComponent,
			FloatingFilterBooleanComponent,
			FloatingFilterEditorComponent,
			FilterBooleanComponent,
			FloatingFilterLovComponent,
		]),
		AutoCompleteModule,
		CommonModule,
		FormsModule,
		SharedModule,
		I18nModule,
		ClickOutsideModule,
		UiComponentsModule,
		NgbModule,
		ViewPreferencesConfigModule,
		GridModule,
		VirtualScrollerModule,
		PlanningTableModule,
		SearchModule,
		DropdownModule,
		MultiSelectModule,
		ChipModule,
		SplitButtonModule
	],
	declarations: [
		AddReferenceTargetComponent,
		EditReferenceTargetComponent,
		GridLayoutComponent,
		LayoutComponent,
		RefreshReferenceTargetComponent,
		SearchReferenceTargetComponent,
		SubformBooleanRendererComponent,
		SubformBooleanEditorComponent,
		SubformButtonsComponent,
		FilterEditorOverlayComponent,
		FilterFavoriteHeaderGroup,
		FavoriteFiltersRow,
		FavoriteFilterHamburger,
		FavoriteFilterSelector,
		SubformSearchFilterSelectorComponent,
		MatrixButtonsComponent,
		SubformComboboxEditorComponent,
		SubformDateEditorComponent,
		SubformDateRendererComponent,
		SubformDocumentRendererComponent,
		SubformEmailRendererComponent,
		SubformEmailEditorComponent,
		SubformHyperlinkRendererComponent,
		SubformHyperlinkEditorComponent,
		SubformLovEditorComponent,
		SubformMultilineEditorComponent,
		SubformNumberRendererComponent,
		SubformReferenceRendererComponent,
		FloatingFilterDateComponent,
		FloatingFilterNumberComponent,
		FloatingFilterTextComponent,
		FilterBooleanComponent,
		FloatingFilterLovComponent,
		FloatingFilterBooleanComponent,
		FloatingFilterEditorComponent,
		WebAddonComponent,
		SubformStateIconRendererComponent,
		SubformStringEditorComponent,
		WebButtonChangeStateComponent,
		WebButtonAddonComponent,
		WebButtonDummyComponent,
		WebButtonExecuteRuleComponent,
		WebButtonGenerateObjectComponent,
		WebButtonHyperlinkComponent,
		WebCheckboxComponent,
		WebColorchooserComponent,
		WebComboboxComponent,
		WebComponentComponent,
		WebContainerComponent,
		WebDatechooserComponent,
		WebEmailComponent,
		WebFileComponent,
		WebGridCalculatedComponent,
		WebHyperlinkComponent,
		WebLabelComponent,
		WebLabelStaticComponent,
		WebListofvaluesComponent,
		WebMatrixComponent,
		WebMultiselectionComboboxComponent,
		WebOptiongroupComponent,
		WebPasswordComponent,
		WebPhonenumberComponent,
		WebPlanningTableComponent,
		WebSeparatorComponent,
		WebSubformComponent,
		WebTabcontainerComponent,
		WebTableComponent,
		WebTextareaComponent,
		WebTextfieldComponent,
		WebTitledSeparatorComponent,
		SubformNumberEditorComponent,
		SubformStringEditorComponent,
		SubformEditRowRendererComponent,
		SubformHiddenRendererComponent,
		WebHtmlEditorComponent,
		WebPanelComponent,
		SubformStateIconRendererComponent,
		SubformButtonsComponent,
		WebScrollableBoxComponent,
		WebImageComponent,
		TextModuleComponent,
		NoRowsOverlayComponent,
		WebScrollpaneComponent,
		WebSplitpaneComponent,
		WebPlanningTableComponent,
		WebHtmlFieldComponent,
		MoreSubformActionsPopup
	],
	providers: [
		LayoutService,
		WebSubformService,
		ImageService,
	],
	exports: [
		LayoutComponent,
		WebScrollableBoxComponent,
		WebListofvaluesComponent,
		WebDatechooserComponent
	]
})
export class LayoutModule {
}
