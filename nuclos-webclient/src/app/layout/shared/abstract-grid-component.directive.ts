import { Directive, DoCheck, ElementRef, Injector } from '@angular/core';
import { IEntityObjectDependents, IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { ObjectUtils } from '@shared/object-utils';
import { FqnService } from '@shared/service/fqn.service';
import { LocalStorageService } from '@shared/service/local-storage.service';
import { ColDef, GridApi, GridOptions, ProcessRowParams, RowNode } from 'ag-grid-community';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { EntityAttrMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { EntityObject, SubEntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { Logger } from '@modules/log/shared/logger';
import { AbstractInputComponentDirective } from './abstract-input-component.directive';

/**
 * TODO: Consolidate with GridComponent and EntityObjectGridComponent
 */
@Directive()
export abstract class AbstractGridComponentDirective<T extends WebInputComponent>
	extends AbstractInputComponentDirective<T>
	implements DoCheck {
	gridOptions: GridOptions = <GridOptions>{};

	/**
	 * The dependents (Sub-EOs).
	 * "undefined" means "loading".
	 */
	protected dependents?: IEntityObjectDependents;

	protected elementRef: ElementRef;

	protected eoChangeListener: IEntityObjectEventListener;

	protected dependentSubscription: Subscription;

	protected rightsChanged = false;

	/**
	 * Listens for changes in the Sub-EOs.
	 * Changes might e.g. be made by Layout-Rules. The grid must then be updated.
	 */
	private eoListener: IEntityObjectEventListener;

	private autonumberAttribute: EntityAttrMeta | undefined;

	private _eo: EntityObject;

	private gridColumns: ColDef[] = [];

	constructor(injector: Injector, protected i18nService: NuclosI18nService, elementRef: ElementRef) {
		super(injector);

		this.elementRef = elementRef;

		this.gridOptions.getRowNodeId = item => {
			return item.getId() ?? item.shadowID;
		};

		this.gridOptions.processRowPostCreate = (params: ProcessRowParams) => {
			if (params.node.data.eoData.rowcolor) {
				params.eRow.setAttribute('nuclos-row-color', params.node.data.eoData.rowcolor);
			}
		};

		this.gridOptions.localeTextFunc = (key, defaultValue) => {
			let gridKey = 'grid.' + key;
			return i18nService.getI18nOrUndefined(gridKey) || defaultValue;
		};

		this.gridOptions.suppressPropertyNamesCheck = true;

		this.eoListener = {
			afterSave: () => this.loadData(),
			beforeCancel: () => this.cancel(),
			afterReload: () => this.init(),
			afterClone: () => this.loadData()
		};
	}

	isVisible(): boolean {
		return !!this.gridOptions.columnDefs;
	}

	/**
	 * TODO: DoCheck is called very often. Maybe there is a better way to reliably detect changes of the EO.
	 */
	ngDoCheck(): void {
		if (this._eo !== this.eo) {
			// ngDoCheck is called several times when changing eo, once without subEos and then with subEos.
			// One of them with subEos is the one we want
			if (!this.eo.getSubEoInfos()) {
				return;
			}
			// Same issue applies here, only one of nearly identical eos is needed
			if (this._eo && this._eo.getEntityClassId() === this.eo.getEntityClassId()) {
				if (this._eo && this._eo.getId() === this.eo.getId()) {
					return;
				}
			}
			this.getLogger().debug('EO changed');
			if (this._eo) {
				this._eo.removeListener(this.eoListener);
			}
			this.rightsChanged = (this.eo?.canWrite() !== this._eo?.canWrite()) || (this.eo?.canDelete() !== this._eo?.canDelete()) ||
				(!ObjectUtils.isEqualJson(this.eo?.getData()?.attrRestrictions, this._eo?.getData()?.attrRestrictions));
			this._eo = this.eo;
			this._eo.addListener(this.eoListener);

			this.updateGridStatusOverlay(true);

			this.init();
		}
	}

	removeEoListener(): void {
		this.eo?.removeListener(this.eoListener);
	}

	updateGridStatusOverlay(forceLoading = false) {
		if (this.gridOptions.api) {
			if (forceLoading) {
				this.gridOptions.api.showLoadingOverlay();
				return;
			}

			if (this.dependents) {
				if (this.dependents.isLoading()) {
					this.gridOptions.api.showLoadingOverlay();
				} else if (this.dependents.isEmpty()) {
					this.gridOptions.api.showNoRowsOverlay();
				} else {
					this.gridOptions.api.hideOverlay();
				}
			} else {
				this.gridOptions.api.hideOverlay();
			}
		}
	}

	/**
	 * called from the component
	 */
	gridReady() {
		if (this.gridOptions.api) {
			this.onGridApiReady(this.gridOptions.api);
		}

		if (this.dependents && !this.dependents.isEmpty()) {
			this.updateGridData();
		}
		this.hardRefresh();
	}

	isEmpty() {
		return !this.dependents || this.dependents.isEmpty();
	}

	// Called by click-outside directive
	stopEditing() {
		if (this.gridOptions.api) {
			this.gridOptions.api.stopEditing();
		}
	}

	getDependents() {
		return this.dependents;
	}

	protected abstract loadData();

	protected abstract init();

	/**
	 * will be called when the ag-grid api is available
	 */
	protected abstract onGridApiReady(api: GridApi);

	protected cancel() {}

	protected updateGridData() {
		this.getLogger().debug('Update grid data: %o', this.dependents);
		if (this.gridOptions.api) {
			let rowData;

			if (this.dependents) {
				// this.dependents.current() may return undefined, if it is not yet loaded.
				// In this case, the grid data should not be touched.
				// If we set an empty array here, the grid would show the "no rows" overlay.
				// See NUCLOS-6953.
				rowData = this.dependents.current();
			} else {
				// this.dependents is sometimes undefined, e.g. when multiple parents are selected for a sub-subform.
				// In this case, we must set an empty list to clear the subform.
				rowData = [];
			}

			if (rowData) {
				this.gridOptions.api.setRowData(rowData);
				this.updateRowSelection();
			}
		}
		this.updateGridStatusOverlay();

		if (this.gridOptions.api) {
			// this.gridOptions.api.doLayout();
		}

		this.getLogger().debug('Update grid data done');
	}

	/**
	 * Does a hard refresh of the view.
	 * Row DOM elements may be recreated.
	 */
	protected hardRefresh() {
		if (this.gridOptions.api) {
			this.updateRowSelection();
			this.softRefresh(true);
		}
	}

	protected updateRowSelection(): void {
		if (this.gridOptions.api) {
			this.gridOptions.api.forEachNode((rowNode: RowNode) => {
				rowNode.setSelected(rowNode.data.isSelected(), false);
			});
		}
	}

	/**
	 * Does a soft refresh of the view.
	 * Row DOM elements are not recreated. Only changed, volatile cells are redrawn.
	 */
	protected softRefresh(force = false) {
		if (this.gridOptions.api) {
			this.updateGridStatusOverlay();

			this.gridOptions.api.refreshCells({
				force: force
			});
		}
	}

	/**
	 * Column definition for selection
	 * @protected
	 */
	protected createSelectionColumn(): ColDef {
		// row selection checkbox
		let rowSelectionCheckboxColDef: ColDef = {
			colId: 'rowSelection',
			checkboxSelection: true,
			headerTooltip: this.i18nService.getI18n('webclient.checkbox.selectAll'),
			headerCheckboxSelection: true,
			headerName: '',
			width: 40,
			editable: false,
			pinned: true,
			lockPosition: true,
			lockPinned: true,
			suppressMovable: true,
			resizable: true
		};

		return rowSelectionCheckboxColDef;
	}

	/**
	 * Transfers new value to the underlying Sub-EO via the Setter method.
	 */
	protected newValueHandler(params: {
		node: RowNode | null;
		data: SubEntityObject;
		oldValue: any;
		newValue: any;
		colDef?: ColDef;
		api: GridApi;
		context: any;
	}): boolean {
		if (!params.colDef?.field) {
			this.getLogger().warn('Field missing: %o', params.colDef);
			return false;
		}

		params.node?.data
			.setAttributeObserved(params.colDef.field, params.newValue)
			.pipe(take(1))
			.subscribe(changed => {
				if (changed) {
					if (this.gridOptions.api) {
						// this leads to loosing the cell focus
						// (needed for row colors - otherwise gridOptions.getRowClass() is not called):
						// this.gridOptions.api.refreshRows([params.node]);

						// Textarea cells might have to be resized
						this.resetRowHeight();

						params.node?.setSelected(params.node.data.isSelected());
					}
				}
			});
		return false;
	}

	protected abstract getLogger(): Logger;

	protected setColumns(colDefs: ColDef[]) {
		if (ObjectUtils.isEqual(colDefs, this.gridColumns)) {
			this.getLogger().debug('ColDefs are equal', colDefs, this.gridColumns);
			return;
		}
		this.getLogger().debug('Setting column definitions', colDefs, this.gridColumns);
		// Use the API when available
		if (this.gridOptions.api) {
			this.gridOptions.api.setColumnDefs(colDefs);
		} else {
			this.gridOptions.columnDefs = colDefs;
		}

		this.autonumberAttribute = this.findAutonumberAttribute(colDefs);
		this.gridColumns = colDefs;
	}

	protected getAutonumberAttribute() {
		return this.autonumberAttribute;
	}

	/**
	 * E.g. textarea heights can change when the content changes.
	 */
	protected resetRowHeight() {
		if (this.gridOptions.api) {
			this.gridOptions.api.resetRowHeights();
		}
	}

	/**
	 * Adds the given sub-EO to the top of the grid.
	 */
	protected addSubEO(subEO: SubEntityObject): void {
		this.addSubEOs([subEO]);
	}

	/**
	 * Adds multiple subEos to the top of the grid.
	 */
	protected addSubEOs(subEOs: SubEntityObject[]): void {
		if (this.dependents) {
			this.dependents.addAll(subEOs);
		}
	}

	protected editCell(subEo: any) {
		if (this.gridOptions.api && this.gridOptions.columnApi) {
			let targetRow;
			let currentDependents = this.dependents?.current();
			if (currentDependents) {
				targetRow = this.gridOptions.api.getRowNode(String(subEo.getId() ?? subEo.shadowID));
			}
			if (targetRow !== undefined) {
				let editableCol = this.gridOptions.columnApi
					.getAllDisplayedColumns()
					.find(col => {
						let ret = false;
						if (targetRow !== null) {
							ret = col.isCellEditable(targetRow!);
						}
						return ret;
					});

				if (editableCol && targetRow?.rowIndex !== null) {
					this.gridOptions.api.startEditingCell({
						rowIndex: targetRow.rowIndex,
						colKey: editableCol
					});
					// apparently startEditingCell is not enough and the cell has to be focused explicitly - otherwise we face NUCLOS-10175
					this.gridOptions.api.setFocusedCell(targetRow.rowIndex, editableCol);
				}
			}
		}
	}

	protected editFirstCell() {
		if (this.gridOptions.api && this.gridOptions.columnApi) {
			let firstRow = this.gridOptions.api.getModel().getRow(0);
			if (firstRow !== undefined) {
				let editableCol = this.gridOptions.columnApi
					.getAllDisplayedColumns()
					.find(col => {
						let ret = false;
						if (firstRow !== null) {
							ret = col.isCellEditable(firstRow!);
						}
						return ret;
					});

				if (editableCol && firstRow?.rowIndex !== null) {
					this.gridOptions.api.startEditingCell({
						rowIndex: firstRow.rowIndex,
						colKey: editableCol
					});
				}
			}
		}
	}

	protected abstract getReferenceAttributeIdImpl();

	protected getReferenceAttributeId() {
		let attributeId = this.getReferenceAttributeIdImpl();

		if (!attributeId) {
			this.getLogger().warn('Unknown reference attribute');
			attributeId = '';
		}

		return attributeId;
	}

	protected abstract getParentForNewSubEo(): EntityObject | undefined;

	/**
	 * Creates a new Sub-EO for insertion into this subform, based on the data of the given EO.
	 */
	protected prepareNewSubEo(eo: EntityObject) {
		let parentForNewSubEo = this.getParentForNewSubEo();
		if (!parentForNewSubEo) {
			this.getLogger().error('New SubEo preparation with unknown parent! %o', eo);
			return;
		}
		let subEo = new SubEntityObject(parentForNewSubEo,
			this.getReferenceAttributeId(),
			eo.getData(),
			this.injector.get(LocalStorageService),
			this.injector.get(FqnService));

		subEo.addListener(this.eoChangeListener);
		subEo.setComplete(true);

		// TODO: Clearing attribute restrictions here, because they apply only for main EOs.
		// This is only a workaround. There should be another generation service to
		// create subform data, which sets correct attribute restrictions in the first place.
		if (subEo.getData() !== null && subEo.getData() !== undefined) {
			subEo.getData()!.attrRestrictions = undefined;
		}

		let refAttrId = this.getReferenceAttributeId();
		if (refAttrId) {
			// Restrict reference to main EO
			let ref = {
				id: this.eo.getId(),
				name: this.eo.getTitle() // TODO: Respect possibly different string representation of VLP
			};
			subEo.setAttribute(refAttrId, ref);
		} else {
			this.getLogger().error('No refAttrId defined.');
		}

		return subEo;
	}

	protected getDefaultCellClasses() {
		return {
			'row-new': params => params.data.isNew(),
			'row-modified': params => params.data.isDirty(),
			'row-deleted': params => params.data.isMarkedAsDeleted(),
		};
	}

	/**
	 * Returns the currently selected Sub-EOs.
	 *
	 * @returns {SubEntityObject[]}
	 */
	protected getSelectedSubEOs(): SubEntityObject[] {
		let currentDependents = this.dependents && this.dependents.current();

		if (!currentDependents) {
			return [];
		}

		return currentDependents.filter(subEO => subEO.isSelected()) as SubEntityObject[];
	}

	/**
	 * Marks already saved Sub-EOs for deletion.
	 * Removes new Sub-EOs completely.
	 */
	protected deleteSelectedImpl(refKey: string): void {
		let selectedSubEOs = this.getSelectedSubEOs();
		for (let i = 0; i < selectedSubEOs.length; i++) {
			let last = (i === selectedSubEOs.length - 1);
			let subEO = selectedSubEOs[i];
			if (!subEO.isNew()) {
				// mark already saved subbos as deleted
				subEO.markAsDeleted(last);
				subEO.setSelected(false);
			} else if (this.dependents) {
				this.dependents.removeAll([subEO]);
			}
		}
		this.updateGridData();
	}

	private findAutonumberAttribute(colDefs: ColDef[]) {
		for (let colDef of colDefs) {
			let params = colDef.cellEditorParams || colDef.cellRendererParams;
			let attribute = params && params.attrMeta;
			if (attribute && attribute.isAutonumber()) {
				return attribute;
			}
		}

		return undefined;
	}
}
