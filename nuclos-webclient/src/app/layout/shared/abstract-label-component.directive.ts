import { Directive } from '@angular/core';
import { AbstractWebComponentDirective } from './abstract-web-component.directive';

@Directive()
export abstract class AbstractLabelComponentDirective<T extends WebLabel | WebLabelStatic> extends AbstractWebComponentDirective<T> {
	getStyle() {
		let style: any = super.getStyle();


		// align label center if it is stretched in whole cell
		if (this.webComponent.verticalAlign === 'stretch' || this.webComponent.horizontalAlign === 'stretch') {
			style['display'] = 'flex';
			style['align-items'] = 'center';
		}

		return style;
	}
}
