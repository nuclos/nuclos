import { Directive, HostBinding, Injector, Input, OnChanges, SimpleChanges } from '@angular/core';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { ColorUtils } from '@shared/color-utils';
import { IdFactoryService } from '@shared/service/id-factory.service';
import { TooltipOptions } from 'primeng/tooltip/tooltip';

@Directive()
export abstract class AbstractWebComponentDirective<T extends WebComponent> implements OnChanges {
	@Input() eo: EntityObject;

	@Input() componentId: string | undefined;

	isHidden = false;
	tooltip: string | undefined;
	tooltipDisabled = true;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;
	attributeDescriptionTooltip: string | undefined;
	@Input() protected webComponent: T;

	private eoListenerInternal: IEntityObjectEventListener =  {
		afterValidationChange: () => {
			this.initIsHidden();
		},
		afterSave: () => {
			this.initTooltipFields();
			this.initIsHidden();
		}
	};

	constructor(protected injector: Injector) {
		if (this.injector) {
			this.webComponent = this.injector.get('webComponent');
			this.eo = this.injector.get('eo');
			this.initTooltipFields();
			this.initIsHidden();
		}
	}

	@HostBinding('style.display') get display(): any {
		return this.getTooltip() !== undefined ? 'flex' : undefined;
	}

	@HostBinding('style.align-items') get flexAlign(): any {
		return this.getTooltip() !== undefined ? 'center' : undefined;
	}

	/**
	 * Let the container grow (aka stretch) in flex context
	 */
	@HostBinding('style.flex-grow') get flexGrow(): any {
		return this.webComponent.horizontalAlign === 'stretch' ? 1 : undefined;
	}

	/**
	 * Hide scrollbars for all elements except container, to ensure we can scroll the layout
	 * but not in layout elements
	 */
	@HostBinding('style.overflow') get overFlow(): any {
		return this.webComponent['_type'] === 'WebContainer' ? 'auto' : 'hidden';
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChanges = changes['eo'];
		if (eoChanges) {
			let previousEO = changes['eo'].previousValue;

			if (previousEO && this.eoListenerInternal) {
				previousEO.removeListener(this.eoListenerInternal);
			}

			let eo: EntityObject = changes['eo'].currentValue;
			if (eo) {
				eo.addListener(this.eoListenerInternal);
			}
		}
		this.initTooltipFields();
		this.initIsHidden();
	}

	getStyle() {
		let style: any = {
			fontSize: this.webComponent.fontSize ? this.webComponent.fontSize : '0.6875rem'
		};

		if (this.webComponent.textColor) {
			style.color = this.webComponent.textColor;
		}
		if (this.webComponent.customBackgroundColor) {
			style['background-color'] = this.webComponent.customBackgroundColor;
			if (!style.color) {
				style.color = ColorUtils.calculateTextColorFromBackgroundColor(this.webComponent.customBackgroundColor);
			}
		}
		if (this.webComponent.bold) {
			style.fontWeight = 'bold';
		}
		if (this.webComponent.italic) {
			style.fontStyle = 'italic';
		}
		if (this.webComponent.underline) {
			style.textDecoration = 'underline';
		}

		style['min-height'] = `${this.webComponent.minimumHeight - 2}px`;
		if (this.getTooltip() === undefined) {
			style['min-width'] = `${this.webComponent.minimumWidth - 2}px`;
		}

		if (this.webComponent.horizontalAlign !== 'stretch' && this.getTooltip() !== undefined) {
			style['width'] = `${this.webComponent.preferredWidth - 2}px`;
			style['min-width'] = style['width'];
		} else {
			style['width'] = '100%';
		}

		if (this.webComponent.verticalAlign !== 'stretch') {
			style['height'] = `${this.webComponent.preferredHeight - 2}px`;
			style['min-height'] = style['height'];
		} else {
			style['height'] = '100%';
		}

		if (this.webComponent.borderProperty) {
			switch (this.webComponent.borderProperty.type) {
				case 'solid':
					style['border'] = `${this.webComponent.borderProperty.thickness}px solid ${this.webComponent.borderProperty.color}`;
					break;
			}
		}

		return style;
	}

	isVisible(): boolean {
		return true;
	}

	isWritable(): boolean {
		return true;
	}

	/**
	 * The ID of this component, to be used as HTML "id" attribute.
	 */
	getId(): string {
		if (!this.componentId) {
			this.componentId = this.getComponentIdOrNew();
		}

		return this.componentId;
	}

	setId(id: string) {
		this.componentId = id;
	}

	getName(): string | undefined {
		return this.webComponent && this.webComponent.name;
	}

	/**
	 * The ID of this component, to be used as HTML "name" attribute.
	 */
	getNameForHtml(): string | undefined {
		return 'attribute-' + this.getName();
	}

	getNextFocusField(): string | undefined {
		return this.webComponent && this.webComponent.nextFocusField && 'attribute-' + this.webComponent.nextFocusField;
	}

	getNextFocusComponent(): string | undefined {
		return this.webComponent && this.webComponent.nextFocusComponent && 'attribute-' + this.webComponent.nextFocusComponent;
	}

	getInitialFocus(): boolean | undefined {
		return this.webComponent.initialFocus;
	}

	getValue(): any {
		let result;

		let attributeName = this.getName();
		if (attributeName) {
			result = this.eo && this.eo.getAttribute(attributeName);
		}

		return result;
	}

	setValue(value: any) {
		let attributeName = this.getName();
		if (attributeName && this.eo) {
			this.eo.setAttribute(attributeName, value);
		}
	}

	getEntityObject(): EntityObject {
		return this.eo;
	}

	/**
	 * advanced properties from layout (Erweiterte Eigenschaften)
	 * @param key
	 * @return {any}
	 */
	getAdvancedProperty(key: string): string | undefined {
		if (!this.webComponent.advancedProperties) {
			return undefined;
		}
		let property = this.webComponent.advancedProperties.filter(p => p.name === key).shift();
		return property ? property.value : undefined;
	}

	protected getAttributeName(): string | undefined {
		return this.webComponent && this.webComponent.name && this.webComponent.name.replace('_', '');
	}

	private getComponentIdOrNew() {
		if (this.webComponent.id) {
			return 'component-' + this.webComponent.id;
		}

		let idFactory = this.injector.get(IdFactoryService);
		return 'generated-' + idFactory.getNextId();
	}

	private initIsHidden(): void {
		let attributeName = this.getAttributeName();
		this.isHidden = this.eo && attributeName ? this.eo.isAttributeHidden(attributeName) : false;
	}

	private initTooltipFields(): void {
		this.tooltip = this.getTooltip();
		this.tooltipDisabled = this.tooltip === undefined;
		this.initAttributeDescriptionTooltip();
	}

	private initAttributeDescriptionTooltip(): void {
		let attributeName = this.getAttributeName();
		if (!attributeName) {
			return;
		}
		this.eo.getAttributeMeta(attributeName).subscribe(meta => {
			if (meta) {
				this.attributeDescriptionTooltip = meta.getDescription();
			}
		});
	}

	private getTooltip(): string | undefined {
		if (this.webComponent && this.webComponent.descriptionAttr !== undefined) {
			return this.eo.getAttribute(this.webComponent.descriptionAttr)?.replace('\\n', '\n');
		}

		return (
			this.webComponent &&
			this.webComponent.alternativeTooltip &&
			this.webComponent.alternativeTooltip.replace('\\n', '\n')
		);
	}
}
