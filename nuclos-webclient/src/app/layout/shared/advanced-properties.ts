
/**
 * advanced properties from layout (Erweiterte Eigenschaften)
 */
export function getAdvancedProperty(component: WebComponent, key: string): string | undefined {
	if (!component.advancedProperties) {
		return undefined;
	}
	let property = component.advancedProperties.filter(p => p.name === key).shift();
	return property ? property.value : undefined;
}
