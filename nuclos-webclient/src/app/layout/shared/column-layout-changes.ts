import {
	ColDef,
	ColumnEvent,
	ColumnMovedEvent,
	ColumnResizedEvent, ColumnVisibleEvent,
	GridOptions,
	NewColumnsLoadedEvent,
	SortChangedEvent
} from 'ag-grid-community';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ArrayUtils } from '@shared/array-utils';

/**
 * Mixin for handling column-layout changes
 */
export class ColumnLayoutChanges {

	/**
	 * provides column layout changes
	 */
	columnLayoutChanged: Subject<Date> = new Subject<Date>();
	columnLayoutChangedSubscription: Subscription;

	/**
	 * subscribe to column changes like column order, width or sort order
	 * @param {GridOptions} gridOptions
	 * @return {Observable<Date>}
	 */
	onColumnChangesDebounced(gridOptions: GridOptions): Observable<Date> {

		gridOptions.onColumnResized = (event: ColumnResizedEvent) => {
			// update column model
			let colDef = this.getColDef(gridOptions, event);
			if (colDef && event.column !== null) {
				colDef.width = event.column.getActualWidth();
			}
			this.columnLayoutChanged.next(new Date());
		};

		gridOptions.onColumnMoved = (event: ColumnMovedEvent) => {
			// update column model
			let colDef = this.getColDef(gridOptions, event);
			if (gridOptions.columnDefs && colDef) {
				let fromIndex = gridOptions.columnDefs.indexOf(colDef);
				if (event.toIndex) {
					ArrayUtils.move(gridOptions.columnDefs, fromIndex, event.toIndex);
				}
			}
			this.columnLayoutChanged.next(new Date());
		};

		gridOptions.onSortChanged = (_) => {
			this.columnLayoutChanged.next(new Date());
		};

		gridOptions.onNewColumnsLoaded = (_) => {
			this.columnLayoutChanged.next(new Date());
		};

		gridOptions.onColumnVisible = (_) => {
			this.columnLayoutChanged.next(new Date());
		};

		return this.columnLayoutChanged
			.asObservable().pipe(
				debounceTime(500),
				distinctUntilChanged(),
			);
	}

	getColDef(gridOptions: GridOptions, event: ColumnEvent): ColDef | undefined {
		let columnDefs = gridOptions.columnDefs as ColDef[];
		if (columnDefs && event.column !== null) {
			let colDef: ColDef = columnDefs.filter(col => {
				let ret = false;
				if (event.column !== null) {
					ret = col.field === event.column.getColDef().field;
				}
				return ret;
			}).shift() as ColDef;
			return colDef;
		}
		return undefined;
	}

}
