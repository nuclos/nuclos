import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { DataService } from 'app/modules/entity-object-data/shared/data.service';
import { EntityObject } from 'app/modules/entity-object-data/shared/entity-object.class';
import { MenuItem } from 'primeng/api';
import { ContextMenu } from 'primeng/contextmenu';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map, takeWhile } from 'rxjs/operators';

@Component({
	selector: 'nuc-text-module',
	templateUrl: './text-module.component.html',
	styleUrls: ['./text-module.component.css']
})
export class TextModuleComponent implements OnInit, OnDestroy {
	@Input() eo: EntityObject;
	@Input() attributeMetaId;
	@Input() controlType;
	@Input() getCaretPosition: Function;
	@Input() insertText?: Function;
	@Input() setFocus: Function;

	textModules: Subject<MenuItem[]> = new BehaviorSubject([{label: '<no entries>', disabled: true}]);
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	@ViewChild('contextMenu') contextMenu: ContextMenu;

	private alive = true;

	constructor(
		private dataService: DataService,
		public router: Router
	) {
	}

	ngOnInit(): void {
		this.alive = true;
	}

	ngOnDestroy() {
		this.alive = false;
	}

	loadTextModules(event: MouseEvent) {
		let boId = this.eo.getId()?.toString() || 'new';
		let boMetaId = this.eo.getData()?.boMetaId;

		this.dataService.loadTextModules(boMetaId, boId, boMetaId + '_' + this.attributeMetaId, this.controlType)
			.pipe(
				map(textModules => {
					if (textModules.length === 0) {
						return [{label: '<no entries>', disabled: true}];
					}
					let menuItems: MenuItem[] = [];
					textModules.forEach(textModule => {
						let menuItem: MenuItem = {
							label: textModule.label,
							command: () => {
								this.selectTextModule(textModule.text);
							}
						};
						menuItems.push(menuItem);
					});
					return menuItems;
				})
			).subscribe((val) => {
				this.textModules.next(val);
				/**
				 * This function is needed as the triggerEvent 'click'
				 * is in current primeNG version (15.0.1) broken
				 * ---> https://github.com/primefaces/primeng/issues/12458
				 */
				this.contextMenu?.show(event);
				event.preventDefault();
				event.stopPropagation();
			});
	}

	selectTextModule(text: string) {
		let textOld = this.eo.getAttribute(this.attributeMetaId);
		textOld = (textOld === null || textOld === undefined) ? '' : textOld;
		// let caretPos: number | undefined = this.getCaretPosition();
		this.getCaretPosition().pipe(
			takeWhile(() => this.alive)
		).subscribe((caretPos: number | undefined) => {
			if (this.insertText) {
				this.insertText(caretPos, text);
			} else {
				text = caretPos !== undefined ? ([textOld.slice(0, caretPos), text, textOld.slice(caretPos)].join('')) : (textOld + text);
				this.eo.setAttribute(this.attributeMetaId, text);
				this.setFocus(caretPos);
			}
		});
	}
}
