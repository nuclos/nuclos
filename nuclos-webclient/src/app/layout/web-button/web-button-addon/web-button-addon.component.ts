import { Component, ComponentFactoryResolver, Injector, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { take } from 'rxjs/operators';
import { NuclosDialogService } from '../../../core/service/nuclos-dialog.service';
import { AddonService } from '../../../modules/addons/addon.service';
import { Logger } from '../../../modules/log/shared/logger';
import { WebButtonComponentDirective } from '../web-button-component.directive';

@Component({
	selector: 'nuc-web-button-addon',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonAddonComponent extends WebButtonComponentDirective<WebButtonAddon> {
	@ViewChild('addonViewContainerDummy', {read: ViewContainerRef, static: true})
	addonViewContainerDummyRef: ViewContainerRef;

	constructor(
		injector: Injector,
		private addonService: AddonService,
		private dialogService: NuclosDialogService,
		private componentFactoryResolver: ComponentFactoryResolver,
		private $log: Logger
	) {
		super(injector);
	}

	buttonClicked() {
		this.dialogService.updateActiveElement(document.activeElement);
		this.instantiateAddonComponent();
	}

	getCssClass(): string {
		return 'nuc-button-addon';
	}
	isEnabled(): boolean {
		return (
			super.isEnabled() &&
			this.eo &&
			!this.eo.isNew() &&
			this.eo.isAttributeWritable(this.webComponent.name) &&
			this.isWritable() &&
			this.eo.canWrite()
		);
	}

	/**
	 * if addon component uses a template the addon will be opened in a modal
	 * otherwise it will be instantiated invisible
	 */
	private instantiateAddonComponent() {
		const addonComponent = this.injector.get(this.webComponent.addonComponentName + 'Component');
		if (addonComponent) {
			const addonComponentFactory = this.addonService.getComponentFactory(
				this.webComponent.addonComponentName + 'Component'
			);

			const injector = Injector.create({
				providers: []
			});
			const cmpRef = addonComponentFactory.create(injector);

			const showUI: boolean = (<any>cmpRef.instance).hasOwnProperty('showAddonUI') ?
				(<any>cmpRef.instance)['showAddonUI'] : true;
			let componentInstance;
			if (!showUI) {
				// instantiate invisible
				this.addonViewContainerDummyRef.insert(cmpRef.hostView);
				componentInstance = cmpRef.instance;
				componentInstance.eo = this.eo;
			} else {
				// open addon in modal
				this.dialogService.custom(addonComponent, {
					'eo': this.eo
				}, {
					size: 'lg',
					windowClass: 'fullsize-modal-window'
				}).pipe(
					take(1)
				).subscribe();
			}
		} else {
			const message = 'Unable to open addon: ' + this.webComponent.addonComponentName;
			this.$log.error(message);
			this.dialogService.alert({title: 'Error', message: message})
				.pipe(
					take(1)
				).subscribe();
		}
	}
}
