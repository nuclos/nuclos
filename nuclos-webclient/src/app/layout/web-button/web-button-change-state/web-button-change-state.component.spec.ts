/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { WebButtonChangeStateComponent } from './web-button-change-state.component';

xdescribe('WebButtonChangeStateComponent', () => {
	let component: WebButtonChangeStateComponent;
	let fixture: ComponentFixture<WebButtonChangeStateComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebButtonChangeStateComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebButtonChangeStateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
