import { Component, Injector } from '@angular/core';
import { SubEntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { State } from '@modules/state/shared/state';
import { take } from 'rxjs/operators';
import { WebButtonComponentDirective } from '../web-button-component.directive';

@Component({
	selector: 'nuc-web-button-change-state',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonChangeStateComponent extends WebButtonComponentDirective<WebButtonChangeState> {

	constructor(
		injector: Injector
	) {
		super(injector);
	}

	buttonClicked() {
		let targetState: State = {
			nuclosStateId: this.webComponent.targetState
		} as State;
		this.eo.changeState(targetState).pipe(take(1)).subscribe();
	}

	getCssClass(): string {
		return 'nuc-button-change-state';
	}

	/**
	 * This button is enabled if the target state is a possible next state for the EO
	 * and the EO is in its own context and not a subEO of another one.
	 *
	 * @returns {boolean}
	 */
	isEnabled(): boolean {
		if (!this.eo) {
			return false;
		}

		if (this.eo instanceof SubEntityObject) {
			return false;
		}

		let nextStates = this.eo.getNextStates();
		if (!nextStates) {
			return false;
		}

		for (let nextState of nextStates) {
			if (nextState.nuclosStateId === this.webComponent.targetState) {
				return true;
			}
		}

		return false;
	}
}
