import { Directive, Injector, OnInit } from '@angular/core';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { AbstractWebComponentDirective } from '../shared/abstract-web-component.directive';

/**
 * Abstract base for different kinds of buttons (state-change, custom-rule, etc.).
 */
@Directive()
export abstract class WebButtonComponentDirective<T extends WebButton> extends AbstractWebComponentDirective<T> implements OnInit {

	loading = false;

	private config: NuclosConfigService;

	constructor(injector: Injector) {
		super(injector);

		this.config = injector.get(NuclosConfigService);
	}

	ngOnInit() {
	}

	abstract buttonClicked();

	abstract getCssClass(): string;

	getStyle(): any {
		let style = super.getStyle();

		style['overflow'] = 'hidden';
		style['white-space'] = 'nowrap';
		style['text-overflow'] = 'ellipsis';

		return style;
	}

	getIconMaxHeight() {
		let iconHeight = +`${this.getStyle()['min-height']}`.replace('px', '');

		return `${iconHeight - 4}px`;
	}

	getLoadingIconSize() {
		let iconHeight = +`${this.getStyle()['min-height']}`.replace('px', '');
		if (iconHeight - 20 < 12) {
			return '12px';
		} else {
			return `${iconHeight - 20}px`;
		}
	}

	getLabelFlexGrow() {
		if (this.loading && this.hasIcon()) {
			return 0.25;
		} else if (this.loading) {
			return 0.5;
		} else if (this.hasIcon()) {
			return 0.25;
		} else {
			return 1;
		}
	}

	isEnabled(): boolean {
		return this.webComponent && this.webComponent.enabled
			&& (!this.webComponent.disableDuringEdit || !this.eo.isDirty());
	}

	hasIcon() {
		return !!this.webComponent.icon;
	}

	getIconUrl() {
		return this.config.getResourceURL(this.webComponent.icon);
	}

	getLabel() {
		if (this.webComponent && this.webComponent.labelAttr) {
			return this.getLabelFromAttribute();
		}
		return this.webComponent && this.webComponent.label;
	}

	private getLabelFromAttribute() {
		let attributeValue = this.eo.getAttribute(this.webComponent.labelAttr);
		if (attributeValue) {
			if (attributeValue !== Object(attributeValue)) {
				return String(attributeValue).replace('\\n', '\n');
			} else if (attributeValue['name']) {
				return attributeValue.name;
			}
		}
		return undefined;
	}

}
