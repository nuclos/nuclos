import { Component, Injector } from '@angular/core';
import { WebButtonComponentDirective } from '../web-button-component.directive';

@Component({
	selector: 'nuc-web-button-dummy',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonDummyComponent extends WebButtonComponentDirective<WebButtonDummy> {

	constructor(injector: Injector) {
		super(injector);
	}

	buttonClicked() {
		alert('Dummy!');
	}

	getCssClass(): string {
		return 'nuc-button-dummy';
	}
}
