/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { WebButtonExecuteRuleComponent } from './web-button-execute-rule.component';

xdescribe('WebButtonExecuteRuleComponent', () => {
	let component: WebButtonExecuteRuleComponent;
	let fixture: ComponentFixture<WebButtonExecuteRuleComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebButtonExecuteRuleComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebButtonExecuteRuleComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
