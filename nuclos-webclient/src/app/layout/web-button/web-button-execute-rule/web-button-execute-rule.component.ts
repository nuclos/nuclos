import { Component, Injector } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { finalize, take } from 'rxjs/operators';
import { WebButtonComponentDirective } from '../web-button-component.directive';

@Component({
	selector: 'nuc-web-button-execute-rule',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonExecuteRuleComponent extends WebButtonComponentDirective<WebButtonExecuteRule> {

	constructor(
		injector: Injector,
		private dialogService: NuclosDialogService
	) {
		super(injector);
	}

	buttonClicked() {
		this.loading = true;
		this.dialogService.updateActiveElement(document.activeElement);
		this.eo.executeRule(this.webComponent.rule).pipe(
			take(1),
			finalize(() => this.loading = false)
		).subscribe(eo => {
			if (this.eo.getState() !== eo.getState()) {
				this.eo.reloaded();
			}
		});
	}

	getCssClass(): string {
		return 'nuc-button-execute-rule';
	}

	isEnabled(): boolean {
		return super.isEnabled()
			&& this.isWritable()
			&& this.eo
			&& this.eo.isAttributeWritable(this.webComponent.name)
			&& this.eo.canWrite();
	}
}

