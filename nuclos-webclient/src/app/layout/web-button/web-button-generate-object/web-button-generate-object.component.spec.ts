/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { WebButtonGenerateObjectComponent } from './web-button-generate-object.component';

xdescribe('WebButtonGenerateObjectComponent', () => {
	let component: WebButtonGenerateObjectComponent;
	let fixture: ComponentFixture<WebButtonGenerateObjectComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebButtonGenerateObjectComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebButtonGenerateObjectComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
