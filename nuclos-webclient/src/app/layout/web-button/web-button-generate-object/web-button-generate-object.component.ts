import { Component, Injector } from '@angular/core';
import { finalize, take } from 'rxjs/operators';
import { NuclosGenerationService } from '../../../modules/generation/shared/nuclos-generation.service';
import { Logger } from '../../../modules/log/shared/logger';
import { WebButtonComponentDirective } from '../web-button-component.directive';

@Component({
	selector: 'nuc-web-button-generate-object',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonGenerateObjectComponent extends WebButtonComponentDirective<WebButtonGenerateObject> {

	constructor(
		injector: Injector,
		private generationService: NuclosGenerationService,
		private $log: Logger
	) {
		super(injector);
	}

	getGeneration() {
		return this.eo.getGenerations().find(
			it => it.generationId === this.webComponent.objectGenerator
		);
	}

	buttonClicked() {
		let generation = this.getGeneration();

		if (!generation) {
			this.$log.error('Unknown generation: %o', this.webComponent.objectGenerator);
			return;
		}

		this.loading = true;
		this.generationService.confirmAndGenerate(
			this.eo,
			generation,
			this.getAdvancedProperty('popupparameter'),
			false,
			document.activeElement
		).pipe(
			take(1),
			finalize(() => this.loading = false)
		).subscribe();
	}

	getCssClass(): string {
		return 'nuc-button-generate-object';
	}

	isEnabled() {
		return super.isEnabled() && this.getGeneration() !== undefined
			&& this.eo.isAttributeWritable(this.webComponent.name) && (this.eo.isVirtual() || this.eo.canWrite());
	}
}
