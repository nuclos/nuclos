/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { WebButtonHyperlinkComponent } from './web-button-hyperlink.component';

xdescribe('WebButtonHyperlinkComponent', () => {
	let component: WebButtonHyperlinkComponent;
	let fixture: ComponentFixture<WebButtonHyperlinkComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebButtonHyperlinkComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebButtonHyperlinkComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
