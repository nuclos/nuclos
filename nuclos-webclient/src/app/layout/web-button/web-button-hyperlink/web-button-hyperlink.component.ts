import { Component, Injector } from '@angular/core';
import { HyperlinkService } from '../../../shared/service/hyperlink.service';
import { WebButtonComponentDirective } from '../web-button-component.directive';

@Component({
	selector: 'nuc-web-button-hyperlink',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonHyperlinkComponent extends WebButtonComponentDirective<WebButtonHyperlink> {
	constructor(
		injector: Injector,
		private hyperlinkService: HyperlinkService
	) {
		super(injector);
	}

	getCssClass(): string {
		return 'nuc-button-hyperlink';
	}

	buttonClicked() {
		this.openLink();
	}

	openLink() {
		let url = this.getUrl();
		if (url) {
			this.hyperlinkService.open(url);
		}
	}

	getUrl(): string {
		let url = this.getHyperlinkAttribute();

		if (url) {
			url = this.hyperlinkService.validateURL(url);
		}

		return url;
	}

	isEnabled(): boolean {
		return super.isEnabled() && !!this.getUrl();
	}

	private getHyperlinkAttribute(): any {
		return this.eo.getAttribute(this.webComponent.hyperlinkField);
	}
}
