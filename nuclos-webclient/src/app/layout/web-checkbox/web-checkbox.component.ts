import { Component, Injector, OnInit } from '@angular/core';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';

@Component({
	selector: 'nuc-web-checkbox',
	templateUrl: './web-checkbox.component.html',
	styleUrls: ['./web-checkbox.component.css']
})
export class WebCheckboxComponent extends AbstractInputComponentDirective<WebCheckbox> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

	getStyle(): any {
		let parentStyles = super.getStyle();

		if (this.tooltip !== undefined) {
			parentStyles['display'] = 'flex';
			parentStyles['align-items'] = 'center';
		}

		return parentStyles;
	}

}
