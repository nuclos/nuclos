import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WebColorchooserComponent } from './web-colorchooser.component';

xdescribe('WebColorchooserComponent', () => {
	let component: WebColorchooserComponent;
	let fixture: ComponentFixture<WebColorchooserComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebColorchooserComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebColorchooserComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
