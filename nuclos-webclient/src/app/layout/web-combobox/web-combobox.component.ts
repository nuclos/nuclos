import { Component, ElementRef, Injector, OnInit } from '@angular/core';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { LovDataService } from '@modules/entity-object-data/shared/lov-data.service';
import { ReferenceTargetService } from '@modules/entity-object-data/shared/reference-target.service';
import { Logger } from '@modules/log/shared/logger';
import { FqnService } from '@shared/service/fqn.service';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';
import { AbstractLovComponentDirective } from '../abstract-lov/abstract-lov-component.directive';
import { ComboboxUtils } from '../shared/combobox-utils';

@Component({
	selector: 'nuc-web-combobox',
	templateUrl: '../abstract-lov/abstract-lov.component.html',
	styleUrls: ['../abstract-lov/abstract-lov.component.scss', '../abstract-lov/abstract-lov.component.css']
})
export class WebComboboxComponent extends AbstractLovComponentDirective<WebCombobox> implements OnInit {

	constructor(
		lovDataService: LovDataService,
		injector: Injector,
		fqnService: FqnService,
		elementRef: ElementRef,
		referenceTargetService: ReferenceTargetService,
		nuclosHotkeysService: NuclosHotkeysService,
		i18nService: NuclosI18nService,
		$log: Logger
	) {
		super(lovDataService, injector, fqnService, elementRef, referenceTargetService, nuclosHotkeysService, i18nService, $log);

		this.handler = {
			loadEntries: () => this.loadEntries(),
			loadFilteredEntries: search => this.loadFilteredEntries(search),
			getValue: () => this.getValue(),
			refreshEntries: () => this.doRefreshEntries(),
			getVlpId: () => this.getVlpId(),
			getVlpParams: () => this.getVlpParams()
		};
	}

	ngOnInit() {
		super.ngOnInit();
	}

	loadEntries() {
		return this.attributeMeta ? this.eo.getLovEntries(
			this.attributeMeta.getAttributeID(),
			this.webComponent.valuelistProvider
		).pipe(take(1)) : of(this.lovEntries);
	}

	loadFilteredEntries(search: string) {
		return ComboboxUtils.filter(this.loadEntries(), search);
	}
}
