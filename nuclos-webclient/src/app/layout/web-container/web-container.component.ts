import { Component, Injector, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AbstractWebComponentDirective } from '../shared/abstract-web-component.directive';

@Component({
	selector: 'nuc-web-container',
	templateUrl: './web-container.component.html',
	styleUrls: ['./web-container.component.css']
})
export class WebContainerComponent extends AbstractWebComponentDirective<WebContainer>
	implements OnInit, OnChanges {
	contentClasses;

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
		this.updateContentClasses();
	}

	ngOnChanges(changes: SimpleChanges): void {
		super.ngOnChanges(changes);
		this.updateContentClasses();
	}

	toggle() {
		// TODO
	}

	getGrid() {
		return this.webComponent.grid;
	}

	getTable() {
		return this.webComponent.table;
	}

	getCalculated() {
		return this.webComponent.calculated;
	}

	getComponents() {
		return this.webComponent.components;
	}

	private updateContentClasses() {
		this.contentClasses = {
			'tab-content': true,
			opaque: this.webComponent.opaque
		};
	}
}
