import { Component, EventEmitter, Injector, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { IEntityObject, IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { DatechooserComponent } from '../../modules/ui-components/datechooser/datechooser.component';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';

@Component({
	selector: 'nuc-web-datechooser',
	templateUrl: './web-datechooser.component.html',
	styleUrls: ['./web-datechooser.component.css']
})
export class WebDatechooserComponent extends AbstractInputComponentDirective<WebDatechooser> implements OnInit, OnChanges {

	@Output() onValueChange = new EventEmitter();

	@ViewChild('nucDatechooser') datechooser: DatechooserComponent;

	private eoListener: IEntityObjectEventListener = {
		afterAttributeChange: (
			entityObject: IEntityObject,
			attributeName: string,
			oldValue: any,
			newValue: any
		) => {
			// delete input text if attribute field is deleted
			if (attributeName === this.getName() && newValue === null || newValue === undefined) {
				this.setInputText('');
			}
		}
	};

	constructor(
		injector: Injector
	) {
		super(injector);
	}

	ngOnInit() {
		this.eo.addListener(this.eoListener);
	}

	ngOnChanges(changes: SimpleChanges) {
		super.ngOnChanges(changes);
		let eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				eoChange.previousValue.removeListener(this.eoListener);
			}
			if (eoChange.currentValue) {
				eoChange.currentValue.addListener(this.eoListener);
			}
		}
	}

	setAttributeValue(value?: string) {
		this.eo.setAttribute(this.webComponent.name, value);
		this.onValueChange.next(true);
	}

	focusInput() {
		this.datechooser.focusInput();
	}

	selectInputText() {
		this.datechooser.selectInputText();
	}

	setInputText(value: string) {
		this.datechooser.setInputText(value);
	}

	commitValue() {
		this.datechooser.commitValue();
	}

	getModel() {
		return this.getModelValue();
	}

	isValid() {
		return this.datechooser && this.datechooser.isValid();
	}

	getRawInput() {
		return this.datechooser.getRawInput();
	}

	getValidationCssClasses(): any {
		return super.getValidationCssClasses();
	}
}
