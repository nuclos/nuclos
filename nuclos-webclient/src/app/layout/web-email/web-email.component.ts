import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NuclosValidationService } from '../../shared/service/nuclos-validation.service';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';

@Component({
	selector: 'nuc-web-email',
	templateUrl: './web-email.component.html',
	styleUrls: ['./web-email.component.css']
})
export class WebEmailComponent extends AbstractInputComponentDirective<WebEmail> implements OnInit {
	@ViewChild('emailInput') emailInput: ElementRef;

	editing = false;
	mouseover = false;

	constructor(
		injector: Injector,
		private validationService: NuclosValidationService,
		public router: Router
	) {
		super(injector);
	}

	ngOnInit() {
	}

	focus() {
		if (!this.mouseover) {
			this.startEditing();
		}
	}

	stopEditing() {
		this.editing = false;
	}

	startEditing() {
		if (!this.isWritable()) {
			return;
		}
		this.editing = true;
		setTimeout(() => this.emailInput!.nativeElement.focus(), 0);
	}

	toggleEditing() {
		if (this.editing) {
			this.stopEditing();
		} else {
			this.startEditing();
		}
	}

	getUrl(): string {
		let url = this.getEmailAttribute();

		if (url) {
			url = 'mailto:' + url;
		}

		return url;
	}

	isValid(): boolean {
		return this.validationService.isValidForInput(this.getModelValue(), 'email');
	}

	getCssClasses(): any {
		let cssClasses = this.getValidationCssClasses();
		cssClasses['nuc-readonly'] = !this.isWritable();
		return cssClasses;
	}

	getEmailAttribute(): any {
		return this.eo.getAttribute(this.webComponent.name);
	}

	setEmailAttribute(email: string) {
		this.eo.setAttribute(this.webComponent.name, email);
	}
}
