import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { WebGridCalculatedComponent } from './web-grid-calculated.component';

xdescribe('WebGridCalculatedComponent', () => {
	let component: WebGridCalculatedComponent;
	let fixture: ComponentFixture<WebGridCalculatedComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebGridCalculatedComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebGridCalculatedComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
