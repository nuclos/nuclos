import {
	AfterContentInit,
	Component,
	HostListener,
	Input,
	OnChanges,
	OnInit,
	SimpleChanges
} from '@angular/core';
import { Logger } from '@modules/log/shared/logger';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-web-grid-calculated',
	templateUrl: './web-grid-calculated.component.html',
	styleUrls: ['./web-grid-calculated.component.css']
})
export class WebGridCalculatedComponent implements OnInit, OnChanges, AfterContentInit {

	@Input() webGridCalculated: WebGridCalculated;
	@Input() eo: EntityObject;

	cellStyles;

	isResponsiveLayoutEnabled = (document.body.clientWidth <= 560);
	responsiveLayoutSorted: Array<Array<WebComponent>> = [];

	constructor(
		private $log: Logger,
	) {
	}

	ngOnInit() {
		this.updateStyles();
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.updateStyles();
	}

	ngAfterContentInit () {
		setTimeout(() => {
			// sort cells into col/rows for responsive layout
			this.responsiveLayoutSorted = [];
			if (this.webGridCalculated.cells?.length > 0) {
				this.webGridCalculated.cells.forEach((comp: WebCalcCell) => {
					let compResponsiv = {...comp.components[0]};
					compResponsiv.minimumWidth = 0;
					compResponsiv.minimumHeight = 0;
					compResponsiv.preferredHeight = 0;
					compResponsiv.preferredWidth = 0;
					if (this.responsiveLayoutSorted[compResponsiv.row ] === undefined) {
						// need to add new row
						this.responsiveLayoutSorted[compResponsiv.row] = [compResponsiv];
					} else {
						let columns = this.responsiveLayoutSorted[compResponsiv.row ]?.sort(
							(a, b) => a.column - b.column
						) ?? [];
						columns.push(compResponsiv);
						this.responsiveLayoutSorted[compResponsiv.row] = columns;
					}
				});
				this.isResponsiveLayoutEnabled = document.body.clientWidth <= 560;
				this.$log.debug('calc responsive', this.responsiveLayoutSorted);
			}
		});
	}

	@HostListener('window:resize')
	checkIfResponsiveLayoutRequired() {
		this.isResponsiveLayoutEnabled = document.body.clientWidth <= 560;
	}

	getStyleForComponent(comp: WebComponent): any {
		return comp.horizontalAlign === undefined || comp.verticalAlign === undefined ? {} : {
			'display': 'flex',
			'align-items': comp.verticalAlign,
			'justify-content': comp.horizontalAlign,
			'width': '100%',
			'height': '100%'
		};
	}

	/**
	 * Update preferred height/width from component to cell height/width if it does not contain calculate css.
	 * We can do this, as it is almost impossible to have to components in one cell.
	 * @param comp
	 * @param cellStyle
	 */
	getWebComponentForView(comp: WebComponent, cellStyle: any): WebComponent {
		if (cellStyle === undefined) {
			return comp;
		}

		if (cellStyle.hasOwnProperty('height') &&
			!cellStyle['height'].includes('calc') &&
			comp.verticalAlign === 'stretch') {
			comp.preferredHeight = +`${cellStyle['height'].replace('px', '')}`;
		} else {
			comp['calculatedHeight'] = cellStyle['height'];
		}
		if (cellStyle.hasOwnProperty('width') &&
			!cellStyle['width'].includes('calc') &&
			comp.horizontalAlign === 'stretch') {
			comp.preferredWidth = +`${cellStyle['width'].replace('px', '')}`;
		} else {
			comp['calculatedWidth'] = cellStyle['width'];
		}

		if (cellStyle.hasOwnProperty('minWidth')) {
			comp.minimumWidth = +`${cellStyle['minWidth'].replace('px', '')}`;
		}

		if (cellStyle.hasOwnProperty('minHeight')) {
			comp.minimumHeight = +`${cellStyle['minHeight'].replace('px', '')}`;
		}
		return comp;
	}

	private updateStyles() {
		this.cellStyles = [];
		if (this.webGridCalculated && this.webGridCalculated.cells) {
			for (let cell of this.webGridCalculated.cells) {
				this.cellStyles.push(
					{
						position: 'absolute',
						left: cell.left,
						top: cell.top,
						width: cell.width,
						height: cell.height,
						minWidth: cell.width,
						minHeight: cell.height
					}
				);
			}
		}
	}
}
