import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WebHtmlEditorComponent } from './web-html-editor.component';

xdescribe('WebHtmlEditorComponent', () => {
	let component: WebHtmlEditorComponent;
	let fixture: ComponentFixture<WebHtmlEditorComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebHtmlEditorComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebHtmlEditorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
