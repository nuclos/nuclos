import {
	AfterViewInit,
	Component,
	ElementRef,
	Injector,
	OnChanges,
	OnDestroy,
	OnInit,
	QueryList,
	SimpleChanges,
	ViewChildren
} from '@angular/core';
import { Editor } from 'primeng/editor';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { map, take, takeWhile } from 'rxjs/operators';
import { IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';

@Component({
	selector: 'nuc-web-html-editor',
	templateUrl: './web-html-editor.component.html',
	styleUrls: ['./web-html-editor.component.css']
})
export class WebHtmlEditorComponent extends AbstractInputComponentDirective<WebHtmlEditor> implements
	OnInit, OnChanges, OnDestroy, AfterViewInit {
	@ViewChildren('editor') editorComps: QueryList<Editor>;

	editor: Subject<Editor> = new ReplaySubject<Editor>(1);
	editorValue: String;

	getCaretPosition: Function;
	insertText: Function;
	setFocus: Function;

	private eoListener: IEntityObjectEventListener = {
		afterCancel: () => {
			this.editorValue = this.getValue();
		},
		afterValidationChange: () => {
			this.editorValue = this.getValue();
		},
		afterAttributeChange: (entityObject, attributeName, oldValue, newValue) => {
			if (attributeName === this.getName()) {
				this.editorValue = this.getValue();
			}
		}
	};

	private editorHeightCalculated: number;
	private alive = true;

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
		this.editorValue = this.getValue();
		this.eo.addListener(this.eoListener);

		this.getCaretPosition = this._getCaretPosition.bind(this);
		this.insertText = this._insertText.bind(this);
		this.setFocus = this._setFocus.bind(this);
		this.alive = true;
	}

	ngOnDestroy() {
		this.eo.removeListener(this.eoListener);
		this.alive = false;
	}

	ngOnChanges(changes: SimpleChanges) {
		const eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				eoChange.previousValue.removeListener(this.eoListener);
			}
			if (eoChange.currentValue) {
				eoChange.currentValue.addListener(this.eoListener);
			}
			this.editorValue = this.getValue();
		}
	}

	getStyle(): any {
		let style = super.getStyle();

		// preferredHeight will contain the cell height if can be calculated or
		// the preferredHeight from component layout
		style['height'] = `${this.webComponent.preferredHeight - 44}px`;

		return style;
	}

	ngAfterViewInit(): void {
		if (this.editorComps) {
			if (this.editorComps.length > 0) {
				this.editor.next(this.editorComps.first);
			}
			this.editorComps.changes.pipe(
				takeWhile(() => this.alive)
			).subscribe((comps: QueryList<Editor>) => {
				if (comps.length > 0) {
					this.editor.next(comps.first);
				}
			});
		}
	}

	onChangeCallback($event) {
		if ($event.htmlValue !== this.getValue()) {
			this.setValue($event.htmlValue);
		}
	}

	hasTextModules() {
		return this.webComponent && this.webComponent.hasTextModules;
	}

	private _getCaretPosition(): Observable<number | undefined> {
		return this.editor.pipe(
			takeWhile(() => this.alive)
		).pipe(map((editor: Editor) => editor.getQuill().getSelection(true)?.index));
	}

	private _insertText(idx: number, text: String) {
		this.editor.pipe(
			takeWhile(() => this.alive)
		).subscribe((editor: Editor) => editor.getQuill().insertText(idx, text, 'user'));
	}

	private _setFocus(idx?: number): void {
		this.editor.pipe(
			take(1)
		).subscribe((editor: Editor) => {
			if (idx) {
				editor.getQuill().setSelection(idx, 0, 'user');
			}
		});
	}
}
