import { Component, Injector, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { AbstractWebComponentDirective } from '../shared/abstract-web-component.directive';

@Component({
	selector: 'nuc-web-html-field',
	templateUrl: './web-html-field.component.html',
	styleUrls: ['./web-html-field.component.scss']
})
export class WebHtmlFieldComponent extends AbstractWebComponentDirective<WebHtmlField>
	implements OnInit, OnChanges {

	htmlContent: string | undefined;

	private eoListener: IEntityObjectEventListener = {
		afterCancel: () => {
			this.updateInnerHtml();
		},
		afterValidationChange: () => {
			this.updateInnerHtml();
		},
		afterAttributeChange: (entityObject, attributeName, oldValue, newValue) => {
			if (attributeName === this.getName()) {
				this.updateInnerHtml();
			}
		}
	};

	constructor(
		injector: Injector) {
		super(injector);
	}

	ngOnInit(): void {
		this.updateInnerHtml();
	}

	ngOnChanges(changes: SimpleChanges) {
		const eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				eoChange.previousValue.removeListener(this.eoListener);
			}
			if (eoChange.currentValue) {
				eoChange.currentValue.addListener(this.eoListener);
			}
			this.updateInnerHtml();
		}
	}

	private updateInnerHtml(): void {
		if (this.webComponent.text !== undefined) {
			this.setHtml(
				this.decodeBase64Unicode(this.webComponent.text)
			);
		} else if (this.getValue() !== undefined && this.getValue() !== null) {
			this.setHtml(
				this.getValue()
			);
		} else {
			this.setHtml('');
		}
	}

	private setHtml(input: string): void {
		this.htmlContent = input;
	}

	private decodeBase64Unicode(base64Encoded: string): string {
		const text = atob(base64Encoded);
		const length = text.length;
		const bytes = new Uint8Array(length);
		for (let i = 0; i < length; i++) {
			bytes[i] = text.charCodeAt(i);
		}
		const decoder = new TextDecoder(); // default is utf-8
		return decoder.decode(bytes);
	}
}
