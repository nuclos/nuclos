/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { WebHyperlinkComponent } from './web-hyperlink.component';

xdescribe('WebHyperlinkComponent', () => {
	let component: WebHyperlinkComponent;
	let fixture: ComponentFixture<WebHyperlinkComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebHyperlinkComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebHyperlinkComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
