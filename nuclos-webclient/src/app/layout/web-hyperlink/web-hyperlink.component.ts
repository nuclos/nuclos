import { Component, ElementRef, HostBinding, Injector, OnInit, ViewChild } from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { HyperlinkService } from '../../shared/service/hyperlink.service';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';

@Component({
	selector: 'nuc-web-hyperlink',
	templateUrl: './web-hyperlink.component.html',
	styleUrls: ['./web-hyperlink.component.css']
})
export class WebHyperlinkComponent extends AbstractInputComponentDirective<WebHyperlink> implements OnInit {

	static readonly ANCHOR_TAG_REGEX = '[ ]*<a.*href=\"(.*)\"[^>]*>(.*)</a>[ ]*';

	@ViewChild('hyperlinkInput') hyperlinkInput: ElementRef;

	editing = false;
	mouseover = false;
	showCompleteLink = false;

	constructor(
		injector: Injector,
		private hyperlinkService: HyperlinkService,
		public router: Router
	) {
		super(injector);
	}

	ngOnInit() {
	}

	focusLink() {
		if (!this.mouseover) {
			this.startEditing();
		}
	}

	stopEditing() {
		this.editing = false;
	}

	startEditing() {
		if (!this.isWritable()) {
			return;
		}
		this.editing = true;
		setTimeout(() => this.hyperlinkInput!.nativeElement.focus(), 0);
	}

	toggleEditing() {
		if (this.editing) {
			this.stopEditing();
		} else {
			this.startEditing();
		}
	}

	getUrl(): string | SafeUrl | null {
		let url = this.getHyperlinkAttribute();

		let match = url?.match(WebHyperlinkComponent.ANCHOR_TAG_REGEX);
		if (match) {
			url = match[1];
		}

		if (url) {
			url = this.hyperlinkService.getSafeUrl(url);
		}

		return url;
	}

	getHyperlinkTitle(): string {
		let hyperlink = this.getHyperlinkAttribute();
		let match = hyperlink.match(WebHyperlinkComponent.ANCHOR_TAG_REGEX);
		if (!this.showCompleteLink && match) {
			return match[2];
		} else {
			return hyperlink;
		}
	}

	getCssClasses(): any {
		let cssClasses = this.getValidationCssClasses();
		cssClasses['nuc-readonly'] = !this.isWritable();
		return cssClasses;
	}

	getHyperlinkAttribute(): any {
		return this.eo.getAttribute(this.webComponent.name);
	}

	setHyperlinkAttribute(link: string) {
		this.eo.setAttribute(this.webComponent.name, link);
	}
}
