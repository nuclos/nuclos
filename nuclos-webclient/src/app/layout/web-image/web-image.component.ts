import { HttpClient } from '@angular/common/http';
import {
	Component,
	Inject,
	Injector,
	NgZone,
	OnChanges,
	OnDestroy,
	OnInit,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { Logger } from '@modules/log/shared/logger';
import { IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { NuclosDropUploadDirective } from '@shared/directives/nuclos-drop-upload.directive';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { MenuItem } from 'primeng/api';
import { ContextMenu } from 'primeng/contextmenu';
import { of, Subject } from 'rxjs';
import { catchError, take } from 'rxjs/operators';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';
import { ImageService } from '../shared/image.service';

@Component({
	selector: 'nuc-web-image',
	templateUrl: './web-image.component.html',
	styleUrls: ['./web-image.component.scss']
})
export class WebImageComponent extends AbstractInputComponentDirective<WebImage> implements OnInit, OnChanges, OnDestroy {

	@ViewChild('imageElement') imageElement: NuclosDropUploadDirective;
	@ViewChild('contextMenu') contextMenu: ContextMenu;
	@ViewChild('htmlContainer') container;

	imageUrl: string | undefined;
	attributeNameId: string;
	enabled = true;
	loading = false;
	fileEvent$: Subject<File> = new Subject<File>();
	imgSizeHeight: string;
	writeable = false;
	isSVGInteraction = false;
	svgContent: SafeHtml | undefined;

	contextMenuItems: MenuItem[] = [
		{
			label: this.i18nService.getI18n('webclient.imageupload.replaceimage'),
			icon: 'pi pi-plus',
			visible: this.enabled,
			command: () => {
				let input = document.createElement('input');
				input.type = 'file';

				input.onchange = e => {
					// @ts-ignore
					let file = e?.target?.files[0];
					this.fileEvent$.next(file);
				};

				input.click();
			}
		},
		{
			label: this.i18nService.getI18n('webclient.imageupload.showimage'),
			icon: 'pi pi-save',
			command: () => {
				window.open(this.imageUrl, '_blank');
			}
		},
		{
			label: this.i18nService.getI18n('webclient.imageupload.removeimage'),
			icon: 'pi pi-trash',
			visible: this.enabled,
			command: () => {
				this.eo.setAttribute(this.webComponent.name, null);
				this.imageUrl = undefined;
			}
		}
	];

	private eoListener: IEntityObjectEventListener = {
		afterSave: () => this.initComponent(this.eo),
		afterCancel: () => this.initComponent(this.eo),
		afterReload: () => this.initComponent(this.eo),
		afterValidationChange: () => this.initComponent(this.eo),
		afterAttributeChange: () => this.initComponent(this.eo)
	};

	constructor(
		injector: Injector,
		protected nuclosConfigService: NuclosConfigService,
		private dialogService: NuclosDialogService,
		private nuclosI18nService: NuclosI18nService,
		private imageService: ImageService,
		private i18nService: NuclosI18nService,
		private $log: Logger,
		public sanitizer: DomSanitizer,
		private httpClient: HttpClient,
		@Inject(NgZone) private zone: NgZone
	) {
		super(injector);
	}

	ngOnInit(): void {
		if (this.eo) {
			this.eo.addListener(this.eoListener);
			this.initComponent(this.eo);
			this.writeable = this.isWritable();
			this.isSVGInteraction = this.webComponent.name.endsWith('isexecutable');
		}
	}

	ngOnDestroy() {
		this.removeJavaScript(document.getElementsByTagName('head')[0]);
	}

	ngOnChanges(changes: SimpleChanges): void {
		super.ngOnChanges(changes);
		let eoChange = changes['eo'];
		if (eoChange) {
			let previousEo = eoChange.previousValue;
			if (previousEo) {
				previousEo.removeListener(this.eoListener);
			}
			let currentEo = eoChange.currentValue;
			if (currentEo) {
				this.initComponent(eoChange.currentValue);
				currentEo.addListener(this.eoListener);
			}
		}
	}

	getStyle(): any {
		let style = super.getStyle();
		style = {
			...style,
			'display': 'flex',
			'flex-wrap:': 'wrap',
			'height': this.imgSizeHeight,
			'text-align': 'center',
			'align-self': this.webComponent.verticalAlign,
			'justify-content': this.webComponent.horizontalAlign
		};

		return style;
	}

	getImageUrl(eo: EntityObject) {
		return this.imageService.getImageHref(eo, this.webComponent.name);
	}

	setAttribute($event: { uploadToken?: string; file: File; error?: string; status: 'UPLOADING' | 'FINISHED' }) {
		if ($event.status === 'FINISHED') {
			this.loading = false;
			if ($event.uploadToken && $event.error === undefined) {
				this.$log.debug('Setting value to: %o', $event);
				this.eo.setAttribute(this.webComponent.name, {
					uploadToken: $event.uploadToken,
					name: $event.file.name
				});
				this.imageUrl = this.nuclosConfigService.getRestHost() + '/boImages/temp/' + $event.uploadToken;
			}
		} else {
			// loading
			this.loading = true;
		}
	}

	updateFileForTest($event) {
		if ($event.target.files.length > 0) {
			this.fileEvent$.next($event.target.files[0]);
		}
	}

	/**
	 * This function is needed as the triggerEvent 'click'
	 * is in current primeNG version (15.0.1) broken
	 * ---> https://github.com/primefaces/primeng/issues/12458
	 * @param $event
	 */
	showContextMenu($event: MouseEvent) {
		this.contextMenu?.show($event);
		$event.preventDefault();
		$event.stopPropagation();
	}

	private initComponent(eo: EntityObject) {
		this.imageUrl = this.getImageUrl(eo);
		this.enabled = this.isWritable();
		this.attributeNameId = this.webComponent?.name;
		// calculatedHeight is be set in web-grid-calculated.component.ts
		if (this.webComponent.verticalAlign !== 'stretch') {
			this.imgSizeHeight = 'fit-content';
		} else if (this.webComponent.preferredHeight) {
			this.imgSizeHeight = `${this.webComponent?.preferredHeight}px`;
		} else if (this.webComponent.minimumHeight) {
			this.imgSizeHeight = `${this.webComponent?.minimumHeight}px`;
		} else {
			this.imgSizeHeight = this.webComponent['calculatedHeight'];
		}
		this.svgContent = undefined;
		if (this.imageUrl && this.isSVGInteraction) {
			this.httpClient.get(
				this.imageUrl,
				{
					headers: {
						'Content-Type': 'image/svg+xml'
					},
					responseType: 'text'
				}
			).pipe(
				take(1),
				catchError(() => {
					return of(undefined);
				})
			).subscribe((data: any) => {
				this.svgContent = this.sanitizer.bypassSecurityTrustHtml(data);

				/**
				 * This needs to be done to execute javascript from SVG.
				 * Since HTML5 script Tags are not allowed to be present in DOM body and wont execute.
				 * So we are grabbing the stuff out and put it into HEAD.
				 */
				setTimeout(() => {
					const scriptNodes = this.container?.nativeElement?.getElementsByTagName('script');
					if (scriptNodes) {
						for (let i = 0; i < scriptNodes.length; i++) {
							const scriptNode = scriptNodes[i];
							// Create a new script element so HTML5 will execute it upon adding to DOM
							const scriptElement = document.createElement('script');
							// Copy all the attributes from the original script element
							for (let aI = 0; aI < scriptNode.attributes.length; aI++) {
								scriptElement.attributes.setNamedItem(<Attr>scriptNode.attributes[aI].cloneNode());
							}
							scriptElement.setAttribute('data-custom-picture', `${this.webComponent.name}-${i}`);
							// Add any content the original script element has
							const scriptContent = document.createTextNode(scriptNode.textContent);
							scriptElement.appendChild(scriptContent);
							// Remove the original script element
							scriptNode.remove();
							// add the new element to the list
							this.exchangeJavaScript(scriptElement);
						}
					}
				});
			});
		}
	}

	private exchangeJavaScript(script: HTMLElement): void {
		const headElement = document.getElementsByTagName('head')[0];
		// just search for it to remove before adding
		this.removeJavaScript(headElement, script.getAttribute('data-custom-picture'));
		// now add it
		headElement.appendChild(script);
	}

	private removeJavaScript(headElement?: HTMLElement, customName?: string | null): void {
		// just search for it to remove before adding
		headElement?.querySelectorAll('script[data-custom-picture^="' +
			(customName ? customName : this.webComponent.name) + '"]').forEach((element) => {
			// remove each found
			element.remove();
		});
	}
}
