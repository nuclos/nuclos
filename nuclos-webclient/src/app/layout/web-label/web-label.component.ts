import { Component, Injector, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { SystemParameter } from '@shared/system-parameters';
import { flatMap, map } from 'rxjs/operators';
import { take } from 'rxjs/operators';
import { AbstractLabelComponentDirective } from '../shared/abstract-label-component.directive';

@Component({
	selector: 'nuc-web-label',
	templateUrl: './web-label.component.html',
	styleUrls: ['./web-label.component.css']
})
export class WebLabelComponent extends AbstractLabelComponentDirective<WebLabel> implements OnInit, OnChanges {

	private label: string;

	constructor(
		injector: Injector,
		private configService: NuclosConfigService
	) {
		super(injector);
	}

	ngOnInit() {
		this.setText();
	}

	ngOnChanges(_changes: SimpleChanges): void {
		super.ngOnChanges(_changes);
		this.setText();
	}

	getText() {
		return this.label;
	}

	getForId() {
		if (this.webComponent.forId) {
			return 'component-' + this.webComponent.forId;
		}

		return undefined;
	}

	private setText() {
		if (this.eo && this.webComponent) {
			if (this.webComponent.label !== undefined) {
				this.label = this.webComponent.label;
			} else {
				this.eo.getAttributeMeta(this.webComponent.name).pipe(
					take(1),
					flatMap(
						meta => this.eo.getAttributeLabel(this.webComponent.name).pipe(
							take(1),
							map(label => ({meta, label}))
						)
					),
					flatMap(
						({meta, label}) => this.configService.getSystemParameters().pipe(
							take(1),
							map(params => {
								let bAddStar = params.is(SystemParameter.ADD_STAR_TO_MANDATORY_FIELD_LABEL);
								return ({meta, label, bAddStar});
							})
						)
					)
				).subscribe(({meta, label, bAddStar}) => {
					if (meta) {
						this.label = label + (!meta.isNullable() && !meta.isBoolean() && bAddStar ? ' *' : '');
					}
				});
			}
		}
	}

}
