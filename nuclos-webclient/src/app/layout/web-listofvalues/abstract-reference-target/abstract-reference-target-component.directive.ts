import { HttpParams } from '@angular/common/http';
import { Directive, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ReferenceTargetService } from '@modules/entity-object-data/shared/reference-target.service';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { take } from 'rxjs/operators';
import { EntityAttrMeta } from '../../../modules/entity-object-data/shared/bo-view.model';
import { EntityObject } from '../../../modules/entity-object-data/shared/entity-object.class';
import { LovDataService } from '../../../modules/entity-object-data/shared/lov-data.service';
import {
	BrowserRefreshService,
	SelectReferenceInOtherWindowEvent
} from '../../../shared/service/browser-refresh.service';
import { FqnService } from '../../../shared/service/fqn.service';
import { LovHandler } from '../../abstract-lov/lov-handler';

/**
 * Created by Oliver Brausch on 21.10.17.
 */
@Directive()
export abstract class AbstractReferenceTargetComponentDirective implements OnChanges, OnInit {
	isVisible = false;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;
	@Input() protected eo: EntityObject | undefined;
	@Input() protected attributeMeta: EntityAttrMeta | undefined;
	@Input() protected popupParameter;
	@Input() protected noCloseAfterSave;
	@Input() protected lovHandler: LovHandler = LovHandler.DUMMY;
	@Output() protected referenceUpdated = new EventEmitter();

	constructor(
		protected lovDataService: LovDataService,
		protected referenceTargetService: ReferenceTargetService
	) {
	}

	ngOnInit() {
		this.updateVisibility();
	}

	ngOnChanges(changes: SimpleChanges) {
		this.updateVisibility();
	}

	getLovHandler(): LovHandler {
		return this.lovHandler;
	}

	protected getVlpId(): string | undefined {
		if (this.lovHandler.getVlpId) {
			return this.lovHandler.getVlpId();
		}
		return undefined;
	}

	protected getVlpParams(): HttpParams | undefined {
		if (this.lovHandler.getVlpParams) {
			return this.lovHandler.getVlpParams();
		}
		return undefined;
	}

	protected updateVisibility() {
		// This !this.isVisible is with purpose: Priviliges by attributeMeta will not be revoked on-the-fly
		if (!this.isVisible) {
			this.referenceTargetService.canOpenReference(this.eo, this.attributeMeta)
				.pipe(take(1))
				.subscribe(canOpen => (this.isVisible = canOpen));
		}
	}

	protected doCloseAfterSave() {
		return this.noCloseAfterSave !== 'yes';
	}

	protected targetReference(
		boId: string | number | undefined,
		withSaving: boolean,
		setReferenceUnsafe: boolean,
		vlpId: string | undefined,
		vlpParams: HttpParams | undefined,
		closeAfterSave = withSaving
	) {
		this.referenceTargetService.targetReference(
			this.eo,
			this.attributeMeta,
			boId,
			withSaving,
			vlpId,
			vlpParams,
			setReferenceUnsafe,
			this.popupParameter,
			closeAfterSave
		).pipe(
			take(1)
		).subscribe(() => {
			this.referenceUpdated.emit();
		});
	}
}
