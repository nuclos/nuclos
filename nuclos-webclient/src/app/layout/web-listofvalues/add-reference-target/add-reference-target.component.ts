import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { ReferenceTargetService } from '@modules/entity-object-data/shared/reference-target.service';
import { take } from 'rxjs/operators';
import { LovDataService } from '@modules/entity-object-data/shared/lov-data.service';
import { AbstractReferenceTargetComponentDirective } from '../abstract-reference-target/abstract-reference-target-component.directive';

@Component({
	selector: 'nuc-add-reference-target',
	templateUrl: './add-reference-target.component.html',
	styleUrls: ['./add-reference-target.component.css']
})
export class AddReferenceTargetComponent extends AbstractReferenceTargetComponentDirective {

	constructor(
		protected lovDataService: LovDataService,
		protected referenceTargetService: ReferenceTargetService,
		public router: Router
	) {
		super(lovDataService, referenceTargetService);
	}

	addReference($event: MouseEvent) {
		if (this.lovHandler.stopEditing) {
			this.lovHandler.stopEditing();
		}
		this.targetReference('new', true, false, this.getVlpId(), undefined);
		$event.stopPropagation();
	}

	protected updateVisibility() {
		this.referenceTargetService.canAddReference(this.eo, this.attributeMeta)
			.pipe(take(1))
			.subscribe(canAddReference => (this.isVisible = canAddReference));
	}
}

