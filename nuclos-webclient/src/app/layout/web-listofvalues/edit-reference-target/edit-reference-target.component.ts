import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ReferenceTargetService } from '@modules/entity-object-data/shared/reference-target.service';
import { take } from 'rxjs/operators';
import { LovEntry } from '../../../modules/entity-object-data/shared/bo-view.model';
import { LovDataService } from '../../../modules/entity-object-data/shared/lov-data.service';
import { AbstractReferenceTargetComponentDirective } from '../abstract-reference-target/abstract-reference-target-component.directive';

@Component({
	selector: 'nuc-edit-reference-target',
	templateUrl: './edit-reference-target.component.html',
	styleUrls: ['./edit-reference-target.component.css']
})
export class EditReferenceTargetComponent extends AbstractReferenceTargetComponentDirective {
	@Input() private attribute: LovEntry | undefined;

	constructor(
		protected lovDataService: LovDataService,
		protected referenceTargetService: ReferenceTargetService,
		public router: Router
	) {
		super(lovDataService, referenceTargetService);
	}

	editReference($event: MouseEvent) {
		if (this.attribute && this.attribute.id) {
			this.targetReference(
				this.attribute.id,
				true,
				true,
				this.getVlpId(),
				undefined,
				this.doCloseAfterSave()
			);
		}
		$event.stopPropagation();
	}

	protected updateVisibility() {
		this.referenceTargetService.canEditReference(this.eo, this.attributeMeta, this.attribute)
			.pipe(take(1))
			.subscribe(
				canEdit => (this.isVisible = canEdit)
			);
	}
}
