import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ReferenceTargetService } from '@modules/entity-object-data/shared/reference-target.service';
import { LovDataService } from '../../../modules/entity-object-data/shared/lov-data.service';
import {
	AbstractReferenceTargetComponentDirective
} from '../abstract-reference-target/abstract-reference-target-component.directive';

@Component({
	selector: 'nuc-refresh-reference-target',
	templateUrl: './refresh-reference-target.component.html',
	styleUrls: ['./refresh-reference-target.component.css']
})
export class RefreshReferenceTargetComponent extends AbstractReferenceTargetComponentDirective {

	constructor(
		protected lovDataService: LovDataService,
		protected referenceTargetService: ReferenceTargetService,
		public router: Router
	) {
		super(lovDataService, referenceTargetService);
	}

	refreshReference($event: MouseEvent) {
		if (this.getLovHandler().stopEditing) {
			this.getLovHandler().stopEditing!();
		}
		if (this.getLovHandler().refreshEntries) {
			this.getLovHandler().refreshEntries!();
		}
		$event.stopPropagation();
	}

}
