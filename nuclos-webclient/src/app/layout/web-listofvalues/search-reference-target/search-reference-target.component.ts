import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ReferenceTargetService } from '@modules/entity-object-data/shared/reference-target.service';
import { LovDataService } from '../../../modules/entity-object-data/shared/lov-data.service';
import { AbstractReferenceTargetComponentDirective } from '../abstract-reference-target/abstract-reference-target-component.directive';

@Component({
	selector: 'nuc-search-reference-target',
	templateUrl: './search-reference-target.component.html',
	styleUrls: ['./search-reference-target.component.css']
})
export class SearchReferenceTargetComponent extends AbstractReferenceTargetComponentDirective {

	constructor(
		protected lovDataService: LovDataService,
		protected referenceTargetService: ReferenceTargetService,
		public router: Router
	) {
		super(lovDataService, referenceTargetService);
	}

	searchReference($event: MouseEvent) {
		this.targetReference(undefined, false, false, this.getVlpId(), this.getVlpParams());
		$event.stopPropagation();
	}
}
