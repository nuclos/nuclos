import { Component, ElementRef, Injector, OnInit } from '@angular/core';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { ReferenceTargetService } from '@modules/entity-object-data/shared/reference-target.service';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { of, Subject } from 'rxjs';
import { LovDataService } from '../../modules/entity-object-data/shared/lov-data.service';
import { FqnService } from '../../shared/service/fqn.service';
import { AbstractLovComponentDirective } from '../abstract-lov/abstract-lov-component.directive';
import { ComboboxUtils } from '../shared/combobox-utils';
import { Logger } from '@modules/log/shared/logger';

@Component({
	selector: 'nuc-web-listofvalues',
	templateUrl: '../abstract-lov/abstract-lov.component.html',
	styleUrls: ['../abstract-lov/abstract-lov.component.scss', '../abstract-lov/abstract-lov.component.css']
})
export class WebListofvaluesComponent extends AbstractLovComponentDirective<WebListofvalues> implements OnInit {

	constructor(
		lovDataService: LovDataService,
		injector: Injector,
		fqnService: FqnService,
		elementRef: ElementRef,
		referenceTargetService: ReferenceTargetService,
		nuclosHotkeysService: NuclosHotkeysService,
		i18nService: NuclosI18nService,
		$log: Logger
	) {
		super(lovDataService, injector, fqnService, elementRef, referenceTargetService, nuclosHotkeysService, i18nService, $log);

		this.handler = {
			loadEntries: () => this.loadEntries(),
			refreshEntries: () => this.doRefreshEntries(),
			loadFilteredEntries: search => this.loadFilteredEntries(search),
			getValue: () => this.getValue(),
			getVlpId: () => this.getVlpId(),
			getVlpParams: () => this.getVlpParams()
		};
	}

	ngOnInit() {
		super.ngOnInit();
	}

	loadEntries() {
		return this.loadFilteredEntries('');
	}

	loadFilteredEntries(search: string) {
		return this.lovDataService !== undefined ? this.lovDataService.loadLovEntries(this.getLovSearchConfig(search)) :
			ComboboxUtils.filter(of(this.lovEntries), search);
	}
}
