import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MatrixButtonsComponent } from './matrix-buttons.component';

xdescribe('MatrixButtonsComponent', () => {
	let component: MatrixButtonsComponent;
	let fixture: ComponentFixture<MatrixButtonsComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [MatrixButtonsComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(MatrixButtonsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
