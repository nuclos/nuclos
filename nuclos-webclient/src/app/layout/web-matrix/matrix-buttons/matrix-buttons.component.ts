import { Component, Input, OnInit } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { EntityObjectPreferenceService } from '@modules/entity-object/shared/entity-object-preference.service';
import { WebMatrixComponent } from '../web-matrix.component';

@Component({
	selector: 'nuc-matrix-buttons',
	templateUrl: './matrix-buttons.component.html',
	styleUrls: ['./matrix-buttons.component.css']
})
export class MatrixButtonsComponent implements OnInit {

	@Input() matrix: WebMatrixComponent;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor(private entityObjectPreferenceService: EntityObjectPreferenceService) {
	}

	ngOnInit() {
	}

}
