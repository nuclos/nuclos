import { Component, ElementRef, Injector, OnDestroy } from '@angular/core';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { EntityMeta, EntityObjectData } from '@modules/entity-object-data/shared/bo-view.model';
import { DataService } from '@modules/entity-object-data/shared/data.service';
import { EntityObject, SubEntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { EntityObjectGridService } from '@modules/entity-object-grid/entity-object-grid.service';
import { Logger } from '@modules/log/shared/logger';
import { ColorUtils } from '@shared/color-utils';
import { FqnService } from '@shared/service/fqn.service';
import { LocalStorageService } from '@shared/service/local-storage.service';
import { StringUtils } from '@shared/string-utils';
import { ColDef, ColGroupDef, GridApi, RowClassParams, RowNode } from 'ag-grid-community';
import { Observable, Subscription } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { AbstractGridComponentDirective } from '../shared/abstract-grid-component.directive';
import { SubformBooleanEditorComponent } from '../web-subform/cell-editors/subform-boolean-editor/subform-boolean-editor.component';
import { SubformComboboxEditorComponent } from '../web-subform/cell-editors/subform-combobox-editor/subform-combobox-editor.component';
import { SubformStringEditorComponent } from '../web-subform/cell-editors/subform-string-editor/subform-string-editor.component';
import { SubformBooleanRendererComponent, SubformReferenceRendererComponent } from '../web-subform/cell-renderer';
import { NuclosCellEditorParams, NuclosCellRendererParams } from '../web-subform/web-subform.model';

@Component({
	selector: 'nuc-web-matrix',
	templateUrl: './web-matrix.component.html',
	styleUrls: ['./web-matrix.component.scss']
})
export class WebMatrixComponent extends AbstractGridComponentDirective<WebMatrix> implements OnDestroy {
	private loadMatrixSubscription: Subscription;

	private hasGrouping = false;
	private numberSelection = 0;
	private alive = true;

	constructor(
		injector: Injector,
		private entityObjectService: EntityObjectService,
		private eoGridService: EntityObjectGridService,
		private eoVLPService: EntityVLPService,
		private dataService: DataService,
		private fqnService: FqnService,
		private $log: Logger,
		i18nService: NuclosI18nService,
		elementRef: ElementRef
	) {
		super(injector, i18nService, elementRef);

		this.eoChangeListener = {
			afterAttributeChange: (
				entityObject: SubEntityObject,
				attributeName: string,
				oldValue: any,
				newValue: any
			) => this.attributeChanged(entityObject, attributeName, oldValue, newValue),
			afterValidationChange: () => this.softRefresh(),
			afterReload: () => this.softRefresh()
		};
	}

	ngOnDestroy() {
		this.removeEoListener();
		this.alive = false;
	}

	cellEditingStopped(event: { node: RowNode }) {
		if (this.gridOptions && this.gridOptions.api) {
			this.gridOptions.api.refreshCells();
		}
	}

	isClickOutside(target: any) {
		// If a datepicker is opened, its panel is attached to the body, not to the subform.
		// Therefore it is not a child of the subform in the DOM and the click appears to be outside.
		// But since there should be always at most 1 datepicker open at the same time, it should
		// be the one belonging to the subform, which means "no outside click".
		// TODO: This assumption might be wrong, if e.g. there was another datepicker component
		//  that is not displayed in an exclusive overlay.
		//  We should make sure, that the datepicker which received the click really belongs to
		//  this subform and the currently active cell editor.
		return $(target).closest('ngb-datepicker').length === 0;
	}

	loadData() {
		this.$log.debug('Matrix init...');
		this.dependents = this.eo.getDependents(this.webComponent.entityYParentField);
		this.gridOptions.api?.setRowData([]);

		this.gridOptions.getRowStyle = (params: RowClassParams) => {
			let rowColor = params.data.getAttribute('nuclosrowcolor');
			let useDefaultTextColor = params.node.data.isNew() || params.node.data.isDirty() || params.node.data.isMarkedAsDeleted();
			let textColor = ColorUtils.calculateTextColorFromBackgroundColor(useDefaultTextColor ? '' : rowColor);
			return {
				'background-color': rowColor !== undefined ? rowColor : '',
				'color': textColor !== undefined ? textColor : ''
			};
		};

		this.setColumns([]);
		this.updateGridStatusOverlay(true);

		this.loadMatrixSubscription = this.dataService
			.loadMatrixData(this.webComponent, this.eo)
			.pipe(
				take(1),
				switchMap(data => this.eo.getMeta().pipe(
					map((meta: EntityMeta) => {
						data['meta'] = meta.getMetaData();
						return data;
					})
				))
			)
			.subscribe(data => {
				this.$log.debug('Init with data %o', data);
				this.initColumns(data, 108);

				this.dependents?.asObservable()
					.pipe(
						take(1)
					).subscribe((dependents: SubEntityObject[] | undefined) => {
					this.initCellData(data, dependents);
					this.updateGridStatusOverlay();
					this.updateGridData();
					this.adjustHeaderHeightToContent();
				});
				this.dependents?.loadIfEmpty();
			});
	}

	gridReady() {
		super.gridReady();
		this.gridOptions.onBodyScroll = () => {
			this.adjustHeaderHeightToContent();
		};
	}

	getMatrixValue(newValue): any {
		if (newValue === true) {
			return 1;
		} else if (newValue === false) {
			return 0;
		} else if (newValue instanceof Object
			&& newValue.hasOwnProperty('id')
			&& newValue.id !== undefined) {
			return newValue.id;
		}
		return newValue;
	}

	/**
	 * Instantiates a new Sub-EO and adds it to the grid data.
	 */
	newEntry() {
		this.createSubEo().pipe(take(1)).subscribe(subEo => {
			if (subEo) {
				this.addSubEO(subEo);
				this.updateGridData();
				this.editFirstCell();
			}
		});
	}

	isNewVisible(): boolean {
		return this.isWritable();
	}

	isNewEnabled(): boolean {
		return true;
	}

	deleteSelected(): void {
		super.deleteSelectedImpl(this.webComponent.entityYParentField);
		this.softRefresh();
	}

	isDeleteVisible(): boolean {
		return this.isWritable();
	}

	isDeleteEnabled(): boolean {
		return this.numberSelection > 0;
	}

	/**
	 * Flags the underlying Sub-EO as selected, when a row is selected.
	 *
	 * @param params
	 */
	rowSelected(params: { node: RowNode }) {
		let subEO: SubEntityObject = params.node.data;
		subEO.setSelected(params.node.isSelected());
		this.numberSelection += params.node.isSelected() ? 1 : -1;
	}

	protected onGridApiReady() {}

	protected init() {
		this.loadData();
	}

	protected cancel() {
		this.loadData();
	}

	protected getLogger(): Logger {
		return this.$log;
	}

	/**
	 * @override set attribute without async validation to fix NUCLOS-6199
	 */
	protected newValueHandler(params: {
		node: RowNode | null;
		data: SubEntityObject;
		oldValue: any;
		newValue: any;
		colDef: ColDef;
		api: GridApi;
		context: any;
	}): boolean {
		if (!params.colDef.field) {
			this.getLogger().warn('Field missing: %o', params.colDef);
			return false;
		}

		// TODO params.colDef.field is a data record id not an attributeName
		params.node?.data.setAttributeValidated(params.colDef.field, params.newValue);

		return false;
	}

	protected getReferenceAttributeIdImpl() {
		return this.webComponent.entityYParentField;
	}

	protected getParentForNewSubEo(): EntityObject | undefined {
		return super.getEntityObject();
	}

	private createSubEo(): Observable<SubEntityObject | undefined> {
		let parentForNewSubEo = this.getEntityObject();
		return this.entityObjectService
			.createNew(this.webComponent.entityY, parentForNewSubEo)
			.pipe(map((eo: EntityObject) => this.prepareNewSubEo(eo)));
	}

	private initCellData(data, dependents: SubEntityObject[] | undefined) {
		let dropDownMap = {};
		if (data.matrixValues) {
			for (let value of data.matrixValues) {
				dropDownMap[value.id] = value;
			}
		}
		this.$log.debug('current dependents: %o', dependents);
		if (dependents !== undefined) {
			for (let row of data.yAxisBoList) {
				if (dependents?.find((sub: SubEntityObject) => sub.getId() === row.boId && sub.isDirty()) !== undefined) {
					// NUCLOS-9674 (NUCLOS-9231) skip replacing data, we already have dirty data here
					return;
				}
			}
		}
		dependents = [];
		this.gridOptions.rowSelection = 'multiple';
		this.gridOptions.suppressRowClickSelection = new EntityMeta(data.meta, this.fqnService).showMatrixSelectionColumn();
		this.numberSelection = 0;

		for (let row of data.yAxisBoList) {
			if (this.webComponent.entityYHiddenField && row.rowHidden) {
				continue;
			}

			let subEo = new SubEntityObject(
				this.eo,
				this.webComponent.entityYParentField,
				new EntityObjectData(),
				this.injector.get(LocalStorageService),
				this.fqnService
			);
			subEo.init(this.webComponent.entityY, row.boId, {});
			this.eoVLPService.addEntityObjectMap(subEo);
			subEo.setAttributeUnsafe('0', row.evaluatedHeaderTitle);
			subEo.setAttributeUnsafe(this.webComponent.entityYParentField, {id: this.eo.getId()});
			if (row.rowColor) {
				subEo.setAttributeUnsafe('nuclosrowcolor', row.rowColor);
			}

			if (row.yAxisColumns) {
				for (let col of row.yAxisColumns) {
					subEo.setAttributeUnsafe(col.boAttrId, col.value);
				}
			}

			let subsubEos: SubEntityObject[] = [];

			for (let column of data.xAxisBoList) {
				let key = column.boId + '_' + row.boId;
				let dict = data.entityMatrixBoDict[key];

				if (dict !== undefined) {
					let attrValue = dropDownMap[dict.value] ? dropDownMap[dict.value] : dict.value;
					subEo.setAttributeUnsafe(column.boId + '', attrValue);

					let sseo = this.createSubSubDependent(
						column.boId,
						row.boId,
						dict.value,
						dict.boId,
						subEo
					);

					subsubEos.push(sseo);
				}
			}

			subEo.getDependents(this.webComponent.entityFieldMatrixParent).set(subsubEos);
			subEo.setDirty(false);
			subEo.addListener(this.eoChangeListener);
			dependents.push(subEo);
		}

		this.$log.debug('updating dependents: %o', dependents);
		this.dependents?.set(dependents);
	}

	private createSubSubDependent(
		columnId: number,
		rowId: number,
		value: any,
		boId: number | undefined,
		parentEo: EntityObject
	): SubEntityObject {
		let valueMap = {};
		valueMap[this.webComponent.entityMatrixValueField] = value;
		valueMap[this.webComponent.entityFieldMatrixXRefField] = {id: columnId};
		valueMap[this.webComponent.entityFieldMatrixParent] = {id: rowId};

		let sseo = new SubEntityObject(
			parentEo,
			this.webComponent.entityFieldMatrixXRefField,
			new EntityObjectData(),
			this.injector.get(LocalStorageService),
			this.fqnService
		);
		this.eoVLPService.addEntityObjectMap(sseo);
		sseo.init(this.webComponent.entityMatrix, boId, valueMap);

		return sseo;
	}

	private initColumns(data: any, width: number) {
		let colDefs = this.createColumnDefinitions(width, data);
		this.setColumns(colDefs);
	}

	private createColumnDefinitions(width: number, data: any) {
		let columnDefinitions: ColDef[] = [];
		if (new EntityMeta(data.meta, this.fqnService).showMatrixSelectionColumn()) {
			columnDefinitions.push(this.createSelectionColumn());
		}

		let editable = this.isWritable();

		if (data.yMatrixColumnsList && data.yMatrixColumnsList.length > 0) {
			let grouping = data.xAxisBoList.length > 0 && data.xAxisBoList[0]._group;

			for (let column of data.yMatrixColumnsList) {
				let colDef: ColDef = {
					headerName: column.value,
					headerTooltip: column.value,
					width: width * (column.value.length + 1) / 8,
					colId: column.boAttrId,
					field: column.boAttrId,
					editable: editable,
					valueGetter: this.eoGridService.valueGetter,
					newValueHandler: this.newValueHandler,
					pinned: true,
					resizable: true,
					cellClassRules: this.getDefaultCellClasses()
				};

				if (column.type === 'Boolean') {
					colDef.width = width * (column.value.length + 1) / 18;
					colDef.cellRendererParams = {
						nuclosCellRenderParams: {
							editable: editable,
							numberStates: '1'
						} as NuclosCellRendererParams
					};

					colDef.cellRendererFramework = SubformBooleanRendererComponent;
					colDef.cellEditorFramework = SubformBooleanEditorComponent;
				}

				if (grouping) {
					let groupDefTemp: ColGroupDef = {
						headerName: colDef.headerName,
						children: [colDef]
					};
					colDef.headerName = '';
					colDef = groupDefTemp;
				}
				this.$log.debug('%o: %o, colDef = %o', column.boAttrId, editable, colDef);
				columnDefinitions.push(colDef);
			}
		} else {
			columnDefinitions.push({
				headerName: '',
				width: width * 2,
				colId: '0',
				field: '0',
				editable: false,
				valueGetter: this.eoGridService.valueGetter.bind(this.eoGridService),
				pinned: true,
				cellClassRules: this.getDefaultCellClasses()
			});
		}

		let groupHasData = false;
		let groupDef: ColGroupDef = {
			headerName: '',
			children: []
		};
		for (let column of data.xAxisBoList) {

			let id: string = String(column.boId);
			let _hidden = column._hidden;

			let colDef: ColDef = {
				headerName: column._title,
				headerTooltip: column._title,
				field: id,
				colId: id,
				width: width,
				editable: editable,
				valueGetter: this.eoGridService.valueGetter.bind(this.eoGridService),
				newValueHandler: this.newValueHandler,
				headerClass: () => {
					if (this.hasGrouping) {
						return 'ag-header-cell-grouped';
					}
					return '';
				},
				sortable: false,
				resizable: true,
				cellClassRules: this.getDefaultCellClasses()
			};

			this.$log.debug('%o: %o, colDef = %o', id, editable, colDef);

			colDef.cellRendererParams = {
				nuclosCellRenderParams: {
					editable: editable,
					numberStates: this.webComponent.entityMatrixNumberState
				} as NuclosCellRendererParams
			};

			if (this.webComponent.cellInputType === 'checkicon') {
				colDef.cellRendererFramework = SubformBooleanRendererComponent;
				colDef.cellEditorFramework = SubformBooleanEditorComponent;
			} else if (this.webComponent.cellInputType === 'combobox') {
				colDef.cellRendererFramework = SubformReferenceRendererComponent;
				colDef.cellEditorFramework = SubformComboboxEditorComponent;

				// provide data for render component
				colDef.cellEditorParams = {
					// TODO: Load Meta object properly instead of faking it here
					staticList: data.matrixValues
				} as NuclosCellEditorParams;
			} else {
				colDef.cellEditorFramework = SubformStringEditorComponent;
			}

			if (column._group) {
				this.hasGrouping = true;
				if (groupDef.headerName !== column._group) {
					groupHasData = false;

					groupDef = {
						headerName: column._group,
						headerTooltip: column._group,
						children: []
					};

					columnDefinitions.push(groupDef);
				}

				let hasData = false;
				for (let yAxis of data.yAxisBoList) {
					let mid: string = id + '_' + yAxis.boId;
					if (data.entityMatrixBoDict[mid]) {
						hasData = true;
						break;
					}
				}
				if (hasData) {
					if (!groupHasData && groupDef.children.length > 0) {
						groupDef.children[0].columnGroupShow = 'open';
					}
					groupHasData = true;
				} else if (groupDef.children.length > 0) {
					colDef.columnGroupShow = 'open';
				}

				if (!hasData && _hidden) {
					continue;
				}

				colDef.width = colDef.width ? colDef.width / 3 : undefined;
				groupDef.children.push(colDef);

			} else {
				columnDefinitions.push(colDef);
			}

		}

		return columnDefinitions;
	}

	/**
	 *
	 * @param entityObject
	 * @param attributeName
	 * @param oldValue
	 * @param newValue
	 */
	private attributeChanged(
		entityObject: SubEntityObject,
		attributeName: string,
		_oldValue: any,
		newValue: any
	) {
		if (entityObject.hasFqnAttribute(attributeName)) {
			return; // NUCLOS-9001 Return if is a FQN Attribute, because the matrix values (BeziehungsWissen) are not.
		}
		if (entityObject.getEntityClassId() + '_' + attributeName === this.webComponent.entityYParentField) {
			return; // NUCLOS-9049 Return when the parentKey is being set on a new row.
		}
		attributeName = StringUtils.replaceAll(attributeName, '_', '');

		this.$log.debug('attribute changed: %o = %o', attributeName, newValue);
		const subdependents = entityObject
			.getDependentsAndLoad(this.webComponent.entityFieldMatrixParent, undefined)
			.current();

		if (!subdependents) {
			return;
		}

		// First find the sub-sub-entity-obejct if it exists.
		let existing: SubEntityObject | undefined;
		for (let subdependent of subdependents) {
			let xId = subdependent.getAttribute(
				this.webComponent.entityFieldMatrixXRefField
			).id;
			if (String(xId) === attributeName) {
				existing = subdependent as SubEntityObject;
				break;
			}
		}

		let value = this.getMatrixValue(newValue);

		if (existing) {
			existing.setAttributeUnsafe(this.webComponent.entityMatrixValueField, value);

			if (value) {
				existing.mergeData({
					_flag: 'update'
				} as EntityObjectData);
			} else {
				if (!existing.getData()?.boId) {
					// It's not persistent, so just remove it from the list
					let index = subdependents.indexOf(existing);
					subdependents.splice(index, 1);
				} else {
					// It's in the DB, so mark it for deletion
					existing.mergeData({
						_flag: 'delete'
					} as EntityObjectData);
				}
			}
		} else if (value) {
			// First time this cell gets a value, thus create the sub-sub-eo.
			let sseo = this.createSubSubDependent(
				Number(attributeName),
				Number(entityObject.getId()),
				value,
				undefined,
				entityObject
			);
			subdependents.push(sseo);
		}
		this.softRefresh();
	}

	private adjustHeaderHeightToContent() {
		if (this.gridOptions.api) {
			const heighestCell = $(
				$('span.ag-header-cell-text')
					.toArray()
					.reduce((prev, current) => {
						return $(prev).height() > $(current).height() ? prev : current;
					}, undefined)
			);
			const maxHeaderHeight = heighestCell ? heighestCell.height() : 28;

			if (this.hasGrouping) {
				// do not adjust grouping header height for better visual
				this.gridOptions.api.setGroupHeaderHeight(28);
			}
			this.gridOptions.api.setHeaderHeight(maxHeaderHeight + 10);

			if (
				this.gridOptions.columnApi &&
				this.gridOptions.columnDefs &&
				this.gridOptions.columnDefs.length > 0
			) {
				let colDef: ColDef = this.gridOptions.columnDefs[0];
				if (colDef.field) {
					this.gridOptions.columnApi.autoSizeColumn(colDef.field);
				}
			}
		}
	}
}
