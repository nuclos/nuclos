/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { WebMultiselectionComboboxComponent } from './web-multiselection-combobox.component';

xdescribe('WebComboboxComponent', () => {
	let component: WebMultiselectionComboboxComponent;
	let fixture: ComponentFixture<WebMultiselectionComboboxComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebMultiselectionComboboxComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebMultiselectionComboboxComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
