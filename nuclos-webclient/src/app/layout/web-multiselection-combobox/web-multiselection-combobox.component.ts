import {
	Component,
	ElementRef,
	HostListener,
	Injector,
	OnDestroy,
	OnInit,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import {
	EntityAttrMeta,
	EntityMeta,
	EntityObjectData,
	LovEntry
} from '@modules/entity-object-data/shared/bo-view.model';
import { EntityObjectDependents } from '@modules/entity-object-data/shared/entity-object-dependents';
import { EntityObject, SubEntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { LovDataService } from '@modules/entity-object-data/shared/lov-data.service';
import { LovSearchConfig } from '@modules/entity-object-data/shared/lov-search-config';
import { MetaService } from '@modules/entity-object-data/shared/meta.service';
import { ReferenceTargetService } from '@modules/entity-object-data/shared/reference-target.service';
import { Logger } from '@modules/log/shared/logger';
import { RuleService } from '@modules/rule/shared/rule.service';
import { IEntityObjectDependents, IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { EXPAND_SIDEVIEW_PARAM, SHOW_CLOSE_ON_SAVE_BUTTON_PARAM } from '@shared/service/browser-refresh.service';
import { FqnService } from '@shared/service/fqn.service';
import { HyperlinkService } from '@shared/service/hyperlink.service';
import { LocalStorageService } from '@shared/service/local-storage.service';
import { OverlayOnShowEvent, OverlayOptions } from 'primeng/api';
import { MultiSelect } from 'primeng/multiselect';
import { OverlayPanel } from 'primeng/overlaypanel';
import { iif, Observable, of, Subscription } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { NuclosDefaults } from '../../data/schema/nuclos-defaults';
import { AbstractWebComponentDirective } from '../shared/abstract-web-component.directive';

declare var require: any;
let naturalCompare = require('string-natural-compare');

const LOV_ENTRY_ID_ASCENDING = (a: LovEntry, b: LovEntry) => {
	if (a.id === b.id) {
		return 0;
	}
	if (a.id === null) {
		return -1;
	}
	if (b.id === null) {
		return +1;
	}
	return a.id - b.id;
};

const LOV_ENTRY_NAME_ASCENDING = (a: LovEntry, b: LovEntry) => {
	if (a.name === b.name) {
		return 0;
	}
	if (a.name === null) {
		return -1;
	}
	if (b.name === null) {
		return +1;
	}
	return naturalCompare(a.name, b.name);
};

@Component({
	selector: 'nuc-web-multiselection-combobox',
	templateUrl: 'web-multiselection-combobox.component.html',
	styleUrls: ['web-multiselection-combobox.component.scss']
})
export class WebMultiselectionComboboxComponent
	extends AbstractWebComponentDirective<WebMultiselectionCombobox> implements OnInit, OnDestroy {

	hover: boolean;
	subformMeta: EntityMeta;
	referenceValuesMeta: EntityMeta;
	valueReferenceAttrMeta: EntityAttrMeta | undefined;
	subEOs: SubEntityObject[];
	suggestions: LovEntry[];
	subModel: LovEntry[];
	displayAttributeFqn: string;
	displayAttributeShortName: string;
	overlayOptions: OverlayOptions = {
		appendTo: 'body',
		baseZIndex: 999999,
		onShow: event => this.onPanelShow(event)
	};

	@ViewChild('multiSelectContainer') multiSelectContainer: ElementRef;
	@ViewChild('multiSelect') multiSelect: MultiSelect | undefined;
	@ViewChild('overlayPanel') overlayPanel: OverlayPanel;
	@ViewChild('scrollContainer') scrollContainer: ElementRef;

	protected eoChangeListener: IEntityObjectEventListener;
	protected dependents?: IEntityObjectDependents;
	protected dependentSubscription: Subscription;

	private statefulParent = false;

	constructor(
		protected injector: Injector,
		public router: Router,
		private $log: Logger,
		private metaService: MetaService,
		private entityObjectService: EntityObjectService,
		private referenceTargetService: ReferenceTargetService,
		private eoVLPService: EntityVLPService,
		private lovDataService: LovDataService,
		private fqnService: FqnService,
		private ruleService: RuleService,
		private hyperlinkService: HyperlinkService
	) {
		super(injector);

		this.eoChangeListener = {
			refreshVlp: (
				entityObject: EntityObject,
				attributeName: string,
			) => {
				if (this.webComponent.labelAttr === attributeName) {
					this.loadSuggestions();
				}
			},
			afterSave: (entityObject: EntityObject) => {
				this.initDependents();
			}
		};

		this.eo.getMeta().pipe(take(1)).subscribe((meta) => {
			this.statefulParent = meta.hasStateModel();
		});
	}

	keydown($event: KeyboardEvent) {
		$event.stopPropagation();
	}

	ngOnInit() {
		this.displayAttributeFqn = this.webComponent.labelAttr;

		this.eo.addListener(this.eoChangeListener);

		this.metaService.getBoMeta(FqnService.getEntityClassIdCaseSafe(this.webComponent.foreignkeyfieldToParent))
			.subscribe(meta => {
				this.subformMeta = meta;
				this.displayAttributeShortName = this.fqnService.getShortAttributeNameFailsafe(
					this.subformMeta.getEntityClassId(),
					this.displayAttributeFqn
				);
				this.valueReferenceAttrMeta = this.subformMeta.getAttributeMetaByFqn(this.displayAttributeFqn);
				this.metaService
					.getBoMeta(this.valueReferenceAttrMeta?.getReferencedEntityClassId())
					.subscribe(refMeta => this.referenceValuesMeta = refMeta);
				this.loadSuggestions();
				this.initDependents();
			});
	}

	ngOnChanges(changes: SimpleChanges): void {
		super.ngOnChanges(changes);
		let eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				eoChange.previousValue.removeListener(this.eoChangeListener);
			}
			if (eoChange.currentValue) {
				eoChange.currentValue.addListener(this.eoChangeListener);
			}
			this.initDependents();
		}
	}

	ngOnDestroy(): void {
		if (this.dependentSubscription) {
			this.dependentSubscription.unsubscribe();
		}
	}

	@HostListener('document:keydown.Escape', ['$event'])
	handleKeydown(event: KeyboardEvent) {
		if (!this.multiSelect) {
			return;
		}
		if (event.key === 'Escape' && this.multiSelect && this.multiSelect.overlayVisible) {
			this.multiSelectContainer.nativeElement.focus();
			this.multiSelect.hide();
		}
	}

	isOverlayContext() {
		return this.eo instanceof SubEntityObject;
	}

	hasStatefulParent() {
		return this.statefulParent;
	}

	isVisible(): boolean {
		return (
			!(this.hasStatefulParent() && this.isOverlayContext())
			&& super.isVisible()
			&& !!this.eo.getSubEoLink(this.webComponent.foreignkeyfieldToParent)
		);
	}

	isWritable(): boolean {
		let restriction = this.eo.getSubEoRestriction(this.webComponent.foreignkeyfieldToParent);
		let result = super.isWritable() && restriction !== 'readonly' && this.subformMeta;
		return !!result && this.eo.canWrite();
	}

	keyDown(event: KeyboardEvent) {
		if (!this.multiSelect) {
			return;
		}
		if (event.code === 'Space') {
			this.multiSelect.show();
		}
	}

	loadSuggestions() {
		if (this.subformMeta) {
			this.lovDataService.loadLovEntries(this.createLovSearchConfig())
				.pipe(take(1))
				.subscribe((entries: LovEntry[]) => {
					entries = entries.filter((entry: LovEntry) => entry.name !== LovEntry.EMPTY.name || entry.id !== LovEntry.EMPTY.id);
					this.suggestions = entries.sort(LOV_ENTRY_NAME_ASCENDING);
					this.rebuildSelectionModel();
				});
		}
	}

	openParentInNewTab() {
		this.eo.getMeta()
			.pipe(take(1))
			.subscribe(meta => {
				// NUCLOS-4435: Details of dynamik BO
				let entityClassId = meta.getDetailEntityClassId() || meta.getBoMetaId();
				if (!entityClassId) {
					entityClassId = this.eo.getEntityClassId();
				}

				let href =
					this.hyperlinkService.getBaseUrl() + '/view/' +
					entityClassId +
					'/' +
					this.eo.getId() +
					'?' +
					EXPAND_SIDEVIEW_PARAM +
					'&' +
					SHOW_CLOSE_ON_SAVE_BUTTON_PARAM +
					'&' +
					`showHeader=${meta.displayAsListOnly() ? 'true' : 'false'}`;

				window.open(href, '_blank');
			});
	}

	addReferenceTarget() {
		this.referenceTargetService.canAddReference(this.eo, this.valueReferenceAttrMeta)
			.pipe(
				take(1),
				switchMap((canAdd) =>
					iif(
						() => canAdd,
						this.referenceTargetService.targetReference(
							this.eo,
							this.valueReferenceAttrMeta,
							'new',
							true,
							undefined,
							undefined,
							false,
							this.getAdvancedProperty('popupparameter')),
						of(void 0)
					)
				),
			)
			.subscribe(result => {
				this.createSubEo()
					.pipe(take(1))
					.subscribe(subEo => {
						if (result && subEo) {
							let lovEntry: LovEntry = {
								id: result.eoId,
								name: result.name
							};
							subEo.setAttribute(this.displayAttributeFqn, lovEntry);

							this.lovDataService.loadLovEntries(this.createLovSearchConfig())
								.pipe(take(1))
								.subscribe((entries: LovEntry[]) => {
									this.suggestions = entries.sort(LOV_ENTRY_NAME_ASCENDING);
									if (subEo) {
										this.addSubEO(subEo);
									}
									this.rebuildSelectionModel();
								});
						}
					});
			});
	}

	/**
	 * Add an identifying attribute to the overlay, so it can be identified within the tests.
	 * @param event
	 */
	onPanelShow(event: OverlayOnShowEvent | undefined) {
		event?.overlay?.setAttribute('ref-attr-id', this.webComponent.foreignkeyfieldToParent);
	}

	/**
	 * Collects both the newly add entries and the removed entries into lists and then applies an add/remove operation
	 * to the eo dependents.
	 * @param $event
	 */
	selectionChange($event) {
		let removedEntries = this.calculateEntriesToRemove($event);
		let addedEntries = this.calculateEntriesToAdd($event);
		let restoreEntries = this.calculateEntriesToRestore($event);

		this.removeEntries(removedEntries);
		this.addEntries(addedEntries);
		this.restoreEntries(restoreEntries);
		this.rebuildSelectionModel();
	}

	protected calculateEntriesToRestore($event) {
		return $event.value
			.filter((lovEntry: LovEntry) => this.existsSubEoForSuggestion(lovEntry));
	}

	protected calculateEntriesToAdd($event) {
		return $event.value
			.filter((lovEntry: LovEntry) => !this.existsSubEoForSuggestion(lovEntry));
	}

	protected existsSubEoForSuggestion(lovEntry: LovEntry) {
		if (this.subEOs) {
			return this.subEOs.some(subEo => subEo.getData()?.attributes[this.displayAttributeShortName].id === lovEntry.id);
		}
		return false;
	}

	protected calculateEntriesToRemove($event) {
		if (this.subEOs) {
			return this.subEOs
				.filter(subEo => !$event.value
				.some((lovEntry: LovEntry) => lovEntry.id === subEo.getData()?.attributes[this.displayAttributeShortName].id));
		}
		return undefined;
	}

	protected restoreEntries(entriesToRestore: LovEntry[] | undefined) {
		if (entriesToRestore) {
			entriesToRestore
				.map(lovEntry =>
					this.subEOs.find(subEo =>
						lovEntry.id === subEo.getData()?.attributes[this.displayAttributeShortName].id))
				.forEach(subEo => {
					if (subEo) {
						subEo.reset();
					}
				});
		}
	}

	protected addEntries(entriesToAdd: LovEntry[] | undefined) {
		if (entriesToAdd) {
			entriesToAdd.forEach((lovEntry: LovEntry) => {
				this.createSubEo()
					.pipe(take(1))
					.subscribe(subEo => {
						subEo?.setAttribute(this.displayAttributeFqn, lovEntry);
						if (subEo) {
							this.addSubEO(subEo);
						}
					});
			});
		}
	}

	protected removeEntries(entriesToRemove: SubEntityObject[] | undefined) {
		if (entriesToRemove) {
			entriesToRemove.forEach(subEO => {
				if (!subEO.isNew()) {
					// mark already saved subbos as deleted
					subEO.markAsDeleted(true);
					subEO.setSelected(false);
				} else {
					this.dependents?.removeAll([subEO]);
				}
			});
		}
	}

	protected addSubEO(subEO: SubEntityObject): void {
		this.addSubEOs([subEO]);
	}

	/**
	 * Adds multiple subEos to the top of the grid.
	 */
	protected addSubEOs(subEOs: SubEntityObject[]): void {
		if (this.dependents) {
			this.dependents.addAll(subEOs);
		}
	}

	protected getReferenceAttributeId() {
		return this.webComponent.foreignkeyfieldToParent;
	}

	protected getLogger(): Logger {
		return this.$log;
	}

	/**
	 * Creates a new Sub-EO for insertion into this subform, based on the data of the given EO.
	 */
	protected prepareNewSubEo(eo: EntityObject) {
		let parentForNewSubEo = this.eo;

		let subEo = new SubEntityObject(parentForNewSubEo,
			this.getReferenceAttributeId(),
			eo.getData(),
			this.injector.get(LocalStorageService),
			this.injector.get(FqnService));

		subEo.setComplete(true);

		// TODO: Clearing attribute restrictions here, because they apply only for main EOs.
		// This is only a workaround. There should be another generation service to
		// create subform data, which sets correct attribute restrictions in the first place.
		if (subEo.getData() !== null && subEo.getData() !== undefined) {
			subEo.getData()!.attrRestrictions = undefined;
		}

		let refAttrId = this.getReferenceAttributeId();
		if (refAttrId) {
			// Restrict reference to main EO
			let ref = {
				id: this.eo.getId(),
				name: this.eo.getTitle() // TODO: Respect possibly different string representation of VLP
			};
			subEo.setAttribute(refAttrId, ref);
		} else {
			this.getLogger().error('No refAttrId defined.');
		}

		if (subEo) {
			subEo.shadowID = eo.shadowID;
			this.eoVLPService.addEntityObjectMap(subEo);
			this.ruleService.updateRuleExecutor(subEo, this.eo).pipe(take(1)).subscribe();
		}

		return subEo;
	}

	private createThrowawaySubEntity() {
		let eoData = new EntityObjectData();
		eoData.boMetaId = this.subformMeta.getEntityClassId();
		return new SubEntityObject(
			this.eo,
			this.webComponent.foreignkeyfieldToParent,
			eoData,
			this.injector.get(LocalStorageService),
			this.fqnService
		);
	}

	private createLovSearchConfig() {
		return {
			eo: this.webComponent.valuelistProvider ? this.createThrowawaySubEntity() : undefined,
			attributeMeta: this.valueReferenceAttrMeta,
			quickSearchInput: '',
			resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT,
			vlp: this.webComponent.valuelistProvider,
			searchmode: true
		} as LovSearchConfig;
	}

	private createSubEo(): Observable<SubEntityObject | undefined> {
		let parentForNewSubEo = this.eo;
		if (!parentForNewSubEo) {
			// Currently only work for a single parent
			return of(undefined);
		}
		return this.entityObjectService
			.createNew(this.subformMeta.getBoMetaId(), parentForNewSubEo)
			.pipe(map((eo: EntityObject) => this.prepareNewSubEo(eo)));
	}

	private initDependents() {
		let dependents = this.eo.getDependentsAndLoad(
			this.webComponent.foreignkeyfieldToParent
		) as EntityObjectDependents;
		this.setDependents(dependents);
	}

	private setSubEos(subEos: SubEntityObject[]) {
		this.subEOs = subEos;
		this.rebuildSelectionModel();
	}

	private rebuildSelectionModel() {
		if (this.subEOs && this.suggestions) {
			let fieldName = this.fqnService.getShortAttributeNameFailsafe(
				this.subformMeta.getEntityClassId(),
				this.displayAttributeFqn
			);
			this.subModel = this.suggestions
				.filter(lovEntrySuggestion =>
					this.subEOs.some(entry =>
						entry.getData()?.attributes[fieldName]?.id === lovEntrySuggestion.id
						&& !entry.isMarkedAsDeleted()
					));
		}
	}

	private setDependents(dependents: EntityObjectDependents | undefined) {
		if (this.dependentSubscription) {
			this.dependentSubscription.unsubscribe();
		}
		this.dependents = dependents;
		if (this.dependents) {
			this.dependentSubscription =
				this.dependents.asObservable().subscribe((subEos: SubEntityObject[]) => this.setSubEos(subEos));
		} else {
			this.setSubEos([]);
		}
	}
}
