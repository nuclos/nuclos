import {
	Component,
	Injector,
	OnChanges,
	OnInit,
	SimpleChanges
} from '@angular/core';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';

@Component({
	selector: 'nuc-web-optiongroup',
	templateUrl: './web-optiongroup.component.html',
	styleUrls: ['./web-optiongroup.component.css']
})
export class WebOptiongroupComponent extends AbstractInputComponentDirective<WebOptiongroup>
	implements OnInit, OnChanges {
	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
		// do nothing
	}

	ngOnChanges(changes: SimpleChanges) {
		super.ngOnChanges(changes);
		setTimeout(() => {
			this.setDefaultIfNull();
		});
	}

	getStyle(): any {
		let superStyle = super.getStyle();

		superStyle['display'] = 'flex';
		superStyle['justify-content'] = 'space-evenly';
		if (this.webComponent.options.orientation === 'horizontal') {
			superStyle['flex-direction'] = 'row';
		} else {
			superStyle['flex-direction'] = 'column';
		}

		return superStyle;
	}

	getOptions() {
		return this.webComponent.options;
	}

	private setDefaultIfNull() {
		if (!super.getValue()) {
			this.setValue(this.webComponent.options.default);
		}
	}
}
