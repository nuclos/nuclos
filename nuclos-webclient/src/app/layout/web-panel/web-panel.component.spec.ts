import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WebPanelComponent } from './web-panel.component';

xdescribe('WebPanelComponent', () => {
	let component: WebPanelComponent;
	let fixture: ComponentFixture<WebPanelComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebPanelComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebPanelComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
