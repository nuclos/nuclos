import { Component, Injector, OnInit } from '@angular/core';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';

@Component({
	selector: 'nuc-web-password',
	templateUrl: './web-password.component.html',
	styleUrls: ['./web-password.component.css']
})
export class WebPasswordComponent extends AbstractInputComponentDirective<WebPassword> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

}
