/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { WebPhonenumberComponent } from './web-phonenumber.component';

xdescribe('WebPhonenumberComponent', () => {
	let component: WebPhonenumberComponent;
	let fixture: ComponentFixture<WebPhonenumberComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebPhonenumberComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebPhonenumberComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
