import { Component, Injector, OnInit } from '@angular/core';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';

@Component({
	selector: 'nuc-web-phonenumber',
	templateUrl: './web-phonenumber.component.html',
	styleUrls: ['./web-phonenumber.component.css']
})
export class WebPhonenumberComponent extends AbstractInputComponentDirective<WebPhonenumber> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

}
