import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebPlanningTableComponent } from './web-planning-table.component';

describe('WebPlanningTableComponent', () => {
	let component: WebPlanningTableComponent;
	let fixture: ComponentFixture<WebPlanningTableComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [WebPlanningTableComponent]
		})
			.compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(WebPlanningTableComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
