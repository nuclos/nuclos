import { Component, Injector, OnInit } from '@angular/core';
import { DateUtils } from '@modules/planning-table/time/date-utils';
import { AbstractWebComponentDirective } from '../shared/abstract-web-component.directive';

@Component({
	selector: 'nuc-web-planning-table',
	templateUrl: './web-planning-table.component.html',
	styleUrls: ['./web-planning-table.component.css']
})
export class WebPlanningTableComponent extends AbstractWebComponentDirective<WebPlanningTable> implements OnInit {

	planningTable: WebPlanningTable;
	fromDate: Date;
	untilDate: Date;

	constructor(protected injector: Injector) {
		super(injector);
		this.planningTable = this.webComponent;
		this.calculateDateBoundaries();
	}

	ngOnInit(): void {
	}

	ngOnChanges(simpleChanges: any): void {
		this.planningTable = this.webComponent;
		this.calculateDateBoundaries();
	}

	private calculateDateBoundaries() {
		if (this.planningTable.fieldDateFrom) {
			let attributeValueFrom = this.eo.getAttribute(this.planningTable.fieldDateFrom);
			if (attributeValueFrom) {
				let from = new Date(this.eo.getAttribute(this.planningTable.fieldDateFrom));
				if (this.isDate(from)) {
					from.setHours(0, 0, 0, 0);
					this.fromDate = from;
				}
			}
		} else if (this.planningTable.dateFrom) {
			this.fromDate = DateUtils.calc(this.planningTable.dateFrom);
			this.fromDate.setHours(0, 0, 0, 0);
		}

		if (this.planningTable.fieldDateUntil) {
			let attributeValueUntil = this.eo.getAttribute(this.planningTable.fieldDateUntil);
			if (attributeValueUntil) {
				let until = new Date(this.eo.getAttribute(this.planningTable.fieldDateUntil));
				if (this.isDate(until)) {
					until.setHours(23, 59, 59, 999);
					this.untilDate = until;
				}
			}
		} else if (this.planningTable.dateUntil) {
			this.untilDate = DateUtils.calc(this.planningTable.dateUntil);
			this.untilDate.setHours(23, 59, 59, 999);
		}

	}

	private isDate(parsedValue) {
		return (parsedValue !== 'Invalid Date') && !isNaN(parsedValue);
	}

}
