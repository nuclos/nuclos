import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WebScrollableBoxComponent } from './web-scrollable-box.component';

describe('WebScrollableBoxComponent', () => {
	let component: WebScrollableBoxComponent;
	let fixture: ComponentFixture<WebScrollableBoxComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [WebScrollableBoxComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebScrollableBoxComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
