import { AfterViewInit, Component, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
	selector: 'nuc-web-scrollable-box',
	templateUrl: './web-scrollable-box.component.html',
	styleUrls: ['./web-scrollable-box.component.scss']
})
export class WebScrollableBoxComponent implements OnInit, AfterViewInit {

	@Input() boxVisible: boolean;
	@Input() boxTitle: string;
	@Input() boxListLayout: TemplateRef<any>;
	@Input() boxHeight: number;
	@Input() boxWidth: number;
	@Input() items: Array<any>;

	constructor() {
	}

	ngOnInit(): void {
	}

	ngOnDestroy(): void {

	}

	ngAfterViewInit(): void {
	}
}
