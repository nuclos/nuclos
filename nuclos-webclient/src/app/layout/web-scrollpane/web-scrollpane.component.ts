import { Component, Injector, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Logger } from '@modules/log/shared/logger';
import { AbstractWebComponentDirective } from '../shared/abstract-web-component.directive';

@Component({
	selector: 'nuc-web-scrollpane',
	templateUrl: './web-scrollpane.component.html',
	styleUrls: ['./web-scrollpane.component.scss']
})
export class WebScrollpaneComponent extends AbstractWebComponentDirective<WebScrollpane> implements OnInit, OnChanges {

	constructor(
		injector: Injector,
		private $log: Logger) {
		super(injector);
	}

	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges): void {
	}

	getWebComponent() {
		return this.webComponent;
	}

	getStyle(): any {
		return {
			...super.getStyle(),
			'overflow-y': this.mapSettingToCss(this.webComponent.verticalscrollbar),
			'overflow-x': this.mapSettingToCss(this.webComponent.horizontalscrollbar)
		};
	}

	private mapSettingToCss(input: string): string {
		switch (input) {
			case 'asneeded':
				return 'auto';
			case 'always':
				return 'visible';
			case 'never':
				return 'hidden';
			default:
				return 'auto';
		}
	}
}
