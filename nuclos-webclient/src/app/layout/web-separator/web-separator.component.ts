import { Component, HostBinding, Injector, OnInit } from '@angular/core';
import { AbstractWebComponentDirective } from '../shared/abstract-web-component.directive';

@Component({
	selector: 'nuc-web-separator',
	templateUrl: './web-separator.component.html',
	styleUrls: ['./web-separator.component.css']
})
export class WebSeparatorComponent extends AbstractWebComponentDirective<WebSeparator> implements OnInit {
	@HostBinding('attr.role') role = 'separator';

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

	getOrientation() {
		return this.webComponent.orientation;
	}

}
