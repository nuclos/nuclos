import { AfterViewInit, Component, Injector, OnDestroy, ViewChild } from '@angular/core';
import { Logger } from '@modules/log/shared/logger';
import {
	Preference,
	PreferenceContent,
	SplitpanePreferenceContent
} from '@modules/preferences/preferences.model';
import { PreferencesService } from '@modules/preferences/preferences.service';
import { PrimeCustomSplitterComponent } from '@shared/components/prime-custom-splitter/prime-custom-splitter.component';
import { ObjectUtils } from '@shared/object-utils';
import { interval, Subject } from 'rxjs';
import { debounce, distinctUntilChanged, switchMap, take, takeWhile } from 'rxjs/operators';
import { AbstractWebComponentDirective } from '../shared/abstract-web-component.directive';

@Component({
	selector: 'nuc-web-splitpane',
	templateUrl: './web-splitpane.component.html',
	styleUrls: ['./web-splitpane.component.scss']
})
export class WebSplitpaneComponent extends AbstractWebComponentDirective<WebSplitpane> implements AfterViewInit, OnDestroy {

	orientation: string;
	dividerSize: number;
	collapsable: boolean;
	firstPanel: WebComponent;
	secondPanel: WebComponent;
	prefPanelSizesChanges$: Subject<SplitpanePreferenceContent> = new Subject<SplitpanePreferenceContent>();
	foundSplitpanePref: Preference<PreferenceContent> | undefined;

	@ViewChild('splitter') splitter: PrimeCustomSplitterComponent;

	private compAlive = true;

	constructor(
		injector: Injector,
		private $log: Logger,
		private preferenceService: PreferencesService) {
		super(injector);

		this.orientation = this.webComponent.dividerOrientation;
		this.collapsable = this.webComponent.collapsable;
		this.dividerSize = this.webComponent.dividerSize;

		this.firstPanel = this.webComponent.components[0];
		this.secondPanel = this.webComponent.components[1];
		this.compAlive = true;

		this.prefPanelSizesChanges$.pipe(
			takeWhile(() => this.compAlive),
			debounce(() => interval(250)),
			distinctUntilChanged((prev, curr) => ObjectUtils.compare(prev, curr)),
			switchMap((prefToSave: SplitpanePreferenceContent) =>
				this.preferenceService.savePreferenceItem({
					prefId: this.foundSplitpanePref?.prefId,
					type: 'userdefined',
					name: this.webComponent.name,
					content: prefToSave,
					boMetaId: this.eo.getEntityClassId()
				})
			)
		).subscribe((pref) => {
			this.foundSplitpanePref = pref;
		});
	}

	ngOnDestroy() {
		this.compAlive = false;
	}

	ngAfterViewInit() {
		this.preferenceService.getPreferences({
			type: ['userdefined'],
			boMetaId: this.eo.getEntityClassId(),
		}).pipe(
			take(1)
		).subscribe((preferences) => {
			this.foundSplitpanePref = preferences?.find((pref) =>
				pref?.name === this.webComponent.name
			);
			if (this.foundSplitpanePref !== undefined) {
				const panelSizes = (this.foundSplitpanePref.content as SplitpanePreferenceContent).panelSizes;
				if (panelSizes) {
					this.splitter.applyPanelsizes(panelSizes);
				}
			}
		});
	}

	panelResized(result: {event: Event, sizes: number[]}) {
		this.prefPanelSizesChanges$.next({
			panelSizes: [...result.sizes]
		});
	}
}
