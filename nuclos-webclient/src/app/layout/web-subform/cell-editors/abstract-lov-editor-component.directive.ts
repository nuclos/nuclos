import { HttpParams } from '@angular/common/http';
import { Directive, ElementRef, Injector, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { NuclosDefaults } from '../../../data/schema/nuclos-defaults';
import { EntityAttrMeta, LovEntry } from '../../../modules/entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../../modules/entity-object-data/shared/entity-object-event.service';
import { SubEntityObject } from '../../../modules/entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../../modules/entity-object-data/shared/entity-object.service';
import { LovDataService } from '../../../modules/entity-object-data/shared/lov-data.service';
import { LovSearchConfig } from '../../../modules/entity-object-data/shared/lov-search-config';
import { FqnService } from '../../../shared/service/fqn.service';
import { IdFactoryService } from '../../../shared/service/id-factory.service';
import { AbstractLovComponentDirective } from '../../abstract-lov/abstract-lov-component.directive';
import { LovHandler } from '../../abstract-lov/lov-handler';
import { NuclosCellEditorParams } from '../web-subform.model';
import { AbstractEditorComponent } from './abstract-editor-component';

export const AUTOCOMPLETE_PANEL_SELECTOR = 'p-autocomplete .ui-autocomplete-panel';

@Directive()
export abstract class AbstractLovEditorComponentDirective extends AbstractEditorComponent {
	/**
	 * Keys which must be caught (and not propagated) by the dropdown component,
	 * because they also trigger navigation in the subform.
	 */
	private static CATCH_KEYS = ['ArrowDown', 'ArrowUp', 'Enter'];

	lovEntries: LovEntry[] = [];
	subEO: SubEntityObject;
	advancedProperties: WebAdvancedProperty[];

	@ViewChild('lovComponent', {static: true}) lovComponent: AbstractLovComponentDirective<WebInputComponent>;

	protected lovHandler: LovHandler;

	protected attribute: any;

	protected staticValues = false;

	private componentId;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private lovDataService: LovDataService,
		protected elementRef: ElementRef,
		protected idFactory: IdFactoryService,
		protected fqnService: FqnService,
		protected injector: Injector
	) {
		super(entityObjectService, eoEventService, elementRef);

		this.lovHandler = {
			loadEntries: () => this.loadEntries(),
			loadFilteredEntries: search => this.loadFilteredEntries(search),
			refreshEntries: () => this.lovComponent.doRefreshEntries(),
			getValue: () => this.getValue(),
			setValue: value => this.setValue(value),
			clearValue: () => (this.params.value = undefined),
			getWidth: () => this.getWidth(),
			getHeight: () => this.getHeight(),
			stopEditing: () => this.stopEditing(),
			searchReferenceClick: () => this.stopEditing(),
			addReferenceClick: () => this.stopEditing(),
			entrySelected: value => this.selectDropdownOption(value),
			isEnabled: () => true,
			keydownHandler: event => this.handleKey(event),
			getVlpId: () => this.getVlpId(),
			getVlpParams: () => this.getVlpParams()
		};

		// when using the dropdown in the primeng p-autocomplete component the input element will be focused when clicking on the dropdown
		// this behaviour will always set back the focus to the input when trying to leave the input with a keyboard tab
		// disabling the dropdown button fixes this
		setTimeout(() => {
			let dropdownButton = this.elementRef.nativeElement.querySelector('button');
			if (dropdownButton) {
				$(dropdownButton).prop('disabled', true);
			}
		}, 0);
	}

	getVlpParams(): HttpParams {
		return this.getLovDataService().getLovParams(this.getLovSearchConfig(''));
	}

	getVlpId(): string | undefined {
		if (this.getVlp()) {
			return this.getVlp()!.value;
		}
		return undefined;
	}

	getLovSearchConfig(search: string): LovSearchConfig {
		return {
			attributeMeta: this.attributeMeta,
			eo: this.subEO,
			quickSearchInput: search,
			vlp: this.getVlp(),
			resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT,
			mandatorId: this.getEntityObject()
				.getRootEo()
				.getMandatorId()
		};
	}

	getLovDataService(): LovDataService {
		return this.lovDataService;
	}

	abstract loadEntries(): Observable<LovEntry[]>;

	abstract loadFilteredEntries(search: string): Observable<LovEntry[]>;

	getHandler() {
		return this.lovHandler;
	}

	getEntityObject() {
		return this.subEO;
	}

	agInit(params: NuclosCellEditorParams) {
		super.agInit(params);

		let attributeName = this.attributeMeta !== undefined ? this.attributeMeta.getAttributeName() : undefined;
		if (attributeName !== undefined && this.subEO !== undefined) {
			this.attribute = this.subEO.getAttribute(attributeName);
		}

		this.rowHeight = params.node.rowHeight + 'px';
		this.subEO = params.data;

		if (params.staticList) {
			this.lovEntries = params.staticList;
			this.staticValues = true;
		}

		setTimeout(() => {
			// fix wrong popup position for small subforms
			let popupEditor = this.elementRef.nativeElement.parentNode;
			let popupEditorPosition = $(popupEditor).position();
			if (popupEditorPosition && popupEditorPosition.top < 0 && params.node.rowHeight) {
				let top = (params.rowIndex + 1) * params.node.rowHeight;
				$(popupEditor).css({top: top + 'px'});
			}

			// open dropdown when starting edit
			let autoCompleteLov = this.lovComponent.getAutocomplete();
			if (autoCompleteLov) {
				this.lovComponent.showAllEntries();
				autoCompleteLov.focus = true;
				autoCompleteLov.focusInput();

				// select lov input text
				let textInput = autoCompleteLov.inputEL.nativeElement;
				if (textInput.selectionEnd - textInput.selectionStart === 0) {
					textInput.setSelectionRange(0, textInput.value.length);
				}
			}
		}, 0);

		this.advancedProperties = params.advancedProperties;
	}

	getName() {
		if (!this.attributeMeta) {
			return undefined;
		}

		let fieldName = this.fqnService.getShortAttributeName(
			this.subEO.getEntityClassId(),
			this.attributeMeta.getAttributeID()
		);
		return fieldName;
	}

	getNameForHtml() {
		let result = this.getName();

		if (result) {
			result = 'attribute-' + result;
		}

		return result;
	}

	/**
	 * The ID of this component, to be used as HTML "id" attribute.
	 */
	getId(): string {
		if (!this.componentId) {
			this.componentId = this.getComponentIdOrNew();
		}

		return this.componentId;
	}

	selectDropdownOption(value) {
		let v = this.getValue();
		if (!v || v.id !== value?.id) {
			this.setValue(value);
		}

		this.getGridApi().stopEditing();

		// set focus to current cell after stopEditing
		super.reFocusCurrentCell();
	}

	getAttributeMeta(): EntityAttrMeta {
		return this.attributeMeta;
	}

	getAttribute(): any {
		return this.attribute;
	}

	stopEditing(): void {
		this.getGridApi().stopEditing();
	}

	protected getAutocomplete() {
		return this.lovComponent.getAutocomplete();
	}

	private getComponentIdOrNew() {
		return 'generated-' + this.idFactory.getNextId();
	}

	private handleKey(event: KeyboardEvent) {
		if (event.key === 'Escape') {
			this.stopEditing();
			event.preventDefault();
			event.stopPropagation();
			setTimeout(() => {
				if (this.params.api.getEditingCells().length === 0) {
					$('div.ag-cell[col-id="' + this.attributeMeta?.getAttributeID() + '"]')
						.first()
						.trigger('focus');
				}
			});
		}

		if (AbstractLovEditorComponentDirective.CATCH_KEYS.indexOf(event.code) >= 0) {
			event.preventDefault();
			event.stopPropagation();
		}

		super.keydown(event);
	}
}
