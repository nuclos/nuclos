import { Component, ElementRef, ViewChild } from '@angular/core';
import { EntityObjectEventService } from '@modules/entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { CellEditingStoppedEvent, CellKeyPressEvent } from 'ag-grid-community';
import { NuclosCellEditorParams, NuclosCellRendererParams } from '../../web-subform.model';
import { AbstractEditorComponent } from '../abstract-editor-component';


/**
 * TODO: This is almost an exact duplicate of SubformBooleanRendererComponent.
 */
@Component({
	selector: 'nuc-subform-boolean-editor',
	templateUrl: './subform-boolean-editor.component.html',
	styleUrls: ['./subform-boolean-editor.component.css']
})
export class SubformBooleanEditorComponent extends AbstractEditorComponent {

	@ViewChild('checkSpan') checkSpan: ElementRef;

	private isEditable = false;
	private numberStates;

	private cellPressHandler = this.cellKeyPressEvent.bind(this);
	private cellStoppedHandler = this.cellEditingStoppedEvent.bind(this);

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		ref: ElementRef
	) {
		super(entityObjectService, eoEventService, ref);
	}

	agInit(params: NuclosCellEditorParams) {
		super.agInit(params);
		let nuclosCellRenderParams = params.colDef.cellRendererParams.nuclosCellRenderParams as NuclosCellRendererParams;
		this.isEditable = nuclosCellRenderParams.editable;
		this.numberStates = nuclosCellRenderParams.numberStates;

		this.getGridApi().addEventListener(
			'cellKeyPress',
			this.cellPressHandler
		);

		this.getGridApi().addEventListener(
			'cellEditingStopped',
			this.cellStoppedHandler
		);

		if (this.numberStates === '1') { // Boolean column in a multi-column y-axis
			this.toggleValue();
		}
	}

	handleKeyboardEvent(event: KeyboardEvent) {
		if (event.target && $(event.target).attr('col-id') !== this.getColDef()?.colId) {
			return;
		}

		if (event.code === 'Space') {
			this.toggleValue();
			event.preventDefault();
			event.stopPropagation();
		}

		if (event.code === 'Enter' || event.code === 'Escape') {
			this.getGridApi().removeEventListener(
				'cellKeyPress',
				this.cellPressHandler
			);
			event.preventDefault();
			event.stopPropagation();
		}
	}

	toggleValue() {
		if (!this.isEditable) {
			return;
		}
		if (this.numberStates === '3') {
			this.setValue((this.getValueSafe() + 1) % 3);
		} else {
			this.setValue(!this.getValue());
		}

		if (this.numberStates === '1') { // Boolean column in a multi-column y-axis
			let subEo = this.getSubEntityObject();
			if (subEo && !subEo.isDirty()) {
				subEo.setDirty(true);
				this.params.api?.refreshCells();
			}
			return;
		}

		let eo = this.getEntityObject();
		if (eo) {
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				this.getSubEntityObject().setAttribute(colDef.field, this.getValue());
			}
		}
	}

	isPopup(): boolean {
		// TODO: This is the only editor without popup.
		// It would be better if all subform editors behaved the same way.
		return false;
	}

	getValueSafe() {
		return this.getValue() ? this.getValue() : 0;
	}

	getValueMapped() {
		if (this.numberStates === '3') {
			return this.getValueSafe() + 2;
		}
		return super.getValue();
	}

	private cellKeyPressEvent(event: CellKeyPressEvent): void {
		const focusedCell = event.api?.getFocusedCell();
		if (focusedCell?.column === event?.column &&
			focusedCell?.rowIndex === event?.rowIndex && event.event) {
			this.handleKeyboardEvent(event.event as KeyboardEvent);
		}
	}

	private cellEditingStoppedEvent(event: CellEditingStoppedEvent): void {
		if (this.params.column === event.column &&
			this.params.rowIndex === event.rowIndex) {
			this.getGridApi().removeEventListener(
				'cellKeyPress',
				this.cellPressHandler
			);
			this.getGridApi().removeEventListener(
				'cellEditingStopped',
				this.cellStoppedHandler
			);
		}
	}
}
