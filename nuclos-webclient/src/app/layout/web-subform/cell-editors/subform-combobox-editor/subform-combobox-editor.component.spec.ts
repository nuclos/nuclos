import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SubformComboboxEditorComponent } from './subform-combobox-editor.component';

xdescribe('ComboboxEditorComponent', () => {
	let component: SubformComboboxEditorComponent;
	let fixture: ComponentFixture<SubformComboboxEditorComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [SubformComboboxEditorComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SubformComboboxEditorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
