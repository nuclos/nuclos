import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EntityObjectEventService } from '../../../../modules/entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../modules/entity-object-data/shared/entity-object.service';
import { Logger } from '../../../../modules/log/shared/logger';
import { NuclosCellEditorParams } from '../../web-subform.model';
import { AbstractEditorComponent } from '../abstract-editor-component';

@Component({
	selector: 'nuc-subform-email-editor',
	templateUrl: './subform-email-editor.component.html',
	styleUrls: ['./subform-email-editor.component.css']
})
export class SubformEmailEditorComponent extends AbstractEditorComponent {

	@ViewChild('input', {static: true}) input: ElementRef;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private $log: Logger,
		ref: ElementRef,
		public router: Router
	) {
		super(entityObjectService, eoEventService, ref);
	}

	agInit(params: NuclosCellEditorParams): any {
		super.agInit(params);

		if (params.charPress) {
			super.setValue(params.charPress);
			return;
		}

		super.setValue(params.value);
	}

	setStringValue(value) {
		super.setValue(value);
	}

	stopEditing(): void {
		this.getGridApi().stopEditing();
	}
}
