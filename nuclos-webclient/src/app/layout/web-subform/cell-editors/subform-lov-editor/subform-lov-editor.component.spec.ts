import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SubformLovEditorComponent } from './subform-lov-editor.component';

xdescribe('LovEditorComponent', () => {
	let component: SubformLovEditorComponent;
	let fixture: ComponentFixture<SubformLovEditorComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [SubformLovEditorComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SubformLovEditorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
