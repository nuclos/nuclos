import { Component, ElementRef, Injector } from '@angular/core';
import { Observable, of } from 'rxjs';
import { LovEntry } from '../../../../modules/entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../../../modules/entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../modules/entity-object-data/shared/entity-object.service';
import { LovDataService } from '../../../../modules/entity-object-data/shared/lov-data.service';
import { FqnService } from '../../../../shared/service/fqn.service';
import { IdFactoryService } from '../../../../shared/service/id-factory.service';
import { ComboboxUtils } from '../../../shared/combobox-utils';
import { AbstractLovEditorComponentDirective } from '../abstract-lov-editor-component.directive';

@Component({
	selector: 'nuc-subform-lov-editor',
	templateUrl: './subform-lov-editor.component.html',
	styleUrls: ['./subform-lov-editor.component.scss']
})
export class SubformLovEditorComponent extends AbstractLovEditorComponentDirective {

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		lovDataService: LovDataService,
		protected elementRef: ElementRef,
		protected idFactory: IdFactoryService,
		fqnService: FqnService,
		injector: Injector
	) {
		super(
			entityObjectService,
			eoEventService,
			lovDataService,
			elementRef,
			idFactory,
			fqnService,
			injector
		);
	}

	loadEntries(): Observable<LovEntry[]> {
		return this.loadFilteredEntries('');
	}

	loadFilteredEntries(search: string): Observable<LovEntry[]> {
		return this.getLovDataService().loadLovEntries(this.getLovSearchConfig(search));
	}

	stopEditing(): void {
		this.getGridApi().stopEditing();
	}
}
