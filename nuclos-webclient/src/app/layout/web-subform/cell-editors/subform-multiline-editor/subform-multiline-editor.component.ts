import { Component, ElementRef, ViewChild } from '@angular/core';
import { CellClassFunc } from 'ag-grid-community/dist/lib/entities/colDef';
import { EntityObjectEventService } from '../../../../modules/entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../modules/entity-object-data/shared/entity-object.service';
import { Logger } from '../../../../modules/log/shared/logger';
import { NuclosCellEditorParams } from '../../web-subform.model';
import { WebSubformService } from '../../web-subform.service';
import { AbstractEditorComponent } from '../abstract-editor-component';

@Component({
	selector: 'nuc-subform-multiline-editor',
	templateUrl: './subform-multiline-editor.component.html',
	styleUrls: ['./subform-multiline-editor.component.css']
})
export class SubformMultilineEditorComponent extends AbstractEditorComponent {

	@ViewChild('input', {static: true}) input: ElementRef;

	height;

	private initialHeight;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private $log: Logger,
		ref: ElementRef
	) {
		super(entityObjectService, eoEventService, ref);
	}

	agInit(params: NuclosCellEditorParams): any {
		super.agInit(params);

		setTimeout(() => {
			if (this.input) {
				this.input.nativeElement.focus();
			}
		});

		if (params.charPress) {
			super.setValue(params.charPress);
			return;
		}

		super.setValue(params.value);
	}

	setStringValue(value) {
		this.setEntityObjectDirty();

		let oldNlCount = this.countNewlineCharacters(this.params.value);
		let newNlCount = this.countNewlineCharacters(value);

		super.setValue(value);

		if (oldNlCount !== newNlCount) {
			this.calculateHeight();
		}
	}

	calculateHeight() {
		let styles = this.isAutoHeightCell(this.params) ? [
			'white-space:normal',
			'word-break:normal',
			'overflow-wrap: break-word'
		] : [];
		let newHeight = WebSubformService.calculateCellHeightForText(
			this.getValue(),
			['form-control', 'form-control-sm']
			, styles, this.getWidth())?.height;

		// Editor should have at least the row height
		newHeight = Math.max(newHeight ?? 0, super.getHeight());

		if (!this.initialHeight) {
			this.initialHeight = newHeight;
		}

		this.height = Math.max(this.initialHeight, newHeight);
	}

	getHeight() {
		return this.height;
	}

	keydown(event: KeyboardEvent): void {
		// Enter
		if (event.keyCode === 13) {
			// Do not propagate to grid, or editing will stop
			event.stopPropagation();
			return;
		}
		super.keydown(event);
	}

	private isAutoHeightCell(params: NuclosCellEditorParams) {
		let cellClass: string | string[] | CellClassFunc | undefined | null = params.colDef?.cellClass;

		if (typeof cellClass === 'function') {
			cellClass = cellClass(params.colDef.cellEditorParams);
		}
		return cellClass?.includes('ag-cell-defaultHeight');
	}

	private countNewlineCharacters(value: string): number {
		return value ? value.split('\n').length - 1 : 0;
	}
}
