import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SubformStringEditorComponent } from './subform-string-editor.component';

xdescribe('SubformStringEditorComponent', () => {
	let component: SubformStringEditorComponent;
	let fixture: ComponentFixture<SubformStringEditorComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [SubformStringEditorComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SubformStringEditorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
