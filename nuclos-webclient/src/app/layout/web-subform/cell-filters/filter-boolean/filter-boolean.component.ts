import { Component } from '@angular/core';
import { IFilterAngularComp } from 'ag-grid-angular';
import { IAfterGuiAttachedParams, IDoesFilterPassParams, IFilterParams, RowNode } from 'ag-grid-community';

type CustomizedFilterType = 'all' | 'true' | 'false';

@Component({
	selector: 'nuc-filter-boolean',
	templateUrl: './filter-boolean.component.html',
	styleUrls: ['./filter-boolean.component.scss']
})
export class FilterBooleanComponent implements IFilterAngularComp {
	booleanFilterType: CustomizedFilterType = 'all';

	private params: IFilterParams;
	private valueGetter: (rowNode: RowNode) => any;

	onChangeIsCustomized(event) {
		this.booleanFilterType = event;
		this.params.filterChangedCallback();
	}

	agInit(params: IFilterParams): void {
		this.params = params;
		this.valueGetter = params.valueGetter;
	}

	isFilterActive(): boolean {
		return this.booleanFilterType === 'true' || this.booleanFilterType === 'false';
	}

	doesFilterPass(params: IDoesFilterPassParams): boolean {
		let value: boolean = this.valueGetter(params.node);
		return (
			params.data.isDirty ||
			this.booleanFilterType === 'all' ||
			(this.booleanFilterType === 'true' && value) ||
			(this.booleanFilterType === 'false' && !value)
		);
	}

	getModel(): any {
		return {
			value: this.booleanFilterType,
			filterType: 'boolean'
		};
	}

	setModel(model: any): void {
		if (model) {
			this.booleanFilterType = model.value;
			this.params.filterChangedCallback();
		}
	}

	// if new rows are inserted e.g with + button we need to reset this filter
	onNewRowsLoaded(): void {
		this.booleanFilterType = 'all';
	}

	afterGuiAttached(params: IAfterGuiAttachedParams): void {
	}
}
