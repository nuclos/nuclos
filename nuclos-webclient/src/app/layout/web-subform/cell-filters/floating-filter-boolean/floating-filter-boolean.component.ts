import { Component } from '@angular/core';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { AgFrameworkComponent } from 'ag-grid-angular';
import {
	FilterChangedEvent,
	IAfterGuiAttachedParams,
	IFloatingFilter,
	IFloatingFilterParams,
} from 'ag-grid-community';

type CustomizedFilterType = 'all' | 'true' | 'false';

@Component({
	selector: 'nuc-floating-filter-boolean',
	templateUrl: './floating-filter-boolean.component.html',
	styleUrls: ['./floating-filter-boolean.component.scss']
})
export class FloatingFilterBooleanComponent implements IFloatingFilter,
	AgFrameworkComponent<IFloatingFilterParams> {
	booleanFilterType: CustomizedFilterType = 'all';
	suggestions: any[] = [
		{value: 'all', name: this.i18n.getI18n('webclient.subform.booleanFilter.all')},
		{value: 'true', name: this.i18n.getI18n('webclient.subform.booleanFilter.true')},
		{value: 'false', name: this.i18n.getI18n('webclient.subform.booleanFilter.false')}
	];

	colId: string;
	private params: IFloatingFilterParams;

	constructor(
		private i18n: NuclosI18nService
	) { }

	onChangeIsCustomized(event) {
		this.booleanFilterType = event;
		this.params.parentFilterInstance(function (instance) {
			instance.setModel(
				{
					value: event ? event.value : undefined
				}
			);
		});
	}

	afterGuiAttached(params?: IAfterGuiAttachedParams): void {
	}

	onParentModelChanged(parentModel: any, filterChangedEvent?: FilterChangedEvent): void {
		if (!parentModel) {
			this.booleanFilterType = 'all';
		} else {
			this.booleanFilterType = this.suggestions.find(filterType => filterType.value === parentModel.value) ?? 'all';
		}
	}

	agInit(params: IFloatingFilterParams): void {
		this.params = params;
		this.colId = params.column.getColId();
	}

}
