import { Component } from '@angular/core';
import { AgFrameworkComponent } from 'ag-grid-angular';
import { FilterChangedEvent, IAfterGuiAttachedParams, IFloatingFilter, IFloatingFilterParams } from 'ag-grid-community';

@Component({
	selector: 'nuc-floating-filter-date',
	templateUrl: './floating-filter-date.component.html',
	styleUrls: ['./floating-filter-date.component.scss']
})
export class FloatingFilterDateComponent implements IFloatingFilter,
	AgFrameworkComponent<IFloatingFilterParams> {

	model: any;
	colId: string;
	private filterType = 'equals';
	private modelChangeTimeout: any;
	private params: IFloatingFilterParams;

	constructor() { }

	agInit(params: IFloatingFilterParams): void {
		this.params = params;
		this.colId = this.params.column.getColId();
	}

	afterGuiAttached?(params?: IAfterGuiAttachedParams | undefined): void { }

	onParentModelChanged(parentModel: any, filterChangedEvent?: FilterChangedEvent | null | undefined): void {
		if (!parentModel) {
			this.model = '';
		} else {
			this.model = parentModel.dateFrom;
			this.filterType = parentModel.type;
		}
	}

	changeFilterModel(newModel: string) {
		let dateFilterModel = this.createDateFilterModel(newModel);
		this.params.parentFilterInstance(function (instance) {
			instance.setModel(dateFilterModel);
		});
		this.params.api.onFilterChanged();
	}

	createDateFilterModel(dateAsString: string) {
		return {
			dateFrom: dateAsString,
			type: this.filterType
		};
	}

	onChangeEvent(newModel: string) {
		clearTimeout(this.modelChangeTimeout);
		this.modelChangeTimeout = setTimeout(() => {
			this.changeFilterModel(newModel);
		}, 400);
	}

}
