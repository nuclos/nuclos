import { Component } from '@angular/core';
import { AgFrameworkComponent } from 'ag-grid-angular';
import {
	FilterChangedEvent,
	IAfterGuiAttachedParams,
	IFloatingFilter,
	IFloatingFilterParams,
} from 'ag-grid-community';
import { WebSubformComponent } from '../../web-subform.component';

@Component({
	selector: 'nuc-floating-filter-editor',
	templateUrl: './floating-filter-editor.component.html',
	styleUrls: ['./floating-filter-editor.component.scss']
})
export class FloatingFilterEditorComponent implements IFloatingFilter,
	AgFrameworkComponent<IFloatingFilterParams> {

	subformFilterKey;
	toggleSearchEditor: Function;
	hideSearchEditor: Function;
	checkOutsideClicked: Function;

	private params: IFloatingFilterParams;

	constructor() {

	}

	afterGuiAttached(params?: IAfterGuiAttachedParams): void {

	}

	onParentModelChanged(parentModel: any, filterChangedEvent?: FilterChangedEvent): void {

	}

	agInit(params: any): void {
		this.params = params;
		this.subformFilterKey = params.filterKey;
		this.hideSearchEditor = params.hideSearchEditor;
		this.toggleSearchEditor = params.toggleSearchEditor;
		this.checkOutsideClicked = params.checkOutsideClicked;
	}

	toggleSubformSearchEditor() {
		this.toggleSearchEditor();
	}

	hideSubformSearchEditor() {
		this.hideSearchEditor();
	}

	clickOutside($event: EventTarget | null) {
		let clickOutsidePopover = this.checkOutsideClicked($event);
		if (clickOutsidePopover
			&& $event
			&& !$($event).hasClass('dd-item') // item from selection
			&& !$($event).hasClass('ui-autocomplete-list-item') // selection for attributes
			&& (
				!$($event).hasClass('icon-label') &&
				!$($event).hasClass('icon-preview') &&
				!$($event).hasClass('material-icons')
			) // icon-picker in search editor favorite
		) {
			this.hideSubformSearchEditor();
		}
	}

}
