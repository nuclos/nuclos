import { Component, OnDestroy, ViewChild } from '@angular/core';
import { EntityAttrMeta, EntityMeta, LovEntry } from '@modules/entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '@modules/entity-object-data/shared/entity-object-event.service';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { LovDataService } from '@modules/entity-object-data/shared/lov-data.service';
import { LovSearchConfig } from '@modules/entity-object-data/shared/lov-search-config';
import { MetaService } from '@modules/entity-object-data/shared/meta.service';
import { VlpContext } from '@modules/entity-object-data/shared/vlp-context';
import { RuleService } from '@modules/rule/shared/rule.service';
import { DropdownComponent } from '@modules/ui-components/dropdown/dropdown.component';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { ObjectUtils } from '@shared/object-utils';
import { RuleUtils } from '@shared/rule-utils';
import { AgFrameworkComponent } from 'ag-grid-angular';
import {
	FilterChangedEvent,
	IAfterGuiAttachedParams,
	IFloatingFilter,
	IFloatingFilterParams,
	TextFilterModel
} from 'ag-grid-community';
import { filter, map, mergeMap, take, takeUntil, tap } from 'rxjs/operators';
import { NuclosDefaults } from '../../../../data/schema/nuclos-defaults';
import { EMPTY, Subject } from 'rxjs';

const definedRules = (rules: Rules) => !!rules && Object.keys(rules).length > 0;
const definedEntityAttrMeta = (entityAttrMeta: EntityAttrMeta) => !!entityAttrMeta;

@Component({
	selector: 'nuc-floating-filter-lov',
	templateUrl: './floating-filter-lov.component.html',
	styleUrls: ['./floating-filter-lov.component.css']
})
export class FloatingFilterLovComponent implements IFloatingFilter,
	AgFrameworkComponent<IFloatingFilterParams>, OnDestroy {

	colId: string;
	modelChangeTimeout: any;
	model: string | LovEntry | undefined = LovEntry.EMPTY;
	suggestions: LovEntry[];
	entityAttrMeta: EntityAttrMeta;
	eo: IEntityObject | undefined;
	meta: EntityMeta;
	valuelistProvider: WebValuelistProvider;

	@ViewChild('dropdown') dropdownElement: DropdownComponent | undefined;

	private params: IFloatingFilterParams;
	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private vlpService: EntityVLPService,
		private lovDataService: LovDataService,
		private ruleService: RuleService,
		private metaService: MetaService,
		private eoEventService: EntityObjectEventService,
	) {
	}

	selectEntry($event: string | LovEntry) {
		const nonDottedListEndClicked = $event instanceof LovEntry ? !this.dropdownElement?.isDottedListEndClicked($event) : true;
		if (nonDottedListEndClicked) {
			clearTimeout(this.modelChangeTimeout);
			this.modelChangeTimeout = setTimeout(() => {
				this.onChangeIsCustomized($event);
			}, 400);
		}
	}

	onChangeIsCustomized(event: string | LovEntry) {
		if (event === this.model) {
			return;
		}
		let filterText = typeof event === 'string' ? event : event.name;
		let textFilterModel = this.createTextFilterModel(filterText);
		this.model = event;
		this.params.parentFilterInstance(function (instance) {
			instance.setModel(textFilterModel);
		});
		this.params.api.onFilterChanged();
	}

	createTextFilterModel(searchText) {
		return {
			filterType: 'text',
			type: 'like',
			filter: searchText
		} as TextFilterModel;
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	afterGuiAttached(params?: IAfterGuiAttachedParams): void { }

	onParentModelChanged(parentModel: any, filterChangedEvent?: FilterChangedEvent): void {
		if (parentModel) {
			if (this.suggestions && this.suggestions.filter(entry => entry.name === parentModel.filter).length === 1) {
				this.model = this.suggestions.find(entry => entry.name.includes(parentModel.filter));
			} else {
				this.model = parentModel.filter;
				this.dropdownElement?.setInput(parentModel.filter);
			}
		} else {
			this.model = '';
		}
		let model = this.params.api.getFilterModel();
		this.refreshParametersAndReload(model);
	}

	agInit(params: IFloatingFilterParams): void {
		this.params = params;
		this.colId = this.params.column.getColId();
		if (params.filterParams) {
			this.eoEventService.observeSelectedEo()
				.pipe(takeUntil(this.unsubscribe$))
				.subscribe(eo => this.eo = eo);
			this.meta = (params.filterParams as any).meta;
			this.entityAttrMeta = (params.filterParams as any).entityAttrMeta;
			this.valuelistProvider = (params.filterParams as any).valuelistProvider;
			let model = this.params.api.getFilterModel();
			this.refreshParametersAndReload(model);
			this.loadEntries();
		}
	}

	private createLovSearchTextConfig(entityAttr: EntityAttrMeta | undefined, searchText: string) {
		return {
			attributeMeta: entityAttr,
			quickSearchInput: searchText,
			resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT
		} as LovSearchConfig;
	}

	private setParameter(searchConfig: LovSearchConfig, entityClassId: string, parameterName: string) {
		return this.lovDataService.loadLovEntries(searchConfig)
			.pipe(
				tap(results => results.shift()),
				map(results => RuleUtils.getLovParameterValue(results)),
				tap(parameterValue => {
					const vlpSearchContext: VlpContext = this.vlpService.getVlpSearchContext();
					vlpSearchContext.setVlpParameter(entityClassId, this.entityAttrMeta.getAttributeID(), parameterName, parameterValue);
					this.vlpService.updateVlpSearchContext(vlpSearchContext);
					this.loadEntries();
				})
			);
	}

	private executeQualifyingVlpAction(rule: Rule, subformFilterPrefs: any) {
		const refreshVlpRule: RuleAction | undefined = RuleUtils.findQualifyingVlpRefreshAction(rule, this.entityAttrMeta);
		if (refreshVlpRule) {
			const entityClassId = refreshVlpRule['entity'];
			const parameterName = refreshVlpRule['parameterForSourcecomponent'];
			const searchText: string = RuleUtils.getSearchTextFromPref(subformFilterPrefs, rule);
			const sourceComponent: string = rule.event.sourcecomponent;
			return this.metaService.getBoMeta(entityClassId)
				.pipe(
					map(meta => meta.getAttributeMetaByFqn(sourceComponent)),
					filter(definedEntityAttrMeta),
					map(entityAttr => this.createLovSearchTextConfig(entityAttr, searchText)),
					mergeMap(searchConfig => this.setParameter(searchConfig, entityClassId, parameterName))
				);
		}
		return EMPTY;
	}

	private refreshParametersAndReload(subformFilterPrefs: any) {
		if (this.eo && this.eo instanceof EntityObject) {
			this.ruleService.getLayoutRules(this.eo as EntityObject)
				.pipe(
					// filter out undefined rules
					filter(definedRules),
					mergeMap((rules: Rules) => rules!.rules),
					mergeMap((rule: Rule) => this.executeQualifyingVlpAction(rule, subformFilterPrefs))
				).subscribe();
		}
	}

	private createLovSearchConfig() {
		return {
			attributeMeta: this.entityAttrMeta,
			entityMeta: this.meta,
			vlp: this.valuelistProvider,
			quickSearchInput: '',
			resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT,
			searchmode: true
		};
	}

	private loadEntries() {
		const search: LovSearchConfig = this.createLovSearchConfig();

		this.lovDataService.loadLovEntries(search)
			.pipe(take(1))
			.subscribe((entries: LovEntry[]) => {
				if (entries.length !== 1 && entries[0] !== LovEntry.EMPTY) {
					entries.unshift(LovEntry.EMPTY);
				}
				if (!ObjectUtils.isEqual(this.suggestions, entries)) {
					this.suggestions = entries;
					if (this.model) {
						let previousModel: LovEntry | string = this.model;
						let newModel = entries.find((entry: LovEntry) =>
							typeof previousModel === 'string' ? entry.name === previousModel : entry.id === previousModel.id);
						if (newModel) {
							this.model = newModel;
						}
					}
				}
			});
	}

}
