import { Component } from '@angular/core';
import { NumberService } from '@shared/service/number.service';
import { AgFrameworkComponent } from 'ag-grid-angular';
import { FilterChangedEvent, IAfterGuiAttachedParams, IFloatingFilter, IFloatingFilterParams } from 'ag-grid-community';

@Component({
	selector: 'nuc-floating-filter-number',
	templateUrl: './floating-filter-number.component.html',
	styleUrls: ['./floating-filter-number.component.css']
})
export class FloatingFilterNumberComponent implements IFloatingFilter,
	AgFrameworkComponent<IFloatingFilterParams> {

	colId: string;
	model: string;
	keydown: boolean;
	private params: IFloatingFilterParams;
	private filterType = 'equals';
	private modelChangeTimeout: any;

	constructor(
		private numberService: NumberService,
	) { }

	agInit(params: IFloatingFilterParams): void {
		this.params = params;
		this.colId = params.column.getColId();
	}

	afterGuiAttached?(params?: IAfterGuiAttachedParams | undefined): void { }

	onParentModelChanged(parentModel: any, filterChangedEvent?: FilterChangedEvent | null | undefined): void {
		if (!parentModel) {
			this.model = '';
		} else {
			this.model = this.numberService.format(parentModel.filter);
			this.filterType = parentModel.type;
		}
	}

	changeFilterModel(newModel: string) {
		let numberFilterModel = this.createNumberFilterModel(newModel);
		this.params.parentFilterInstance(function (instance) {
			instance.setModel(numberFilterModel);
		});
		this.params.api.onFilterChanged();
	}

	createNumberFilterModel(filterNumberString: string) {
		return {
			filter: filterNumberString,
			type: this.filterType
		};
	}

	onChangeEvent(newModel: string) {
		clearTimeout(this.modelChangeTimeout);
		this.modelChangeTimeout = setTimeout(() => {
			this.changeFilterModel(newModel);
		}, 400);
	}

}
