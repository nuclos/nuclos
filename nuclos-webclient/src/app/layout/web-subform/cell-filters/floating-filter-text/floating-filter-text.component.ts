import { Component, OnDestroy } from '@angular/core';
import { AgFrameworkComponent } from 'ag-grid-angular';
import { FilterChangedEvent, IAfterGuiAttachedParams, IFloatingFilter, IFloatingFilterParams } from 'ag-grid-community';

@Component({
	selector: 'nuc-floating-filter-text',
	templateUrl: './floating-filter-text.component.html',
	styleUrls: ['./floating-filter-text.component.css']
})
export class FloatingFilterTextComponent implements IFloatingFilter,
	AgFrameworkComponent<IFloatingFilterParams>, OnDestroy {

	colId: string;
	model: string;
	keydown: boolean;
	private modelChangeTimeout: any;
	private filterType = 'contains';
	private params: IFloatingFilterParams;

	constructor() { }

	ngOnDestroy(): void { }

	agInit(params: IFloatingFilterParams): void {
		this.params = params;
		this.colId = params.column.getColId();
	}

	afterGuiAttached?(params?: IAfterGuiAttachedParams | undefined): void { }

	onParentModelChanged(parentModel: any, filterChangedEvent?: FilterChangedEvent | null | undefined): void {
		if (!parentModel) {
			this.model = '';
		} else {
			this.model = parentModel.filter + '';
			this.filterType = parentModel.type;
		}
	}

	changeFilterModel(parentModel) {
		if (this.keydown) {
			this.queueFilterUpdate(parentModel);
		} else {
			let textFilterModel = this.createTextFilterModel(parentModel);
			this.params.parentFilterInstance(function (instance) {
				instance.setModel(textFilterModel);
			});
			this.params.api.onFilterChanged();
		}
	}

	createTextFilterModel(filterText) {
		return {
			filter: filterText,
			type: this.filterType
		};
	}

	onChangeEvent(parentModel) {
		this.queueFilterUpdate(parentModel);
	}

	queueFilterUpdate(parentModel) {
		clearTimeout(this.modelChangeTimeout);
		this.modelChangeTimeout = setTimeout(() => {
			this.changeFilterModel(parentModel.target.value);
		}, 400);
	}

}
