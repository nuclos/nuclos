import { DateFilter, IDoesFilterPassParams } from 'ag-grid-community';

export class IgnoreDirtyDateFilter extends DateFilter {
	doesFilterPass(params: IDoesFilterPassParams): boolean {
		if (params.node.data.isDirty()) {
			return true;
		} else {
			return super.doesFilterPass(params);
		}
	}
}
