import { IDoesFilterPassParams, NumberFilter } from 'ag-grid-community';

export class IgnoreDirtyNumberFilter extends NumberFilter {
	doesFilterPass(params: IDoesFilterPassParams): boolean {
		if (params.node.data.isDirty()) {
			return true;
		} else {
			return super.doesFilterPass(params);
		}
	}
}
