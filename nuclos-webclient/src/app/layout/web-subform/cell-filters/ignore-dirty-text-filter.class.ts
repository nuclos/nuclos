import { IDoesFilterPassParams, TextFilter } from 'ag-grid-community';

export class IgnoreDirtyTextFilter extends TextFilter {
	doesFilterPass(params: IDoesFilterPassParams): boolean {
		if (params.node.data.isDirty()) {
			return true;
		} else {
			return super.doesFilterPass(params);
		}
	}
}
