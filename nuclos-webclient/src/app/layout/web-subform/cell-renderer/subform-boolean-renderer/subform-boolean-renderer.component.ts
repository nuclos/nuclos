import { Component, ElementRef, HostListener } from '@angular/core';
import { EntityObjectEventService } from '@modules/entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { NuclosCellRendererParams } from '../../web-subform.model';
import { AbstractRendererComponent } from '../abstract-renderer-component';

@Component({
	selector: 'nuc-subform-boolean-renderer',
	templateUrl: '../../cell-editors/subform-boolean-editor/subform-boolean-editor.component.html',
	styleUrls: ['subform-boolean-renderer.component.css']
})
export class SubformBooleanRendererComponent extends AbstractRendererComponent {

	private isEditable = false;
	private numberStates;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		protected elementRef: ElementRef
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any) {
		super.agInit(params);
		let nuclosCellRenderParams = params.colDef.cellRendererParams.nuclosCellRenderParams as NuclosCellRendererParams;
		this.isEditable = nuclosCellRenderParams.editable;
		this.numberStates = nuclosCellRenderParams.numberStates;
	}

	@HostListener('document:keydown.Space', ['$event'])
	toggleValueOnEnter(event: KeyboardEvent) {
		if (event?.target &&
			(
				$(event.target).attr('col-id') === this.getColDef()?.colId &&
				(
					event.target === this.elementRef.nativeElement ||
					$(event.target).find('nuc-subform-boolean-renderer').get(0) === this.elementRef.nativeElement
				)
			)
		) {
			this.toggleValue();
			event.preventDefault();
			event.stopPropagation();
		}
	}

	toggleValue() {
		if (!this.isEditable) {
			return;
		}
		if (this.numberStates === '3') {
			this.setValue((this.getValueSafe() + 1) % 3);
		} else {
			this.setValue(!this.getValue());
		}

		if (this.numberStates === '1') { // Boolean column in a multi-column y-axis
			return;
		}

		let eo = this.getEntityObject();
		if (eo) {
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				this.getSubEntityObject().setAttribute(colDef.field, this.getValue());
			}
		}
	}

	getValueSafe() {
		return this.getValue() ? this.getValue() : 0;
	}

	getValueMapped(): any {
		if (this.numberStates === '3') {
			return this.getValueSafe() + 2;
		}
		return super.getValue();
	}
}
