import { Component, ElementRef, HostListener, Inject, Injector, NgZone, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { EntityObjectEventService } from '@modules/entity-object-data/shared/entity-object-event.service';
import { FqnService } from '@shared/service/fqn.service';
import { AgRendererComponent } from 'ag-grid-angular';
import { CellKeyPressEvent, ColDef, ICellRendererParams } from 'ag-grid-community';
import { UploadOutput } from 'ngx-uploader';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { BoAttr, DocumentAttribute, EntityAttrMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { EntityObject, SubEntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { ImageService } from '../../../shared/image.service';
import { WebFileComponent } from '../../../web-file/web-file.component';
import { NuclosCellRendererParams } from '../../web-subform.model';

@Component({
	selector: 'nuc-subform-document-renderer',
	templateUrl: '../../../web-file/web-file.component.html',
	styleUrls: [
		'../../../web-file/web-file.component.scss',
		'./subform-document-renderer.component.css'
	]
})
export class SubformDocumentRendererComponent extends WebFileComponent
	implements AgRendererComponent, OnDestroy {
	isEditable = false;
	private params: ICellRendererParams;
	private value: any;

	private colDef: ColDef;
	private attrMeta: EntityAttrMeta;

	private imageHref: string | undefined;
	private cellPressHandler = this.cellKeyPressEvent.bind(this);

	constructor(
		injector: Injector,
		nuclosConfigService: NuclosConfigService,
		dialogService: NuclosDialogService,
		nuclosI18nService: NuclosI18nService,
		imageService: ImageService,
		i18nService: NuclosI18nService,
		private fqnService: FqnService,
		@Inject(NgZone) zone: NgZone,
		public router: Router,
		protected elementRef: ElementRef,
		protected eoEventService: EntityObjectEventService,
	) {
		super(
			injector,
			nuclosConfigService,
			dialogService,
			nuclosI18nService,
			imageService,
			i18nService,
			zone,
			router,
			elementRef,
			eoEventService
		);
	}

	agInit(params: ICellRendererParams) {
		this.refresh(params);

		this.params.api.addEventListener(
			'cellKeyPress',
			this.cellPressHandler
		);
	}

	refresh(params: any): boolean {
		this.colDef = params.colDef;
		this.params = params;
		this.value = params.value;
		if (this.colDef.cellEditorParams) {
			this.attrMeta = this.colDef.cellEditorParams['attrMeta'];
		}
		setTimeout(() => {
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				let documentAttribute = this.getDocumentAttribute();
				if (documentAttribute === undefined) {
					this.isImage = true;
					this.documentIsSelected = false;
					let subEoData = this.getSubEntityObject().getData();
					if (
						subEoData &&
						subEoData.attrImages &&
						subEoData.attrImages.links[colDef.field]
					) {
						this.imageHref = subEoData.attrImages.links[colDef.field].href;
						this.documentIsSelected = true;
					}
				} else {
					this.isImage = false;

					// if file was just uploaded use filename from attribute otherwise from params
					this.fileName =
						(documentAttribute && documentAttribute.name) ||
						(params.value && params.value.name);
					this.fileIconUrl = this.getFileIconUrl();
					this.documentIsSelected =
						documentAttribute &&
						(!!documentAttribute['uploadToken'] || !!documentAttribute.id);
				}
			}
		});

		let nuclosCellRenderParams = params['nuclosCellRenderParams'] as NuclosCellRendererParams;
		this.isEditable = nuclosCellRenderParams.editable;
		return true;
	}

	ngOnDestroy() {
		this.params.api.removeEventListener('cellKeyPress', this.cellPressHandler);
	}

	@HostListener('document:keydown.Delete', ['$event'])
	keydown(event: KeyboardEvent) {
		if (event?.target &&
			(
				$(event.target).attr('col-id') === this.getColDef()?.colId &&
				(
					event.target === this.elementRef.nativeElement ||
					$(event.target).find('nuc-subform-document-renderer').get(0) === this.elementRef.nativeElement
				)
			)
		) {
			if (event.key === 'Enter') {
				if (this.isEditable && (this.fileName === undefined || this.fileName === null)) {
					this.startUpload(undefined);
				} else if (this.fileName) {
					window.open(this.getDownloadUrl());
				}
			} else if (event.key === 'Delete' && this.isEditable) {
				this.removeDocument();
			}
		}
	}

	getColDef(): ColDef | undefined {
		return this.params.colDef;
	}

	getName() {
		if (this.attrMeta === undefined || this.attrMeta.getAttributeID() === undefined) {
			return this.webComponent.name;
		}

		return this.fqnService.getShortAttributeName(
			this.getEntityObject().getEntityClassId(),
			this.attrMeta.getAttributeID()
		);
	}

	getStyle(): any {
		let style = super.getStyle();

		// we need to remove these added values as this
		// is not applicable for subform renderers
		delete style['min-height'];
		delete style['min-width'];

		return style;
	}

	setValue(value) {
		this.params.value = value;
		let eo = this.getEntityObject();
		if (eo) {
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				this.getSubEntityObject().setAttribute(colDef.field, value);
			}
		}
	}

	setDocumentAttribute(value): void {
		this.setValue(value);
		return this.getSubEntityObject().setAttribute(this.webComponent.name, value);
	}

	getEntityObject(): EntityObject {
		return this.getSubEntityObject();
	}

	handleUpload(uploadedOutput: UploadOutput) {
		if (this.isEditable) {
			super.handleUpload(uploadedOutput);
		}
	}

	getNextFocusField(): string | undefined {
		return undefined;
	}

	getNextFocusComponent(): string | undefined {
		return undefined;
	}

	getInitialFocus(): boolean | undefined {
		return undefined;
	}

	isWritable() {
		return this.isEditable;
	}

	/**
	 * @return undefined if attribute is image
	 */
	getDocumentAttribute(): DocumentAttribute | undefined {
		let colDef = this.getColDef();
		if (colDef && colDef.field) {
			return this.getSubEntityObject().getAttribute(colDef.field);
		} else {
			throw Error('Unable to get ag-grid column definition.');
		}
	}

	openImageModal() {
		let subEo = this.getSubEntityObject();

		this.getImageService().openGalleryForSubform(subEo, this.getImageAttributeId());
	}

	getImageService() {
		return this.injector.get(ImageService);
	}

	getImageUrl(): string | undefined {
		return this.imageHref;
	}

	getLinkHref(): string | undefined {
		let docAttrId =
			this.getSubEntityObject().getEntityClassId() + '_' + this.params.colDef?.field;
		if (this.isImage) {
			if (this.imageHref) {
				return this.imageHref;
			}
		} else {
			// TODO: HATEOAS
			let link =
				this.nuclosConfigService.getRestHost() +
				'/boDocuments/' +
				this.eo.getEntityClassId() +
				'/' +
				(this.eo.getId() ?? this.eo.getTemporaryId()) +
				'/subBos/' +
				this.getSubEntityObject().getReferenceAttributeId() +
				'/' +
				(this.getSubEntityObject().getId() ?? this.getSubEntityObject().getTemporaryId()) +
				'/documents/' +
				docAttrId;
			return link;
		}

		return undefined;
	}

	protected resetDocument(): void {
		if (!this.params.colDef?.field) {
			return;
		}
		this.getSubEntityObject().setAttribute(this.params.colDef.field, null);
		delete this.imageHref;

		this.resetModel(this.getSubEntityObject().getData()?.attrImages);
	}

	protected getImageAttributeId(): string {
		let colDef = this.getColDef();
		let attributeId = colDef && colDef.field;

		if (!attributeId) {
			throw Error('Field is not defined');
		}

		return attributeId;
	}

	private getSubEntityObject(): SubEntityObject {
		return this.params.data;
	}

	private cellKeyPressEvent(event: CellKeyPressEvent): void {
		this.keydown(event.event as KeyboardEvent);
	}
}
