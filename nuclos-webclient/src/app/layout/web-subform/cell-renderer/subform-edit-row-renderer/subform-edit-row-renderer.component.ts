import { Component, ElementRef } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { EntityObjectEventService } from '@modules/entity-object-data/shared/entity-object-event.service';
import { SubEntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { Logger } from '@modules/log/shared/logger';
import { RuleService } from '@modules/rule/shared/rule.service';
import {
	EXPAND_SIDEVIEW_PARAM,
	SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM,
	SHOW_CLOSE_ON_SAVE_BUTTON_PARAM
} from '@shared/service/browser-refresh.service';
import { HyperlinkService } from '@shared/service/hyperlink.service';
import { finalize, take } from 'rxjs/operators';
import { RowEditCellParams } from '../../web-subform.model';
import { AbstractRendererComponent } from '../abstract-renderer-component';

@Component({
	selector: 'nuc-subform-edit-row-renderer',
	templateUrl: './subform-edit-row-renderer.component.html',
	styleUrls: ['./subform-edit-row-renderer.component.css']
})
export class SubformEditRowRendererComponent extends AbstractRendererComponent {
	showEditRowInNewTab = false;
	showEditRowInPopup = false;

	subformComponentEnabled: boolean | undefined;

	private cellRenderParams: RowEditCellParams;
	private popupParameter: string | undefined;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private nuclosDialogService: NuclosDialogService,
		private ruleService: RuleService,
		private hyperlinkService: HyperlinkService,
		private elementRef: ElementRef
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any) {
		super.agInit(params);
		this.initColumnSettins(params);
	}

	openInModal() {
		if (!this.isSubformEnabled()) {
			return;
		}
		let subEo = this.getSubEntityObject();
		if (!subEo.isComplete()) {
			this.loadCompleteSubEO(subEo);
		}
		this.nuclosDialogService.openEoInModal(
			subEo,
			undefined,
			false,
			$(this.elementRef.nativeElement)
				.parentsUntil('div[role="gridcell"]')
				.parent()
				.first()
				.get(0)
		).pipe(take(1),
			// After the modal is closed, layout rules may change again
			finalize(() => {
				this.ruleService.updateRuleExecutor(subEo)
					.pipe(
						take(1)
					);
			})).subscribe();
	}

	openInNewTab() {
		this.openInNewTabOrPopup(undefined);
	}

	openInPopup() {
		this.openInNewTabOrPopup(this.popupParameter);
	}

	canOpenWindow() {
		if (this.canOpenModal() && !this.popupParameter) {
			return false;
		}
		let subEO = this.getSubEntityObject();
		return !subEO.isDirty();
	}

	canOpenModal() {
		if (!this.cellRenderParams.canOpenModal) {
			return false;
		}
		return this.isCompleteOrUntouched();
	}

	isSubformEnabled() {
		return this.cellRenderParams.subformComponentEnabled;
	}

	isCompleteOrUntouched() {
		let subEO = this.getSubEntityObject();
		return subEO.isComplete() || !subEO.isDirty();
	}

	private initColumnSettins(params: any) {
		this.cellRenderParams = params;
		this.subformComponentEnabled = this.cellRenderParams.subformComponentEnabled;
		this.getSubEntityObject()
			.getMeta()
			.pipe(take(1))
			.subscribe(meta => {
				this.popupParameter = this.getPopupParameter(meta);

				let showEditRowInNewTabOrPopup: boolean = !!this.getSubEntityObject().getDetailLink();

				this.showEditRowInNewTab =
					showEditRowInNewTabOrPopup && this.popupParameter === undefined;
				this.showEditRowInPopup =
					showEditRowInNewTabOrPopup && this.popupParameter !== undefined;
			});
	}

	private loadCompleteSubEO(subEo: SubEntityObject) {
		let id = subEo.getId();
		if (id) {
			subEo.reload().pipe(take(1)).subscribe(() => subEo.setComplete(true));
		}
	}

	private getPopupParameter(meta): string | undefined {
		// popupaprameter from advanced properties
		if (this.cellRenderParams instanceof RowEditCellParams) {
			// TODO this is never true ??
			return this.cellRenderParams.getAdvancedProperty('popupparameter');
		} else {
			let advancedProperties = new RowEditCellParams(
				this.cellRenderParams['advancedProperties']
			);
			let popupParameter = advancedProperties.getAdvancedProperty('popupparameter');
			if (popupParameter) {
				return popupParameter;
			}
		}

		// popupparameter from look and feel parameters
		return meta.getPopupParameter();
	}

	private openInNewTabOrPopup(popupParameter) {
		// TODO use Angular router here?
		let link = this.getSubEntityObject().getDetailLink();
		if (!link) {
			Logger.instance.error(
				'Can not open Sub-EO without detail link: %o',
				this.getSubEntityObject()
			);
			return;
		}

		this.eoEventService.emitExternalOpenEo(this.getSubEntityObject());

		let externalChangesSubscription = this.eoEventService.listenForExternalChanges(
			this.getSubEntityObject()
		).subscribe();

		this.getSubEntityObject()
			.getMeta()
			.pipe(take(1))
			.subscribe(meta => {
				// NUCLOS-4435: Details of dynamik BO
				let entityClassId = meta.getDetailEntityClassId() || meta.getBoMetaId();
				if (!entityClassId) {
					entityClassId = this.getSubEntityObject().getEntityClassId();
				}

				let href =
					this.hyperlinkService.getBaseUrl() +
					(popupParameter || meta.displayAsListOnly() ? '/popup/' : '/view/') +
					entityClassId +
					'/' +
					this.getSubEntityObject().getId() +
					'?' +
					EXPAND_SIDEVIEW_PARAM +
					'&' +
					SHOW_CLOSE_ON_SAVE_BUTTON_PARAM +
					'&' +
					`showHeader=${meta.displayAsListOnly() ? 'true' : 'false'}` +
					'&' +
					SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM + '=' + new Date().getTime();

				let popupWindow = window.open(href, '_blank', popupParameter);
				// need to wait for GUI tick to be popupWindow opened
				setTimeout(() => {
					if (popupWindow !== null) {
						popupWindow.onbeforeunload = () => {
							externalChangesSubscription.unsubscribe();
						};
					}
				});
			});
	}
}
