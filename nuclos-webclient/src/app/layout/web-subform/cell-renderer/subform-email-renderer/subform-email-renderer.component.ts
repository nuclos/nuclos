import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { AgRendererComponent } from 'ag-grid-angular';
import { ColDef, ICellRendererParams } from 'ag-grid-community';
import { EntityAttrMeta } from '../../../../modules/entity-object-data/shared/bo-view.model';
import { EntityObject, SubEntityObject } from '../../../../modules/entity-object-data/shared/entity-object.class';
import { NuclosValidationService } from '../../../../shared/service/nuclos-validation.service';
import { WebEmailComponent } from '../../../web-email/web-email.component';
import { NuclosCellRendererParams } from '../../web-subform.model';

@Component({
	selector: 'nuc-subform-email-renderer',
	templateUrl: '../../../web-email/web-email.component.html',
	styleUrls: [
		'../../../web-email/web-email.component.css',
		'./subform-email-renderer.component.css'
	]
})
export class SubformEmailRendererComponent extends WebEmailComponent
	implements AgRendererComponent {
	isEditable = false;
	private params: ICellRendererParams;
	private value: any;

	private colDef: ColDef;
	private attrMeta: EntityAttrMeta;

	constructor(
		injector: Injector,
		validationService: NuclosValidationService,
		public router: Router
	) {
		super(
			injector,
			validationService,
			router
		);
	}

	agInit(params: ICellRendererParams) {
		this.refresh(params);
		let nuclosCellRenderParams = params['nuclosCellRenderParams'] as NuclosCellRendererParams;
		this.isEditable = nuclosCellRenderParams.editable;
	}

	refresh(params: any): boolean {
		this.colDef = params.colDef;
		this.params = params;
		this.value = params.value;
		if (this.colDef.cellEditorParams) {
			this.attrMeta = this.colDef.cellEditorParams['attrMeta'];
		}
		return false;
	}

	getColDef(): ColDef | undefined {
		return this.params.colDef;
	}

	setEmailAttribute(value) {
		// no editing here
	}

	getEmailAttribute(): any {
		let colDef = this.getColDef();
		if (colDef && colDef.field) {
			return this.getSubEntityObject().getAttribute(colDef.field);
		}
		return null;
	}

	getEntityObject(): EntityObject {
		return this.getSubEntityObject();
	}

	isWritable() {
		return this.isEditable && super.isWritable();
	}

	startEditing() {
		// do nothing here... this will start cell editor
	}

	private getSubEntityObject(): SubEntityObject {
		return this.params.data;
	}
}
