import { Component, ElementRef, HostListener, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { AgRendererComponent } from 'ag-grid-angular';
import { ColDef, ICellRendererParams } from 'ag-grid-community';
import { EntityAttrMeta } from '../../../../modules/entity-object-data/shared/bo-view.model';
import { EntityObject, SubEntityObject } from '../../../../modules/entity-object-data/shared/entity-object.class';
import { HyperlinkService } from '../../../../shared/service/hyperlink.service';
import { WebHyperlinkComponent } from '../../../web-hyperlink/web-hyperlink.component';
import { NuclosCellRendererParams } from '../../web-subform.model';

@Component({
	selector: 'nuc-subform-hyperlink-renderer',
	templateUrl: '../../../web-hyperlink/web-hyperlink.component.html',
	styleUrls: [
		'../../../web-hyperlink/web-hyperlink.component.css',
		'./subform-hyperlink-renderer.component.css'
	]
})
export class SubformHyperlinkRendererComponent extends WebHyperlinkComponent
	implements AgRendererComponent {
	isEditable = false;
	private params: ICellRendererParams;
	private value: any;

	private colDef: ColDef;
	private attrMeta: EntityAttrMeta;

	constructor(
		injector: Injector,
		hyperlinkService: HyperlinkService,
		router: Router,
		protected elementRef: ElementRef
	) {
		super(
			injector,
			hyperlinkService,
			router
		);
	}

	agInit(params: ICellRendererParams) {
		this.refresh(params);
		let nuclosCellRenderParams = params['nuclosCellRenderParams'] as NuclosCellRendererParams;
		this.isEditable = nuclosCellRenderParams.editable;
		this.showCompleteLink = false;
	}

	refresh(params: any): boolean {
		this.colDef = params.colDef;
		this.params = params;
		this.value = params.value;
		if (this.colDef.cellEditorParams) {
			this.attrMeta = this.colDef.cellEditorParams['attrMeta'];
		}
		return false;
	}

	@HostListener('document:keydown.Enter', ['$event'])
	openLinkIfAvailable(event: KeyboardEvent) {
		// try to get safeUrl Value if it is present
		// this will not break safe guarding of javascript execution
		let url = this.getUrl();
		if (url !== null && typeof url !== 'string' && 'changingThisBreaksApplicationSecurity' in url) {
			url = url['changingThisBreaksApplicationSecurity'];
		}

		if (event?.target &&
			(
				$(event.target).attr('col-id') === this.getColDef()?.colId &&
				(
					event.target === this.elementRef.nativeElement ||
					$(event.target).find('nuc-subform-hyperlink-renderer').get(0) === this.elementRef.nativeElement
				)
			) && !this.isWritable() && url !== null
		) {
			// this will be always string due to above check
			window.open(url as string, '_blank');
			event.preventDefault();
			event.stopPropagation();
		}
	}

	getColDef(): ColDef | undefined {
		return this.params.colDef;
	}

	setHyperlinkAttribute(value) {
		// no editing here
	}

	getHyperlinkAttribute(): any {
		let colDef = this.getColDef();
		if (colDef && colDef.field) {
			return this.getSubEntityObject().getAttribute(colDef.field);
		}
		return null;
	}

	getEntityObject(): EntityObject {
		return this.getSubEntityObject();
	}

	isWritable() {
		return this.isEditable && super.isWritable();
	}

	startEditing() {
		// do nothing here... this will start cell editor
	}

	private getSubEntityObject(): SubEntityObject {
		return this.params.data;
	}
}
