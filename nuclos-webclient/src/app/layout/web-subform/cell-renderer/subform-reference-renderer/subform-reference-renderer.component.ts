import { Component, ElementRef } from '@angular/core';
import { ReferenceTargetService } from '@modules/entity-object-data/shared/reference-target.service';
import { AgRendererComponent } from 'ag-grid-angular';
import { EntityAttrMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { SubEntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { FqnService } from '@shared/service/fqn.service';
import { NuclosCellRendererParams } from '../../web-subform.model';

/**
 * TODO: Consolidate with ReferenceRendererComponent.
 */
@Component({
	selector: 'nuc-subform-reference',
	templateUrl: 'subform-reference-renderer.component.html',
	styleUrls: ['subform-reference-renderer.component.scss']
})
export class SubformReferenceRendererComponent implements AgRendererComponent {
	popupParameter: string | undefined;
	attributeMeta: EntityAttrMeta | undefined;
	subEO: SubEntityObject;
	isEditable = false;
	value: string;
	attribute: any;

	private params: any;
	private nuclosCellRenderParams: NuclosCellRendererParams;

	constructor(
		private fqnService: FqnService,
		private referenceTargetService: ReferenceTargetService
	) {
	}

	agInit(params: any) {
		this.params = params;
		this.value = params.value && params.value.id ? params.value.name : undefined;

		this.nuclosCellRenderParams = params.nuclosCellRenderParams as NuclosCellRendererParams;
		if (params.nuclosCellRenderParams !== undefined && this.nuclosCellRenderParams.attrMeta !== undefined) {
			this.attributeMeta = this.nuclosCellRenderParams.attrMeta;
			this.isEditable = this.nuclosCellRenderParams.editable;
			this.popupParameter = this.getPopupParameter();
			if (params.api !== null) {
				let row = params.api.getModel().getRow(params.rowIndex);
				if (row) {
					this.subEO = row.data;
				}
			}

			if (this.attributeMeta && this.subEO) {
				let fieldName = this.fqnService.getShortAttributeName(
					this.subEO.getEntityClassId(),
					this.attributeMeta.getAttributeID()
				);
				if (fieldName !== undefined) {
					this.attribute = this.subEO.getAttribute(fieldName);
				}
			}
		}
	}

	// this is called by web-subform.component.ts:794
	editReferenceTarget($event: KeyboardEvent) {
		this.referenceTargetService.targetReference(
			this.subEO,
			this.attributeMeta,
			this.attribute.id,
			true,
			undefined,
			undefined,
			true,
			this.popupParameter).subscribe(() => this.refreshCell());
	}

	refresh(params: any): boolean {
		return false;
	}

	refreshCell() {
		this.params?.api?.refreshCells({
			columns: [this.params?.column ?? ''],
			force: true
		});
	}

	private getPopupParameter(): string | undefined {
		if (this.nuclosCellRenderParams.advancedProperties) {
			const key = 'popupparameter';
			let popupparameterProperty = this.nuclosCellRenderParams.advancedProperties
				.filter(p => p.name === key)
				.shift();
			if (popupparameterProperty) {
				return popupparameterProperty.value;
			}
		}
		return undefined;
	}
}
