import { Component, Input, OnInit } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { AuthenticationService } from '@modules/authentication';
import { EntityMeta } from '@modules/entity-object-data/shared/bo-view.model';
import {
	DependentFilterKey,
	DependentsSearchfilterService
} from '@modules/entity-object-data/shared/dependents-searchfilter.service';
import {
	Preference,
	SubformSearchtemplatePreferenceContent
} from '@modules/preferences/preferences.model';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { UserAction } from '@nuclos/nuclos-addon-api';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { take, takeUntil } from 'rxjs/operators';
import { SubscriptionHandleDirective } from '../../../data/classes/subscription-handle.directive';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import * as _ from 'lodash';

@Component({
	selector: 'nuc-filter-editor-overlay',
	templateUrl: './filter-editor-overlay.component.html',
	styleUrls: ['./filter-editor-overlay.component.scss']
})
export class FilterEditorOverlayComponent extends SubscriptionHandleDirective implements OnInit {

	@Input() mainEo: EntityObject;
	@Input() dependentFilterKey: DependentFilterKey;
	@Input() dependentMeta: EntityMeta;


	mainEntityMeta: EntityMeta;
	mainEntityClassId: string;
	customSearchfilterName;
	isTemp;
	showAsFavorite;
	favoriteIcon;
	position: number | undefined;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;
	private selectedSearchfilter: Preference<SubformSearchtemplatePreferenceContent> | undefined;
	private allSearchfilters: Preference<SubformSearchtemplatePreferenceContent>[];

	constructor(
		private dependentSearchfilterService: DependentsSearchfilterService,
		private i18n: NuclosI18nService,
		private authService: AuthenticationService,
		private dialogService: NuclosDialogService
		) {
		super();
	}

	ngOnInit(): void {
		this.mainEo.getMeta().subscribe(meta => {
			this.mainEntityMeta = meta;
			this.mainEntityClassId = meta.getEntityClassId();
		});
		this.dependentSearchfilterService.observeSelectedSubformSearchfilter(this.dependentFilterKey)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(searchfilter => {
				this.selectedSearchfilter = searchfilter;
				this.editMeta();
			});
		this.dependentSearchfilterService.observeAllDependentSearchfilters(this.dependentFilterKey)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(searchfilters => {
				if (searchfilters) {
					this.allSearchfilters = searchfilters;
				}
			});

	}

	editMeta() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.customSearchfilterName = searchfilter.name;
			this.showAsFavorite = searchfilter.content.showAsFavorite
				? searchfilter.content.showAsFavorite
				: false;
			this.favoriteIcon = searchfilter.content.favoriteIcon;
			this.position = searchfilter.content.position;
			this.isTemp = searchfilter.content.isTemp;
		}
	}

	editMetaDone() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			searchfilter.content.userdefinedName = true;
			searchfilter.name = this.customSearchfilterName;
			searchfilter.content.showAsFavorite = this.showAsFavorite;
			searchfilter.content.favoriteIcon = this.favoriteIcon;
			searchfilter.content.position = this.validatePosition(this.position);
		}
	}

	saveChanges() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter && searchfilter.dirty === true) {
			// write name changes to pref
			this.editMetaDone();
			// changes to temp are stored immediately
			if (!searchfilter.content.isTemp) {
				// this.dependentSearchfilterService.saveAndSelectSearchfilter(searchfilter).pipe(take(1)).subscribe();
				this.dependentSearchfilterService.saveSearchfilter(searchfilter).pipe(take(1)).subscribe();
			}
		}
	}

	getSelectedSearchfilter(): Preference<SubformSearchtemplatePreferenceContent> | undefined {
		return this.selectedSearchfilter;
	}

	getAllSearchfilters(): Preference<SubformSearchtemplatePreferenceContent>[] {
		return this.allSearchfilters;
	}

	undoChanges() {
		let searchfilter = this.getSelectedSearchfilter();
		if (
			searchfilter &&
			searchfilter.dirty === true &&
			!searchfilter.content.isTemp &&
			searchfilter.prefId
		) {
			this.dependentSearchfilterService.resetFilterChanges(searchfilter);
		}
	}

	deleteSearchfilter() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.dialogService
				.confirm({
					title: this.i18n.getI18n('webclient.searcheditor.searchfilter.delete'),
					message: this.i18n.getI18n('webclient.searcheditor.searchfilter.delete.confirm')
				})
				.pipe(
					take(1)
				).subscribe(
				(result) => {
					if (result && searchfilter) {
						this.dependentSearchfilterService.deleteSearchfilter(searchfilter);
					}
			});
		}
	}

	isShareAllowed(): boolean {
		return this.authService.isActionAllowed(UserAction.SharePreferences);
	}

	isTempSearchfilter(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			return searchfilter.content.isTemp === true;
		}
		return false;
	}

	isShared(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			return searchfilter.shared === true;
		}
		return false;
	}

	isDirty(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			return searchfilter.dirty === true;
		}
		return false;
	}

	clearSearchfilter() {
		this.dependentSearchfilterService.selectTempFilter(this.dependentFilterKey);
	}

	makeTempPersistent(): void {
		let tempSearchfilter = this.getSelectedSearchfilter();
		if (this.dependentMeta && tempSearchfilter && tempSearchfilter.content.isTemp) {
			let newSearchfilter = this.createSearchfilter(this.dependentMeta);
			this.customSearchfilterName = newSearchfilter.name;
			newSearchfilter.content = _.clone(tempSearchfilter.content);
			newSearchfilter.content.isTemp = false;
			newSearchfilter.dirty = true;
			this.dependentSearchfilterService.saveSearchfilter(newSearchfilter).subscribe();
		}
	}

	private createSearchfilter(dependentMeta: EntityMeta): Preference<SubformSearchtemplatePreferenceContent> {
		let searchfilter = this.dependentSearchfilterService.createSearchfilter(this.dependentFilterKey, dependentMeta);
		const searchfilterNew = this.i18n.getI18n('webclient.searchfilter.new');
		let searchfilterNumber = this.getNextFilterNumber(searchfilterNew);
		searchfilter.name = searchfilterNew + ' ' + searchfilterNumber;
		searchfilter.content.userdefinedName = true;
		searchfilter.boMetaId = dependentMeta.getEntityClassId();
		searchfilter.boName = dependentMeta.getEntityName();
		searchfilter.layoutId = this.mainEo.getLayoutId();
		return searchfilter;
	}

	private getNextFilterNumber(searchfilterNew) {
		const regexp = new RegExp('^' + searchfilterNew + ' [0-9]* *$');
		let searchfilterNumber = 0;
		for (const sf of this.getAllSearchfilters()) {
			if (sf.name && regexp.test(sf.name)) {
				let usedNumber = +sf.name.substr(sf.name.indexOf(' ') + 1).trim();
				if (usedNumber > searchfilterNumber) {
					searchfilterNumber = usedNumber;
				}
			}
		}
		return ++searchfilterNumber;
	}

	private validatePosition(position: number | undefined): number | undefined {
		if (position) {
			if (position < 1) {
				return 1;
			}
			if (position > 9999) {
				return 9999;
			}
			return position;
		}
		return undefined;
	}

}
