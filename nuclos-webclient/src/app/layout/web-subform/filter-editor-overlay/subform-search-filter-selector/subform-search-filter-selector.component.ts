import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Preference, SubformSearchtemplatePreferenceContent } from 'app/modules/preferences/preferences.model';
import { takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../../core/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../../../data/classes/subscription-handle.directive';
import {
	DependentFilterKey,
	DependentsSearchfilterService
} from '../../../../modules/entity-object-data/shared/dependents-searchfilter.service';
import { ObjectUtils } from '../../../../shared/object-utils';
import { NuclosHotkeysService } from '../../../../shared/service/nuclos-hotkeys.service';

@Component({
	selector: 'nuc-subform-search-filter-selector',
	templateUrl: './subform-search-filter-selector.component.html',
	styleUrls: ['./subform-search-filter-selector.component.css']
})
export class SubformSearchFilterSelectorComponent extends SubscriptionHandleDirective implements OnInit {

	@Input() dependentFilterKey: DependentFilterKey;

	@Input() searchfilter: Preference<SubformSearchtemplatePreferenceContent> | undefined;

	@Input() disabled = false;

	@ViewChild('subformSearchfilterSelectorDropdownMenu', {static: true}) searchfilterSelectorDropdownMenu: ElementRef;

	popoverShowing = false;

	allSearchfilters: Preference<SubformSearchtemplatePreferenceContent>[] = [];

	constructor(
		private dependentsSearchfilterService: DependentsSearchfilterService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService
	) {
		super();
	}

	ngOnInit() {
		this.dependentsSearchfilterService.observeAllDependentSearchfilters(this.dependentFilterKey)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				allDependentFilters => {
					if (allDependentFilters) {
						this.allSearchfilters = allDependentFilters;
					} else {
						this.allSearchfilters = [];
					}
				}
			);
		document.querySelector('body')!.appendChild(this.searchfilterSelectorDropdownMenu.nativeElement);

		this.nuclosHotkeysService.addShortcut({
			keys: 't',
			modifier: 'ALT_SHIFT',
			description: this.i18n.getI18n('webclient.shortcut.search.toggleFilter')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.selectNextSearchFilter();
		});
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
		document.querySelector('body')!.removeChild(this.searchfilterSelectorDropdownMenu.nativeElement);
	}

	getCurrentSearchfilterName() {
		if (this.searchfilter) {
			return this.searchfilter.name;
		}
		return '';
	}

	togglePopover() {
		this.setPopoverShowing(!this.popoverShowing);
	}

	setPopoverShowing(popoverShowing: boolean) {
		if (this.disabled) {
			return;
		}
		this.popoverShowing = popoverShowing;
		if (popoverShowing) {
			this.adjustPopover();
		}
		let menu = $('#searchfilter-selector-dropdown-menu');
		menu.css('display', popoverShowing ? 'block' : 'none');
	}

	adjustPopover() {
		let dropdown = $('#searchfilter-selector-dropdown');
		if (dropdown.offset() !== undefined) {
			let menu = $('#searchfilter-selector-dropdown-menu');
			menu.css('top', (dropdown.offset().top + dropdown.outerHeight() - 6) + 'px');
			menu.css('left', (dropdown.offset().left) + 'px');
		}
	}

	selectSearchfilter(searchfilter) {
		this.dependentsSearchfilterService.selectSubformSearchfilter(this.dependentFilterKey, searchfilter);
	}

	private selectNextSearchFilter() {
		if (this.allSearchfilters.length > 0) {
			const currentIndexOfFilter = this.allSearchfilters.findIndex(sf => ObjectUtils.isEqual(sf, this.searchfilter));
			if ((currentIndexOfFilter + 1) > (this.allSearchfilters.length - 1)) {
				// last index
				this.selectSearchfilter(this.allSearchfilters[0]);
			} else {
				this.selectSearchfilter(this.allSearchfilters[currentIndexOfFilter + 1]);
			}
		}
	}
}
