import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { take } from 'rxjs/operators';
import {
	DependentsSearchfilterService
} from '../../../../modules/entity-object-data/shared/dependents-searchfilter.service';
import { Preference, SubformSearchtemplatePreferenceContent } from '../../../../modules/preferences/preferences.model';
import { FavoriteFilterSelector } from '../favorite-filter-selector/favorite-filter-selector';

@Component({
	selector: 'nuc-favorite-filter-hamburger',
	templateUrl: './favorite-filter-hamburger.html',
	styleUrls: ['./favorite-filter-hamburger.css']
})
export class FavoriteFilterHamburger {

	@Input()
	favoriteFilters: Preference<SubformSearchtemplatePreferenceContent>[] | undefined;

	@Input()
	selectedFilter: Preference<SubformSearchtemplatePreferenceContent> | undefined;

	@Output()
	toggleDropdown: EventEmitter<undefined> = new EventEmitter<undefined>();

	constructor(private dependentSearchfilterService: DependentsSearchfilterService, private modalService: NuclosDialogService) {

	}

	showDialog() {
		this.modalService.custom(
			FavoriteFilterSelector,
			{
				favoriteFilters: this.favoriteFilters,
				selectedFilter: this.selectedFilter
			}, {
				size: 'sm',
			}
		).pipe(
			take(1)
		).subscribe();
	}

}
