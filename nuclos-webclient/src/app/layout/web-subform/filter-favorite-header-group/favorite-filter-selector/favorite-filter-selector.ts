import { Component, Input } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import {
	DependentsSearchfilterService
} from '../../../../modules/entity-object-data/shared/dependents-searchfilter.service';
import { Preference, SubformSearchtemplatePreferenceContent } from '../../../../modules/preferences/preferences.model';

@Component({
	selector: 'nuc-favorite-filter-selector',
	templateUrl: './favorite-filter-selector.html',
	styleUrls: ['./favorite-filter-selector.css']
})
export class FavoriteFilterSelector {

	@Input()
	favoriteFilters: Preference<SubformSearchtemplatePreferenceContent>[] | undefined;

	@Input()
	selectedFilter: Preference<SubformSearchtemplatePreferenceContent> | undefined;

	@Input()
	visible = false;

	constructor(
		private dependentSearchfilterService: DependentsSearchfilterService,
		private modalService: NuclosDialogService
	) {

	}

	selectFilter(filter) {
		this.dependentSearchfilterService.selectSubformSearchfilter(filter.content.filterKey,  filter);
		this.modalService.closeCurrentModal();
	}

}
