import { Component, Input } from '@angular/core';
import {
	DependentsSearchfilterService
} from '@modules/entity-object-data/shared/dependents-searchfilter.service';
import { Preference, SubformSearchtemplatePreferenceContent } from '@modules/preferences/preferences.model';

@Component({
	selector: 'nuc-favorite-filters-row',
	templateUrl: './favorite-filters-row.html',
	styleUrls: ['./favorite-filters-row.css']
})
export class FavoriteFiltersRow {

	@Input()
	favoriteFilters: Preference<SubformSearchtemplatePreferenceContent>[] | undefined;

	constructor(private dependentsSearchfilterService: DependentsSearchfilterService) {

	}

	toggleFilterSelection(searchfilter) {
		if (searchfilter.selected) {
			this.dependentsSearchfilterService.selectTempFilter(searchfilter.content.filterKey);
		} else {
			this.dependentsSearchfilterService.selectSubformSearchfilter(searchfilter.content.filterKey, searchfilter);
		}
	}

}
