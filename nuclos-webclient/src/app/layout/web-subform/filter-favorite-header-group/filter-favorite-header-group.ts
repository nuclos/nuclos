import { ChangeDetectorRef, Component } from '@angular/core';
import {
	DependentFilterKey,
	DependentsSearchfilterService
} from '@modules/entity-object-data/shared/dependents-searchfilter.service';
import { Preference, SubformSearchtemplatePreferenceContent } from '@modules/preferences/preferences.model';
import { AgFrameworkComponent } from 'ag-grid-angular';
import {
	FilterChangedEvent,
	IAfterGuiAttachedParams,
	IFloatingFilter,
	IFloatingFilterParams,
} from 'ag-grid-community';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { WebSubformComponent } from '../web-subform.component';

@Component({
	selector: 'nuc-filter-favorite-header-group',
	templateUrl: './filter-favorite-header-group.html',
	styleUrls: ['./filter-favorite-header-group.scss']
})
export class FilterFavoriteHeaderGroup implements IFloatingFilter,
	AgFrameworkComponent<IFloatingFilterParams> {
	subformFilterKey: DependentFilterKey;

	selectedFilter: Preference<SubformSearchtemplatePreferenceContent> | undefined;
	favoriteFilters: Preference<SubformSearchtemplatePreferenceContent>[] | undefined;
	private params: IFloatingFilterParams;
	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private dependentsSearchfilterService: DependentsSearchfilterService,
		private changeDetectorRef: ChangeDetectorRef
	) {

	}

	afterGuiAttached(params?: IAfterGuiAttachedParams): void {

	}

	onParentModelChanged(parentModel: any, filterChangedEvent?: FilterChangedEvent): void {

	}

	agInit(params: any): void {
		this.params = params;
		this.subformFilterKey = params.subformFilterKey;
		this.dependentsSearchfilterService.observeSelectedSubformSearchfilter(this.subformFilterKey)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(selectedFilter => this.selectedFilter = selectedFilter);
		this.dependentsSearchfilterService.observeAllDependentSearchfilters(this.subformFilterKey)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				allDependentFilters => {
					if (allDependentFilters) {
						this.favoriteFilters = allDependentFilters
							.filter(pref => pref.content.showAsFavorite)
							.sort((f1, f2) => {
								if (!f1.content.position && !f2.content.position) {
									return 0;
								}
								if (!f1.content.position) {
									return -1;
								}
								if (!f2.content.position) {
									return 1;
								}
								return f1.content.position - f2.content.position;
							});
					} else {
						this.favoriteFilters = [];
					}
					this.changeDetectorRef.detectChanges();
				}
			);
	}

	destroy(): void {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

}
