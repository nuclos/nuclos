import { Component } from '@angular/core';
import { INoRowsOverlayAngularComp } from 'ag-grid-angular';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

@Component({
	selector: 'nuc-no-rows-overlay',
	templateUrl: './no-rows-overlay.component.html',
	styleUrls: ['./no-rows-overlay.component.css']
})
export class NoRowsOverlayComponent implements INoRowsOverlayAngularComp {
	params;

	fileDropAllowed: false;
	favoriteFilterDisplayed: false;
	filterRowDisplayed: false;
	padding = '32px';
	private unsubscribe$ = new Subject<boolean>();

	agInit(params): void {
		this.params = params;

		this.params.fileDropAllowedSubject
			.pipe(
				take(1)
			)
			.subscribe(fileDropAllowed => this.fileDropAllowed = fileDropAllowed);

		this.params.filteringSubject
			.pipe(
				takeUntil(this.unsubscribe$)
			)
			.subscribe(showFilterHeader => {
				this.filterRowDisplayed = showFilterHeader;
				this.calculateTopPadding();
			});

		this.params.showFavoriteSubject
			.pipe(
				takeUntil(this.unsubscribe$)
			)
			.subscribe(showFavoritesHeader => {
				this.favoriteFilterDisplayed = showFavoritesHeader;
				this.calculateTopPadding();
			});
	}

	calculateTopPadding() {
		let paddingNew = 32;
		if (this.filterRowDisplayed) {
			paddingNew += 32;
		}
		if (this.favoriteFilterDisplayed) {
			paddingNew += 32;
		}
		this.padding = paddingNew + 'px';
	}

	destroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

}
