import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	HostBinding,
	HostListener,
	Input,
	OnInit,
	ViewChild
} from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { BehaviorSubject } from 'rxjs';
import { EntityMeta } from '../../../../modules/entity-object-data/shared/bo-view.model';
import { EntityObjectPreferenceService } from '../../../../modules/entity-object/shared/entity-object-preference.service';
import { Preference, SideviewmenuPreferenceContent } from '../../../../modules/preferences/preferences.model';
import { WebSubformComponent } from '../../web-subform.component';

@Component({
	selector: 'nuc-copy-and-paste-popup',
	templateUrl: './more-subform-actions-popup.html',
	styleUrls: ['./more-subform-actions-popup.css']
})
export class MoreSubformActionsPopup implements OnInit {

	@Input() subform: WebSubformComponent;
	@Input() meta: EntityMeta;
	@HostBinding('class') cssClass;
	@ViewChild('copyButton', { static: false }) copyButton: ElementRef;
	@ViewChild('pasteButton', { static: false }) pasteButton: ElementRef;
	@ViewChild('exportButton', { static: false }) exportButton: ElementRef;

	openPopupButton: ElementRef;
	selectedSideviewmenuPref$: BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined> = new BehaviorSubject(undefined);
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;
	showCopyAndPastePopup = false;
	popupPositionLeft = 0;
	popupPositionTop = 0;

	constructor(private entityObjectPreferenceService: EntityObjectPreferenceService, private changeDetectorRef: ChangeDetectorRef) {
	}

	ngOnInit() {
		this.selectedSideviewmenuPref$
			= this.entityObjectPreferenceService.getSubformColumnPreferenceSelection(this.meta.getEntityClassId());
		this.cssClass = this.subform.getToolbarOrientation() === 'HORIZONTAL' ? 'horizontal' : 'vertical';
	}

	@HostListener('focusout', ['$event'])
	onFocusOut(event: FocusEvent) {
		if (!this.copyButton || !this.showCopyAndPastePopup) {
			return;
		}
		const nextFocusElement = event.relatedTarget as HTMLElement;
		if (nextFocusElement !== this.openPopupButton.nativeElement
			&& nextFocusElement !== this.copyButton.nativeElement
			&& (!this.exportButton || nextFocusElement !== this.exportButton.nativeElement)
			&& (!this.pasteButton || nextFocusElement !== this.pasteButton.nativeElement)) {
			this.showCopyAndPastePopup = false;
		}
	}

	keydown($event: KeyboardEvent) {
		if ($event.key === 'Escape' && this.showCopyAndPastePopup) {
			$event.stopPropagation();
			$event.preventDefault();
		}
	}

	keyup($event: KeyboardEvent) {
		if ($event.key === 'Escape' && this.showCopyAndPastePopup) {
			this.showCopyAndPastePopup = false;
			$event.stopPropagation();
			$event.preventDefault();
		}
	}

	calculatePopupPosition() {
		if (this.cssClass === 'horizontal') {
			this.popupPositionLeft = this.openPopupButton.nativeElement.offsetLeft - 24;
			this.popupPositionTop = 24;
		} else {
			this.popupPositionTop = this.openPopupButton.nativeElement.offsetTop - 24;
			this.popupPositionLeft = 24;
		}
	}

	showPopup() {
		this.calculatePopupPosition();
		if (!this.showCopyAndPastePopup) {
			this.changeDetectorRef.detectChanges();
			this.attemptFocusTransferIntoPopup();
		}
		this.showCopyAndPastePopup = !this.showCopyAndPastePopup;
	}

	outsideClicked($event: any) {
		if (this.openPopupButton.nativeElement !== $event
			&& !this.openPopupButton.nativeElement.contains($event)) {
			this.showCopyAndPastePopup = false;
		}
	}

	private attemptFocusTransferIntoPopup() {
		setTimeout(() => {
			const copyButtonNativeElementPresent = this.copyButton && this.copyButton.nativeElement;
			const pasteButtonNativeElementPresent = this.pasteButton && this.pasteButton.nativeElement;
			const exportButtonNativeElementPresent = this.exportButton && this.exportButton.nativeElement;
			if (this.subform.isCopyEnabled() && copyButtonNativeElementPresent) {
				this.copyButton.nativeElement.focus();
			} else if (this.subform.isPasteEnabled() && pasteButtonNativeElementPresent) {
				this.pasteButton.nativeElement.focus();
			} else if (this.subform.showListExportButton() && exportButtonNativeElementPresent) {
				this.exportButton.nativeElement.focus();
			}
		}, 10);
	}

}
