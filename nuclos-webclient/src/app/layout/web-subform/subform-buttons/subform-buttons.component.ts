import {
	Component,
	ElementRef,
	HostBinding,
	Input,
	OnInit,
	ViewChild
} from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { BehaviorSubject } from 'rxjs';
import { EntityMeta } from '../../../modules/entity-object-data/shared/bo-view.model';
import { EntityObjectPreferenceService } from '../../../modules/entity-object/shared/entity-object-preference.service';
import { Preference, SideviewmenuPreferenceContent } from '../../../modules/preferences/preferences.model';
import { WebSubformComponent } from '../web-subform.component';
import { MoreSubformActionsPopup } from './more-subform-actions-popup/more-subform-actions-popup';

@Component({
	selector: 'nuc-subform-buttons',
	templateUrl: './subform-buttons.component.html',
	styleUrls: ['./subform-buttons.component.css']
})
export class SubformButtonsComponent implements OnInit {

	@Input() subform: WebSubformComponent;
	@Input() meta: EntityMeta;
	@HostBinding('class') cssClass;
	@ViewChild('openPopupButton') openPopupButtonRef: ElementRef;
	@ViewChild('copyAndPastePopupComponent') copyAndPasteComponent: MoreSubformActionsPopup;

	selectedSideviewmenuPref$: BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined> = new BehaviorSubject(undefined);
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor(private entityObjectPreferenceService: EntityObjectPreferenceService) {
	}

	ngOnInit() {
		this.selectedSideviewmenuPref$ = this.entityObjectPreferenceService.getSubformColumnPreferenceSelection(this.meta.getBoMetaId());
		this.cssClass = this.subform.getToolbarOrientation() === 'HORIZONTAL' ? 'horizontal' : 'vertical';
	}

	ngAfterViewInit() {
		if (this.copyAndPasteComponent) {
			this.copyAndPasteComponent.openPopupButton = this.openPopupButtonRef;
		}
	}

	isAnyFilterActive(): boolean {
		return this.subform.isAnyFilterActive();
	}

	keydown($event: KeyboardEvent) {
		if ($event.key === 'Escape') {
			if (this.copyAndPasteComponent.showCopyAndPastePopup) {
				$event.stopPropagation();
				$event.preventDefault();
			}
		}
	}

	keyup($event: KeyboardEvent) {
		if ($event.key === 'Escape') {
			if (this.copyAndPasteComponent.showCopyAndPastePopup) {
				this.copyAndPasteComponent.showCopyAndPastePopup = false;
				$event.stopPropagation();
				$event.preventDefault();
			}
		}
	}

	showPopup() {
		this.copyAndPasteComponent.showPopup();
	}

}
