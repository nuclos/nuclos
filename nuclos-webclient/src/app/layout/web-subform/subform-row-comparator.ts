import { RowNode } from 'ag-grid-community';

declare var require: any;
let naturalCompare = require('string-natural-compare');

/**
 * Does "natural comparison", but sorts empty Strings to first/last place (depending on sort order).
 * The naturalCompare would put empty Strings somewhere in between.
 */
let nuclosTextCompare = (textA, textB) => {
	if ((textA === undefined || textA === null) && textB) {
		return -1;
	} else if (textA && (textB === undefined || textB === null)) {
		return 1;
	} else if ((textA === undefined || textA === null) && (textB === undefined || textB === null)) {
		return 0;
	} else if (typeof textA === 'string' && typeof textB === 'string') {
		return naturalCompare(textA, textB);
	} else {
		return 0;
	}
};

const compareSelectionFirst = (subformRowComparator: SubformRowComparator, valueComparator: (valueA?, valueB?) => any) => {
	return (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted) => {
		let result;
		if (!!nodeA.data.isNew() === !!nodeB.data.isNew()) {
			return valueComparator(valueA, valueB);
		} else if (nodeA.data.isNew()) {
			result = -1 * (subformRowComparator.newEntriesToBottom ? -1 : 1);
		} else {
			result = 1 * (subformRowComparator.newEntriesToBottom ? -1 : 1);
		}
		if (isInverted) {
			result = -result;
		}
		return result;
	};
};

const nullAndUndefinedFirst = (valueComparator: (valueA?, valueB?) => any) => {
	return (valueA, valueB) => {
		let undefinedA = valueA === null || typeof valueA === 'undefined';
		let undefinedB = valueB === null || typeof valueB === 'undefined';

		if (undefinedA && undefinedB) {
			return 0;
		} else if (undefinedA && !undefinedB) {
			return -1;
		} else if (!undefinedA && undefinedB) {
			return 1;
		} else {
			return valueComparator(valueA, valueB);
		}
	};
};

export class SubformRowComparator {

	newEntriesToBottom = false;

	nameComparator = compareSelectionFirst(this,
		(valueA?: { name: string }, valueB?: { name: string }) => {
			let nameA = valueA && valueA.name;
			let nameB = valueB && valueB.name;
			return nuclosTextCompare(nameA, nameB);
		}
	);

	textComparator = compareSelectionFirst(this, nuclosTextCompare);

	booleanComparator = compareSelectionFirst(this,
		nullAndUndefinedFirst(
			(valueA, valueB) => valueA - valueB
		)
	);

	numberComparator = compareSelectionFirst(this,
		nullAndUndefinedFirst(
			(valueA, valueB) => valueA - valueB
		)
	);

	constructor(newEntriesToBottom) {
		this.newEntriesToBottom = newEntriesToBottom;
	}

}
