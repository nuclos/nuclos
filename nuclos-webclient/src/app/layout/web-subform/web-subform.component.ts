import {
	Component,
	DoCheck,
	ElementRef,
	EventEmitter,
	HostBinding,
	HostListener,
	Injector,
	OnDestroy,
	OnInit, QueryList, ViewChild, ViewChildren
} from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { AuthenticationService } from '@modules/authentication';
import { EntityAttrMeta, EntityMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { DependentsSearchfilterService } from '@modules/entity-object-data/shared/dependents-searchfilter.service';
import { EntityObjectDependency } from '@modules/entity-object-data/shared/entity-object-dependency';
import { EntityObjectDependents } from '@modules/entity-object-data/shared/entity-object-dependents';
import { EntityObjectEventService } from '@modules/entity-object-data/shared/entity-object-event.service';
import { EntityObject, SubEntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { MetaService } from '@modules/entity-object-data/shared/meta.service';
import { SelectableService } from '@modules/entity-object-data/shared/selectable.service';
import { SortAttribute, SortModel } from '@modules/entity-object-data/shared/sort.model';
import { EntityObjectGridColumn } from '@modules/entity-object-grid/entity-object-grid-column';
import { EntityObjectGridService } from '@modules/entity-object-grid/entity-object-grid.service';
import { EntityObjectPreferenceService } from '@modules/entity-object/shared/entity-object-preference.service';
import { SideviewmenuService } from '@modules/entity-object/shared/sideviewmenu.service';
import {
	ExportBoListComponent
} from '@modules/entity-object/sidebar/statusbar/export-bo-list/export-bo-list.component';
import { ExportBoListTemplateState } from '@modules/entity-object/sidebar/statusbar/statusbar.component';
import { ValueToSafeHtmlPipe } from '@modules/html/pipes/value-to-safe-html.pipe';
import { Logger } from '@modules/log/shared/logger';
import {
	ColumnAttribute,
	Preference,
	SelectedAttribute,
	SideviewmenuPreferenceContent, SubformSearchtemplatePreferenceContent
} from '@modules/preferences/preferences.model';
import { RuleService } from '@modules/rule/shared/rule.service';
import { SearchEditorComponent } from '@modules/search/search-editor/search-editor.component';
import { SearchService } from '@modules/search/shared/search.service';
import { EntityOrder, IEntityObject, SubEntityInsertLocation, UserAction } from '@nuclos/nuclos-addon-api';
import { ColorUtils } from '@shared/color-utils';
import { Mixin } from '@shared/mixin';
import { ObjectUtils } from '@shared/object-utils';
import { BrowserDetectionService } from '@shared/service/browser-detection.service';
import {
	BrowserRefreshService,
	EXPAND_SIDEVIEW_PARAM,
	SelectReferenceInOtherWindowEvent,
	SHOW_CLOSE_ON_SAVE_BUTTON_PARAM
} from '@shared/service/browser-refresh.service';
import { FqnService } from '@shared/service/fqn.service';
import { HyperlinkService } from '@shared/service/hyperlink.service';
import { LocalStorageService } from '@shared/service/local-storage.service';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { NuclosValidationService } from '@shared/service/nuclos-validation.service';
import { NumberService } from '@shared/service/number.service';
import { SystemParameter } from '@shared/system-parameters';
import {
	CellClassParams,
	ColDef, ColGroupDef,
	ColumnEvent,
	GridApi,
	GridOptions,
	NewValueParams,
	RowClassParams,
	RowNode,
	SuppressKeyboardEventParams
} from 'ag-grid-community';
import { SearchConditionBuilder } from 'app/modules/entity-object-data/shared/search-condition-builder';
import { UploaderOptions, UploadInput, UploadOutput } from 'ngx-uploader';
import { BehaviorSubject, EMPTY, interval, Observable, of, of as observableOf, Subject, Subscription } from 'rxjs';

import {
	debounce,
	debounceTime,
	distinctUntilChanged,
	finalize,
	map,
	mergeMap,
	skip,
	switchMap,
	take,
	takeUntil,
	takeWhile,
	tap
} from 'rxjs/operators';
import { UnsubscribeNotifier } from '../../data/interfaces/unsubscribe-notifier';
import { Link, SubEOLinkContainer } from '../../data/schema/link.model';
import { EntityObjectClipboardRepresentation } from '../../modules/entity-object-data/shared/entity-object.class';
import { AbstractGridComponentDirective } from '../shared/abstract-grid-component.directive';

import { ColumnLayoutChanges } from '../shared/column-layout-changes';
import { LayoutService } from '../shared/layout.service';
import { SubformBooleanEditorComponent } from './cell-editors/subform-boolean-editor/subform-boolean-editor.component';
import {
	SubformComboboxEditorComponent
} from './cell-editors/subform-combobox-editor/subform-combobox-editor.component';
import { SubformDateEditorComponent } from './cell-editors/subform-date-editor/subform-date-editor.component';
import { SubformEmailEditorComponent } from './cell-editors/subform-email-editor/subform-email-editor.component';
import {
	SubformHyperlinkEditorComponent
} from './cell-editors/subform-hyperlink-editor/subform-hyperlink-editor.component';
import { SubformLovEditorComponent } from './cell-editors/subform-lov-editor/subform-lov-editor.component';
import {
	SubformMultilineEditorComponent
} from './cell-editors/subform-multiline-editor/subform-multiline-editor.component';
import { SubformNumberEditorComponent } from './cell-editors/subform-number-editor/subform-number-editor.component';
import { SubformStringEditorComponent } from './cell-editors/subform-string-editor/subform-string-editor.component';
import { FilterBooleanComponent } from './cell-filters/filter-boolean/filter-boolean.component';
import { FloatingFilterDateComponent } from './cell-filters/floating-filter-date/floating-filter-date.component';
import { FloatingFilterLovComponent } from './cell-filters/floating-filter-lov/floating-filter-lov.component';
import {
	FloatingFilterBooleanComponent
} from './cell-filters/floating-filter-boolean/floating-filter-boolean.component';
import {
	FloatingFilterEditorComponent,
} from './cell-filters/floating-filter-editor/floating-filter-editor.component';
import { FloatingFilterNumberComponent } from './cell-filters/floating-filter-number/floating-filter-number.component';
import { FloatingFilterTextComponent } from './cell-filters/floating-filter-text/floating-filter-text.component';
import { IgnoreDirtyDateFilter } from './cell-filters/ignore-dirty-date-filter.class';
import { IgnoreDirtyNumberFilter } from './cell-filters/ignore-dirty-number-filter.class';
import { IgnoreDirtyTextFilter } from './cell-filters/ignore-dirty-text-filter.class';
import {
	SubformBooleanRendererComponent,
	SubformDateRendererComponent,
	SubformDocumentRendererComponent,
	SubformEditRowRendererComponent,
	SubformEmailRendererComponent,
	SubformHiddenRendererComponent,
	SubformHyperlinkRendererComponent,
	SubformNumberRendererComponent,
	SubformReferenceRendererComponent,
	SubformStateIconRendererComponent
} from './cell-renderer';
import { FilterFavoriteHeaderGroup } from './filter-favorite-header-group/filter-favorite-header-group';
import { NoRowsOverlayComponent } from './no-rows-overlay/no-rows-overlay.component';
import { SubformCellValidator } from './subform-cell-validator';
import { SubformRowComparator } from './subform-row-comparator';
import {
	NuclosCellEditorParams,
	NuclosCellRendererParams,
	RowEditCellParams,
	SubformColumn
} from './web-subform.model';
import { CLIPBOARD_KEY, WebSubformService } from './web-subform.service';

@Component({
	selector: 'nuc-web-subform',
	templateUrl: './web-subform.component.html',
	styleUrls: ['./web-subform.component.scss']
})
@Mixin([ColumnLayoutChanges])
export class WebSubformComponent extends AbstractGridComponentDirective<WebSubform>
	implements UnsubscribeNotifier, ColumnLayoutChanges, OnInit, DoCheck, OnDestroy {
	static isUndefinedOrTrue(value: any) {
		return value === undefined || value === true;
	}

	showPrintSearchResultListButton = false;
	hiddenColumns: Array<string | undefined>;
	frameworkComponents;

	overlayLoadingTemplate = '<i class="fa fa-hourglass-half gridLoadingIcon" aria-hidden="true"></i><span class="fas sr-only">'
		+ this.nuclos18nService.getI18n('common.loading') + '</span>';
	unsubscribe$ = new Subject<boolean>();

	subformStyle;
	options: UploaderOptions;
	uploadInput: EventEmitter<UploadInput> = new EventEmitter();
	fileDropAllowedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	filteringSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	showFavoriteSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	dragOver: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	noRowsOverLayComponent;
	noRowsOverLayComponentParams;
	@ViewChildren('subformPopoverContainer', {read: ElementRef}) subformPopoverContainers: QueryList<ElementRef>;
	@ViewChild('toolbarButtons', {read: ElementRef}) toolbarButtonsRef: ElementRef;
	private popoverShowing = false;

	private subformLinks: SubEOLinkContainer | undefined;

	private subformMeta: EntityMeta;

	private selectedFilter: Preference<SubformSearchtemplatePreferenceContent>;

	private selectedColumnPref$: BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined> = new BehaviorSubject(undefined);

	private tablePreferenceSubscription: Subscription;

	private sortModel: SortModel = new SortModel([]);

	private columnsFromLayout = new Map<string, WebSubformColumn>();

	/**
	 * The dependency between this subform and the root EO, might span multiple subforms.
	 */
	private dependency: EntityObjectDependency;

	/**
	 * The selected SubEos of the parent subform, if this is a sub-subform.
	 */
	private selectedParents?: SubEntityObject[];

	private columnsPref?: Preference<SideviewmenuPreferenceContent>;

	private sortCalculatedAttributes: boolean;

	private filtering = false;
	private active = false;
	private firstFileAttributeColumn?: ColumnAttribute;

	private filterPrefsChange = new Subject<any>();

	private turnColumnChangeTriggerOff = false;
	private insertOnTop: boolean;
	private entityOrder?: EntityOrder;

	private statefulParent = false;


	private mpSelectedSubEOs: Map<string, any[]> = new Map<string, any[]>();

	/* mixins */
	// tslint:disable-next-line
	columnLayoutChanged: Subject<Date> = new Subject<Date>();
	// tslint:disable-next-line
	columnLayoutChangedSubscription: Subscription;

	/**
	 * TODO: Too many dependencies
	 */
	constructor(
		injector: Injector,
		private metaService: MetaService,
		private entityObjectService: EntityObjectService,
		private browserRefreshService: BrowserRefreshService,
		private webSubformService: WebSubformService,
		private selectableService: SelectableService,
		private fqnService: FqnService,
		private $log: Logger,
		private ruleService: RuleService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		protected i18nService: NuclosI18nService,
		elementRef: ElementRef,
		private modalService: NuclosDialogService,
		private exportBoListTemplateState: ExportBoListTemplateState,
		private authenticationService: AuthenticationService,
		private browserDetectionService: BrowserDetectionService,
		private validationService: NuclosValidationService,
		private sideviewmenuService: SideviewmenuService,
		private eoGridService: EntityObjectGridService,
		private layoutService: LayoutService,
		private nuclosConfig: NuclosConfigService,
		private nuclosHotkeysService: NuclosHotkeysService,
		private eoVLPService: EntityVLPService,
		private eoEventService: EntityObjectEventService,
		private localStorageService: LocalStorageService,
		private hyperlinkService: HyperlinkService,
		private numberService: NumberService,
		private htmlSanitizerPipe: ValueToSafeHtmlPipe,
		private nuclos18nService: NuclosI18nService,
		private searchService: SearchService,
		private dependentSearchService: DependentsSearchfilterService
	) {
		super(injector, i18nService, elementRef);

		this.eoChangeListener = {
			refreshVlp: (
				entityObject: EntityObject,
				attributeName: string,
			) => {
				if (this.columnsFromLayout.get(attributeName)?.valuelistProvider) {
					entityObject.refreshVlp(attributeName, this.columnsFromLayout.get(attributeName)?.valuelistProvider);
				}
			},
			afterAttributeChange: () => this.softRefresh(),
			afterValidationChange: () => this.softRefresh(true),
			afterReload: () => this.softRefresh()
		};

		this.showPrintSearchResultListButton = this.authenticationService.isActionAllowed(
			UserAction.PrintSearchResultList
		);

		this.nuclosConfig.getSystemParameters().subscribe(params => {
			this.sortCalculatedAttributes = params.is(SystemParameter.SORT_CALCULATED_ATTRIBUTES);
		});

		this.eo.getMeta().pipe(take(1)).subscribe((meta) => {
			this.statefulParent = meta.hasStateModel();
		});
		this.frameworkComponents = {
			filterBooleanComponent: FilterBooleanComponent,
			ignoreDirtyTextFilter: IgnoreDirtyTextFilter,
			ignoreDirtyNumberFilter: IgnoreDirtyNumberFilter,
			ignoreDirtyDateFilter: IgnoreDirtyDateFilter,
			floatingFilterDateComponent: FloatingFilterDateComponent,
			floatingFilterNumberComponent: FloatingFilterNumberComponent,
			floatingFilterTextComponent: FloatingFilterTextComponent,
			floatingFilterBooleanComponent: FloatingFilterBooleanComponent,
			filterLovComponent: FloatingFilterLovComponent,
			noRowsOverlayComponent: NoRowsOverlayComponent
		};

		this.noRowsOverLayComponent = 'noRowsOverlayComponent';
		this.noRowsOverLayComponentParams = {
			fileDropAllowedSubject: this.fileDropAllowedSubject,
			showFavoriteSubject: this.showFavoriteSubject,
			filteringSubject: this.filteringSubject
		};

		this.eoEventService.observeExternalOpenEo()
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe((eo: IEntityObject) => {
				if (eo.getEntityClassId() === this.subformMeta.getEntityClassId()) {
					if (this.getDependents()?.current()?.find(element => element.getId() === eo.getId())) {
						this.eoEventService.listenForExternalChanges(eo)
							.pipe(takeUntil(this.unsubscribe$))
							.subscribe((data: any | undefined) => {
								if (eo.getEntityClassId().endsWith('DYN')) {
									this.getDependents()?.reload();
								}
							});
					}
				}
			});
	}

	@HostBinding('attr.name') get nameForComponent(): String | undefined {
		return this.getNameForHtml();
	}

	@HostBinding('tabindex') get tabIndex(): number {
		return 0;
	}

	@HostBinding('attr.next-focus-field') get nextFocusField(): String | undefined {
		return this.getNextFocusField();
	}

	@HostBinding('attr.next-focus-comp') get nextFocusComp(): String | undefined {
		return this.getNextFocusComponent();
	}

	@HostBinding('attr.initial-focus') get hasInitialFocus(): boolean | undefined {
		return this.getInitialFocus();
	}

	@HostListener('document:keydown.Enter') setFocusOnToolbarInside(): void {
		if (this.subformHasFocus() && document.activeElement?.tagName === 'NUC-WEB-SUBFORM') {
			setTimeout(() => {
				let visibleButtonsOrInputs = $(document.activeElement!).find('button:visible:not(.btn:disabled),input:visible:not(.btn:disabled)');
				if (visibleButtonsOrInputs.length > 0) {
					visibleButtonsOrInputs
						.first()
						.trigger('focus');
				} else {
					// NUCLOS-10372 no active button or checkbox visible? Jump to the first column header instead.
					$(document.activeElement!)
						.find('[tabindex]')
						.first()
						.trigger('focus');
				}
			});
		}
	}

	@HostListener('document:keydown.Escape') focusOutOfSubformInner(): void {
		if (this.subformHasFocus() && this.gridOptions.api?.getEditingCells().length === 0) {
			setTimeout(() => {
				this.elementRef.nativeElement.focus();
			});
		}
	}

	// handleColumnChanges(gridOptions: GridOptions): void {};
	// onColumnChangesDebounced(): Observable<Date> {return {} as Observable<Date>};
	onColumnChangesDebounced(gridOptions: GridOptions): Observable<Date> {
		return {} as Observable<Date>;
	}

	onColumnResizedOrMoved($event: any) {
		this.gridOptions.api?.stopEditing();
	}

	getColDef(gridOptions: GridOptions, event: ColumnEvent): ColDef | undefined {
		return;
	}

	ngOnInit() {
		this.active = true;
		// NuclosRowColor
		this.gridOptions.getRowStyle = (params: RowClassParams) => {
			let rowColor = (params.data as SubEntityObject).getRowColor();
			let useDefaultTextColor = params.node.data.isNew() || params.node.data.isDirty() || params.node.data.isMarkedAsDeleted();
			let textColor = ColorUtils.calculateTextColorFromBackgroundColor(useDefaultTextColor ? '' : rowColor);
			return {
				'background-color': rowColor !== undefined ? rowColor : '',
				'color': textColor !== undefined ? textColor : ''
			};
		};

		this.calculateSubformStyle();

		// use timeout to let shortcut registration run in next GUI Loop
		setTimeout(() => {
			this.registerShortCuts();
		});

		if (
			this.authenticationService.isActionAllowed(
				UserAction.WorkspaceCustomizeEntityAndSubFormColumn
			)
		) {
			// save column layout when column order, width or sort order was changed
			this.onColumnChangesDebounced(this.gridOptions).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
				if (!this.turnColumnChangeTriggerOff) {
					this.storeSubformColumnLayout();
					this.updateRowHeightFunction(this.getVisibleColumns());
					this.resetRowHeight();
				} else {
					this.turnColumnChangeTriggerOff = false;
				}
			});
		}

		// this is needed to refresh content of dependent opened by e.g. generation and edited
		this.browserRefreshService.onReferenceSelection(null).pipe(
			takeWhile(() => this.active)
		).subscribe((event: SelectReferenceInOtherWindowEvent | undefined) => {
			if (event !== undefined) {
				this.eo.getAllDependents().forEach((value) => {
					value.current()?.forEach((subBo) => {
						if (subBo.getId() === event.eoId) {
							subBo.reload().pipe(
								take(1)
							).subscribe();
						}
					});
				});
			}
		});

		this.filterPrefsChange.pipe(
			takeWhile(() => this.active),
			debounce(() => interval(250)),
			distinctUntilChanged((prev, curr) => ObjectUtils.compare(prev, curr))
		).subscribe((model) => {
			this.storeFilterPreferences(model);
			this.updateFilterAndReload();
		});

		this.dragOver.pipe(
			takeWhile(() => this.active),
			distinctUntilChanged()
		).subscribe(isDragOver => {
			this.$log.log('dragOver: ' + isDragOver);
			if (this.gridOptions.api && this.firstFileAttributeColumn) {
				if (isDragOver && this.isNewEnabled() && !this.firstFileAttributeColumn.readonly) {
					this.fileDropAllowedSubject.next(true);
					this.gridOptions.api.showNoRowsOverlay();
				} else {
					this.fileDropAllowedSubject.next(false);
					if (this.gridOptions.api.getDisplayedRowCount() > 0) {
						this.gridOptions.api.hideOverlay();
					}
				}
			}
		});

		this.dependentSearchService
			.observeSelectedSubformSearchfilter(this.getSubformFilterPrefId()).pipe(takeUntil(this.unsubscribe$))
			.subscribe( selectedFilter => {
				if (selectedFilter) {
					this.selectedFilter = selectedFilter;
					// puts the newly selected filters columns into the column prefs
					this.restoreColumnPrefsFromSubformSearchfilter(selectedFilter);
				}
		});
		this.dependentSearchService
			.observeAllDependentSearchfilters(this.getSubformFilterPrefId()).pipe(takeUntil(this.unsubscribe$))
			.subscribe( subformFilters => {
				if (subformFilters) {
					let favoriteFilters = subformFilters.filter(filter => filter.content.showAsFavorite);
					let showFavoriteFilters = favoriteFilters.length > 0;
					if (showFavoriteFilters !== this.showFavoriteSubject.getValue()) {
						this.showFavoriteSubject.next(showFavoriteFilters);
						// only call init, if the subform links are already available otherwise, we assume the init routine will call initColumns()
						if (this.columnsPref && this.subformMeta) {
							this.initColumns().pipe(take(1)).subscribe();
						}
					}
				}
			});
	}

	/**
	 * Calculates the style via getStyle() and stores it in the subformStyle field.
	 * Binding [ngStyle] to getStyle() directly would kill performance. So we calculate the style only, when calling
	 * this method and store it in a field, which unlike the getStyle() method, will not be constantly evaluated when used
	 * in the template.
	 */
	calculateSubformStyle() {
		this.subformStyle = this.getStyle();
		// remove color attributes for now, it is unclear how they should affect the subform and even the richclient gives us no clue
		delete this.subformStyle['background-color'];
		delete this.subformStyle['color'];
	}

	showListExportButton(): Observable<boolean | undefined> {
		return this.eo.getMeta().pipe(
			map((m) => this.showPrintSearchResultListButton && this.subformMeta.canExport() !== false &&
				(this.subformMeta.canExport() === true || m.canExport()))
		);
	}

	ngOnDestroy(): void {
		this.clearSubscriptions();
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
		this.active = false;
	}

	isWritable(): boolean {
		let restriction = this.eo.getSubEoRestriction(this.webComponent.foreignkeyfieldToParent);
		let result = super.isWritable() && restriction !== 'readonly' && this.subformLinks;
		return !!result;
	}

	isVisible(): boolean {
		return (
			!(this.hasStatefulParent() && this.isOverlayContext())
			&& super.isVisible()
			&& !!this.eo.getSubEoLink(this.webComponent.foreignkeyfieldToParent)
		);
	}

	/**
	 * Loads Subform data from the server.
	 */
	loadData() {
		if (!this.subformLinks) {
			this.$log.error('Could not load subform data - no link');
			return;
		}

		this.$log.debug('Loading Subform %o...', this.subformLinks.boMeta.href);
		this.updateDependents();
		if (this.dependents) {
			this.dependents.loadIfEmpty(this.sortModel, this.isReadonly());
		}
	}

	/**
	 * Instantiates a new Sub-EO and adds it to the grid data.
	 */
	newEntry(event: Event | undefined) {
		/* the first condition is the same, which determines the visibility of the add button - nevertheless, we need to check again because
		this code can be reached, by triggering the new entry shortcut */
		if (!this.isNewEnabled() || !this.webComponent.enabled) {
			return;
		}
		this.createSubEo().pipe(take(1)).subscribe(subEo => {
			if (subEo) {
				this.addSubEO(subEo);
				this.updateGridData();
				if (this.canOpenModal()) {
					this.modalService.openEoInModal(
						subEo,
						undefined,
						false,
						event ? event.target : undefined
					).pipe(
						take(1),
						// After the modal is closed, layout rules may change again
						finalize(() => {
							this.ruleService.updateRuleExecutor(subEo).pipe(
								take(1)
							);
						})
					).subscribe(() => {
						// This is called when the modal is closed.
						// Textarea cells might have to be resized if their content was changed in the modal.
						this.resetRowHeight();
						this.softRefresh();
						// This line scrolls to the bottom of the grid
						this.ensureSubEoVisible(subEo);
					});
				} else {
					this.editCell(subEo);
				}
			}
		});
	}

	/**
	 * Ensures that the SubEntityObject is visible.
	 * This means it will scroll to it, if necessary.
	 * @param subEo
	 */
	ensureSubEoVisible(subEo: any) {
		this.gridOptions.api?.ensureNodeVisible(subEo, 'bottom');
	}

	/**
	 * Unselect with Space
	 */
	@HostListener('document:keydown.Space', ['$event'])
	unselectEntry($event: Event): void {
		if ($event.target !== null && $($event.target).parent().hasClass('ag-row') &&
		this.subformHasFocus()) {
			const indexRow = +$($event.target).parent().attr('row-index');
			this.$log.debug('Check row index: ' + indexRow);
			if (this.gridOptions.api) {
				const rowNode = this.gridOptions.api.getDisplayedRowAtIndex(indexRow);
				if (rowNode !== undefined && rowNode.isSelected()) {
					rowNode.setSelected(false, true, true);
					$event.preventDefault();
				}
			}
		}
	}

	deleteSelected(): void {
		super.deleteSelectedImpl(this.webComponent.foreignkeyfieldToParent);
	}

	/**
	 * Clones the selected Sub-EOs (for insert).
	 */
	cloneSelected(): void {
		let clones: SubEntityObject[] = [];
		this.createSubEo().pipe(take(1)).subscribe(newSubEo => {
			if (newSubEo) {
				this.getSelectedSubEOs().forEach(selectedSubEo => {
					let clone = newSubEo.clone();
					this.eoVLPService.addEntityObjectMap(clone);
					this.transferClonableAttributes(selectedSubEo, clone);
					clones.push(clone);
				});
				this.addSubEOs(clones);
				this.ensureSubEoVisible(clones[clones.length - 1]);
			}
		});
	}

	copy() {
		this.webSubformService.copySelectedRowsToClipboard(this.gridOptions, this.subformMeta);
		this.webSubformService.copySelectedRowsToStorage(this.getSelectedSubEOs());
	}

	pasteRowsFromStorage(): void {
		const parsedContent = JSON.parse(this.localStorageService.getItem(CLIPBOARD_KEY));
		if (Array.isArray(parsedContent) && parsedContent.length > 0) {
			parsedContent.reverse().forEach(entry => {
				this.pasteStorageRow(entry);
			});
		}
	}

	toggleFilter(): void {
		// TODO enable floatingFilters for each column or disable is a toggle
		this.filtering = !this.filtering;
		this.filteringSubject.next(this.filtering);
		this.initColumns().pipe(take(1)).subscribe();
	}

	isAnyFilterActive(): boolean {
		return (this.gridOptions.columnApi?.getAllColumns()?.filter(value => value.isFilterActive()).length || 0) > 0;
	}

	/**
	 * Flags the underlying Sub-EO as selected, when a row is selected.
	 *
	 * @param params
	 */
	rowSelected(params: { node: RowNode }) {
		if (!this.dependents?.isLoading()) {
			let subEO: SubEntityObject = params.node.data;
			subEO.setSelected(params.node.isSelected());

			this.updateSubEoSelectionInRoot();
		}
	}

	openExportBoListModal() {
		this.exportBoListTemplateState.boId = this.eo.getId();
		this.exportBoListTemplateState.refAttrId = this.subformMeta.getRefAttrId();
		if (this.isSubSubform()) {
			if (this.selectedParents && this.selectedParents.length === 1) {
				// Currently only works one selected parent
				this.exportBoListTemplateState.boId = this.selectedParents[0].getId();
			} else {
				return;
			}
		}
		if (this.selectedColumnPref$.getValue()) {
			this.exportBoListTemplateState.columnPreference = this.selectedColumnPref$.getValue() as Preference<SideviewmenuPreferenceContent>;
		}
		this.eo.getMeta().pipe(take(1)).subscribe(meta => {
			this.exportBoListTemplateState.meta = meta;
			this.modalService.custom(
				ExportBoListComponent,
				{
					'meta': meta,
					'useSearchText': false
				}
			).pipe(
				take(1)
			).subscribe();
		});
	}

	isReadonly() {
		if (!this.isWritable()) {
			return true;
		}

		let subEORestriction = this.eo.getSubEoRestriction(
			this.webComponent.foreignkeyfieldToParent
		);
		return subEORestriction === 'readonly';
	}

	isNewVisible() {
		if (this.isReadonly()) {
			return false;
		}

		if (!WebSubformComponent.isUndefinedOrTrue(this.webComponent.newEnabled)) {
			return false;
		}

		// NUCLOS-6134
		let subEORestriction = this.eo.getSubEoRestriction(
			this.webComponent.foreignkeyfieldToParent
		);

		// TODO: Use a map!
		return subEORestriction !== 'nocreate' && subEORestriction !== 'nocreate,nodelete';
	}

	isNewEnabled() {
		if (this.isSubSubform()) {
			// Currently sub-subforms only work for a single parent
			if (this.selectedParents && this.selectedParents.length !== 1) {
				return false;
			}
		}

		return this.isNewVisible();
	}

	isEditEnabled() {
		if (this.isReadonly()) {
			return false;
		}

		return WebSubformComponent.isUndefinedOrTrue(this.webComponent.editEnabled);
	}

	isCloneVisible() {
		if (this.isReadonly()) {
			return false;
		}

		if (!WebSubformComponent.isUndefinedOrTrue(this.webComponent.cloneEnabled)) {
			return false;
		}

		// NUCLOS-6134
		let subEORestriction = this.eo.getSubEoRestriction(
			this.webComponent.foreignkeyfieldToParent
		);
		return subEORestriction !== 'nocreate' && subEORestriction !== 'nocreate,nodelete';
	}

	isCopyEnabled() {
		return this.isCloneVisible() && this.getSelectedSubEOs().length > 0;
	}

	isPasteEnabled() {
		return this.isCloneVisible() && this.subformMeta.getEntityClassId() === this.webSubformService.getStorageContentSubformId();
	}

	isCloneEnabled() {
		return this.isCloneVisible() && this.getSelectedSubEOs().length > 0;
	}

	isDeleteVisible() {
		if (this.isReadonly()) {
			return false;
		}

		if (!WebSubformComponent.isUndefinedOrTrue(this.webComponent.deleteEnabled)) {
			return false;
		}

		// NUCLOS-6134
		let subEORestriction = this.eo.getSubEoRestriction(
			this.webComponent.foreignkeyfieldToParent
		);
		return subEORestriction !== 'nodelete' && subEORestriction !== 'nocreate,nodelete';
	}

	isDeleteEnabled() {
		return this.isDeleteVisible()
			&& this.getSelectedSubEOs().length > 0
			&& this.getSelectedSubEOs().filter(subEo => (!subEo.canDelete() && !subEo.isNew())).length === 0;
	}

	hasSubformLayout() {
		if (this.subformMeta) {
			return this.subformMeta.getLinks().defaultLayout !== undefined;
		}
		return false;
	}

	canOpenModal() {
		if (this.subformMeta) {
			return this.hasSubformLayout() && !this.webComponent.ignoreSubLayout;
		}
		return false;
	}

	refresh(params: any): boolean {
		return false;
	}

	cellEditingStopped(event: { node: RowNode }) {
		if (this.gridOptions && this.gridOptions.api) {
			this.gridOptions.api.refreshCells();
		}
	}

	getColumnFromLayout(attributeId: string): WebSubformColumn | undefined {
		return (
			this.webComponent.subformColumns &&
			this.webComponent.subformColumns.find(column => column.name === attributeId)
		);
	}

	isCloneable(attributeId: string): boolean {
		let column = this.getColumnFromLayout(attributeId);

		if (!column) {
			this.$log.debug('Could not find layout column for attribute %o', attributeId);
			return true;
		}

		return !column.notCloneable;
	}

	hasAutonumber() {
		return this.getAutonumberAttribute() !== undefined;
	}

	hasAutonumberRestrictions() {
		let autoNumberAttr = this.getAutonumberAttribute();
		return autoNumberAttr === undefined || this.getColumnRestriction(autoNumberAttr, true) !== '';
	}

	isAutonumberUnique() {
		let autoNumberAttr = this.getAutonumberAttribute();
		return autoNumberAttr !== undefined && autoNumberAttr.isUnique();
	}

	canMoveSelectedRowsUp() {
		if (!this.gridOptions.api) {
			return;
		}

		let model = this.gridOptions.api.getModel();
		if (!model || model.getRowCount() === 0 || this.hasAutonumberRestrictions() || this.isAutonumberUnique()) {
			return false;
		}

		// Can't move up, if the first entry is selected
		let firstRow = model.getRow(0);
		if (firstRow !== null && firstRow?.data.isSelected()) {
			return false;
		}

		// We can move up, if there is any other selected dependent
		for (let i = 1; i < model.getRowCount(); i++) {
			let row = model.getRow(i);
			if (row !== null && row?.data.isSelected()) {
				return true;
			}
		}

		return false;
	}

	canMoveSelectedRowsDown() {
		if (!this.gridOptions.api) {
			return;
		}

		let model = this.gridOptions.api.getModel();
		if (!model || model.getRowCount() === 0 || this.hasAutonumberRestrictions() || this.isAutonumberUnique()) {
			return false;
		}

		// Can't move down, if the last entry is selected
		let row = model.getRow(model.getRowCount() - 1);
		if (row !== null && row?.data.isSelected()) {
			return false;
		}

		// We can move down, if there is any other selected dependent
		for (let i = 0; i < model.getRowCount() - 1; i++) {
			let rowI = model.getRow(i);
			if (rowI !== null && rowI?.data.isSelected()) {
				return true;
			}
		}

		return false;
	}

	moveSelectedRowsUp() {
		if (!this.gridOptions.api) {
			return;
		}

		let attribute = this.getAutonumberAttribute();
		if (!attribute) {
			this.$log.warn('Could not move selected dependents up - no autonumber attribute');
			return;
		}

		let model = this.gridOptions.api.getModel();
		if (model.getRowCount() === 0) {
			this.$log.warn('Could not move selected dependents up - no dependents');
			return;
		}

		for (let i = 1; i < model.getRowCount(); i++) {
			let rowI = model.getRow(i);
			let otherRowI = model.getRow(i - 1);
			if (rowI !== null && otherRowI !== null) {
				let dep = rowI?.data;
				let otherDep = otherRowI?.data;
				if (dep.isSelected()) {
					// Change position with previous dependent
					let value = dep.getAttribute(attribute.getAttributeID());
					let otherValue = otherDep.getAttribute(attribute.getAttributeID());

					// Set the attribute without notifying listeners - otherwise this would trigger an autonumber update
					dep.setDirty(true);
					dep.setAttributeUnsafe(attribute.getAttributeID(), otherValue);

					otherDep.setAttribute(attribute.getAttributeID(), value);
				}
			}
		}
	}

	moveSelectedRowsDown() {
		if (!this.gridOptions.api) {
			return;
		}

		let attribute = this.getAutonumberAttribute();
		if (!attribute) {
			this.$log.warn('Could not move selected dependents up - no autonumber attribute');
			return;
		}

		let model = this.gridOptions.api.getModel();
		if (model.getRowCount() === 0) {
			this.$log.warn('Could not move selected dependents up - no dependents');
			return;
		}

		for (let i = model.getRowCount() - 2; i >= 0; i--) {
			let rowI = model.getRow(i);
			let otherRowI = model.getRow(i + 1);
			if (rowI !== null && otherRowI !== null) {
				let dep = rowI?.data;
				let otherDep = otherRowI?.data;
				if (dep.isSelected()) {
					// Change position with previous dependent
					let value = dep.getAttribute(attribute.getAttributeID());
					let otherValue = otherDep.getAttribute(attribute.getAttributeID());

					// Set the attribute without notifying listeners - otherwise this would trigger an autonumber update
					dep.setDirty(true);
					dep.setAttributeUnsafe(attribute.getAttributeID(), otherValue);

					otherDep.setAttribute(attribute.getAttributeID(), value);
				}
			}
		}
	}

	isToolbarVisible(): boolean {
		return this.getToolbarOrientation() !== 'HIDE';
	}

	getToolbarOrientation(): WebToolbarOrientation {
		return this.webComponent.toolbarOrientation;
	}

	isClickOutside(target: any) {
		// If a datepicker is opened, its panel is attached to the body, not to the subform.
		// Therefore it is not a child of the subform in the DOM and the click appears to be outside.
		// But since there should be always at most 1 datepicker open at the same time, it should
		// be the one belonging to the subform, which means "no outside click".
		// TODO: This assumption might be wrong, if e.g. there was another datepicker component
		//  that is not displayed in an exclusive overlay.
		//  We should make sure, that the datepicker which received the click really belongs to
		//  this subform and the currently active cell editor.
		return $(target).closest('ngb-datepicker').length === 0;
	}

	/**
	 * Override method from abstract-grid.component.directive to fix NUCLOS-10273
	 * We need to call this.restoreFilterPreferences() exactly at the moment the grid is ready.
	 * Because before it has no effect and an indefinite amount later is too late.
	 */
	gridReady() {
		super.gridReady();
		this.restoreFilterPreferences();
	}

	dataChanged() {
		// reload preferences filter settings if data has changed in the grid
		this.restoreFilterPreferences();
	}

	onCellKeyDown(e) {
		this.$log.log(e.event);
		if (e.event.key === 'Enter') {
			if (e.column.colId === 'editRowIcon') {
				let elLink = $(e.event.target).find('a');
				if (elLink.length > 0) {
					elLink[0].click();
				}
			}
		} else if (e.event.shiftKey && e.event.altKey && e.event.code === 'KeyE') {
			if (this.gridOptions.api) {
				if (e.data && e.data.meta) {
					let attributeMeta = (e.data.meta as EntityMeta).getAttributeMetaByFqn(e.column.colId);
					if (attributeMeta && attributeMeta.isReference()) {
						let cellRendererInstances = this.gridOptions.api.getCellRendererInstances({
							rowNodes: [e.node],
							columns: [e.column]
						});
						if (cellRendererInstances.length > 0) {
							// @ts-ignore
							let referenceRenderer = (cellRendererInstances[0] as DynamicAgNg2Component)._agAwareComponent;
							referenceRenderer.editReferenceTarget(e.event);
						}
					}
				}
			}
		}
	}

	handleUpload(uploadedOutput: UploadOutput) {
		if (uploadedOutput.type === 'dragOver') {
			this.dragOver.next(true);
		} else {
			this.dragOver.next(false);
			if (uploadedOutput.type === 'start'
				&& (!this.firstFileAttributeColumn || !this.isNewEnabled() || this.firstFileAttributeColumn.readonly)) {
				if (uploadedOutput.file) {
					this.$log.log('cancelled ' + uploadedOutput.file.id);
					this.uploadInput.emit({type: 'cancel', id: uploadedOutput.file.id});
				}
				return;
			}
			if (this.isNewEnabled() && this.firstFileAttributeColumn && !this.firstFileAttributeColumn.readonly) {
				if (uploadedOutput.type === 'allAddedToQueue') {
					// when all files added in queue
					// auto upload files when added
					this.uploadInput.emit({
						type: 'uploadAll',
						url: this.nuclosConfig.getRestHost() + '/boDocuments/upload',
						withCredentials: true
					});
				}

				if (uploadedOutput.type === 'done') {
					let uploadedFile = uploadedOutput.file;

					if (uploadedFile) {
						if (uploadedFile.responseStatus && uploadedFile.responseStatus !== 200) {
							this.modalService.alert({
								title: this.i18nService.getI18n('webclient.error.title'),
								message: uploadedFile.response
							});
						}

						this.createSubEo().pipe(take(1)).subscribe(subEo => {
							if (subEo) {
								subEo.setAttribute(this.firstFileAttributeColumn!.boAttrId, {
									uploadToken: uploadedFile?.response,
									name: uploadedFile?.name
								});
								this.addSubEO(subEo);
								this.updateGridData();
							}
						});
					}
				}
			}
		}
	}

	openParentInNewTab() {
		this.eo.getMeta()
			.pipe(take(1))
			.subscribe(meta => {
				// NUCLOS-4435: Details of dynamik BO
				let entityClassId = meta.getDetailEntityClassId() || meta.getBoMetaId();
				if (!entityClassId) {
					entityClassId = this.eo.getEntityClassId();
				}

				let href =
					this.hyperlinkService.getBaseUrl() + '/view/' +
					entityClassId +
					'/' +
					this.eo.getId() +
					'?' +
					EXPAND_SIDEVIEW_PARAM +
					'&' +
					SHOW_CLOSE_ON_SAVE_BUTTON_PARAM +
					'&' +
					`showHeader=${meta.displayAsListOnly() ? 'true' : 'false'}`;

				window.open(href, '_blank');
			});
	}

	isOverlayContext() {
		return this.eo instanceof SubEntityObject;
	}

	hasStatefulParent() {
		return this.statefulParent;
	}

	hideSearchEditor() {
		this.setPopoverShowing(false);
	}

	toggleSearchEditor() {
		this.setPopoverShowing(!this.isPopoverShowing());
	}

	adjustPopover() {
		SearchEditorComponent.adjustPopover();
	}

	isPopoverShowing(): boolean {
		return this.popoverShowing;
	}

	setPopoverShowing(popoverShowing: boolean) {
		this.popoverShowing = popoverShowing;
		if (popoverShowing) {
			this.adjustPopover();
		}
	}

	/**
	 * Returns a unique identifier to map a searchfilter to a subform of an entity
	 */
	getSubformFilterPrefId() {
		return DependentsSearchfilterService.createKey(this.webComponent.foreignkeyfieldToParent, this.webComponent.parentSubform);
	}

	restoreColumnPrefsFromSubformSearchfilter(dependentSearchfilter: Preference<SubformSearchtemplatePreferenceContent>) {
		if (!dependentSearchfilter) {
			return;
		}
		// this condition should prevent pulling in empty column prefs, which could result in columns not being displayed
		if (this.columnsPref) {
			this.columnsPref.content.columns = ObjectUtils.cloneDeep(dependentSearchfilter.content.columns);
		}
		this.updateFilterAndReload();
	}

	protected updateFilterAndReload() {
		let nextFilter = SearchConditionBuilder.getSearchCondition(this.fqnService, this.subformMeta, this.columnsPref);
		if (this.dependents?.getFilter() === nextFilter) {
			return;
		}
		this.dependents?.setFilter(nextFilter);
		// only call reloadSoft(), if the subform links are already available otherwise, we assume the init routine will call loadData()
		if (this.subformLinks) {
			this.dependents?.reloadSoft();
			this.updateGridStatusOverlay(true);
			this.loadData();
		}
	}

	protected onGridApiReady(api: GridApi) {
		// set initial sorting when api is ready
		this.gridOptions.columnApi?.resetColumnState();
		this.gridOptions.columnApi?.applyColumnState({
			state: this.sortModel.getColumns()
		});
		api.refreshHeader();

		this.gridOptions.onFilterChanged = () => {
			if (this.gridOptions.api) {
				const filterModel = this.gridOptions.api.getFilterModel();
				this.filterPrefsChange.next(filterModel);
			}
		};
	}

	protected init() {
		this.$log.debug('Subform init...');

		this.clearSubscriptions();

		this.subformLinks = this.getSubformLinks();

		if (!this.subformLinks) {
			this.$log.error('Could not init subform - no link');
			return;
		}

		this.metaService
			.getSubBoMeta(this.subformLinks.boMeta.href)
			.pipe(
				take(1),
				tap(meta => {
					this.subformMeta = meta;

					// configure insert location for new entries
					this.insertOnTop = !(this.subformMeta.isSubformInsertBottom());
					if (this.insertOnTop) {
						this.entityOrder = undefined;
					} else {
						this.entityOrder = EntityObjectDependents.inverseSort;
					}
					this.dependents?.setInsertLocation((this.insertOnTop ? SubEntityInsertLocation.INSERT_TOP : SubEntityInsertLocation.INSERT_BOTTOM));
					this.dependents?.setEntityOrder(this.entityOrder);

					// configure grid options
					this.gridOptions.defaultColDef = {
						sortable: true,
						resizable: true
					};
					this.gridOptions.rowSelection = 'multiple';
					this.gridOptions.suppressRowClickSelection = this.subformMeta.showSubformSelectionColumn();
					this.gridOptions.enableCellTextSelection = true;
				}),
				switchMap(() => this.loadTablePreferences()),
				switchMap(() => this.initDependents().pipe(
					takeWhile(() => this.active)
				))
			)
			.subscribe(dependents => {
				dependents?.setEntityOrder(this.entityOrder);
				dependents?.setInsertLocation(this.insertOnTop ? SubEntityInsertLocation.INSERT_TOP : SubEntityInsertLocation.INSERT_BOTTOM);

				this.setDependents(dependents);
				this.loadData();
			});
	}

	protected getLogger(): Logger {
		return this.$log;
	}

	protected updateRowSelection() {
		super.updateRowSelection();

		this.updateSubEoSelectionInRoot();
	}

	protected getReferenceAttributeIdImpl() {
		return this.subformMeta.getMetaData().refAttrId;
	}

	protected getParentForNewSubEo(): EntityObject | undefined {
		let parentForNewSubEo: EntityObject | undefined;
		if (this.isSubSubform()) {
			// Sub-Sub-Eo: Parent must not be root!
			if (this.selectedParents && this.selectedParents.length === 1) {
				// Currently only work for a single parent
				parentForNewSubEo = this.selectedParents[0] as EntityObject;
			}
		} else {
			parentForNewSubEo = this.eo;
		}
		return parentForNewSubEo;
	}

	/**
	 * Creates a new Sub-EO for insertion into this subform, based on the data of the given EO.
	 */
	protected prepareNewSubEo(eo: EntityObject) {
		let subEo = super.prepareNewSubEo(eo);

		if (subEo) {
			subEo.shadowID = eo.shadowID;
			this.eoVLPService.addEntityObjectMap(subEo);
			this.ruleService.updateRuleExecutor(subEo, this.eo).pipe(take(1)).subscribe();
		}

		return subEo;
	}

	private registerShortCuts() {
		if (!this.isVisible() && this.isOverlayContext() && this.hasStatefulParent()) {
			return;
		}

		this.unsubscribe$ = new Subject<boolean>();

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT', keys: '0', description:
				this.i18nService.getI18n('webclient.shortcut.subform.focus')
		})
		.pipe(
			takeUntil(this.unsubscribe$)
		).subscribe(() => {
			if (this.gridOptions.api && this.subformHasFocus()) {
				this.gridOptions.api.clearFocusedCell();
				const firstColumn = this.getVisibleColumns()[0];
				if (firstColumn) {
					this.gridOptions.api.setFocusedCell(0, firstColumn.boAttrId, undefined);
				}
			}
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT', keys: 'n', description:
				this.i18nService.getI18n('webclient.shortcut.subform.new')
		})
		.pipe(
			takeUntil(this.unsubscribe$),
			debounceTime(500)
		).subscribe(() => {
				if (this.subformHasFocus()) {
					this.newEntry(undefined);
				}
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT', keys: 'x', description:
				this.i18nService.getI18n('webclient.shortcut.subform.delete')
		})
		.pipe(
			takeUntil(this.unsubscribe$)
		).subscribe(() => {
			if (this.subformHasFocus()) {
				this.deleteSelected();
			}
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT', keys: 'd', description:
				this.i18nService.getI18n('webclient.shortcut.subform.clone')
		})
		.pipe(
			takeUntil(this.unsubscribe$)
		).subscribe(() => {
			if (this.subformHasFocus()) {
				this.cloneSelected();
			}
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT', keys: 'w', description:
				this.i18nService.getI18n('webclient.shortcut.subform.open')
		})
		.pipe(
			takeUntil(this.unsubscribe$)
		).subscribe(() => {
			if (this.subformHasFocus()) {
				this.openSelectedSubEOInNewTab();
			}
		});
	}

	private openSelectedSubEOInNewTab() {
		if (this.gridOptions.api) {
			const selectedNodes = this.gridOptions.api.getSelectedNodes();
			if (selectedNodes.length > 0) {
				const subEO: SubEntityObject = selectedNodes[0].data;
				if (!subEO.getDetailLink()) {
					this.$log.error(
						'Can not open Sub-EO without detail link: %o',
						subEO
					);
					return;
				}

				subEO
					.getMeta()
					.pipe(take(1))
					.subscribe(meta => {
						// NUCLOS-4435: Details of dynamik BO
						let entityClassId = meta.getDetailEntityClassId() || meta.getBoMetaId();
						if (!entityClassId) {
							entityClassId = subEO.getEntityClassId();
						}

						let href =
							window.location.href.substring(0, window.location.href.indexOf('#/') + 2) + // baseURL
							'view/' +
							entityClassId +
							'/' +
							subEO.getId() +
							'?' +
							EXPAND_SIDEVIEW_PARAM +
							'&' +
							SHOW_CLOSE_ON_SAVE_BUTTON_PARAM;

						window.open(href, '_blank');
					});
			}
		}
	}

	private isColumnEnabledByLayout(attributeId: string) {
		let subformColumn = this.columnsFromLayout.get(attributeId);

		if (!subformColumn) {
			// No definition means no restriction
			return true;
		}

		return subformColumn.enabled;
	}

	private isColumnVisibleByLayout(attributeId: string) {
		let subformColumn = this.columnsFromLayout.get(attributeId);

		if (!subformColumn) {
			// No definition means no restriction
			return true;
		}

		return subformColumn.visible;
	}

	private clearSubscriptions() {
		if (this.dependentSubscription) {
			this.dependentSubscription.unsubscribe();
		}
		if (this.tablePreferenceSubscription) {
			this.tablePreferenceSubscription.unsubscribe();
		}
		if (this.columnLayoutChangedSubscription) {
			this.columnLayoutChangedSubscription.unsubscribe();
		}
	}

	private initDependents(): Observable<EntityObjectDependents | undefined> {
		if (this.webComponent.parentSubform) {
			this.$log.debug('Subscribing to parent selection changes...');
			return this.handleParentSelections();
		} else {
			let dependents = this.eo.getDependentsAndLoad(
				this.getReferenceAttributeId(), undefined, this.isReadonly()
			) as EntityObjectDependents;
			return observableOf(dependents);
		}
	}

	private handleParentSelections(): Observable<EntityObjectDependents | undefined> {
		return this.layoutService.getWebLayout(this.eo).pipe(
			mergeMap(layout => {
				let result: Observable<EntityObjectDependency> = EMPTY;
				if (layout) {
					result = observableOf(
						EntityObjectDependency.fromLayout(
							layout,
							this.webComponent.foreignkeyfieldToParent
						)
					);
				} else {
					this.$log.warn('No Layout');
				}
				return result;
			}),
			mergeMap(dependency => {
				this.dependency = dependency;
				this.$log.trace('Dependency: %o', dependency);
				return this.eo
					.getSelectionChangesInParentSubform(this.webComponent.parentSubform)
					.pipe(
						mergeMap((selectedParents: SubEntityObject[]) => {
							this.selectedParents = selectedParents;
							let dependents = this.getChildDependents(selectedParents);
							return observableOf(dependents);
						})
					);
			})
		);
	}

	private getChildDependents(
		selectedParents: SubEntityObject[]
	): EntityObjectDependents | undefined {
		let dependents;
		this.$log.debug('Parent changed');
		if (selectedParents.length === 1) {
			let parentEo = selectedParents[0];
			dependents = parentEo.getDependentsAndLoad(
				this.getReferenceAttributeId(),
				this.sortModel
			);
			let parentEoId = selectedParents[0].getId();
			let parentDependency = this.dependency.getParent();
			if (parentDependency) {
				this.dependency.getParent()!.setEoId('' + parentEoId);
			}
		}
		return dependents;
	}

	private setDependents(dependents: EntityObjectDependents | undefined) {
		if (this.dependentSubscription) {
			this.dependentSubscription.unsubscribe();
		}

		this.dependents = dependents;

		if (this.dependents) {
			this.dependentSubscription =
				this.dependents.asObservable().subscribe((subEos: SubEntityObject[]) => {
					this.updateGridStatusOverlay();

					this.restoreSubformSelection(this.getReferenceAttributeId(), subEos);

					this.setSubEos(subEos);
					this.safariLayoutHack();
				});
		} else {
			this.setSubEos([]);
		}

		this.updateGridStatusOverlay();
	}

	private updateDependents() {
		if (this.webComponent.parentSubform) {
			let selectedParents = this.eo.getSelectionInParentSubform(
				this.webComponent.parentSubform
			);
			let dependents = this.getChildDependents(selectedParents);
			this.setDependents(dependents);
		}
	}

	private loadTablePreferences() {
		let parentEntityUid = this.eo.getEntityClassId();

		return this.entityObjectPreferenceService
			.loadSubformPreferences(this.subformMeta, parentEntityUid, this.eo.getLayoutId())
			.pipe(
				tap(() => {
					this.selectedColumnPref$ = this.entityObjectPreferenceService.getSubformColumnPreferenceSelection(
						this.subformMeta.getBoMetaId()
					);

					if (this.tablePreferenceSubscription) {
						this.tablePreferenceSubscription.unsubscribe();
					}

					this.tablePreferenceSubscription = this.selectedColumnPref$.pipe(
						tap((pref) => {
							if (pref && (!ObjectUtils.isEqualJson(pref, this.columnsPref) || this.rightsChanged)) {
								this.turnColumnChangeTriggerOff = true;
								this.columnsPref = pref;
								// this.restoreColumnPrefsFromSubformSearchfilter(this.selectedFilter);
								let newState: SortAttribute[] = this.sortModel.getColumns();
								this.sortModel = new SortModel(
									this.sideviewmenuService.getAgGridSortModel(
										pref.content.columns.filter(
											col => col.sort && col.sort.direction
										),
										pref
									)
								);

								if (this.sortModel.getColumns().length === 0) {
									// for a sorting reset we need to update the sort attr on every current column
									newState.forEach((v: any) => {
										let sortModelCol = this.sortModel.getColumns()
											.find((c => c.colId === v.colId));
										if (sortModelCol) {
											// @ts-ignore
											sortModelCol.sort = null;
											// @ts-ignore
											sortModelCol.sortIndex = null;
										} else {
											v.sort = null;
											v.sortIndex = null;
											this.sortModel.getColumns().push(v);
										}
									});
								}

								if (this.gridOptions.api) {
									this.onGridApiReady(this.gridOptions.api);
								}

								// if there is a column with a file attribute files can be dropped directly
								// onto the subform to create new entries
								this.firstFileAttributeColumn = this.columnsPref.content.columns.find(column =>
									this.subformMeta.getAttributeMetaByFqn(column.boAttrId)?.isDocument()
									|| this.subformMeta.getAttributeMetaByFqn(column.boAttrId)?.isImage());
							}
						}),
						switchMap(() => this.initColumns())
					).subscribe();
				})
			);
	}

	private setSubEos(dependents: SubEntityObject[] | undefined) {
		this.getLogger().debug('updateDep: ' + this.dependents + ' => ' + dependents);
		let hardRefresh: boolean = dependents === undefined && this.dependents === undefined;
		let softRefresh: boolean = dependents !== undefined && this.dependents !== undefined;

		if (dependents) {
			dependents.forEach(eo => eo.addListener(this.eoChangeListener));
		}

		this.updateGridStatusOverlay();

		if (hardRefresh) {
			this.hardRefresh();
			return;
		}

		this.updateGridData();

		if (softRefresh) {
			this.softRefresh();
		}

		if (
			dependents &&
			this.gridOptions.defaultColDef &&
			this.gridOptions.defaultColDef.sortable &&
			this.sortModel &&
			!ObjectUtils.isEqual(this.getSortAttributes(), this.sortModel.getColumns())
		) {
			this.gridOptions.columnApi?.applyColumnState({
				state: this.sortModel.getColumns()
			});
		}
	}

	/**
	 * handle safari flexbox bug
	 */
	private safariLayoutHack() {
		if (this.browserDetectionService.isSafari()) {
			setTimeout(() => {
				let height = $(this.elementRef.nativeElement)
					.closest('.tab-container-panel')
					.height();
				$(this.elementRef.nativeElement)
					.find('.ag-bl')
					.height(height);
			});
		}
	}

	private initColumns(): Observable<ColDef[]> {
		if (!this.columnsPref || !this.subformMeta) {
			this.$log.error('Could not init subform');
			return of([]);
		}

	return this.createColumnDefinitionsAndRowHeight()
			.pipe(
				take(1),
				tap(colDefs => {
					if (this.isWritable()) {
						for (let colDef of colDefs) {
							this.addStatusClasses(colDef);
						}
					}
					this.setColumns(colDefs);
					this.restoreFilterPreferences();
				}),
				tap(() => {
					this.updateRowHeightFunction(this.getVisibleColumns());
					this.resetRowHeight();
				})
			);
	}

	private createSelectionColumnWithFilterEditor() {
		let selectionColDef = this.createSelectionColumn();
		selectionColDef.floatingFilter = this.filtering;
		selectionColDef.filter = 'filter-editor-component';
		selectionColDef.suppressFiltersToolPanel = true;
		selectionColDef.floatingFilterComponentFramework = FloatingFilterEditorComponent;
		selectionColDef.floatingFilterComponentParams = {
			filterKey: this.getSubformFilterPrefId(),
			hideSearchEditor: () => this.hideSearchEditor(),
			toggleSearchEditor: () => this.toggleSearchEditor(),
			checkOutsideClicked: ($event) => this.checkClickOutsideFilterEditor($event),
		};
		selectionColDef.suppressMenu = true;
		return selectionColDef;
	}

	private checkClickOutsideFilterEditor($event) {
		let clickOutsidePopover = $event
			&& this.subformPopoverContainers
			&& !this.subformPopoverContainers.some(element => element.nativeElement.contains($event))
			&& !this.subformPopoverContainers.some(element => element.nativeElement === $event);
		return clickOutsidePopover;
	}

	private createColumnDefinitionsAndRowHeight(): Observable<ColDef[]> {
		let columnDefinitions: ColDef[] = [];

		let meta = this.subformMeta;
		let visibleColumns = this.getVisibleColumns();

		for (let column of visibleColumns) {
			let colDef = this.createColumnDefinition(column, meta);
			if (colDef) {
				columnDefinitions.push(colDef);
			}
		}

		this.updateRowHeightFunction(visibleColumns);
		if (this.showFavoriteSubject.getValue()) {
			columnDefinitions = [this.createFavoriteFilterHeaderGroup(columnDefinitions, this)];
		}

		return new Observable(observer => {
			this.getEditRowColumnIfLayoutExists().pipe(
				take(1)
			).subscribe(colDef => {
				if (colDef) {
					columnDefinitions.unshift(colDef);
				}

				if (this.subformMeta.showSubformSelectionColumn()) {
					columnDefinitions.unshift(this.createSelectionColumnWithFilterEditor());
				}

				observer.next(columnDefinitions);
			});
		});
	}

	private createFavoriteFilterHeaderGroup(children: ColDef[], subform?): ColGroupDef {
		// row selection checkbox
		let rowSelectionCheckboxColDef: ColGroupDef = {
			headerTooltip: this.i18nService.getI18n('webclient.checkbox.selectAll'),
			headerName: 'Favorite filters header',
			headerGroupComponentFramework: FilterFavoriteHeaderGroup,
			headerGroupComponentParams: {
				subformFilterKey: subform?.getSubformFilterPrefId()
			},
			children: [
				...children
			]
		};

		return rowSelectionCheckboxColDef;
	}

	private getVisibleColumns(): Array<ColumnAttribute> {
		let columns = this.columnsPref?.content.columns ?? [];
		let meta = this.subformMeta;

		this.updateColumnsFromLayout();

		this.hiddenColumns = Array.from(meta.getAttributes().values())
			.filter(attrMeta => attrMeta && !this.isColumnVisible(attrMeta, meta))
			.map(attrMeta => (attrMeta ? attrMeta.getAttributeID() : undefined))
			.filter(attributeId => attributeId !== undefined);

		let visibleColumns = this.filterVisibleColumns(columns, meta);

		visibleColumns.forEach(col => {
			if (!col.width) {
				col.width = 200;
			}
			let attribute = this.subformMeta.getAttributeMetaByFqn(col.boAttrId);
			col.getPreferredHeight = (eo: SubEntityObject) => {
				if (attribute) {
					return this.getPreferredHeight(eo, attribute, col.width);
				}
				return undefined;
			};
		});

		return visibleColumns;
	}

	private updateRowHeightFunction(visibleColumns: Array<ColumnAttribute>) {
		this.gridOptions.getRowHeight = (params: { data: SubEntityObject, node: RowNode }) => {
			Logger.instance.debug('getRowHeight for row %o', params.node.rowIndex);
			let height = WebSubformService.defaultLineHeightInPx;
			visibleColumns.forEach(column => {
				if (column.getPreferredHeight) {
					let cellHeight = column.getPreferredHeight(params.data);
					if (cellHeight && cellHeight > height) {
						height = cellHeight;
					}
				}
			});
			return height;
		};
	}

	private storeFilterPreferences(model: any, savePreferences = true) {
		this.$log.info('current filter model', model);
		this.columnsPref?.content.columns.forEach((columnPrefs) => {
			let keyName = columnPrefs.boAttrId;

			if (model.hasOwnProperty(keyName)) {
				let filterObject = model[keyName];
				this.$log.info('updating filter prefs for ' + keyName);

				if (filterObject['filterType'] === 'boolean') {
					if (filterObject['value'] === 'all') {
						// reset filter on prefs
						columnPrefs.filterOp = undefined;
						columnPrefs.filterValueBoolean = undefined;
					} else {
						columnPrefs.filterOp = 'EQUAL';
						columnPrefs.filterValueBoolean = filterObject['value'];
					}
				} else if (filterObject['filterType'] === 'number') {
					if (filterObject['type'] === 'equals') {
						columnPrefs.filterOp = 'EQUAL';
					} else if (filterObject['type'] === 'notEqual') {
						columnPrefs.filterOp = 'NOT_EQUAL';
					} else if (filterObject['type'] === 'greaterThan') {
						columnPrefs.filterOp = 'GREATER';
					} else if (filterObject['type'] === 'lessThan') {
						columnPrefs.filterOp = 'LESS';
					} else if (filterObject['type'] === 'greaterThanOrEqual') {
						columnPrefs.filterOp = 'GREATER_OR_EQUAL';
					} else if (filterObject['type'] === 'lessThanOrEqual') {
						columnPrefs.filterOp = 'LESS_OR_EQUAL';
					}
					columnPrefs.filterValueDouble = filterObject['filter'].toString();
				} else if (filterObject['filterType'] === 'text') {
					if (!filterObject.hasOwnProperty('condition1') && !filterObject.hasOwnProperty('condition2')) {
						if (filterObject['type'] === 'equals') {
							columnPrefs.filterOp = 'EQUAL';
						} else if (filterObject['type'] === 'notEqual') {
							columnPrefs.filterOp = 'NOT_EQUAL';
						} else if (filterObject['type'] === 'contains') {
							columnPrefs.filterOp = 'LIKE';
						} else if (filterObject['type'] === 'notContains') {
							columnPrefs.filterOp = 'NOT_LIKE';
						}
						columnPrefs.filterValueString = filterObject['filter'];
					} else {
						// only save condition1 (contains) AND condition2 (contains) other is not supported
						if (filterObject['operator'] === 'AND' &&
							filterObject['condition1']['type'] === 'contains' && filterObject['condition2']['type'] === 'contains') {
							columnPrefs.filterOp = 'LIKE';
							columnPrefs.filterValueString = filterObject['condition1']['filter'] + '*' +
								filterObject['condition2']['filter'];
						}
					}
				} else if (filterObject['filterType'] === 'date') {
					if (filterObject['type'] !== 'inRange') {
						if (filterObject['type'] === 'equals') {
							columnPrefs.filterOp = 'EQUAL';
						} else if (filterObject['type'] === 'notEqual') {
							columnPrefs.filterOp = 'NOT_EQUAL';
						} else if (filterObject['type'] === 'greaterThan') {
							columnPrefs.filterOp = 'GREATER';
						} else if (filterObject['type'] === 'lessThan') {
							columnPrefs.filterOp = 'LESS';
						}
						columnPrefs.filterValueDate = filterObject['dateFrom'].toString();
					} else {
						columnPrefs.filterOp = 'BETWEEN';
						columnPrefs.filterValueDate = filterObject['dateFrom'].toString().split(' ')[0]
							+ ';' + filterObject['dateTo'.toString()].split(' ')[0];
					}
				}
			} else {
				columnPrefs.filterOp = undefined;
				columnPrefs.filterValueDouble = undefined;
				columnPrefs.filterValueString = undefined;
				columnPrefs.filterValueBoolean = undefined;
				columnPrefs.filterValueDate = undefined;
			}
		});
		if (savePreferences && this.columnsPref) {
			this.selectableService.savePreference(this.columnsPref).pipe(take(1)).subscribe();
		}
		if (this.selectedFilter && this.columnsPref) {
			let contentDiffers = !DependentsSearchfilterService.isFilterEqual(this.columnsPref, this.selectedFilter);
			this.selectedFilter.content.columns = ObjectUtils.cloneDeep(this.columnsPref.content.columns);
			this.selectedFilter.dirty = contentDiffers;
			this.dependentSearchService.putDependentFilterWithoutSave(this.selectedFilter, this.getSubformFilterPrefId());
		}
	}

	private restoreFilterPreferences() {
		this.columnsPref?.content.columns.forEach((value: ColumnAttribute) => {
			let filterModel = {};
			if (value.filterOp !== undefined) {
				if (value.filterOp === 'EQUAL' || value.filterOp === 'NOT_EQUAL') {
					const typeF = value.filterOp === 'EQUAL' ? 'equals' : 'notEqual';
					if (value.filterValueString !== undefined) {
						filterModel = {
							type: typeF,
							filter: value.filterValueString
						};
					} else if (value.filterValueBoolean !== undefined) {
						filterModel = {
							value: value.filterValueBoolean
						};
					} else if (value.filterValueDouble !== undefined) {
						filterModel = {
							type: typeF,
							filter: this.numberService.format(+value.filterValueDouble),
							filterTo: this.numberService.format(+value.filterValueDouble)
						};
					} else if (value.filterValueDate !== undefined) {
						filterModel = {
							type: typeF,
							dateFrom: value.filterValueDate
						};
					}
				} else if (value.filterOp === 'LIKE' || value.filterOp === 'NOT_LIKE') {
					const typeF = value.filterOp === 'LIKE' ? 'contains' : 'notContains';
					if (value.filterValueString !== undefined) {
						let splittedByStar = value.filterValueString.split('*')
							.filter(v => v !== '');
						if (splittedByStar.length < 2) {
							filterModel = {
								filter: splittedByStar[0],
								type: typeF
							};
						} else {
							// combined
							filterModel = {
								filterType: 'text',
								operator: 'AND',
								condition1: {
									type: typeF,
									filter: splittedByStar[0]
								},
								condition2: {
									type: typeF,
									filter: splittedByStar[1]
								}
							};
						}
					}
				} else if (value.filterOp === 'GREATER') {
					if (value.filterValueDouble !== undefined) {
						filterModel = {
							type: 'greaterThan',
							filter: this.numberService.format(+value.filterValueDouble),
							filterTo: null
						};
					} else if (value.filterValueDate !== undefined) {
						filterModel = {
							type: 'greaterThan',
							dateFrom: value.filterValueDate
						};
					}
				} else if (value.filterOp === 'GREATER_OR_EQUAL') {
					if (value.filterValueDouble !== undefined) {
						filterModel = {
							type: 'greaterThanOrEqual',
							filter: this.numberService.format(+value.filterValueDouble),
							filterTo: null
						};
					}
				} else if (value.filterOp === 'LESS') {
					if (value.filterValueDouble !== undefined) {
						filterModel = {
							type: 'lessThan',
							filter: this.numberService.format(+value.filterValueDouble),
							filterTo: null
						};
					} else if (value.filterValueDate !== undefined) {
						filterModel = {
							type: 'lessThan',
							dateFrom: value.filterValueDate
						};
					}
				} else if (value.filterOp === 'LESS_OR_EQUAL') {
					if (value.filterValueDouble !== undefined) {
						filterModel = {
							type: 'lessThanOrEqual',
							filter: this.numberService.format(+value.filterValueDouble),
							filterTo: null
						};
					}
				} else if (value.filterOp === 'BETWEEN') {
					if (value.filterValueDate !== undefined) {
						filterModel = {
							type: 'inRange',
							dateFrom: value.filterValueDate.split(';')[0],
							dateTo: value.filterValueDate.split(';')[1]
						};
					}
				}
			}
			if (this.gridOptions.api && Object.keys(filterModel).length > 0) {
				this.gridOptions.api.getFilterInstance(value.boAttrId)?.setModel(filterModel);
				this.gridOptions.api.onFilterChanged();
			}
		});
	}

	private createColumnDefinition(column: ColumnAttribute, meta: EntityMeta) {
		let fieldName = this.fqnService.getShortAttributeName(
			this.subformMeta.getBoMetaId(),
			column.boAttrId
		);
		let attribute = meta.getAttributeMeta(fieldName);
		if (!attribute) {
			this.$log.warn('Unknown attribute: %o', fieldName);
			return;
		}

		// lookup if we do have any column definition in layout
		let layoutColumn = this.webComponent.subformColumns?.find(
			element => element?.name === attribute?.getAttributeID()
		);

		let restriction = this.getColumnRestriction(attribute);
		let editable = !restriction;

		let colDef: EntityObjectGridColumn = {
			attributeMeta: attribute,
			headerName: layoutColumn?.label ? layoutColumn.label : attribute.getName(),
			// headerTooltip: attribute.getDescription(), // TooltipModule is used instead
			field: fieldName,
			colId: attribute.getAttributeID(),
			width: column.width,
			editable: function (params) {
				return editable && (params.data.eoData.canWrite || params.data.eoData._flag === 'insert');
			},
			valueGetter: this.eoGridService.valueGetter.bind(this.eoGridService),

			// allow access to this.elementRef in AbstractGridComponent
			newValueHandler: (params: NewValueParams) => {
				return this.newValueHandler(params);
			},

			cellEditorParams: {},
			sort: column.sort && column.sort.direction!,
			resizable: true,
			hide: layoutColumn !== undefined ? !layoutColumn.visible : false,
			cellClass: () => {
				if (this.isMultiline(attribute) && this.webComponent.dynamicCellHeightsDefault) {
					return [
						'ag-cell-defaultHeight',
						'ag-cell'
					];
				}
				return 'ag-cell';
			},
			cellStyle: (params: CellClassParams) => {
				let attrColor = !!params.data ? (params.data as SubEntityObject).getAttributeColor(
					(params.colDef as EntityObjectGridColumn).attributeMeta!.getAttributeName()
				) : undefined;
				let useTextColor = !params.node.data.isNew() && !params.node.data.isDirty() && !params.node.data.isMarkedAsDeleted();
				return {
					'line-height': '1.5',
					'background-color': attrColor !== undefined ? attrColor : '',
					'color': attrColor !== undefined && useTextColor ? ColorUtils.calculateTextColorFromBackgroundColor(attrColor) : ''
				};
			},
			floatingFilter: this.filtering,
			filter: IgnoreDirtyTextFilter,
			floatingFilterComponent: 'floatingFilterTextComponent',
			pinned: column.fixed,
			lockPinned: true
		};

		this.addStylesAndClasses(editable, colDef, attribute);
		colDef.headerClass = this.eoGridService.getTextAlignmentClass(attribute);

		// provide data for render component
		colDef.cellRendererParams = {
			nuclosCellRenderParams: {
				editable: editable,
				attrMeta: attribute,
				advancedProperties: layoutColumn?.advancedProperties
			} as NuclosCellRendererParams
		};

		// provide data for editor component
		colDef.cellEditorParams = {
			attrMeta: attribute,
			subformMeta: this.subformMeta,
			subformColumn: layoutColumn,
			advancedProperties: layoutColumn?.advancedProperties,
			fixedHeight: this.isMultiline(attribute)
				? undefined
				: WebSubformService.defaultLineHeightInPx
		} as NuclosCellEditorParams;

		if (attribute.isCalculated() && !this.sortCalculatedAttributes) {
			colDef.sortable = false;
		}
		let rowComparator = new SubformRowComparator(!this.insertOnTop);

		if (restriction === 'hidden') {
			colDef.cellRendererFramework = SubformHiddenRendererComponent;
		} else if (attribute.isBoolean()) {
			colDef.comparator = rowComparator.booleanComparator;
			colDef.cellRendererFramework = SubformBooleanRendererComponent;
			colDef.cellEditorFramework = SubformBooleanEditorComponent;
			colDef.filter = 'filterBooleanComponent';
			colDef.floatingFilterComponent = 'floatingFilterBooleanComponent';
		} else if (attribute.isNumber()) {
			colDef.comparator = rowComparator.numberComparator;
			colDef.cellRendererFramework = SubformNumberRendererComponent;
			colDef.cellEditorFramework = SubformNumberEditorComponent;
			colDef.filter = IgnoreDirtyNumberFilter;
			colDef.floatingFilterComponent = 'floatingFilterNumberComponent';

			colDef.filterParams = {
				suppressAndOrCondition: true,
				allowedCharPattern: this.numberService.getAllowedCharPatternForFilter('number'),
				numberParser: text => this.numberService.parseNumber(text)
			};
			colDef.valueFormatter = params => this.numberService.format(params.value);
		} else if (attribute.isDate() || attribute.isTimestamp()) {
			colDef.cellRendererFramework = SubformDateRendererComponent;
			colDef.cellEditorFramework = SubformDateEditorComponent;
			colDef.cellClass = 'subform-date-cell';
			colDef.filter = IgnoreDirtyDateFilter;
			colDef.floatingFilterComponent = 'floatingFilterDateComponent';
			colDef.filterParams = {
				comparator: (localDate, cellValue) => {
					let dateAsString = cellValue;
					if (dateAsString == null) {
						return -1;
					}
					let dateParts = dateAsString.split('-');
					dateParts[2] = dateParts[2].substr(0, 2);
					let cellDate = new Date(
						Number(dateParts[0]),
						Number(dateParts[1]) - 1,
						Number(dateParts[2])
					);

					if (localDate.getTime() === cellDate.getTime()) {
						return 0;
					}

					if (cellDate < localDate) {
						return -1;
					}

					if (cellDate > localDate) {
						return 1;
					}

					return 0;
				},
				inRangeInclusive: true,
				suppressAndOrCondition: true
			};
		} else if (attribute.isStateIcon()) {
			colDef.sortable = true;
			colDef.cellRendererFramework = SubformStateIconRendererComponent;
			colDef.editable = false;
			colDef.filter = false;
		} else if (attribute.isDocument() || attribute.isImage()) {
			if (attribute.isDocument()) {
				colDef.filter = IgnoreDirtyTextFilter;
				colDef.filterValueGetter = (params) => {
					const attributeID = attribute?.getAttributeID();
					const attributeName = this.fqnService.getShortAttributeName(meta.getEntityClassId(), attributeID);
					if (attributeName) {
						return params.data.eoData.attributes[attributeName].name;
					}
				};
			}
			colDef.comparator = rowComparator.nameComparator;
			colDef.cellRendererFramework = SubformDocumentRendererComponent;
			colDef.editable = false;
			colDef.suppressKeyboardEvent = (params: SuppressKeyboardEventParams) => {
				const e = params.event;
				if (e.code === 'Tab' || e.key === 'Tab') {
					// get focusable children of parent cell
					// @ts-ignore
					let focusableChildrenOfParent = e.srcElement.closest('.ag-cell')
						.querySelectorAll('[href]');

					if (focusableChildrenOfParent.length === 0 ||
						(!e.shiftKey && e.srcElement === focusableChildrenOfParent[focusableChildrenOfParent.length - 1])
						|| (e.shiftKey && e.srcElement === focusableChildrenOfParent[0]) ||
						// @ts-ignore
						(e.shiftKey && e.srcElement.classList.contains('ag-cell'))) {
						return false;
					} // do not suppress
					focusableChildrenOfParent[0].focus();
					return true; // suppress
				}
				return false; // do not suppress by default
			};
		} else if (attribute.isEmail() || layoutColumn?.controlType === 'EMAIL') {
			colDef.comparator = rowComparator.nameComparator;
			colDef.cellRendererFramework = SubformEmailRendererComponent;
			colDef.cellEditorFramework = SubformEmailEditorComponent;
		} else if (attribute.isHyperlink() || layoutColumn?.controlType === 'HYPERLINK') {
			colDef.comparator = rowComparator.nameComparator;
			colDef.cellRendererFramework = SubformHyperlinkRendererComponent;
			colDef.cellEditorFramework = SubformHyperlinkEditorComponent;
		} else if (attribute.isReference() || layoutColumn?.controlType === 'COMBOBOX') {
			if (attribute.isReference()) {
				colDef.cellRendererFramework = SubformReferenceRendererComponent;
			}
			colDef.filter = IgnoreDirtyTextFilter;
			// our custom text formatter for reference filtering
			colDef.filterParams = {
				textFormatter: (value: object | undefined): string => {
					if (value === undefined) {
						return '';
					}
					return value.hasOwnProperty('name') ? value['name'] : value.toString();
				},
				// does follow the agGrid behavior but handles also null and undefined values
				textCustomComparator: (filter, value: string | undefined | null, filterText): boolean => {
					const filterTextLowerCase = filterText.toLowerCase();
					const valueLowerCase = value?.toLowerCase();

					switch (filter) {
						case 'contains':
							return valueLowerCase ? valueLowerCase.indexOf(filterTextLowerCase) >= 0 : false;
						case 'notContains':
							return valueLowerCase ? valueLowerCase.indexOf(filterTextLowerCase) === -1 : true;
						case 'equals':
							return valueLowerCase === filterTextLowerCase;
						case 'notEqual':
							return valueLowerCase !== filterTextLowerCase;
						case 'startsWith':
							return valueLowerCase?.indexOf(filterTextLowerCase) === 0;
						case 'endsWith':
							if (valueLowerCase === undefined) {
								return false;
							}
							const index = valueLowerCase.lastIndexOf(filterTextLowerCase);
							return index >= 0 && index === (valueLowerCase.length - filterTextLowerCase.length);
						default:
							return false;
					}
				}
			};
			if (attribute.isState()) {
				// the state can't be changed in subform
				colDef.editable = false;
			} else {
				colDef.comparator = rowComparator.nameComparator;
				if (attribute.getComponentType() === 'Combobox' || layoutColumn?.controlType === 'COMBOBOX') {
					colDef.cellEditorFramework = SubformComboboxEditorComponent;

					if (attribute.getType() === 'String' && !attribute.isReference()) {
						colDef.comparator = rowComparator.textComparator;
					}
				} else {
					colDef.cellEditorFramework = SubformLovEditorComponent;
				}
			}
		} else if (this.isMultiline(attribute)) {
			colDef.cellEditorFramework = SubformMultilineEditorComponent;
			colDef.cellRenderer = this.htmlRenderer.bind(this);
			colDef.comparator = rowComparator.textComparator;
		} else {
			colDef.cellEditorFramework = SubformStringEditorComponent;
			colDef.comparator = rowComparator.textComparator;
		}

		if (layoutColumn?.valuelistProvider || layoutColumn?.controlType === 'COMBOBOX') {
			let entityAttrMeta: EntityAttrMeta | undefined = this.subformMeta.getAttributeMetaByFqn(attribute.getAttributeID());
			colDef.floatingFilterComponent = 'filterLovComponent';
			colDef.filterParams = {
				valuelistProvider: layoutColumn.valuelistProvider,
				entityAttrMeta: entityAttrMeta,
				meta: this.subformMeta,
				...colDef.filterParams
			};
		}
		if (colDef.cellRenderer === 'agTextColumnFilter') {
			colDef.filterParams = {
				cssClass: ' form-control form-control-sm',
				...colDef.filterParams
			};
		}
		// add custom CellEditor/CellRenderer if configured
		if (layoutColumn) {
			const customCellRenderer = this.webSubformService.getCustomCellRenderer(
				layoutColumn.advancedProperties
			);
			if (customCellRenderer) {
				colDef.cellRenderer = undefined;
				colDef.cellRendererFramework = customCellRenderer;
			}
			const customCellEditor = this.webSubformService.getCustomCellEditor(
				layoutColumn.advancedProperties
			);
			if (customCellEditor) {
				colDef.cellEditor = undefined;
				colDef.cellEditorFramework = customCellEditor;
			}
		}

		this.addVlpToColumnDefinition(attribute, colDef);

		if (!colDef.comparator) {
			colDef.comparator = rowComparator.textComparator;
		}

		return colDef;
	}

	private htmlRenderer(params): string {
		if (params.value !== null && params.value) {
			const safeHtml = this.htmlSanitizerPipe.transform(params.value, this.webComponent.dynamicCellHeightsDefault);
			this.$log.debug('rendering safeHtml from %o to %o', params.value, safeHtml);
			return safeHtml !== undefined ? safeHtml['changingThisBreaksApplicationSecurity'] : '';
		}
		return '';
	}

	private filterVisibleColumns(columns: ColumnAttribute[], meta: EntityMeta): ColumnAttribute[] {
		return columns.filter(column => {
			if (!column.selected) {
				return false;
			}

			let attr = meta.getAttributeMetaByFqn(column.boAttrId);

			if (!attr) {
				this.$log.warn('Unknown attribute: %o', column.boAttrId);
				return false;
			}

			if (!this.isColumnVisible(attr, meta)) {
				return false;
			}
			return true;
		});
	}

	private isColumnVisible(attr: EntityAttrMeta, meta: EntityMeta): boolean {
		let visible =
			!attr.isHidden() &&
			attr.getAttributeID() !== meta.getRefAttrId() &&
			this.isColumnVisibleByLayout(attr.getAttributeID());
		return visible;
	}

	private getColumnRestriction(attr: EntityAttrMeta, autoNumberField = false): string {
		let roAttributes = this.eo.getSubEOReadOnlyAttributes(
			this.webComponent.foreignkeyfieldToParent
		);
		if (roAttributes !== undefined) {
			let restr = roAttributes[attr.getAttributeID()];
			if (restr) {
				return restr;
			}
		}

		let editable =
			this.isWritable() &&
			!attr.isReadonly() &&
			(autoNumberField || this.isColumnEnabledByLayout(attr.getAttributeID()));
		return editable ? '' : 'ro';
	}

	private subformHasFocus(): boolean {
		return document.activeElement !== null && (this.elementRef.nativeElement === document.activeElement ||
			// look up if we are in subform
			// need to do parent on parentsUntil, cause later does not select nuc-web-subform
			$(document.activeElement)
				.parentsUntil('nuc-web-subform')
				.parent()
				.first()
				.get(0) === this.elementRef.nativeElement);
	}

	private updateColumnsFromLayout() {
		this.columnsFromLayout.clear();
		if (this.webComponent.subformColumns) {
			for (let subformColumn of this.webComponent.subformColumns) {
				this.columnsFromLayout.set(subformColumn.name, subformColumn);
			}
		}
	}

	private addVlpToColumnDefinition(attr: EntityAttrMeta, colDef: ColDef) {
		let subformColumn = this.columnsFromLayout.get(attr.getAttributeID());

		if (subformColumn) {
			let vlp = subformColumn.valuelistProvider;
			if (vlp) {
				// TODO: Use a proper interface here
				colDef.cellEditorParams['valuelistProvider'] = vlp;
			}
		}
	}

	private getEditRowColumnIfLayoutExists(): Observable<ColDef | undefined> {
		return new Observable<ColDef | undefined>(observer => {
			this.getSubformLayoutLink().pipe(
				take(1)
			).subscribe((layout) => {
				if (!layout) {
					this.$log.debug('Subform BO has no default layout');
					observer.next(undefined);
				} else {
					let couldDisplayPopupIcon = !!this.subformMeta.getPopupParameter();

					let editRowIconColDef: ColDef = {
						colId: 'editRowIcon',
						headerName: '',
						cellRendererFramework: SubformEditRowRendererComponent,
						width: couldDisplayPopupIcon ? 40 : 40,
						editable: false,
						pinned: this.subformMeta.getPositionSubformEditColumn(),
						lockPinned: true,
						lockPosition: true,
						suppressMovable: true,
						resizable: true
					};

					editRowIconColDef.cellRendererParams = new RowEditCellParams(
						this.webComponent.advancedProperties
					);
					editRowIconColDef.cellRendererParams.canOpenModal = this.canOpenModal();
					editRowIconColDef.cellRendererParams.subformComponentEnabled = this.webComponent.enabled;

					observer.next(editRowIconColDef);
				}

			});
		});
	}

	private getSubformLayoutLink(): Observable<Link | undefined> {
		return new Observable<Link | undefined>(observer => {
			let detailEntityClassId = this.subformMeta.getDetailEntityClassId();
			if (detailEntityClassId) {
				this.metaService
					.getEntityMeta(detailEntityClassId)
					.pipe(
						take(1)
					).subscribe(detailMeta => {
					observer.next(detailMeta.getLinks().defaultLayout);
				});
			} else {
				observer.next(this.subformMeta.getLinks().defaultLayout);
			}
		});
	}

	private addStylesAndClasses(editable: boolean, colDef: ColDef, attr: EntityAttrMeta) {
		if (this.isWritable()) {
			this.addValidationClasses(attr, colDef);
			if (!editable) {
				this.lightenBackground(colDef);
			}
		}
	}

	private addValidationClasses(attr: EntityAttrMeta, colDef: ColDef) {
		let validator;

		if (attr.isNumber()) {
			validator = _params => this.validationService.isValidForInput(_params.value, 'number');
		}

		if (!colDef.cellClassRules) {
			colDef.cellClassRules = {};
		}

		colDef.cellClassRules['nuc-validation'] = () => true;
		colDef.cellClassRules['nuc-validation-valid'] = params =>
			new SubformCellValidator(params, validator).hasClass('nuc-validation-valid');
		colDef.cellClassRules['nuc-validation-missing'] = params =>
			new SubformCellValidator(params, validator).hasClass('nuc-validation-missing');
		colDef.cellClassRules['nuc-validation-invalid'] = params =>
			new SubformCellValidator(params, validator).hasClass('nuc-validation-invalid');
	}

	private addStatusClasses(colDef: ColDef) {
		colDef.cellClassRules = {...colDef.cellClassRules, ...this.getDefaultCellClasses()};
	}

	private lightenBackground(colDef: ColDef) {
		if (!colDef.cellClassRules) {
			colDef.cellClassRules = {};
		}
		colDef.cellClassRules['row-uneditable'] = () => true;

		colDef.cellStyle = {
			'line-height': '1.5'
		};
	}

	private subscribeToColumnChanges() {
		// save column layout when column order, width or sort order was changed
		// TODO sort order
		if (this.columnLayoutChangedSubscription) {
			this.columnLayoutChangedSubscription.unsubscribe();
		}

		this.columnLayoutChangedSubscription = this.columnLayoutChanged
			.asObservable()
			.pipe(
				debounceTime(500),
				distinctUntilChanged(),
				skip(1) // TODO: Prevent the initial event instead of skipping it here
			)
			.subscribe(() => {
				this.storeSubformColumnLayout();
				this.updateRowHeightFunction(this.getVisibleColumns());
				this.resetRowHeight();
			});
	}

	private storeSubformColumnLayout(): void {
		if (
			!this.authenticationService.isActionAllowed(
				UserAction.WorkspaceCustomizeEntityAndSubFormColumn
			)
		) {
			return;
		}

		if (this.gridOptions.columnApi && this.gridOptions.api) {
			let sortAttributes = this.getSortAttributes();
			this.sortModel = new SortModel(sortAttributes);

			// column order and column width
			let subformColumns: SubformColumn[] = this.gridOptions.columnApi
				.getColumnState()
				.filter(col => col?.hide !== true) // do not save hidden columns as they are dragged out
				.filter(col => this.fqnService.getShortAttributeName(
						this.subformMeta.getBoMetaId(),
						col.colId
					)
				)
				.map(cs => {
					let fieldName = this.fqnService.getShortAttributeName(
						this.subformMeta.getBoMetaId(),
						cs.colId
					);
					return {
						eoAttrFqn: cs.colId,
						name: this.subformMeta.getAttribute(fieldName!).name,
						fieldName: fieldName,
						width: cs.width,
						fixed: cs.pinned
					};
				}) as SubformColumn[];

			// map ag-grid columns to preference columns
			subformColumns = this.gridOptions.columnApi
				.getAllGridColumns()
				.map(c => c.getId())
				.map((colId: string) => {
					return (
						subformColumns.filter(col => col.eoAttrFqn === colId).shift() ||
						({} as SubformColumn)
					);
				});

			// column sort order
			sortAttributes.forEach(col => {
				let subformColumn = subformColumns.filter(sc => sc.eoAttrFqn === col.colId).shift();
				if (subformColumn) {
					subformColumn.sort = {
						direction: col.sort,
						prio: col.sortIndex
					};
				}
			});

			let columnPref = this.selectedColumnPref$.getValue();

			if (columnPref) {
				let position = 0;
				columnPref.content.columns = subformColumns
					.filter(col => col.eoAttrFqn)
					.map(gc => {
						return {
							boAttrId: gc.eoAttrFqn,
							name: gc['name'], // TODO refactor column interfaces
							width: gc.width,
							position: position++,
							selected: true,
							sort: gc.sort,
							fixed: gc.fixed
						} as SelectedAttribute;
					});

				if (this.gridOptions.api) {
					this.storeFilterPreferences(this.gridOptions.api.getFilterModel(), false);
				}

				this.webSubformService.evictSubformColumnLayoutInCache(
					this.eo.getEntityClassId(),
					this.subformMeta.getBoMetaId(),
					{columns: subformColumns}
				);

				columnPref.layoutId = this.eo.getLayoutId();
				this.selectableService.savePreference(columnPref).pipe(take(1)).subscribe();
			}
		}
	}

	private getSortAttributes(): SortAttribute[] {
		let sortAttributes = this.gridOptions.columnApi?.getColumnState().filter(
			c => c.sort !== null
		).map(value => {
			return {
				colId: value.colId,
				sort: value.sort,
				sortIndex: value.sortIndex !== undefined ? value.sortIndex! + 1 : undefined
			} as SortAttribute;
		});

		return sortAttributes ? sortAttributes : [];
	}

	private createSubEo(): Observable<SubEntityObject | undefined> {
		let parentForNewSubEo = this.getParentForNewSubEo();
		if (!parentForNewSubEo) {
			// Currently only work for a single parent
			return of(undefined);
		}
		return this.entityObjectService
			.createNew(this.subformMeta.getBoMetaId(), parentForNewSubEo)
			.pipe(map((eo: EntityObject) => this.prepareNewSubEo(eo)));
	}

	private transferClonableAttributes(sourceSubEo: SubEntityObject, clone: SubEntityObject) {
		for (let attrKey of Object.keys(sourceSubEo.getAttributes())) {
			let attributeMeta = this.subformMeta.getAttributeMeta(attrKey);

			if (!attributeMeta) {
				this.$log.warn('No attribute meta for %o', attrKey);
				continue;
			}

			if (!this.isCloneable(attributeMeta.getAttributeID())) {
				continue;
			}

			// set attribute unsafe - without triggering layoutml rules
			if (
				!attributeMeta.isAutonumber() &&
				!attributeMeta.isDocument() &&
				(!attributeMeta.isSystemAttribute() || attributeMeta.isProcess())
			) {
				clone.setAttributeUnsafe(attrKey, sourceSubEo.getAttribute(attrKey));
			} else if (attributeMeta.isDocument() && sourceSubEo.getAttribute(attrKey)?.id) {
				let documentValue = {
					uploadToken: 'clone_' + sourceSubEo.getAttribute(attrKey)?.id,
					name: sourceSubEo.getAttribute(attrKey)?.name
				};
				clone.setAttributeUnsafe(attrKey, documentValue);
			}
		}
		// Update VLP params for a clone... (NUCLOS-7015)
		this.ruleService.updateRuleExecutor(clone, this.eo).pipe(take(1)).subscribe();
	}

	private updateSubEoSelectionInRoot() {
		this.eo.setSubEosSelected(this.getReferenceAttributeId(), this.getSelectedSubEOs());
		this.storeSelectedSubEos(this.getReferenceAttributeId(), this.getSelectedSubEOs());
	}

	/**
	 * Returns true if this subform has a parent subform.
	 */
	private isSubSubform() {
		return this.webComponent.parentSubform !== undefined;
	}

	private getPreferredHeight(eo: SubEntityObject, attribute: EntityAttrMeta, width = 0) {
		if (this.isMultiline(attribute) && this.webComponent.dynamicCellHeightsDefault) {
			let value: string | undefined = this.getAttributeText(eo.getAttribute(attribute.getAttributeID()), attribute);

			let safeHtml = this.htmlSanitizerPipe.transform(value, this.webComponent.dynamicCellHeightsDefault);
			value = safeHtml !== undefined ? safeHtml['changingThisBreaksApplicationSecurity'] : '';
			let styles = this.webComponent.dynamicCellHeightsDefault ? [
				'white-space:normal',
				'word-break:normal',
				'overflow-wrap: break-word'
			] : [];
			return WebSubformService.calculateCellHeightForText(value, [
				'form-control',
				'form-control-sm'
			], styles, width)?.height;
		}

		return undefined;
	}

	private getAttributeText(attribute, attrMeta: EntityAttrMeta): string {
		if (attribute) {
			if (attrMeta.isNumber()) {
				let formattedValue = this.numberService.format(
					attribute,
					attrMeta.getPrecision()
				);
				return formattedValue;
			} else if (attrMeta.isReference()) {
				return attribute.name;
			}
		}
		return attribute;
	}

	private isMultiline(subformAttributeMeta: EntityAttrMeta | undefined) {
		if (!subformAttributeMeta) {
			return false;
		}
		return subformAttributeMeta.getType() === 'String' &&
			subformAttributeMeta.getScale() &&
			subformAttributeMeta.getScale()! > 255;
	}

	private getSubformLinks() {
		let subformLinks = this.eo.getSubEoLink(this.webComponent.foreignkeyfieldToParent);

		if (subformLinks === undefined && this.eo.isNew()) {
			subformLinks = {
				boMeta: {
					href: `${this.nuclosConfig.getRestHost()}/boMetas/${this.eo.getEntityClassId()}/subBos/${this.webComponent.foreignkeyfieldToParent}`,
					methods: ['GET']
				},
				bos: {
					href: '',
					methods: undefined
				}
			} as SubEOLinkContainer;
		}

		return subformLinks;
	}

	private isFileDropSupported(): boolean {
		let supported = this.active && !!this.firstFileAttributeColumn;
		this.$log.debug('isFileDropSupported: ' + supported);
		return supported;
	}

	private storeSelectedSubEos(referenceAttribute: string, selectedSubEos: SubEntityObject[]) {
		let jsonSelection = selectedSubEos
			.filter((subEoFilter) => !subEoFilter.isMarkedAsDeleted())
			.map(subEo => {
				return {
					id: subEo.getId(),
					shadowID: subEo.shadowID,
					selected: subEo.isSelected()
				};
			});

		this.mpSelectedSubEOs.set(referenceAttribute, jsonSelection);
	}

	private restoreSubformSelection(referenceAttribute: string, subEos: SubEntityObject[] | undefined) {
		let mpEoDependents: any[] | undefined = undefined;
		if (this.eo.getId()) {
			mpEoDependents = this.mpSelectedSubEOs.get(
				referenceAttribute
			);
		}

		if (!mpEoDependents) {
			mpEoDependents = this.mpSelectedSubEOs.get(
				referenceAttribute
			);
		}

		mpEoDependents?.forEach(subEOSelection => {
			let dependentSubEo = subEos?.find((dep) =>
				(subEOSelection?.id !== null && dep?.getId() === subEOSelection.id) || dep?.shadowID === subEOSelection.shadowID
			);

			dependentSubEo?.setSelected(subEOSelection.selected);
		});
	}

	/**
	 * Method responsible for taking a single clipboard entry object and generating a new subform row from it.
	 * @private
	 */
	private pasteStorageRow(entry) {
		this.createSubEo().pipe(take(1)).subscribe((subEo) => {
			const attributes = entry['attributes'];
			if (attributes) {
				this.assignAttributesFromClipboardToRowEo(subEo, attributes);
			}
			if (subEo) {
				this.addSubEO(subEo);
				this.updateGridData();
			}
		});
	}

	private assignAttributesFromClipboardToRowEo(subEo, attributes) {
		for (const key in attributes) {
			if (attributes.hasOwnProperty(key)) {
				const attrUnique = this.subformMeta.getAttributeMetaByFqn(key)?.isUnique();
				const attrReadonly = this.subformMeta.getAttributeMetaByFqn(key)?.isReadonly();
				const isState = this.subformMeta.getAttributeMetaByFqn(key)?.isState();
				if (attrUnique || attrReadonly || isState) {
					continue;
				}
				const value = attributes[key];
				subEo?.setAttribute(key, value);
			}
		}
	}

}
