import { Component, Injectable, Injector, OnDestroy, Type } from '@angular/core';
import { Column, GridOptions, RowNode } from 'ag-grid-community';
import { Observable, Subject } from 'rxjs';
import { AddonService } from '@modules/addons/addon.service';
import { NuclosCache, NuclosCacheService } from '@modules/cache/shared/nuclos-cache.service';
import { Logger } from '@modules/log/shared/logger';
import { takeUntil } from 'rxjs/operators';
import { EntityAttrMeta, EntityMeta } from '../../modules/entity-object-data/shared/bo-view.model';
import {
	EntityObjectClipboardRepresentation,
	SubEntityObject
} from '../../modules/entity-object-data/shared/entity-object.class';
import { DatetimeService } from '../../shared/service/datetime.service';
import { FqnService } from '../../shared/service/fqn.service';
import { LocalStorageService } from '../../shared/service/local-storage.service';
import { NumberService } from '../../shared/service/number.service';
import { StringUtils } from '../../shared/string-utils';
import { SubformLayout } from './web-subform.model';

export const CLIPBOARD_KEY = 'SUBFORM_CLIPBOARD';

@Injectable()
export class WebSubformService implements OnDestroy {
	static defaultLineHeightInPx = 25;
	static cleanUpTimeout: number;

	static calculateCellHeightForText(text, classes, styles: Array<string> = [], width = 0) {
		if (text === undefined || text === null) {
			text = ' ';
		}

		if (this.cleanUpTimeout) {
			clearTimeout(this.cleanUpTimeout);
		}

		if (text.replace !== undefined) {
			text = (text as string).replace(/\n/g, '<br />');
		}

		classes = classes || [];

		if (typeof text !== 'string') {
			return undefined;
		}

		// append only once, use innerHtml instead to avoid "recalculate style"
		let elExists = true;
		let div = document.getElementById('div-calculateCellHeightForText') as HTMLDivElement;
		if (!div) {
			elExists = false;
			div = document.createElement('div');
			div.id = 'div-calculateCellHeightForText';
			$(div)
				.css('position', 'absolute')
				.css('visibility', 'hidden')
				.css('height', 'auto')
				.css('white-space', 'normal')
				.css('padding-left', '11px')
				.css('padding-right', '11px')
				.css('font-size', '0.6875rem');
		}

		div.setAttribute('class', classes.join(' '));

		$(div).css('width', width === 0 ? 'auto' : (width + 'px'));

		if (styles && styles.length > 0) {
			styles.forEach((value) => {
				let split = value.split(':');
				if (split.length === 2) {
					$(div)
						.css(split[0], split[1]);
				}
			});
		}

		if (elExists) {
			div.innerHTML = text;
		} else {
			div.innerHTML = text;
			document.body.appendChild(div);
		}
		this.cleanUpTimeout = window.setTimeout(WebSubformService.cleanUp, 500);

		let dimensions = {
			width: jQuery(div).outerWidth(),
			height: jQuery(div).outerHeight()
		};

		Logger.instance.debug(
			'calculated height for classes %o of text %o with width %o = %o',
			classes,
			text,
			width,
			dimensions
		);

		return dimensions;
	}

	private static cleanUp() {
		let div = document.getElementById('div-calculateCellHeightForText') as HTMLDivElement;
		if (div) {
			document.body.removeChild(div);
		}
	}

	private unsubscribe$ = new Subject<boolean>();
	private subformLayoutCache: NuclosCache;
	private clipboardSubformId;

	constructor(
		private cacheService: NuclosCacheService,
		private addonService: AddonService,
		private datetimeService: DatetimeService,
		private numberService: NumberService,
		private fqnService: FqnService,
		private localStorageService: LocalStorageService,
		private injector: Injector
	) {
		this.subformLayoutCache = this.cacheService.getCache('subformLayoutCache');
		this.refreshClipboardSubformId();
		this.localStorageService.observeItem(CLIPBOARD_KEY)
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => this.refreshClipboardSubformId());
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	refreshClipboardSubformId() {
		const parsedContent = JSON.parse(this.localStorageService.getItem(CLIPBOARD_KEY));
		if (Array.isArray(parsedContent) && parsedContent.length > 0 && parsedContent[0]['boMetaId']) {
			this.clipboardSubformId = parsedContent[0]['boMetaId'];
		} else {
			this.clipboardSubformId = '';
		}
	}

	evictSubformColumnLayoutInCache(
		mainEntityUid: string,
		subformEoMetaId: string,
		subformLayout: SubformLayout
	) {
		let cacheKey = SubformLayout.getCacheKey(mainEntityUid, subformEoMetaId);

		this.subformLayoutCache.delete(cacheKey);

		this.subformLayoutCache.get(
			cacheKey,
			new Observable<SubformLayout>(observer => {
				observer.next(subformLayout);
				observer.complete();
			})
		);
	}

	getStorageContentSubformId() {
		return this.clipboardSubformId;
	}

	copySelectedRowsToStorage(selectedSubEOs: SubEntityObject[]): void {
		const dataToPersist = selectedSubEOs
			.map(subEo => subEo.getData())
			.filter(data => !!data)
			.map(data => new EntityObjectClipboardRepresentation(data!.boMetaId, data!.attributes ?? undefined));
		const stringContent = JSON.stringify(dataToPersist);
		this.localStorageService.setItem(CLIPBOARD_KEY, stringContent);
		if (selectedSubEOs.length > 0) {
			this.clipboardSubformId = selectedSubEOs[0].getData()?.boMetaId ?? '';
		} else {
			this.clipboardSubformId = '';
		}
	}

	copySelectedRowsToClipboard(gridOptions: GridOptions, subformMeta: EntityMeta) {
		const selectedRowNodes: RowNode[] | undefined = gridOptions.api?.getSelectedNodes();
		if (!selectedRowNodes || selectedRowNodes.length === 0) {
			return;
		}

		const rows = selectedRowNodes.map(rowNode => this.getRowAsText(rowNode, gridOptions, subformMeta));
		const resultString = rows.join('\n');

		StringUtils.copyToClipboard(resultString);
	}

	/**
	 * a custom CellRenderer can be configured as an advanced property of a subform column with the key 'cellRenderer'
	 */
	public getCustomCellRenderer(
		advancedProperties: WebAdvancedProperty[]
	): Type<Component> | undefined {
		return this.getAddonComponent(advancedProperties, 'cellRenderer');
	}

	/**
	 * a custom CellEditor can be configured as an advanced property of a subform column with the key 'cellEditor'
	 */
	public getCustomCellEditor(
		advancedProperties: WebAdvancedProperty[]
	): Type<Component> | undefined {
		return this.getAddonComponent(advancedProperties, 'cellEditor');
	}

	private getAddonComponent(
		advancedProperties: WebAdvancedProperty[],
		advancedPropertyKey: string
	): Type<Component> | undefined {
		const cellEditor = advancedProperties
			? advancedProperties.find(ap => ap.name === advancedPropertyKey)
			: undefined;
		const cellEditorName = cellEditor ? cellEditor.value : undefined;
		if (cellEditorName) {
			return this.injector.get(cellEditorName);
		}
		return undefined;
	}

	/**
	 * Formats a whole grid row as a string. Column values will be joined by a tabulator char.
	 * @param rowNode the row to format
	 * @param gridOptions ag grid GridOptions
	 * @param subformMeta EntityMeta of the subform
	 * @private
	 */
	private getRowAsText(rowNode: RowNode, gridOptions: GridOptions, subformMeta: EntityMeta): string {
		const rowValues = gridOptions.columnApi?.getAllColumns()
			?.filter(column => this.isCopyAndPasteColumn(column))
			.map(column =>
				this.getColumnAsText(column, rowNode, subformMeta)
			) || [];

		return rowValues.join('\t');
	}

	private isCopyAndPasteColumn(column: Column) {
		const nuclosCellParams = column.getColDef().cellRendererParams;
		const entityAttrMeta: EntityAttrMeta = nuclosCellParams?.nuclosCellRenderParams?.attrMeta;

		return nuclosCellParams && entityAttrMeta;
	}

	/**
	 * Converts the value at the specified column & row position and formats it as text
	 * @param column the specific column to format
	 * @param rowNode the specific row to look up the column value
	 * @param subformMeta EntityMeta of the subform
	 * @private
	 */
	private getColumnAsText(column: Column, rowNode: RowNode, subformMeta: EntityMeta): string {
		const nuclosCellParams = column.getColDef().cellRendererParams;
		const entityAttrMeta: EntityAttrMeta = nuclosCellParams?.nuclosCellRenderParams?.attrMeta;

		if (!nuclosCellParams || !entityAttrMeta) {
			return '';
		}

		Object.setPrototypeOf(entityAttrMeta, EntityAttrMeta.prototype);
		const attrName = this.fqnService.getShortAttributeName(subformMeta.getEntityClassId(), entityAttrMeta.getAttributeID());
		const value = attrName ? rowNode.data.eoData.attributes[attrName] : undefined;

		return this.formatValue(value, entityAttrMeta);
	}

	/**
	 * Formats a single value according to its EntityAttrMeta
	 * @param value the value to format
	 * @param entityAttrMeta the meta, which defines the formatting
	 * @private
	 */
	private formatValue(value: any, entityAttrMeta: EntityAttrMeta): string {
		if (entityAttrMeta.isReference()) {
			return value?.name || '';
		} else if (entityAttrMeta.isNumber()) {
			return this.numberService.format(value, entityAttrMeta.getPrecision());
		} else if (entityAttrMeta.isDate() || entityAttrMeta.isTimestamp()) {
			return entityAttrMeta.isTimestamp() ?
				this.datetimeService.formatTimestamp(value) :
				this.datetimeService.formatDate(value);
		} else {
			return value || '';
		}
	}
}
