import { AfterContentInit, Component, Injector, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { ColorUtils } from '@shared/color-utils';
import { BehaviorSubject, iif, Observable, of, Subject, Subscription } from 'rxjs';
import { switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import { UnsubscribeNotifier } from '../../data/interfaces/unsubscribe-notifier';
import { EntityMeta } from '../../modules/entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../modules/entity-object-data/shared/entity-object-event.service';
import { EntityObject, SubEntityObject } from '../../modules/entity-object-data/shared/entity-object.class';
import { MetaService } from '../../modules/entity-object-data/shared/meta.service';
import { Logger } from '../../modules/log/shared/logger';
import { NuclosHotkeysService } from '../../shared/service/nuclos-hotkeys.service';
import { AbstractWebComponentDirective } from '../shared/abstract-web-component.directive';

@Component({
	selector: 'nuc-web-tabcontainer',
	templateUrl: './web-tabcontainer.component.html',
	styleUrls: ['./web-tabcontainer.component.css']
})
export class WebTabcontainerComponent extends AbstractWebComponentDirective<WebTabcontainer>
	implements UnsubscribeNotifier, OnInit, OnChanges, OnDestroy, AfterContentInit {

	public unsubscribe$ = new Subject<boolean>();
	calculateTextColorFromBackgroundColor = ColorUtils.calculateTextColorFromBackgroundColor;
	dynamicHeight;
	visibleTabs;

	private eoListener: IEntityObjectEventListener = {
		afterSave: (entityObject: EntityObject) => {
			this.selectDefaultTabbedPane(entityObject);
			this.visibleTabs = this.getVisibleTabs();
		},
		afterAttributeChange: (entityObject: EntityObject) => { this.updateDynamicTabTitles(entityObject, false); },
		afterReload: (entityObject: EntityObject) => {
			if (entityObject) {
				this.updateDynamicTabTitles(entityObject, true);
			}
		}
	};

	private subscriptions = new Subscription();

	private count: Map<any, BehaviorSubject<number>> = new Map<any, BehaviorSubject<number>>();

	private statefulParent = false;

	constructor(
		injector: Injector,
		private entityObjectEventService: EntityObjectEventService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService,
		private metaService: MetaService,
		protected route: ActivatedRoute,
		private $log: Logger
	) {
		super(injector);

		this.eo.getMeta().pipe(take(1)).subscribe((meta) => {
			this.statefulParent = meta.hasStateModel();
		});
	}

	ngOnInit() {
		this.dynamicHeight = Number.isNaN(this.webComponent.minimumHeight);
		this.addShortcuts();
		this.subscribeToSubEoSelectionChanges();
		this.visibleTabs = this.getVisibleTabs();
	}

	ngAfterContentInit() {
		this.webComponent.selectedIndex = 0;

		this.route.queryParams.pipe(
		).subscribe(params => {
			if (params['selectTab']) {
				let tabsToBeSelected = params['selectTab'];

				if (Array.isArray(tabsToBeSelected)) {
					tabsToBeSelected.forEach((tab) => {
						this.selectTabByName(tab);
					});
				} else {
					this.selectTabByName(tabsToBeSelected);
				}
			}
		});

		this.selectDefaultTabbedPane(this.eo);
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				eoChange.previousValue.removeListener(this.eoListener);
			}
			if (eoChange.currentValue) {
				eoChange.currentValue.addListener(this.eoListener);
			}
			this.count = new Map<string, BehaviorSubject<number>>();

			this.visibleTabs = this.getVisibleTabs();

			this.selectDefaultTabbedPane(this.eo);
			this.subscriptions.unsubscribe();
			// little timeout to avoid that newly created subscriptions get canceled due to previous complete
			setTimeout(() => {
				this.subscribeToSubEoSelectionChanges();
			});
		}
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	updateDynamicTabTitles(eo: EntityObject, recount: boolean): void {
		this.getTabs().forEach((tab) => {
			if (tab && !eo.isNew()) {
				this.getCountForTab(tab, eo, recount)
					.pipe(
						take(1)
					)
					.subscribe((result) => {
						if (result !== -1) {
							this.updateTabTitleForCounter(tab, result);
						} else {
							this.updateTabTitleWithEoAttributeNoCounter(tab);
						}
					});
			} else {
				if (this.findReferenceKeyForSubEO(tab, 'foreignkeyfieldToParent')) {
					this.updateTabTitleForCounter(tab, 0);
				} else {
					this.updateTabTitleWithEoAttributeNoCounter(tab);
				}
			}
		});
	}

	/**
	 * Generates a unique ID for the given Tab.
	 */
	getTabID(tab: WebTab): number {
		return this.hashCode(this.webComponent.name + '.' + tab.title);
	}

	selectTab(index: number) {
		if (this.webComponent) {
			this.webComponent.selectedIndex = index;
		}
	}

	isSelected(index: number) {
		return this.webComponent && this.webComponent.selectedIndex === index;
	}

	getSelectedTab() {
		return this.webComponent && this.webComponent.tabs[this.webComponent.selectedIndex];
	}

	/**
	 * Calculates a simple numeric hash for the given string.
	 *
	 * @see http://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript-jquery
	 */

	/* tslint:disable:no-bitwise */
	hashCode(text: string): number {
		let hash = 0, i, chr, len;

		if (text.length === 0) {
			return hash;
		}

		for (i = 0, len = text.length; i < len; i++) {
			chr = text.charCodeAt(i);
			hash = ((hash << 5) - hash) + chr;
			hash |= 0; // Convert to 32bit integer
		}

		if (hash < 0) {
			hash = -hash;
		}

		return hash;
	}

	layoutChanged() {
		this.entityObjectEventService.emitLayoutUpdatedForEo(this.eo);
	}

	getTabs() {
		return this.webComponent.tabs ? this.webComponent.tabs : [];
	}

	getVisibleTabs() {
		return this.webComponent.tabs ?
			this.webComponent.tabs
				.filter(tab =>
					!tab.visibilityAttribute ||
					this.eo.isNew() ||
					!!this.eo.getAttribute(tab.visibilityAttribute)
				) : [];
	}

	getCountForTab(tab: WebTab, eo: EntityObject, recount = true): Observable<number> {
		const foreignKey = this.findReferenceKeyForSubEO(tab, 'foreignkeyfieldToParent');
		if (foreignKey === undefined) {
			// if no key found abort and return -1
			return of(-1);
		}
		this.$log.debug('Key found for ' + tab.title + ': ' + foreignKey);

		if (!recount) {
			if (!this.count.get(foreignKey)) {
				this.count.set(foreignKey, new BehaviorSubject(-1));
			}

			return this.count.get(foreignKey)!;
		}

		return this.metaService.getSubBoMetaByReferenceAttribute(eo.getEntityClassId(), foreignKey).pipe(
			take(1),
			switchMap((meta: EntityMeta) =>
				iif(() => meta.showTabRowCount() && !((eo instanceof SubEntityObject) && this.statefulParent),
					eo.getDependents(foreignKey).getTotalCount(),
					of(-1)
				)
			),
			tap(count => {
				if (!this.count.get(foreignKey)) {
					this.count.set(foreignKey, new BehaviorSubject(-1));
				}

				this.count.get(foreignKey)!.next(count);
			})
		);
	}

	private subscribeToSubEoSelectionChanges() {
		/**
		 * This is a bit fuzzing here, we need to crawl through tabs content
		 * to find the subform and check if it has parentSubform set and if the entity
		 * of this subform does not match the found parentSubform, as this function
		 * crawls through all objects and fields within the tab and can potentially match
		 * the other subform if it's in the same container.
		 *
		 * This seems to work now for sub - sub bo's showing their tab count.
		 */
		const parentSubform = this.webComponent.tabs.length > 0 ?
			this.findReferenceKeyForSubEO(this.webComponent.tabs[0], 'parentSubform') : undefined;
		const entitySubform = this.webComponent.tabs.length > 0 ?
			this.findReferenceKeyForSubEO(this.webComponent.tabs[0], 'entity') : undefined;

		if (parentSubform && (parentSubform !== entitySubform)) {
			this.subscriptions =
				this.eo.getSelectionChangesInParentSubform(parentSubform)
					.subscribe((parentSelection: SubEntityObject[]) => {
						if (parentSelection.length === 1) {
							this.updateDynamicTabTitles(parentSelection[0], true);
						} else {
							this.updateDynamicTabTitles(this.eo, true);
						}
					});
		}
	}

	private addShortcuts() {
		this.nuclosHotkeysService.addShortcut({
			modifier: 'CTRL_ALT', keys: 'T', description:
				this.i18n.getI18n('webclient.shortcut.detail.NextTab')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.selectNextTab();
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'CTRL_ALT_SHIFT', keys: 'T', description:
				this.i18n.getI18n('webclient.shortcut.detail.PreviousTab')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.selectPreviousTab();
		});
	}

	private updateTabTitleWithEoAttributeNoCounter(tab: WebTab) {
		if (tab.titleAttribute) {
			let attributeValue = this.eo.getAttribute(tab.titleAttribute);
			if (attributeValue && attributeValue !== null) {
				tab.title = attributeValue;
			} else {
				tab.title = '';
			}
		}
	}

	private updateTabTitleForCounter(tab: WebTab, result: number) {
		const match = tab.title.match(/(.*)\s\((\d+)\)/);
		if (tab.titleAttribute) {
			let attributeValue = this.eo.getAttribute(tab.titleAttribute);
			if (attributeValue && attributeValue !== null) {
				tab.title = attributeValue;
			} else {
				tab.title = '';
			}
			tab.title = tab.title + ' (' + result + ')';
		} else if (match === null) {
			tab.title = tab.title + ' (' + result + ')';
		} else {
			tab.title = match[1] + ' (' + result + ')';
		}
	}

	private selectDefaultTabbedPane(eo: EntityObject | undefined) {
		if (eo) {
			let tabbedPaneName = eo.getData()?.defaultTabbedPaneName;
			if (tabbedPaneName) {
				this.selectTabByName(tabbedPaneName);
			}
			this.updateDynamicTabTitles(eo, true);
		}
	}

	private selectTabByName(tabbedPaneName: string | undefined) {
		let index = this.webComponent.tabs.findIndex(tab => this.tabMatches(tab.title, tab.internalName, tabbedPaneName));
		if (index >= 0) {
			this.selectTab(index);
		}
	}

	private selectNextTab() {
		const nextTab = this.webComponent.selectedIndex + 1;
		if (nextTab < this.webComponent.tabs.length) {
			this.selectTab(nextTab);
		}
	}

	private selectPreviousTab() {
		const previousTab = this.webComponent.selectedIndex - 1;
		if (previousTab >= 0) {
			this.selectTab(previousTab);
		}
	}

	private findReferenceKeyForSubEO(data, keyName): any | undefined {
		for (const key in data) {
			if (data.hasOwnProperty(key)) {
				const entry = data[key];
				if (key === keyName) {
					return entry;
				}

				if (typeof entry === 'object') {
					const found = this.findReferenceKeyForSubEO(entry, keyName);

					if (found) {
						return found;
					}
				}
			}
		}

		return undefined;
	}

	private tabMatches(title: string, internalName: string, tabbedPaneName: string | undefined) {
		return title === tabbedPaneName || title.match(new RegExp(tabbedPaneName + '\\s\\(\\d+\\)')) || internalName === tabbedPaneName;
	}
}
