import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	Injector,
	OnChanges,
	OnDestroy,
	OnInit,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { Observable, of } from 'rxjs';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { EntityAttrMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { LocalizationDialogComponent } from '@modules/entity-object/localization-dialog/localization-dialog.component';
import { takeWhile, take } from 'rxjs/operators';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';

@Component({
	selector: 'nuc-web-textarea',
	templateUrl: './web-textarea.component.html',
	styleUrls: ['./web-textarea.component.scss']
})
export class WebTextareaComponent extends AbstractInputComponentDirective<WebTextarea> implements OnInit, OnDestroy, OnChanges {
	@ViewChild('textarea') textarea: ElementRef;

	getCaretPosition: Function;
	setFocus: Function;
	isLocalized: boolean | undefined;

	attributeMeta: EntityAttrMeta;
	private alive = true;

	constructor(
		injector: Injector,
		private cdr: ChangeDetectorRef,
		private i18n: NuclosI18nService,
		private dialogService: NuclosDialogService
	) {
		super(injector);
	}

	ngOnChanges(changes: SimpleChanges) {
		super.ngOnChanges(changes);
		this.cdr.detectChanges();
		if (!this.attributeMeta) {
			this.fetchMeta();
		}
	}

	ngOnInit() {
		this.getCaretPosition = this._getCaretPosition.bind(this);
		this.setFocus = this._setFocus.bind(this);
		this.alive = true;
		this.fetchMeta();
	}

	ngOnDestroy() {
		this.alive = false;
	}

	getInputGroupStyle() {
		let style: any = {};
		if (this.webComponent.verticalAlign !== 'stretch') {
			style['height'] = `${this.webComponent.preferredHeight - 2}px`;
			style['min-height'] = style['height'];
		} else {
			style['height'] = '100%';
		}
		return style;
	}

	getRows() {
		let rows = this.webComponent && this.webComponent.rows;

		if (rows > 0) {
			return rows;
		}

		return 2;
	}

	hasTextModules() {
		return (this.webComponent && this.webComponent.hasTextModules) ?? false;
	}

	processKeyInput(event) {
		if (event instanceof KeyboardEvent && event.key !== 'Enter' && event.key !== ' ') {
			return;
		}
		this.editLocalization();
	}

	editLocalization() {
		this.dialogService.custom(
			LocalizationDialogComponent,
			{
				eo: this.eo,
				attributeMeta: this.attributeMeta,
			}, {
				size: 'lg',
			}
		).pipe(
			take(1)
		).subscribe();
	}

	private _getCaretPosition(): Observable<number | undefined> {
		return of(this.textarea.nativeElement.selectionStart).pipe(
			takeWhile(() => this.alive)
		);
	}

	private _setFocus(idx?: number): void {
		this.textarea.nativeElement.focus();
		if (idx) {
			// this.textarea.nativeElement.setSelectionRange(idx, idx + 2);
		}
	}

	private fetchMeta() {
		if (this.eo) {
			this.eo.getAttributeMeta(this.webComponent.name).pipe(take(1)).subscribe(meta => {
				if (meta) {
					this.attributeMeta = meta;
					this.isLocalized = meta && meta.isLocalized();
				}
			});
		}
	}
}
