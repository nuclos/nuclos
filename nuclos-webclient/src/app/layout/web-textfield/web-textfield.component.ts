import { Component, Injector, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { LocalizationDialogComponent } from '@modules/entity-object/localization-dialog/localization-dialog.component';
import { DatetimeService } from '@shared/service/datetime.service';
import { take } from 'rxjs/operators';
import { EntityAttrMeta } from '../../modules/entity-object-data/shared/bo-view.model';
import { NuclosValidationService } from '../../shared/service/nuclos-validation.service';
import { NumberService } from '../../shared/service/number.service';
import { AbstractInputComponentDirective } from '../shared/abstract-input-component.directive';

@Component({
	selector: 'nuc-web-textfield',
	templateUrl: './web-textfield.component.html',
	styleUrls: ['./web-textfield.component.css']
})
export class WebTextfieldComponent extends AbstractInputComponentDirective<WebTextfield>
	implements OnInit, OnChanges {
	isNumber: boolean;
	isLocalized: boolean | undefined;
	maxLength: number | undefined;
	attributeMeta: EntityAttrMeta;
	private valueString;

	constructor(
		injector: Injector,
		private numberService: NumberService,
		private validationService: NuclosValidationService,
		private datetimeService: DatetimeService,
		private i18n: NuclosI18nService,
		private dialogService: NuclosDialogService
	) {
		super(injector);
	}

	ngOnInit() {
		this.fetchMeta();
	}

	ngOnChanges(changes: SimpleChanges) {
		super.ngOnChanges(changes);
		if (!this.attributeMeta) {
			this.fetchMeta();
		}
	}

	getAutoCompleteValue(): string | null {
		if (this.webComponent.autocomplete) {
			return this.webComponent.autocomplete;
		}

		return null;
	}

	getValueString(): string {
		if (this.valueString) {
			return this.valueString;
		} else {
			let value = this.eo && this.eo.getAttribute(this.webComponent.name);
			return this.format(value);
		}
	}

	getStyle(): any {
		let style = super.getStyle();

		if (this.isLocalized) {
			style['text-overflow'] = 'ellipsis';
			delete style['min-width'];
		}

		return style;
	}

	format(value) {
		if (this.attributeMeta) {
			if (this.attributeMeta.isNumber()) {
				value = this.numberService.format(value, this.attributeMeta.getPrecision());
			} else if (this.attributeMeta.isDate()) {
				value = this.datetimeService.formatDate(value);
			} else if (this.attributeMeta.isReference()) {
				if (value) {
					value = value.name;
				} else {
					value = undefined;
				}
			} else if (this.attributeMeta.isBoolean()) {
				value = (value && this.i18n.getI18n('common.yes')) || (!value && this.i18n.getI18n('common.no'));
			}
		}

		return value;
	}

	setValueString(value) {
		this.valueString = value;
		this.updateModel();
	}

	updateModel() {
		let value = this.valueString;

		if (this.attributeMeta && this.attributeMeta.isNumber()) {
			value = this.numberService.parseNumber(value);
		}

		this.eo.setAttribute(this.webComponent.name, value);
	}

	stopEditing() {
		this.valueString = undefined;
	}

	isValid(): boolean {
		let result = true;

		if (this.attributeMeta && this.attributeMeta.isNumber()) {
			let value = this.getModelValue();
			result = this.validationService.isValidForInput(value, 'number');
		}

		return result;
	}

	getCssClasses() {
		let css = this.getValidationCssClasses() || {};
		css.number = this.isNumber;
		return css;
	}

	processKeyInput(event) {
		if (event instanceof KeyboardEvent && event.key !== 'Enter' && event.key !== ' ') {
			return;
		}
		this.editLocalization();
	}

	editLocalization() {
		this.dialogService.custom(
			LocalizationDialogComponent,
			{
				eo: this.eo,
				attributeMeta: this.attributeMeta,
			}, {
				size: 'lg',
			}
		).pipe(
			take(1)
		).subscribe();
	}

	private fetchMeta() {
		if (this.eo) {
			this.eo.getAttributeMeta(this.webComponent.name).pipe(take(1)).subscribe(meta => {
				if (meta) {
					this.attributeMeta = meta;
					this.isNumber = meta && meta.isNumber();
					this.maxLength = meta.getScale() === undefined || this.isNumber ? 255 : meta.getScale();
					this.isLocalized = meta && meta.isLocalized();
				}
			});
		}
	}
}
