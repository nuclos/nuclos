import { Component, Injector, OnInit } from '@angular/core';
import { AbstractWebComponentDirective } from '../shared/abstract-web-component.directive';

@Component({
	selector: 'nuc-web-titled-separator',
	templateUrl: './web-titled-separator.component.html',
	styleUrls: ['./web-titled-separator.component.css']
})
export class WebTitledSeparatorComponent extends AbstractWebComponentDirective<WebTitledSeparator> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

	getTitle() {
		return this.webComponent.title;
	}

}
