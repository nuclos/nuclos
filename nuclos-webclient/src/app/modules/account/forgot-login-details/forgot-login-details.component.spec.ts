import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ForgotLoginDetailsComponent } from './forgot-login-details.component';

xdescribe('ForgotLoginDetailsComponent', () => {
	let component: ForgotLoginDetailsComponent;
	let fixture: ComponentFixture<ForgotLoginDetailsComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ForgotLoginDetailsComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ForgotLoginDetailsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
