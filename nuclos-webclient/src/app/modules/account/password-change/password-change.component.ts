import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { HasMessage } from '../../../shared/has-message';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { SystemParameter, SystemParameters } from '../../../shared/system-parameters';
import { AuthenticationService } from '../../authentication';
import { NuclosAccountService } from '../shared/nuclos-account.service';
import { UserPassword } from './password';

@Component({
	selector: 'nuc-password-change',
	templateUrl: './password-change.component.html',
	styleUrls: ['./password-change.component.css']
})
export class PasswordChangeComponent extends HasMessage implements OnInit, OnDestroy {
	passwordItem: UserPassword;
	showForm;
	showLoginLink = false;

	@ViewChild('oldPassword') oldPassword;

	private unsubscribe$ = new Subject<boolean>();

	private passwordSecurityStrengthDescr = '';

	constructor(
		private accountService: NuclosAccountService,
		private authenticationService: AuthenticationService,
		private nuclosConfig: NuclosConfigService,
		i18n: NuclosI18nService
	) {
		super(i18n);
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.nuclosConfig.getSystemParameters().pipe(take(1))
			.subscribe((params: SystemParameters) => {
				if (params.has(SystemParameter.SECURITY_PASSWORD_STRENGTH_DESCRIPTION)) {
					this.passwordSecurityStrengthDescr =
						params.get(SystemParameter.SECURITY_PASSWORD_STRENGTH_DESCRIPTION);
				}
			});

		let credentialsExpired = this.authenticationService.getCredentialsExpired();
		if (credentialsExpired) {
			this.passwordItem = credentialsExpired;
			this.setMessage(
				'warning',
				'webclient.credentials.expired.title',
				'webclient.credentials.expired.message'
			);
			this.showForm = true;
		} else {
			this.requireLogin();
		}
	}

	changePassword() {
		this.clearMessage();

		let valid = this.validatePassword();
		if (!valid) {
			return;
		}

		this.accountService.changePassword(this.passwordItem)
		.pipe(take(1))
		.subscribe(
			() => {
				this.setMessage('success', 'webclient.user.changepassword.changed', '');
				this.showForm = false;
				if (!this.authenticationService.isLoggedIn()) {
					this.showLoginLink = true;
				}
			},
			error => this.showErrorFromResponse('webclient.account.error', error)
		);
	}

	showErrorFromResponse(titleKey: string, error: HttpErrorResponse) {
		if (error.status === 403) {
			this.showError(titleKey, 'webclient.user.changepassword.passwordwrong');
		} else if (error.status === 406) {
			if (this.passwordSecurityStrengthDescr !== '' && this.passwordSecurityStrengthDescr !== 'null') {
				this.showError(titleKey, this.passwordSecurityStrengthDescr);
			} else {
				this.showError(titleKey, 'webclient.user.changepassword.passwordnotsuitable');
			}
		} else {
			super.showErrorFromResponse(titleKey, error);
		}
	}

	private requireLogin() {
		this.authenticationService.observeLoginStatus()
		.pipe(takeUntil(this.unsubscribe$))
		.subscribe(loggedIn => {
			if (!loggedIn) {
				this.showForm = false;
				return;
			}

			let user = this.authenticationService.getCurrentUser();
			if (user) {
				this.passwordItem = {
					userName: user.username
				};
				this.showForm = true;
				setTimeout(() => {
					this.oldPassword.nativeElement.focus();
				});
			}
		});
	}

	private validatePassword(): boolean {
		if (!this.passwordItem.newPassword) {
			this.showError('webclient.account.error', 'webclient.user.changepassword.empty');
			return false;
		}

		if (this.passwordItem.newPassword !== this.passwordItem.confirmNewPassword) {
			this.showError('webclient.account.error', 'webclient.user.changepassword.nomatch');
			return false;
		}

		return true;
	}
}
