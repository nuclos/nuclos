import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { HasMessage } from '../../../shared/has-message';
import { BusyService } from '../../../shared/service/busy.service';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { SystemParameter } from '../../../shared/system-parameters';
import { NewsService } from '../../news/shared/news.service';
import { AccountData } from '../shared/account-data';
import { NuclosAccountService } from '../shared/nuclos-account.service';

@Component({
	selector: 'nuc-registration',
	templateUrl: './registration.component.html',
	styleUrls: ['./registration.component.css']
})
export class RegistrationComponent extends HasMessage implements OnInit, OnDestroy {

	account: AccountData;

	showForm = true;

	passwordStrengthDescription: string | undefined;

	usernameDescription: string | undefined;

	private privacyPolicy: News | undefined;
	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private accountService: NuclosAccountService,
		private newsService: NewsService,
		private busyService: BusyService,
		private location: Location,
		private configService: NuclosConfigService,
		i18n: NuclosI18nService
	) {
		super(i18n);

		this.account = {};
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.newsService.getPrivacyPolicy()
		.pipe(takeUntil(this.unsubscribe$))
		.subscribe(
			privacyPolicy => this.privacyPolicy = privacyPolicy
		);

		this.configService.getSystemParameters()
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(systemParameters => {
				this.passwordStrengthDescription = systemParameters.get(SystemParameter.SECURITY_PASSWORD_STRENGTH_DESCRIPTION);
				this.usernameDescription = systemParameters.get(SystemParameter.USERNAME_DESCRIPTION);
			});
	}

	onSubmit() {
		if (this.showForm && this.hasDisclaimer() && !this.account.privacyconsent) {
			this.setMessage(
				'danger',
				'webclient.account.privacyconsent2',
				'webclient.account.noprivacyconsent'
			);
			return;
		}

		if (this.showForm && this.account.newPassword !== this.account.newPassword2) {
			this.setMessage(
				'danger',
				'webclient.account.passwordsNotMatching.title',
				'webclient.account.passwordsNotMatching.message'
			);
			return;
		}

		this.busyService.busy(
			this.accountService.create(this.account)
		).pipe(takeUntil(this.unsubscribe$)).subscribe(
				() => {
					this.showSuccessMessage();
					this.showForm = false;
				},
				error => this.showErrorFromResponse('webclient.account.error', error)
			);
	}

	cancel() {
		this.showForm = false;
		this.location.back();
	}

	hasDisclaimer() {
		return this.privacyPolicy;
	}

	showDisclaimer() {
		if (this.privacyPolicy) {
			this.newsService.showNews(this.privacyPolicy);
		}
	}

	private showSuccessMessage() {
		this.setMessage(
			'success',
			'webclient.account.successful.registered',
			'webclient.account.you.get.an.email.to.activate.your.account'
		);
	}
}
