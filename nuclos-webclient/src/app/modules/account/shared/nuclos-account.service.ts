import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BusyService } from '../../../shared/service/busy.service';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { UserPassword } from '../password-change/password';
import { AccountData } from './account-data';

@Injectable()
export class NuclosAccountService {

	constructor(
		private config: NuclosConfigService,
		private busyService: BusyService,
		private http: HttpClient
	) {
	}

	create(account: AccountData): Observable<any> {
		return this.busyService.busy(
			this.http.post(this.config.getRestHost() + '/user/register', account)
		);
	}

	activate(activationCode: any): Observable<any> {
		// Activation is possible without anonymous login... (NUCLOS-5614, NUCLOS-8289)
		return this.busyService.busy(
			this.http.post(
				this.config.getRestHost() + '/user/activate/' + activationCode,
				{}
			)
		);
	}

	changePassword(passwordItem: any) {
		let url = this.config.getRestHost() + '/user/' + passwordItem.userName + '/password';

		return this.busyService.busy(
			this.http.put(
				url, passwordItem
			)
		);
	}

	resetPassword(passwordItem: UserPassword) {
		let url = this.config.getRestHost() + '/user/' + passwordItem.userName + '/passwordReset';

		return this.busyService.busy(
			this.http.post(
				url, passwordItem
			)
		);
	}

	checkResetToken(passwordItem: UserPassword): Observable<any> {
		return this.busyService.busy(
			this.http.get(
				`${this.config.getRestHost()}/user/${passwordItem.userName}/passwordResetCheck/${passwordItem.passwordResetCode}`
			)
		);
	}

	requestUsername(email: string) {
		let url = this.config.getRestHost() + '/user/forgotUsername/' + email;

		return this.busyService.busy(
			this.http.post(url, {})
		);
	}

	requestPasswordReset(username: string) {
		let url = this.config.getRestHost() + '/user/' + username + '/forgotPassword';

		return this.busyService.busy(
			this.http.post(url, {})
		);
	}
}
