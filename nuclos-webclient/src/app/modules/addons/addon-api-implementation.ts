import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import {
	AddonContext,
	DashboardContext,
	DatasourceParams,
	DialogApi,
	DialogButton,
	EntityObjectApi,
	GlobalContext,
	IEntityObject,
	IPreference,
	IPreferenceContent,
	IPreferenceFilter,
	ISubEntityObject,
	LayoutContext,
	MenuApi,
	MenuItem,
	NavigationApi,
	NgbModalOptions,
	PreferencesApi,
	PreferenceTypeName,
	ResultlistContext
} from '@nuclos/nuclos-addon-api';
import { Locale } from '@nuclos/nuclos-addon-api/api/locale';
import { FqnService } from '@shared/service/fqn.service';
import { LocalStorageService } from '@shared/service/local-storage.service';
import { Observable } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { AbstractWebComponentDirective } from '../../layout/shared/abstract-web-component.directive';
import { DatasourceService } from '@shared/service/datasource.service';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { DashboardAddonItem } from '../dashboard/preference-items/dashboard-addon-item';
import { EntityObjectEventService } from '../entity-object-data/shared/entity-object-event.service';
import { EntityObjectNavigationService } from '../entity-object-data/shared/entity-object-navigation.service';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { EntityObject, SubEntityObject } from '../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../entity-object-data/shared/entity-object.service';
import { Logger } from '../log/shared/logger';
import { MenuService } from '../menu/menu.service';
import { Preference, PreferenceContent } from '../preferences/preferences.model';
import { PreferencesService } from '../preferences/preferences.service';
import { SearchfilterService } from '../search/shared/searchfilter.service';
import { AddonService, Property } from './addon.service';

export class EntityObjectApiImplementation implements EntityObjectApi {

	constructor(
		private entityObjectService: EntityObjectService,
		private entityObjectEventService: EntityObjectEventService,
		private entityObjectNavigationService: EntityObjectNavigationService,
		private entityObjectResultService: EntityObjectResultService,
		private fqnService: FqnService,
	) {
	}

	loadEo(entityClassId: string, entityObjectId: number): Observable<IEntityObject> {
		return this.entityObjectService.loadEO(entityClassId, entityObjectId);
	}

	reloadSelectedEo(): Observable<IEntityObject> {
		return new Observable<IEntityObject>(observer => {

			let selectedEo = this.entityObjectResultService.getSelectedEo();
			if (selectedEo) {
				this.entityObjectService.reloadEo(<EntityObject>selectedEo)
				.pipe(take(1))
				.subscribe(eo => {
					this.entityObjectEventService.emitModifiedEo(eo);
					observer.next(eo);
				});
			} else {
				observer.next(undefined);
			}
			observer.complete();
		});
	}



	onEoCreation(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeCreatedEo();
	}

	onEoAdd(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeAddEo();
	}

	onEoModification(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeEoModification();
	}

	onEoSelection(): Observable<IEntityObject | undefined> {
		return this.entityObjectEventService.observeSelectedEo();
	}

	onEoReset(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeResetEo();
	}

	onEoSave(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeSavedEo();
	}

	onEoDelete(): Observable<IEntityObject> {
		return this.entityObjectEventService.observeDeletedEo();
	}

	onEntityClassIdChange(): Observable<string | undefined> {
		return this.entityObjectResultService.observeSelectedEntityClassId();
	}
}

export class NavigationApiImplementation implements NavigationApi {

	constructor(
		private entityObjectNavigationService: EntityObjectNavigationService,
		private entityObjectResultService: EntityObjectResultService,
		private menuService: MenuService,
		private router: Router
	) {
	}

	navigateToEo(entityObject: IEntityObject): void {
		this.entityObjectNavigationService.navigateToEo(entityObject).pipe(
			take(1)
		).subscribe();
	}

	navigateToEoById(entityClassId: string, eoId: number | string): void {
		this.entityObjectNavigationService.navigateToEoById(entityClassId, eoId);
	}

	onEntityClassIdChange(): Observable<string | undefined> {
		return this.entityObjectResultService.observeSelectedEntityClassId();
	}

	navigateViaMenuItem(itemName: string): void {
		this.menuService.getMenuStructure()
			.pipe(
				take(1)
			)
			.subscribe((menuStructure) => {
				let arrayOfMenuItems = [menuStructure, ...this.menuService.getCurrentAddonMenuItems()];
				let foundMenuItem = this.findRecursive(arrayOfMenuItems, itemName);
				if (foundMenuItem) {
					if (foundMenuItem.hasOwnProperty('boMetaId')) {
						this.router.navigate(['/view', foundMenuItem['boMetaId']]);
					} else if (foundMenuItem.href) {
						this.router.navigateByUrl(foundMenuItem.href);
					}
				}
			});
	}

	private findRecursive(array: Array<MenuItem>, value: string): MenuItem | undefined {
		let initialFind = array.find(m => m.name === value);
		let elementsWithChildren = new Array<MenuItem>();
		array.forEach(a => {
			if (a.entries) {
				elementsWithChildren.push(...a.entries);
			}
		});
		if (initialFind) {
			return initialFind;
		} else if (elementsWithChildren && elementsWithChildren.length > 0) {
			let childElements = new Array<MenuItem>();
			elementsWithChildren.forEach(x => {
				childElements.push(x);
			});
			return this.findRecursive(childElements, value);
		} else {
			return undefined;
		}
	}

}

export class DialogApiImplementation implements DialogApi {
	constructor(
		private dialogService: NuclosDialogService
	) {
	}

	alert(title: string, message: string): Promise<any> {
		return this.dialogService.alert({title: title, message: message}).toPromise();
	}


	openDialog(title: string, message: string, buttonOptions: DialogButton[], modalOptions?: NgbModalOptions): void {
		this.dialogService.openDialog(title, message, buttonOptions, modalOptions)
			.pipe(
				take(1)
			).subscribe();
	}

	openEoInModal(eo: IEntityObject): Observable<any> {
		return this.dialogService.openEoInModal(eo);
	}

}

export class PreferencesApiImplementation implements PreferencesApi {

	constructor(
		private preferencesService: PreferencesService
	) {}


	newPreference(
		entityClassId: string,
		app: string,
		name: string,
		type: PreferenceTypeName = 'userdefined',
	): IPreference<IPreferenceContent> {
		let pref: IPreference<any> = new Preference(type, entityClassId);
		pref.name = name;
		pref.app = app;
		return pref;
	}

	getPreference(prefId: string): Observable<IPreference<IPreferenceContent>> {
		return this.preferencesService.getPreference(prefId);
	}

	getPreferences(filter: IPreferenceFilter, useCache?: boolean): Observable<Array<IPreference<PreferenceContent>>> {
		return this.preferencesService.getPreferences(filter, useCache);
	}

	savePreference(preferenceItem: IPreference<any>): Observable<IPreference<any>> {
		return this.preferencesService.savePreferenceItem(preferenceItem);
	}

	deletePreference(preferenceItem: IPreference<any>): Observable<boolean> {
		return this.preferencesService.deletePreferenceItem(preferenceItem);
	}
}

@Injectable()
export abstract class AddonContextImplementation extends AddonContext {

	protected navigation: NavigationApi;
	protected entityobject: EntityObjectApi;
	protected preferences: PreferencesApi;
	protected dialog: DialogApi;
	protected menu: MenuApi;

	constructor(
		protected addonService: AddonService,
		protected nuclosConfigService: NuclosConfigService,
		protected entityObjectService: EntityObjectService,
		protected entityObjectEventService: EntityObjectEventService,
		protected entityObjectResultService: EntityObjectResultService,
		protected entityObjectNavigationService: EntityObjectNavigationService,
		protected eoVLPSerivice: EntityVLPService,
		protected preferencesService: PreferencesService,
		protected datasourceService: DatasourceService,
		protected searchfilterService: SearchfilterService,
		protected dialogService: NuclosDialogService,
		protected menuService: MenuService,
		protected i18n: NuclosI18nService,
		protected $log: Logger,
		protected router: Router,
		protected localStorageService: LocalStorageService,
		protected fqnService: FqnService,
	) {
		super();
		this.navigation = new NavigationApiImplementation(entityObjectNavigationService, entityObjectResultService,
			this.menuService, this.router);
		this.entityobject = new EntityObjectApiImplementation(
			entityObjectService,
			entityObjectEventService,
			entityObjectNavigationService,
			entityObjectResultService,
			fqnService
		);
		this.menu = this.menuService;
		this.preferences = new PreferencesApiImplementation(preferencesService);
		this.dialog = new DialogApiImplementation(dialogService);
	}


	getEntityObjectApi(): EntityObjectApi {
		return this.entityobject;
	}

	getNavigationApi(): NavigationApi {
		return this.navigation;
	}

	getPreferencesApi(): PreferencesApi {
		return this.preferences;
	}

	getDialogApi(): DialogApi {
		return this.dialog;
	}

	getMenuApi(): MenuApi {
		return this.menu;
	}

	getEntityClassId(): string | undefined {
		return this.entityObjectResultService.getSelectedEntityClassId();
	}


	executeDatasource(datasourceId: string, datasourceParams: DatasourceParams, maxRowCount?: number): Observable<object[]> {
		return this.datasourceService.executeDatasource(datasourceId, datasourceParams, maxRowCount);
	}


	abstract getAddonProperty(key: string): string | undefined;


	openDialog(title: string, message: string, buttonOptions: DialogButton[], modalOptions?: NgbModalOptions): void {
		this.dialogService.openDialog(title, message, buttonOptions, modalOptions)
			.pipe(
				take(1)
			).subscribe();
	}


	/*
	TODO fix type safety in nuclos-api:
	'AddonContextImplementation' is not assignable to the same property in base type 'AddonContext'
	getComponentFactoryClass(componentName: string): Type<Component> | undefined {
		return this.addonService.getComponentFactoryClass(componentName);
	}
	 */
	getComponentFactoryClass(componentName: string) {
		return this.addonService.getComponentFactory(componentName) as any;
	}

	getRestHost(): string {
		return this.nuclosConfigService.getRestHost();
	}

	getLocale(): Locale {
		return this.i18n.getCurrentLocale();
	}

}

@Injectable()
export class LayoutContextImplementation extends AddonContextImplementation implements LayoutContext {

	private webComponent: AbstractWebComponentDirective<any>;

	newDependentEo(parentEo: EntityObject, referenceAttributeId: string): Observable<ISubEntityObject> {

		let subformEoMetaId = referenceAttributeId.substring(0, referenceAttributeId.lastIndexOf('_'));
		return this.entityObjectService.createNew(subformEoMetaId, parentEo).pipe(
			map((subEo: SubEntityObject) => new SubEntityObject(
				parentEo,
				referenceAttributeId,
				subEo.getData(),
				this.localStorageService,
				this.fqnService
			)),
			tap((subEO: SubEntityObject) => {
				this.eoVLPSerivice.addEntityObjectMap(subEO);
			}));
	}


	setComponent(webComponent: AbstractWebComponentDirective<any>): void {
		this.webComponent = webComponent;
	}

	getAddonProperty(key: string): string | undefined {

		if (this.webComponent) {
			return this.webComponent.getAdvancedProperty(key);
		}

		return undefined;
	}
}

@Injectable()
export class ResultlistContextImplementation extends AddonContextImplementation implements ResultlistContext {

	private properties: Property[];

	onEntityObjectListUpdate(): Observable<boolean> {
		return this.entityObjectResultService.observeResultListUpdate();
	}

	getEntityObjectList(): IEntityObject[] {
		return this.entityObjectResultService.getResults();
	}

	applyAdditionalResultlistWhereCondition(condition: string | undefined, appIdentifier: string): void {
		let entityClassId = this.getEntityClassId();
		if (entityClassId) {
			this.searchfilterService.applyAdditionalResultlistWhereCondition(condition, entityClassId, appIdentifier);
		} else {
			this.$log.error('Unable to get entity class id.');
		}
	}

	setAddonproperties(properties: Property[]) {
		this.properties = properties;
	}

	getAddonProperty(key: string): string | undefined {
		if (this.properties !== undefined) {
			let property = this.properties.find(p => p.name === key);
			return property ? property.value : undefined;
		}
		return undefined;
	}

}

@Injectable()
export class DashboardContextImplementation extends AddonContextImplementation implements DashboardContext {

	private item: DashboardAddonItem;

	setDashboardAddonItem(item: DashboardAddonItem): void {
		this.item = item;
	}

	getAddonProperty(key: string): string | undefined {

		if (this.item) {
			let prop = this.item.properties.find(p => p.name === key);
			if (prop) {
				return String(prop.value);
			}
		}

		return undefined;
	}
}

@Injectable()
export class GlobalContextImplementation extends AddonContextImplementation implements GlobalContext {
	getAddonProperty(key: string): string | undefined {
		return undefined;
	}
}
