import { NgModule } from '@angular/core';
import {
	DASHBOARD_CONTEXT,
	GlobalContext,
	ISecurityService,
	LAYOUT_CONTEXT,
	RESULTLIST_CONTEXT
} from '@nuclos/nuclos-addon-api';
import { SecurityService } from '../../core/service/security.service';
import {
	DashboardContextImplementation,
	GlobalContextImplementation,
	LayoutContextImplementation,
	ResultlistContextImplementation
} from './addon-api-implementation';
import { AddonExecutorComponent } from './addon-executor/addon-executor.component';
import { AddonService } from './addon.service';

@NgModule({
	exports: [
		AddonExecutorComponent
	],
	declarations: [
		AddonExecutorComponent
	],
	providers: [
		AddonService,
		ResultlistContextImplementation,
		LayoutContextImplementation,
		DashboardContextImplementation,
		GlobalContextImplementation,
		{
			// TODO: Provide ResultlistContext directly - no need for an extra InjectionToken
			provide: RESULTLIST_CONTEXT,
			useExisting: ResultlistContextImplementation
		},
		{
			// TODO: Provide LayoutContext directly - no need for an extra InjectionToken
			provide: LAYOUT_CONTEXT,
			useExisting: LayoutContextImplementation
		},
		{
			// TODO: Provide DashboardContext directly - no need for an extra InjectionToken
			provide: DASHBOARD_CONTEXT,
			useExisting: DashboardContextImplementation
		},
		{
			provide: GlobalContext,
			useExisting: GlobalContextImplementation
		},
		{
			provide: ISecurityService,
			useClass: SecurityService
		}
	]
})
export class AddonModule {
}
