import { HttpClient } from '@angular/common/http';
import {
	ComponentFactory,
	ComponentFactoryResolver,
	ComponentRef,
	Injectable,
	Injector
} from '@angular/core';
import {
	REQUIRED_RESULTLIST_ATTRIBUTES_KEY,
	REQUIRED_RESULTLIST_ATTRIBUTES_VIA_ADDON_PROPERTY_KEY
} from '@nuclos/nuclos-addon-api';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { Logger } from '../log/shared/logger';
import { AddonPosition } from './addon';

export class AddonRegistration {
	constructor(public addonPosition: AddonPosition,
		public entityClassId: string | undefined,
		public addonComponentName: string,
		public properties: Property[]) {
	}
}

@Injectable()
export class AddonService {
	/**
	 * @type {Map<string, string[]>} entityClassId, attributes
	 */
	private requiredResultlistAttributes: Map<string, string[]> = new Map<string, string[]>();

	private addonRegistrations: AddonRegistration[] = [];
	private addonRegistrationsInitialized = false;

	private dashboardAddons: Addon[] = [];

	constructor(
		private injector: Injector,
		private componentFactoryResolver: ComponentFactoryResolver,
		private nuclosConfigService: NuclosConfigService,
		private http: HttpClient,
		private $log: Logger
	) {
		this.nuclosConfigService.getAddonUsages().subscribe(addonUsages => {
			if (addonUsages) {
				this.registerAddons(addonUsages);
			}
		});
	}

	getDashboardAddons(): Addon[] {
		return this.dashboardAddons;
	}

	getAddonComponentRegistrationsForCurrentEntityClass(
		addonPosition: string
	): AddonRegistration[] {
		return this.addonRegistrations
			.filter(reg => reg.addonPosition === addonPosition)
			.filter(
				reg =>
					reg.entityClassId === undefined ||
					window.location.href.indexOf('/' + reg.entityClassId) !== -1
			);
	}

	/**
	 * attribute names which are required by result list addons for a certain entityClassId
	 * @param {string} entityClassId
	 * @return {string[]}
	 */
	getRequiredResultlistAttributeNames(entityClassId: string): string[] {
		return this.requiredResultlistAttributes.get(entityClassId) || [];
	}

	/**
	 * instantiate an addon component with the given name
	 * @param {string} addonComponentName
	 * @param injector
	 * @return {ComponentRef<any>}
	 */
	instantiateComponent(addonComponentName: string, injector): ComponentRef<any> | undefined {
		this.$log.info('Executing addon "%s".', addonComponentName);
		const addOnComponentFactoryClass = this.getComponentFactory(addonComponentName);
		if (addOnComponentFactoryClass) {
			return addOnComponentFactoryClass.create(injector);
		}
		return undefined;
	}

	getComponentFactory(addonComponentName: string): ComponentFactory<unknown> {
		return this.componentFactoryResolver.resolveComponentFactory(
			this.injector.get(addonComponentName)
		);
	}

	private registerAddons(addonUsages: Addonusages) {
		if (this.addonRegistrationsInitialized) {
			return;
		}

		this.addonRegistrationsInitialized = true;

		if (addonUsages !== undefined) {
			let resultlistAddons = addonUsages.resultlists;
			if (resultlistAddons) {
				resultlistAddons.forEach(addon => {
					if (addon.resultlists) {
						addon.resultlists.forEach(addonusageConfig => {
							let position = addonusageConfig.properties
								.filter(prop => prop.name === 'position')
								.map(prop => prop.value)
								.shift() as AddonPosition | undefined;
							if (position) {
								let addonRegistration = new AddonRegistration(
									position,
									addonusageConfig.boMetaId,
									addon.name + 'Component',
									addonusageConfig.properties
								);
								this.$log.info('Addon registered: ', addonRegistration);
								this.addonRegistrations.push(addonRegistration);
							}
						});
					}
				});
			}
			let dashboardAddons = addonUsages.dashboards;
			if (dashboardAddons) {
				this.dashboardAddons = dashboardAddons;
			}

			addonUsages.global.forEach(addon => {
				// instantiate it
				this.instantiateComponent(addon.name + 'Component', this.injector);
			});
		}

		this.registerRequiredResultlistAttributes();
	}

	private registerRequiredResultlistAttributes() {
		this.addonRegistrations.forEach(reg => {
			if (reg.entityClassId) {
				let requiredResultlistAttributesForComponent = this.getRequiredResultlistAttributesForComponent(
					reg.addonComponentName
				);
				if (requiredResultlistAttributesForComponent) {
					requiredResultlistAttributesForComponent.forEach(attr => {
						if (reg.entityClassId) {
							this.registerRequiredResultlistAttribute(reg.entityClassId, attr);
						}
					});
				}
			}
		});
	}

	private registerRequiredResultlistAttribute(entityClassId: string, attribute: string) {
		let requiredResultlistAttributes = this.getRequiredResultlistAttributeNames(entityClassId);
		if (requiredResultlistAttributes.indexOf(attribute) === -1) {
			requiredResultlistAttributes.push(attribute);
		}
		if (requiredResultlistAttributes.length > 0) {
			this.requiredResultlistAttributes.set(entityClassId, requiredResultlistAttributes);
		}
	}

	private getRequiredResultlistAttributesForComponent(addonComponentName: string) {
		let addOnComponentFactoryClass = this.getComponentFactory(addonComponentName);

		// required attributes defined via @RequiredResultlistAttributes
		let requiredAttributes = addOnComponentFactoryClass
			? addOnComponentFactoryClass[REQUIRED_RESULTLIST_ATTRIBUTES_KEY]
			: [];
		requiredAttributes = requiredAttributes || [];

		// required attributes defined via @RequiredResultlistAttributesViaAddonProperties
		let requiredAttributePropertyNames = addOnComponentFactoryClass
			? addOnComponentFactoryClass[REQUIRED_RESULTLIST_ATTRIBUTES_VIA_ADDON_PROPERTY_KEY]
			: [];
		requiredAttributePropertyNames = requiredAttributePropertyNames || [];
		let addonRegistration = this.addonRegistrations
			.filter(reg => reg.addonComponentName === addonComponentName)
			.shift();
		let requiredAttributesViaAddonProperty = [];
		if (addonRegistration) {
			let addonComponentProperties = addonRegistration.properties;
			requiredAttributesViaAddonProperty = requiredAttributePropertyNames
				.map(propertyName => {
					let property = addonComponentProperties
						.filter(p => p.name === propertyName)
						.shift();
					if (property) {
						return property.value;
					}
					this.$log.error('Configuration error. Attribute not found:', propertyName);
					return undefined;
				})
				.filter(name => !!name);
		} else {
			this.$log.error('No addon registration found:', addonComponentName);
		}

		let attributes: string[] = [...requiredAttributesViaAddonProperty, ...requiredAttributes];
		return attributes.filter((a, index) => attributes.indexOf(a) === index); // remove duplicates
	}
}

export interface Addon {
	webAddonId: string;
	name: string;
	resultlists?: AddonusageConfig[];
	propertyDefinitions?: Property[];
}

export interface AddonusageConfig {
	boMetaId: string;
	properties: Property[];
}

export interface Property {
	name: string;
	type?: string;
	value: any;
}

export interface Addonusages {
	resultlists: Addon[];
	dashboards: Addon[];
	global: Addon[];
}
