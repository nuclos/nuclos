import { Component } from '@angular/core';
import { take } from 'rxjs/operators';
import { MaintenanceService } from './maintenance.service';

@Component({
	selector: 'nuc-maintenance',
	templateUrl: './maintenance.component.html',
	styleUrls: ['./maintenance.component.scss']
})
export class MaintenanceComponent {

	maintenanceModeStatus: string;

	constructor(private maintenanceService: MaintenanceService) {
		this.maintenanceService.maintenanceModeStatus().pipe(take(1)).subscribe(status => this.maintenanceModeStatus = status);
	}

	activateMaintenaceMode() {
		this.maintenanceService.enableMaintenanceMode().pipe(take(1)).subscribe(status => this.setMaintenanceMode(status));
	}

	deactivateMaintenaceMode() {
		this.maintenanceService.disableMaintenanceMode().pipe(take(1)).subscribe(status => this.setMaintenanceMode(status));
	}

	private setMaintenanceMode(status: string) {
		this.maintenanceModeStatus = status;
		this.maintenanceService.setMaintenanceCssClass(status === 'on');
	}
}
