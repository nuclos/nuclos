import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { I18nModule } from '../../i18n/i18n.module';
import { MaintenanceComponent } from './maintenance.component';
import { MaintenanceRoutesModule } from './maintenance.routes';
import { MaintenanceService } from './maintenance.service';

@NgModule({
	declarations: [MaintenanceComponent],
	imports: [
		FormsModule,
		CommonModule,
		RouterModule,
		SharedModule,
		I18nModule,
		MaintenanceRoutesModule
	],
	providers: [
		MaintenanceService
	]
})
export class MaintenanceModule {
}
