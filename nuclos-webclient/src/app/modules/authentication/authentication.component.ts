import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ColorUtils } from '@shared/color-utils';
import { ImageUtils } from '@shared/image-utils';
import {
	FORBIDDEN,
	LOCKED,
	PRECONDITION_FAILED,
	SERVICE_UNAVAILABLE,
	TOO_MANY_REQUESTS,
	UNAUTHORIZED,
	NOT_FOUND
} from 'http-status-codes';
import { EMPTY, of } from 'rxjs';

import { catchError, filter, take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../data/classes/subscription-handle.directive';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { NuclosHotkeysService } from '../../shared/service/nuclos-hotkeys.service';
import { SystemParameter } from '../../shared/system-parameters';
import { MaintenanceService } from '../admin/maintenance/maintenance.service';
import { AuthenticationService, LoginData, LoginResponse } from './authentication.service';

export class UiSsoAuthEndpoint implements SsoAuthEndpoint {
	ssoAuthId: string;
	label: string;
	style: any;
	imageIcon?: string;
	faIcon?: string;
}

@Component({
	selector: 'nuc-authentication',
	templateUrl: './authentication.component.html',
	styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent extends SubscriptionHandleDirective implements OnInit, AfterViewInit {
	@Input()
	username = '';

	@Input()
	password: string;

	@Input()
	autologin: boolean;

	@ViewChildren('usernameInput') usernameInput: QueryList<ElementRef>;
	@ViewChildren('passwordInput') passwordInput: QueryList<ElementRef>;

	showRegistrationLink = false;
	showForgotLoginDetailsLink = false;

	loginFormEnabled = true;
	showSso = false;
	showMandatorForm = false;
	showMandatorlessSessionButton = false;
	selectedMandatorId;
	loginInfo: LoginInfo;
	forceBrowserReloadAfterLogin: boolean;

	showDataLanguages = false;
	dataLanguages: DataLanguage[];
	selectedDataLanguageId: string | undefined;

	ssoAuthEndpoints: UiSsoAuthEndpoint[] = [];

	/**
	 * Should be set to an i18n key, e.g. 'webclient.nosession.error.wrongpass'.
	 */
	errorMessage: string | undefined;

	showPasswordBasedLogin = false;
	showRichClientCompleted = false;
	atLeasedOneSsoAuthReceived = false;

	private focused = false;
	private requestAutoLogin = true;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private authenticationService: AuthenticationService,
		private i18nService: NuclosI18nService,
		private maintenanceService: MaintenanceService,
		private configService: NuclosConfigService,
		private nuclosHotkeysService: NuclosHotkeysService
	) {
		super();

		this.route.queryParams
			.pipe(
				filter(params => params.autoLogin !== undefined)
			).subscribe(params => {
				this.requestAutoLogin = params.autoLogin === 'true';
			});
	}

	ngOnInit() {
		if (this.authenticationService.isSsoAuthRequested()) {
			$('body').css('cursor', 'progress');
			this.loginFormEnabled = false;
			let ssoAuthResponse = this.authenticationService.onSsoAuthResponse().getValue();
			if (ssoAuthResponse === undefined) {
				// wait for a SSO response
				this.authenticationService.onSsoAuthResponse().pipe(takeUntil(this.unsubscribe$)).subscribe(response => {
					if (response !== undefined) {
						this.ssoAuthReceived(response);
					}
				});
			} else {
				// SSO request already processed during load
				this.ssoAuthReceived(ssoAuthResponse);
			}

		} else {
			if (this.requestAutoLogin) {
				// initialize username input with username from last login
				let lastLoginUsername = this.authenticationService.getUsername();
				if (lastLoginUsername) {
					this.username = lastLoginUsername;
				}
			}

			// try if we can auto login
			this.authenticationService.autologin(this.requestAutoLogin);
		}


		this.route.params.subscribe(params => {
			this.i18nService.setLocaleString(params['locale']);
		});

		this.i18nService.getLocale().subscribe(locale => {
			this.authenticationService.getDataLanguages(locale.key).subscribe(dataLanguages => {
					this.dataLanguages = dataLanguages;
					this.showDataLanguages = dataLanguages.length > 0;
					if (this.showDataLanguages) {
						this.selectedDataLanguageId = this.authenticationService.getDataLanguageCookie();
					}
				}
			);
		});

		this.configService.getSystemParameters().pipe(take(1)).subscribe(params => {
			this.showPasswordBasedLogin = params.is(SystemParameter.SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED);
			this.showRegistrationLink = params.is(SystemParameter.USER_REGISTRATION_ENABLED);
			this.showForgotLoginDetailsLink = params.is(
				SystemParameter.FORGOT_LOGIN_DETAILS_ENABLED
			);
		});

		this.authenticationService.getSsoAuthEndpoints().pipe(take(1)).subscribe(endpoints => {
			this.configService.getSystemParameters().pipe(take(1)).subscribe(params => {
				this.ssoAuthEndpoints = endpoints.map(ep => {
					let uiEndpoint: UiSsoAuthEndpoint = {
						ssoAuthId: ep.ssoAuthId,
						label: ep.label,
						style: this.getSsoAuthStyle(ep),
						imageIcon: this.getSsoAuthImageIcon(ep),
						faIcon: this.getSsoAuthFaIcon(ep)
					};
					return uiEndpoint;
				});

				let showSsoAuthEndpoints = true;
				if (!params.is(SystemParameter.SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED) &&
					endpoints.length === 1 &&
					!this.authenticationService.isSsoAuthRequested() &&
					!this.authenticationService.isLogOutRequested() &&
					!this.atLeasedOneSsoAuthReceived) {
					// Password based logins are forbidden...
					// Only one SSO service has been configured...
					// And not logged in before...
					// SSO Login is not running...
					// Use this single service directly!
					this.ssoAuth(endpoints[0].ssoAuthId);
				}

				if (showSsoAuthEndpoints) {
					this.updateSsoVisibility();
				}
			});
		});
	}

	ngAfterViewInit() {
		this.usernameInput.changes.subscribe(() => {
			this.setFocus();
		});
		this.passwordInput.changes.subscribe(() => {
			this.setFocus();
		});
	}

	login() {
		if (this.username === undefined || this.username.trim().length === 0) {
			return;
		}
		if (this.showMandatorForm) {
			// already logged in
			this.authenticationService.autoNavigation();
			return;
		}

		this.selectDataLanguage();

		let loginData: LoginData = {
			username: this.username,
			password: this.password,
			locale: this.i18nService.getCurrentLocale().key,
			autologin: this.autologin,
			loginUri: undefined,
			serverUri: this.configService.getNuclosURL(),
			datalanguage: this.selectedDataLanguageId
		};

		this.errorMessage = undefined;
		this.authenticationService
			.login(loginData)
			.pipe(
				catchError((error: HttpErrorResponse) => {
					this.handleLoginError(error, loginData);
					return of(undefined);
				}),
				take(1)
			)
			.subscribe(authenticationData => {
				if (authenticationData) {
					this.handleLoginInfo(authenticationData);
				}
			});
	}

	selectMandator() {
		this.authenticationService.onAuthenticationDataAvailable().pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			if (this.loginInfo.mandators) {
				let mandator = this.loginInfo.mandators
					.filter(m => m.mandatorId === this.selectedMandatorId)
					.shift();
				this.authenticationService.completeLoginWithMandator(this.loginInfo, mandator).pipe(take(1)).subscribe(() => {
					this.navigateHome();
				});
			}
		});
	}

	mandatorlessSession() {
		this.navigateHome();
	}

	selectDataLanguage() {
		if (this.selectedDataLanguageId) {
			this.authenticationService.setDataLanguageCookie(this.selectedDataLanguageId);
		}
	}

	navigateHome() {
		this.authenticationService.navigateHome();

		if (window.location.href.indexOf('/login') === -1) {
			window.location.reload();
		}
	}

	hasSsoAuthEndpoints() {
		return this.getSsoAuthEndpoints().length > 0;
	}

	getSsoAuthEndpoints() {
		return this.ssoAuthEndpoints;
	}

	ssoAuth(ssoAuthId: string) {
		this.selectDataLanguage();
		let ssoAuthEndpoint = this.ssoAuthEndpoints.find(endpoint => endpoint.ssoAuthId === ssoAuthId);
		if (ssoAuthEndpoint !== undefined) {
			this.authenticationService.redirectToSso(ssoAuthEndpoint);
		}
	}

	ssoAuthReceived(response: LoginResponse) {
		this.atLeasedOneSsoAuthReceived = true;
		if (!this.authenticationService.isSsoAuthRequested()) {
			// to prevent double processing
			return;
		}
		this.loginFormEnabled = true;
		if (response.error !== undefined) {
			this.handleLoginError(response.error, undefined, true);
		} else if (response.loginInfo !== undefined) {
			this.handleLoginInfo(response.loginInfo);
		}
		$('body').css('cursor', 'default');
		this.authenticationService.resetSsoAuthRequestAndResponses();
	}

	private setFocus() {
		if (!this.focused &&
			this.usernameInput.length > 0 &&
			this.passwordInput.length > 0) {
			this.focused = true;
			if (this.usernameInput) {
				// focus username respectively password input
				if (this.username.length === 0) {
					this.usernameInput.first.nativeElement.focus();
				} else {
					this.passwordInput.first.nativeElement.focus();
				}
			}

			this.nuclosHotkeysService.addShortcut({
				keys: 'L',
				modifier: 'ALT',
				description: this.i18nService.getI18n('webclient.shortcut.loginFocus')
			}).pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
				this.usernameInput.first.nativeElement.focus();
			});
		}
	}

	private updateSsoVisibility() {
		this.showSso = this.hasSsoAuthEndpoints() && !this.showMandatorForm;
	}

	private getSsoAuthStyle(ssoAuthEndpoint: SsoAuthEndpoint) {
		let style: any = {};
		style.hover = false;
		style.darkColor = '#5E5E5E';
		style.lightColor = '#FFFFFF';
		if (ssoAuthEndpoint.color !== undefined && ssoAuthEndpoint.color !== null) {
			let calculated = ColorUtils.calculateTextColorFromBackgroundColor(ssoAuthEndpoint.color);
			if (calculated !== undefined) {
				if (calculated === '#000000') {
					// calc is black? it is to hard here, only use the lightColor
					style.lightColor = ssoAuthEndpoint.color;
				} else {
					// calc is white
					style.darkColor = ssoAuthEndpoint.color;
				}
			}
		}
		return style;
	}

	private getSsoAuthImageIcon(ssoAuthEndpoint: SsoAuthEndpoint) {
		return ImageUtils.getBase64ImageSrc(ssoAuthEndpoint.imageIcon);
	}

	private getSsoAuthFaIcon(ssoAuthEndpoint: SsoAuthEndpoint) {
		let result = ssoAuthEndpoint.fontAwesomeIcon;
		if (result !== undefined && result !== null && result.length > 0) {
			result = result.toLowerCase();
			if (!result.startsWith('fa-')) {
				result = 'fa-' + result;
			}
			return result;
		}
		return undefined;
	}

	private handleLoginInfo(authenticationData: LoginInfo) {
		this.username = authenticationData.username;
		this.showRichClientCompleted = authenticationData.showRichClientCompleted === true;
		// show mandator selection dialog if configured
		this.showMandatorlessSessionButton = authenticationData.superUser;
		if (authenticationData.mandators) {
			this.loginInfo = authenticationData;

			// check if the last selected mandator (from cookie) still exists
			let lastMandatorLoggedInId = this.authenticationService.getLastMandatorIdCookie();

			if (!lastMandatorLoggedInId) {
				this.selectedMandatorId = authenticationData.mandators[0].mandatorId;
			} else {
				this.selectedMandatorId = lastMandatorLoggedInId;
			}

			if (authenticationData.mandators.length === 1) {
				// only one mandator configured no need for mandator selection
				this.authenticationService
					.completeLoginWithMandator(this.loginInfo, authenticationData.mandators[0])
					.pipe(take(1))
					.subscribe(() => {
						this.authenticationService.autoNavigation();
					});
			} else {
				this.loginFormEnabled = false;
				this.showMandatorForm = true;
				this.updateSsoVisibility();
			}
		} else {
			if (this.showRichClientCompleted) {
				this.authenticationService.autoNavigation();
			}
		}

		if (this.forceBrowserReloadAfterLogin) {
			location.reload();
		}
	}

	private handleLoginError(error: HttpErrorResponse, loginData?: LoginData, isSsoAuth = false) {
		if (error.status === UNAUTHORIZED) {
			if (isSsoAuth) {
				this.errorMessage = 'webclient.nosession.error.ssoFailed';
			} else {
				this.errorMessage = 'webclient.nosession.error.wrongpass';
			}
		} else if (error.status === NOT_FOUND) {
			this.errorMessage = 'webclient.nosession.error.notFound';
		} else if (error.status === TOO_MANY_REQUESTS) {
			this.errorMessage = 'webclient.nosession.error.tooManyRequests';
		} else if (this.isCredentialsExpired(error)) {
			this.authenticationService.setCredentialsExpired({
				userName: `${loginData?.username}`
			});
		} else if (error.error.message === 'nuclos.security.authentication.locked') {
			this.errorMessage = 'nuclos.security.authentication.locked';
		} else if (error.error.message === 'nuclos.security.authentication.accountexpired') {
			this.errorMessage = 'nuclos.security.authentication.accountexpired';
		} else if (error.status === SERVICE_UNAVAILABLE) {
			this.errorMessage = 'webclient.maintenance.mode.nologin';
			this.maintenanceService.setMaintenanceCssClass(true);
			this.forceBrowserReloadAfterLogin = true;
		} else if (error.status === FORBIDDEN) {
			this.errorMessage = 'webclient.nosession.error.forbidden';
		} else if (error.status === LOCKED) {
			this.errorMessage = error.error;
		} else {
			this.errorMessage = 'webclient.nosession.error.nocode';
		}
	}

	private isCredentialsExpired(error: HttpErrorResponse) {
		return (
			error.status === PRECONDITION_FAILED &&
			error.error &&
			error.error.message === 'nuclos.security.authentication.credentialsexpired'
		);
	}

}
