import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RichClientCompletedComponent } from '@modules/authentication/rich-client-completed/rich-client-completed.component';
import { SharedModule } from '../../shared/shared.module';
import { CacheModule } from '../cache/cache.module';
import { HtmlModule } from '../html/html.module';
import { I18nModule } from '../i18n/i18n.module';
import { LogModule } from '../log/log.module';
import { AuthenticationRoutesModule } from './authentication.routes';
import { AuthenticationComponent, AuthenticationService } from './index';
import { LogoutComponent } from './logout/logout.component';
import { SessionInfoComponent } from './session-info/session-info.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		RouterModule,

		SharedModule,

		// Nuclos modules
		LogModule,
		I18nModule,
		CacheModule,

		AuthenticationRoutesModule,
		HtmlModule
	],
	declarations: [
		AuthenticationComponent,
		LogoutComponent,
		SessionInfoComponent,
		RichClientCompletedComponent
	],
	providers: [
		AuthenticationService
	],
	exports: [
	]
})
export class AuthenticationModule {
}
