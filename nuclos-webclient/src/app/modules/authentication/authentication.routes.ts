import { RouterModule, Routes } from '@angular/router';
import { ServerConnectivityGuard } from '@app/guard/server-connectivity-guard';
import { RichClientCompletedComponent } from '@modules/authentication/rich-client-completed/rich-client-completed.component';
import { AuthenticationComponent } from './authentication.component';
import { LogoutComponent } from './logout/logout.component';
import { SessionInfoComponent } from './session-info/session-info.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'session-info',
		data: {
			localizedTitle: 'webclient.sessionInfo'
		},
		canActivate: [ServerConnectivityGuard],
		component: SessionInfoComponent
	},
	{
		path: 'login',
		data: {
			localizedTitle: 'webclient.login'
		},
		canActivate: [ServerConnectivityGuard],
		component: AuthenticationComponent
	},
	{
		path: 'login/richClientCompleted',
		component: RichClientCompletedComponent
	},
	{
		path: 'logout',
		data: {
			localizedTitle: 'webclient.logout'
		},
		component: LogoutComponent
	}
];

export const AuthenticationRoutesModule = RouterModule.forChild(ROUTE_CONFIG);
