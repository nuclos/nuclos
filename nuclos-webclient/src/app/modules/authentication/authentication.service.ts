import { HTTP_INTERCEPTORS, HttpClient, HttpErrorResponse, HttpInterceptor } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { PreferencesService } from '@modules/preferences/preferences.service';
import { UserAction } from '@nuclos/nuclos-addon-api';
import {
	BehaviorSubject,
	EMPTY,
	from,
	Observable,
	of,
	of as observableOf,
	ReplaySubject,
	Subject,
	throwError as observableThrowError
} from 'rxjs';

import { catchError, filter, map, mergeMap, switchMap, take, tap } from 'rxjs/operators';
import { NuclosHttpInterceptor } from '../../core/interceptors/nuclos-http-interceptor';
import { NuclosCookieService } from '../../core/service/nuclos-cookie.service';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import { BusyService } from '../../shared/service/busy.service';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { StringUtils } from '../../shared/string-utils';
import { SystemParameter } from '../../shared/system-parameters';
import { UserPassword } from '../account/password-change/password';
import { NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { Logger } from '../log/shared/logger';
import { User } from './user';

export class LoginData {
	username?: string;
	password?: string;
	locale?: string;
	datalanguage?: string;
	autologin?: boolean;
	loginUri?: string;
	serverUri: string;
}

export class LoginResponse {
	loginInfo?: LoginInfo;
	error?: HttpErrorResponse;
}

/**
 * TODO: Perform only authentication here. Extract permission checks (authorization) and other stuff.
 */
@Injectable()
export class AuthenticationService {
	static instance: AuthenticationService;

	static readonly COOKIE_KEY_REDIRECT = 'redirect';
	static readonly COOKIE_KEY_LAST_MANDATOR = 'lastMandator';
	static readonly COOKIE_KEY_LAST_ENTITY = 'lastEntity';
	static readonly COOKIE_KEY_DATALANGUAGE = 'dataLanguage';

	/**
	 * Determines if it is okay to automatically navigate away from the current URL.
	 */
	static isAutoNavigationAllowed() {
		let location = window.location.href;
		return (
			location.indexOf('/account/register') === -1 &&
			location.indexOf('/account/activate') === -1 &&
			location.indexOf('/account/resetPassword') === -1 &&
			location.indexOf('/account/changePassword') === -1 &&
			location.indexOf('/account/forgotLoginDetails') === -1 &&
			location.indexOf('/login/richClientCompleted') === -1
		);
	}

	private currentUser: User | undefined;
	private allowedActions: Map<string, boolean>;

	private readonly ANONYMOUS_USER_NAME = 'anonymous';

	private loggedIn = false;
	private loginSubject: ReplaySubject<boolean>;
	private logOutRequested = false;

	private anonymousLoginAllowed = false;
	private ldapActive = false;
	private ssoActive = false;

	private authentication: LoginInfo | undefined;

	private dataLanguages: DataLanguage[] | undefined;
	private mandatorSelection: BehaviorSubject<Mandator | undefined>;
	private authenticationDataAvailable: BehaviorSubject<boolean> = new BehaviorSubject(false);
	private selectStartDashboard: BehaviorSubject<boolean> = new BehaviorSubject(true);

	private ssoAuthRequested = false;
	private ssoAuthResponse: BehaviorSubject<LoginResponse | undefined> = new BehaviorSubject(undefined);

	private loginInProgress = false;
	private loginError = false;
	private httpInterceptor: NuclosHttpInterceptor | undefined;
	private externalLink = false;

	/**
	 * If set, forces the user to change his password
	 */
	private credentialsExpired?: UserPassword;

	constructor(
		private router: Router,
		private http: HttpClient,
		@Inject(HTTP_INTERCEPTORS) private interceptors: HttpInterceptor[],
		private cookieService: NuclosCookieService,
		private nuclosConfig: NuclosConfigService,
		private busyService: BusyService,
		private $log: Logger,
		private i18nService: NuclosI18nService,
		private preferenceService: PreferencesService,
		private cacheService: NuclosCacheService
	) {
		this.handleSsoAuthRequestIfAny(window.location.href);

		this.httpInterceptor = this.interceptors.find(x => x instanceof NuclosHttpInterceptor) as NuclosHttpInterceptor;
		AuthenticationService.instance = this;
		this.loginSubject = new ReplaySubject<boolean>(1);

		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				let url = (<NavigationEnd>event).url;
				// Redirect to the login page, if not logged in and the page requires authentication
				if (this.isAuthenticationRequired(url)) {
					this.observeLoginStatus()
						.pipe(take(1))
						.subscribe(loggedIn => this.checkLogin(loggedIn, url));
				}
			}
		});

		this.nuclosConfig.getSystemParameters().subscribe(parameters => {
			this.anonymousLoginAllowed = parameters.is(
				SystemParameter.ANONYMOUS_USER_ACCESS_ENABLED
			);
		});

		this.nuclosConfig.getSystemParameters().subscribe(parameters => {
			this.anonymousLoginAllowed = parameters.is(
				SystemParameter.ANONYMOUS_USER_ACCESS_ENABLED
			);
		});

		this.httpInterceptor.getUnauthorizedRequests().subscribe(() => {
			this.setUnauthorized();
		});

		this.mandatorSelection = new BehaviorSubject(undefined);

		this.observeLoginStatus().subscribe(() => this.cacheService.invalidateAllCaches());

		this.init();
	}

	init() {
		const url = window.location.href;
		if (!this.isSsoAuthRequest(url)) {
			this.removeRedirectCookie();
			this.setRedirectCookie(window.location.hash);
			if (this.getRedirectCookie()) {
				this.externalLink = true;
			}
			this.autologin();
		}
		this.loadAllDataLanguages();
	}

	login(loginData: LoginData): Observable<LoginInfo> {
		this.loginInProgress = true;
		this.removeLastEntityCookie();
		this.credentialsExpired = undefined;

		return this.busyService.busy(
			this.http.post(this.nuclosConfig.getRestHost(), JSON.stringify(loginData), {
				headers: {
					'Content-Type': 'application/json'
				}
			}).pipe(
				tap((data: LoginInfo) => {
					this.loginSuccessful(data, true);
					this.loginInProgress = false;
				}),
				catchError(error => {
					this.$log.debug('Login with data %o failed: ', loginData, error);
					this.loginError = true;
					return observableThrowError(error);
				})
			)
		);
	}

	onAuthenticationDataAvailable(): BehaviorSubject<boolean> {
		return this.authenticationDataAvailable;
	}

	onSelectStartDashboard(): BehaviorSubject<boolean> {
		return this.selectStartDashboard;
	}

	getSsoAuthEndpoints(): Observable<SsoAuthEndpoint[]> {
		const restUri = this.nuclosConfig.getRestHost() + '/ssoAuthEndpoints';
		return this.cacheService.getCache('http.GET').get(
			restUri
			, this.http
				.get<SsoAuthEndpoint[]>(
					restUri
				)) as Observable<SsoAuthEndpoint[]>;
	}

	onSsoAuthResponse(): BehaviorSubject<LoginResponse | undefined> {
		return this.ssoAuthResponse;
	}

	handleSsoAuthRequestIfAny(url: string) {
		if (this.isSsoAuthRequest(url)) {
			let loginData: LoginData = {
				loginUri: url,
				serverUri: this.nuclosConfig.getNuclosURL(),
				datalanguage: this.getDataLanguageCookie()
			};
			this.ssoAuthRequested = true;
			this.login(loginData)
				.pipe(
					catchError((error: HttpErrorResponse) => {
						let loginResponse: LoginResponse = {
							error: error
						};
						this.ssoAuthResponse.next(loginResponse);
						this.redirectToLoginPage();
						return EMPTY;
					}),
					take(1)
				)
				.subscribe(authData => {
					let loginResponse: LoginResponse = {
						loginInfo: authData
					};
					this.ssoAuthResponse.next(loginResponse);
					this.redirectToLoginPage();
				});
		}
	}

	isSsoAuthRequest(url: string) {
		let callbackUrl = document.baseURI;
		if (!callbackUrl.endsWith('/')) {
			callbackUrl += '/';
		}
		callbackUrl += '?'; // url without a path
		return url.startsWith(callbackUrl);
	}

	isSsoAuthRequested(): boolean {
		return this.ssoAuthRequested;
	}

	resetSsoAuthRequestAndResponses(): void {
		this.ssoAuthRequested = false;
		this.ssoAuthResponse.next(undefined);
	}

	getCurrentUser(): User | undefined {
		return this.currentUser;
	}

	/**
	 * Determines if the given Action is allowed for the current user.
	 */
	isActionAllowed(action: UserAction): boolean {
		if (this.isSuperUser()) {
			return true;
		}

		// Check allowed actions if available
		if (this.authenticationDataAvailable.getValue() && this.allowedActions) {
			let actionString = UserAction[action];
			return this.allowedActions[actionString] ?? false;
		}

		return false;
	}

	/**
	 * Determines if the current user is logged in as "anonymous",
	 * based on the given data or the current username.
	 */
	isAnonymous(data?: LoginInfo): boolean {
		let username = (data && data.username) || this.getUsername();

		return (
			StringUtils.equalsIgnoreCase(username, this.ANONYMOUS_USER_NAME) &&
			this.anonymousLoginAllowed
		);
	}

	autoNavigation(): Promise<boolean> {
		if (AuthenticationService.isAutoNavigationAllowed()) {
			let redirect = this.getRedirectCookie();
			if (redirect) {
				this.removeRedirectCookie();
				let externalLink = this.externalLink;
				this.externalLink = false;
				return new Promise<boolean>(resolve => {
					this.preferenceService.getPreferences({
						orLayoutIsNull: true,
						type: ['dashboard']
					}).subscribe(prefs => {
						let startupDefault = prefs.find(pref => pref.startupDefault) && !externalLink;
						if (startupDefault) {
							this.selectStartDashboard.next(true);
							this.navigateHome().then(res => resolve(res));
						} else if (redirect && !window.location.href.endsWith(redirect)) {
							this.$log.debug('Redirecting to: %o', redirect);
							this.router.navigateByUrl(redirect).then(res => resolve(res));
						} else {
							resolve(false);
						}
					});
				});
			} else {
				return this.navigateHome();
			}
		}
		return Promise.resolve(false);
	}

	navigateHome(): Promise<boolean> {
		return this.router.navigate(['dashboard']);
	}

	navigateToLastEntity(): void {
		let redirect = this.getLastEntityCookie();
		if (redirect) {
			this.router.navigate(['/view', redirect]);
		} else {
			this.navigateHome();
		}
	}

	getMandatorSelection(): Subject<Mandator | undefined> {
		return this.mandatorSelection;
	}

	getCurrentMandatorId(): string | undefined {
		let mandatorData = this.mandatorSelection.getValue();
		return mandatorData ? mandatorData.mandatorId : undefined;
	}

	completeLoginWithMandator(data: LoginInfo, mandator): Observable<void> {
		return this.selectMandator(mandator).pipe(
			take(1),
			mergeMap(() => this.http.get(this.nuclosConfig.getRestHost()).pipe()),
			map((loginData: LoginInfo) => {
				data.mandator = mandator!;
				data.allowedActions = loginData.allowedActions;
				this.loginSuccessful(data, true);
			})
		);
	}

	selectMandator(mandator): Observable<any> {
		let mandatorSelection = this.http.post(mandator.links.chooseMandator.href, {});
		this.setLastMandatorCookie(mandator);
		return mandatorSelection.pipe(tap(() => this.mandatorSelection.next(mandator)));
	}

	isLoginInProgress() {
		return this.loginInProgress;
	}

	waitForLogin(): Observable<any> {
		if (this.isLoginInProgress()) {
			return this.observeLoginStatus().pipe(
				filter(loggedIn => loggedIn),
				take(1)
			);
		}
		return observableOf(true);
	}

	/**
	 * Determines if the page with the given location requires authentication.
	 * This is not the case for start, login/registration and error pages.
	 */
	isAuthenticationRequired(locationPath) {
		let result =
			locationPath.indexOf('/login') === -1 &&
			locationPath.indexOf('/logout') === -1 &&
			locationPath.indexOf('/locale/') === -1 &&
			locationPath !== '' &&
			locationPath !== '/' &&
			locationPath !== '#/' &&
			locationPath.indexOf('/error') === -1 &&
			locationPath.indexOf('/account/register') === -1 &&
			locationPath.indexOf('/account/activate') === -1 &&
			// Password change must be possible without login for expired credentials
			locationPath.indexOf('/account/changePassword') === -1 &&
			locationPath.indexOf('/account/forgotLoginDetails') === -1 &&
			locationPath.indexOf('/account/resetPassword') === -1;

		this.$log.debug('isAuthenticationRequired(%o) = %o', locationPath, result);

		return result;
	}

	hasRedirectCookie() {
		return this.getRedirectCookie() !== undefined;
	}

	setRedirectCookie(path: string) {
		path = path ? path.replace(/^#*/, '') : '';
		this.$log.info('Setting redirect cookie for location: %o', path);
		if (this.isAuthenticationRequired(path)) {
			this.$log.debug('Setting redirect cookie: %o', path);
			this.cookieService.put(AuthenticationService.COOKIE_KEY_REDIRECT, path);
		}
	}

	removeRedirectCookie() {
		this.$log.debug('Removing redirect cookie');
		this.cookieService.remove(AuthenticationService.COOKIE_KEY_REDIRECT);
	}

	getRedirectCookie() {
		let redirect = this.cookieService.getCached(AuthenticationService.COOKIE_KEY_REDIRECT);
		return redirect ? redirect : undefined;
	}

	getLastEntityCookie() {
		let redirect = this.cookieService.getCached(AuthenticationService.COOKIE_KEY_LAST_ENTITY);
		return redirect ? redirect : undefined;
	}

	removeLastEntityCookie() {
		this.$log.debug('Removing last Entity cookie');
		this.cookieService.remove(AuthenticationService.COOKIE_KEY_LAST_ENTITY);
	}

	setLastEntityCookie(entityClassId: string | undefined) {
		if (entityClassId) {
			this.cookieService.put(AuthenticationService.COOKIE_KEY_LAST_ENTITY, entityClassId);
		}
	}

	setLastMandatorCookie(mandator: Mandator) {
		this.cookieService.put(AuthenticationService.COOKIE_KEY_LAST_MANDATOR, mandator.mandatorId);
	}

	getLastMandatorIdCookie() {
		let lastMandatorId = this.cookieService.getCached(
			AuthenticationService.COOKIE_KEY_LAST_MANDATOR
		);
		return lastMandatorId ? lastMandatorId : undefined;
	}

	setDataLanguageCookie(dataLanguageId: string) {
		this.cookieService.put(AuthenticationService.COOKIE_KEY_DATALANGUAGE, dataLanguageId);
	}

	getDataLanguageCookie() {
		let dataLanguageId = this.cookieService.getCached(
			AuthenticationService.COOKIE_KEY_DATALANGUAGE
		);
		return dataLanguageId ? dataLanguageId : undefined;
	}

	invalidateSessionId() {
		this.cookieService.putObject('authentication', {});
		// delete old JSESSIONID cookie to avoid problems in further requests
		const nuclosUrl = this.nuclosConfig.getNuclosURL();
		const backendPath = nuclosUrl.slice(nuclosUrl.lastIndexOf('/'), nuclosUrl.length);
		this.cookieService.remove('JSESSIONID', `${backendPath}/`);
	}

	/**
	 * Sets the redirect cookie for a redirect to the last location that is stored via the "location" cookie,
	 * if the user is not anonymous and does not have a redirect cookie already.
	 */
	redirectToLastLocation(data: LoginInfo) {
		if (this.isAnonymous(data) || this.hasRedirectCookie()) {
			return false;
		}

		let username = data.username;
		let lastLocation = this.cookieService.getCached('location.' + username);

		if (lastLocation) {
			this.$log.info('Redirecting to last location: %o', lastLocation);
			this.setRedirectCookie(lastLocation);
			return true;
		}

		return false;
	}

	/**
	 * Sets the redirect cookie for a redirect (after login) to the current location.
	 *
	 * @returns {boolean}
	 */
	redirectToCurrentLocation() {
		let location = window.location.hash;
		if (location && this.isAuthenticationRequired(location)) {
			this.setRedirectCookie(location);
			return true;
		}
		return false;
	}

	/**
	 * Redirects immediately to the login page.
	 */
	redirectToLoginPage() {
		if (this.router.url !== '/login') {
			this.$log.debug('Redirecting to login page...');
			this.router.navigate(['/login']);
		}
	}

	redirectToSso(authEndpoint: SsoAuthEndpoint) {
		const restUri = this.nuclosConfig.getRestHost() + '/ssoAuthEndpoints/' + authEndpoint.ssoAuthId;
		this.http.put(restUri, null, {
			responseType: 'text'
		}).subscribe(
			authEndpointUri => window.location.href = authEndpointUri
		);
	}

	getUsername(): string | undefined {
		return this.cookieService.getCached('authentication')
			? (<LoginInfo>this.cookieService.getObjectCached('authentication')).username
			: undefined;
	}

	getAuthentication(): LoginInfo | undefined {
		return this.authentication;
	}

	/**
	 * Determines if the current user is a superuser.
	 */
	isSuperUser(): boolean {
		// TODO: Use current auth data, not from cookie
		let authentication = this.getAuthentication();
		return !!(authentication && authentication.superUser);
	}

	/**
	 * Deletes the current session from the server and invalidates the session ID.
	 */
	logout(): Observable<any> {
		this.logOutRequested = true;
		let ssoLogoutUri = this.authentication?.ssoLogoutUri;
		return this.http.delete(this.nuclosConfig.getRestHost()).pipe(
			switchMap(data => {
				this.$log.debug('Logout successful', data);
				return this.destroySession();
			}),
			catchError(error => {
				this.$log.error('Logout failed', error);
				return this.destroySession();
			})
		);
	}

	/**
	 * Possibly saves the given url in the "last location" cookie.
	 * The user is redirected to this location after the next login.
	 */
	rememberUrl(url: string) {
		this.$log.debug('Remembering URL: %o', url);

		let key = 'location.' + this.getUsername();
		this.cookieService.put(
			key,
			url,
			new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 365)
		);
	}

	shouldUrlBeRemembered(url: string) {
		return (
			!this.isAnonymous() &&
			this.getUsername() &&
			this.isAuthenticationRequired(url) &&
			!url.endsWith('/new') &&
			!url.startsWith('/popup') &&
			!url.startsWith('/account/changePassword') &&
			!url.endsWith('index.html') &&
			!url.endsWith('richClientCompleted')
		);
	}

	isLoggedIn(): boolean {
		return this.loggedIn;
	}

	isLogOutRequested(): boolean {
		return this.logOutRequested;
	}

	observeLoginStatus(): Observable<boolean> {
		return this.loginSubject;
	}

	selectDataLanguage(dataLanguage: DataLanguage): Observable<any> {
		this.setDataLanguageCookie(dataLanguage?.datalanguage);
		const restUri = this.nuclosConfig.getRestHost() + '/datalanguage';
		return this.http.put(restUri, dataLanguage);
	}

	getPrimaryDataLanguage(): DataLanguage | undefined {
		if (this.dataLanguages) {
			return this.dataLanguages.find(dataLanguage => dataLanguage.primary);
		}
		return undefined;
	}

	loadAllDataLanguages() {
		const restUri = this.nuclosConfig.getRestHost() + '/datalanguages';
		(this.cacheService.getCache('http.GET').get(
			restUri
			, this.http
				.get<DataLanguage[]>(
					restUri
				)) as Observable<DataLanguage[]>).subscribe(allDataLanguages => {
			this.dataLanguages = allDataLanguages;
		});
	}

	getAllDataLanguages(): DataLanguage[] | undefined {
		return this.dataLanguages;
	}

	getDataLanguages(localeString: string): Observable<DataLanguage[]> {
		const restUri = this.nuclosConfig.getRestHost() + '/datalanguages?language=' + localeString;
		return this.cacheService.getCache('http.GET').get(
			restUri
			, this.http
				.get<DataLanguage[]>(
					restUri
				)) as Observable<DataLanguage[]>;
	}

	resetWorkspace() {
		let link = this.nuclosConfig.getRestHost() + '/meta/resetworkspace';
		this.http.post(link, {}).subscribe(() => window.location.reload());
	}

	setCredentialsExpired(credentialsExpired: UserPassword) {
		this.credentialsExpired = credentialsExpired;
		this.router.navigate(['account', 'changePassword']);
	}

	getCredentialsExpired() {
		return this.credentialsExpired;
	}

	isLdapActive() {
		return this.ldapActive;
	}

	isSsoActive() {
		return this.ssoActive;
	}

	destroySession(): Observable<boolean> {
		let ssoLogoutUri = this.authentication?.ssoLogoutUri;
		this.invalidateSessionId();
		this.currentUser = undefined;
		this.authentication = undefined;
		this.setLoggedIn(false);
		this.removeLastEntityCookie();

		let navigatingObservable = from(this.router.navigateByUrl('/login?autoLogin=false'));
		if (ssoLogoutUri) {
			window.location.href = ssoLogoutUri;
			navigatingObservable = of(true);
		}

		return navigatingObservable;
	}

	autologin(withAnonymousLogin = true) {
		this.loginInProgress = true;

		let checkIfLoggedIn = this.http.get(this.nuclosConfig.getRestHost()).pipe(
			tap((data: LoginInfo) => {
				if (data.username === this.ANONYMOUS_USER_NAME && !withAnonymousLogin) {
					throw('no anonymous auto login');
				} else {
					this.loginSuccessful(data, false);
				}
			}),
			catchError(() => of(undefined))
		);

		let anonymousLogin = withAnonymousLogin ? this.anonymousLogin() : of(undefined);

		this.busyService
			.busy(
				checkIfLoggedIn.pipe(
					switchMap((loginData) => {
						if (loginData) {
							return of(loginData);
						} else {
							return anonymousLogin;
						}
					}),
					take(1)
				)
			)
			.subscribe((data: LoginInfo | undefined) => {
				if (data === undefined) {
					this.loginInProgress = false;
					this.setUnauthorized();
				} else {
					this.autoNavigation();
				}
			});
	}

	private checkLogin(loggedIn, url: string) {
		this.$log.debug('Logged in: %o', loggedIn);
		if (!loggedIn) {
			this.redirectToLoginPage();
		} else {
			// Remember the last location after successful navigation
			this.$log.debug('Location changed: %o', url);
			if (this.shouldUrlBeRemembered(url)) {
				this.rememberUrl(url);
			}
		}
	}

	/**
	 * Performs a login as the anonymous user.
	 */
	private loginAsAnonymous(): Observable<LoginInfo> {
		this.$log.info('Trying anonymous login...');
		let loginData = {
			username: this.ANONYMOUS_USER_NAME,
			password: this.ANONYMOUS_USER_NAME,
			locale: this.i18nService.getCurrentLocale().key,
			autologin: false,
			serverUri: this.nuclosConfig.getNuclosURL()
		};

		return this.login(loginData);
	}

	/**
	 * Handles login data after a successful login.
	 */
	private loginSuccessful(data: LoginInfo, newLogin: boolean) {
		this.loginError = false;
		let loggedInCompleted = true;
		if (data.mandators !== undefined && data.mandators.length > 0) {
			if (data.mandator === undefined && !data.superUser) {
				// mandator selection not completed
				loggedInCompleted = false;
			}
		}
		if (!loggedInCompleted) {
			this.$log.info('LOGIN NOT COMPLETED: %o', data);
			return;
		}
		this.$log.info('LOGIN SUCCESSFUL: %o', data);

		this.authentication = data;

		this.currentUser = new User(data.username);
		this.allowedActions = (data.allowedActions as any) as Map<string, boolean>;
		this.authenticationDataAvailable.next(true);

		this.redirectToLastLocation(data);
		this.redirectToInitialEntity(data);
		if (data.showRichClientCompleted === true) {
			this.setRedirectCookie('login/richClientCompleted');
		}

		// SAVE COOKIE
		this.cookieService.putObject('authentication', data);
		if (data.datalanguage) {
			if (data.datalanguage !== this.getDataLanguageCookie()) {
				this.setDataLanguageCookie(data.datalanguage);
			}
		}

		if (data.mandator) {
			this.mandatorSelection.next(data.mandator);
		}

		if (!data.mandators && data.showRichClientCompleted !== true && newLogin) {
			this.autoNavigation();
		}

		this.ldapActive = data.ldapActive === true;
		this.ssoActive = data.ssoActive === true;

		this.setLoggedIn(true);
	}

	/**
	 * Logs in as anonymous, if the anonymous login is enabled via system parameter.
	 */
	private anonymousLogin() {
		return this.nuclosConfig.getSystemParameters().pipe(
			mergeMap(params => {
				this.$log.debug('Got system params: %o', params);
				if (params.is(SystemParameter.ANONYMOUS_USER_ACCESS_ENABLED)) {
					return this.loginAsAnonymous().pipe(
						tap(data => this.$log.debug('Logged in as anonymous: %o', data))
					);
				} else {
					return of(undefined);
				}
			})
		);
	}

	/**
	 * Sets the redirect cookie to the "initial entity" of this user, if there is not
	 * already a redirect cookie set.
	 */
	private redirectToInitialEntity(data: LoginInfo) {
		if (!data.initialEntity || this.hasRedirectCookie()) {
			return;
		}

		this.setRedirectCookie('/view/' + data.initialEntity);
	}

	private setLoggedIn(loggedIn: boolean) {
		this.loggedIn = loggedIn;
		if (loggedIn) {
			this.logOutRequested = true;
		}
		this.loginSubject.next(loggedIn);
	}

	private setUnauthorized() {
		if (!this.loginError) {
			this.setLoggedIn(false);

			if (AuthenticationService.isAutoNavigationAllowed()) {
				/**
				 * Redirects back to the current location after login.
				 */
				this.redirectToCurrentLocation();
				this.router.navigate(['/login']);
			}
		}
	}
}
