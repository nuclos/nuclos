import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@modules/authentication';

@Component({
	selector: 'nuc-rich-client-completed',
	templateUrl: './rich-client-completed.component.html',
	styleUrls: ['./rich-client-completed.component.css']
})
export class RichClientCompletedComponent {

	constructor(
		private authenticationService: AuthenticationService
	) {
	}

	navigateHome() {
		this.authenticationService.navigateHome();
	}

}
