import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SessionInfoComponent } from './session-info.component';

xdescribe('SessionInfoComponent', () => {
	let component: SessionInfoComponent;
	let fixture: ComponentFixture<SessionInfoComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [SessionInfoComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SessionInfoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
