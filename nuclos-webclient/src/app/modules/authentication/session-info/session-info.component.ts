import { Component, OnInit } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { take } from 'rxjs/operators';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { SystemParameter } from '../../../shared/system-parameters';
import { AuthenticationService } from '../authentication.service';

@Component({
	selector: 'nuc-session-info',
	templateUrl: './session-info.component.html',
	styleUrls: ['./session-info.component.css']
})
export class SessionInfoComponent implements OnInit {

	displayData: Array<{ key, value }>;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor(
		private configService: NuclosConfigService,
		private authenticationService: AuthenticationService
	) {
	}

	ngOnInit() {
		this.authenticationService.waitForLogin().subscribe((state) => {
			if (state) {
				this.configService.getSystemParameters().pipe(take(1)).subscribe(params => {
					if (params.is(SystemParameter.ENVIRONMENT_DEVELOPMENT)) {
						let authentication = this.authenticationService.getAuthentication();
						this.displayData = [];

						if (!authentication) {
							return;
						}

						for (let i in authentication) {
							if (authentication.hasOwnProperty(i)) {
								let value = JSON.stringify(authentication[i]);
								this.displayData.push({key: i, value: value});
							}
						}
					}
				});
			}
		});
	}

}
