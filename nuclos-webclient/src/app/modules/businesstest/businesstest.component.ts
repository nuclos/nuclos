import { Component, OnDestroy, OnInit } from '@angular/core';
import { concat as observableConcat, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NuclosTitleService } from '../../shared/service/nuclos-title.service';
import { BusinesstestLogHandler, BusinesstestService } from './shared/businesstest.service';

@Component({
	selector: 'nuc-businesstest',
	templateUrl: './businesstest.component.html',
	styleUrls: ['./businesstest.component.css']
})
export class BusinesstestComponent implements OnInit, OnDestroy {
	started;
	progress;
	executionResult = '';
	logHandler: BusinesstestLogHandler;

	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private businesstestService: BusinesstestService,
		private titleService: NuclosTitleService
	) {
		this.started = false;
		this.progress = 0;

		this.logHandler = {
			start: () => {
				this.progress = 0;
				this.executionResult = '';
			},
			receiveMessage: message => {
				this.executionResult += message;
				let output = $('#output');
				if (output.length) {
					output.scrollTop(output[0].scrollHeight - output.height());
				}
			},
			receiveProgress: progress => {
				this.progress = progress;
			},
			done: () => {
				this.progress = 100;
			},
			fail: () => {
			}
		};
	}

	ngOnInit() {
		this.setTitle();
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	generateAndRun(): void {
		this.started = true;
		observableConcat(
			this.generateAllTests(),
			this.runAllTests()
		).pipe(takeUntil(this.unsubscribe$)).subscribe({
			complete: () => this.started = false,
			error: () => this.started = false
		});
	}

	private generateAllTests(): Observable<any> {
		return this.businesstestService.generateAllTests(this.logHandler);
	}


	private runAllTests(): Observable<any> {
		return this.businesstestService.runAllTests(this.logHandler);
	}

	private setTitle() {
		this.titleService.setLocalizedTitle('webclient.businesstests.menu');
	}
}
