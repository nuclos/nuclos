import { RouterModule, Routes } from '@angular/router';
import { BusinesstestAdvancedComponent } from './businesstest-advanced/businesstest-advanced.component';
import { BusinesstestDetailComponent } from './businesstest-detail/businesstest-detail.component';
import { BusinesstestComponent } from './businesstest.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: '',
		data: {
			setDefaultTitle: false
		},
		component: BusinesstestComponent
	},
	{
		path: 'advanced',
		data: {
			setDefaultTitle: false
		},
		component: BusinesstestAdvancedComponent
	},
	{
		path: ':testId',
		data: {
			setDefaultTitle: false
		},
		component: BusinesstestDetailComponent
	}
];

export const BusinesstestRoutesModule = RouterModule.forChild(ROUTE_CONFIG);
