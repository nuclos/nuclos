/* tslint:disable:no-unused-variable */

import { inject, TestBed } from '@angular/core/testing';
import { BusinesstestService } from './businesstest.service';

xdescribe('Service: Businesstest', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [BusinesstestService]
		});
	});

	it('should ...', inject([BusinesstestService], (service: BusinesstestService) => {
		expect(service).toBeTruthy();
	}));
});
