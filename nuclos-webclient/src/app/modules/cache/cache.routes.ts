import { RouterModule, Routes } from '@angular/router';
import { CacheOverviewComponent } from './cache-overview/cache-overview.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'cache',
		data: {
			localizedTitle: 'webclient.cache.overview'
		},
		component: CacheOverviewComponent
	},
];

export const CacheRoutesModule = RouterModule.forChild(ROUTE_CONFIG);
