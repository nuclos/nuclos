import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartButtonComponent } from './chart-button.component';

xdescribe('ChartButtonComponent', () => {
	let component: ChartButtonComponent;
	let fixture: ComponentFixture<ChartButtonComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ChartButtonComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartButtonComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
