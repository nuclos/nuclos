import { Component, Input, OnInit } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { take } from 'rxjs/operators';
import { NuclosDialogService } from '../../../core/service/nuclos-dialog.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { ChartModalComponent } from '../chart-modal/chart-modal.component';

@Component({
	selector: 'nuc-chart-button',
	templateUrl: './chart-button.component.html',
	styleUrls: ['./chart-button.component.css']
})
export class ChartButtonComponent implements OnInit {

	@Input()
	eo: EntityObject;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor(
		private modalService: NuclosDialogService,
	) {
	}

	ngOnInit() {
	}

	openModal() {
		this.modalService.custom(
			ChartModalComponent,
			{
				eo: this.eo
			}, {
				size: 'lg',
				windowClass: 'fullsize-modal-window'
			}
		).pipe(
			take(1)
		).subscribe();
	}
}
