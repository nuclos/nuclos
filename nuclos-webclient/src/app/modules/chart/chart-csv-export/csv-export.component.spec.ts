import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartCsvExportComponent } from './chart-csv-export.component';

xdescribe('CsvExportComponent', () => {
	let component: ChartCsvExportComponent;
	let fixture: ComponentFixture<ChartCsvExportComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ChartCsvExportComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartCsvExportComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
