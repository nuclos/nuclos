import { Component, Input } from '@angular/core';
import { ChartService } from '../shared/chart.service';
import { EoChartWrapper } from '../shared/eo-chart-wrapper';
import { ChartSearchTemplate } from '../shared/chart-search-template';

@Component({
	selector: 'nuc-chart-filter-lov',
	templateUrl: './chart-filter-lov.component.html'
})
export class ChartFilterLovComponent {

	@Input() searchTemplate: ChartSearchTemplate;
	@Input() eoChart: EoChartWrapper;

	constructor(
		private chartService: ChartService
	) {
	}

	search() {
		this.chartService.search(this.eoChart);
	}
}
