import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartFilterComponent } from './chart-filter.component';

xdescribe('ChartFilterComponent', () => {
	let component: ChartFilterComponent;
	let fixture: ComponentFixture<ChartFilterComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ChartFilterComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartFilterComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
