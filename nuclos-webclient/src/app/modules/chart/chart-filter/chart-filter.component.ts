import { Component, Input, OnInit } from '@angular/core';
import { ChartService } from '../shared/chart.service';
import { EoChartWrapper } from '../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-filter',
	templateUrl: './chart-filter.component.html',
	styleUrls: ['./chart-filter.component.css']
})
export class ChartFilterComponent implements OnInit {
	@Input() eoChart: EoChartWrapper;

	constructor(
		private chartService: ChartService
	) {
	}

	ngOnInit() {
		if (!this.eoChart.searchItems) {
			this.chartService.buildSearchItems(this.eoChart);
		}
	}

	search() {
		this.chartService.search(this.eoChart);
	}

}
