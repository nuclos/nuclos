import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartConfigComponent } from './chart-config.component';

xdescribe('ChartConfigComponent', () => {
	let component: ChartConfigComponent;
	let fixture: ComponentFixture<ChartConfigComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ChartConfigComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartConfigComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
