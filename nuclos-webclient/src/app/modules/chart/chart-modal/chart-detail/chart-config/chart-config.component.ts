import { Component, Input, OnInit } from '@angular/core';
import { EoChartWrapper } from '../../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-config',
	templateUrl: './chart-config.component.html',
	styleUrls: ['./chart-config.component.css']
})
export class ChartConfigComponent implements OnInit {

	@Input() eo: EoChartWrapper;


	constructor() {
	}

	ngOnInit() {
	}

}
