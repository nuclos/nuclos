import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartExtendedComponent } from './chart-extended.component';

xdescribe('ExtendedComponent', () => {
	let component: ChartExtendedComponent;
	let fixture: ComponentFixture<ChartExtendedComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ChartExtendedComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartExtendedComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
