import { Component, Input, OnInit } from '@angular/core';
import { Logger } from '../../../../../log/shared/logger';
import { ChartService } from '../../../../shared/chart.service';
import { EoChartWrapper } from '../../../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-extended',
	templateUrl: './chart-extended.component.html',
	styleUrls: ['./chart-extended.component.css']
})
export class ChartExtendedComponent implements OnInit {

	@Input() eoChart: EoChartWrapper;

	constructor(
		private chartService: ChartService,
		private $log: Logger
	) {
	}

	ngOnInit() {
	}

	setContent(content) {
		if (!content) {
			content = '{}';
		}

		try {
			this.eoChart.setPreferenceContent(content);
			this.chartService.buildSearchItems(this.eoChart);
			this.chartService.search(this.eoChart);
		} catch (e) {
			this.$log.warn('Could not parse as JSON: %o', e);
		}
	}
}
