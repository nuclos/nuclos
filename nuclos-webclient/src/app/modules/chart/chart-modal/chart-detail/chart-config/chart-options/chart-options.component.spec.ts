import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartOptionsComponent } from './chart-options.component';

xdescribe('OptionsComponent', () => {
	let component: ChartOptionsComponent;
	let fixture: ComponentFixture<ChartOptionsComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ChartOptionsComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartOptionsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
