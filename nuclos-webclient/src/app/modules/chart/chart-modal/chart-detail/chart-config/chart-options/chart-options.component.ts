import { Component, Input, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { ChartService } from '../../../../shared/chart.service';
import { EoChartWrapper } from '../../../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-options',
	templateUrl: './chart-options.component.html',
	styleUrls: ['./chart-options.component.css']
})
export class ChartOptionsComponent implements OnInit {
	@Input() eoChart: EoChartWrapper;

	chartTypes = ChartService.chartTypes;
	referenceAttributes: { id: string; name: string }[];
	entityAttributes: { id: string; name: string }[];
	seriesAttributes: { id: string; name: string }[];
	boMetaId: string | undefined;

	constructor(private chartService: ChartService) {}

	ngOnInit() {
		if (!this.eoChart.eo.getId()) {
			this.eoChart.chartPreference.name = this.eoChart.chartPreference.content.name.de;
			this.boMetaId = this.eoChart.eo.getEntityClassId();
			this.setReferenceAttributeIdImpl(this.boMetaId, false);
		} else {
			this.referenceAttributes = this.chartService.findReferenceAttributes(this.eoChart.eo);
			if (this.referenceAttributes.length === 1) {
				this.setReferenceAttributeIdImpl(this.referenceAttributes[0].id, false);
			}
		}
		this.updateCategoryAttributes();
	}

	setName(name: string) {
		if (!this.eoChart.eo.getId()) {
			this.eoChart.chartPreference.content.name.de = name;
		}
		this.eoChart.chartPreference.name = name;
		this.eoChart.chartPreference.dirty = true;
	}

	setChartType(chartType: string) {
		this.eoChart.chartPreference.content.chart.type = chartType;

		// TODO: Create proper wrappers for preferences with change- and event-handling
		this.chartChanged();
	}

	getReferenceAttributeId() {
		return (
			this.eoChart.chartPreference &&
			this.eoChart.chartPreference.content &&
			this.eoChart.chartPreference.content.chart &&
			this.eoChart.chartPreference.content.chart.primaryChart &&
			this.eoChart.chartPreference.content.chart.primaryChart.refBoAttrId
		);
	}

	setReferenceAttributeId(attributeId: string) {
		this.setReferenceAttributeIdImpl(attributeId, true);
	}

	setReferenceAttributeIdImpl(attributeId: string, updateCatAttr: boolean) {
		if (this.eoChart.chartPreference.content.chart.primaryChart.refBoAttrId !== attributeId) {
			this.eoChart.chartPreference.content.chart.primaryChart.refBoAttrId = attributeId;
			this.eoChart.chartPreference.dirty = true;
			if (updateCatAttr) {
				this.updateCategoryAttributes();
			}
		}
	}

	setCategoryAttributeId(attributeId: string) {
		if (this.eoChart.chartPreference.content.chart.primaryChart.categoryBoAttrId !== attributeId) {
			this.eoChart.chartPreference.content.chart.primaryChart.categoryBoAttrId = attributeId;
			this.reloadChartData();
		}
	}

	chartChanged() {
		this.eoChart.chartPreference.dirty = true;
		this.eoChart.chartOptionsChanged.next(true);
	}

	reloadChartData() {
		this.chartChanged();
		this.chartService.search(this.eoChart);
	}

	private updateCategoryAttributes() {
		this.entityAttributes = [];
		this.seriesAttributes = [];

		let observer = entityMeta => {
			let attributes = entityMeta.getAttributes();
			attributes.forEach(attributeMeta => {
				if ((attributeMeta.getType() === 'Integer' || attributeMeta.getType() === 'Long' ||
						attributeMeta.getType() === 'String' || attributeMeta.getType() === 'Date')
					&& !attributeMeta.isSystemAttribute()) {
					this.entityAttributes.push({
						id: attributeMeta.getAttributeID(),
						name: attributeMeta.getName() || attributeMeta.getAttributeID()
					});
				}
				if ((attributeMeta.getType() === 'Decimal' || attributeMeta.getType() === 'Integer')
					&& !attributeMeta.isSystemAttribute()) {
					this.seriesAttributes.push({
						id: attributeMeta.getAttributeID(),
						name: attributeMeta.getName() || attributeMeta.getAttributeID()
					});
				}
			});
			if (this.entityAttributes.length === 1) {
				this.setCategoryAttributeId(this.entityAttributes[0].id);
			}
		};

		let referenceAttributeId = this.getReferenceAttributeId();
		if (referenceAttributeId === this.eoChart.eo.getEntityClassId()) {
			this.eoChart.eo.getMeta().pipe(take(1))
				.subscribe(observer);
		} else if (referenceAttributeId) {
			this.chartService
				.getSubEntityMeta(this.eoChart.eo, referenceAttributeId)
				.pipe(take(1))
				.subscribe(observer);
		}
	}
}
