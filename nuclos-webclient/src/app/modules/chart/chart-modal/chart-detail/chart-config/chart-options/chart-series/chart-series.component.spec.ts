import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartSeriesComponent } from './chart-series.component';

xdescribe('SeriesComponent', () => {
	let component: ChartSeriesComponent;
	let fixture: ComponentFixture<ChartSeriesComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ChartSeriesComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartSeriesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
