import { AfterContentInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChartSeries } from '../../../../../shared/chart-series';
import { EoChartWrapper } from '../../../../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-series',
	templateUrl: './chart-series.component.html',
	styleUrls: ['./chart-series.component.css']
})
export class ChartSeriesComponent implements OnInit, AfterContentInit {

	@Input() eoChart: EoChartWrapper;
	@Input() referenceAttributeId: string;
	@Input() entityAttributes: { id: string, name: string }[] = [];

	@Output() seriesChanged = new EventEmitter();

	selectedSeries: ChartSeries | undefined;

	constructor() {
	}

	ngOnInit() {
	}

	ngAfterContentInit() {
		setTimeout(() => {
			if (this.eoChart.chartOptions.primaryChart.series.length === 0) {
				this.addSeries();
			}
		});
	}

	addSeries() {
		let series: ChartSeries = {
			boAttrId: '',
			label: {
				de: ''
			},
			type: 'bar'
		};
		this.eoChart.chartOptions.primaryChart.series.push(series);
		this.selectedSeries = series;
		if (this.entityAttributes.length === 1) {
			this.setAttributeId(series, this.entityAttributes[0].id);
		}
	}

	setAttributeId(series: ChartSeries, attributeId: string) {
		let attribute = this.entityAttributes.find(it => it.id === attributeId);

		if (!attribute) {
			return;
		}

		if (series.boAttrId === attribute.id && series.label.de === attribute.name) {
			return;
		}

		series.boAttrId = attribute.id;
		series.label.de = attribute.name;

		this.seriesChanged.emit();
	}

	removeSeries(series: ChartSeries) {
		let allSeries = this.eoChart.chartOptions.primaryChart.series;
		let index = allSeries.indexOf(series);
		if (index >= 0) {
			allSeries.splice(index, 1);
			this.selectPreviousSeries(index);
			this.seriesChanged.emit();
		}
	}

	private selectPreviousSeries(index: number) {
		let allSeries = this.eoChart.chartOptions.primaryChart.series;
		if (index > 0) {
			this.selectedSeries = allSeries[index - 1];
		} else if (allSeries.length > 0) {
			this.selectedSeries = allSeries[0];
		} else {
			this.selectedSeries = undefined;
		}
	}
}
