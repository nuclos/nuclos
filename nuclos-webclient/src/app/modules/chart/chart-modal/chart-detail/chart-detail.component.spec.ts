import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartDetailComponent } from './chart-detail.component';

xdescribe('DetailComponent', () => {
	let component: ChartDetailComponent;
	let fixture: ComponentFixture<ChartDetailComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ChartDetailComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartDetailComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
