import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChartService } from '../../shared/chart.service';
import { EoChartWrapper } from '../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-detail',
	templateUrl: './chart-detail.component.html',
	styleUrls: ['./chart-detail.component.css']
})
export class ChartDetailComponent implements OnInit {

	@Input() eoChart: EoChartWrapper;
	@Input() allCharts: EoChartWrapper[];
	@Input() toolbar: boolean;
	@Output() selectedChart = new EventEmitter();

	constructor(
		private chartService: ChartService
	) {
	}

	ngOnInit() {
	}

	isShowConfig() {
		return this.chartService.isShowConfig();
	}
}
