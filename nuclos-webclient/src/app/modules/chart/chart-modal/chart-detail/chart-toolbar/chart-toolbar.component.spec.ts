import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartToolbarComponent } from './chart-toolbar.component';

xdescribe('ToolbarComponent', () => {
	let component: ChartToolbarComponent;
	let fixture: ComponentFixture<ChartToolbarComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ChartToolbarComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartToolbarComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
