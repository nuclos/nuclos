import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { take } from 'rxjs/operators';
import { NuclosDialogService } from '../../../../../core/service/nuclos-dialog.service';
import { NuclosI18nService } from '../../../../../core/service/nuclos-i18n.service';
import { ChartService } from '../../../shared/chart.service';
import { EoChartWrapper } from '../../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-toolbar',
	templateUrl: './chart-toolbar.component.html',
	styleUrls: ['./chart-toolbar.component.css']
})
export class ChartToolbarComponent implements OnInit {
	@Input() eoChart: EoChartWrapper;
	@Input() eoCharts: EoChartWrapper[];
	@Output() selectedChart = new EventEmitter();
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor(
		private chartService: ChartService,
		private nuclosI18nService: NuclosI18nService,
		private dialogService: NuclosDialogService
	) {}

	ngOnInit() {}

	canConfigureCharts() {
		return this.chartService.canConfigureCharts();
	}

	save() {
		this.chartService.saveChart(this.eoChart).pipe(take(1)).subscribe();
	}

	delete() {
		this.dialogService
			.confirm({
				title: this.nuclosI18nService.getI18n('webclient.charts.delete'),
				message: this.nuclosI18nService.getI18n('webclient.charts.delete.confirm')
			})
			.pipe(
				take(1)
			).subscribe(result => {
			if (result) {
				// ok, delete
				if (this.eoChart.chartPreference.prefId) {
					this.chartService
						.deleteChart(this.eoChart.chartPreference)
						.pipe(take(1))
						.subscribe(() => {
							this.removeChart();
						});
				} else {
					this.removeChart();
				}
			}
		});
	}

	toggleConfig() {
		this.chartService.toggleConfig();
	}

	exportChart() {
		this.chartService.exportAsCsv(this.eoChart);
	}

	isExportAvailable() {
		let configurationValid = this.chartService.isValidChartPreference(
			this.eoChart.chartPreference.content
		);
		let dataCount = this.eoChart.getCurrentDataCount();
		return configurationValid && dataCount > 0;
	}

	private removeChart() {
		let index = this.eoCharts.indexOf(this.eoChart);
		if (index >= 0) {
			this.eoCharts.splice(index, 1);
		}
		// select first chart
		this.eoChart = this.eoCharts[this.eoCharts.length - 1];
		this.selectedChart.next(this.eoChart);
	}
}
