
export class ChartData {
	category: AxisData;
	series: AxisData[];
}

export class AxisData {
	key?: string;
	values: any[];
	color?: string;
}
