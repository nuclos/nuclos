import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChartModalComponent } from './chart-modal.component';

xdescribe('ChartModalComponent', () => {
	let component: ChartModalComponent;
	let fixture: ComponentFixture<ChartModalComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ChartModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChartModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
