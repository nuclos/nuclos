import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { SharedModule } from '../../shared/shared.module';
import { ClickOutsideModule } from '../click-outside/click-outside.module';
import { I18nModule } from '../i18n/i18n.module';
import { LogModule } from '../log/log.module';
import { UiComponentsModule } from '../ui-components/ui-components.module';
import { ChartButtonComponent } from './chart-button/chart-button.component';
import { ChartFilterLovComponent } from './chart-filter/chart-filter-lov.component';
import { ChartFilterComponent } from './chart-filter/chart-filter.component';
import { ChartModalComponent } from './chart-modal/chart-modal.component';
import { ChartTabsComponent } from './chart-modal/chart-tabs/chart-tabs.component';
import { ChartViewComponent } from './chart-modal/chart-detail/chart-view/chart-view.component';
import { ChartViewService } from './chart-modal/chart-detail/chart-view/chart-view.service';
import { ChartConfigComponent } from './chart-modal/chart-detail/chart-config/chart-config.component';
import { ChartExtendedComponent } from './chart-modal/chart-detail/chart-config/chart-extended/chart-extended.component';
import { ChartOptionsComponent } from './chart-modal/chart-detail/chart-config/chart-options/chart-options.component';
import { ChartSeriesComponent } from './chart-modal/chart-detail/chart-config/chart-options/chart-series/chart-series.component';
import { ChartDetailComponent } from './chart-modal/chart-detail/chart-detail.component';
import { ChartToolbarComponent } from './chart-modal/chart-detail/chart-toolbar/chart-toolbar.component';
import { ChartCsvExportComponent } from './chart-csv-export/chart-csv-export.component';
import { ChartService } from './shared/chart.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		SharedModule,
		I18nModule,
		LogModule,
		UiComponentsModule,
		// TODO: Should not be necessary here
		ClickOutsideModule,
		AutoCompleteModule,
		NgbModule,
		PrettyJsonModule,
	],
	declarations: [
		ChartButtonComponent,
		ChartFilterComponent,
		ChartFilterLovComponent,
		ChartModalComponent,
		ChartConfigComponent,
		ChartCsvExportComponent,
		ChartDetailComponent,
		ChartViewComponent,
		ChartOptionsComponent,
		ChartToolbarComponent,
		ChartExtendedComponent,
		ChartSeriesComponent,
		ChartTabsComponent,
	],
	exports: [
		ChartButtonComponent,
		ChartDetailComponent,
	],
	providers: [
		ChartService,
		ChartViewService,
	]
})
export class ChartModule {
}
