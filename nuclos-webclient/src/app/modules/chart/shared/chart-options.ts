import { ChartSeries } from './chart-series';
import { ChartSearchTemplate } from './chart-search-template';

export interface ChartOptions {
	type: string;
	primaryChart: {
		boMetaId: string;
		refBoAttrId: string;
		categoryBoAttrId: string;
		series: ChartSeries[];
		sort?: {
			sortByBoAttrId?: string;
			sortDirection?: 'asc' | 'desc';
		};
	};
	x?: string | Function;
	y?: string | Function;
	yAxis?: {
		tickFormat?: string | Function
	};
	xAxis?: {
		tickFormat?: string | Function
	};
	searchTemplate?: ChartSearchTemplate[];

	chart?: any;
}
