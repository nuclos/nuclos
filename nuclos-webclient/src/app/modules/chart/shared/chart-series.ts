export interface ChartSeries {
	boAttrId: string;
	color?: string;
	label: {
		de: string
	};
	type: string; // TODO enum
}
