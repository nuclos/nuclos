import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ClickOutsideDirective } from './click-outside.directive';
import { ClickOutsideService } from './click-outside.service';

@NgModule({
	imports: [
		CommonModule
	],
	providers: [
		ClickOutsideService
	],
	declarations: [
		ClickOutsideDirective
	],
	exports: [
		ClickOutsideDirective
	]
})
export class ClickOutsideModule {
}
