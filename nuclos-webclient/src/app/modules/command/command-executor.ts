import { Logger } from '../log/shared/logger';
import { CloseCommand, Command, MessageCommand, NavigationCommand } from './shared/command';
import { CommandType } from './shared/command-type.enum';
import { CommandService } from './shared/command.service';

export class CommandExecutor {
	private executors: Map<CommandType, AbstractCommandExecutor<CommandType>>;

	constructor(
		private commandService: CommandService,
		private $log: Logger
	) {
		this.executors = new Map<CommandType, AbstractCommandExecutor<CommandType>>();

		this.executors.set('MESSAGEBOX', new MessageCommandExecutor());
		this.executors.set('NAVIGATE', new NavigationCommandExecutor());
		this.executors.set('CLOSE', new CloseCommandExecutor());
	}

	execute(command: Command<CommandType>) {
		let executor = this.executors.get(command.commandType);

		if (!executor) {
			this.$log.warn('No executor for command: %o', command);
			return;
		}

		executor.execute(command, this.commandService);
	}
}

abstract class AbstractCommandExecutor<T extends CommandType> {
	abstract execute(command: Command<T>, service: CommandService);
}

class MessageCommandExecutor extends AbstractCommandExecutor<'MESSAGEBOX'> {
	execute(command: MessageCommand, service: CommandService) {
		service.showMessage(command.title, command.message);
	}
}

class NavigationCommandExecutor extends AbstractCommandExecutor<'NAVIGATE'> {
	execute(command: NavigationCommand, _service: CommandService) {
		let href = location.href.substring(0, location.href.indexOf('/#/'));

		if (command.additionalNavigation != null) {
			if (command.additionalNavigation === 'LOGIN') {
				href = href + '/#/login';
			} else if (command.additionalNavigation === 'LOGOUT') {
				href = href + '/#/logout';
			}
		} else if (command.url != null) {
			href = command.url;
		} else {
			href = href + '/#/view/' + command.path;
			if (command.id != null) {
				href = href + '/' + command.id;
			}
			if (command.searchfilterId != null) {
				href = href + '?searchFilterId=' + command.searchfilterId;
			}
		}

		if (command.newTab) {
			window.open(href);
		} else {
			window.location.href = href;
		}
	}
}

class CloseCommandExecutor extends AbstractCommandExecutor<'CLOSE'> {
	execute(_command: CloseCommand, _service: CommandService) {
		window.close();
	}
}
