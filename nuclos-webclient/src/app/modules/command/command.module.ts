import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CommandService } from './shared/command.service';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [],
	providers: [
		CommandService
	]
})
export class CommandModule {
}
