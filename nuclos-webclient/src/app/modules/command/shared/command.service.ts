import { Injectable } from '@angular/core';
import { Command } from '@modules/command/shared/command';
import { take } from 'rxjs/operators';
import { NuclosDialogService } from '../../../core/service/nuclos-dialog.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { CommandExecutor } from '../command-executor';

@Injectable()
export class CommandService {
	private commandExecutor: CommandExecutor;

	constructor(
		private dialogService: NuclosDialogService,
		private $log: Logger
	) {
		this.commandExecutor = new CommandExecutor(this, $log);
	}

	executeCommands(commands: Command<any>[] | undefined) {
		if (!commands) {
			return;
		}

		this.$log.debug('Executing %o commands...', commands.length);
		for (let command of commands) {
			this.commandExecutor.execute(command);
		}
	}

	showMessage(title: string, message: string) {
		this.dialogService.alert({
			title: title,
			message: message
		}).pipe(
			take(1)
		).subscribe();
	}
}
