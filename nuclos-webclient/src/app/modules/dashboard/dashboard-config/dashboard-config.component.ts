import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { SystemParameter } from '@shared/system-parameters';
import { Subject } from 'rxjs';
import { debounceTime, take, takeUntil } from 'rxjs/operators';
import { Addon } from '../../addons/addon.service';
import { EntityMetaData } from '../../entity-object-data/shared/bo-view.model';
import { Preference } from '../../preferences/preferences.model';
import { DropdownComponent } from '../../ui-components/dropdown/dropdown.component';
import { DashboardAddonItem } from '../preference-items/dashboard-addon-item';
import { DashboardTasklistItem } from '../preference-items/dashboard-tasklist-item';
import { DashboardPreferenceContent } from '../shared/dashboard-preference-content';
import { DashboardService } from '../shared/dashboard.service';
import { DashboardChartItem } from '@modules/dashboard/preference-items/dashboard-chart-item';

@Component({
	selector: 'nuc-dashboard-config',
	templateUrl: './dashboard-config.component.html',
	styleUrls: ['./dashboard-config.component.scss']
})
export class DashboardConfigComponent implements OnInit, OnDestroy, OnChanges {
	dynamicTaskListMetas: EntityMetaData[];
	searchfilterTaskListMetas: EntityMetaData[];
	addons: Addon[];
	dynamicChartListMetas: EntityMetaData[];
	searchfilterChartListMetas: EntityMetaData[];

	@Input() dashboard: Preference<DashboardPreferenceContent>;
	@Input() itemsChanged: any;
	@Output() configurationChange = new EventEmitter();
	@ViewChild('dynamicTaskLists', {static: true}) taskListDropdown: DropdownComponent;
	@ViewChild('searchfilterTaskLists', {static: true}) searchfilterDropdown: DropdownComponent;
	@ViewChild('addons') addonDropdown: DropdownComponent;
	@ViewChild('dynamicChartLists', {static: true}) dynamicChartListDropdown: DropdownComponent;
	@ViewChild('searchfilterChartLists', {static: true}) searchfilterChartListDropdown: DropdownComponent;

	private configurationChangeSubject = new Subject();
	private unsubscribe$ = new Subject<boolean>();
	private onChangesSubject: Subject<SimpleChanges> = new Subject<SimpleChanges>();
	private defaultShowTasklistDetailNewTab: boolean;

	constructor(
		private dashboardService: DashboardService,
		private configService: NuclosConfigService
	) {
		this.onChangesSubject.pipe(
			debounceTime(250),
			takeUntil(this.unsubscribe$)
		).subscribe(() => this.updateTaskLists());

		this.configService.getSystemParameters().pipe(
			takeUntil(this.unsubscribe$)
		).subscribe(params =>
			this.defaultShowTasklistDetailNewTab = params.is(SystemParameter.DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB)
		);
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.configurationChangeSubject
			.pipe(debounceTime(250), takeUntil(this.unsubscribe$))
			.subscribe(() => this.configurationChange.emit(this.dashboard));
	}

	ngOnChanges(changes: SimpleChanges): void {
		// Maybe a task list was removed from the dashboard, we must therefore update the task list dropdown
		if (changes['itemsChanged']) {
			this.onChangesSubject.next(changes);
		}
	}

	addTaskList(taskList: EntityMetaData) {
		if (!taskList) {
			return;
		}

		let item = new DashboardTasklistItem(
			taskList.boMetaId,
			taskList.name || 'UNKNOWN',
			taskList.searchfilter,
			this.defaultShowTasklistDetailNewTab
		);

		this.dashboard.content.items.push(item);

		this.dynamicTaskListMetas = this.filterTaskLists(this.dynamicTaskListMetas, false, 'tasklist');
		this.searchfilterTaskListMetas = this.filterTaskLists(this.searchfilterTaskListMetas, true, 'tasklist');
		this.clearInput();

		this.configurationChangeSubject.next(true);
	}

	updateTaskLists() {
		this.dashboardService.getTasklistMetas().pipe(take(1)).subscribe(taskListMetas => {
			this.dynamicTaskListMetas = this.filterTaskLists(taskListMetas, false, 'tasklist');
			this.searchfilterTaskListMetas = this.filterTaskLists(taskListMetas, true, 'tasklist');
			this.dynamicChartListMetas = this.filterTaskLists(taskListMetas, false, '');
			this.searchfilterChartListMetas = this.filterTaskLists(taskListMetas, true, '');
		});
	}

	filterTaskLists(taskLists: EntityMetaData[], searchfilter: boolean, type: string) {
		return taskLists.filter(taskListMeta =>
			!this.isTasklistOnDashboard(taskListMeta, type)
			&& (!searchfilter
			? (taskListMeta.searchfilter === undefined || taskListMeta.searchfilter === null)
			: (taskListMeta.searchfilter !== undefined && taskListMeta.searchfilter !== null))
		);
	}

	addChart(taskList: EntityMetaData) {
		if (!taskList) {
			return;
		}

		let item = new DashboardChartItem(
			taskList.boMetaId,
			taskList.name || 'UNKNOWN',
			taskList.searchfilter
		);

		this.dashboard.content.items.push(item);
		this.clearInput();

		this.configurationChangeSubject.next(true);
	}

	clearInput() {
		window.setTimeout(() => {
			if (this.taskListDropdown) {
				this.taskListDropdown.clearInput();
			}
			if (this.searchfilterDropdown) {
				this.searchfilterDropdown.clearInput();
			}
			if (this.addonDropdown) {
				this.addonDropdown.clearInput();
			}
			if (this.dynamicChartListDropdown) {
				this.dynamicChartListDropdown.clearInput();
			}
			if (this.searchfilterChartListDropdown) {
				this.searchfilterChartListDropdown.clearInput();
			}
		});
	}

	setName(name) {
		this.dashboard.name = name;
		this.configurationChangeSubject.next(true);
	}

	setStartupDefault(startupDefault) {
		this.dashboard.startupDefault = startupDefault;
		this.configurationChangeSubject.next(true);
	}

	getAddons(): Addon[] {
		if (!this.addons) {
			this.addons = this.dashboardService.getAddons();
			this.addons.sort((a, b) => a.name > b.name ? 1 : (a.name < b.name ? -1 : 0));
		}
		return this.addons;
	}

	existAddons(): boolean {
		let dashboardAddons = this.getAddons();
		return dashboardAddons && dashboardAddons.length > 0;
	}

	addAddon(addon: Addon) {
		if (!addon) {
			return;
		}

		let item = new DashboardAddonItem(addon);

		this.dashboard.content.items.push(item);

		this.clearInput();

		this.configurationChangeSubject.next(true);
	}

	private isTasklistOnDashboard(taskListMeta: EntityMetaData, type: string): boolean {
		if (!this.dashboard || !type) {
			return false;
		}

		return !!this.dashboard.content.items.find(
			item =>
				item.type === type
				&& (item as DashboardTasklistItem).taskList.entityClassId === taskListMeta.boMetaId
				&& (item as DashboardTasklistItem).taskList.searchfilter === taskListMeta.searchfilter
		);
	}
}
