import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Logger } from '@nuclos/nuclos-addon-api';
import { ConsoleLogService } from '../../log/shared/console-log.service';
import { DashboardService } from '../shared/dashboard.service';

import { DashboardGridComponent } from './dashboard-grid.component';

xdescribe('DashboardGridComponent', () => {
	let component: DashboardGridComponent;
	let fixture: ComponentFixture<DashboardGridComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [DashboardGridComponent],
			providers: [
				DashboardService,
				{provide: Logger, useValue: new ConsoleLogService()}
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DashboardGridComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
