import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	Output,
	QueryList,
	SimpleChanges,
	ViewChild,
	ViewChildren
} from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { DashboardTasklistConfigComponent } from '@modules/dashboard/items/dashboard-tasklist/dashboard-tasklist-config/dashboard-tasklist-config.component';
import { DashboardTasklistItem } from '@modules/dashboard/preference-items/dashboard-tasklist-item';
import { DashboardGridItem } from '@modules/dashboard/shared/dashboard-grid-item';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { ObjectUtils } from '@shared/object-utils';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { SystemParameter } from '@shared/system-parameters';
import { GridsterConfig, GridsterItemComponent } from 'angular-gridster2';
import { $e } from 'codelyzer/angular/styles/chars';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { SubscriptionHandleDirective } from '../../../data/classes/subscription-handle.directive';
import { Logger } from '../../log/shared/logger';
import { Preference } from '../../preferences/preferences.model';
import { DashboardAddonConfigComponent } from '../items/dashboard-addon/dashboard-addon-config/dashboard-addon-config.component';
import { DashboardAddonItem } from '../preference-items/dashboard-addon-item';
import { DashboardPreferenceContent } from '../shared/dashboard-preference-content';
import { DashboardService } from '../shared/dashboard.service';
import { DashboardChartComponent } from '@modules/dashboard/items/dashboard-chart/dashboard-chart.component';
import { DashboardChartItem } from '@modules/dashboard/preference-items/dashboard-chart-item';

@Component({
	selector: 'nuc-dashboard-grid',
	templateUrl: './dashboard-grid.component.html',
	styleUrls: ['./dashboard-grid.component.css']
})
export class DashboardGridComponent extends SubscriptionHandleDirective implements OnChanges {
	options: GridsterConfig;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;
	tooltips = {};

	@Input() dashboard: Preference<DashboardPreferenceContent>;
	@Input() configMode: boolean;

	@Output() configurationChange = new EventEmitter();

	@ViewChild('configButton') configButton: ElementRef;

	private defaultShowTasklistDetailNewTab: boolean;

	constructor(
		private dashboardService: DashboardService,
		private modalService: NuclosDialogService,
		private $log: Logger,
		private i18n: NuclosI18nService,
		private nuclosHotKeyService: NuclosHotkeysService,
		private configService: NuclosConfigService
	) {
		super();

		this.configService.getSystemParameters().pipe(
			takeUntil(this.unsubscribe$)
		).subscribe(params =>
			this.defaultShowTasklistDetailNewTab = params.is(SystemParameter.DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB)
		);

		this.options = {
			minRows: 2,
			minCols: 2,
			itemChangeCallback: (item, itemComponent) => {
				this.$log.debug('itemChanged', item, itemComponent);
				this.configurationChange.emit();
			},
		};

		this.tooltips['addon'] = this.i18n.getI18n('webclient.dashboard.addon.config');
		this.tooltips['tasklist'] = this.i18n.getI18n('webclient.dashboard.tasklist.config');
		this.tooltips['chart'] = this.i18n.getI18n('webclient.dashboard.chart.config');
	}

	@ViewChildren(GridsterItemComponent)
	set gridsterComponents(gridsterItemComp: QueryList<GridsterItemComponent>) {
		// this is triggered if gridsterItem changes
		this.addShortCuts();
	}

	removeItem($event: MouseEvent | TouchEvent, item: DashboardGridItem) {
		$event.preventDefault();
		$event.stopPropagation();

		let index = this.dashboard.content.items.indexOf(item);

		if (index < 0) {
			this.$log.warn('Could not find item %o', item);
			return;
		}

		if (item.type === 'chart') {
			let chartItem = item as DashboardChartItem;
			chartItem.active = false;
		}

		setTimeout(() => {
			this.dashboard.content.items.splice(index, 1);
			this.configurationChange.emit(this.dashboard);
		});
	}

	showConfig($event: MouseEvent | TouchEvent, item: DashboardGridItem) {
		$event.preventDefault();
		$event.stopPropagation();

		if (item.type === 'addon') {
			let addonItem = item as DashboardAddonItem;
			let unconfiguredItem = this.dashboardService.getAddons().find(
				addon => addon.webAddonId === addonItem.addon.webAddonId);
			if (unconfiguredItem) {
				addonItem.addon.name = unconfiguredItem.name;
				addonItem.addon.propertyDefinitions = unconfiguredItem.propertyDefinitions;
			}
			if (this.configButton) {
				// focus on button to prevent error from other focused components outside the gridster:
				// [Error: ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked.
				// 						  Previous value: 'ng-untouched: true'. Current value: 'ng-untouched: false'
				this.configButton.nativeElement.focus();
			}
			this.modalService.custom(DashboardAddonConfigComponent, {'item': addonItem}, {size: 'lg'})
				.pipe(
					take(1)
				).subscribe(() => {
				this.configurationChange.emit();
			});
		} else if (item.type === 'tasklist') {
			let tasklistItem = item as DashboardTasklistItem;

			if (this.configButton) {
				// focus on button to prevent error from other focused components outside the gridster:
				// [Error: ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked.
				// 						  Previous value: 'ng-untouched: true'. Current value: 'ng-untouched: false'
				this.configButton.nativeElement.focus();
			}

			this.modalService.custom(DashboardTasklistConfigComponent,
				{'defaultShowDetailsNewTab': this.defaultShowTasklistDetailNewTab, 'item': tasklistItem},
				{size: 'sm'})
				.pipe(
					take(1)
				).subscribe(() => {
				this.configurationChange.emit();
			});
		} else if (item.type === 'chart') {
			for (let gridItem of this.getItems()) {
				if (gridItem.type === 'chart') {
					gridItem.active = false;
				}
			}
			item.active = false;
			this.modalService.custom(DashboardChartComponent,
				{'defaultConfigMode': true, 'item': item},
				{size: 'lg', windowClass: 'fullsize-modal-window'})
				.pipe(
					take(1)
				).subscribe(() => {
				for (let gridItem of this.getItems()) {
					if (gridItem.type === 'chart') {
						gridItem.active = true;
					}
				}
				this.configurationChange.emit();
			});
		}
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes.hasOwnProperty('configMode')) {
			let configMode = changes['configMode'];

			if (this.options) {
				this.options.draggable = {
					enabled: configMode.currentValue
				};
				this.options.resizable = {
					enabled: configMode.currentValue
				};

				if (this.options.api && this.options.api.optionsChanged) {
					this.options.api.optionsChanged();
				}
			}
		}
	}

	getItems() {
		return this.dashboard.content.items;
	}

	private addShortCuts() {
		// end subscriptions with completing the unsubscribe
		// we can safely do this here as this function is the only one using it
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
		this.unsubscribe$ = new Subject<boolean>();

		this.getItems().forEach((val, index) => {
			this.nuclosHotKeyService.addShortcut({
				keys: index.toString(),
				modifier: 'ALT',
				description: this.i18n.getI18n('webclient.shortcut.dashboard.activatePanel', (index + 1))
			}).pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
				this.activateElementAndDeactivateOther(val);
			});
		});
	}

	private activateElementAndDeactivateOther(val: DashboardGridItem) {
		val.active = true;
		this.getItems().filter(fn => !ObjectUtils.isEqual(fn, val)).forEach((other) => {
			other.active = false;
		});
	}
}
