import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DashboardNotificationCountComponent } from './dashboard-notification-count.component';

xdescribe('DashboardNotificationCountComponent', () => {
	let component: DashboardNotificationCountComponent;
	let fixture: ComponentFixture<DashboardNotificationCountComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [DashboardNotificationCountComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DashboardNotificationCountComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
