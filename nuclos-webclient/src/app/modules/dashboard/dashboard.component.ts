import { AfterContentInit, Component, OnInit } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { IPreference, UserAction } from '@nuclos/nuclos-addon-api';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { debounceTime, take, takeUntil } from 'rxjs/operators';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../data/classes/subscription-handle.directive';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { Logger } from '../log/shared/logger';
import { Preference } from '../preferences/preferences.model';
import { DashboardAddonItem } from './preference-items/dashboard-addon-item';
import { DashboardPreferenceContent } from './shared/dashboard-preference-content';
import { DashboardService } from './shared/dashboard.service';
import { AuthenticationService } from '@modules/authentication';

@Component({
	selector: 'nuc-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends SubscriptionHandleDirective implements OnInit, AfterContentInit {
	dashboard: Preference<DashboardPreferenceContent>;
	dashboards: Preference<DashboardPreferenceContent>[];

	configMode = false;
	itemsChanged = new Date();
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;
	tooltipPositionLeft: TooltipOptions;
	viewonly = false;

	constructor(
		private $log: Logger,
		private dashboardService: DashboardService,
		private dialogService: NuclosDialogService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService,
		private authenticationService: AuthenticationService
	) {
		super();
	}

	ngOnInit() {
		this.dashboardService.resetNotificationsSince();

		// register shortcuts for switch
		this.nuclosHotkeysService.addShortcut({
			keys: 'T',
			modifier: 'CTRL_ALT',
			description: this.i18n.getI18n('webclient.shortcut.dashboardNext')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.selectNextDashboard();
		});
		this.nuclosHotkeysService.addShortcut({
			keys: 'T',
			modifier: 'CTRL_ALT_SHIFT',
			description: this.i18n.getI18n('webclient.shortcut.dashboardPrevious')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.selectPreviousDashboard();
		});
		this.authenticationService.onAuthenticationDataAvailable().pipe(takeUntil(this.unsubscribe$)).subscribe((ready) => {
			if (ready) {
				this.viewonly = !this.authenticationService.isActionAllowed(UserAction.ConfigureDashboards);
			}
		});

		this.authenticationService.onSelectStartDashboard().pipe(debounceTime(200), takeUntil(this.unsubscribe$)).subscribe((select) => {
			if (select) {
				if (this.dashboards) {
					let startDashboard = this.dashboards.find(pref => pref.startupDefault);
					if (startDashboard && startDashboard !== this.dashboard) {
						this.selectDashboard(startDashboard);
					}
					this.authenticationService.onSelectStartDashboard().next(false);
				} else {
					this.authenticationService.onSelectStartDashboard().next(true);
				}
			}
		});

		this.tooltipPositionLeft = {... this.tooltipDefaults};
		this.tooltipPositionLeft.tooltipPosition = 'left';
	}

	ngAfterContentInit() {
		this.dashboardService
			.getDashboardPrefs()
			.pipe(take(1))
			.subscribe(dashboards => {
				this.$log.debug('ngAfterContentInit: %s', JSON.stringify(dashboards));
				this.setDashboards(dashboards);
			});
	}

	/**
	 * TODO: Is called too often, needs debouncing
	 */
	saveDashboard() {
		if (this.viewonly) {
			return;
		}
		this.itemsChanged = new Date();
		this.dashboard.content.items
			.filter(item => item.type === 'addon')
			.forEach(item => {
				delete (item as DashboardAddonItem).addon.resultlists;
				delete (item as DashboardAddonItem).addon.propertyDefinitions;
			});
		this.dashboardService
			.saveDashboard(this.dashboard)
			.pipe(take(1))
			.subscribe((dashboard: Preference<DashboardPreferenceContent>) => {
				this.$log.info('Dashboard saved: %o', dashboard);

				// TODO: Completely replace the old dashboard object
				this.dashboard.customized = dashboard.customized;
			});
	}

	createNew() {
		let newDashboard = this.dashboardService.createNew();
		this.dashboards.push(newDashboard);
		this.configMode = true;
		this.selectDashboard(newDashboard);
	}

	selectDashboard(dashboard) {
		this.dashboard = dashboard;
		this.dashboardService.selectDashboard(this.dashboard).pipe(take(1)).subscribe();
	}

	toggleConfig() {
		this.configMode = !this.configMode;
	}

	deleteDashboard() {
		this.dialogService
			.confirm({
				title: this.i18n.getI18n('webclient.dashboard.delete'),
				message: this.i18n.getI18n(
					'webclient.dashboard.delete.confirm',
					this.dashboard.name
				)
			})
			.pipe(
				take(1)
			).subscribe((result) => {
			if (result) {
				this.dashboardService.deleteDashboard(this.dashboard).pipe(take(1)).subscribe(() => {
					let index = this.dashboards.indexOf(this.dashboard);
					this.dashboards.splice(index, 1);
					if (index > 0) {
						index--;
					}
					let nextDashboard = this.dashboards[index];
					if (nextDashboard) {
						this.selectDashboard(nextDashboard);
					}
				});
			}
		});
	}

	resetDashboard() {
		this.dashboardService.resetDashboard(this.dashboard).pipe(take(1)).subscribe(uncustomizedDashboard => {
			this.replaceDashboard(uncustomizedDashboard);
		});
	}

	replaceDashboard(dashboard: IPreference<DashboardPreferenceContent>) {
		let index = this.dashboards.findIndex(pref => pref.prefId === dashboard.prefId);
		if (index >= 0) {
			this.dashboards[index] = dashboard;
		}
		if (this.dashboard.prefId === dashboard.prefId) {
			this.dashboard = dashboard;
		}
	}

	private setDashboards(dashboards: Preference<DashboardPreferenceContent>[]) {
		this.dashboards = dashboards;

		let selectedDashboardInService = this.dashboardService.getSelectedDashboard()?.prefId;

		let selected = this.dashboards.find(pref =>
			selectedDashboardInService ? pref.prefId === selectedDashboardInService : pref.selected as boolean);
		if (selected) {
			this.dashboard = selected;
		} else if (this.dashboards.length > 0) {
			this.dashboard = this.dashboards[0];
		}
	}

	private selectNextDashboard() {
		// don't do anything if there are now tabs
		if (this.dashboards.length > 0) {
			const currentIndexOfDashboard = this.getCurrentIndexOfDashboard();
			if (this.dashboards.length > (currentIndexOfDashboard + 1)) {
				this.selectDashboard(this.dashboards[currentIndexOfDashboard + 1]);
			} else {
				// we may have reached the end of the tabs circle to the beginning
				this.selectDashboard(this.dashboards[0]);
			}
		}
	}

	private selectPreviousDashboard() {
		// don't do anything if there are now tabs
		if (this.dashboards.length > 0) {
			const currentIndexOfDashboard = this.getCurrentIndexOfDashboard();
			if ((currentIndexOfDashboard - 1) >= 0) {
				this.selectDashboard(this.dashboards[currentIndexOfDashboard - 1]);
			} else {
				// we may have reached the beginning of the tabs circle to the end
				this.selectDashboard(this.dashboards[this.dashboards.length - 1]);
			}
		}
	}

	private getCurrentIndexOfDashboard() {
		return this.dashboards.indexOf(this.dashboard);
	}
}
