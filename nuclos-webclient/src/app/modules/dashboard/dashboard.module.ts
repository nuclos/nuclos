import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EntityObjectGridModule } from '@modules/entity-object-grid/entity-object-grid.module';
import { GridsterModule } from 'angular-gridster2';
import { SharedModule } from '../../shared/shared.module';
import { EntityObjectModule } from '../entity-object/entity-object.module';
import { I18nModule } from '../i18n/i18n.module';
import { LogModule } from '../log/log.module';
import { UiComponentsModule } from '../ui-components/ui-components.module';
import { DashboardConfigComponent } from './dashboard-config/dashboard-config.component';
import { DashboardGridComponent } from './dashboard-grid/dashboard-grid.component';
import {
	DashboardNotificationCountComponent
} from './dashboard-notification-count/dashboard-notification-count.component';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routes';
import {
	DashboardAddonConfigComponent
} from './items/dashboard-addon/dashboard-addon-config/dashboard-addon-config.component';
import { DashboardAddonComponent } from './items/dashboard-addon/dashboard-addon.component';
import { DashboardTasklistComponent } from './items/dashboard-tasklist/dashboard-tasklist.component';
import { DashboardService } from './shared/dashboard.service';
import {
	DashboardTasklistConfigComponent
} from './items/dashboard-tasklist/dashboard-tasklist-config/dashboard-tasklist-config.component';
import { DashboardChartComponent } from '@modules/dashboard/items/dashboard-chart/dashboard-chart.component';
import { ChartModule } from '@modules/chart/chart.module';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		GridsterModule,
		FormsModule,
		SharedModule,
		EntityObjectModule,
		I18nModule,
		LogModule,
		UiComponentsModule,
		DashboardRoutes,
		EntityObjectGridModule,
		ChartModule
	],
	exports: [
		DashboardNotificationCountComponent,
		DashboardAddonConfigComponent
	],
	declarations: [
		DashboardComponent,
		DashboardGridComponent,
		DashboardConfigComponent,
		DashboardTasklistComponent,
		DashboardAddonComponent,
		DashboardChartComponent,
		DashboardAddonConfigComponent,
		DashboardNotificationCountComponent,
		DashboardTasklistConfigComponent
	],
	providers: [
		DashboardService
	]
})
export class DashboardModule {
}
