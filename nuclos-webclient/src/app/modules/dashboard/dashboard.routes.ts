import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'dashboard',
		data: {
			localizedTitle: 'webclient.dashboard.menu'
		},
		component: DashboardComponent
	}
];

export const DashboardRoutes = RouterModule.forChild(ROUTE_CONFIG);
