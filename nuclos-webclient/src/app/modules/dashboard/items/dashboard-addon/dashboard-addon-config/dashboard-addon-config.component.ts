import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Property } from '../../../../addons/addon.service';
import { DashboardAddonItem } from '../../../preference-items/dashboard-addon-item';

@Component({
	selector: 'nuc-dashboard-addon-config',
	templateUrl: './dashboard-addon-config.component.html',
	styleUrls: ['./dashboard-addon-config.component.css']
})
export class DashboardAddonConfigComponent {

	private static prepareItem(item: DashboardAddonItem) {
		let propertyDefinitionsBackup: Property[] = [];
		if (item.addon.propertyDefinitions) {
			propertyDefinitionsBackup = item.addon.propertyDefinitions;
		}
		// copy propertyDefinitions for editing
		item.addon.propertyDefinitions = propertyDefinitionsBackup.map(propDef => {
				let value = propDef.value;
				if (item.properties) {
					let propFromPreference = item.properties.find(propPref => propPref.name === propDef.name);
					if (propFromPreference && propFromPreference.value) {
						value = propFromPreference.value;
					}
				}
				return DashboardAddonItem.copyPropertyAndSetNewValue(propDef, value);
		});
		if (item.addon.propertyDefinitions) {
			item.addon.propertyDefinitions.sort((a, b) => a.name > b.name ? 1 : (a.name < b.name ? -1 : 0));
		}
	}

	private static saveChanges(item: DashboardAddonItem) {
		if (item.addon.propertyDefinitions) {
			item.properties = item.addon.propertyDefinitions;
		}
	}

	_item: DashboardAddonItem;

	constructor(private activeModal: NgbActiveModal) {
	}

	@Input()
	set item(item: DashboardAddonItem) {
		DashboardAddonConfigComponent.prepareItem(item);
		this._item = item;
	}

	closeModal() {
		this.activeModal.close();
	}

	cancelConfig() {
		this.closeModal();
	}

	saveConfig() {
		DashboardAddonConfigComponent.saveChanges(this._item);
		this.closeModal();
	}
}
