import {
	Component,
	ComponentFactoryResolver,
	Input, OnChanges,
	OnInit,
	ReflectiveInjector, SimpleChanges,
	ViewChild,
	ViewContainerRef
} from '@angular/core';
import { DASHBOARD_CONTEXT } from '@nuclos/nuclos-addon-api';
import { DashboardContextImplementation } from '../../../addons/addon-api-implementation';
import { AddonService } from '../../../addons/addon.service';
import { Logger } from '../../../log/shared/logger';
import { DashboardAddonItem } from '../../preference-items/dashboard-addon-item';

@Component({
	selector: 'nuc-dashboard-addon',
	templateUrl: './dashboard-addon.component.html',
	styleUrls: ['./dashboard-addon.component.css']
})
export class DashboardAddonComponent implements OnInit {

	@Input() item: DashboardAddonItem;

	@ViewChild('addonViewContainer', {read: ViewContainerRef, static: true})
	addonViewContainerRef: ViewContainerRef;

	message: string;

	constructor(
		private addonService: AddonService,
		private componentFactoryResolver: ComponentFactoryResolver,
		private dashboardContextImplementation: DashboardContextImplementation,
		private $log: Logger
	) {
	}

	@Input()
	set active(val: boolean) {
		if (val) {
			this.$log.debug('This should activate it');
		} else {
			this.$log.debug('This should deactivate it');
		}
	}

	ngOnInit() {
		// create a new object from the dashboardContextImplementation singleton to make it possible to use multiple addon instances
		let dashboardContextImplementation = Object.create(this.dashboardContextImplementation);
		dashboardContextImplementation.setDashboardAddonItem(this.item);

		let inputProviders = [
			{provide: DASHBOARD_CONTEXT, useValue: dashboardContextImplementation},
		];
		let resolvedInputs = ReflectiveInjector.resolve(inputProviders);

		// injector provides the input data to the new component
		let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.addonViewContainerRef.parentInjector);

		const cmpRef = this.addonService.instantiateComponent(this.item.addon.name + 'Component', injector);
		if (cmpRef) {
			this.addonViewContainerRef.insert(cmpRef.hostView);
		} else {
			const msgUnableToOpen = 'Unable to open addon: ' + this.item.addon.name;
			this.$log.error(msgUnableToOpen);
			this.message = msgUnableToOpen;
		}
	}
}
