import {
	Component,
	Input, OnDestroy,
	OnInit
} from '@angular/core';
import { FqnService } from '@shared/service/fqn.service';
import { Logger } from '../../../log/shared/logger';
import { DashboardChartItem } from '@modules/dashboard/preference-items/dashboard-chart-item';
import { Subject } from 'rxjs';
import { LocalStorageService } from '@shared/service/local-storage.service';
import { ChartService } from '@modules/chart/shared/chart.service';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityObjectData } from '@modules/entity-object-data/shared/bo-view.model';
import { EoChartWrapper } from '@modules/chart/shared/eo-chart-wrapper';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { ObjectUtils } from '@shared/object-utils';

@Component({
	selector: 'nuc-dashboard-chart',
	templateUrl: './dashboard-chart.component.html',
	styleUrls: ['./dashboard-chart.component.css']
})
export class DashboardChartComponent implements OnInit, OnDestroy {
	@Input() item: DashboardChartItem;
	eoChart: EoChartWrapper;

	@Input() defaultConfigMode = false;
	private eo: EntityObject;
	private unsubscribe$ = new Subject<boolean>();
	private itemBackup: DashboardChartItem;

	constructor(
		private localStorageService: LocalStorageService,
		private chartService: ChartService,
		private dialogService: NuclosDialogService,
		private fqnService: FqnService,
		private $log: Logger
	) {
	}

	@Input()
	set active(val: boolean) {
		if (val) {
			this.$log.debug('This should activate it');
		} else {
			this.$log.debug('This should deactivate it');
		}
	}

	ngOnDestroy() {
		if (this.chartService.isShowConfig()) {
			this.chartService.toggleConfig();
		}
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.itemBackup = ObjectUtils.cloneDeep(this.item);
		let eoData = new EntityObjectData();
		eoData.boMetaId = this.item.chart.chart.primaryChart.boMetaId;
		this.eo = new EntityObject(eoData, this.localStorageService, this.fqnService);
		this.eoChart = this.chartService.newChart(this.eo);
		this.eoChart.setChartOptions(this.itemBackup.chart.chart);
		this.eoChart.chartPreference.content = this.itemBackup.chart;
		if (this.chartService.isShowConfig() !== this.defaultConfigMode) {
			this.chartService.toggleConfig();
		}
	}

	closeModal() {
		this.dialogService.closeCurrentModal();
	}

	saveConfig() {
		this.item.chart = this.itemBackup.chart;
		this.closeModal();
	}

	cancelConfig() {
		this.closeModal();
	}
}
