import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardTasklistConfigComponent } from './dashboard-tasklist-config.component';

describe('DashboardTasklistConfigComponent', () => {
	let component: DashboardTasklistConfigComponent;
	let fixture: ComponentFixture<DashboardTasklistConfigComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [DashboardTasklistConfigComponent]
		})
			.compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(DashboardTasklistConfigComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
