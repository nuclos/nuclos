import { Component, Input, OnInit } from '@angular/core';
import { DashboardTasklistItem } from '@modules/dashboard/preference-items/dashboard-tasklist-item';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'nuc-dashboard-tasklist-config',
	templateUrl: './dashboard-tasklist-config.component.html',
	styleUrls: ['./dashboard-tasklist-config.component.css']
})
export class DashboardTasklistConfigComponent implements OnInit {

	_item: DashboardTasklistItem;
	private _itemBackup: DashboardTasklistItem;
	private _defaultShowDetailsNewTab: boolean;

	constructor(private activeModal: NgbActiveModal) {
	}

	@Input()
	set defaultShowDetailsNewTab(defaultShowDetailsNewTab: boolean) {
		this._defaultShowDetailsNewTab = defaultShowDetailsNewTab;
	}

	@Input()
	set item(item: DashboardTasklistItem) {
		this._itemBackup = item;
		this._item = this.cloneItem(item);
		if (this._item.taskList.showDetailsNewTab === undefined) {
			this._item.taskList.showDetailsNewTab = this._defaultShowDetailsNewTab;
		}
	}

	ngOnInit(): void {
	}

	closeModal() {
		this.activeModal.close();
	}

	cancelConfig() {
		this.closeModal();
	}

	saveConfig() {
		this._itemBackup.taskList.showDetailsNewTab = this._item.taskList.showDetailsNewTab;
		this.closeModal();
	}

	private cloneItem(item: DashboardTasklistItem): DashboardTasklistItem {
		return new DashboardTasklistItem(
			item.taskList.entityClassId,
			item.taskList.name,
			item.taskList.searchfilter,
			item.taskList.showDetailsNewTab
		);
	}
}
