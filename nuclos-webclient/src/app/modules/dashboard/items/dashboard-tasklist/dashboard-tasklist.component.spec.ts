import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DashboardTasklistComponent } from './dashboard-tasklist.component';

xdescribe('DashboardTasklistComponent', () => {
	let component: DashboardTasklistComponent;
	let fixture: ComponentFixture<DashboardTasklistComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [DashboardTasklistComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DashboardTasklistComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
