import { Component, Input, OnInit } from '@angular/core';
import { AuthenticationService } from '@modules/authentication';
import { UserAction } from '@nuclos/nuclos-addon-api';
import { ColDef, ColumnEvent, GridOptions, RowNode } from 'ag-grid-community';
import { EMPTY, Observable, Subject, Subscription } from 'rxjs';
import { catchError, take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../../core/service/nuclos-i18n.service';
import { TaskService } from '../../../../core/service/task.service';
import { SubscriptionHandleDirective } from '../../../../data/classes/subscription-handle.directive';
import { ColumnLayoutChanges } from '../../../../layout/shared/column-layout-changes';
import { Mixin } from '../../../../shared/mixin';
import { NuclosHotkeysService } from '../../../../shared/service/nuclos-hotkeys.service';
import { BoViewModel, EntityMeta } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectNavigationService } from '../../../entity-object-data/shared/entity-object-navigation.service';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';
import { MetaService } from '../../../entity-object-data/shared/meta.service';
import { SortModel } from '../../../entity-object-data/shared/sort.model';
import { EntityObjectGridColumn } from '../../../entity-object-grid/entity-object-grid-column';
import { EntityObjectGridService } from '../../../entity-object-grid/entity-object-grid.service';
import { SideviewmenuService } from '../../../entity-object/shared/sideviewmenu.service';
import { Logger } from '../../../log/shared/logger';
import {
	AttributeSelectionContent,
	ColumnAttribute,
	Preference,
	PreferenceType,
	SideviewmenuPreferenceContent
} from '../../../preferences/preferences.model';
import { PreferencesService } from '../../../preferences/preferences.service';
import { DashboardTasklistItem } from '../../preference-items/dashboard-tasklist-item';
import { TaskListPreferenceContent } from '../../shared/task-list-preference-content';

@Component({
	selector: 'nuc-dashboard-tasklist',
	templateUrl: './dashboard-tasklist.component.html',
	styleUrls: ['./dashboard-tasklist.component.css']
})
@Mixin([ColumnLayoutChanges])
export class DashboardTasklistComponent extends SubscriptionHandleDirective implements OnInit, ColumnLayoutChanges {
	/* mixins */
	// tslint:disable-next-line
	columnLayoutChanged: Subject<Date> = new Subject<Date>();
	// tslint:disable-next-line
	columnLayoutChangedSubscription: Subscription;

	@Input() item: DashboardTasklistItem;

	bos: EntityObject[] = [];
	visible = false;

	gridOptions: GridOptions = <GridOptions>{};
	gridColumns: EntityObjectGridColumn[] = [];

	private subscriptions: Subscription = new Subscription();

	private attributeSelection?: Preference<AttributeSelectionContent>;
	private taskListMeta;
	private _tasklistPreference?: Preference<TaskListPreferenceContent>;

	// The full entity meta (including attributes) for the current task list
	// TODO: It is very ugly that we must fetch 2 meta definitions here.
	private taskListEntityMeta?: EntityMeta;

	private sortModel: SortModel;

	private _active: boolean;

	constructor(
		private taskService: TaskService,
		private $log: Logger,
		private eoGridService: EntityObjectGridService,
		private eoNavigationService: EntityObjectNavigationService,
		private preferenceService: PreferencesService,
		private sideviewmenuService: SideviewmenuService,
		private metaService: MetaService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService,
		private authenticationService: AuthenticationService
	) {
		super();
	}

	get active(): boolean {
		return this._active;
	}

	@Input()
	set active(val: boolean) {
		this._active = val;
		if (val) {
			if (this.gridOptions.api) {
				if (this.gridOptions.api.getRenderedNodes().length > 0) {
					this.gridOptions.api.getRenderedNodes()[0].selectThisNode(true);
				}
			}
		} else {
			if (this.gridOptions.api) {
				this.gridOptions.api.deselectAll();
			}
		}
	}

	onColumnChangesDebounced(gridOptions: GridOptions): Observable<Date> {
		return {} as Observable<Date>;
	}

	getColDef(gridOptions: GridOptions, event: ColumnEvent): ColDef | undefined {
		return;
	}

	ngOnInit() {
		this.taskService
			.getTaskListDefinition(this.item.taskList.entityClassId)
			.pipe(take(1))
			.subscribe(taskListMeta => {
				this.metaService.getEntityMeta(taskListMeta.getEntityClassId()).pipe(take(1)).subscribe(
					// Get a full entity meta from the meta service
					entityMeta => {
						this.taskListEntityMeta = entityMeta;
						this.taskListMeta = taskListMeta;
						this.loadPreference();
					}
				);
			});

		this.gridOptions.onSortChanged = () => {
			if (this._tasklistPreference) {
				// prevent saving a preference which was just deleted
				if (this._tasklistPreference.prefId === undefined) {
					return;
				}

				if (this.gridOptions.api) {
					// update preference
					this._tasklistPreference.content.columns.forEach(col => delete col.sort);
					this.gridOptions.columnApi?.getColumnState().forEach((sortModelColumn) => {
						if (this._tasklistPreference) {
							let column = this._tasklistPreference.content.columns
								.filter(co => co.boAttrId === sortModelColumn.colId)
								.shift();
							if (column) {
								column.sort = {
									direction: sortModelColumn.sort,
									enabled: true,
									prio: sortModelColumn.sortIndex
								};
							}
						}
					});
				}

				this.saveTasklistPreference();
			}
		};

		this.nuclosHotkeysService.addShortcut({
			keys: 'ENTER',
			modifier: 'ALT',
			description: this.i18n.getI18n('webclient.short.dashboard.panel.enter')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			if (!this.active) {
				return;
			}
			if (this.gridOptions.api) {
				this.rowClicked({
					node: this.gridOptions.api.getSelectedNodes()[0]
				});
			}
		});
		this.nuclosHotkeysService.addShortcut({
			keys: 'DOWN',
			modifier: 'ALT',
			description: this.i18n.getI18n('webclient.short.dashboard.panel.selectNext')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			if (!this.active) {
				return;
			}
			if (this.gridOptions.api) {
				if (this.gridOptions.api.getRenderedNodes().length > 0) {
					const currentIndex = this.gridOptions.api.getRenderedNodes()
						.findIndex(row => row.isSelected());

					if (currentIndex + 1 < this.gridOptions.api.getRenderedNodes().length) {
						this.gridOptions.api.deselectAll();
						this.gridOptions.api.getRenderedNodes()[currentIndex + 1].selectThisNode(true);
					}
				}
			}
		});
		this.nuclosHotkeysService.addShortcut({
			keys: 'UP',
			modifier: 'ALT',
			description: this.i18n.getI18n('webclient.short.dashboard.panel.selectPrev')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			if (!this.active) {
				return;
			}
			if (this.gridOptions.api) {
				if (this.gridOptions.api.getRenderedNodes().length > 0) {
					const currentIndex = this.gridOptions.api.getRenderedNodes()
						.findIndex(row => row.isSelected());

					if (currentIndex - 1 >= 0) {
						this.gridOptions.api.deselectAll();
						this.gridOptions.api.getRenderedNodes()[currentIndex - 1].selectThisNode(true);
					}
				}
			}
		});

		this.subscriptions.add(
			this.onColumnChangesDebounced(this.gridOptions).subscribe(() => {
				if (this._tasklistPreference) {
					this._tasklistPreference.content.columns = this.sideviewmenuService.getSelectedAttributesFromGrid(
						this.gridOptions,
						this.taskListMeta !== undefined ? this.taskListMeta : this.taskListEntityMeta
					);

					this.saveTasklistPreference();
				}
				return;
			})
		);
	}

	ngOnDestroy() {
		this.subscriptions.unsubscribe();
	}

	gridReady() {
		this.gridOptions.api!.setRowData(this.bos);
	}

	rowClicked(params: { node: RowNode }) {
		let eo: EntityObject = params.node.data;

		let entityClassId = eo.getEntityClassId();
		let taskListId = undefined;
		if (this.taskListEntityMeta!.isTaskListForceJumpIntoDetailBo()) {
			let detailEntityClassId = this.taskListEntityMeta!.getDetailEntityClassId();
			if (detailEntityClassId) {
				entityClassId = detailEntityClassId;
				taskListId = this.taskListMeta.getTaskMetaId();
			}
		}

		this.eoNavigationService.navigateToEoById(
			entityClassId,
			eo.getId()!,
			this.item.taskList.showDetailsNewTab,
			this.item.taskList.searchfilter,
			taskListId
		);
	}

	private saveTasklistPreference() {
		if (this._tasklistPreference) {
			if (
				this.authenticationService.isActionAllowed(
					UserAction.WorkspaceCustomizeEntityAndSubFormColumn
				)
			) {
				this.preferenceService.savePreferenceItem(this._tasklistPreference)
					.pipe(take(1))
					.subscribe();
			}
		}
	}

	private loadTaskData() {
		// TODO: Auto-refresh, even in background (via service)
		this.taskService
			.getTaskData(this.item.taskList, this.attributeSelection)
			.pipe(
				take(1),
				catchError(e => {
					this.$log.warn(e);

					// Prevents redirect to error page
					return EMPTY;
				})
			)
			.subscribe((result: BoViewModel) => {
				this.bos = result.bos;
				this.visible = true;
			});
	}

	private loadPreference() {
		this.preferenceService
			.getPreferences({
				type: [PreferenceType.tasklistTable.name]
			})
			.pipe(take(1))
			.subscribe((prefs: Array<Preference<TaskListPreferenceContent>>) => {
				if (prefs) {
					this._tasklistPreference = prefs.find(
						pref =>
								(this.taskListMeta.getTaskMetaId() !== null
								&& pref.content.taskListId === this.taskListMeta.getTaskMetaId())
							||  (this.taskListMeta.getTaskMetaId() === null
								&& pref.content.searchFilterId === this.item.taskList.searchfilter)
					);

					this.attributeSelection = this._tasklistPreference;

					if (this.attributeSelection) {
						this.applyColumnAndSortingPreference(this.attributeSelection);
					} else {
						// No task list table preference found -> try to load normal sidebar column preference.
						// Sidebar prefs are all we have for search filter based task lists.
						this.sideviewmenuService
							.loadSideviewmenuPreference(this.taskListMeta.getEntityClassId())
							.pipe(take(1))
							.subscribe(pref => {
								if (!pref && this.taskListEntityMeta) {
									// Create default table preference if nothing else is available
									pref = this.sideviewmenuService.emptySideviewmenuPreference(
										this.taskListEntityMeta,
										true
									);
								}

								this._tasklistPreference = this.createNewTasklistPreference(pref as Preference<
									SideviewmenuPreferenceContent
									>);

								this.applyColumnAndSortingPreference(pref as Preference<
									SideviewmenuPreferenceContent
								>);
							});
					}
				}
			});
	}

	private createNewTasklistPreference(pref: Preference<SideviewmenuPreferenceContent>): Preference<TaskListPreferenceContent> {
		let tasklistPref: Preference<TaskListPreferenceContent> = {
			content: {
				taskListId: this.taskListMeta.getTaskMetaId(),
				searchFilterId: this.item.taskList.searchfilter,
				columns: pref.content.columns,
				userdefinedName: false
			},
			type: 'tasklist-table',
			boMetaId: this.taskListEntityMeta!.getEntityClassId()
		};

		this.preferenceService.savePreferenceItem(tasklistPref).pipe(take(1)).subscribe();

		return tasklistPref;
	}

	private applyColumnAndSortingPreference(pref: Preference<AttributeSelectionContent>) {
		if (!pref) {
			return;
		}

		// Fix incomplete tasklist-table preferences, generated by the desktop client
		// See NUCLOS-7828, NUCLOS-7893
		pref.content.columns.forEach(column => {
			let entityClassId = this.taskListEntityMeta!.getEntityClassId();

			if (!column.boAttrId && column.name) {
				column.boAttrId = column.name;
			}

			if (column.boAttrId && !column.boAttrId.startsWith(entityClassId)) {
				column.boAttrId = entityClassId + '_' + column.boAttrId;
			}

			column.selected = true;
		});

		this.attributeSelection = pref;
		let columns = this.attributeSelection.content.columns;

		if (columns.length === 0) {
			for (const entityAttrMeta of this.taskListEntityMeta!.getAttributes().values()) {
				columns.push({...entityAttrMeta.getMetaData(), selected: true} as ColumnAttribute);
			}
		}

		// TODO: Refactor - do not use SideviewmenuService here
		let sortedColumns = this.sideviewmenuService.sortColumns(columns);

		this.setColumns(sortedColumns);
		this.sortModel = new SortModel(this.sideviewmenuService.getAgGridSortModel(columns, pref));

		this.$log.debug('SortModel = %o, sortedColumns = %o', this.sortModel, sortedColumns);

		this.loadTaskData();
	}

	private setColumns(columns: ColumnAttribute[]) {
		if (!this.taskListEntityMeta) {
			this.$log.warn('No task list entity meta');
			return;
		}

		let newColumns = columns
			.filter(col => !col.boAttrId.toUpperCase().endsWith('NUCLOSROWCOLOR'))
			.map(col => {
				let attributeMeta;

				if (col.boAttrId) {
					attributeMeta = this.taskListEntityMeta!.getAttributeMetaByFqn(col.boAttrId);
				} else if (col.name) {
					// Try to get the attribute meta by name as a fallback
					attributeMeta = this.taskListEntityMeta!.getAttributeMeta(col.name);
				}

				if (!attributeMeta) {
					this.$log.warn('Could not find attribute meta for column %o', col);
					return {};
				}

				let column = this.eoGridService.createColumnDefinition(attributeMeta);

				column.width = col.width;
				column.pinned = col.fixed;

				return column;
			})
			.filter(col => col.colId);

		if (newColumns.length > 0) {
			this.gridColumns = newColumns;
		}
	}
}
