import { Addon, Property } from '../../addons/addon.service';
import { DashboardGridItem } from '../shared/dashboard-grid-item';

export class DashboardAddonItem extends DashboardGridItem {

	static copyProperty(prop: Property): Property {
		return this.copyPropertyAndSetNewValue(prop, prop.value);
	}

	static copyPropertyAndSetNewValue(prop: Property, newValue: any): Property {
		let value = newValue;
		if (value === undefined) {
			if (prop.type === 'boolean') {
				value = false;
			} else if (prop.type === 'number') {
				value = 0;
			} else {
				value = '';
			}
		}
		return {
			name: prop.name,
			type: prop.type,
			value: value
		} as Property;
	}

	addon: Addon;
	properties: Property[];

	constructor(addon: Addon) {
		super();

		this.type = 'addon';
		this.active = false;
		if (addon.propertyDefinitions && addon.propertyDefinitions.length > 0) {
			this.configurable = addon.propertyDefinitions.length > 0;
		} else {
			this.configurable = false;
		}
		if (addon.propertyDefinitions) {
			this.properties = addon.propertyDefinitions.map(propDef => DashboardAddonItem.copyProperty(propDef));
		}

		this.addon = {
			webAddonId: addon.webAddonId,
			name: addon.name
		};
	}
}
