import { ChartPreferenceContent } from '@modules/preferences/preferences.model';
import { DashboardGridItem } from '@modules/dashboard/shared/dashboard-grid-item';

export class DashboardChartItem extends DashboardGridItem {
	chart: ChartPreferenceContent;

	constructor(
		boMetaId: string,
		name: string,
		searchFilterId?: string
	) {
		super();

		this.type = 'chart';
		this.configurable = true;
		this.active = true;

		this.chart = {
			chart: {
				type: 'multiBar',
				primaryChart: {
					boMetaId: boMetaId,
					refBoAttrId: '',
					categoryBoAttrId: '',
					series: []
				}
			},
			name: {
				de: name
			}
		};
	}
}
