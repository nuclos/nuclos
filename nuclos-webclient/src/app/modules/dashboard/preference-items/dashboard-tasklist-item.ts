import { TaskList } from '../../../data/schema/task-list';
import { DashboardGridItem } from '../shared/dashboard-grid-item';

export class DashboardTasklistItem extends DashboardGridItem {
	taskList: TaskList;

	constructor(
		taskListId: string,
		taskListName: string,
		searchFilterId?: string,
		showDetailsNewTab?: boolean
	) {
		super();

		this.type = 'tasklist';
		this.configurable = true;
		this.active = false;

		this.taskList = {
			entityClassId: taskListId,
			name: taskListName,
			searchfilter: searchFilterId,
			showDetailsNewTab: showDetailsNewTab
		};
	}
}
