export type DashboardGridItemType = 'tasklist' | 'addon' | 'chart';

export class DashboardGridItem {

	type: DashboardGridItemType;
	configurable: boolean;

	x: number;
	y: number;

	rows?: number;
	cols?: number;

	active: boolean;
}
