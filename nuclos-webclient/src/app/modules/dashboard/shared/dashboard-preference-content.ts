import { PreferenceContent } from '../../preferences/preferences.model';
import { DashboardGridItem } from './dashboard-grid-item';

export class DashboardPreferenceContent extends PreferenceContent {
	items: DashboardGridItem[] = [];
}
