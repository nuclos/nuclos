import { inject, TestBed } from '@angular/core/testing';

import { AutonumberService } from './autonumber.service';

xdescribe('AutonumberService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [AutonumberService]
		});
	});

	it('should be created', inject([AutonumberService], (service: AutonumberService) => {
		expect(service).toBeTruthy();
	}));
});
