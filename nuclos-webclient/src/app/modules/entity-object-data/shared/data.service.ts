import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { SearchConditionBuilder } from '@modules/entity-object-data/shared/search-condition-builder';
import { LocalStorageService } from '@shared/service/local-storage.service';
import { FORBIDDEN } from 'http-status-codes';
import * as _ from 'lodash';
import * as hash from 'object-hash';
import { EMPTY, Observable, Observer, of, throwError } from 'rxjs';
import { catchError, finalize, map, mergeMap, switchMap, take, tap } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { ValidationStatus } from '../../../data/schema/validation-status.enum';
import { DatetimeService } from '../../../shared/service/datetime.service';
import { FqnService } from '../../../shared/service/fqn.service';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { AddonService } from '../../addons/addon.service';
import { NuclosCacheService } from '../../cache/shared/nuclos-cache.service';
import { Logger } from '../../log/shared/logger';
import {
	AttributeSelectionContent,
	ColumnAttribute,
	Preference,
	SearchtemplatePreferenceContent, SidebarLayoutType,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { WhereCondition } from '../../search/shared/search.model';
import { SearchService } from '../../search/shared/search.service';
import { SearchfilterService } from '../../search/shared/searchfilter.service';
import { BoViewModel, EntityAttrMeta, EntityMeta, EntityObjectData } from './bo-view.model';
import { EntityObjectErrorService } from './entity-object-error.service';
import { EntityObjectResultService } from './entity-object-result.service';
import { EntityObject } from './entity-object.class';
import { EntityVLPService } from './entity-v-l-p.service';
import { MetaService } from './meta.service';
import { ResultParams } from './result-params';

export interface QueryObject {
	where: WhereCondition | undefined;
	multiSelectionCondition?: MultiSelectionResult;
	editSelectionResult?: EditSelectionResult;
}

export interface LoadContext {
	meta: EntityMeta;
	vlpId: string | undefined;
	vlpParams: HttpParams | undefined;
	multiSelectionCondition?: MultiSelectionResult;
	editSelectionResult?: EditSelectionResult;
	searchFilter?: string | undefined;
	attributeSelection?: Preference<AttributeSelectionContent>;
	searchtemplatePreference?: Preference<SearchtemplatePreferenceContent>;
	resultParams?: ResultParams;
}

export interface LoadInnerContext extends LoadContext {
	search: string;
	columns;
	sort;
	resultParams: ResultParams;
}

/**
 * TODO: Split this into more meaningful separate services, instead of one monolith for everything.
 * TODO: Methods are too complex and have too many parameters.
 */
@Injectable()
export class DataService {

	eoRequests = new Map<string, Observable<BoViewModel>>();

	constructor(
		private http: HttpClient,
		private nuclosConfig: NuclosConfigService,
		private fqnService: FqnService,
		private searchService: SearchService,
		private searchfilterService: SearchfilterService,
		private dialogService: NuclosDialogService,
		private addonService: AddonService,
		private i18n: NuclosI18nService,
		private dateTimeService: DatetimeService,
		private $log: Logger,
		private cache: NuclosCacheService,
		private eoResultService: EntityObjectResultService,
		private metaService: MetaService,
		private eoVLPService: EntityVLPService,
		private eoErrorService: EntityObjectErrorService,
		private localStorageService: LocalStorageService,
	) {
	}

	executeCollectiveProcessing(
		context: LoadContext,
		restUrl: string
	): Observable<CollectiveProcessingExecution> {
		let innerContext = this.buildInnerContext(context);
		let queryParams = this.buildQueryParams(innerContext);
		let queryObject = this.buildQueryObject(innerContext);
		return queryObject.pipe(
			switchMap(q => {
				return this.http.post(restUrl, q, {params: queryParams}).pipe(
					tap((result: CollectiveProcessingExecution) => {
						this.$log.debug('CollectiveProccesingExecution before map', result);
					}),
					catchError(e => {
						this.$log.warn(
							'Could not execute collective processing %o for entity meta %o with params %o: %o',
							restUrl,
							context.meta,
							queryParams,
							e
						);
						if (e.status === FORBIDDEN) {
							// Re-throw error 403 - will be handled globally
							return throwError(e);
						} else {
							return EMPTY;
						}
					})
				);
			})
		);
	}

	getCollectiveProcessingInformation(
		context: LoadContext,
		restUrl: string
	): Observable<any> {
		let innerContext = this.buildInnerContext(context);
		let queryParams = this.buildQueryParams(innerContext);
		let queryObject = this.buildQueryObject(innerContext);
		return queryObject.pipe(
			switchMap(q => {
				return this.http.post(restUrl, q, {params: queryParams}).pipe(
					tap((result: any) => {
						this.$log.debug('CollectiveProcessingInformation before map', result);
					}),
					catchError(e => {
						this.$log.warn(
							'Could not execute collective processing %o for entity meta %o with params %o: %o',
							restUrl,
							context.meta,
							queryParams,
							e
						);
						if (e.status === FORBIDDEN) {
							// Re-throw error 403 - will be handled globally
							return throwError(e);
						} else {
							return EMPTY;
						}
					})
				);
			})
		);
	}

	abortCollectiveProcessing(
		restUrl: string
	): Observable<Object> {
		return this.http.get(restUrl).pipe(
			tap((result: Object) => {
				this.$log.debug('CollectiveProccesingAbortion before map', result);
			})
		);
	}

	loadCollectiveProcessingSelectionOptions(
		context: LoadContext
	): Observable<CollectiveProcessingSelectionOptions> {
		let innerContext = this.buildInnerContext(context);
		let queryParams = this.buildQueryParams(innerContext);
		let queryObject = this.buildQueryObject(innerContext);

		this.$log.debug(
			'Loading collective processing selection options for %o...',
			context.meta.getEntityClassId()
		);


		return queryObject.pipe(
			switchMap(q => {
				return this.http.post(this.nuclosConfig.getRestHost() +
					'/collectiveProcessing/' +
					context.meta.getEntityClassId(), q, {params: queryParams})
					.pipe(
						catchError(e => {
							this.$log.warn(
								'Could not load collective processing selection options for entity meta %o with params %o: %o',
								context.meta,
								queryParams,
								e
							);
							if (e.status === FORBIDDEN) {
								return throwError(e);
							} else {
								return EMPTY;
							}
						}),
						map((result: CollectiveProcessingSelectionOptions) => {
							return result;
						})
					);
			})
		);
	}

	/**
	 *  @deprecated: Too many parameters, use loadEoList(LoadContext)
	 */
	loadList(
		meta: EntityMeta,
		vlpId: string | undefined,
		vlpParams: HttpParams | undefined,
		attributeSelection?: Preference<AttributeSelectionContent>,
		searchtemplatePreference?: Preference<SearchtemplatePreferenceContent>,
		resultParams = ResultParams.DEFAULT
	): Observable<BoViewModel | undefined> {
		let context: LoadContext = {
			meta: meta,
			vlpId: vlpId,
			vlpParams: vlpParams,
			attributeSelection: attributeSelection,
			searchtemplatePreference: searchtemplatePreference,
			resultParams: resultParams
		};

		return this.loadEoList(context);
	}

	loadEoList(context: LoadContext, loadInnerContext?: LoadInnerContext): Observable<BoViewModel | undefined> {
		let innerContext = (loadInnerContext) ? loadInnerContext : this.buildInnerContext(context);
		let queryParams = this.buildQueryParams(innerContext);

		return new Observable<BoViewModel>((observer: Observer<BoViewModel | undefined>) => {
			this.buildQueryObject(innerContext).subscribe(queryObject => {
				this.loadEoData(innerContext.meta, queryParams, queryObject).subscribe(model => {
					observer.next(model);
					observer.complete();
				});
			});
		});
	}

	/**
	 * Loads an EO result for the given entity and query params.
	 * Results for identical requests will be cached in EOResultService!
	 */
	loadEoData(
		meta: EntityMeta,
		queryParams,
		queryObject: { where: WhereCondition | undefined } = {where: undefined}
	): Observable<BoViewModel | undefined> {
		this.$log.debug('Loading EO list for %o...', meta.getEntityClassId());

		let requestHash = hash({
			meta: meta,
			queryParams: queryParams,
			queryObject: queryObject
		});

		let observable = this.http
			.post(
				this.nuclosConfig.getRestHost() + '/bos/' + meta.getEntityClassId() + '/query',
				queryObject,
				{params: queryParams}
			)
			.pipe(
				catchError(e => {
					this.$log.warn(
						'Could not load EO data for entity meta %o with params %o: %o',
						meta,
						queryParams,
						e
					);
					if (e.status === FORBIDDEN) {
						// Re-throw error 403 - will be handled globally
						return throwError(e);
					} else {
						return this.dialogService.alert({
							message: e.error?.message ? e.error.message : e.toString(),
							title: this.i18n.getI18n('webclient.error.title')
						});
					}
				}),
				map((result: BoViewModel | undefined) => {
					let entityObjects: EntityObject[] = [];
					if (result !== undefined) {
						for (let bo of result.bos) {
							let eo = new EntityObject(<any>bo, this.localStorageService, this.fqnService);
							entityObjects.push(eo);
							eo.prefillAttributeValidationStatus(ValidationStatus.VALID);
							// this.eoVLPService.addEntityObjectMap(eo);
						}
						result.bos = entityObjects;
					}
					return result;
				}),
				finalize(() => {
					this.$log.debug(
						'Loading EO list -> hash = %o, deleting after request finished!',
						requestHash
					);
					this.eoRequests.delete(requestHash);
				})
			);

		return this.eoResultService.cachedResults(requestHash, observable);
	}

	formatAttribute(value, attrMeta: EntityAttrMeta): string {
		let result = value;
		if (value.name !== undefined) {
			result = value.name !== null ? value.name : '';
		} else if (attrMeta.isBoolean()) {
			result = value ? this.i18n.getI18n('common.yes') : this.i18n.getI18n('common.no');
		} else if (attrMeta.isDate()) {
			result = this.dateTimeService.formatDate(value);
		} else if (attrMeta.isTimestamp()) {
			result = this.dateTimeService.formatTimestamp(value);
		}
		return result;
	}

	/**
	 * TODO
	 *    - subform column selection
	 *    - searchfilter
	 *    - search (text)
	 */
	exportBoList(
		boId: number | undefined,
		refAttrId: string | undefined,
		entityMeta: EntityMeta,
		columnPreference: Preference<SideviewmenuPreferenceContent>,
		searchtemplatePreference: Preference<SearchtemplatePreferenceContent>,
		format: string,
		pageOrientationLandscape: boolean,
		isColumnScaled: boolean,
		useSearchText: boolean
	): Observable<string> {
		let isSubform: boolean = boId !== undefined && refAttrId !== undefined;

		let offset = undefined;
		let chunkSize = undefined;

		let columns: ColumnAttribute[] = columnPreference
			? columnPreference.content.columns.filter(c => c.selected)
			: [];

		let filter = {
			offset: offset,
			chunksize: chunkSize,
			gettotal: false,
			countTotal: true,
			search: useSearchText ? this.searchService.getCurrentSearchInputText() : '',
			searchFilter: undefined, // TODO
			withTitleAndInfo: false,
			attributes: '',
			sort: '',
			searchCondition: '',
			where: {}
		};
		// request only given attributes
		filter.attributes = columns
			// filter only bo attributes
			.filter(attribute => attribute.boAttrId.startsWith(
				!isSubform
					? entityMeta.getEntityClassId()
					: FqnService.getEntityClassId(refAttrId!) +
					'_')
			)
			// use short name instead of fqn to avoid problems with to long query param
			.map(attribute => this.fqnService.getShortAttributeName(
				!isSubform
					? entityMeta.getEntityClassId()
					: FqnService.getEntityClassId(refAttrId!),
				attribute.boAttrId
			))
			.join(',');

		filter.sort = this.getSortString(columns);

		return (!isSubform ? of(entityMeta) : this.metaService.getBoMeta(FqnService.getEntityClassId(refAttrId!)))
			.pipe(
				take(1),
				mergeMap(meta => {
					let searchCondition = SearchConditionBuilder.getSearchCondition(this.fqnService, meta, columnPreference);
					if (searchCondition) {
						filter.searchCondition = searchCondition;
					}

					let url;
					if (!isSubform) {
						url =
							this.nuclosConfig.getRestHost() +
							'/bos/' +
							entityMeta.getEntityClassId() +
							'/boListExport/' +
							format +
							'/' +
							pageOrientationLandscape +
							'/' +
							isColumnScaled;
					} else {
						url =
							this.nuclosConfig.getRestHost() +
							'/bos/' +
							entityMeta.getEntityClassId() +
							'/' +
							boId +
							'/subBos/' +
							refAttrId +
							'/export/' +
							format +
							'/' +
							pageOrientationLandscape +
							'/' +
							isColumnScaled;
					}
					url +=
						'?countTotal=true&gettotal=false&offset=0&search=' +
						'&sort=' +
						filter.sort +
						'&withTitleAndInfo=false';

					if (searchtemplatePreference && searchtemplatePreference.content.columns) {
						let entityClassIds = SearchfilterService.entityClassIdsFromSearchfilter(searchtemplatePreference);
						this.metaService.getEntityMetas(entityClassIds).subscribe(mapMeta => {
							filter.where = this.buildWhereCondition(
								searchtemplatePreference,
								meta,
								mapMeta,
								this.searchfilterService.getAdditionalWhereConditions(meta.getEntityClassId()),
								ResultParams.DEFAULT
							);
						});
					} else {
						filter.where = {};
					}

					return this.http.post(url, filter).pipe(
						map(data => data['links'].export.href),
						catchError(error => {
						return this.eoErrorService.handleErrorWithoutMessage(error);
						})
					);
				})
			);
	}

	/**
	 * Fetches the corresponding EO for the given attribute (with only this one attribute filled).
	 * attributeId must be a fully quallified name!
	 */
	fetchByAttribute(
		sourceAttributeId: string,
		sourceEOId: number,
		targetAttributeId: string,
		targetEOId?: number
	): Observable<EntityObject> {
		// TODO: Splitting by underscore is potentially unsafe, the FQN format is flawed.
		let index = sourceAttributeId.lastIndexOf('_');
		if (index <= 0) {
			throw 'Attribute ID must be a fully qualified name! Illegal value: ' +
			sourceAttributeId;
		}
		let url =
			this.nuclosConfig.getRestHost() +
			'/data/fieldget/' +
			sourceAttributeId +
			'/' +
			sourceEOId +
			'/' +
			targetAttributeId +
			'/' +
			targetEOId;

		return this.http.get(url).pipe(map((data: EntityObjectData) => {
			let eo = new EntityObject(data, this.localStorageService, this.fqnService);
			this.eoVLPService.addEntityObjectMap(eo);
			return eo;
		}));
	}

	loadMatrixData(webMatrix: WebMatrix, eo: EntityObject): Observable<any> {
		let link = this.nuclosConfig.getRestHost() + '/data/data/matrix/';

		let postData: any = webMatrix;
		postData.entity = eo.getEntityClassId();
		postData.boId = eo.getId();

		return this.http.post(link, JSON.stringify(postData), {
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	loadTreeRootData(treeBoMetaId, treeBoId): Observable<any[]> {
		let link =
			this.nuclosConfig.getRestHost() + '/data/treeroot/' + treeBoMetaId + '/' + treeBoId;

		return this.http.get<any[]>(link)
			.pipe(catchError(e => {
				return this.eoErrorService.handleError(e, undefined);
			}));
	}

	loadSubTreeData(node_id: string): Observable<any[]> {
		let link = this.nuclosConfig.getRestHost() + '/data/subtree/' + node_id;

		return this.http.get<any[]>(link)
			.pipe(catchError(e => {
				return this.eoErrorService.handleError(e, undefined);
			}));

	}

	loadTextModules(
		boMetaId: string | undefined | null,
		boId: string,
		attributeMetaId: string,
		controlType?: string
	): Observable<any[]> {
		let link = this.nuclosConfig.getRestHost()
			+ '/data/textModule/'
			+ boMetaId + '/' + (boId ? boId : 'new') + '/' + attributeMetaId + (controlType ? '?controlType=' + controlType : '');

		return this.http.get<any[]>(link);
	}

	buildColumns(
		meta: EntityMeta,
		attributeSelection: Preference<AttributeSelectionContent> | undefined,
		sidebarView: SidebarLayoutType | undefined
	) {
		let columns: (ColumnAttribute | undefined)[] = attributeSelection
			? attributeSelection.content.columns.filter(c => c.selected)
			: [];

		let attributeFqns: (string | undefined)[] = sidebarView !== 'card' ? columns.map(a => (a ? a.boAttrId : undefined)) : [];
		let titleAttributeFqns: string[] = sidebarView !== 'card' ? [] : this.fqnService.parseFqns(meta.getTitlePattern());
		let infoAttributeFqns: string[] = sidebarView !== 'card' ? [] : this.fqnService.parseFqns(meta.getInfoPattern());

		// add attributes required by addons
		let requiredAddonAttributeFqns: string[] = this.addonService
			.getRequiredResultlistAttributeNames(meta.getEntityClassId())
			.map(attrName => meta.getEntityClassId() + '_' + attrName);

		columns = Array.from(
			new Set<string | undefined>([
				...attributeFqns,
				...titleAttributeFqns,
				...infoAttributeFqns,
				...requiredAddonAttributeFqns
			])
		)

			// filter out columns from other EO's (at the moment only supported by the Java-client result list)
			.filter(fqn => fqn && fqn.startsWith(meta.getEntityClassId() + '_'))

			.map(fqn => {
				if (fqn) {
					let attr = meta.getAttributeMetaByFqn(fqn);
					if (attr) {
						return {
							boAttrId: attr.getAttributeID()
						} as ColumnAttribute;
					}
				}
				return undefined;
			})
			.filter(fqn => fqn !== undefined);

		return columns;
	}

	getSortString(columns: ColumnAttribute[]) {
		return columns
			.filter(col => col.sort && col.sort.direction && col.boAttrId)
			.sort((a, b) => {
				if (!a.sort || !b.sort || a.sort.prio === null || b.sort.prio === null) {
					return 0;
				}
				if (a.sort.prio! < b.sort.prio!) {
					return -1;
				}
				if (a.sort.prio! > b.sort.prio!) {
					return 1;
				}
				return 0;
			})
			.map(column => column.boAttrId + (column.sort ? '+' + column.sort.direction : ''))
			.join(',');
	}

	private buildInnerContext(context: LoadContext): LoadInnerContext {
		let columns = this.buildColumns(context.meta, context.attributeSelection, context.resultParams?.sidebarView);

		let sort = '';
		if (context.attributeSelection && context.attributeSelection.content.columns) {
			sort = this.getSortString(context.attributeSelection.content.columns);
		}

		let innerContext: LoadInnerContext = {
			...context,
			search: this.searchService.getCurrentSearchInputText(),
			columns: columns,
			sort: sort,
			resultParams:
				context.resultParams === undefined ? ResultParams.DEFAULT : context.resultParams
		};

		return innerContext;
	}

	private buildQueryParams(context: LoadInnerContext) {
		let columns = this.buildColumns(context.meta, context.attributeSelection, context.resultParams?.sidebarView);

		let sort = '';
		if (context.attributeSelection && context.attributeSelection.content.columns) {
			sort = this.getSortString(context.attributeSelection.content.columns);
		}

		let queryParams = this.buildFilter(context);

		_.forOwn(context.resultParams, (value, key) => {
			if (value === null || value === undefined) {
				value = '';
			}

			queryParams = queryParams.append(key, value as string);
		});

		return queryParams;
	}

	private buildQueryObject(context: LoadInnerContext): Observable<QueryObject> {
		let entityClassIds = SearchfilterService.entityClassIdsFromSearchfilter(context.searchtemplatePreference);

		return new Observable<QueryObject>((observer: Observer<QueryObject>) => {
			this.metaService.getEntityMetas(entityClassIds).subscribe(mapMeta => {
				let queryObject: QueryObject = {
					where: undefined
				};

				queryObject.where = this.buildWhereCondition(
					context.searchtemplatePreference,
					context.meta,
					mapMeta,
					this.searchfilterService.getAdditionalWhereConditions(
						context.meta.getEntityClassId()
					),
					context.resultParams
				);
				queryObject.multiSelectionCondition = context.multiSelectionCondition;
				queryObject.editSelectionResult = context.editSelectionResult;

				if (
					context.meta.getEmptyResultListIfNoSearchCondition() &&
					queryObject.where !== undefined &&
					queryObject.where.clause.length === 0 &&
					context.search.length === 0
				) {
					this.$log.info('Search will be executed only if a search critera is set.');
					// actual a result list query call shouldn't be necessary here
					// but the result list response contains the 'canCreateBo' flag which will be evaluated for displaying the new-button
					// so just request an empty list
					// TODO create a REST service for retrieving 'canCreateBo' flag
					queryObject.where = {
						clause: context.meta.getEntityClassId() + '.id is null',
						aliases: {}
					};
				}
				observer.next(queryObject);
				observer.complete();
			});
		});
	}

	/**
	 * Builds the 'AND'-concatenated WHERE condition based on the given search object.
	 *
	 * TODO: searchObject type
	 */
	private buildWhereCondition(
		searchtemplatePreference: Preference<SearchtemplatePreferenceContent> | undefined,
		mainMeta: EntityMeta,
		dependentMapMeta: Map<string, EntityMeta>,
		additionalWhereConditions: string[],
		resultParams: ResultParams
	): WhereCondition {
		let result = this.searchfilterService.buildWhereCondition(
			searchtemplatePreference,
			mainMeta,
			dependentMapMeta
		);

		let andArray: string[] = [];

		if (result.clause !== undefined &&
			result.clause !== null &&
			result.clause.trim().length > 0) {
			andArray.push(result.clause);
		}
		andArray.push(...additionalWhereConditions);

		if (resultParams.entityObjectId) {
			let systemEntity = mainMeta.isSystemEntity();
			andArray.push(
				' ' +
				mainMeta.getEntityClassId() +
				'.id=' +
				(systemEntity ? '\'' : '') +
				resultParams.entityObjectId +
				(systemEntity ? '\' ' : ' ')
			);
		}

		result.clause = andArray.join(' AND ');

		return result;
	}

	private buildFilter(context: LoadInnerContext): HttpParams {
		let searchParams = new HttpParams();
		if (context.vlpParams) {
			for (let key of context.vlpParams.keys()) {
				let values = context.vlpParams.getAll(key);
				if (values) {
					for (let value of values) {
						searchParams = searchParams.append(key, value);
					}
				}
			}
		}

		searchParams = searchParams.append('search', context.search);
		if (context.searchFilter) {
			searchParams = searchParams.append('searchFilter', context.searchFilter);
		}
		if (context.vlpId) {
			searchParams = searchParams.append('vlpId', context.vlpId);
		}
		searchParams = searchParams.append('withTitleAndInfo', 'false');

		if (context.columns === undefined) {
			context.columns = [];
		}

		if (context.resultParams.entityObjectIdOnlySelection === true) {
			searchParams = searchParams.append('idOnlySelection', 'true');
		}

		// request only given attributes, in case of id-only-selection we need it for a text search
		if (context.columns !== undefined) {
			searchParams = searchParams.append(
				'attributes',
				context.columns
					// use short name instead of fqn to avoid problems with to long query param
					.map(attribute =>
						this.fqnService.getShortAttributeName(
							context.meta.getEntityClassId(),
							attribute.boAttrId
						)
					)
					.join(',')
			);
		}

		if (context.sort !== undefined) {
			searchParams = searchParams.append('sort', context.sort);
		}

		let searchCondition = SearchConditionBuilder.getSearchCondition(this.fqnService, context.meta);
		if (searchCondition) {
			searchParams = searchParams.append('searchCondition', searchCondition);
		}

		// NUCLOS-6311 4)
		searchParams = searchParams.append('skipStatesAndGenerations', 'true');
		return searchParams;
	}
}
