import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { EntityObjectSearchfilterService } from '@modules/entity-object-data/shared/entity-object-searchfilter.service';
import { EntityObjectPreferenceService } from '@modules/entity-object/shared/entity-object-preference.service';
import { PreferencesService } from '@modules/preferences/preferences.service';
import { FqnService } from '@shared/service/fqn.service';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import * as _ from 'lodash';
import { BehaviorSubject, EMPTY, Observable } from 'rxjs';
import { catchError, distinctUntilChanged, map, take, tap } from 'rxjs/operators';
import { LayoutService } from '../../../layout/shared/layout.service';
import { AuthenticationService } from '../../authentication';
import { Logger } from '../../log/shared/logger';
import { PerspectiveService } from '../../perspective/shared/perspective.service';
import {
	Preference,
	SubformSearchtemplatePreferenceContent
} from '../../preferences/preferences.model';
import { SearchService } from '../../search/shared/search.service';
import { SearchfilterService } from '../../search/shared/searchfilter.service';
import { EntityMeta } from './bo-view.model';
import { EntityObjectResultService } from './entity-object-result.service';
import { MetaService } from './meta.service';
import { SelectableService } from './selectable.service';


const temporaryFilter = (searchFilterPref) => searchFilterPref.content.isTemp;

const anySelectedFilter = (searchFilterPref) => searchFilterPref.selected;

const persistentSelectedFilter = (searchFilterPref) => searchFilterPref.selected && !searchFilterPref.content.isTemp;

/**
 * Defines a unique identifier for a subform and its filter list within a layout.
 */
export class DependentFilterKey {
	parentSubform?: string;
	foreignkeyfieldToParent: string;
}

/**
 * Provides access to all available search filter for the currently selected entity.
 * And to the currently selected searchfilter.
 */
@Injectable()
export class DependentsSearchfilterService {

	static createKey(foreignkeyfieldToParent: string, parentSubform?: string) {
		return {
			parentSubform: parentSubform,
			foreignkeyfieldToParent: foreignkeyfieldToParent
		};
	}

	/**
	 * Converts a {@link DependentFilterKey} into a string representation.
	 * @param dependentFilterKey the DependentFilterKey to convert
	 */
	static toKeyString(dependentFilterKey: DependentFilterKey) {
		return dependentFilterKey.parentSubform + '|' + dependentFilterKey.foreignkeyfieldToParent;
	}

	/**
	 * Checks if all columns of filter1 are present and equal in filter2.
	 * It DOES NOT check the other way around. Background: The temporary filter initializes all columns empty, even
	 * the ones that are not showing. So those columns do not appear in the subforms view prefs. And checking the other way around
	 * would find, that those columns are missing in the view prefs.
	 * @param filter1
	 * @param filter2
	 */
	static isFilterEqual(filter1, filter2) {
		for (let columnAttribute of filter1.content.columns) {
			let keyName = columnAttribute.boAttrId;
			if (!filter2.content.columns.some(c => keyName === c.boAttrId)) {
				return false;
			} else {
				let respectiveColumnAttribute = filter2.content.columns.find(col => keyName === col.boAttrId);
				if (!DependentsSearchfilterService.isColumnAttributeEqual(columnAttribute, respectiveColumnAttribute)) {
					return false;
				}
			}
		}
		return true;
	}

	static isColumnAttributeEqual(columnAttributeA, columnAttributeB) {
		return columnAttributeA.filterOp === columnAttributeB['filterOp']
			&& columnAttributeA.filterValueBoolean === columnAttributeB['filterValueBoolean']
			&& columnAttributeA.filterValueDate === columnAttributeB['filterValueDate']
			&& columnAttributeA.filterValueDouble === columnAttributeB['filterValueDouble']
			&& columnAttributeA.filterValueString === columnAttributeB['filterValueString'];
	}

	private selectedSubformSearchfilters: BehaviorSubject<
		Map<string, Preference<SubformSearchtemplatePreferenceContent> | undefined>
	> = new BehaviorSubject(new Map());

	private allSubformSearchfilters: BehaviorSubject<
		Map<string, Preference<SubformSearchtemplatePreferenceContent>[]>
	> = new BehaviorSubject(new Map());

	private currentLayoutId;
	private loggedIn = false;

	private init = false;

	constructor(
		private authenticationService: AuthenticationService,
		private selectableService: SelectableService,
		private perspectiveService: PerspectiveService,
		private preferenceService: PreferencesService,
		private searchService: SearchService,
		private searchfilterService: SearchfilterService,
		private eoResultService: EntityObjectResultService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private layoutService: LayoutService,
		private fqnService: FqnService,
		private metaService: MetaService,
		private nuclosI18n: NuclosI18nService,
		private dialogService: NuclosDialogService,
		private route: ActivatedRoute,
		private nuclosConfig: NuclosConfigService,
		private http: HttpClient,
		private $log: Logger
	) {
		this.route.queryParams.subscribe(() => {
			if (this.init) {
				this.loadDependentSearchfilters();
			} else {
				this.authenticationService.observeLoginStatus().subscribe(loggedIn => {
					this.loggedIn = loggedIn;
					if (loggedIn) {
						this.loadDependentSearchfilters();
					}
				});

				this.eoResultService.observeSelectedEntityClassId().subscribe(() => {
					this.loadDependentSearchfilters();
				});

				this.init = true;
			}
		});
	}

	putDependentFilterWithoutSave(searchfilter: Preference<SubformSearchtemplatePreferenceContent>, dependentFilterKey: DependentFilterKey) {
		let searchFilters = this.getAllDependentSearchfilters();
		if (searchFilters) {
			let dependentKeyString = DependentsSearchfilterService.toKeyString(dependentFilterKey);
			let allEntitySubformSearchfilters = searchFilters.get(dependentKeyString);
			if (allEntitySubformSearchfilters) {
				allEntitySubformSearchfilters = allEntitySubformSearchfilters.filter(filter => filter.prefId !== searchfilter.prefId);
				allEntitySubformSearchfilters.push(searchfilter);
				searchFilters.set(dependentKeyString, _.clone(this._sortSearchfilters(allEntitySubformSearchfilters)));
				this.allSubformSearchfilters.next(searchFilters);
			}
		}
	}

	selectTempFilter(dependentFilterKey: DependentFilterKey) {
		let filters = this.getAllSearchfiltersForDependents(dependentFilterKey);
		if (filters) {
			let tempFilter = filters.find(temporaryFilter);
			if (tempFilter) {
				this.clearFilterContent(tempFilter);
			}
			this.selectSubformSearchfilter(dependentFilterKey, tempFilter);
		}
	}

	selectSubformSearchfilter(
		dependentFilterKey: DependentFilterKey,
		searchfilter: Preference<SubformSearchtemplatePreferenceContent> | undefined,
		updatePreference = true,
		clearSearchfilter = false
	) {
		if (clearSearchfilter && searchfilter) {
			searchfilter.content.columns = [];
		}

		let dependentKeyString = DependentsSearchfilterService.toKeyString(dependentFilterKey);
		let selectedSubformSearchFilters = this.selectedSubformSearchfilters.getValue();
		selectedSubformSearchFilters.set(dependentKeyString, searchfilter);

		// update selected flag in all searchfilters
		let allFilters = this.getAllDependentSearchfilters();
		let filters = allFilters.get(dependentKeyString);
		filters?.forEach(filter => {
			let shouldSelect = (filter === searchfilter);
			if (filter.selected !== shouldSelect) {
				filter.selected = shouldSelect;
				this.persistFilterSelectionStatus(filter);
			}
		});

		this.allSubformSearchfilters.next(allFilters);
		this.selectedSubformSearchfilters.next(selectedSubformSearchFilters);
	}

	observeSelectedSubformSearchfilter(dependentFilterPrefId: DependentFilterKey): Observable<
		Preference<SubformSearchtemplatePreferenceContent> | undefined
	> {
		let dependentKeyString = DependentsSearchfilterService.toKeyString(dependentFilterPrefId);
		if (!this.selectedSubformSearchfilters.getValue().get(dependentKeyString)) {
			this.setSubformSearchfilters(dependentFilterPrefId, []);
		}
		return this.selectedSubformSearchfilters.pipe(
			map(selectedSubformFilters => selectedSubformFilters.get(dependentKeyString))
		);
	}

	observeAllSubformSearchfilters(): Observable<Map<string, Preference<SubformSearchtemplatePreferenceContent>[]>> {
		return this.allSubformSearchfilters.pipe(distinctUntilChanged());
	}

	observeAllDependentSearchfilters(dependentFilterKey: DependentFilterKey)
		: Observable<Preference<SubformSearchtemplatePreferenceContent>[] | undefined> {
		let dependentKeyString = DependentsSearchfilterService.toKeyString(dependentFilterKey);
		return this.allSubformSearchfilters.pipe(
			map(allFilters => allFilters.get(dependentKeyString)),
			distinctUntilChanged()
		);
	}

	getAllSearchfiltersForDependents(filter: DependentFilterKey) {
		return this.allSubformSearchfilters.getValue().get(DependentsSearchfilterService.toKeyString(filter));
	}

	getAllDependentSearchfilters(): Map<string, Preference<SubformSearchtemplatePreferenceContent>[]> {
		return this.allSubformSearchfilters.getValue();
	}

	getSelectedDependentSearchfilter(dependentEntityClassId: DependentFilterKey) {
		return this.getAllSelectedSearchfilters().get(DependentsSearchfilterService.toKeyString(dependentEntityClassId));
	}

	resetFilterChanges(searchfilter: Preference<SubformSearchtemplatePreferenceContent>) {
		if (searchfilter.prefId) {
			this.searchfilterService.getDependentSearchfilter(searchfilter.prefId).subscribe(refreshFilter => {
				this.putDependentFilterWithoutSave(refreshFilter as Preference<SubformSearchtemplatePreferenceContent>, searchfilter.content.filterKey);
				if (this.getSelectedDependentSearchfilter(searchfilter.content.filterKey)?.prefId === refreshFilter.prefId) {
					this.selectSubformSearchfilter(searchfilter.content.filterKey, refreshFilter);
				}
			});
		}
	}

	createTempSearchfilter(
		dependentFilterKey: DependentFilterKey,
		select: boolean
	): Preference<SubformSearchtemplatePreferenceContent> {
		let tempFilter = this.createSearchfilterWithoutAttributes(dependentFilterKey);
		tempFilter.name = this.nuclosI18n.getI18n('webclient.searcheditor.searchfilter.temp.name');
		tempFilter.content.filterKey = dependentFilterKey;
		tempFilter.content.isTemp = true;
		tempFilter.selected = select;

		let metaClassId = this.getEntityClassIdFromKey(dependentFilterKey);
		this.metaService.getEntityMeta(metaClassId)
			.pipe(
				catchError(
					(e: HttpErrorResponse) => {
						return this.dialogService.alert({
							message: e.error?.message ? e.error.message : e.toString(),
							title: this.nuclosI18n.getI18n('webclient.error.title')
						});
						return EMPTY;
					}
				)
			)
			.subscribe((metaData) => this.pushColumnAttributesFromMeta(metaData, tempFilter));

		return tempFilter;
	}

	createSearchfilterWithoutAttributes(
		dependentFilterKey: DependentFilterKey
	): Preference<SubformSearchtemplatePreferenceContent> {
		return {
			type: 'subform-searchtemplate',
			boMetaId: this.getEntityClassIdFromKey(dependentFilterKey),
			selected: false,
			content: {
				columns: [],
				filterKey: dependentFilterKey
			}
		};
	}

	createSearchfilter(
		dependentIdentifier: DependentFilterKey,
		metaData: EntityMeta
	): Preference<SubformSearchtemplatePreferenceContent> {
		let searchtemplatePreference: Preference<SubformSearchtemplatePreferenceContent>
			= this.createSearchfilterWithoutAttributes(dependentIdentifier);

		this.pushColumnAttributesFromMeta(metaData, searchtemplatePreference);

		return searchtemplatePreference;
	}

	saveSearchfilter(searchfilter: Preference<SubformSearchtemplatePreferenceContent>) {
		return this.selectableService.savePreference(searchfilter)
			.pipe(
				tap(savedFilter => {
						let subformFilter = savedFilter as Preference<SubformSearchtemplatePreferenceContent>;
						let filters = this.getAllSearchfiltersForDependents(subformFilter.content.filterKey);
						if (!filters) {
							filters = [];
						}
						// filter out the now corrupted temporary template
						filters = filters.filter(filter => filter.name !== '[Meine Suche]');
						// filter out, the previous instance of the subform filter
						filters = filters.filter(filter => filter.prefId !== subformFilter.prefId);
						filters.push(subformFilter);
						this.setSubformSearchfilters(subformFilter.content.filterKey, filters);
						this.selectSubformSearchfilter(subformFilter.content.filterKey, subformFilter);
					}
				),
				take(1)
			);
	}

	deleteSearchfilter(searchfilter: Preference<SubformSearchtemplatePreferenceContent>) {
		this.searchfilterService
			.deleteSubformSearchfilter(searchfilter)
			.subscribe(() => {
				let filterKey = searchfilter.content.filterKey;
				let dependentKeyString = this.toKeyString(filterKey);
				let allSearchfilters = this.getAllDependentSearchfilters();
				let searchfilters = allSearchfilters.get(dependentKeyString);
				if (!searchfilters) {
					return;
				}
				let index = searchfilters.findIndex(filter => filter.prefId === searchfilter.prefId);
				if (index >= 0) {
					searchfilters.splice(index, 1);
					searchfilters = _.clone(searchfilters);
					allSearchfilters.set(dependentKeyString, searchfilters);
					this.allSubformSearchfilters.next(allSearchfilters);
				}
				if (this.getSelectedDependentSearchfilter(filterKey) === searchfilter) {
					this.selectTempFilter(filterKey);
				}
			});
	}

	private pushColumnAttributesFromMeta(metaData: EntityMeta, tempFilter: Preference<SubformSearchtemplatePreferenceContent>) {
		metaData.getAttributes().forEach(attributeMeta => {
			if (
				!attributeMeta.isStateIcon() &&
				!attributeMeta.isDeletedFlag() &&
				!attributeMeta.isHidden() &&
				!attributeMeta.isStateIcon() &&
				!attributeMeta.isImage()
			) {
				tempFilter.content.columns.push({
					name: attributeMeta.getName(),
					boAttrId: attributeMeta.getAttributeID(),
					system: attributeMeta.isSystemAttribute(),
					selected: true
				});
			}
		});
	}

	private getAllSelectedSearchfilters() {
		return this.selectedSubformSearchfilters.getValue();
	}

	private setAllSubformSearchfilters(
		preferences: Map<string, Preference<SubformSearchtemplatePreferenceContent>[]>,
		keyLookupMap: Map<string, DependentFilterKey>
	) {
		preferences.forEach((dependentSearchFilters, key) => {
			let filterKeyObject = keyLookupMap.get(key);
			if (filterKeyObject) {
				this.setSubformSearchfilters(filterKeyObject, dependentSearchFilters);
			}
		});
	}

	private clearAll() {
		this.selectedSubformSearchfilters.next(new Map<string, Preference<SubformSearchtemplatePreferenceContent> | undefined>());
		this.allSubformSearchfilters.next(new Map<string, Preference<SubformSearchtemplatePreferenceContent>[]>());
	}

	private _sortSearchfilters(searchfilters) {
		return EntityObjectSearchfilterService.sortSearchfilters(searchfilters, false);
	}

	private loadDependentSearchfilters() {
		let eo = this.eoResultService.getSelectedEo();
		if (eo) {
			if (!this.loggedIn) {
				this.$log.debug('Load dependent searchfilters canceled, not logged in!');
				return;
			}
			if (this.currentLayoutId === eo.getLayoutId()) {
				return;
			}
			this.currentLayoutId = eo.getLayoutId();
			this.$log.debug('Load dependent searchfilters for %o', eo.getLayoutId());

			this.clearAll();

			this.searchfilterService
				.getSubformSearchfiltersForEntity(eo.getLayoutId())
				.subscribe(preferences => {
					let searchfilters = new Map<string, Preference<SubformSearchtemplatePreferenceContent>[]>;
					let keyLookupMap = new Map<string, DependentFilterKey>();
					this.buildDependentMaps(searchfilters, keyLookupMap, preferences);
					this.setAllSubformSearchfilters(searchfilters, keyLookupMap);
				});
		}
	}

	private buildDependentMaps(searchfilters, keyLookupMap, preferences) {
		preferences.forEach(pref => {
			let subformSearchPrefContent: SubformSearchtemplatePreferenceContent = pref.content as SubformSearchtemplatePreferenceContent;
			let dependentKeyString = this.toKeyString(subformSearchPrefContent.filterKey);
			let subBoFilters = searchfilters.get(dependentKeyString);
			if (!subBoFilters) {
				subBoFilters = [];
				keyLookupMap.set(dependentKeyString, subformSearchPrefContent.filterKey);
			}
			subBoFilters.push(pref as Preference<SubformSearchtemplatePreferenceContent>);
			searchfilters.set(dependentKeyString, subBoFilters);
		});
	}

	private setSubformSearchfilters(
		dependentEntityClassId: DependentFilterKey,
		preferences: Preference<SubformSearchtemplatePreferenceContent>[]
	) {
		this.enforceExactlyOneTempFilterPresent(preferences, dependentEntityClassId);
		let dependentKeyString = DependentsSearchfilterService.toKeyString(dependentEntityClassId);
		let sortedFilterList = this._sortSearchfilters(preferences);
		let updatedMap = this.getAllDependentSearchfilters().set(dependentKeyString, sortedFilterList);
		updatedMap = _.clone(updatedMap);
		this.allSubformSearchfilters.next(updatedMap);
		this.autoSelectFilter(dependentEntityClassId, sortedFilterList);
	}

	private autoSelectFilter(dependentFilterKey: DependentFilterKey, sorted: Preference<SubformSearchtemplatePreferenceContent>[]) {
		// find selected non-temp filter
		let preferredFilter = sorted.find(persistentSelectedFilter);
		if (!preferredFilter) {
			// select temp filter
			preferredFilter = sorted.find(anySelectedFilter);
		}
		this.selectSubformSearchfilter(dependentFilterKey, preferredFilter, false);
	}

	private toKeyString(dependentFilterKey: DependentFilterKey) {
		return DependentsSearchfilterService.toKeyString(dependentFilterKey);
	}

	private getEntityClassIdFromKey(dependentEntityClassId: DependentFilterKey): string {
		let lastUnderscoreIndex = dependentEntityClassId.foreignkeyfieldToParent.lastIndexOf('_');
		return dependentEntityClassId.foreignkeyfieldToParent.substring(0, lastUnderscoreIndex);
	}

	private createTempSearchfilterAndPushToPreferences(
		preferences: Preference<SubformSearchtemplatePreferenceContent>[],
		dependentFilterKey: DependentFilterKey
	) {
		let tempFilter = this.createTempSearchfilter(dependentFilterKey, true);
		let dependentEntityClass = this.getEntityClassIdFromKey(dependentFilterKey);
		let subformColumnPreferenceSelection
			= this.entityObjectPreferenceService.getSubformColumnPreferenceSelection(dependentEntityClass).getValue();
		if (subformColumnPreferenceSelection) {
			tempFilter.content.columns = subformColumnPreferenceSelection.content.columns;
		}
		preferences.push(tempFilter);
	}

	private persistFilterSelectionStatus(filter) {
		if (!filter.content.isTemp) {
			if (filter.selected) {
				this.preferenceService.selectPreference(filter).subscribe();
			} else {
				this.preferenceService.deselectPreference(filter).subscribe();
			}
		}
	}

	private clearFilterContent(tempFilter: Preference<SubformSearchtemplatePreferenceContent>) {
		tempFilter.content.columns.forEach(columnAttribute => {
			columnAttribute.filterOp = undefined;
			columnAttribute.filterValueDouble = undefined;
			columnAttribute.filterValueString = undefined;
			columnAttribute.filterValueBoolean = undefined;
			columnAttribute.filterValueDate = undefined;
		});
	}

	private cleanUpTempDuplicates(preferences, tempFilters) {
		// clean up to many temps
		for (const tempFilter of tempFilters.splice(0, tempFilters.length - 1)) {
			let index = preferences.indexOf(tempFilter);
			preferences.splice(index, 1);
		}
	}

	private enforceExactlyOneTempFilterPresent(
		preferences: Preference<SubformSearchtemplatePreferenceContent>[],
		dependentFilterKey: DependentFilterKey
	) {
		let tempFilters = preferences.filter(it => it.content.isTemp === true);
		if (tempFilters.length < 1) {
			this.createTempSearchfilterAndPushToPreferences(preferences, dependentFilterKey);
		} else if (tempFilters.length > 1) {
			this.cleanUpTempDuplicates(preferences, tempFilters);
		}
	}

}
