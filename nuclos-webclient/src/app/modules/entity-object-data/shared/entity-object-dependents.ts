import {
	EntityOrder,
	IEntityObject,
	IEntityObjectDependents,
	ISortModel,
	ISubEntityObject,
	SubEntityInsertLocation
} from '@nuclos/nuclos-addon-api';
import { BehaviorSubject, iif, Observable, of } from 'rxjs';
import { catchError, map, switchMap, take } from 'rxjs/operators';
import { AutonumberService } from './autonumber.service';
import { EntityObjectDependency } from './entity-object-dependency';
import { EntityObjectSearchConfig } from './entity-object-search-config';
import { EntityObject, SubEntityObject } from './entity-object.class';
import { EntityObjectService } from './entity-object.service';

export class EntityObjectDependents implements IEntityObjectDependents {

	/* If autonumbers are present in the eo, it will sort by them ascending.
	*  If no autonumbers are present, it will sort them the other way around than they are normally sorted in the webclient. */
	public static inverseSort = function(eod: EntityObjectDependents, dependents: ISubEntityObject[]) {
		AutonumberService.instance.findAutonumberAttribute(eod).subscribe(attribute => {
			if (!attribute) {
				dependents.sort((a, b) => {
					if (a.getId() && b.getId()) {
						return (a.getId() as number - (b.getId() as number));
					} else if (a.getId()) {
						return -1;
					} else if (b.getId()) {
						return 1;
					}
					return 0;
				});
			} else {
				dependents.sort((a, b) => {
					let d1 = a.getAttribute(attribute.getAttributeID());
					let d2 = b.getAttribute(attribute.getAttributeID());

					return d1 - d2;
				});
			}
			eod.subject.next(dependents);
		});
	};

	private subject = new BehaviorSubject<ISubEntityObject[] | undefined>(undefined);
	private loading = false;
	private insertLocation: SubEntityInsertLocation = SubEntityInsertLocation.INSERT_TOP;
	private entityOrder: EntityOrder | undefined;
	private searchfilter: string | undefined;

	constructor(private parent: EntityObject, private referenceAttributeId: string) {}

	asObservable(): Observable<ISubEntityObject[] | undefined> {
		return this.subject;
	}

	current(): ISubEntityObject[] | undefined {
		return this.subject.value;
	}

	set(subEos: ISubEntityObject[]) {
		this.subject.next(subEos);
		AutonumberService.instance.updateAutonumbers(this, this.getDependency().getReferenceAttributeFqn());
	}

	reload(sortModel?: ISortModel) {
		this.clear();
		this.loadIfEmpty(sortModel);
	}

	setFilter(searchfilter: string | undefined) {
		this.searchfilter = searchfilter;
	}

	getFilter(): string | undefined {
		return this.searchfilter;
	}

	/**
	 * Reloads the dependents from the server but preserves local changes by integrating them into the fresh results from the server
	 */
	reloadSoft(sortModel?: ISortModel): void {
		this.load(false, true, sortModel);
	}

	/**
	 * Loads all dependents for the given attribute FQN.
	 */
	loadIfEmpty(sortModel?: ISortModel, readOnly = false): void {
		this.load(true, false, sortModel, readOnly);
	}

	setEntityOrder(order: EntityOrder | undefined) {
		this.entityOrder = order;
	}

	applyEntityOrder(dependents: ISubEntityObject[]) {
		if (this.entityOrder) {
			this.entityOrder.call(this, this, dependents);
		}
	}

	setInsertLocation(insertLocation: SubEntityInsertLocation) {
		this.insertLocation = insertLocation;
	}

	getTotalCount(): Observable<number> {
		return this.getService().countDependents(
			this.parent,
			this.getDependency()
		);
	}

	addAll(dependents: ISubEntityObject[]) {
		let currentDependents = this.subject.value || [];
		if (dependents.length > 0) {
			this.parent.setDirty(true);
		}
		if (SubEntityInsertLocation.INSERT_TOP === this.insertLocation) {
			currentDependents.unshift(...dependents);
		} else {
			currentDependents.push(...dependents);
		}
		AutonumberService.instance.updateAutonumbers(this, this.getDependency().getReferenceAttributeFqn());

		this.applyEntityOrder(currentDependents);

		this.subject.next(currentDependents);
	}

	removeAll(dependents: ISubEntityObject[]) {
		if (dependents.length === 0) {
			return;
		}

		let currentDependents = this.subject.value || [];
		dependents.forEach(dependent => {
			let index = currentDependents.indexOf(dependent);
			if (index >= 0) {
				currentDependents.splice(index, 1);
				this.parent.setDirty(true);
			}
		});

		AutonumberService.instance.updateAutonumbers(this, this.getDependency().getReferenceAttributeFqn());

		this.subject.next(currentDependents);
	}

	isEmpty(): boolean {
		let current = this.current();
		return current === undefined || current.length === 0;
	}

	isLoading(): boolean {
		return this.loading;
	}

	getSelectedSubEos(): ISubEntityObject[] {
		let currentSubEos = this.current();

		if (!currentSubEos || currentSubEos.length === 0) {
			return [];
		}

		return currentSubEos.filter(subEo => subEo.isSelected());
	}

	clear() {
		this.subject.next(undefined);
	}

	fetchBySearchConfig(searchConfig: EntityObjectSearchConfig) {
		return this.getService().loadDependents(this.parent, this.getDependency(), searchConfig);
	}

	fetchValueList(attributeId: string) {
		return this.getService().loadValueList(this.parent, this.getDependency(), attributeId);
	}

	private restoreLocalChanges(previousDependents: readonly ISubEntityObject[], dependents: ISubEntityObject[]) {
		this.restoreDirtyEntries(previousDependents, dependents);
		this.restoreNewEntries(previousDependents, dependents);
		return dependents;
	}

	private restoreNewEntries(previousDependents: readonly ISubEntityObject[], dependents: ISubEntityObject[]) {
		let newEntries: ISubEntityObject[] = previousDependents.filter(entry => entry.isNew());
		dependents.unshift(...newEntries);
	}

	private restoreDirtyEntries(previousDependents: readonly ISubEntityObject[], dependents: ISubEntityObject[]) {
		let dirtyEntries: ISubEntityObject[] = previousDependents.filter(entry => entry.isDirty());
		dirtyEntries.forEach(entry => {
			let index = dependents.findIndex(newEntry => newEntry.getId() === entry.getId());
			if (index !== -1) {
				dependents[index] = entry;
			}
		});
	}

	private load(skipIfDefined = true, preserveLocalChanges = false, sortModel?, readOnly = false) {
		if (this.loading) {
			return;
		}

		let currentData = this.subject.getValue();
		if (currentData && skipIfDefined) {
			this.subject.next(currentData);
			return;
		}

		if (this.parent.isNew() && !this.parent.getTemporaryId()) {
			this.subject.next([]);
		} else {
			this.subject.next(undefined);

			this.loading = true;
			this.getService()
				.loadDependents(this.parent, this.getDependency(), {
					searchCondition: this.searchfilter ?? this.searchfilter,
					sort: sortModel && sortModel.toString()
				}, readOnly)
				.pipe(
					take(1),
					catchError((err) => {
						this.loading = false;
						throw err;
					}),
					map((dependents: ISubEntityObject[]) => {
							if (preserveLocalChanges && currentData) {
								this.restoreLocalChanges(currentData, dependents);
							}
							return dependents;
						}
					)
				)
				.subscribe((dependents: ISubEntityObject[]) => {
					this.loading = false;
					this.subject.next(dependents);
					this.applyEntityOrder(dependents);
				});
		}
	}

	private getDependency(): EntityObjectDependency {
		let result = new EntityObjectDependency(this.referenceAttributeId);
		let parent = <IEntityObject>this.parent;
		while (parent instanceof SubEntityObject) {
			let parentDependency = new EntityObjectDependency(
				parent.getReferenceAttributeId(),
				undefined,
				'' + (parent.getId() ? parent.getId() : parent.getTemporaryId())
			);
			result.getRootDependency().setParent(parentDependency);
			parent = <EntityObject>parent.getParent();
		}
		return result;
	}

	private getService() {
		return EntityObjectService.instance;
	}

}
