import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NuclosError } from '@shared/nuclos-error';
import { EMPTY, throwError as observableThrowError } from 'rxjs';
import { take } from 'rxjs/operators';
import { NuclosDialogService } from '../../../core/service/nuclos-dialog.service';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { ValidationError } from '../../../data/schema/validation-error';
import { StringUtils } from '../../../shared/string-utils';
import { Logger } from '../../log/shared/logger';
import { EntityObjectEventService } from './entity-object-event.service';
import { EntityObject } from './entity-object.class';

@Injectable()
export class EntityObjectErrorService {

	constructor(
		private eoEventService: EntityObjectEventService,
		private dialog: NuclosDialogService,
		private $log: Logger,
		private nuclosI18nService: NuclosI18nService,
	) {
		this.eoEventService.observeSelectedEo().subscribe(
			eo => {
				let error = eo && eo.getError();
				if (error) {
					this.$log.debug('Error %o in selected EO %o', error, eo);
					this.dialog.alert({
						title: 'Error',
						message: error
					}).pipe(
						take(1)
					).subscribe();
				}
			}
		);
	}

	handleErrorWithoutMessage(error: HttpErrorResponse | any) {
		return this._handleError(error, undefined, true);
	}

	handleError(error: HttpErrorResponse | any, eo?: EntityObject) {
		return this._handleError(error, eo, false);
	}

	private _handleError(error: HttpErrorResponse | any, eo: EntityObject | undefined, neverShowMessage: boolean) {
		let errorMessage = this.getErrorMessage(error);

		this.handleValidationErrors(error, eo);

		if (errorMessage) {
			let bShowDefaultErrorDialog = eo == null || !eo.isSuppressDefaultErrorMessage();
			if (bShowDefaultErrorDialog && !neverShowMessage) {
				this.showErrorMessage(errorMessage, error);
			}
			if (eo) {
				eo.notifyError(errorMessage);
			}

			let nuclosError = new NuclosError(errorMessage ? errorMessage : '');
			nuclosError.alreadyHandled = true;
			return observableThrowError(nuclosError);
		}

		return observableThrowError(errorMessage);
	}

	private getErrorMessage(error: HttpErrorResponse | any) {
		let errorMessage: string | undefined;

		let messageFromResponse = this.getErrorMessageFromResponse(error);
		if (messageFromResponse) {
			errorMessage = messageFromResponse;
		} else if (error.status) {
			errorMessage = 'webclient.error.code' + error.status;
		} else {
			errorMessage = error.message || undefined;
		}

		if (errorMessage) {
			errorMessage = this.nuclosI18nService.getI18n(errorMessage);
		}

		return errorMessage;
	}

	private getErrorMessageFromResponse(error: HttpErrorResponse | any) {
		let result = undefined;

		try {
			const json = error.error || '';
			result = json.message
				.replace('<html>', '')
				.replace('</html>', '')
				.replace('<body>', '')
				.replace('</body>', '');
		} catch (e) {
		}

		return result;
	}

	private showErrorMessage(errorMessage: string, error: Response | any) {
		this.$log.error(errorMessage, error);

		this.dialog.alert(
			{
				title: this.nuclosI18nService.getI18n('webclient.error.title'),
				message: StringUtils.textToHtml(errorMessage)
			}
		).pipe(
			take(1)
		).subscribe();
	}

	private handleValidationErrors(error: Response | any, eo?: EntityObject) {
		if (eo) {
			let validationErrors: ValidationError[] = this.getValidationErrors(error);

			if (validationErrors) {
				for (let validationError of validationErrors) {
					eo.setAttributeValidationError(validationError);
				}
			}
		}
	}

	private getValidationErrors(error: any) {
		let json;
		try {
			json = error.error;
		} catch (e) {
		}

		return json && json.validationErrors;
	}
}
