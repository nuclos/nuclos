import { Injectable } from '@angular/core';
import { EntityObjectData } from '@modules/entity-object-data/shared/bo-view.model';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { LocalStorageService } from '@shared/service/local-storage.service';
import { NuclosTitleService } from '@shared/service/nuclos-title.service';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map, takeWhile, tap } from 'rxjs/operators';
import { EntityObject } from './entity-object.class';

@Injectable({
	providedIn: 'root'
})
export class EntityObjectEventService {
	static instance: EntityObjectEventService;

	private selectedEo: BehaviorSubject<IEntityObject | undefined>;
	private deletedEo: Subject<IEntityObject>;
	private savedEo: Subject<IEntityObject>;
	private addEo: Subject<IEntityObject>;
	private createdEo: Subject<IEntityObject>;
	private resetEo: Subject<IEntityObject>;
	private reloadEo: Subject<IEntityObject>;
	private modifiedEo = new Subject<IEntityObject>();
	private eoInDialog: BehaviorSubject<IEntityObject | undefined>;
	private stateChanged = new Subject<IEntityObject>();
	private colorChanged = new Subject<IEntityObject>();
	private layoutUpdated = new Subject<IEntityObject>();
	private externalOpenEo: Subject<IEntityObject> = new Subject<IEntityObject>();

	constructor(
		private titleService: NuclosTitleService,
		private localStorageService: LocalStorageService
	) {
		EntityObjectEventService.instance = this;
		this.selectedEo = new BehaviorSubject<IEntityObject | undefined>(undefined);
		this.deletedEo = new Subject<IEntityObject>();
		this.savedEo = new Subject<IEntityObject>();
		this.addEo = new Subject<IEntityObject>();
		this.createdEo = new Subject<IEntityObject>();
		this.resetEo = new Subject<IEntityObject>();
		this.reloadEo = new Subject<IEntityObject>();
		this.eoInDialog = new BehaviorSubject<IEntityObject | undefined>(undefined);
		this.layoutUpdated = new Subject<IEntityObject>();

		this.selectedEo.subscribe(
			(eo: IEntityObject | undefined) => {
				if (eo) {
					this.setWindowTitleFromEo(eo);
				}
			}
		);

		this.deletedEo.subscribe(
			eo => {
				if (eo) {
					(<EntityObject><unknown>eo).getMeta().subscribe(
						meta => {
							this.titleService.setTitle(undefined, meta.getEntityName());
						}
					);
				}
			}
		);
	}

	/**
	 * fires when another EO gets selected
	 */
	observeSelectedEo() {
		return this.selectedEo;
	}

	/**
	 * Emits the deleted EOs.
	 */
	observeDeletedEo() {
		return this.deletedEo;
	}

	/**
	 * Emits the saved EOs.
	 */
	observeSavedEo() {
		return this.savedEo;
	}

	observeEoModification() {
		return this.modifiedEo;
	}

	observeEoLayoutUpdated() {
		return this.layoutUpdated;
	}

	observeStateChanges() {
		return this.stateChanged;
	}

	observeColorChange() {
		return this.colorChanged;
	}

	/**
	 * Emits new added but unsaved EOs.
	 */
	observeAddEo(): Observable<IEntityObject> {
		return this.addEo;
	}

	/**
	 * Emits new created EOs.
	 */
	observeCreatedEo(): Observable<IEntityObject> {
		return this.createdEo;
	}

	/**
	 * Emits the IDs of reset EOs.
	 */
	observeResetEo() {
		return this.resetEo;
	}

	/**
	 * Emits reloaded EOs.
	 */
	observeReloadEo() {
		return this.reloadEo;
	}

	observeEoInDialog() {
		return this.eoInDialog;
	}

	observeExternalOpenEo(): Subject<IEntityObject> {
		return this.externalOpenEo;
	}

	emitSelectedEo(eo: IEntityObject | undefined) {
		this.selectedEo.next(eo);
		this.listenForExternalChanges(eo).pipe(
			takeWhile(
				() => EntityObjectService.equals(eo, this.selectedEo.getValue())
			)
		).subscribe();
	}

	emitModifiedEo(eo: IEntityObject) {
		this.modifiedEo.next(eo);
	}

	emitStateChange(eo: IEntityObject) {
		this.stateChanged.next(eo);
	}

	emitColorChange(eo: IEntityObject) {
		this.colorChanged.next(eo);
	}

	emitSavedEo(eo: IEntityObject) {
		this.savedEo.next(eo);
	}

	emitLayoutUpdatedForEo(eo: IEntityObject) {
		this.layoutUpdated.next(eo);
	}

	emitDeletedEo(eo: IEntityObject) {
		this.deletedEo.next(eo);
	}

	emitCreatedEo(eo: IEntityObject) {
		this.createdEo.next(eo);
	}

	emitAddEo(eo: IEntityObject) {
		this.addEo.next(eo);
	}

	emitResetEo(eo: IEntityObject) {
		this.resetEo.next(eo);
	}

	emitReloadEo(eo: IEntityObject) {
		this.reloadEo.next(eo);
	}

	emitEoInDialog(eo: IEntityObject | undefined) {
		this.eoInDialog.next(eo);
	}

	emitExternalOpenEo(eo: IEntityObject) {
		this.externalOpenEo.next(eo);
	}

	listenForExternalChanges(eo: IEntityObject | undefined): Observable<any> {
		if (eo === undefined || eo.isNew()) {
			return of(undefined);
		}

		let key = EntityObjectService.EO_LOCALSTORAGE_KEY_PREFIX + eo.getId();
		return this.localStorageService.observeItem(key).pipe(
			tap((data: EntityObjectData) => {
				if (!eo.getEntityClassId().endsWith('DYN')) {
					eo.mergeData(data);
				}
			})
		);
	}

	/**
	 * TODO: The EO title is not always available:
	 * If the EO is initially selected from the result list, it must be reloaded first.
	 * Only then will the title be available.
	 */
	private setWindowTitleFromEo(eo: IEntityObject) {
		// TODO this demonstrates the wrong definition of api
		(<EntityObject><unknown>eo).getMeta().subscribe(
			meta => {
				let name = meta.getEntityName();
				let title = (<EntityObject><unknown>eo).getTitle();
				if (title) {
					this.titleService.setTitle(title, name);
				} else if (name) {
					this.titleService.setTitle(undefined, name);
				} else {
					this.titleService.setDefaultTitle();
				}
			}
		);
	}
}
