import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Params, Router, UrlTree } from '@angular/router';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import * as _ from 'lodash';
import { EMPTY, from, Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Logger } from '../../log/shared/logger';
import { EntityObject } from './entity-object.class';

@Injectable()
export class EntityObjectNavigationService {

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private location: Location,
		private $log: Logger
	) {
	}

	navigateToEo(eo: IEntityObject, processMetaId?: string): Observable<boolean> {
		let id = eo.getId() || eo.getTemporaryId() || 'new';
		let entityClassId = eo.getEntityClassId();
		this.$log.info('(selecting) Navigating to EO: %o [%o]', id, entityClassId);

		return this.navigateToEoById(
			entityClassId, id, undefined, undefined, undefined, processMetaId)
			.pipe(
				catchError(() => {
					this.$log.error('(selecting) Navigating to EO:%o [%o] failed', id, entityClassId);
					return EMPTY;
				})
			);
	}

	navigateToEoById(
		entityClassId: string,
		eoId: number | string,
		newWindow?: boolean,
		searchFilterId?: string,
		taskListId?: string,
		processMetaId?: string
	): Observable<boolean> {
		let tree = this.createUrlTree(entityClassId, eoId, searchFilterId, taskListId, processMetaId);
		if (newWindow) {
			let baseUrl = window.location.href.substring(0, window.location.href.indexOf('/#') + 2);
			window.open(baseUrl + this.router.serializeUrl(tree), '_blank');
			return of(true);
		}
		return from(this.router.navigateByUrl(tree));
	}

	navigateToNew(entityClassId: string) {
		return this.router.navigate(['/view', entityClassId, 'new']);
	}

	navigateToView(entityClassId: string, processMetaId?: string) {
		let queryParams = this.extendRouteParams(undefined, undefined, processMetaId);
		let tree = this.router.createUrlTree(['/view', entityClassId], {queryParams: queryParams});
		return this.router.navigateByUrl(tree);
	}

	setLocationToNew(entityClassId: string) {
		let tree = this.router.createUrlTree(['/view', entityClassId, 'new']);
		let url = this.router.serializeUrl(tree);
		this.location.go(url);
	}

	setLocationToEo(eo: IEntityObject) {
		this.$log.info('set location to EO: %o', eo && (<EntityObject><unknown>eo).getTitle());
		let entityClassId = eo.getEntityClassId();
		let id = eo.getId() || eo.getTemporaryId() || 'new';

		return this.setLocationEoById(entityClassId, id);
	}

	setLocationEoById(
		entityClassId: string,
		eoId: number | string,
		searchFilterId?: string,
		taskListId?: string
	) {
		let tree = this.createUrlTree(entityClassId, eoId, searchFilterId, taskListId);
		let url = this.router.serializeUrl(tree);
		this.location.replaceState(url);
	}

	private createUrlTree(
		entityClassId: string,
		eoId: number | string,
		searchFilterId?: string,
		taskListId?: string,
		processMetaId?: string
	): UrlTree {
		let commands = [
			this.router.url.indexOf('/popup/') > -1 ? '/popup' : '/view',
			entityClassId,
			eoId
		];
		let queryParams = this.extendRouteParams(searchFilterId, taskListId, processMetaId);
		return this.router.createUrlTree(commands, {queryParams: queryParams});
	}

	private extendRouteParams(
		searchFilterId?: string,
		taskListId?: string,
		processMetaId?: string
	) {
		let queryParams: Params = this.route.snapshot.queryParams;
		let newParams = {};
		_.assign(newParams, queryParams);

		if (searchFilterId) {
			newParams['searchFilterId'] = searchFilterId;
		}
		if (taskListId) {
			newParams['taskListId'] = taskListId;
		}
		if (processMetaId) {
			newParams['processMetaId'] = processMetaId;
		}
		return newParams;
	}
}
