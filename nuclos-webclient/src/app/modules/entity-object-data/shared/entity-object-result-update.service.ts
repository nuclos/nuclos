import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SideviewmenuService } from '@modules/entity-object/shared/sideviewmenu.service';
import { SidebarLayoutType } from '@modules/preferences/preferences.model';
import { EMPTY, Observable, of, Subject } from 'rxjs';

import { delay, finalize, map, mergeMap, retryWhen, take, takeWhile, tap } from 'rxjs/operators';
import { ObservableUtils } from '../../../shared/observable-utils';
import { AuthenticationService } from '../../authentication';
import { EntityObjectGridMultiSelectionResult } from '../../entity-object-grid/entity-object-grid-multi-selection-result';
import { LoadMoreResultsEvent } from '../../entity-object/entity-object.component';
import { EntityObjectPreferenceService } from '../../entity-object/shared/entity-object-preference.service';
import { InputRequiredService } from '../../input-required/shared/input-required.service';
import { Logger } from '../../log/shared/logger';
import { SearchService } from '../../search/shared/search.service';
import { BoViewModel } from './bo-view.model';
import { DataService, LoadContext } from './data.service';
import { EntityObjectErrorService } from './entity-object-error.service';
import { EntityObjectResultService } from './entity-object-result.service';
import { EntityObjectSearchfilterService } from './entity-object-searchfilter.service';
import { ResultParams } from './result-params';

@Injectable()
export class EntityObjectResultUpdateService {
	private loading = false;

	/**
	 * Triggered when the EO list should be reloaded.
	 */
	private mustLoadListSubject = new Subject<any>();

	private collectiveProcessingInitiated = new Subject<CollectiveProcessingExecution>();

	private collectiveProcessingProgress = new Subject<CollectiveProcessingProgressInfo>();

	private collectiveProcessingFinished = new Subject<any>();

	private collectiveProcessingAborted = new Subject<any>();

	private searchFilterId;
	private taskListId;
	private sidebarView: SidebarLayoutType;
	private collectiveProcessingAction: CollectiveProcessingAction | undefined;

	constructor(
		private $log: Logger,
		private authenticationService: AuthenticationService,
		private dataService: DataService,
		private eoPreferenceService: EntityObjectPreferenceService,
		private eoResultService: EntityObjectResultService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private inputRequired: InputRequiredService,
		private eoErrorService: EntityObjectErrorService,
		private route: ActivatedRoute,
		private searchService: SearchService,
		private sideviewMenuService: SideviewmenuService,
		private http: HttpClient
	) {
		this.authenticationService.observeLoginStatus().subscribe(loggedIn => {
			if (!loggedIn) {
				this.eoResultService.clear();
			}
		});

		this.eoSearchfilterService
			.observeSelectedSearchfilter()
			.subscribe(() => this.triggerLoadList());

		this.route.queryParams.subscribe(params => {
			this.taskListId = params['taskListId'];
		});

		this.searchService
			.subscribeDataLoad()
			.subscribe(() => this.eoResultService.invalidateCache());

		this.sideviewMenuService.getViewType().subscribe(
			view => this.sidebarView = view
		);
	}

	observeMustLoadList(): Observable<any> {
		return this.mustLoadListSubject;
	}

	triggerLoadList() {
		if (!this.authenticationService.isLoggedIn()) {
			this.$log.warn('Skipping result list update - requires login');
			return;
		}

		this.$log.debug('Trigger loading of eo list');

		let selectedEntityClassId = this.eoResultService.getSelectedEntityClassId();
		if (selectedEntityClassId) {
			let entityMeta = this.eoResultService.getSelectedMeta();
			if (entityMeta && entityMeta.getBoMetaId() === selectedEntityClassId) {
				this.mustLoadListSubject.next(true);
			} else {
				this.eoResultService
					.updateSelectedMeta()
					.subscribe(() => this.mustLoadListSubject.next(true));
			}
		} else {
			this.$log.warn('Cannot load result list - no entity class selected');
		}
	}

	/**
	 * load data - will be called subsequently when scrolling down in sidebar
	 */
	loadData(event: LoadMoreResultsEvent): Observable<BoViewModel | undefined> {
		return this.loadResultsForSelectedEntityClass(event).pipe(
			tap(boViewModel => {
				if (boViewModel !== undefined) {
					this.eoResultService.addNewData(boViewModel);

					// reset selected EO if list is empty and bo meta is different
					const selectedEo = this.eoResultService.getSelectedEo();
					if (
						boViewModel.bos.length === 0 &&
						selectedEo &&
						selectedEo.getId() !== null &&
						selectedEo.getEntityClassId() !== boViewModel.boMetaId
					) {
						this.eoResultService.selectEo(undefined);
					}
				}
			})
		);
	}

	isLoading() {
		return this.loading;
	}

	setLoading(loading) {
		this.loading = loading;
	}

	getCollectiveProcessingAction(): CollectiveProcessingAction | undefined {
		return this.collectiveProcessingAction;
	}

	isEntityObjectIncludedInResult(entityObjectId?: string): Observable<boolean> {
		const meta = this.eoResultService.getSelectedMeta();

		if (!meta) {
			return EMPTY;
		} else {
			let params: ResultParams = {
				offset: 0,
				chunkSize: 1,
				countTotal: false,
				searchFilterId: this.searchFilterId,
				taskListId: this.taskListId,
				entityObjectId: entityObjectId,
				entityObjectIdOnlySelection: true
			};

			const vlpId = this.eoResultService.vlpId;
			const vlpParams: HttpParams | undefined = this.eoResultService.vlpParams;

			return this.dataService
				.loadList(
					meta,
					vlpId,
					vlpParams,
					this.eoPreferenceService.getSelectedTablePreference(),
					this.eoSearchfilterService.getSelectedSearchfilter(),
					params
				)
				.pipe(map(boViewModel => boViewModel !== undefined && boViewModel.bos.length > 0));
		}
	}

	executeCollectiveProcessing(
		multiSelectionResult: EntityObjectGridMultiSelectionResult,
		action: CollectiveProcessingAction
	): Observable<CollectiveProcessingExecution> {
		this.collectiveProcessingAction = action;
		let observable: Observable<CollectiveProcessingExecution>;
		const dataLoadContext = this.buildDataLoadContext();
		if (!dataLoadContext) {
			observable = EMPTY;
		} else {
			dataLoadContext.resultParams = {
				offset: 0,
				chunkSize: -1,
				countTotal: false,
				searchFilterId: this.searchFilterId,
				taskListId: this.taskListId
			};
			dataLoadContext.multiSelectionCondition = multiSelectionResult.toRestMultiSelectionResult();
			observable = this.dataService.executeCollectiveProcessing(dataLoadContext, action.links.execute.href!);
		}

		return ObservableUtils.onSubscribe(observable, () => {
			this.setLoading(true);
		}).pipe(
			tap((execution: CollectiveProcessingExecution) => {
				this.progressCollectiveProcessing(execution);
			}),
			finalize(() => {
				this.setLoading(false);
			})
		);
	}

	executeCollectiveEditProcessing(
		multiSelectionResult: EntityObjectGridMultiSelectionResult,
		editSelection: EditSelectionResult,
		restUrl: string
	): Observable<CollectiveProcessingExecution> {
		let observable: Observable<CollectiveProcessingExecution>;
		const dataLoadContext = this.buildDataLoadContext();
		if (!dataLoadContext) {
			observable = EMPTY;
		} else {
			dataLoadContext.resultParams = {
				offset: 0,
				chunkSize: -1,
				countTotal: false,
				searchFilterId: this.searchFilterId,
				taskListId: this.taskListId
			};
			dataLoadContext.editSelectionResult = editSelection;
			dataLoadContext.multiSelectionCondition = multiSelectionResult.toRestMultiSelectionResult();
			observable = this.dataService.executeCollectiveProcessing(dataLoadContext, restUrl);
		}

		return ObservableUtils.onSubscribe(observable, () => {
			this.setLoading(true);
		}).pipe(
			tap((execution: CollectiveProcessingExecution) => {
				this.progressCollectiveProcessing(execution);
			}),
			finalize(() => {
				this.setLoading(false);
			})
		);
	}

	getCollectiveProcessingInformation(
		multiSelectionResult: EntityObjectGridMultiSelectionResult,
		restUrl: string
	): Observable<any> {
		let observable: Observable<any>;
		const dataLoadContext = this.buildDataLoadContext();
		if (!dataLoadContext) {
			observable = EMPTY;
		} else {
			dataLoadContext.resultParams = {
				offset: 0,
				chunkSize: -1,
				countTotal: false,
				searchFilterId: this.searchFilterId,
				taskListId: this.taskListId
			};
			dataLoadContext.multiSelectionCondition = multiSelectionResult.toRestMultiSelectionResult();
			observable = this.dataService.getCollectiveProcessingInformation(dataLoadContext, restUrl);
		}

		return observable;
	}

	getCollectiveProcessingAttributeInformation(attributeID: string): Observable<any> {
		let restLink = this.eoResultService
			.getCollectiveEditing()
			.getLinks().info.href! + '/' + attributeID;
		return this.getCollectiveProcessingInformation(
			this.eoResultService.getMultiSelectionResult(),
			restLink
		);
	}

	abortCollectiveProcessing(
		restUrl: string
	): Observable<Object> {
		let observable: Observable<Object>;
		observable = this.dataService.abortCollectiveProcessing(restUrl);

		return ObservableUtils.onSubscribe(observable, () => {
			this.setLoading(true);
		}).pipe(
			tap((response: any) => {
				this.collectiveProcessingAborted.next(true);
				this.collectiveProcessingAborted.complete();
				this.$log.debug('Got response: %o', response);
			}),
			finalize(() => {
				this.setLoading(false);
			})
		);
	}

	progressCollectiveProcessing(execution: CollectiveProcessingExecution) {
		this.eoResultService.forceCollectiveProcessingView = true;
		this.collectiveProcessingInitiated.next(execution);

		let postData = {};
		let globalResults = {};
		let itemCount = 0;

		function updateGlobalResults() {
			if (postData['inputrequired']) {
				if (postData['inputrequired']['applyForMultiEdit'] === true) {
					// save result in globalResults
					const key = postData['inputrequired']['specification']['key'] as string;
					globalResults[key] = postData['inputrequired']['result'][key];
				}
			}
		}

		const progressHttpGetSubscription = this.http
			.post(execution.links.progress.href!, postData)
			.pipe(
				takeWhile(() => this.eoResultService.forceCollectiveProcessingView),
				map((info: CollectiveProcessingProgressInfo) => {
					this.collectiveProcessingProgress.next(info);

					if (info.percent >= 100) {
						this.eoResultService.invalidateCache();
						this.eoResultService.updateSelectedMeta();
						this.collectiveProcessingFinished.next(true);
						progressHttpGetSubscription.unsubscribe();
					} else {
						updateGlobalResults(); // needed to get last result if successful
						if (info.objectInfos) {
							if (itemCount > 0 && itemCount < info.objectInfos.length) {
								if (postData['inputrequired'] !== undefined && Object.keys(globalResults).length === 0) {
									postData['inputrequired'] = undefined;
								} else {
									postData['currentRowInfoNumber'] = (postData['currentRowInfoNumber'] as number) + 1;
									postData['inputrequired'] = {result: globalResults};
								}
							}
							itemCount = info.objectInfos.length;
						}
						throw info;
					}
				}),
				retryWhen(
					// Retry until all InputRequired exceptions are handled
					errors => errors.pipe(mergeMap(
						error => {
							if (error.error) {
								if (error.error.objectInfos) {
									this.collectiveProcessingProgress.next(error.error.objectInfos);
									let currentObject = error.error.objectInfos.find(r =>
										r.inputRequired !== undefined && r.inputRequired);

									if (currentObject !== undefined) {
										postData['currentRowInfoNumber'] = currentObject.infoRowNumber;
									}
								}
								let returnable = this.inputRequired.handleError(error, postData, undefined);

								updateGlobalResults();

								return returnable;
							}

							return of(error).pipe(delay(500));
						})
					)
				)
			)
			.subscribe();
	}

	loadCollectiveProcessingSelectionOptions(
		multiSelectionResult: EntityObjectGridMultiSelectionResult
	): Observable<CollectiveProcessingSelectionOptions> {
		return this.eoResultService.updateSelectedMeta().pipe(
			mergeMap(() => {
				let observable: Observable<CollectiveProcessingSelectionOptions>;
				const dataLoadContext = this.buildDataLoadContext();
				if (!dataLoadContext) {
					observable = EMPTY;
				} else {
					dataLoadContext.resultParams = {
						offset: 0,
						chunkSize: -1,
						countTotal: false,
						searchFilterId: this.searchFilterId,
						taskListId: this.taskListId
					};
					dataLoadContext.multiSelectionCondition = multiSelectionResult.toRestMultiSelectionResult();
					observable = this.dataService
						.loadCollectiveProcessingSelectionOptions(dataLoadContext)
						.pipe
						// tap(selOptions => selOptions.boMetaId = dataLoadContext.meta.getEntityClassId()),
						(take(1));
				}

				return ObservableUtils.onSubscribe(observable, () => this.setLoading(true)).pipe(
					finalize(() => this.setLoading(false))
				);
			})
		);
	}

	subscribeCollectiveProcessingInitiated(): Subject<CollectiveProcessingExecution> {
		return this.collectiveProcessingInitiated;
	}

	subscribeCollectiveProcessingProgress(): Subject<CollectiveProcessingProgressInfo> {
		return this.collectiveProcessingProgress;
	}

	subscribeCollectiveProcessingFinished(): Subject<any> {
		return this.collectiveProcessingFinished;
	}

	private buildDataLoadContext(): LoadContext | undefined {
		const meta = this.eoResultService.getSelectedMeta();
		if (!meta) {
			return undefined;
		}
		let result: LoadContext = {
			meta: meta,
			vlpId: this.eoResultService.vlpId,
			vlpParams: this.eoResultService.vlpParams,
			attributeSelection: this.eoPreferenceService.getSelectedTablePreference(),
			searchtemplatePreference: this.eoSearchfilterService.getSelectedSearchfilter()
		};
		return result;
	}

	private loadResultsForSelectedEntityClass(
		event: LoadMoreResultsEvent
	): Observable<BoViewModel | undefined> {
		return this.eoResultService.updateSelectedMeta().pipe(
			mergeMap(() => {
				let observable: Observable<BoViewModel | undefined>;
				const meta = this.eoResultService.getSelectedMeta();
				const vlpId = this.eoResultService.vlpId;
				const vlpParams: HttpParams | undefined = this.eoResultService.vlpParams;
				if (!meta) {
					observable = EMPTY;
				} else {
					let resultParams: ResultParams = {
						offset: event.offset,
						chunkSize: event.limit,
						countTotal: event.count,
						searchFilterId: this.searchFilterId,
						taskListId: this.taskListId,
						sidebarView: this.sidebarView
					};
					observable = this.dataService
						.loadList(
							meta,
							vlpId,
							vlpParams,
							this.eoPreferenceService.getSelectedTablePreference(),
							this.eoSearchfilterService.getSelectedSearchfilter(),
							resultParams
						)
						.pipe(tap(boViewModel => {
							if (boViewModel !== undefined) {
								boViewModel.boMetaId = meta.getBoMetaId();
							}
						}));
				}

				return ObservableUtils.onSubscribe(observable, () => this.setLoading(true)).pipe(
					finalize(() => this.setLoading(false))
				);
			})
		);
	}
}
