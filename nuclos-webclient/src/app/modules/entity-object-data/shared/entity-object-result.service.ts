import { HttpParams } from '@angular/common/http';
import { Injectable, Input, OnInit } from '@angular/core';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { LocalStorageService } from '@shared/service/local-storage.service';
import {
	BehaviorSubject,
	EMPTY,
	Observable,
	of as observableOf,
	Subject,
	throwError as observableThrowError
} from 'rxjs';

import { distinctUntilChanged, finalize, mergeMap, switchMap, take, tap } from 'rxjs/operators';
import { CollectiveEditingContent } from '../../../data/schema/collective-editing-content';
import { AuthenticationService } from '../../authentication';
import { NuclosCacheService } from '../../cache/shared/nuclos-cache.service';
import {
	EntityObjectGridMultiSelectionResult
} from '../../entity-object-grid/entity-object-grid-multi-selection-result';
import { Logger } from '../../log/shared/logger';
import { SearchService } from '../../search/shared/search.service';
import { BoViewModel, EntityMeta } from './bo-view.model';
import { EntityObjectEventService } from './entity-object-event.service';
import { EntityObjectNavigationService } from './entity-object-navigation.service';
import { EntityObject } from './entity-object.class';
import { EntityObjectService } from './entity-object.service';
import { MetaService } from './meta.service';

/**
 * Holds currently selected entity object(s).
 */
@Injectable()
export class EntityObjectResultService implements OnInit {
	static instance: EntityObjectResultService;

	canCreateBo: boolean | undefined;
	canCreateBoForEntity: string | undefined;

	// true: collective processing is executing or result must be confirmed
	@Input() forceCollectiveProcessingView = false;

	@Input() showCollectiveProcessing = false;

	@Input() showCollectiveEditing = false;

	vlpId: string | undefined;
	vlpParams: HttpParams | undefined;

	/**
	 * Holds information about collective processing editing attributes
	 */
	private collectiveEditingInfo: CollectiveEditingContent;

	/**
	 * Currently selected entity class ID (aka "boMetaId").
	 */
	private selectedEntityClassId = new BehaviorSubject<string | undefined>(undefined);

	private selectedMeta: EntityMeta | undefined;

	/**
	 * Currently selected entity object ID (aka "boId").
	 */
	private selectedEoId: number | string | undefined;

	/**
	 * Currenty selected entity object, should be always consistent with selected EO class and ID.
	 */
	private selectedEo: IEntityObject | undefined;

	private entityObjects: IEntityObject[] = [];

	/**
	 * Determines if there already was result data for the currently selected entity class.
	 * This is important to decide about auto-selection of the first result, when new results are loaded.
	 * Auto-selection should not occur initially if an EO was directly selected (via deep link).
	 * But it should occur if new results are loaded after search filter changes etc.
	 */
	private pristine = true;

	private totalEoCount: number | undefined;

	private eoSelectionInProgress = false;

	private eoNotFound = false;

	private resultListUpdateSubject = new Subject<boolean>();

	private eoSelectionSubject = new Subject<boolean>();

	private multiSelectionResult = new EntityObjectGridMultiSelectionResult();
	private multiSelectionChangeSubject = new Subject<EntityObjectGridMultiSelectionResult>();

	constructor(
		private searchService: SearchService,
		private eoEventService: EntityObjectEventService,
		private eoNavigationService: EntityObjectNavigationService,
		private eoVLPService: EntityVLPService,
		private authenticationService: AuthenticationService,
		private metaService: MetaService,
		private $log: Logger,
		private cacheService: NuclosCacheService,
		private localStorageService: LocalStorageService,
	) {
		EntityObjectResultService.instance = this;
	}

	ngOnInit() {
		this.eoEventService.observeDeletedEo().subscribe(eo => {
			this.removeEo(eo);

			// TODO: Check if the deleted EO really belonged to the currently shown EOs
			if (this.totalEoCount) {
				this.totalEoCount--;
			}
		});

		this.eoEventService.observeCreatedEo().subscribe(() => {
			// TODO: Check if the created EO really belongs to the currently shown EOs
			if (this.totalEoCount !== undefined) {
				this.totalEoCount++;
			}
		});

		this.authenticationService.observeLoginStatus().subscribe(loggedIn => {
			if (!loggedIn) {
				this.selectEo(undefined);
			}
		});

		this.observeSelectedEntityClassId().subscribe(() => {
			this.clear();
			this.updateSelectedMeta().subscribe();
			this.authenticationService.setLastEntityCookie(this.getSelectedEntityClassId());
		});
	}

	getSelectedEntityClassId() {
		return this.selectedEntityClassId.getValue();
	}

	getSelectedMeta() {
		return this.selectedMeta;
	}

	updateSelectedMeta(): Observable<EntityMeta> {
		let entityClassId = this.getSelectedEntityClassId();

		if (!entityClassId) {
			this.selectedMeta = undefined;
			return EMPTY;
		}

		return this.metaService
			.getEntityMeta(entityClassId)
			.pipe(tap(meta => (this.selectedMeta = meta)));
	}

	getSelectedEoId(): number | string | undefined {
		return this.selectedEoId;
	}

	getSelectedEo(): IEntityObject | undefined {
		return this.selectedEo;
	}

	isSelected(eo: EntityObject) {
		if (eo.getEntityClassId() !== this.getSelectedEntityClassId()) {
			return false;
		}

		if (eo.isNew()) {
			if (eo.getTemporaryId()) {
				return this.getSelectedEoId() === eo.getTemporaryId();
			}
			return this.getSelectedEoId() === 'new';
		}

		return eo.getId() === this.getSelectedEoId();
	}

	selectEo(eo: IEntityObject | undefined, navigate = true, enableLocationToFallback = true, processMetaId?: string) {
		// only select if eo changed and if previous was not new
		if (this.isSameEOToCurrentlySelected(eo)) {
			this.$log.debug('Not selecting EO: %o cause it is already selected %o', eo, this.selectedEo);
			return;
		}
		this.$log.debug('Selecting EO: %o', eo);
		this.selectedEo = eo;

		this.selectedEoId = undefined;
		if (eo !== undefined) {
			this.selectEntityClassId(eo.getEntityClassId());
			this.selectedEoId = eo.getId() || eo.getTemporaryId() || 'new';
			eo['navigated'] = navigate;
		}

		this.eoSelectionInProgress = false;

		this.eoEventService.emitSelectedEo(eo);
		if (eo !== undefined) {
			if (navigate) {
				this.eoNavigationService.navigateToEo(eo, processMetaId)
					.pipe(
						take(1)
					).subscribe();
			} else if (enableLocationToFallback) {
				this.eoNavigationService.setLocationToEo(eo);
			}
		}
	}

	waitForEoSelection(): Observable<boolean> {
		if (!this.eoSelectionInProgress) {
			return observableOf(false);
		}

		return this.eoSelectionSubject.pipe(take(1));
	}

	/**
	 * Loads the EO for the given entity and ID
	 * and sets it as the selected EO.
	 * The ID could also be a temporary ID, starting with 'temp_'.
	 */
	selectEoByClassAndId(
		entityClassId: string | undefined,
		entityObjectId: number | string,
		navigate = true
	): Observable<IEntityObject> {
		let selectedEO = this.getSelectedEo();

		this.updateEONotFound(false);

		if (this.eoSelectionInProgress) {
			this.$log.warn('EO selection is already in progress, ignoring');
			if (selectedEO) {
				return observableOf(selectedEO);
			} else {
				return EMPTY;
			}
		}

		if (
			selectedEO &&
			selectedEO.getEntityClassId() === entityClassId &&
			'' + selectedEO.getId() === '' + entityObjectId
		) {
			this.selectedEoId = selectedEO.getId();
			return observableOf(selectedEO);
		}

		this.eoSelectionInProgress = true;

		return this.getEoService()
			.loadEO(entityClassId, <number>entityObjectId)
			.pipe(tap(
				eo => eo.select(navigate),
				error => this.selectEo(undefined, navigate)))
			.pipe(
				take(1),
				finalize(() => {
					this.eoSelectionInProgress = false;
					this.eoSelectionSubject.next(true);
				})
			);
	}

	selectEntityClassId(entityClassId: string | undefined) {
		if (entityClassId !== this.getSelectedEntityClassId()) {
			// clear everything if we change entityclassid
			this.clear();
			this.$log.info('Selecting entity class: %o', entityClassId);

			// reset search after switching entity
			this.searchService.updateSearchInputText('');

			this.selectedEntityClassId.next(entityClassId);
			this.validateCanCreateBo(entityClassId);
		}
	}

	observeSelectedEntityClassId(): Observable<string | undefined> {
		return this.selectedEntityClassId.pipe(distinctUntilChanged());
	}

	observeResultListUpdate(): Observable<boolean> {
		return this.resultListUpdateSubject;
	}

	observeMultiSelectionChange(): Observable<EntityObjectGridMultiSelectionResult> {
		return this.multiSelectionChangeSubject;
	}

	notifyMultiSelectionChange(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this.$log.debug('Update multiselection result: %o', multiSelectionResult);
		this.multiSelectionResult = multiSelectionResult;
		this.saveMultiSelectionResultToStorage(multiSelectionResult);
		this.multiSelectionChangeSubject.next(multiSelectionResult);
	}

	getMultiSelectionResult(): EntityObjectGridMultiSelectionResult {
		return this.multiSelectionResult;
	}

	resetEo(eo: IEntityObject | undefined) {
		if (eo) {
			eo.reset();
			this.eoEventService.emitResetEo(eo);
		}
	}

	navigateToNew(processMetaId?: string | undefined) {
		let entityClassId = this.getSelectedEntityClassId();
		if (entityClassId) {
			// this.eoNavigationService.navigateToNew(entityClassId);
			this.eoNavigationService.setLocationToNew(entityClassId);
			this.createNew(processMetaId).subscribe(
				eo => {
					(eo as unknown as EntityObject).select(false, false);
				}
			);
		}
	}

	createNew(processMetaId?: string | undefined): Observable<IEntityObject> {
		let entityClassId = this.getSelectedEntityClassId();
		if (!entityClassId) {
			return EMPTY;
		}

		return this.getEoService()
			.createNew(entityClassId, undefined, processMetaId)
			.pipe(tap(eo => this.entityObjects.unshift(eo)));
	}

	updateEONotFound(val: boolean) {
		this.eoNotFound = val;
	}

	isEoNotFound() {
		return this.eoNotFound;
	}

	getResults() {
		return this.entityObjects;
	}

	addNewData(boViewModel: any) {
		this.entityObjects.push(...boViewModel.bos);
		this.totalEoCount = boViewModel.total;
		this.canCreateBo = boViewModel.canCreateBo;
		this.canCreateBoForEntity = boViewModel.boMetaId;
		this.resultsUpdated();
	}

	selectFirstResult() {
		this.$log.debug('Selecting first result...');
		if (this.entityObjects && this.entityObjects.length > 0) {
			if (this.selectedEo && this.findEoInResults(this.selectedEo)) {
				this.$log.warn('Already selected a result from the result list');
				return;
			}
			let firstResult = this.entityObjects[0];
			if (!firstResult.isNew()) {
				firstResult.reload().subscribe(() => {
						(firstResult as unknown as EntityObject).select(false);
						this.updateEONotFound(false);
					}
				);
			}
		}
	}

	getTotalResultCount() {
		return this.totalEoCount;
	}

	/**
	 * The number of results loaded from the server.
	 * Might be smaller than the current number of EOs, as the user can add new EOs.
	 */
	getLoadedResultCount() {
		return this.entityObjects.length;
	}

	validateCanCreateBo(boMetaId: string | undefined) {
		if (boMetaId) {
			if (boMetaId !== this.canCreateBoForEntity) {
				this.reloadCanCreateBoIfNotSet(boMetaId, true);
			}
		} else {
			this.canCreateBo = undefined;
			this.canCreateBoForEntity = undefined;
		}
	}

	reloadCanCreateBoIfNotSet(boMetaId: string, force = false) {
		if (force || this.canCreateBo === undefined) {
			this.getEoService()
				.loadEmptyList(boMetaId)
				.subscribe(model => {
					this.canCreateBo = model.canCreateBo;
					this.canCreateBoForEntity = boMetaId;
				});
		}
	}

	setPristine(pristine: boolean) {
		this.pristine = pristine;
	}

	clear() {
		this.selectedMeta = undefined;
		this.selectEo(undefined);
		this.entityObjects = [];
		this.totalEoCount = undefined;
		this.canCreateBo = undefined;
		this.canCreateBoForEntity = undefined;
		this.pristine = true;
	}

	resetResults() {
		this.entityObjects = [];
	}

	addSelectedEoToList() {
		if (this.selectedEo && !this.selectedMeta?.shouldRefreshOnSearch()) {
			if (!this.findEoInResults(this.selectedEo)) {
				this.entityObjects.unshift(this.selectedEo);
				this.resultsUpdated();
			}
		} else {
			this.$log.debug('No EO selected - can not add to result list');
		}
	}

	cachedResults(requestHash: string, observable: Observable<BoViewModel  | undefined>) {
		return this.cacheService.getCache('eoResultData').get(requestHash, observable);
	}

	invalidateCache() {
		this.cacheService.getCache('eoResultData').clear();
	}

	setSelectedMeta(meta: EntityMeta) {
		this.selectedMeta = meta;
	}

	retrieveMultiSelectionResultFromStorage(): EntityObjectGridMultiSelectionResult {
		let selectionResult = new EntityObjectGridMultiSelectionResult();
		let multiSelectionResult = localStorage.getItem('multiSelectionResult');
		if (multiSelectionResult !== null) {
			this.$log.debug('Read selection result from storage: %o %o', multiSelectionResult,
				JSON.parse(atob(multiSelectionResult)) as EntityObjectGridMultiSelectionResult);
			selectionResult.readFromRestMultiSelectionResult(JSON.parse(atob(multiSelectionResult)));
		}

		this.$log.debug('Selection result selectAll: %o', selectionResult.hasSelection());
		return selectionResult;
	}

	deleteMultiSelectionResultFromStroage() {
		localStorage.removeItem('multiSelectionResult');
	}

	updateCollectiveEditing(info: CollectiveEditingContent) {
		this.collectiveEditingInfo = info;
	}

	getCollectiveEditing(): CollectiveEditingContent {
		return this.collectiveEditingInfo;
	}

	private isSameEOToCurrentlySelected(eo: IEntityObject | undefined) {
		return this.selectedEo && eo &&
			this.selectedEoId && !isNaN(+this.selectedEoId) &&
			eo.getId() === this.selectedEo.getId() &&
			eo.getEntityClassId() === this.selectedEo.getEntityClassId();
	}

	private saveMultiSelectionResultToStorage(result: EntityObjectGridMultiSelectionResult) {
		localStorage.setItem('multiSelectionResult', btoa(JSON.stringify(result.toRestMultiSelectionResult())));
	}


	private getEoService() {
		return EntityObjectService.instance;
	}

	/**
	 * Removes the EO with the given ID from the list.
	 */
	private removeEo(eo: IEntityObject): void {
		let eoId = eo.getId();

		let entityObjects = this.getResults();

		let wasSelected = eo === this.selectedEo;
		if (wasSelected) {
			this.selectEo(undefined);
		}

		// May not have been loaded yet
		if (!this.entityObjects) {
			return;
		}

		let index = this.entityObjects.findIndex(e => e.getId() === eo.getId());
		this.$log.debug('removeEO(%o): index = %o', eoId, index);
		if (index > -1) {
			entityObjects.splice(index, 1);
			// If the removed EO was selected, select the previous EO in the list
			if (wasSelected) {
				if (index > 0) {
					index--;
				}
				let nextEO = entityObjects[index];
				if (nextEO) {
					this.eoNavigationService.navigateToEo(nextEO).pipe(
						take(1)
					).subscribe();
				}
			}
		} else {
			this.$log.warn('Not found: %o', eo);
		}
	}

	private resultsUpdated() {
		this.resultListUpdateSubject.next(this.pristine);
		this.pristine = false;
	}

	private findEoInResults(eo: IEntityObject) {
		let index = this.findEoIndexInResults(eo);

		if (index < 0) {
			return undefined;
		}

		return this.entityObjects[index];
	}

	private findEoIndexInResults(eo: IEntityObject): number {
		if (eo.isNew()) {
			return this.entityObjects.findIndex(
				resultEo => resultEo === eo || resultEo.getTemporaryId() === eo.getTemporaryId()
			);
		} else {
			return this.entityObjects.findIndex(resultEo => resultEo.getId() === eo.getId());
		}
	}
}
