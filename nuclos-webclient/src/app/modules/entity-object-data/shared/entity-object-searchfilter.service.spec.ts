import { inject, TestBed } from '@angular/core/testing';

import { EntityObjectSearchfilterService } from './entity-object-searchfilter.service';

xdescribe('EntityObjectSearchfilterService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [EntityObjectSearchfilterService]
		});
	});

	it('should be created', inject([EntityObjectSearchfilterService], (service: EntityObjectSearchfilterService) => {
		expect(service).toBeTruthy();
	}));
});
