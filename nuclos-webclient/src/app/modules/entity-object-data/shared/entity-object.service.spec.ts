/* tslint:disable:no-unused-variable */
import { inject, TestBed } from '@angular/core/testing';
import { EntityObjectService } from './entity-object.service';

xdescribe('Service: EntityObject', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [EntityObjectService]
		});
	});

	it('should ...', inject([EntityObjectService], (service: EntityObjectService) => {
		expect(service).toBeTruthy();
	}));
});
