import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { IEntityObject, IEntityObjectDependents, ISubEntityObject } from '@nuclos/nuclos-addon-api';
import { FqnService } from '@shared/service/fqn.service';
import { FORBIDDEN } from 'http-status-codes';
import {
	EMPTY,
	forkJoin,
	Observable,
	of,
	of as observableOf,
	Subject,
	throwError,
	throwError as observableThrowError
} from 'rxjs';

import { catchError, filter, finalize, map, mergeMap, retryWhen, switchMap, take, tap } from 'rxjs/operators';
import { ValidationStatus } from '../../../data/schema/validation-status.enum';
import { LayoutService } from '../../../layout/shared/layout.service';
import { ObservableUtils } from '../../../shared/observable-utils';
import { LocalStorageService } from '../../../shared/service/local-storage.service';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { NuclosValidationService } from '../../../shared/service/nuclos-validation.service';
import { NuclosCacheService } from '../../cache/shared/nuclos-cache.service';
import { CommandService } from '../../command/shared/command.service';
import { InputRequiredService } from '../../input-required/shared/input-required.service';
import { Logger } from '../../log/shared/logger';
import { RuleService } from '../../rule/shared/rule.service';
import { NuclosStateService } from '../../state/shared/nuclos-state.service';
import { State } from '../../state/shared/state';
import { AutonumberService } from './autonumber.service';
import { BoViewModel, EntityAttrMeta, EntityMeta, EntityObjectData, LovEntry } from './bo-view.model';
import { EntityObjectDependency } from './entity-object-dependency';
import { EntityObjectErrorService } from './entity-object-error.service';
import { EntityObjectEventService } from './entity-object-event.service';
import { EntityObjectResultUpdateService } from './entity-object-result-update.service';
import { EntityObjectResultService } from './entity-object-result.service';
import { EntityObjectSearchConfig } from './entity-object-search-config';
import { EntityObject, SubEntityObject } from './entity-object.class';
import { LovDataService } from './lov-data.service';
import { LovSearchConfig } from './lov-search-config';
import { MetaService } from './meta.service';

/**
 * Provides access to the currently selected EntityObject.
 */
@Injectable()
export class EntityObjectService {
	static instance: EntityObjectService;

	static readonly EO_LOCALSTORAGE_KEY_PREFIX = 'EntityObject_';
	static readonly SUBEO_SELECTION_LOCALSTORAGE_KEY_PREFIX = 'EntityObject_SubEO_Selection_';

	static equals(...eos: (IEntityObject | undefined)[]): boolean {
		return eos !== undefined && eos.length > 1 &&
			Array.from(new Set(eos.map((eo) => eo?.getId()))).length === 1 &&
			Array.from(new Set(eos.map((eo) => eo?.getEntityClassId()))).length === 1;
	}

	// TODO: Mandator stuff should be in its own module and service.
	/* an EntityObject could be assigned to a certain mandator */
	private selectedMandator: Mandator;

	constructor(
		private nuclosConfig: NuclosConfigService,
		private metaService: MetaService,
		private http: HttpClient,
		private cache: NuclosCacheService,
		private $log: Logger,
		private ruleService: RuleService,
		private inputRequiredService: InputRequiredService,
		private lovDataService: LovDataService,
		private validationService: NuclosValidationService,
		private commandService: CommandService,
		private layoutService: LayoutService,
		private eoEventService: EntityObjectEventService,
		private localStorageService: LocalStorageService,
		private autonumberService: AutonumberService,
		private nuclosStateService: NuclosStateService,
		private eoErrorService: EntityObjectErrorService,
		private eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private eoVLPService: EntityVLPService,
		private dialogService: NuclosDialogService,
		private fqnService: FqnService,
		private i18n: NuclosI18nService,
	) {
		EntityObjectService.instance = this;
	}

	selectMandator(mandator: Mandator) {
		this.selectedMandator = mandator;
	}

	getSelectedMandator(): Mandator {
		return this.selectedMandator;
	}

	publishChanges(eo: EntityObject) {
		if (eo.isNew()) {
			return;
		}

		let key = EntityObjectService.EO_LOCALSTORAGE_KEY_PREFIX + eo.getId();
		this.localStorageService.setItem(key, eo.getData());
		this.localStorageService.removeItem(key);
	}

	/**
	 * Loads the EO for the given entity and ID.
	 */
	loadEO(entityClassId: string | undefined, entityObjectId: any): Observable<EntityObject> {
		this.$log.debug('Loading EO %o (%o)...', entityClassId, entityObjectId);
		return this.http
			.get(this.nuclosConfig.getRestHost() + '/bos/' + entityClassId + '/' + entityObjectId)
			.pipe(
				map((data: EntityObjectData) => new EntityObject(data, this.localStorageService, this.fqnService)),
				tap((eo: EntityObject) => {
					this.eoVLPService.purgeVLPs();
					this.eoVLPService.purgeSearchVLPs();
					this.eoVLPService.addEntityObjectMap(eo);
					eo.prefillAttributeValidationStatus(ValidationStatus.VALID);
				}),
				switchMap((eo: EntityObject) => this.ruleService.updateRuleExecutor(eo))
			);
	}

	reloadEo(eo: IEntityObject): Observable<IEntityObject> {
		let url = eo.getSelfURL();

		if (!url) {
			return observableThrowError('Could not reload EO %o - no self URL');
		}

		return this.http.get<EntityObjectData>(url).pipe(
			map((data: EntityObjectData) => {
				eo.setData(data);

				return eo;
			}),
			switchMap((updatedEo: EntityObject) => this.ruleService.updateRuleExecutor(updatedEo))
		);
	}

	updateRuleExecutor(eo: EntityObject): Observable<EntityObject> {
		return this.ruleService.updateRuleExecutor(eo);
	}

	changeState(eo: EntityObject, state: State): Observable<EntityObject> {
		let result = new Subject<EntityObject>();
		this._changeState(eo, state).subscribe(
			entityObject => {
				this.eoEventService.emitStateChange(entityObject);
				result.next(entityObject);
			},
			error => {
				result.error(error);
			}
		);
		return result;
	}

	/**
	 * Saves the given EO.
	 * If it is new, it is inserted via a POST request.
	 * If it is not new, it is updated via a PUT request on its self-url.
	 *
	 * @param eo
	 * @returns {Observable<R>}
	 */
	save(eo: EntityObject, customRule?: string): Observable<IEntityObject> {
		this.$log.debug('Saving: %o', eo);

		let postData: EntityObjectData = eo.serialize();

		if (customRule) {
			// TODO: Do not manipulate EO data for this!
			// Use the rule execution REST service instead and pass the rule name as parameter.
			postData.executeCustomRule = customRule;
			postData.selection = this.buildSelectionMap(eo.getAllDependents());
		}

		let url = eo.isNew() ? eo.getInsertURL() : eo.getSelfURL();

		// force update of title / info
		delete eo.title;
		delete eo.info;

		// No self-link yet, if this EO is new
		if (!url) {
			// TODO: Get the INSERT-Link via HATEOAS
			url = this.nuclosConfig.getRestHost() + '/bos/' + eo.getEntityClassId();
		}

		let isNew = eo.isNew();
		let tempId = eo.getTemporaryId();
		let result = isNew ? this.http.post(url, postData) : this.http.put(url, postData);

		return this.handleSaveRequest(eo, postData, result).pipe(
			switchMap(() => this.updateRuleExecutor(eo).pipe(
				take(1)
			)),
			tap(() => {
				if (eo.getData() !== undefined && eo.getData() !== null) {
					this.invalidateEoResultsCache();
					if (isNew) {
						// TODO API!!!
						// @ts-ignore
						this.eoEventService.emitCreatedEo(eo);
					}
					if (tempId !== undefined) {
						// we need to clean up localStorage
						this.localStorageService.removeItem(tempId);
					}
					this.eoResultUpdateService
						.isEntityObjectIncludedInResult(eo.getId() + '')
						.pipe()
						.subscribe(isIncluded => {
							let oldTextColor = eo.getTextColor();
							let newTextColor = isIncluded ? undefined : '#aaaaaa';
							if (oldTextColor !== newTextColor) {
								eo.setTextColor(newTextColor);
								// TODO API!!!
								// @ts-ignore
								this.eoEventService.emitColorChange(eo);
							}
						});
				}
			})
		);
	}

	clone(eo: EntityObject, layoutid?: string): Observable<IEntityObject> {
		this.$log.debug('Cloning: %o', eo);

		let url = eo.getCloneURL();

		if (!url) {
			url =
				this.nuclosConfig.getRestHost() +
				'/bos/' +
				eo.getEntityClassId() +
				'/' +
				eo.getId() +
				'/clone';
		}
		let postData = {};
		let requestBody = {
			params: layoutid ? { layoutid: layoutid } : {},
			postData: postData
		};
		let result = this.http.post(url, requestBody);

		return eo.getMeta().pipe(
			map((meta: EntityMeta) => {
				return meta.getEntityClassId();
			}),
			switchMap((metaId: string) => {
				return this.createNew(metaId);
			}),
			switchMap((entityObject: IEntityObject) => {
				// Use handleInputRequired to manage potential InputRequiredException errors
				return this.handleInputRequired(entityObject as EntityObject, requestBody, result).pipe(
					switchMap((data: EntityObjectData) => {
						if (data.subBos) {
							Object.keys(data.subBos).forEach(key => {
								entityObject.getDependents(key).clear();
							});
						}

						(entityObject as unknown as EntityObject).setData(data);

						return of(entityObject);
					})
				);
			})
		);
	}

	executeCustomRule(eo: EntityObject, rule: string) {
		if (eo.isDirty()) {
			// TODO: Use the same REST service here
			return this.save(eo, rule);
		} else {
			let url = eo.getSelfURL() + '/execute/' + rule;

			let postData = this.buildSelectionMap(eo.getAllDependents());
			let result = this.http.post(url, postData);

			return this.handleSaveRequest(eo, postData, result);
		}
	}

	buildSelectionMap(subEntityObjects: Map<string, IEntityObjectDependents>) {
		let subJson = {};
		subEntityObjects.forEach((value, key) => {
			let subEntities: ISubEntityObject[] | undefined = value.current();
			if (subEntities) {
				let selectedSubEntities = subEntities.filter(entity => entity.isSelected()).map(entity => entity.getId());
				let dependants: any[] = [];
				subEntities.forEach(dep => { dependants.push(this.buildSelectionMap(dep.getAllDependents())); });
				subJson[key] = { selectedIds: selectedSubEntities, selectedSubEntities: dependants };
			}
		});
		return subJson;
	}

	/**
	 * Deletes the given EO.
	 * If it is new, it only a "delete" event is emitted.
	 * If it is not new, it is deleted from the server first.
	 *
	 * @param eo
	 * @returns {any}
	 */
	delete(eo: EntityObject): Observable<EntityObject> {
		// If the EO is new, just emit a "deleted" event and return
		if (eo.isNew()) {
			// TODO API!!!
			// @ts-ignore
			this.eoEventService.emitDeletedEo(eo);
			return observableOf(eo);
		}

		// If the EO is not new, delete it on the server first
		let url = eo.getSelfURL();
		if (!url) {
			this.$log.error('EO has no self-URL: %o', eo);
			return observableOf(eo);
		}

		let body = {};
		let request_errored = false;
		return this.http.post(url, body).pipe(
			retryWhen(
				// Retry until all InputRequired exceptions are handled
				errors => {
					return errors.pipe(
						mergeMap(error => {
							// TODO API!!!
							// @ts-ignore
							return this.inputRequiredService.handleError(error, body, eo);
						})
					);
				}
			),
			catchError(error => {
				request_errored = true;
				return this.eoErrorService.handleError(error, eo);
			}),
			finalize(() => {
				if (!request_errored) {
					this.invalidateEoResultsCache();
					eo.deleted();
					// TODO API!!!
					// @ts-ignore
					this.eoEventService.emitDeletedEo(eo);
				}
			}),
			map(() => eo)
		);
	}

	deleteByIds(metaId: string, eoIds: number[]) {
		return forkJoin(
			eoIds.map(eoId => {
				return this.http.delete(this.nuclosConfig.getRestHost() +
					'/bos/' +
					metaId +
					'/' +
					eoId).pipe(
					catchError(error => {
						this.$log.warn(
							'Could not delete EO data for entity meta %o with ids %o: %o',
							metaId,
							eoIds,
							error
						);
						return this.dialogService.alert({
							message: error.error?.message ? error.error.message : error.toString(),
							title: this.i18n.getI18n('webclient.error.title')
						});
					}),
				);
			})
		).pipe(tap(() => this.invalidateEoResultsCache()));
	}

	/**
	 * Unlocks the given EO.
	 */
	unlock(eo: EntityObject): Observable<EntityObject> {
		let links = eo.getLinks();

		if (!links || !links.lock) {
			this.$log.error('EO has no lock-URL: %o', eo);
			return observableOf(eo);
		}

		return this.http.delete(links.lock.href).pipe(
			map(() => {
				eo.clearOwner();
				return eo;
			}),
			catchError(error => this.eoErrorService.handleError(error, eo))
		);
	}

	/**
	 * Creates a new empty EO for the currently selected entity class,
	 * which can be used for inserts.
	 *
	 * @param entityMetaId Optional parent EO, if a subform EO is being created.
	 */
	createNew(
		entityMetaId: string,
		parentEO?: EntityObject,
		processMetaId?: string | undefined
	): Observable<IEntityObject> {
		let options: {};
		if (!!processMetaId) {
			options = {
				params: {
					processMetaId: processMetaId
				}
			};
		}

		return this.metaService.getBoMeta(entityMetaId).pipe(
			map(meta => meta.getLinks().defaultGeneration),
			filter(defaultGenerationLink => !!defaultGenerationLink),
			mergeMap(defaultGenerationLink =>
				this.http.get(defaultGenerationLink!.href, options).pipe(
					map((data: EntityObjectData) => new EntityObject(data, this.localStorageService, this.fqnService) as IEntityObject),
					switchMap((eo: EntityObject) => this.ruleService.updateRuleExecutor(eo, parentEO)),
					tap((eo: EntityObject) => {
						this.eoVLPService.addEntityObjectMap(eo);
						// see the comment from Falk Zilm inside mergeData()
						// we do not want eo.subBos to be set to undefined, so we have to explicitly pass the current value at this point
						eo.mergeData({_flag: 'insert', subBos: eo.getData()?.subBos} as EntityObjectData);

						if (this.selectedMandator) {
							eo.setMandator(this.selectedMandator);
						}

						this.eoEventService.emitAddEo(eo);
					})
				)
			)
		);
	}

	loadDependents(
		parent: EntityObject,
		dependency: EntityObjectDependency,
		searchConfig?: EntityObjectSearchConfig,
		readOnly = false
	) {
		this.$log.debug('Loading dependents for dependency %o...', dependency);

		let url =
			this.nuclosConfig.getRestHost() +
			'/bos/' +
			parent.getRootEo().getEntityClassId() +
			'/' +
			(parent.getRootEo().getId()
				? parent.getRootEo().getId()
				: parent.getRootEo().getTemporaryId()) +
			'/subBos/recursive/' +
			dependency.toPath();

		if (searchConfig) {
			let first = true;
			for (let x in searchConfig) {
				if (searchConfig[x]) {
					if (first) {
						url += '?';
						first = false;
					} else {
						url += '&';
					}
					url += encodeURI(x) + '=' + encodeURI(searchConfig[x]);
				}
			}
		}

		return this.http.get<{ bos: EntityObjectData[] }>(url).pipe(
			map(response => {
				let result: SubEntityObject[] = [];
				let eoData: EntityObjectData[] = response.bos;
				for (let data of eoData) {
					let subEO = new SubEntityObject(
						parent,
						dependency.getReferenceAttributeFqn(),
						data,
						this.localStorageService,
						this.fqnService
					);
					if (data.shadowID) {
						subEO.shadowID = data.shadowID;
					}
					if (subEO.getTemporaryId()) {
						subEO.setDirty(true);
					}
					subEO.prefillAttributeValidationStatus(ValidationStatus.VALID);
					result.push(subEO);
					subEO.getMeta().pipe(take(1)).subscribe(meta => {
						if (!(meta.getMetaData().readonly || subEO.getEntityClassId().endsWith('DYN') || readOnly)) {
							this.eoVLPService.addEntityObjectMap(subEO);
							this.ruleService.updateRuleExecutor(subEO, parent.getRootEo())
								.pipe(
									take(1)
								)
								.subscribe();
						}
					});
				}
				this.$log.debug(
					'Successfully loaded %o dependents for %o',
					result.length,
					dependency
				);
				return result;
			}),
			catchError(error => {
				this.$log.error('Failed to load depenents for %o: %o', dependency, error);
				return observableThrowError(error);
			})
		);
	}

	countDependents(
		parent: EntityObject,
		dependency: EntityObjectDependency
	) {
		this.$log.debug('Counting dependents for dependency %o...', dependency);

		let url =
			this.nuclosConfig.getRestHost() +
			'/bos/' +
			parent.getEntityClassId() +
			'/' +
			(parent.getId()
				? parent.getId()
				: parent.getTemporaryId()) +
			'/subBos/' +
			dependency.getReferenceAttributeFqn() +
			'/countOnly';

		return this.http.get(url).pipe(
			map((response: any) => {
				if (response.hasOwnProperty('total')) {
					return +response.total;
				} else {
					return 0;
				}
			}),
			catchError(error => {
				this.$log.error('Failed to load depenents for %o: %o', dependency, error);
				return observableThrowError(error);
			})
		);
	}

	loadValueList(
		parent: EntityObject,
		dependency: EntityObjectDependency,
		attributeId: string
	): Observable<{ name: string; id: any }[]> {
		this.$log.debug('Loading dependents for dependency %o...', dependency);

		let url =
			this.nuclosConfig.getRestHost() +
			'/bos/' +
			parent.getRootEo().getEntityClassId() +
			'/' +
			parent.getRootEo().getId() +
			'/subBos/' +
			dependency.getReferenceAttributeFqn() +
			'/valuelist/' +
			attributeId;

		return this.http.get<{ name: string; id: any }[]>(url).pipe(
			catchError(error => {
				this.$log.error('Failed to load depenents for %o: %o', dependency, error);
				return observableThrowError(error);
			})
		);
	}

	loadLovEntries(search: LovSearchConfig): Observable<LovEntry[]> {
		return this.lovDataService.loadLovEntries(search);
	}

	/**
	 * Checks for a layout change (because of changed nuclos-process).
	 * Sets the new layout URL on the given EO.
	 *
	 * @param eo
	 */
	nuclosProcessChanged(eo: EntityObject): Observable<any> {
		let nuclosProcess = eo.getNuclosProcess();
		let nuclosProcessId = nuclosProcess && nuclosProcess.id;

		// TODO: Get From HATEOAS
		let url =
			this.nuclosConfig.getRestHost() +
			'/meta/processchange/' +
			nuclosProcessId +
			'/' +
			eo.getEntityClassId();
		if (!eo.isNew()) {
			url += '/' + eo.getId();
		}

		this.cache.getCache('http.GET').get(url, this.http.get<any>(url)).subscribe(data => {
			let layoutURL = data.links && data.links.layout && data.links.layout.href;
			if (layoutURL) {
				eo.setLayoutURL(layoutURL);
			}
		});

		let options: {};
		if (!!nuclosProcessId) {
			options = {
				params: {
					processMetaId: nuclosProcessId
				}
			};
		}

		this.metaService.getBoMeta(eo.getEntityClassId()).pipe(
			map(meta => meta.getLinks().defaultGeneration),
			filter(defaultGenerationLink => !!defaultGenerationLink),
			mergeMap(defaultGenerationLink =>
				this.http.get(defaultGenerationLink!.href, options)))
			.subscribe((newData: EntityObjectData) => {
				if (eo.isNew()) {
					if (eo.getData() !== null && eo.getData() !== undefined && newData) {
						// do not use mergeData, because existing data will be overwritten
						eo.getData()!.nextStates = newData.nextStates;
						eo.getData()!.subBos = newData.subBos;

						eo.setData({...newData, ...eo.getData()});
						for (let attributeName in eo.getData()?.attributes) {
							if (newData.attributes.hasOwnProperty(attributeName)) {
								let value: any;
								let attribute = eo.getData()?.attributes[attributeName];
								if (attribute !== null && attribute !== undefined
									// check reference fields
									&& ((attribute.hasOwnProperty('id') && attribute.hasOwnProperty('name'))
										? (attribute['id'] !== null && attribute['id'] !== undefined && attribute['name'] !== null && attribute['name'] !== undefined)
										: true)) {
									value = attribute;
								} else {
									value = newData.attributes[attributeName];
								}
								eo.setAttributeUnsafe(attributeName, value);
							}
						}

						eo.reloaded();
					}
				} else {
					let mpEoDataSubBos: Map<string, any> = new Map(Object.entries(eo.getData()?.subBos));
					let mpDataSubBos: Map<string, any> = new Map(Object.entries(newData.subBos));

					// add new subbos
					for (let key of mpDataSubBos.keys()) {
						if (!mpEoDataSubBos.has(key)) {
							mpEoDataSubBos.set(key, mpDataSubBos.get(key));
						}
					}

					// remove all subbos not visible with current usage
					for (let eoKey of mpEoDataSubBos.keys()) {
						if (!mpDataSubBos.has(eoKey)) {
							mpEoDataSubBos.delete(eoKey);
						}
					}

					let eoData = eo.getData();
					eoData!.subBos = Object.fromEntries(mpEoDataSubBos);

					eo.reloaded();
				}
			});

		return this.cache.getCache('http.GET').get(url, this.http.get<any>(url))
			.pipe(
				take(1),
				catchError((error) => {
					this.$log.error(error);
					return of(null);
				}),
				tap(data => {
					if (data) {
						if (data.links.defaultPath) {
							let eoData = eo.getData();
							if (eoData) {
								eoData.links.defaultPath = data.links.defaultPath;
							}
						}
						let layoutURL = data.links && data.links.layout && data.links.layout.href;
						if (layoutURL) {
							eo.setLayoutURL(layoutURL);
						}
					}
				})
			);
	}

	validateAttributeValue(value: any, meta: EntityAttrMeta) {
		return this.validationService.validate(value, meta);
	}

	getValidationStatus(attributeMeta: EntityAttrMeta, value: any): ValidationStatus {
		return this.validationService.getValidationStatus(attributeMeta, value);
	}

	getLayoutURLDynamically(eo: EntityObject): Observable<string | undefined> {
		return this.layoutService.getWebLayoutURLDynamically(eo);
	}

	loadEmptyList(metaId: string): Observable<BoViewModel> {
		return this.http
			.get<BoViewModel>(
				this.nuclosConfig.getRestHost() +
				'/bos/' +
				metaId +
				'/query?offset=0&chunkSize=0&countTotal=false&gettotal=false'
			)
			.pipe(
				catchError(e => {
					this.$log.warn('Could not load empty list for entity %o: %o', metaId, e);
					return EMPTY;
				})
			);
	}

	invalidateEoResultsCache() {
		this.eoResultService.invalidateCache();
	}

	emitSavedEO(eo: EntityObject) {
		this.eoEventService.emitSavedEo(eo);
	}

	setFocusOnInitialFocusElementForEntityObject(force?: boolean): boolean {
		const elem = this.getInitialFocusElement();

		if (elem === undefined ||
			(this.eoResultService.getMultiSelectionResult().hasSelection()
				&& !this.eoResultService.showCollectiveProcessing) && force !== true) {
			return false;
		}

		if (elem.length > 0) {
			setTimeout(() => {
				elem.first().trigger('focus');
			});
		}
		return true;
	}

	getInitialFocusElement(): JQuery | undefined {
		let layoutContainer = $('ngb-modal-window .layout-container');
		if (layoutContainer.length < 1) {
			layoutContainer = $('.layout-container');
		}
		if (layoutContainer.length < 1) {
			return undefined;
		}
		let elem = layoutContainer
			.find('.form-control[name][initial-focus],div.lov-container[name][initial-focus],div.multi-selection-container[name][initial-focus],button[initial-focus]')
			.first();
		if (elem.length < 1) {
			elem = layoutContainer
				.find('.form-control[name],div.lov-container[name],div.multi-selection-container[name],button[name]')
				.first();
		}
		return elem;
	}

	getAllLayoutFocusElements(): JQuery | undefined {
		let layoutContainer = $('ngb-modal-window .layout-container');
		if (layoutContainer.length < 1) {
			layoutContainer = $('.layout-container');
		}
		if (layoutContainer.length < 1) {
			return undefined;
		}
		return layoutContainer.find('.form-control[name],div.lov-container[name]');
	}

	/**
	 * Changes the state of the given EO, without any confirmation dialogs etc.
	 * If the EO is dirty, it is saved. Else the state is changed directly.
	 */
	private _changeState(eo: EntityObject, state: State): Observable<any> {
		// NUCLOS-4661: Backup data in case the state change fails
		let wasDirty = eo.isDirty();
		let oldState = eo.getAttribute('nuclosState');

		eo.setAttribute('nuclosState', {id: state.nuclosStateId});
		eo.setDirty(wasDirty);

		// DO NOT use the stateService here, as it cannot deliver commands such as "CLOSE" from the API!
		return eo.save().pipe(
			catchError(error => {
				eo.setAttribute('nuclosState', oldState);
				eo.setDirty(wasDirty);
				return observableThrowError(error);
			})
		);
	}

	private handleSaveRequest(eo: EntityObject, postData: any, saveRequest: Observable<any>) {
		saveRequest = ObservableUtils.onSubscribe(saveRequest, () => eo.saving(true)).pipe(
			map((newData: EntityObjectData) => {
				this.$log.debug('Saving complete: %o', eo);
				this.invalidateEoResultsCache();
				eo.saved(newData);
				return eo;
			})
		);

		return this.handleInputRequired(eo, postData, saveRequest).pipe(
			tap(savedEO => this.commandService.executeCommands((savedEO as EntityObject).getCommands()))
		);
	}

	private handleInputRequired(eo: EntityObject, body: any, httpRequest: Observable<any>) {
		return httpRequest.pipe(
			retryWhen(
				// Retry until all InputRequired exceptions are handled
				errors => {
					eo.saving(false);
					return errors.pipe(
						mergeMap(error => {
							eo.saving(false);
							// TODO API!!!
							// @ts-ignore
							return this.inputRequiredService.handleError(error, body, eo);
						})
					);
				}
			),
			catchError(error => {
				eo.saving(false);
				return this.eoErrorService.handleError(error, eo);
			}),
			finalize(() => {
				eo.saving(false);
				// TODO API!!!
				// @ts-ignore
				this.eoEventService.emitSavedEo(eo);
			})
		);
	}
}
