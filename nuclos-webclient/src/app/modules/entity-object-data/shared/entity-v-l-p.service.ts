import { Injectable } from '@angular/core';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { VlpContext } from '@modules/entity-object-data/shared/vlp-context';
import { Logger } from '@modules/log/shared/logger';
import { FqnService } from '@shared/service/fqn.service';

@Injectable({
	providedIn: 'root'
})
export class EntityVLPService {

	private vlpMap: Map<string, VlpContext> = new Map<string, VlpContext>();
	private vlpSubformSearchContext: VlpContext = new VlpContext(this.fqnService);

	constructor(
		private fqnService: FqnService,
		private $log: Logger
	) {
	}

	cloneContext(sourceEo: EntityObject | undefined, targetEo: EntityObject) {
		if (!sourceEo) {
			return;
		}
		const vlpContext: VlpContext | undefined = this.vlpMap.get(this.getIdStringFromEO(sourceEo));
		if (vlpContext) {
			const targetContext = vlpContext.clone();
			const targetId = this.getIdStringFromEO(targetEo);
			this.vlpMap.set(targetId, targetContext);
		}
	}

	purgeVLPs() {
		this.vlpMap.clear();
	}

	/**
	 * Frees memory by clearing all currently stored VlpContexts used by the web-subform filter mechanism.
	 */
	purgeSearchVLPs() {
		this.vlpSubformSearchContext.clear();
	}

	addEntityObjectMap(eo: EntityObject): void {
		this.$log.debug('Add VLPContext in Map: %o', this.vlpMap);
		if (!this.vlpMap.has(this.getIdStringFromEO(eo))) {
			this.vlpMap = this.vlpMap.set(this.getIdStringFromEO(eo), new VlpContext(this.fqnService));
		}
	}

	getVlpContext(eo: EntityObject): VlpContext | undefined {
		const idString = this.getIdStringFromEO(eo);
		this.$log.debug('Get VLPContext Map: %o for id: %o', this.vlpMap, idString);
		return this.vlpMap.get(idString);
	}

	getVlpSearchContext(): VlpContext {
		return this.vlpSubformSearchContext;
	}

	updateVlpContext(eo: EntityObject, ctx: VlpContext | undefined): void {
		let idString = this.getIdStringFromEO(eo);
		if (ctx) {
			this.$log.debug('Updating VLPContext Map: %o before with ctx: %o for id: %o', this.vlpMap, ctx, idString);
			this.vlpMap = this.vlpMap.set(idString, ctx);
		}
	}

	updateVlpSearchContext(ctx: VlpContext | undefined): void {
		if (ctx) {
			this.vlpSubformSearchContext = ctx;
		}
	}

	getVlpParametersForEO(eo: EntityObject, attributeId: string) {
		const idString = this.getIdStringFromEO(eo.getRootEo());
		this.$log.debug('Find VLPContext Parameter in Map: %o for eo[%o]: %o', this.vlpMap, idString, eo);
		let result = this.getEOVlpParameters(eo, attributeId);
		let entityClassId = eo.getEntityClassId();

		let vlpParameters = this.vlpMap.get(idString)?.getVlpParameters(entityClassId);

		if (vlpParameters) {
			let parentParametersForAttribute = vlpParameters.getParametersForAttribute(attributeId);
			if (result) {
				if (parentParametersForAttribute) {
					result = result.merge(parentParametersForAttribute);
				}
			} else {
				result = parentParametersForAttribute;
			}
		}

		return result;
	}

	getVlpSearchParameters(entityClassId: string, attributeId: string) {
		let result = this.getEOVlpSearchParameters(entityClassId, attributeId);
		let vlpParameters = this.vlpSubformSearchContext.getVlpParameters(entityClassId);

		if (vlpParameters) {
			let parentParametersForAttribute = vlpParameters.getParametersForAttribute(attributeId);
			if (result && parentParametersForAttribute) {
				result = result.merge(parentParametersForAttribute);
			} else {
				result = parentParametersForAttribute;
			}
		}

		return result;
	}

	private getEOVlpSearchParameters(entityClassId: string, attributeId: string) {
		let vlpParameters = this.vlpSubformSearchContext.getVlpParameters(entityClassId);
		if (vlpParameters) {
			return vlpParameters.getParametersForAttribute(attributeId);
		}
		return undefined;
	}

	private getEOVlpParameters(eo: EntityObject, attributeId: string) {
		let entityClassId = eo.getEntityClassId();
		let vlpParameters = this.vlpMap.get(this.getIdStringFromEO(eo))?.getVlpParameters(entityClassId);

		let result;

		if (vlpParameters) {
			result = vlpParameters.getParametersForAttribute(attributeId);
		}

		return result;
	}

	private getIdStringFromEO(eo: EntityObject): string {
		return eo.getEntityClassId() + '_' + (eo.isNew() ? eo.shadowID : eo.getId());
	}
}
