import { TestBed } from '@angular/core/testing';

import { ReferenceTargetService } from './reference-target.service';

describe('ReferenceTargetService', () => {
	let service: ReferenceTargetService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(ReferenceTargetService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});
