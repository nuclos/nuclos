import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EntityAttrMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { LovDataService } from '@modules/entity-object-data/shared/lov-data.service';
import { Logger } from '@modules/log/shared/logger';
import { BrowserRefreshService, SelectReferenceInOtherWindowEvent } from '@shared/service/browser-refresh.service';
import { FqnService } from '@shared/service/fqn.service';
import { iif, Observable, of } from 'rxjs';
import { catchError, map, switchMap, take, tap } from 'rxjs/operators';
import { LayoutService } from '../../../layout/shared/layout.service';

@Injectable({
	providedIn: 'root'
})
export class ReferenceTargetService {

	constructor(
		private browserRefreshService: BrowserRefreshService,
		private fqnService: FqnService,
		private lovDataService: LovDataService,
		private layoutService: LayoutService,
		private $log: Logger
	) {
	}

	targetReference(
		eo: EntityObject | undefined,
		attributeMeta: EntityAttrMeta | undefined,
		boId: string | number | undefined,
		withSaving: boolean,
		vlpId: string | undefined,
		vlpParams: HttpParams | undefined,
		setReferenceUnsafe: boolean,
		popupParameter: string | undefined,
		closeAfterSave = withSaving
	) {
		let refAttrFqn = attributeMeta && attributeMeta.getReferencedEntityClassId();
		if (!refAttrFqn) {
			return of(void 0);
		}

		return this.browserRefreshService
			.openEoInNewTab(
				refAttrFqn,
				boId,
				undefined,
				undefined,
				closeAfterSave,
				!withSaving,
				withSaving,
				popupParameter,
				vlpId,
				vlpParams
			)
			.pipe(
				take(1),
				tap((message: SelectReferenceInOtherWindowEvent | undefined) => {
					if (eo && attributeMeta !== undefined) {
						let fieldName = this.fqnService.getShortAttributeName(
							eo.getEntityClassId(),
							attributeMeta.getAttributeID()
						);
						if (fieldName !== undefined) {
							let value = {
								id: message?.eoId,
								name: message?.name
							};
							if (setReferenceUnsafe) {
								eo.setAttributeUnsafe(fieldName, value);
							} else {
								eo.setAttribute(fieldName, value);
							}
							if (!message?.updateOnly) {
								eo.setDirty(true);
							}
						}
					}
				})
			);
	}

	canAddReference(
		eo: EntityObject | undefined,
		attributeMeta: EntityAttrMeta | undefined
	): Observable<boolean> {
		if (!attributeMeta || !attributeMeta.isReference() || !eo) {
			return of(false);
		}

		return this.checkForLayoutWhenEnabled(
			attributeMeta,
			this.lovDataService.canAddReference(eo, attributeMeta)
		);
	}

	canEditReference(
		eo: EntityObject | undefined,
		attributeMeta: EntityAttrMeta | undefined,
		attribute: any | undefined
	): Observable<boolean> {
		let bVisible =
			attribute !== undefined &&
			attribute !== null &&
			attribute.id !== undefined &&
			attribute.id !== null;
		if (!bVisible) {
			return of(false);
		}

		return this.checkForLayoutWhenEnabled(
			attributeMeta,
			this.canOpenReference(eo, attributeMeta)
		);
	}

	canOpenReference(
		eo: EntityObject | undefined,
		attributeMeta: EntityAttrMeta | undefined
	): Observable<boolean> {
		if (attributeMeta && attributeMeta.isReference()) {
			return this.checkForLayoutWhenEnabled(
				attributeMeta,
				this.lovDataService.canOpenReference(eo, attributeMeta)
			);
		}

		return of(false);
	}

	private checkForLayoutWhenEnabled(attributeMeta: EntityAttrMeta | undefined, source: Observable<boolean>): Observable<boolean> {
		return source.pipe(
			switchMap((result: boolean) => iif(() => result, this.metaIdHasLayout(attributeMeta?.getReferencedEntityClassId()), of(result)))
		);
	}

	private metaIdHasLayout(metaId?: string): Observable<boolean> {
		return metaId === undefined ? of(false) : this.layoutService.getLayoutsForEntity(metaId).pipe(
			take(1),
			catchError(() => of([])),
			map((result: Array<any>) => result.length > 0)
		);
	}
}
