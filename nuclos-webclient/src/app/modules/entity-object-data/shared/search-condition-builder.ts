import { FqnService } from '../../../shared/service/fqn.service';
import { AttributeSelectionContent, Preference } from '../../preferences/preferences.model';
import { EntityMeta } from './bo-view.model';

export class SearchConditionBuilder {

	static getSearchCondition(fqnService: FqnService, metaData: EntityMeta, columnPreference?: Preference<AttributeSelectionContent>) {
		if (!metaData || !metaData.getAttributes()) {
			return undefined;
		}

		let hasFieldFilter = false;
		let fieldFilter = 'CompositeCondition:AND:[';

		for (let attrKey of metaData.getAttributes().keys()) {
			let attribute = <any>metaData.getAttribute(attrKey);

			if (!attribute || attribute.ticked) {
				continue;
			}

			let fieldName = fqnService.getShortAttributeName(
				metaData.getBoMetaId(),
				attribute.boAttrId
			);
			if (attribute.search) {
				if (hasFieldFilter) {
					fieldFilter += ',';
				}

				fieldFilter += 'LikeCondition:LIKE:';
				fieldFilter += fieldName;
				fieldFilter += ':*' + attribute.search + '*';

				hasFieldFilter = true;

			}

			let columnWithFilter = columnPreference?.content.columns.find(column => column.boAttrId === attribute.boAttrId && !!column.filterOp);
			if (columnWithFilter) {
				if (hasFieldFilter) {
					fieldFilter += ',';
				}

				let condition = (columnWithFilter.filterOp === 'LIKE' || columnWithFilter.filterOp === 'NOT_LIKE') ? 'LikeCondition'
					: (columnWithFilter.filterOp === 'BETWEEN' ? 'ComparisonDateValues' : 'Comparison');

				fieldFilter += condition + ':' + columnWithFilter.filterOp + ':';
				fieldFilter += fieldName;
				fieldFilter += condition === 'LikeCondition'
					? (':*' + columnWithFilter.filterValueString + '*')
					: (':' + (columnWithFilter.filterValueString
						|| columnWithFilter.filterValueBoolean
						|| columnWithFilter.filterValueDouble
						|| columnWithFilter.filterValueDate));

				hasFieldFilter = true;
			}

			if (attribute.valuelist && attribute.valuelist.length > 0) {
				let hasInCondition = false;
				let inCondition = '';

				for (let key of Object.keys(attribute.valuelist)) {
					let value = attribute.valuelist[key];
					if (value.ticked && value.name) {
						if (hasInCondition) {
							inCondition += ',';
						}

						// NUCLOS-4111 CollectableInCondition works with the String Name of a Reference Field
						// TODO: It would be better if it worked with the IDs
						inCondition += '\'' + value.name + '\'';
						hasInCondition = true;
					}
				}

				if (hasInCondition) {
					if (hasFieldFilter) {
						fieldFilter += ',';
					}

					fieldFilter += 'InCondition:IN:';
					fieldFilter += fieldName;
					fieldFilter += ':[';
					fieldFilter += inCondition;
					fieldFilter += ']';

					hasFieldFilter = true;
				}
			}
		}

		if (hasFieldFilter) {
			return fieldFilter + ']';
		}

		return undefined;
	}

}
