export class VlpParameter {
	private changedAt: Date;

	constructor(
		private name: string,
		private value: string
	) {
		this.changedAt = new Date();
	}

	getName() {
		return this.name;
	}

	getValue() {
		return this.value;
	}

	getChangedAt() {
		return new Date(this.changedAt);
	}
}
