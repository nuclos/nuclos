import { FqnService } from '../../../shared/service/fqn.service';
import { VlpParameter } from './vlp-parameter';
import { VlpParametersForAttribute } from './vlp-parameters-for-attribute';

export class VlpParameters {
	private parametersByAttribute = new Map<string, VlpParametersForAttribute>();

	constructor(
		private fqnService: FqnService,
	) {
	}

	/**
	 * Returns true, if the parameter value was changed.
	 */
	setParameter(
		attributeId: string,
		parameterName: string,
		parameterValue: any
	): boolean {
		let attributeName = this.fqnService.getShortAttributeName('', attributeId);

		if (attributeName) {
			let parameters = this.parametersByAttribute.get(attributeName);
			if (!parameters) {
				parameters = new VlpParametersForAttribute();
				this.parametersByAttribute.set(attributeName, parameters);
			}

			return parameters.set(new VlpParameter(parameterName, parameterValue));
		}
		return false;
	}

	getParametersForAttribute(
		attributeId: string
	) {
		let attributeName = this.fqnService.getShortAttributeName('', attributeId);
		if (attributeName) {
			return this.parametersByAttribute.get(attributeName);
		}
		return;
	}
}
