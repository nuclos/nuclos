import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReferenceRendererComponent } from './reference-renderer.component';

describe('ReferenceRendererComponent', () => {
	let component: ReferenceRendererComponent;
	let fixture: ComponentFixture<ReferenceRendererComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ReferenceRendererComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ReferenceRendererComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
