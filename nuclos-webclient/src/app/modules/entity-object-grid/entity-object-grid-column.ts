import { ColDef } from 'ag-grid-community';
import { EntityAttrMeta } from '../entity-object-data/shared/bo-view.model';

export interface EntityObjectGridColumn extends ColDef {
	attributeMeta?: EntityAttrMeta;
}
