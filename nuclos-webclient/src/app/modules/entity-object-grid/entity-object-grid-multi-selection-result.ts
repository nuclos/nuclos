export class EntityObjectGridMultiSelectionResult {

	headerMultiSelectionAll = false;
	selectedIds = new Set();
	unselectedIds = new Set();

	clear(): void {
		this.selectedIds.clear();
		this.unselectedIds.clear();
	}

	hasIds(): boolean {
		return this.selectedIds.size > 0 || this.unselectedIds.size > 0;
	}

	countIds(): number {
		return this.selectedIds.size + this.unselectedIds.size;
	}

	hasSelection(): boolean {
		return this.hasIds() || this.headerMultiSelectionAll;
	}

	toRestMultiSelectionResult(): MultiSelectionResult {
		let result: MultiSelectionResult = {
			allSelected: this.headerMultiSelectionAll,
			selectedIds: this.transformSetToObjectIdList(this.selectedIds),
			unselectedIds: this.transformSetToObjectIdList(this.unselectedIds)
		};

		return result;
	}

	readFromRestMultiSelectionResult(result: MultiSelectionResult) {
		this.headerMultiSelectionAll = result.allSelected;
		if (result.selectedIds !== undefined) {
			result.selectedIds.forEach(v => {
				if (v.long !== undefined) {
					this.selectedIds.add(v.long);
				} else if (v.string !== undefined) {
					this.selectedIds.add(v.string);
				}
			});
		}

		if (result.unselectedIds !== undefined) {
			result.unselectedIds.forEach(v => {
				if (v.long !== undefined) {
					this.unselectedIds.add(v.long);
				} else if (v.string !== undefined) {
					this.unselectedIds.add(v.string);
				}
			});
		}
	}

	private transformSetToObjectIdList(idSet: Set<any>): ObjectId[] {
		let result: ObjectId[] = [];
		idSet.forEach(id => {
			let oId: ObjectId = {
				long: typeof id === 'number' ? id : undefined,
				string: typeof id === 'string' ?  id : undefined
			};
			result.push(oId);
		});
		return result;
	}

}
