import {
	Component,
	EventEmitter,
	HostListener,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	SimpleChanges
} from '@angular/core';
import { ReferenceRendererComponent } from '@modules/entity-object-grid/cell-renderer';
import { CellClassParams, ColDef, GridOptions } from 'ag-grid-community';
import { EntityObjectResultService } from 'app/modules/entity-object-data/shared/entity-object-result.service';
import { ObjectUtils } from 'app/shared/object-utils';
import { NuclosConfigService } from 'app/shared/service/nuclos-config.service';
import { SystemParameter } from 'app/shared/system-parameters';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { NuclosCellEditorParams, NuclosCellRendererParams } from '../../layout/web-subform/web-subform.model';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { SortModel } from '../entity-object-data/shared/sort.model';
import { SidebarViewItem } from '../entity-object/sidebar/view/sidebar-view.model';
import {
	BooleanRendererComponent,
	DateRendererComponent,
	HyperlinkRendererComponent,
	NumberRendererComponent,
	StateIconRendererComponent
} from '../grid/grid/cell-renderer';
import { ImageRendererComponent } from '../grid/grid/cell-renderer/image-renderer/image-renderer.component';
import { Logger } from '../log/shared/logger';
import { EntityObjectGridColumn } from './entity-object-grid-column';
import { EntityObjectGridMultiSelectionResult } from './entity-object-grid-multi-selection-result';
import { EntityObjectGridService } from './entity-object-grid.service';
import { MultiSelectionEditorComponent } from './multi-selection-editor/multi-selection-editor.component';
import { MultiSelectionHeaderComponent } from './multi-selection-header/multi-selection-header.component';
import { MultiSelectionRendererComponent } from './multi-selection-renderer/multi-selection-renderer.component';

/**
 * A Grid component for EntityObject data that handles column preferences and NuclosRowColor.
 *
 * TODO: Init columns
 * TODO: Init sorting
 * TODO: Handle NuclosRowColor
 */
@Component({
	selector: 'nuc-entity-object-grid',
	templateUrl: './entity-object-grid.component.html',
	styleUrls: ['./entity-object-grid.component.css']
})
export class EntityObjectGridComponent implements OnInit, OnDestroy, OnChanges {
	@Input() columns: EntityObjectGridColumn[];
	@Input() sortModel: SortModel;

	// TODO: Should not be used externally
	@Input() gridOptions: GridOptions = <GridOptions>{};
	@Input() allowMultiRowSelection = false;

	@Output() onMultiSelectionChange = new EventEmitter();
	@Output() rowSelected = new EventEmitter();
	@Output() gridReady = new EventEmitter();
	@Output() rowClicked = new EventEmitter();
	@Output() modelUpdated = new EventEmitter();

	public rowClassRules: any;
	private _showMultiSelection = false;
	private _multiSelectionDisabled = false;
	private _headerMultiSelectionChangeObserver = new Subject<boolean>();
	private _rowMultiSelectionChangeObserver = new Subject();
	private _multiSelectionResult = new EntityObjectGridMultiSelectionResult();

	private unsubscribe$ = new Subject<boolean>();
	private sortCalculatedAttributes: boolean;
	private selectionColumnShown = false;

	constructor(
		private eoGridService: EntityObjectGridService,
		private $log: Logger,
		private eoResultService: EntityObjectResultService,
		private nuclosConfig: NuclosConfigService
	) {

		this.nuclosConfig.getSystemParameters().subscribe(params => {
			this.sortCalculatedAttributes = params.is(SystemParameter.SORT_CALCULATED_ATTRIBUTES);
		});
	}

	get multiSelectionDisabled(): boolean {
		return this._multiSelectionDisabled;
	}

	@Input()
	set multiSelectionDisabled(multiSelectionDisabled: boolean) {
		this._multiSelectionDisabled = multiSelectionDisabled;
		if (this.gridOptions && this.gridOptions.context) {
			this.gridOptions.context.multiSelectionDisabled = multiSelectionDisabled;
		}
	}

	get showMultiSelection(): boolean {
		return this._showMultiSelection;
	}

	@Input()
	set showMultiSelection(showMultiselection: boolean) {
		if (this._showMultiSelection !== showMultiselection) {
			if (this._showMultiSelection) {
				this.columns.splice(0, 1);
			}
			this._showMultiSelection = showMultiselection;
			this.applyColumns();
			// only reset if we set showMultiSelection to false
			if (!this._showMultiSelection) {
				this.eoResultService.deleteMultiSelectionResultFromStroage();
				this.resetMultiSelection();
			}
		}
	}

	ngOnInit() {
		this.initGrid();
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.$log.debug('changes (entity-object-grid component) = %o', changes);

		if (changes['sortModel']) {
			if (!ObjectUtils.isEqual(changes['sortModel'].currentValue, changes['sortModel'].previousValue)) {
				this.applySortModel();
			}
		}

		if (changes['columns']) {
			if (!ObjectUtils.isEqual(changes['columns'].currentValue, changes['columns'].previousValue)) {
				this.applyColumns();
			}
		}
	}

	gridRowSelectionChanged(data) {
		const rows = data.api.getSelectedNodes();
		this.$log.debug('row selection changed, selected rows: %o', rows);
		if (rows.length > 1) {
			rows.forEach((node) => {
				node.setSelected(false);
				this.updateNodeSelection(node);
			});
		}
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	handleModelUpdated(value?: any) {
		this.modelUpdated.emit();
	}

	@HostListener('document:keydown.Space', ['$event'])
	handleSpaceSelect(event) {
		if (this.gridOptions.api && this.selectionColumnShown
			&& !this.eoResultService.showCollectiveEditing &&
			this.isRelevantEvent(event)
		) {
			this.gridOptions.api.getSelectedNodes().forEach(node => {
				this.updateNodeSelection(node);
			});
		}
	}

	private isRelevantEvent(event) {
		return event.target !== null && (
			$(event.target).parents('nuc-entity-object-grid').length > 0
			|| $(event.target).is('body')
		);
	}

	private updateNodeSelection(node) {
		let data = node.data as SidebarViewItem;
		if (data.selected === undefined) {
			data.selected = false;
		}
		if (!data.selected) {
			this.$log.debug('selecting row: %o', data);
			data.selected = true;
			data.unselected = false;
		} else {
			this.$log.debug('deselecting row: %o', data);
			data.selected = false;
			data.unselected = true;
		}
		node.setData(data);
		this.rowMultiSelectionChanged(data);
	}

	private applySortModel() {
		if (!this.sortModel) {
			return;
		}

		if (this.gridOptions.columnApi) {
			let gridColumnState = this.gridOptions.columnApi.getColumnState()
				.filter((s) => s.sort !== null && s.sort !== undefined);
			let changedSortModel = this.sortModel.getColumns();

			if (!ObjectUtils.isEqual(gridColumnState, changedSortModel)) {
				let newState: any = [...changedSortModel];
				if (changedSortModel.length === 0) {
					// for a sorting reset we need to update the sort attr on every current column
					newState = gridColumnState.map(v => {
						v.sort = null;
						v.sortIndex = null;
						return v;
					});
				}
				this.gridOptions.columnApi.applyColumnState({
					state: newState
				});
				this.gridOptions.api?.refreshHeader();
				this.$log.debug('SortModel changed: %o', this.sortModel);
			}

		} else {
			this.gridReady.pipe(take(1)).subscribe(() => this.applySortModel());
		}
	}

	private applyColumns() {
		if (!this.gridOptions.api || !this.columns) {
			return;
		}

		this.columns.forEach(column => {
			let attributeMeta = column.attributeMeta;
			if (attributeMeta) {
				column.headerClass = this.eoGridService.getTextAlignmentClass(attributeMeta);
				column.cellClass = this.eoGridService.getTextAlignmentClass(attributeMeta);
				column.resizable = true;

				// suppress sorting for images (NULCOS-8092)
				column.sortable = !(attributeMeta.isImage()
					|| attributeMeta.isStateIcon()
					|| (attributeMeta.isCalculated()
						&& !this.sortCalculatedAttributes));

				// provide data for render component
				column.cellRendererParams = {
					nuclosCellRenderParams: {
						editable: false,
						attrMeta: attributeMeta
					} as NuclosCellRendererParams
				};

				column.cellStyle = (params: CellClassParams) => {
					let attrColor = !!params.data?.entityObject ? (params.data.entityObject as EntityObject).getAttributeColor(
						(params.colDef as EntityObjectGridColumn).attributeMeta!.getAttributeName()
					) : undefined;

					return {
						'border-right': '1px dotted #888888',
						'background-color': !!attrColor ? attrColor : ''
					};
				};

				// provide data for editor component
				column.cellEditorParams = {
					attrMeta: attributeMeta
				} as NuclosCellEditorParams;

				if (attributeMeta.isBoolean()) {
					column.cellRendererFramework = BooleanRendererComponent;
				} else if (attributeMeta.isDate() || attributeMeta.isTimestamp()) {
					column.cellRendererFramework = DateRendererComponent;
				} else if (attributeMeta.isNumber()) {
					column.cellRendererFramework = NumberRendererComponent;
				} else if (attributeMeta.isStateIcon()) {
					column.cellRendererFramework = StateIconRendererComponent;
				} else if (attributeMeta.isReference()) {
					column.cellRendererFramework = ReferenceRendererComponent;
				} else if (attributeMeta.isImage()) {
					column.cellRendererFramework = ImageRendererComponent;
				} else if (attributeMeta.isHyperlink()) {
					column.cellRendererFramework = HyperlinkRendererComponent;
				}
			}
		});

		let gridColumns: ColDef[] = this.columns;
		if (this.showMultiSelection) {
			this.selectionColumnShown = true;
			gridColumns.unshift(this.createMultiSelectionColumn());
		}
		this.gridOptions.api.setColumnDefs(gridColumns);

		// we dont want to change the sort model if we enable multiselection
		if (!this.showMultiSelection) {
			this.selectionColumnShown = false;
			this.applySortModel();
		}
	}

	private createMultiSelectionColumn(): ColDef {
		let result = {
			width: 34,
			pinned: true,
			lockPosition: true,
			lockPinned: true,
			filter: false,
			suppressMovable: true,
			suppressNavigable: true,
			suppressResize: true,
			sortable: false,
			field: 'selected',
			headerName: '',
			headerComponentFramework: MultiSelectionHeaderComponent,
			editable: true,
			cellRendererFramework: MultiSelectionRendererComponent,
			cellEditorFramework: MultiSelectionEditorComponent
		} as ColDef;
		return result;
	}

	private initGrid() {
		this._headerMultiSelectionChangeObserver.pipe(takeUntil(this.unsubscribe$)).subscribe(value => {
			if (!this.checkForDirty()) {
				this.setHeaderMultiSelectionAll(value);
			} else {
				this.resetMultiSelection();
			}
		});
		this._rowMultiSelectionChangeObserver.pipe(takeUntil(this.unsubscribe$)).subscribe((rowNode: any) => {
			if (!this.checkForDirty()) {
				this.rowMultiSelectionChanged(rowNode.data);
			} else {
				this.resetMultiSelection();
			}
		});
		this.gridOptions.context = {
			headerMultiSelectionChangeObserver: this._headerMultiSelectionChangeObserver,
			rowMultiSelectionChangeObserver: this._rowMultiSelectionChangeObserver,
			multiSelectionDisabled: this.multiSelectionDisabled
		};

		this.gridOptions.defaultColDef = {
			sortable: true,
			resizable: true
		};
		this.gridOptions.cacheBlockSize = 100;

		this.gridOptions.columnDefs = [{}];

		this.gridOptions.getRowNodeId = item => {
			return item.getId();
		};

		this.gridOptions.rowSelection = this.allowMultiRowSelection ? 'multiple' : 'single';

		// NuclosRowColor
		this.rowClassRules = {
			'ag-row-multiselected': (params: { data: SidebarViewItem }) => {
				return this._multiSelectionResult.headerMultiSelectionAll ||
					params.data !== undefined && this._multiSelectionResult.selectedIds.has(params.data.getId());
			}
		};
		this.gridOptions.getRowClass = (params: { data: SidebarViewItem }) => {
			if (params.data) {
				return params.data.rowclass || '';
			}
			return '';
		};
		this.gridOptions.getRowStyle = (params: { data: SidebarViewItem }) => {
			if (params.data) {
				let rowColor = params.data.getRowColor();
				let textColor = params.data.getTextColor();
				let result = {
					'background-color': '',
					'color': ''
				};
				if (rowColor !== undefined) {
					result['background-color'] = rowColor;
				}
				if (textColor !== undefined) {
					result['color'] = textColor;
				}
				return result;
			}
			return {};
		};

		this.eoResultService.observeMultiSelectionChange().pipe(takeUntil(this.unsubscribe$)).subscribe(multiSelectionResult => {
			// update selection
			if (!this.checkForDirty()) {
				this.updateMultiSelection(multiSelectionResult);
			} else {
				this.resetMultiSelection();
			}
		});

		this.gridOptions.onGridReady = () => {
			this.applyColumns();
		};
	}

	private setHeaderMultiSelectionAll(headerMultiSelectionAll: boolean) {
		this._multiSelectionResult.headerMultiSelectionAll = headerMultiSelectionAll;
		this.resetMultiSelectionIds();
		this.gridOptions.api!.refreshCells();
		this.onMultiSelectionChange.emit(this._multiSelectionResult);
	}

	private getDirtyEOFromNodes(): EntityObject | undefined {
		let dirtyEO = undefined;
		this.gridOptions.api!.forEachNode(function (rowNode) {
			if (rowNode.data) {
				let rowNodeEO = rowNode.data.getEO();
				if (rowNodeEO.isDirty()) {
					dirtyEO = rowNodeEO;
				}
			}
		});
		return dirtyEO;
	}

	private checkForDirty(): boolean {
		let selectedEo = this.getDirtyEOFromNodes();
		if (selectedEo !== undefined && selectedEo.isDirty()) {
			selectedEo.triggerWarnChange();
			return true;
		}
		return false;
	}

	private rowMultiSelectionChanged(rowNode: any) {
		let eoId = rowNode.getId();
		if (eoId) {
			if (this._multiSelectionResult.headerMultiSelectionAll) {
				if (rowNode.unselected !== true) {
					this._multiSelectionResult.unselectedIds.delete(eoId);
				} else if (rowNode.unselected === true) {
					this._multiSelectionResult.unselectedIds.add(eoId);
				}
			} else {
				if (rowNode.selected !== true) {
					this._multiSelectionResult.selectedIds.delete(eoId);
				} else if (rowNode.selected === true) {
					this._multiSelectionResult.selectedIds.add(eoId);
				}
			}
			this.onMultiSelectionChange.emit(this._multiSelectionResult);
		}
	}

	private updateMultiSelection(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this._multiSelectionResult = multiSelectionResult;
		this.gridOptions.context.headerMultiSelectionAll = multiSelectionResult.headerMultiSelectionAll;
		if (this.gridOptions.api) {
			let foundNodes = new Array<number | undefined>();
			this.gridOptions.api.forEachNode(function (rowNode) {
				if (rowNode.data) {
					let data = rowNode.data as SidebarViewItem;
					let eoId = data.getId();
					data.selected = multiSelectionResult.selectedIds.has(eoId) ||
						multiSelectionResult.headerMultiSelectionAll;
					data.unselected = multiSelectionResult.unselectedIds.has(eoId);
					rowNode.setData(data);

					if (data.selected || data.unselected) {
						foundNodes.push(eoId);
					}
				}
			});

			this.$log.debug('foundNodes: %o', foundNodes);
			if (foundNodes.length === 0 && !multiSelectionResult.headerMultiSelectionAll) {
				this.eoResultService.deleteMultiSelectionResultFromStroage();
				this.resetMultiSelection();
				return;
			}

			this.gridOptions.api.refreshCells();

			if (!multiSelectionResult.hasSelection()) {
				this.gridOptions.api.deselectAll();
			}
		}
	}

	private resetMultiSelectionIds() {
		if (this._multiSelectionResult.hasIds()) {
			this._multiSelectionResult.clear();
		}
		if (this.gridOptions.api) {
			this.gridOptions.api.forEachNode(function (rowNode) {
				if (rowNode.data) {
					let data = rowNode.data as SidebarViewItem;
					data.selected = undefined;
					data.unselected = undefined;
					rowNode.setData(data);
				}
			});
			this.gridOptions.api.refreshCells();
		}
	}

	private resetMultiSelection() {
		let storageMultiSelection = this.eoResultService.retrieveMultiSelectionResultFromStorage();
		if (storageMultiSelection.hasSelection()) {
			this.updateMultiSelection(storageMultiSelection);
			return;
		}
		let selectionChanged = this._multiSelectionResult.hasSelection();
		this._multiSelectionResult.headerMultiSelectionAll = false;
		this.gridOptions.context.headerMultiSelectionAll = this._multiSelectionResult.headerMultiSelectionAll;
		this.resetMultiSelectionIds();
		if (selectionChanged) {
			// ExpressionChangedAfterItHasBeenCheckedErrors will occur without timeout
			setTimeout(() => this.onMultiSelectionChange.emit(this._multiSelectionResult));
		}
	}
}
