import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { EntityObjectGridComponent } from './entity-object-grid.component';

@NgModule({
	declarations: [
		EntityObjectGridComponent
	],
	imports: [
		CommonModule,
		SharedModule
	],
	exports: [
		EntityObjectGridComponent
	]
})
export class EntityObjectGridModule {
}
