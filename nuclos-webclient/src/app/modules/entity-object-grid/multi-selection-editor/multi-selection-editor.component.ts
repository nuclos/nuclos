import { Component, ElementRef } from '@angular/core';
import { AgEditorComponent } from 'ag-grid-angular';
import { IAfterGuiAttachedParams, ICellEditorParams } from 'ag-grid-community';
import { Subject } from 'rxjs';

@Component({
	selector: 'nuc-multi-selection-editor',
	templateUrl: './multi-selection-editor.component.html',
	styleUrls: ['./multi-selection-editor.component.css']
})
export class MultiSelectionEditorComponent implements AgEditorComponent {

	protected params: ICellEditorParams;

	private rowMultiSelectionChangeObserver: Subject<any>;

	private context: any;

	constructor(
		protected ref: ElementRef
	) {
	}

	agInit(params: ICellEditorParams): void {
		this.params = params;
		this.context = this.params.context;
		this.rowMultiSelectionChangeObserver = this.params.context.rowMultiSelectionChangeObserver;
	}

	toggleValue() {
		this.setValue(!this.getValue());
	}

	isPopup(): boolean {
		return false;
	}

	afterGuiAttached(params?: IAfterGuiAttachedParams): void {
		this.toggleValue();
	}

	focusIn(): void {
	}

	focusOut(): void {
	}

	getValue(): any {
		if (this.params.node && this.params.node.data) {
			if (this.params.context.headerMultiSelectionAll === true) {
				return !(this.params.node.data.unselected === true);
			} else {
				return this.params.node.data.selected === true;
			}
		}
		return false;
	}

	setValue(value) {
		if (this.context.multiSelectionDisabled === true) {
			return;
		}
		if (this.params.node && this.params.node.data) {
		if (this.params.node.data.id == null) {
			return;
		}
		if (this.params.node) {
			if (value !== this.getValue()) {
				if (this.params.context.headerMultiSelectionAll === true) {
					this.params.node.data.selected = undefined;
					this.params.node.data.unselected = value === false;
				} else {
					this.params.node.data.selected = value === true;
					this.params.node.data.unselected = undefined;
				}
				this.params.node.setData(this.params.node.data);
			}
			this.rowMultiSelectionChangeObserver.next(this.params.node);
		}
		}
	}

	isCancelAfterEnd(): boolean {
		return false;
	}

	isCancelBeforeStart(): boolean {
		return false;
	}
}
