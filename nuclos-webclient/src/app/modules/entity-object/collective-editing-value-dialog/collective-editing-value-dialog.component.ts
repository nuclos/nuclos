import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { EntityAttrMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';

@Component({
	selector: 'nuc-collective-editing-value-dialog',
	templateUrl: './collective-editing-value-dialog.component.html',
	styleUrls: ['./collective-editing-value-dialog.component.css']
})
export class CollectiveEditingValueDialogComponent implements OnInit {

	@Input() attribute: EntityAttrMeta;

	values = new Subject<any>();

	constructor(
		private activeModal: NgbActiveModal,
		private eoResultUpdateService: EntityObjectResultUpdateService
	) {
	}

	ngOnInit() {
		this.eoResultUpdateService
			.getCollectiveProcessingAttributeInformation(this.attribute.getAttributeID())
			.subscribe((result) => {
				if (result.hasOwnProperty('fieldValues')) {
					this.values.next(result['fieldValues']);
				}
			});
	}

	ok() {
		this.activeModal.close();
	}
}
