import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { DropdownComponent } from '@modules/ui-components/dropdown/dropdown.component';
import { Observable } from 'rxjs';
import { map, take, takeWhile } from 'rxjs/operators';
import { NuclosDialogService } from '../../../core/service/nuclos-dialog.service';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { NuclosDefaults } from '../../../data/schema/nuclos-defaults';
import { NuclosValidationService } from '../../../shared/service/nuclos-validation.service';
import { EntityAttrMeta, LovEntry } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { LovDataService } from '../../entity-object-data/shared/lov-data.service';
import { LovSearchConfig } from '../../entity-object-data/shared/lov-search-config';
import { CollectiveEditingValueDialogComponent } from '../collective-editing-value-dialog/collective-editing-value-dialog.component';

@Component({
	selector: 'nuc-collective-editing',
	templateUrl: './collective-editing.component.html',
	styleUrls: ['./collective-editing.component.scss']
})
export class CollectiveEditingComponent implements OnInit, OnDestroy {
	@Output() editingEvent = new EventEmitter();

	@ViewChild('dropdown') dropdownElement: DropdownComponent;

	autocompleteResults: string[];
	addAttribute: string | undefined;

	private lovValues: Map<string, LovEntry[]> = new Map<string, LovEntry[]>();
	private componentCreated = true;
	private clientRuleExecutions: Array<Object> = new Array<Object>();

	constructor(
		public eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private eoService: EntityObjectService,
		private dialogService: NuclosDialogService,
		private nuclosI18nService: NuclosI18nService,
		private lovDataService: LovDataService,
		private validation: NuclosValidationService
	) {
	}

	ngOnInit() {
		this.eoResultService.observeMultiSelectionChange()
			.pipe(
				takeWhile(() => this.componentCreated)
			)
			.subscribe((multiSelectionResult) => {
				// re assert attributes
				let info = this.eoResultService.getCollectiveEditing();
				this.eoResultUpdateService.getCollectiveProcessingInformation(multiSelectionResult, info.getLinks().info.href!)
					.pipe(take(1))
					.subscribe((resultAttributes: any) => {
						let editableAttributes = new Array<EntityAttrMeta>();

						for (let attributeId in resultAttributes) {
							if (resultAttributes.hasOwnProperty(attributeId)) {
								editableAttributes.push(new EntityAttrMeta(resultAttributes[attributeId]));
							}
						}

						info.updateAvailableAttributes(editableAttributes);
						this.eoResultService.updateCollectiveEditing(info);
					});
			});
	}

	ngOnDestroy(): void {
		this.componentCreated = false;
	}

	startEditing() {
		this.editingEvent.emit(true);
	}

	stopEditing() {
		this.editingEvent.emit(false);
	}

	/**
	 * Used to query @Input attributes for text entered in dropdown
	 * @param event
	 */
	queryAttribute(event) {
		this.autocompleteResults =
			[...this.eoResultService.getCollectiveEditing().getAvailableAttributes()
				.filter(value => value.getName()!.includes(event))
				.map(value => value.getName()!)];
		this.dropdownElement?.updateAutoCompleteMessage(
			this.autocompleteResults.length === 0 ? this.nuclosI18nService.getI18n('common.empty') : '',
			false
		);
	}

	/**
	 * Adds selected attribute to editing fields and remove it from dropdown values
	 */
	addAttributeToTable(addAttribute: any) {
		let info = this.eoResultService.getCollectiveEditing();
		info.addEditAttribute(addAttribute);
		this.eoResultService.updateCollectiveEditing(
			info
		);
		this.addAttribute = '';
		this.dropdownElement.writeValue('');
	}

	removeAttributeFromTable(removeAttr: string) {
		this.dialogService.confirm({
			title: this.nuclosI18nService.getI18n('webclient.collectiveProcessing.editingFieldDeleteDialogTitle'),
			message: this.nuclosI18nService.getI18n('webclient.collectiveProcessing.editingFieldDeleteDialogMessage')
		}).pipe(
			take(1)
		).subscribe((result) => {
			if (result) {
				let info = this.eoResultService.getCollectiveEditing();
				info.removeEditAttribute(removeAttr);
				this.eoResultService.updateCollectiveEditing(info);
			}
		});
	}

	enterValue(attrMeta: EntityAttrMeta, value: any, gui = true) {
		if (gui) {
			// value was set from gui purge clientRuleExecutions
			this.clientRuleExecutions = new Array<Object>();
		}

		let info = this.eoResultService.getCollectiveEditing();
		info.updateAttributeValue(attrMeta.getAttributeID(), this.validation.validate(value, attrMeta));

		if (attrMeta.getClientRules() !== undefined) {
			let executingRules = new Array();
			for (let i = 0; i < attrMeta.getClientRules()!.rules.length; i++) {
				let rule = attrMeta.getClientRules()!.rules[i];
				if (rule.triggerOnValueChange) {
					if (rule.clearValueStatements !== undefined) {
						for (let j = 0; j < rule.clearValueStatements!.length; j++) {
							executingRules.push({...rule.clearValueStatements![j], type: 'clearValueStatements'});
						}
					}
					if (rule.transferLookedupValueStatements !== undefined) {
						for (let j = 0; j < rule.transferLookedupValueStatements!.length; j++) {
							executingRules.push({
								...rule.transferLookedupValueStatements![j],
								type: 'transferLookedupValueStatements'
							});
						}
					}
				}
			}

			executingRules.sort((
				a: EntityFieldClientRuleStatement,
				b: EntityFieldClientRuleStatement
			) => a.order - b.order)
				.forEach((rule) => {
					let ruleType = '';

					if (rule.hasOwnProperty('type')) {
						ruleType = rule['type'];
					}

					// execute rule only if it was not executed yet
					if (this.clientRuleExecutions.find(v => {
						return v['attrId'] === attrMeta.getAttributeID() &&
							v['rule'] === ruleType &&
							v['count'] === 1;
					}) === undefined) {
						if (ruleType === 'transferLookedupValueStatements') {
							// transferRule
							this.eoService.loadEO(attrMeta.getReferencedEntityClassId(), value['id'])
								.pipe(take(1))
								.subscribe((eo: EntityObject) => {
									this.setValueOfSpecificFieldById(
										rule['targetEntityFieldId'],
										eo.getAttribute(rule['sourceEntityFieldId']));
								});
						} else if (ruleType === 'clearValueStatements') {
							// clear Rule
							this.setValueOfSpecificFieldById(rule['targetEntityFieldId'], '');
						}
						this.clientRuleExecutions.push({attrId: attrMeta.getAttributeID(), rule: ruleType, count: 1});
					}
				});
		}
	}

	getInputType(attrMeta: EntityAttrMeta): string | undefined {

		if (attrMeta.getComponentType() === undefined && attrMeta.getType() !== 'Date'
			&& attrMeta.getType() !== 'Document') {
			if (attrMeta.getScale() && attrMeta.getScale()! > 255) {
				return 'text';
			} else {
				return 'input';
			}
		} else if (attrMeta.getType() === 'Date') {
			return 'date';
		} else if (attrMeta.getComponentType() === 'ListOfValues' || attrMeta.getComponentType() === 'Combobox') {
			return 'dropdown';
		} else if (attrMeta.getComponentType() === 'Textarea') {
			return 'text';
		}

		return 'not-supported';
	}

	getLovsForField(attrMeta: EntityAttrMeta): LovEntry[] {
		if (this.lovValues.has(attrMeta.getAttributeID())) {
			return this.lovValues.get(attrMeta.getAttributeID())!;
		}

		return [];
	}

	openValuesInDialog(f: EntityAttrMeta) {
		this.dialogService.custom(CollectiveEditingValueDialogComponent,
			{'attribute': f}, {size: 'lg'})
			.pipe(
				take(1)
			).subscribe();
	}

	loadFilteredEntries(search: any, attrMeta: EntityAttrMeta) {
		this.getLovSearchConfig(search, attrMeta).then((config) => {
			this.lovDataService.loadLovEntries(config)
				.pipe(take(1))
				.subscribe((result: LovEntry[]) => {
					this.lovValues.set(attrMeta.getAttributeID(), result);
				});
		});
	}

	loadEntries(attrMeta: EntityAttrMeta) {
		this.loadFilteredEntries('', attrMeta);
	}

	private setValueOfSpecificFieldById(fieldId: string, value: any) {
		let availableField = this.eoResultService.getCollectiveEditing()
			.getAvailableAttributes().find(v => v.getAttributeID() === fieldId);
		if (availableField === undefined) {
			availableField = this.eoResultService.getCollectiveEditing()
				.getEditingAttributes().find(v => v.getAttributeID() === fieldId);
		} else {
			this.addAttributeToTable(availableField.getName()!);
		}

		if (availableField) {
			if (availableField.getComponentType() !== undefined && value === '') {
				// for references it needs to be set id null so if
				// you execute editing it will reset references correctly
				this.enterValue(availableField, {id: null, name: ''}, false);
			} else {
				this.enterValue(availableField, value, false);
			}
		}
	}

	/**
	 * Calls REST service for attribute information
	 * If fieldIdValues are found and there is only one return its value
	 * @param attrMeta
	 */
	private getFirstIdValueFromAttribute(attrMeta: EntityAttrMeta): Observable<number> {
		return this.eoResultUpdateService
			.getCollectiveProcessingAttributeInformation(attrMeta.getAttributeID())
			.pipe(
				map((result) => {
					if (result.hasOwnProperty('fieldIdValues')) {
						if (result['fieldIdValues'].length === 1) {
							return result['fieldIdValues'][0]['value'];
						}
					}
					return '';
				})
			);
	}

	private async getLovSearchConfig(search: string, attrMeta: EntityAttrMeta): Promise<LovSearchConfig> {
		let config = {
			attributeMeta: attrMeta,
			quickSearchInput: search,
			resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT
		};

		if (attrMeta.getVlpConfig()) {
			let params: Array<{name: any, value: any}> = [
				{name: 'valuelistProvider', value: attrMeta.getVlpConfig()!.vlpId},
				{name: attrMeta.getVlpConfig()!.idField, value: 0},
				{name: 'id-fieldname', value: attrMeta.getVlpConfig()!.idField},
				{name: 'fieldname', value: attrMeta.getVlpConfig()!.valueField}
			];

			// add params from vlpContexts
			if (attrMeta.getVlpConfig()!.vlpContexts !== undefined) {
				for (let i = 0; i < attrMeta.getVlpConfig()!.vlpContexts!.length; i++) {
					if (attrMeta.getVlpConfig()!.vlpContexts![i].vlpParams !== undefined &&
						attrMeta.getVlpConfig()!.vlpContexts![i].input) {
						for (let j = 0; j < attrMeta.getVlpConfig()!.vlpContexts![i].vlpParams!.length; j++) {
							let param = attrMeta.getVlpConfig()!.vlpContexts![i].vlpParams![j];
							if (param.valueFromEntityFieldId) {
								let editingValue = this.eoResultService.getCollectiveEditing().getAttributeValue(param.valueFromEntityFieldId);

								if (editingValue === '') {
									let attrMetaDest = this.eoResultService.getCollectiveEditing()
										.getAvailableAttributes().find(
											value => value.getAttributeID() === param.valueFromEntityFieldId);
									if (attrMetaDest) {
										await this.getFirstIdValueFromAttribute(attrMetaDest)
											.toPromise()
											.then((value) => {
												params.push({name: param.name, value: value});
											});
									}
								} else {
									params.push({name: param.name, value: editingValue});
								}
							} else {
								params.push({name: param.name, value: param.value});
							}
						}
					}
				}
			}

			config['vlp'] = {
				parameter: params,
				type: 'ds',
				value: attrMeta.getVlpConfig()!.vlpId,
				idFieldname: attrMeta.getVlpConfig()!.idField,
				fieldname: attrMeta.getVlpConfig()!.valueField,
			};
		}

		return config;
	}
}
