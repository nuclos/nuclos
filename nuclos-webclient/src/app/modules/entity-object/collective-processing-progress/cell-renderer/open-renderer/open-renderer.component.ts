import { Component } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { AgRendererComponent } from 'ag-grid-angular';
import { TooltipOptions } from 'primeng/tooltip/tooltip';

@Component({
	selector: 'nuc-open-renderer',
	templateUrl: 'open-renderer.component.html',
	styleUrls: ['open-renderer.component.css']
})
export class OpenRendererComponent implements AgRendererComponent {
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor() {
	}

	agInit(params: any) {
	}

	refresh(params: any): boolean {
		return false;
	}
}
