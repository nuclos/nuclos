import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { EntityObjectResultService } from '@modules/entity-object-data/shared/entity-object-result.service';
import { GridOptions } from 'ag-grid-community';
import { BooleanRendererComponent } from 'app/modules/grid/grid/cell-renderer';
import { Observable, of, Subject } from 'rxjs';
import { finalize, map, take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { GenerationResult } from '../../generation/shared/generation';
import { NuclosGenerationService } from '../../generation/shared/nuclos-generation.service';
import { Logger } from '../../log/shared/logger';
import { OpenRendererComponent } from './cell-renderer/open-renderer/open-renderer.component';

@Component({
	selector: 'nuc-collective-processing-progress',
	templateUrl: 'collective-processing-progress.component.html',
	styleUrls: ['collective-processing-progress.component.scss']
})
export class CollectiveProcessingProgressComponent implements OnInit, OnDestroy {

	static baseUrl() {
		return window.location.origin + '/#/';
	}

	gridOptions: GridOptions = <GridOptions>{};

	@Output() progressNumber = new EventEmitter<number>();
	@Output() progressStatus = new EventEmitter();

	rowData = new Array<CollectiveProcessingObjectInfo>();
	overlayLoading = '<i class="fa fa-hourglass-half gridLoadingIcon" aria-hidden="true"></i>';

	private ngDestroy$ = new Subject();
	private genDialogsKeys: Array<string> = [];

	constructor(
		protected i18n: NuclosI18nService,
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService,
		private genService: NuclosGenerationService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private eoEventService: EntityObjectEventService,
		private $log: Logger
	) {
		this.gridOptions.columnDefs = [
			{
				headerName: '#',
				field: 'infoRowNumber',
				width: 40,
				resizable: true
			},
			{
				headerName: this.i18n.getI18n('webclient.collectiveProcessingProgress.header.name'),
				field: 'name',
				width: 200,
				resizable: true
			},
			{
				headerName: this.i18n.getI18n('webclient.collectiveProcessingProgress.header.generatedEOName'),
				field: 'boGenerationResult.bo.title',
				width: 200,
				resizable: true
			},
			{
				headerName: this.i18n.getI18n('webclient.collectiveProcessingProgress.header.success'),
				field: 'success',
				width: 80,
				cellRendererFramework: BooleanRendererComponent,
				editable: false,
				resizable: true
			},
			{
				headerName: this.i18n.getI18n('webclient.collectiveProcessingProgress.header.note'),
				field: 'note',
				width: 400,
				resizable: true
			},
			{
				headerName: this.i18n.getI18n('webclient.collectiveProcessingProgress.header.openEO'),
				field: 'open',
				width: 200,
				cellRendererFramework: OpenRendererComponent,
				resizable: true
			},
			{
				headerName: this.i18n.getI18n('webclient.collectiveProcessingProgress.header.openGeneratedEO'),
				field: 'open-generated',
				width: 200,
				cellRendererFramework: OpenRendererComponent,
				resizable: true
			},
		];
		this.gridOptions.onCellClicked = event => {
			if (this.gridOptions.api) {
				switch (event.colDef.field) {
					case 'open':
						this.openInNewTab(event.data);
						break;
					case 'open-generated':
						this.openGeneratedInNewTab(event.data);
						break;
				}
			}
		};
	}

	ngOnInit() {
		this.genDialogsKeys = [];
		setTimeout(() => this.gridOptions.api?.showLoadingOverlay());
		this.eoResultUpdateService.subscribeCollectiveProcessingProgress().pipe(
			takeUntil(this.ngDestroy$)
		).subscribe(async info => {
			this.progressNumber.emit(info.percent);
			if (info.objectInfos) {
				for (const oInfo of info.objectInfos) {
					if (oInfo.boGenerationResult !== undefined) {
						const eoData = await this.openGenerationResultDialog(oInfo).toPromise();
						this.$log.debug('object from result: ', eoData);
					}
					this.rowData[oInfo.infoRowNumber - 1] = oInfo;
					if (this.gridOptions && this.gridOptions.api) {
						this.gridOptions.api.setRowData(this.rowData);
					}

					let sourceEo = this.getSourceEo(oInfo.id.long);
					if (oInfo.success &&  sourceEo !== undefined) {
						if (this.eoResultUpdateService.getCollectiveProcessingAction()?.name === 'delete') {
							this.eoEventService.emitDeletedEo(
								sourceEo
							);
						} else {
							this.eoService.reloadEo(
								sourceEo
							).pipe(
								take(1)
							).subscribe((reloadedEo) => {
								this.eoEventService.emitReloadEo(reloadedEo);
							});
						}
					}
				}
			}
			this.gridOptions.api?.refreshCells();
			if (info.percent >= 100) {
				// finished
				setTimeout(() => {
					this.gridOptions.api?.hideOverlay();
				});
			} else {
				setTimeout(() => {
					this.gridOptions.api?.showLoadingOverlay();
				});
			}


			const totalObjects = this.rowData.length;
			const failureObjects = this.rowData.filter(o => !o.success).length;
			this.progressStatus.emit({total: totalObjects, failure: failureObjects});
		});

		this.eoEventService.observeCreatedEo().pipe(takeUntil(this.ngDestroy$)).subscribe((eo: EntityObject) => {
			if (this.gridOptions.api) {
				let rows =
					this.rowData.filter(
					d =>
						(d.boGenerationResult !== undefined &&
						(eo.getTitle() as string).includes((d.boGenerationResult.bo.title as string)))
				);
				rows.forEach(row => {
					if (row) {
						this.rowData[row.infoRowNumber - 1].boGenerationResult.bo.title = eo.getTitle();
						this.rowData[row.infoRowNumber - 1].boGenerationResult.bo.boId = eo.getId();
					}
				});
				if (this.gridOptions && this.gridOptions.api) {
					this.gridOptions.api.setRowData(this.rowData);
				}
			}
		});
	}

	ngOnDestroy() {
		this.genDialogsKeys.forEach(k => {
			localStorage.removeItem(k);
		});
		this.ngDestroy$.next(true);
		this.ngDestroy$.complete();
	}

	private openGenerationResultDialog(data: CollectiveProcessingObjectInfo): Observable<any> {
		let genResult = data.boGenerationResult as GenerationResult;
		let id = genResult.bo.boId === null ? genResult.bo.temporaryId : genResult.bo.boId;
		let key = 'genDialog_' + id + '_done';
		let localStorageResult = localStorage.getItem(key);
		if (localStorageResult === null) {
			this.genDialogsKeys.push(key);
			localStorage.setItem(key, JSON.stringify(genResult));
			return this.genService.showResult(
				genResult,
				this.getSourceEo(data.id.long)
			).pipe(
				map(() => genResult.bo),
				finalize(() => localStorage.setItem(key, JSON.stringify(genResult)))
			);
		} else {
			genResult.bo = (JSON.parse(localStorageResult) as GenerationResult).bo;
			return of(genResult.bo);
		}
	}

	private getSourceEo(id: number | undefined) {
		return this.eoResultService.getResults().find((parent) => parent.getId() === id);
	}

	private openInNewTab(oInfo) {
		let href =
			CollectiveProcessingProgressComponent.baseUrl() +
			'popup/' +
			oInfo.boMetaId +
			'/' +
			(oInfo.id.long ? oInfo.id.long : oInfo.id.string);

		window.open(href, '_blank');
	}

	private openGeneratedInNewTab(oInfo) {
		if (oInfo.boGenerationResult !== undefined) {
			let genResult = (oInfo.boGenerationResult as GenerationResult);
			if (genResult.bo.boId !== null) {
				let href =
					CollectiveProcessingProgressComponent.baseUrl() +
					'popup/' +
					genResult.bo.boMetaId +
					'/' +
					genResult.bo.boId;

				window.open(href, '_blank');
			}
		}
	}

}
