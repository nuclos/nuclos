import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { ColorUtils } from '@shared/color-utils';
import { BusyService } from 'app/shared/service/busy.service';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { BehaviorSubject, EMPTY, Observable, Subject } from 'rxjs';
import { catchError, take, takeUntil } from 'rxjs/operators';
import { NuclosDialogService } from '../../../core/service/nuclos-dialog.service';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { CollectiveEditingContent } from '../../../data/schema/collective-editing-content';
import { EntityAttrMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { Logger } from '../../log/shared/logger';
import { EntityObjectGridMultiSelectionResult } from '../../entity-object-grid/entity-object-grid-multi-selection-result';

@Component({
	selector: 'nuc-collective-processing',
	templateUrl: 'collective-processing.component.html',
	styleUrls: ['collective-processing.component.scss']
})
export class CollectiveProcessingComponent implements OnInit, OnDestroy {
	@Output() onMultiSelectionChange = new EventEmitter();
	@Output() statusLine = new EventEmitter();

	multiSelectionResult: EntityObjectGridMultiSelectionResult;

	actions$: BehaviorSubject<CollectiveProcessingAction[]> =
		new BehaviorSubject<CollectiveProcessingAction[]>([]);
	stateChanges$: BehaviorSubject<CollectiveProcessingAction[]> =
		new BehaviorSubject<CollectiveProcessingAction[]>([]);
	functions$: BehaviorSubject<CollectiveProcessingAction[]> =
		new BehaviorSubject<CollectiveProcessingAction[]>([]);
	isProgressing = false;
	noProgressingOptions = false;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	private unsubscribe$ = new Subject<boolean>();

	private abortUrl: string | undefined;


	constructor(
		protected i18n: NuclosI18nService,
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private $log: Logger,
		private dialogService: NuclosDialogService,
		private busyService: BusyService
	) { }

	ngOnDestroy() {
		if (this.isProgressing) {
			this.abortProgress();
		}
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.setMultiSelectionResult(this.eoResultService.getMultiSelectionResult());
		this.eoResultService.observeMultiSelectionChange().pipe(takeUntil(this.unsubscribe$)).subscribe(
			multiSelectionResult => {
			this.setMultiSelectionResult(multiSelectionResult);
		});
		this.statusLine.emit('');
	}

	executeAction(action: CollectiveProcessingAction) {
		if (action.description && action.withoutConfirmation !== undefined && !action.withoutConfirmation) {
			this.dialogService.confirm({
				title: this.i18n.getI18n('webclient.dialog.statechange'),
				message: action.description
			}).pipe(
				take(1)
			).subscribe((result) => {
				if (result) {
					this.triggerAction(action);
				}
			});
		} else if (action.name === 'delete') {
			this.dialogService.confirm({
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n('webclient.dialog.delete.multiple')
			}).pipe(
				take(1)
			).subscribe((result) => {
				if (result) {
					this.triggerAction(action);
				}
			});
		} else if (action.name === 'edit') {
			// custom logic for editing
			this.$log.debug(action);
			this.eoResultUpdateService.getCollectiveProcessingInformation(this.multiSelectionResult, action.links.info.href!)
				.pipe(take(1))
				.subscribe((resultAttributes: any) => {
					let editableAttributes = new Array<EntityAttrMeta>();

					for (let attributeId in resultAttributes) {
						if (resultAttributes.hasOwnProperty(attributeId)) {
							editableAttributes.push(new EntityAttrMeta(resultAttributes[attributeId]));
						}
					}
					let info = new CollectiveEditingContent();
					info.updateAvailableAttributes(editableAttributes);
					info.setLinks(action.links);
					this.eoResultService.updateCollectiveEditing(info);
					this.eoResultService.showCollectiveEditing = true;
				});
		} else {
			this.triggerAction(action);
		}
	}

	triggerEditing(value: boolean) {
		if (!value) {
			this.eoResultService.showCollectiveEditing = false;
		} else {
			let info = this.eoResultService.getCollectiveEditing();

			this.eoResultUpdateService
				.executeCollectiveEditProcessing(
					this.multiSelectionResult,
					info.getResult(),
					info.getLinks().execute.href!)
				.pipe(take(1))
				.subscribe((execution: CollectiveProcessingExecution) => {
					this.eoResultService.showCollectiveEditing = false;
					this.abortUrl = execution.links.abort.href!;
					this.isProgressing = true;
				});
		}
	}

	showProgress() {
		return this.eoResultService.forceCollectiveProcessingView;
	}

	showEdit() {
		return this.eoResultService.showCollectiveEditing;
	}

	closeProgress() {
		this.eoResultService.forceCollectiveProcessingView = false;
		// while closing progress refresh selected eo
		const selectedEo = this.eoResultService.getSelectedEo();
		if (selectedEo) {
			selectedEo.reload().pipe(
				take(1),
				catchError(e => {
					// probably deleted!
					return EMPTY;
				}))
				.subscribe();
		} else {
			this.eoResultService.showCollectiveProcessing = false;
		}

		// reloading selection data
		this.loadSelectionOptions(this.multiSelectionResult);
	}

	backToDetails() {
		this.eoResultService.showCollectiveProcessing = false;
	}

	cancelCollectiveProcessing() {
		this.eoResultService.showCollectiveEditing = false;
		this.multiSelectionResult.clear();
		this.multiSelectionResult.headerMultiSelectionAll = false;
		this.onMultiSelectionChange.emit(this.multiSelectionResult);
	}

	abortProgress() {
		this.eoResultUpdateService
			.abortCollectiveProcessing(this.abortUrl!)
			.pipe(
				take(1),
				catchError(e => {
					if (e.status === 404) {
						this.dialogService.alert({
							title: this.i18n.getI18n('webclient.error.404.title'),
							message: this.i18n.getI18n('webclient.error.code404')
						}).pipe(
							take(1)
						).subscribe();
					} else {
						this.dialogService.alert({
							title: this.i18n.getI18n('webclient.error.unknown.title'),
							message: this.i18n.getI18n('webclient.error.unknown.text')
						}).pipe(
							take(1)
						).subscribe();
					}

					return EMPTY;
				})
			)
			.subscribe(_ => {
				this.dialogService.alert({
					title: this.i18n.getI18n('webclient.collectiveProcessing.confirm.title'),
					message: this.i18n.getI18n('webclient.collectiveProcessing.confirm.message')
				}).pipe(
					take(1)
				).subscribe();
				this.progress(100);
			});
	}

	progress(newProgress: number) {
		if (newProgress >= 100) {
			this.isProgressing = false;
		}
	}

	updateStatusLine(infoObject) {
		if (infoObject.total > 1) {
			if (infoObject.failure > 1) {
				this.statusLine.emit(this.i18n.getI18n('webclient.collectiveProcessing.status.line.manyAll',
					infoObject.total, infoObject.failure));
			} else if (infoObject.failure === 1) {
				this.statusLine.emit(
					this.i18n.getI18n('webclient.collectiveProcessing.status.line.manyDataOneFailure',
						infoObject.total)
				);
			} else {
				this.statusLine.emit(
					this.i18n.getI18n('webclient.collectiveProcessing.status.line.manyDataNoFailure',
						infoObject.total)
				);
			}
		} else {
			if (infoObject.failure === 1) {
				this.statusLine.emit(
					this.i18n.getI18n('webclient.collectiveProcessing.status.line.oneDataOneFailure')
				);
			} else {
				this.statusLine.emit(
					this.i18n.getI18n('webclient.collectiveProcessing.status.line.oneDataNoFailure')
				);
			}
		}
	}

	calculateTextColorFromBackgroundColor(color: string, isSubText: boolean) {
		let calculated = ColorUtils.calculateTextColorFromBackgroundColor(color);
		if (calculated === undefined) {
			if (isSubText) {
				return 'gray';
			}
			return '#3d93d9';
		}
		return calculated;
	}

	cleanStringForId(input: string) {
		return input.replace(/[^a-zA-Z0-9]+/g, '');
	}

	private triggerAction(action: CollectiveProcessingAction) {
		this.eoResultUpdateService
			.executeCollectiveProcessing(this.multiSelectionResult, action)
			.pipe(take(1))
			.subscribe((execution: CollectiveProcessingExecution) => {
				this.abortUrl = execution.links.abort.href!;
				this.isProgressing = true;
			});
	}

	private setMultiSelectionResult(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this.multiSelectionResult = multiSelectionResult;
		if (multiSelectionResult.hasSelection()) {
			this.loadSelectionOptions(multiSelectionResult);
		}
	}

	private loadSelectionOptions(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		this.busyService.busy(
			this.eoResultUpdateService.loadCollectiveProcessingSelectionOptions(multiSelectionResult)
		)
		.pipe(take(1))
		.subscribe(selOptions => {
			this.actions$.next(selOptions && selOptions.actions ? selOptions.actions : []);
			this.stateChanges$.next(selOptions && selOptions.stateChanges ? selOptions.stateChanges : []);

			// as we do show these items in one block we need to
			// combine them in one array
			const combined: CollectiveProcessingAction[] = [];
			if (selOptions && selOptions.generators) {
				combined.push(...selOptions.generators);
			}
			if (selOptions && selOptions.customRules) {
				combined.push(...selOptions.customRules);
			}

			this.functions$.next(combined);
			this.noProgressingOptions = false;
			if (Object.keys(selOptions).length === 0 && multiSelectionResult.hasSelection()) {
				this.$log.warn('selOptions received: %o (MultiSelection: %o)', selOptions, multiSelectionResult);
				this.noProgressingOptions = true;
			}
		});
	}
}
