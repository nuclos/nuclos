import {
	AfterContentInit,
	AfterViewInit,
	Component,
	Input,
	OnChanges,
	OnInit,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { EntityObjectRouteService } from '@modules/entity-object/entity-object-route.service';
import { RuleService } from '@modules/rule/shared/rule.service';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { IEntityObject, IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { ObservableUtils } from '@shared/observable-utils';
import { BusyService } from '@shared/service/busy.service';
import { LocalStorageService } from '@shared/service/local-storage.service';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { EMPTY, Observable } from 'rxjs';
import { map, switchMap, take, takeUntil } from 'rxjs/operators';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../../data/classes/subscription-handle.directive';
import {
	BrowserRefreshService,
	SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM,
	SelectReferenceInOtherWindowEvent,
	SHOW_CLOSE_ON_SAVE_BUTTON_PARAM,
	SHOW_SELECT_BUTTON_PARAM
} from '@shared/service/browser-refresh.service';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { AuthenticationService } from '../../authentication';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectNavigationService } from '../../entity-object-data/shared/entity-object-navigation.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { MetaService } from '../../entity-object-data/shared/meta.service';
import { Logger } from '../../log/shared/logger';

interface DetailButton {
	id: string;
	labelKey: string;

	buttonCssClass: string;
	iconCssClass: string;

	action(eo: EntityObject | undefined): void;

	isVisible(eo: EntityObject | undefined): boolean;

	isEnabled(eo: EntityObject | undefined): boolean;

	blur?(): void;
}

@Component({
	selector: 'nuc-detail-buttons',
	templateUrl: './detail-buttons.component.html',
	styleUrls: ['./detail-buttons.component.css']
})
export class DetailButtonsComponent extends SubscriptionHandleDirective implements OnInit, OnChanges {
	@Input()
	canCreateBo: boolean;
	@Input() autoSelectAfterSaveEnabled = true;
	@Input() autoSelectAfterDeleteEnabled = true;
	@Input() useResultServiceForCancelation = true;
	@Input() showCancelForNewObject = true;
	@Input() modal = false;

	@Input() entityClassId: string;

	saveInProgress = false;
	showReferenceSelectionButton = false;
	showCloseOnSaveButton = false;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	// TODO: Move mandator stuff to own component or module.
	mandators: Mandator[] | undefined;
	selectedMandator: Mandator | undefined;
	mandatorLabel: string | undefined;
	showMandatorSelection = false;

	@ViewChild('popover') popover;

	buttons: DetailButton[] = [];

	private isNewAllowedForMandator = false;
	private selectEntryInOtherWindowToken: string;
	private _eo: EntityObject | undefined;

	/**
	 * Remember the previous EO when adding a new one.
	 * The previous EO should be selected again, if adding a new one is cancelled.
	 */
	private previousEO: EntityObject | undefined;

	private eoChangeListener: IEntityObjectEventListener = {
		isSaving: (entityObject: EntityObject, inProgress: boolean) => {
			this.saveInProgress = inProgress;
		}
	};

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private authenticationService: AuthenticationService,
		private eoService: EntityObjectService,
		private eoEventService: EntityObjectEventService,
		private eoResultService: EntityObjectResultService,
		private eoNavigationService: EntityObjectNavigationService,
		private eoRouteService: EntityObjectRouteService,
		private entityVlpService: EntityVLPService,
		private ruleService: RuleService,
		private metaService: MetaService,
		private browserRefreshService: BrowserRefreshService,
		private dialogService: NuclosDialogService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService,
		private busyService: BusyService,
		private localStorageService: LocalStorageService,
		private $log: Logger
	) {
		super();
	}

	@Input()
	set triggerUnsavedChangesPopover(date: Date) {
		if (this.popover !== undefined) {
			this.$log.debug('Show popover on %o', this.popover);
			if (date && (!this.dialogService.hasOpenDialog() || this.modal) && !this.isPopoverVisible()) {
				ObservableUtils.runOrRetry(() => $(this.popover.nativeElement)['popover']('show'), 3);
				$('#saveEO').focus();
			}
		}
	}

	get eo(): EntityObject | undefined {
		return this._eo;
	}

	@Input()
	set eo(eo: EntityObject | undefined) {
		this._eo = eo;
		if (eo) {
			this.entityClassId = eo.getEntityClassId();
		}
	}

	ngOnInit() {
		this.entityClassId = this.eoResultService.getSelectedEntityClassId()!;

		// show 'select entry in other window' button if requested
		this.route.queryParams.subscribe((params: Params) => {
			let showReferenceSelectionButton = params[SHOW_SELECT_BUTTON_PARAM];
			if (showReferenceSelectionButton) {
				this.showReferenceSelectionButton = true;
			}
			let selectEntryInOtherWindowToken = params[SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM];
			if (selectEntryInOtherWindowToken) {
				this.selectEntryInOtherWindowToken = selectEntryInOtherWindowToken;
			}
			let showCloseOnSaveButton = params[SHOW_CLOSE_ON_SAVE_BUTTON_PARAM];
			if (showCloseOnSaveButton && !this.modal) {
				this.showCloseOnSaveButton = showCloseOnSaveButton;
			}
		});

		let checkNewAllowedFunction = () => {
			this.checkNewAllowedForMandator().pipe(take(1)).subscribe(isNewAllowed => {
				this.isNewAllowedForMandator = isNewAllowed;
			});
		};

		checkNewAllowedFunction();

		this.route.url.subscribe(() => {
			checkNewAllowedFunction();
		});

		this.initButtons();
	}

	ngOnChanges(changes: SimpleChanges): void {
		let change = changes['eo'];
		if (change) {
			let previousEo: EntityObject = change.previousValue;
			if (previousEo) {
				previousEo.removeListener(this.eoChangeListener);
			}

			let currentEo = change.currentValue;
			if (currentEo) {
				currentEo.addListener(this.eoChangeListener);
			}
		}
	}

	/**
	 * Prepares for adding a new EO.
	 * A dummy EO instance is instantiated and selected.
	 */
	addNew() {
		this.previousEO = this.eo;

		this.metaService.getEntityMeta(this.entityClassId).pipe(take(1)).subscribe((meta: EntityMeta) => {
			// if more then 1 mandators are available ask user to choose mandator
			let mandatorLevelId = meta.getMandatorLevelId();
			this.mandators = mandatorLevelId ? this.getMandatorsByLevelAndCurrentMandator(mandatorLevelId) : undefined;

			let nuclosMandatorAttributeMeta = meta.getMandatator();
			if (nuclosMandatorAttributeMeta) {
				this.mandatorLabel = nuclosMandatorAttributeMeta.getName();
			}
			if (this.mandators) {
				if (this.mandators.length > 1 && this.selectedMandator === undefined) {
					this.showMandatorSelection = true;

					// TODO: Hack!
					$('nuc-layout').hide();
					$('nuc-graph').hide();

					return;
				} else if (this.mandators.length === 1) {
					this.selectedMandator = this.mandators[0];
				}
			}

			this.eoResultService.navigateToNew(this.eoRouteService.processMetaId);
		});

		$('#detailButtonsContainer')['popover']();
	}

	/**
	 * clones the selected EO
	 */
	clone() {
		if (this.eo) {
			this.eo
				.getLayoutURLDynamically()
				.pipe(
					take(1),
					map(
						(layoutUrl: string) =>
							layoutUrl && layoutUrl.split('/').find(value => value.endsWith('LO'))
					),
					switchMap((layoutid: string) => {
						if (this.eo) {
							return this.eo.cloneOnServerSide(layoutid);
						} else {
							return EMPTY;
						}
					})
				)
				.subscribe((eo: EntityObject) => {
					// this is a hack to ensure the cloned eo is selected and not the previously first in the list
					this.eoResultService.setPristine(true);
					eo.select(false);
					this.entityVlpService.cloneContext(this.eo, eo);
					this.ruleService.updateRuleExecutor(eo)
						.pipe(take(1))
						.subscribe();
					this.eoEventService.emitAddEo(eo);
					this.$log.log(eo);
				});
		}
	}

	// TODO: Why is mandator logic in a button component?!
	chooseMandatorCancel() {
		this.selectedMandator = undefined;
		this.showMandatorSelection = false;
		if (this.eo) {
			this.eo.setDirty(false);
			$('nuc-layout').show();
			$('nuc-graph').show();
		}
	}

	// TODO: Why is mandator logic in a button component?!
	selectMandator(mandator: Mandator) {
		this.selectedMandator = mandator;
		this.showMandatorSelection = false;
		$('nuc-layout').show();
		$('nuc-graph').show();

		this.eoService.selectMandator(mandator);
		this.addNew();
	}

	isShowNewButton() {
		return (
			this.canCreateBo &&
			!(this.eo && (this.eo.isDirty() || this.eo.isNew())) &&
			this.isNewAllowedForMandator
		);
	}

	isShowCloneButton() {
		return (
			this.canCreateBo &&
			!!this.eo &&
			!(this.eo.isDirty() || this.eo.isNew()) &&
			this.isNewAllowedForMandator
		);
	}

	isShowRefreshButton() {
		return !!this.eo && !(this.eo.isDirty() || this.eo.isNew());
	}

	isShowTreeButton() {
		return !!this.eo && !(this.eo.isDirty() || this.eo.isNew()) && !!this.eoResultService.getSelectedMeta()?.isTreeActive();
	}

	/**
	 * Saves the current (modified) EO and selects it again.
	 */
	save() {
		if (this.modal && this.isPopoverVisible()) {
			// NUCLOS-9311: if the popover is visible, the user expects the modal to automatically close, when clicking save
			this.dialogService.closeCurrentModal();
		}

		this.hideUnsavedChangesPopover();

		// Using setTimeout to save the EO only after other actions could complete.
		// E.g. dirty subform editors might call setAttributeObserved, which runs asynchronously.
		// TODO: Make setting of attributes synchronous and get rid of setTimeout here
		setTimeout(() => {
			if (this.eo) {
				this.eo.save().pipe(take(1)).subscribe(
					// Select this EO again after saving - triggers updates in other components
					eo => {
						if (this.autoSelectAfterSaveEnabled) {
							if (this.selectEntryInOtherWindowToken) {
								this.selectEoInOtherWindow(eo, true);
							} else {
								eo.select();
								this.selectEoInOtherWindow(eo);
							}
							this.eoEventService.emitSelectedEo(eo);
						}
						if (this.showCloseOnSaveButton) {
							window.close();
						}
					}
				);
			}
		});
	}

	/**
	 * Cancels the "new" operation.
	 * Selects the previous EO again.
	 */
	cancel(sourceEvent: EventTarget | undefined) {
		this.dialogService
			.confirm({
				title: this.i18n.getI18n('webclient.button.cancel'),
				message: this.i18n.getI18n('webclient.dialog.undo.changes')
			}, sourceEvent)
			.pipe(
				take(1)
			)
			.subscribe(
				(result) => {
					// canceled
					if (!result) {
						return;
					}

					if (!this.eo) {
						// Should never be reached - "cancel" should not be available when there's no EO
						this.$log.error('Could not cancel editing - no EO');
						return;
					}

					if (this.useResultServiceForCancelation) {
						this.eoResultService.resetEo(this.eo);
						if (this.eo.isNew()) {
							this.eo.setDirty(false);
							this.eoResultService.selectEo(undefined);
							this.eoService.delete(this.eo)
								.pipe(
									take(1)
								)
								.subscribe(() => {
									if (this.previousEO) {
										this.eoResultService.selectEo(
											this.previousEO,
											undefined,
											undefined,
											this.eoRouteService.processMetaId);
									}
								});
							if (this.eo.getTemporaryId() !== undefined) {
								this.localStorageService.removeItem(this.eo.getTemporaryId()!);
							}
						}
					} else {
						if (this.modal && this.isPopoverVisible()) {
							// NUCLOS-9311: if the popover is visible, the user expects the modal to automatically close, when clicking save
							this.dialogService.closeCurrentModal();
						}
						this.eo.reset();
					}

					this.selectedMandator = undefined;
					this.hideUnsavedChangesPopover();
				});
	}

	refresh() {
		if (!this.eo) {
			// Should never be reached - "refresh" should not be available when there's no EO
			this.$log.error('Could not refresh - no EO');
			return;
		}

		this.busyService.busy(
			this.eo.reload()
		).pipe(takeUntil(this.unsubscribe$)).subscribe(() =>
			this.eoEventService.emitSelectedEo(this.eo)
		);
	}

	openInTreeView() {
		if (!this.eo) {
			// Should never be reached - "refresh" should not be available when there's no EO
			this.$log.error('Could not refresh - no EO');
			return;
		}

		this.browserRefreshService.openEoInNewTab(this.entityClassId,
			this.eo.getId(),
			this.entityClassId,
			'' + this.eo.getId(),
			false,
			false,
			true,
			'false',
			undefined,
			undefined,
			false,
			true);
	}

	/**
	 * Deletes the current EO and goes back to the previous EO.
	 */
	delete() {
		this.dialogService
			.confirm({
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n('webclient.dialog.delete')
			})
			.pipe(
				take(1)
			)
			.subscribe(
				(result) => {
					// canceled
					if (!result) {
						return;
					}

					// ok, delete
					if (this.eo) {
						let entityClassId = this.eo.getEntityClassId();
						this.eo.delete().pipe(take(1)).subscribe(eo => {
							if (this.autoSelectAfterSaveEnabled) {
								this.eoResultService.selectEo(undefined);
								if (entityClassId) {
									this.eoNavigationService.navigateToView(entityClassId);
								}
							}
						});
						this.hideUnsavedChangesPopover();
					}
				}
			);
	}

	/**
	 * will be called when selecting a referenced attribute in another window
	 */
	selectThisEoInOtherWindow() {
		if (this.eo) {
			this.selectEoInOtherWindow(this.eo);
			window.close();
		}
	}

	/**
	 * determines whether the popover is visible or not by looking at the attribute 'aria-describedby'
	 */
	isPopoverVisible() {
		let ariaDescribedBy = $(this.popover.nativeElement).attr('aria-describedby');
		return (ariaDescribedBy && ariaDescribedBy.startsWith('popover'));
	}

	hideUnsavedChangesPopover() {
		ObservableUtils.runOrRetry(() => $(this.popover.nativeElement)['popover']('hide'), 3);
	}

	hasVisibleButtons() {
		return this.buttons.find(button => button.isVisible(this.eo));
	}

	private initButtons() {
		this.buttons = [
			{
				id: 'newEO',
				labelKey: 'webclient.button.new',
				buttonCssClass: 'btn btn-sm btn-success',
				iconCssClass: 'fa fa-fw fa-plus',
				action: () => this.addNew(),
				isVisible: () => this.isShowNewButton(),
				isEnabled: () => !this.saveInProgress
			},
			{
				id: 'cloneEO',
				labelKey: 'webclient.button.clone',
				buttonCssClass: 'btn btn-sm btn-success',
				iconCssClass: 'fa fa-fw fa-clone',
				action: () => this.clone(),
				isVisible: () => this.isShowCloneButton(),
				isEnabled: () => !this.saveInProgress
			},
			{
				id: 'saveEO',
				labelKey: 'webclient.button.save',
				buttonCssClass: 'btn btn-sm btn-primary',
				iconCssClass: 'fa fa-fw fa-floppy-o',
				action: () => this.save(),
				isVisible: eo => this.isShowSaveButton(),
				isEnabled: () => this.isSaveButtonEnabled(),
			},
			{
				id: 'cancelEO',
				labelKey: 'webclient.button.cancel',
				buttonCssClass: 'btn btn-sm btn-warning',
				iconCssClass: 'fa fa-fw fa-times',
				action: () => this.cancel($('button[id="cancelEO"]').get(0)),
				isVisible: eo => this.isShowCancelButton(),
				isEnabled: () => !this.saveInProgress
			},
			{
				id: 'refreshEO',
				labelKey: 'webclient.button.refresh',
				buttonCssClass: 'btn btn-sm btn-success',
				iconCssClass: 'fa fa-fw fa-sync',
				action: () => this.refresh(),
				isVisible: eo => this.isShowRefreshButton(),
				isEnabled: () => !this.saveInProgress
			},
			{
				id: 'openTreeViewTab',
				labelKey: 'webclient.button.openInTreeView',
				buttonCssClass: 'btn btn-sm btn-success',
				iconCssClass: 'fa fa-fw fa-sitemap',
				action: () => this.openInTreeView(),
				isVisible: eo => this.isShowTreeButton(),
				isEnabled: () => !this.saveInProgress
			},
			{
				id: 'deleteEO',
				labelKey: 'webclient.button.delete',
				buttonCssClass: 'btn btn-sm btn-danger',
				iconCssClass: 'fa fa-fw fa-trash',
				action: () => this.delete(),
				isVisible: eo => this.isShowDeleteButton(),
				isEnabled: eo => !!eo && eo.canDelete() && !this.saveInProgress
			}
		];
		this.registerShortCutForButtons();
	}

	private isShowDeleteButton() {
		return !!this.eo && this.eo.canDelete() && !this.isOpenedInPopup();
	}

	private isShowCancelButton() {
		return !!this.eo &&
			((this.showCancelForNewObject && (this.eo.isNew() || this.eo.isDirty())) ||
				(!this.showCancelForNewObject && !this.eo.isNew() && this.eo.isDirty()));
	}

	private isShowSaveButton() {
		return !!this.eo && this.eo.isDirty();
	}

	private isSaveButtonEnabled(): boolean {
		return !!this.eo && this.eo.canWrite() && !this.saveInProgress;
	}

	private isOpenedInPopup(): boolean {
		return this.router.url.indexOf('/popup/') !== -1;
	}

	// TODO: Why is mandator logic in a button component?!
	private getMandatorsByLevelAndCurrentMandator(mandatorLevelId: string): Mandator[] {
		let authentication = this.authenticationService.getAuthentication();
		if (authentication) {
			let mandators = authentication.mandators;
			let currentMandator = authentication.mandator;
			if (mandators) {
				let result = mandators
					.filter(m => m.mandatorLevelId === mandatorLevelId)
					.filter(m => !currentMandator || m.path.indexOf(currentMandator.path) === 0);
				return result;
			}
		}
		return [];
	}

	/**
	 * check if user can create a new entry dependent on selected mandator
	 * TODO: Why is mandator logic in a button component?!
	 */
	private checkNewAllowedForMandator(): Observable<boolean> {
		return new Observable(observer => {
			let authentication = this.authenticationService.getAuthentication();
			this.metaService.getEntityMeta(this.entityClassId).pipe(take(1)).subscribe((meta: EntityMeta) => {
				let mandatorLevelId = meta.getMandatorLevelId();

				let isNewAllowed =
					// mandators not present
					(authentication !== undefined &&
						authentication.mandators &&
						authentication.mandators.length === 0) ||
					// eo is not mandator dependent
					!mandatorLevelId ||
					// mandators present but no mandator selection available for this bo
					(!!mandatorLevelId &&
						this.getMandatorsByLevelAndCurrentMandator(mandatorLevelId).length !== 0);
				observer.next(isNewAllowed);
				observer.complete();
			});
		});
	}

	private selectEoInOtherWindow(eo: IEntityObject, updateOnly = false) {
		let eoId = eo.getId();
		if (!eoId) {
			this.$log.warn('Could not select EO without ID: %o', eo);
			return;
		}
		// send event to other browser instance
		let selectReferenceEvent = new SelectReferenceInOtherWindowEvent();
		selectReferenceEvent.entityClassId = eo.getEntityClassId();
		selectReferenceEvent.eoId = eoId;
		selectReferenceEvent.updateOnly = updateOnly;
		let name = (eo as unknown as EntityObject).getData()?.title;
		if (name) {
			selectReferenceEvent.name = name;
		}
		selectReferenceEvent.token = Number(this.selectEntryInOtherWindowToken);
		this.browserRefreshService.selectEntryInOtherWindow(selectReferenceEvent);
	}

	private registerShortCutForButtons() {
		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT', keys: 'N', description:
				this.i18n.getI18n('webclient.shortcut.detail.new')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.executeAfterBlurAndCheck(
				this.isShowNewButton(),
				this.addNew.bind(this)
			);
		});
		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT', keys: 'S', description:
				this.i18n.getI18n('webclient.shortcut.detail.save')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.executeAfterBlurAndCheck(
				this.isShowSaveButton(),
				this.save.bind(this)
			);
		});
		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT', keys: 'U', description:
				this.i18n.getI18n('webclient.shortcut.detail.cancel')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.executeAfterBlurAndCheck(
				this.isShowCancelButton(),
				this.cancel.bind(this, $('button[id="cancelEO"]').get(0))
			);
		});
		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT', keys: 'D', description:
				this.i18n.getI18n('webclient.shortcut.detail.clone')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.executeAfterBlurAndCheck(
				this.isShowCloneButton(),
				this.clone.bind(this)
			);
		});
		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT', keys: 'R', description:
				this.i18n.getI18n('webclient.shortcut.detail.refresh')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.executeAfterBlurAndCheck(
				this.isShowRefreshButton(),
				this.refresh.bind(this)
			);
		});
		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT', keys: 'X', description:
				this.i18n.getI18n('webclient.shortcut.detail.delete')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.executeAfterBlurAndCheck(
				this.isShowDeleteButton(),
				this.delete.bind(this)
			);
		});
	}

	private executeAfterBlurAndCheck(visibleCheck: boolean, cb: Function) {
		if (visibleCheck) {
			this.blurFocusedElements();
			setTimeout(() => {
				cb();
			});
		}
	}

	private blurFocusedElements() {
		$(':focus').each(function (index, el) {
			$(el).trigger('blur');
		});
	}
}
