import { Component, ElementRef, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IEntityObject, IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { from as observableFrom, Observable, of as observableOf } from 'rxjs';
import { take } from 'rxjs/operators';
import { StringUtils } from '@shared/string-utils';
import { ClickOutsideService } from '../../click-outside/click-outside.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-detail-dialog',
	templateUrl: './detail-dialog.component.html',
	styleUrls: ['./detail-dialog.component.css']
})
export class DetailDialogComponent {

	@Input() sourceEo: EntityObject | undefined;
	@Input() title: string;
	@Input() width = 400;
	@Input() height = 200;
	errorMessage: string | undefined;
	disableControls = false;

	private _eo: EntityObject;
	private cancled = false;


	private eoDefaultListener: IEntityObjectEventListener = {
		isSaving: (entityObject: EntityObject, inProgress: boolean) => {
			this.disableControls = inProgress;
			if (inProgress) {
				this.errorMessage = undefined;
			}
		},
		error: (entityObject: EntityObject, errorMessage: any) => {
			this.errorMessage = StringUtils.textToHtml(errorMessage);
			this._eo.removeListener(this.eoDuringSaveListener);
		}
	};

	// close only on 'OK', not if saved via custom-rule
	private eoDuringSaveListener: IEntityObjectEventListener = {
		afterSave: (entityObject: EntityObject) => {
			if (entityObject.getData() === undefined || entityObject.getData() === null) {
				this.cancel();
			} else {
				this.reloadSourceObject().pipe(take(1)).subscribe(() => {
						this.cancel();
					}
				);
			}
		}
	};

	constructor(
		protected clickoutside: ClickOutsideService,
		protected activeModal: NgbActiveModal,
		protected ref: ElementRef
	) {}

	@Input()
	set eo(eo: EntityObject) {
		this._eo = eo;
		this._eo.setSuppressDefaultErrorMessage(true);
		this._eo.addListener(this.eoDefaultListener);
		let error = eo.getError();
		if (error) {
			this.errorMessage = error;
		}
	}

	get eo(): EntityObject {
		if (this._eo.getData() === undefined || this._eo.getData() === null) {
			this.cancel();
		}
		return this._eo;
	}

	cancel() {
		if (!this.cancled) {
			this.cancled = true;
			this.activeModal.close(true);
			this.removeListeners();
		}
	}

	ok() {
		this.disableControls = true;
		this._eo.addListener(this.eoDuringSaveListener);
		this._eo.save()
			.pipe(
				take(1)
			)
			.subscribe();
	}

	removeListeners() {
		this._eo.removeListener(this.eoDefaultListener);
		this._eo.removeListener(this.eoDuringSaveListener);
	}

	private reloadSourceObject(): Observable<IEntityObject | undefined> {
		if (this.sourceEo) {
			return observableFrom(this.sourceEo.reload());
		}
		return observableOf(undefined);
	}
}
