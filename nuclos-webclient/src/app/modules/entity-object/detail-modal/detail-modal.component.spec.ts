import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DetailModalComponent } from './detail-modal.component';

xdescribe('DetailModalComponent', () => {
	let component: DetailModalComponent;
	let fixture: ComponentFixture<DetailModalComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [DetailModalComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetailModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
