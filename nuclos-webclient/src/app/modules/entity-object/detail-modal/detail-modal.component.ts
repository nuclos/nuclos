import { Component, ElementRef, Input } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IEntityObject, IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { from as observableFrom, Observable, of as observableOf } from 'rxjs';
import { take } from 'rxjs/operators';
import { ClickOutsideService } from '../../click-outside/click-outside.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-detail-modal',
	templateUrl: './detail-modal.component.html',
	styleUrls: ['./detail-modal.component.css']
})
export class DetailModalComponent {
	@Input() independentContext = false;

	@Input() sourceEo: EntityObject | undefined;

	triggerUnsavedChangesPopover: Date;

	private _eo: EntityObject;

	private eoChangeListener: IEntityObjectEventListener = {
		afterDelete: (entityObject: EntityObject) => {
			this.reloadSourceObject().pipe(take(1)).subscribe(() => this.close());
		},
		afterSave: (entityObject: EntityObject) => {
			this.reloadSourceObject().pipe(take(1)).subscribe();
		}
	};

	constructor(
		protected clickoutside: ClickOutsideService,
		private dialog: NuclosDialogService,
		protected activeModal: NgbActiveModal,
		protected ref: ElementRef
	) {}

	@Input()
	set eo(eo: EntityObject) {
		this._eo = eo;
		this._eo.addListener(this.eoChangeListener);
		if (this.independentContext) {
			let error = eo.getError();
			if (error) {
				this.dialog.alert({
					title: 'Error',
					message: error
				}).pipe(
					take(1)
				).subscribe();
			}
		}
	}

	get eo(): EntityObject {
		return this._eo;
	}

	cancel() {
		this._eo.reset();
		this.close(false);
	}

	close(closeResult = true) {
		if (this.independentContext && this._eo.isDirty()) {
			this.warnAboutUnsavedChanges();
			return;
		}
		// To notify open subform cell editors etc.
		this.clickoutside.outsideClick(this.ref.nativeElement);

		this.activeModal.close(closeResult);
	}

	hasDefaultPath(): boolean {
		return this.eo?.getLinks()?.defaultPath !== undefined;
	}

	private warnAboutUnsavedChanges() {
		this.triggerUnsavedChangesPopover = new Date();
	}

	private reloadSourceObject(): Observable<IEntityObject | undefined> {
		if (this.independentContext && this.sourceEo) {
			return observableFrom(this.sourceEo.reload());
		}
		return observableOf(undefined);
	}
}
