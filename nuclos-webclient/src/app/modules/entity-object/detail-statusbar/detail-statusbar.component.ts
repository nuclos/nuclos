import { Component, Input } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';

@Component({
	selector: 'nuc-detail-statusbar',
	templateUrl: './detail-statusbar.component.html',
	styleUrls: ['./detail-statusbar.component.scss']
})
export class DetailStatusbarComponent {
	@Input() rightSideString;
	@Input() rightSideTitleString;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;
}
