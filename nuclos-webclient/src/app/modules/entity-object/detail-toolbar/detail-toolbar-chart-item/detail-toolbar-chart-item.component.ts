import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UserAction } from '@nuclos/nuclos-addon-api';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { AuthenticationService } from '../../../authentication';
import { ChartService } from '../../../chart/shared/chart.service';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-detail-toolbar-chart-item',
	templateUrl: './detail-toolbar-chart-item.component.html',
	styleUrls: ['./detail-toolbar-chart-item.component.css']
})
export class DetailToolbarChartItemComponent implements OnInit, OnDestroy {

	@Input() modal: boolean;

	visible = false;

	private _eo: EntityObject;

	private chartsAllowed = false;

	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private authService: AuthenticationService,
		private chartService: ChartService
	) {
	}

	@Input() set eo(eo: EntityObject) {
		this._eo = eo;
		this.updateVisibility();
	}

	get eo() {
		return this._eo;
	}

	ngOnInit(): void {
		this.authService.onAuthenticationDataAvailable().pipe(takeUntil(this.unsubscribe$)).subscribe(loginSuccessful => {
			this.chartsAllowed =
				loginSuccessful &&
				this.authService.isActionAllowed(
					UserAction.ConfigureCharts
				);
			this.updateVisibility();
		});
		this.updateVisibility();
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	private updateVisibility() {
		if (this.eo && this.chartService) {
			this.chartService.getCharts(this.eo.getEntityClassId()).pipe(take(1)).subscribe(charts => {
				// Show the chart button, if charts are configured for this user
				if (charts.length > 0) {
					this.visible = true;
					return;
				}

				// Else show the chart button only if this user can configure new charts
				if (      this.eo
					// && this.eo.hasSubEos()
					// SubEos are loaded with a time delay.
					// Could be determined using a DoCheck, as in the AbstractGridComponent, but it is too expensive!
				) {
					this.visible = this.chartsAllowed;
				} else {
					this.visible = false;
				}
			});
		} else {
			this.visible = false;
		}
	}
}
