import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DetailToolbarItemComponent } from './detail-toolbar-item.component';

xdescribe('DetailToolbarItemComponent', () => {
	let component: DetailToolbarItemComponent;
	let fixture: ComponentFixture<DetailToolbarItemComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [DetailToolbarItemComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetailToolbarItemComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
