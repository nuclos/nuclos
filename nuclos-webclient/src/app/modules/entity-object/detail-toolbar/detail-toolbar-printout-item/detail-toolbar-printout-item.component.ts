import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-detail-toolbar-printout-item',
	templateUrl: './detail-toolbar-printout-item.component.html',
	styleUrls: ['./detail-toolbar-printout-item.component.css']
})
export class DetailToolbarPrintoutItemComponent implements OnInit {

	@Input() eo: EntityObject;

	constructor() {
	}

	ngOnInit() {
	}

	isVisible(): boolean {
		if (!this.eo) {
			return false;
		}

		let links = this.eo.getLinks();
		return links !== undefined && links.printouts !== undefined;
	}
}
