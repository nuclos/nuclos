import {
	AfterContentInit,
	Component, ElementRef,
	EventEmitter,
	HostListener,
	Input,
	OnDestroy,
	OnInit,
	Output,
	TemplateRef
} from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { GraphComponent } from '@modules/state-path/graph/graph.component';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { Subscription } from 'rxjs';
import { finalize, take } from 'rxjs/operators';
import { TaskService } from '../../../core/service/task.service';
import { StringUtils } from '../../../shared/string-utils';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { Logger } from '../../log/shared/logger';

@Component({
	selector: 'nuc-detail',
	templateUrl: './detail.component.html',
	styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit, OnDestroy, AfterContentInit {
	@Input() meta: EntityMeta;
	@Input() boId: number;
	@Input() eo: EntityObject | undefined;
	@Input() canCreateBo: boolean;
	@Input() triggerUnsavedChangesPopover: Date;
	@Input() collectiveProcessing: boolean;

	@Output() onBackToCollectiveProcessingClick = new EventEmitter();
	@Output() onDetailLayoutLoaded = new EventEmitter();

	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;
	displayDefaultPathFully = true;
	pathBtnDisabled = false;

	private subscriptions: Subscription = new Subscription();

	private warnChangeSubscription: Subscription | undefined;

	constructor(
		private eoEventService: EntityObjectEventService,
		private eoService: EntityObjectService,
		private $log: Logger,
		private taskService: TaskService,
		private dialogService: NuclosDialogService,
		private elementRef: ElementRef,
		public eoResultService: EntityObjectResultService,
	) {
	}

	ngOnInit() {
		this.subscriptions.add(
			this.eoEventService.observeSelectedEo().subscribe(
				eo => this.setEo(eo as EntityObject)
			)
		);
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	@HostListener('window:resize')
	ngAfterContentInit () {
		// getting width after next GUI Tick to ensure we are getting correct size
		setTimeout(() => {
			this.displayDefaultPathFully = $(this.elementRef.nativeElement).width() >= 560;
		});
	}

	layoutLoaded($event: any): void {
		this.onDetailLayoutLoaded.emit($event);
	}

	hasDefaultPath(): boolean {
		return this.eo?.getLinks()?.defaultPath !== undefined;
	}

	showDefaultPathContentInDialog(templateContent: TemplateRef<GraphComponent>): void {
		this.pathBtnDisabled = true;
		this.dialogService.custom(templateContent).pipe(
			take(1),
			finalize(() => this.pathBtnDisabled = false)
		).subscribe();
	}

	private setEo(eo: EntityObject | undefined) {
		// TODO: There are more kinds of dynamic entities (dynamic BOs, virtual BOs), not only dynamic task lists - handle this more generic
		if (eo && eo.isDynamicTaskList()) {
			this.taskService.getTaskListDefinition(eo.getEntityClassId()).pipe(take(1)).subscribe( taskListMeta => {
				let realEntity = this.getRealEntity(taskListMeta, eo);

				if (!realEntity) {
					this.$log.warn('%o is a dynamic task list and the real entity could not be determined -> Could not load the real entity object!');
					return;
				}

				let realId = eo.getId();
				if (realEntity && realId) {
					this.$log.info(
						'Loading real EO %o:%o for dynamic EO %o:%o...',
						realEntity,
						realId,
						eo.getEntityClassId(),
						eo.getId()
					);

					this.eoService.loadEO(realEntity, realId).pipe(take(1)).subscribe(
						realEo => this.setEo(realEo)
					);
					return;
				}
			});
		}

		// unsubscripe old eo warner
		if (this.warnChangeSubscription) {
			this.subscriptions.remove(this.warnChangeSubscription);
			this.warnChangeSubscription.unsubscribe();
			this.warnChangeSubscription = undefined;
		}

		this.eo = eo;

		if (this.eo) {
			this.warnChangeSubscription = this.eo!.observeWarnChanges().subscribe(
				val => {
					if (val && this.eo && this.eo.isDirty()) {
						this.triggerUnsavedChangesPopover = new Date();
					}
				}
			);
			this.subscriptions.add(this.warnChangeSubscription);
		}
	}

	private getRealEntity(taskListMeta: EntityMeta, eo: EntityObject) {
		let dynamicEntityFieldName = taskListMeta.getDynamicEntityFieldName();
		let realEntity;

		if (dynamicEntityFieldName) {
			dynamicEntityFieldName = StringUtils.replaceAll(dynamicEntityFieldName, '_', '');
			realEntity = eo.getAttribute(dynamicEntityFieldName);
		}

		if (!realEntity) {
			realEntity = taskListMeta.getTaskEntity();
		}

		return realEntity;
	}
}
