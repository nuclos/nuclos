import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { RuleService } from '@modules/rule/shared/rule.service';
import { LocalStorageService } from '@shared/service/local-storage.service';
import { iif, of } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import { EntityObjectNavigationService } from '../entity-object-data/shared/entity-object-navigation.service';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { Logger } from '../log/shared/logger';

@Injectable()
export class EntityObjectRouteService {

	processMetaId: string | undefined;

	constructor(
		private eoNavigationService: EntityObjectNavigationService,
		private eoResultService: EntityObjectResultService,
		private ruleService: RuleService,
		private $log: Logger,
		private locaStorageService: LocalStorageService
	) {
	}

	handleParams(params: Params) {
		this.locaStorageService.removeItemsStartingWith(EntityObjectService.SUBEO_SELECTION_LOCALSTORAGE_KEY_PREFIX);
		let eoId = params['entityObjectId'];

		let entityClassId = params['entityClassId'];
		this.eoResultService.selectEntityClassId(entityClassId);

		this.processMetaId = params['processMetaId'];

		// Load the selected EO or reset
		if (eoId) {
			if (eoId === 'new') {
				this.createNewEO(this.processMetaId);
			} else {
				this.eoResultService.selectEoByClassAndId(entityClassId, eoId).pipe(
					catchError(e => {
						// this will probably be fired if we do not found our eo
						this.$log.error('EntityObject could not be found', entityClassId, eoId);
						this.$log.error(e);
						this.eoResultService.updateEONotFound(true);
						return of();
					}),
					switchMap((eo: EntityObject) =>
						iif(() => this.isPopupView(params['url']),
							this.ruleService.updateRuleExecutor(eo),
							of(void 0)
						)
					)
				).subscribe();
			}
		}
	}

	private isPopupView(url: string): boolean {
		return url.split('/').shift() === 'popup';
	}

	private createNewEO(processMetaId: string | undefined) {
		this.eoResultService.createNew(processMetaId).subscribe(
			eo => {
				eo.select();
				this.eoNavigationService.navigateToEo(eo)
					.pipe(
						take(1)
					).subscribe();
			}
		);
	}
}
