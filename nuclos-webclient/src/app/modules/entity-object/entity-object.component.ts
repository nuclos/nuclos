import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router, UrlSegment } from '@angular/router';
import { SideviewmenuService } from '@modules/entity-object/shared/sideviewmenu.service';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { IEntityObject, UserAction } from '@nuclos/nuclos-addon-api';
import {
	BrowserRefreshService,
	EXPLORER_TREE_ID,
	EXPLORER_TREE_META_ID,
	SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM,
	SelectReferenceInOtherWindowEvent
} from '@shared/service/browser-refresh.service';
import { NuclosTitleService } from '@shared/service/nuclos-title.service';
import { NuclosI18nService } from 'app/core/service/nuclos-i18n.service';
import { DatetimeService } from 'app/shared/service/datetime.service';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map, switchMap, take } from 'rxjs/operators';
import { AuthenticationService } from '../authentication';
import { EntityMeta } from '../entity-object-data/shared/bo-view.model';
import { EntityObjectErrorService } from '../entity-object-data/shared/entity-object-error.service';
import { EntityObjectEventService } from '../entity-object-data/shared/entity-object-event.service';
import { EntityObjectResultUpdateService } from '../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { EntityObjectSearchfilterService } from '../entity-object-data/shared/entity-object-searchfilter.service';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../entity-object-data/shared/entity-object.service';
import { MetaService } from '../entity-object-data/shared/meta.service';
import { EntityObjectGridMultiSelectionResult } from '../entity-object-grid/entity-object-grid-multi-selection-result';
import { Logger } from '../log/shared/logger';
import { PerspectiveService } from '../perspective/shared/perspective.service';
import { EntityObjectRouteService } from './entity-object-route.service';
import { EntityObjectPreferenceService } from './shared/entity-object-preference.service';

export class LoadMoreResultsEvent {
	constructor(
		public offset: number,
		public limit: number,
		public count: boolean,
		public successCallback: Function,
		public failCallback: Function
	) {
	}
}

export type SidebarMode = 'sidebar' | 'tree';

@Component({
	selector: 'nuc-entity-object',
	templateUrl: './entity-object.component.html',
	styleUrls: ['./entity-object.component.css']
})
export class EntityObjectComponent implements OnInit, OnDestroy {
	collectiveProcessing = false;
	collectiveProcessingAllowed = false;
	triggerUnsavedChangesPopover: Date;

	changedByString = '';
	createdByString = '';
	collectiveProcessingStatusLine = '';

	collectiveProcessingExecuting = false;
	contentIsHorizontal = false;

	sidebarShowExplorerTree: boolean;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	entityClassId: string | undefined;

	layoutLoaded$: Subject<any> = new Subject<any>();

	protected urlQuery: string | undefined;
	protected isPopupOpened = false;

	private compId = Math.random();

	private subscriptions: Subscription = new Subscription();
	private previousEntityObject: EntityObject | undefined;
	private shouldSetInitialFocus = false;

	constructor(
		public perspectiveService: PerspectiveService,
		private authenticationService: AuthenticationService,
		protected route: ActivatedRoute,
		protected router: Router,
		private metaService: MetaService,
		private eoEventService: EntityObjectEventService,
		protected eoService: EntityObjectService,
		private eoRouteService: EntityObjectRouteService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		protected $log: Logger,
		protected browserRefreshService: BrowserRefreshService,
		private eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private dateTimeService: DatetimeService,
		private nuclosI18n: NuclosI18nService,
		private sideViewMenuService: SideviewmenuService,
		// TODO: Only injected here in order to be instantiated by angular
		private eoErrorService: EntityObjectErrorService,
		private titleService: NuclosTitleService
	) {
	}

	ngOnInit() {
		this.subscriptions.add(
			this.eoEventService.observeSelectedEo().pipe(
				map((eo) => (eo as unknown as EntityObject))
			).subscribe((eo: EntityObject) => {
				if (eo !== this.previousEntityObject) {
					this.detailSelectionChanged(eo);
					// TODO: Find a better place for this
					this.perspectiveService.selectEo(eo);
					this.shouldSetInitialFocus = (this.previousEntityObject !== undefined && eo !== undefined) ||
						this.isPopupOpened;
					this.previousEntityObject = eo;
				}
			})
		);

		this.subscriptions.add(
			this.layoutLoaded$.subscribe(() => {
				if (this.shouldSetInitialFocus) {
					setTimeout(() => {
						this.eoService.setFocusOnInitialFocusElementForEntityObject();
					});
				}
			})
		);

		// gets automatically unsubscribed from angular
		this.route.params.pipe(
			switchMap((params: Params) => this.route.url.pipe(
				map((segments) => segments.join('/')),
				map((url) => {
					return {
						...params,
						url: url
					};
				})
			)),
			switchMap((params: Params) => this.route.queryParams.pipe(
				map((queryParams: Params) => {
						return {
							...params,
							...queryParams
						};
					}
				)))
		).subscribe((params: Params) => {
			this.urlQuery = params['query'];
			this.sidebarShowExplorerTree = (params[EXPLORER_TREE_META_ID] !== undefined || params[EXPLORER_TREE_ID] !== undefined);
			let selectedEo = this.eoResultService.getSelectedEo() as unknown as EntityObject;
			if (selectedEo && selectedEo.isDirty()) {
				selectedEo.triggerWarnChange();
			} else {
				this.eoRouteService.handleParams(params);
			}
		});

		this.subscriptions.add(
			this.browserRefreshService.onEoChanges().subscribe((changed) => {
				// reload eo data
				if (changed) {
					let selectedEo = this.eoResultService.getSelectedEo() as unknown as EntityObject;
					if (selectedEo && !selectedEo.isDirty()) {
						selectedEo.reload();
					}
					this.detailSelectionChanged(selectedEo);
				}
			})
		);

		this.subscriptions.add(
			this.eoResultService
				.observeSelectedEntityClassId()
				.subscribe((entityClassId: string) => {
					if (this.entityClassId !== entityClassId &&
						entityClassId !== this.previousEntityObject?.getEntityClassId()) {
						// set focus to 'quick find'
						setTimeout(() => {
							let quickFindInput = $('#text-search-input');
							if (quickFindInput) {
								quickFindInput.trigger('focus');
							}
						});
					} else {
						this.shouldSetInitialFocus = true;
						this.triggerLayoutLoaded();
					}

					this.entityClassId = entityClassId;
					let urlQueryFromSearchfilter = this.eoSearchfilterService.getUrlQuery();
					let reloadPrefs = false;
					if (this.urlQuery !== undefined) {
						reloadPrefs = true;
					} else if (!urlQueryFromSearchfilter) {
						// nur wenn noch keine URL-Query registriert wurde dürfen die Prefs aktualisiert werden,
						// ansonsten setzt die Aktualisierung die URL-Query wieder zurück.
						reloadPrefs = true;
					}
					if (
						entityClassId !== this.perspectiveService.getEntityClassId() ||
						reloadPrefs
					) {
						this.metaService.getEntityMeta(entityClassId).pipe(take(1)).subscribe(meta => {
							this.perspectiveService.selectEntityClass(meta);
							this.contentIsHorizontal = meta.showInHorizontalView();
							this.loadPrefsAndEoListAfterPerspective(entityClassId);
						});
					}

					if (entityClassId !== this.perspectiveService.getEntityClassId()) {
						this.metaService.getEntityMeta(entityClassId).pipe(take(1)).subscribe(meta => {
							this.titleService.setTitle(undefined, meta.getEntityName());
							this.contentIsHorizontal = meta.showInHorizontalView();
						});
					}
				})
		);

		this.subscriptions.add(
			this.eoResultService.observeResultListUpdate().subscribe(pristine => {
				this.eoResultService.waitForEoSelection().pipe(take(1)).subscribe(() => {
					let selectedEo = this.eoResultService.getSelectedEo();
					if (pristine && selectedEo) {
						this.eoResultService.addSelectedEoToList();
					} else if (selectedEo === undefined) {
						this.route.url.pipe(
							take(1)
						).subscribe((url: UrlSegment[]) => {
							// url segments is an array containing the activated route from angular
							// we do check here if we wanted the top eo list and thus can select the first result
							// if the length is not 2 (view/:eoId) then there was a id given which we did not found
							// if third param is search then just do select the first result
							if (url.length === 2 || ((url.length > 2) && url[2].path === 'search')) {
								this.eoResultService.selectFirstResult();
							}
						});
					}
				});
			})
		);

		this.subscriptions.add(
			this.eoResultUpdateService
				.subscribeCollectiveProcessingInitiated()
				.subscribe(_ => {
					this.collectiveProcessingExecuting = true;
				})
		);

		this.subscriptions.add(
			this.eoResultUpdateService.subscribeCollectiveProcessingFinished().subscribe(() => {
				this.collectiveProcessingExecuting = false;
			})
		);

		this.subscriptions.add(
			this.getSideviewmenuPrefSubject().subscribe(value => {
				this.contentIsHorizontal = value?.content?.sideviewHorizontal ?? this.contentIsHorizontal;
			})
		);

		this.subscriptions.add(
			this.eoResultService.waitForEoSelection().pipe(filter(selected => selected), take(1)).subscribe(() =>
				this.eoResultService.getSelectedEo()?.addListener({afterReload: eo => this.detailSelectionChanged(eo)})
			)
		);

		this.collectiveProcessingAllowed = this.authenticationService.isActionAllowed(UserAction.CollectiveProcessing);
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	toggleLayoutDirection() {
		this.contentIsHorizontal = !this.contentIsHorizontal;
		let sidevieMenu = this.getSideviewmenuPref();
		if (sidevieMenu !== undefined && sidevieMenu.content !== undefined) {
			sidevieMenu.content.sideviewHorizontal = this.contentIsHorizontal;
			if (this.contentIsHorizontal && sidevieMenu.content.sideviewMenuHeight === undefined) {
				// set default height for user
				sidevieMenu.content.sideviewMenuHeight = 200;
			}
			this.sideViewMenuService.saveSideviewmenuPreference(sidevieMenu).pipe(
				take(1)
			).subscribe();
		}
	}

	getSideviewmenuPref() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$.getValue();
	}

	getSearchtemplatePref() {
		return this.eoSearchfilterService.getSelectedSearchfilter();
	}

	getSideviewmenuPrefSubject() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$;
	}

	getSearchtemplatePrefSubject() {
		return this.eoSearchfilterService.observeSelectedSearchfilter();
	}

	warnAboutUnsavedChanges() {
		this.triggerUnsavedChangesPopover = new Date();
	}

	getMeta(): EntityMeta | undefined {
		return this.eoResultService.getSelectedMeta();
	}

	getSelectedEo() {
		return this.eoResultService.getSelectedEo();
	}

	canCreateBo() {
		return this.eoResultService.canCreateBo;
	}

	isShowCollectiveProcessing() {
		return this.eoResultService.showCollectiveProcessing;
	}

	setShowCollectiveProcessing(show: boolean) {
		this.eoResultService.showCollectiveProcessing = show;
	}

	triggerLayoutLoaded() {
		this.layoutLoaded$.next(true);
	}

	isSidebarMaximized(): boolean {
		return this.getSideviewmenuPref()?.content.sideviewState === 1;
	}

	isMultiSelectionDisabled() {
		return this.eoResultService.forceCollectiveProcessingView;
	}

	multiSelectionChanged(multiSelectionResult: EntityObjectGridMultiSelectionResult) {
		/*if (this.collectiveProcessingExecuting) {
			// just for safety, events should not be fired at all
			return;
		}*/
		// let collectiveProcessingBefore = this.collectiveProcessing;
		if (this.collectiveProcessingAllowed) {
			this.collectiveProcessing = multiSelectionResult.hasSelection();
			if (!this.collectiveProcessing) {
				if (this.isShowCollectiveProcessing()) {
					// collective processing ended, hide it...
					this.hideCollectiveProcessingIfPossible();
				}
				if (this.getSelectedEo() !== undefined) {
					let selectedEo = this.getSelectedEo() as unknown as EntityObject;
					selectedEo.setReadonly(undefined);
					// TODO API!!
					// @ts-ignore
					this.eoEventService.emitResetEo(selectedEo);
				}
			} else {
				// collective processing in selection mode, show it...
				this.setShowCollectiveProcessing(true);
				if (this.getSelectedEo() !== undefined) {
					let selectedEo = this.getSelectedEo() as unknown as EntityObject;
					selectedEo.setReadonly(true);
					// TODO API!!
					// @ts-ignore
					this.eoEventService.emitResetEo(selectedEo);
				}
			}
		}

		this.eoResultService.notifyMultiSelectionChange(multiSelectionResult);
	}

	updateCollectiveProcessingStatus(newStatus) {
		this.collectiveProcessingStatusLine = newStatus;
	}

	/**
	 * TODO: Prefs and EO list are now loaded twice in some cases, because this component is
	 * not a singleton, but instantiated at least twice by angular (because of different routes).
	 *
	 * @param entityClassId
	 */
	private loadPrefsAndEoListAfterPerspective(entityClassId: string) {
		this.subscriptions.add(
			this.perspectiveService
				.waitForLoadingFinished()
				.pipe(take(1))
				.subscribe(() => this.loadPrefsAndEoList(entityClassId))
		);
	}

	private loadPrefsAndEoList(entityClassId: string) {
		this.$log.debug('Load searchtemplate Prefs and EO list for entity class %o', entityClassId);

		let entityMeta = this.metaService.getBoMeta(entityClassId);
		if (!entityMeta) {
			return;
		}

		this.subscriptions.add(
			entityMeta.pipe(distinctUntilChanged()).subscribe(meta => {
				this.eoResultService.setSelectedMeta(meta);

				this.subscriptions.add(
					this.entityObjectPreferenceService
						.loadPreferences(meta, this.urlQuery)
						.subscribe(() => {
							this.$log.debug('Prefs loaded');
						})
				);
			})
		);
	}

	private hasResults() {
		return this.eoResultService.getLoadedResultCount() > 0;
	}

	private hideCollectiveProcessingIfPossible() {
		if (
			this.isShowCollectiveProcessing() &&
			!this.eoResultService.forceCollectiveProcessingView
		) {
			this.setShowCollectiveProcessing(false);
		}
	}

	private getChangedByString(eo: IEntityObject): string {
		let changedBy = eo.getChangedBy();
		let changedByString = changedBy ? changedBy : '';

		let changedAt = eo.getChangedAt();
		let changedAtString = changedAt
			? this.dateTimeService.formatTimestamp(changedAt, this.nuclosI18n.getCurrentLocale().dateTimeWithSecondsPattern)
			: '';
		let changedLangStr = this.nuclosI18n.getI18n('webclient.sideview.detail.changed');
		let statusBarChangedByString = `${changedLangStr}: ${changedByString} [${changedAtString}]`;

		return statusBarChangedByString;
	}

	private getCreatedByString(eo: IEntityObject): string {
		let createdBy = eo.getCreatedBy();
		let createdByString = createdBy ? createdBy : '';

		let createdAt = eo.getCreatedAt();
		let createdAtString = createdAt
			? this.dateTimeService.formatTimestamp(createdAt, this.nuclosI18n.getCurrentLocale().dateTimeWithSecondsPattern)
			: '';

		let createdLangStr = this.nuclosI18n.getI18n('webclient.sideview.detail.created');
		let statusBarCreatedByString = `${createdLangStr}: ${createdByString} [${createdAtString}]`;

		return statusBarCreatedByString;
	}

	private detailSelectionChanged(eo: IEntityObject) {
		if (eo) {
			this.changedByString = this.getChangedByString(eo);
			this.createdByString = this.getCreatedByString(eo);

			if (eo instanceof EntityObject) {
				eo.setReadonly(this.collectiveProcessing);
				if (this.collectiveProcessing) {
					this.hideCollectiveProcessingIfPossible();
				}
			}
		}
	}
}

/**
 * view eo in popup
 * no sidebar/toolbar/header
 */
@Component({
	selector: 'nuc-entity-object',
	templateUrl: './entity-object-popup.component.html',
	styleUrls: ['./entity-object.component.css']
})
export class EntityObjectPopupComponent extends EntityObjectComponent {
	private selectEntryInOtherWindowToken: string | undefined;

	ngOnInit() {
		this.route.queryParams.pipe(
			take(1)
		).subscribe((params) => {
			this.isPopupOpened = this.router.url.indexOf('/popup/') !== -1;
			if (params['showHeader'] !== 'true' && this.isPopupOpened) {
				$('header').hide();
			}
			let selectEntryInOtherWindowToken = params[SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM];
			if (selectEntryInOtherWindowToken) {
				this.selectEntryInOtherWindowToken = selectEntryInOtherWindowToken;
			}
		});
		super.ngOnInit();
	}

	@HostListener('window:unload', ['$event'])
	ngOnDestroy() {
		super.ngOnDestroy();
		if (this.selectEntryInOtherWindowToken) {
			let selectReferenceEvent = new SelectReferenceInOtherWindowEvent();
			selectReferenceEvent.entityClassId = this.getMeta() ? this.getMeta()!.getEntityClassId() : '';
			selectReferenceEvent.eoId = this.getSelectedEo()?.getId()!;
			selectReferenceEvent.updateOnly = true;
			let name = this.getSelectedEo()?.getData().title;
			if (name) {
				selectReferenceEvent.name = name;
			}
			selectReferenceEvent.token = +this.selectEntryInOtherWindowToken;
			this.browserRefreshService.deleteListenerInOtherWindow(selectReferenceEvent);
		}
	}
}
