import { NgModule } from '@angular/core';
import { ReferenceRendererComponent } from '@modules/entity-object-grid/cell-renderer';
import { EntityObjectGridModule } from '@modules/entity-object-grid/entity-object-grid.module';
import {
	SideExplorerTreeComponent
} from '@modules/entity-object/sidebar/side-explorer-tree/side-explorer-tree.component';
import { StatePathModule } from '@modules/state-path/state-path.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgGridModule } from 'ag-grid-angular';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { SharedModule } from 'app/shared/shared.module';
import { TableModule } from 'primeng/table';
import { TreeModule } from 'primeng/tree';
import { LayoutModule } from '../../layout/layout.module';
import { AddonModule } from '../addons/addon.module';
import { AuthenticationModule } from '../authentication/authentication.module';
import { ChartModule } from '../chart/chart.module';
import { CommandModule } from '../command/command.module';
import { EntityObjectDataModule } from '../entity-object-data/entity-object-data.module';
import { EntityObjectGridService } from '../entity-object-grid/entity-object-grid.service';
import {
	MultiSelectionEditorComponent
} from '../entity-object-grid/multi-selection-editor/multi-selection-editor.component';
import {
	MultiSelectionHeaderComponent
} from '../entity-object-grid/multi-selection-header/multi-selection-header.component';
import {
	MultiSelectionRendererComponent
} from '../entity-object-grid/multi-selection-renderer/multi-selection-renderer.component';
import { GenerationModule } from '../generation/generation.module';
import {
	BooleanRendererComponent,
	DateRendererComponent,
	HyperlinkRendererComponent,
	NumberRendererComponent,
	StateIconRendererComponent
} from '../grid/grid/cell-renderer';
import { ImageRendererComponent } from '../grid/grid/cell-renderer/image-renderer/image-renderer.component';
import { I18nModule } from '../i18n/i18n.module';
import { InputRequiredModule } from '../input-required/input-required.module';
import { PerspectiveModule } from '../perspective/perspective.module';
import { PrintoutModule } from '../printout/printout.module';
import { RuleModule } from '../rule/rule.module';
import { SearchModule } from '../search/search.module';
import { StateModule } from '../state/state.module';
import { UiComponentsModule } from '../ui-components/ui-components.module';
import {
	CollectiveEditingValueDialogComponent
} from './collective-editing-value-dialog/collective-editing-value-dialog.component';
import { CollectiveEditingComponent } from './collective-editing/collective-editing.component';
import {
	OpenRendererComponent
} from './collective-processing-progress/cell-renderer/open-renderer/open-renderer.component';
import {
	CollectiveProcessingProgressComponent
} from './collective-processing-progress/collective-processing-progress.component';
import { CollectiveProcessingComponent } from './collective-processing/collective-processing.component';
import { DetailButtonsComponent } from './detail-buttons/detail-buttons.component';
import { DetailDialogComponent } from './detail-dialog/detail-dialog.component';
import { DetailModalComponent } from './detail-modal/detail-modal.component';
import { DetailStatusbarComponent } from './detail-statusbar/detail-statusbar.component';
import {
	DetailToolbarChartItemComponent,
	DetailToolbarGenerationItemComponent,
	DetailToolbarLockingItemComponent,
	DetailToolbarPrintoutItemComponent,
	DetailToolbarStateItemComponent
} from './detail-toolbar';
import { DetailToolbarItemComponent } from './detail-toolbar/detail-toolbar-item/detail-toolbar-item.component';
import { DetailToolbarComponent } from './detail-toolbar/detail-toolbar.component';
import { DetailComponent } from './detail/detail.component';
import { MandatorStripeComponent } from './detail/mandator-stripe/mandator-stripe.component';
import { EntityObjectRouteService } from './entity-object-route.service';
import { EntityObjectComponent, EntityObjectPopupComponent } from './entity-object.component';
import { EntityObjectRoutes } from './entity-object.routes';
import { LegacyRouteComponent } from './legacy-route/legacy-route.component';
import { LockingComponent } from './locking/locking.component';
import { EntityObjectPreferenceService } from './shared/entity-object-preference.service';
import { SideviewmenuService } from './shared/sideviewmenu.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ExportBoListComponent } from './sidebar/statusbar/export-bo-list/export-bo-list.component';
import {
	ExportBoListTemplateDirective,
	ExportBoListTemplateState,
	StatusbarComponent
} from './sidebar/statusbar/statusbar.component';
import { ViewPreferencesConfigModule } from './view-preferences-config/view-preferences-config.module';
import { LocalizationDialogComponent } from './localization-dialog/localization-dialog.component';
import { TreeDirective } from './sidebar/side-explorer-tree/tree.directive';


@NgModule({
	imports: [
		AddonModule,
		SharedModule,

		AuthenticationModule,
		ChartModule,
		CommandModule,
		EntityObjectDataModule,
		EntityObjectRoutes,
		GenerationModule,
		EntityObjectGridModule,
		I18nModule,
		InputRequiredModule,
		LayoutModule,
		PerspectiveModule,
		PrintoutModule,
		RuleModule,
		SearchModule,
		StateModule,
		ViewPreferencesConfigModule,

		AgGridModule,
		PrettyJsonModule,
		NgbModule,
		UiComponentsModule,
		StatePathModule,
		TableModule,
		TreeModule,
	],
	declarations: [
		ImageRendererComponent,
		DetailButtonsComponent,
		DetailComponent,
		DetailModalComponent,
		DetailDialogComponent,
		DetailToolbarChartItemComponent,
		DetailToolbarComponent,
		DetailToolbarGenerationItemComponent,
		DetailToolbarItemComponent,
		DetailToolbarLockingItemComponent,
		DetailToolbarPrintoutItemComponent,
		DetailToolbarStateItemComponent,
		EntityObjectComponent,
		EntityObjectPopupComponent,
		ExportBoListComponent,
		ExportBoListTemplateDirective,
		LegacyRouteComponent,
		LockingComponent,
		MandatorStripeComponent,
		SidebarComponent,
		StatusbarComponent,
		SideExplorerTreeComponent,
		ReferenceRendererComponent,
		DetailStatusbarComponent,
		MultiSelectionEditorComponent,
		MultiSelectionHeaderComponent,
		MultiSelectionRendererComponent,
		CollectiveProcessingComponent,
		CollectiveProcessingProgressComponent,
		OpenRendererComponent,
		CollectiveEditingComponent,
		CollectiveEditingValueDialogComponent,
		LocalizationDialogComponent,
		TreeDirective,
	],
	providers: [
		SideviewmenuService,
		ExportBoListTemplateState,
		EntityObjectGridService,
		EntityObjectRouteService,
		EntityObjectPreferenceService
	],
	exports: [
		DetailModalComponent,
	]
})
export class EntityObjectModule {
}
