import { Component, Input, OnInit } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { AuthenticationService } from '@modules/authentication';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityAttrMeta } from '@modules/entity-object-data/shared/bo-view.model';

@Component({
	selector: 'nuc-localization-dialog',
	templateUrl: './localization-dialog.component.html',
	styleUrls: ['./localization-dialog.component.css']
})
export class LocalizationDialogComponent implements OnInit {

	@Input() eo: EntityObject;
	@Input() attributeMeta: EntityAttrMeta;

	dataLanguages: DataLanguage[] | undefined;
	languageEntryMap: Map<DataLanguage, string> = new Map<DataLanguage, string>();
	primaryDataLanguage: DataLanguage | undefined;
	attrMetaScale: number | undefined;

	constructor(
		private dialogService: NuclosDialogService,
		private authenticationService: AuthenticationService
	) {
	}

	ngOnInit(): void {
		this.attrMetaScale = this.attributeMeta.getScale();
		this.dataLanguages = this.authenticationService.getAllDataLanguages();
		this.dataLanguages?.forEach(dl => {
			this.languageEntryMap.set(dl, this.eo.getAttributeTranslation(this.attributeMeta.getAttributeName(), dl.datalanguage));
		});
		this.primaryDataLanguage = this.authenticationService.getPrimaryDataLanguage();
	}

	ngOnChanges(): void {

	}

	close(): void {
		this.languageEntryMap.forEach((value, key) => {
			this.eo.setAttributeTranslation(this.attributeMeta.getAttributeName(), key.datalanguage, value);
		});
		this.dialogService.closeCurrentModal();
	}

	cancel(): void {
		this.dialogService.closeCurrentModal();
	}

}
