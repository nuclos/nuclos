import { Injectable } from '@angular/core';
import { Logger } from '@modules/log/shared/logger';
import { UserAction } from '@nuclos/nuclos-addon-api';
import { ArrayUtils } from '@shared/array-utils';
import { FqnService } from '@shared/service/fqn.service';
import { GridOptions } from 'ag-grid-community';
import { BehaviorSubject, Observable, of as observableOf, Subject } from 'rxjs';

import { finalize, map, take } from 'rxjs/operators';
import { SubformColumn } from '../../../layout/web-subform/web-subform.model';
import { AuthenticationService } from '../../authentication';
import { EntityAttrMeta, EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { SelectableService } from '../../entity-object-data/shared/selectable.service';
import {
	AttributeSelectionContent,
	ColumnAttribute,
	Preference,
	SelectedAttribute,
	SidebarLayoutType,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';

@Injectable({
	providedIn: 'root'
})
export class SideviewmenuService {
	private sideviewmenuPrefChanges: Subject<any>;
	private sidebarLayoutType: BehaviorSubject<SidebarLayoutType>;

	constructor(
		private selectableService: SelectableService,
		private authenticationService: AuthenticationService,
		private fqnService: FqnService,
		private preferenceService: PreferencesService,
		private $log: Logger
	) {
		this.sideviewmenuPrefChanges = new Subject();
		this.sidebarLayoutType = new BehaviorSubject<SidebarLayoutType>(
			this.getViewTypeFromLocalStorage()
		);
	}

	onSideviewmenuPrefChange(): Subject<any> {
		return this.sideviewmenuPrefChanges;
	}

	sideviewmenuPrefChanged(): void {
		this.sideviewmenuPrefChanges.next(new Date().getTime());
	}

	emptySideviewmenuPreference(
		entityMeta: EntityMeta,
		selectDefaultColumns: boolean,
		addNotSelectedColumns = true
	): Preference<SideviewmenuPreferenceContent> {
		let sideviewmenuPreference = new Preference<SideviewmenuPreferenceContent>(
			'table',
			entityMeta.getBoMetaId()
		);
		sideviewmenuPreference.content.sideviewMenuWidth = 250;
		sideviewmenuPreference.content.columns = [];

		if (selectDefaultColumns) {
			if (sideviewmenuPreference.type === 'table') {
				sideviewmenuPreference.content.columns = [];

				// show state icon in first column for EO's with statemodel
				if (entityMeta.hasStateModel()) {
					let stateAttr = entityMeta.getAttributes().get('nuclosStateIcon');
					if (stateAttr) {
						sideviewmenuPreference.content.columns.push({
							name: stateAttr.getName(),
							boAttrId: stateAttr.getAttributeID(),
							system: true,
							selected: true,
							sort: {
								enabled: true
							},
							width: 25
						});
					}
				}

				let titleAttributes: string[] = selectDefaultColumns
					? this.fqnService.parseFqns(entityMeta.getMetaData().titlePattern)
					: [];

				// load all sideviewmenuColumns from meta data
				entityMeta.getAttributes().forEach(attributeMeta => {
					let visible = this.isVisibleAttribute(attributeMeta);
					if (visible) {
						let selected =
							selectDefaultColumns &&
							titleAttributes.indexOf(attributeMeta.getAttributeID()) !== -1;
						if (selected || addNotSelectedColumns) {
							sideviewmenuPreference.content.columns.push({
								name: attributeMeta.getName(),
								boAttrId: attributeMeta.getAttributeID(),
								system: attributeMeta.isSystemAttribute(),
								selected: selected,
								sort: {
									enabled: attributeMeta && !attributeMeta.isCalculated()
								}
							});
						}
					}
				});
			}
		}

		return sideviewmenuPreference;
	}

	emptySubformPreference(
		metaData: EntityMeta,
		parentEntityUid: string,
		selectDefaultColumns: boolean
	): Preference<SideviewmenuPreferenceContent> {
		let sideviewmenuPreference = new Preference<SideviewmenuPreferenceContent>(
			'subform-table',
			metaData.getBoMetaId()
		);
		sideviewmenuPreference.selected = false;
		sideviewmenuPreference.content.columns = [];

		if (selectDefaultColumns) {
			// default column layout (see ProfileUtils.java for Java-Client)

			let defaultColumnWidth = (attributeMeta: EntityAttrMeta): number => {
				switch (attributeMeta.getType()) {
					case 'String':
						return 120;
					case 'Integer':
					case 'Decimal':
					case 'Date':
						return 80;
					case 'Boolean':
					case 'Image':
						return 40;
				}
				return 120;
			};

			sideviewmenuPreference.content.columns = Array.from(metaData.getAttributes().values())

				// no hidden attributes
				.filter(attributeMeta => !attributeMeta.isHidden())

				// no calculated attributes
				.filter(attributeMeta => !attributeMeta.isCalculated())

				// no system attributes except state/stateIcon
				.filter(
					attributeMeta =>
						!attributeMeta.isSystemAttribute() ||
						attributeMeta.isState() ||
						attributeMeta.isStateIcon()
				)

				// no blobs
				.filter(attributeMeta => {
					let scale = attributeMeta.getScale();
					return !(attributeMeta.getType() === 'String' && scale && scale > 255);
				})

				// max 30 columns
				.slice(0, 30)

				.map(attributeMeta => {
					return {
						name: attributeMeta.getName(),
						boAttrId: attributeMeta.getAttributeID(),
						system: attributeMeta.isSystemAttribute(),
						selected: true,
						width: defaultColumnWidth(attributeMeta),
						sort: {
							enabled: attributeMeta && !attributeMeta.isCalculated()
						}
					};
				});

			// show state icon at first column
			let stateIconIndex = sideviewmenuPreference.content.columns.findIndex(c =>
				c.boAttrId.endsWith('nuclosStateIcon')
			);
			if (stateIconIndex !== -1) {
				ArrayUtils.move(sideviewmenuPreference.content.columns, stateIconIndex, 0);
			}
		}

		return sideviewmenuPreference;
	}

	saveSideviewmenuPreference(
		preferenceItem: Preference<SideviewmenuPreferenceContent>
	): Observable<Preference<SideviewmenuPreferenceContent>> {
		if (
			!this.authenticationService.isActionAllowed(
				UserAction.WorkspaceCustomizeEntityAndSubFormColumn
			)
		) {
			return observableOf(preferenceItem).pipe(
				finalize(() => this.sideviewmenuPrefChanged())
			);
		}
		// @ts-ignore
		return this.selectableService.savePreference(preferenceItem).pipe(
			finalize(() => this.sideviewmenuPrefChanged())
		);
	}

	loadSideviewmenuPreference(entityClassId: string) {
		return this.preferenceService
			.getPreferences({
				boMetaId: entityClassId
			})
			.pipe(
				map(prefs => {
					// Try to find a selected preference first
					let result = prefs.find(pref => pref.type === 'table' && !!pref.selected);

					// Or simply use the first one
					if (!result && prefs.length > 0) {
						result = prefs.find(pref => pref.type === 'table');
					}

					return result;
				})
			);
	}

	getSelectedColumnsSorted(
		sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>
	): ColumnAttribute[] {
		if (!sideviewmenuPreference.content.columns) {
			return [];
		}
		return this.sortColumns(sideviewmenuPreference.content.columns.filter(c => c.selected));
	}

	sortColumns(columns: ColumnAttribute[]): ColumnAttribute[] {
		return columns.sort((a, b) => {
			if (a.selected && !b.selected) {
				return -1;
			}
			if (!a.selected && b.selected) {
				return 1;
			}
			if (a.position === undefined || b.position === undefined) {
				return 0;
			}
			if (a.position < b.position) {
				return -1;
			}
			if (a.position > b.position) {
				return 1;
			}
			return 0;
		});
	}

	getViewType(): BehaviorSubject<SidebarLayoutType> {
		return this.sidebarLayoutType;
	}

	setViewType(type: SidebarLayoutType): void {
		localStorage.setItem('SidebarLayoutType', type);
		this.sidebarLayoutType.next(type);
	}

	/**
	 * TODO: Sideviewmenu-Service should not handle GridOptions - we have a GridService for this
	 */
	getSelectedAttributesFromGrid(
		gridOptions: GridOptions,
		subformMeta: EntityMeta
	): SelectedAttribute[] {
		let subformColumns: SubformColumn[] = [];
		if (gridOptions.columnApi && gridOptions.api) {
			// column order and column width
			subformColumns = gridOptions.columnApi
				.getColumnState()
				.filter(col =>
					col?.colId?.length !== undefined && col?.colId?.length > 1 && col.colId !== 'selected') // filter out row selection column
				.filter (col => col?.hide !== true) // do not save hidden columns as they are dragged out
				.map(cs => {
					try {
						let fieldName = this.fqnService.getShortAttributeName(
							subformMeta.getBoMetaId(),
							cs.colId
						);
						let name;
						if (subformMeta.hasAttributes()) {
							let nameMeta = subformMeta.getAttributeMetaByFqn(cs.colId);
							if (nameMeta) {
								name = nameMeta.getName();
							}
						}
						return {
							eoAttrFqn: cs.colId,
							name: name,
							fieldName: fieldName,
							width: cs.width,
							fixed: cs.pinned
						};
					} catch (e) {
						return [];
					}
				}) as SubformColumn[];

			// map ag-grid columns to preference columns
			subformColumns = gridOptions.columnApi
				.getAllGridColumns()
				.map(c => c.getId())
				.map((colId: string) => {
					return (
						subformColumns.filter(col => col.eoAttrFqn === colId).shift() ||
						({} as SubformColumn)
					);
				});

			// column sort order
			gridOptions.columnApi.getColumnState().forEach(col => {
				let subformColumn = subformColumns.filter(sc => sc.eoAttrFqn === col.colId).shift();
				if (subformColumn) {
					subformColumn.sort = {
						direction: col.sort,
						prio: col.sortIndex
					};
				}
			});
		}

		let position = 0;
		let selectedAttributes = subformColumns
			.filter(col => col.eoAttrFqn)
			.map(gc => {
				return {
					boAttrId: gc.eoAttrFqn,
					name: gc['name'], // TODO refactor column interfaces
					width: gc.width,
					position: position++,
					selected: true,
					sort: gc.sort,
					fixed: gc.fixed
				} as SelectedAttribute;
			});
		return selectedAttributes;
	}

	/**
	 * TODO: Result is always wrapped in SortModel. Return SortModel directly here.
	 */
	getAgGridSortModel(columns: ColumnAttribute[], pref: Preference<AttributeSelectionContent>): { colId; sort; sortIndex }[] {
		let agSortColumns = columns
			.filter(column => column.sort)
			.sort((a: ColumnAttribute, b: ColumnAttribute) => {
				if (!a.sort || !b.sort || a.sort.prio === null || b.sort.prio === null) {
					return 0;
				}
				if (a.sort.prio! < b.sort.prio!) {
					return -1;
				}
				if (a.sort.prio! > b.sort.prio!) {
					return 1;
				}
				return 0;
			})
			.map(column => {
				return {
					// Sometimes there are columns without "boAttrId", but only "name"...
					colId: column.boAttrId || column.name,
					sort: column.sort ? column.sort.direction : 'asc',
					sortIndex: column.sort?.prio !== undefined ? column.sort?.prio! - 1 : undefined
				};
			});

		if (this.needFixup(pref, agSortColumns)) {
			// need a fixup
			[...agSortColumns]
				.filter((element) => element.sortIndex !== undefined)
				.sort((a, b) => a.sortIndex! < b.sortIndex! ? -1 : 1).forEach(
				(item, index) => {
					agSortColumns.find((origin) => origin.colId === item.colId)!.sortIndex = index;
				}
			);
			pref.content.columns.forEach((prefColumn) => {
				if (prefColumn.sort !== undefined) {
					prefColumn.sort.prio = agSortColumns.find((sortColumn) => sortColumn.colId === prefColumn.boAttrId)?.sortIndex;
				}
			});
			this.$log.warn('FIXING Preference Sort-Indecies', pref);
			this.selectableService.savePreference(pref).pipe(
				take(1)
			).subscribe();
		}


		return agSortColumns;
	}

	private isVisibleAttribute(attributeMeta: EntityAttrMeta) {
		let visible =
			!attributeMeta.isStateIcon() &&
			!attributeMeta.isDeletedFlag() &&
			!attributeMeta.isHidden();

		return visible;
	}

	private getViewTypeFromLocalStorage(): SidebarLayoutType {
		let type = localStorage.getItem('SidebarLayoutType');
		if (type !== undefined) {
			return type as SidebarLayoutType;
		}
		return 'table';
	}

	private needFixup(
		pref: Preference<AttributeSelectionContent>,
		agSortColumns: { sortIndex: number | undefined; sort: string | null | undefined; colId: string | undefined }[]
	) {
		return agSortColumns
			.find(
				(element) =>
					element.sortIndex &&
					(
						element.sortIndex < 0 ||
						element.sortIndex > [...pref.content.columns]
							.filter(prefColumn => prefColumn.sort !== undefined).length
					)
			);
	}
}
