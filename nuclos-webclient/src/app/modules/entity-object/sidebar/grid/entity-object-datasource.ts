import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { IDatasource, IGetRowsParams } from 'ag-grid-community';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, take } from 'rxjs/operators';
import { FqnService } from '../../../../shared/service/fqn.service';
import { NuclosConfigService } from '../../../../shared/service/nuclos-config.service';
import { BoViewModel } from '../../../entity-object-data/shared/bo-view.model';
import { DataService } from '../../../entity-object-data/shared/data.service';
import { EntityObjectEventService } from '../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectResultUpdateService } from '../../../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../../../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';
import { LoadMoreResultsEvent } from '../../entity-object.component';
import { SidebarCardLayoutViewItem, SidebarViewItem } from '../view/sidebar-view.model';

export class EntityObjectDatasource implements IDatasource {

	private boResult: BoViewModel | undefined;
	private loadMoreResultEvent: LoadMoreResultsEvent;

	/**
	 * provides LoadMoreResultsEvent to debounce scroll events
	 */
	private loadMoreResults$: Subject<LoadMoreResultsEvent> = new Subject<LoadMoreResultsEvent>();

	private loadTrigger = new Subject();

	private dataLoaded = new Subject();

	private eventBuffer: LoadMoreResultsEvent[] = [];

	private destroyed = false;

	constructor(
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private entityObjectResultService: EntityObjectResultService,
		private nuclosConfigService: NuclosConfigService,
		private eoEventService: EntityObjectEventService,
		private i18nService: NuclosI18nService
	) {
		this.loadMoreResults$.asObservable().pipe(
			distinctUntilChanged()).subscribe(event => {
				if (this.destroyed) {
					return;
				}
				/**
				 * TODO: We would want to load only the last request after debouncing.
				 * But there seems to be no way at the moment to cancel the previous requests
				 * while keeping the grid intact.
				 * Even when calling IGetRowsParams.failCallback there will still be gaps
				 * in the result list.
				 * See NUCLOS-6902
				 *
				 * The current workaround is to load the buffered requests in reverse order,
				 * so that the user gets to see the last requested data first (which should correspond to the current
				 * scroll position in the sidebar)
				 */
				this.eventBuffer.push(event);

				// Using a secondary, debounced Observable as the primary one can't be debounced directly.
				this.loadTrigger.next(true);
			}
		);

		this.eoEventService.observeCreatedEo().subscribe((eo: EntityObject) => {
			this.addSelectedEoToModel(eo);
		});

		// Debouncing of requests happens indirectly via this secondary Observable.
		this.loadTrigger.asObservable().pipe(
			debounceTime(300))
			.subscribe(() => this.handleEventsAfterBufferTime());
	}

	getRows(params: IGetRowsParams): void {
		if (this.destroyed) {
			return;
		}
		let selectedEo = this.entityObjectResultService.getSelectedEo() as EntityObject;

		// if the selected eo is new and not in the list already - add it on top of the list
		let addUnsavedEo = selectedEo && selectedEo.isNew()
			&& params.startRow === 0;

		// TODO: Too complex - simplify!
		if (addUnsavedEo && this.boResult && this.entityObjectResultService.getResults().length > 0) {
			let sidebarView = this.buildSidebarView(this.boResult, selectedEo);
			if (this.destroyed) {
				return;
			}
			params.successCallback(
				sidebarView,
				this.boResult.total ? this.boResult.total + 1 : this.boResult.total
			);

		} else {
			let limit = params.endRow - params.startRow;
			this.loadMoreResultEvent = new LoadMoreResultsEvent(
				params.startRow,
				limit,
				this.loadMoreResultEvent === undefined,
				params.successCallback,
				params.failCallback
			);
			this.loadMoreResults$.next(this.loadMoreResultEvent);
		}
	}

	destroy() {
		this.destroyed = true;
		this.loadTrigger.unsubscribe();
		this.loadMoreResults$.unsubscribe();
		this.dataLoaded.unsubscribe();
	}

	addSelectedEoToModel(eo: EntityObject): void {
		let selectedEo = this.entityObjectResultService.getSelectedEo() as EntityObject;
		if (this.boResult && eo === selectedEo) {
			if (this.boResult.bos.indexOf(selectedEo!) < 0) {
				this.boResult.bos.unshift(selectedEo!);
				if (this.boResult.total !== undefined) {
					this.boResult.total++;
				}
			}
		}
	}

	observeLoadedData(): Observable<any> {
		return this.dataLoaded.asObservable();
	}

	protected buildSidebarView(boViewModel: BoViewModel | undefined, selectedEo?: EntityObject) {
		let eos: EntityObject[] = [];
		if (selectedEo) {
			eos.push(selectedEo);
		}
		boViewModel?.bos.forEach(bo => eos.push(bo));
		return eos.map(
			(eo: EntityObject) => new SidebarViewItem(
				eo,
				this.nuclosConfigService,
				this.i18nService
			).build()
		);
	}

	private handleEventsAfterBufferTime() {
		if (this.destroyed) {
			return;
		}
		this.eventBuffer.reverse().forEach(event => {
			this.eoResultUpdateService.loadData(event).pipe(take(1)).subscribe(
				boResult => {
					if (this.destroyed) {
						return;
					}
					this.boResult = boResult;
					let sidebarView = this.buildSidebarView(boResult);
					if (this.destroyed) {
						return;
					}
					event.successCallback(
						sidebarView,
						boResult?.total
					);
					this.dataLoaded.next(boResult);
				});
		});

		this.eventBuffer = [];
	}
}

export class EntityObjectTitleInfoDatasource extends EntityObjectDatasource {

	private configService;
	private i18n;

	constructor(
		eoResultUpdateService: EntityObjectResultUpdateService,
		entityObjectResultService: EntityObjectResultService,
		nuclosConfigService: NuclosConfigService,
		eoEventService: EntityObjectEventService,
		i18nService: NuclosI18nService,
		private fqnService: FqnService,
		private dataService: DataService,
	) {
		super(eoResultUpdateService, entityObjectResultService, nuclosConfigService, eoEventService, i18nService);
		this.configService = nuclosConfigService;
		this.i18n = i18nService;
	}

	protected buildSidebarView(boViewModel: BoViewModel, selectedEo?: EntityObject) {
		let eos: EntityObject[] = [];
		if (selectedEo) {
			eos.push(selectedEo);
		}
		boViewModel.bos.forEach(bo => eos.push(bo));
		return eos.map(eo => {
			return new SidebarCardLayoutViewItem(
				eo,
				this.configService,
				this.i18n,
				this.fqnService,
				this.dataService
			).build();
		});
	}
}
