import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { EXPLORER_TREE_ID, EXPLORER_TREE_META_ID, VLP_ID_PARAM } from '@shared/service/browser-refresh.service';
import { MetaService } from 'app/modules/entity-object-data/shared/meta.service';
import { MenuService } from 'app/modules/menu/menu.service';
import { TooltipDefaultOptions } from 'app/modules/ui-components/tooltip/tooltip-default-options';
import { DataService } from 'app/modules/entity-object-data/shared/data.service';
import { EntityObjectResultService } from 'app/modules/entity-object-data/shared/entity-object-result.service';
import { EntityObjectService } from 'app/modules/entity-object-data/shared/entity-object.service';
import { NuclosConfigService } from 'app/shared/service/nuclos-config.service';
import { TreeNode } from 'primeng/api';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { defaultIfEmpty, takeUntil } from 'rxjs/operators';
import { IEntityObject, Logger } from '@nuclos/nuclos-addon-api';
import { BehaviorSubject, forkJoin, Observable, Subject, Subscription } from 'rxjs';

interface ToolbarButton {
	id: string;
	labelKey: string;

	buttonCssClass: string;
	iconCssClass: string;

	action(eo: EntityObject | undefined): void;

	isVisible(eo: EntityObject | undefined): boolean;

	isEnabled(eo: EntityObject | undefined): boolean;

	blur?(): void;
}

@Component({
	selector: 'nuc-side-explorer-tree',
	templateUrl: './side-explorer-tree.component.html',
	styleUrls: ['./side-explorer-tree.component.css']
})
export class SideExplorerTreeComponent implements OnInit {

	private static imagePath: string;

	RELATION_ICONS_MAP = {
		'tree-parent-to-child': 'fa-turn-up',
		'tree-child-to-parent': 'fa-turn-down',
		'part-of': 'fa-object-ungroup',
		'composite-of': 'fa-object-group'
	};

	RELATION_ICON_STYLE = {
		'tree-parent-to-child': {'transform': 'rotate(90deg)'},
		'tree-child-to-parent': {'transform': 'scale(-1)'},
		'part-of': {},
		'composite-of': {}
	};

	treeMetaId: string | undefined;

	treeId: string | undefined;

	@Input()
	selectedEo: IEntityObject;

	@Output()
	selectedNode = new EventEmitter<any>();

	/** the node, that is currently opened in the main view **/
	mainViewTreeNode: TreeNode | undefined;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	buttons: ToolbarButton[] = [];
	primeTreeData = new BehaviorSubject<TreeNode[] | undefined>(undefined);
	treeListener: Subscription;
	refreshInProgress = false;
	loading = false;
	protected unsubscribe$ = new Subject<boolean>();
	protected cancelRefresh$ = new Subject<boolean>();


	constructor(
		private route: ActivatedRoute,
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService,
		private nuclosConfig: NuclosConfigService,
		private menuService: MenuService,
		private dataService: DataService,
		private dialogService: NuclosDialogService,
		private i18n: NuclosI18nService,
		private metaService: MetaService,
		private $log: Logger
	) {
		SideExplorerTreeComponent.imagePath = this.nuclosConfig.getRestHost() + '/meta/icon';
	}

	ngOnInit(): void {
		this.fetchTreeRootFromQueryParams();
		this.initializeToolbarButtons();
		this.initializeTreeListener();
	}

	ngOnChanges(changes: SimpleChanges): void {
		let rootNode = this.primeTreeData.value;
		if (rootNode && rootNode[0]) {
			this.mainViewTreeNode = this.recursiveSearchMainViewNode(rootNode[0]);
		}
	}

	ngOnDestroy(): void {
		if (this.treeListener) {
			this.treeListener.unsubscribe();
		}
		if (this.primeTreeData) {
			this.primeTreeData.unsubscribe();
		}
		this.cancelRefresh$.next(true);
		this.cancelRefresh$.complete();
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	fetchTreeRootFromQueryParams() {
		this.route.queryParams
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe((params: Params) => {
				this.treeMetaId = params[EXPLORER_TREE_META_ID];
				this.treeId = params[EXPLORER_TREE_ID];
				this.loadRootNode();
			});
	}

	initializeTreeListener() {
		if (!this.treeListener) {
			this.treeListener = this.primeTreeData.subscribe((primeTreeData => {
				if (primeTreeData === undefined) {
					return;
				}
				this.mainViewTreeNode = primeTreeData ? this.recursiveSearchMainViewNode(primeTreeData[0]) : undefined;
			}));
		}
	}

	/**
	 * Wraps the node data inside a primeng node.
	 * @param data - node entry response from the rest service
	 */
	createPrimeNode(data): TreeNode {
		let treeNode: TreeNode = {
			key: data.node_id,
			label: data.title,
			leaf: false,
			data: data,
		};
		if (data.icon) {
			treeNode.icon = SideExplorerTreeComponent.imagePath + '/' + data.icon;
		}
		if (data.leaf) {
			treeNode.leaf = true;
		}
		return treeNode;
	}

	/**
	 * Assigns the nodes contained inside the requestResult to the specified parent node
	 * If the length of the children array is equal to 0, the 'leaf' property will be set for the parent node
	 * Finally, the resulting tree is pushed to the BehaviorSubject
	 * @param parent
	 * @param requestResult
	 */
	assignSubNodes(parent: TreeNode, requestResult) {
		parent.children = [];
		requestResult.forEach(entry => {
			this.insertSubNode(parent, this.createPrimeNode(entry));
		});
		if (parent.children.length === 0) {
			parent['leaf'] = true;
		}
		this.primeTreeData.next(this.primeTreeData.value);
	}

	/**
	 * Sends a REST Request to the nuclos server in order to fetch the children of the events' node.
	 * If the server response is not empty, the loaded children will be inserted into the tree.
	 * Otherwise, the node of the event will be marked as a leaf.
	 * @param node - node to load the subNodes for
	 */
	loadSubNodes(node: TreeNode | undefined) {
		// undefined children means, we have never attempted to load sub nodes
		if (node && !('children' in node)) {
			this.dataService.loadSubTreeData(node.data.node_id)
				.pipe(takeUntil(this.unsubscribe$))
				.subscribe((result) => {
					this.assignSubNodes(node, result);
				});
		}
	}

	/**
	 * Inserts a subnode into a primeng tree node.
	 * If not already, the children list will be initialized before the insertion.
	 * @param locationNode - the node, where the new node will be inserted
	 * @param subNode - the new subnode, that will be inserted
	 */
	insertSubNode(locationNode: TreeNode, subNode: TreeNode) {
		if (!locationNode['children']) {
			locationNode['children'] = [];
		}
		locationNode.children.push(subNode);
	}

	/**
	 * Opens the entity object of the specified node inside the main view
	 * This method has no effect, if no displayable entity object is associated with the specified node.
	 * @param node
	 */
	openTreeNodeInEoView(node: TreeNode) {
		if (this.isNodeDisplayable(node)) {
			this.eoService.loadEO(node.data.boMetaId, node.data.boId)
				.pipe(takeUntil(this.unsubscribe$))
				.subscribe(eo => {
					this.$log.info('Selected EO, Entity:' + node.data.boMetaId + ' PK:' + node.data.boId);
					this.eoResultService.selectEo(eo);
				});
		}
	}

	/**
	 * Checks if the specified tree node can be opened as an entity object
	 * @param node
	 * @return true, if node can be opened as an entity object
	 */
	isNodeDisplayable(node: TreeNode) {
		return (node.data.static !== 'true');
	}

	/**
	 * Refreshes the explorer tree.
	 * Expanded node should still be expanded afterwards.
	 * This means, we can not just call initialize roots. Because the state of all nodes will be lost.
	 * The refresh process should be cancelable, which means it can not work directly on the model.
	 */
	refreshTree() {
		let tree = this.primeTreeData.value;
		// do not refresh if tree/rootNode is undefined
		if (!tree || !tree[0]) {
			return;
		}
		let rootNode = tree[0];

		// do not refresh, if another refresh is already in progress
		if (this.refreshInProgress) {
			return;
		}
		this.refreshInProgress = true;

		this.refreshNode(rootNode)
			.pipe(takeUntil(this.cancelRefresh$))
			.subscribe(refreshedNode => {
				if (refreshedNode) {
					this.primeTreeData.next([refreshedNode]);
				} else {
					this.primeTreeData.next(undefined);
				}
				this.refreshInProgress = false;
			}, error => {
				this.refreshInProgress = false;
			});

	}

	cancelRefresh() {
		this.cancelRefresh$.next(true);
		this.refreshInProgress = false;
	}

	refreshNode(originalTreeNode: TreeNode): Observable<TreeNode> {
		return new Observable<TreeNode>(observer => {
			// static nodes always stay the same, so only update the other ones
			if (originalTreeNode.data.static !== 'true') {
				this.dataService.loadTreeRootData(originalTreeNode.data.boMetaId, originalTreeNode.data.boId)
					.pipe(takeUntil(this.cancelRefresh$))
					.subscribe(updatedNodeData => {
						if (updatedNodeData) {
							let updatedNode = this.createPrimeNode(updatedNodeData);
							this.refreshChildren(originalTreeNode, updatedNode)
								.pipe(takeUntil(this.cancelRefresh$))
								.subscribe(newSubnodes => {
									this.restorePrimeNodeStatus(originalTreeNode, updatedNode);
									observer.next(updatedNode);
									observer.complete();
								});
						} else {
							// we have received no data for the node, which means it was removed
							observer.next(undefined);
							observer.complete();
						}
					});
			}
		});
	}

	restorePrimeNodeStatus(oldNode: TreeNode, newNode: TreeNode) {
		if (oldNode.expanded && newNode.children && newNode.children.length > 0) {
			newNode.expanded = true;
		} else {
			newNode.expanded = false;
		}
	}

	refreshChildren(oldTreeNode: TreeNode, refreshedTreeNode: TreeNode): Observable<TreeNode[]> {
		return new Observable<TreeNode[]>(observer => {
			let oldChildren = oldTreeNode.children;
			if (oldChildren) {
				this.dataService.loadSubTreeData(refreshedTreeNode.data.node_id)
					.pipe(takeUntil(this.cancelRefresh$))
					.subscribe(result => {
						// collect all nodes, that existed before the refresh call, to refresh them recursively
						let refreshProcesses: Array<Observable<TreeNode[]>> = [];
						refreshedTreeNode.children = [];
						result.forEach(nodeData => {
							let primeSubNode = this.createPrimeNode(nodeData);
							this.insertSubNode(refreshedTreeNode, primeSubNode);
							if (oldChildren) {
								for (let i = 0; i < oldChildren.length; i++) {
									if (this.isDataFromEqualNode(oldChildren[i].data, nodeData)) {
										// we potentially have to refresh further down into the hierarchie
										refreshProcesses.push(this.refreshChildren(oldChildren[i], primeSubNode).pipe(takeUntil(this.unsubscribe$)));
										break;
									}
								}
							}
						});
						forkJoin(refreshProcesses)
							.pipe(defaultIfEmpty(undefined))
							.subscribe(next => {
								this.restorePrimeNodeStatus(oldTreeNode, refreshedTreeNode);
								observer.next(refreshedTreeNode.children);
								observer.complete();
							});
					});
			} else {
				// the parent node has not been expanded before, so we do not want to refresh the subnodes
				observer.next(undefined);
				observer.complete();
			}
		});
	}

	/**
	 * Checks, whether 2 nodes equal by its data.
	 * PrimeNg nodes can be different, because they are at different locations in the tree, but their data is the same.
	 * @param nodeData1
	 * @param nodeData2
	 */
	isDataFromEqualNode(nodeData1, nodeData2): boolean {
		let equal = (nodeData1['boMetaId'] === nodeData2['boMetaId']
				&& nodeData1['boId'] === nodeData2['boId'])
			&& nodeData1['relationId'] === nodeData2['relationId'];
		return equal;
	}

	expandAllTreeNodes() {
		if (this.primeTreeData.value) {
			// this.setExpandedRecursive(this.primeTreeData.value[0], true);
			this.expandRecursively(this.primeTreeData.value[0], []);
		}
	}

	collapseAllTreeNodes() {
		if (this.primeTreeData.value) {
			this.collapseRecursively(this.primeTreeData.value[0]);
		}
	}

	/**
	 * Gets called when the key is released, while a .p-treenode-content element is focussed.
	 * This method handles accessibility features for keyboard users.
	 * Hitting the <SPACE> key, will toggle the expansion status of a node.
	 * Hitting the <ENTER> key, will open the focussed tree node inside the entity object view.
	 * @param keyEvent - KeyboardEvent, that was fired on a treenode
	 * @param node - TreeNode, which was focussed, when the event fired
	 */
	onNodeKeyEvent(keyEvent: KeyboardEvent, node: TreeNode) {
		if (' ' === keyEvent.key) {
			node.expanded = !node.expanded;
		} else if ('Enter' === keyEvent.key) {
			this.openTreeNodeInEoView(node);
		}
	}

	private recursiveSearchMainViewNode(node: TreeNode): TreeNode | undefined {
		if (!this.selectedEo) {
			return undefined;
		}
		if (node.data.boMetaId === this.selectedEo.getEntityClassId() && node.data.boId === String(this.selectedEo.getId())) {
			return node;
		}
		let result: TreeNode | undefined = undefined;
		if (node.children) {
			for (let entry of node.children) {
				result = this.recursiveSearchMainViewNode(entry);
				if (result) {
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Sets @code{node.expanded = false} for the specified node and all of it's direct and indirect sub-nodes
	 * @param node
	 * @private
	 */
	private collapseRecursively(node: TreeNode) {
		if (node.children) {
			node.children.forEach(childNode => {
				this.collapseRecursively(childNode);
			});
		}
		node.expanded = false;
	}

	/**
	 * Checks if the node, is on the list of already expanded tree nodes.
	 * @param node
	 * @param alreadyExpanded
	 * @private
	 */
	private isNodeAlreadyExpandedInHierarchie(node: TreeNode, alreadyExpanded: Array<TreeNode>) {
		let alreadyExpandedNode = false;
		alreadyExpanded.forEach(entry => {
			if (this.isDataFromEqualNode(node.data, entry.data)) {
				alreadyExpandedNode = true;
			}
		});
		return alreadyExpandedNode;
	}

	private expandRecursively(node: TreeNode, alreadyExpanded: Array<TreeNode>) {
		// stop infinite trees from expanding infinitely
		if (this.isNodeAlreadyExpandedInHierarchie(node, alreadyExpanded)) {
			return;
		}
		node.expanded = true;
		alreadyExpanded.push(node);
		if (node.children) {
			node.children.forEach(childNode => {
				this.expandRecursively(childNode, alreadyExpanded);
			});
		} else if (!node.leaf) {
			// undefined children means, we have never attempted to load sub nodes
			this.dataService.loadSubTreeData(node.data.node_id)
				.pipe(takeUntil(this.unsubscribe$))
				.subscribe((result) => {
					this.assignSubNodes(node, result);
					node.children?.forEach(subNode => {
						this.expandRecursively(subNode, alreadyExpanded);
					});
				});
		}
	}

	private initializeToolbarButtons() {
		this.buttons = [
			{
				id: 'refreshTree',
				labelKey: 'webclient.button.refresh',
				buttonCssClass: 'btn btn-sm btn-success',
				iconCssClass: 'fa fa-fw fa-sync',
				action: () => this.refreshTree(),
				isVisible: eo => this.isShowRefreshButton(),
				isEnabled: () => !this.refreshInProgress
			},
			{
				id: 'cancelRefresh',
				labelKey: 'webclient.button.cancelRefresh',
				buttonCssClass: 'btn btn-sm btn-warning',
				iconCssClass: 'fa fa-fw fa-times',
				action: () => this.cancelRefresh(),
				isVisible: eo => !this.isShowRefreshButton(),
				isEnabled: () => this.refreshInProgress
			},
			{
				id: 'expandAll',
				labelKey: 'webclient.button.expandAll',
				buttonCssClass: 'btn btn-sm btn-success',
				iconCssClass: 'fa fa-fw fa-expand',
				action: () => this.expandAllTreeNodes(),
				isVisible: eo => this.isShowExpandAllButton(),
				isEnabled: () => true
			},
			{
				id: 'collapseAll',
				labelKey: 'webclient.button.collapseAll',
				buttonCssClass: 'btn btn-sm btn-success',
				iconCssClass: 'fa fa-fw fa-solid fa-compress',
				action: () => this.collapseAllTreeNodes(),
				isVisible: eo => this.isShowCollapseAllButton(),
				isEnabled: () => true
			},
		];
	}

	private isShowRefreshButton() {
		return !this.refreshInProgress;
	}

	private isShowExpandAllButton() {
		return true;
	}

	private isShowCollapseAllButton() {
		return true;
	}

	private loadRootNode() {
		if (this.loading) {
			return;
		}
		if (this.treeMetaId && this.treeId) {
			this.loading = true;
			this.dataService.loadTreeRootData(this.treeMetaId, this.treeId)
				.pipe(takeUntil(this.unsubscribe$))
				.subscribe(rootNode => {
					if (rootNode) {
						this.primeTreeData.next([this.createPrimeNode(rootNode)]);
					}
					this.loading = false;
					this.autoExpandIfEnabled();
				});
		}
	}

	private autoExpandIfEnabled() {
		if (this.treeMetaId) {
			this.metaService.getBoMeta(this.treeMetaId).subscribe(treeMeta => {
				if (treeMeta && treeMeta.isExpandTreeOnOpen()) {
					this.expandAllTreeNodes();
				}
			});
		}
	}

}
