import { Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
	selector: '[nucTreeKeyUp]',
})
export class TreeDirective {

	@Output('nucTreeKeyUp') keyUpEmitter = new EventEmitter();

	constructor(private el: ElementRef) {
	}

	@HostListener('window:keyup', ['$event'])
	onKeyUp(event) {
		if (event.target === this.getLevel3Parent()) {
			this.keyUpEmitter.emit(event);
		}
	}

	private getLevel3Parent() {
		return this.getParent(this.getParent(this.getParent(this.el.nativeElement)));
	}

	private getParent(element) {
		if (element.parentNode !== undefined) {
			return element.parentNode;
		}
		return undefined;
	}

}
