import { HttpParams } from '@angular/common/http';
import {
	Component,
	ElementRef,
	EventEmitter,
	HostListener,
	Input,
	NgZone,
	OnDestroy,
	OnInit,
	Output,
	ViewChild
} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavigationGuard } from '@app/guard/navigation-guard';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { EntityObjectRouteService } from '@modules/entity-object/entity-object-route.service';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { ColDef, ColumnEvent, GridOptions, IDatasource, RowNode } from 'ag-grid-community';
import { ResizeEvent } from 'angular-resizable-element';
import { EMPTY, Observable, of, Subject, Subscription } from 'rxjs';
import { catchError, debounceTime, filter, switchMap, take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../../data/classes/subscription-handle.directive';
import { ColumnLayoutChanges } from '../../../layout/shared/column-layout-changes';
import { Mixin } from '../../../shared/mixin';
import { BrowserRefreshService, VLP_ID_PARAM } from '../../../shared/service/browser-refresh.service';
import { FqnService } from '../../../shared/service/fqn.service';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { NuclosHotkeysService } from '../../../shared/service/nuclos-hotkeys.service';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectNavigationService } from '../../entity-object-data/shared/entity-object-navigation.service';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { MetaService } from '../../entity-object-data/shared/meta.service';
import { SortModel } from '../../entity-object-data/shared/sort.model';
import { EntityObjectGridColumn } from '../../entity-object-grid/entity-object-grid-column';
import { Logger } from '../../log/shared/logger';
import {
	ColumnAttribute,
	Preference,
	SearchtemplatePreferenceContent,
	SidebarLayoutType,
	SideviewColumnSort,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { SearchService } from '../../search/shared/search.service';
import { EntityObjectComponent, SidebarMode } from '../entity-object.component';
import { EntityObjectPreferenceService } from '../shared/entity-object-preference.service';
import { SideviewmenuService } from '../shared/sideviewmenu.service';
import { EntityObjectDatasource, EntityObjectTitleInfoDatasource } from './grid/entity-object-datasource';
import { SidebarCardLayoutViewItem, SidebarViewItem } from './view/sidebar-view.model';

@Component({
	selector: 'nuc-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.scss']
})
@Mixin([ColumnLayoutChanges])
export class SidebarComponent extends SubscriptionHandleDirective implements ColumnLayoutChanges, OnInit, OnDestroy {
	// the divider width or height for resizing
	public resizeWidth = 16;
	// this is a fixed size we need to adjust around for resizing the content with divider
	public headerToolbarSize = 80;

	@Input() searchtemplatePreference: Preference<SearchtemplatePreferenceContent>;
	@Input() multiSelectionDisabled = false;
	@Input() horizontalMode = false;
	@Input() sidebarMode: SidebarMode;
	@Input() collectiveProcessingAllowed: boolean;

	@Output() onMultiSelectionChange = new EventEmitter();

	@Output() onColumnChange: EventEmitter<Preference<SideviewmenuPreferenceContent>> =
		new EventEmitter<Preference<SideviewmenuPreferenceContent>>();

	sidebarLayoutType: SidebarLayoutType;
	selectedColumns: ColumnAttribute[] = [];
	gridColumns: EntityObjectGridColumn[] = [];
	gridColumnsEntityClassId: string;
	sortModel: SortModel;

	// TODO: Do not access GridOptions directly here
	gridOptions: GridOptions = <GridOptions>{
		rowModelType: 'infinite'
	};

	gridDatasource: IDatasource | undefined;

	displayAsListOnly = false;

	showMultiSelection = false;

	@ViewChild('listContainer') private listContainerElement: ElementRef;
	@ViewChild('sidebar', {static: true}) private sidebar: ElementRef;

	private _selectedEo: IEntityObject | undefined;

	private _meta: EntityMeta;

	private _sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>;

	private initialized = false;

	private lastKeyboardNavigationTime = 0;

	private subscriptions: Subscription = new Subscription();

	private ignoreDataReloads = true;

	private dataReloadSubject = new Subject<any>();

	private ignoreGridApiColumnChanges = false;

	/* mixins */
	// tslint:disable-next-line
	columnLayoutChanged: Subject<Date> = new Subject<Date>();
	// tslint:disable-next-line
	columnLayoutChangedSubscription: Subscription;

	private searchEditorVisibilityObserver: MutationObserver;

	/**
	 * TODO: Too many dependencies!
	 */
	constructor(
		private route: ActivatedRoute,
		private entityObjectComponent: EntityObjectComponent,
		private sideviewmenuService: SideviewmenuService,
		private entityObjectService: EntityObjectService,
		private eoNavigationService: EntityObjectNavigationService,
		private eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private eoEventService: EntityObjectEventService,
		private eoRouteService: EntityObjectRouteService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private searchService: SearchService,
		private nuclosConfigService: NuclosConfigService,
		private browserRefreshService: BrowserRefreshService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService,
		private fqnService: FqnService,
		private dataService: DataService,
		private activatedRoute: ActivatedRoute,
		private navigationGuard: NavigationGuard,
		private metaService: MetaService,
		private dialogService: NuclosDialogService,
		private ngZone: NgZone,
		private $log: Logger
	) {
		super();
		this.gridOptions.suppressPropertyNamesCheck = true;
	}

	@Input() set meta(meta: EntityMeta) {
		let ignoreDataReloadsOld = this.ignoreDataReloads;
		this.ignoreDataReloads = true;
		if (this.gridDatasource) {
			this.gridDatasource.destroy!();
			this.gridDatasource = undefined;
		}
		let entityClassIdOld = this._meta === undefined ? undefined : this._meta.getEntityClassId();
		this._meta = meta;

		if (meta && meta.getEntityClassId() !== entityClassIdOld) {
			// switching entity: check for already updated columns and ignored data reloads.
			// If this is the case, we start a data reload now!
			if (ignoreDataReloadsOld && this.gridColumnsEntityClassId === meta.getEntityClassId()) {
				this.ignoreDataReloads = false;
				this.initiateDataReload();
			}

			this.displayAsListOnly = meta.displayAsListOnly();
		}
	}

	@Input() set selectedEo(eo: IEntityObject | undefined) {
		this._selectedEo = eo;
	}

	get selectedEo(): IEntityObject | undefined {
		return this._selectedEo;
	}

	get meta(): EntityMeta {
		return this._meta;
	}

	get sideviewmenuPreference(): Preference<SideviewmenuPreferenceContent> {
		return this._sideviewmenuPreference;
	}

	@Input()
	set sideviewmenuPreference(sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>) {
		this._sideviewmenuPreference = sideviewmenuPreference;
		this.setSearchEditorAttributesContainerHeight();
	}

	get width() {
		return this.sidebar.nativeElement.style.width;
	}

	set width(width: string) {
		this.sidebar.nativeElement.style.width = width;
	}

	onColumnChangesDebounced(gridOptions: GridOptions): Observable<Date> {
		return {} as Observable<Date>;
	}

	getColDef(gridOptions: GridOptions, event: ColumnEvent): ColDef | undefined {
		return;
	}

	ngOnInit() {
		this.searchEditorVisibilityObserver = new MutationObserver(
			(mutations: MutationRecord[]) => {
				let relayoutWorkaround = false;
				for (let mutation of mutations) {
					let addedNodes: any = mutation.addedNodes;
					let addedCount = addedNodes.length;
					for (let i = 0; i < addedCount; i++) {
						if (
							this.isFirefox() &&
							addedNodes[i].attributes['id'] &&
							addedNodes[i].attributes['id'].value === 'search-editor-container'
						) {
							relayoutWorkaround = true;
						}
						if (
							addedNodes[i].attributes['id'] &&
							addedNodes[i].attributes['id'].value === 'sidebar-statusbar'
						) {
							relayoutWorkaround = true;
						}
					}
				}
				if (relayoutWorkaround) {
					this.relayoutWorkaround();
				}
			}
		);
		this.searchEditorVisibilityObserver.observe(this.sidebar.nativeElement, {
			childList: true
		});

		this.subscriptions.add(
			// use VLP restrictions (NUCLOS-5330)
			this.route.queryParams.subscribe((params: Params) => {
				let vlpId = params[VLP_ID_PARAM];
				if (vlpId) {
					this.eoResultService.vlpId = vlpId;
					let vlpParams = new HttpParams();
					for (const param of Object.keys(params)) {
						vlpParams = vlpParams.append(param, params[param]);
					}
					this.eoResultService.vlpParams = vlpParams;
				}
			})
		);

		this.subscriptions.add(
			this.entityObjectPreferenceService.selectedSideviewmenuPref$
				.pipe(filter(() => this.meta !== undefined))
				.subscribe(pref => {
					if (pref) {
						// FIXME hack to ignore preference change events from old view - avoid emitting them
						// this is caused by the 'redirect' /view/eoClassId --> /view/eoClassId/eoId
						let href = window.location.href + '/';
						if (
							href.indexOf(pref.boMetaId + '/') === -1 &&
							href.indexOf(pref.boMetaId + '?') === -1
						) {
							return;
						}
						let colCompare1 = this.selectedColumns
							.map(col => this.stringifyColumnAttribute(col)).join(', ');
						let colCompare2 = this.sideviewmenuService.getSelectedColumnsSorted(pref)
							.map(col => this.stringifyColumnAttribute(col)).join(', ');
						if (colCompare1 === colCompare2 &&
							this.sideviewmenuPreference &&
							this.sideviewmenuPreference.prefId === pref.prefId &&
							SideviewmenuPreferenceContent.equals(
								this.sideviewmenuPreference.content,
								pref.content
							)
						) {
							this.sideviewmenuPreference = pref;
							return;
						}
						this.sideviewmenuPreference = pref;

						// TODO: Handle missing column names.
						this.selectedColumns = this.selectedSortedColumns();

						this.$log.debug('Selected columns: %o', this.selectedColumns);

						this.initOrUpdateGridColumns(this.sideviewmenuPreference.boMetaId);
						this.initiateDataReload(this.sideviewmenuPreference.boMetaId);
					}
				})
		);

		this.subscriptions.add(
			this.sideviewmenuService.onSideviewmenuPrefChange().subscribe(() => {
				this.onColumnChange.emit(this.sideviewmenuPreference);
			})
		);

		this.subscriptions.add(
			this.sideviewmenuService
				.getViewType()
				.subscribe(type => (this.sidebarLayoutType = type))
		);

		this.subscriptions.add(
			this.eoResultService.observeResultListUpdate().subscribe(() => {
				if (this.initialized) {
					return;
				}
				this.initialized = true;

				// save column layout when column order, width or sort order was changed (debounced)
				if (this.sideviewmenuService.getViewType().getValue() !== 'card') {
					this.subscriptions.add(
						this.onColumnChangesDebounced(this.gridOptions).subscribe(() => {
							this.sideviewmenuPreference.content.columns = this.sideviewmenuService.getSelectedAttributesFromGrid(
								this.gridOptions,
								this.meta
							);

							this.saveSideviewmenuPreference();
						})
					);
				}

				// sort columns immediately - not debounced
				this.gridOptions.onSortChanged = () => {
					// ignore sort change events during business object switching
					if (this.ignoreGridApiColumnChanges ||
						this.ignoreDataReloads) {
						return;
					}

					// prevent saving a preference which was just deleted
					if (this.sideviewmenuPreference.prefId === undefined) {
						return;
					}

					// emit columnLayoutChanged event for debounced saving
					/*
				TODO
				this results in saving prefs all the time because onSortChange is not only called when user changes sort
				but also when grid is initialized:
				this.columnLayoutChanged.next(new Date());
				*/

					// update preference
					this.sideviewmenuPreference.content.columns.forEach(col => delete col.sort);
					this.gridOptions.columnApi?.getColumnState().forEach((sortModelColumn) => {
						let column = this.sideviewmenuPreference.content.columns
							.filter(co => co.boAttrId === sortModelColumn.colId)
							.shift();
						if (column) {
							column.sort = {
								direction: sortModelColumn.sort,
								enabled: true,
								prio: sortModelColumn.sortIndex
							};
						}
					});

					this.saveSideviewmenuPreference(); // TODO save debounced - see TODO above

					// load data again after sorting was changed
					this.initiateDataReload(this.sideviewmenuPreference.boMetaId);
				};
			})
		);

		// load data when search conditions were changed
		this.subscriptions.add(
			this.searchService.subscribeDataLoad().subscribe(o => {
				this.initiateDataReload(o.getEntityClassId());
			})
		);

		this.subscriptions.add(
			this.dataReloadSubject.pipe(debounceTime(500)).subscribe(() => setTimeout(() => this.reloadData()))
		);

		// needed for routing changes
		if (this.meta) {
			this.initiateDataReload(this.meta.getEntityClassId());
		}

		setTimeout(() => {
			this.registerShortCuts();
		});
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
		this.subscriptions.unsubscribe();
		this.searchEditorVisibilityObserver.disconnect();
	}

	initiateDataReload(entityClassId: string | undefined = undefined) {
		if (entityClassId && this.meta &&
			entityClassId !== this.meta.getEntityClassId()) {
			// sidebar is switching the bo, ignore until columns are (re-)load
			this.ignoreDataReloads = true;
		}
		if (!this.ignoreDataReloads) {
			this.ignoreGridApiColumnChanges = true;
			this.dataReloadSubject.next(true);
		}
	}

	isSearchEditorFixedAndShowing(): boolean {
		if (this._sideviewmenuPreference) {
			return (
				this._sideviewmenuPreference.content.sideviewSearchEditorFixed &&
				this._sideviewmenuPreference.content.sideviewSearchEditorFixedAndShowing
			);
		}
		return false;
	}

	rowClicked(params: { node: RowNode }) {
		if (this.getMeta()?.displayAsListOnly() === true) {
			let sidebarViewItem: SidebarViewItem = params.node.data;
			if (this.getMeta()?.openInNewTabListOnly()) {
				this.subscriptions.add(this.browserRefreshService.openEoInNewTab(
					this.getMeta()!.getBoMetaId()!,
					params.node.id,
					undefined,
					undefined,
					false,
					false,
					true,
					undefined,
					undefined,
					undefined,
					true,
					true
				).pipe(
					switchMap(() => sidebarViewItem?.entityObject.reload())
				).subscribe((eo: EntityObject) => {
					this.updateViewData(eo);
				}));
			} else {
				this.browserRefreshService.openEoInSameTab(
					this.getMeta()!.getBoMetaId()!,
					params.node.id
				);
			}
		} else {
			if (params.node.isSelected()) {
				if (!this.navigationGuard.canDeactivateSelectedEo(
					this.entityObjectComponent,
					params.node.id,
					true
				)) {
					return;
				}
			}
		}
	}

	/**
	 * is called when a row was selected
	 */
	rowSelected(params: { node: RowNode }) {
		if (this.getMeta()?.displayAsListOnly() !== true) {
			if (this.isMultiSelectingRows()) {
				this.showMultiSelection = true;
			} else {
				if (params.node.isSelected()) {
					// ignore deselection event
					try {
						if (!this.navigationGuard.canDeactivateSelectedEo(
							this.entityObjectComponent,
							params.node.id,
							false // rowSelected triggers already
						)) {
							params.node.setSelected(false);
							return;
						}
					} catch (error) {
						params.node.setSelected(false);
						throw error;
					}
					if (params.node.id !== undefined) {
						// TODO: If this is a dynamic task list, get the entity class from the EO data (attribute "ENTITY")
						/*this.selectEo(this.getMeta()!.getBoMetaId(), params.node.id).catch(() => {
							// If navigation fails, the selection was somehow prevented
							// -> Deselect the row again
							params.node.setSelected(false);
						});*/
						let entityClassId = this.getMeta()?.getBoMetaId();
						this.eoResultService.selectEoByClassAndId(entityClassId, params.node.id, false).pipe(
							catchError(e => {
								// If navigation fails, the selection was somehow prevented
								// -> Deselect the row again
								params.node.setSelected(false);
								return of();
							}),
							take(1)
						).subscribe();
					}
				}
			}
		}
	}

	isMultiSelectingRows(): boolean {
		if (this.gridOptions.api) {
			if (this.gridOptions.api.getSelectedRows().filter(row => row.id !== null).length > 1) {
				return true;
			}
		}
		return false;
	}

	gridReady() {
		// update changed item (from detail form) in list
		this.subscriptions.add(
			this.eoEventService
				.observeEoModification()
				.pipe(
					filter(
						eo =>
							eo.getEntityClassId() ===
							this.eoResultService.getSelectedEntityClassId()
					)
				)
				.subscribe((eo: EntityObject) => {
					// update data in sidebar
					this.updateViewData(eo);
				})
		);

		// update item reset (from detail form) in list
		this.subscriptions.add(
			this.eoEventService
				.observeResetEo()
				.pipe(
					filter(
						eo =>
							eo.getEntityClassId() ===
							this.eoResultService.getSelectedEntityClassId()
					)
				)
				.subscribe((eo: EntityObject) => {
					// resetting a new unsaved eo removes it from list
					if (eo.isNew()) {
						this.eoNavigationService.navigateToView(this.getMeta()!.getBoMetaId(), this.eoRouteService.processMetaId);
					}

					// update data in sidebar
					this.updateViewData(eo);
				})
		);

		this.subscriptions.add(
			this.eoEventService
				.observeSelectedEo()
				.pipe(
					filter(
						eo =>
							eo?.getEntityClassId() ===
							this.eoResultService.getSelectedEntityClassId()
					)
				)
				.subscribe((eo: EntityObject | undefined) => {
					if (eo) {
						this.markEoAsSelectedInGrid(eo.getId(), eo.navigated);
						this.updateViewData(eo);
					}
				})
		);

		this.subscribeSavedEo();

		this.subscriptions.add(
			this.eoEventService
				.observeDeletedEo()
				.pipe(
					filter(
						eo =>
							eo.getEntityClassId() ===
							this.eoResultService.getSelectedEntityClassId()
					)
				)
				.subscribe(eo => {
					this.initiateDataReload();  // Warum hier reload? Behandlung über UpdateViewData wäre besser
					if (eo.getId() === this.eoResultService.getSelectedEo()?.getId()) {
						this.eoResultService.selectEo(undefined);
					}
				})
		);

		this.subscriptions.add(
			this.eoEventService
				.observeColorChange()
				.pipe(
					filter(
						eo =>
							eo.getEntityClassId() ===
							this.eoResultService.getSelectedEntityClassId()
					)
				)
				.subscribe((eo: EntityObject) => {
					// update data in sidebar
					this.updateViewData(eo);
				})
		);

		this.subscriptions.add(
			this.eoEventService
				.observeReloadEo()
				.pipe(
					filter(
						eo =>
							eo.getEntityClassId() ===
							this.eoResultService.getSelectedEntityClassId()
					)
				)
				.subscribe((eo: EntityObject) => {
					this.$log.debug('Got reloaded eo event: %o', eo);
					// update data in sidebar
					this.updateViewData(eo);
				})
		);

		this.subscriptions.add(
			this.eoEventService
				.observeAddEo()
				.pipe(
					filter(
						eo =>
							eo.getEntityClassId() ===
							this.eoResultService.getSelectedEntityClassId()
					)
				)
				.subscribe(() => {
					if (this.gridOptions.api) {
						this.gridOptions.api.deselectAll();
						this.gridOptions.api.refreshInfiniteCache();
					}
				})
		);
	}

	getMeta() {
		return this.eoResultService.getSelectedMeta();
	}

	selectedSortedColumns(): ColumnAttribute[] {
		return this.sideviewmenuService.getSelectedColumnsSorted(this.sideviewmenuPreference);
	}

	onResize(event: ResizeEvent): void {
		if (this.sidebarIsResizableState()) {
			if (!this.horizontalMode) {
				this.sideviewmenuPreference.content.sideviewMenuWidth =
					this.calculateResizeDestination(event.rectangle.width!, window.innerWidth);
			} else {
				this.sideviewmenuPreference.content.sideviewMenuHeight =
					this.calculateResizeDestination(event.rectangle.height!, window.innerHeight - this.headerToolbarSize);
			}
			$(document.body).addClass('disable-text-selection');
		}
	}

	onResizeEnd(event: ResizeEvent): void {
		if (this.sidebarIsResizableState()) {
			if (!this.horizontalMode) {
				this.sideviewmenuPreference.content.sideviewMenuWidth =
					this.calculateResizeDestination(event.rectangle.width!, window.innerWidth);
			} else {
				this.sideviewmenuPreference.content.sideviewMenuHeight =
					this.calculateResizeDestination(event.rectangle.height!, window.innerHeight - this.headerToolbarSize);
			}
			this.sidebarWidthChanged();
			setTimeout(() => {
				$(document.body).removeClass('disable-text-selection');
				this.fixRowBackgroundColor();
			});
		}
	}

	sidebarIsResizableState(): boolean {
		return !(this.sideviewmenuPreference.content.sideviewState !== undefined &&
			this.sideviewmenuPreference.content.sideviewState !== 0);
	}

	getSearchContainerHeight(): number {
		return this.getSearchAttributesContainerHeight() + this.getSearchConstantsHeight();
	}

	getSearchAttributesContainerHeight(): number {
		let result = this.isSearchEditorFixedAndShowing() ? 154 : 0;
		if (
			this.sideviewmenuPreference &&
			this.sideviewmenuPreference.content.sideviewSearchAttributesContainerHeight
		) {
			result = this.sideviewmenuPreference.content.sideviewSearchAttributesContainerHeight;
		}

		// set max height so it does not overflow
		let maxHeight = this.horizontalMode ? this.getCurrentHeight() / 2 : window.innerHeight - 182;
		result = result > maxHeight ? maxHeight : result;
		result = result < 40 ? 0 : result;

		if (result === 0 && this.sideviewmenuPreference &&
			this.sideviewmenuPreference?.content?.sideviewSearchEditorFixed === true) {
			// change to popover search editor
			setTimeout(() => {
				this.sideviewmenuPreference.content.sideviewSearchEditorFixed = false;
				this.sideviewmenuPreference.content.searchEditorPopoverShowing = true;
				this.saveSideviewmenuPreference();
			});
		}
		return result;
	}

	getSearchConstantsHeight(): number {
		if (this.isSearchEditorFixedAndShowing()) {
			return (
				$('#search-editor-toolbar').height() + $('#search-editor-resize-handle').height()
			);
		} else {
			return 0;
		}
	}

	onResizeSearch(event: ResizeEvent): void {
		if (event.rectangle.height) {
			this.sideviewmenuPreference.content.sideviewSearchAttributesContainerHeight =
				event.rectangle.height - this.getSearchConstantsHeight();
		}
	}

	onResizeEndSearch(event: ResizeEvent): void {
		if (event.rectangle.height) {
			this.sideviewmenuPreference.content.sideviewSearchAttributesContainerHeight =
				event.rectangle.height - this.getSearchConstantsHeight();
		}
		this.saveSideviewmenuPreference();
		if (this.isFirefox()) {
			this.relayoutWorkaround();
		}
	}

	getCurrentWidth(): number {
		if (this.sideviewmenuPreference) {
			let width = this.sideviewmenuPreference.content.sideviewMenuWidth ?? this.resizeWidth;
			if (width > window.innerWidth) {
				width = window.innerWidth;
			} else if (width < 16) {
				width = 16;
			}
			if (this.sideviewmenuPreference.content.sideviewState !== undefined) {
				if (this.sideviewmenuPreference.content.sideviewState === -1) {
					return this.resizeWidth;
				} else if (this.sideviewmenuPreference.content.sideviewState === 1) {
					return window.innerWidth;
				}
			}

			return width;
		}

		return this.resizeWidth;
	}

	getCurrentHeight(): number {
		if (this.sideviewmenuPreference) {
			// in height we need to calculate status bar into
			let height = this.sideviewmenuPreference.content.sideviewMenuHeight ?? (this.resizeWidth + 222);
			if (height > window.innerHeight) {
				height = window.innerHeight - this.headerToolbarSize;
			}
			if (this.sideviewmenuPreference.content.sideviewState !== undefined) {
				if (this.sideviewmenuPreference.content.sideviewState === -1) {
					return this.resizeWidth;
				} else if (this.sideviewmenuPreference.content.sideviewState === 1) {
					return window.innerHeight - this.headerToolbarSize - 30;
				}
			}

			return height;
		}

		return this.resizeWidth;
	}

	getVisualizationStyle(offset = 0): any {
		if (this.displayAsListOnly) {
			return {width: `${window.innerWidth}px`};
		}

		let size = this.horizontalMode ? (this.getCurrentHeight() - offset) : (this.getCurrentWidth() - offset);
		if (this.horizontalMode) {
			return {height: size + 'px', width: '100%', display: size === 0 ? 'none' : 'flex'};
		} else {
			return {width: size + 'px', height: '100%', display: size === 0 ? 'none' : 'flex'};
		}
	}

	resizeLeftMax() {
		this.sideviewmenuPreference.content.sideviewState = this.sideviewmenuPreference.content.sideviewState === 1 ? 0 : -1;
		this.saveSideviewmenuPreference();
	}

	resizeRightMax() {
		this.sideviewmenuPreference.content.sideviewState = this.sideviewmenuPreference.content.sideviewState === -1 ? 0 : 1;
		this.saveSideviewmenuPreference();
	}

	sidebarWidthChanged(): void {
		this.saveSideviewmenuPreference();
	}

	@HostListener('window:keydown.ArrowDown', ['$event'])
	@HostListener('window:keydown.ArrowUp', ['$event'])
	handleKeyboardEvent(event) {
		// don't navigate if focus is in vlp
		if (
			$(event.target).closest('p-autocomplete').length !== 0 ||
			$(event.target).closest('select').length !== 0 ||
			$(event.target).closest('nuc-web-component').length !== 0
		) {
			return;
		}
		// don't navigate if a dialog is being displayed (NUCLOS-10180)
		if (this.dialogService.hasOpenDialog()) {
			return;
		}

		event.stopPropagation();
		event.preventDefault();

		let selectedEo = this.eoResultService.getSelectedEo();
		if (selectedEo) {
			// TODO check object identity - indexOf should work
			// let boIndex = this.bos.indexOf(this.selectedBo);
			let entityObjects = this.eoResultService.getResults();
			let boIndex = entityObjects.map(eo => eo.getId()).indexOf(selectedEo.getId());
			let boId = selectedEo.getId();
			if (boId && boIndex !== -1) {
				let nextEO: IEntityObject | undefined = undefined;
				if (event.key === 'ArrowUp') {
					if (boIndex > 0) {
						nextEO = entityObjects[boIndex - 1];
					}
				} else if (event.key === 'ArrowDown') {
					if (boIndex + 1 < entityObjects.length) {
						nextEO = entityObjects[boIndex + 1];
					}
				}

				if (nextEO) {
					this.gridOptions!.api!.forEachNode(rowNode => {
						if (rowNode.id!.toString() === nextEO!.getId()!.toString()) {
							rowNode.setSelected(true);
							rowNode.selectThisNode(true);
							this.lastKeyboardNavigationTime = new Date().getTime();
						}
						if (rowNode.id!.toString() === boId!.toString()) {
							rowNode.setSelected(false);
							this.gridOptions.api?.refreshCells({force: true, rowNodes: [rowNode] });
							this.lastKeyboardNavigationTime = new Date().getTime();
						}
					});
				}
			}
		}
	}

	selectEo(entityClassId: string, eoId: string) {
		return this.eoNavigationService.navigateToEoById(entityClassId, eoId);
	}

	scrollLeft() {
		$(this.listContainerElement.nativeElement).animate(
			{
				scrollLeft: 0
			},
			500
		);
	}

	closeGridOverlayAndShowNoticeIfNeeded() {
		if (this.gridOptions.api) {
			this.gridOptions.api.hideOverlay();

			if (this.eoResultService.getResults().length === 0) {
				this.gridOptions.api.showNoRowsOverlay();
			}
		}
	}

	autoSizeColumnsForCardLayout() {
		if (this.sidebarLayoutType === 'card') {
			this.gridOptions!.columnApi!.autoSizeAllColumns();
		}
	}

	getSidebarWidth() {
		return this.horizontalMode ? window.innerWidth : this.sideviewmenuPreference.content.sideviewMenuWidth;
	}

	protected getGridColumns(): EntityObjectGridColumn[] {
		let columns: EntityObjectGridColumn[] = [];
		let cellStyle = {'border-right': '1px dotted #888888'};

		if (this.sidebarLayoutType === 'card') {
			// layout type 'card'
			// TODO cellrenderer
			let titleColDef = {
				headerName: 'Title',
				field: 'title',
				cellStyle: cellStyle
			} as EntityObjectGridColumn;
			columns.push(titleColDef);

			let infoColDef = {
				headerName: 'Info',
				field: 'info',
				cellStyle: cellStyle
			} as EntityObjectGridColumn;
			columns.push(infoColDef);
		} else {
			// layout type 'table'
			columns = this.selectedColumns.map(col => {
				// TODO: Use EntityObjectGridService to create column definition
				let column = {
					headerName: col.name,
					// headerTooltip: col.description, // TooltipModule is used instead
					colId: col.boAttrId, // NUCLOS-8732
					field: col.boAttrId, // FQN
					width: col.width,
					cellStyle: cellStyle,
					pinned: col.fixed,
					lockPinned: true
				} as EntityObjectGridColumn;
				if (this.meta) {
					let attributeMeta = this.meta.getAttributeMetaByFqn(col.boAttrId);
					if (!attributeMeta) {
						// NUCLOS-8801 4) Something went wrong with meta. It's the wrong bo. Connection to NULOCS-8742?
						let bo = col.boAttrId.substr(0, col.boAttrId.lastIndexOf('_'));
						if (bo !== '') {
							this.metaService.getBoMeta(bo)
								.pipe(
									catchError(() => {
										return EMPTY;
									})
								).subscribe(meta => {
								column.attributeMeta = meta.getAttributeMetaByFqn(col.boAttrId);
								if (column.attributeMeta) {
									column.cellEditorParams = {
										attrMeta: column.attributeMeta
									};
								}
							});
						}
					} else {
						column.attributeMeta = attributeMeta;
						if (attributeMeta) {
							column.cellEditorParams = {
								attrMeta: attributeMeta
							};
						}
					}
				}

				return column;
			}).filter(
				col => col.attributeMeta !== undefined
			);
		}
		return columns;
	}

	private calculateResizeDestination(resizeValue: number, resizeMaximum: number): number {
		let resizeDest = resizeValue;
		if (resizeDest > (resizeMaximum - this.resizeWidth)) {
			resizeDest = (resizeMaximum - this.resizeWidth);
		} else if (resizeDest <= this.resizeWidth) {
			resizeDest = this.resizeWidth;
		}

		return resizeDest;
	}

	private reloadData() {
		this.$log.debug('Reloading data for Sidebar...');
		if (this.gridOptions.api) {
			this.eoResultService.resetResults();
			this.gridOptions.api.showLoadingOverlay();
			if (this.sidebarLayoutType === 'card') {
				// layout type 'card'
				let ds = new EntityObjectTitleInfoDatasource(
					this.eoResultUpdateService,
					this.eoResultService,
					this.nuclosConfigService,
					this.eoEventService,
					this.i18n,
					this.fqnService,
					this.dataService,
				);

					this.subscriptions.add(
						ds.observeLoadedData().subscribe(() => {
							this.markCurrentEoAsSelectedInGrid();
							this.closeGridOverlayAndShowNoticeIfNeeded();
							this.resetMultiSelection();
						})
					);

					this.gridOptions.api.setDatasource(
						ds
					);
					this.gridDatasource = ds;
			} else {
				// layout type 'table'
				if (this.meta.getEntityClassId() === this.eoResultService.getSelectedMeta()?.getEntityClassId()) {
					let ds = new EntityObjectDatasource(
						this.eoResultUpdateService,
						this.eoResultService,
						this.nuclosConfigService,
						this.eoEventService,
						this.i18n
					);

					this.subscriptions.add(
						ds.observeLoadedData().subscribe(() => {
							this.markCurrentEoAsSelectedInGrid();
							this.closeGridOverlayAndShowNoticeIfNeeded();
							this.resetMultiSelection();
						})
					);

					this.gridOptions.api.setDatasource(
						ds
					);
					this.gridDatasource = ds;
				}
			}
		} else if (this.sidebarMode === 'sidebar') {
			// If grid is not ready yet, try again
			this.ignoreDataReloads = false;
			this.initiateDataReload();
		}
		this.ignoreGridApiColumnChanges = false;
	}

	private resetMultiSelection() {
		let storageMultiSelection = this.eoResultService.retrieveMultiSelectionResultFromStorage();
		if (storageMultiSelection.hasSelection()) {
			setTimeout(() => {
				if (this.gridOptions.api) {
					this.showMultiSelection = true;
					this.onMultiSelectionChange.emit(storageMultiSelection);
				} else {
					this.resetMultiSelection();
				}
			});
		}
	}

	private setSearchEditorAttributesContainerHeight(): void {
		$('search-editor-attributes-container').css(
			'height',
			this.getSearchAttributesContainerHeight() + 'px'
		);
	}

	/**
	 * init or update grid columns
	 */
	private initOrUpdateGridColumns(entityClassId: string): void {
		this.gridColumns = this.getGridColumns();
		this.gridColumnsEntityClassId = entityClassId;
		this.sortModel = new SortModel(
			this.sideviewmenuService.getAgGridSortModel(
				this.selectedColumns.filter(col => col.sort && col.sort.direction),
				this._sideviewmenuPreference
			)
		);

		if (entityClassId && this.meta &&
			entityClassId === this.meta.getEntityClassId()) {
			// from now on, further initializations may be allowed
			this.ignoreDataReloads = false;
		}
	}

	private subscribeSavedEo() {
		// update sidebar after saving the eo to remove dirty row color
		this.subscriptions.add(
			this.eoEventService.observeSavedEo()
				.pipe(
					filter(
						eo =>
							eo.getEntityClassId() ===
							this.eoResultService.getSelectedEntityClassId()
					)
				)
				.subscribe((eo: EntityObject) => {
					// update data in sidebar
					this.updateViewData(eo);

					// select new eo if it was created
					this.markEoAsSelectedInGrid(eo.getId());

					// if the first eo is created on an empty list it will not show up in grid for whatever reason - so refresh list after save
					if (this.gridOptions.api!.getRenderedNodes().length === 0) {
						this.initiateDataReload();
					}
				})
		);
	}

	/**
	 * mark row of selectedEo in grid
	 * @return {boolean} true if row was found
	 */
	private markCurrentEoAsSelectedInGrid(): boolean {
		let selectedEo = this.eoResultService.getSelectedEo();
		this.$log.debug('selected eo', selectedEo);
		if (selectedEo) {
			let selectedEoId = selectedEo.getId();
			return this.markEoAsSelectedInGrid(selectedEoId, selectedEo['navigated']);
		} else if (this.meta?.shouldRefreshOnSearch()) {
			this.eoResultService.selectFirstResult();
		}
		return false;
	}

	/**
	 * mark row as selected in grid
	 * @return {boolean} true if row was found
	 */
	private markEoAsSelectedInGrid(selectedEoId, navigated = false): boolean {
		if (this.gridOptions.api) {
			this.gridOptions.api.deselectAll();
			if (selectedEoId !== undefined) {
				let rowNode = this.gridOptions.api.getRowNode(selectedEoId);
				this.$log.debug('selected eo in rowNode %o', rowNode);
				if (rowNode) {
					rowNode.selectThisNode(true);
					rowNode.setSelected(true);
					return true;
				} else {
					// current selection is not in search result - select first index instead if configured
					if (this.meta?.shouldRefreshOnSearch() && !navigated) {
						this.eoResultService.selectFirstResult();
					}
				}
			}
		}
		return false;
	}

	private updateViewData(eo: EntityObject): void {
		this.gridOptions.api!.forEachNode(rowNode => {
			let eoId = eo.getId();
			if (rowNode.data) {
				rowNode.id = rowNode.data.getId();
			}

			if (
				(eoId && rowNode.id && rowNode.id.toString() === eoId.toString()) ||
				(eo.isNew() && rowNode.id === null)
			) {
				if (this.sidebarLayoutType === 'card') {
					rowNode.setData(
						new SidebarCardLayoutViewItem(
							eo,
							this.nuclosConfigService,
							this.i18n,
							this.fqnService,
							this.dataService
						).build()
					);
				} else {
					let newData = new SidebarViewItem(eo, this.nuclosConfigService, this.i18n).build();
					newData.selected = rowNode.data.selected;
					newData.unselected = rowNode.data.unselected;

					rowNode.setData(newData);
				}

				// update rowClass to display correct row color
				this.gridOptions.api!.redrawRows({rowNodes: [rowNode] });

				return;
			}
		});
	}

	private saveSideviewmenuPreference(): void {
		this.sideviewmenuService
			.saveSideviewmenuPreference(this.sideviewmenuPreference)
			.pipe(take(1))
			.subscribe(preferenceItem => {
				if (preferenceItem) {
					// preference was just created
					this.sideviewmenuPreference = preferenceItem;
				}
			});
	}

	private stringifyColumnAttribute(col: ColumnAttribute): String {
		if (col) {
			return col.boAttrId + ':' + col.width + this.stringifySideviewColumnSort(col.sort);
		}
		return '';
	}

	private stringifySideviewColumnSort(sort: SideviewColumnSort | undefined): String {
		if (sort) {
			return '[sort=' + sort.direction + ',' + sort.prio + ']';
		}
		return '';
	}

	/**
	 * FIXME: Does not seem to work anymore.
	 * TODO: Move to GridComponent.
	 *
	 * if columns in ag-grid don't use the complete width of the grid then there is a white gap on the right side
	 * which is not wanted if the row background color is anything but white
	 */
	private fixRowBackgroundColor() {
		// it would be desirable to implement this via CSS
		(<any>document['styleSheets'][0]).addRule(
			'.ag-row::after',
			'width: ' + $('#sidebar-grid ag-grid-angular').width() + 'px!important'
		);
	}

	/**
	 * Workaround for Firefox and wrong height calculation after showing the search editor
	 */
	private relayoutWorkaround() {
		let sidebarGrid = $('#sidebar-grid');
		sidebarGrid.css('flex', '');
		setTimeout(() => {
			sidebarGrid.css('flex', '1');
		});
	}

	/**
	 * TODO: Use some lib instead, e.g. https://www.npmjs.com/package/detect-browser
	 * TODO: Or at least move this method somewhere else, browser detection is not a job for the Sidebar.
	 */
	private isFirefox(): boolean {
		if (navigator.userAgent.indexOf('Firefox') !== -1) {
			return true;
		}
		return false;
	}

	private registerShortCuts() {
		this.nuclosHotkeysService.addShortcut({
			modifier: 'CTRL_ALT', keys: 'HOME', description:
				this.i18n.getI18n('webclient.shortcut.sidebar.SelectFirst')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			if (this.gridOptions.api) {
				if (this.eoResultService.getResults().length > 0) {
					this.gridOptions.api.ensureIndexVisible(0);
					const renderedNodes = this.gridOptions.api.getRenderedNodes();
					this.$log.debug('Rendered Nodes: %o', renderedNodes);
					if (renderedNodes.length > 0) {
						this.selectNodeIfIdIsVisible(renderedNodes[0], true);
					}
				}
			}
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'CTRL_ALT', keys: 'END', description:
				this.i18n.getI18n('webclient.shortcut.sidebar.SelectLast')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			if (this.gridOptions.api) {
				let total = this.gridOptions.api.getInfiniteRowCount() ?? 1;
				this.gridOptions.api.ensureIndexVisible(total - 1);
				const renderedNodes = this.gridOptions.api.getRenderedNodes();
				this.$log.debug('Rendered Nodes: %o', renderedNodes);
				if (renderedNodes.length > 0) {
					this.selectNodeIfIdIsVisible(renderedNodes[renderedNodes.length - 1], true);
				}
			}
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'CTRL_ALT', keys: 'UP', description:
				this.i18n.getI18n('webclient.shortcut.sidebar.SelectPrev')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
				// don't navigate if a dialog is being displayed (NUCLOS-10180)
				if (this.dialogService.hasOpenDialog())  {
					return;
				}
				if (this.gridOptions.api) {
					this.$log.debug('Rendered Nodes: %o', this.gridOptions.api.getRenderedNodes());
					if (this.gridOptions.api.getRenderedNodes().length > 1) {
						const selectedNode =
							this.gridOptions.api.getRenderedNodes().findIndex(v => this.compareGridNodeToSelectedEO(v));
						const nextNodeIndex = selectedNode - 1;
						const rowIndex = this.gridOptions.api.getRenderedNodes()[nextNodeIndex].rowIndex;
						if (nextNodeIndex >= 0 && rowIndex !== null) {
							this.gridOptions.api.ensureIndexVisible(rowIndex);
							this.selectNodeIfIdIsVisible(this.gridOptions.api.getRenderedNodes()[selectedNode], false);
							this.selectNodeIfIdIsVisible(this.gridOptions.api.getRenderedNodes()[nextNodeIndex], true);
						}
					}
				}
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'CTRL_ALT', keys: 'DOWN', description:
				this.i18n.getI18n('webclient.shortcut.sidebar.SelectNext')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
				// don't navigate if a dialog is being displayed (NUCLOS-10180)
				if (this.dialogService.hasOpenDialog())  {
					return;
				}
				if (this.gridOptions.api) {
					this.$log.debug('Rendered Nodes: %o', this.gridOptions.api.getRenderedNodes());
					if (this.gridOptions.api.getRenderedNodes().length > 1) {
						const selectedNode =
							this.gridOptions.api.getRenderedNodes().findIndex(v => this.compareGridNodeToSelectedEO(v));
						const nextNodeIndex = selectedNode + 1;
						const rowCount = this.gridOptions.api.getInfiniteRowCount() ?? 0;
						const rowIndex = this.gridOptions.api.getRenderedNodes()[nextNodeIndex].rowIndex;
						if (nextNodeIndex < rowCount && rowIndex !== null) {
							this.gridOptions.api.ensureIndexVisible(rowIndex);
							this.selectNodeIfIdIsVisible(this.gridOptions.api.getRenderedNodes()[selectedNode], false);
							this.selectNodeIfIdIsVisible(this.gridOptions.api.getRenderedNodes()[nextNodeIndex], true);
						}
					}
			}
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'CTRL_ALT', keys: 'LEFT', description:
				this.i18n.getI18n('webclient.shortcut.sidebar.resizeLeft')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.resizeLeftMax();
		});

		this.nuclosHotkeysService.addShortcut({
			modifier: 'CTRL_ALT', keys: 'RIGHT', description:
				this.i18n.getI18n('webclient.shortcut.sidebar.resizeRight')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.resizeRightMax();
		});
	}

	private selectNodeIfIdIsVisible(n: RowNode, selectValue: boolean) {
		let count = 0;
		if (n.id !== undefined) {
			if (this.gridOptions.api) {
				this.gridOptions.api.getSelectedNodes().forEach((node) => {
					if (node.isSelected()) {
						node.setSelected(false);
					}
				});
			}
			n.setSelected(selectValue);
		} else {
			setTimeout(() => {
				this.selectNodeIfIdIsVisible(n, selectValue);
			}, 1000);
		}
	}

	private compareGridNodeToSelectedEO(v: RowNode) {
		if (this.eoResultService.getSelectedEo()) {
			return v.data.getId() === this.eoResultService.getSelectedEo()!.getId();
		}
		return false;
	}
}
