import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { EntityMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { DataService } from '@modules/entity-object-data/shared/data.service';
import { of, Subject } from 'rxjs';
import { catchError, take } from 'rxjs/operators';
import { ExportBoListTemplateState } from '../statusbar.component';

@Component({
	selector: 'nuc-export-bo-list',
	templateUrl: './export-bo-list.component.html',
	styleUrls: ['./export-bo-list.component.css']
})
export class ExportBoListComponent implements OnInit, OnDestroy {

	@Input() dialogId: number | undefined;

	format: string;
	pageOrientationLandscape = false;
	isColumnScaled = false;
	exportLink: string | undefined;
	exporting = false;
	error_msg: string | undefined;

	@Input()
	meta: EntityMeta;
	@Input()
	useSearchText = true;

	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private state: ExportBoListTemplateState,
		private dataService: DataService,
		private dialogService: NuclosDialogService
	) {
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnInit() {
	}

	executeListExport() {
		this.exporting = true;
		this.dataService.exportBoList(
			this.state.boId,
			this.state.refAttrId,
			this.state.meta ? this.state.meta : this.meta,
			this.state.columnPreference,
			this.state.searchtemplatePreference,
			this.format,
			this.pageOrientationLandscape,
			this.isColumnScaled,
			this.useSearchText
		)
			.pipe(
				take(1),
				catchError((error) => {
					if (error['message']) {
						this.error_msg = error.message;
					} else {
						this.error_msg = error;
					}
					return of(undefined);
				})
			)
		.subscribe(
			exportLink => {
				this.exporting = false;
				this.exportLink = exportLink;
				if (exportLink) {
					this.download();
				}
			}
		);
	}

	close() {
		const modal = this.dialogService.getDialog(this.dialogId);
		if (modal) {
			modal.close('confirmed');
		}
	}

	download() {
		window.open(this.exportLink, '_blank');
	}

}
