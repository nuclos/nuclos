import {
	AfterContentInit,
	Component,
	Directive,
	EventEmitter,
	Injectable,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	TemplateRef,
	ViewChild
} from '@angular/core';
import { ExportBoListComponent } from '@modules/entity-object/sidebar/statusbar/export-bo-list/export-bo-list.component';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { UserAction } from '@nuclos/nuclos-addon-api';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { BehaviorSubject, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { NuclosDialogService } from '../../../../core/service/nuclos-dialog.service';
import { NuclosConfigService } from '../../../../shared/service/nuclos-config.service';
import { SystemParameter } from '../../../../shared/system-parameters';
import { AuthenticationService } from '../../../authentication';
import { EntityMeta, EntityMetaData } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectResultService } from '../../../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';
import {
	Preference,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from '../../../preferences/preferences.model';
import { SidebarComponent } from '../sidebar.component';

/**
 * contains reference to the modal template
 */
@Injectable()
export class ExportBoListTemplateState {
	templateRef: TemplateRef<any>;
	boId: number | undefined;
	refAttrId: string | undefined;
	meta: EntityMeta | undefined;
	columnPreference: Preference<SideviewmenuPreferenceContent>;
	searchtemplatePreference: Preference<SearchtemplatePreferenceContent>;
}

@Directive({
	selector: '[nucExportBoListTemplate]'
})
export class ExportBoListTemplateDirective {
	constructor(template: TemplateRef<any>, state: ExportBoListTemplateState) {
		state.templateRef = template;
	}
}

@Component({
	selector: 'nuc-statusbar',
	templateUrl: './statusbar.component.html',
	styleUrls: ['./statusbar.component.scss']
})
export class StatusbarComponent implements OnInit, OnChanges, OnDestroy, AfterContentInit {
	isEoModified = false;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	@ViewChild('totoalCount')
	totoalCount;

	@Input()
	meta: EntityMetaData;

	@Input()
	sideviewMenuWidth: number;

	totalEOCount: number;

	@Input()
	sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>;

	@Input()
	searchtemplatePreference: Preference<SearchtemplatePreferenceContent>;

	@Output()
	sideviewMenuWidthChange = new EventEmitter<number>();

	@Input() showCollectiveProcessingButton: boolean;

	@Input() private sidebar: SidebarComponent;

	private showPrintSearchResultListButton = false;
	private showMultiselectionButton = false;
	private subscriptions: Subscription = new Subscription();

	constructor(
		authenticationService: AuthenticationService,
		private entityObjectResultService: EntityObjectResultService,
		private entityObjectEventService: EntityObjectEventService,
		private modalService: NuclosDialogService,
		private exportBoListTemplateState: ExportBoListTemplateState,
		private config: NuclosConfigService,
		private eoResultService: EntityObjectResultService
	) {
		this.showPrintSearchResultListButton = authenticationService.isActionAllowed(
			UserAction.PrintSearchResultList
		);
	}

	ngOnInit() {
		this.subscriptions.add(
			this.entityObjectResultService.observeResultListUpdate().subscribe(() => {
				this.udateCount();
			})
		);
		this.subscriptions.add(
			this.entityObjectEventService.observeSavedEo().subscribe(() => {
				this.udateCount();
			})
		);
		this.subscriptions.add(
			this.config
				.getSystemParameters()
				.pipe(take(1))
				.subscribe(params => {
					this.showMultiselectionButton =
						this.showPrintSearchResultListButton
						|| this.showCollectiveProcessingButton
						|| params.is(SystemParameter.FUNCTION_BLOCK_DEV);
				})
		);
	}

	ngAfterContentInit() {
		this.subscriptions.add(
			this.entityObjectEventService.observeEoModification().subscribe(() => {
				setTimeout(() => {
					this.isEoModified = true;
				});
			})
		);
		this.subscriptions.add(
			this.entityObjectEventService.observeSavedEo().subscribe(() => {
				setTimeout(() => {
					this.isEoModified = false;
				});
			})
		);
		this.subscriptions.add(
			this.entityObjectEventService.observeResetEo().subscribe(() => {
				setTimeout(() => {
					this.isEoModified = false;
				});
			})
		);
	}

	showListExportButton() {
		let metad = this.entityObjectResultService.getSelectedMeta();
		return this.showPrintSearchResultListButton && metad && metad.canExport();
	}

	ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	ngOnChanges(): void {
	}

	openExportBoListModal() {
		this.exportBoListTemplateState.boId = undefined;
		this.exportBoListTemplateState.refAttrId = undefined;
		this.exportBoListTemplateState.meta = undefined;
		this.exportBoListTemplateState.columnPreference = this.sideviewmenuPreference;
		this.exportBoListTemplateState.searchtemplatePreference = this.searchtemplatePreference;
		this.modalService.custom(
			ExportBoListComponent,
			{
				'meta': this.meta
			}
		).pipe(
			take(1)
		).subscribe();
	}

	toggleMultiselection() {
		let selectedEo = (this.eoResultService.getSelectedEo() as EntityObject);
		if (selectedEo !== undefined && selectedEo.isDirty()) {
			selectedEo.triggerWarnChange();
			return;
		} else {
			this.sidebar.showMultiSelection = !this.sidebar.showMultiSelection;
		}
	}

	private udateCount() {
		let totalEOCount = this.entityObjectResultService.getTotalResultCount();
		if (totalEOCount !== undefined) {
			this.totalEOCount = totalEOCount;
			if (this.totoalCount) {
				$(this.totoalCount.nativeElement).text(this.totalEOCount);
			}
		}
	}
}
