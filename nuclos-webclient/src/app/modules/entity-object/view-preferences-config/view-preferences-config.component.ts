import { DOCUMENT } from '@angular/common';
import {
	Component,
	Directive,
	ElementRef,
	Inject,
	Injectable,
	Input,
	OnDestroy,
	OnInit,
	TemplateRef,
	ViewChild
} from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { IPreferenceFilter, UserAction } from '@nuclos/nuclos-addon-api';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { NuclosDialogService } from '../../../core/service/nuclos-dialog.service';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { FqnService } from '../../../shared/service/fqn.service';
import { AuthenticationService } from '../../authentication';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { SelectableService } from '../../entity-object-data/shared/selectable.service';
import {
	AttributeSelectionContent,
	ColumnAttribute,
	Preference,
	PreferenceType,
	SelectedAttribute,
	SidebarLayoutType,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { Selectable } from '../../two-side-multi-select/two-side-multi-select.model';
import { SideviewmenuService } from '../shared/sideviewmenu.service';

@Injectable()
export class PreferenceDialogState {

	/**
	 * ref containing the modal component
	 */
	templateRef: TemplateRef<any>;

	meta: EntityMeta;
}

@Component({
	selector: 'nuc-view-preferences-modal',
	templateUrl: './view-preferences-modal.component.html',
	styleUrls: ['./view-preferences-modal.component.css']
})
export class ViewPreferencesModalComponent implements OnInit, OnDestroy {

	@Input() dialogId: number | undefined;
	/*
	 injected via ViewPreferencesModalContainerComponent
	 */
	meta: EntityMeta;

	/* id  of the layout where the subform is configured */
	layoutId: string | undefined;

	/* list of attribute fqns */
	hiddenColumns: string[];

	selectedSideviewmenuPref$: BehaviorSubject<
		Preference<SideviewmenuPreferenceContent> | undefined
	> = new BehaviorSubject(undefined);

	showResetSortingButton = false;
	editSideviewmenu;
	sideviewmenuColumns: Selectable[] = [];
	selectedSideviewmenuPreference: Preference<SideviewmenuPreferenceContent> | undefined;
	sideviewmenuPreferences: Preference<SideviewmenuPreferenceContent>[] = [];

	viewtype: SidebarLayoutType | undefined;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	allowSidebarCustomization: boolean;

	@ViewChild('sideviewmenuSelector') sideviewmenuSelector: ElementRef;

	private unsubscribe$ = new Subject<boolean>();

	constructor(
		@Inject(DOCUMENT) private document,
		private state: PreferenceDialogState,
		private eoResultService: EntityObjectResultService,
		private preferencesService: PreferencesService,
		private sideviewmenuService: SideviewmenuService,
		private selectableService: SelectableService,
		private fqnService: FqnService,
		private authenticationService: AuthenticationService,
		private dialogService: NuclosDialogService,
		private i18n: NuclosI18nService
	) {}

	ngOnInit() {
		this.selectedSideviewmenuPref$.pipe(takeUntil(this.unsubscribe$)).subscribe(pref => {
			this.selectSideviewmenu(pref);
		});

		// load preferences for sideview menu and searchtemplate
		let filter: IPreferenceFilter = {
			boMetaId: this.meta.getBoMetaId(),
			type: [
				this.meta.isSubform() ? PreferenceType.subformTable.name : PreferenceType.table.name
			]
		};

		if (this.layoutId) {
			filter.layoutId = this.layoutId;
			filter.orLayoutIsNull = true;
		}

		this.preferencesService.getPreferences(filter).pipe(takeUntil(this.unsubscribe$)).subscribe(preferences => {
			/*
				 sidebar preferences
				 */

			// set table prefs
			this.sideviewmenuPreferences = preferences.filter(
				p =>
					p.type === PreferenceType.table.name ||
					p.type === PreferenceType.subformTable.name
			) as Preference<SideviewmenuPreferenceContent>[];
			this.sideviewmenuPreferences.sort((a, b) =>
				this.nameOf(a) > this.nameOf(b) ? 1 : (this.nameOf(a) < this.nameOf(b) ? -1 : 0));

			// in preferences only selected columns are stored (without selected flag)
			this.sideviewmenuPreferences.forEach(pref =>
				pref.content.columns.forEach(column => (column.selected = true))
			);

			if (this.sideviewmenuPreferences.length === 0) {
				let sidebarSelectedSideviewmenuPreference = this.selectedSideviewmenuPref$.getValue();
				if (
					sidebarSelectedSideviewmenuPreference &&
					sidebarSelectedSideviewmenuPreference.prefId === undefined
				) {
					// only one temporary pref exists - add it to option list
					this.selectedSideviewmenuPreference = JSON.parse(
						JSON.stringify(sidebarSelectedSideviewmenuPreference)
					) as Preference<SideviewmenuPreferenceContent>;
					this.sideviewmenuPreferences.push(this.selectedSideviewmenuPreference);
				}
			} else {
				// selected pref
				this.selectedSideviewmenuPreference = this.sideviewmenuPreferences
					.filter(p => p.selected)
					.shift();

				// if no pref is selected select the first pref in list
				if (
					!this.selectedSideviewmenuPreference &&
					this.sideviewmenuPreferences.length > 0
				) {
					this.selectedSideviewmenuPreference = this.sideviewmenuPreferences[0];
				}
			}

			// add attributes which are not already in pref
			this.sideviewmenuPreferences.forEach(pref =>
				this.addAvailableColumnsFromMeta(pref, this.meta)
			);

			// add meta data to columns
			this.sideviewmenuPreferences.forEach(pref =>
				this.selectableService.addMetaDataToColumns(pref, this.meta)
			);

			this.selectSideviewmenu(this.selectedSideviewmenuPreference);

			this.viewtype = this.sideviewmenuService.getViewType().getValue();
		});

		this.checkAllowCustomization();
	}

	ngOnDestroy(): void {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	setViewtype(viewtype: SidebarLayoutType): void {
		this.viewtype = viewtype;
		this.sideviewmenuService.setViewType(viewtype);
	}

	showViewTypeConfig(): boolean {
		// TODO not implemented for subforms
		return !this.meta.isSubform();
	}

	sideviewmenuColumnsChanged(dirty: boolean): void {
		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.dirty = dirty;
			// update position columns
			let pos = 0;
			this.sideviewmenuColumns
				.filter(col => col.selected)
				.forEach(col => (col as ColumnAttribute).position = pos++);
		}
		this.selectableService.updatePreferenceName(this.selectedSideviewmenuPreference);
	}

	close(): void {
		if (
			this.selectedSideviewmenuPreference !== undefined) {
			let sidebarSelectedSideviewmenuPreference = this.selectedSideviewmenuPref$.getValue();
			if (sidebarSelectedSideviewmenuPreference &&
				sidebarSelectedSideviewmenuPreference.prefId !== this.selectedSideviewmenuPreference.prefId) {
				this.selectedSideviewmenuPreference.selected = true;
				if (this.selectedSideviewmenuPreference.prefId) {
					this.preferencesService
						.selectPreference(this.selectedSideviewmenuPreference, this.layoutId)
						.pipe(take(1))
						.subscribe();
				}
				this.selectedSideviewmenuPref$.next(this.selectedSideviewmenuPreference);
			}
		}
		this.closeModal('confirmed');
	}

	save(): void {
		if (
			this.selectedSideviewmenuPreference &&
			this.selectedSideviewmenuPreference.content.columns.filter(c => c.selected).length === 0
		) {
			this.dialogService.alert({
				title: this.i18n.getI18n('webclient.error.title'),
				message: this.i18n.getI18n('webclient.preferences.dialog.columnsRequired')
			});
			return;
		}

		if (
			this.selectedSideviewmenuPreference &&
			this.selectedSideviewmenuPreference.content.columns.filter(c => c.selected && !c.fixed).length === 0
		) {
			this.dialogService.alert({
				title: this.i18n.getI18n('webclient.error.title'),
				message: this.i18n.getI18n('webclient.preferences.dialog.notAllColumnsPinned')
			});
			return;
		}

		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.content.columns.sort(
				((a, b) => {
						if (a.selected && !b.selected) {
							return -1;
						}
						if (!a.selected && b.selected) {
							return 1;
						}
						if (a.position === undefined || b.position === undefined) {
							return 0;
						}
						if (a.position < b.position) {
							return -1;
						}
						if (a.position > b.position) {
							return 1;
						}
						return 0;
					}
			));
			this.selectedSideviewmenuPreference.selected = true;

			if (
				this.selectedSideviewmenuPreference.prefId ||
				this.selectedSideviewmenuPreference.dirty
			) {
				this.selectedSideviewmenuPreference.layoutId = this.layoutId;
				this.selectableService.savePreference(
					this.selectedSideviewmenuPreference
				)
				.pipe(take(1))
				.subscribe((preferenceItem: Preference<SideviewmenuPreferenceContent>) => {
					this.sideviewmenuService.sideviewmenuPrefChanged();
					if (preferenceItem) {
						this.selectedSideviewmenuPref$.next(preferenceItem);
					}
					this.closeModal('confirmed');
				});
			} else {
				this.close();
			}
		}
	}

	selectSideviewmenu(
		sideviewmenuPreference: Preference<SideviewmenuPreferenceContent> | undefined
	): void {
		if (!sideviewmenuPreference) {
			return;
		}

		this.showResetSortingButton =
			sideviewmenuPreference.content.columns.filter(column => column.sort && column.sort.prio !== null)
				.length > 0;
		this.editSideviewmenu = sideviewmenuPreference.prefId === undefined;

		this.selectedSideviewmenuPreference = this.sideviewmenuPreferences
			.filter(p => p.prefId === sideviewmenuPreference.prefId)
			.shift();

		this.prepareForEditing();
	}

	checkAllowCustomization(): void {
		this.authenticationService.onAuthenticationDataAvailable().pipe(take(1)).subscribe(loginSuccessful => {
			this.allowSidebarCustomization =
				loginSuccessful &&
				this.authenticationService.isActionAllowed(
					UserAction.WorkspaceCustomizeEntityAndSubFormColumn
				);
		});
	}

	editSideviewmenuPref(): void {
		window.setTimeout(() => {
			this.editSideviewmenu = true;
			window.setTimeout(() => {
				// focus/select text input
				this.document.getElementById('sideviewmenu-name-input').select();
			});
		});
	}

	editSideviewmenuPrefDone(): void {
		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.layoutId = this.layoutId;
			this.selectableService
				.savePreference(this.selectedSideviewmenuPreference)
				.pipe(take(1))
				.subscribe(() => {
					this.editSideviewmenu = false;
				});
		}
	}

	showDeleteSideviewmenuPref(): boolean {
		return (
			this.selectedSideviewmenuPreference !== undefined &&
			!this.editSideviewmenu &&
			!this.selectedSideviewmenuPreference.shared &&
			(this.sideviewmenuPreferences.length > 1 ||
				(this.sideviewmenuPreferences.length === 1 &&
					this.sideviewmenuPreferences[0].prefId !== undefined))
		);
	}

	/**
	 * delete a not shared pref
	 */
	deleteSideviewmenuPref() {
		if (this.selectedSideviewmenuPreference) {
			this.deletePref(
				this.selectedSideviewmenuPreference,
				this.sideviewmenuPreferences
			).pipe(take(1)).subscribe(() => {
				delete this.selectedSideviewmenuPreference;
				if (this.sideviewmenuPreferences.length > 0) {
					this.selectedSideviewmenuPreference = this.sideviewmenuPreferences[0];
				}
				if (this.sideviewmenuPreferences.length === 0) {
					this.newSideviewmenuPref(false, true);
				} else {
					this.selectSideviewmenu(this.sideviewmenuPreferences[0]);
				}
			});
		}
	}

	/**
	 * discard user changes of sideviewmenu pref
	 * deletes customized pref
	 * reselect shared pref
	 */
	discardSideviewmenuPref() {
		if (this.selectedSideviewmenuPreference) {
			this.preferencesService
				.deleteCustomizedPreferenceItem(this.selectedSideviewmenuPreference)
				.pipe(take(1))
				.subscribe(() => {
					if (
						this.selectedSideviewmenuPreference &&
						this.selectedSideviewmenuPreference.prefId
					) {
						this.preferencesService
							.getPreference(this.selectedSideviewmenuPreference.prefId)
							.pipe(take(1))
							.subscribe((pref: Preference<SideviewmenuPreferenceContent>) => {
								if (pref) {
									let prefInList = this.sideviewmenuPreferences
										.filter(p => p.prefId === pref.prefId)
										.shift();
									if (prefInList) {
										prefInList.content = pref.content;
										prefInList.customized = pref.customized;
									}
									this.selectSideviewmenu(pref);
									this.selectedSideviewmenuPref$.next(pref);
								}
							});
					}
				});
		}
	}

	resetSorting() {
		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.content.columns.forEach(col => {
				delete col.sort;
			});
		}
		this.showResetSortingButton = false;
	}

	newSideviewmenuPref(editMode = true, selectDefaultColumns = false) {
		this.editSideviewmenu = editMode;

		let sideviewmenuPref;
		let selectedEo = this.eoResultService.getSelectedEo();
		if (this.meta.isSubform()) {
			if (selectedEo) {
				sideviewmenuPref = this.sideviewmenuService.emptySubformPreference(
					this.meta,
					selectedEo.getEntityClassId(),
					selectDefaultColumns
				);
			}
		} else {
			sideviewmenuPref = this.sideviewmenuService.emptySideviewmenuPreference(
				this.meta,
				selectDefaultColumns
			);
		}
		if (sideviewmenuPref) {
			this.sideviewmenuPreferences.push(sideviewmenuPref);
		}
		this.selectSideviewmenu(sideviewmenuPref);

		if (editMode) {
			window.setTimeout(() => {
				// focus text input
				this.document.getElementById('sideviewmenu-name-input').focus();
			});
		}
	}

	handleNameChange(preferenceItem: Preference<AttributeSelectionContent>) {
		preferenceItem.content.userdefinedName =
			preferenceItem.name !== undefined &&
			preferenceItem.name.length > 0 &&
			preferenceItem.name !== this.selectableService.getDefaultName(preferenceItem);
	}

	private nameOf(pref: Preference<AttributeSelectionContent>): string {
		if (pref.name === undefined) {
			return '';
		}
		return pref.name;
	}

	private prepareForEditing() {
		this.sideviewmenuColumnsChanged(false);

		// add attributes which are not already in pref
		this.sideviewmenuPreferences.forEach(p => {
			this.addAvailableColumnsFromMeta(p, this.meta);
			this.removeHiddenColumns(p);
		});
		if (this.selectedSideviewmenuPreference) {
			this.addAvailableColumnsFromMeta(
				this.selectedSideviewmenuPreference,
				this.meta
			);
			this.removeHiddenColumns(this.selectedSideviewmenuPreference);

			// add meta data to columns
			this.sideviewmenuPreferences.forEach(p =>
				this.selectableService.addMetaDataToColumns(p, this.meta)
			);

			this.sideviewmenuColumns = this.selectedSideviewmenuPreference.content.columns
				.filter(value => value.hidden === undefined || !value.hidden)
				.filter(value => !this.meta.getAttributeMetaByFqn(value.boAttrId) || !this.meta.getAttributeMetaByFqn(value.boAttrId)?.isHidden())
				.map(
					(column) => {
						// This comes directly from preferences and is zero based
						// we need to increase this so that save logic will work properly
						if (column.sort?.prio !== undefined && column.sort?.prio !== null) {
							column.sort.prio += 1;
						}
						return column;
					}
				) as Selectable[];
		}
	}

	private removeHiddenColumns(pref: Preference<AttributeSelectionContent>) {
		if (this.hiddenColumns) {
			pref.content.columns.forEach(col =>
				col.hidden = this.hiddenColumns.indexOf(col.boAttrId) !== -1);
		}
	}

	private addAvailableColumnsFromMeta(
		pref: Preference<AttributeSelectionContent>,
		metaData: EntityMeta
	): void {
		this.meta.getAttributes().forEach((_attributeMeta, attrKey) => {
			let entityAttrMeta = this.meta.getAttributeMeta(attrKey);
			if (entityAttrMeta) {
				let attributeKey = this.fqnService.getShortAttributeName(
					metaData.getBoMetaId(),
					entityAttrMeta.getAttributeID()
				);
				if (attributeKey) {
					let attribute = <SelectedAttribute>this.meta.getAttribute(attributeKey);
					if (attribute) {
						if (
							pref.content.columns &&
							pref.content.columns.filter(
								column => column.boAttrId === attribute.boAttrId
							).length === 0
						) {
							if (attribute.boAttrId !== metaData.getRefAttrId()) {
								// make a copy of the attribute object
								let attr: ColumnAttribute = Object.assign({}, attribute);

								if (attr && attr.boAttrId && metaData) {
									let fieldName = this.fqnService.getShortAttributeName(
										metaData.getBoMetaId(),
										attr.boAttrId
									);
									let attributeMeta = metaData.getAttributeMeta(fieldName);
									attr.sort = {
										enabled: attributeMeta && !attributeMeta.isCalculated()
									};
								}
								pref.content.columns.push(attr);
							}
						}
					}
				}
			}
		});
	}

	private deletePref(
		prefItem: Preference<AttributeSelectionContent>,
		prefList: Preference<AttributeSelectionContent>[]
	): Observable<boolean> {
		return new Observable<boolean>(observer => {
			let removeFromModel = () => {
				prefList.splice(prefList.indexOf(prefItem), 1);
				if (prefList.length > 0) {
					prefItem = prefList[0];
				}
			};
			if (prefItem.prefId) {
				this.preferencesService
					.findReferencingPreferences([prefItem])
					.toPromise()
					.then(prefs => {
						if (prefs?.length ?? 0 > 0) {
							this.dialogService.alert({
								title: this.i18n.getI18n(
									'webclient.preferences.dialog.error.referencingPreferences'
								),
								message: this.i18n.getI18n(
									'webclient.preferences.dialog.error.referencingPreferencesMessage',
									prefs?.map(p => p.name).join(', ')
								)
							}).pipe(
								take(1)
							).subscribe();
						} else {
							this.preferencesService.deletePreferenceItem(prefItem).pipe(take(1)).subscribe(() => {
								if (prefItem) {
									removeFromModel();
								}
								observer.next(true);
								observer.complete();
							});
						}
					});
			} else {
				removeFromModel();
				observer.next(true);
				observer.complete();
			}
		});
	}

	private closeModal(confirmed: string) {
		const modal = this.dialogService.getDialog(this.dialogId);
		if (modal) {
			modal.close(confirmed);
		}
	}
}

@Directive({
	selector: '[nucViewPreferencesModal]'
})
export class ViewPreferencesModalDirective {
	constructor(confirmTemplate: TemplateRef<any>, state: PreferenceDialogState) {
		state.templateRef = confirmTemplate;
	}
}

@Component({
	selector: 'nuc-view-preferences-config',
	styleUrls: ['./view-preferences-modal-container.component.css'],
	template: `
		<div *ngIf="mainbarButton" tooltip="{{'webclient.button.preferencesConfig' | i18n}}" show-delay="2000">
			<button
				class="btn btn-sm view-preferences-button-mainbar"
				(click)="showViewPreferencesModal()"
			>
				<i class="fa fa-columns"></i>
			</button>
		</div>
		<div *ngIf="!mainbarButton" tooltip="{{'webclient.button.preferencesConfig' | i18n}}" show-delay="2000">
			<button
					class="btn btn-sm view-preferences-button"
					(click)="showViewPreferencesModal()"
			>
				<i class="fa fa-columns"></i>
			</button>
		</div>
	`
})
export class ViewPreferencesModalContainerComponent {
	@Input() mainbarButton: boolean;

	@Input()
	meta: EntityMeta;

	@Input()
	layoutId: string;

	@Input()
	hiddenColumns: any[];

	@Input()
	selectedSideviewmenuPref$: BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined>;

	constructor(private modalService: NuclosDialogService) {
	}

	showViewPreferencesModal(): void {
		this.modalService.custom(ViewPreferencesModalComponent, {
			'selectedSideviewmenuPref$': this.selectedSideviewmenuPref$,
			'meta': this.meta,
			'layoutId': this.layoutId,
			'hiddenColumns': this.hiddenColumns
		}, {size: 'lg'}, document.activeElement)
			.pipe(
				take(1)
			).subscribe();
	}
}
