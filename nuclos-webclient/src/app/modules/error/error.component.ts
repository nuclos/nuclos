import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import { NuclosTitleService } from '../../shared/service/nuclos-title.service';
import { AuthenticationService } from '../authentication';

@Component({
	selector: 'nuc-error',
	templateUrl: './error.component.html',
	styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
	statusCode: number;
	errorTitle: string;
	errorText: string;

	constructor(
		private route: ActivatedRoute,
		private nuclosI18n: NuclosI18nService,
		private authService: AuthenticationService,
		private titleService: NuclosTitleService
	) {
		this.route.params.subscribe(params => {
			this.statusCode = +params['status'];
			this.errorTitle = this.getErrorTitle();
			this.errorText = this.getErrorText();

			if (this.statusCode === 503) {
				this.authService.destroySession();
			}
		});
	}

	ngOnInit() {
		this.setTitle();
	}

	private getErrorTitle() {
		let text = this.nuclosI18n.getI18n('webclient.error.' + this.statusCode + '.title');
		if (!text) {
			text = this.nuclosI18n.getI18n('webclient.error.unknown.title');
		}
		return text;
	}

	private getErrorText() {
		let text = this.nuclosI18n.getI18n('webclient.error.' + this.statusCode + '.text');
		if (!text) {
			text = this.nuclosI18n.getI18n('webclient.error.unknown.text');
		}
		return text;
	}

	private setTitle() {
		this.titleService.setTitle(
			this.nuclosI18n.getI18n('webclient.error.title')
			+ ((this.errorTitle && this.errorTitle !== '') ? (' | ' + this.errorTitle) : ''));
	}
}
