import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { I18nModule } from '../i18n/i18n.module';
import { ErrorComponent } from './error.component';
import { ErrorRoutesModule } from './error.routes';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		I18nModule,

		ErrorRoutesModule
	],
	declarations: [
		ErrorComponent
	]
})
export class ErrorModule {
}
