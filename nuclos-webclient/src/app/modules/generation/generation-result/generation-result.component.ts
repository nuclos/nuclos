import { Component, Input, OnInit } from '@angular/core';
import { MetaService } from '@modules/entity-object-data/shared/meta.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GenerationResult } from '../shared/generation';

@Component({
	selector: 'nuc-generation-result',
	templateUrl: './generation-result.component.html',
	styleUrls: ['./generation-result.component.css']
})
export class GenerationResultComponent implements OnInit {
	@Input() generationResult: GenerationResult;
	@Input() targetURL: string;

	protected metaName: string | undefined;

	constructor(
		private activeModal: NgbActiveModal,
		private metaService: MetaService
	) {
	}

	ngOnInit() {
		this.metaService.getBoMeta(this.generationResult.bo.boMetaId).subscribe(meta => {
			this.metaName = meta.getEntityName();
		});
	}

	close() {
		this.activeModal.dismiss();
	}
}
