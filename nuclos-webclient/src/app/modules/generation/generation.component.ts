import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { finalize, take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../data/classes/subscription-handle.directive';
import { NuclosHotkeysService } from '../../shared/service/nuclos-hotkeys.service';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { Logger } from '../log/shared/logger';
import { Generation } from './shared/generation';
import { NuclosGenerationService } from './shared/nuclos-generation.service';

@Component({
	selector: 'nuc-generation',
	templateUrl: './generation.component.html',
	styleUrls: ['./generation.component.css']
})
export class GenerationComponent extends SubscriptionHandleDirective implements OnInit {
	@Input() modal: boolean;

	// Reference dropdown button for shortcut interaction
	@ViewChild('generateButton', {static: true}) generateButtonRef: ElementRef;

	loading = false;

	generationListingsName: string | undefined = this.i18n.getI18n('webclient.generator');

	private _eo: EntityObject;

	constructor(
		private $log: Logger,
		private generationService: NuclosGenerationService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService
	) {
		super();

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT', keys: '2', description:
				this.i18n.getI18n('webclient.shortcut.detail.focusObjectGen')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.generateButtonRef.nativeElement.focus();
			this.generateButtonRef.nativeElement.click();
		});
	}

	@Input() set eo(eo: EntityObject) {
		this._eo = eo;
		this._eo.getMeta().subscribe(meta => {
				if (meta.getGenerationListingsName() !== undefined) {
					this.generationListingsName = meta.getGenerationListingsName();
				} else {
					this.generationListingsName = this.i18n.getI18n('webclient.generator');
				}
			}
		);
	}

	get eo() {
		return this._eo;
	}

	ngOnInit() {
	}

	generate(generation: Generation) {
		this.loading = true;
		this.generationService.confirmAndGenerate(
			this.eo,
			generation,
			undefined,
			this.modal,
			this.generateButtonRef.nativeElement
		).pipe(
			take(1),
			finalize(() => {
				this.loading = false;
				setTimeout(() => $(this.generateButtonRef.nativeElement)['dropdown']('hide'));
			})
		).subscribe();
	}
}
