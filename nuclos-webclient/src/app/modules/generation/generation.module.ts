import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EntityObjectGridComponent } from '@modules/entity-object-grid/entity-object-grid.component';
import { EntityObjectGridModule } from '@modules/entity-object-grid/entity-object-grid.module';
import { EntityObjectModule } from '@modules/entity-object/entity-object.module';
import { SearchModule } from '@modules/search/search.module';
import { SharedModule } from '../../shared/shared.module';
import { I18nModule } from '../i18n/i18n.module';
import { InputRequiredModule } from '../input-required/input-required.module';
import { GenerationResultComponent } from './generation-result/generation-result.component';
import { GenerationComponent } from './generation.component';
import { NuclosGenerationService } from './shared/nuclos-generation.service';
import { ParameterObjectComponent } from './parameter-object/parameter-object.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		SharedModule,
		I18nModule,
		InputRequiredModule,
		SearchModule,
		EntityObjectGridModule
	],
	declarations: [
		GenerationComponent,
		GenerationResultComponent,
		ParameterObjectComponent
	],
	providers: [
		NuclosGenerationService
	],
	exports: [
		GenerationComponent
	]
})
export class GenerationModule {
}
