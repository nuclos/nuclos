import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterObjectComponent } from './parameter-object.component';

describe('ParameterObjectComponent', () => {
	let component: ParameterObjectComponent;
	let fixture: ComponentFixture<ParameterObjectComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ParameterObjectComponent]
		})
			.compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ParameterObjectComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
