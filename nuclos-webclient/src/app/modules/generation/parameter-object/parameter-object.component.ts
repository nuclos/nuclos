import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { BoViewModel, EntityMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { DataService, LoadContext, LoadInnerContext } from '@modules/entity-object-data/shared/data.service';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { MetaService } from '@modules/entity-object-data/shared/meta.service';
import { ResultParams } from '@modules/entity-object-data/shared/result-params';
import { SortModel } from '@modules/entity-object-data/shared/sort.model';
import { EntityObjectGridColumn } from '@modules/entity-object-grid/entity-object-grid-column';
import { EntityObjectGridService } from '@modules/entity-object-grid/entity-object-grid.service';
import { SideviewmenuService } from '@modules/entity-object/shared/sideviewmenu.service';
import { Logger } from '@modules/log/shared/logger';
import {
	AttributeSelectionContent,
	ColumnAttribute,
	Preference,
	SideviewmenuPreferenceContent
} from '@modules/preferences/preferences.model';
import { GridOptions, RowNode } from 'ag-grid-community';
import { EMPTY, Observable, of, ReplaySubject } from 'rxjs';
import { catchError, mergeMap, take, tap } from 'rxjs/operators';

@Component({
	selector: 'nuc-parameter-object',
	templateUrl: './parameter-object.component.html',
	styleUrls: ['./parameter-object.component.css']
})
export class ParameterObjectComponent implements OnInit {

	@Input() paramEntity: string;
	@Input() vlpId?: string;
	@Input() sourceId?: number;

	_vlpName?: string;
	gridOptions: GridOptions = <GridOptions>{};
	gridColumns: EntityObjectGridColumn[] = [];
	sortModel: SortModel;
	entityMeta: EntityMeta;
	entityName?: string;
	parameterObject?: EntityObject;

	private bos: ReplaySubject<EntityObject[]> = new ReplaySubject<EntityObject[]>(1);

	constructor(
		private $log: Logger,
		private metaService: MetaService,
		private dialogService: NuclosDialogService,
		private dataService: DataService,
		private sideviewmenuService: SideviewmenuService,
		private eoGridService: EntityObjectGridService
	) {
	}

	@Input()
	set vlpName(vlpName: string) {
		if (vlpName) {
			this._vlpName = vlpName;
		} else {
			this._vlpName = '---';
		}
	}

	ngOnInit(): void {
		this.metaService.getBoMeta(this.paramEntity).pipe(
			take(1),
			tap((meta: EntityMeta) => {
				this.entityMeta = meta;
				this.entityName = meta.getEntityName();
			}),
			mergeMap((value: EntityMeta) => {
					return this.sideviewmenuService
						.loadSideviewmenuPreference(this.paramEntity)
						.pipe(take(1));
			}),
			mergeMap(pref => {
				if (!pref) {
					// Create default table preference if nothing else is available
					pref = this.sideviewmenuService.emptySideviewmenuPreference(
						this.entityMeta,
						true
					);
				}

				this.applyColumnAndSortingPreference(pref as Preference<SideviewmenuPreferenceContent>);

				return of(true);
			})
		).subscribe(
			() => this.loadParameterObjectData()
		);

		this.bos.subscribe((data: EntityObject[]) => {
			if (this.gridOptions && this.gridOptions.api) {
				this.gridOptions.api.setRowData(data);
			}
		});
	}

	confirm($event: MouseEvent) {
		this.dialogService.closeCurrentModal(this.parameterObject!.getId());
	}

	cancel($event: MouseEvent) {
		this.dialogService.closeCurrentModal();
	}

	rowSelected(params: { node: RowNode }) {
		if (params.node.isSelected()) {
			this.parameterObject = params.node.data;
		} else {
			if (this.parameterObject === params.node.data) {
				this.parameterObject = undefined;
			}
		}
	}

	gridReady() {
		this.bos.pipe(take(1)).subscribe((data: EntityObject[]) => this.gridOptions.api!.setRowData(data));
	}

	getBoMeta(): Observable<EntityMeta> {
		return this.metaService.getBoMeta(this.paramEntity).pipe(take(1));
	}

	private loadParameterObjectData() {
		this.getBoMeta().pipe(
			take(1),
			mergeMap((meta: EntityMeta) => {
				let vlpParams = new HttpParams();
				if (this.sourceId) {
					vlpParams = vlpParams.append('intid', this.sourceId);
				}

				let loadContext: LoadContext = {
					meta: meta,
					vlpId: this.vlpId,
					vlpParams: vlpParams
				};

				let innerContext: LoadInnerContext = {
					...loadContext,
					columns: undefined,
					sort: '',
					search: '',
					resultParams: ResultParams.DEFAULT
				};

				return this.dataService.loadEoList(loadContext, innerContext).pipe(
					take(1),
					catchError(e => {
						this.$log.warn(e);

						return EMPTY;
					})
				);
			})
		).subscribe((result: BoViewModel) => this.bos.next(result.bos));
	}

	private applyColumnAndSortingPreference(pref: Preference<AttributeSelectionContent>) {
		if (!pref) {
			return;
		}

		let columns = pref.content.columns;
		let sortedColumns = this.sideviewmenuService.sortColumns(columns);

		this.setColumns(sortedColumns);
		this.sortModel = new SortModel(this.sideviewmenuService.getAgGridSortModel(columns, pref));
	}

	private setColumns(columns: ColumnAttribute[]) {

		let newColumns = columns
			.filter(col => !col.boAttrId.toUpperCase().endsWith('NUCLOSROWCOLOR'))
			.map(col => {
				let attributeMeta;

				if (col.boAttrId) {
					attributeMeta = this.entityMeta!.getAttributeMetaByFqn(col.boAttrId);
				} else if (col.name) {
					// Try to get the attribute meta by name as a fallback
					attributeMeta = this.entityMeta!.getAttributeMeta(col.name);
				}

				if (!attributeMeta) {
					this.$log.warn('Could not find attribute meta for column %o', col);
					return {};
				}

				let column = this.eoGridService.createColumnDefinition(attributeMeta);

				column.width = col.width;

				return column;
			})
			.filter(col => col.colId);

		if (newColumns.length > 0) {
			this.gridColumns = newColumns;
		}
	}
}
