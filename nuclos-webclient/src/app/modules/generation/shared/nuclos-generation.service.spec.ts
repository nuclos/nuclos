/* tslint:disable:no-unused-variable */
import { inject, TestBed } from '@angular/core/testing';
import { NuclosGenerationService } from './nuclos-generation.service';

xdescribe('Service: NuclosGeneration', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [NuclosGenerationService]
		});
	});

	it('should ...', inject([NuclosGenerationService], (service: NuclosGenerationService) => {
		expect(service).toBeTruthy();
	}));
});
