import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EntityObjectEventService } from '@modules/entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { ParameterObjectComponent } from '@modules/generation/parameter-object/parameter-object.component';
import { RuleService } from '@modules/rule/shared/rule.service';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { FqnService } from '@shared/service/fqn.service';
import { LocalStorageService } from '@shared/service/local-storage.service';
import {
	BehaviorSubject, EMPTY,
	iif,
	Observable, of,
	of as observableOf,
	ReplaySubject,
	Subject,
	throwError as observableThrowError
} from 'rxjs';

import { catchError, finalize, map, mergeMap, retryWhen, switchMap, take, tap } from 'rxjs/operators';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { EntityObjectErrorService } from '../../entity-object-data/shared/entity-object-error.service';
import { EntityObject, SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { InputRequiredService } from '../../input-required/shared/input-required.service';
import { Logger } from '../../log/shared/logger';
import { GenerationResultComponent } from '../generation-result/generation-result.component';
import { Generation, GenerationResult } from './generation';
import { CommandService } from 'app/modules/command/shared/command.service';

@Injectable()
export class NuclosGenerationService {

	constructor(
		private http: HttpClient,
		private inputRequired: InputRequiredService,
		private eoErrorService: EntityObjectErrorService,
		private eoVLPService: EntityVLPService,
		private nuclosI18n: NuclosI18nService,
		private cmdService: CommandService,
		private dialog: NuclosDialogService,
		private ruleService: RuleService,
		private eoEventService: EntityObjectEventService,
		private $log: Logger,
		private localStorageService: LocalStorageService,
		private eoService: EntityObjectService,
		private fqnService: FqnService,
	) {
	}

	confirmAndGenerate(
		eo: EntityObject,
		generation: Generation,
		popupParameter?: string,
		modal?: boolean,
		sourceEvent?: EventTarget | null
	): Observable<any> {
		if (!generation) {
			return of(undefined);
		}

		return eo.getMeta().pipe(
			switchMap((meta) => {
				let title = this.nuclosI18n.getI18n(
					'webclient.dialog.generate.header',
					generation.target
				);

				let message = this.nuclosI18n.getI18n(
					'webclient.dialog.generate.message',
					meta.getEntityName(),
					generation.target
				);

				if (generation.nonstop) {
					this.$log.debug('Generate %o', generation);
					return this.generateEO(
						eo,
						generation.generationId,
						popupParameter,
						modal,
						sourceEvent
					);
				} else {
					return this.dialog.confirm({
						title: title,
						message: message
					}, sourceEvent).pipe(
						take(1),
						switchMap((result) => {
							if (!result) {
								this.$log.debug('Generation aborted');
								return of(undefined);
							} else {
								this.$log.debug('Generate %o', generation);
								return this.generateEO(
									eo,
									generation.generationId,
									popupParameter,
									modal,
									sourceEvent
								);
							}
						})
					);
				}
			})
		);
	}

	/**
	 * Executes the given generation.
	 * Possibly opens the result in the given target window (or else closes it).
	 *
	 * Saves the source EO first, if it is dirty.
	 */
	generateEO(
		sourceEo: EntityObject,
		generationId: string,
		popupparameter?: string,
		modal?: boolean,
		sourceEvent?: EventTarget | null
	): Observable<GenerationResult> {
		let eoSubject: BehaviorSubject<EntityObject | undefined> =
			new BehaviorSubject<EntityObject | undefined>(undefined);
		let generationSubject: BehaviorSubject<Generation | undefined> =
			new BehaviorSubject<Generation | undefined>(undefined);

		return observableOf(sourceEo).pipe(
			mergeMap(eo => eo.isDirty() ? eo.save() : observableOf(eo)),
			mergeMap((eo: EntityObject) => {
					let generation = eo.getGeneration(generationId);
					if (!generation) {
						return observableThrowError('Unknown generation: ' + generationId);
					}

					eoSubject.next(eo);
					generationSubject.next(generation);

					if (generation.parameterEntity) {
						return this.dialog.custom(
							ParameterObjectComponent,
							{
								paramEntity: generation.parameterEntity,
								vlpId: generation.parameterEntityVLPId,
								vlpName: generation.parameterEntityVLPName,
								sourceId: sourceEo.getId()
							},
							{
								size: 'xl'
							},
							sourceEvent
						);
					} else {
						return of(undefined);
					}
				}
			),
			mergeMap((parameterObjectId: number | undefined) => {
				if (generationSubject.getValue()?.parameterEntity && !parameterObjectId) {
					return EMPTY;
				}
				let postData = {
					parameterObjectId: parameterObjectId?.toString(),
					selection: this.eoService.buildSelectionMap(sourceEo.getAllDependents())
				};

				return this.http.post(generationSubject.getValue()!.links.generate.href, postData).pipe(
					tap((result: GenerationResult) => {
						if (!result.complete) {
							result.bo.businessError = result.businessError;
						}

						this.showResult(result, eoSubject.getValue(), popupparameter, sourceEvent).pipe(
							take(1)
						).subscribe();
					}),
					switchMap((result: GenerationResult) => this.refreshSourceIfNeeded(
						result,
						eoSubject.getValue(),
						modal === undefined || !modal
					)),
					retryWhen(
						// Retry until all InputRequired exceptions are handled
						errors => errors.pipe(mergeMap(
							error => this.inputRequired.handleError(error, postData, eoSubject.getValue())
						))
					),
					catchError(
						e => this.eoErrorService.handleError(e)
					),
				);
			}),
			tap((eoResult) => {
				if (eoResult.bo) {
					this.cmdService.executeCommands(new EntityObject(eoResult.bo, this.localStorageService, this.fqnService).getCommands());
				}
			})
		);
	}

	showResult(
		result: GenerationResult,
		sourceEo: IEntityObject | undefined,
		popupparameter?: string,
		sourceEvent?: EventTarget | null
	): Observable<any> {
		if (result.showGenerated || (!result.closeOnException && !result.complete)) {
			if (result.openInOverlay) {
				let eo = new EntityObject(result.bo, this.localStorageService, this.fqnService);
				this.eoVLPService.addEntityObjectMap(eo);
				return this.ruleService.updateRuleExecutor(eo as SubEntityObject).pipe(
					switchMap(() =>
						result.dialogMode ?
							this.dialog.openEoInDialog(
								eo,
								sourceEo,
								result.dialogTitle,
								result.dialogWidth,
								result.dialogHeight,
								sourceEvent
							) :
							this.dialog.openEoInModal(
								eo,
								sourceEo,
								true,
								sourceEvent
							)
					),
					take(1),
					tap(() => {
						if (eo.getData() !== undefined && eo.getData() !== null) {
							result.bo = eo.getData()!;
						}
					}),
					// After the modal is closed, layout rules may change again
					finalize(() => {
						if (eo instanceof SubEntityObject) {
							this.ruleService.updateRuleExecutor(eo as SubEntityObject)
								.pipe(
									take(1)
								);
						}
						this.refreshSourceIfNeeded(
							result,
							sourceEo,
							false
						).pipe(
							take(1)
						).subscribe();
					})
				);
			} else {
				let url = window.location.href;
				let eoLink = url.substring(0, url.indexOf('#'))
					+ (popupparameter ? '#/popup/' : '#/view/')
					+ result.bo.boMetaId
					+ '/' + (result.bo.boId || result.bo.temporaryId)
					+ '?refreshothertabs';

				let newWindow = window.open(eoLink, '_blank', popupparameter);
				if (!newWindow) {
					return this.showResultModal(result, eoLink);
				}
			}
		}
		return of(result);
	}

	private showResultModal(result: GenerationResult, eoLink: string) {
		return this.dialog.custom(
			GenerationResultComponent,
			{
				'generationResult': result,
				'targetURL': eoLink
			}
		).pipe(
			take(1),
			tap((res) => this.$log.debug('Result: %o', res))
		);
	}

	private refreshSourceIfNeeded(result: GenerationResult, source: IEntityObject | undefined, navigate: boolean):
		Observable<GenerationResult> {
		if (result.refreshSource) {
			return source?.reload().pipe(
				take(1),
				tap((reloadedEo) => {
					this.$log.debug('EO reloaded after generation %o', reloadedEo);
					this.eoEventService.emitReloadEo(reloadedEo);
					if (navigate) {
						reloadedEo.select();
					}
				}),
				map(() => result)
			) ?? of(result);
		}
		return of(result);
	}
}
