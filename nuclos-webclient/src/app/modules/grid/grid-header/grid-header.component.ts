import { Component } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { IHeaderAngularComp } from 'ag-grid-angular';
import { ColumnState, IHeaderParams } from 'ag-grid-community';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { EntityObjectGridColumn } from '../../entity-object-grid/entity-object-grid-column';

@Component({
	selector: 'nuc-grid-header',
	templateUrl: './grid-header.component.html',
	styleUrls: ['./grid-header.component.css']
})
export class GridHeaderComponent implements IHeaderAngularComp {
	params: IHeaderParams;
	menuOpacity = '0';
	tooltip: string | undefined;
	multiSorting = false;
	sortingAsc = false;
	sortingDesc = false;
	sortIndex = 0;

	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor() {
	}

	agInit(params: IHeaderParams): void {
		this.setHeaderValues(params);
	}

	getTooltip() {
		let colDef = this.params.column.getColDef() as EntityObjectGridColumn;
		if (colDef && colDef.attributeMeta) {
			return colDef.attributeMeta.getDescription();
		}
		return undefined;
	}

	setMenuOpacity(opacity) {
		this.menuOpacity = opacity;
	}

	sort() {
		if (!this.params.enableSorting) {
			// suppress sorting for images (NULCOS-8092)
			return;
		}
		let sortModel = this.params.columnApi.getColumnState();

		this.addOrUpdateSortColumn(sortModel);

		this.params.columnApi.applyColumnState({state: sortModel});
		this.multiSorting = this.isMultiSort();
		this.sortIndex = this.getSortIndex() ? this.getSortIndex()! : 0;
		this.params.api.refreshHeader();
	}

	openMenu(event) {
		this.params.showColumnMenu(event.target as HTMLElement);
	}

	isMultiSort() {
		return this.params.columnApi.getColumnState().filter(c => c.sort !== null).length > 1;
	}

	refresh(params: IHeaderParams): boolean {
		this.setHeaderValues(params);
		return true;
	}

	private setHeaderValues(params: IHeaderParams) {
		this.params = params;
		this.tooltip = this.getTooltip();
		this.multiSorting = this.isMultiSort();
		this.sortingAsc = params.column.isSortAscending();
		this.sortingDesc = params.column.isSortDescending();
		this.sortIndex = this.getSortIndex() ? this.getSortIndex()! : 0;
	}

	private addOrUpdateSortColumn(sortModel: ColumnState[]) {
		let column = sortModel.find(c => c.colId === this.params.column.getColId());
		if (column === undefined) {
			return;
		}

		let sortedColumns = sortModel.filter(v => v.sort !== null).sort((a: ColumnState, b: ColumnState) => {
			return a.sortIndex! - b.sortIndex!;
		});
		switch (column.sort) {
			case 'asc':
				column.sort = 'desc';
				this.sortingDesc = true;
				this.sortingAsc = false;
				break;
			case 'desc':
				column.sort = null;
				column.sortIndex = null;
				this.sortingDesc = false;
				this.sortingAsc = false;
				let deleteIndex = sortedColumns.findIndex(v => v.colId === column?.colId);
				sortedColumns.splice(deleteIndex, 1);
				sortedColumns.forEach(
					(value, index) => {
						value.sortIndex = index;
					}
				);
				break;
			default:
				column.sort = 'asc';
				column.sortIndex = sortedColumns.length;
				this.sortingDesc = false;
				this.sortingAsc = true;
				break;
		}
	}

	private getSortIndex(): number | null | undefined {
		return this.params.columnApi
			.getColumnState()
			.find(sort => sort.colId === this.params.column.getColId())?.sortIndex;
	}
}
