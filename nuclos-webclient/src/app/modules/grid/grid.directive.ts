import { Directive } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { ColDef } from 'ag-grid-community';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { GridHeaderComponent } from './grid-header/grid-header.component';

/**
 * Makes some adjustments to the column headers for any ag-Grid component
 * annotated with this directive.
 *
 * TODO: After refactoring the grid components so that ag-Grid is not used directly
 * anymore except in the grid module, this directive probably won't be needed anymore.
 */
@Directive({
	selector: '[nucGrid]'
})
export class GridDirective {

	constructor(
		grid: AgGridAngular,
		private i18nService: NuclosI18nService
	) {
		if (!grid.defaultColDef) {
			grid.defaultColDef = {
				sortable: true,
				resizeable: true
			} as ColDef;
		}
		grid.defaultColDef.headerComponentFramework = GridHeaderComponent;

		grid.localeTextFunc = (key: string, defaultValue: string) => {
			const data = this.i18nService.getI18nOrUndefined('webclient.aggrid.lang.' + key);
			return data === undefined ? defaultValue : data;
		};

		// grid.applyColumnDefOrder = true;
		grid.enableBrowserTooltips = true;
	}

}
