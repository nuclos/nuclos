import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { TooltipModule } from 'primeng/tooltip';
import { HtmlModule } from '../html/html.module';
import { TooltipValueModule } from '../tooltip-value/tooltip-value.module';
import { GridHeaderComponent } from './grid-header/grid-header.component';
import { GridDirective } from './grid.directive';
import { GridService } from './grid.service';
import {
	BooleanRendererComponent,
	DateRendererComponent,
	HyperlinkRendererComponent,
	NumberRendererComponent,
	StateIconRendererComponent
} from './grid/cell-renderer';
import { GridComponent } from './grid/grid.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		AgGridModule.withComponents([
			StateIconRendererComponent,
			BooleanRendererComponent,
			NumberRendererComponent,
			DateRendererComponent,
			HyperlinkRendererComponent
		]),
		TooltipModule,
		HtmlModule,
		TooltipValueModule
	],
	declarations: [
		GridHeaderComponent,
		GridDirective,
		GridComponent,
		BooleanRendererComponent,
		StateIconRendererComponent,
		NumberRendererComponent,
		DateRendererComponent,
		HyperlinkRendererComponent
	],
	providers: [
		GridService
	],
	exports: [
		GridDirective,
		GridComponent,
		BooleanRendererComponent,
		StateIconRendererComponent,
		NumberRendererComponent,
		DateRendererComponent,
		HyperlinkRendererComponent
	]
})
export class GridModule {
}
