import { AgRendererComponent } from 'ag-grid-angular';
import { ColDef, ICellRendererParams } from 'ag-grid-community';
import { EntityAttrMeta } from '../../../entity-object-data/shared/bo-view.model';

export class AbstractCellRenderer implements AgRendererComponent {

	private entityAttrMeta: EntityAttrMeta;

	private params;

	constructor() {
	}

	agInit(params: ICellRendererParams): void {
		this.refresh(params);
		if ( params.colDef?.cellEditorParams) {
			this.entityAttrMeta =  params.colDef.cellEditorParams['attrMeta'];
		}
	}

	refresh(params: any): boolean {
		this.params = params;
		return false;
	}

	protected getAttributeMeta(): EntityAttrMeta | undefined {
		return this.entityAttrMeta;
	}

}
