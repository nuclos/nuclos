import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';

@Component({
	selector: 'nuc-boolean-renderer',
	templateUrl: './boolean-renderer.component.html',
	styleUrls: ['./boolean-renderer.component.css']
})
export class BooleanRendererComponent implements AgRendererComponent {

	params;

	constructor() {
	}

	agInit(params: any) {
		this.params = params;
	}

	refresh(params: any): boolean {
		this.params = params;
		return false;
	}

}
