import { Component } from '@angular/core';
import { AbstractCellRenderer } from '../abstract-cell-renderer';

@Component({
	selector: 'nuc-hyperlink-renderer',
	templateUrl: './hyperlink-renderer.component.html',
	styleUrls: ['./hyperlink-renderer.component.css']
})
export class HyperlinkRendererComponent extends AbstractCellRenderer {

	static readonly ANCHOR_TAG_REGEX = '[ ]*<a.*href=\"(.*)\"[^>]*>(.*)</a>[ ]*';

	formattedTitle: string;
	linkTarget: string;

	constructor() {
		super();
	}

	agInit(params: any) {
		super.agInit(params);
		this.refresh(params);
	}

	refresh(params: any): boolean {
		super.refresh(params);
		let attributeMeta = this.getAttributeMeta();
		if (attributeMeta) {

			let match = params.value?.toString().match(HyperlinkRendererComponent.ANCHOR_TAG_REGEX);
			if (match) {
				this.formattedTitle = match[2];
				this.linkTarget = match[1];
			} else {
				this.formattedTitle = this.linkTarget = params.value;
			}
		}
		return false;

	}
}
