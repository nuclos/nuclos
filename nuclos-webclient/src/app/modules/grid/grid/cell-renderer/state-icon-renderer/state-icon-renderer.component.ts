import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';

@Component({
	selector: 'nuc-state-icon-renderer',
	templateUrl: './state-icon-renderer.component.html',
	styleUrls: ['./state-icon-renderer.component.css']
})
export class StateIconRendererComponent implements AgRendererComponent {

	params;

	constructor() {
	}

	agInit(params: any) {
		this.params = params;
	}

	refresh(params: any): boolean {
		return false;
	}

	getStateIconUrl(): string | undefined {
		return this.params.node.data?.eoData?.links?.stateIcon?.href ?? this.params.node.data?.nuclosStateIconUrl;
	}

	getStateIconAltText(): string | undefined {
		return this.params.node.data ? this.params.node.data.nuclosStateIconAltText : undefined;
	}
}
