import { NgModule } from '@angular/core';
import { ValueToSafeHtmlStringPipe } from './pipes/value-to-safe-html-string.pipe';
import { ValueToSafeHtmlPipe } from './pipes/value-to-safe-html.pipe';

@NgModule({
	declarations: [
		ValueToSafeHtmlPipe,
		ValueToSafeHtmlStringPipe
	],
	exports: [
		ValueToSafeHtmlPipe,
		ValueToSafeHtmlStringPipe
	],
	providers: [
		ValueToSafeHtmlPipe,
		ValueToSafeHtmlStringPipe
	]
})
export class HtmlModule {
}
