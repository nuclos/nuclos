import { Pipe, PipeTransform, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Transforms a value string to html, e.g. for attribute descriptions (tooltips) formatted with html.
 */
@Pipe({name: 'valueToSafeHtmlString'})
export class ValueToSafeHtmlStringPipe implements PipeTransform {
	constructor(
		private sanitizer: DomSanitizer) {
	}
	transform(value: string | undefined): string | undefined {
		let result: string | undefined;
		if (value !== undefined && value !== null) {
			let sanitizedValue = this.sanitizer.sanitize(SecurityContext.HTML,
				value.replace(/\n/g, '<br/>'));
			if (sanitizedValue === null) {
				result = undefined;
			} else {
				result = sanitizedValue;
			}
		}
		return result;
	}
}
