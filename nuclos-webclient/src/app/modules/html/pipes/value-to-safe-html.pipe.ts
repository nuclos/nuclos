import { Pipe, PipeTransform, SecurityContext } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

/**
 * Transforms a value string to html, e.g. for attribute descriptions (tooltips) formatted with html.
 */
@Pipe({name: 'valueToSafeHtml'})
export class ValueToSafeHtmlPipe implements PipeTransform {
	constructor(
		private sanitizer: DomSanitizer) {
	}
	transform(value: string | undefined, replaceLineBreaks = true): SafeHtml | undefined {
		let result: SafeHtml | undefined;
		if (value !== undefined && value !== null) {
			if (!value.replace) {
				replaceLineBreaks = false;
			}
			let sanitizedValue = this.sanitizer.sanitize(SecurityContext.HTML,
				replaceLineBreaks ? value.replace(/\n/g, '<br/>') : value);
			if (sanitizedValue === null) {
				result = undefined;
			} else {
				result = this.sanitizer.bypassSecurityTrustHtml(sanitizedValue);
			}
		}
		return result;
	}
}
