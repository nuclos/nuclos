import { Component, OnInit } from '@angular/core';
import { NavigationGuard } from '@app/guard/navigation-guard';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { Logger } from '../../log/shared/logger';

@Component({
	selector: 'nuc-locale',
	templateUrl: './locale.component.html',
	styleUrls: ['./locale.component.css']
})
export class LocaleComponent implements OnInit {
	locales;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor(
		private nuclosI18n: NuclosI18nService,
		private navigationGuard: NavigationGuard,
		private $log: Logger
	) {
	}

	ngOnInit() {
		this.locales = this.nuclosI18n.getLocales();
	}

	getSelectedLocale() {
		return this.nuclosI18n.getCurrentLocale();
	}

	setSelectedLocale(locale: string, event: Event) {
		// suppress navigation
		if (this.navigationGuard.canDeactivateSelectedEo(
			undefined,
			undefined,
			true)) {
			event.preventDefault();
			this.$log.debug('set locale: %o', locale);
			this.nuclosI18n.setLocaleString(locale);
		}
	}
}
