/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { InputRequiredDialogComponent } from './input-required-dialog.component';

xdescribe('InputRequiredDialogComponent', () => {
	let component: InputRequiredDialogComponent;
	let fixture: ComponentFixture<InputRequiredDialogComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [InputRequiredDialogComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(InputRequiredDialogComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
