import { AfterContentInit, Component, ElementRef, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { StringUtils } from '../../../shared/string-utils';
import { InputRequiredSepecification } from '../shared/model';

@Component({
	selector: 'nuc-input-required-dialog',
	templateUrl: './input-required-dialog.component.html',
	styleUrls: ['./input-required-dialog.component.css']
})
export class InputRequiredDialogComponent implements OnInit, AfterContentInit {

	@Input() dialogId: number | undefined;

	messageFormatted: string;

	@Input() specification: InputRequiredSepecification;

	inputValue: string;

	constructor(private activeModal: NgbActiveModal, private elementRef: ElementRef) {
	}

	ngOnInit() {
		if (this.specification.type === 'input_option') {
			this.inputValue = this.specification.defaultoption;
		}
		this.messageFormatted = StringUtils.textToHtml(this.specification.message);
	}

	ngAfterContentInit() {
		setTimeout(() => {
			// set initial focus in dialog consistently
			if (this.elementRef.nativeElement !== undefined && this.specification?.type !== undefined) {
				if (this.specification.type.includes('input_')) {
					// focus input component
					$(this.elementRef.nativeElement).find('input,textarea,select').first().trigger('focus');
				} else {
					// focus first button
					$(this.elementRef.nativeElement).find('button[type="submit"]').first().trigger('focus');
				}
			}
		}, 150);
	}

	ok(result: any) {
		this.activeModal.close(result);
	}

	cancel() {
		this.activeModal.dismiss();
	}
}
