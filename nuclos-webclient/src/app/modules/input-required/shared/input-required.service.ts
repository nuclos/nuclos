import { HttpErrorResponse } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { EntityObjectErrorService } from '@modules/entity-object-data/shared/entity-object-error.service';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import {
	EMPTY,
	Observable,
	of as observableOf, throwError,
	throwError as observableThrowError
} from 'rxjs';

import { map, take, tap } from 'rxjs/operators';
import { NuclosDialogService } from '../../../core/service/nuclos-dialog.service';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { StringUtils } from '../../../shared/string-utils';
import { AddonService } from '../../addons/addon.service';
import { Logger } from '../../log/shared/logger';
import { InputRequiredDialogComponent } from '../input-required-dialog/input-required-dialog.component';
import { InputRequired } from './model';

/**
 * Handles "Input Required" which can occur whenever an EO is saved.
 * This service does not save the EO itself or some strange callback magic.
 */
@Injectable()
export class InputRequiredService {

	constructor(
		private addonService: AddonService,
		private injector: Injector,
		private dialogService: NuclosDialogService,
		private eoErrorService: EntityObjectErrorService,
		private nuclosI18nService: NuclosI18nService,
		private $log: Logger
	) {
	}

	/**
	 * Handles possible InputRequired "errors".
	 * Returns an Observable that handles the InputRequired specification
	 * or just an Observable containing the given error if no InputRequired was found.
	 *
	 * @param response The error response that occurred while sending the data. Could contain an InputRequired specification.
	 * @param data The original request data.
	 * @returns {any}
	 */
	handleError(
		response: HttpErrorResponse,
		data: any,
		eo: IEntityObject | undefined
	): Observable<any> {
		if (response.error && response.error.inputrequired) {
			let inputRequired = <InputRequired>response.error.inputrequired;
			return this.handleInputRequired(inputRequired, eo).pipe(
				tap(result => {
					if (result === undefined) {
						throw this.eoErrorService.handleError({
							message: 'webclient.error.inputrequired.cancel'
						});
					}

					// data might have a "inputrequired" attribute from a previous InputRequired error.
					// The previous result must be merged with the current one and sent again.
					let nextInputRequired = data.inputrequired || inputRequired;
					if (!nextInputRequired.result) {
						nextInputRequired.result = {};
					}
					if (inputRequired.specification.key) {
						nextInputRequired.result[inputRequired.specification.key] = result;
						nextInputRequired.specification = inputRequired.specification;
					} else {
						// addon (InputDelegateSpecification has no key/message/type)
						for (const key of Object.keys(result)) {
							const value = result[key];
							nextInputRequired.result[key] = value;
						}
					}
					data.inputrequired = nextInputRequired;
				})
			);
		}

		// If no InputRequired was handled, just re-throw the error
		return observableThrowError(response);
	}

	/**
	 * Handles a concrete InputRequired object.
	 */
	private handleInputRequired(
		inputRequired: InputRequired,
		eo: IEntityObject | undefined
	): Observable<any> {
		if (!inputRequired || !inputRequired.specification) {
			return observableOf(undefined);
		}

		// if the InputRequiredException is called with InputDelegateSpecification an addon will be instantiated (counterpart to client extension)
		let inputRequiredDialogComponent = InputRequiredDialogComponent;
		const delegateComponentPackageName = inputRequired.specification.delegateComponent;
		if (delegateComponentPackageName) {
			// use 'class-name' for instantiating addon, package will be ignored
			// this makes it possible to use InputRequiredExceptions with InputDelegateSpecification for Java-Extensions and WebAddons
			const delegateComponentName = delegateComponentPackageName.split('.').pop();
			if (delegateComponentName) {
				const delegateComponent = this.injector.get(delegateComponentName);
				if (!delegateComponent) {
					let errorMessage = 'Unable to find addon component ' + delegateComponentPackageName + ' for input required exception.';
					this.$log.error(errorMessage);

					this.dialogService.alert(
						{
							title: this.nuclosI18nService.getI18n('webclient.error.title'),
							message: StringUtils.textToHtml(errorMessage)
						}
					).pipe(
						take(1)
					).subscribe();
					return EMPTY;
				} else {
					inputRequiredDialogComponent = <any>delegateComponent;
				}
			}
		}


		return this.dialogService.custom(
			inputRequiredDialogComponent,
			{
				'specification': inputRequired.specification,
				'eo': eo
			},
			{
				size: 'lg'
			}
		).pipe(
			take(1)
		);
	}
}
