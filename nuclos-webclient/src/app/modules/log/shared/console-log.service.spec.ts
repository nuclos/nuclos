/* tslint:disable:no-unused-variable */

import { inject, TestBed } from '@angular/core/testing';
import { ConsoleLogService } from './console-log.service';

xdescribe('Service: ConsoleLog', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [ConsoleLogService]
		});
	});

	it('should ...', inject([ConsoleLogService], (service: ConsoleLogService) => {
		expect(service).toBeTruthy();
	}));
});
