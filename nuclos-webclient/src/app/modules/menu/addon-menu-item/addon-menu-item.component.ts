import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from '@nuclos/nuclos-addon-api';
import { MenuService } from '../menu.service';

@Component({
	selector: 'nuc-addon-menu-item',
	templateUrl: './addon-menu-item.component.html',
	styleUrls: ['./addon-menu-item.component.css']
})
export class AddonMenuItemComponent implements OnInit {
	@Input() menuItem: MenuItem;
	@Input() position: string;

	constructor(
		public menuService: MenuService
	) {
	}

	ngOnInit() {
	}

	isVisible(): boolean {
		if (this.position === 'mainmenu') {
			return this.menuItem.position === undefined || this.menuItem.position === 'mainmenu';
		}
		return this.menuItem.position === this.position;
	}
}
