import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { DropdownComponent } from '@modules/ui-components/dropdown/dropdown.component';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FullTextSearchResultItem } from './full-text-search.model';
import { FullTextSearchService } from './full-text-search.service';

@Component({
	selector: 'nuc-full-text-search',
	templateUrl: './full-text-search.component.html',
	styleUrls: ['./full-text-search.component.css']
})
export class FullTextSearchComponent implements OnInit, OnDestroy {

	showFullTextSearch = false;

	@Input() searchtext: string;

	results$: BehaviorSubject<FullTextSearchResultItem[] | undefined> = new BehaviorSubject<FullTextSearchResultItem[] | undefined>(undefined);
	@ViewChild('dropdown') dropdownComponent: DropdownComponent;

	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private fullTextSearchService: FullTextSearchService,
		private i18nService: NuclosI18nService,
		private router: Router
	) {
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.fullTextSearchService.isFullTextSearchEnabled()
			.pipe(takeUntil(this.unsubscribe$)).subscribe(enabled => this.showFullTextSearch = enabled);
	}


	searchtextChanged() {
		if (this.searchtext) {
			this.dropdownComponent.updateAutoCompleteMessage(this.i18nService.getI18n('common.loading'), true);
			this.fullTextSearchService.search(this.searchtext).pipe(takeUntil(this.unsubscribe$)).subscribe(
				(results: FullTextSearchResultItem[]) => {
					this.results$.next(results);
					this.dropdownComponent.updateAutoCompleteMessage(
						results && results.length > 0 ? '' : this.i18nService.getI18n('common.empty'),
						false
					);
				}
			);
		} else {
			this.dropdownComponent.updateAutoCompleteMessage('', false);
			this.results$.next([]);
		}
	}

	navigateToResult($event: FullTextSearchResultItem) {
		this.router.navigateByUrl($event.url);
	}

	modelChanged($event: any) {
		if ($event !== null && typeof $event === 'string') {
			this.searchtext = $event;
			this.searchtextChanged();
		} else {
			this.dropdownComponent.autoComplete.inputEL.nativeElement.value = this.searchtext;
		}
	}
}
