import { UrlTree } from '@angular/router';

export class FullTextSearchResultItem {
	// TODO rename in REST service
	pk: number;
	uid: string;
	text: string;
	url: UrlTree;
}
