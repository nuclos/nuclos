import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { SystemParameter } from '@shared/system-parameters';
import { FullTextSearchResultItem } from './full-text-search.model';

@Injectable()
export class FullTextSearchService {

	private searchUrl: string;

	constructor(
		private nuclosConfigService: NuclosConfigService,
		private router: Router,
		private http: HttpClient
	) {
		this.searchUrl = nuclosConfigService.getRestHost() + '/data/search/{text}';
	}

	isFullTextSearchEnabled(): Observable<boolean> {
		return this.nuclosConfigService.getSystemParameters().pipe(map(
			params => params.is(SystemParameter.FULLTEXT_SEARCH_ENABLED)));
	}

	search(searchtext: string): Observable<FullTextSearchResultItem[]> {
		let fullTextSearchUrl = this.searchUrl.replace('{text}', searchtext);
		return this.http.get(fullTextSearchUrl).pipe(
			map((result: FullTextSearchResultItem[]) => {
				result.forEach(
					(resultItem: FullTextSearchResultItem) => {
						resultItem.url = this.router.createUrlTree(['view', resultItem.uid, resultItem.pk]);
					}
				);
				return result;
			}),
		);
	}
}
