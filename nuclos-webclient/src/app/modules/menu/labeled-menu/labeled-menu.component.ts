import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from '@nuclos/nuclos-addon-api';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { MenuService } from '../menu.service';
import { WebMenuItem } from '../shared/menu.model';

@Component({
	selector: 'nuc-labeled-menu',
	templateUrl: './labeled-menu.component.html',
	styleUrls: ['./labeled-menu.component.css']
})
export class LabeledMenuComponent implements OnInit {
	@Input() name: string;
	@Input() iconClass?: string;
	@Input() icon?: string;
	@Input() staticItems: WebMenuItem[] | undefined;
	@Input() label?: string;
	@Input() addonMenuItems: MenuItem[];
	@Input() addonMenuPosition?: string;

	dynamicItems: Subject<WebMenuItem[]> = new Subject<WebMenuItem[]>();
	visible: Subject<boolean> = new BehaviorSubject<boolean>(true);
	menuEmpty = false;
	item: WebMenuItem;

	private alive = true;

	constructor(
		public menuService: MenuService
	) {
	}

	ngOnInit() {
		this.dynamicItems.pipe(takeWhile(() => this.alive))
			.subscribe(dynamicItems => {
					this.item.entries = (dynamicItems !== undefined ? dynamicItems : [])
						.concat(this.staticItems !== undefined ? this.staticItems : []);
					this.menuEmpty = this.item.entries.length < 1;
				}
			);
		this.item = {
			name: this.name,
			createNew: false,
			icon: this.iconClass
		};
		this.item.entries = this.staticItems !== undefined ? this.staticItems : [];
		this.menuEmpty = this.item.entries.length < 1;
	}

	ngOnDestroy() {
		this.alive = false;
	}
}
