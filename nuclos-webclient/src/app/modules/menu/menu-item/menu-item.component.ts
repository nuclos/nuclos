import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@modules/authentication';
import { distinctUntilChanged, takeWhile } from 'rxjs/operators';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { NuclosHotkeysService } from '../../../shared/service/nuclos-hotkeys.service';
import { Logger } from '../../log/shared/logger';
import { MenuService } from '../menu.service';
import { WebMenuItem } from '../shared/menu.model';

@Component({
	selector: 'nuc-menu-item',
	templateUrl: './menu-item.component.html',
	styleUrls: ['./menu-item.component.css', '../menu.component.css']
})
export class MenuItemComponent implements OnInit, OnDestroy {

	iconBaseHref: string;

	mpVisibility: Map<WebMenuItem, boolean> = new Map<WebMenuItem, boolean>();
	_menuItem: WebMenuItem;

	private alive = true;

	constructor(
		public menuService: MenuService,
		private config: NuclosConfigService,
		private hotkeysService: NuclosHotkeysService,
		private authenticationService: AuthenticationService,
		private router: Router,
		private $log: Logger
	) {
		this.authenticationService.observeLoginStatus().pipe(takeWhile(() => this.alive), distinctUntilChanged())
			.subscribe(loggedIn => {
				if (loggedIn) {
					this.updateVisibility();
				}
			});
	}

	@Input()
	set menuItem(item: WebMenuItem) {
		this._menuItem = item;
		if (item) {
			this.updateVisibility();
		}
	}

	ngOnInit() {
		this.alive = true;
		this.iconBaseHref = this.config.getRestHost() + '/meta/icon';
		this.lookForShortCutAndRegister(this._menuItem);
	}

	ngOnDestroy(): void {
		this.alive = false;
	}

	// get's only called by view if keyStrokeModifier is set
	// so we can assume it is there at this point
	getModifierStringRepresentation(menuItem: WebMenuItem): string {
		if (menuItem.keyStrokeModifier === 'CTRL|SHIFT') {
			return 'CTRL + SHIFT';
		}

		return menuItem.keyStrokeModifier!;
	}

	closeDropdown($event: MouseEvent) {
		if ($event.target) {
			// close all dropdowns open
			document.querySelectorAll('.dropdown-toggle')?.forEach((element) => {
				// this is available by bootstrap 5 via global
				// @ts-ignore
				bootstrap.Dropdown.getInstance(element)?.hide();
			});

			// close backdrop if visible
			document.querySelectorAll('.offcanvas')?.forEach((element) => {
				// this is available by bootstrap 5 via global
				// @ts-ignore
				bootstrap.Offcanvas.getInstance(element)?.hide();
			});
		}
	}

	private lookForShortCutAndRegister(menuItem: WebMenuItem) {
		if (menuItem.keyStroke && menuItem.keyStrokeModifier) {
			this.registerShortCut(menuItem.keyStroke, menuItem,
				this.getModifierFromString(menuItem.keyStrokeModifier));
		}
		if (menuItem.entries) {
			menuItem.entries.forEach((entry) => {
				this.lookForShortCutAndRegister(entry);
			});
		}
	}

	private getModifierFromString(mod: string): 'CTRL' | 'ALT' | 'CTRL_SHIFT' {
		switch (mod) {
			default:
			case 'ALT':
				return 'ALT';
			case 'CTRL':
				return 'CTRL';
			case 'CTRL|SHIFT':
				return 'CTRL_SHIFT';
		}
	}

	private registerShortCut(key: string, menuItem: WebMenuItem, modifier: 'CTRL' | 'ALT' | 'CTRL_SHIFT') {
		this.$log.debug('Register ShortCut ' + menuItem.keyStrokeModifier + menuItem.keyStroke + ' for ' +
			menuItem.name);
		this.hotkeysService.addShortcut({keys: key, modifier: modifier, description: menuItem.name})
			.pipe(
				takeWhile(() => this.alive)
			)
			.subscribe(() => {
				if (menuItem.boMetaId !== undefined) {
					this.router.navigate(['/view', menuItem.boMetaId]);
				} else if (menuItem.href !== undefined) {
					this.router.navigateByUrl(menuItem.href);
				}
			});
	}

	private updateVisibility() {
		if (this._menuItem) {
			this._menuItem.entries?.forEach(menuEntry => {
				if (menuEntry.isVisible) {
					this.mpVisibility.set(menuEntry, menuEntry.isVisible());
				}
			});
		}
	}
}
