import { AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, UserAction } from '@nuclos/nuclos-addon-api';
import { BehaviorSubject } from 'rxjs';
import { take, takeWhile } from 'rxjs/operators';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { Options } from '../../data/schema/types.intern';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { SystemParameter } from '@shared/system-parameters';
import { AuthenticationService } from '../authentication';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { LabeledMenuComponent } from './labeled-menu/labeled-menu.component';
import { MenuService } from './menu.service';
import { LabeledMenuItem, WebMenuItem } from './shared/menu.model';

@Component({
	selector: 'nuc-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, OnDestroy, AfterViewInit {
	@ViewChildren('labeledMenu')
	labeledMenus: QueryList<LabeledMenuComponent>;

	menuStructure: WebMenuItem | undefined;
	username: string | undefined;
	fadingClass: string | undefined;
	backgroundColor: string | undefined;
	backgroundSuColor: string | undefined;
	su: boolean | undefined;
	loggedIn = false;
	addonMenu: MenuItem[] = [];
	labeledMenuItems: LabeledMenuItem[] = [
		new LabeledMenuItem({
			name: this.i18nService.getI18n('webclient.devUtils.name').replace(' ', '-'),
			entries: [
				{
					name: this.i18nService.getI18n('webclient.swaggerUI'),
					href: '/swagger-ui',
					createNew: false
				},
				{
					name: this.i18nService.getI18n('webclient.businesstests.menu'),
					href: '/businesstests',
					createNew: false
				},
				{
					name: this.i18nService.getI18n('webclient.cache.overview'),
					href: '/cache',
					createNew: false
				}
			],
			iconClass: 'fa-code',
			_isVisible: () => this.isDevMode && this.loggedIn
		}),
		new LabeledMenuItem({
			name: this.i18nService.getI18n('webclient.menu.administration'),
			entries: [
				{
					name: this.i18nService.getI18n('webclient.serverInfo.name'),
					href: '/serverinfo',
					// icon: 'fa-file-text',
					createNew: false,
					isVisible: () => this.isSuperUser()
				},
				{
					name: this.i18nService.getI18n('webclient.admin.maintenance.mode'),
					href: '/maintenance',
					// icon: 'fa-clock-o',
					createNew: false,
					isVisible: () => this.isSuperUser()
				}
			],
			iconClass: 'fa-cog',
			_isVisible: () => this.loggedIn,
			addonMenuPosition: 'adminmenu'
		}),
			new LabeledMenuItem({
			name: this.i18nService.getI18n('webclient.menu.configuration'),
			iconClass: 'fa-wrench',
			_isVisible: () => this.loggedIn,
			addonMenuPosition: 'configmenu'
		}),
	];

	private loggedInSubject = new BehaviorSubject<boolean>(false);
	private isDevMode = false;
	private devModeSubject = new BehaviorSubject<boolean>(false);
	private alive = true;
	private mpMenuItemsByName: Map<string, WebMenuItem[]> = new Map<string, WebMenuItem[]>();

	constructor(
		public menuService: MenuService,
		private nuclosConfig: NuclosConfigService,
		private authenticationService: AuthenticationService,
		private i18nService: NuclosI18nService,
		private hotkeysService: NuclosHotkeysService,
		private eoResultService: EntityObjectResultService,
		private router: Router
	) {
		this.authenticationService.observeLoginStatus().pipe(takeWhile(() => this.alive)).subscribe(loggedIn => {
			this.loggedIn = loggedIn;
			if (this.loggedIn) {
				this.updateMenu(this.loggedIn);
				this.username = this.authenticationService.getUsername();
				this.su = this.isSuperUser();
			} else {
				this.menuStructure = undefined;
				this.username = undefined;
				this.mpMenuItemsByName.clear();
				this.updateLabeledMenuItems(this.labeledMenus);
				this.loggedInSubject.next(loggedIn);
			}
		});

		this.fadingClass = 'bg-faded';
		this.backgroundColor = '#666666';
		this.nuclosConfig.getSystemParameters().pipe(take(1)).subscribe(params => {
			this.isDevMode = params.is(SystemParameter.ENVIRONMENT_DEVELOPMENT);
			this.devModeSubject.next(this.isDevMode);
			if (params.has(SystemParameter.ROOTPANE_BACKGROUND_COLOR)) {
				this.fadingClass = 'bg-solid';
				this.backgroundColor = this.backgroundSuColor = this.parseColor(params.get(SystemParameter.ROOTPANE_BACKGROUND_COLOR));
			}

			if (params.has(SystemParameter.ROOTPANE_SU_BACKGROUND_COLOR)) {
				this.fadingClass = 'bg-solid';
				this.backgroundSuColor = this.parseColor(params.get(SystemParameter.ROOTPANE_SU_BACKGROUND_COLOR));
			}
		});

		this.menuService.getAddonMenuStructure().pipe(
			takeWhile(() => this.alive)
		).subscribe((addonMenu: MenuItem[]) => {
			this.addonMenu = addonMenu;
		});

		this.devModeSubject.pipe(takeWhile(() => this.alive)).subscribe(() => {
				this.updateLabeledMenuVisibility(this.labeledMenus);
			}
		);

		this.loggedInSubject.pipe(takeWhile(() => this.alive)).subscribe(() => {
				this.updateLabeledMenuVisibility(this.labeledMenus);
			}
		);
	}

	getBgColor() {
		return this.su ? this.backgroundSuColor : this.backgroundColor;
	}

	parseColor(par: string) {
		let color = '#';
		for (let split of par.split(',', 3)) {
			let hexValue = parseInt(split, 10).toString(16);
			if (hexValue.length === 1) {
				hexValue =  `0${hexValue}`;
			}
			color += hexValue;
		}
		return color;
	}

	ngOnInit() {
		let user = this.authenticationService.getCurrentUser();
		if (user) {
			this.username = user.username;
		}

		this.hotkeysService.addShortcut({
			keys: 'D',
			modifier: 'CTRL_SHIFT',
			description: this.i18nService.getI18n('webclient.shortcut.dashboardHome')
		}).pipe(takeWhile(() => this.alive)).subscribe(() => {
			let selectedEO = this.eoResultService.getSelectedEo();
			if (selectedEO === undefined || (!selectedEO.isDirty())) {
				this.router.navigate(['/dashboard']);
			}
		});
	}

	ngAfterViewInit() {
		this.labeledMenus.changes
			.pipe(takeWhile(() => this.alive))
			.subscribe((namedMenus: QueryList<LabeledMenuComponent>) => {
				this.updateLabeledMenuItems(namedMenus);
			});
	}

	ngOnDestroy() {
		this.alive = false;
	}

	isAnonymous(): boolean {
		return this.authenticationService.isAnonymous();
	}

	isSuperUser() {
		return this.authenticationService.isSuperUser();
	}

	getMenuItems(name: string): WebMenuItem[] {
		let items = this.mpMenuItemsByName.get(name);
		if (items === undefined) {
			return [];
		}
		return items;
	}

	canSharePreferences() {
		return this.authenticationService.isActionAllowed(UserAction.SharePreferences);
	}

	canExecuteReport() {
		return this.authenticationService.isActionAllowed(UserAction.ExecuteReports);
	}

	getUnlabeledMenuStructure(): WebMenuItem[] {
		if (this.menuStructure) {
			if (this.menuStructure.entries) {
				return this.menuStructure.entries
					.filter(m => !this.labeledMenuItems.find(n => n.name === m.name));
			}
		}

		return [];
	}

	private updateMenu(loggedIn: boolean): void {
		this.menuService.getMenuStructure().pipe(take(1)).subscribe(menuStructure => {
			this.mpMenuItemsByName.clear();
			this.menuStructure = menuStructure;

			if (this.menuStructure !== undefined && this.menuStructure.entries !== undefined) {
				this.menuStructure.entries.forEach((menuItem: WebMenuItem) =>
					this.mpMenuItemsByName.set(menuItem.name, menuItem.entries !== undefined ? menuItem.entries : [])
				);
			}

			this.updateLabeledMenuItems(this.labeledMenus);

			this.getUnlabeledMenuStructure().forEach((entry, index) => {
				const option = {
					modifier: 'CTRL_SHIFT',
					keys: (index + 1).toString(),
					description: 'Dropdown ' + entry.name
				} as Options;
				if (!this.hotkeysService.hasShortCut(option)) {
					this.hotkeysService.addShortcut(option).pipe(takeWhile(() => this.alive)).subscribe(() => {
						// toggle dropdown menu with toggle on dropdown function
						const dropdownListToggle = $('#Dropdown-' + entry.name.replace(' ', '').trim());
						dropdownListToggle.focus();
						if (dropdownListToggle.hasClass('show')) {
							dropdownListToggle.dropdown('hide');
						} else {
							dropdownListToggle.dropdown('show');
						}
					});
				}
			});

			this.loggedInSubject.next(loggedIn);
		});
	}

	private updateLabeledMenuItems(labeledMenus: QueryList<LabeledMenuComponent>) {
		labeledMenus.forEach((labeledMenu: LabeledMenuComponent) => {
			const menuItem = this.mpMenuItemsByName.get(labeledMenu.name);
			if (menuItem) {
				labeledMenu.dynamicItems.next(menuItem);
			}
		});
	}

	private updateLabeledMenuVisibility(labeledMenus: QueryList<LabeledMenuComponent>) {
		if (labeledMenus !== undefined) {
			labeledMenus.forEach((labeledMenu: LabeledMenuComponent) => {
				let labeledMenuItem = this.labeledMenuItems.find(item => item.name === labeledMenu.name);
				if (labeledMenuItem !== undefined && labeledMenuItem.isVisible !== undefined) {
					labeledMenu.visible.next(labeledMenuItem.isVisible(labeledMenu.item.entries));
				}
			});
		}
	}
}
