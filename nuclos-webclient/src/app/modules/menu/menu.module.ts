import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UiComponentsModule } from '@modules/ui-components/ui-components.module';
import { SharedModule } from '../../shared/shared.module';
import { DashboardModule } from '../dashboard/dashboard.module';
import { I18nModule } from '../i18n/i18n.module';
import { NewsModule } from '../news/news.module';
import { AddonMenuItemComponent } from './addon-menu-item/addon-menu-item.component';
import { FullTextSearchComponent } from './full-text-search/full-text-search.component';
import { FullTextSearchService } from './full-text-search/full-text-search.service';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { MenuComponent } from './menu.component';
import { MenuService } from './menu.service';
import { LabeledMenuComponent } from './labeled-menu/labeled-menu.component';
import { NewsMenuComponent } from './news-menu/news-menu.component';
import { PreferencesResetModalComponent } from './user-menu/preferences-reset-modal/preferences-reset-modal.component';
import { UserMenuComponent } from './user-menu/user-menu.component';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		FormsModule,
		I18nModule,
		DashboardModule,
		NewsModule,
		SharedModule,
		UiComponentsModule
	],
	declarations: [
		MenuComponent,
		MenuItemComponent,
		UserMenuComponent,
		LabeledMenuComponent,
		NewsMenuComponent,
		FullTextSearchComponent,
		PreferencesResetModalComponent,
		AddonMenuItemComponent
	],
	exports: [
		MenuComponent
	],
	providers: [
		MenuService,
		FullTextSearchService
	]
})
export class MenuModule {
}
