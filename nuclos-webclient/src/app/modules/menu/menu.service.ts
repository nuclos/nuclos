import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Logger, MenuApi, MenuItem } from '@nuclos/nuclos-addon-api';
import { Observable, Subject } from 'rxjs';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { WebMenuItem } from './shared/menu.model';

@Injectable({
	providedIn: 'root'
})
export class MenuService implements MenuApi {

	private addonMenuItems: MenuItem[] = [];
	private addonMenu$: Subject<MenuItem[]> = new Subject<MenuItem[]>();

	constructor(
		private http: HttpClient,
		private nuclosConfig: NuclosConfigService,
		private router: Router,
		private $log: Logger
	) {
	}

	getMenuStructure(): Observable<WebMenuItem> {
		return this.http.get<WebMenuItem>(
			this.nuclosConfig.getRestHost() + '/meta/menustructure'
		);
	}

	registerAdditionalMenuItem(item: MenuItem): void {
		this.addonMenuItems.push(item);
		this.addonMenu$.next(this.addonMenuItems);
	}

	getAddonMenuStructure(): Subject<MenuItem[]> {
		return this.addonMenu$;
	}

	getCurrentAddonMenuItems(): MenuItem[] {
		return this.addonMenuItems;
	}

	addonMenuClick(menuItem: MenuItem) {
		if (menuItem.href) {
			this.router.navigate([menuItem.href]);
		} else if (menuItem.clickEventHandler) {
			menuItem.clickEventHandler.next(true);
		}
	}

	/**
	 * it is not possible to make menu panel scrollable (in case it will not fit on the page)
	 * without loosing submenu entry functionality
	 *
	 * this workaround makes it still possible to scroll the menu panel
	 * @param $event
	 * @param isSubMenu
	 * @param wheelSpeed
	 */
	doScroll($event, isSubMenu = false, wheelSpeed = 100) {
		$event.preventDefault();
		$event.stopPropagation();

		let menuPanel = $($event.target).closest('.dropdown-menu');
		let delta = -$event.deltaY;
		let scrollY = menuPanel.position().top + (delta * wheelSpeed);
		let headerHeight = $('header nav').height() - 5;
		let mainHeight = $('main').height();
		let initialTop = ((menuPanel.offset().top + -menuPanel.position().top) + menuPanel.height());
		let mostNegativeTop = (mainHeight - initialTop);
		let boundTop = isSubMenu ? 0 : headerHeight;


		scrollY = Math.max(mostNegativeTop > boundTop ? boundTop : mostNegativeTop, Math.min(scrollY, boundTop)); // limit scrolling
		menuPanel.css('max-height', 'unset').css('top', scrollY + 'px');
	}
}
