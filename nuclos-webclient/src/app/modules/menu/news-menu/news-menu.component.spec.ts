import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NewsMenuComponent } from './news-menu.component';

xdescribe('NewsMenuComponent', () => {
	let component: NewsMenuComponent;
	let fixture: ComponentFixture<NewsMenuComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [NewsMenuComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(NewsMenuComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
