import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AuthenticationService } from '@modules/authentication';
import { MenuItem } from '@nuclos/nuclos-addon-api';
import { Subject } from 'rxjs';
import { distinctUntilChanged, take, takeUntil } from 'rxjs/operators';
import { NewsService } from '../../news/shared/news.service';

@Component({
	selector: 'nuc-news-menu',
	templateUrl: './news-menu.component.html',
	styleUrls: ['./news-menu.component.css']
})
export class NewsMenuComponent implements OnInit, OnDestroy {
	news: News[];
	positionStart = false;
	newsExpanded = false;
	@Input() addonMenuItems: MenuItem[];

	private unsubscribe$ = new Subject<boolean>();


	constructor(
		private newsService: NewsService,
		private authService: AuthenticationService,
	) {
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.authService.observeLoginStatus().pipe(
			takeUntil(this.unsubscribe$),
			distinctUntilChanged()
		).subscribe((loggedIn) => {
			setTimeout(() => {
				this.positionStart = !loggedIn;
			});
			this.loadNews();
		});
	}

	loadNews() {
		this.newsExpanded = $('#news > li > a').attr('aria-expanded') === 'true';
		this.newsService.getNews().pipe(
			take(1)
		).subscribe(
			data => {
				if (data && data.length > 0) {
					this.news = data;
				}
			}
		);
	}

	showNews(news) {
		this.newsService.showNews(news);
	}
}
