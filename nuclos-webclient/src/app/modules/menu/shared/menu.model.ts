import { MenuItem } from '@nuclos/nuclos-addon-api';
import { Subject } from 'rxjs';

export interface WebMenuItem extends MenuItem {
	boMetaId?: string;
	createNew: boolean;
	entries?: WebMenuItem[];
	keyStroke?: string;
	keyStrokeModifier?: string;
	isVisible?: Function;
	processMetaId?: string;
}

export interface ILabeledMenuItem extends MenuItem {
	label?: string;
	iconClass?: string;
	entries?: WebMenuItem[];
	_isVisible?: Function;
	addonMenuPosition?: string;
	keyStroke?: string;
	keyStrokeModifier?: string;
}

export class LabeledMenuItem implements MenuItem {

	label?: string;
	iconClass?: string;
	entries?: WebMenuItem[];
	_isVisible?: Function;
	addonMenuPosition?: string;
	keyStroke?: string;
	keyStrokeModifier?: string;
	clickEventHandler?: Subject<any>;
	href?: string;
	icon?: string;
	name: string;
	position?: string;
	isVisible: Function;
	isAtLeastOneEntryVisible: Function;

	constructor(menuItem: ILabeledMenuItem) {

		this.label = menuItem.label;
		this.iconClass = menuItem.iconClass;
		this.entries = menuItem.entries;
		this._isVisible = menuItem._isVisible;
		this.addonMenuPosition = menuItem.addonMenuPosition;
		this.keyStroke = menuItem.keyStroke;
		this.keyStrokeModifier = menuItem.keyStrokeModifier;
		this.clickEventHandler = menuItem.clickEventHandler;
		this.href = menuItem.href;
		this.icon = menuItem.icon;
		this.name = menuItem.name;
		this.position = menuItem.position;

		this.isVisible = (entries: WebMenuItem[] | undefined) => {
			if (this._isVisible && this.isAtLeastOneEntryVisible) {
				return this._isVisible() && this.isAtLeastOneEntryVisible(entries);
			}
			return true;
		};

		this.isAtLeastOneEntryVisible = (entries: WebMenuItem[] | undefined) => {
			let retVal = false;

			entries?.forEach(entry => retVal = retVal || (entry.isVisible ? entry.isVisible() : true));

			return retVal;
		};
	}
}
