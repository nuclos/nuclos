import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationGuard } from '@app/guard/navigation-guard';
import { MenuItem } from '@nuclos/nuclos-addon-api';
import { take, takeUntil } from 'rxjs/operators';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../../data/classes/subscription-handle.directive';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { AuthenticationService } from '@modules/authentication';
import { PreferencesResetModalComponent } from './preferences-reset-modal/preferences-reset-modal.component';

@Component({
	selector: 'nuc-user-menu',
	templateUrl: './user-menu.component.html',
	styleUrls: ['./user-menu.component.css']
})
export class UserMenuComponent extends SubscriptionHandleDirective implements OnInit {
	@Input() loggedIn = false;
	@Input() username: string | undefined;
	@Input() anonymous: boolean;
	@Input() canSharePreferences: boolean;
	@Input() canExecuteReport: boolean;
	@Input() addonMenuItems: MenuItem[];

	mandators: Array<Mandator>;
	currentMandator: Mandator;

	dataLanguages: Array<DataLanguage> | undefined;
	currentDataLanguageId: string | undefined;

	constructor(
		private authenticationService: AuthenticationService,
		private dialogService: NuclosDialogService,
		public router: Router,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService,
		private navigationGuard: NavigationGuard
	) {
		super();
	}

	ngOnInit() {

		this.authenticationService.observeLoginStatus().pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			let authData = this.authenticationService.getAuthentication();
			if (authData) {
				if (authData.mandators) {
					this.mandators = authData.mandators;
				}
				if (authData.mandator) {
					this.currentMandator = authData.mandator;
				}
				this.currentDataLanguageId = authData.datalanguage;
			} else {
				this.currentDataLanguageId = this.authenticationService.getDataLanguageCookie();
			}
		});

		this.i18n.getLocale().subscribe(locale => {
			this.authenticationService.getDataLanguages(locale.key).subscribe(dataLanguages => {
				if (dataLanguages && dataLanguages.length > 0) {
					this.dataLanguages = dataLanguages;
				} else {
					this.dataLanguages = undefined;
				}
			});
		});

		this.nuclosHotkeysService.addShortcut({
			keys: 'l',
			modifier: 'ALT_SHIFT',
			description: this.i18n.getI18n('webclient.shortcut.logout')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.router.navigate(['logout']);
		});

		this.nuclosHotkeysService.addShortcut({
			keys: 'p',
			modifier: 'ALT_SHIFT',
			description: this.i18n.getI18n('webclient.shortcut.changePassword')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.router.navigate(['/account/changePassword']);
		});
	}

	selectMandator(mandator: Mandator) {
		this.currentMandator = mandator;
		this.authenticationService.selectMandator(mandator).pipe(take(1)).subscribe(() => {
			$('body').css('cursor', 'progress');

			// this.authenticationService.navigateHome();
			// TODO auskommentiert wegen NUCLOS-6836 / RRPTD-94 mit Andreas/Ramin nochmal klären, wie das gemerged werden soll
			this.authenticationService.navigateToLastEntity();
			window.location.reload();
		});
	}

	selectDataLanguage(dataLanguage: DataLanguage) {
		this.currentDataLanguageId = dataLanguage?.datalanguage;
		this.authenticationService.selectDataLanguage(dataLanguage).pipe(take(1)).subscribe(() => {
			$('body').css('cursor', 'progress');
			window.location.reload();
		});
	}

	openPreferencesResetModal() {
		if (this.navigationGuard.canDeactivateSelectedEo(
			undefined,
			undefined,
			true
		)) {
			this.dialogService.custom(PreferencesResetModalComponent, undefined, {size: 'lg'})
				.pipe(
					take(1)
				).subscribe();
		}
	}

	isPasswordChangeAvailable() {
		return !this.authenticationService.isLdapActive() &&
			!this.authenticationService.isSsoActive();
	}

	openShortcutsHelp() {
		this.nuclosHotkeysService.openShortcutHelp();
	}
}
