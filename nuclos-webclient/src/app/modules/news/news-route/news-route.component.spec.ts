import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NewsRouteComponent } from './news-route.component';

xdescribe('NewsRouteComponent', () => {
	let component: NewsRouteComponent;
	let fixture: ComponentFixture<NewsRouteComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [NewsRouteComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(NewsRouteComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
