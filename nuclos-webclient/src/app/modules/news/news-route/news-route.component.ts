import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { NewsService } from '../shared/news.service';

@Component({
	selector: 'nuc-news-route',
	templateUrl: './news-route.component.html',
	styleUrls: ['./news-route.component.css']
})
export class NewsRouteComponent implements OnInit {

	constructor(
		private newsService: NewsService,
		private router: Router
	) {
	}

	ngOnInit() {
		this.router.events.pipe(
			filter(e => e instanceof NavigationStart))
			.subscribe((event: NavigationStart) => this.showNews(event.url));
	}

	private showNews(url: string) {
		let param = this.getNewsParam(url);

		if (param) {
			this.newsService.showNewsByIdOrNameOrTitle(param);
		}
	}

	private getNewsParam(url: string) {
		let result: string | undefined = undefined;
		let regex = /\/(?:news|disclaimer)\/(.+?)(?:\/|$)/;

		let regexResult = regex.exec(url);
		if (regexResult) {
			result = decodeURIComponent(regexResult[1]);
		}

		return result;
	}
}
