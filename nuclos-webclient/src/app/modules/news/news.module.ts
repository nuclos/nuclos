import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NewsRouteModule } from './news-route/news-route.module';
import { NewsRoutes } from './news.routes';

@NgModule({
	imports: [
		CommonModule,
		NewsRouteModule,
		NewsRoutes
	]
})
export class NewsModule {
}
