import { inject, TestBed } from '@angular/core/testing';

import { NewsService } from './news.service';

xdescribe('NewsService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [NewsService]
		});
	});

	it('should be created', inject([NewsService], (service: NewsService) => {
		expect(service).toBeTruthy();
	}));
});
