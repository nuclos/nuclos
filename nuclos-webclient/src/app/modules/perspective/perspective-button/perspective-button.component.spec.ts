import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PerspectiveButtonComponent } from './perspective-button.component';

xdescribe('PerspectiveButtonComponent', () => {
	let component: PerspectiveButtonComponent;
	let fixture: ComponentFixture<PerspectiveButtonComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [PerspectiveButtonComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PerspectiveButtonComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
