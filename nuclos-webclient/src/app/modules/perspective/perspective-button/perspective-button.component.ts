import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Preference } from '../../preferences/preferences.model';
import { IPerspective } from '../perspective';
import { PerspectiveService } from '../shared/perspective.service';

@Component({
	selector: 'nuc-perspective-button',
	templateUrl: './perspective-button.component.html',
	styleUrls: ['./perspective-button.component.css']
})
export class PerspectiveButtonComponent implements OnInit {
	@Input() perspectivePref: Preference<IPerspective>;

	mouseover = false;

	constructor(
		private perspectiveService: PerspectiveService
	) {
	}

	ngOnInit() {
	}

	togglePerspective() {
		this.perspectiveService.togglePerspective(this.perspectivePref);
	}

	getId() {
		return this.perspectivePref.prefId;
	}
}
