import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { IPerspective } from '../perspective';
import { PerspectiveModel } from '../perspective-model';

@Directive()
export abstract class AbstractPerspectiveEditComponentDirective {
	@Input()
	model: PerspectiveModel;

	@Input()
	perspective: IPerspective;

	@Output()
	change = new EventEmitter<any>();
}
