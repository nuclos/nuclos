import { Component } from '@angular/core';
import { AbstractPerspectiveEditComponentDirective } from '../abstract-perspective-edit-component.directive';

@Component({
	selector: 'nuc-perspective-edit-detailview',
	templateUrl: './perspective-edit-detailview.component.html',
	styleUrls: ['./perspective-edit-detailview.component.css']
})
export class PerspectiveEditDetailviewComponent extends AbstractPerspectiveEditComponentDirective {

}
