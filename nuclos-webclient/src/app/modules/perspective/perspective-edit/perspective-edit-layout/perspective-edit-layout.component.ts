import { Component } from '@angular/core';
import { PerspectiveService } from '../../shared/perspective.service';
import { AbstractPerspectiveEditComponentDirective } from '../abstract-perspective-edit-component.directive';

@Component({
	selector: 'nuc-perspective-edit-layout',
	templateUrl: './perspective-edit-layout.component.html',
	styleUrls: ['./perspective-edit-layout.component.css']
})
export class PerspectiveEditLayoutComponent extends AbstractPerspectiveEditComponentDirective {

	constructor(
		private perspectiveService: PerspectiveService
	) {
		super();
	}

	onChange() {
		this.change.next(true);
		this.perspectiveService.updateSubformPreferencesForSelectedLayout();
	}
}
