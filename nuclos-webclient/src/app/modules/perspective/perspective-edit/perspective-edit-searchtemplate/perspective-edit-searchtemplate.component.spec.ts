import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PerspectiveEditSearchtemplateComponent } from './perspective-edit-searchtemplate.component';

xdescribe('PerspectiveEditSearchtemplateComponent', () => {
	let component: PerspectiveEditSearchtemplateComponent;
	let fixture: ComponentFixture<PerspectiveEditSearchtemplateComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [PerspectiveEditSearchtemplateComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PerspectiveEditSearchtemplateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
