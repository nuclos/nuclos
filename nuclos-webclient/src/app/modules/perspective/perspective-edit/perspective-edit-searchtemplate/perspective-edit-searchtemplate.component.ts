import { Component } from '@angular/core';
import { AbstractPerspectiveEditComponentDirective } from '../abstract-perspective-edit-component.directive';

@Component({
	selector: 'nuc-perspective-edit-searchtemplate',
	templateUrl: './perspective-edit-searchtemplate.component.html',
	styleUrls: ['./perspective-edit-searchtemplate.component.css']
})
export class PerspectiveEditSearchtemplateComponent extends AbstractPerspectiveEditComponentDirective {

	hasSearchtemplatePrefs() {
		return this.model.getSearchTemplatePrefs(this.perspective).length > 0;
	}

}
