import { Component } from '@angular/core';
import { AbstractPerspectiveEditComponentDirective } from '../abstract-perspective-edit-component.directive';

@Component({
	selector: 'nuc-perspective-edit-sideview',
	templateUrl: './perspective-edit-sideview.component.html',
	styleUrls: ['./perspective-edit-sideview.component.css']
})
export class PerspectiveEditSideviewComponent extends AbstractPerspectiveEditComponentDirective {

	hasSideviewMenuPrefs() {
		return this.model.getSideviewMenuPrefs(this.perspective).length > 0;
	}

}
