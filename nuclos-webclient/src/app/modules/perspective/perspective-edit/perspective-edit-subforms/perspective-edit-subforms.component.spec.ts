import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PerspectiveEditSubformsComponent } from './perspective-edit-subforms.component';

xdescribe('PerspectiveEditSubformsComponent', () => {
	let component: PerspectiveEditSubformsComponent;
	let fixture: ComponentFixture<PerspectiveEditSubformsComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [PerspectiveEditSubformsComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PerspectiveEditSubformsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
