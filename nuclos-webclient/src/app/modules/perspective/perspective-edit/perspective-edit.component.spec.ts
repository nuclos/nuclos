import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PerspectiveEditComponent } from './perspective-edit.component';

xdescribe('PerspectiveEditComponent', () => {
	let component: PerspectiveEditComponent;
	let fixture: ComponentFixture<PerspectiveEditComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [PerspectiveEditComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PerspectiveEditComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
