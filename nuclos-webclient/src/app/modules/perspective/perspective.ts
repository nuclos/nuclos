export interface IPerspective {
	name: string;
	boMetaId: string;
	layoutId: string;
	searchTemplatePrefId: string;
	sideviewMenuPrefId: string;

	/**
	 * Mapping of subform entity UID to preference UID.
	 */
	subformTablePrefIds: { [key: string]: string };

	layoutForNew: boolean;

}
