import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { IPerspective } from '@modules/perspective/perspective';
import { Preference } from '@modules/preferences/preferences.model';
import { take } from 'rxjs/operators';
import { PerspectiveModel } from '../perspective-model';
import { PerspectiveService } from '../shared/perspective.service';

@Component({
	selector: 'nuc-perspective',
	templateUrl: './perspective.component.html',
	styleUrls: ['./perspective.component.css']
})
export class PerspectiveComponent implements OnInit, OnChanges {
	@Input() activatedPerspective: IPerspective | undefined;
	visible = true;

	currentPerspectivePref: Preference<IPerspective> | undefined;

	constructor(
		private perspectiveService: PerspectiveService
	) {
	}

	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges) {
		this.currentPerspectivePref = this.getModel().perspectives.find(
			(perspectivePref) => perspectivePref.content === changes['activatedPerspective'].currentValue
		);
	}

	getModel(): PerspectiveModel {
		return this.perspectiveService.getModel();
	}

	openNew() {
		this.perspectiveService.newPerspective();
	}

	isNewAllowed() {
		return this.perspectiveService.isNewAllowed();
	}

	isEditAllowed() {
		return this.perspectiveService.isEditAllowed();
	}

	editPerspective() {
		this.perspectiveService.editPerspective(this.currentPerspectivePref);
	}

	deletePerspective() {
		this.perspectiveService.unshareAndDelete(this.currentPerspectivePref).pipe(take(1)).subscribe();
	}
}
