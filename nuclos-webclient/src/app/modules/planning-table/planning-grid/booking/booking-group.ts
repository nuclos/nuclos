import { BookingViewElement } from 'app/modules/planning-table/planning-grid/booking/booking-view-element';
import { Booking } from '../../planning-table.service';
import { BookingUtils } from '../../shared/booking-utils';

/**
 * A BookingGroup is a collection of bookings with the same resource, so that for all possible booking pairs
 * there is a path of overlapping bookings connecting those 2.
 * At least one Booking is required to create a booking group.
 * The goal is to collect overlapping bookings in a booking group first and then to resolve the booking group
 * into multiple booking lanes, where all the bookings in a lane are non overlapping.
 * These lanes can then be stacked on top of each other and create a overlap free visualization of the bookings.
 * @see BookingLane
 */
export class BookingGroup {

	/** **/
	resource: string | undefined;
	/** list of all bookings assigned to this group **/
	bookings: BookingViewElement[] = [];
	lanes: BookingLane[] = [];
	/** time in ms, where the first booking of this group begins **/
	startTime: number;
	/** time in ms, where the last booking of this group ends **/
	endTime: number;

	constructor(initialBooking: BookingViewElement) {
		this.resource = initialBooking.getResource();
		this.bookings.push(initialBooking);
		this.startTime = initialBooking.getStartTime();
		this.endTime = initialBooking.getEndTime();
	}

	/**
	 * Checks whether a booking overlaps with the time interval of this group.
	 * true - if the booking lies within the interval of all the bookings that make up this group.
	 * false - otherwise
	 * @param booking
	 */
	public overlaps(booking: Booking) {
		if (booking.resource !== this.resource) {
			return false;
		}

		if (BookingUtils.getStart(booking).getTime() >= this.endTime
			|| BookingUtils.getEnd(booking).getTime() < this.startTime) {
			return false;
		}
		return true;
	}

	/**
	 * Adds a booking to this BookingGroup, if it isn't already part of the group.
	 * Updates the start-&end-time of this booking group and sorts the bookings by their order numeral.
	 * @param booking
	 */
	public addBooking(booking: BookingViewElement) {
		if (this.bookings.find(e => e.booking.id === booking.booking.id)) {
			return;
		}
		this.bookings.push(booking);
		if (this.startTime > booking.getStartTime()) {
			this.startTime = booking.getStartTime();
		}
		if (this.endTime < booking.getEndTime()) {
			this.endTime = booking.getEndTime();
		}
		this.recalculateOrder();
	}

	/**
	 * Returns the index of the lane, the booking is assigned to.
	 * @param booking
	 */
	public getLane(booking: Booking): number {
		let i = 0, len = this.lanes.length;
		while (i < len) {
			if (this.lanes[i].fixableBookings.find(x => (x.booking === booking)) !== undefined) {
				return i;
			}
			i++;
		}
		return 0;
	}

	/**
	 * This method invokes the creation of lanes and the assignment of bookings to them.
	 * It does so in a way that there are no overlapping bookings in a lane.
	 */
	public computeLanes() {
		this.lanes = [];
		let firstLane = new BookingLane(this.bookings);
		this.lanes = this.lanes.concat(firstLane.solveCollisions());
	}

	/**
	 * Sorts the bookings ascending by their order numeral.
	 * @private
	 */
	private recalculateOrder() {
		this.bookings.sort((a: BookingViewElement, b: BookingViewElement) => {
			let orderA = a.booking.order ?? 0;
			let orderB = b.booking.order ?? 0;
			return (orderA - orderB);
		});
	}

}

/**
 * A BookingLane has the end goal, to only contain non overlapping bookings.
 * It can be initialized with overlapping bookings first.
 * @see solveCollisions() will recursively create new BookingLanes until every BookingLane is non-overlapping.
 */
export class BookingLane {

	/* Contains all bookings of a lane and a boolean if the booking is fixed. */
	fixableBookings: FixableBooking[] = [];
	collisions: BookingCollision[] = [];

	constructor(bookings?: BookingViewElement[]) {
		bookings?.forEach((booking: BookingViewElement) => {
			this.fixableBookings.push(new FixableBooking(booking.booking));
		});
	}

	/**
	 * 1. fixate the first booking to the lane
	 * 2. while there are bookings left:
	 * 		if the booking is not overlapping with the previous booking, fixate it
	 * 3. push bookings that weren't fixed to this lane to the next lane
	 * 4. call solveCollisions() for the next lane
	 */
	solveCollisions(): BookingLane[] {
		let extraLanes: BookingLane[] = [];

		// sort bookings by beginning ascending
		this.setup();
		// fixate all bookings that are allowed to stay
		this.fixCollisionFreeBookings();
		// compute pairs of collisions
		this.computeCollisions();
		// create a new 'resolution' lane if there are collisions
		let resolutionLane: BookingLane | undefined = (this.collisions.length > 0) ? new BookingLane() : undefined;

		// push all the bookings that weren't fixated to the new resolution lane
		let i = this.collisions.length;
		while (i--) {
			if (this.collisions[i].fixableBooking1.fixed) {
				// booking1 has been fixated
				// push booking2 to the resolution lane, if it hasn't been already
				if (!resolutionLane?.fixableBookings.find(b => (b.booking.id === this.collisions[i].fixableBooking2.booking.id))) {
					resolutionLane?.fixableBookings.push(this.collisions[i].fixableBooking2);
				}
			} else if (this.collisions[i].fixableBooking2.fixed) {
				// booking2 has been fixated
				// push booking1 to the resolution lane, if it hasn't been already
				if (!resolutionLane?.fixableBookings.find(b => (b.booking.id === this.collisions[i].fixableBooking1.booking.id))) {
					resolutionLane?.fixableBookings.push(this.collisions[i].fixableBooking1);
				}
			} else {
				// none of the 2 bookings has been fixated
				// push both to the resolution lane, if they haven't been already
				if (!resolutionLane?.fixableBookings.find(b => (b.booking.id === this.collisions[i].fixableBooking1.booking.id))) {
					resolutionLane?.fixableBookings.push(this.collisions[i].fixableBooking1);
				}
				if (!resolutionLane?.fixableBookings.find(b => (b.booking.id === this.collisions[i].fixableBooking2.booking.id))) {
					resolutionLane?.fixableBookings.push(this.collisions[i].fixableBooking2);
				}
			}
		}
		// removes all bookings, which haven't been fixated now from the list of this lane
		this.fixableBookings = this.fixableBookings.filter(b => b.fixed);
		// mark as collision free
		this.collisions = [];

		if (resolutionLane !== undefined) {
			// recursive function call here to solve the collisions in the resolution lane
			let result: BookingLane[] = resolutionLane.solveCollisions();
			extraLanes = extraLanes.concat(result);
		}
		// push this lane to the result set and return the result set
		extraLanes.push(this);
		return extraLanes;
	}

	/**
	 * Sorts the FixableBooking by time ascending.
	 */
	setup() {
		this.fixableBookings.sort((b1: FixableBooking, b2: FixableBooking) => {
			return BookingUtils.getStart(b1.booking).getTime() - BookingUtils.getStart(b2.booking).getTime();
		});
	}

	/**
	 * Fixates the first booking.
	 * Then iterates through the remaining bookings and always fixates a booking, when it does not overlap the previously fixated booking.
	 */
	fixCollisionFreeBookings() {
		this.fixableBookings[0].fixed = true;
		let lastFixedBookingId = 0;
		let i = 1, len = this.fixableBookings.length;
		while (i < len) {
			if (!BookingUtils.overlaps(this.fixableBookings[lastFixedBookingId].booking, this.fixableBookings[i].booking)) {
				this.fixableBookings[i].fixed = true;
				lastFixedBookingId = i;
			}
			i++;
		}
	}

	/**
	 * Computes pairs of overlapping bookings and stores them in the collisions list of this object.
	 */
	computeCollisions() {
		let i = 0, j = 0, len = this.fixableBookings.length;
		while (i < len) {
			j = i + 1;
			while (j < len) {
				if (BookingUtils.overlaps(this.fixableBookings[i].booking, this.fixableBookings[j].booking)) {
					this.collisions.push(new BookingCollision(this.fixableBookings[i], this.fixableBookings[j]));
				}
				j++;
			}
			i++;
		}
	}

}

/**
 * Helper class, that allows a booking to be fixated.
 * Fixated bookings are allowed to stay in a BookingLane because they do not collide with other FixableBookings.
 * @see BookingLane
 */
class FixableBooking {

	booking: Booking;
	fixed = false;

	constructor(booking?: Booking) {
		if (booking !== undefined) {
			this.booking = booking;
		}
	}

}

/**
 * Data structure of 2 FixableBookings that have an overlapping interval.
 */
class BookingCollision {

	constructor(public fixableBooking1: FixableBooking, public fixableBooking2: FixableBooking) {

	}

}
