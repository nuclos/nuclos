import { SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { GridModel, Vector2 } from '@modules/planning-table/planning-grid/planning-grid.component';
import { PlanningTableAlignment } from '@modules/planning-table/shared/planning-table-alignment';
import { TimeUnitCustom } from '@modules/planning-table/time/time-unit-custom';
import { ColorUtils } from '@shared/color-utils';
import { AuthenticationService } from 'app/modules/authentication';
import { ViewElement, ViewElementBounds } from 'app/modules/planning-table/planning-grid/shared/view-element';
import { BookingUtils } from 'app/modules/planning-table/shared/booking-utils';
import { Booking, PlanningTable } from '../../planning-table.service';
import { PlanningViewConfiguration } from '../../shared/planning-view-configuration';
import { BookingGroup } from './booking-group';

export class BookingViewElement extends ViewElement {

	public bookingGroup: BookingGroup;

	resource: any;
	dateFrom: Date;
	dateUntil: Date;

	top: number;
	left: number;
	width: number;
	height: number;

	constructor(
		public booking: Booking,
		private planningTable: PlanningTable,
		private viewConfiguration: PlanningViewConfiguration,
		private resourceIndex: number,
		private authenticationService: AuthenticationService,
		private domSanitizer: DomSanitizer,
	) {
		super();
		this.dateFrom = BookingUtils.getStart(this.booking);
		this.dateUntil = BookingUtils.getEnd(this.booking);
		if (planningTable.resources) {
			this.resource = planningTable.resources[resourceIndex];
		}
		this.bounds = new ViewElementBounds(this.dateFrom.getTime(),
			this.dateUntil.getTime(), this.resourceIndex, this.resourceIndex);
	}

	isEnabled(): boolean {
		if (this.viewConfiguration.baseUnit instanceof TimeUnitCustom) {
			if ((this.viewConfiguration.baseUnit as TimeUnitCustom).getVisibleMinutes(this.dateFrom, this.dateUntil) === 0) {
				return false;
			}
		}
		return this.isVisibleForCurrentUser();
	}

	isVisibleForCurrentUser() {
		return ((this.authenticationService.getCurrentUser()?.username === this.booking.booker) || (!this.viewConfiguration.showOwnEventsOnly));
	}

	layout(gridModel: GridModel) {
		let location: Vector2 = gridModel.getGridCoordinate(this.dateFrom, this.resource);
		let timeStart = gridModel.getTimeLocation(this.dateFrom);
		let timeEnd = gridModel.getTimeLocation(this.dateUntil);
		let bookingOrder = this.bookingGroup.getLane(this.booking);
		let bookingGroupSize = this.bookingGroup.lanes.length;

		if (PlanningTableAlignment.TIME_RESOURCE === this.viewConfiguration.tableAlignment) {
			this.width = (timeEnd - timeStart) * this.viewConfiguration.baseTimeUnitCellSize;
			this.height = this.viewConfiguration.resourceCellSize / bookingGroupSize;
			location.y += this.height * bookingOrder;
			location.x *= this.viewConfiguration.baseTimeUnitCellSize;
		} else {
			this.width = this.viewConfiguration.resourceCellSize / bookingGroupSize;
			this.height = (timeEnd - timeStart) * this.viewConfiguration.baseTimeUnitCellSize;
			location.x += this.width * bookingOrder;
			location.y *= this.viewConfiguration.baseTimeUnitCellSize;
		}
		this.top = location.y;
		this.left = location.x;
	}

	getSvgContent(): string {
		let backgroundColor = this.booking.backgroundColor ?? '#FFFFFF';
		let foregroundColor;
		if (this.booking.backgroundColor !== undefined && this.booking.backgroundColor !== '') {
			foregroundColor = ColorUtils.calculateTextColorFromBackgroundColor(this.booking.backgroundColor);
		} else {
			foregroundColor = 'rgb(0, 0, 0)';
		}
		let style = `top: ${this.top}px; left: ${this.left}px; width: ${this.width}px; height: ${this.height}px; `;
		style += `z-index: 1; position: absolute; border: 2px groove rgb(0, 0, 0); overflow: visible; padding-left: 2px; `;
		style += 'box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); font-size: 13px; ';
		style += `background-color: ${backgroundColor}; color: ${foregroundColor}; white-space: nowrap;`;

		return `<div style="${style}">${this.domSanitizer.sanitize(SecurityContext.HTML, '' + this.booking.label)}</div>`;
	}

	setBookingGroup(bookingGroup: BookingGroup) {
		this.bookingGroup = bookingGroup;
	}

	getResource() {
		return this.booking.resource;
	}

	getStartTime() {
		return BookingUtils.getStart(this.booking).getTime();
	}

	getEndTime() {
		return BookingUtils.getEnd(this.booking).getTime();
	}

}
