import {
	ChangeDetectorRef,
	Component,
	Input,
	OnChanges,
	OnInit,
} from '@angular/core';
import { BookingViewElement } from '@modules/planning-table/planning-grid/booking/booking-view-element';
import { PlanningView } from '@modules/planning-table/shared/planning-view';
import { TooltipDefaultOptions } from 'app/modules/ui-components/tooltip/tooltip-default-options';
import { ColorUtils } from 'app/shared/color-utils';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { Booking, PlanningTable } from '../../planning-table.service';
import { BookingUtils } from '../../shared/booking-utils';
import { PlanningViewConfiguration } from '../../shared/planning-view-configuration';

@Component({
	selector: 'nuc-booking',
	templateUrl: './booking.component.html',
	styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit, OnChanges {

	@Input() booking: Booking;
	@Input() viewElement: BookingViewElement;
	@Input() planningTable: PlanningTable;
	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() planningView: PlanningView;
	@Input() selected: boolean;

	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	resource: any;

	dateFrom: Date;
	dateUntil: Date;
	styleClasses: any;
	bookingIndex;
	resourceIndex;
	valid: boolean;
	textColor;

	constructor(
		private ref: ChangeDetectorRef,
	) {
	}

	ngOnInit(): void {
		if (this.planningTable.bookings) {
			this.bookingIndex = this.planningTable.bookings.indexOf(this.booking);
		}
		this.valid = BookingUtils.isValid(this.booking);
		this.dateFrom = BookingUtils.getStart(this.booking);
		this.dateUntil = BookingUtils.getEnd(this.booking);
		if (this.booking.resource) {
			this.resource = this.planningTable.resources?.find(x => x.resourceId === this.booking.resource);
		}
		if (this.planningTable.resources) {
			this.resourceIndex = this.planningTable.resources.indexOf(this.resource);
		}
		if (this.booking.backgroundColor !== undefined && this.booking.backgroundColor !== '') {
			this.textColor = ColorUtils.calculateTextColorFromBackgroundColor(this.booking.backgroundColor);
		} else {
			this.textColor = '#000000';
		}
		this.updateStyleClasses();
	}

	ngOnChanges(): void {
		this.updateStyleClasses();
	}

	updateStyleClasses() {
		this.styleClasses = {
			'booking-invalid': !this.valid,
			'booking-selected': this.selected,
		};
	}

}
