import { GridModel, Vector2 } from '@modules/planning-table/planning-grid/planning-grid.component';
import { PlanningTableAlignment } from '@modules/planning-table/shared/planning-table-alignment';
import { TimeUnit } from '@modules/planning-table/time/time-unit';
import { ViewElement, ViewElementBounds } from 'app/modules/planning-table/planning-grid/shared/view-element';
import { Milestone, PlanningTable } from '../../planning-table.service';
import { PlanningViewConfiguration } from '../../shared/planning-view-configuration';

export class MilestoneViewElement extends ViewElement {

	resource: any;
	top: number;
	left: number;
	width: number;
	height: number;

	constructor(
		public milestone: Milestone,
		private planningTable: PlanningTable,
		private viewConfiguration: PlanningViewConfiguration,
		private resourceIndex: number
	) {
		super();
		let dateFrom: Date = new Date(milestone.fromDate);
		dateFrom.setHours(11, 30, 0, 0);
		let dateUntil: Date = new Date(milestone.fromDate);
		dateUntil.setHours(12, 30, 0, 0);
		if (planningTable.resources) {
			this.resource = planningTable.resources[resourceIndex];
		}
		this.bounds = new ViewElementBounds(dateFrom.getTime(), dateUntil.getTime(), this.resourceIndex, this.resourceIndex);
	}

	layout(gridModel: GridModel) {
		let dateFrom: Date = new Date(this.milestone.fromDate);
		dateFrom.setHours(12, 0, 0, 0);

		let location: Vector2 = gridModel.getGridCoordinate(dateFrom, this.resource);
		let timeLocation = gridModel.getTimeLocation(dateFrom);

		if (PlanningTableAlignment.TIME_RESOURCE === this.viewConfiguration.tableAlignment) {
			this.width = this.viewConfiguration.baseTimeUnitCellSize;
			this.height = this.viewConfiguration.resourceCellSize * 1;
			this.width = Math.min(this.width, 32);
			this.height = Math.min(this.height, 32);
			location.x = timeLocation * this.viewConfiguration.baseTimeUnitCellSize;
			location.x -= (this.width / 2);
			location.y += (this.viewConfiguration.resourceCellSize - this.height) / 2;
		} else {
			this.width = this.viewConfiguration.resourceCellSize / 2;
			this.height = this.viewConfiguration.baseTimeUnitCellSize;
			this.width = Math.min(this.width, 32);
			this.height = Math.min(this.height, 32);
			location.x += (this.viewConfiguration.resourceCellSize - this.width) / 2;
			location.y = timeLocation * this.viewConfiguration.baseTimeUnitCellSize;
			location.y -= this.height * 0.5;
		}

		this.top = location.y;
		this.left = location.x;
	}

	getSvgContent(): string {
		let outerHexagonStyle = 'z-index: 5; position: absolute; overflow: hidden; background-color: black; clip-path: polygon(25% 0%, 75% 0%, 100% 50%, 75% 100%, 25% 100%, 0% 50%);';
		outerHexagonStyle += ` left: ${this.left}px; top: ${this.top}px; width: ${this.width}px; height: ${this.height}px;`;
		let innerHexagonStyle = 'background-color: rgb(255, 255, 255); z-index: 5; position: absolute; overflow: hidden; clip-path: polygon(25% 0%, 75% 0%, 100% 50%, 75% 100%, 25% 100%, 0% 50%);';
		innerHexagonStyle += ` left: 2px; top: 2px; width: ${this.width - 4}px; height: ${this.height - 4}px`;

		let spanStyle = 'z-index: 5; position: absolute; font-size: 13px; display: inline-flex; justify-content: center; align-items: center; white-space: nowrap;';
		spanStyle += ` top: ${this.top}px; left: ${this.left + this.width + 8}px; height: ${this.height}px;`;

		let content = `<div style="${outerHexagonStyle}"><div style="${innerHexagonStyle}"></div></div><span style="${spanStyle}">${this.milestone.fromDate}</span>`;
		return content;
	}

}
