import { ChangeDetectorRef, Component, Input, OnChanges, OnInit } from '@angular/core';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { Milestone, PlanningTable } from '../../planning-table.service';
import { PlanningViewConfiguration } from '../../shared/planning-view-configuration';
import { MilestoneViewElement } from './milestone-view-element';

@Component({
	selector: 'nuc-milestone',
	templateUrl: './milestone.component.html',
	styleUrls: ['./milestone.component.css']
})
export class MilestoneComponent implements OnInit, OnChanges {

	@Input() milestone: Milestone;
	@Input() viewElement: MilestoneViewElement;
	@Input() planningTable: PlanningTable;
	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() selected: boolean;

	milestoneIndex;
	resourceIndex;
	resource: any;
	formattedDate: string;

	styleClasses: any;

	constructor(
		private ref: ChangeDetectorRef,
		private i18n: NuclosI18nService
	) {
	}

	ngOnInit(): void {
		if (this.planningTable.milestones) {
			this.milestoneIndex = this.planningTable.milestones.indexOf(this.milestone);
		}
		if (this.milestone.resource) {
			this.resource = this.planningTable.resources?.find(x => x.resourceId === this.milestone.resource);
		}
		if (this.planningTable.resources) {
			this.resourceIndex = this.planningTable.resources.indexOf(this.resource);
		}
		this.formattedDate = moment(this.milestone.fromDate, 'YYYY-MM-DD').format(this.i18n.getI18n('webclient.planningTable.time.format.milestone'));
	}

	ngOnChanges(): void {
		this.updateStyleClasses();
	}

	updateStyleClasses() {
		this.styleClasses = {
			'milestone-selected': this.selected,
		};
	}

}
