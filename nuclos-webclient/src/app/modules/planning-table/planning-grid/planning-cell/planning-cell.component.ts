import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	Input,
	OnInit,
} from '@angular/core';
import { TimeAllocation } from 'app/modules/planning-table/time/time-allocation';
import { PlanningTable, Resource } from '../../planning-table.service';
import { PlanningViewConfiguration } from '../../shared/planning-view-configuration';
import { DateUtils, Today } from '../../time/date-utils';

@Component({
	selector: 'nuc-planning-cell',
	templateUrl: './planning-cell.component.html',
	styleUrls: ['./planning-cell.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanningCellComponent implements OnInit {

	@Input() timeAllocation: TimeAllocation;
	@Input() resource: Resource;
	@Input() planningTable: PlanningTable;
	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() selected: boolean;
	@Input() timeIndex: number;
	@Input() resourceIndex: number;
	@Input() cellWidth: number;
	@Input() cellHeight: number;

	styleClasses: any;

	constructor(private ref: ChangeDetectorRef) {
	}

	ngOnInit(): void {
		this.updateStyleClasses();
	}

	ngOnChanges() {
		this.updateStyleClasses();
	}

	updateStyleClasses() {
		let lastIncludedDate = new Date(this.timeAllocation.untilTime.getTime() - 1);
		this.styleClasses = {
			'cell-today': this.timeAllocation.contains(Today.current().getTime())
				,
			'cell-weekend': DateUtils.isWeekend(this.timeAllocation.fromTime)
				&& DateUtils.isDateEqual(this.timeAllocation.fromTime, lastIncludedDate),
			'cell-holiday': this.isHoliday(),
			'cell-selected': this.selected,
		};
	}

	private isHoliday() {
		let result = this.planningTable.backgroundEvents?.find((x => {
				if (x.eventDate !== undefined) {
					let start = new Date(Date.parse(x.eventDate)).setHours(0, 0, 0, 0);
					let end = new Date(Date.parse(x.eventDate)).setHours(23, 59, 59, 999);
					return (this.timeAllocation.fromTime.getTime() >= start && this.timeAllocation.untilTime.getTime() - 1 <= end);
				} else {
					return false;
				}
			}
		));
		return result !== undefined;
	}

}
