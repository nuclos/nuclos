import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ComponentFactoryResolver,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output, QueryList,
	ViewChild, ViewChildren,
	ViewContainerRef
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthenticationService } from '@modules/authentication';
import { RelationViewElement } from '@modules/planning-table/planning-grid/relation/relation-view-element';
import { RelationComponent } from '@modules/planning-table/planning-grid/relation/relation.component';
import { ViewElement, ViewElementBounds } from '@modules/planning-table/planning-grid/shared/view-element';
import { ViewTree } from '@modules/planning-table/planning-grid/shared/view-tree';
import { GridLocator } from '@modules/planning-table/shared/grid-locator';
import { PlanningView } from '@modules/planning-table/shared/planning-view';
import { CellSelection } from '@modules/planning-table/shared/selection/cell-selection';
import { Milestone, PlanningTable, Resource } from '../planning-table.service';
import { PlanningTableAlignment } from '../shared/planning-table-alignment';
import { PlanningViewConfiguration } from '../shared/planning-view-configuration';
import { DateUtils } from '../time/date-utils';
import { TimeAllocation } from '../time/time-allocation';
import { BookingGroup } from './booking/booking-group';
import { BookingViewElement } from './booking/booking-view-element';
import { MilestoneViewElement } from './milestone/milestone-view-element';
import { TransformedElementsComponent } from './transformed-elements/transformed-elements.component';

@Component({
	selector: 'nuc-planning-grid',
	templateUrl: './planning-grid.component.html',
	styleUrls: ['./planning-grid.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanningGridComponent implements OnInit {

	@Input() planningTable: PlanningTable;
	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() planningView: PlanningView;
	@Output() reloadPlanningTable: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild('grid') grid: ElementRef;

	@ViewChild('viewElementContainer', {read: ViewContainerRef}) viewElementContainer: ViewContainerRef;
	@ViewChild('transformedElements') transformationRef: TransformedElementsComponent;
	@ViewChildren(RelationComponent) relationViewChildren: QueryList<RelationComponent>;

	rowHeight: number;
	columnWidth: number;

	gridModel: GridModel;
	gridColumns: Map<number, GridColumn> = new Map<number, GridColumn>();
	currentSelectedCell: CellContext | undefined;

	viewEntityFactories: any[];
	bookingFactory: BookingFactory;
	viewTree: ViewTree;
	viewBounds: ViewElementBounds;
	visibleEntities: ViewElement[] = [];
	visibleBookings: BookingViewElement[] = [];
	visibleRelations: RelationViewElement[] = [];
	visibleMilestones: MilestoneViewElement[] = [];

	constructor(
		private authenticationService: AuthenticationService,
		public changeDetectorRef: ChangeDetectorRef,
		private domSanitizer: DomSanitizer,
	) {
	}

	ngOnInit(): void {
		this.gridModel = new GridModel(this.planningTable, this.viewConfiguration);
		this.computeCellBounds();
	}

	ngOnChanges(): void {
		this.clearGrid();
		this.gridModel = new GridModel(this.planningTable, this.viewConfiguration);

		this.computeCellBounds();
		this.buildViewTree();
		this.buildView();
	}

	ngAfterViewInit(): void {
		this.buildView();
		this.recalculateGridCells();
	}

	refreshGrid() {
		this.buildView();
	}

	recalculateGridCells() {
		this.gridModel.update();
		this.computeCellBounds();
		this.buildView();
		this.updateContainerSize();
	}

	recalculateBookingsGroups() {
		this.bookingFactory.recalculateBookingGroups();
		this.buildView();
	}

	/** Destroys all grid cells. **/
	clearGrid() {
		this.gridColumns.forEach((value: GridColumn, key: number) => {
			value.destroyAllCells();
		});
		this.gridColumns.clear();
		this.clearEntities();
	}

	updateRelations() {
		if (this.relationViewChildren) {
			this.relationViewChildren.forEach(rel => {
				rel.changeDetectorRef.detectChanges();
			});
		}
	}

	clearEntities() {
		this.visibleEntities = [];
		this.visibleBookings = [];
		this.visibleRelations = [];
		this.visibleMilestones = [];
		if (this.viewElementContainer) {
			this.viewElementContainer.clear();
		}
	}

	getWidthOverall() {
		return this.columnWidth * this.gridModel.columnCount;
	}

	getHeightOverall() {
		return this.rowHeight * this.gridModel.rowCount;
	}

	setSelectedCell(xIndex?: number, yIndex?: number) {
		if (xIndex === undefined || yIndex === undefined) {
			this.planningView.cellSelection = undefined;
			if (this.currentSelectedCell) {
				this.currentSelectedCell.selected = false;
				this.currentSelectedCell = undefined;
			}
		} else {
			this.clearSelectedEntities();

			let resourceIndex = this.getResourceIndex(xIndex, yIndex);
			let timeIndex = this.getTimeIndex(xIndex, yIndex);
			let resource = (this.planningTable.resources) ? this.planningTable.resources[resourceIndex] : undefined;
			if (resource) {
				try {
					let cellContext: CellContext = this.getCellContext(timeIndex, resourceIndex);
					cellContext.selected = true;
					this.currentSelectedCell = cellContext;
					this.planningView.cellSelection = new CellSelection(timeIndex, resourceIndex,
						cellContext.timeAllocation.fromTime, cellContext.timeAllocation.untilTime, resource);
				} catch (e: any) {
				}
			}
		}
		this.changeDetectorRef.detectChanges();
	}

	getTimeIndex(xIndex: number, yIndex: number) {
		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			return xIndex;
		} else {
			return yIndex;
		}
	}

	getResourceIndex(xIndex: number, yIndex: number) {
		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			return yIndex;
		} else {
			return xIndex;
		}
	}

	clearSelectedEntities() {
		this.planningView.elementSelection.clearSelection();
	}

	columnTrackBy(index, column) {
		return column.key;
	}

	cellTrackBy(index, cellContext) {
		return cellContext.value.resource.label + cellContext.value.timeIndex + cellContext.selected;
	}

	private updateContainerSize() {
		this.grid.nativeElement.style.width = this.columnWidth * this.gridModel.columnCount + 'px';
		this.grid.nativeElement.style.height = this.rowHeight * this.gridModel.rowCount + 'px';
	}

	/**
	 * Computes the bounds of a single table cell.
	 * Therefore it takes the table alignment into account.
	 **/
	private computeCellBounds() {
		if (this.viewConfiguration.tableAlignment === 0) {
			this.columnWidth = this.viewConfiguration.resourceCellSize;
			this.rowHeight = this.viewConfiguration.baseTimeUnitCellSize;
		} else {
			this.columnWidth = this.viewConfiguration.baseTimeUnitCellSize;
			this.rowHeight = this.viewConfiguration.resourceCellSize;
		}
	}

	private buildViewTree() {
		let completeBounds = new ViewElementBounds(this.viewConfiguration.dateFrom.getTime(),
			this.viewConfiguration.dateUntil.getTime(),
			0,
			this.planningTable.resources?.length ?? 0);

		this.viewEntityFactories = [];
		this.viewEntityFactories.push(this.bookingFactory = new BookingFactory(this.planningTable,
			this.viewConfiguration,
			this.reloadPlanningTable,
			this.gridModel,
			this.authenticationService,
			this.domSanitizer));
		this.viewEntityFactories.push(new MilestoneFactory(this.planningTable, this.viewConfiguration));
		this.viewEntityFactories.push(new RelationFactory(this.planningTable, this.viewConfiguration, this, this.bookingFactory));

		this.viewTree = new ViewTree(completeBounds);
		this.viewEntityFactories.forEach((factory: ViewElementFactory) => {
			factory.createViewElements().forEach((viewElement: ViewElement) => {
				this.viewTree.insert(viewElement);
			});
		});
	}

	private refreshViewBounds() {
		let firstResourceIndex = PlanningViewConfiguration.getFirstVisibleResourceIndex(this.viewConfiguration);
		let lastResourceIndex = PlanningViewConfiguration.getLastVisibleResourceIndex(this.viewConfiguration);
		let firstVisibleTime = PlanningViewConfiguration.getFirstVisibleTimeStamp(this.viewConfiguration);
		let lastVisibleTime = PlanningViewConfiguration.getLastVisibleTimeStamp(this.viewConfiguration);
		this.viewBounds = new ViewElementBounds(firstVisibleTime, lastVisibleTime, firstResourceIndex, lastResourceIndex);
	}

	private updateGridEntities() {
		this.refreshViewBounds();

		this.destroyEntitiesOutsideView();

		let newVisibleEntities = this.viewTree.queryRange(this.viewBounds);

		this.visibleEntities = newVisibleEntities.filter(entity => entity.isEnabled());
		this.visibleBookings = this.visibleEntities.filter(entity => (entity instanceof BookingViewElement)) as Array<BookingViewElement>;
		this.visibleMilestones = this.visibleEntities.filter(entity => (entity instanceof MilestoneViewElement)) as Array<MilestoneViewElement>;
		this.visibleRelations = this.visibleEntities.filter(entity => (entity instanceof RelationViewElement)) as Array<RelationViewElement>;
		this.visibleBookings.forEach(x => x.layout(this.gridModel));
		this.visibleMilestones.forEach(x => x.layout(this.gridModel));
		this.visibleRelations.forEach(x => x.layout(this.gridModel));
		this.updateRelations();
	}

	private destroyEntitiesOutsideView() {
		let i = this.visibleEntities.length - 1;
		while (i >= 0) {
			if (!this.viewBounds.overlaps(this.visibleEntities[i].getViewBounds()) || !this.visibleEntities[i].isEnabled()) {
				this.visibleEntities.splice(i, 1);
			}
			i--;
		}
	}

	private buildView() {
		let cellWidth: number;
		let cellHeight: number;
		if (this.viewConfiguration.tableAlignment === 0) {
			cellWidth = this.viewConfiguration.resourceCellSize;
			cellHeight = this.viewConfiguration.baseTimeUnitCellSize;
		} else {
			cellWidth = this.viewConfiguration.baseTimeUnitCellSize;
			cellHeight = this.viewConfiguration.resourceCellSize;
		}
		let firstVisibleColumn = Math.floor(this.viewConfiguration.horizontalScroll / cellWidth);
		let lastVisibleColumn = Math.ceil((this.viewConfiguration.viewportWidth + this.viewConfiguration.horizontalScroll) / cellWidth);
		let firstVisibleRow = Math.floor(this.viewConfiguration.verticalScroll / cellHeight);
		let lastVisibleRow = Math.ceil((this.viewConfiguration.viewportHeight + this.viewConfiguration.verticalScroll) / cellHeight);

		if (lastVisibleRow > this.gridModel.rowCount) {
			lastVisibleRow = this.gridModel.rowCount;
		}

		if (lastVisibleColumn > this.gridModel.columnCount) {
			lastVisibleColumn = this.gridModel.columnCount;
		}

		this.destroyCellsOutsideView(firstVisibleRow, lastVisibleRow, firstVisibleColumn, lastVisibleColumn);

		this.addNewCellsToViewport(firstVisibleRow, lastVisibleRow, firstVisibleColumn, lastVisibleColumn);

		this.updateGridEntities();

		this.changeDetectorRef.detectChanges();
	}

	private addNewCellsToViewport(
		firstVisibleRow: number,
		lastVisibleRow: number,
		firstVisibleColumn: number,
		lastVisibleColumn: number
	) {
		for (let x = firstVisibleColumn; x < lastVisibleColumn; x++) {
			if (!this.columnExists(x)) {
				this.gridColumns.set(x, new GridColumn());
			}
			let column = this.gridColumns.get(x);
			if (column) {
				for (let y = firstVisibleRow; y < lastVisibleRow; y++) {
					if (!(column.cells.has(y))) {
						let timeIndex;
						let resourceIndex;
						let cellWidth;
						let cellHeight;

						if (this.viewConfiguration.tableAlignment === 1) {
							timeIndex = x;
							resourceIndex = y;
							cellWidth = this.viewConfiguration.baseTimeUnitCellSize;
							cellHeight = this.viewConfiguration.resourceCellSize;
						} else {
							timeIndex = y;
							resourceIndex = x;
							cellHeight = this.viewConfiguration.baseTimeUnitCellSize;
							cellWidth = this.viewConfiguration.resourceCellSize;
						}

						if (timeIndex >= Math.max(this.viewConfiguration.numBaseUnitAllocations, 1)) {
							continue;
						}

						let resource;

						let timeAllocation =
							DateUtils.getTimeAllocation(this.viewConfiguration.intervall, this.viewConfiguration.baseUnit, timeIndex);
						if (this.planningTable.resources) {
							resource = this.planningTable.resources[resourceIndex];
						}

						if (resourceIndex >= (this.planningTable.resources?.length ?? 0)) {
							continue;
						}

						const context = new CellContext(resource, timeAllocation);
						context.resourceIndex = resourceIndex;
						context.timeIndex = timeIndex;
						context.cellWidth = cellWidth;
						context.cellHeight = cellHeight;
						context.colIndex = x;
						context.rowIndex = y;

						column.addCell(y, context);
					} else {
						let cellWidth;
						let cellHeight;

						if (this.viewConfiguration.tableAlignment === 1) {
							cellWidth = this.viewConfiguration.baseTimeUnitCellSize;
							cellHeight = this.viewConfiguration.resourceCellSize;
						} else {
							cellHeight = this.viewConfiguration.baseTimeUnitCellSize;
							cellWidth = this.viewConfiguration.resourceCellSize;
						}
						let cell = column.cells.get(y);
						if (cell) {
							cell.cellWidth = cellWidth;
							cell.cellHeight = cellHeight;
						}
					}
				}
			}
		}
	}

	private getCellContext(timeIndex: number, resourceIndex: number): CellContext {
		if (this.viewConfiguration.tableAlignment === 1) {
			let column: GridColumn | undefined = this.gridColumns.get(timeIndex);
			let cellContext: CellContext | undefined = column?.cells.get(resourceIndex);
			if (cellContext) {
				return cellContext;
			}
		} else {
			let column: GridColumn | undefined = this.gridColumns.get(resourceIndex);
			let cellContext: CellContext | undefined = column?.cells.get(timeIndex);
			if (cellContext) {
				return cellContext;
			}
		}
		throw new Error('Cell Context not found.');
	}

	private columnExists(colId: number) {
		return (this.gridColumns.has(colId));
	}

	private cellExists(rowId: number, colId: number) {
		let column = this.gridColumns.get(colId);
		if (column) {
			if (column.cells.has(rowId)) {
				return true;
			}
		}
		return false;
	}

	private destroyCellsOutsideView(
		firstVisibleRow: number,
		lastVisibleRow: number,
		firstVisibleColumn: number,
		lastVisibleColumn: number
	) {
		this.gridColumns.forEach((value: GridColumn, key: number) => {
			if (key < firstVisibleColumn || key > lastVisibleColumn) {
				value.destroyAllCells();
				this.gridColumns.delete(key);
			} else {
				value.destroyCellsOutsideVisibleRows(firstVisibleRow, lastVisibleRow);
			}
		});
	}

}

class CellContext {

	resourceIndex: number;
	timeIndex: number;
	cellWidth: number;
	cellHeight: number;

	rowIndex: number;
	colIndex: number;

	selected = false;

	constructor(private resource: Resource, public timeAllocation: TimeAllocation) {

	}

}

export class GridModel {

	rowCount: number;
	columnCount: number;
	gridLocator: GridLocator;

	constructor(private planningTable: PlanningTable, private viewConfiguration: PlanningViewConfiguration) {
		if (PlanningTableAlignment.TIME_RESOURCE === viewConfiguration.tableAlignment) {
			this.rowCount = planningTable.resources?.length ?? 0;
			this.columnCount = Math.max(viewConfiguration.numBaseUnitAllocations, 1);
		} else if (PlanningTableAlignment.RESOURCE_TIME === viewConfiguration.tableAlignment) {
			this.rowCount = Math.max(viewConfiguration.numBaseUnitAllocations, 1);
			this.columnCount = planningTable.resources?.length ?? 0;
		}
		this.gridLocator = new GridLocator(viewConfiguration);
	}

	update() {
		if (PlanningTableAlignment.TIME_RESOURCE === this.viewConfiguration.tableAlignment) {
			this.rowCount = this.planningTable.resources?.length ?? 0;
			this.columnCount = Math.max(this.viewConfiguration.numBaseUnitAllocations, 1);
		} else if (PlanningTableAlignment.RESOURCE_TIME === this.viewConfiguration.tableAlignment) {
			this.rowCount = Math.max(this.viewConfiguration.numBaseUnitAllocations, 1);
			this.columnCount = this.planningTable.resources?.length ?? 0;
		}
	}

	/**
	 * Computes where the given time lies on the grid, considering the resource it belongs to.
	 * Also takes the tableAlignment into account.
	 * @param time
	 * @param resource
	 */
	getGridCoordinate(time: Date, resource: Resource) {
		if (PlanningTableAlignment.RESOURCE_TIME === this.viewConfiguration.tableAlignment) {
			return new Vector2(this.getResourceLocation(resource), this.getTimeLocation(time));
		} else {
			return new Vector2(this.getTimeLocation(time), this.getResourceLocation(resource));
		}
	}

	public getTimeLocation(time: Date) {
		return this.gridLocator.computeGridLocation(time).getBaseTimeLocation();
	}

	public getResourceLocation(resource: Resource) {
		return (this.planningTable.resources?.indexOf(resource) ?? 0) * this.viewConfiguration.resourceCellSize;
	}

}

export class GridColumn {

	public cells = new Map<number, CellContext>();

	constructor() {

	}

	addCell(rowIndex: number, cell: CellContext) {
		this.cells.set(rowIndex, cell);
	}

	destroyAllCells() {
		this.cells.clear();
	}

	destroyCell(rowIndex: number) {
		this.cells.delete(rowIndex);
	}

	destroyCellsOutsideVisibleRows(firstVisibleRow: number, lastVisibleRow: number) {
		this.cells.forEach((value: CellContext, key: number) => {
			if (key < firstVisibleRow || key > lastVisibleRow) {
				this.destroyCell(key);
			}
		});
	}

}

export class Vector2 {

	public x: number;
	public y: number;

	constructor(x: number, y: number) {
		this.x = x;
		this.y = y;
	}

}

export interface ViewElementFactory {

	createViewElements(): ViewElement[];

}

export class BookingFactory implements ViewElementFactory {

	public viewElements: BookingViewElement[] = [];
	private bookingGroups: BookingGroup[] = [];

	constructor(
		private planningTable: PlanningTable,
		private viewConfiguration: PlanningViewConfiguration,
		private reloadPlanningTable: EventEmitter<any>,
		private gridModel: GridModel,
		private authenticationService: AuthenticationService,
		private domSanitizer: DomSanitizer,
	) {

	}

	/**
	 * Creates a booking-view-element for every booking of the planning table.
	 * It also assigns a booking-group to each booking-view-elements.
	 * After all booking-view-elements have been assigned to a booking-group it will calculate the lanes for all of the
	 * booking-groups.
	 * @return Array of all booking-view-elements
	 */
	createViewElements(): ViewElement[] {
		let elements: BookingViewElement[] = [];
		this.planningTable.bookings?.forEach((booking: Booking) => {
			let resourceId = this.planningTable.resources?.findIndex(r => (r.resourceId === booking.resource));
			if (resourceId !== undefined) {
				let element: BookingViewElement = new BookingViewElement(booking,
					this.planningTable,
					this.viewConfiguration,
					resourceId,
					this.authenticationService,
					this.domSanitizer);
				let bookingGroup = this.getBookingGroup(element);
				element.setBookingGroup(bookingGroup);
				elements.push(element);
			}
		});
		this.bookingGroups.forEach((group: BookingGroup) => {
			group.computeLanes();
		});
		this.viewElements = elements;
		return elements;
	}

	recalculateBookingGroups() {
		this.viewElements.forEach((viewElement) => {
			viewElement.setBookingGroup(new BookingGroup(viewElement));
		});
		this.bookingGroups = [];
		this.viewElements.forEach((viewElement) => {
			if (viewElement.isVisibleForCurrentUser()) {
				viewElement.setBookingGroup(this.getBookingGroup(viewElement));
			}
		});

		this.bookingGroups.forEach((group: BookingGroup) => {
			group.computeLanes();
		});

	}

	getBookingGroup(booking: BookingViewElement) {
		let bookingGroup: BookingGroup | undefined;
		let len = this.bookingGroups.length;
		while (len--) {
			if (this.bookingGroups[len].overlaps(booking.booking)) {
				if (bookingGroup === undefined) {
					// we 'choose' the first group and add the booking to it
					bookingGroup = this.bookingGroups[len];
					bookingGroup.addBooking(booking);
				} else {
					// we merge this group with the chosen booking group
					this.bookingGroups[len].bookings.forEach((b: BookingViewElement) => {
						bookingGroup?.addBooking(b);
						if (bookingGroup) {
							b.setBookingGroup(bookingGroup);
						}
					});
					this.bookingGroups.splice(len, 1);
				}
			}
		}
		if (!bookingGroup) {
			bookingGroup = new BookingGroup(booking);
			this.bookingGroups.push(bookingGroup);
		}
		return bookingGroup;
	}

}

class MilestoneFactory implements ViewElementFactory {

	constructor(
		private planningTable: PlanningTable,
		private viewConfiguration: PlanningViewConfiguration,
	) {

	}

	/**
	 * Creates a milestone-view-element for every milestone of the planning-table.
	 * If the resource for a milestone is not present in the planning table, this milestone is left out.
	 * @return Array of all milestone-view-elements
	 */
	createViewElements(): ViewElement[] {
		let elements: ViewElement[] = [];
		this.planningTable.milestones?.forEach((milestone: Milestone) => {
			let resourceId = this.planningTable.resources?.findIndex(r => (r.resourceId === milestone.resource));
			if (resourceId !== undefined) {
				let element: MilestoneViewElement = new MilestoneViewElement(milestone,
					this.planningTable,
					this.viewConfiguration,
					resourceId);
				elements.push(element);
			}
		});
		return elements;
	}
}

class RelationFactory implements ViewElementFactory {

	constructor(
		private planningTable: PlanningTable,
		private viewConfiguration: PlanningViewConfiguration,
		private planningGrid: PlanningGridComponent,
		private bookingFactory: BookingFactory
	) {

	}

	/**
	 * Creates a relation-view-element for every relation of the planning-table.
	 * Relations, where the 'from booking' or the 'to booking' are missing, are being left out.
	 * @return Array of all relation-view-elements
	 */
	createViewElements(): ViewElement[] {
		let elements: ViewElement[] = [];
		this.planningTable.relations?.forEach(relation => {
			let fromViewElement: BookingViewElement | undefined
				= this.bookingFactory.viewElements.find(x => x.booking.id === relation.from);
			let toViewElement: BookingViewElement | undefined
				= this.bookingFactory.viewElements.find(x => x.booking.id === relation.to);
			if (fromViewElement && toViewElement) {
				let element: RelationViewElement = new RelationViewElement(relation,
					this.planningTable,
					this.viewConfiguration,
					this.planningGrid,
					fromViewElement,
					toViewElement);
				elements.push(element);
			}
		});
		return elements;
	}

}
