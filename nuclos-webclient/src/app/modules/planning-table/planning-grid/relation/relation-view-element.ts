import { BookingViewElement } from '@modules/planning-table/planning-grid/booking/booking-view-element';
import { PlanningTableAlignment } from '@modules/planning-table/shared/planning-table-alignment';
import { RotatedEdge } from '@modules/planning-table/shared/rotated-edge';
import { PlanningTable, Relation } from '../../planning-table.service';
import { PlanningViewConfiguration } from '../../shared/planning-view-configuration';
import { GridModel, PlanningGridComponent, Vector2 } from '../planning-grid.component';
import { ViewElement, ViewElementBounds } from '../shared/view-element';

export class RelationViewElement extends ViewElement {

	fromX: number;
	fromY: number;

	toX: number;
	toY: number;

	/** path of edges, that connects the fromBooking with the toBooking **/
	edges: Array<RotatedEdge> = [];

	/** thickness of the relation line **/
	lineThickness = 2;

	constructor(
		public relation: Relation,
		public planningTable: PlanningTable,
		public viewConfiguration: PlanningViewConfiguration,
		public planningGrid: PlanningGridComponent,
		public toElement: BookingViewElement,
		public fromElement: BookingViewElement
	) {
		super();
		let index1 = this.planningTable.resources?.findIndex(r => (r.resourceId === fromElement.booking.resource));
		let index2 = this.planningTable.resources?.findIndex(r => (r.resourceId === toElement.booking.resource));
		if (index1 && index2) {
			let fromTime = Math.min(this.fromElement.dateFrom.getTime(), this.toElement.dateFrom.getTime());
			let untilTime = Math.max(this.fromElement.dateUntil.getTime(), this.toElement.dateUntil.getTime());
			this.bounds = new ViewElementBounds(fromTime, untilTime, Math.min(index1, index2), Math.max(index1, index2));
		}
	}

	/**
	 * Calculates the edge path for this relation-view-element.
	 * @param gridModel
	 */
	layout(gridModel: GridModel) {
		if (this.planningGrid.visibleBookings.indexOf(this.fromElement) === -1) {
			// if the booking is outside the visible area -> we have to lay it out manually
			this.fromElement.layout(gridModel);
		}
		if (this.planningGrid.visibleBookings.indexOf(this.toElement) === -1) {
			// if the booking is outside the visible area -> we have to lay it out manually
			this.toElement.layout(gridModel);
		}
		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			this.fromX = this.fromElement.left + this.fromElement.width;
			this.fromY = this.fromElement.top + this.fromElement.height / 2;
			this.toX = this.toElement.left;
			this.toY = this.toElement.top + this.toElement.height / 2;
		} else {
			this.fromX = this.fromElement.left + this.fromElement.width / 2;
			this.fromY = this.fromElement.top + this.fromElement.height;
			this.toX = this.toElement.left + this.toElement.width / 2;
			this.toY = this.toElement.top;
		}
		this.edges = [];

		let toVector: Vector2 = new Vector2(this.toX, this.toY);
		let fromVector: Vector2 = new Vector2(this.fromX, this.fromY);

		if (this.relation.presentationMode === '1') {
			this.buildDirectRepresentation(fromVector, toVector);
		} else if (this.relation.presentationMode === '2') {
			this.buildOrthogonalRepresentation(fromVector, toVector);
		}
	}

	/**
	 * Builds the direct representation from the fromBooking to the toBooking.
	 * The direct representation consists of 3 edges.
	 * The middle edge may connect the other 2 edges diagonally, hence the name direct representation.
	 * @param fromVector
	 * @param toVector
	 */
	buildDirectRepresentation(fromVector: Vector2, toVector: Vector2) {
		this.edges = [];
		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			this.buildEdge(fromVector.x, fromVector.y, fromVector.x + 14, fromVector.y);
			this.buildEdge(toVector.x - 14, toVector.y, toVector.x, toVector.y);
			this.buildEdge(toVector.x - 14, toVector.y, fromVector.x + 14, fromVector.y);
		} else {
			this.buildEdge(fromVector.x, fromVector.y, fromVector.x, fromVector.y + 14);
			this.buildEdge(toVector.x, toVector.y - 14, toVector.x, toVector.y);
			this.buildEdge(toVector.x, toVector.y - 14, fromVector.x, fromVector.y + 14);
		}
	}

	/**
	 * Builds the orthogonal representation from the fromBooking to the toBooking.
	 * The orthogonal representation consists of 5 edges.
	 * All the edges are aligned either to the x-axis or the y-axis.
	 * @param fromVector
	 * @param toVector
	 */
	buildOrthogonalRepresentation(fromVector: Vector2, toVector: Vector2) {
		let deltaX = Math.abs(this.toX - this.fromX);
		let deltaY = Math.abs(this.toY - this.fromY);
		let fromVector2: Vector2;
		let toVector2: Vector2;
		let supportFrom: Vector2;
		let supportTo: Vector2;

		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			fromVector2 = new Vector2(fromVector.x + 14, fromVector.y);
			toVector2 = new Vector2(toVector.x - 14, toVector.y);

			supportFrom = new Vector2(fromVector2.x, 0);
			supportTo = new Vector2(toVector2.x, 0);

			if (deltaY !== 0) {
				// build on deltaY / 2
				supportFrom.y = fromVector2.y + deltaY / 2;
				supportTo.y = fromVector2.y + deltaY / 2;
			} else {
				supportFrom.y = this.fromElement.top + this.fromElement.height;
				supportTo.y = this.fromElement.top + this.fromElement.height;
			}
		} else {
			fromVector2 = new Vector2(fromVector.x, fromVector.y + 14);
			toVector2 = new Vector2(toVector.x, toVector.y - 14);

			supportFrom = new Vector2(0, fromVector2.y);
			supportTo = new Vector2(0, toVector2.y);

			if (deltaX !== 0) {
				supportFrom.x = fromVector2.x + deltaX / 2;
				supportTo.x = fromVector2.x + deltaX / 2;
			} else {
				supportFrom.x = this.fromElement.left + this.fromElement.width;
				supportTo.x = this.fromElement.left + this.fromElement.width;
			}
		}
		this.buildEdge(fromVector.x, fromVector.y, fromVector2.x, fromVector2.y);
		this.buildEdge(toVector2.x, toVector2.y, toVector.x, toVector.y);
		this.buildEdge(fromVector2.x, fromVector2.y, supportFrom.x, supportFrom.y);
		this.buildEdge(toVector2.x, toVector2.y, supportTo.x, supportTo.y);
		this.buildEdge(supportFrom.x, supportFrom.y, supportTo.x, supportTo.y);
	}

	/** Builds a rotated edge from (x1; y1) to (x2; y2) and pushes it to the edges list. **/
	buildEdge(x1: number, y1: number, x2: number, y2: number) {
		let directLength: number = Math.hypot((x2 - x1), (y2 - y1));
		let directAngle: number = Math.atan2((y2 - y1), (x2 - x1));
		let edge: RotatedEdge = new RotatedEdge(x1, y1, directLength, this.lineThickness, directAngle);
		this.edges.push(edge);
	}

	/** Exports this relation-view-element to the svg. **/
	getSvgContent(): string {
		let lineStyle = 'position: absolute; background: blue; transform-origin: center left; z-index: 6;';
		let lineStructure = '';
		this.edges.forEach((re: RotatedEdge) => {
			lineStructure += `<div style="left: ${re.x}px; top: ${re.y}px; width: ${re.width}px;  height: ${this.lineThickness}px; transform: rotate(${re.rotation}rad); ${lineStyle}"></div>`;
		});

		return `${lineStructure} ${this.getSvgFromCap()} ${this.getSvgToCap()}`;
	}

	/**
	 * Export the fromCap as a string to a svg file.
	 */
	getSvgFromCap(): string {
		let fromCapStyle = 'z-index: 6; background-color: blue;';
		if (this.relation.startDecoration === '2') {
			fromCapStyle +=
				`left: ${this.fromX + ((this.viewConfiguration.tableAlignment === 0) ? -4 : 0)}px;
				top: ${this.fromY + ((this.viewConfiguration.tableAlignment === 0) ? 0 : -4) }px;
				height: 9px;
				width: 9px;
				border-radius: 50%;
				position: absolute;`;
		} else if (this.relation.startDecoration === '3') {
			fromCapStyle +=
				`position: absolute;
				transform-origin: center right;
				width: 10px;
				height: 10px;
				`;
			if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
				fromCapStyle += 'clip-path: polygon(0% 50%, 100% 100%, 100% 0%);';
				fromCapStyle += `left: calc(${this.fromX}px); top: calc(${this.fromY}px - 5px);`;
			} else {
				fromCapStyle += 'clip-path: polygon(50% 0, 100% 100%, 0 100%);';
				fromCapStyle += `left: calc(${this.fromX}px - 4px); top: calc(${this.fromY}px);`;
			}
		}
		return `<div style="${fromCapStyle}"></div>`;
	}

	/**
	 * Export the toCap as a string to a svg file.
	 */
	getSvgToCap(): string {
		let toCapStyle = 'z-index: 6; position: absolute; background-color: blue;';
		if (this.relation.endDecoration === '2') {
			toCapStyle +=
				`left: ${this.toX + ((this.viewConfiguration.tableAlignment === 0) ? -4 : -4)}px;
				top: ${this.toY + ((this.viewConfiguration.tableAlignment === 0) ? -4 : -4) }px;
				height: 9px;
				width: 9px;
				border-radius: 50%;`;
		} else if (this.relation.endDecoration === '3') {
			toCapStyle +=
				` width: 10px; height: 10px; `;
			if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
				toCapStyle += `clip-path: polygon(100% 50%, 0% 100%, 0 0%);`;
				toCapStyle += ` left: calc(${this.toX}px - 10px); top: calc(${this.toY}px - 4px);`;
			} else {
				toCapStyle += `clip-path: polygon(50% 100%, 100% 0%, 0 0%);`;
				toCapStyle += ` left: calc(${this.toX}px - 4px); top: calc(${this.toY}px - 9px);`;
			}
		}
		return `<div style="${toCapStyle}"></div>`;
	}

}
