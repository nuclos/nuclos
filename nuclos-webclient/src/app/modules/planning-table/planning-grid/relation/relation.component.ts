import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { BookingViewElement } from '@modules/planning-table/planning-grid/booking/booking-view-element';
import { RelationViewElement } from '@modules/planning-table/planning-grid/relation/relation-view-element';
import { PlanningTableAlignment } from '@modules/planning-table/shared/planning-table-alignment';
import { RotatedEdge } from '@modules/planning-table/shared/rotated-edge';

@Component({
	selector: 'nuc-relation',
	templateUrl: './relation.component.html',
	styleUrls: ['./relation.component.css']
})
export class RelationComponent implements OnInit {

	@Input() viewElement: RelationViewElement;
	@Input() fromBooking: BookingViewElement;
	@Input() toBooking: BookingViewElement;
	@Input() selected: boolean;
	@Input() edges: Array<RotatedEdge> = [];

	parentStyle;
	fromCapStyle;
	toCapStyle;
	relationIndex;

	constructor(public changeDetectorRef: ChangeDetectorRef) {
	}

	ngOnInit(): void {
		this.relationIndex = this.viewElement.planningTable.relations?.indexOf(this.viewElement.relation);
		this.updateStyleClasses();
	}

	ngOnChanges(): void {
		this.updateStyleClasses();
	}

	/**
	 * Updates, which cap styles will be used to display the relation.
	 */
	updateStyleClasses() {
		this.parentStyle = {
			'parent': true,
			'parent-selected': this.selected
		};
		if (this.viewElement.relation.presentationMode === '2') {
			this.fromCapStyle = {
				'dot': this.viewElement.relation.startDecoration === '2',
				'arrow-left': this.viewElement.relation.startDecoration === '3'
					&& this.viewElement.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE,
				'arrow-up': this.viewElement.relation.startDecoration === '3'
					&& this.viewElement.viewConfiguration.tableAlignment === PlanningTableAlignment.RESOURCE_TIME,
			};
			this.toCapStyle = {
				'dot': this.viewElement.relation.endDecoration === '2',
				'arrow-right': this.viewElement.relation.endDecoration === '3'
					&& this.viewElement.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE,
				'arrow-down': this.viewElement.relation.endDecoration === '3'
					&& this.viewElement.viewConfiguration.tableAlignment === PlanningTableAlignment.RESOURCE_TIME,
			};
		} else if (this.viewElement.relation.presentationMode === '1') {
			this.fromCapStyle = {
				'dot': this.viewElement.relation.startDecoration === '2',
				'arrow-left': this.viewElement.relation.startDecoration === '3'
					&& this.viewElement.viewConfiguration.tableAlignment === PlanningTableAlignment.RESOURCE_TIME,
				'arrow-up': this.viewElement.relation.startDecoration === '3'
					&& this.viewElement.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE,
			};
			this.toCapStyle = {
				'dot': this.viewElement.relation.endDecoration === '2',
				'arrow-right': this.viewElement.relation.endDecoration === '3'
					&& this.viewElement.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE,
				'arrow-down': this.viewElement.relation.endDecoration === '3'
					&& this.viewElement.viewConfiguration.tableAlignment === PlanningTableAlignment.RESOURCE_TIME,
			};
		}
	}

}
