/**
 * Abstract definition of an element that can be shown in the planning-grid.component.
 */
export abstract class ViewElement {

	constructor(protected bounds?: ViewElementBounds) {
	}

	getSvgContent() {
		return '';
	}

	/**
	 * Returns the view bounds of this ViewElement. These bounds are used by the ViewTree to determine, whether a ViewElement is
	 * visible in the current viewport or not.
	 * @see ViewTree
	 */
	getViewBounds(): ViewElementBounds | undefined {
		return this.bounds;
	}

	/**
	 * If a ViewElement is enabled, it will create a component when it is on screen.
	 */
	isEnabled(): boolean {
		return true;
	}

}

/**
 * Specifies the bounds of a ViewElement.
 * @see ViewElement
 * @see ViewTree
 */
export class ViewElementBounds {

	constructor(public startTime: number, public endTime: number, public startResourceIndex, public endResourceIndex) {

	}

	/**
	 * Checks whether this object overlaps with the specified ViewElementBounds.
	 * true - if bounds overlap
	 * false - otherwise
	 * @param bounds
	 */
	overlaps(bounds: ViewElementBounds | undefined) {
		if (!bounds) {
			return false;
		}
		if (bounds.startResourceIndex > this.endResourceIndex
			|| bounds.endResourceIndex < this.startResourceIndex
			|| bounds.endTime < this.startTime
			|| bounds.startTime > this.endTime) {
			return false;
		}
		return true;
	}

}

