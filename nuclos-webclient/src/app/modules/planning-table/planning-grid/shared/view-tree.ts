import { ViewElement, ViewElementBounds } from './view-element';

/**
 * Hierarchical structure to quickly find the elements that should be displayed in the scene.
 * There is a detailed explanation of this concept available here: https://en.wikipedia.org/wiki/Quadtree
 * This Implementation has a little modification instead of having a bounds object with x- and y- coordinate properties,
 * this ViewTree has a bounds object with date- and resource- properties because those 2 properties determine the coordinates.
 */
export class ViewTree {

	static readonly NODE_CAPACITY = 16;

	/** specifies the area in which all view elements contained by this ViewTree lie inside **/
	viewBounds: ViewElementBounds;

	/** collection of ViewElement, which are located inside this ViewTrees viewBounds **/
	viewElements: ViewElement[] = [];

	/** reference to subtrees once the NODE_CAPACITY inside this ViewTree has been reached. **/
	private northWest: ViewTree;
	private northEast: ViewTree;
	private southWest: ViewTree;
	private southEast: ViewTree;

	constructor(viewBounds: ViewElementBounds) {
		this.viewBounds = viewBounds;
	}

	/**
	 * Inserts a view element into this ViewTree.
	 * @param element
	 */
	insert(element: ViewElement) {
		if (!this.viewBounds.overlaps(element.getViewBounds())) {
			// the bounds of the element to insert do not overlap with the bounds of this ViewTree -> no insertion
			return false;
		}

		if (this.viewElements.length < ViewTree.NODE_CAPACITY && !this.northWest) {
			// there is still enough capacity in this tree node, so we insert here and return
			this.viewElements.push(element);
			return true;
		}

		// subdivide the tree, if not already
		if (this.northWest == null) {
			this.subdivide();
		}

		// try to insert in 1 of the sub sectors
		if (this.northWest.insert(element)) {
			return true;
		}
		if (this.northEast.insert(element)) {
			return true;
		}
		if (this.southWest.insert(element)) {
			return true;
		}
		if (this.southEast.insert(element)) {
			return true;
		}

		// returning false means no insertion took place
		return false;
	}

	/**
	 * Recursively collects all elements that are within the specified bounds and returns them in a list.
	 * @param bounds
	 */
	queryRange(bounds: ViewElementBounds): ViewElement[] {
		let elements: ViewElement[] = [];

		if (!this.viewBounds.overlaps(bounds)) {
			return elements;
		}

		this.viewElements.forEach((element: ViewElement) => {
			if (bounds.overlaps(element.getViewBounds())) {
				elements.push(element);
			}
		});

		if (this.northWest == null) {
			return elements;
		}

		elements.push(...this.northWest.queryRange(bounds));
		elements.push(...this.northEast.queryRange(bounds));
		elements.push(...this.southWest.queryRange(bounds));
		elements.push(...this.southEast.queryRange(bounds));

		return elements;
	}

	/**
	 * Subdivides this ViewTree into 4 subtrees.
	 * @private
	 */
	private subdivide() {
		this.northWest = new ViewTree(
			new ViewElementBounds(
				this.viewBounds.startTime,
				(this.viewBounds.startTime + this.viewBounds.endResourceIndex) / 2,
				this.viewBounds.startResourceIndex,
				(this.viewBounds.startResourceIndex + this.viewBounds.endResourceIndex) / 2)
		);
		this.northEast = new ViewTree(
			new ViewElementBounds(
				(this.viewBounds.startTime + this.viewBounds.endResourceIndex) / 2,
				this.viewBounds.endTime,
				this.viewBounds.startResourceIndex,
				(this.viewBounds.startResourceIndex + this.viewBounds.endResourceIndex) / 2)
		);
		this.southWest = new ViewTree(new ViewElementBounds(
			this.viewBounds.startTime,
			(this.viewBounds.startTime + this.viewBounds.endResourceIndex) / 2,
			(this.viewBounds.startResourceIndex + this.viewBounds.endResourceIndex) / 2,
			this.viewBounds.endResourceIndex)
		);
		this.southEast = new ViewTree(new ViewElementBounds(
			(this.viewBounds.startTime + this.viewBounds.endResourceIndex) / 2,
			this.viewBounds.endTime,
			(this.viewBounds.startResourceIndex + this.viewBounds.endResourceIndex) / 2,
			this.viewBounds.endResourceIndex)
		);
	}

}
