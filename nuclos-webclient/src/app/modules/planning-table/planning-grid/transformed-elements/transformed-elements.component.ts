import { ChangeDetectorRef, Component, HostListener, Input, OnInit } from '@angular/core';
import { GridModel, Vector2 } from '@modules/planning-table/planning-grid/planning-grid.component';
import { BookingUtils } from '@modules/planning-table/shared/booking-utils';
import { PlanningTableAlignment } from '@modules/planning-table/shared/planning-table-alignment';
import { PlanningViewConfiguration } from '@modules/planning-table/shared/planning-view-configuration';
import { ColorUtils } from '@shared/color-utils';
import { BookingTransformation, MilestoneTransformation, Transformation } from '../../shared/transform/transformation';
import { Booking, PlanningTable, Milestone } from '@modules/planning-table/planning-table.service';

@Component({
	selector: 'nuc-transformed-elements',
	templateUrl: './transformed-elements.component.html',
	styleUrls: ['./transformed-elements.component.css']
})
export class TransformedElementsComponent implements OnInit {

	@Input() transformation: Transformation;
	@Input() gridModel: GridModel;
	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() planningTable: PlanningTable;

	transformBookings: TransformedBooking[] = [];
	transformMilestones: TransformedMilestone[] = [];

	constructor(private changeDetectorRef: ChangeDetectorRef) {
	}

	ngOnInit(): void {
		this.transformation.transformMap.forEach(transformation => {
			if (transformation instanceof BookingTransformation) {
				this.transformBookings.push(this.createTransformedBooking(transformation));
			} else if (transformation instanceof MilestoneTransformation) {
				this.transformMilestones.push(this.createTransformedMilestone(transformation));
			}
		});

		this.transformBookings.forEach(tb => {
			tb.layout(this.gridModel);
		});
		this.transformMilestones.forEach(tb => {
			tb.layout(this.gridModel);
		});
	}

	ngOnChanges(): void {
	}

	update() {
		this.transformBookings.forEach(tb => {
			let transformation = this.transformation.transformMap.get(tb.booking) as BookingTransformation;
			if (transformation) {
				let newStart = transformation.newFromDate;
				let newEnd = transformation.newUntilDate;
				let newResource = this.planningTable.resources?.find(x => x.resourceId === transformation.newResource);
				tb.dateFrom = newStart;
				tb.dateUntil = newEnd;
				tb.resource = newResource;
			}
			tb.layout(this.gridModel);
		});
		this.transformMilestones.forEach(tm => {
			let transformation = this.transformation.transformMap.get(tm.milestone) as MilestoneTransformation;
			if (transformation) {
				let newResource = this.planningTable.resources?.find(x => x.resourceId === transformation.newResource);
				tm.resource = newResource;
				tm.date = transformation.newDate;
			}
			tm.layout(this.gridModel);
		});
		this.changeDetectorRef.detectChanges();
	}

	private createTransformedBooking(transformation: BookingTransformation): TransformedBooking {
		let transformedBooking: TransformedBooking = new TransformedBooking(transformation.booking, this.viewConfiguration);
		let newResource = this.planningTable.resources?.find(x => x.resourceId === transformation.newResource);
		if (this.planningTable.resources && newResource) {
			transformedBooking.resource = newResource;
			transformedBooking.resourceIndex = this.planningTable.resources.indexOf(newResource);
		}
		if (this.planningTable.bookings) {
			transformedBooking.bookingIndex = this.planningTable.bookings.indexOf(transformation.booking);
		}
		return transformedBooking;
	}

	private createTransformedMilestone(transformation: MilestoneTransformation): TransformedMilestone {
		let newResource = this.planningTable.resources?.find(x => x.resourceId === transformation.newResource);
		let transformedMilestone: TransformedMilestone = new TransformedMilestone(transformation.milestone, this.viewConfiguration);
		transformedMilestone.resource = newResource;
		transformedMilestone.date = transformation.newDate;
		if (this.planningTable.resources && newResource) {
			transformedMilestone.resourceIndex = this.planningTable.resources.indexOf(newResource);
		}
		if (this.planningTable.milestones) {
			transformedMilestone.milestoneIndex = this.planningTable.milestones.indexOf(transformation.milestone);
		}
		return transformedMilestone;
	}

}

class TransformedBooking {

	top: number;
	left: number;
	width: number;
	height: number;

	resource: any;
	resourceIndex: number;
	bookingIndex: number;
	dateFrom: Date;
	dateUntil: Date;
	textColor: string;

	constructor(public booking: Booking, private viewConfiguration: PlanningViewConfiguration) {
		this.dateFrom = BookingUtils.getStart(this.booking);
		this.dateUntil = BookingUtils.getEnd(this.booking);

		if (this.booking.backgroundColor !== undefined && this.booking.backgroundColor !== '') {
			this.textColor = ColorUtils.calculateTextColorFromBackgroundColor(this.booking.backgroundColor);
		} else {
			this.textColor = '#000000';
		}
	}

	layout(gridModel: GridModel) {
		let location: Vector2 = gridModel.getGridCoordinate(this.dateFrom, this.resource);
		let timeStart = gridModel.getTimeLocation(this.dateFrom);
		let timeEnd = gridModel.getTimeLocation(this.dateUntil);
		let bookingOrder = 0;
		let bookingGroupSize = 1;

		if (PlanningTableAlignment.TIME_RESOURCE === this.viewConfiguration.tableAlignment) {
			this.width = (timeEnd - timeStart) * this.viewConfiguration.baseTimeUnitCellSize;
			this.height = this.viewConfiguration.resourceCellSize / bookingGroupSize;
			location.y += this.height * bookingOrder;
			location.x *= this.viewConfiguration.baseTimeUnitCellSize;
		} else {
			this.width = this.viewConfiguration.resourceCellSize / bookingGroupSize;
			this.height = (timeEnd - timeStart) * this.viewConfiguration.baseTimeUnitCellSize;
			location.x += this.width * bookingOrder;
			location.y *= this.viewConfiguration.baseTimeUnitCellSize;
		}
		this.top = location.y;
		this.left = location.x;
	}

}

class TransformedMilestone {

	top: number;
	left: number;
	width: number;
	height: number;

	date: Date;
	dateLabel: string;
	resource: any;
	resourceIndex: number;
	milestoneIndex: number;

	constructor(public milestone: Milestone, private viewConfiguration: PlanningViewConfiguration) {
		this.date = new Date(milestone.fromDate);
		this.dateLabel = moment(this.date).format('YYYY-MM-DD');
	}

	layout(gridModel: GridModel) {
		this.dateLabel = moment(this.date).format('YYYY-MM-DD');
		let location: Vector2 = gridModel.getGridCoordinate(this.date, this.resource);
		let timeLocation = gridModel.getTimeLocation(this.date);

		if (PlanningTableAlignment.TIME_RESOURCE === this.viewConfiguration.tableAlignment) {
			this.width = this.viewConfiguration.baseTimeUnitCellSize;
			this.height = this.viewConfiguration.resourceCellSize * 1;
			this.width = Math.min(this.width, 32);
			this.height = Math.min(this.height, 32);
			location.x = timeLocation * this.viewConfiguration.baseTimeUnitCellSize;
			location.x -= (this.width / 2);
			location.y += (this.viewConfiguration.resourceCellSize - this.height) / 2;
		} else {
			this.width = this.viewConfiguration.resourceCellSize / 2;
			this.height = this.viewConfiguration.baseTimeUnitCellSize;
			this.width = Math.min(this.width, 32);
			this.height = Math.min(this.height, 32);
			location.x += (this.viewConfiguration.resourceCellSize - this.width) / 2;
			location.y = timeLocation * this.viewConfiguration.baseTimeUnitCellSize;
			location.y -= this.height * 0.5;
		}

		this.top = location.y;
		this.left = location.x;
	}

}
