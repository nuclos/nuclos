import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { PlanningTableComponent } from '@modules/planning-table/planning-table.component';
import { Filter, PlanningTable } from '@modules/planning-table/planning-table.service';
import { PlanningViewConfiguration } from '@modules/planning-table/shared/planning-view-configuration';
import { TimeUnits } from '@modules/planning-table/time/time-unit';
import { DatechooserComponent } from '@modules/ui-components/datechooser/datechooser.component';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import * as moment from 'moment';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { Subscriber } from 'rxjs';

@Component({
	selector: 'nuc-planning-modal',
	templateUrl: './planning-modal.component.html',
	styleUrls: ['./planning-modal.component.css']
})
export class PlanningModalComponent implements OnInit {

	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	@Input() planningTable: PlanningTable;
	@Input() planningTableComponent: PlanningTableComponent;
	@Input() viewConfiguration: PlanningViewConfiguration;

	@ViewChild('chooserFrom') fromComponent: DatechooserComponent;
	@ViewChild('chooserUntil') untilComponent: DatechooserComponent;

	timeModes = [
		TimeUnits.UNIT_HOUR,
		TimeUnits.UNIT_DAY,
		TimeUnits.UNIT_WEEK,
		TimeUnits.UNIT_MONTH,
		TimeUnits.UNIT_QUARTER
	];

	currentFilter;
	searchFilters;

	subscriberFrom = Subscriber.create(x => {
		this.nextDateFrom = moment(x as string, 'YYYY-MM-DD').toDate();
	});

	subscriberUntil = Subscriber.create(x => {
		this.nextDateUntil = moment(x as string, 'YYYY-MM-DD').toDate();
	});

	private nextDateFrom;
	private nextDateUntil;

	constructor() { }

	ngOnInit(): void {
		if (this.planningTableComponent.planningView.customTimeUnit) {
			this.timeModes.push(this.planningTableComponent.planningView.customTimeUnit);
		}
		this.currentFilter = this.viewConfiguration.searchFilter;
		this.searchFilters = [];
		this.searchFilters.push(undefined);
		this.planningTable.searchFilters?.forEach((filter: Filter) => {
			this.searchFilters.push(filter);
		});
	}

	ngOnChange(): void { }

	ngAfterViewInit() {
		this.fromComponent.modelChange.subscribe(this.subscriberFrom);
		this.untilComponent.modelChange.subscribe(this.subscriberUntil);
	}

	onFromPickerLeft() {
		if (!this.nextDateFrom) {
			return;
		}
		if (isNaN(this.nextDateFrom)) {
			this.nextDateFrom = undefined;
			return;
		}
		if (this.viewConfiguration.dateFrom.getTime() !== this.nextDateFrom.getTime()) {
			this.planningTableComponent.setDateFrom(this.nextDateFrom);
		}
	}

	onUntilPickerLeft() {
		if (!this.nextDateUntil) {
			return;
		}
		if (isNaN(this.nextDateUntil)) {
			this.nextDateUntil = undefined;
			return;
		}
		this.nextDateUntil.setHours(23, 59, 59, 999);
		if (this.viewConfiguration.dateUntil.getTime() !== this.nextDateUntil.getTime()) {
			this.planningTableComponent.setDateUntil(this.nextDateUntil);
		}
	}

}
