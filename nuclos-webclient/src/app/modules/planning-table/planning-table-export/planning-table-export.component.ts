import { Component, Input, OnInit } from '@angular/core';
import { PlanningTable } from '@modules/planning-table/planning-table.service';
import { PlanningView } from '@modules/planning-table/shared/planning-view';
import { PlanningViewConfiguration } from '@modules/planning-table/shared/planning-view-configuration';

@Component({
	selector: 'nuc-planning-table-export',
	templateUrl: './planning-table-export.component.html',
	styleUrls: ['./planning-table-export.component.css']
})
export class PlanningTableExportComponent implements OnInit {

	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() planningTable: PlanningTable;
	@Input() planningView: PlanningView;

	horizontalHeaderHeight;
	verticalHeaderWidth;

	constructor() {
	}

	ngOnInit(): void {
	}

}
