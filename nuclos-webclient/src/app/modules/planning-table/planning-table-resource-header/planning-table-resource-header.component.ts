import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewChild, ViewContainerRef
} from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { MetaService } from '@modules/entity-object-data/shared/meta.service';
import { ResizeHandler } from '@modules/planning-table/shared/resize-handler';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { iif, of } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';
import { PlanningTable, Resource } from '../planning-table.service';
import { PlanningTableAlignment } from '../shared/planning-table-alignment';
import { PlanningViewConfiguration } from '../shared/planning-view-configuration';

@Component({
	selector: 'nuc-planning-table-resource-header',
	templateUrl: './planning-table-resource-header.component.html',
	styleUrls: ['./planning-table-resource-header.component.css']
})
export class PlanningTableResourceHeaderComponent implements OnInit {

	@Input() planningTable: PlanningTable;
	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() isExport;
	@Output() cellSizeChanged = new EventEmitter<any>();
	@Output() reloadPlanningTable = new EventEmitter<any>();
	@Output() savePreferences = new EventEmitter<any>();

	@ViewChild('resourceContainer') resourceContainer: ElementRef;

	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;
	resourceElements: Map<number, ResourceContext> = new Map<number, ResourceContext>();

	resizeHandler: ResizeHandler;

	constructor(
		private metaService: MetaService,
		private modalService: NuclosDialogService,
		private eoService: EntityObjectService,
		public changeDetectorRef: ChangeDetectorRef
	) {
	}

	ngOnInit(): void {
		this.resizeHandler = new ResizeHandler(this.processResize.bind(this), this.processResize.bind(this));
	}

	ngOnChanges(): void {
		this.clearHeader();
		this.layoutHeader();
	}

	refreshDisplay() {
		this.clearHeader();
		this.layoutHeader();
	}

	ngAfterViewInit(): void {
		this.layoutHeader();
	}

	processResize(newSize: number) {
		this.changeResourceCellSize(newSize);
		this.cellSizeChanged.emit();
		this.layoutHeader();
	}

	changeResourceCellSize(val: number) {
		this.viewConfiguration.resourceCellSize =
			Math.max(PlanningViewConfiguration.MINIMUM_RESOURCE_CELL_SIZE, val);
	}

	setScaleMode($event: Event, mode: number) {
		if (mode > 0) {
			// this prevents accidentally selecting text while resizing the header cells
			if ($event instanceof MouseEvent) {
				this.resizeHandler.startResizing($event.clientX, $event.clientY, this.viewConfiguration.resourceCellSize, mode);
				$event.preventDefault();
			} else if (window.TouchEvent && $event instanceof TouchEvent) {
				if (!$event.cancelable) {
					return;
				}
				let dragStartSize = this.viewConfiguration.resourceCellSize;
				this.resizeHandler.startResizing($event.touches[0].clientX, $event.touches[0].clientY, dragStartSize, mode);
				$event.stopPropagation();
				$event.preventDefault();
			}
		} else if (mode !== this.resizeHandler.scaleMode) {
			this.resizeHandler.stopResizing();
			this.savePreferences.emit();
		}
	}

	/**
	 * Opens the BO overlay for the specified resource entry.
	 * Does nothing, if there is no layout present.
	 * @param resource
	 */
	openEo(resource: Resource) {
		this.metaService.getBoMeta(resource.entityMeta).pipe(
			switchMap(
				(meta) =>
					iif(
						() => meta.getLinks().defaultLayout !== undefined,
						this.eoService.loadEO(meta.getEntityClassId(), resource.resourceId)
							.pipe(
								switchMap(
									(eo) => this.modalService.openEoInModal(eo)
										.pipe(
											switchMap(
												() =>
													iif(
														() => eo.isDirty(),
														this.eoService.save(eo).pipe(
															tap(() => this.reloadPlanningTable.emit())
														),
														of(void 0)
													)
											)
										)
								)
							),
						of(void 0)
					)
			),
			take(1)
		).subscribe();
	}

	layoutHeader() {
		let firstVisibleResource;
		let lastVisibleResource;

		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			firstVisibleResource =
				Math.floor(this.viewConfiguration.verticalScroll / this.viewConfiguration.resourceCellSize);
			lastVisibleResource =
				Math.ceil((this.viewConfiguration.viewportHeight + this.viewConfiguration.verticalScroll)
					/ this.viewConfiguration.resourceCellSize);
		} else {
			firstVisibleResource =
				Math.floor(this.viewConfiguration.horizontalScroll / this.viewConfiguration.resourceCellSize);
			lastVisibleResource =
				Math.ceil((this.viewConfiguration.viewportWidth + this.viewConfiguration.horizontalScroll)
					/ this.viewConfiguration.resourceCellSize);
		}

		if (this.isExport) {
			firstVisibleResource = 0;
			lastVisibleResource = this.planningTable.resources?.length ?? 0;
		}

		this.destroyCellsOutsideView(firstVisibleResource, lastVisibleResource);

		this.addNewCellsToViewport(firstVisibleResource, lastVisibleResource);

		this.changeDetectorRef.detectChanges();
	}

	resourceTrackBy(index, resourceContext) {
		return resourceContext.resourceId;
	}

	private clearHeader() {
		this.resourceElements.clear();
	}

	/**
	 * Removes all resource cells, with indices < firstVisibleResource and > lastVisibleResource.
	 * @param firstVisibleResource
	 * @param lastVisibleResource
	 * @private
	 */
	private destroyCellsOutsideView(firstVisibleResource: number, lastVisibleResource: number) {
		this.resourceElements.forEach((element: ResourceContext, key: number) => {
			if (key < firstVisibleResource || key > lastVisibleResource
				|| this.planningTable.resources?.find(r => (r.resourceId === element.resource.resourceId)) === undefined) {
				this.resourceElements.delete(key);
			}
		});
	}

	/**
	 * Checks if the creation of a new resource cell is necessary inside the specified indices.
	 * @param firstVisibleResource
	 * @param lastVisibleResource
	 * @private
	 */
	private addNewCellsToViewport(firstVisibleResource: number, lastVisibleResource: number) {
		for (let r = firstVisibleResource; r < lastVisibleResource; r++) {
			if (r >= (this.planningTable.resources?.length ?? 0)) {
				break;
			}
			if (!this.resourceElements.has(r) && this.planningTable.resources) {
				let context: ResourceContext = new ResourceContext(this.planningTable.resources[r], r);
				this.resourceElements.set(r, context);
			}
		}
	}

}

class ResourceContext {

	constructor(public resource: Resource, public resourceId: number) {
	}

}
