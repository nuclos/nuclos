import { Component, Input, OnInit } from '@angular/core';
import { PlanningViewConfiguration } from '@modules/planning-table/shared/planning-view-configuration';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { TooltipOptions } from 'primeng/tooltip/tooltip';

@Component({
	selector: 'nuc-resource-header-cell',
	templateUrl: './resource-header-cell.component.html',
	styleUrls: ['./resource-header-cell.component.css']
})
export class ResourceHeaderCellComponent implements OnInit {

	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() resource;
	@Input() resourceIndex;

	constructor() {
	}

	ngOnInit(): void {
	}

}
