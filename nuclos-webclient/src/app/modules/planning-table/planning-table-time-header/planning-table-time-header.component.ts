import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output, QueryList,
	SimpleChanges,
	ViewChild, ViewChildren
} from '@angular/core';
import { PlanningView } from '../shared/planning-view';
import { PlanningViewConfiguration } from '../shared/planning-view-configuration';
import { ResizeHandler } from '../shared/resize-handler';
import { TimeUnit } from '../time/time-unit';
import {
	CustomTimeHeaderBuilder,
	DayTimeHeaderBuilder,
	HourTimeHeaderBuilder, MonthTimeHeaderBuilder, QuarterTimeHeaderBuilder,
	TimeUnitHeaderBuilder,
	WeekTimeHeaderBuilder
} from './time-header-builder';
import { TimeUnitDisplayComponent } from './time-unit-display/time-unit-display.component';

@Component({
	selector: 'nuc-planning-table-time-header',
	templateUrl: './planning-table-time-header.component.html',
	styleUrls: ['./planning-table-time-header.component.css'],
})
export class PlanningTableTimeHeaderComponent implements OnInit {

	@Input() planningView: PlanningView;
	@Input() viewConfiguration: PlanningViewConfiguration;
	@Output() cellSizeChanged = new EventEmitter<any>();
	@Output() savePreferences = new EventEmitter<any>();

	@ViewChild('timeContainer') timeContainer: ElementRef;
	@ViewChildren('timeUnitDisplay') timeDisplays: QueryList<TimeUnitDisplayComponent>;

	displaySize: number;

	resizeHandler: ResizeHandler;

	timeUnitHeader: TimeUnitHeader;
	headerBuilder?: TimeUnitHeaderBuilder;
	availableHeaderBuilders: TimeUnitHeaderBuilder[] = [];

	constructor(public changeDetectorRef: ChangeDetectorRef) {
		this.resizeHandler = new ResizeHandler(this.changeBaseTimeUnitSize.bind(this), this.changeBaseTimeUnitSize.bind(this));
		this.availableHeaderBuilders.push(new CustomTimeHeaderBuilder());
		this.availableHeaderBuilders.push(new HourTimeHeaderBuilder());
		this.availableHeaderBuilders.push(new DayTimeHeaderBuilder());
		this.availableHeaderBuilders.push(new WeekTimeHeaderBuilder());
		this.availableHeaderBuilders.push(new MonthTimeHeaderBuilder());
		this.availableHeaderBuilders.push(new QuarterTimeHeaderBuilder());
	}

	ngOnInit(): void {
		this.updateDisplaySize();
		this.initializeTimeUnitDisplays(this.viewConfiguration.baseUnit);
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.initializeTimeUnitDisplays(this.viewConfiguration.baseUnit);
		this.timeUnitHeader.timeDisplays.forEach(x => {
			x.viewConfiguration = this.viewConfiguration;
		});
		if (this.timeDisplays) {
			this.timeDisplays.forEach(x => {
				x.setViewConfiguration(this.viewConfiguration);
			});
		}
		this.updateDisplaySize();
		this.refreshTimeUnitDisplays();
	}

	changeBaseTimeUnitSize(val: number) {
		this.viewConfiguration.baseTimeUnitCellSize =
			Math.max(PlanningViewConfiguration.MINIMUM_BASE_TIME_UNIT_SIZE, val);

		this.cellSizeChanged.emit();
		this.updateDisplaySize();
		this.refreshTimeUnitDisplays();
	}

	refreshDisplay() {
		this.updateDisplaySize();

		this.refreshTimeUnitDisplays();
	}

	ngAfterViewInit() {
		this.refreshTimeUnitDisplays();
	}

	/**
	 * Recalculates the space the container needs to display the header.
	 * An arbitrary amount of 64 is added because when the grid has scrollbars, there is a little offset, when scrolling to the end.
	 */
	updateDisplaySize() {
		this.displaySize = this.viewConfiguration.numBaseUnitAllocations * this.viewConfiguration.baseTimeUnitCellSize + 512;
	}

	/**
	 * Initializes or updates to the right Header Display
	 * @param timeUnit
	 */
	initializeTimeUnitDisplays(timeUnit: TimeUnit) {
		let newHeaderBuilder = this.availableHeaderBuilders.find(x => (x.baseUnit.abbreviation === timeUnit.abbreviation));
		if (newHeaderBuilder !== this.headerBuilder) {
			this.headerBuilder = newHeaderBuilder;
			if (this.headerBuilder) {
				this.timeUnitHeader = this.headerBuilder.buildHeader(this.viewConfiguration, this.planningView);
			}
		}
	}

	/**
	 * Recalculates the layout for all time-unit-display.components and updates their references to the current viewConfiguration.
	 * @private
	 */
	private refreshTimeUnitDisplays() {
		if (this.timeDisplays) {
			this.timeDisplays.forEach(item => {
				item.viewConfiguration = this.viewConfiguration;
				item.layoutHeader();
			});
		}
	}

}

/**
 * A TimeUnitHeader is defined by multiple rows/columns of TimeUnitDisplays.
 * Depending on the alignment of the planning-table each row/column only contains cells for the time-unit specified
 * in the TimeUnitDisplay of this row/column.
 * @see TimeUnitDisplay
 */
export class TimeUnitHeader {

	timeDisplays: TimeUnitDisplay[] = [];

	constructor() {

	}

}

/** A TimeUnitDisplay displays a sequence of time interval cells for one time unit. **/
export class TimeUnitDisplay {

	timeUnit: TimeUnit;
	viewConfiguration: PlanningViewConfiguration;
	/** function that will format the interval of the time-unit-display.component **/
	formattingFunction: any;

	constructor(viewConfiguration: PlanningViewConfiguration, timeUnit: TimeUnit, formattingFunction) {
		this.viewConfiguration = viewConfiguration;
		this.timeUnit = timeUnit;
		this.formattingFunction = formattingFunction;
	}

}
