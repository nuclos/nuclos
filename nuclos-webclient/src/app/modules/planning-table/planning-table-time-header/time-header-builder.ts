import { TimeRepresentation } from '@modules/planning-table/time/time-representation';
import { PlanningView } from '../shared/planning-view';
import { PlanningViewConfiguration } from '../shared/planning-view-configuration';
import { TimeUnit, TimeUnits } from '../time/time-unit';
import { TimeUnitCustom } from '../time/time-unit-custom';
import { TimeUnitDisplay, TimeUnitHeader } from './planning-table-time-header.component';

/**
 * A TimeUnitHeaderBuilder specifies for a certain base time unit, which TimeUnitDisplays should be used in the TimeUnitHeader.
 * Each time the user changes the time-unit, the planning-table-timer-header searches for the HeaderBuilder of the new TimeUnit
 * and calls the buildHeader() function.
 */
export abstract class TimeUnitHeaderBuilder {

	/** the baseUnit this builder builds a header for **/
	baseUnit: TimeUnit;

	abstract buildHeader(viewConfiguration: PlanningViewConfiguration, planningView: PlanningView);

}

export class CustomTimeHeaderBuilder extends TimeUnitHeaderBuilder {

	constructor() {
		super();
		this.baseUnit = new TimeUnitCustom([]);
	}

	buildHeader(viewConfiguration: PlanningViewConfiguration, planningView: PlanningView) {
		if (planningView.customTimeUnit) {
			let timeUnitHeader: TimeUnitHeader = new TimeUnitHeader();
			timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_WEEK, TimeRepresentation.week));
			timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_DAY, TimeRepresentation.day_of_month));
			timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, planningView.customTimeUnit, TimeRepresentation.custom));
			return timeUnitHeader;
		}
		return null;
	}

}

export class HourTimeHeaderBuilder extends TimeUnitHeaderBuilder {

	constructor() {
		super();
		this.baseUnit = TimeUnits.UNIT_HOUR;
	}

	buildHeader(viewConfiguration: PlanningViewConfiguration, planningView: PlanningView) {
		let timeUnitHeader: TimeUnitHeader = new TimeUnitHeader();
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_WEEK, TimeRepresentation.week));
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_DAY, TimeRepresentation.day_of_month));
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_HOUR, TimeRepresentation.hour_interval));
		return timeUnitHeader;
	}

}

export class DayTimeHeaderBuilder extends TimeUnitHeaderBuilder {

	constructor() {
		super();
		this.baseUnit = TimeUnits.UNIT_DAY;
	}

	buildHeader(viewConfiguration: PlanningViewConfiguration, planningView: PlanningView) {
		let timeUnitHeader: TimeUnitHeader = new TimeUnitHeader();
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_MONTH, TimeRepresentation.month_and_year));
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_WEEK, TimeRepresentation.week));
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_DAY, TimeRepresentation.day_of_month));
		return timeUnitHeader;
	}

}

export class WeekTimeHeaderBuilder extends TimeUnitHeaderBuilder {

	constructor() {
		super();
		this.baseUnit = TimeUnits.UNIT_WEEK;
	}

	buildHeader(viewConfiguration: PlanningViewConfiguration, planningView: PlanningView) {
		let timeUnitHeader: TimeUnitHeader = new TimeUnitHeader();
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_MONTH, TimeRepresentation.month_and_year));
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_WEEK, TimeRepresentation.week));
		return timeUnitHeader;
	}

}

export class MonthTimeHeaderBuilder extends TimeUnitHeaderBuilder {

	constructor() {
		super();
		this.baseUnit = TimeUnits.UNIT_MONTH;
	}

	buildHeader(viewConfiguration: PlanningViewConfiguration, planningView: PlanningView) {
		let timeUnitHeader: TimeUnitHeader = new TimeUnitHeader();
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_QUARTER, TimeRepresentation.quarter_year));
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_MONTH, TimeRepresentation.month_3_leading_chars));
		return timeUnitHeader;
	}

}

export class QuarterTimeHeaderBuilder extends TimeUnitHeaderBuilder {

	constructor() {
		super();
		this.baseUnit = TimeUnits.UNIT_QUARTER;
	}

	buildHeader(viewConfiguration: PlanningViewConfiguration, planningView: PlanningView) {
		let timeUnitHeader: TimeUnitHeader = new TimeUnitHeader();
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_YEAR, TimeRepresentation.full_year_only));
		timeUnitHeader.timeDisplays.push(new TimeUnitDisplay(viewConfiguration, TimeUnits.UNIT_QUARTER, TimeRepresentation.quarter_only));
		return timeUnitHeader;
	}

}
