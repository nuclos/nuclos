import { Component, Input, OnInit } from '@angular/core';
import { PlanningViewConfiguration } from '@modules/planning-table/shared/planning-view-configuration';

@Component({
	selector: 'nuc-time-unit-cell',
	templateUrl: './time-unit-cell.component.html',
	styleUrls: ['./time-unit-cell.component.css']
})
export class TimeUnitCellComponent implements OnInit {

	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() label: string;
	@Input() start: number;
	@Input() end: number;

	constructor() {
	}

	ngOnInit(): void {
	}

}
