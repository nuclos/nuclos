import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeUnitDisplayComponent } from './time-unit-display.component';

describe('TimeUnitDisplayComponent', () => {
	let component: TimeUnitDisplayComponent;
	let fixture: ComponentFixture<TimeUnitDisplayComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [TimeUnitDisplayComponent]
		})
			.compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(TimeUnitDisplayComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
