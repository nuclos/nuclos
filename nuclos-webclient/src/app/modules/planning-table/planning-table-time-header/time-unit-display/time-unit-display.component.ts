import {
	ChangeDetectorRef,
	Component,
	Input,
	OnInit,
} from '@angular/core';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { GridLocator } from '../../shared/grid-locator';
import { PlanningTableAlignment } from '../../shared/planning-table-alignment';
import { PlanningViewConfiguration } from '../../shared/planning-view-configuration';
import { TimeAllocation } from '../../time/time-allocation';
import { TimeUnit } from '../../time/time-unit';

@Component({
	selector: 'nuc-time-unit-display',
	templateUrl: './time-unit-display.component.html',
	styleUrls: ['./time-unit-display.component.css']
})
export class TimeUnitDisplayComponent implements OnInit {

	static readonly FIXED_DIMENSION_SIZE_VERTICAL = 48;
	static readonly FIXED_DIMENSION_SIZE_HORIZONTAL = 48;

	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() timeUnit: TimeUnit;
	@Input() dateFormattingFunction;

	timeElements: Map<number, TimeAllocationContext> = new Map<number, TimeAllocationContext>();
	gridLocator: GridLocator;

	constructor(
		public i18n: NuclosI18nService,
		private changeDetectorRef?: ChangeDetectorRef
	) {

	}

	ngOnInit(): void {
		this.gridLocator = new GridLocator(this.viewConfiguration);
	}

	ngOnChanges(): void {
		this.clear();
		this.gridLocator = new GridLocator(this.viewConfiguration);
		this.layoutHeader();
	}

	ngAfterViewInit(): void {
		this.layoutHeader();
	}

	setViewConfiguration(viewConfiguration: PlanningViewConfiguration) {
		this.viewConfiguration = viewConfiguration;
		this.gridLocator = new GridLocator(this.viewConfiguration);
	}

	clear() {
		this.timeElements.clear();
	}

	layoutHeader() {
		let areaStart;
		let areaEnd;

		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			areaStart = this.viewConfiguration.horizontalScroll;
			areaEnd = this.viewConfiguration.horizontalScroll + this.viewConfiguration.viewportWidth;
		} else {
			areaStart = this.viewConfiguration.verticalScroll;
			areaEnd = this.viewConfiguration.verticalScroll + this.viewConfiguration.viewportHeight;
		}

		this.destroyCellsOutsideView(areaStart, areaEnd);

		this.addNewCellsToViewport(areaStart, areaEnd);

		this.changeDetectorRef?.detectChanges();
	}

	destroyCellsOutsideView(timeStart: number, timeEnd: number) {
		this.timeElements.forEach((value: TimeAllocationContext, key: number) => {
			if (value.locationStart * this.viewConfiguration.baseTimeUnitCellSize > timeEnd
				|| value.locationEnd * this.viewConfiguration.baseTimeUnitCellSize < timeStart) {
				this.timeElements.delete(key);
			}
		});
	}

	/**
	 * This functions computes the correct location of time unit cells.
	 * Because not all time units are linear (1 month is not always equal to one month) this function
	 * is a bit more complicated.
	 * @param areaStart the first visible Pixel in the TimeHeader Viewport
	 * @param areaEnd the last visible Pixel in the TimeHeader Viewport
	 */
	addNewCellsToViewport(areaStart: number, areaEnd: number) {
		let startCellIndex: number = Math.floor(areaStart / this.viewConfiguration.baseTimeUnitCellSize);
		let endCellIndex: number = Math.min(
			this.viewConfiguration.numBaseUnitAllocations,
			Math.ceil(areaEnd / this.viewConfiguration.baseTimeUnitCellSize)
		);

		let currentStartTime: number = this.timeUnit.getNextStep(
			new Date(this.viewConfiguration.baseUnit.getNextStep(this.viewConfiguration.dateFrom, startCellIndex)), 0);
		let currentEndTime: number = this.timeUnit.getStepEnd(new Date(currentStartTime)).getTime();

		let timeEnd: Date = this.timeUnit.getNextStep(
			new Date(this.viewConfiguration.baseUnit.getNextStep(this.viewConfiguration.dateFrom, endCellIndex)), 0);
		timeEnd = new Date(this.viewConfiguration.baseUnit.getStepEnd(new Date(timeEnd)));
		timeEnd = this.viewConfiguration.baseUnit.getStepEnd(timeEnd);
		do {
			let locationBeginning = this.gridLocator.computeGridLocation(new Date(currentStartTime));
			let locationEnd = this.gridLocator.computeGridLocation(new Date(currentEndTime));
			let ignore = false;

			if (locationBeginning.getBaseTimeLocation() === locationEnd.getBaseTimeLocation()) {
				ignore = true;
			}

			if (!ignore && (locationEnd.getBaseTimeLocation() - locationBeginning.getBaseTimeLocation()) > 0) {
				if (!this.timeElements.has(locationEnd.getBaseTimeLocation())) {
					let timeAllocation: TimeAllocation = new TimeAllocation(new Date(currentStartTime), new Date(currentEndTime), this.timeUnit);
					let context: TimeAllocationContext = new TimeAllocationContext(this.dateFormattingFunction(timeAllocation, this.i18n));
					context.locationStart = locationBeginning.getBaseTimeLocation();
					context.locationEnd = locationEnd.getBaseTimeLocation();
					this.timeElements.set(locationEnd.getBaseTimeLocation(), context);
				} else {
					let viewRef: TimeAllocationContext | undefined = this.timeElements.get(locationEnd.getBaseTimeLocation());
					if (viewRef) {
						viewRef.locationStart = locationBeginning.getBaseTimeLocation();
						viewRef.locationEnd = locationEnd.getBaseTimeLocation();
					}

				}
			}

			currentStartTime = this.timeUnit.getNextStep(new Date(currentStartTime), 1).getTime();
			currentEndTime = this.timeUnit.getStepEnd(new Date(currentStartTime)).getTime();
		}
		while (currentStartTime < timeEnd.getTime());
	}

	cellTrackBy(index, timeCellContext) {
		return timeCellContext.locationStart;
	}

}

export class TimeAllocationContext {

	public locationStart: number;
	public locationEnd: number;

	constructor(public allocationLabel: string) {

	}

}
