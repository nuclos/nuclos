import {
	AfterViewInit,
	ChangeDetectionStrategy, ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnInit, Output,
	ViewChild
} from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { EntityMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { EntityObjectService } from '@modules/entity-object-data/shared/entity-object.service';
import { MetaService } from '@modules/entity-object-data/shared/meta.service';
import { PlanningModalComponent } from '@modules/planning-table/planning-modal/planning-modal.component';
import { PlanningExport } from '@modules/planning-table/shared/planning-export';
import { CellSelection } from '@modules/planning-table/shared/selection/cell-selection';
import { ElementSelection } from '@modules/planning-table/shared/selection/element-selection';
import { DateUtils } from '@modules/planning-table/time/date-utils';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import * as moment from 'moment';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { iif, of, Subscriber } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { DatechooserComponent } from '../../ui-components/datechooser/datechooser.component';
import { TooltipDefaultOptions } from '../../ui-components/tooltip/tooltip-default-options';
import { PlanningTableComponent } from '../planning-table.component';
import {
	Filter,
	PlanningTable,
	PlanningTableService,
	BookingDef,
	MilestoneDef,
	RelationDef, Booking
} from '../planning-table.service';
import { PlanningView } from '../shared/planning-view';
import { PlanningViewConfiguration } from '../shared/planning-view-configuration';
import { TimeUnits } from '../time/time-unit';

@Component({
	changeDetection: ChangeDetectionStrategy.OnPush,
	selector: 'nuc-planning-table-toolbar',
	templateUrl: './planning-table-toolbar.component.html',
	styleUrls: ['./planning-table-toolbar.component.css']
})
export class PlanningTableToolbarComponent implements OnInit, AfterViewInit {

	@Input() planningTable: PlanningTable;
	@Input() planningTableComponent: PlanningTableComponent;
	@Input() viewConfiguration: PlanningViewConfiguration;
	@Input() planningView: PlanningView;
	@Input() elementSelection: ElementSelection;
	@Input() cellSelection: CellSelection;

	@Output() reloadPlanningTable: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild('chooserFrom') fromComponent: DatechooserComponent;
	@ViewChild('chooserUntil') untilComponent: DatechooserComponent;

	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	timeModes = [
		TimeUnits.UNIT_HOUR,
		TimeUnits.UNIT_DAY,
		TimeUnits.UNIT_WEEK,
		TimeUnits.UNIT_MONTH,
		TimeUnits.UNIT_QUARTER
	];

	searchFilters;
	currentRelationFrom: Booking;

	subscriberFrom = Subscriber.create(x => {
		this.nextDateFrom = moment(x as string, 'YYYY-MM-DD').toDate();
	});

	subscriberUntil = Subscriber.create(x => {
		this.nextDateUntil = moment(x as string, 'YYYY-MM-DD').toDate();
	});

	/**
	 * Note: to prevent the planning table from loading after every single keystroke inside a date field, we store the
	 * date in a temporary variable until the user confirms the date either by click, enter or focusout.
	 */
	private nextDateFrom;
	private nextDateUntil;

	constructor(
		protected i18: NuclosI18nService,
		protected modalService: NuclosDialogService,
		private eoService: EntityObjectService,
		private metaService: MetaService,
		private planningTableService: PlanningTableService,
		public changeDetectorRef: ChangeDetectorRef
	) {

	}

	ngOnInit(): void {
		if (this.planningTableComponent.planningView.customTimeUnit) {
			this.timeModes.push(this.planningTableComponent.planningView.customTimeUnit);
		}
	}

	ngOnChanges(): void {
		this.searchFilters = [];
		this.searchFilters.push(undefined);
		this.planningTable.searchFilters?.forEach((filter: Filter) => {
			this.searchFilters.push(filter);
		});
		if (this.fromComponent) {
			this.fromComponent.dateModel = DateUtils.toNgbDate(this.viewConfiguration.dateFrom);
		}
		if (this.untilComponent) {
			this.untilComponent.dateModel = DateUtils.toNgbDate(this.viewConfiguration.dateUntil);
		}
	}

	ngAfterViewInit() {
		this.fromComponent.modelChange.subscribe(this.subscriberFrom);
		this.untilComponent.modelChange.subscribe(this.subscriberUntil);
	}

	ngOnDestroy() {
		this.subscriberFrom.unsubscribe();
		this.subscriberUntil.unsubscribe();
	}

	setShowOwnEventsOnly($event) {
		this.viewConfiguration.showOwnEventsOnly = $event.checked;
		this.planningTableComponent.refresh();
	}

	isBooking(value) {
		let meta = value['entityMeta'];
		if (meta !== undefined) {
			let m = this.planningView.bookingDefMetas.find(x => (x.planElementDef.bookingDef?.entity === meta));
			return (m !== undefined);
		}
		return false;
	}

	onFromPickerLeft() {
		if (!this.nextDateFrom) {
			return;
		}
		if (isNaN(this.nextDateFrom)) {
			this.nextDateFrom = undefined;
			return;
		}
		if (this.viewConfiguration.dateFrom.getTime() !== this.nextDateFrom.getTime()) {
			this.planningTableComponent.setDateFrom(this.nextDateFrom);
			this.nextDateFrom = this.viewConfiguration.dateFrom;
			this.nextDateUntil = this.viewConfiguration.dateUntil;
			// update the text in the date picker, because it might still have focus and will only update once the focus is lost
			this.fromComponent.setInputText(moment(this.viewConfiguration.dateFrom).format(this.fromComponent.datePattern));
			this.untilComponent.setInputText(moment(this.viewConfiguration.dateUntil).format(this.untilComponent.datePattern));
		}
	}

	onUntilPickerLeft() {
		if (!this.nextDateUntil) {
			return;
		}
		if (isNaN(this.nextDateUntil)) {
			this.nextDateUntil = undefined;
			return;
		}
		this.nextDateUntil.setHours(23, 59, 59, 999);
		if (this.viewConfiguration.dateUntil.getTime() !== this.nextDateUntil.getTime()) {
			this.planningTableComponent.setDateUntil(this.nextDateUntil);
			this.nextDateFrom = this.viewConfiguration.dateFrom;
			this.nextDateUntil = this.viewConfiguration.dateUntil;
			this.fromComponent.model = this.viewConfiguration.dateFrom;
			this.untilComponent.model = this.viewConfiguration.dateUntil;
			// update the text in the date picker, because it might still have focus and will only update once the focus is lost
			this.fromComponent.setInputText(moment(this.viewConfiguration.dateFrom).format(this.fromComponent.datePattern));
			this.untilComponent.setInputText(moment(this.viewConfiguration.dateUntil).format(this.untilComponent.datePattern));
		}
	}

	/**
	 * Opens the planning-modal-component.ts modal for mobile devices.
	 */
	openModal() {
		this.modalService.custom(
			PlanningModalComponent,
			{
				planningTable: this.planningTable,
				planningTableComponent: this.planningTableComponent,
				viewConfiguration: this.viewConfiguration
			}, {
				size: 'sm',
			}
		).pipe(
			take(1)
		).subscribe();
	}

	removeSelectedElements() {
		if (this.elementSelection) {
			let metaEntitiesMap: Map<string, number[]> = new Map<string, number[]>();
			this.elementSelection.selectedElements.forEach(entity => {
				let ids: number[] | undefined = metaEntitiesMap.get(entity.entityMeta);
				if (ids !== undefined) {
					ids.push(entity.id);
				} else {
					ids = [entity.id];
					metaEntitiesMap.set(entity.entityMeta, ids);
				}
			});
			metaEntitiesMap.forEach((value, key) => {
				this.eoService.deleteByIds(key, value).subscribe(() => {
					},
					(error) => {
					});
			});
		}
		this.reloadPlanningTable.emit();
	}

	showDetailsForSelectedElement() {
		let metaId = this.elementSelection.selectedElements[0].entityMeta;
		let eoId = this.elementSelection.selectedElements[0].id;
		this.metaService.getBoMeta(metaId).pipe(
			switchMap(
				(meta) =>
					iif(
						() => meta.getLinks().defaultLayout !== undefined,
						this.eoService.loadEO(meta.getEntityClassId(), eoId)
							.pipe(
								switchMap(
									(eo) => this.modalService.openEoInModal(eo)
										.pipe(
											switchMap(
												() =>
													iif(
														() => eo.isDirty(),
														this.eoService.save(eo).pipe(
															tap(() => this.reloadPlanningTable.emit())
														),
														of(void 0)
													)
											)
										)
								)
							),
						of(void 0)
					)
			),
			take(1)
		).subscribe();
	}

	createPlanElement(meta: EntityMeta, metaId: string) {
		if (meta.getLinks().defaultLayout === undefined) {
			return;
		}
		this.eoService.createNew(metaId).pipe(
			take(1),
			tap(
				(eo) => this.planningTableService.getPlanElementDef(this.planningView.planningTableId, metaId)
					.pipe(
						take(1),
						tap(
							(planElement) => {
								if (planElement.milestoneDef) {
									this.applyMilestoneValues(eo, planElement.milestoneDef);
								} else if (planElement.bookingDef) {
									this.applyBookingValues(eo, planElement.bookingDef);
								}
							}
						)
					).subscribe()
			),
			switchMap(eo => this.modalService.openEoInModal(eo)
				.pipe(
					switchMap(
						(val) =>
							iif(
								() => eo.isDirty() && val,
								eo.save().pipe(
									tap(() => this.reloadPlanningTable.emit())
								),
								of(void 0)
							)
					)
				))
		).subscribe();
	}

	/**
	 * Stores the booking, where the user would like the next relation to start, if one is created.
	 * @param fromBooking
	 */
	setRelationFrom(fromBooking: Booking) {
		this.currentRelationFrom = fromBooking;
	}

	/**
	 * Creates a new relation for the specified EntityMeta.
	 * @param relationMeta
	 * @param metaId
	 * @param bookingTo
	 * @private
	 */
	createRelation(relationMeta: EntityMeta, metaId: string, bookingTo: Booking) {
		if (relationMeta.getLinks().defaultLayout === undefined) {
			return;
		}
		this.eoService.createNew(metaId).pipe(
			take(1),
			tap(
				(eo) => this.planningTableService.getPlanElementDef(this.planningView.planningTableId, metaId)
					.pipe(
						take(1),
						tap(
							(planElement) => {
								if (planElement.relationDef) {
									this.applyRelationValues(eo, planElement.relationDef, this.currentRelationFrom, bookingTo);
								}
							}
						)
					).subscribe()
			),
			switchMap(eo => this.modalService.openEoInModal(eo)
				.pipe(
					switchMap(
						(val) =>
							iif(
								() => eo.isDirty() && val,
								eo.save().pipe(
									tap(() => this.reloadPlanningTable.emit())
								),
								of(void 0)
							)
					)
				))
		).subscribe();
	}

	private applyBookingValues(eo: IEntityObject, bookingDef: BookingDef) {
		if (this.cellSelection) {
			eo.setAttribute(bookingDef.dateFromField, moment(this.cellSelection.timeFrom).format('YYYY-MM-DD'));
			eo.setAttribute(bookingDef.dateUntilField, moment(this.cellSelection.timeUntil).format('YYYY-MM-DD'));
			if (bookingDef.timeFromField) {
				eo.setAttribute(bookingDef.timeFromField, moment(this.cellSelection.timeFrom).format('HH:mm'));
			}
			if (bookingDef.timeUntilField) {
				eo.setAttribute(bookingDef.timeUntilField, moment(this.cellSelection.timeUntil).format('HH:mm'));
			}
			let ref = {
				id: +this.cellSelection.resource.resourceId,
				name: this.cellSelection.resource.label
			};
			eo.setAttribute(bookingDef.resourceRefField, ref);
		} else {
			if (this.elementSelection.creationFromTime && this.elementSelection.creationUntilTime) {
				eo.setAttribute(bookingDef.dateFromField, moment(this.elementSelection.creationFromTime).format('YYYY-MM-DD'));
				eo.setAttribute(bookingDef.dateUntilField, moment(this.elementSelection.creationUntilTime).format('YYYY-MM-DD'));
				if (bookingDef.timeFromField) {
					eo.setAttribute(bookingDef.timeFromField, moment(this.elementSelection.creationFromTime).format('HH:mm'));
				}
				if (bookingDef.timeUntilField) {
					eo.setAttribute(bookingDef.timeUntilField, moment(this.elementSelection.creationUntilTime).format('HH:mm'));
				}
				let ref = {
					id: +this.elementSelection.creationResource.resourceId,
					name: this.elementSelection.creationResource.label
				};
				eo.setAttribute(bookingDef.resourceRefField, ref);
			}
		}
	}

	private applyMilestoneValues(eo, milestoneDef: MilestoneDef) {
		if (this.cellSelection) {
			eo.setAttribute(milestoneDef.dateFromField, moment(this.cellSelection.timeFrom).format('YYYY-MM-DD'));
			let ref = {
				id: +this.cellSelection.resource.resourceId,
				name: this.cellSelection.resource.label
			};
			eo.setAttribute(milestoneDef.resourceRefField, ref);
		} else {
			if (this.elementSelection.creationFromTime) {
				eo.setAttribute(milestoneDef.dateFromField, moment(this.elementSelection.creationFromTime).format('YYYY-MM-DD'));
			}
			let ref = {
				id: +this.elementSelection.creationResource.resourceId,
				name: this.elementSelection.creationResource.label
			};
			eo.setAttribute(milestoneDef.resourceRefField, ref);
		}
	}

	private applyRelationValues(eo, relationDef: RelationDef, bookingFrom: Booking, bookingTo: Booking) {
		let fromRef = {
			id: +bookingFrom.id,
			name: bookingFrom.label
		};
		let toRef = {
			id: +bookingTo.id,
			name: bookingTo.label
		};
		eo.setAttribute(relationDef.fromField, fromRef);
		eo.setAttribute(relationDef.toField, toRef);
	}

	private exportSvg() {
		let exporter = new PlanningExport(this.i18);
		let name = this.planningView.planningTable.componentLabel;
		exporter.exportSvg(this.planningTableComponent).then(function (dataUrl) {
			let link = document.createElement('a');
			link.download = name + '.svg';
			link.href = dataUrl;
			link.click();
			link.remove();
		});
	}

}
