import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, ElementRef, Input, NgZone, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { MetaService } from '@modules/entity-object-data/shared/meta.service';
import {
	PlanningTableToolbarComponent
} from '@modules/planning-table/planning-table-toolbar/planning-table-toolbar.component';
import { ResizeHandler } from '@modules/planning-table/shared/resize-handler';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import {
	TransformationScaleEnd,
	TransformationScaleStart,
	TransformationTranslateSelection
} from '@modules/planning-table/shared/transform/transformation';
import { IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { StringUtils } from '@shared/string-utils';
import { map, switchMap, take } from 'rxjs/operators';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { BusyService } from '@shared/service/busy.service';
import { DatetimeService } from '@shared/service/datetime.service';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { NuclosTitleService } from '@shared/service/nuclos-title.service';
import { Logger } from '../log/shared/logger';
import { PreferencesService } from '../preferences/preferences.service';
import { PlanningGridComponent } from './planning-grid/planning-grid.component';
import {
	PlanningTableResourceHeaderComponent
} from './planning-table-resource-header/planning-table-resource-header.component';
import { PlanningTableTimeHeaderComponent } from './planning-table-time-header/planning-table-time-header.component';
import { PlanningTable, PlanningTableService } from './planning-table.service';
import { PlanningTableAlignment } from './shared/planning-table-alignment';
import { PlanElementDefMeta, PlanningView } from './shared/planning-view';
import { PlanningViewConfiguration } from './shared/planning-view-configuration';
import { Interval } from './time/interval';
import { TimeUnit } from './time/time-unit';

@Component({
	selector: 'nuc-planning-table',
	templateUrl: './planning-table.component.html',
	styleUrls: ['./planning-table.component.css']
})
export class PlanningTableComponent implements OnInit {

	public loading = true;
	public initialized = false;

	planningView: PlanningView;
	@Input() planningTableId: string;
	@Input() dateFrom: Date;
	@Input() dateUntil: Date;
	@Input() layoutId: string;
	@Input() layoutEo: EntityObject;
	@Input() resourceForeignKey: string;
	@Input() usePadding = true;
	@Input() enabled = true;
	planningTable: PlanningTable;
	viewConfiguration: PlanningViewConfiguration;
	inputProcessor: InputProcessor;
	headerResizer: ResizeHandler;
	formattedLegendLabel: string;
	formattedLegendTooltip: string;

	@ViewChild('scrollDiv') scrollDiv: ElementRef;
	@ViewChild('displayContainer') displayContainer: ElementRef;
	@ViewChild('verticalHeader') verticalHeader: ElementRef;
	@ViewChild('horizontalHeader') horizontalHeader: ElementRef;
	@ViewChild('gridComponent') gridComponent: PlanningGridComponent;
	@ViewChild('resourceHeader') resourceHeader: PlanningTableResourceHeaderComponent;
	@ViewChild('timeHeader') timeHeader: PlanningTableTimeHeaderComponent;
	@ViewChild('toolbar') toolbar: PlanningTableToolbarComponent;

	private eoListener: IEntityObjectEventListener = {
		afterCancel: () => {
			this.formatLegend();
		},
		afterValidationChange: () => {
			this.formatLegend();
		},
		afterAttributeChange: () => {
			this.formatLegend();
		}
	};
	private periodicRefreshTimer: any;

	constructor(
		private log: Logger,
		private nuclosConfig: NuclosConfigService,
		private nuclosHttp: HttpClient,
		protected i18: NuclosI18nService,
		private renderer2: Renderer2,
		private ngZone: NgZone,
		public planningTableService: PlanningTableService,
		private metaService: MetaService,
		private preferenceService: PreferencesService,
		private changeDetectorRef: ChangeDetectorRef,
		private dateTimeService: DatetimeService,
		private busyService: BusyService,
		private titleService: NuclosTitleService,
		private dialogService: NuclosDialogService,
		protected route: ActivatedRoute,
	) {
	}

	ngOnInit() {
		this.loading = true;
		if (this.layoutEo) {
			this.layoutEo.addListener(this.eoListener);
		}
		if (this.planningTableId) {
			this.planningTableService.loadPlanningView(this.planningTableId, this.dateFrom, this.dateUntil, this.layoutId, this.resourceForeignKey)
				.subscribe((planningView) => {
					this.planningView = planningView;
					this.planningView.elementSelection.reloadPlanningTable = this.refresh.bind(this);
					this.planningTable = planningView.planningTable;
					this.viewConfiguration = planningView.preferences.content.planningViewConfiguration;
					this.viewConfiguration.horizontalScroll = 0;
					this.viewConfiguration.verticalScroll = 0;
					this.loadPlanElementMetas();
					this.formatLegend();
					this.setupRefreshTimerIfConfigured();

					this.loading = false;
				});
		} else {
			// gets automatically unsubscribed from angular
			this.route.params.pipe(
				switchMap((params: Params) => this.route.url.pipe(
					map((segments) => segments.join('/')),
					map((url) => {
						return {
							...params,
							url: url
						};
					})
				)),
				switchMap((params: Params) => this.route.queryParams.pipe(
					map((queryParams: Params) => {
							return {
								...params,
								...queryParams
							};
						}
					)))
			).subscribe((params: Params) => {
				this.planningTableId = params['planTableId'];
				this.planningTableService.loadPlanningView(this.planningTableId, this.dateFrom, this.dateUntil, this.layoutId, this.resourceForeignKey)
					.subscribe((planningView) => {
						this.planningView = planningView;
						this.planningView.elementSelection.reloadPlanningTable = this.refresh.bind(this);
						this.planningTable = planningView.planningTable;
						this.viewConfiguration = planningView.preferences.content.planningViewConfiguration;
						this.viewConfiguration.horizontalScroll = 0;
						this.viewConfiguration.verticalScroll = 0;
						this.loadPlanElementMetas();
						this.formatLegend();
						this.setupRefreshTimerIfConfigured();
						this.titleService.setTitle(
							this.i18.getI18n('webclient.planningTable.name') + ' | ' + this.planningTable.componentLabel);
						this.loading = false;
					});
			});
		}
		this.headerResizer = new ResizeHandler(this.onHorizontalResize.bind(this), this.onVerticalResize.bind(this));
		this.headerResizer.sensitivity = 1;
		this.inputProcessor = new InputProcessor(this.renderer2, this.ngZone, this);
	}

	ngOnChanges(simpleChanges: any) {
		if (!this.loading && simpleChanges['layoutId']) {
			// the planning table is embedded as a web-planning-table.component and a different eo entry was selected
			// thus a refresh is needed
			let dateFrom = simpleChanges.dateFrom?.currentValue;
			let dateUntil = simpleChanges.dateUntil?.currentValue;
			this.refresh(dateFrom, dateUntil);
		}
		const eoChange = simpleChanges['layoutEo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				eoChange.previousValue.removeListener(this.eoListener);
			}
			if (eoChange.currentValue) {
				eoChange.currentValue.addListener(this.eoListener);
			}
		}
	}

	ngOnDestroy(): void {
		this.inputProcessor.destroy();
		if (this.layoutEo) {
			this.layoutEo.removeListener(this.eoListener);
		}
		if (this.periodicRefreshTimer) {
			clearInterval(this.periodicRefreshTimer);
		}
	}

	ngAfterContentChecked(): void {
		if (!this.initialized && this.scrollDiv) {
			// initialize the viewport size to the current browser size, when the scrollDiv is ready
			this.onViewportResize();
			this.initialized = true;
		}
	}

	/**
	 * Recalculates the baseUnitCellSize so the time header fits inside the browser.
	 * If there are too many cells, it will stop at a minimum size value.
	 * Automatically saves the new baseUnitCellSize in the preferences.
	 */
	fitContent() {
		let scrollbarSize = this.scrollDiv.nativeElement.offsetWidth - this.scrollDiv.nativeElement.clientWidth;
		this.viewConfiguration.viewportWidth = this.scrollDiv.nativeElement.getBoundingClientRect().width;
		this.viewConfiguration.viewportHeight = this.scrollDiv.nativeElement.getBoundingClientRect().height;
		PlanningViewConfiguration.fitHeaderIntoViewport(this.viewConfiguration, scrollbarSize);
		this.refreshGrid();
		this.timeHeader.refreshDisplay();
		this.resourceHeader.layoutHeader();
		this.savePreferences();
	}

	/**
	 * Saves the current preferences & then refreshes the planning table.
	 * If dateFrom & dateUntil are not specified, it will choose the date boundaries from the preferences it just saved.
	 **/
	refresh(dateFrom?, dateUntil?) {
		this.planningTableService.savePreferences(this.planningView).subscribe((x) => {
			this.busyService.busy(
				this.planningTableService.loadPlanningView(this.planningTableId, dateFrom, dateUntil,
					this.layoutId, this.resourceForeignKey)
			).pipe(take(1)).subscribe((planningView) => {
					let oldView = this.planningView;
					this.planningView = planningView;
					this.planningView.elementSelection.reloadPlanningTable = this.refresh.bind(this);
					this.planningView.restoreSelection(oldView);
					this.planningTable = planningView.planningTable;
					this.viewConfiguration = planningView.preferences.content.planningViewConfiguration;
					this.loadPlanElementMetas();
					if (this.isStandalone()) {
						this.titleService.setTitle(
							this.i18.getI18n('webclient.planningTable.name') + ' | ' + this.planningTable.componentLabel);
					}
				}
			);
		});
	}

	loadPlanElementMetas() {
		this.planningView.bookingDefMetas = [];
		this.planningView.milestoneDefMetas = [];
		this.planningView.relationDefMetas = [];
		this.planningView.metaMap.clear();
		this.planningTableService.getAllPlanElementDefs(this.planningTableId).subscribe(result => {
			result.planElementDefs.forEach(planElementDef => {
				let meta;
				let targetList;
				if (planElementDef.bookingDef) {
					meta = planElementDef.bookingDef.entity;
					targetList = this.planningView.bookingDefMetas;
				} else if (planElementDef.milestoneDef) {
					meta = planElementDef.milestoneDef.entity;
					targetList = this.planningView.milestoneDefMetas;
				} else if (planElementDef.relationDef) {
					meta = planElementDef.relationDef.entity;
					targetList = this.planningView.relationDefMetas;
				}
				if (meta) {
					this.metaService.getBoMeta(meta).subscribe((loadedMeta) => {
						this.planningView.metaMap.set(meta, loadedMeta);
						targetList.push(new PlanElementDefMeta(meta, planElementDef, loadedMeta));
					});
				}
			});
		});
	}

	/**
	 * Determines, whether the planning table is used as a standalone component or embedded inside a layout.
	 * Returns true, if used as standalone component
	 * Returns false, if embedded in layout
	 */
	isStandalone(): boolean {
		return !this.layoutId;
	}

	/**
	 * Saves the preferences of the planningView managed by this component.
	 */
	savePreferences() {
		this.planningTableService.savePreferences(this.planningView).subscribe();
	}

	/* this resizes the vertical header along the x-axis */
	onHorizontalResize(val: number) {
		this.resizeContentWidth(val);
	}

	/* this resizes the horizontal header along the y-axis */
	onVerticalResize(val: number) {
		this.resizeContentHeight(val);
	}

	/**
	 * Resizes the width of the vertical content header.
	 * The width is always calculated relatively to the width of the viewport next to it.
	 * @param newContentWidth
	 */
	resizeContentWidth(newContentWidth: number) {
		this.viewConfiguration.verticalHeaderWidth = Math.max(32, newContentWidth);
		this.viewConfiguration.viewportWidth = this.scrollDiv.nativeElement.getBoundingClientRect().width;
	}

	/**
	 * Resizes the height of the horizontal header.
	 * The height is always calculated relatively to the height of the viewport next to it.
	 * @param newContentHeight
	 */
	resizeContentHeight(newContentHeight: number) {
		this.viewConfiguration.horizontalHeaderHeight = Math.max(32, newContentHeight);
		this.viewConfiguration.viewportHeight = this.scrollDiv.nativeElement.getBoundingClientRect().height;
	}

	formatLegend() {
		if (this.planningTable) {
			this.formattedLegendLabel = this.formatTemplateText(this.planningTable.legendLabel);
			this.formattedLegendTooltip = this.formatTemplateText(this.planningTable.legendTooltip);
		}
	}

	/**
	 * Formats a template text by substituting the placeholders with the field values of the layout eo
	 * @param templateText
	 */
	formatTemplateText(templateText: string | undefined): string {
		if (!templateText) {
			return '';
		}
		const html: boolean = templateText.startsWith('<html>');
		const pattern = '\\$\\{([a-zA-z0-9\\._-]*)\\}';
		let result = templateText;
		if (!this.layoutEo) {
			return templateText;
		}
		const regex = new RegExp(pattern, 'gm');
		let matches = templateText.match(regex);
		if (matches) {
			matches.forEach(match => {
				let fieldValue = this.layoutEo.getAttribute(match.substring(2, match.length - 1)) || '';
				if (html) {
					fieldValue = StringUtils.xmlEncode(fieldValue);
				}
				result = result.replace('' + match, fieldValue);
			});
		}
		return result;
	}

	/**
	 * Restores the scroll values from the PlanningViewConfiguration.
	 */
	scrollViewToConfiguration() {
		if (this.scrollDiv.nativeElement.scrollLeft !== this.viewConfiguration.horizontalScroll
			|| this.scrollDiv.nativeElement.scrollTop !== this.viewConfiguration.verticalScroll) {
			this.scrollDiv.nativeElement.scrollLeft = this.viewConfiguration.horizontalScroll;
			this.scrollDiv.nativeElement.scrollTop = this.viewConfiguration.verticalScroll;

			this.gridComponent.ngAfterViewInit();
			this.resourceHeader.ngOnChanges();
			this.timeHeader.refreshDisplay();
		}
	}

	/**
	 *  This method synchronizes the scroll between the central div and the 2 header divs.
	 *  When the user scrolls in the main div, the headers should update too.
	 */
	synchronizeHeadersWithViewScroll() {
		this.horizontalHeader.nativeElement.scrollLeft = this.scrollDiv.nativeElement.scrollLeft;
		this.verticalHeader.nativeElement.scrollTop = this.scrollDiv.nativeElement.scrollTop;
		this.viewConfiguration.horizontalScroll = this.scrollDiv.nativeElement.scrollLeft;
		this.viewConfiguration.verticalScroll = this.scrollDiv.nativeElement.scrollTop;
		this.viewConfiguration.viewportWidth = this.scrollDiv.nativeElement.getBoundingClientRect().width;
		this.viewConfiguration.viewportHeight = this.scrollDiv.nativeElement.getBoundingClientRect().height;

		this.resourceHeader.refreshDisplay();
		this.timeHeader.refreshDisplay();
		this.gridComponent.refreshGrid();
	}

	/**
	 * This method performs a heavy refresh on the grid.
	 * The complete grid gets cleared and reinitialized.
	 * Automatically stores the new alignment in the preferences.
	 */
	switchAxis() {
		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.RESOURCE_TIME) {
			this.viewConfiguration.tableAlignment = PlanningTableAlignment.TIME_RESOURCE;
		} else {
			this.viewConfiguration.tableAlignment = PlanningTableAlignment.RESOURCE_TIME;
		}
		let tmp = this.viewConfiguration.horizontalScroll;
		this.viewConfiguration.horizontalScroll = this.viewConfiguration.verticalScroll;
		this.viewConfiguration.verticalScroll = tmp;
		this.gridComponent.clearGrid();
		this.gridComponent.recalculateGridCells();
		this.gridComponent.updateRelations();

		this.savePreferences();
	}

	/**
	 * This method performs a light refresh on the grid.
	 * It is called, when the column or row size has changed.
	 **/
	refreshGrid() {
		this.gridComponent.recalculateGridCells();
	}

	/**
	 * Forces the grid component to recalculate Booking Groups.
	 * Application: This method should be called, when a bookings visibility changed.
	 */
	refreshGridBookingGroups() {
		this.gridComponent.recalculateBookingsGroups();
	}

	/**
	 * Gets called, when the user resizes the browser window or moves it to another screen.
	 * Also, when changing the device in chrome debug mode.
	 * Recalculates the area and which cells are visible inside of it.
	 */
	onViewportResize() {
		this.viewConfiguration.viewportWidth = this.scrollDiv.nativeElement.getBoundingClientRect().width;
		this.viewConfiguration.viewportHeight = this.scrollDiv.nativeElement.getBoundingClientRect().height;
		this.refreshGrid();
		this.resourceHeader.layoutHeader();
		this.timeHeader.refreshDisplay();
	}

	/**
	 * Changes the base time unit of this planning table.
	 * Updates the GridComponent and the Timer Header.
	 * Changing the time unit requires no reloading of the planning-table from the service.
	 * It is only a change in the way of displaying the planning-table data.
	 * Automatically saves the time unit change in the preferences.
	 * @param baseUnit
	 */
	setBaseUnit(baseUnit: TimeUnit) {
		if (baseUnit && baseUnit !== this.viewConfiguration.baseUnit) {
			this.planningView.setBaseUnit(baseUnit);

			this.gridComponent.clearGrid();
			this.gridComponent.recalculateGridCells();

			this.timeHeader.initializeTimeUnitDisplays(this.viewConfiguration.baseUnit);

			this.savePreferences();
		}
	}

	/**
	 * Sets the search filter for this planning view configuration.
	 * Reloads the planning-view via the planning-table-service with the newly selected filter.
	 * Automatically saves the search filter change in the preferences.
	 * @param filter
	 */
	setSearchFilter(filter) {
		if (filter !== this.viewConfiguration.searchFilter) {
			this.viewConfiguration.searchFilter = filter;
			this.refresh();
		}
	}

	getDaysBetweenCurrentInterval() {
		let momentFrom = moment(this.viewConfiguration.dateFrom);
		let momentUntil = moment(this.viewConfiguration.dateUntil);
		return moment.duration(momentUntil.startOf('day').diff(momentFrom)).asDays();
	}

	/**
	 * Changes the start date from which this component displays the planning table.
	 * It automatically saves the new from date to the preferences and reloads the planning table via the service.
	 * @param dateFrom
	 */
	setDateFrom(dateFrom: Date) {
		if (dateFrom && dateFrom.getTime() !== this.viewConfiguration.dateFrom.getTime()) {
			let newInterval;
			let newDateUntil = this.viewConfiguration.dateUntil;
			let daysBetween = this.getDaysBetweenCurrentInterval();
			if (dateFrom.getTime() > this.viewConfiguration.dateUntil.getTime() || this.planningTable.dateChooserPolicy === 'DATECHOOSER_POLICY_PRESERVE') {
				newDateUntil = moment(dateFrom)
					.add(daysBetween, 'days')
					.toDate();
				newDateUntil.setHours(23, 59, 59, 999);
			}
			newInterval = new Interval(dateFrom, newDateUntil);
			this.planningView.setInterval(newInterval);
			this.refresh();
		}
	}

	/**
	 * Changes the end date until which this component displays the planning table.
	 * It automatically saves the new until date to the preferences and reloads the planning table via the service.
	 * @param dateUntil
	 */
	setDateUntil(dateUntil: Date) {
		if (dateUntil && dateUntil.getTime() !== this.viewConfiguration.dateUntil.getTime()) {
			let newDateFrom = this.viewConfiguration.dateFrom;
			let daysBetween = this.getDaysBetweenCurrentInterval();
			let negativeInterval = (dateUntil.getTime() < this.viewConfiguration.dateFrom.getTime());
			if (negativeInterval) {
				// resolve the negative interval according to the date chooser policy
				if (this.planningTable.dateChooserPolicy === 'DATECHOOSER_POLICY_PRESERVE') {
					dateUntil = new Date(this.viewConfiguration.dateUntil.getTime());
				} else {
					newDateFrom = new Date(dateUntil.getTime());
					newDateFrom.setHours(0, 0, 0, 0);
					dateUntil = moment(dateUntil).add(daysBetween, 'days').toDate();
				}
			}
			dateUntil.setHours(23, 59, 59, 999);
			let newInterval;
			newInterval = new Interval(newDateFrom, dateUntil);
			this.planningView.setInterval(newInterval);
			this.refresh();
		}
	}

	/**
	 * Starts a periodic refresh timer, if the flag usePeriodicRefresh is set for the planning table.
	 * @private
	 */
	private setupRefreshTimerIfConfigured() {
		if (this.planningTable.usePeriodicRefresh) {
			this.startPeriodicRefreshTimer(this.planningTable.periodicRefreshInterval);
		}
	}

	/**
	 * Checks, if a refresh is allowed in the current state.
	 * A refresh is not allowed, when the user is in drag-and-drop mode or is editing an entry in the entity overlay.
	 * @private
	 */
	private isRefreshAllowed() {
		let isDragAndDropActive = !!this.planningView.elementSelection.transformation;
		let noDialogOpen = !this.dialogService.hasOpenDialog();
		return !isDragAndDropActive && noDialogOpen;
	}

	/**
	 * Starts a timer, which will refresh the planning table every N seconds, where N = periodicDelayInSeconds
	 * If, there is already a timer running, it will be canceled.
	 * @param periodicDelayInSeconds the delay, the refresh timer will have
	 * @private
	 */
	private startPeriodicRefreshTimer(periodicDelayInSeconds: number) {
		this.periodicRefreshTimer = setInterval(() => {
			if (this.isRefreshAllowed()) {
				this.refresh();
			}
		}, periodicDelayInSeconds * 1000);
	}

}

/**
 * The InputProcessor handles the input for the planning-table.component centrally.
 * It significantly reduces the amount of registered input listeners at the cost of slightly decreasing code readability.
 */
class InputProcessor {

	unlistenMouseDown: () => void;
	unlistenMouseUp: () => void;
	unlistenMouseMove: () => void;

	unlistenTouchStart: () => void;
	unlistenTouchMove: () => void;
	unlistenTouchCancel: () => void;
	unlistenTouchStop: () => void;

	unlistenContextMenu: () => void;

	private startMouseDown: Date;
	private startTouchDown: Date;
	private clientStartX: number;
	private clientStartY: number;
	private touchStartX: number;
	private touchStartY: number;

	constructor(
		private renderer2: Renderer2,
		private ngZone: NgZone,
		private planningTableComponent: PlanningTableComponent
	) {
		this.unlistenMouseDown = this.renderer2.listen('window', 'mousedown', (evt) => {
			if (evt.button === 0) {
				let event = this.checkStartResizingHandler(evt, evt.clientX, evt.clientY);
				if (!event) {
					this.checkStartElementDragAndDrop(evt, evt.clientX, evt.clientY);
				}
				this.startMouseDown = new Date();
				this.clientStartX = evt.clientX;
				this.clientStartY = evt.clientY;
			}
		});

		this.unlistenMouseUp = this.renderer2.listen('window', 'mouseup', (evt) => {
			if (evt.button === 0) {
				let mouseMovement = Math.hypot(evt.clientX - this.clientStartX, evt.clientY - this.clientStartY);
				if (new Date().getTime() - this.startMouseDown.getTime() < 250 && mouseMovement < 10) {
					// time between mouse down and up < 250ms -> it was just a click
					this.cancelSelectionTransformation();
					this.stopResizingHandlers();
					this.processClickEvent(evt);
					evt.preventDefault();
					evt.stopPropagation();

				} else {
					this.stopResizingHandlers();
				}
			}
		});

		this.unlistenMouseMove = this.renderer2.listen('window', 'mousemove', (evt) => {
			if (this.planningTableComponent.resourceHeader) {
				// update the resize process of resource header cells, if active
				this.planningTableComponent.resourceHeader.resizeHandler.updateResizing(evt.clientX, evt.clientY);
				this.planningTableComponent.resourceHeader.changeDetectorRef.detectChanges();
			}
			if (this.planningTableComponent.timeHeader) {
				// update the resize process of time header cells, if active
				this.planningTableComponent.timeHeader.resizeHandler.updateResizing(evt.clientX, evt.clientY);
			}
			// update the resizing process of the header container, if active
			this.planningTableComponent.headerResizer.updateResizing(evt.clientX, evt.clientY);
			if (this.planningTableComponent.planningView) {
				// update the drag and drop action of the selection, if active
				this.planningTableComponent.planningView.elementSelection.processDragEvent(evt.clientX, evt.clientY);
			}
			if (this.planningTableComponent.gridComponent && this.planningTableComponent.gridComponent.transformationRef) {
				this.planningTableComponent.gridComponent.transformationRef.update();
			}
		});
		this.unlistenTouchMove = this.renderer2.listen('window', 'touchmove', (evt) => {
			if (this.planningTableComponent.resourceHeader) {
				this.planningTableComponent.resourceHeader.resizeHandler.updateResizing(evt.touches[0].clientX, evt.touches[0].clientY);
				this.planningTableComponent.resourceHeader.changeDetectorRef.detectChanges();
			}
			if (this.planningTableComponent.timeHeader) {
				this.planningTableComponent.timeHeader.resizeHandler.updateResizing(evt.touches[0].clientX, evt.touches[0].clientY);
			}
			if (this.planningTableComponent.planningView) {
				// update the drag and drop action of the selection, if active
				this.planningTableComponent.planningView.elementSelection.processDragEvent(evt.touches[0].clientX, evt.touches[0].clientY);
			}
			if (this.planningTableComponent.gridComponent.transformationRef) {
				this.planningTableComponent.gridComponent.transformationRef.update();
			}
			// update the resizing process of the header container, if active
			this.planningTableComponent.headerResizer.updateResizing(evt.touches[0].clientX, evt.touches[0].clientY);
		});
		this.unlistenTouchStart = this.renderer2.listen('window', 'touchstart', (evt) => {
			let event = this.checkStartResizingHandler(evt, evt.touches[0].clientX, evt.touches[0].clientY);
			if (!event) {
				event = this.checkStartElementDragAndDrop(evt, evt.touches[0].clientX, evt.touches[0].clientY);
			}
			if (event) {
				evt.preventDefault();
				evt.stopPropagation();
			}
			this.startTouchDown = new Date();
			this.touchStartX = evt.touches[0].clientX;
			this.touchStartY = evt.touches[0].clientY;
		});
		this.unlistenTouchStop = this.renderer2.listen('window', 'touchend', (evt) => {
			let mouseMovement = Math.hypot(
				evt.changedTouches[0].clientX - this.touchStartX, evt.changedTouches[0].clientY - this.touchStartY
			);
			if (new Date().getTime() - this.startTouchDown.getTime() < 250 && mouseMovement < 10) {
				// time between mouse down and up < 250ms -> it was just a click
				this.cancelSelectionTransformation();
				this.stopResizingHandlers();
				this.processClickEvent(evt);
				// evt.preventDefault();
				// evt.stopPropagation();

			} else {
				this.stopResizingHandlers();
			}
		});
		this.unlistenTouchCancel = this.renderer2.listen('window', 'touchcancel', (evt) => {
			this.stopResizingHandlers();
		});
		this.unlistenContextMenu = this.renderer2.listen('window', 'contextmenu', (evt) => {
			// when right-clicking during drag-and-drop: prevent default popup and reset the transformation
			if (this.planningTableComponent.planningView
				&& this.planningTableComponent.planningView.elementSelection.transformation) {
				this.planningTableComponent.planningView.elementSelection.transformation = undefined;
				this.planningTableComponent.gridComponent.changeDetectorRef.detectChanges();
				evt.preventDefault();
				evt.stopPropagation();
				return false;
			}
			return true;
		});
	}

	checkStartResizingHandler(evt, clientX, clientY): boolean {
		if (evt.target.className.includes('resize-handle')) {
			if (evt.target.parentNode && evt.target.parentNode.className.includes('resource')) {
				if (this.planningTableComponent.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
					let resourceHeight = evt.target.parentNode.offsetHeight;
					this.planningTableComponent.resourceHeader.resizeHandler.startResizing(clientX, clientY, resourceHeight, 2);
				} else {
					let resourceWidth = evt.target.parentNode.offsetWidth;
					this.planningTableComponent.resourceHeader.resizeHandler.startResizing(clientX, clientY, resourceWidth, 1);
				}
				if (evt.cancelable) {
					evt.preventDefault();
				}
				return true;
			}
			if (evt.target.parentNode && evt.target.parentNode.className.includes('time')) {
				if (this.planningTableComponent.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
					this.planningTableComponent.timeHeader.resizeHandler.startResizing(
						clientX, clientY, this.planningTableComponent.viewConfiguration.baseTimeUnitCellSize, 1);
				} else {
					this.planningTableComponent.timeHeader.resizeHandler.startResizing(
						clientX, clientY, this.planningTableComponent.viewConfiguration.baseTimeUnitCellSize, 2);
				}
				if (evt.cancelable) {
					evt.preventDefault();
				}
				return true;
			}
			if (evt.target.parentNode && evt.target.parentNode.className.includes('horizontal-header-container')) {
				this.planningTableComponent.headerResizer.startResizing(clientX, clientY, evt.target.parentNode.offsetHeight, 2);
				if (evt.cancelable) {
					evt.preventDefault();
				}
				return true;
			}
			if (evt.target.parentNode && evt.target.parentNode.className.includes('vertical-header-container')
				|| (evt.target.parentNode && evt.target.parentNode.className.includes('legend-container'))) {
				this.planningTableComponent.headerResizer.startResizing(clientX, clientY, evt.target.parentNode.offsetWidth, 1);
				if (evt.cancelable) {
					evt.preventDefault();
				}
				return true;
			}
			if ((evt.target.parentNode
					&& evt.target.parentNode.className.includes('booking')
					&& evt.target.parentNode.className.includes('selected'))
				|| (evt.target.parentNode
					&& evt.target.parentNode.className.includes('milestone')
					&& evt.target.parentNode.className.includes('selected'))) {
				if (this.isEditingEnabled()) {
					let scaleStart = evt.target.className.includes('left') || evt.target.className.includes('top');
					if (scaleStart) {
						this.planningTableComponent.planningView.elementSelection.transformation
							= new TransformationScaleStart(evt.clientX, evt.clientY, this.planningTableComponent.planningView);
					} else {
						this.planningTableComponent.planningView.elementSelection.transformation
							= new TransformationScaleEnd(evt.clientX, evt.clientY, this.planningTableComponent.planningView);
					}
					this.planningTableComponent.gridComponent.changeDetectorRef.detectChanges();
				}
				if (evt.cancelable) {
					evt.preventDefault();
				}
				return true;
			}
		}
		return false;
	}

	processSelectCellEvent(evt) {
		if (this.planningTableComponent.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			let indexX = Math.floor((evt.target.offsetLeft) / this.planningTableComponent.viewConfiguration.baseTimeUnitCellSize);
			let indexY = Math.floor((evt.target.offsetTop) / this.planningTableComponent.viewConfiguration.resourceCellSize);
			this.planningTableComponent.gridComponent.setSelectedCell(indexX, indexY);
			this.planningTableComponent.toolbar.changeDetectorRef.detectChanges();
		} else {
			let indexX = Math.floor(evt.target.offsetLeft / this.planningTableComponent.viewConfiguration.resourceCellSize);
			let indexY = Math.floor(evt.target.offsetTop / this.planningTableComponent.viewConfiguration.baseTimeUnitCellSize);
			this.planningTableComponent.gridComponent.setSelectedCell(indexX, indexY);
			this.planningTableComponent.toolbar.changeDetectorRef.detectChanges();
		}
	}

	/**
	 * Opens the entity object overlay of the resource the user clicked on.
	 * @param evt
	 */
	processOpenResourceEvent(evt) {
		let resourceIndex;
		if (this.planningTableComponent.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			resourceIndex = Math.floor(evt.target.parentNode.offsetTop / this.planningTableComponent.viewConfiguration.resourceCellSize);
		} else {
			resourceIndex = Math.floor(evt.target.parentNode.offsetLeft / this.planningTableComponent.viewConfiguration.resourceCellSize);
		}
		if (this.planningTableComponent.planningTable.resources) {
			let resource = this.planningTableComponent.planningTable.resources[resourceIndex];
			this.planningTableComponent.resourceHeader.openEo(resource);
		}
	}

	checkStartElementDragAndDrop(evt, clientX, clientY) {
		// check if selection is non-empty
		if (this.planningTableComponent.planningView.elementSelection.selectedElements.length > 0) {
			// check if client-X&-Y are inside a selected plan element
			if (evt.target.className.includes('booking-selected')
				|| evt.target.className.includes('milestone-selected')
				|| (evt.target.className.includes('milestone-inside') && evt.target.parentNode.className.includes('milestone-selected'))) {
				if (this.isEditingEnabled()) {
					this.planningTableComponent.planningView.elementSelection.transformation
						= new TransformationTranslateSelection(clientX, clientY, this.planningTableComponent.planningView);
					this.planningTableComponent.gridComponent.changeDetectorRef.detectChanges();
				}
				if (evt.cancelable) {
					evt.preventDefault();
				}
				return true;
			}
		}
		return false;
	}

	toggleBookingSelection(bookingIndex, resourceIndex, clientX, clientY, ctrlKey) {
		if (this.planningTableComponent.planningTable.bookings
			&& this.planningTableComponent.planningTable.resources) {
			let booking = this.planningTableComponent.planningTable.bookings[bookingIndex];
			let resource = this.planningTableComponent.planningTable.resources[resourceIndex];
			clientX = clientX - $('#planningGrid').offset().left;
			clientY = clientY - $('#planningGrid').offset().top;
			let cellIndex;
			if (this.planningTableComponent.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
				cellIndex = Math.floor((this.planningTableComponent.viewConfiguration.horizontalScroll + clientX)
					/ this.planningTableComponent.viewConfiguration.baseTimeUnitCellSize);
			} else {
				cellIndex = Math.floor((this.planningTableComponent.viewConfiguration.verticalScroll + clientY)
					/ this.planningTableComponent.viewConfiguration.baseTimeUnitCellSize);
			}
			let creationDateFrom = this.planningTableComponent.viewConfiguration.baseUnit.getNextStep(
				this.planningTableComponent.viewConfiguration.baseUnit.getNextStep(
					this.planningTableComponent.viewConfiguration.dateFrom, 0), cellIndex);
			let creationDateUntil = this.planningTableComponent.viewConfiguration.baseUnit.getStepEnd(creationDateFrom);
			this.planningTableComponent.planningView.elementSelection.toggleEntitySelection(booking, ctrlKey);
			this.planningTableComponent.planningView.elementSelection.setCreationDateBoundaries(creationDateFrom, creationDateUntil);
			this.planningTableComponent.planningView.elementSelection.creationResource = resource;
			this.planningTableComponent.toolbar.changeDetectorRef.detectChanges();
			this.planningTableComponent.gridComponent.refreshGrid();
		}
	}

	toggleMilestoneSelection(milestoneIndex, resourceIndex, clientX, clientY, ctrlKey) {
		if (this.planningTableComponent.planningTable.milestones
			&& this.planningTableComponent.planningTable.resources) {
			let milestone = this.planningTableComponent.planningTable.milestones[milestoneIndex];
			let resource = this.planningTableComponent.planningTable.resources[resourceIndex];
			clientX = clientX - $('#planningGrid').offset().left;
			clientY = clientY - $('#planningGrid').offset().top;
			let cellIndex;
			if (this.planningTableComponent.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
				cellIndex = (this.planningTableComponent.viewConfiguration.horizontalScroll + clientX)
					/ this.planningTableComponent.viewConfiguration.baseTimeUnitCellSize;
			} else {
				cellIndex = (this.planningTableComponent.viewConfiguration.verticalScroll + clientY)
					/ this.planningTableComponent.viewConfiguration.baseTimeUnitCellSize;
			}
			let creationDateFrom = this.planningTableComponent.viewConfiguration.baseUnit.getNextStep(
				this.planningTableComponent.viewConfiguration.dateFrom, cellIndex);
			let creationDateUntil = this.planningTableComponent.viewConfiguration.baseUnit.getStepEnd(creationDateFrom);
			this.planningTableComponent.planningView.elementSelection.toggleEntitySelection(milestone, ctrlKey);
			this.planningTableComponent.planningView.elementSelection.setCreationDateBoundaries(creationDateFrom, creationDateUntil);
			this.planningTableComponent.planningView.elementSelection.creationResource = resource;
			this.planningTableComponent.toolbar.changeDetectorRef.detectChanges();
			this.planningTableComponent.gridComponent.refreshGrid();
		}
	}

	toggleRelationSelection(relationIndex, resourceIndex, clientX, clientY, ctrlKey) {
		if (this.planningTableComponent.planningTable.relations
			&& this.planningTableComponent.planningTable.resources) {
			let relation = this.planningTableComponent.planningTable.relations[relationIndex];
			let resource = this.planningTableComponent.planningTable.resources[resourceIndex];
			clientX = clientX - $('#planningGrid').offset().left;
			clientY = clientY - $('#planningGrid').offset().top;
			let cellIndex;
			if (this.planningTableComponent.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
				cellIndex = (this.planningTableComponent.viewConfiguration.horizontalScroll + clientX)
					/ this.planningTableComponent.viewConfiguration.baseTimeUnitCellSize;
			} else {
				cellIndex = (this.planningTableComponent.viewConfiguration.verticalScroll + clientY)
					/ this.planningTableComponent.viewConfiguration.baseTimeUnitCellSize;
			}
			let creationDateFrom = this.planningTableComponent.viewConfiguration.baseUnit.getNextStep(
				this.planningTableComponent.viewConfiguration.dateFrom, cellIndex);
			let creationDateUntil = this.planningTableComponent.viewConfiguration.baseUnit.getStepEnd(creationDateFrom);
			this.planningTableComponent.planningView.elementSelection.toggleEntitySelection(relation, ctrlKey);
			this.planningTableComponent.planningView.elementSelection.setCreationDateBoundaries(creationDateFrom, creationDateUntil);
			this.planningTableComponent.planningView.elementSelection.creationResource = resource;
			this.planningTableComponent.toolbar.changeDetectorRef.detectChanges();
			this.planningTableComponent.gridComponent.refreshGrid();
		}
	}

	stopResizingHandlers() {
		if (this.planningTableComponent.planningView && this.planningTableComponent.planningView.elementSelection.transformation) {
			this.planningTableComponent.planningView.elementSelection.processReleaseEvent(this.planningTableComponent.planningTableService);
			this.planningTableComponent.gridComponent.changeDetectorRef.detectChanges();
		}
		let needToSave = false;
		if (this.planningTableComponent.resourceHeader) {
			needToSave = needToSave || this.planningTableComponent.resourceHeader.resizeHandler.stopResizing();
		}
		if (this.planningTableComponent.timeHeader) {
			needToSave = needToSave || this.planningTableComponent.timeHeader.resizeHandler.stopResizing();
		}
		needToSave = needToSave || this.planningTableComponent.headerResizer.stopResizing();
		if (needToSave) {
			this.planningTableComponent.savePreferences();
		}
	}

	/**
	 * Clears up all the input listeners of the planning-table.component.
	 */
	destroy() {
		this.unlistenMouseDown();
		this.unlistenMouseUp();
		this.unlistenMouseMove();

		this.unlistenTouchStart();
		this.unlistenTouchMove();
		this.unlistenTouchCancel();
		this.unlistenTouchStop();

		this.unlistenContextMenu();
	}

	private processClickEvent(evt) {
		// all events require the left mouse button to be clicked
		if (evt.button !== 0 && !(evt instanceof TouchEvent)) {
			return;
		}
		if (evt.target.className.includes('cell-base')) {
			// cell selection event
			this.processSelectCellEvent(evt);
		} else if (evt.target.className.includes('resource-content')) {
			if (this.isEditingEnabled()) {
				// resource overlay event
				this.processOpenResourceEvent(evt);
			}
		} else if (this.hasAttribute(evt, 'data-booking-index')) {
			// booking selection event
			let bookingIndex = evt.target.getAttribute('data-booking-index');
			let resourceIndex = evt.target.getAttribute('data-resource-index');
			this.toggleBookingSelection(bookingIndex, resourceIndex, evt.clientX, evt.clientY, evt.ctrlKey);
		} else if (this.parentHasAttribute(evt, 'data-booking-index')) {
			let bookingIndex = evt.target.parentNode.getAttribute('data-booking-index');
			let resourceIndex = evt.target.parentNode.getAttribute('data-resource-index');
			this.toggleBookingSelection(bookingIndex, resourceIndex, evt.clientX, evt.clientY, evt.ctrlKey);
		} else if (this.hasAttribute(evt, 'data-milestone-index')) {
			// milestone selection event
			let milestoneIndex = evt.target.getAttribute('data-milestone-index');
			let resourceIndex = evt.target.getAttribute('data-resource-index');
			this.toggleMilestoneSelection(milestoneIndex, resourceIndex, evt.clientX, evt.clientY, evt.ctrlKey);
		} else if (this.hasAttribute(evt, 'data-relation-index')) {
			let relationIndex = evt.target.getAttribute('data-relation-index');
			let resourceIndex;
			if (this.planningTableComponent.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
				resourceIndex = Math.floor(evt.target.offsetTop / this.planningTableComponent.viewConfiguration.resourceCellSize);
			} else {
				resourceIndex = Math.floor(evt.target.offsetLeft / this.planningTableComponent.viewConfiguration.resourceCellSize);
			}
			this.toggleRelationSelection(relationIndex, resourceIndex, evt.clientX, evt.clientY, evt.ctrlKey);
		}
	}

	private isEditingEnabled() {
		return this.planningTableComponent.enabled;
	}

	private hasAttribute(evt, attName: string) {
		return (evt.target.getAttribute(attName) !== undefined && evt.target.getAttribute(attName) != null && evt.target.getAttribute(attName) !== '');
	}

	private parentHasAttribute(evt, attName: string) {
		return (evt.target.parentNode.getAttribute(attName) !== undefined
			&& evt.target.parentNode.getAttribute(attName) != null
			&& evt.target.parentNode.getAttribute(attName) !== '');
	}

	private cancelSelectionTransformation() {
		if (this.planningTableComponent.planningView.elementSelection.transformation) {
			this.planningTableComponent.planningView.elementSelection.transformation = undefined;
			this.planningTableComponent.gridComponent.changeDetectorRef.detectChanges();
		}
	}

}
