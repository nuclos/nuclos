import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { ToolbarModule } from 'primeng/toolbar';
import { SharedModule } from '../../shared/shared.module';
import { UiComponentsModule } from '../ui-components/ui-components.module';
import { PlanningTableComponent } from './planning-table.component';
import { PlanningTableRoutes } from './planning-table.routes';
import { PlanningTableService } from './planning-table.service';
import { PlanningTableToolbarComponent } from './planning-table-toolbar/planning-table-toolbar.component';
import {
	PlanningTableResourceHeaderComponent
} from './planning-table-resource-header/planning-table-resource-header.component';
import { PlanningTableTimeHeaderComponent } from './planning-table-time-header/planning-table-time-header.component';
import { PlanningGridComponent } from './planning-grid/planning-grid.component';
import { PlanningCellComponent } from './planning-grid/planning-cell/planning-cell.component';
import { BookingComponent } from './planning-grid/booking/booking.component';
import { TimeUnitDisplayComponent } from './planning-table-time-header/time-unit-display/time-unit-display.component';
import { MilestoneComponent } from './planning-grid/milestone/milestone.component';
import { PlanningModalComponent } from './planning-modal/planning-modal.component';
import {
	ResourceHeaderCellComponent
} from './planning-table-resource-header/resource-header-cell/resource-header-cell.component';
import {
	TimeUnitCellComponent
} from './planning-table-time-header/time-unit-display/time-unit-cell/time-unit-cell.component';
import { TransformedElementsComponent } from './planning-grid/transformed-elements/transformed-elements.component';
import { PlanningTableExportComponent } from './planning-table-export/planning-table-export.component';
import { RelationComponent } from './planning-grid/relation/relation.component';

@NgModule({
	declarations: [
		PlanningTableComponent,
		PlanningTableToolbarComponent,
		PlanningTableResourceHeaderComponent,
		PlanningTableTimeHeaderComponent,
		PlanningGridComponent,
		PlanningCellComponent,
		BookingComponent,
		TimeUnitDisplayComponent,
		MilestoneComponent,
		PlanningModalComponent,
		ResourceHeaderCellComponent,
		TimeUnitCellComponent,
		TransformedElementsComponent,
		PlanningTableExportComponent,
		RelationComponent,
	],
	providers: [PlanningTableService],
	exports: [
		PlanningTableComponent
	],
	imports: [
		CommonModule,
		SharedModule,
		PlanningTableRoutes,
		UiComponentsModule,
		ToolbarModule,
		ButtonModule,
		CheckboxModule
	]
})
export class PlanningTableModule {

}
