import { RouterModule, Routes } from '@angular/router';
import { PlanningTableComponent } from './planning-table.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'planningtable/',
		component: PlanningTableComponent
	},
	{
		path: 'planningtable/:planTableId',
		component: PlanningTableComponent
	}
];

export const PlanningTableRoutes = RouterModule.forChild(ROUTE_CONFIG);
