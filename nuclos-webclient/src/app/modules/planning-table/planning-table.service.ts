import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { Logger } from '@modules/log/shared/logger';
import { WhereCondition } from '@modules/search/shared/search.model';
import { SearchfilterService } from '@modules/search/shared/searchfilter.service';
import { forkJoin, Observable, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { MetaService } from '../entity-object-data/shared/meta.service';
import {
	PlanningTablePreferenceContent,
	Preference,
} from '../preferences/preferences.model';
import { PreferencesService } from '../preferences/preferences.service';
import { PlanningTableAlignment } from './shared/planning-table-alignment';
import { PlanningView } from './shared/planning-view';
import { PlanningViewConfiguration } from './shared/planning-view-configuration';
import { Interval } from './time/interval';
import { TimeUnits } from './time/time-unit';

/**
 * Service responsible for loading PlanningTables via rest request from the server.
 * It provides the PlanningTables with the preferences of the user in the PlanningView Wrapper class.
 * @see PlanningView
 */
@Injectable()
export class PlanningTableService {

	constructor(
		private nuclosHttp: HttpClient,
		private nuclosConfig: NuclosConfigService,
		private preferencesService: PreferencesService,
		private searchfilterService: SearchfilterService,
		private metaService: MetaService,
		private dialogService: NuclosDialogService,
		private $log: Logger,
		private i18n: NuclosI18nService
	) {
	}

	/**
	 * Loads a PlanningView and provides it to the Subject<PlanningView> that is returned.
	 * General steps:
	 * 		1. load preferences - if no existing preferences, create default prefs
	 * 		2. build where condition via search filter service
	 * 		3. use part of the preferences and where condition to request the correct planning table via rest service.
	 * @param planningTableId
	 * @param dateFrom
	 * @param dateUntil
	 * @param layoutId
	 * @param resourceForeignKey
	 * @return Subject<PlanningView>
	 */
	public loadPlanningView(
		planningTableId: string,
		dateFrom?: Date,
		dateUntil?: Date,
		layoutId?: string,
		resourceForeignKey?: string
	) {
		const subject = new Subject<PlanningView>();
		let planningView = new PlanningView(planningTableId);
		this.loadOrCreatePreferences(planningTableId).subscribe((preferences) => {
			this.getWhereCondition(preferences.content).subscribe(condition => {
				planningView.preferences = preferences;
				let viewConfiguration = preferences.content.planningViewConfiguration;
				if (dateFrom) {
					viewConfiguration.dateFrom = dateFrom;
				}
				if (dateUntil) {
					viewConfiguration.dateUntil = dateUntil;
				}
				if (preferences.content.planningViewConfiguration.horizontalHeaderHeight === undefined) {
					preferences.content.planningViewConfiguration.horizontalHeaderHeight = 128;
				}
				if (preferences.content.planningViewConfiguration.verticalHeaderWidth === undefined) {
					preferences.content.planningViewConfiguration.verticalHeaderWidth = 256;
				}
				planningView.updateInterval();
				this.nuclosHttp.get<PlanningTable>(this.nuclosConfig.getRestHost() + '/planningTables/' + planningTableId
					+ '?from=' + moment(viewConfiguration.dateFrom).format('YYYY-MM-DD')
					+ '&until=' + moment(viewConfiguration.dateUntil).format('YYYY-MM-DD')
					+ (layoutId ? '&layoutEoId=' + layoutId : '')
					+ (resourceForeignKey ? '&resourceForeignKey=' + resourceForeignKey : '')
					+ (viewConfiguration.showOwnEventsOnly ? '&showOwnEventsOnly' : ''
					+ (condition) ? '&where=' + encodeURI(condition.clause) : '')
				)
					.subscribe((planningTable) => {
						planningView.setPlanningTable(planningTable);
						subject.next(planningView);
						subject.complete();
					}, (error) => {
						const title = this.i18n.getI18n('webclient.error.title');
						const message = this.i18n.getI18n('webclient.planningTable.open.error.message') + planningTableId;
						this.$log.error(message);
						this.dialogService.alert({title: title, message: message})
							.pipe(
								take(1)
							).subscribe();
					});
			});
		});

		return subject;
	}

	public getPlanElementDef(planningTableId, metaId) {
		return this.nuclosHttp.get<PlanElementDef>(this.nuclosConfig.getRestHost() + '/planningTables/'
			+ planningTableId + '/plan-elements/' + metaId);
	}

	public getAllPlanElementDefs(planningTableId) {
		return this.nuclosHttp.get<PlanElementDefList>(this.nuclosConfig.getRestHost() + '/planningTables/'
			+ planningTableId + '/plan-elements');
	}

	public updatePlanElement(planningTableId, metaId, entityId, resource, dateFrom, dateUntil?, timeFrom?, timeUntil?) {
		let params = new HttpParams();
		params = params.set('resource', resource);
		params = params.set('fromDate', moment(dateFrom).format('YYYY-MM-DD'));
		if (dateUntil) {
			params = params.set('untilDate', moment(dateUntil).format('YYYY-MM-DD'));
		}
		if (timeFrom) {
			params = params.set('fromTime', timeFrom);
		}
		if (timeUntil) {
			params = params.set('untilTime', timeUntil);
		}
		return this.nuclosHttp.put(this.nuclosConfig.getRestHost() + '/planningTables/'
			+ planningTableId + '/events/' + metaId + '/' + entityId,
			{}, {params: params}
		).subscribe();
	}

	/**
	 * Saves the preferences of the given planningView object.
	 * @param planningView
	 * @return Observable - Observable, that can be subscribed to.
	 */
	public savePreferences(planningView: PlanningView) {
		return this.preferencesService.savePreferenceItem(planningView.preferences).pipe(take(1));
	}

	/**
	 * If the current user has existing preferences for the specified planning table, it will load these preferences,
	 * otherwise, it will create a new Preference Object with the default settings for the planning-view-configuration.
	 * @param planningTableId
	 * @private
	 */
	private loadOrCreatePreferences(planningTableId: string): Observable<Preference<PlanningTablePreferenceContent>> {
		return new Observable<Preference<PlanningTablePreferenceContent>>(observer => {
			let preferences = this.preferencesService.getPreferences({
				type: ['planning-table']
			}, false) as Observable<Preference<PlanningTablePreferenceContent>[]>;

			preferences.pipe(take(1)).subscribe(value => {
				let preference = value.find(x => (x.content.planningTableId === planningTableId));
				if (preference) {
					this.prepareViewConfiguration(preference.content.planningViewConfiguration);
					observer.next(preference);
				} else {
					let planningTablePreference = new Preference<PlanningTablePreferenceContent>('planning-table', '');
					let content = planningTablePreference.content;
					content.planningTableId = planningTableId;
					content.planningViewConfiguration = new PlanningViewConfiguration(TimeUnits.UNIT_DAY);
					planningTablePreference.content = content;

					observer.next(planningTablePreference);
				}
				observer.complete();
			});
		});
	}

	/**
	 * Builds and returns a SQL WHERE condition of the selected filter.
	 * If no filter is selected, it will return an empty WHERE Condition.
	 * @param prefContent
	 * @private
	 */
	private getWhereCondition(prefContent: PlanningTablePreferenceContent): Observable<WhereCondition> {
		return new Observable<WhereCondition>(observer => {
			let emptyCondition: WhereCondition = {clause: '', aliases: {}};
			if (prefContent.planningViewConfiguration.searchFilter) {
				this.searchfilterService.getSearchfilter(prefContent.planningViewConfiguration.searchFilter.id)
					.subscribe((pref) => {
						if (pref) {
							let entityClassIds = SearchfilterService.entityClassIdsFromSearchfilter(pref);
							let getMapMetas = this.metaService.getEntityMetas(entityClassIds);
							let getMainMeta = this.metaService.getBoMeta(pref.boMetaId);
							forkJoin([getMainMeta, getMapMetas]).subscribe(results => {
								let condition: WhereCondition = this.searchfilterService.buildWhereCondition(pref, results[0], results[1], false);
								observer.next(condition);
								observer.complete();
							});

						} else {
							observer.next(emptyCondition);
							observer.complete();
						}
					});
			} else {
				observer.next(emptyCondition);
				observer.complete();
			}
		});
	}

	/**
	 * This method fixes the viewConfiguration after deserializing.
	 * Because some types are lost while deserializing they are being manually restored at this point.
	 * @param viewConfiguration
	 * @private
	 */
	private prepareViewConfiguration(viewConfiguration: PlanningViewConfiguration) {
		// Functions do not get (de)serialized, so we need to find the correct functions for the deserialized timeunit
		let fixedBaseUnit = TimeUnits.ALL.find(x => (x.abbreviation === viewConfiguration.baseUnit.abbreviation));
		if (fixedBaseUnit) {
			viewConfiguration.baseUnit.getNextStep = fixedBaseUnit.getNextStep;
			viewConfiguration.baseUnit.getStepEnd = fixedBaseUnit.getStepEnd;
			viewConfiguration.baseUnit.getNumSteps = fixedBaseUnit.getNumSteps;
		} else {
			// Time Unit Custom can not be restored without initializing a time-unit-custom object, so we switch the TimeUnit to day.
			viewConfiguration.baseUnit = TimeUnits.UNIT_DAY;
		}
		// Deserializing only restores the numeric value of the PlanningTableAlignmentEnum
		// We need to restore the enum value, otherwise comparisons like
		// viewConfiguration.tableAlignment === PlanningTableAlignment.RESOURCE_TIME will fail
		if (viewConfiguration.tableAlignment === 0) {
			viewConfiguration.tableAlignment = PlanningTableAlignment.RESOURCE_TIME;
		} else {
			viewConfiguration.tableAlignment = PlanningTableAlignment.TIME_RESOURCE;
		}
		// date objects get deserialized as strings, so we need to initialize them as dates again.
		viewConfiguration.dateFrom = new Date(viewConfiguration.dateFrom);
		viewConfiguration.dateFrom.setHours(0, 0, 0, 0);
		viewConfiguration.dateUntil = new Date(viewConfiguration.dateUntil);
		viewConfiguration.dateUntil.setHours(23, 59, 59, 999);
		viewConfiguration.intervall = new Interval(viewConfiguration.dateFrom, viewConfiguration.dateUntil);
	}

}

export class PlanningTable {

	componentLabel?: string;
	componentMenupath?: string;
	resourceEntity?: string;
	resources?: Array<Resource>;
	bookings?: Array<Booking>;
	relations?: Array<Relation>;
	milestones?: Array<Milestone>;
	searchFilters?: Array<Filter>;
	businessHours?: Array<BusinessHour>;
	showBackgroundEvents?: boolean;
	backgroundEvents?: Array<BackgroundEvent>;
	dateFrom: string;
	dateUntil: string;
	legendLabel?: string;
	legendTooltip?: string;
	usePeriodicRefresh: boolean;
	periodicRefreshInterval: number;
	dateChooserPolicy: string;

}

/* A resource that can be booked at different locations in time. */
export class Resource {

	entityMeta: string;
	resourceId: string;
	label?: string;
	tooltip?: string;

}

/* An event books a resource for a specific time interval. (refers to RichClient booking) */
export class Booking {

	entityMeta: string;
	id: string;
	booker?: string;
	resource?: string;
	fromTime?: string;
	fromDate: string;
	untilTime?: string;
	untilDate: string;
	label?: string;
	toolTip?: string;
	backgroundColor?;
	order?: number;
	ownBooking?: boolean;

}

export class Milestone {
	entityMeta: string;
	id: string;
	resource?: string;
	booker?: string;
	fromDate: string;
	backgroundColor?: string;
	icon?: string;
}

export class Relation {
	entityMeta: string;
	id: string;
	color: string;
	from: string;
	to: string;
	startDecoration: string;
	endDecoration: string;
	presentationMode: string;
}

export class BackgroundEvent {

	eventEntity?: string;
	eventName?: string;
	eventDate?: string;
	eventTip?: string;

}

export class Filter {

	id?: String;
	type?: string;
	icon?: string;

}

export class BusinessHour {
	from: string;
	until: string;
}

export class BookingDef {

	entity: string;
	resourceRefField: string;
	dateFromField: string;
	dateUntilField: string;
	timeFromField?: string;
	timeUntilField?: string;
	color: string;
	withTime: boolean;
	businessHours?: Array<BusinessHour>;

}

export class MilestoneDef {

	entity: string;
	resourceRefField: string;
	dateFromField: string;
	icon: string;

}

export class RelationDef {

	entity: string;
	fromField: string;
	toField: string;
	fromLineCap: string;
	toLineCap: string;
	fromIcon: string;
	toIcon: string;
	color: string;

}

/** the definition of a plan element. **/
export class PlanElementDef {

	elementType: string;
	milestoneDef?: MilestoneDef;
	bookingDef?: BookingDef;
	relationDef?: RelationDef;

}

/** collection of plan element definitions **/
export class PlanElementDefList {

	planElementDefs: Array<PlanElementDef>;

}
