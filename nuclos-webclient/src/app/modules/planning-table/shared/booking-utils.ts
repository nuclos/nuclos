import { Booking } from '../planning-table.service';

export namespace BookingUtils {

	/**
	 * Checks if a booking is valid.
	 * true - if dateFrom <= dateUntil
	 * false - if dateFrom > dateUntil
	 * @param booking
	 */
	export function isValid(booking: Booking) {
		let dateFrom: Date = new Date(booking.fromDate);
		let dateUntil: Date = new Date(booking.untilDate);
		if (dateUntil.getFullYear() < dateFrom.getFullYear()) {
			return false;
		} else if (dateUntil.getFullYear() === dateFrom.getFullYear()) {
			if (dateUntil.getMonth() < dateFrom.getMonth()) {
				return false;
			} else if (dateUntil.getMonth() === dateFrom.getMonth()) {
				if (dateUntil.getDate() < dateFrom.getDate()) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Returns the start of a booking as a date object.
	 * Invalid bookings should start from the earlier date (which is untilDate because they are the wrong way around).
	 * Valid bookings start from dateFrom.
	 * If a fromTime is defined, it will be set for a valid resulting date, otherwise, the booking starts
	 * at 00h:00m:00s:000ms.
	 * @param booking
	 */
	export function getStart(booking: Booking) {
		if (BookingUtils.isValid(booking)) {
			let dateFrom: Date = new Date(booking.fromDate);
			if (booking.fromTime) {
				let split = booking.fromTime.split(':', 2);
				dateFrom.setHours(+split[0], +split[1]);
			} else {
				dateFrom.setHours(0, 0, 0, 0);
			}
			return dateFrom;
		} else {
			let dateFrom: Date = new Date(booking.untilDate);
			dateFrom.setHours(0, 0, 0, 0);
			return dateFrom;
		}
	}

	/**
	 * Returns the end of a booking as a date object.
	 * Invalid bookings should span 1 day of the earliest of the 2 dates.
	 * Valid bookings end at their defined untilDate.
	 * If a untilTime is defined, it will be set for a valid resulting date, otherwise, the booking ends at 23h:59m:59s:999ms.
	 * @param booking
	 */
	export function getEnd(booking: Booking) {
		if (BookingUtils.isValid(booking)) {
			let dateUntil: Date = new Date(booking.untilDate);
			if (booking.untilTime) {
				let split2 = booking.untilTime.split(':', 2);
				dateUntil.setHours(+split2[0], +split2[1]);
			} else {
				dateUntil.setHours(23, 59, 59, 999);
			}
			return dateUntil;
		} else {
			let dateFrom: Date = new Date(booking.untilDate);
			dateFrom.setHours(23, 59, 59, 999);
			return dateFrom;
		}
	}

	/**
	 * Checks whether the intervals of 2 bookings overlap.
	 * @param booking1
	 * @param booking2
	 */
	export function overlaps(booking1: Booking, booking2: Booking) {
		if (getStart(booking1).getTime() >= getEnd(booking2).getTime()
			|| getEnd(booking1).getTime() <= getStart(booking2).getTime()) {
			return false;
		}
		return true;
	}

}
