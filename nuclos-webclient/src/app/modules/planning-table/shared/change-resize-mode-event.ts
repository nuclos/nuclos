/**
 * ChangeResizeModeEvent is fired whenever the user starts or ends drag & drop on a resizable component.
 * The variables clientX and clientY are optional. They are of interest, when resizing is started, but not when it is finished.
 * This event is created as a result of a Mouse-/TouchEvent.
 * It is mainly used by the time-unit-display.component.ts
 * @see time-unit-display.component.ts
 */
export class ChangeResizeModeEvent {

	constructor(public resizeMode, public clientX?, public clientY?) {

	}

}
