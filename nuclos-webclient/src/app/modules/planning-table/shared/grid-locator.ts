import { PlanningViewConfiguration } from './planning-view-configuration';

/**
 * Purpose of the GridLocator is to find the pixel offset for a input date inside the grid specified by a PlanningViewConfiguration.
 * The result is a TimeGridLocation object, which describes the offset relative to the nearest cell or neighbour.
 * 1. If the TimeGridLocation is a TimeGridLocationInCell, the offset is specified by the cell the date lies in
 * 2. If the TimeGridLocation is a TimeGridLocationBetweenCells
 */
export class GridLocator {

	/**
	 * Constructs a GridLocator for the given PlanningViewConfiguration.
	 * The GridLocator will use the baseUnit of this viewConfiguration to calculate TimeGridLocations for input dates.
	 * @param viewConfiguration
	 */
	constructor(private viewConfiguration: PlanningViewConfiguration) {

	}

	public computeGridLocation(time: Date) {
		let approximatedIndex;
		// getNumSteps only operates with positive intervals, so we have to account for that
		if (this.viewConfiguration.dateFrom <= time) {
			approximatedIndex = this.viewConfiguration.baseUnit.getNumSteps(this.viewConfiguration.dateFrom, time);
		} else {
			approximatedIndex = -this.viewConfiguration.baseUnit.getNumSteps(time, this.viewConfiguration.dateFrom);
		}
		let t1 = Math.min(this.viewConfiguration.baseUnit.getNextStep(this.viewConfiguration.dateFrom, approximatedIndex - 1),
			this.viewConfiguration.dateUntil.getTime());
		let t2 = this.viewConfiguration.baseUnit.getStepEnd(new Date(t1));
		let t3 = this.viewConfiguration.baseUnit.getNextStep(this.viewConfiguration.dateFrom, approximatedIndex);
		let t4 = this.viewConfiguration.baseUnit.getStepEnd(new Date(t3));
		if (time.getTime() < t1) {
			return new TimeGridLocationBetweenCells(time, this.clipAllocationIndex(approximatedIndex - 2),
				this.clipAllocationIndex(approximatedIndex - 1));
		} else if (time.getTime() >= t1 && time.getTime() <= t2) {
			let lerp = (time.getTime() - t1) / (t2 - t1);
			return new TimeGridLocationInCell(time, this.clipAllocationIndex(approximatedIndex - 1),
				this.clipAllocationIndex(approximatedIndex - 1 + lerp));
		} else if (time.getTime() < t3) {
			return new TimeGridLocationBetweenCells(time, this.clipAllocationIndex(approximatedIndex),
				this.clipAllocationIndex(approximatedIndex));
		} else {
			/* it has to be: time.getTime() >= t3 && time.getTime() <= t4 otherwise, the approximatedIndex was wrong. */
			let lerp = (time.getTime() - t3) / (t4 - t3);
			return new TimeGridLocationInCell(time, this.clipAllocationIndex(approximatedIndex),
				this.clipAllocationIndex(approximatedIndex + lerp));
		}
	}

	/**
	 * The grid consists of n = numBaseUnitAllocations cells.
	 * We do not want header cells or bookings to go outside the indices from [0, n-1]
	 * This is the reason for clipping them.
	 * @param index
	 * @private
	 */
	private clipAllocationIndex(index: number) {
		return Math.max(0, Math.min(Math.max(this.viewConfiguration.numBaseUnitAllocations, 1), index));
	}

}

/**
 * Defines the grid location for a point in time.
 * It does so by referring to the location by the index location.
 * *
 * Example: Assume the date 16.03 09:00 has the index location 3.4.
 * This means the date we are referring to lies in cell 3 and takes 40% of the baseTimeUnitCellSize.
 * Thus, the pixel location can then easily be calculated the following way:
 * viewConfiguration.baseUnitCellSize * this.baseTimeLocation
 */
abstract class TimeGridLocation {

	baseTimeLocation;

	/**
	 * Constructs a TimeGridLocation object.
	 * @param time - the time this grid location is referring to.
	 */
	constructor(public time: Date) {

	}

	getBaseTimeLocation(): number {
		return this.baseTimeLocation;
	}

}

/**
 * Defines the grid location for a point in time.
 * It does so, by referring to the location by the index of the cell, which contains the time location.
 * The pixel location can then be calculated the following way:
 * viewConfiguration.baseUnitCellSize * baseTimeLocation
 */
class TimeGridLocationInCell extends TimeGridLocation {

	cellIndex: number;

	constructor(time: Date, cellIndex: number, baseTimeAllocation: number) {
		super(time);
		this.cellIndex = cellIndex;
		this.baseTimeLocation = baseTimeAllocation;
	}

}

/**
 * Defines the grid location for a point in time.
 * The location of the time is not contained by any cell, because it lies between 2 cells.
 * So the 2 indices surrounding the time point are stored.
 */
class TimeGridLocationBetweenCells extends TimeGridLocation {

	cell1: number;
	cell2: number;

	constructor(time: Date, cell1: number, cell2: number) {
		super(time);
		if (cell1 < cell2) {
			this.cell1 = cell1;
			this.cell2 = cell2;
		} else {
			this.cell1 = cell2;
			this.cell2 = cell1;
		}
		this.baseTimeLocation = this.cell2;
	}

}

