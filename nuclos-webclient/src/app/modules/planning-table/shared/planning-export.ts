import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { BookingViewElement } from '@modules/planning-table/planning-grid/booking/booking-view-element';
import { MilestoneViewElement } from '@modules/planning-table/planning-grid/milestone/milestone-view-element';
import { RelationViewElement } from '@modules/planning-table/planning-grid/relation/relation-view-element';
import { ViewElement } from '@modules/planning-table/planning-grid/shared/view-element';
import {
	TimeUnitDisplay
} from '@modules/planning-table/planning-table-time-header/planning-table-time-header.component';
import {
	TimeAllocationContext,
	TimeUnitDisplayComponent
} from '@modules/planning-table/planning-table-time-header/time-unit-display/time-unit-display.component';
import { Resource } from '@modules/planning-table/planning-table.service';
import { PlanningTableAlignment } from '@modules/planning-table/shared/planning-table-alignment';
import { GridModel, PlanningGridComponent, ViewElementFactory } from '../planning-grid/planning-grid.component';
import { PlanningTableComponent } from '../planning-table.component';

export class PlanningExport {

	cellExporter: CellExporter;

	constructor(private i18n: NuclosI18nService) {
		this.cellExporter = new CellExporter();
	}

	exportSvg(component: PlanningTableComponent) {
		let leftHeaderWidth = component.verticalHeader.nativeElement.getBoundingClientRect().width;
		let topHeaderHeight = component.horizontalHeader.nativeElement.getBoundingClientRect().height;
		let width = Math.ceil(leftHeaderWidth + component.gridComponent.getWidthOverall()) + 2;
		let height = Math.ceil(topHeaderHeight + component.gridComponent.getHeightOverall()) + 2;

		let result = '<svg xmlns="http://www.w3.org/2000/svg" width="' + width + '" height="' + height + '">';
		result += '<foreignObject x="0" y="0" width="100%" height="100%">';
		result += this.buildExportLayout(component);
		result += '</foreignObject>';
		result += '</svg>';
		let resNode = document.createRange().createContextualFragment(result);
		return Promise.resolve(resNode)
			.then(function (node) {
				return 'data:image/svg+xml;charset=utf-8,' + new XMLSerializer().serializeToString(node);
			})
			.then(function (node) {
				return node.replace(/#/g, '%23').replace(/\n/g, '%0A');
			});
	}

	buildExportLayout(component: PlanningTableComponent) {
		let fontFamily = window.getComputedStyle(component.horizontalHeader.nativeElement, null).fontFamily;
		fontFamily = fontFamily.replace(/"/g, '');
		let leftHeaderWidth = component.verticalHeader.nativeElement.getBoundingClientRect().width;
		let topHeaderHeight = component.horizontalHeader.nativeElement.getBoundingClientRect().height;
		let contentTop = this.exportLayoutTop(component, topHeaderHeight, leftHeaderWidth);
		let contentBottom = this.exportLayoutBottom(component, topHeaderHeight, leftHeaderWidth);

		let result = '<div style="font-family: ' + fontFamily + '; width: ' + (leftHeaderWidth + component.gridComponent.getWidthOverall()) + 'px; height: '
			+ (component.gridComponent.getHeightOverall() + topHeaderHeight) + 'px; border: 1px solid #999999; padding-right: 1px; padding-bottom: 1px; background-color: white;">';
		result += contentTop;
		result += contentBottom;
		result += '</div>';
		return result;
	}

	exportLayoutTop(
		component: PlanningTableComponent,
		topHeight: number,
		leftWidth: number
	) {
		let legendContent = this.exportLegend(component);
		let headerContent;
		if (component.viewConfiguration.tableAlignment === 0) {
			headerContent = this.exportResourceHeader(component, topHeight, leftWidth);
		} else {
			headerContent = this.exportTimeHeader(component, topHeight, leftWidth);
		}
		let gridWidth = component.gridComponent.getWidthOverall();
		let overallWidth = leftWidth + gridWidth;
		let headerTop =
			`<div style="z-index: 10; height: ${topHeight}px; width: ${overallWidth}px; border-bottom: 1px solid rgb(153, 153, 153);">
			<div style="z-index: 10; float: left; height: ${topHeight}px; width: ${leftWidth}px; border-right: 1px solid rgb(153, 153, 153); font-size: 13px;">${legendContent}</div>
			<div style="z-index: 10; position: absolute; float: right; height: ${topHeight}px; left: ${leftWidth}px; width: ${gridWidth}px;">${headerContent}</div>
			</div>`;
		return headerTop;
	}

	exportLayoutBottom(component: PlanningTableComponent, topHeight: number, leftWidth: number) {
		let gridContent = this.exportGrid(component);
		let gridHeight = component.gridComponent.getHeightOverall();
		let headerContent;
		if (component.viewConfiguration.tableAlignment === 0) {
			headerContent = this.exportTimeHeader(component, topHeight, leftWidth);
		} else {
			headerContent = this.exportResourceHeader(component, topHeight, leftWidth);
		}
		let bottomDiv =
			`<div style="">
			<div style="z-index: 10; background-color: white; position: absolute; left: 0px; width: ${leftWidth}px; height: ${gridHeight}px; border-right: 1px solid rgb(153, 153, 153);">${headerContent}</div>
			<div style="position: absolute; float: right; left: ${leftWidth}px; overflow: visible;">${gridContent}</div>
			</div>`;
		return bottomDiv;
	}

	exportLegend(component: PlanningTableComponent) {
		let legendExport = '';
		if (component.formattedLegendLabel) {
			legendExport += component.formattedLegendLabel;
		}
		return legendExport;
	}

	exportResourceHeader(component: PlanningTableComponent, topHeight: number, leftWidth: number) {
		let content = '';
		component.planningTable.resources?.forEach((resource, index) => {
			let x;
			let y;
			let width;
			let height;
			if (component.viewConfiguration.tableAlignment === 0) {
				x = index * component.viewConfiguration.resourceCellSize;
				width = component.viewConfiguration.resourceCellSize;
				y = 0;
				height = topHeight;
			} else {
				x = 0;
				width = leftWidth;
				y = index * component.viewConfiguration.resourceCellSize;
				height = component.viewConfiguration.resourceCellSize;
			}
			content += `<div style="border-right: 1px solid #999999; border-bottom: 1px solid #999999; ${this.cellExporter.resourceStyle} left: ${x}px; top: ${y}px; width: ${width}px; height: ${height}px; font-size: 13px;">${resource.label}</div>`;
		});
		return content;
	}

	exportTimeHeader(component: PlanningTableComponent, topHeight: number, leftWidth: number) {
		let timeDisplays = component.timeHeader.timeUnitHeader.timeDisplays;
		let index = 0;
		let content = '';
		let containerWidth;
		let containerHeight;
		let areaEnd;
		if (component.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			containerWidth = component.gridComponent.getWidthOverall();
			containerHeight = topHeight;
			areaEnd = containerWidth;
		} else {
			containerWidth = leftWidth;
			containerHeight = component.gridComponent.getHeightOverall();
			areaEnd = containerHeight;
		}
		while (index < timeDisplays.length) {
			let timeUnitDisplayComp: TimeUnitDisplayComponent = new TimeUnitDisplayComponent(this.i18n);
			timeUnitDisplayComp.viewConfiguration = component.viewConfiguration;
			timeUnitDisplayComp.timeUnit = timeDisplays[index].timeUnit;
			timeUnitDisplayComp.dateFormattingFunction = timeDisplays[index].formattingFunction;
			timeUnitDisplayComp.ngOnInit();
			timeUnitDisplayComp.addNewCellsToViewport(0, areaEnd);
			let timeCellContent = '';
			timeUnitDisplayComp.timeElements.forEach((timeAllocationContext) => {
				timeCellContent += this.cellExporter.exportTimeUnitCell(component, timeAllocationContext);
			});
			let x, y, width, height;
			if (component.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
				x = 0;
				width = containerWidth;
				height = containerHeight / timeDisplays.length;
				y = index * height;
			} else {
				y = 0;
				height = containerHeight;
				width = containerWidth / timeDisplays.length;
				x = index * width;
			}
			content += `<div style="float: left; position: relative; width: ${width}px; height: ${height}px;">${timeCellContent}</div>`;
			index++;
		}
		content = `<div style="width: ${containerWidth}px; height: ${containerHeight}px;">` + content + '</div>';
		return content;
	}

	exportTimeUnitDisplay(component: PlanningTableComponent, timeUnitDisplay: TimeUnitDisplay) {
		return '';
	}

	exportGrid(component: PlanningTableComponent) {
		let result = '<div style="position: relative; overflow: visible;">';
		let gridModel = component.gridComponent.gridModel;
		// first export all cells in the background
		let columnIndex = 0;
		while (columnIndex < gridModel.columnCount) {
			let rowIndex = 0;
			while (rowIndex < gridModel.rowCount) {
				result += this.cellExporter.exportCell(columnIndex, rowIndex, gridModel, component.gridComponent);
				rowIndex++;
			}
			columnIndex++;
		}
		// then export all view-elements
		component.gridComponent.viewEntityFactories.forEach((factory: ViewElementFactory) => {
			let viewElements: ViewElement[] = factory.createViewElements();
			viewElements.forEach((viewElement) => {
				if (viewElement instanceof BookingViewElement) {
					viewElement.layout(component.gridComponent.gridModel);
				} else if (viewElement instanceof MilestoneViewElement) {
					viewElement.layout(component.gridComponent.gridModel);
				} else if (viewElement instanceof RelationViewElement) {
					viewElement.layout(component.gridComponent.gridModel);
				}
				result += viewElement.getSvgContent();
			});
		});
		result += '</div>';
		return result;
	}

}

class CellExporter {

	cellStyle = ' border-right-style: dashed; border-bottom-style: solid; border-right-width: 1px;  border-bottom-width: 1px; border-bottom-color: rgb(170, 170, 170);  border-right-color: rgb(170, 170, 170); position: absolute; z-index: 0;';
	resourceStyle = 'box-shadow: rgba(0, 0, 0, 0.15) 0px -50px 36px -28px inset; overflow: hidden; position: absolute; z-index: 10;';
	timeUnitStyle = 'font-size: 13px; justify-content: center; align-items: center; box-shadow: rgba(0, 0, 0, 0.15) 0px -50px 36px -28px inset; overflow: hidden; position: absolute; z-index: 10;';
	timeUnitSpanStyle = 'white-space: nowrap; font-size: 12px; display: inline-block; overflow: visible;';

	exportCell(colIndex: number, rowIndex: number, gridModel: GridModel, gridComponent: PlanningGridComponent): string {
		let x = colIndex * gridComponent.columnWidth;
		let y = rowIndex * gridComponent.rowHeight;
		let width = gridComponent.columnWidth;
		let height = gridComponent.rowHeight;
		let style = `left: ${x}px; top: ${y}px; width: ${width}px; height: ${height}px;`;
		style += this.cellStyle;
		return `<div style="${style}"></div>`;
	}

	exportBooking(bookingElement: BookingViewElement) {

	}

	exportMilestone(milestoneElement: MilestoneViewElement) {

	}

	exportResourceCell(resource: Resource) {

	}

	exportTimeUnitCell(component: PlanningTableComponent, timeCell: TimeAllocationContext): string {
		let x, y, width, height;
		if (component.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			y = '0px';
			height = '100%';
			x = timeCell.locationStart * component.viewConfiguration.baseTimeUnitCellSize + 'px';
			width = (timeCell.locationEnd - timeCell.locationStart) * component.viewConfiguration.baseTimeUnitCellSize + 'px';
		} else {
			x = '0px';
			width = '100%';
			y = timeCell.locationStart * component.viewConfiguration.baseTimeUnitCellSize + 'px';
			height = (timeCell.locationEnd - timeCell.locationStart) * component.viewConfiguration.baseTimeUnitCellSize + 'px';
		}
		return `<div style="border-right: 1px solid #999999; border-bottom: 1px solid #999999; left: ${x}; top: ${y};
				width: ${width}; height: ${height}; ${this.timeUnitStyle}">
					<div style="position: absolute; width: 100%; height: 100%; display: flex; justify-content: center; align-items: center;">
						<span style="${this.timeUnitSpanStyle}">${timeCell.allocationLabel}</span>
					</div>
				</div>`;
	}

}
