/** defines content of a PlanningTables axes **/
export enum PlanningTableAlignment {
	/** x = resource & y = time **/
	RESOURCE_TIME,
	/** x = time & y = resource **/
	TIME_RESOURCE
}
