import { Today } from '../time/date-utils';
import { Interval } from '../time/interval';
import { TimeUnit } from '../time/time-unit';
import { PlanningTableAlignment } from './planning-table-alignment';

export class PlanningViewConfiguration {

	public static readonly MINIMUM_BASE_TIME_UNIT_SIZE = 24;
	public static readonly MINIMUM_RESOURCE_CELL_SIZE = 20;

	static fitHeaderIntoViewport(viewConfiguration: PlanningViewConfiguration, scrollbarSize: number) {
		let delta = viewConfiguration.numBaseUnitAllocations;

		let preferredBaseUnitSize;
		if (viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			preferredBaseUnitSize = (viewConfiguration.viewportWidth - scrollbarSize - PlanningViewConfiguration.FIT_ERROR_TOLERANCE) / delta;
		} else {
			preferredBaseUnitSize = (viewConfiguration.viewportHeight - scrollbarSize - PlanningViewConfiguration.FIT_ERROR_TOLERANCE) / delta;
		}
		viewConfiguration.baseTimeUnitCellSize = Math.max(preferredBaseUnitSize, PlanningViewConfiguration.MINIMUM_BASE_TIME_UNIT_SIZE);
	}

	static getFirstVisibleResourceIndex(viewConfiguration: PlanningViewConfiguration) {
		if (viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			return Math.floor(viewConfiguration.verticalScroll / viewConfiguration.resourceCellSize);
		} else {
			return Math.floor(viewConfiguration.horizontalScroll / viewConfiguration.resourceCellSize);
		}
	}

	static getLastVisibleResourceIndex(viewConfiguration: PlanningViewConfiguration) {
		if (viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			return Math.ceil((viewConfiguration.viewportHeight + viewConfiguration.verticalScroll)
				/ viewConfiguration.resourceCellSize);
		} else {
			return Math.ceil((viewConfiguration.viewportWidth + viewConfiguration.horizontalScroll)
				/ viewConfiguration.resourceCellSize);
		}
	}

	static getFirstVisibleTimeStamp(viewConfiguration: PlanningViewConfiguration) {
		let firstVisibleTimeCell;
		if (viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			firstVisibleTimeCell = Math.floor(viewConfiguration.horizontalScroll / viewConfiguration.baseTimeUnitCellSize);
		} else {
			firstVisibleTimeCell = Math.floor(viewConfiguration.verticalScroll / viewConfiguration.baseTimeUnitCellSize);
		}
		return viewConfiguration.baseUnit.getNextStep(viewConfiguration.dateFrom, firstVisibleTimeCell);
	}

	static getLastVisibleTimeStamp(viewConfiguration: PlanningViewConfiguration) {
		let lastVisibleTimeCell;
		if (viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			lastVisibleTimeCell = Math.ceil((viewConfiguration.horizontalScroll + viewConfiguration.viewportWidth)
				/ viewConfiguration.baseTimeUnitCellSize);
		} else {
			lastVisibleTimeCell = Math.ceil((viewConfiguration.verticalScroll + viewConfiguration.viewportHeight)
				/ viewConfiguration.baseTimeUnitCellSize);
		}
		return viewConfiguration.baseUnit.getNextStep(viewConfiguration.dateFrom, lastVisibleTimeCell);
	}

	private static readonly FIT_ERROR_TOLERANCE = 2;

	/** how many pixels, should be used for a grid cells resource dimension **/
	resourceCellSize = 64;

	/** how many pixels, should be used for a grid cells time dimension **/
	baseTimeUnitCellSize = 50;

	/** how many times the timeunit fits into the view interval **/
	numBaseUnitAllocations: number;

	/**
	 * defines which axis is responsible for displaying resources and which one for time.
	 */
	tableAlignment: PlanningTableAlignment = PlanningTableAlignment.TIME_RESOURCE;

	/**
	 * the search filter, that is currently being applied to the view.
	 */
	searchFilter;

	showOwnEventsOnly = false;

	dateFrom: Date = Today.start();

	dateUntil: Date = Today.end();

	intervall: Interval;

	horizontalScroll = 0;

	verticalScroll = 0;

	viewportWidth = 1920;

	viewportHeight = 1080;

	/** width of the vertical header in pixels. **/
	verticalHeaderWidth = 128;

	/** height of the horizontal header in pixels. **/
	horizontalHeaderHeight = 64;

	constructor(public baseUnit: TimeUnit, interval?: Interval) {
		if (interval) {
			this.dateFrom = interval.fromTime;
			this.dateUntil = interval.untilTime;
		} else {
			interval = new Interval(this.dateFrom, this.dateUntil);
		}
		this.intervall = interval;
		this.numBaseUnitAllocations = baseUnit.getNumSteps(interval.fromTime, interval.untilTime);
	}

}
