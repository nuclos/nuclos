import { EntityMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { CellSelection } from '@modules/planning-table/shared/selection/cell-selection';
import { ElementSelection } from '@modules/planning-table/shared/selection/element-selection';
import { LocalTime } from '@modules/planning-table/time/local-time';
import { LocalTimeInterval } from '@modules/planning-table/time/local-time-interval';
import { TimeUnitCustom } from '@modules/planning-table/time/time-unit-custom';
import { PlanningTablePreferenceContent, Preference } from '../../preferences/preferences.model';
import { PlanElementDef, BusinessHour, PlanningTable } from '../planning-table.service';
import { Interval } from '../time/interval';
import { TimeUnit } from '../time/time-unit';

/**
 * PlanningView is a wrapper class.
 * It combines a PlanningTable (data) + preferences (configuration, how to view the data).
 */
export class PlanningView {

	planningTableId: string;
	planningTable: PlanningTable;
	preferences: Preference<PlanningTablePreferenceContent>;
	customTimeUnit: TimeUnitCustom | undefined;

	cellSelection: CellSelection | undefined;
	elementSelection: ElementSelection;
	metaMap: Map<string, EntityMeta> = new Map<string, EntityMeta>();
	bookingDefMetas: Array<PlanElementDefMeta> = [];
	milestoneDefMetas: Array<PlanElementDefMeta> = [];
	relationDefMetas: Array<PlanElementDefMeta> = [];

	constructor(planningTableId) {
		this.planningTableId = planningTableId;
		this.elementSelection = new ElementSelection(() => this.cellSelection = undefined);
	}

	/**
	 * Checks, if the planning table is a 'MultiUserTable'
	 * true - if there are bookings from more than 1 user
	 * false - otherwise
	 */
	isMultiUserTable() {
		let bookers = this.planningTable.bookings?.map(x => x.booker);
		if (bookers && bookers.length > 1) {
			return true;
		}
		return false;
	}

	/**
	 * Sets the PlanningTable object for this PlanningView.
	 * If the planningTable contains a businessHours definition, it will create a custom time unit.
	 * @param planningTable
	 */
	setPlanningTable(planningTable: PlanningTable) {
		this.planningTable = planningTable;
		this.preferences.name = planningTable.componentLabel;
		if (this.planningTable.businessHours) {
			let allocations: LocalTimeInterval[] = [];
			this.planningTable.businessHours.forEach((value: BusinessHour) => {
				let localFromTime = LocalTime.of(value.from);
				let localUntilTime = LocalTime.of(value.until);
				allocations.push(new LocalTimeInterval(localFromTime, localUntilTime));
			});
			this.customTimeUnit = new TimeUnitCustom(allocations);
		}
	}

	restoreSelection(preView: PlanningView) {
		preView.elementSelection.selectedElements.forEach(prevSelEl => {
			let booking = this.planningTable.bookings?.find(value =>
				(value.entityMeta === prevSelEl.entityMeta && value.id === prevSelEl.id)
			);
			let milestone = this.planningTable.milestones?.find(value =>
				(value.entityMeta === prevSelEl.entityMeta && value.id === prevSelEl.id)
			);
			if (booking && this.planningTable.bookings) {
				this.elementSelection.toggleEntitySelection(booking, true);
			}
			if (milestone && this.planningTable.milestones) {
				this.elementSelection.toggleEntitySelection(milestone, true);
			}
		});
	}

	updateInterval() {
		this.getViewConfig().intervall = new Interval(this.getViewConfig().dateFrom, this.getViewConfig().dateUntil);
		this.getViewConfig().numBaseUnitAllocations =
			this.getViewConfig().baseUnit.getNumSteps(this.getViewConfig().intervall.fromTime, this.getViewConfig().intervall.untilTime);
	}

	setInterval(interval: Interval) {
		this.getViewConfig().intervall = interval;
		this.getViewConfig().dateFrom = interval.fromTime;
		this.getViewConfig().dateUntil = interval.untilTime;
		this.getViewConfig().numBaseUnitAllocations = this.getViewConfig().baseUnit.getNumSteps(interval.fromTime, interval.untilTime);
	}

	setBaseUnit(baseUnit: TimeUnit) {
		this.getViewConfig().baseUnit = baseUnit;
		this.getViewConfig().numBaseUnitAllocations =
			baseUnit.getNumSteps(this.getViewConfig().intervall.fromTime, this.getViewConfig().intervall.untilTime);
	}

	/**
	 * Helper function to make code that references the viewConfiguration more compact.
	 * @private
	 */
	private getViewConfig() {
		return this.preferences.content.planningViewConfiguration;
	}

}

export class PlanElementDefMeta {

	constructor(public entityName: string, public planElementDef: PlanElementDef, public meta: EntityMeta) {

	}
}
