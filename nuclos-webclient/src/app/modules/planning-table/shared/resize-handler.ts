import { ChangeResizeModeEvent } from './change-resize-mode-event';

/**
 * ResizeHandler is a helper class, which encapsulates commonly needed variables for resizing a visual component via drag & drop.
 */
export class ResizeHandler {

	/** a factor how much the mouse-/touch-movement translates into the resize amount **/
	sensitivity = 0.5;
	/** variable keeping track in which direction the user currently is resizing. **/
	scaleMode = 0;
	/** the x location where the user originally started the resize process. **/
	dragStartX: number;
	/** the y location where the user originally started the resize process. **/
	dragStartY: number;
	/** the size the visual component originally had, when starting to resize. **/
	dragStartSize: number;

	/**
	 * Initializes a new ResizeHandler.
	 * The functions that get passed to this constructor should take a number as a parameter.
	 * @param onHorizontalScale - a function, that will be called, when resizing horizontally.
	 * @param onVerticalScale - a function, that will be called, when resizing vertically.
	 */
	constructor(public onHorizontalScale?, public onVerticalScale?) {

	}

	/**
	 * Starts the resizing process.
	 * @param startX
	 * @param startY
	 * @param startSize
	 * @param scaleMode
	 */
	startResizing(startX, startY, startSize, scaleMode) {
		this.dragStartX = startX;
		this.dragStartY = startY;
		this.dragStartSize = startSize;
		this.scaleMode = scaleMode;
	}

	/**
	 * Starts the resizing process.
	 * Alternative to startResizing. Needs fewer parameters.
	 * This is because scaleMode, startX & startY are passed within the wrapper class ChangeResizeModeEvent.
	 * @param event
	 * @param startSize
	 */
	startResizingByEvent(event: ChangeResizeModeEvent, startSize) {
		this.dragStartX = event.clientX;
		this.dragStartY = event.clientY;
		this.dragStartSize = startSize;
		this.scaleMode = event.resizeMode;
	}

	/**
	 * Updates the resize state by the current clientX & clientY location.
	 * Depending on the resize mode the onHorizontalScale and/or onVerticalScale functions will be invoked with the newly calculated size(s).
	 * @param clientX
	 * @param clientY
	 */
	updateResizing(clientX, clientY) {
		if (this.scaleMode === ResizeMode.NONE) {
			return;
		}
		if (this.scaleMode === ResizeMode.HORIZONTAL) {
			/* this resizes the vertical header along the x-axis */
			let sizeX
				= this.dragStartSize + (clientX - this.dragStartX) * this.sensitivity;
			if (this.onHorizontalScale) {
				this.onHorizontalScale(sizeX);
			}
		} else if (this.scaleMode === ResizeMode.VERTICAL) {
			/* this resizes the horizontal header along the y-axis */
			let sizeY
				= this.dragStartSize + (clientY - this.dragStartY) * this.sensitivity;
			if (this.onVerticalScale) {
				this.onVerticalScale(sizeY);
			}
		} else if (this.scaleMode === ResizeMode.BOTH) {
			/* this resizes the header along both axis */
			let sizeX
				= this.dragStartSize + (clientX - this.dragStartX) * this.sensitivity;
			let sizeY
				= this.dragStartSize + (clientY - this.dragStartY) * this.sensitivity;
			if (this.onHorizontalScale) {
				this.onHorizontalScale(sizeX);
			}
			if (this.onVerticalScale) {
				this.onVerticalScale(sizeY);
			}
		}
	}

	/**
	 * Stops the resizing process.
	 * @return true - if, resize process was running and is now stopped. false, if resize process was stopped before.
	 */
	stopResizing(): boolean {
		if (this.scaleMode !== ResizeMode.NONE) {
			this.scaleMode = ResizeMode.NONE;
			return true;
		}
		return false;
	}

}

/**
 * Specification of the ResizeModes.
 * The ResizeMode BOTH is currently not in use.
 */
export enum ResizeMode {
	NONE, HORIZONTAL, VERTICAL, BOTH
}
