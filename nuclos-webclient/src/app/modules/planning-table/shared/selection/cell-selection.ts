import { Resource } from '@modules/planning-table/planning-table.service';

export class CellSelection {

	constructor(public timeIndex: number, public resourceIndex: number, public timeFrom, public timeUntil, public resource: Resource) {

	}

}
