import { EventEmitter } from '@angular/core';
import { PlanningTableService } from '@modules/planning-table/planning-table.service';
import { Transformation } from '../transform/transformation';

export class ElementSelection {

	creationFromTime: Date | undefined;
	creationUntilTime: Date | undefined;
	creationResource;
	selectedElements: any[] = [];
	transformation: Transformation | undefined;
	selectionChange: EventEmitter<any> = new EventEmitter<any>();
	reloadPlanningTable: Function;

	constructor(private deselectCells: Function) {
	}

	setCreationDateBoundaries(fromTime: Date, untilTime: Date) {
		this.creationFromTime = fromTime;
		this.creationUntilTime = untilTime;
	}

	isSelected(element: any): boolean {
		return (this.selectedElements.indexOf(element) !== -1);
	}

	processDragEvent(clientX, clientY) {
		if (this.transformation) {
			this.transformation.update(clientX, clientY);
		}
	}

	processReleaseEvent(planningTableService: PlanningTableService) {
		if (this.transformation) {
			if (this.transformation.hasChanged) {
				this.transformation.finish(planningTableService);
				if (this.reloadPlanningTable) {
					this.reloadPlanningTable();
				}
			}
			this.transformation = undefined;
		}
	}

	toggleEntitySelection(toggleEntity, keepSelection) {
		/** set selected cell to undefined **/
		this.deselectCells();
		let index = this.selectedElements.indexOf(toggleEntity);
		if (!keepSelection) {
			this.selectedElements = this.selectedElements.filter(entity => (entity === toggleEntity));
		}
		if (index === -1) {
			this.selectedElements = [...this.selectedElements, toggleEntity];
		} else if (keepSelection) {
			this.selectedElements = this.selectedElements.filter(x => (x !== toggleEntity));
		}
		this.selectionChange.emit();
	}

	clearSelection() {
		this.selectedElements = [];
	}

}
