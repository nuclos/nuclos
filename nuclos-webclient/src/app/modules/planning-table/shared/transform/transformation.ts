import { Booking, Milestone, PlanningTableService } from '@modules/planning-table/planning-table.service';
import { BookingUtils } from '@modules/planning-table/shared/booking-utils';
import { GridLocator } from '@modules/planning-table/shared/grid-locator';
import { PlanningViewConfiguration } from '@modules/planning-table/shared/planning-view-configuration';
import { TimeUnitCustom } from '@modules/planning-table/time/time-unit-custom';
import { PlanningTableAlignment } from '../planning-table-alignment';
import { PlanningView } from '../planning-view';

/**
 * Base class for a selection transformation.
 * A transformation is initiated by the user 'dragging' a selection of plan-elements.
 * It is then updated as long as the mouse is pressed or the touch is held to the current pointer location.
 * @see update()
 * A transformation can be applied with the finish() Method.
 * @see finish()
 * Currently, there are 3 child classes:
 * @see TransformationScaleStart
 * @see TransformationScaleEnd
 * @see TransformationTranslateSelection
 */
export abstract class Transformation {

	transformMap: Map<any, PlanElementTransformation> = new Map<any, PlanElementTransformation>();
	hasChanged = false;

	/**
	 * @param startX - pointer x location, at the beginning of the transformation
	 * @param startY - pointer y location, at the beginning of the transformation
	 */
	constructor(startX: number, startY: number) {

	}

	/**
	 * Calculates the transformation by calculating the difference between the pointer start location and the current pointer location.
	 * @param pointerX - current pointer x
	 * @param pointerY - current pointer y
	 */
	update(pointerX: number, pointerY: number) {

	}

	/**
	 * Attempts to update all EOs via the REST-Service.
	 * To get the changes, the planning table has to be loaded via the PlanningTableService.
	 * @param planningTableService
	 */
	finish(planningTableService: PlanningTableService) {
		this.transformMap.forEach((transformation: PlanElementTransformation) => {
			transformation.applyTransform(planningTableService);
		});
	}

}

/**
 * TransformationTranslateSelection handles moving a selection of plan-elements.
 */
export class TransformationTranslateSelection extends Transformation {

	private viewConfiguration;

	constructor(private startX: number, private startY: number, private planningView: PlanningView) {
		super(startX, startY);
		this.viewConfiguration = planningView.preferences.content.planningViewConfiguration;
		this.planningView.elementSelection.selectedElements.forEach(element => {
			if (this.planningView.planningTable.bookings?.indexOf(element) !== -1) {
				this.transformMap.set(element, new BookingTransformation(element, planningView));
			} else if (this.planningView.planningTable.milestones?.indexOf(element) !== -1) {
				this.transformMap.set(element, new MilestoneTransformation(element, planningView));
			}
		});
	}

	update(pointerX: number, pointerY: number) {
		/* calculate by how many indices the cells get shifted */
		let resourceTranslation;
		let timeTranslation;
		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			timeTranslation = (pointerX - this.startX) / this.viewConfiguration.baseTimeUnitCellSize;
			resourceTranslation = (pointerY - this.startY) / this.viewConfiguration.resourceCellSize;
		} else {
			timeTranslation = (pointerY - this.startY) / this.viewConfiguration.baseTimeUnitCellSize;
			resourceTranslation = (pointerX - this.startX) / this.viewConfiguration.resourceCellSize;
		}
		resourceTranslation = Math.round(resourceTranslation);
		timeTranslation = Math.round(timeTranslation);
		this.transformMap.forEach(pet => {
			pet.setTranslationTo(resourceTranslation, timeTranslation);
		});
		this.hasChanged = (resourceTranslation !== 0 || timeTranslation !== 0);
	}

}

/**
 * TransformationScaleStart handles scaling a selection of plan-elements.
 */
export class TransformationScaleStart extends Transformation {

	private viewConfiguration;

	constructor(private startX: number, private startY: number, private planningView: PlanningView) {
		super(startX, startY);
		this.viewConfiguration = planningView.preferences.content.planningViewConfiguration;
		this.planningView.elementSelection.selectedElements.forEach(element => {
			if (this.planningView.planningTable.bookings?.indexOf(element) !== -1) {
				this.transformMap.set(element, new BookingTransformation(element, planningView));
			} else if (this.planningView.planningTable.milestones?.indexOf(element) !== -1) {
				this.transformMap.set(element, new MilestoneTransformation(element, planningView));
			}
		});
	}

	update(pointerX: number, pointerY: number) {
		/* calculate by how many indices the cells get shifted */
		let timeTranslation;
		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			timeTranslation = (pointerX - this.startX) / this.viewConfiguration.baseTimeUnitCellSize;
		} else {
			timeTranslation = (pointerY - this.startY) / this.viewConfiguration.baseTimeUnitCellSize;
		}
		/* round the value to get a 'snapping' effect */
		timeTranslation = Math.round(timeTranslation);
		this.transformMap.forEach(pet => {
			pet.scaleStartBy(timeTranslation);
		});
		/* only set to changed, if the beginning of the selection has really changed */
		this.hasChanged = (timeTranslation !== 0);
	}

}

/**
 * TransformationScaleEnd handles scaling a selection of plan-elements.
 */
export class TransformationScaleEnd extends Transformation {

	private viewConfiguration;

	constructor(private startX: number, private startY: number, private planningView: PlanningView) {
		super(startX, startY);
		this.viewConfiguration = planningView.preferences.content.planningViewConfiguration;
		this.planningView.elementSelection.selectedElements.forEach(element => {
			if (this.planningView.planningTable.bookings?.indexOf(element) !== -1) {
				this.transformMap.set(element, new BookingTransformation(element, planningView));
			} else if (this.planningView.planningTable.milestones?.indexOf(element) !== -1) {
				this.transformMap.set(element, new MilestoneTransformation(element, planningView));
			}
		});
	}

	update(pointerX: number, pointerY: number) {
		/* calculate by how many indices the cells get shifted */
		let timeTranslation;
		if (this.viewConfiguration.tableAlignment === PlanningTableAlignment.TIME_RESOURCE) {
			timeTranslation = (pointerX - this.startX) / this.viewConfiguration.baseTimeUnitCellSize;
		} else {
			timeTranslation = (pointerY - this.startY) / this.viewConfiguration.baseTimeUnitCellSize;
		}
		/* round the value to get a 'snapping' effect */
		timeTranslation = Math.round(timeTranslation);
		/* scale the end of all individual elements by the calculated timeTranslation */
		this.transformMap.forEach(pet => {
			pet.scaleEndBy(timeTranslation);
		});
		/* only set to changed, if the end location has really changed */
		this.hasChanged = (timeTranslation !== 0);
	}

}

/**
 * PlanElementTransformation is an interface to provide common transformation functions for all types of plan-elements.
 * See the following implementing classes:
 * @see BookingTransformation
 * @see MilestoneTransformation
 * Because the visual positioning of a relation is dependent on its 2 bookings, it needs no RelationTransformation class by itself.
 */
interface PlanElementTransformation {

	/**
	 * Translates the specific PlanElement by the specified offsets.
	 * The offset parameters should be integer numbers, because a plan-element is always moved by whole cell steps.
	 * @param resourceOffset
	 * @param timeOffset
	 */
	setTranslationTo(resourceOffset: number, timeOffset: number);

	/**
	 * Scales a plan-element by moving its start.
	 * The end of the plan-element should remain at the same location as it is the origin of this operation.
	 * @param startOffset
	 */
	scaleStartBy(startOffset: number);

	/**
	 * Scales a plan-element by moving its end.
	 * The start of the plan-element should remain at the same location as it is the origin of this operation.
	 * @param endOffset
	 */
	scaleEndBy(endOffset: number);

	/**
	 * Attempts to apply the transformation via the REST-service.
	 **/
	applyTransform(planningTableService: PlanningTableService);

}

/**
 * Provides methods for transforming a Milestone.
 * @see PlanElementTransformation
 */
export class MilestoneTransformation implements PlanElementTransformation {

	/** the resource, this milestone should move to. **/
	newResource: string;

	/** the new date location, this milestone should move to. **/
	newDate: Date;

	gridLocator: GridLocator;

	constructor(public milestone: Milestone, public planningView: PlanningView) {
		this.gridLocator = new GridLocator(planningView.preferences.content.planningViewConfiguration);
		this.setTranslationTo(0, 0);
	}

	/**
	 * Moves the milestone of this transformation by the specified offsets.
	 * @param resourceOffset - how many resource-steps the milestone should move, relative to the current resource index
	 * @param timeOffset - how many time-steps the milestone should move, relative to the current cell time index
	 */
	setTranslationTo(resourceOffset: number, timeOffset: number) {
		this.calculateTimeTranslation(timeOffset);
		this.calculateResourceTranslation(resourceOffset);
	}

	/**
	 * A milestone is only a point in time. It remains at the same location when scaling.
	 * @param startOffset
	 */
	scaleStartBy(startOffset: number) {

	}

	/**
	 * A milestone is only a point in time. It remains at the same location when scaling.
	 * @param endOffset
	 */
	scaleEndBy(endOffset: number) {

	}

	/**
	 * Applies the transformation by sending the new time and resource values for the milestone to the rest service.
	 */
	applyTransform(planningTableService: PlanningTableService) {
		planningTableService.updatePlanElement(this.planningView.planningTableId, this.milestone.entityMeta,
			this.milestone.id, this.newResource, this.newDate);
	}

	/**
	 * Calculates the new date, where the milestone will be after shifting from the current cell by the input timeOffset.
	 * @param timeOffset
	 * @private
	 */
	private calculateTimeTranslation(timeOffset: number) {
		let viewConfiguration: PlanningViewConfiguration = this.planningView.preferences.content.planningViewConfiguration;
		let firstCellDate: Date = new Date(viewConfiguration.baseUnit.getNextStep(viewConfiguration.dateFrom, 0));
		let originalDate = new Date(this.milestone.fromDate);
		originalDate.setHours(12, 0, 0, 0);
		let originalTimeCellIndex = this.gridLocator.computeGridLocation(originalDate);
		let targetDate: Date
			= new Date(viewConfiguration.baseUnit.getNextStep(firstCellDate, Math.floor(originalTimeCellIndex.baseTimeLocation) + timeOffset));
		targetDate.setHours(12, 0, 0, 0);
		if (timeOffset !== 0) {
			this.newDate = targetDate;
		} else {
			this.newDate = originalDate;
		}
	}

	/**
	 * Calculates the new resource, where the milestone will be after shifting from the current resource by the input resourceOffset.
	 * @param resourceOffset
	 * @private
	 */
	private calculateResourceTranslation(resourceOffset: number) {
		let resources = this.planningView.planningTable.resources;
		if (resources) {
			let originalResourceIndex = resources.findIndex(r => (r.resourceId === this.milestone.resource));
			if (originalResourceIndex + resourceOffset >= 0 && originalResourceIndex + resourceOffset < resources.length) {
				// valid new resource location -> apply
				this.newResource = resources[originalResourceIndex + resourceOffset].resourceId;
			}
		}
	}

}

/**
 * Provides method for transforming a Booking.
 * @see PlanElementTransformation
 */
export class BookingTransformation implements PlanElementTransformation {

	/** the resource, this milestone should move to. **/
	newResource: string;

	/** the new date location, this milestone should move to. **/
	newFromDate: Date;

	newUntilDate: Date;

	newFromTime: string;

	newUntilTime: string;

	gridLocator: GridLocator;

	constructor(public booking: Booking, private planningView: PlanningView) {
		this.gridLocator = new GridLocator(planningView.preferences.content.planningViewConfiguration);
		// initialize the date fields by setting the offset to 0
		this.setTranslationTo(0, 0);
	}

	/**
	 * Translates the booking of this transformation by moving it by the specified offsets.
	 * @param resourceOffset - should be a whole number
	 * @param timeOffset - should be a whole number
	 */
	setTranslationTo(resourceOffset: number, timeOffset: number) {
		this.calculateTimeTranslation(timeOffset);
		this.calculateResourceTranslation(resourceOffset);
	}

	/**
	 * Scales the booking of this transformation by moving the beginning by the specified offset.
	 * If the beginning of the booking is in the middle of a cell a startOffset of -/+1 will snap to the beginning/end of this cell.
	 * @param startOffset - should be a whole number
	 */
	scaleStartBy(startOffset: number) {
		if (startOffset !== 0) {
			let viewConfiguration: PlanningViewConfiguration = this.planningView.preferences.content.planningViewConfiguration;
			let firstCellDate: Date = new Date(viewConfiguration.baseUnit.getNextStep(viewConfiguration.dateFrom, 0));
			let originalDate = new Date(this.booking.fromDate);
			if (this.booking.fromTime) {
				let split = this.booking.fromTime.split(':', 2);
				originalDate.setHours(+split[0], +split[1]);
			} else {
				originalDate.setHours(0, 0, 0, 0);
			}
			let originalTimeCellIndex = this.gridLocator.computeGridLocation(originalDate);
			let currentCellStartDate: Date = new Date(viewConfiguration.baseUnit.getNextStep(originalDate, 0));
			let targetDate: Date
				= new Date(viewConfiguration.baseUnit.getNextStep(firstCellDate, Math.floor(originalTimeCellIndex.baseTimeLocation) + startOffset));

			this.newFromDate = targetDate;

			let originalUntilDate = new Date(this.booking.untilDate);
			if (this.booking.untilTime) {
				let split = this.booking.untilTime.split(':', 2);
				originalUntilDate.setHours(+split[0], +split[1]);
			} else {
				originalUntilDate.setHours(23, 59, 59, 999);
			}
			if (originalUntilDate.getTime() < this.newFromDate.getTime()) {
				this.newUntilDate = this.newFromDate;
				this.newFromDate = originalUntilDate;
			} else {
				this.newUntilDate = originalUntilDate;
			}
		} else {
			this.newFromDate = BookingUtils.getStart(this.booking);
			this.newUntilDate = BookingUtils.getEnd(this.booking);
		}

		if (startOffset === 0) {
			if (this.booking.fromTime) {
				this.newFromTime = this.booking.fromTime;
			}
			if (this.booking.untilTime) {
				this.newUntilTime = this.booking.untilTime;
			}
		} else {
			this.newFromTime = moment(this.newFromDate).format('HH:mm');
			this.newUntilTime = moment(this.newUntilDate).format('HH:mm');
		}

		let resources = this.planningView.planningTable.resources;
		if (resources) {
			let originalResourceIndex = resources.findIndex(r => (r.resourceId === this.booking.resource));
			this.newResource = resources[originalResourceIndex].resourceId;
		}
	}

	/**
	 * Scales the booking of this transformation by moving the end by the specified offset.
	 * If the end of the booking is in the middle of a cell a endOffset of -/+1 will snap to the beginning/end of this cell.
	 * @param startOffset - should be a whole number
	 */
	scaleEndBy(endOffset: number) {
		if (endOffset !== 0) {
			if (endOffset < 0) {
				endOffset += 1;
			}
			let viewConfiguration: PlanningViewConfiguration = this.planningView.preferences.content.planningViewConfiguration;
			let firstCellDate: Date = new Date(viewConfiguration.baseUnit.getNextStep(viewConfiguration.dateFrom, 0));
			let originalDate = new Date(this.booking.untilDate);
			if (this.booking.untilTime) {
				let split = this.booking.untilTime.split(':', 2);
				originalDate.setHours(+split[0], +split[1]);
			} else {
				originalDate.setHours(23, 59, 59, 999);
			}
			let originalTimeCellIndex = this.gridLocator.computeGridLocation(originalDate);
			let currentCellStartDate: Date = new Date(viewConfiguration.baseUnit.getNextStep(originalDate, 0));
			let targetDate: Date
				= new Date(viewConfiguration.baseUnit.getNextStep(firstCellDate, Math.floor(originalTimeCellIndex.baseTimeLocation) + endOffset));

			this.newUntilDate = targetDate;

			let originalFromDate = new Date(this.booking.fromDate);
			if (this.booking.fromTime) {
				let split = this.booking.fromTime.split(':', 2);
				originalFromDate.setHours(+split[0], +split[1]);
			} else {
				originalFromDate.setHours(0, 0, 0, 0);
			}
			if (originalFromDate.getTime() > this.newUntilDate.getTime()) {
				this.newFromDate = this.newUntilDate;
				this.newUntilDate = originalFromDate;
			} else {
				this.newFromDate = originalFromDate;
			}
		} else {
			this.newFromDate = BookingUtils.getStart(this.booking);
			this.newUntilDate = BookingUtils.getEnd(this.booking);
		}

		if (endOffset === 0) {
			if (this.booking.fromTime) {
				this.newFromTime = this.booking.fromTime;
			}
			if (this.booking.untilTime) {
				this.newUntilTime = this.booking.untilTime;
			}
		} else {
			this.newFromTime = moment(this.newFromDate).format('HH:mm');
			this.newUntilTime = moment(this.newUntilDate).format('HH:mm');
		}

		let resources = this.planningView.planningTable.resources;
		if (resources) {
			let originalResourceIndex = resources.findIndex(r => (r.resourceId === this.booking.resource));
			this.newResource = resources[originalResourceIndex].resourceId;
		}
	}

	/**
	 * Applies the transformation by sending the new time and resource values for the booking to the rest service.
	 */
	applyTransform(planningTableService: PlanningTableService) {
		planningTableService.updatePlanElement(this.planningView.planningTableId, this.booking.entityMeta,
			this.booking.id, this.newResource, this.newFromDate, this.newUntilDate, this.newFromTime, this.newUntilTime);
	}

	/**
	 * Shifts the booking of this transformation by the specified timeOffset.
	 * There is a special specification, that was carried over from the RichClient implementation:
	 * When shifting a booking in time, the amount of visible time should stay the same.
	 * E.g.: CustomTimeUnits can have gaps, that are not shown in the grid.
	 * Moving a booking will not result in time disappearing in such a gap.
	 * Also, cells of CustomTimeUnits can contain different amounts of time.
	 * Moving a booking into another cell, can change how much space the booking will visually take.
	 * @param timeOffset - should be a whole number
	 * @private
	 */
	private calculateTimeTranslation(timeOffset: number) {
		// store the viewConfiguration in a local variable for readability
		let viewConfiguration: PlanningViewConfiguration = this.planningView.preferences.content.planningViewConfiguration;
		// calculate the start date of the grid
		let firstCellDate: Date = new Date(viewConfiguration.baseUnit.getNextStep(viewConfiguration.dateFrom, 0));
		// store the original from date
		let originalFromDate: Date = BookingUtils.getStart(this.booking);
		// store the original until date
		let originalUntilDate: Date = BookingUtils.getEnd(this.booking);

		// calculate the index of the original from date in the grid
		let originalFromTimeCellIndex = this.gridLocator.computeGridLocation(originalFromDate);
		// calculate the index of the original until date in the grid
		let originalUntilTimeCellIndex = this.gridLocator.computeGridLocation(originalUntilDate);
		// from the start date of the grid -> take Math.floor(originalFromIndex) + timeOffset steps to get the new from date
		this.newFromDate
			= new Date(viewConfiguration.baseUnit.getNextStep(firstCellDate, Math.floor(originalFromTimeCellIndex.baseTimeLocation) + timeOffset));
		// from the start date of the grid -> take Math.floor(originalUntilIndex) + timeOffset steps to get the new from date
		this.newUntilDate
			= new Date(viewConfiguration.baseUnit.getNextStep(firstCellDate, Math.floor(originalUntilTimeCellIndex.baseTimeLocation) + timeOffset));
		if (this.planningView.preferences.content.planningViewConfiguration.baseUnit instanceof TimeUnitCustom) {
			// if the baseUnit is a TimeUnitCustom, we calculate a end location, so the visible time portion of the booking stays constant
			let visibleMinutes = this.planningView.preferences.content.planningViewConfiguration.baseUnit.getVisibleMinutes(
				BookingUtils.getStart(this.booking), BookingUtils.getEnd(this.booking));
			this.newUntilDate = this.planningView.preferences.content.planningViewConfiguration.baseUnit.findDateToSpanBusinessHours(
				this.newFromDate, visibleMinutes);
		} else {
			// now we need to calculate the exact location inside the grid cells for the new fromDate & untilDate
			let fromCellEndTime = viewConfiguration.baseUnit.getStepEnd(this.newFromDate);
			let untilCellEndTime = viewConfiguration.baseUnit.getStepEnd(this.newUntilDate);

			let startFrac = originalFromTimeCellIndex.baseTimeLocation % 1;
			let endFrac = originalUntilTimeCellIndex.baseTimeLocation % 1;

			this.newFromDate.setTime(this.newFromDate.getTime() + (fromCellEndTime.getTime() - this.newFromDate.getTime()) * startFrac);
			this.newUntilDate.setTime(this.newUntilDate.getTime() + (untilCellEndTime.getTime() - this.newUntilDate.getTime()) * endFrac);
		}
		if (timeOffset === 0 || viewConfiguration.baseUnit.abbreviation !== 'h') {
			// if the time unit is not hour, then it is larger and translating the booking won't affect the hours and minuts
			// so, we restore them to the original values for precision purposes
			if (this.booking.fromTime) {
				this.newFromTime = this.booking.fromTime;
			}
			if (this.booking.untilTime) {
				this.newUntilTime = this.booking.untilTime;
			}
		} else {
			// the time unit is hours and has a non-zero offset
			// so we extract the hours:minutes portion from the newly calculated newFromDate & new UntilDate
			this.newFromTime = moment(this.newFromDate).format('HH:mm');
			this.newUntilTime = moment(this.newUntilDate).format('HH:mm');
		}
	}

	/**
	 * Shifts the booking on the resource axis by the specified amount.
	 * A new resource will only be set, if the offset still lies within the resource list.
	 * @param resourceOffset - should be a whole number
	 * @private
	 */
	private calculateResourceTranslation(resourceOffset: number) {
		let resources = this.planningView.planningTable.resources;
		if (resources) {
			let originalResourceIndex = resources.findIndex(r => (r.resourceId === this.booking.resource));
			if (originalResourceIndex + resourceOffset >= 0 && originalResourceIndex + resourceOffset < resources.length) {
				// valid new resource location -> apply
				this.newResource = resources[originalResourceIndex + resourceOffset].resourceId;
			}
		}
	}

}
