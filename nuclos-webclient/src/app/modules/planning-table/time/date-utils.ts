import { Interval } from './interval';
import { TimeAllocation } from './time-allocation';
import { TimeUnit } from './time-unit';

export const Today = {
	start: () => new Date(new Date().setHours(0, 0, 0, 0)),
	current: () => new Date(),
	end: () => new Date(new Date().setHours(23, 59, 59, 999))
};

const DAY_OF_YEAR = 1;
const WEEK_OF_YEAR = 2;
const MONTH = 3;
const YEAR = 4;

const mapCalcStrings: Map<string, number> = new Map([
	['D', DAY_OF_YEAR],
	['T', DAY_OF_YEAR],
	['W', WEEK_OF_YEAR],
	['M', MONTH],
	['Y', YEAR],
	['J', YEAR]
]);

export class DateUtils {

	/**
	 * Returns true, if both dates match by year, month and day.
	 * The hour, minutes, seconds, milliseconds portion may differ.
	 * @param d1
	 * @param d2
	 */
	static isDateEqual(d1: Date, d2: Date) {
		return (d1.getDate() === d2.getDate() && d1.getMonth() === d2.getMonth() && d1.getFullYear() === d2.getFullYear());
	}

	static toNgbDate(date: Date) {
		return { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
	}

	/**
	 * Checks whether a given date is on a weekend.
	 * true, if date is Sunday or Saturday
	 * false, if date lies between Monday and Friday (both included)
	 * @param date
	 */
	static isWeekend(date: Date) {
		return (date.getDay() === 6 || date.getDay() === 0);
	}

	/**
	 * Returns the number of the calendar week (cw) of the input date.
	 * @param date
	 */
	static getWeekNumber(date: Date) {
		let d: Date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
		let dayNum: number = d.getUTCDay() || 7;
		d.setUTCDate(d.getUTCDate() + 4 - dayNum);
		let yearStart: Date = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
		return Math.ceil((((d.getTime() - yearStart.getTime()) / 86400000) + 1) / 7);
	}

	/**
	 * Returns the start of input date.
	 * It sets the hours, minutes, seconds and milliseconds to 0.
	 * @param date
	 */
	static getStart(date: Date) {
		let result: Date = new Date(date);
		result.setHours(0, 0, 0, 0);
		return result;
	}

	/**
	 * Returns the last moment of the input dates day.
	 * The last moment of a day has following properties: 23h:59m:59s:999ms.
	 * @param date
	 */
	static getEnd(date: Date) {
		let result: Date = new Date(date);
		result.setHours(23, 59, 59, 999);
		return result;
	}

	static getTimeAllocation(interval: Interval, unit: TimeUnit, n: number) {
		let allocationStart: Date = unit.getNextStep(interval.fromTime, n);
		let allocationEnd: number = unit.getStepEnd(allocationStart).getTime();
		if (allocationEnd > interval.untilTime.getTime()) {
			allocationEnd = interval.untilTime.getTime();
		}
		let endDate: Date = new Date(allocationEnd);
		return new TimeAllocation(allocationStart, endDate, unit);
	}


	/**
	 * This function takes a relative date expression (e.g.: ".m[" or "+6d.d]") and calculates it based of today.
	 * The code has been ported from DateUtils.java to this typescript class.
	 * @see DateUtils.java
	 * @param calculation - relative date expression
	 * @param today - optional change the today date, the relative expression gets evaluated to (useful for testing)
	 */
	public static calc(calculation: string, today?: Date): Date {
		let result: Date = new Date();
		if (today) {
			result = today;
		}
		if (!calculation || calculation.length === 0) {
			return result;
		}
		calculation = calculation.toUpperCase();

		let operand = '';
		let cf: CalcFunction | undefined = undefined;
		for (let i = 0; i < calculation.length; i++) {
			let c: string = calculation.charAt(i);

			switch (c) {
				case '+':
				case '-':
				case '.':
					if (cf != null) {
						DateUtils.calcEnd(result, cf, DateUtils.getCalcPair(operand));
						operand = '';
					}
					break;
			}

			switch (c) {
				case '+':
					cf = CalcFunction.ADD;
					break;
				case '-':
					cf = CalcFunction.SUBTRACT;
					break;
				case '.':
					cf = CalcFunction.SET;
					break;
				default:
					operand += c;
				}
			}

			if (operand.length > 0 && cf !== undefined) {
				DateUtils.calcEnd(result, cf, DateUtils.getCalcPair(operand));
			}

			return result;
	}

	private static getCalcPair(input: string): CalcPair  {
		let result: CalcPair = new CalcPair();

		let end: string = input.substring(input.length - 1, input.length);
		if ('[' === end || ']' === end) {
			// set start or end
			let field: string = input.substring(input.length - 2, input.length - 1);
			result.x = mapCalcStrings.get(field) as number;
			result.y = ('[' === end) ? Number.MIN_SAFE_INTEGER : Number.MAX_SAFE_INTEGER;
		} else {
			let value: string = input.substring(0, input.length - 1);
			result.x = mapCalcStrings.get(end) as number;
			result.y = +value;
		}

		return result;
	}

	private static calcEnd(result: Date, cf: CalcFunction , cp: CalcPair ) {
		if (cf === CalcFunction.SUBTRACT || cf === CalcFunction.ADD) {
			if (cf === CalcFunction.SUBTRACT) {
				cp.y = -cp.y;
			}
			switch (cp.x) {
				case DAY_OF_YEAR:
					result.setDate(result.getDate() + cp.y);
					break;
				case WEEK_OF_YEAR:
					result.setDate(result.getDate() + (7 * cp.y));
					break;
				case MONTH:
					let newDate = moment().set('year', result.getFullYear()).set('month', result.getMonth())
														.set('date', result.getDate()).add(cp.y, 'month').toDate();
					result.setTime(newDate.getTime());
					break;
				case YEAR:
					result.setFullYear(result.getFullYear() + cp.y);
					break;
			}
		} else if (cf === CalcFunction.SET) {
			switch (cp.x) {
				case YEAR:
					if (cp.y === Number.MIN_SAFE_INTEGER) {
						result.setMonth(0, 1);
					} else {
						result.setMonth(11, 31);
					}
					break;
				case MONTH:
					if (cp.y === Number.MIN_SAFE_INTEGER) {
						result.setMonth(result.getMonth(), 1);
					} else {
						result.setMonth(result.getMonth() + 1, 0);
					}
					break;
				case WEEK_OF_YEAR:
					let newDate = moment().set('year', result.getFullYear()).set('month', result.getMonth())
						.set('date', result.getDate()).day((cp.y === Number.MIN_SAFE_INTEGER) ? 'Monday' : 'Sunday').toDate();
					result.setTime(newDate.getTime());
					break;
			}
		}
	}
}

class CalcPair {

	public x: number;
	public y: number;

	constructor() {

	}

}

enum CalcFunction {
	ADD, SUBTRACT, SET, DEFAULT
}
