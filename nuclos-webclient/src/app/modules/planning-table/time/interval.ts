/**
 * Defines a time intervall by 2 dates.
 * fromTime indicates where the intervall begins.
 * untilTime indicates where the intervall ends.
 */
export class Interval {

	/** The start date, where this interval begins. **/
	fromTime: Date;
	/** The end date, where this interval ends. **/
	untilTime: Date;

	/**
	 * Constructs a new time intervall.
	 * Negative Intervalls will throw an error.
	 * @throws RangeError - when the untilTime < fromTime
	 * @param fromTime - where the intervall beginns
	 * @param untilTime - where the intervall ends
	 */
	constructor(fromTime: Date, untilTime: Date) {
		if (fromTime.getTime() > untilTime.getTime()) {
			throw new RangeError();
		}
		this.fromTime = fromTime;
		this.untilTime = untilTime;
	}

}

