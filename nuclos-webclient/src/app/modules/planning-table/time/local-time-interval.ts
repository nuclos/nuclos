import { LocalTime } from './local-time';

/**
 * This class specifies an interval of 2 LocalTime objects.
 * [fromTime, untilTime[
 * It is used by the TimeUnitCustom class to specify, which time intervals of a day should be displayed by
 * the planning-table component, when selecting the custom time unit.
 * @see time-unit-custom.ts
 */
export class LocalTimeInterval {

	constructor(public fromTime: LocalTime, public untilTime: LocalTime) {

	}

	contains(date: Date) {
		if (date.getHours() * 60 + date.getMinutes() >= this.fromTime.hours * 60 + this.fromTime.minutes
			&& date.getHours() * 60 + date.getMinutes() < this.untilTime.hours * 60 + this.untilTime.minutes) {
			return true;
		}
		return false;
	}

}
