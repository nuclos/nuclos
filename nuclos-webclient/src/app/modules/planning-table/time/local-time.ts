/**
 * LocalTime defines a time location within a unknown day.
 * Information about time-zone, date, week, month, year are not represented by this concept.
 * @see local-time-interval.ts
 * @see time-unit-custom.ts
 */
export class LocalTime {

	/**
	 * Builds a LocalTime object from a string that matches the pattern hh:mm.
	 * @param parsableString
	 */
	static of(parsableString: String): LocalTime {
		let result = new LocalTime();
		const split = parsableString.split(':');
		result.hours = +split[0];
		result.minutes = +split[1];
		return result;
	}

	public hours: number;
	public minutes: number;

	constructor() {

	}

	asMinutes(): number {
		return this.hours * 60 + this.minutes;
	}

}
