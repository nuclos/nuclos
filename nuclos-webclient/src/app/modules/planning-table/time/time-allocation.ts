import { Interval } from './interval';
import { TimeUnit } from './time-unit';

/**
 * A TimeAllocation is an intervall, which does not exceed the maximum step size of the time unit given in the constructor.
 * TimeAllocations are assigned to time header cells and planning cells to keep track of the interval that is managed by those cells.
 * @see time-unit-display.ts
 * @see planning-cell.component.ts
 */
export class TimeAllocation extends Interval {

	constructor(fromTime: Date, untilTime: Date, public timeUnit: TimeUnit) {
		if (fromTime.getTime() > untilTime.getTime()) {
			untilTime = fromTime;
		}
		super(fromTime, untilTime);
	}

	contains(date: number | undefined) {
		if (date === undefined) {
			return false;
		}
		return (date >= this.fromTime.getTime() && date < this.untilTime.getTime());
	}

	toString() {
		return '[' + moment(this.fromTime).format('YYYY-MM-DD HH:mm:ss') + ' / ' + moment(this.untilTime).format('YYYY-MM-DD HH:mm:ss') + ']';
	}

}
