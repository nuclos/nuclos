import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { DateUtils } from './date-utils';
import { Interval } from './interval';

/**
 * Namespace providing formatting functions for intervals.
 * Mainly used by time-unit-display.ts to format the title of a time header cell.
 * @see time-unit-display.ts
 */
export namespace TimeRepresentation {

	/**
	 * Returns a String like XX:XX-YY:YY
	 * @param interval
	 * @param i18n
	 */
	export function hour_interval(interval: Interval, i18n: NuclosI18nService) {
		return String(interval.fromTime.getHours()).padStart(2, '0')
			+ '-' + String(interval.untilTime.getHours() + (interval.untilTime.getMinutes() > 0 ? 1 : 0)).padStart(2, '0');
	}

	/**
	 * Formats the interval start to have a day of month pattern.
	 * german locale pattern: DD.MM
	 * english locale pattern: MM.DD
	 * The pattern is defined by the locale, so it can be changed for each language individually in the locale.ts file.
	 * @param interval
	 * @param i18n
	 */
	export function day_of_month(interval: Interval, i18n: NuclosI18nService) {
		let fromMoment = moment(interval.fromTime);
		return fromMoment.format(i18n.getI18n('webclient.planningTable.time.format.date'));
	}

	/**
	 * Extracts the calendar week of the interval start and appends it to the abbreviation of 'Calendar Week' in the current locale.
	 * Example results: 'CW 23' (english) or 'KW 48' (german)
	 * @param interval
	 * @param i18n
	 */
	export function week(interval: Interval, i18n: NuclosI18nService) {
		return i18n.getI18n('webclient.planningTable.time.unit.abbreviation.calendar.week')
			+ ' ' + DateUtils.getWeekNumber(interval.fromTime);
	}

	/**
	 * Returns the first 3 letters of the month the interval is starting at.
	 * @param interval
	 * @param i18n
	 */
	export function month_3_leading_chars(interval: Interval, i18n: NuclosI18nService) {
		return getAbbreviatedMonth(interval, i18n);
	}

	/**
	 * Returns the first 3 letters of the month and the last 2 digits of the year the interval is starting at.
	 * Example results (english locale): Mar 22, May 16
	 * Example results (german locale): Mär 22, Mai 16
	 * @param interval
	 * @param i18n
	 */
	export function month_and_year(interval: Interval, i18n: NuclosI18nService) {
		return getAbbreviatedMonth(interval, i18n) + ' ' + getYearTrailingNumbers(interval);
	}

	/**
	 * Returns the business quarter abbreviation and number with the last 2 digits of the year the interval is starting at.
	 * Example results: Q1 22, Q3 20
	 * @param interval
	 * @param i18n
	 */
	export function quarter_year(interval: Interval, i18n: NuclosI18nService) {
		return i18n.getI18n('webclient.planningTable.time.unit.abbreviation.quarter')
			+ Math.floor((interval.fromTime.getMonth() + 3) / 3) + ' ' + getYearTrailingNumbers(interval);
	}

	/**
	 * Returns only the business quarter abbreviation and number where the interval is starting at.
	 * Result set: Q1, Q2, Q3, Q4
	 * @param interval
	 * @param i18n
	 */
	export function quarter_only(interval: Interval, i18n: NuclosI18nService) {
		return i18n.getI18n('webclient.planningTable.time.unit.abbreviation.quarter')
			+ Math.floor((interval.fromTime.getMonth() + 3) / 3);
	}

	/**
	 * Returns the full year the interval is starting at, without omitting any digits.
	 * Example results: 2016, 2023
	 * @param interval
	 * @param i18n
	 */
	export function full_year_only(interval: Interval, i18n: NuclosI18nService) {
		return '' + interval.fromTime.getFullYear();
	}

	/**
	 * Formats the time-unit-custom for a time header cell.
	 * Returns a string like: XX:XX-YY:YY
	 * @param interval
	 * @param i18n
	 */
	export function custom(interval: Interval, i18n: NuclosI18nService) {
		return String(interval.fromTime.getHours()).padStart(2, '0')
			+ '-' + String(interval.untilTime.getHours() + (interval.untilTime.getMinutes() > 0 ? 1 : 0)).padStart(2, '0');
	}

	/***
	 * Helper function, which returns the first 3 letters of the interval start months name.
	 * Example results: Jan, Aug, Sep
	 * @param interval
	 * @param i18
	 * @private
	 */
	function getAbbreviatedMonth(interval: Interval, i18: NuclosI18nService) {
		return i18.getI18n('webclient.planningTable.time.unit.month.' + interval.fromTime.getMonth()).slice(0, 3);
	}

	/**
	 * Helper function, which returns the last 2 digits of a year.
	 * Example: input 2016 -> output 16
	 * @param interval
	 * @private
	 */
	function getYearTrailingNumbers(interval: Interval) {
		return String(interval.fromTime.getFullYear()).slice(-2);
	}

}
