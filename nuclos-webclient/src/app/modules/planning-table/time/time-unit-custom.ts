import { LocalTimeInterval } from '@modules/planning-table/time/local-time-interval';
import { TimeUnit } from './time-unit';

/**
 * The TimeUnitCustom class is responsible for representing a concept also known as business hours.
 * One custom time unit can be created for a planning table.
 * The configurator chooses a set of local time intervals in the planning table wizard.
 * This set HAS to be non-overlapping. It MAY be discontinuous.
 * Examples:
 * 		([08:00-12:00], [11:00-14:00]) NOT ALLOWED
 * 		([03:00-04:00], [06:00-11:00], [11:00-13:00]) ALLOWED
 * When a user chooses the custom time unit, in the planning-table component, the grid will only generate cells
 * for the specified local time intervals.
 * *
 * Because unlike the other time units, where the end of a unit step is right next to the start of the next step,
 * this time unit can be discontinuous. This is the reason, why there is a customStepToEnd() function in this class.
 * @see customStepToEnd
 * In the constructor it replaces the default getStepEnd() that is inherited from the time-unit.ts class.
 * The function implementations of getNextStep, getStepEnd and timeAllocationsBetween are more complex than the versions
 * in time-unit.ts. This is because this way of defining a time unit has 2 aspects that make it more complicated.
 * 1. Aspect: Interval sizes may vary
 * 2. Aspect: There may be gaps between the intervals
 * @see planning-grid.component.ts
 * @see local-time.ts
 * @see local-time-interval.ts
 */
export class TimeUnitCustom extends TimeUnit {

	/** stores how many business minutes a day has **/
	minutesPerDay = 0;

	/**
	 * Constructs a custom time unit object.
	 * The LocalTimeInterval is sorted ascending.
	 * @param localIntervals
	 */
	constructor(private localIntervals: LocalTimeInterval[]) {
		super();
		this.getNextStep = this.stepAllocations;
		this.getNumSteps = this.timeAllocationsBetween;
		this.getStepEnd = this.customStepToEnd;
		this.nameKey = 'Custom';
		this.abbreviation = 'custom';

		localIntervals.sort((a: LocalTimeInterval, b: LocalTimeInterval) => {
			return (a.fromTime.hours * 60 + a.fromTime.minutes) - (b.fromTime.hours * 60 + b.fromTime.minutes);
		});
		localIntervals.forEach(lti => {
			this.minutesPerDay += (lti.untilTime.hours * 60 + lti.untilTime.minutes) - (lti.fromTime.hours * 60 + lti.fromTime.minutes);
		});
	}

	/**
	 * Returns the number of steps, that are required to reach the end time from the specified start time.
	 * @param start
	 * @param end
	 */
	timeAllocationsBetween(start: Date, end: Date) {
		let numAllocations = 0;

		let startIndex = 0;
		let endIndex = 0;
		let negative = (end.getTime() < start.getTime());

		if (negative) {
			[start, end] = [end, start];
		}

		this.localIntervals.forEach((value: LocalTimeInterval, index: number) => {
			if (start.getHours() >= value.fromTime.hours && start.getMinutes() >= value.fromTime.minutes) {
				startIndex = index;
			}
			if (end.getHours() >= value.fromTime.hours && end.getMinutes() >= value.fromTime.minutes) {
				endIndex = index;
			}
		});

		if (start.getDate() === end.getDate() && start.getMonth() === end.getMonth() && start.getFullYear() === end.getFullYear()) {
			/* startIndex and endIndex lie within the same day. -> difference between endIndex and startindex */
			numAllocations += (endIndex + 1 - startIndex);
		} else {
			/* startIndex and endIndex are on different days. */
			/* remaining indices from startIndex + potentially number of whole days between start & end + number of indices to reach end index */
			/* calculate how many complete days are between start and end. */
			let daysBetween = this.daysBetween(start, end);
			/* We know that in each day we have n = this.localIntervals.length LocalTimeIntervals that count as a step. */
			numAllocations += Math.max(0, daysBetween - 1) * this.localIntervals.length;
			/* calculate the border cases: add amount of indices of the border cases startIndex & endIndex */
			numAllocations += (this.localIntervals.length - startIndex) + (endIndex + 1);
		}

		return Math.max(1, numAllocations) * (negative ? -1 : 1);
	}

	daysBetween(start: Date, end: Date) {
		let startMom = moment(start).add(12, 'hours').startOf('day');
		let endMom = moment(end).startOf('day');
		return endMom.diff(startMom, 'days');
	}

	/**
	 * Makes n unit steps.
	 * n should be >= 0 to work correctly - n < 0 is not implemented, because it isn't a requirement for the module to work.
	 * Approach:
	 * 		1. Calculate the new day, that will be reached after n steps
	 * 		2. Calculate and set the exact time of the day, that will be reached after n steps
	 * Prerequisites to understand the logic of this method:
	 * 1. Every n = this.localIntervals.length, it stepped over all localIntervals of a day. -> (n / this.localIntervals.length)
	 * 2. We are not always at the beginning of a day, we need to include the index of the local-time-interval the date is currently at.
	 * 		--> (startIndex + n) / this.localIntervals.length
	 * 3. Because we only want whole days, we need to floor the result: ⎣(startIndex + n) / this.localIntervals.length⎦
	 * 4. If we apply the modulo operator to the last formula, we get the remaining steps or the location inside the day, we are looking for.
	 * @param date
	 * @param n
	 */
	stepAllocations(date: Date, n: number) {
		let result = new Date(date.getTime());
		// this snaps the grid from 00:00 to the real beginning
		if (result.getHours() < this.localIntervals[0].fromTime.hours) {
			result.setHours(this.localIntervals[0].fromTime.hours,
				this.localIntervals[0].fromTime.minutes);
		}
		// note: this call probably costs the most performance because the findCurrentAllocationIndex has a for loop.
		// if there was a possibility to map every hour and every minute of a day to the correct index, one could maybe optimize this part.
		let startIndex = this.findCurrentAllocationIndex(date);

		if (n > 0) {
			let days = Math.floor((startIndex + n) / this.localIntervals.length);
			result.setDate(date.getDate() + days);
			let targetIndex = (startIndex + n) % this.localIntervals.length;
			result.setHours(this.localIntervals[targetIndex].fromTime.hours, this.localIntervals[targetIndex].fromTime.minutes);
		} else if (n < 0) {
			// not supported and currently not needed
		}

		return result;
	}

	/**
	 * Returns the index of the interval, which contains the input date.
	 * If the input date lies between 2 intervalls, but not directly in one of them, it will return the index to the right.
	 * This method assumes the intervals are ordered ascending.
	 * @param date
	 */
	findCurrentAllocationIndex(date: Date): number {
		let currentIndex = 0;
		for (let i = 0; i < this.localIntervals.length; i++) {
			if (date.getHours() * 60 + date.getMinutes() <= this.localIntervals[i].fromTime.hours * 60 + this.localIntervals[i].fromTime.minutes) {
				currentIndex = i;
				break;
			}
		}
		return currentIndex;
	}

	/**
	 * Checks, if there is an interval that contains the input date.
	 * true - if, there exists such an interval
	 * false - otherwise
	 * @param date
	 */
	containsDate(date: Date): boolean {
		for (let i = 0; i < this.localIntervals.length; i++) {
			if (date.getHours() * 60 + date.getMinutes() >= this.localIntervals[i].fromTime.hours * 60 + this.localIntervals[i].fromTime.minutes
				&& date.getHours() * 60 + date.getMinutes() < this.localIntervals[i].untilTime.hours * 60 + this.localIntervals[i].untilTime.minutes) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Calculates the amount of business minutes between the fromTime and the untilTime.
	 * Business minutes are minutes, which are in an intervall of this TimeUnitCustom.
	 * @param fromTime
	 * @param untilTime
	 */
	getVisibleMinutes(fromTime: Date, untilTime: Date): number {
		let visibleMinutes = 0;
		let startIndex = this.findCurrentAllocationIndex(fromTime);
		// may or may not be included
		let endIndex = this.findCurrentAllocationIndex(untilTime);

		if (this.localIntervals[startIndex].contains(fromTime)
			|| (fromTime.getHours() * 60 + fromTime.getMinutes() <
				this.localIntervals[startIndex].fromTime.hours * 60 + this.localIntervals[startIndex].fromTime.minutes)) {

		}
		let currIndex = startIndex;
		if (fromTime.getDate() === untilTime.getDate() && fromTime.getMonth() === untilTime.getMonth()
			&& fromTime.getFullYear() === untilTime.getFullYear()) {
			while (currIndex <= endIndex) {
				let start = Math.min(this.localIntervals[currIndex].fromTime.asMinutes(), untilTime.getHours() * 60 + untilTime.getMinutes());
				let end = Math.min(this.localIntervals[currIndex].untilTime.asMinutes(), untilTime.getHours() * 60 + untilTime.getMinutes());
				visibleMinutes += end - start;
				currIndex++;
			}
		} else {
			let daysBetween = this.daysBetween(fromTime, untilTime);
			/* For whole days, we know the amount of minutesPerDay. */
			visibleMinutes += Math.max(0, daysBetween - 1) * this.minutesPerDay;
			/* For the borders we have to sum up the rest of the steps. */
			let restSteps = (this.localIntervals.length - startIndex) + (endIndex + 1);
			let currRestStep = 0;
			while (currRestStep < restSteps) {
				let start = (this.localIntervals[(startIndex + currRestStep) % this.localIntervals.length].fromTime.asMinutes());
				let end = (this.localIntervals[(startIndex + currRestStep) % this.localIntervals.length].untilTime.asMinutes());
				if (currRestStep === 0) {
					start = (Math.max(start, fromTime.getHours() * 60 + fromTime.getMinutes()));
					end = (Math.min(end, untilTime.getHours() * 60 + untilTime.getMinutes()));
				} else if (currRestStep === restSteps - 1) {
					start = (Math.min(start, untilTime.getHours() * 60 + untilTime.getMinutes()));
					end = (Math.min(end, untilTime.getHours() * 60 + untilTime.getMinutes()));
				}
				visibleMinutes += (end - start);
				currRestStep++;
			}
		}
		return visibleMinutes;
	}

	/**
	 * Calculates the end date required to have the specified amount of visible minutes from the fromTime.
	 * @param fromTime
	 * @param visibleMinutes
	 */
	findDateToSpanBusinessHours(fromTime: Date, visibleMinutes: number): Date {
		let daysToStep = Math.max(0, Math.floor(visibleMinutes / this.minutesPerDay) - 1);
		let result = new Date(fromTime);
		let restMinutesToStep = visibleMinutes - daysToStep * this.minutesPerDay;
		let numIntervals = this.localIntervals.length;
		result.setDate(result.getDate() + daysToStep);
		let currIndex = this.findCurrentAllocationIndex(result);
		while (restMinutesToStep > 0) {
			let start = (this.localIntervals[currIndex % numIntervals].fromTime.asMinutes());
			let end = (this.localIntervals[currIndex % numIntervals].untilTime.asMinutes());
			let delta = end - start;
			if (restMinutesToStep >= delta) {
				if (currIndex % numIntervals < (currIndex - 1) % numIntervals) {
					result.setDate(result.getDate() + 1);
				}
				result.setHours(this.localIntervals[currIndex % numIntervals].untilTime.hours,
					this.localIntervals[currIndex % numIntervals].untilTime.minutes);
				restMinutesToStep -= delta;
			} else {
				if (currIndex === this.localIntervals.length) {
					result.setDate(result.getDate() + 1);
					result.setHours(this.localIntervals[0].fromTime.hours, this.localIntervals[0].fromTime.minutes + restMinutesToStep, 0, 0);
				} else {
					result.setHours(result.getHours(), result.getMinutes() + restMinutesToStep, 0, 0);
				}
				restMinutesToStep = 0;
			}
			currIndex++;
		}
		return result;
	}

	/**
	 * Steps from the input date to the next end date.
	 * @param date
	 * @private
	 */
	private customStepToEnd(date: Date) {
		for (let i = 0; i < this.localIntervals.length; i++) {
			if (date.getHours() < this.localIntervals[i].untilTime.hours
				|| (date.getHours() === this.localIntervals[i].untilTime.hours && date.getMinutes() < this.localIntervals[i].untilTime.minutes)) {
				// because our list is ordered ascending, we found the first interval that in time is after the date.
				// so we step to the end of this interval
				let stepResult: Date = new Date(date.getTime());
				stepResult.setHours(this.localIntervals[i].untilTime.hours,
					this.localIntervals[i].untilTime.minutes);
				return stepResult;
			} else {
				// special case: by stepping to the end, we step into a new day
				if (this.localIntervals[i].untilTime.hours < this.localIntervals[i].fromTime.hours
					&& date.getHours() >= this.localIntervals[i].fromTime.hours) {
					let stepResult: Date = new Date(date.getTime());
					stepResult.setDate(stepResult.getDate() + 1);
					stepResult.setHours(this.localIntervals[i].untilTime.hours,
						this.localIntervals[i].untilTime.minutes);
					return stepResult;
				}
			}
		}
		let result: Date = new Date(date.getTime());
		result.setHours(this.localIntervals[0].untilTime.hours, this.localIntervals[0].untilTime.minutes);
		if (result.getTime() < date.getTime()) {
			result.setDate(result.getDate() + 1);
		}
		return result;
	}

}
