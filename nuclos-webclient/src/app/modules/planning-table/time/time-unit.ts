
function stepMilliseconds(date: Date, n: number) {
	let result: Date = new Date(date);
	result.setMilliseconds(result.getMilliseconds() + n);
	return result;
}

function stepSeconds(date: Date, n: number) {
	let result: Date = new Date(date);
	result.setSeconds(result.getSeconds() + n, 0);
	return result;
}

function stepMinutes(date: Date, n: number) {
	let result: Date = new Date(date);
	result.setMinutes(result.getMinutes() + n, 0, 0);
	return result;
}

function stepHours(date: Date, n: number) {
	let result: Date = new Date(date);
	result.setHours(result.getHours() + n, 0, 0, 0);
	return result;
}

function stepDays(date: Date, n: number) {
	let result: Date = new Date(date);
	result.setDate(result.getDate() + n);
	result.setHours(0, 0, 0, 0);
	return result;
}

function stepWeeks(date: Date, n: number) {
	let result: Date = new Date(date.getTime());
	result.setDate(result.getDate() + (n - 1) * 7 + ((7 - date.getDay() + 1) % 7 || 7));
	result.setHours(0, 0, 0, 0);
	return result;
}

function stepMonths(date: Date, n: number) {
	return new Date(date.getFullYear(), date.getMonth() + n, 1);
}

function stepQuarters(date: Date, n: number) {
	return new Date(date.getFullYear(), (Math.floor(date.getMonth() / 3)) * 3 + (3 * n), 1);
}

function stepYears(date: Date, n: number) {
	return new Date(date.getFullYear() + n, 0, 1, 0, 0, 0, 0);
}

function millisBetween(start: Date, end: Date) {
	return end.getTime() - start.getTime();
}

function secondsBetween(start: Date, end: Date) {
	return Math.floor((end.getTime() - start.getTime()) / 1000);
}

function minutesBetween(start: Date, end: Date) {
	return Math.floor((end.getTime() - start.getTime()) / 1000 / 60);
}

function hoursBetween(start: Date, end: Date) {
	return Math.floor((end.getTime() - start.getTime()) / 1000 / 60 / 60);
}

function daysBetween(start: Date, end: Date) {
	return moment(end).diff(moment(start), 'days') + 1;
}

function weeksBetween(start: Date, end: Date) {
	return getWeeksBetweenDates(start, end);
}

function getISOWeek(date: Date): number {
	const target = new Date(date.valueOf());
	const dayNr = (date.getDay() + 6) % 7; // Align Sunday as 7, Monday as 1
	target.setDate(target.getDate() - dayNr + 3); // Move to nearest Thursday

	// Find the first Thursday of the year
	const yearStart = new Date(target.getFullYear(), 0, 4); // January 4th
	const firstThursday = new Date(yearStart.valueOf());
	firstThursday.setDate(yearStart.getDate() - ((yearStart.getDay() + 6) % 7)); // Adjust to Thursday

	// Calculate week number
	const weekNumber = Math.floor((target.getTime() - firstThursday.getTime()) / 604800000) + 1;

	return weekNumber;
}

function isLeapYear(year: number): boolean {
	return year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0);
}

function getWeeksInYear(year: number): number {
	const lastDayOfYear = new Date(year, 11, 28); // December 28th should always be in the last iso week of the year
	return getISOWeek(lastDayOfYear); // Get the ISO week number of the last day of the year
}

/**
 * Calculates the number of calendar weeks, that are fully or partially contained in the [startDate, endDate] intervall
 * Takes into account that the interval may span over multiple years with leap years in between or at the edges.
 * It also handles a case, where a calendar week starts in one year and ends in the other, but should not be counted twice.
 * It is assumed that: startDate <= endDate
 * @param startDate
 * @param endDate
 */
function getWeeksBetweenDates(startDate: Date, endDate: Date): number {
	const startWeek = getISOWeek(startDate);
	const endWeek = getISOWeek(endDate);

	const startYear = startDate.getFullYear();
	const endYear = endDate.getFullYear();

	let weeks = 0;

	if (startYear === endYear) {
		if (endWeek >= startWeek) {
			return endWeek - startWeek + 1;
		} else {
			return (getWeeksInYear(startYear) - startWeek + 1) + endWeek;
		}
	}

	// prevent over accounting a year, if the calendar weeks start in the previous year and reach into the next
	let notStartingAtNextYearsCwOne = !(startDate.getMonth() === 11 && startWeek === 1);
	if (notStartingAtNextYearsCwOne) {
		weeks += getWeeksInYear(startYear) - startWeek + 1;
	}

	for (let year = startYear + 1; year < endYear; year++) {
		weeks += getWeeksInYear(year);
	}

	// if the interval laps into a new year, but the new year starts with old calendar week, do not add it to the overall result again
	let notEndingAtPreviousYearsCwEnd = (endDate.getMonth() !== 0 || endWeek < 51);
	if (notEndingAtPreviousYearsCwEnd) {
		weeks += endWeek;
	}

	// adjust for overlapping week 1 if startWeek is greater than 50
	if (endWeek === 1 && startWeek > 50) {
		weeks--;
	}

	return weeks;
}

function monthsBetween(start: Date, end: Date) {
	let months = (end.getFullYear() - start.getFullYear()) * 12;
	months -= start.getMonth();
	months += end.getMonth() + 1;
	return months <= 0 ? 0 : months;
}

function quartersBetween(start: Date, end: Date) {
	let quarterStart = start.getFullYear() * 4 + Math.floor((start.getMonth() + 3) / 3);
	let quarterEnd = end.getFullYear() * 4 + Math.floor((end.getMonth() + 3) / 3);
	return (quarterEnd - quarterStart) + 1;
}

function yearsBetween(start: Date, end: Date) {
	return (end.getFullYear() - start.getFullYear()) + 1;
}

/**
 * A TimeUnit is defined by following properties:
 * 1. getNextStep function: takes in a date and returns the date that results, when you add 1 of the unit.
 * 2. getNumSteps function: takes in 2 dates and returns how many steps of the time unit lie between.
 * 3. getStepEnd function: takes in a date and returns the end of the current unit step. (useful for discontinuous time definitions)
 * 4. abbreviation string: a abbreviation which should be unique, because it helps identifying a time-unit
 * 5. nameKey string: a i18n key, which gets used, when the time-representation references the time-unit
 */
export class TimeUnit {

	static constructTimeUnit(getNextStep: Function, getNumSteps: Function, abbreviation: string, nameKey?: string) {
		let timeUnit = new TimeUnit();
		timeUnit.getNextStep = getNextStep;
		timeUnit.getNumSteps = getNumSteps;
		timeUnit.abbreviation = abbreviation;
		timeUnit.nameKey = nameKey;
		return timeUnit;
	}

	static getSizeInMillis(timeUnit: TimeUnit) {
		let current: Date = new Date(0, 0, 0, 0, 0, 0, 0);
		let next: Date = timeUnit.getNextStep(current, 1);
		return next.getTime() - current.getTime();
	}

	abbreviation: string;
	nameKey?: string;

	/** Function to make n unit steps. Steps to the beginning of a TimeAllocation. **/
	getNextStep: Function;

	/** Function to compute how many unit steps an intervall has. **/
	getNumSteps: Function;

	/** Returns the size of a timestep. The step size is not always constant (e.g. for months, quarters, years, or custom time unit) **/
	getStepEnd: Function;

	constructor() {
		this.getStepEnd = this.getDefaultStepSize;
	}

	equals(timeUnit: TimeUnit) {
		return (this.abbreviation === timeUnit.abbreviation && this.nameKey === timeUnit.nameKey);
	}

	/**
	 * All time units except time-unit-custom use this method as the getStepEnd Function.
	 * This is because all of these time units are continuous and a step ends, where the next step begins.
	 * @param date
	 */
	getDefaultStepSize(date: Date) {
		return this.getNextStep(date, 1);
	}

}

/**
 * Namespace containing time unit definitions.
 */
export namespace TimeUnits {
	export const UNIT_MILLISECOND: TimeUnit = TimeUnit.constructTimeUnit(stepMilliseconds, millisBetween, 'ms');
	export const UNIT_SECOND: TimeUnit = TimeUnit.constructTimeUnit(stepSeconds, secondsBetween, 's');
	export const UNIT_MINUTE: TimeUnit = TimeUnit.constructTimeUnit(stepMinutes, minutesBetween, 'min');
	export const UNIT_HOUR: TimeUnit = TimeUnit.constructTimeUnit(stepHours, hoursBetween,
			'h', 'webclient.planningTable.time.mode.hour');
	export const UNIT_DAY: TimeUnit = TimeUnit.constructTimeUnit(stepDays, daysBetween,
		'd', 'webclient.planningTable.time.mode.day');
	export const UNIT_WEEK: TimeUnit = TimeUnit.constructTimeUnit(stepWeeks, weeksBetween,
		'w', 'webclient.planningTable.time.mode.week');
	export const UNIT_MONTH: TimeUnit = TimeUnit.constructTimeUnit(stepMonths, monthsBetween,
		'm', 'webclient.planningTable.time.mode.month');
	export const UNIT_QUARTER: TimeUnit = TimeUnit.constructTimeUnit(stepQuarters, quartersBetween,
		'q', 'webclient.planningTable.time.mode.quarter');
	export const UNIT_YEAR: TimeUnit = TimeUnit.constructTimeUnit(stepYears, yearsBetween,
		'y', 'webclient.planningTable.time.mode.year');

	export const ALL: TimeUnit[] = [UNIT_MILLISECOND, UNIT_SECOND, UNIT_MINUTE, UNIT_HOUR,
		UNIT_DAY, UNIT_WEEK, UNIT_MONTH, UNIT_QUARTER, UNIT_YEAR];

}
