import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { AgRendererComponent } from 'ag-grid-angular';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { take } from 'rxjs/operators';
import { NuclosDialogService } from '../../../../core/service/nuclos-dialog.service';
import { NuclosI18nService } from '../../../../core/service/nuclos-i18n.service';
import { PreferencesAdminService } from '../../preferences-admin.service';
import { PreferencesComponent } from '../../preferences.component';
import { Preference } from '../../preferences.model';
import { PreferencesService } from '../../preferences.service';

@Component({
	selector: 'nuc-edit-row-renderer',
	templateUrl: 'edit-row-renderer.component.html',
	styleUrls: ['edit-row-renderer.component.css']
})
export class EditRowRendererComponent implements AgRendererComponent {

	preferenesComponent: PreferencesComponent;
	preferenceItem: Preference<any>;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor(
		private dialogService: NuclosDialogService,
		private i18n: NuclosI18nService,
		private router: Router,
		private preferences: PreferencesService,
		private preferencesAdminService: PreferencesAdminService
	) {
	}

	agInit(params: any) {
		this.preferenceItem = params.value;
		this.preferenesComponent = params.context.componentParent;
	}

	deletePreferenceItem(preferenceItem: Preference<any>): void {
		this.dialogService.confirm(
			{
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n('webclient.dialog.delete')
			}
		).pipe(
			take(1)
		).subscribe((result) => {
			if (result) {
				this.preferences.deletePreferenceItem(preferenceItem).pipe(take(1)).subscribe(
					() => {
						this.router.navigate(['/preferences']);

						// refresh PreferencesComponent
						let index = this.preferenesComponent.preferencesItems.findIndex(p => p.prefId === preferenceItem.prefId);
						if (index > -1) {
							this.preferenesComponent.preferencesItems.splice(index, 1);
						}
						this.preferenesComponent.updatePreferenceItemView();
					}
				);
			}
		});
	}

	updatePreferenceShare(preferenceItem: Preference<any>): void {
		this.preferencesAdminService.updatePreferenceShare(preferenceItem);
	}


	/**
	 * discard the changes the user made on a shared preference
	 * @param preferenceItem
	 */
	discardChanges(preferenceItem: Preference<any>): void {
		this.dialogService.confirm(
			{
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n('webclient.preferences.dialog.confirm.discardCustomizedPreferenceItem')
			}
		).pipe(
			take(1)
		).subscribe((result) => {
			if (result) {
				this.preferencesAdminService.deleteCustomizedPreferenceItem(preferenceItem);
			}
		});
	}

	refresh(params: any): boolean {
		return false;
	}
}
