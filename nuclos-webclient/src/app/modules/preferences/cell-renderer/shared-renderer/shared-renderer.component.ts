import { Component } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { AgRendererComponent } from 'ag-grid-angular';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { Preference } from '../../preferences.model';

@Component({
	selector: 'nuc-shared-renderer',
	templateUrl: 'shared-renderer.component.html',
	styleUrls: ['shared-renderer.component.css']
})
export class SharedRendererComponent implements AgRendererComponent {

	preferenceItem: Preference<any>;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor() {
	}

	agInit(params: any) {
		this.preferenceItem = params.value;
	}

	refresh(params: any): boolean {
		return false;
	}
}
