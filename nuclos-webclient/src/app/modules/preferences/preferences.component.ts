import { Component, HostListener, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { AceEditorComponent } from '@modules/ace-editor-internal/component';
import { GridOptions } from 'ag-grid-community';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { Subject, Subscription } from 'rxjs';
import { catchError, take, takeUntil, takeWhile } from 'rxjs/operators';
import { NuclosDialogService } from '../../core/service/nuclos-dialog.service';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import { BrowserRefreshService } from '../../shared/service/browser-refresh.service';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { SystemParameter } from '../../shared/system-parameters';
import { AuthenticationService } from '../authentication';
import { EditRowRendererComponent } from './cell-renderer/edit-row-renderer/edit-row-renderer.component';
import { SharedRendererComponent } from './cell-renderer/shared-renderer/shared-renderer.component';
import { TypeRendererComponent } from './cell-renderer/type-renderer/type-renderer.component';
import { CustomizedFilterComponent } from './filter/customized-filter/customized-filter.component';
import { SharedFilterComponent } from './filter/shared-filter/shared-filter.component';
import { PreferencesAdminService } from './preferences-admin.service';
import { IUserRole, Preference } from './preferences.model';
import { PreferencesService } from './preferences.service';

type SelectionType = 'none' | 'single' | 'multiple';

declare var ace;

@Component({
	selector: 'nuc-preferences',
	templateUrl: './preferences.component.html',
	styleUrls: ['./preferences.component.css']
})
export class PreferencesComponent implements OnInit, OnDestroy {
	@ViewChildren('editor')
	editors: QueryList<AceEditorComponent>;

	@ViewChildren('popover')
	popovers: QueryList<any>;
	error: string;

	gridOptions = <GridOptions>{};

	selection: SelectionType = 'none';

	isDevMode = false;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;
	isSharingAllowed = true;
	preferencesItems: Preference<any>[] = [];
	selectedPreferenceItem: Preference<any> | undefined = undefined;
	selectedUserUserRoles = [];
	multiselectUserRoles: IUserRole[];
	filter = {
		boMetaId: undefined,
		layoutName: undefined,
		name: undefined
	};

	isContentInEditingMode = false;
	oldContent: string | undefined = undefined;
	editedContent: string;

	routeParamsSubscription: Subscription;

	private editor: AceEditorComponent;

	private unsubscribe$ = new Subject<boolean>();
	private editorSession;
	private popover;

	private componentAlive = false;

	constructor(
		private nuclosConfig: NuclosConfigService,
		private dialogService: NuclosDialogService,
		private authenticationService: AuthenticationService,
		private i18n: NuclosI18nService,
		private router: Router,
		private route: ActivatedRoute,
		private browserRefreshService: BrowserRefreshService,
		private preferencesService: PreferencesService,
		private preferencesAdminService: PreferencesAdminService
	) { }

	ngOnInit() {
		this.ngOnInitImpl();
		this.componentAlive = true;
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
		this.componentAlive = false;
	}

	ngAfterViewInit(): void {
		this.editors.changes
			.pipe(takeWhile(() => this.componentAlive))
			.subscribe((editors: QueryList<AceEditorComponent>) => {
			this.editor = editors.first;

			if (this.editor) {
				let _editor = this.editor.getEditor();

				_editor.setShowInvisibles(true);
				_editor.setFontSize('14');
				_editor.setWrapBehavioursEnabled(true);
				_editor.$blockScrolling = Infinity;

				this.editorSession = _editor.getSession();

				this.editorSession.setUseSoftTabs(false);
				this.editorSession.setUndoManager(new ace.UndoManager());
			}
		});

		this.popovers.changes
			.pipe(takeWhile(() => this.componentAlive))
			.subscribe((popovers: QueryList<any>) => {
			this.popover = popovers.first;
		});
	}

	gridReady(param): void {
		this.gridOptions.api = param.api;
		this.updatePreferenceItemView();
	}

	selectPreferenceItem(preferenceItem: Preference<any>): void {
		this.router.navigate(['/preferences', preferenceItem.prefId]);
	}

	shareOrUnshareSelectedPreferenceItem(userRole: IUserRole): void {
		if (this.selectedPreferenceItem) {
			this.preferencesService
				.shareOrUnsharePreferenceItem(this.selectedPreferenceItem, userRole)
				.pipe(take(1))
				.subscribe(() => {
					if (this.selectedPreferenceItem && this.selectedPreferenceItem.prefId) {
						this.preferencesService
							.getPreference(this.selectedPreferenceItem.prefId)
							.pipe(take(1))
							.subscribe(pref => {
								this.selectedPreferenceItem = pref;
								let prefInList = this.preferencesAdminService.preferencesItems
									.getValue()
									.filter(p => p.prefId === pref.prefId)
									.shift();
								if (prefInList) {
									prefInList.shared = this.selectedPreferenceItem.shared;
								}
							});
					}
				});
		}
	}

	shareOrUnshareMultiSelectionPreferenceItem(userRole: IUserRole): void {
		if (this.selection === 'multiple') {
			this.gridOptions.api?.getSelectedRows().forEach(row => {
				this.preferencesService
					.shareOrUnsharePreferenceItem(row, userRole)
					.pipe(take(1))
					.subscribe(() => {
						if (this.selectedPreferenceItem && this.selectedPreferenceItem.prefId) {
							this.preferencesService
								.getPreference(this.selectedPreferenceItem.prefId)
								.pipe(take(1))
								.subscribe(pref => {
									this.selectedPreferenceItem = pref;
									let prefInList = this.preferencesAdminService.preferencesItems
										.getValue()
										.filter(p => p.prefId === pref.prefId)
										.shift();
									if (prefInList) {
										prefInList.shared = this.selectedPreferenceItem.shared;
									}
								});
						}
					});
			});
		}
	}
	updatePreferenceItemView(): void {
		if (this.gridOptions.api) {
			this.gridOptions.api.setRowData(this.preferencesItems);
		}
	}

	layoutAssignmentText(): string {
		return this.selectedPreferenceItem
			? this.i18n.getI18n(
				'webclient.preferences.layoutAssignmentText',
				this.selectedPreferenceItem.layoutName
			)
			: '';
	}

	layoutAssignmentButtonText(): string {
		return this.selectedPreferenceItem
			? this.i18n.getI18n(
				'webclient.preferences.layoutAssignmentButtonText',
				this.getBoName(this.selectedPreferenceItem)
			)
			: '';
	}

	resetLayoutAssignment(): void {
		if (this.selectedPreferenceItem) {
			delete this.selectedPreferenceItem.layoutId;
			this.preferencesService.savePreferenceItem(this.selectedPreferenceItem).pipe(take(1)).subscribe(pref => {
				if (pref) {
					this.selectedPreferenceItem = pref;
					this.selectedPreferenceItem.layoutName = '';
					this.preferencesItems
						.filter(p => p.prefId === pref.prefId)
						.forEach(p => (p.layoutName = ''));
					if (this.gridOptions.api) {
						this.gridOptions.api.redrawRows();
					}
				}
			});
		}
	}

	/**
	 * check if any of the selected rows are customized
	 * boolean is used to determine if buttons for discard/save changes should be displayed
	 */
	selectedPreferencesCustomized(): boolean {
		if (this.gridOptions.api) {
			let selectedRows = this.gridOptions.api.getSelectedRows().filter(p => p.customized);
			return selectedRows.length > 0;
		}
		return false;
	}

	/**
	 * update preference share for all selected rows which are customized
	 */
	updatePreferenceShares(): void {
		if (this.gridOptions.api) {
			this.gridOptions.api
				.getSelectedRows()
				.filter(p => p.customized)
				.forEach(p => {
					this.preferencesAdminService.updatePreferenceShare(p);
				});
		}
	}

	/**
	 * discard the changes the user made on a shared preference for all selected rows which are customized
	 */
	discardChanges(): void {
		this.dialogService
			.confirm({
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n(
					'webclient.preferences.dialog.confirm.discardCustomizedPreferenceItem'
				)
			})
			.pipe(
				take(1)
			).subscribe((result) => {
			if (result) {
				if (this.gridOptions.api) {
					this.gridOptions.api
						.getSelectedRows()
						.filter(p => p.customized)
						.forEach(p => {
							this.preferencesAdminService.deleteCustomizedPreferenceItem(p);
						});
				}
			}
		});
	}

	/**
	 * delete selected preferences
	 * shared preferences will be unshared before deletion
	 */
	deleteSelectedPreferences(): void {
		this.dialogService
			.confirm({
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n(
					'webclient.preferences.dialog.confirm.deleteSelectedPreferences'
				)
			})
			.pipe(
				take(1)
			).subscribe((result) => {
			if (result) {
				if (this.gridOptions.api) {
					let preferenceItems = this.gridOptions.api.getSelectedRows();
					this.preferencesService
						.findReferencingPreferences(preferenceItems)
						.toPromise()
						.then(prefs => {
							if (prefs?.length ?? 0 > 0) {
								this.dialogService.alert({
									title: this.i18n.getI18n(
										'webclient.preferences.dialog.error.referencingPreferences'
									),
									message: this.i18n.getI18n(
										'webclient.preferences.dialog.error.referencingPreferencesMessage',
										prefs?.map(p => p.name).join(', ')
									)
								}).pipe(
									take(1)
								).subscribe();
							} else {
								this.preferencesService
									.deletePreferenceItems(preferenceItems)
									.toPromise()
									.then(() => {
										this.router.navigate(['/preferences/list']).then(() => {
											this.selectedPreferenceItem = undefined;
											this.loadData();
										});
									});
							}
						});
				}
			}
		});
	}

	/**
	 * assign/remove selected user groups to all selected preferences
	 * not selected user groups will be removed
	 */
	assignUserRolesForSelectedPreferences(): void {
		this.dialogService
			.confirm({
				title: this.i18n.getI18n(
					'webclient.preferences.assignUserRolesForSelectedPreferences'
				),
				message: this.i18n.getI18n(
					'webclient.preferences.dialog.confirm.assignUserRolesForSelectedPreferencesText'
				)
			})
			.pipe(
				take(1)
			).subscribe((result) => {
			if (result) {
				if (this.gridOptions.api) {
					let preferenceItems = this.gridOptions.api.getSelectedRows();
					this.preferencesService
						.shareUserRoles(preferenceItems, this.multiselectUserRoles)
						.then(() => {
							this.loadData();
						});
				}
			}
		});
	}

	selectAllCustomizedRows() {
		if (this.gridOptions.api) {
			this.setCustomizedFiltering('customized');
			this.handleKeyboardEventSelectAll();
		}
	}

	/**
	 * select all preferences with CTRL-A / CMD-A
	 */
	@HostListener('window:keydown.control.a', ['$event'])
	@HostListener('window:keydown.meta.a', ['$event'])
	handleKeyboardEventSelectAll() {
		if (this.gridOptions.api) {
			this.gridOptions.api.selectAllFiltered();
			this.router.navigate(['/preferences/list']);
		}
	}

	/**
	 * deselect all preferences with CTRL-SHIFT-A / CMD-SHIFT-A
	 */
	@HostListener('window:keydown.control.shift.a', ['$event'])
	@HostListener('window:keydown.meta.shift.a', ['$event'])
	handleKeyboardEventDeselectAll() {
		if (this.gridOptions.api) {
			this.gridOptions.api.deselectAllFiltered();
			this.router.navigate(['/preferences/list']);
		}
	}

	resetSelection() {
		if (this.gridOptions.api) {
			this.setCustomizedFiltering('all');
			this.setSharedFiltering('all');
			this.gridOptions.api.setFilterModel(null);
			this.gridOptions.api.onFilterChanged();
			this.gridOptions.api.deselectAll();
			this.router.navigate(['/preferences/list']);
		}
	}

	startContentEditing() {
		if (this.selectedPreferenceItem) {
			this.oldContent = this.selectedPreferenceItem.content;
			this.editedContent = JSON.stringify(this.selectedPreferenceItem.content, null, 3);
			this.isContentInEditingMode = !this.isContentInEditingMode;
		}
	}

	cancelContentEditing(popover: NgbPopover) {
		if (this.selectedPreferenceItem) {
			popover.close();
			this.selectedPreferenceItem.content = this.oldContent;
			this.isContentInEditingMode = false;
		}
	}

	saveEditedContent(popover: NgbPopover) {
		if (this.selectedPreferenceItem) {
			if (popover) {
				popover.close();
			}
			try {
				this.selectedPreferenceItem.content = JSON.parse(this.editedContent);

				this.preferencesService.savePreferenceItem(this.selectedPreferenceItem)
					.pipe(catchError(error => {
						return this.dialogService.alert({
							title: 'Error',
							message: error.error
						});
					}))
					.subscribe((pref) => {
						if (pref) {
							this.selectedPreferenceItem = pref;
							this.isContentInEditingMode = false;
						}
					});
			} catch (e) {
				if (popover) {
					this.error = e.message;
					popover.open();
				}
			}
		}
	}

	setEditedContent(content: string) {
		this.editedContent = content;
	}

	private setSharedFiltering(value: string) {
		if (this.gridOptions.api) {
			this.gridOptions.api.getFilterInstance('shared')?.setModel({
				value: value
			});
			this.gridOptions.api.onFilterChanged();
		}
	}

	private setCustomizedFiltering(value: string) {
		if (this.gridOptions.api) {
			this.gridOptions.api.getFilterInstance('customized')?.setModel({
				value: value
			});
			this.gridOptions.api.onFilterChanged();
		}
	}

	private ngOnInitImpl() {
		this.nuclosConfig.getSystemParameters().pipe(take(1)).subscribe(params => {
			this.isDevMode = params.is(SystemParameter.ENVIRONMENT_DEVELOPMENT);
		});

		/**
		 * update view if preferences have been changed from other browser tabs
		 */
		this.browserRefreshService
			.onPreferenceChange()
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe((preference: Preference<any> | undefined) => {
				if (preference === undefined) {
					this.loadData();
					this.updatePreferenceItemView();
				} else {
					let index = this.preferencesItems.findIndex(
						p => p.prefId === preference.prefId
					);
					if (index > -1) {
						if (preference.prefId) {
							this.preferencesService.getPreference(preference.prefId).pipe(take(1)).subscribe(
								loadedPref => {
									loadedPref.boName = this.getBoName(loadedPref);
									this.preferencesItems[index] = loadedPref;
									if (
										this.selectedPreferenceItem &&
										this.selectedPreferenceItem.prefId === preference.prefId
									) {
										this.selectedPreferenceItem = loadedPref;
									}
									this.updatePreferenceItemView();
								},
								error => {
									// remove pref from list
									this.preferencesItems.splice(index, 1);
								}
							);
						}
					} else {
						// new entry load complete list
						this.loadData();
						this.updatePreferenceItemView();
					}
				}
			});

		this.loadData();
	}

	private loadData() {
		this.preferencesAdminService.loadPreferences().pipe(takeUntil(this.unsubscribe$)).subscribe(prefs => {
			this.preferencesItems = prefs;
			this.updatePreferenceItemView();

			if (this.gridOptions.api) {
				this.gridOptions.api.sizeColumnsToFit();
			}

			if (this.routeParamsSubscription) {
				this.routeParamsSubscription.unsubscribe();
			}

			this.routeParamsSubscription = this.route.params.subscribe((params: Params) => {
				let prefId = params['prefId'];
				if (prefId) {
					// TODO
					// navigating between "/preferences" and "/preferences/{prefId}" destroys/recreates new component
					// prevent this by using 'list' as prefId parameter
					if (prefId === 'list') {
						return;
					}

					this.selectedPreferenceItem = this.preferencesItems.filter(
						pref => pref.prefId === prefId
					)[0];
					if (this.selectedPreferenceItem) {
						// select row in ag-grid
						setTimeout(() => {
							if (this.gridOptions.api) {
								let rowCount = this.gridOptions.api.getModel().getRowCount();
								for (let i = 0; i < rowCount; i++) {
									let row = this.gridOptions.api.getModel().getRow(i);
									if (row?.data.prefId === prefId) {
										row?.setSelected(true);
										this.gridOptions.api.ensureIndexVisible(i);
										break;
									}
								}
							}
						});

						if (this.isSharingAllowed) {
							this.preferencesService
								.getPreferenceShareGroups(this.selectedPreferenceItem)
								.pipe(take(1))
								.subscribe((data: any) => {
									this.selectedUserUserRoles = data.userRoles;
								});
						}
					}
				}
			});

			this.gridOptions.context = {
				componentParent: this
			};
			this.gridOptions.immutableData = true;
			this.gridOptions.getRowNodeId = data => data.prefId;
			this.gridOptions.columnDefs = [
				{
					headerName: this.i18n.getI18n('webclient.preferences.shared'),
					field: 'shared',
					width: 80,
					cellRendererFramework: SharedRendererComponent,
					headerCheckboxSelection: true,
					sortable: true,
					resizable: true,
					filter: true,
					headerCheckboxSelectionFilteredOnly: true,
					valueGetter: this.valueGetter,
					comparator: (valueA, valueB) => {
						if (valueA && valueB) {
							if (valueA.shared === valueB.shared) {
								return 0;
							}
							return valueA.shared < valueB.shared ? -1 : 1;
						}
						if (valueA) {
							return 1;
						}
						if (valueB) {
							return -1;
						}
						return 0;
					},
					filterFramework: SharedFilterComponent
				},
				{
					headerName: this.i18n.getI18n('webclient.preferences.type'),
					field: 'type',
					width: 80,
					sortable: true,
					resizable: true,
					filter: true,
					cellRendererFramework: TypeRendererComponent,
					valueGetter: this.valueGetter,
					comparator: (valueA, valueB) => {
						if (valueA && valueB) {
							if (valueA.type === valueB.type) {
								return 0;
							}
							return valueA.type < valueB.type ? -1 : 1;
						}
						if (valueA) {
							return 1;
						}
						if (valueB) {
							return -1;
						}
						return 0;
					}
				},
				{
					headerName: this.i18n.getI18n('webclient.preferences.businessObject'),
					field: 'boName',
					width: 200,
					sortable: true,
					resizable: true,
					filter: true,
					sort: 'asc',
					sortIndex: 0,
				},
				{
					headerName: this.i18n.getI18n('webclient.preferences.layout'),
					field: 'layoutName',
					sortable: true,
					resizable: true,
					filter: true,
					width: 200
				},
				{
					headerName: this.i18n.getI18n('webclient.preferences.name'),
					field: 'name',
					sortable: true,
					resizable: true,
					filter: true,
					width: 200
				},
				{
					headerName: '',
					width: 50,
					pinned: 'right',
					field: 'customized',
					resizable: true,
					filter: true,
					cellRendererFramework: EditRowRendererComponent,
					valueGetter: this.valueGetter,
					sortable: true,
					filterFramework: CustomizedFilterComponent
				}
			];

			this.gridOptions.rowSelection = 'multiple';

			this.gridOptions.onSelectionChanged = () => {
				if (this.gridOptions.api) {
					let selectedRows = this.gridOptions.api.getSelectedRows();
					if (selectedRows.length === 1) {
						this.selection = 'single';
						let preferenceItem = selectedRows[0];
						this.selectPreferenceItem(preferenceItem);
					} else if (selectedRows.length > 1) {
						this.selection = 'multiple';
						this.selectedPreferenceItem = selectedRows[0];
						this.displayMultipleRowUserGroupSelection();
					} else {
						this.selection = 'none';
						this.selectedPreferenceItem = undefined;
					}
				}
			};
			if (this.gridOptions.api) {
				this.gridOptions.api.redrawRows();
			}
		});
	}

	private valueGetter(col): Preference<any> {
		return col.data;
	}

	private getBoName(item: Preference<any>): string {
		return item.boMetaId.substring(item.boMetaId.lastIndexOf('_') + 1);
	}

	private displayMultipleRowUserGroupSelection() {
		if (!this.multiselectUserRoles && this.selectedPreferenceItem) {
			/*
			 the user should be able to share preferences to the same group he is assigned to
			 it is appropriate to get the share groups of any preference
			 */
			this.preferencesService
				.getPreferenceShareGroups(this.selectedPreferenceItem)
				.pipe(take(1))
				.subscribe((data: any) => {
					this.multiselectUserRoles = data.userRoles.map(ur => {
						return {
							name: ur.name,
							userRoleId: ur.userRoleId,
							shared: false
						};
					});
				});
		}
	}

	private hidePopover(): void {
		if (this.popover) {
			$(this.popover.nativeElement)['popover']('hide');
		}
	}
}
