import { PlanningViewConfiguration } from '@modules/planning-table/shared/planning-view-configuration';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { IPreference, IPreferenceContent, IPreferenceType, PreferenceTypeName } from '@nuclos/nuclos-addon-api';
import * as _ from 'lodash';
import { ChartOptions } from '../chart/shared/chart-options';
import { BoAttr } from '../entity-object-data/shared/bo-view.model';
import { DependentFilterKey } from '../entity-object-data/shared/dependents-searchfilter.service';
import { SubEntityObject } from '../entity-object-data/shared/entity-object.class';
import { WhereCondition } from '../search/shared/search.model';

export type SidebarLayoutType = 'table' | 'card';

export class PreferenceType implements IPreferenceType {
	static searchtemplate: PreferenceTypeConfig = {
		name: 'searchtemplate',
		iconClass: 'fa-search'
	};
	static subformSearchtemplate: PreferenceTypeConfig = { name: 'subform-searchtemplate', iconClass: 'fa-magnifying-glass-location' };
	static table: PreferenceTypeConfig = { name: 'table', iconClass: 'fa-columns' };
	static subformTable: PreferenceTypeConfig = { name: 'subform-table', iconClass: 'fa-table' };
	static planningTable: PreferenceTypeConfig = { name: 'planning-table', iconClass: 'fa-calendar' };
	static perspective: PreferenceTypeConfig = { name: 'perspective', iconClass: 'fa-eye' };
	static chart: PreferenceTypeConfig = { name: 'chart', iconClass: 'fa-pie-chart' };
	static dashboard: PreferenceTypeConfig = { name: 'dashboard', iconClass: 'fa-dashboard' };
	static tasklistTable: PreferenceTypeConfig = {
		name: 'tasklist-table',
		iconClass: 'fa-dashboard'
	};
	static default: PreferenceTypeConfig = { name: 'menu', iconClass: 'fa-asterisk' };

	static getIconClass(preferenceTypeName: string | undefined) {
		/**
		 * TODO: Bad architecture. Do a generic lookup instead of a switch over the possible types.
		 */
		switch (preferenceTypeName) {
			case PreferenceType.searchtemplate.name:
				return PreferenceType.searchtemplate.iconClass;
			case PreferenceType.subformSearchtemplate.name:
				return PreferenceType.subformSearchtemplate.iconClass;
			case PreferenceType.table.name:
				return PreferenceType.table.iconClass;
			case PreferenceType.subformTable.name:
				return PreferenceType.subformTable.iconClass;
			case PreferenceType.perspective.name:
				return PreferenceType.perspective.iconClass;
			case PreferenceType.chart.name:
				return PreferenceType.chart.iconClass;
			case PreferenceType.dashboard.name:
				return PreferenceType.dashboard.iconClass;
			case PreferenceType.planningTable.name:
				return PreferenceType.planningTable.iconClass;
		}

		return PreferenceType.default.iconClass;
	}
}

export interface IPreferenceFilter {
	type?: Array<PreferenceTypeName>;
	app?: string;
	boMetaId?: string;
	selected?: boolean;
	layoutId?: string;
	orLayoutIsNull?: boolean;

	/**
	 * If true, only preferences for sub-BOs of the given BO with the given boMetaId should be returned.
	 */
	returnSubBoPreferences?: boolean;
}

export class Preference<T> implements IPreference<T> {
	prefId?: string;
	layoutId?: string;
	layoutName?: string;
	boName?: string;
	name?: string;
	shared?: boolean;
	selected?: boolean;
	startupDefault?: boolean;
	iconClass?: string;
	customized?: boolean;
	content: T;
	dirty?: boolean;
	taskMetaId?: string;
	filterOp?: string;
	filterValueString?: string;
	filterValueDouble?: string;

	constructor(public type: PreferenceTypeName, public boMetaId: string) {
		this.content = {} as T;
	}
}

export interface SelectedAttribute extends BoAttr {
	selected: boolean;
	mouseover?: boolean;
	width?: number;
	fixed?: boolean;
}

export abstract class PreferenceContent implements IPreferenceContent {}

export class SplitpanePreferenceContent implements  PreferenceContent {
	panelSizes: number[] = [];
}

export abstract class AttributeSelectionContent extends PreferenceContent {
	static equals(a: AttributeSelectionContent, b: AttributeSelectionContent) {
		let equal = true;
		b.columns.forEach((newColumn: ColumnAttribute) => {
			let existingColumn = a.columns
				.filter(col => col.boAttrId === newColumn.boAttrId)
				.shift();
			if (existingColumn) {
				// check if column was modified
				equal =
					equal &&
					_.isEqual(existingColumn.sort, newColumn.sort) &&
					existingColumn.width === newColumn.width &&
					existingColumn.position === newColumn.position &&
					existingColumn.fixed === newColumn.fixed;
			} else {
				// a column was added
				equal = false;
			}
		});

		// a column was removed
		equal =
			equal &&
			a.columns.filter(acol => !b.columns.find(bcol => acol.boAttrId === bcol.boAttrId))
				.length === 0;
		return equal;
	}
	columns: Array<ColumnAttribute>;

	// true if Preference.name is not default generated from selected columns
	userdefinedName?: boolean;
}

export class SearchtemplatePreferenceContent extends AttributeSelectionContent {
	static equals(
		a: SearchtemplatePreferenceContent,
		b: SearchtemplatePreferenceContent
	) {
		return (
			a.disabled === b.disabled &&
			a.isValid === b.isValid &&
			this.columnsEqual(a, b) &&
			super.equals(a, b)
		);
	}

	static validate(content: SearchtemplatePreferenceContent) {
		// Columns might be undefined for legacy searchtemplates, see NUCLOS-7067
		if (!content.columns) {
			content.columns = [];
		}
	}

	private static columnsEqual(
		a: SearchtemplatePreferenceContent,
		b: SearchtemplatePreferenceContent
	) {
		SearchtemplatePreferenceContent.validate(a);
		SearchtemplatePreferenceContent.validate(b);

		if (
			a.columns.find(
				colA =>
					!b.columns.find(
						colB =>
							colA.operator === colB.operator &&
							colA.value === colB.value
					)
			)
		) {
			return false;
		}

		if (
			b.columns.find(
				colB =>
					!a.columns.find(
						colA =>
							colA.operator === colB.operator &&
							colA.value === colB.value
					)
			)
		) {
			return false;
		}

		return true;
	}
	columns: Array<SearchtemplateAttribute> = [];

	disabled: boolean;
	isValid: boolean;
	isTemp?: boolean;
	showAsFavorite?: boolean;
	favoriteIcon?: string;
	position?: number;
	isNewBoInstanceTemplate?: boolean;
	expertWhereCondition?: WhereCondition;
}

export class SubformSearchtemplatePreferenceContent extends AttributeSelectionContent {
	filterKey: DependentFilterKey;
	isTemp?: boolean;
	showAsFavorite?: boolean;
	favoriteIcon?: string;
	position?: number;
}

export interface SearchtemplateAttribute extends SelectedAttribute {
	operator?: string;
	value?: any;
	value2?: any;
	values?: any[];
	formattedValue?: string;
	enableSearch?: boolean;
	isValid?: boolean;
	focusInput?: boolean;
	datepickerValue?: NgbDateStruct;
	dependentBoMetaId?: string;
}

export class SideviewmenuPreferenceContent extends AttributeSelectionContent {
	static equals(
		a: SideviewmenuPreferenceContent,
		b: SideviewmenuPreferenceContent
	) {
		return a.sideviewMenuWidth === b.sideviewMenuWidth && a.sideviewState === b.sideviewState && super.equals(a, b);
	}
	sideviewMenuWidth?: number;
	sideviewMenuHeight?: number;
	sideviewState?: number;
	sideviewHorizontal?: boolean;
	sideviewSearchAttributesContainerHeight?: number;
	sideviewSearchEditorFixed = false;
	sideviewSearchEditorFixedAndShowing = false;
	sidebarLayout?: SidebarLayoutType;

	// not stored, only temporary!
	searchEditorPopoverShowing = false;

	constructor() {
		super();
		this.columns = [];
	}
}

export class ChartPreferenceContent extends PreferenceContent {
	chart: ChartOptions;
	name: {
		de: string;
	};
	openReferenceOnClick?: boolean;

	constructor() {
		super();
	}
}

export class PlanningTablePreferenceContent extends PreferenceContent {

	planningTableId: string;
	planningViewConfiguration: PlanningViewConfiguration;

	constructor() {
		super();

	}

}

export interface ColumnAttribute extends SelectedAttribute {
	sort?: SideviewColumnSort;
	position?: number;
	getPreferredHeight?: (eo: SubEntityObject) => number | undefined;
	filterOp?: string;
	filterValueString?: string;
	filterValueDouble?: string;
	filterValueBoolean?: string;
	filterValueDate?: string;
}

export interface SideviewColumnSort {
	direction?: string | null;
	prio?: number | null;
	enabled?: boolean;
}

export interface IUserRole {
	name: string;
	userRoleId: string;
	shared: boolean;
}

export class PreferenceTypeConfig {
	name: PreferenceTypeName;
	iconClass: string;
}
