import { RouterModule, Routes } from '@angular/router';
import { PreferencesNavigationGuard } from './preferences-navigation-guard';
import { PreferencesComponent } from './preferences.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'preferences',
		redirectTo: 'preferences/list',
		pathMatch: 'full'
	},
	{
		path: 'preferences/:prefId',
		data: {
			localizedTitle: 'webclient.preferences.manage'
		},
		component: PreferencesComponent,
		canActivate: [
			PreferencesNavigationGuard
		]
	}
];

export const PreferencesRoutes = RouterModule.forChild(ROUTE_CONFIG);
