import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPreference, IPreferenceContent, IPreferenceFilter } from '@nuclos/nuclos-addon-api';
import { EMPTY, Observable, of as observableOf, throwError as observableThrowError } from 'rxjs';

import { catchError, concatMap, map, mergeMap, tap } from 'rxjs/operators';
import { BrowserRefreshService } from '../../shared/service/browser-refresh.service';
import { FqnService } from '../../shared/service/fqn.service';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { NuclosCache, NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { MetaService } from '../entity-object-data/shared/meta.service';
import { Logger } from '../log/shared/logger';
import {
	IUserRole,
	Preference,
	PreferenceContent,
	PreferenceType,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from './preferences.model';

export const PREFERENCE_NAME_MAX_LENGTH = 255;

@Injectable()
export class PreferencesService {
	private preferencesCache: NuclosCache;
	private preferenceMentionCache: Map<string, Set<string>>;

	constructor(
		private nuclosConfig: NuclosConfigService,
		private cacheService: NuclosCacheService,
		private http: HttpClient,
		private metaService: MetaService,
		private fqnService: FqnService,
		private browserRefreshService: BrowserRefreshService,
		private $log: Logger
	) {
		this.preferencesCache = this.cacheService.getCache('preferencesCache');
		this.preferenceMentionCache = new Map<string, Set<string>>();
	}

	getPreference(
		prefId: string,
		useCache = true
	): Observable<IPreference<IPreferenceContent>> {
		let observable = this.http
			.get(this.nuclosConfig.getRestHost() + '/preferences/' + prefId)
			.pipe(map(this.handleSideviewmenuPrefs));

		if (useCache) {
			// always return a fresh copy of the cached original
			return this.preferencesCache.get(prefId, observable).
			pipe(map((preference => JSON.parse(JSON.stringify(preference)))));
		}
		return observable;
	}

	getPreferences(
		filter: IPreferenceFilter,
		useCache = true
	): Observable<Array<Preference<PreferenceContent>>> {
		this.$log.debug('filter: %o', filter);

		let params = new HttpParams();
		let cacheKey = '';
		if (filter.boMetaId) {
			params = params.append('boMetaId', filter.boMetaId);
			cacheKey += '/boMetaId=' + filter.boMetaId;
		}
		if (filter.layoutId) {
			params = params.append('layoutId', filter.layoutId);
			cacheKey += '/layoutId=' + filter.layoutId;
		}
		if (filter.orLayoutIsNull) {
			params = params.append('orLayoutIsNull', 'true');
			cacheKey += '/orLayoutIsNull';
		}
		if (filter.returnSubBoPreferences) {
			params = params.append('returnSubBo', 'true');
			cacheKey += '/returnSubBo';
		}
		if (filter.app) {
			params = params.append('app', filter.app);
			cacheKey += '/app=' + filter.app;
		}
		if (filter.type) {
			let types = filter.type.join(',');
			params = params.append('type', types);
			cacheKey += '/type=' + types;
		}

		let observable = this.http
			.get(this.nuclosConfig.getRestHost() + '/preferences', {
				params: params
			})
			.pipe(
				catchError(e => {
					this.$log.warn('Could not load prefs for filter %o: %o', filter, e);
					return EMPTY;
				}),
				map((preferences: Preference<any>[]) => {
					if (useCache) {
						preferences.forEach(pref => {
							if (pref.prefId) {
								let cacheKeySet = this.preferenceMentionCache.get(pref.prefId);
								if (!cacheKeySet) {
									cacheKeySet = new Set<string>();
									this.preferenceMentionCache.set(pref.prefId, cacheKeySet);
								}
								cacheKeySet.add(cacheKey);
							}
						});
					}
					return preferences.map(this.handleSideviewmenuPrefs);
				}),
				map(this.preferenceFormatMapper()),
				tap(prefs =>
					Logger.instance.debug('Loaded prefs for filter %o = %o', filter, prefs)
				)
			);

		this.$log.debug('pref cache key: %o', cacheKey);

		if (useCache) {
			// always return a fresh copy of the cached original
			return this.preferencesCache.get(cacheKey, observable).
				pipe(map((
					preferences => JSON.parse(JSON.stringify(preferences))
			)));
		}
		return observable;
	}

	getPreferenceShareGroups(preferenceItem: Preference<any>) {
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId + '/share'
		);
	}

	shareOrUnsharePreferenceItem(preferenceItem: Preference<any>, userRole: IUserRole) {
		this.clearAllCaches();
		let url =
			this.nuclosConfig.getRestHost() +
			'/preferences/' +
			preferenceItem.prefId +
			'/share/' +
			userRole.userRoleId;

		let restCall: Observable<any>;
		if (userRole.shared) {
			restCall = this.http.post(url, null);
		} else {
			restCall = this.http.delete(url);
		}

		return restCall.pipe(
			catchError(e => {
				this.$log.warn('Could not share or unshare preference %o: %o', preferenceItem, e);
				return EMPTY;
			}),
			tap(() => {
				this.browserRefreshService.emitPreferenceChange(preferenceItem);
			})
		);
	}

	/**
	 * removes all shared roles
	 * @param preferenceItem
	 * @return {Promise<T>}
	 */
	unsharePreferenceItem(preferenceItem: Preference<any>): Promise<any> {
		this.clearAllCaches();
		return new Promise(resolve => {
			this.getPreferenceShareGroups(preferenceItem).subscribe((data: any) => {
				let promises: Promise<any>[] = [];
				for (let userRole of data.userRoles) {
					if (userRole.shared) {
						let url =
							this.nuclosConfig.getRestHost() +
							'/preferences/' +
							preferenceItem.prefId +
							'/share/' +
							userRole.userRoleId;
						let promise = this.http.delete(url).toPromise();
						promises.push(promise);
					}
				}
				Promise.all(promises).then(() => {
					this.browserRefreshService.emitPreferenceChange(preferenceItem);
					resolve(void 0);
				});
			});
		});
	}

	updatePreferenceShare(preferenceItem: Preference<any>): Observable<void> {
		this.clearAllCaches();
		return this.http.put<any>(
			this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId + '/share',
			preferenceItem
		);
	}

	/**
	 * reset customized preferences
	 */
	resetCustomizedPreferenceItems(
		types: PreferenceType[],
		eoClassId?: string
	): Observable<boolean> {
		this.clearAllCaches();
		let typesParamString = '?types=' + types.join(',');
		let eoClasssIdParamString = eoClassId ? '&boMetaId=' + eoClassId : '';
		return this.http
			.delete(
				this.nuclosConfig.getRestHost() +
					'/preferences/customized' +
					typesParamString +
					eoClasssIdParamString
			)
			.pipe(
				map(
					// no json response when resetting preference
					() => true
				),
				tap(() => {
					this.browserRefreshService.emitPreferencesChange();
				})
			);
	}

	/**
	 * TODO: Should be moved to SelectableService?!
	 */
	selectPreference(preferenceItem: Preference<any>, layoutId?: string): Observable<void> {
		// If this is a new preference, it must be saved first
		if (!preferenceItem.prefId) {
			return this.savePreferenceItem(preferenceItem).pipe(
				concatMap(() => this.selectPreference(preferenceItem, layoutId))
			);
		}

		this.clearCachesForPref(preferenceItem);
		return this.http.put<any>(this.getSelectionURL(preferenceItem, layoutId), undefined).pipe(
			catchError(e => {
				this.$log.warn('Could not select preference %o: %o', preferenceItem, e);
				return EMPTY;
			})
		);
	}

	/**
	 * TODO: Should be moved to SelectableService?!
	 */
	deselectPreference(preferenceItem: Preference<any>, layoutId?: string): Observable<void> {
		if (!preferenceItem.prefId) {
			this.$log.warn('Can not delete preference without prefId');
			return EMPTY;
		}

		this.clearCachesForPref(preferenceItem);
		return this.http.delete<any>(this.getSelectionURL(preferenceItem, layoutId)).pipe(
			catchError(e => {
				this.$log.warn('Could not deselect preference %o: %o', preferenceItem, e);
				return EMPTY;
			})
		);
	}

	deleteCustomizedPreferenceItem(preferenceItem: Preference<any>): Observable<boolean> {
		if (preferenceItem.customized) {
			return this.deletePreferenceItem(preferenceItem);
		}
		return observableOf(false);
	}

	/**
	 * Resets a customized preference, i.e. deletes the customization and reloads the preference.
	 * The given preferenceItem is updated with the reloaded preference data.
	 */
	resetCustomizedPreferenceItem(preferenceItem: Preference<any>) {
		if (preferenceItem.customized) {
			return this.deletePreferenceItem(preferenceItem).pipe(
				mergeMap(() => this.getPreference(preferenceItem.prefId!))
			);
		}

		return observableThrowError('Preference is not customized - can not reset');
	}

	/**
	 * Saves the given preference item.
	 *
	 * If the item is new (without prefId), it is saved as new preference.
	 * If it is updated, it might be saved as 'customization' of a shared preference.
	 *
	 */
	savePreferenceItem(preferenceItem: Preference<any>): Observable<Preference<any>> {
		if (preferenceItem.name && preferenceItem.name.length > PREFERENCE_NAME_MAX_LENGTH) {
			preferenceItem.name =
				preferenceItem.name.substr(0, PREFERENCE_NAME_MAX_LENGTH - 2) + '..';
		}

		if (preferenceItem.prefId) {
			// update existing preference
			this.clearCachesForPref(preferenceItem);
			return this.http
				.put(
					this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId,
					preferenceItem
				)
				.pipe(
					map(
						// no json response when updating preference
						() => preferenceItem
					),
					tap(pref => {
						pref.dirty = false;
						// update a shared preference? -> set it customized
						if (pref.shared === true) {
							pref.customized = true;
						}
						this.browserRefreshService.emitPreferenceChange(pref);
					})
				);
		} else {
			this.clearAllCaches();
			return this.http
				.post(this.nuclosConfig.getRestHost() + '/preferences', preferenceItem)
				.pipe(
					tap((pref: Preference<any>) => {
						preferenceItem.prefId = pref.prefId;
						preferenceItem.dirty = false;
						this.browserRefreshService.emitPreferenceChange(pref);
					})
				);
		}
	}

	deletePreferenceItem(preferenceItem: Preference<any>): Observable<boolean> {
		this.clearCachesForPref(preferenceItem);
		return this.http
			.delete(this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId)
			.pipe(
				map(
					// no json response when deleting preference
					() => true
				),
				tap(() => {
					this.browserRefreshService.emitPreferenceChange(preferenceItem);
				})
			);
	}

	/**
	 * Delete multipe preference items.
	 * Shared preferences will be unshared.
	 * @param preferenceItems
	 * @return {Observable<R>}
	 */
	deletePreferenceItems(preferenceItems: Preference<any>[]): Observable<boolean> {
		preferenceItems.forEach(p => this.clearCachesForPref(p));
		let prefIds = preferenceItems.map(p => p.prefId);
		return this.http
			.post(this.nuclosConfig.getRestHost() + '/preferences/delete', prefIds)
			.pipe(
				map(
					// no json response when deleting preference
					() => true
				)
			);
	}

	/**
	 * find preferences which are referencing given preferences
	 * @param preferenceItems
	 * @return {Observable<R>}
	 */
	findReferencingPreferences(preferenceItems: Preference<any>[]): Observable<Preference<any>[]> {
		let prefIds = preferenceItems.map(p => p.prefId);
		return this.http.post<Preference<any>[]>(
			this.nuclosConfig.getRestHost() + '/preferences/findReferencingPreferences',
			prefIds
		);
	}

	/**
	 * assign/remove user roles to all preferences depending on userRole.selected flag
	 * not selected user roles will be removed
	 *
	 * TODO: Use Observables instead of Promises
	 */
	shareUserRoles(preferenceItems: Preference<any>[], userRoles: IUserRole[]): Promise<any> {
		this.clearAllCaches();
		return Promise.all([
			...preferenceItems.map(pref => {
				return Promise.all([
					...userRoles.map(userRole => {
						return this.shareOrUnsharePreferenceItem(pref, userRole).toPromise();
					})
				]);
			})
		]);
	}

	/**
	 * convert preferences format from webclient1 to webclient2
	 */
	private preferenceFormatMapper() {
		return (preferences: Preference<any>[]) => {
			return preferences.map((pref: Preference<any>) => {
				if (pref.type === 'searchtemplate') {
					let searchtemplatePref = pref as Preference<SearchtemplatePreferenceContent>;
					if (pref.content.attributes && !pref.content.columns) {
						searchtemplatePref.content.columns = pref.content.attributes;
						this.metaService.getBoMeta(pref.boMetaId).subscribe(meta => {
							searchtemplatePref.content.columns.forEach(column => {
								if (column.enableSearch) {
									column.selected = true;
									column.isValid = true;
								}
								let attrName = this.fqnService.getShortAttributeName(
									pref.boMetaId,
									column.boAttrId
								);
								if (attrName) {
									let attributeLabel = meta.getAttributeLabel(attrName);
									if (attributeLabel) {
										column.name = attributeLabel;
									}
								}
							});
						});
					}
				}
				return pref;
			});
		};
	}

	/*
	 * in preferences only selected columns are stored (without selected flag)
	 */
	private handleSideviewmenuPrefs(
		pref: Preference<PreferenceContent>
	): Preference<PreferenceContent> {
		if (pref.type === 'table' || pref.type === 'subform-table') {
			let sideviewmenuPref = <Preference<SideviewmenuPreferenceContent>>pref;
			if (sideviewmenuPref.content) {
				if (sideviewmenuPref.content.columns) {
					sideviewmenuPref.content.columns.forEach(c => (c.selected = true));
				}
				if (sideviewmenuPref && !sideviewmenuPref.content.sideviewMenuWidth) {
					sideviewmenuPref.content.sideviewMenuWidth = 200;
				}
			}
		}
		return pref;
	}

	private getSelectionURL(preferenceItem: Preference<any>, layoutId?: string) {
		return (
			this.nuclosConfig.getRestHost() +
			'/preferences/' +
			preferenceItem.prefId +
			'/select' +
			(layoutId ? '/' + layoutId : '')
		);
	}

	private clearAllCaches() {
		this.preferencesCache.clear();
		this.preferenceMentionCache.clear();
	}

	private clearCachesForPref(preferenceItem: Preference<any>) {
		if (preferenceItem.prefId) {
			this.preferencesCache.delete(preferenceItem.prefId);
			let cacheKeySet = this.preferenceMentionCache.get(preferenceItem.prefId);
			if (cacheKeySet) {
				cacheKeySet.forEach(cacheKey => this.preferencesCache.delete(cacheKey));
			}
		}
	}
}
