import { Component, Input } from '@angular/core';

@Component({
	selector: 'nuc-printout-datechooser',
	templateUrl: './printout-datechooser.component.html',
	styleUrls: ['../printout-dialog.component.css']
})
export class PrintoutDatechooserComponent {
	@Input() param: { value: string, parameter: any, model?: any };
	@Input() outoutFormatId: string;

	setDateValue(date: string) {
		this.param.value = date;
	}

	getInputStyle(): any {
		return {
			fontSize: '.7rem',
			height: '22px'
		};
	}
}
