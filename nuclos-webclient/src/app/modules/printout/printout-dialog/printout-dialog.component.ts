import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, Input, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { EntityObjectErrorService } from '@modules/entity-object-data/shared/entity-object-error.service';
import { DropdownComponent } from '@modules/ui-components/dropdown/dropdown.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Logger } from '@nuclos/nuclos-addon-api';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { AUTOCOMPLETE_PANEL_SELECTOR } from '../../../layout/web-subform/cell-editors/abstract-lov-editor-component.directive';
import { BusyService } from '../../../shared/service/busy.service';
import { AuthenticationService } from '../../authentication';
import { LovEntry } from '../../entity-object-data/shared/bo-view.model';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { LovDataService } from '../../entity-object-data/shared/lov-data.service';
import { Printout, PrintoutOutputFormat, PrintoutOutputFormatParameter } from '../shared/printout.model';
import { PrintoutService } from '../shared/printout.service';

@Component({
	selector: 'nuc-printout-dialog',
	templateUrl: './printout-dialog.component.html',
	styleUrls: ['./printout-dialog.component.css']
})
export class PrintoutDialogComponent implements OnInit, OnDestroy {
	@ViewChildren('autoComplete') autoComplete: QueryList<DropdownComponent>;

	lovEntries: LovEntry[];
	titleLangKey: string;
	// TODO: Injecting this service via constructor does not work

	@Input() printoutService: PrintoutService;

	@Input() eo?: EntityObject;
	@Input() reportId?: string;
	@Input() printouts?: Printout[];
	@Input() printoutsExecuted?: Printout[] = [];
	@Input() printoutExecuted = false;

	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private activeModal: NgbActiveModal,
		private lovDataService: LovDataService,
		protected elementRef: ElementRef,
		protected busyService: BusyService,
		private authenticationService: AuthenticationService,
		private eoErrorService: EntityObjectErrorService,
		private http: HttpClient,
		private $log: Logger
	) {}

	ngOnInit() {
		if (!this.printouts) {
			if (this.eo) {
				this.printoutService.getPrintoutData(this.eo).pipe(takeUntil(this.unsubscribe$)).subscribe(printouts => {
					this.printouts = printouts;
					if (this.printouts && this.printouts.length === 1 && this.printouts[0].outputFormats.length === 1) {
						// toggle call first: otherwise vlp default value, will not be loaded
						this.toggleOutputFormat(this.printouts[0].outputFormats[0]);
						this.printouts[0].outputFormats[0].selected = true;
					} else if (this.printouts) {
						this.initializeAllPrintoutDefaults(this.printouts, false);
					}
				});
			} else if (this.reportId) {
				this.printoutService.getPrintoutDataByReport(this.reportId).pipe(takeUntil(this.unsubscribe$)).subscribe((printout: Printout) => {
					this.printouts = [];
					this.printouts.push(printout);
					if (this.printouts && this.printouts.length === 1 && this.printouts[0].outputFormats.length === 1) {
						// toggle call first: otherwise vlp default value, will not be loaded
						this.toggleOutputFormat(this.printouts[0].outputFormats[0]);
						this.printouts[0].outputFormats[0].selected = true;
					}
					this.initializeAllPrintoutDefaults(this.printouts, false);
				});
			}
		}
		this.titleLangKey = this.printoutExecuted
			? 'webclient.modal.printouts.result'
			: ('webclient.modal.printouts.title.' + (this.eo ? 'forms' : 'reports'));
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	initializeAllPrintoutDefaults(printouts: Printout[], initializeVlp?: boolean) {
		printouts.forEach(printout => {
			printout.outputFormats.forEach(outputFormat => {
				this.initializeDefaultPrintoutParams(outputFormat, initializeVlp);
			});
		});
	}

	initializeDefaultPrintoutParams(printoutOutputFormat: PrintoutOutputFormat, initializeVlp?: boolean) {
		printoutOutputFormat.parameters?.forEach(parameter => {
			if (parameter.type === 'Boolean') {
				parameter.value = true;
			} else if (parameter.vlp && initializeVlp) {
				this.initializeParameterWithVlpDefault(parameter);
			}
		});
	}

	initializeParameterWithVlpDefault(parameter: PrintoutOutputFormatParameter) {
		this.lovDataService
			.deprecatedLoadLovEntriesForVlp(
				{ reffield: '', vlp: parameter.vlp },
				null,
				undefined,
				this.authenticationService.getCurrentMandatorId()
			)
			.pipe(take(1))
			.subscribe(lovEntries => {
				parameter.value = lovEntries.find(entry => entry.selected);
			});
	}

	autocomplete(param: PrintoutOutputFormatParameter, outputFormat: PrintoutOutputFormat, search?: string, defaultValue?: boolean) {
		this.lovDataService
			.deprecatedLoadLovEntriesForVlp(
				{ reffield: '', vlp: param.vlp },
				null,
				search,
				this.authenticationService.getCurrentMandatorId()
			)
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(lovEntries => {
				this.lovEntries = lovEntries;
				let autoComplete = this.getAutoComplete(param.parameter, outputFormat.outputFormatId);
				if (autoComplete) {
					this.lovEntries.some(entry => {
						if (entry.selected && defaultValue && (!autoComplete?.value || autoComplete.value === autoComplete.emptyValue)) {
							autoComplete!.setValue(entry);
						}
						return !!entry.selected;
					});
				}
			});
	}

	/**
	 * execute button will be enabled when at least 1 printout is selected
	 * and all required fields are filled
	 */
	executeButtonEnabled(printoutsData: Printout[]) {
		let nrOfSelectedPrintouts = 0;
		let requiredFieldsFilled = true;
		if (printoutsData === undefined) {
			return false;
		}
		for (let item of printoutsData) {
			if (item.outputFormats) {
				for (let j = item.outputFormats.length - 1; j >= 0; j--) {
					let outputFormat = item.outputFormats[j];
					if (outputFormat.selected) {
						nrOfSelectedPrintouts++;
						if (outputFormat.parameters) {
							for (let param of outputFormat.parameters) {
								if (!param.nullable && !param.value && param.type !== 'Boolean') {
									requiredFieldsFilled = false;
								}
							}
						}
					}
				}
			}
		}
		return nrOfSelectedPrintouts !== 0 && requiredFieldsFilled;
	}

	executePrintout() {
		if (this.eo) {
			this.busyService
				.busy(this.printoutService.executePrintout(this.eo, this.printouts!))
				.pipe(
					takeUntil(this.unsubscribe$)
				)
				.subscribe(printouts => {
					if (printouts !== undefined) {
						this.printoutExecuted = true;
						let closeImmediatly = true;

						for (let printout of printouts) {
							for (let _printout of this.printouts!) {
								if (_printout.printoutId === printout.printoutId) {
									let executedOutputFormats: PrintoutOutputFormat[] = [];
									for (let outputFormat of printout.outputFormats) {
										for (let _outputFormat of _printout.outputFormats) {
											if (
												_outputFormat.outputFormatId ===
												outputFormat.outputFormatId
											) {
												let newWindow: Window | null = null;
												if (outputFormat.links?.file?.href) {
													newWindow = window.open(outputFormat.links.file.href, '_blank');
												}
												if (!newWindow) {
													closeImmediatly = false;
													executedOutputFormats.push(outputFormat);
												}
											}
										}
									}
									if (executedOutputFormats.length > 0) {
										printout.outputFormats = executedOutputFormats;
										this.printoutsExecuted?.push(printout);
									}
								}
							}
						}
						if (closeImmediatly) {
							this.ok();
						}
					}
				});
		} else if (this.reportId) {
			this.busyService
				.busy(this.printoutService.executePrintoutOfReport(this.reportId, this.printouts!))
				.pipe(
					takeUntil(this.unsubscribe$)
				)
				.subscribe(result => {
					if (result !== undefined) {
						this.printoutExecuted = true;
						let closeImmediatly = true;

						let printouts: any[] = [];
						printouts.push(result);

						for (let printout of printouts) {
							for (let _printout of this.printouts!) {
								if (_printout.printoutId === printout.printoutId) {
									let executedOutputFormats: PrintoutOutputFormat[] = [];
									for (let outputFormat of printout.outputFormats) {
										for (let _outputFormat of _printout.outputFormats) {
											if (
												_outputFormat.outputFormatId ===
												outputFormat.outputFormatId
											) {
												let newWindow: Window | null = null;
												if (outputFormat.links?.file?.href) {
													newWindow = window.open(outputFormat.links.file.href, '_blank');
												}
												if (!newWindow) {
													closeImmediatly = false;
													executedOutputFormats.push(outputFormat);
												}
											}
										}
									}
									if (executedOutputFormats.length > 0) {
										printout.outputFormats = executedOutputFormats;
										this.printoutsExecuted?.push(printout);
									}
								}
							}
						}
						if (closeImmediatly) {
							this.ok();
						}
					}
				});
		}
	}

	toggleOutputFormat(outputFormat: PrintoutOutputFormat) {
		if (!outputFormat.selected && outputFormat.parameters) {
			outputFormat.parameters
				.filter(parameter => !!parameter.vlp)
				.forEach((parameter: PrintoutOutputFormatParameter) => {
					this.autocomplete(parameter, outputFormat, '', true);
					this.autocompletePanel().hide();
				});
		}
	}

	showAllEntries(param: PrintoutOutputFormatParameter, outputFormat: PrintoutOutputFormat) {
		this.getAutoComplete(param.parameter, outputFormat.outputFormatId)?.showAllEntries();
	}

	ok() {
		this.activeModal.close();
	}

	cancel() {
		this.activeModal.dismiss();
	}

	private autocompletePanel() {
		return $(this.elementRef.nativeElement.querySelector(AUTOCOMPLETE_PANEL_SELECTOR));
	}

	private isAutocompletePanelVisible() {
		return this.autocompletePanel().is(':visible');
	}

	private getAutoComplete(parameter: string, outputFormat: string): DropdownComponent | undefined {
		return this.autoComplete.find(autoComplete => autoComplete.id === (outputFormat + ':' + parameter));
	}
}
