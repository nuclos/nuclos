import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../data/classes/subscription-handle.directive';
import { NuclosHotkeysService } from '../../shared/service/nuclos-hotkeys.service';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { PrintoutService } from './shared/printout.service';

@Component({
	selector: 'nuc-printout',
	templateUrl: './printout.component.html',
	styleUrls: ['./printout.component.css']
})
export class PrintoutComponent extends SubscriptionHandleDirective implements OnInit, OnChanges {

	@Input() eo: EntityObject;

	saveInProgress: boolean;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	private eoListener: IEntityObjectEventListener = {
		isSaving: (entityObject: EntityObject, inProgress: boolean) => this.saveInProgress = inProgress
	};

	constructor(
		private printoutService: PrintoutService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService
	) {
		super();
	}

	ngOnInit() {
		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT', keys: 'P', description:
				this.i18n.getI18n('webclient.shortcut.detail.print')
		})
			.pipe(
				takeUntil(this.unsubscribe$)
			).subscribe(() => {
			this.openPrintoutDialog();
		});
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				let previousEo: EntityObject = eoChange.previousValue;
				previousEo.removeListener(this.eoListener);
			}
			this.eo.addListener(this.eoListener);
		}
	}

	/**
	 * open printout dialog
	 */
	openPrintoutDialog() {
		this.printoutService.openPrintoutDialog(this.eo).pipe(take(1)).subscribe();
	}

}
