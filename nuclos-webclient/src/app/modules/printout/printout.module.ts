import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { UiComponentsModule } from '@modules/ui-components/ui-components.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { SharedModule } from '@shared/shared.module';
import { AuthenticationModule } from '../authentication/authentication.module';
import { I18nModule } from '../i18n/i18n.module';
import { PrintoutDatechooserComponent } from './printout-dialog/printout-datechooser/printout-datechooser.component';
import { PrintoutDialogComponent } from './printout-dialog/printout-dialog.component';
import { PrintoutComponent } from './printout.component';
import { PrintoutService } from './shared/printout.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		SharedModule,
		AuthenticationModule,
		I18nModule,
		AutoCompleteModule,
		NgbModule,
		UiComponentsModule,
	],
	declarations: [
		PrintoutComponent,
		PrintoutDialogComponent,
		PrintoutDatechooserComponent
	],
	providers: [
		PrintoutService
	],
	exports: [
		PrintoutComponent,
	]
})
export class PrintoutModule {
}
