import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Command } from '@modules/command/shared/command';
import { CommandService } from '@modules/command/shared/command.service';
import { EntityObjectErrorService } from '@modules/entity-object-data/shared/entity-object-error.service';
import { Report } from '@modules/reports/reports.model';
import { Logger } from '@nuclos/nuclos-addon-api';
import { BusyService } from '@shared/service/busy.service';
import { EMPTY, Observable, of, of as observableOf } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { catchError, take } from 'rxjs/operators';
import { NuclosDialogService } from '../../../core/service/nuclos-dialog.service';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { PrintoutDialogComponent } from '../printout-dialog/printout-dialog.component';
import {
	Printout,
	PrintoutOutputFormat as PrintoutOutputFormatModel,
	PrintoutOutputFormatParameter as PrintoutOutputFormatParameterModel
} from './printout.model';

@Injectable()
export class PrintoutService {

	constructor(
		private modalService: NuclosDialogService,
		private http: HttpClient,
		private nuclosConfig: NuclosConfigService,
		private busyService: BusyService,
		private eoErrorService: EntityObjectErrorService,
		private commandService: CommandService,
		private $log: Logger
	) {
	}

	/**
	 * Displays printout modal dialog.
	 *
	 */
	openPrintoutDialog(eo: EntityObject): Observable<any> {
		return this._openPrintoutDialog(eo, undefined);
	}

	/**
	 * Displays printout modal dialog.
	 *
	 */
	openPrintoutDialogForReport(report: Report): Observable<any> {
		return this._openPrintoutDialog(undefined, report);
	}

	getPrintoutData(eo: EntityObject): Observable<any | undefined> {
		let printoutsLink = eo.getPrintoutURL();
		if (printoutsLink !== undefined) {
			return this.http.get(printoutsLink);
		}
		return observableOf(undefined);
	}

	executePrintout(
		eo: EntityObject,
		printoutsData: Printout[]
	): Observable<any | undefined> {
		// submit selected outputFormat to the server - remove not selected
		let postData = JSON.parse(JSON.stringify(printoutsData));
		for (let item of postData) {
			for (let j = item.outputFormats.length - 1; j >= 0; j--) {
				let outputFormat = item.outputFormats[j];

				// TODO: ui-select component in printout-dialog.html stores the value in parameter.value.name.name
				// maybe there is a more elegant way to configure where to store the model value
				if (outputFormat.parameters) {
					for (let parameter of outputFormat.parameters) {
						if (parameter.type === 'Boolean'
								&& parameter.value && parameter.value.name
								&& parameter.value.name.name === undefined) {
							parameter.value.name.name = false;
						}
						if (parameter.type === 'Number'
							&& parameter.vlp !== undefined
							&& (parameter.value === undefined || parameter.value === '')) {
							parameter.value = {name: null, id: null};
						} else if (parameter.value && parameter.value.name && parameter.value.name.name) {
							parameter.value = parameter.value.name.name;
						}
					}
				}

				if (!outputFormat.selected) {
					item.outputFormats.splice(j, 1);
				}
			}

		}

		let printoutsLink = eo.getPrintoutURL();
		if (printoutsLink) {
			return this.http.post(printoutsLink, postData).pipe(
				catchError(e => {
					// TODO: Show some error message to the user...
					this.$log.warn('Printout failed: %o', e);
					return this.eoErrorService.handleError(e);
				}),
				tap((results: Printout[] | undefined) => {
					if (results) {
						const commandsToExecute = results.reduce<Command<any>[]>((commands , printout) => {
							if (printout.commands) {
								commands = commands.concat(printout.commands!.list);
							}
							return commands;
						}, []);
						this.commandService.executeCommands(commandsToExecute);
					}
				}),
				tap((results: Printout[] | undefined) => {
					if (results && results.some(printout => printout.boChanged) && eo) {
						eo.reload().subscribe();
					}
				})
			);
		}

		return observableOf(undefined);
	}

	getPrintoutDataByReport(reportId: string): Observable<any | undefined> {
		if (reportId) {
			return this.http.get(this.nuclosConfig.getRestHost() + '/reports/' + reportId + '/printout');
		}

		return of(undefined);
	}

	executePrintoutOfReport(reportId: string, printouts: Printout[]): Observable<any | undefined> {
		if (reportId && printouts.length === 1) {
			// submit selected outputFormat to the server - remove not selected
			let outputFormats: any[] = [];
			printouts[0].outputFormats.forEach((sourceFormat: PrintoutOutputFormatModel) => {
				let targetParameters: PrintoutOutputFormatParameter[] = [];
				if (sourceFormat.parameters) {
					sourceFormat.parameters.forEach((sourceParameter: PrintoutOutputFormatParameterModel) => {
						let targetVlp: Vlp | undefined = sourceParameter.vlp ? {
							params: {
								idFieldname: sourceParameter.vlp.params['id-fieldname'],
								fieldname: sourceParameter.vlp.params.fieldname,
								valuelistProvider: sourceParameter.vlp.params.valuelistProvider
							},
							type: sourceParameter.vlp.type,
							value: sourceParameter.vlp.value
						} : undefined;

						if (sourceParameter.type === 'Boolean' && sourceParameter.value === undefined) {
							sourceParameter.value = false;
						}

						let targetParameter: PrintoutOutputFormatParameter = {
							name: sourceParameter.name,
							nullable: sourceParameter.nullable,
							parameter: sourceParameter.parameter,
							type: sourceParameter.type,
							value: sourceParameter.value,
							vlp: targetVlp
						};

						targetParameters.push(targetParameter);
					});
				}

				let targetFormat: PrintoutOutputFormat = {
					fileName: sourceFormat.fileName,
					name: sourceFormat.name,
					outputFormatId: sourceFormat.outputFormatId,
					parameters: targetParameters,
					selected: sourceFormat.selected
				};
				outputFormats.push(targetFormat);
			});

			let printoutData = {
				printoutId: printouts[0].printoutId,
				name: printouts[0].name,
				outputFormats: outputFormats
			};

			for (let j = printoutData.outputFormats.length - 1; j >= 0; j--) {
				let outputFormat = printoutData.outputFormats[j];

				if (outputFormat.parameters) {
					for (let parameter of outputFormat.parameters) {
						if (parameter.value && parameter.value.name) {
							parameter.value = parameter.value.name;
						}
					}
				}

				if (!outputFormat.selected) {
					printoutData.outputFormats.splice(j, 1);
				}
			}

			return this.http.post(this.nuclosConfig.getRestHost() + '/reports/' + reportId + '/execute', printoutData).pipe(
				catchError(e => {
					// TODO: Show some error message to the user...
					this.$log.warn('Printout failed: %o', e);
					return this.eoErrorService.handleError(e);
				})
			);
		}

		return observableOf(undefined);
	}

	private _openPrintoutDialog(eo: EntityObject | undefined, report: Report | undefined): Observable<any> {
		return (eo
			? this.getPrintoutData(eo)
			: ( report
				? this.getPrintoutDataByReport(report.reportId).pipe(take(1), map(printout => {
						let printouts: any[] = [];
						printouts.push(printout);
						return printouts;
					}))
				: of(EMPTY))
		).pipe(take(1), map((printouts: Printout[] | undefined) => {
			let reportId = report?.reportId;
			if ((report?.reportType === 'Single' || eo) &&
				printouts &&
				(printouts.length > 1 ||
					(printouts.length === 1 &&
						(printouts[0].outputFormats.length > 1
							|| (printouts[0].outputFormats[0].parameters && printouts[0].outputFormats[0].parameters.length > 0))))) {
				return this.modalService.custom(
					PrintoutDialogComponent,
					{
						'printoutService': this,
						'eo': eo,
						'reportId': reportId
					}
				).pipe(
					take(1)
				);
			} else if (printouts) {
				if (report && report.reportType !== 'Single') {
					printouts.forEach(
						printout => printout.outputFormats.forEach(
							outputFormat => outputFormat.selected = true
						)
					);
				} else {
					printouts[0].outputFormats[0].selected = true;
				}
				return this.busyService.busy(
					eo
						? this.executePrintout(eo, printouts)
						: ( reportId
							? this.executePrintoutOfReport(reportId, printouts)
							: of(EMPTY))
				).subscribe((results: Printout[] | Printout | undefined) => {
						if (results) {
							let printoutResults: Printout[] = [];
							if (!results[0]) {
								printoutResults.push(results as Printout);
							} else {
								printoutResults = results as Printout[];
							}
							let newWindow: Window | null = null;
							if (printoutResults.length <= 1 && printoutResults[0].outputFormats.length <= 1) {
								newWindow = window.open(printoutResults[0].outputFormats[0].links?.file.href);
							}
							if (!newWindow) {
								return this.modalService.custom(
									PrintoutDialogComponent,
									{
										'printoutService': this,
										'eo': eo,
										'reportId': reportId,
										'printoutsExecuted': printoutResults,
										'printoutExecuted': true
									}
								).pipe(
									take(1)
								);
							}
						}
						return EMPTY;
					});
			} else {
				return EMPTY;
			}
		}));
	}
}
