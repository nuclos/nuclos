import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { GridOptions } from 'ag-grid-community';
import { Subject, Subscription } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { NuclosDialogService } from '../../core/service/nuclos-dialog.service';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import { BrowserRefreshService } from '../../shared/service/browser-refresh.service';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { AuthenticationService } from '../authentication';
import { PrintoutService } from '../printout/shared/printout.service';
import { Report } from './reports.model';
import { ReportsService } from './reports.service';

type SelectionType = 'none' | 'single' | 'multiple';

declare var ace;

@Component({
	selector: 'nuc-reports',
	templateUrl: './reports.component.html',
	styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit, OnDestroy {
	gridOptions = <GridOptions>{};

	selection: SelectionType = 'none';

	reportsItems: Report[] = [];
	selectedReportItem: Report | undefined = undefined;

	routeParamsSubscription: Subscription;

	private unsubscribe$ = new Subject<boolean>();

	private componentAlive = false;

	constructor(
		private nuclosConfig: NuclosConfigService,
		private dialogService: NuclosDialogService,
		private authenticationService: AuthenticationService,
		private i18n: NuclosI18nService,
		private router: Router,
		private route: ActivatedRoute,
		private browserRefreshService: BrowserRefreshService,
		private reportsService: ReportsService,
		private printoutService: PrintoutService
	) { }

	ngOnInit() {
		this.ngOnInitImpl();
		this.componentAlive = true;
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
		this.componentAlive = false;
	}

	gridReady(param): void {
		this.gridOptions.api = param.api;
		this.updateReportItemView();
	}

	selectReportItem(reportItem: Report): void {
		this.router.navigate(['/reports', reportItem.reportId]);
	}

	updateReportItemView(): void {
		if (this.gridOptions.api) {
			this.gridOptions.api.setRowData(this.reportsItems);
		}
	}

	executeReport(selectedReportItem: Report) {
		this.printoutService.openPrintoutDialogForReport(selectedReportItem).pipe(take(1)).subscribe();
	}

	private ngOnInitImpl() {
		this.loadData();
	}

	private loadData() {
		this.gridOptions.context = {
			componentParent: this
		};
		this.gridOptions.immutableData = true;
		this.gridOptions.getRowNodeId = data => data.reportId;
		this.gridOptions.columnDefs = [
			{
				headerName: this.i18n.getI18n('webclient.reports.name'),
				field: 'name',
				sort: 'asc',
				width: 300,
				sortable: true,
				resizable: true,
				filter: true,
				comparator: (valueA, valueB) => {
					if (valueA && valueB) {
						return valueA <= valueB ? -1 : 1;
					}
					return 0;
				}
			},
			{
				headerName: this.i18n.getI18n('webclient.reports.description'),
				field: 'description',
				width: 300,
				sortable: true,
				resizable: true,
				filter: true,
				comparator: (valueA, valueB) => {
					if (valueA && valueB) {
						return valueA <= valueB ? -1 : 1;
					}
					return 0;
				}
			}
		];

		this.gridOptions.rowSelection = 'single';

		this.gridOptions.onSelectionChanged = () => {
			if (this.gridOptions.api) {
				let selectedRows = this.gridOptions.api.getSelectedRows();
				if (selectedRows.length === 1) {
					this.selection = 'single';
					let preferenceItem = selectedRows[0];
					this.selectReportItem(preferenceItem);
				} else {
					this.selection = 'none';
					this.selectedReportItem = undefined;
				}
			}
		};
		if (this.gridOptions.api) {
			this.gridOptions.api.setColumnDefs(this.gridOptions.columnDefs);
			this.gridOptions.api.redrawRows();
		}

		this.reportsService.getReports().pipe(takeUntil(this.unsubscribe$)).subscribe(reports => {
			this.reportsItems = reports;
			this.updateReportItemView();

			if (this.gridOptions.api) {
				this.gridOptions.api.sizeColumnsToFit();
			}

			if (this.routeParamsSubscription) {
				this.routeParamsSubscription.unsubscribe();
			}

			this.routeParamsSubscription = this.route.params.subscribe((params: Params) => {
				let reportId = params['reportId'];
				if (reportId) {
					if (reportId === 'list') {
						return;
					}

					this.selectedReportItem = this.reportsItems.filter(
						report => report.reportId === reportId
					)[0];
					if (this.selectedReportItem) {
						// select row in ag-grid
						setTimeout(() => {
							if (this.gridOptions.api) {
								let firstElement = this.gridOptions.api
									.getRenderedNodes()
									.filter(node => node.data.reportId === reportId)
									.shift();
								if (firstElement) {
									firstElement.setSelected(true);
								}
							}
						});
					}
				}
			});
		});
	}

	private valueGetter(col): Report {
		return col.data;
	}
}
