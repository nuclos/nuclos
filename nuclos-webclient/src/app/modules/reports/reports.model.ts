export class Report {
	reportId: string;
	name: string;
	description: string;
	reportType: string;
	datasource: string;
}
