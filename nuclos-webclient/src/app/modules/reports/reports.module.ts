import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { AgGridModule } from 'ag-grid-angular';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { SharedModule } from '../../shared/shared.module';
import { AuthenticationService } from '../authentication';
import { GridModule } from '../grid/grid.module';
import { I18nModule } from '../i18n/i18n.module';
import { ReportsNavigationGuard } from './reports-navigation-guard';
import { ReportsComponent } from './reports.component';
import { ReportsRoutes } from './reports.routes';
import { ReportsService } from './reports.service';

export function reportsNavGuardFactory(authenticationService: AuthenticationService) {
	return new ReportsNavigationGuard(authenticationService);
}


@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		PrettyJsonModule,

		SharedModule,

		AgGridModule,

		I18nModule,
		GridModule,

		ReportsRoutes,
		NgbPopoverModule
	],
	declarations: [
		ReportsComponent
	],
	providers: [
		ReportsService,
		{
			provide: ReportsNavigationGuard,
			useFactory: reportsNavGuardFactory,
			deps: [
				AuthenticationService
			]
		},
	]
})
export class ReportsModule {
}
