import { RouterModule, Routes } from '@angular/router';
import { ReportsNavigationGuard } from './reports-navigation-guard';
import { ReportsComponent } from './reports.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: '',
		redirectTo: 'list',
		pathMatch: 'full'
	},
	{
		path: ':reportId',
		component: ReportsComponent,
		canActivate: [
			ReportsNavigationGuard
		]
	}
];

export const ReportsRoutes = RouterModule.forChild(ROUTE_CONFIG);
