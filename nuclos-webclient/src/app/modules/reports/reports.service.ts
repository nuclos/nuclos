import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BrowserRefreshService } from '../../shared/service/browser-refresh.service';
import { FqnService } from '../../shared/service/fqn.service';
import { NuclosConfigService } from '../../shared/service/nuclos-config.service';
import { NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { MetaService } from '../entity-object-data/shared/meta.service';
import { Logger } from '../log/shared/logger';
import { Report } from './reports.model';

@Injectable()
export class ReportsService {
	constructor(
		private nuclosConfig: NuclosConfigService,
		private cacheService: NuclosCacheService,
		private http: HttpClient,
		private metaService: MetaService,
		private fqnService: FqnService,
		private browserRefreshService: BrowserRefreshService,
		private $log: Logger
	) {

	}

	getReport(reportId: string): Observable<Report> {
		return this.http
			.get(this.nuclosConfig.getRestHost() + '/reports/' + reportId)
			.pipe(map((data: Report) => data));
	}

	getReports(): Observable<Report[]> {
		return this.http
			.get(this.nuclosConfig.getRestHost() + '/reports')
			.pipe(
				catchError(e => {
					this.$log.warn('Could not get reports', e);
					return EMPTY;
				}),
				map((data: Report[]) => data),
				tap(reports =>
					Logger.instance.debug('Loaded reports', reports)
				)
			);
	}
}
