import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { RuleService } from '@modules/rule/shared/rule.service';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

export abstract class AbstractActionExecutor<T extends RuleAction> {
	constructor(
		protected event: RuleEvent,
		protected action: T
	) {
	}

	abstract execute(eo: EntityObject, dataService?: DataService, eoVLPService?: EntityVLPService, ruleService?: RuleService);
}
