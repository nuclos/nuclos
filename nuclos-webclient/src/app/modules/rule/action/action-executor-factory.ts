import { Logger } from '../../log/shared/logger';
import { AbstractActionExecutor } from './abstract-action-executor';
import { ClearActionExecutor } from './clear-action-executor';
import { EnableActionExecutor } from './enable-action-executor';
import { RefreshValuelistActionExecutor } from './refresh-valuelist-action-executor';
import { ReinitSubformActionExecutor } from './reinit-subform-action-executor';
import { TransferLookedupValueActionExecutor } from './transfer-lookedup-value-action-executor';

export class ActionExecutorFactory {

	/**
	 * Creates a new executor for the given action.
	 *
	 * TODO: This should be done generically instead of via if-else switches.
	 *
	 * @param action
	 * @returns {any}
	 */
	static newExecutor<T extends RuleAction>(event: RuleEvent, action: T): AbstractActionExecutor<RuleAction> {
		let executor;

		if (action['_type'] === 'RuleActionClear') {
			executor = new ClearActionExecutor(event, <RuleActionClear><RuleAction>action);
		} else if (action['_type'] === 'RuleActionEnable') {
			executor = new EnableActionExecutor(event, <RuleActionEnable><RuleAction>action);
		} else if (action['_type'] === 'RuleActionRefreshValuelist') {
			executor = new RefreshValuelistActionExecutor(event, <RuleActionRefreshValuelist><RuleAction>action);
		} else if (action['_type'] === 'RuleActionReinitSubform') {
			executor = new ReinitSubformActionExecutor(event, <RuleActionReinitSubform><RuleAction>action);
		} else if (action['_type'] === 'RuleActionTransferLookedupValue') {
			executor = new TransferLookedupValueActionExecutor(event, <RuleActionTransferLookedupValue><RuleAction>action);
		}

		if (!executor) {
			Logger.instance.warn('No executor for rule action: %o', action);
		}

		return executor;
	}
}
