import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { AbstractActionExecutor } from './abstract-action-executor';

export class ClearActionExecutor extends AbstractActionExecutor<RuleActionClear> {
	constructor(
		event: RuleEvent,
		action: RuleActionClear
	) {
		super(event, action);
	}

	/**
	 * Clears an attribute by settings its value to null.
	 */
	execute(eo: EntityObject) {
		Logger.instance.debug('Clear: %o on %o', this.action, eo);
		eo.clearAttribute(this.action.targetcomponent);
	}
}
