import { DataService } from '@modules/entity-object-data/shared/data.service';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { take } from 'rxjs/operators';
import { EntityObject, SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { AbstractActionExecutor } from './abstract-action-executor';

export class RefreshValuelistActionExecutor extends AbstractActionExecutor<RuleActionRefreshValuelist> {
	constructor(
		event: RuleEvent,
		action: RuleActionRefreshValuelist
	) {
		super(event, action);
	}

	execute(eo: EntityObject, dataService?: DataService, eoVLPService?: EntityVLPService) {
		Logger.instance.debug('Refresh valuelist: %o on %o', this.action, eo);
		this.setVlpParameters(eo, eoVLPService);
		eo.invalidateValuelist(this.action.targetcomponent);
	}

	setVlpParameters(eo: EntityObject, eoVLPService?: EntityVLPService) {
		let entity = this.action.entity || eo.getEntityClassId();
		let parameterName = this.action.parameterForSourcecomponent;

		let sourceEo = eo;

		eo.getMeta().pipe(take(1)).subscribe(meta => {
			if (!(meta.getMetaData().readonly || eo.getEntityClassId().endsWith('DYN'))) {
				// If the sourcecomponent FQN does not match the given EO, then the given EO
				// is a Sub-EO and the parameter must be taken from the root EO or parent EO (in case of Sub-Sub-EO).
				if (!this.event.sourcecomponent.startsWith(eo.getEntityClassId() + '_')) {
					// This is not enough:
					// sourceEo = eo.getRootEo(); <-- Source could also be a higher-level Sub-EO
					// Check every parent:
					while (sourceEo instanceof SubEntityObject && sourceEo !== sourceEo.getParent()) {
						sourceEo = sourceEo.getParent() as EntityObject;
						if (this.event.sourcecomponent.startsWith(sourceEo.getEntityClassId() + '_')) {
							Logger.instance.debug('Using root/parent EO for %o on %o', this.event.sourcecomponent, eo);
							break;
						}
					}
				}

				if (!this.event.sourcecomponent.startsWith(sourceEo.getEntityClassId() + '_')) {
					Logger.instance.warn('Source EO %o does not match source component %o - ignoring', sourceEo, this.event.sourcecomponent);
					return;
				}

				let parameterValue = sourceEo.getAttribute(this.event.sourcecomponent);

				let targetAttributeName = this.action.targetcomponent;
				sourceEo.getMeta().pipe(take(1)).subscribe(entityMeta => {
					let attributeMeta = entityMeta.getAttributeMeta(this.event.sourcecomponent);

					if (!attributeMeta) {
						Logger.instance.error('No attributeMeta for %o on %o', this.event.sourcecomponent, entityMeta);
						return;
					}

					// TODO: It should not be necessary to set a missing reference parameter explicitly to null
					if (attributeMeta && attributeMeta.isReference() && (!parameterValue || !parameterValue.id)) {
						parameterValue = {id: null};
					}

					let vlpContext = eoVLPService?.getVlpContext(eo);

					if (!vlpContext) {
						eoVLPService?.addEntityObjectMap(eo);
						vlpContext = eoVLPService?.getVlpContext(eo);
					}

					if (vlpContext?.setVlpParameter(
						entity,
						targetAttributeName,
						parameterName,
						parameterValue
					)) {
						eo.clearLovEntries(targetAttributeName);
						eo.refreshVlpComponent(targetAttributeName, true);
					}
					eoVLPService?.updateVlpContext(eo, vlpContext);
				});
			}
		});

	}
}
