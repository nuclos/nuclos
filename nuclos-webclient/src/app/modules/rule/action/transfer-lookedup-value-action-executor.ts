import { RuleService } from '@modules/rule/shared/rule.service';
import { switchMap, take } from 'rxjs/operators';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { AbstractActionExecutor } from './abstract-action-executor';

/**
 * Transfers a value from a referenced object to another attribute of the given EO.
 */
export class TransferLookedupValueActionExecutor extends AbstractActionExecutor<RuleActionTransferLookedupValue> {
	constructor(
		event: RuleEvent,
		action: RuleActionTransferLookedupValue
	) {
		super(event, action);
	}

	/**
	 * Transfers the value.
	 * Needs a data service to fetch the referenced EO attribute.
	 */
	execute(eo: EntityObject, dataService: DataService, entityVlpService, ruleService: RuleService) {
		Logger.instance.debug('Transfer looked up value: %o on %o', this.action, eo);
		let source = eo.getAttribute(this.event.sourcecomponent);
		let id = source && source.id;
		if (id) {
			dataService.fetchByAttribute(
				this.action.sourcefield,
				id,
				this.action.targetcomponent,
				eo.getId()
			).pipe(take(1)).subscribe(sourceEO => {
				this.transferValue(eo, sourceEO, ruleService);
			});
		} else {
			eo.clearAttribute(this.action.targetcomponent);
		}
	}

	private transferValue(eo: EntityObject, sourceEO: EntityObject, ruleService: RuleService) {
		let value = sourceEO.getAttribute(this.action.sourcefield);
		eo.getAttributeMeta(this.action.targetcomponent).subscribe(targetMeta => {
			if (targetMeta.isReference()) {
				if (value && value.id) {
					eo.setAttribute(this.action.targetcomponent, value);
				} else if (!value) {
					eo.clearAttribute(this.action.targetcomponent);
				} else {
					this.transferValueWithReferenceLookup(eo, ruleService, value);
				}
			} else {
				eo.setAttribute(this.action.targetcomponent, value);
			}
		});
	}

	private transferValueWithReferenceLookup(eo: EntityObject, ruleService: RuleService, value: any) {
		let prefix = eo.getEntityClassId() + '_';
		if (!this.action.targetcomponent.startsWith(prefix)) {
			Logger.instance.warn(
				'TransferLookedUpValueAction failed: target attribute %o is not an attribute of %o',
				this.action.targetcomponent, eo);
		}
		let shortAttributeName = this.action.targetcomponent.substring(prefix.length);
		ruleService.getLayoutVlps(eo).pipe(
			take(1),
			switchMap(vlps => {
				const vlp = vlps?.vlps.find(entry => entry.attribute === shortAttributeName);
				const webVLP = vlp ? ruleService.toWebVlp(vlp) : undefined;
				return eo.getLovEntries(this.action.targetcomponent, webVLP).pipe(take(1));
			})
		).subscribe(entries => {
			const match = entries.find(entry => entry.name === value);
			if (match) {
				eo.setAttribute(this.action.targetcomponent, match);
			} else {
				Logger.instance.warn(
					'TransferLookedUpValueAction failed: the value %o from source component %o is incompatible with the target %o',
					value, this.action.sourcefield, this.action.targetcomponent);
			}
		});
	}

}
