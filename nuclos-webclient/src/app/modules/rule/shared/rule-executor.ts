import { Injectable, Injector } from '@angular/core';
import { EntityMeta } from '@modules/entity-object-data/shared/bo-view.model';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { RuleService } from '@modules/rule/shared/rule.service';
import { IEntityObjectDependents, IEntityObjectEventListener, ISubEntityObject } from '@nuclos/nuclos-addon-api';
import { IEntityObject } from '@nuclos/nuclos-addon-api/api/entity-object';
import { FqnService } from '@shared/service/fqn.service';
import { take } from 'rxjs/operators';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { ActionExecutorFactory } from '../action/action-executor-factory';
import { RefreshValuelistActionExecutor } from '../action/refresh-valuelist-action-executor';

@Injectable({
	providedIn: 'root'
})
export class RuleExecutor implements IEntityObjectEventListener {

	private static ruleAppliesTo(
		rule: Rule,
		meta: EntityMeta,
		attributeName: string
	): boolean {
		return FqnService.toFqn(meta, attributeName) === rule.event.sourcecomponent;
	}

	private static ruleActionAppliesTo(
		action: RuleAction,
		entityClassId: string
	): boolean {
		return entityClassId === FqnService.getEntityClassId(action.targetcomponent);
	}

	private static getExecutor(rule: Rule, action) {
		return ActionExecutorFactory.newExecutor(rule.event, action);
	}

	private ruleMap: Map<string, Rules> = new Map<string, Rules>();

	private changeList: Array<any> = new Array<any>();

	constructor(
		private injector: Injector,
		private dataService: DataService,
		private entityVLPService: EntityVLPService
	) {
	}

	afterAttributeChange(
		entityObject: IEntityObject,
		attributeName: string,
		oldValue: any,
		newValue: any
	) {
		Logger.instance.debug('attributeChanged: %o, %o, %o, %o', entityObject, attributeName, oldValue, newValue);
		// only remain elements younger than 500ms as this indicates system executions and not user
		this.changeList = [...this.changeList.filter(
			(val: any) => val['changeTime'] > (Date.now() - 50) && val['attributeName'].match(
				new RegExp('.*\\[' + entityObject.getId() + '\\]')
			) !== null
		)];
		this.changeList.push({
			attributeName: `${entityObject.getEntityClassId()}_${attributeName}[${
				entityObject.getId()
			}]`,
			changeTime: Date.now()
		});
		Logger.instance.debug('changeList: %o', this.changeList);
		(entityObject as EntityObject).getMeta().pipe(take(1)).subscribe(meta => {
			this.executeRules(meta, (entityObject as any as EntityObject), attributeName);
		});
	}

	afterDelete(entityObject: IEntityObject) {
		const idString = this.getIdStringFromEO((entityObject as any as EntityObject));
		if (this.ruleMap.has(idString)) {
			this.ruleMap.delete(idString);
		}
	}

	afterReload(entityObject: IEntityObject) {
		this.initializeVlps((entityObject as EntityObject));
	}

	afterCancel(entityObject: IEntityObject) {
		this.initializeVlps((entityObject as EntityObject));
	}

	isSaving(entityObject: IEntityObject, inProgress: boolean) {
		if (!inProgress) {
			// Re-initialize VLP params after (attempted) save because VLP relevant attributes could have been changed by rules
			this.initializeVlps((entityObject as EntityObject));
		}
	}

	initializeVlps(entityObject: EntityObject) {
		for (let rule of this.getRules(entityObject)) {
			for (let action of rule.actions) {
				// TODO: Make this type safe
				if (action['_type'] === 'RuleActionRefreshValuelist') {
					if (RuleExecutor.ruleActionAppliesTo(action, entityObject.getEntityClassId())) {
						let executor = RuleExecutor.getExecutor(rule, action);
						(executor as RefreshValuelistActionExecutor).setVlpParameters(
							entityObject,
							this.entityVLPService
						);
					}
				}
			}
		}

		Logger.instance.debug('VLP parameters initialized on %o: %o', entityObject.getTitle() || entityObject,
			this.entityVLPService.getVlpContext(entityObject));
	}

	executeRules(
		meta: EntityMeta,
		entityObject: EntityObject,
		attributeName: string
	) {
		for (let rule of this.getRules(entityObject)) {
			if (RuleExecutor.ruleAppliesTo(rule, meta, attributeName)) {
				this.executeActions(rule, entityObject);
			}
		}

		// execute rules for dependents also
		entityObject.getAllDependents().forEach((dependent: IEntityObjectDependents) => {
			dependent.current()?.forEach((subEO: ISubEntityObject) => {
				for (let dependentsRule of this.getRules(subEO as any)) {
					if (RuleExecutor.ruleAppliesTo(dependentsRule, meta, attributeName)) {
						this.executeActions(dependentsRule, subEO as any);
					}
				}
			});
		});
	}

	addOrUpdateRulesForEO(eo: EntityObject, rules: Rules): void {
		this.ruleMap.set(this.getIdStringFromEO(eo), rules);
		Logger.instance.debug('addOrUpdateRulesForEO %o: %o', eo, rules);
	}

	/**
	 * Executes all actions of the given Rule on the given EO.
	 */
	private executeActions(rule: Rule, eo: EntityObject) {
		for (let action of rule.actions) {
			/**
			 * Rules can also apply in the other direction and the read dependents needs to be updated
			 */
			if (!action.targetcomponent.startsWith(eo.getEntityClassId() + '_')) {
				const dependents = eo.getAllDependents();
				const filteredDependentKeys =
					Array.from(dependents.keys())
						.find((key: string) => key.startsWith(action.targetcomponent.substr(0, action.targetcomponent.lastIndexOf('_')) + '_'));
				if (filteredDependentKeys) {
					const filteredDependents = eo.getDependents(filteredDependentKeys).current();
					filteredDependents?.forEach((subEo) => {
						this.executeAction(rule, action, subEo as unknown as EntityObject);
					});
				}
			}
			this.executeAction(rule, action, eo);
		}
	}

	private executeAction(rule: Rule, action, eo: EntityObject) {
		let executor = RuleExecutor.getExecutor(rule, action);
		executor.execute(eo, this.dataService, this.entityVLPService, this.injector.get(RuleService));
	}

	private getIdStringFromEO(eo: EntityObject): string {
		return eo.getEntityClassId() + '_' + (eo.isNew() ? 'new' : eo.getId());
	}

	private getRules(eo: EntityObject): Rule[] {
		const idString = this.getIdStringFromEO(eo);
		const rules: Rules | undefined = this.ruleMap.get(idString);
		if (rules === undefined || rules.rules === undefined) {
			return [];
		}
		return rules.rules.map((rule) => {
			// filter out system indicated clearing, so we do not clear our previously set
			let ruleUpdated = {...rule};
			ruleUpdated.actions = ruleUpdated.actions.filter(
					(action) => {
						if (action['_type'] === 'RuleActionClear') {
							return this.changeList.find(
								(change) => change['attributeName'].match(
									new RegExp(action.targetcomponent + '\\[.*\\]')
								) !== null
							) === undefined;
						}
						return true;
					}
			);
			return ruleUpdated;
		});
	}
}

