import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EntityVLPService } from '@modules/entity-object-data/shared/entity-v-l-p.service';
import { Observable, of } from 'rxjs';

import { map, mergeMap, publishReplay, refCount, take } from 'rxjs/operators';
import { LayoutService } from '../../../layout/shared/layout.service';
import { FqnService } from '../../../shared/service/fqn.service';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { RuleExecutor } from './rule-executor';

@Injectable({
	providedIn: 'root'
})
export class RuleService {
	private rulesCache: Map<string, Observable<Rules>>;
	private vlpsCache: Map<string, Observable<ValueListProviders>>;

	constructor(
		private layoutService: LayoutService,
		private dataService: DataService,
		private eoVLPService: EntityVLPService,
		private fqnService: FqnService,
		private ruleExecutor: RuleExecutor,
		private http: HttpClient,
		private $log: Logger,
	) {
		this.rulesCache = new Map<string, Observable<Rules>>();
		this.vlpsCache = new Map<string, Observable<ValueListProviders>>();
	}

	/**
	 * Fetches rules for the current layout of the given EO.
	 *
	 * TODO: Rules should later be defined directly on the Entity class.
	 */
	getLayoutRules(eo: EntityObject): Observable<Rules | undefined> {
		return this.layoutService.getWebLayoutURLDynamically(eo).pipe(
			mergeMap(url => {
					if (!url) {
						this.$log.warn('No layout defined for %o - could not fetch layout rules', eo);
						return of(undefined);
					}

					if (!this.rulesCache.has(url)) {
						this.rulesCache.set(url, this.http
							.get<Rules>(url.replace('/calculated', '/rules'))    // TODO: Avoid URL manipulations
							.pipe(
								publishReplay(1),
								refCount(),
							)
						);
					}

					return this.rulesCache.get(url)!;
				}
			));
	}

	getLayoutVlps(eo: EntityObject): Observable<ValueListProviders | undefined> {
		return this.layoutService.getWebLayoutURLDynamically(eo).pipe(
			mergeMap(url => {
					if (!url) {
						this.$log.warn('No layout defined for %o - could not fetch layout rules', eo);
						return of(undefined);
					}

					if (!this.vlpsCache.has(url)) {
						this.vlpsCache.set(url, this.http
							.get<ValueListProviders>(url.replace('/calculated', '/vlps'))    // TODO: Avoid URL manipulations
							.pipe(
								publishReplay(1),
								refCount(),
							)
						);
					}

					return this.vlpsCache.get(url)!;
				}
			));
	}

	toWebVlp(vlp: ValueListProvider): WebValuelistProvider {
		return {
			name: vlp.name,
			type: vlp.type,
			value: vlp.value,
			idFieldname: vlp.idFieldname,
			fieldname: vlp.fieldname,
			parameter: vlp.parameters
		};
	}

	/**
	 * Fetches the rules of the given EO and subscribes a new RuleExecutor to it.
	 *
	 * If a parent EO is given, the Rules are fetched from its layout, as the eo is a then
	 * a subform EO and subform rules are defined in the parent layout.
	 */
	updateRuleExecutor(eo: EntityObject, parentEO?: EntityObject): Observable<EntityObject> {
		let layoutEO = parentEO || eo;
		return this.getLayoutRules(layoutEO).pipe(map(
			(rules: Rules) => {
				if (rules) {
					eo.removeListenersByType(RuleExecutor);
					eo.addListener(this.ruleExecutor);
					this.ruleExecutor.addOrUpdateRulesForEO(eo, rules);
					this.ruleExecutor.initializeVlps(eo);
					this.getLayoutVlps(layoutEO).subscribe(
						vlps => {
							let targetComponents: string[] = [];
							if (rules.rules) {
								rules.rules.forEach(rule => {
									if (rule.actions) {
										rule.actions.forEach(action => {
											const target: string = (action as RuleActionRefreshValuelist).targetcomponent;
											if (action['_type'] === 'RuleActionRefreshValuelist' &&
												targetComponents.find(targetFromList => targetFromList === target) === undefined) {
												targetComponents.push(target);
											}
										});
									}
								});
							}
							if (vlps?.vlps && targetComponents.length > 0) {
								vlps?.vlps.forEach(vlp => {
									targetComponents.forEach(attribute => {
										if (this.fqnService.getShortAttributeNameFailsafe(
												eo.getEntityClassId(), attribute) === vlp.attribute
												&& vlp.defaultFieldname !== undefined
												&& vlp.defaultFieldname !== '') {
											let webVLP: WebValuelistProvider = this.toWebVlp(vlp);
											eo.refreshVlp(attribute, webVLP).pipe(take(1)).subscribe();
										}
									});
								});
							}
						});
				}
				return eo;
			}
		));
	}
}
