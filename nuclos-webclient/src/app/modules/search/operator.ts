export interface Operator {
	operator: string;
	isUnary: boolean;
	is2ValueComparison: boolean;
	label: string;
	label2ValueComparison?: string;
	oppositeOpFor2ValueComparison?: string;
	combiningFor2ValueComparison?: string;
}
