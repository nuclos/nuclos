import { Component, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TaskService } from '../../../core/service/task.service';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { EntityObjectSearchfilterService } from '../../entity-object-data/shared/entity-object-searchfilter.service';
import {
	Preference,
	SearchtemplatePreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { SearchService } from '../shared/search.service';

@Component({
	selector: 'nuc-search-bar',
	templateUrl: './search-bar.component.html',
	styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnDestroy {
	@Input() meta: EntityMeta;

	eo: IEntityObject | undefined;

	mouseOverTaskListName = false;

	taskListEntityMeta?: EntityMeta;

	private favoriteSearchfilters: Preference<SearchtemplatePreferenceContent>[] = [];

	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private eoEventService: EntityObjectEventService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private searchService: SearchService,
		private route: ActivatedRoute,
		private router: Router,
		private taskService: TaskService,
		private prefService: PreferencesService,
	) {
		this.eoEventService.observeSelectedEo().pipe(takeUntil(this.unsubscribe$)).subscribe(eo => (this.eo = eo));

		this.eoSearchfilterService.observeAllSearchfilters().pipe(takeUntil(this.unsubscribe$)).subscribe(allSearchfilters => {
			this.setFavoriteSearchfilters(allSearchfilters);
		});

		this.route.queryParams.subscribe(params => {
			let taskListId = params['taskListId'];
			let searchFilterId = params['searchFilterId'];
			let processMetaId = params['processMetaId'];
			if (taskListId) {
				this.taskService.getTaskListDefinitionByTaskMetaId(taskListId).subscribe(
					entityMeta => (this.taskListEntityMeta = entityMeta)
				);
			} else {
				this.taskListEntityMeta = undefined;
			}

			if (searchFilterId) {
				prefService.getPreference(searchFilterId).subscribe(pref => {
					this.selectSearchfilter(pref);
				});
			}
		});
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	getTaskListName(): string | undefined {
		if (this.taskListEntityMeta) {
			return this.taskListEntityMeta.getEntityName();
		}
		return undefined;
	}

	getFavoriteSearchfilters(): Preference<SearchtemplatePreferenceContent>[] {
		return this.favoriteSearchfilters;
	}

	hasFavoriteSearchfilters() {
		return this.favoriteSearchfilters.length > 0;
	}

	isSelectedSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		/*
		let selected = this.eoSearchfilterService.getSelectedSearchfilter();
		return selected === searchfilter;
		 */
		return searchfilter.selected === true;
	}

	/**
	 * for tests
	 */
	getSearchfilterElementClass(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		if (searchfilter.name) {
			return (
				'searchfilter-favorite-button-' +
				searchfilter.name.toLowerCase().replace(/[^a-z0-9-_]/g, '-')
			);
		} else {
			return 'searchfilter-favorite-button-no-name';
		}
	}

	selectSearchfilter(searchfilter) {
		if (this.isSelectedSearchfilter(searchfilter)) {
			// unselect -> find "My search" and select it
			this.selectTempFilter();
		} else {
			this.eoSearchfilterService.selectSearchfilter(searchfilter);
		}
	}

	removeTaskListFilter() {
		this.router.navigate(
			[],
			{
				relativeTo: this.route,
				queryParams: { taskListId: undefined },
				queryParamsHandling: 'merge'
			});
		this.taskListEntityMeta = undefined;
		this.executeSearch();
	}

	private selectTempFilter(): Preference<SearchtemplatePreferenceContent> | undefined {
		let tempFilter = this.eoSearchfilterService
			.getAllSearchfilters()
			.find(sf => sf.content.isTemp === true);
		if (tempFilter) {
			this.eoSearchfilterService.selectSearchfilter(tempFilter);
		}

		return tempFilter;
	}

	private executeSearch() {
		this.searchService.initiateDataLoad(this.meta.getEntityClassId());
		this.scrollSidebarToTop();
	}

	/**
	 * @deprecated: TODO: Sidebar should do this by itself, instead of being manipulated here
	 */
	private scrollSidebarToTop(): void {
		$('#sidebar-list-container').scrollTop(0);
	}

	private setFavoriteSearchfilters(
		allSearchfilters: Preference<SearchtemplatePreferenceContent>[]
	) {
		this.favoriteSearchfilters = EntityObjectSearchfilterService.sortSearchfilters(
			allSearchfilters.filter(searchfilter => searchfilter.content.showAsFavorite === true),
			true
		);
	}
}
