import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../../data/classes/subscription-handle.directive';
import { NuclosHotkeysService } from '../../../shared/service/nuclos-hotkeys.service';
import { EntityObjectSearchfilterService } from '../../entity-object-data/shared/entity-object-searchfilter.service';
import { EntityObjectPreferenceService } from '../../entity-object/shared/entity-object-preference.service';
import { SideviewmenuService } from '../../entity-object/shared/sideviewmenu.service';
import { Preference, SearchtemplatePreferenceContent } from '../../preferences/preferences.model';
import { SearchEditorComponent } from '../search-editor/search-editor.component';

@Component({
	selector: 'nuc-search-editor-button',
	templateUrl: './search-editor-button.component.html',
	styleUrls: ['./search-editor-button.component.css']
})
export class SearchEditorButtonComponent extends SubscriptionHandleDirective implements OnInit {

	@ViewChild('searchEditorButtonContainer') searchEditorButtonContainerEl: ElementRef;

	constructor(
		private sideviewmenuService: SideviewmenuService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService
	) {
		super();
	}

	ngOnInit() {
		this.nuclosHotkeysService.addShortcut({
			keys: 'f',
			modifier: 'ALT_SHIFT',
			description: this.i18n.getI18n('webclient.shortcut.searchPanel')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.showFixedSearchEditor(!this.isEditorFixedAndShowing());
		});
	}

	toggleSearchEditor() {
		if (this.isEditorFixed()) {
			this.setEditorFixedAndShowing(!this.isEditorFixedAndShowing());
			this.setPopoverShowing(false);
		} else {
			this.setPopoverShowing(!this.isPopoverShowing());
		}
	}

	adjustPopover() {
		SearchEditorComponent.adjustPopover();
	}

	isSelectedSearchfilterEnabled(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			for (let searchElement of searchfilter.content.columns) {
				if (searchElement.operator && searchElement.enableSearch !== false && searchElement.selected !== false) {
					return true;
				}
			}
		}
		return false;
	}

	getSelectedSearchfilter(): Preference<SearchtemplatePreferenceContent> | undefined {
		return this.eoSearchfilterService.getSelectedSearchfilter();
	}

	getSideviewmenuPref() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$.getValue();
	}

	isPopoverShowing(): boolean {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			return pref.content.searchEditorPopoverShowing;
		}
		return false;
	}

	setPopoverShowing(popoverShowing: boolean) {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			pref.content.searchEditorPopoverShowing = popoverShowing;
		}
		if (popoverShowing) {
			this.adjustPopover();
		}
	}

	isEditorFixed(): boolean {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			return pref.content.sideviewSearchEditorFixed;
		}
		return false;
	}

	isEditorFixedAndShowing() {
		let pref = this.getSideviewmenuPref();
		if (pref && pref.content.sideviewSearchEditorFixed) {
			return pref.content.sideviewSearchEditorFixedAndShowing;
		}
		return false;
	}

	clickOutside($event: EventTarget | null) {
		if ($event && $event !== this.searchEditorButtonContainerEl.nativeElement
			&& !$($event).hasClass('dd-item') // item from selection
			&& !$($event).hasClass('ui-autocomplete-list-item') // selection for attributes
			&& !this.isDatePicker($event) // date picker navigation
			&& !this.isMultiSelectItemParent($event)
			&& (
				!$($event).hasClass('icon-label') &&
				!$($event).hasClass('icon-preview') &&
				!$($event).hasClass('material-icons')
			) // icon-picker in search editor favorite
		) {
			this.setPopoverShowing(false);
		}
	}

	isDatePicker(event: EventTarget): boolean {
		return $(event).parents().is('ngb-datepicker');
	}

	isMultiSelectItemParent(event: EventTarget): boolean {
		const $target = $(event);
		return this.hasMultiSelectItemParent($target);
	}

	hasMultiSelectItemParent($element: JQuery, levels = 4): boolean {
		for (let i = 0; i <= levels; i++) {
			if ($element.hasClass('p-multiselect-panel') || $element.hasClass('p-multiselect-item')) {
				return true;
			}
			$element = $element.parent();
			if ($element.length === 0) {
				break;
			}
		}
		return false;
	}

	private setEditorFixedAndShowing(value: boolean) {
		let pref = this.getSideviewmenuPref();
		if (pref && pref.content.sideviewSearchEditorFixed) {
			pref.content.sideviewSearchEditorFixedAndShowing = value;
			this.saveSideviewmenuPreference();
		}
	}

	private saveSideviewmenuPreference(): void {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			this.sideviewmenuService.saveSideviewmenuPreference(pref).pipe(take(1)).subscribe();
		}
	}

	private showFixedSearchEditor(value: boolean) {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			pref.content.sideviewSearchEditorFixed = value;
			pref.content.sideviewSearchEditorFixedAndShowing = value;
			this.saveSideviewmenuPreference();
		}
	}
}
