import { EventEmitter, Input, Output, Directive } from '@angular/core';

@Directive()
export class AbstractSearchItemDirective {
	@Input() model: any;
	@Input() disabled;

	@Output() modelChanged = new EventEmitter();
}
