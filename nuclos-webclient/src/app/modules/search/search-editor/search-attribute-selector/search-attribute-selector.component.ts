import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../../../data/classes/subscription-handle.directive';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { EntityMeta } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectSearchfilterService } from '../../../entity-object-data/shared/entity-object-searchfilter.service';
import { MetaService } from '../../../entity-object-data/shared/meta.service';
import {
	Preference,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent
} from '../../../preferences/preferences.model';
import { Selectable } from '../../../two-side-multi-select/two-side-multi-select.model';
import { DropdownComponent } from '../../../ui-components/dropdown/dropdown.component';
import { SearchfilterService } from '../../shared/searchfilter.service';

export class EntityMetaRenderer {
	meta: EntityMeta;
	ec: EntityContext | undefined;
	name = '';
	mainEntity = false;

	constructor(meta: EntityMeta, ec: EntityContext | undefined, mainEntity: boolean) {
		this.meta = meta;
		this.ec = ec;
		this.mainEntity = mainEntity;
		let entityName = meta.getEntityName();
		if (entityName !== undefined) {
			this.name = entityName;
		}
		if (this.name === undefined) {
			this.name = meta.getEntityClassId();
		}
		if (mainEntity) {
			this.name = '< ' + this.name + ' >';
		}
	}
}

@Component({
	selector: 'nuc-search-attribute-selector',
	templateUrl: './search-attribute-selector.component.html',
	styleUrls: ['./search-attribute-selector.component.css']
})
export class SearchAttributeSelectorComponent extends SubscriptionHandleDirective implements OnInit {
	_meta: EntityMeta;
	@Input() searchfilter: Preference<SearchtemplatePreferenceContent>;

	@Output() handleChangeOfSelectedSearchfilter = new EventEmitter();
	@ViewChild('attributeSelectionInput') attributeSelectionInput: DropdownComponent;
	@ViewChild('attributeEntitiesInput') attributeEntitiesInput: DropdownComponent;

	subformSearchEnabled = false;

	filter = '';
	selectedValue;
	availableAttributes: SearchtemplateAttribute[];
	selectedAttributes: Selectable[];

	filterEntity = '';
	selectedEntity;
	mainEntity;
	allDependentEntities: EntityMetaRenderer[];
	availableEntities: EntityMetaRenderer[];

	constructor(
		private searchfilterService: SearchfilterService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private metaService: MetaService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService
	) {
		super();
	}

	get meta(): EntityMeta {
		return this._meta;
	}

	@Input()
	set meta(meta: EntityMeta) {
		this._meta = meta;
		this.updateAttributes('');
		this.loadEntities('');
	}

	ngOnInit() {
		this.updateAttributes('');
		this.loadEntities('');
		if (this.searchfilter) {
			let withDependentColumns = this.searchfilter.content.columns.find(
				c => c.dependentBoMetaId !== undefined
			);
			if (withDependentColumns) {
				this.subformSearchEnabled = true;
			}
		}

		this.nuclosHotkeysService.addShortcut({
			keys: 'g',
			modifier: 'ALT_SHIFT',
			description: this.i18n.getI18n('webclient.shortcut.search.focusAttribute')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.attributeSelectionInput.focusInput();
		});
	}

	selectAttribute(attribute: Selectable) {
		let meta = this._meta;
		let dependentBoMetaId;
		if (this.selectedEntity !== undefined) {
			meta = this.selectedEntity.meta;
			dependentBoMetaId = meta.getEntityClassId();
			attribute.name = meta.getEntityName() + '.' + attribute.name;
		}
		if (this.searchfilter.content.expertWhereCondition) {
			let newAttribute = attribute as SearchtemplateAttribute;
			this.searchfilter.content.expertWhereCondition.clause += newAttribute.boAttrId + ' ';
		} else {
			let fromSearchfilter = this.findAttributeInSearchfilter(attribute.name);
			if (fromSearchfilter) {
				fromSearchfilter.selected = true;
				fromSearchfilter.enableSearch = true;
				fromSearchfilter.focusInput = true;
				let index = this.searchfilter.content.columns.indexOf(fromSearchfilter, 0);
				if (index > -1) {
					this.searchfilter.content.columns.splice(index, 1);
				}
				this.searchfilter.content.columns.push(fromSearchfilter);
			} else {
				attribute.selected = true;
				let newAttribute = attribute as SearchtemplateAttribute;
				newAttribute.enableSearch = true;
				newAttribute.focusInput = true;
				newAttribute.dependentBoMetaId = dependentBoMetaId;
				let attributeMeta = meta.getAttributeMetaByFqn(newAttribute.boAttrId);
				if (attributeMeta && attributeMeta.isBoolean()) {
					newAttribute.value = 'false';
				}
				newAttribute.reference = attributeMeta?.isReference();
				this.searchfilter.content.columns.push(newAttribute);
			}
		}

		this.filter = '';
		this.selectedValue = { name: ''};
		this.attributeSelectionInput.writeValue('');
		this.attributeSelectionInput.autoComplete.highlightOption = undefined;

		this.eoSearchfilterService.updateColumns(this.searchfilter);
		if (this.handleChangeOfSelectedSearchfilter) {
			this.handleChangeOfSelectedSearchfilter.emit();
		}
	}

	loadAttributes(query: string) {
		this.updateAttributes(query);
	}

	modelChange($event) {
		if (typeof $event === 'string') {
			this.filter = $event;
		}
	}

	enableSubformSearch() {
		this.subformSearchEnabled = true;
		this.updateEntitiesIfNecessary();
	}

	hasDependentEntities(): boolean {
		return this.allDependentEntities?.length > 0;
	}

	updateEntitiesIfNecessary() {
		if (this._meta) {
			if (
				this.mainEntity === undefined ||
				this.mainEntity !== this._meta.getEntityClassId()
			) {
				let entityContexts = this._meta.getEntityContexts();
				if (entityContexts) {
					let entityClassIds: Set<string> = new Set();
					entityContexts.forEach(ec => entityClassIds.add(ec.dependentEntityClassId));
					this.metaService.getEntityMetas(entityClassIds).pipe(take(1)).subscribe(mapMeta => {
						let allDeps: EntityMetaRenderer[] = [];
						if (entityContexts) {
							entityContexts.forEach(ec => {
								let meta = mapMeta.get(ec.dependentEntityClassId);
								if (meta) {
									allDeps.push(new EntityMetaRenderer(meta, ec, false));
								}
							});
							allDeps.sort((a, b) => {
								if (a.mainEntity && !b.mainEntity) {
									return -1;
								} else if (!a.mainEntity && b.mainEntity) {
									return 1;
								}
								return a.name
									.toLocaleString()
									.localeCompare(b.name.toLocaleString());
							});
						}
						this.allDependentEntities = allDeps;
						this.filterAvailableEntities();
					});
				}
			}
			this.mainEntity = this._meta.getEntityClassId();
		}
	}

	selectEntity(entityRenderer: EntityMetaRenderer) {
		if (entityRenderer.mainEntity) {
			this.selectedEntity = undefined;
		} else {
			this.selectedEntity = entityRenderer;
		}
		this.updateAttributes(this.filter);
	}

	loadEntities(query: string) {
		this.filterEntity = query;
		this.updateEntitiesIfNecessary();
		this.filterAvailableEntities();
		this.attributeEntitiesInput?.updateAutoCompleteMessage(
			this.availableEntities?.length === 0 ? this.i18n.getI18n('common.empty') : '',
			false
		);
	}

	modelChangeEntity($event) {
		if (typeof $event === 'string') {
			this.loadEntities($event);
		}
	}

	private updateAttributes(query: string) {
		this.updateSelectedAttributes();
		this.updateAvailableAttributes(query);
		this.attributeSelectionInput?.updateAutoCompleteMessage(
			this.availableAttributes?.length === 0 ? this.i18n.getI18n('common.empty') : '',
			false
		);
	}

	private updateAvailableAttributes(query: string) {
		let meta = this._meta;
		let dependentEntityFieldId;
		if (this.selectedEntity !== undefined && this.selectedEntity.meta !== undefined) {
			meta = this.selectedEntity.meta;
			dependentEntityFieldId = this.selectedEntity.ec.dependentEntityFieldId;
		}
		if (meta) {
			let emptySearchPref = this.searchfilterService.createSearchfilter(meta);
			if (!this.searchfilter) {
				this.searchfilter = emptySearchPref;
			}

			this.availableAttributes = [...(emptySearchPref.content
				.columns as SearchtemplateAttribute[]).filter(
				attribute =>
					attribute.name &&
					attribute.name.toLowerCase().indexOf(query.toLowerCase()) >= 0 &&
					!this.isSelected(attribute.boAttrId) &&
					dependentEntityFieldId !== attribute.boAttrId
			)];
			this.availableAttributes?.sort((a, b) => {
				if (a.name && b.name) {
					if (a.system !== true && b.system === true) {
						return -1;
					}
					if (a.system === true && b.system !== true) {
						return 1;
					}
					return a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 1;
				}
				return 0;
			});
		} else {
			this.availableAttributes = [];
		}
	}

	private updateSelectedAttributes() {
		if (this.searchfilter) {
			this.selectedAttributes = [...(this.searchfilter.content.columns as Selectable[]).filter(
				it => it.selected
			)];
		} else {
			this.selectedAttributes = [];
		}
	}

	private isSelected(boAttriId: string) {
		return (this.selectedAttributes as SearchtemplateAttribute[]).find(
			it => it.boAttrId === boAttriId
		);
	}

	private findAttributeInSearchfilter(name: string) {
		let result;
		if (this.searchfilter) {
			result = (this.searchfilter.content.columns as Selectable[]).find(
				it => it.name === name
			);
		}
		return result;
	}

	private filterAvailableEntities() {
		let filter = this.filterEntity;
		let result: EntityMetaRenderer[] = [];
		let all = this.allDependentEntities;
		if (this._meta) {
			result.push(new EntityMetaRenderer(this._meta, undefined, true));
		}
		all?.filter(
			er => '' === filter || er.name.toLowerCase().indexOf(filter.toLowerCase()) >= 0
		).forEach(er => result.push(er));
		this.availableEntities = result;
	}
}
