import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../../core/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../../../data/classes/subscription-handle.directive';
import { ObjectUtils } from '../../../../shared/object-utils';
import { NuclosHotkeysService } from '../../../../shared/service/nuclos-hotkeys.service';
import { EntityMeta } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectSearchfilterService } from '../../../entity-object-data/shared/entity-object-searchfilter.service';
import { Preference, SearchtemplatePreferenceContent } from '../../../preferences/preferences.model';

@Component({
	selector: 'nuc-search-filter-selector',
	templateUrl: './search-filter-selector.component.html',
	styleUrls: ['./search-filter-selector.component.css']
})
export class SearchFilterSelectorComponent extends SubscriptionHandleDirective implements OnInit {

	@Input() meta: EntityMeta;

	@Input() searchfilter: Preference<SearchtemplatePreferenceContent> | undefined;

	@Input() disabled = false;

	@ViewChild('searchfilterSelectorDropdownMenu', {static: true}) searchfilterSelectorDropdownMenu: ElementRef;

	popoverShowing = false;

	allSearchfilters: Preference<SearchtemplatePreferenceContent>[] = [];

	constructor(
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private i18n: NuclosI18nService,
		private nuclosHotkeysService: NuclosHotkeysService
	) {
		super();
	}

	ngOnInit() {
		this.eoSearchfilterService.observeAllSearchfilters().pipe(takeUntil(this.unsubscribe$)).subscribe(
			allSearchfilters => {
				if (allSearchfilters) {
					this.allSearchfilters = allSearchfilters;
				} else {
					this.allSearchfilters = [];
				}
			}
		);
		document.querySelector('body')!.appendChild(this.searchfilterSelectorDropdownMenu.nativeElement);

		this.nuclosHotkeysService.addShortcut({
			keys: 't',
			modifier: 'ALT_SHIFT',
			description: this.i18n.getI18n('webclient.shortcut.search.toggleFilter')
		}).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
			this.selectNextSearchFilter();
		});
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
		document.querySelector('body')!.removeChild(this.searchfilterSelectorDropdownMenu.nativeElement);
	}

	getCurrentSearchfilterName() {
		if (this.searchfilter) {
			return this.searchfilter.name;
		}
		return '';
	}

	togglePopover() {
		this.setPopoverShowing(!this.popoverShowing);
	}

	setPopoverShowing(popoverShowing: boolean) {
		if (this.disabled) {
			return;
		}
		this.popoverShowing = popoverShowing;
		if (popoverShowing) {
			this.adjustPopover();
		}
		let menu = $('#searchfilter-selector-dropdown-menu');
		menu.css('display', popoverShowing ? 'block' : 'none');
	}

	adjustPopover() {
		let dropdown = $('#searchfilter-selector-dropdown');
		if (dropdown.offset() !== undefined) {
			let menu = $('#searchfilter-selector-dropdown-menu');
			menu.css('top', (dropdown.offset().top + dropdown.outerHeight() - 6) + 'px');
			menu.css('left', (dropdown.offset().left) + 'px');
		}
	}

	selectSearchfilter(searchfilter) {
		this.eoSearchfilterService.selectSearchfilter(searchfilter);
	}

	private selectNextSearchFilter() {
		if (this.allSearchfilters.length > 0) {
			const currentIndexOfFilter = this.allSearchfilters.findIndex(sf => ObjectUtils.isEqual(sf, this.searchfilter));
			if ((currentIndexOfFilter + 1) > (this.allSearchfilters.length - 1)) {
				// last index
				this.selectSearchfilter(this.allSearchfilters[0]);
			} else {
				this.selectSearchfilter(this.allSearchfilters[currentIndexOfFilter + 1]);
			}
		}
	}
}
