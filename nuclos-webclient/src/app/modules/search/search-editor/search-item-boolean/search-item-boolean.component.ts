import { Component, Input, OnInit } from '@angular/core';
import { AbstractSearchItemDirective } from '../abstract-search-item.directive';

@Component({
	selector: 'nuc-search-item-boolean',
	templateUrl: './search-item-boolean.component.html',
	styleUrls: ['./search-item-boolean.component.css']
})
export class SearchItemBooleanComponent extends AbstractSearchItemDirective implements OnInit {

	@Input() groupname: string;

	ngOnInit() {
	}
}
