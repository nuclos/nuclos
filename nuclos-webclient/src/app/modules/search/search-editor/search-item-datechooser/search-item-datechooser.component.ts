import { Component, OnInit } from '@angular/core';
import { AbstractSearchItemDirective } from '../abstract-search-item.directive';

@Component({
	selector: 'nuc-search-item-datechooser',
	templateUrl: './search-item-datechooser.component.html',
	styleUrls: ['./search-item-datechooser.component.css']
})
export class SearchItemDatechooserComponent extends AbstractSearchItemDirective implements OnInit {

	ngOnInit() {
	}
}
