import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { ObjectUtils } from '@shared/object-utils';
import { OverlayOnShowEvent, OverlayOptions } from 'primeng/api';
import { MultiSelect } from 'primeng/multiselect';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { NuclosDefaults } from '../../../../data/schema/nuclos-defaults';
import { EntityAttrMeta, EntityMeta, LovEntry } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectSearchfilterService } from '../../../entity-object-data/shared/entity-object-searchfilter.service';
import { LovDataService } from '../../../entity-object-data/shared/lov-data.service';
import { LovSearchConfig } from '../../../entity-object-data/shared/lov-search-config';
import { Preference, SearchtemplatePreferenceContent } from '../../../preferences/preferences.model';
import { AbstractSearchItemDirective } from '../abstract-search-item.directive';

@Component({
	selector: 'nuc-search-item-lov',
	templateUrl: './search-item-lov.component.html',
	styleUrls: ['./search-item-lov.component.css']
})
export class SearchItemLovComponent extends AbstractSearchItemDirective implements OnInit, OnDestroy {
	@Input() meta: EntityMeta | undefined;
	@Input() attributeMeta: EntityAttrMeta | undefined;
	@Input() suggestions: LovEntry[];
	@Input() attribute: any;

	@ViewChild('dropdown') multiSelect: MultiSelect | undefined;

	id: any;
	overlayOptions: OverlayOptions = {
		appendTo: 'body',
		onShow: event => this.onPanelShow(event)
	};

	private selectedSearchfilter: Preference<SearchtemplatePreferenceContent> | undefined;
	private unsubscribe$ = new Subject<boolean>();

	constructor(
		private lovDataService: LovDataService,
		private i18nService: NuclosI18nService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private _elemRef: ElementRef
	) {
		super();
	}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnInit() {
		this.eoSearchfilterService.observeSelectedSearchfilter().pipe(takeUntil(this.unsubscribe$)).subscribe(searchfilter => {
			this.selectedSearchfilter = searchfilter;
		});
		this.id = this._elemRef.nativeElement.id;
		if (this.model === undefined) {
			this.model = [];
		}
	}

	updateAutoCompleteMessage(message: string, loading?: boolean): void {
		if (this.multiSelect) {
			this.multiSelect.emptyFilterMessage = message;
		}
	}

	loadSearchLovDropDownOptions(query): void {
		this.updateAutoCompleteMessage(this.i18nService.getI18n('common.loading'), true);

		if (this.attributeMeta) {
			let search: LovSearchConfig = {
				attributeMeta: this.attributeMeta,
				vlp: this.getWebVlp(this.attributeMeta),
				quickSearchInput: query,
				resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT,
				searchmode: true
			};

			this.lovDataService.loadLovEntries(search).pipe(
				take(1)
			).subscribe(data => {
				if (data) {
					data = data.filter(d => !ObjectUtils.isEqual(d, LovEntry.EMPTY));
					if (data.length === NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT) {
						data.push({
							name: '...',
							id: null
						});
					}
					this.updateFilteredOptions(data);
					this.model.forEach(entry => {
						if (!data.some(suggestion => suggestion.id === entry.id)) {
							data.push(entry);
						}
					});
					this.suggestions = data;
				}
				this.updateAutoCompleteMessage(this.i18nService.getI18n('common.empty'), false);
			});
		}
	}

	updateFilteredOptions(data: LovEntry[]) {
		if (this.multiSelect && !ObjectUtils.isEqual(data, this.multiSelect._filteredOptions)) {
			// NUCLOS-10679 manually set filter options because the multiselect does not filter again, if changed inside onFilter callback
			this.multiSelect._filteredOptions = [...data];
		}
	}

	getWebVlp(attrMeta: EntityAttrMeta): WebValuelistProvider | undefined {
		if (attrMeta.isState() || attrMeta.isStateNumber()) {
			let statusVlp: WebValuelistProvider = {
				fieldname: '',
				idFieldname: '',
				name: 'status',
				value: 'status',
				type: 'named',
				parameter: []
			};
			if (this.meta) {
				let processId = this.getSearchValueFromSelectedFilter(
					this.meta.getEntityClassId() + '_nuclosProcess'
				);
				if (processId) {
					statusVlp.parameter.push({
						name: 'processId',
						value: processId
					});
				}
			}
			return statusVlp;
		}
		if (attrMeta.isProcess()) {
			let processVlp: WebValuelistProvider = {
				fieldname: '',
				idFieldname: '',
				name: 'process',
				value: 'process',
				type: 'named',
				parameter: []
			};
			return processVlp;
		}

		let vlpConfig = attrMeta.getVlpConfig();
		if (!vlpConfig) {
			return undefined;
		}
		let vlp: WebValuelistProvider = {
			fieldname: vlpConfig.valueField,
			idFieldname: vlpConfig.idField === undefined ? '' : vlpConfig.idField,
			name: vlpConfig.vlpId,
			value: vlpConfig.vlpId,
			type: 'ds',
			parameter: []
		};
		// this should be set automatically :-(
		vlp.parameter.push({
			name: 'id-fieldname',
			value: vlp.idFieldname
		});
		vlp.parameter.push({
			name: 'fieldname',
			value: vlp.fieldname
		});
		let vlpContext = this.getSuitableVlpContext(vlpConfig);
		if (vlpContext) {
			let vlpParams = vlpContext.vlpParams;
			if (vlpParams) {
				vlpParams.forEach(p => {
					let valueFromSearchfilter = false;
					if (p.valueFromEntityFieldId !== undefined) {
						let searchValueFromSelectedFilter = this.getSearchValueFromSelectedFilter(
							p.valueFromEntityFieldId
						);
						if (searchValueFromSelectedFilter) {
							vlp.parameter.push({
								name: p.name,
								value: searchValueFromSelectedFilter
							});
							valueFromSearchfilter = true;
						}
					}
					if (!valueFromSearchfilter && p.value !== undefined) {
						vlp.parameter.push({
							name: p.name,
							value: p.value
						});
					}
				});
			}
		}
		return vlp;
	}

	getSuitableVlpContext(vlpConfig: EntityFieldVlpConfig): EntityFieldVlpContext | undefined {
		let result: EntityFieldVlpContext | undefined;
		if (vlpConfig.vlpContexts) {
			// look for a exact context match
			if (this.meta) {
				let entityClassId = this.meta.getEntityClassId();
				result = vlpConfig.vlpContexts.find(
					c => c.entityClassId === entityClassId && c.search
				);
			}
			// look for the generic context
			if (!result) {
				result = vlpConfig.vlpContexts.find(c => c.entityClassId === undefined && c.search);
			}
		}
		return result;
	}

	getSearchValueFromSelectedFilter(valueFromEntityFieldId: string): string | undefined {
		if (this.selectedSearchfilter) {
			let attribute = this.selectedSearchfilter.content.columns.find(
				c => c.boAttrId === valueFromEntityFieldId
			);
			if (attribute) {
				if (attribute.values && attribute.values.length === 1) {
					return attribute.values[0].id;
				}
				if (attribute.value && attribute.value.id) {
					return attribute.value.id;
				}
				return attribute.value;
			}
		}
		return undefined;
	}

	/**
	 * This is only used for determining a click on the last line if we have MAX_SIZE elements
	 * in list. So the user has some notice and the remaining code does not care about this entry.
	 * @param value - AutoComplete Event Value to check
	 * @return boolean - true if found otherwise false
	 */
	isDottedListEndClicked(value: LovEntry | LovEntry[] | undefined): boolean {
		if (value instanceof Array) {
			// ngModelChange
			if (value?.find(e => e.id === null && e.name === '...') !== undefined) {
				return true;
			}
		} else {
			if (value?.id === null && value?.name === '...') {
				return true;
			}
		}

		return false;
	}

	selectEntry($event: LovEntry | LovEntry[] | undefined) {
		if (!this.isDottedListEndClicked($event)) {
			if ($event instanceof Array && $event !== this.model) {
				if ($event !== this.model) {
					this.model = $event;
					this.modelChanged.emit($event);
				}
			}
		}
	}

	removeEntry(item: any) {
		this.selectEntry(this.model.filter((entry: LovEntry) => entry.id !== item.id));
	}

	/**
	 * Add an identifying attribute to the overlay, so it can be identified within the tests.
	 * @param event
	 */
	onPanelShow(event: OverlayOnShowEvent | undefined) {
		event?.overlay?.setAttribute('ref-attr-id', this.attribute.boAttrId);
	}

}
