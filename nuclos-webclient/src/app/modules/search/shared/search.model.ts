
export interface Column {
	boAttrId: string;
}
interface Alias {
	[key: string]: Column;
}
export interface WhereCondition {
	aliases: Alias;
	clause: string;
}
