import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

/**
 * Triggers a search based on the current searchfilter.
 */
@Injectable()
export class SearchService {

	private searchInputText: BehaviorSubject<string>;
	private initateDataLoadSubject: Subject<SearchDataLoadObject>;

	constructor() {
		this.searchInputText = new BehaviorSubject<string>('');
		this.initateDataLoadSubject = new Subject<SearchDataLoadObject>();
	}

	updateSearchInputText(text: string) {
		this.searchInputText.next(text);
	}

	getCurrentSearchInputText(): string {
		return this.searchInputText.getValue();
	}

	observeSearchInputText() {
		return this.searchInputText;
	}

	initiateDataLoad(entityClassId: string): void {
		this.initateDataLoadSubject.next(new SearchDataLoadObject(entityClassId));
	}

	subscribeDataLoad(): Subject<SearchDataLoadObject> {
		return this.initateDataLoadSubject;
	}
}

export class SearchDataLoadObject {

	private date: Date;

	private entityClassId: string;

	constructor(entityClassId: string) {
		this.entityClassId = entityClassId;
		this.date = new Date();
	}

	getEntityClassId(): string {
		return this.entityClassId;
	}

	getDate(): Date {
		return this.date;
	}

}
