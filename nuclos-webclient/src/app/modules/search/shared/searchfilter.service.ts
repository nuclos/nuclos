import { Injectable } from '@angular/core';
import { Observable, of as observableOf } from 'rxjs';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { DatetimeService } from '../../../shared/service/datetime.service';
import { FqnService } from '../../../shared/service/fqn.service';
import { BoAttr, EntityMeta, InputType } from '../../entity-object-data/shared/bo-view.model';
import { SelectableService } from '../../entity-object-data/shared/selectable.service';
import { Logger } from '../../log/shared/logger';
import {
	Preference,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent, SubformSearchtemplatePreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { Operator } from '../operator';
import { OperatorDefinitions } from '../operator-definitions';
import { WhereCondition } from './search.model';
import { SearchService } from './search.service';

@Injectable()
export class SearchfilterService {

	/**
	 * Validates and formats the search input value according to the "input type" of the attribute.
	 */
	static formatSearchTemplateValue(
		value: any,
		attribute: BoAttr,
		useTicks: boolean
	): string {
		// if (attribute.inputType === 'reference') {
		if (attribute.reference) {
			value = value && value.id ? value.id : value;
		}

		if (
			(value !== undefined &&
				value.length === undefined &&
				(attribute.inputType === InputType.NUMBER ||
					attribute.inputType === InputType.REFERENCE ||
					attribute.inputType === InputType.DATE)) ||
			(value !== undefined && value.length > 0)
		) {
			if (useTicks) {
				if (attribute.inputType === InputType.DATE) {
					value = moment(value).format('YYYY-MM-DD');
				}
				if (typeof value !== 'number') {
					value = value.replace(/'/g, ''); // remove '
				}
			}
			if (useTicks) {
				value = '\'' + value + '\'';
			}
		} else if (attribute.inputType === InputType.STRING && value && value['name']) {
			value = value['name'];
			if (useTicks) {
				value = '\'' + value + '\'';
			}
		} else {
			value = null;
		}

		return value;
	}

	static columnsFromSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent> | undefined) {
		let cols: SearchtemplateAttribute[] = [];
		if (searchfilter && searchfilter.content.columns) {
			cols = searchfilter.content.columns;
		}
		return cols;
	}

	static entityClassIdsFromSearchfilterAttributes(cols: SearchtemplateAttribute[]) {
		let entityClassIds: Set<string> = new Set<string>();
		cols.forEach(c => {
			if (c.dependentBoMetaId !== undefined) {
				entityClassIds.add(c.dependentBoMetaId);
			}
		});
		return entityClassIds;
	}

	static entityClassIdsFromSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent> | undefined) {
		return SearchfilterService.entityClassIdsFromSearchfilterAttributes(
			SearchfilterService.columnsFromSearchfilter(searchfilter));
	}

	static getInputType(attribute: BoAttr): string {
		if (attribute.type === 'Decimal' || attribute.type === 'Integer' || attribute.type === 'Long') {
			return InputType.NUMBER;
		} else if (attribute.reference) {
			return InputType.REFERENCE;
		} else if (attribute.type === 'String') {
			return InputType.STRING;
		} else if (attribute.type === 'Date' || attribute.type === 'Timestamp') {
			return InputType.DATE;
		} else if (attribute.type === 'Boolean') {
			return InputType.BOOLEAN;
		}
		return InputType.STRING;
	}


	/*static getInputType(attribute: SearchtemplateAttribute): string {
		if (attribute.reference) {
			return InputType.REFERENCE;
		} else if (attribute.type === 'Decimal' || attribute.type === 'Integer' || attribute.type === 'Long') {
			return InputType.NUMBER;
		} else if (attribute.type === 'String') {
			return InputType.STRING;
		} else if (attribute.type === 'Date' || attribute.type === 'Timestamp') {
			return InputType.DATE;
		} else if (attribute.type === 'Boolean') {
			return InputType.BOOLEAN;
		}
		return InputType.STRING;
	}*/

	private static urlQueryCounter = 0;

	/**
	 * might be set from addon to filter result via addon
	 */
	private additionalWhereConditions: Map<
		string /* entityClassId */,
		Map<string /* appIdentifier */, string | undefined>
	> = new Map();

	private operatorDefinitions: OperatorDefinitions;

	constructor(
		private datetimeService: DatetimeService,
		// TODO: We should not need 2 different services here for handling searchfilter preferences
		private selectableService: SelectableService,
		private preferenceService: PreferencesService,
		private nuclosI18n: NuclosI18nService,
		// TODO: Temporarily injected here to resolve merge conflicts - remove later
		private searchService: SearchService,
		private fqnService: FqnService,
		private $log: Logger
	) {
		this.operatorDefinitions = {
			NUMBER: [
				{
					operator: '=',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.equal')
				},
				{
					operator: '!=',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n(
						'webclient.searchtemplate.operator.number.notequal'
					)
				},
				{
					operator: '>',
					isUnary: false,
					is2ValueComparison: true,
					combiningFor2ValueComparison: 'AND',
					oppositeOpFor2ValueComparison: '<',
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.gt'),
					label2ValueComparison: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.gt.value2')
				},
				{
					operator: '<',
					isUnary: false,
					is2ValueComparison: true,
					combiningFor2ValueComparison: 'OR',
					oppositeOpFor2ValueComparison: '>',
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.lt'),
					label2ValueComparison: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.lt.value2')
				},
				{
					operator: '>=',
					isUnary: false,
					is2ValueComparison: true,
					combiningFor2ValueComparison: 'AND',
					oppositeOpFor2ValueComparison: '<=',
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.gte'),
					label2ValueComparison: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.gte.value2')
				},
				{
					operator: '<=',
					isUnary: false,
					is2ValueComparison: true,
					combiningFor2ValueComparison: 'OR',
					oppositeOpFor2ValueComparison: '>=',
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.lte'),
					label2ValueComparison: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.lte.value2')
				},
				{
					operator: 'is null',
					isUnary: true,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.null')
				},
				{
					operator: 'is not null',
					isUnary: true,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.notnull')
				}
			],
			BOOLEAN: [
				{
					operator: '=',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.equal')
				}
			],
			STRING: [
				{
					operator: '=',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.equal')
				},
				{
					operator: '!=',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.notequal')
				},
				{
					operator: 'like',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.like')
				},
				{
					operator: 'not like',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.not.like')
				},
				{
					operator: 'is null',
					isUnary: true,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.null')
				},
				{
					operator: 'is not null',
					isUnary: true,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.notnull')
				}
			],
			REFERENCE: [
				{
					operator: '=',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.equal')
				},
				{
					operator: '!=',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.notequal')
				},
				{
					operator: 'like',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.like')
				},
				{
					operator: 'not like',
					isUnary: false,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.not.like')
				},
				{
					operator: 'is null',
					isUnary: true,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.null')
				},
				{
					operator: 'is not null',
					isUnary: true,
					is2ValueComparison: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.notnull')
				}
			]
		};
		this.operatorDefinitions[InputType.DATE] = this.operatorDefinitions[InputType.NUMBER];
	}

	getOperatorDefinitions(): OperatorDefinitions {
		return this.operatorDefinitions;
	}

	getOperatorDefinition(attribute: SearchtemplateAttribute): Operator | undefined {
		let type = attribute.inputType;
		let operator = attribute.operator;

		if (type === undefined) {
			return;
		}
		return this.operatorDefinitions[type].filter(op => op.operator === operator).shift();
	}

	createTempSearchfilter(entityClassId: string): Preference<SearchtemplatePreferenceContent> {
		let tempFilter = this.createSearchfilterWithoutAttributes(entityClassId);
		tempFilter.name = this.nuclosI18n.getI18n('webclient.searcheditor.searchfilter.temp.name');
		tempFilter.content.isTemp = true;
		tempFilter.selected = true;
		return tempFilter;
	}

	createSearchfilterWithoutAttributes(
		entityClassId: string
	): Preference<SearchtemplatePreferenceContent> {
		let searchtemplatePreference: Preference<SearchtemplatePreferenceContent> = {
			type: 'searchtemplate',
			boMetaId: entityClassId,
			selected: false,
			content: {
				columns: [],
				isValid: false,
				disabled: false
			}
		};
		return searchtemplatePreference;
	}

	createSearchfilter(metaData: EntityMeta): Preference<SearchtemplatePreferenceContent> {
		let searchtemplatePreference: Preference<SearchtemplatePreferenceContent> = {
			type: 'searchtemplate',
			boMetaId: metaData.getBoMetaId(),
			selected: false,
			content: {
				columns: [],
				isValid: false,
				disabled: false
			}
		};

		// load all attributes from meta data
		metaData.getAttributes().forEach(attributeMeta => {
			if (
				!attributeMeta.isStateIcon() &&
				!attributeMeta.isDeletedFlag() &&
				!attributeMeta.isHidden() &&
				!attributeMeta.isStateIcon() &&
				!attributeMeta.isImage()
			) {
				searchtemplatePreference.content.columns.push({
					name: attributeMeta.getName(),
					boAttrId: attributeMeta.getAttributeID(),
					system: attributeMeta.isSystemAttribute(),
					selected: false
				});
			}
		});

		return searchtemplatePreference;
	}

	createFromUrlSearch(
		meta: EntityMeta,
		searchString: string
	): Preference<SearchtemplatePreferenceContent> {
		let searchtemplatePreference = this.createSearchfilter(meta);

		let sp = searchString.split(',');
		let searchColumns: Array<SearchtemplateAttribute> = [];
		for (let searchParameter of sp) {
			let paramName = searchParameter.split('=')[0];
			let paramValue = searchParameter.split('=')[1];
			let attrFqn = meta.getBoMetaId() + '_' + paramName;
			if (paramName !== undefined && paramValue !== undefined) {
				let attributes = searchtemplatePreference.content.columns.filter(attr => {
					return attr.boAttrId === attrFqn;
				});

				if (attributes !== undefined && attributes.length !== 0) {
					let attrMeta = <SearchtemplateAttribute>meta.getAttribute(paramName);

					let attribute = attributes[0];
					searchColumns.push(attribute);
					if (attribute.inputType === 'number') {
						attribute.value = Number(paramValue);
					} else {
						attribute.value = paramValue;
					}

					// Do a LIKE search if the search value starts or ends with a wildcard charater (* or ?)
					if (/(^[*?])|([*?]$)/.test(attribute.value)) {
						attribute.operator = 'like';
					} else {
						attribute.operator = '=';
					}
					let inputType = SearchfilterService.getInputType(attrMeta);
					let operatorDefsForInputType = this.operatorDefinitions[inputType];
					if (operatorDefsForInputType.length !== 0) {
						let operatorDefs = operatorDefsForInputType.filter(
							elem => elem.operator === attribute.operator
						);
						let operatorDef;
						if (operatorDefs.length !== 0) {
							operatorDef = operatorDefs[0];
						} else {
							operatorDef = operatorDefsForInputType[0];
						}
						attribute.operator = operatorDef.operator;
						this.formatValue(attribute, operatorDef);
					}

					attribute.selected = true;
					attribute.enableSearch = true;
					attribute.isValid = true;
				} else {
					alert('Wrong attribute names: ' + paramName);
					// TODO use errorhandler
				}
			}
		}
		searchtemplatePreference.content.columns = searchColumns;
		searchtemplatePreference.content.isValid = true;
		searchtemplatePreference.content.isTemp = true;
		searchtemplatePreference.selected = true;
		searchtemplatePreference.name = '[URL:' + ++SearchfilterService.urlQueryCounter + ']';

		return searchtemplatePreference;
	}

	formatValue(attribute: SearchtemplateAttribute, operatorDef: Operator) {
		if (attribute.values) {
			attribute.formattedValue = '(';
			attribute.values.forEach((value, index) => {
				if (index > 0) {
					attribute.formattedValue += ', ';
				}
				attribute.formattedValue += this.valueToString(attribute, operatorDef, value);
			});
			attribute.formattedValue += ')';
		} else {
			attribute.formattedValue = this.valueToString(attribute, operatorDef, attribute.value);
		}
	}

	valueToString(attribute: SearchtemplateAttribute, operatorDef: Operator, value: any) {
		let result = value;

		if (attribute.inputType === InputType.DATE) {
			result = this.datetimeService.formatDate(attribute.value);
		} else if (attribute.inputType === InputType.REFERENCE) {
			result = value && value.name ? value.name : value;
		}

		if (
			(attribute.inputType === InputType.STRING ||
				attribute.inputType === InputType.REFERENCE) &&
			(operatorDef && !operatorDef.isUnary)
		) {
			result = '\'' + result + '\'';
		}

		return result;
	}

	saveSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		return this.selectableService.savePreference(searchfilter);
	}

	saveAndSelectSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		searchfilter.selected = true;
		// updating the preference does not set customization flag :(
		// we do it manually in advance
		if (searchfilter.shared === true) {
			searchfilter.customized = true;
		}
		return this.selectableService.savePreference(searchfilter);
	}

	deleteSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		if (!searchfilter.prefId) {
			return observableOf(true);
		}

		return this.preferenceService.deletePreferenceItem(searchfilter);
	}

	deleteSubformSearchfilter(searchfilter: Preference<SubformSearchtemplatePreferenceContent>) {
		if (!searchfilter.prefId || searchfilter.content.isTemp) {
			return observableOf(true);
		}

		return this.preferenceService.deletePreferenceItem(searchfilter);
	}

	resetCustomizedSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		return this.preferenceService.resetCustomizedPreferenceItem(searchfilter);
	}

	getSearchfilter(prefId: string): Observable<Preference<SearchtemplatePreferenceContent>> {
		return this.preferenceService.getPreference(prefId) as Observable<Preference<SearchtemplatePreferenceContent>>;
	}

	getDependentSearchfilter(prefId: string): Observable<Preference<SubformSearchtemplatePreferenceContent>> {
		return this.preferenceService.getPreference(prefId) as Observable<Preference<SubformSearchtemplatePreferenceContent>>;
	}

	getSearchfiltersForEntity(entityClassId: string | undefined) {
		return this.preferenceService.getPreferences({
			boMetaId: entityClassId,
			type: ['searchtemplate']
		});
	}

	getSubformSearchfiltersForEntity(layoutId: string | undefined) {
		return this.preferenceService.getPreferences({
			layoutId: layoutId,
			type: ['subform-searchtemplate']
		});
	}

	applyAdditionalResultlistWhereCondition(
		condition: string | undefined,
		entityClassId: string,
		appIdentifier: string
	) {
		let additionWehereConditionsForEntity = this.additionalWhereConditions.get(entityClassId);
		if (!additionWehereConditionsForEntity) {
			additionWehereConditionsForEntity = new Map();
			this.additionalWhereConditions.set(entityClassId, additionWehereConditionsForEntity);
		}
		additionWehereConditionsForEntity.set(appIdentifier, condition);
		this.initateDataLoad(entityClassId);
	}

	getAdditionalWhereConditions(entityClassId: string): string[] {
		let additionWehereConditionsForEntity =
			this.additionalWhereConditions.get(entityClassId) || new Map();
		return Array.from(additionWehereConditionsForEntity.values()).filter(
			c => c !== undefined
		) as string[];
	}

	initateDataLoad(entityClassId: string): void {
		this.searchService.initiateDataLoad(entityClassId);
	}

	select(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		if (searchfilter && searchfilter.prefId) {
			this.preferenceService
				.selectPreference(searchfilter)
				.subscribe(() => (searchfilter.selected = true));
		}
	}

	deselect(searchfilter: Preference<SearchtemplatePreferenceContent> | undefined) {
		if (searchfilter) {
			this.preferenceService.deselectPreference(searchfilter).subscribe();
		}
	}

	/**
	 * Builds the 'AND'-concatenated WHERE condition based on the given search object.
	 */
	buildWhereCondition(
		searchfilter: Preference<SearchtemplatePreferenceContent> | undefined,
		mainMeta: EntityMeta,
		dependentMapMeta: Map<string, EntityMeta>,
		useAliases = true
	): WhereCondition {
		let result: WhereCondition = {
			aliases: {},
			clause: ''
		};
		if (searchfilter) {
			if (searchfilter.content.expertWhereCondition !== undefined) {
				let expertWhereConditionCopy = JSON.parse(JSON.stringify(searchfilter.content.expertWhereCondition)) as WhereCondition;
				return expertWhereConditionCopy;
			}
			let index = 0;
			let andArray: string[] = [];

			let dependentAnds: Map<string, string[]> = new Map();
			let columns = SearchfilterService.columnsFromSearchfilter(searchfilter);
			for (let searchElement of columns) {
				if (
					!searchElement.operator ||
					searchElement.enableSearch === false ||
					searchElement.selected === false
				) {
					continue;
				}

				let meta =
					searchElement.dependentBoMetaId === undefined
						? mainMeta
						: dependentMapMeta.get(searchElement.dependentBoMetaId);
				if (meta !== undefined) {
					let attribute = this.fqnService.getAttributeByFqn(meta, searchElement.boAttrId);
					attribute.inputType = SearchfilterService.getInputType(attribute);
					let fqn = searchElement.boAttrId;
					let alias = useAliases ? ('col' + index) : fqn;
					let andPart = this.buildWhereAndPart(searchElement, attribute, alias);
					if (andPart !== undefined) {
						if (searchElement.dependentBoMetaId !== undefined) {
							let depAndArray = dependentAnds.get(searchElement.dependentBoMetaId);
							if (depAndArray === undefined) {
								depAndArray = [];
								dependentAnds.set(searchElement.dependentBoMetaId, depAndArray);
							}
							depAndArray.push(andPart);
						} else {
							andArray.push(andPart);
						}
						if (useAliases) {
							result.aliases[alias] = {boAttrId: fqn};
						}
						index++;
					} else {
						continue;
					}
				}
			}

			dependentAnds.forEach((depAndArray, entityClassId) => {
				let entityContexts = mainMeta.getEntityContexts();
				if (entityContexts) {
					let entityContext = entityContexts.find(
						ec => ec.dependentEntityClassId === entityClassId
					);
					if (entityContext) {
						let alias = useAliases ? ('col' + index) : entityContext.dependentEntityFieldId;
						let depClause =
							' EXISTS ( SELECT ' +
							entityClassId + '.id' +
							' FROM ' +
							entityClassId +
							' WHERE ' +
							mainMeta.getEntityClassId() + '.id' +
							'=' +
							alias +
							' AND ' +
							depAndArray.join(' AND ') +
							' ) ';
						if (useAliases) {
							result.aliases[alias] = {boAttrId: entityContext.dependentEntityFieldId};
						}
						index++;
						andArray.push(depClause);
					}
				}
			});

			result.clause = andArray.join(' AND ');
		}

		return result;
	}

	private buildWhereAndPart(
		searchElement: SearchtemplateAttribute,
		attribute: BoAttr,
		alias: string
	): string | undefined {
		let andPart = alias + ' ' + searchElement.operator + ' ';
		let useTicks = this.needsTicks(attribute, searchElement.operator);
		let operatorDef = this.getOperatorDefinition(searchElement);
		if (operatorDef && !operatorDef.isUnary) {
			if (this.hasMultipleValues(searchElement) && searchElement.operator === '=') {
				let values = searchElement.values;
				if (values) {
					andPart =
						'(' +
						values
							.map(
								value =>
									alias +
									' ' +
									searchElement.operator +
									' ' +
									SearchfilterService.formatSearchTemplateValue(
										value,
										attribute,
										useTicks
									)
							)
							.join(' OR ') +
						')';
				}
			} else if (this.hasMultipleValues(searchElement) && searchElement.operator === '!=') {
				// special case for NOT EQUAL condition on reference field columns
				let values = searchElement.values;
				if (values) {
					andPart =
						'(' +
						values
							.map(
								value =>
									alias +
									' ' +
									searchElement.operator +
									' ' +
									SearchfilterService.formatSearchTemplateValue(
										value,
										attribute,
										useTicks
									)
							)
							.join(' AND ') +
						')';
				}
			} else {
				let value = SearchfilterService.formatSearchTemplateValue(
					searchElement.value,
					attribute,
					useTicks
				);
				if (value === null) {
					this.$log.warn(
						'No value given for non-unary operator \'%o\' on attribute \'%o\', ignoring',
						searchElement.operator,
						attribute
					);
					return undefined;
				}

				andPart += value;

				if (operatorDef.is2ValueComparison && searchElement.value2) {
					let value2 = SearchfilterService.formatSearchTemplateValue(
						searchElement.value2,
						attribute,
						useTicks
					);
					if (value2 === null) {
						this.$log.warn(
							'No value given for non-unary operator \'%o\' on attribute \'%o\', ignoring',
							operatorDef.oppositeOpFor2ValueComparison,
							attribute
						);
					} else {
						andPart = '(' + andPart;
						andPart += ' ' + operatorDef.combiningFor2ValueComparison + ' ';
						andPart += alias + ' ' + operatorDef.oppositeOpFor2ValueComparison + ' ';
						andPart += value2;
						andPart += ')';
					}
				} else if (searchElement.inputType === 'BOOLEAN' && searchElement.value === 'false') {
					andPart = '(' + andPart;
					andPart += ' OR ';
					andPart += alias + ' IS NULL ';
					andPart += ')';
				}
			}
		}
		return andPart;
	}

	/**
	 * Determines if the ticks should be used to escape the value for the given attribute in a WHERE-query.
	 *
	 * @param attribute
	 * @param operator
	 * @returns {boolean}
	 */
	private needsTicks(attribute: any, operator: string | undefined): boolean {
		if (attribute.inputType === InputType.NUMBER) {
			return false;
		} else if (attribute.system) {
			return true;
		} else if (
			attribute.inputType === InputType.REFERENCE &&
			attribute.referenceWithUid === false &&
			(operator === '=' || operator === '!=')
		) {
			return false;
		}

		return true;
	}

	private hasMultipleValues(searchElement: SearchtemplateAttribute) {
		return !searchElement.value && searchElement.values && searchElement.values.length > 0;
	}
}
