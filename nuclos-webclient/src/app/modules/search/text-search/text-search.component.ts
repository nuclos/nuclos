import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, take, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { SubscriptionHandleDirective } from '../../../data/classes/subscription-handle.directive';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { NuclosHotkeysService } from '../../../shared/service/nuclos-hotkeys.service';
import { SystemParameter } from '../../../shared/system-parameters';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { SearchService } from '../shared/search.service';

@Component({
	selector: 'nuc-text-search',
	templateUrl: './text-search.component.html',
	styleUrls: ['./text-search.component.css']
})
export class TextSearchComponent extends SubscriptionHandleDirective implements OnInit {
	@Input() eo: EntityObject | undefined;
	@Input() meta: EntityMeta;

	textSearch: string;

	quicksearchDelayTime: number;

	@ViewChild('textSearchInput', {static: true}) textSearchInput;

	/**
	 * provides text input changes for debounced search
	 */
	private textSearchUpdated: Subject<string> = new Subject<string>();

	private ignoreTextChanges = false;

	constructor(
		private config: NuclosConfigService,
		private i18n: NuclosI18nService,
		private searchService: SearchService,
		private nuclosHotkeysService: NuclosHotkeysService
	) {
		super();
	}

	ngOnInit() {
		this.searchService.observeSearchInputText()
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(
				searchString => {
					this.ignoreTextChanges = true;
					this.textSearch = searchString;
					this.ignoreTextChanges = false;
				});
		this.config.getSystemParameters().pipe(take(1)).subscribe(params => {
			this.quicksearchDelayTime = params.get(SystemParameter.QUICKSEARCH_DELAY_TIME);

			// debounce search input
			this.textSearchUpdated
				.asObservable()
				.pipe(
					debounceTime(this.quicksearchDelayTime),
					distinctUntilChanged(),
					takeUntil(this.unsubscribe$)
				)
				.subscribe(searchInputText => this.triggerSearchService(searchInputText));
		});

		this.nuclosHotkeysService.addShortcut({
			keys: 'f',
			modifier: 'ALT',
			description: this.i18n.getI18n('webclient.shortcut.quicksearch')
		})
			.pipe(takeUntil(this.unsubscribe$))
			.subscribe(() => {
				this.textSearchInput.nativeElement.focus();
			});

	}

	getPlaceholder(): string {
		return (
			(this.meta ? this.meta.getEntityName() + ' ' : '') +
			this.i18n.getI18n('webclient.search.quickfind')
		);
	}

	doTextSearch(newTextSearch: string, doSearch = false): void {
		this.textSearch = newTextSearch;
		if (!this.ignoreTextChanges) {
			if (doSearch) {
				this.triggerSearchService(newTextSearch);
			} else {
				this.textSearchUpdated.next(this.textSearch);
			}
		}
	}

	isDisabled() {
		return this.eo && this.eo.isDirty();
	}

	private triggerSearchService(searchInputText: string) {
		if (this.searchService.getCurrentSearchInputText() !== searchInputText) {
			this.searchService.updateSearchInputText(searchInputText);
			if (this.meta) {
				this.searchService.initiateDataLoad(this.meta.getEntityClassId());
			}
			this.scrollSidebarToTop();
		}
	}

	/**
	 * scroll sidebar to top
	 * @deprecated: TODO: Sidebar should do this by itself, instead of being manipulated here
	 */
	private scrollSidebarToTop(): void {
		$('#sidebar-list-container').scrollTop(0);
	}
}
