import { AfterViewChecked, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
	selector: 'nuc-is-last',
	templateUrl: './is-last.component.html',
	styleUrls: ['./is-last.component.css']
})
export class IsLastComponent implements AfterViewChecked {
	@Input() isLast: boolean;
	@Output() onLastDone: EventEmitter<boolean> = new EventEmitter<boolean>();

	constructor() {
	}

	ngAfterViewChecked(): void {
		if (this.isLast) {
			this.onLastDone.emit(true);
		}
	}
}
