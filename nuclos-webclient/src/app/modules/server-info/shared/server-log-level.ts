export enum ServerLogLevel {
	OFF,
	FATAL,
	ERROR,
	WARN,
	INFO,
	DEBUG,
	TRACE,
	ALL
}
