import { StateInfo } from '@modules/state/shared/state';

export interface GraphStep extends StateInfo {
	isComplete: boolean;
	isActive: boolean;
	isEnabled: boolean;
	stateId: string;
	transitionDescription?: string;
	transitionLabel?: string;
}
