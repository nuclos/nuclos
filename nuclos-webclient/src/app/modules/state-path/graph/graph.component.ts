import { HttpClient } from '@angular/common/http';
import {
	AfterContentInit,
	Component,
	ElementRef, EventEmitter,
	HostListener,
	Input,
	OnChanges,
	OnDestroy,
	OnInit, Output,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { NuclosCacheService } from '@modules/cache/shared/nuclos-cache.service';
import { EntityObject } from '@modules/entity-object-data/shared/entity-object.class';
import { EntityObjectPreferenceService } from '@modules/entity-object/shared/entity-object-preference.service';
import { SideviewmenuService } from '@modules/entity-object/shared/sideviewmenu.service';
import { Logger } from '@modules/log/shared/logger';
import { GraphStep } from '@modules/state-path/graph/graph-step';
import { PathData } from '@modules/state-path/graph/path-data';
import { NuclosStateService } from '@modules/state/shared/nuclos-state.service';
import { StateInfo } from '@modules/state/shared/state';
import { IEntityObject, IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import { ColorUtils } from '@shared/color-utils';
import { ObjectUtils } from '@shared/object-utils';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { cloneDeep } from 'lodash';
import { BehaviorSubject, iif, Observable, of } from 'rxjs';
import { catchError, map, switchMap, take, takeWhile, tap } from 'rxjs/operators';
import { Link } from '../../../data/schema/link.model';

@Component({
	selector: 'nuc-graph',
	templateUrl: './graph.component.html',
	styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit, OnDestroy, OnChanges, AfterContentInit {

	@Input() eo: IEntityObject | undefined;
	@Input() elementsToShow = 4;
	@Input() modal = false;

	graphStepsDisplay: GraphStep[] = [];
	pathData: PathData | undefined;
	progressValue = new BehaviorSubject(0);
	progressBoxWidth = 156;
	viewLoaded = false;

	@ViewChild('progressElement') progressElement: ElementRef;

	private styleNode: HTMLElement | undefined = undefined;

	private eoListener: IEntityObjectEventListener = {
		afterProcessChange: () => this.updateFetchPath(),
		afterSave: () => this.updateFetchPath(),
		afterReload: () => this.updateFetchPath()
	};

	private graphSteps: GraphStep[] = [];
	private historicView = false;
	private active = false;

	constructor(
		private http: HttpClient,
		private config: NuclosConfigService,
		private stateService: NuclosStateService,
		private sideViewMenuService: SideviewmenuService,
		private eoPrefService: EntityObjectPreferenceService,
		private nuclosRestConfig: NuclosConfigService,
		private cache: NuclosCacheService,
		private $log: Logger,
		private elementRef: ElementRef
	) {
	}

	get stepBoxWidth(): number {
		return this.progressBoxWidth / this.graphStepsDisplay.length ?? 156;
	}

	ngOnInit(): void {
		this.active = true;
		this.sideViewMenuService.onSideviewmenuPrefChange().pipe(
			takeWhile(() => this.active)
		).subscribe(() => this.ngAfterContentInit());
		this.eoPrefService.selectedSideviewmenuPref$.pipe(
			takeWhile(() => this.active)
		).subscribe(() => this.ngAfterContentInit());
	}

	ngOnDestroy() {
		this.active = false;
	}

	@HostListener('window:resize')
	ngAfterContentInit() {
		// getting width after next GUI Tick to ensure we are getting correct size
		setTimeout(() => {
			this.progressBoxWidth = $(this.progressElement.nativeElement).width();
			this.elementsToShow = Math.round(this.progressBoxWidth / 154);
			this.elementsToShow = this.elementsToShow < 4 ? 4 : this.elementsToShow;
			this.$log.debug('boxWidth', this.progressBoxWidth);
			this.updateFetchPath();
		});
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes['eo'] !== undefined) {
			if (changes['eo'].previousValue) {
				let previousEo: EntityObject = changes['eo'].previousValue;
				previousEo.removeListener(this.eoListener);
			}
			this.eo?.addListener(this.eoListener);
		}
		this.ngAfterContentInit();
	}

	getFontColorForBackground(step: GraphStep) {
		if (step.isActive && step.isComplete) {
			return ColorUtils.calculateTextColorFromBackgroundColor(step.color);
		} else if (step.isActive || !step.isComplete) {
			return ColorUtils.calculateTextColorFromBackgroundColor('#FFFFFF');
		} else {
			return ColorUtils.calculateTextColorFromBackgroundColor(step.color);
		}
	}

	updateProgressBarValue(steps: GraphStep[]): void {
		const stepProgress = (this.stepBoxWidth / (this.stepBoxWidth * this.graphStepsDisplay.length)) * 100;
		const stepIndex = steps.findIndex((step) => step.isActive);
		let progress = ((stepIndex + 1) * stepProgress) - (stepProgress / 2);

		this.progressValue.next(progress < 0 ? 0 : progress);
	}

	mouseOverCalculateGraph(step: GraphStep): void {
		if (!step.isEnabled) {
			// don't do this if it is already complete or disabled
			return;
		}
		const newData = this.calculateGraph(this.graphSteps, step.stateId);
		if (!ObjectUtils.isEqualJson(newData, this.graphStepsDisplay)) {
			this.graphStepsDisplay = newData;
		}
		this.updateProgressBarValue(this.graphStepsDisplay);
	}

	mouseOutResetGraph(): void {
		if (!ObjectUtils.isEqualJson(this.graphSteps, this.graphStepsDisplay)) {
			this.graphStepsDisplay = cloneDeep(this.graphSteps);
		}
		this.updateProgressBarValue(this.graphStepsDisplay);
	}

	changeState(step: GraphStep) {
		if (this.eo === undefined || !step.isEnabled) { return; }

		if (this.eo.getAttribute('nuclosState')['id'] !== step.stateId) {
			const indexStep = this.graphSteps
				.findIndex(
					(listStep) => listStep.stateId === step.stateId
				);
			const currentStateIndex = this.graphSteps
				.findIndex(
					(listStep) => listStep.stateId === this.eo!.getAttribute('nuclosState')['id']
				);
			if ((indexStep - currentStateIndex) > 1) {
				// bulk execute
				let bulkStateInfos: StateInfo[] = [];
				for (let i = currentStateIndex; i <= indexStep; i++) {
					bulkStateInfos.push(
						this.mergeStateInfos(this.graphSteps[i])
					);
				}
				this.stateService.changeStateBulk(this.eo, this.modal, bulkStateInfos, this.graphSteps[currentStateIndex]).subscribe();
			} else if ((currentStateIndex - indexStep) > 1) {
				// bulk execute
				let bulkStateInfos: StateInfo[] = [];
				for (let i = currentStateIndex; i >= indexStep; i--) {
					bulkStateInfos.push(
						this.mergeStateInfos(this.graphSteps[i])
					);
				}
				this.stateService.changeStateBulk(this.eo, this.modal, bulkStateInfos, this.graphSteps[currentStateIndex]).subscribe();
			} else {
				this.changeStateWithStateInfo(
					this.mergeStateInfos(step)
				);
			}
		}
	}

	changeStateWithStateInfo(stateInfo: StateInfo): void {
		if (this.eo !== undefined) {
			this.stateService.changeState(this.eo, this.modal, stateInfo, this.graphSteps.find((step) => step.isActive)).pipe(
				take(1),
				tap(() => this.updateFetchPath())
			).subscribe();
		}
	}

	outsidePathStates(graphStep: GraphStep): Array<any> {
		const hiddenStep = this.historicView ? graphStep : [...this.graphSteps].reverse().find((el) => el.stateId === graphStep.stateId);
		if (!hiddenStep?.isActive || this.isReadOnly()) {
			return [];
		}

		if (this.historicView) {
			return this.getNextStates();
		} else {
			return this.getNextStates()
				.filter((state) => !this.isInPathData(state.nuclosStateId)
					|| !this.isDirectDefaultTransition(state.nuclosStateId));
		}
	}

	fetchNextStateInfo(stateInfo: any): Observable<any> {
		return this.stateService.fetchInfo(stateInfo.nuclosStateId).pipe(
			take(1),
			map((info: StateInfo) => {
				return this.mergeStateInfos(info, stateInfo);
			})
		);
	}

	showNumerals(): boolean {
		return this.eo?.getData().showStateNumerals ?? true;
	}

	showIcons(): boolean {
		return this.eo?.getData().showStateIcons ?? true;
	}

	stateIconURI(step: GraphStep): string {
		return this.nuclosRestConfig.getStateIconURL(step.stateId);
	}

	stateButtonURI(step: GraphStep): string {
		return step.buttonIcon ? this.nuclosRestConfig.getResourceURL(step.buttonIcon) : '';
	}

	hideStateButton(step: GraphStep): boolean {
		return this.showNumerals() || step.buttonIcon == null;
	}

	private createStyleTag(graphStep: GraphStep[]): HTMLElement {
		const element = document.createElement('style');
		graphStep.forEach((step) => {
			if (step.color !== undefined) {
				element.textContent += `
					#${step.stateId}.is-complete:before {
					  background: ${step.color};
					}
				`;
			}
		});
		return element;
	}

	private updateFetchPath(): void {
		if (this.eo === undefined) { return; }

		const pathLink: Link = this.eo.getData().links.defaultPath;
		this.historicView = this.eo.getData().links.defaultPath === undefined;
		if (pathLink !== undefined) {
			this.cache.getCache('http.GET')
				.get(
					this.getPathHrefWithElementsToShow(pathLink.href),
					this.getPathObservable(pathLink.href)
				).pipe(
					take(1),
					switchMap((pathData: PathData) =>
						iif(
							() => pathData.outside,
							this.getPathObservable(this.eo?.getData().links.objectPath?.href).pipe(
								tap(() => this.historicView = true)
							),
							of(pathData)
						)
					),
					catchError((err) => {
						this.$log.error(err);
						return of(undefined);
					})
				).subscribe((pathData: PathData) => {
					if (pathData !== undefined) {
						const hasState = this.eo!.getAttribute('nuclosState');
						this.viewLoaded = true;
						this.pathData = pathData;
						this.graphSteps = this.calculateGraph(pathData.infos, hasState ? this.eo!.getAttribute('nuclosState')['id'] : '');
						this.graphStepsDisplay = cloneDeep(this.graphSteps);
						this.updateProgressBarValue(this.graphStepsDisplay);

						if (this.styleNode !== undefined) {
							this.elementRef.nativeElement.removeChild(this.styleNode);
						}
						this.styleNode = this.createStyleTag(this.graphSteps);
						this.elementRef.nativeElement.appendChild(this.styleNode);
					}
				});
		} else {
			this.graphSteps = [];
			this.graphStepsDisplay = [];
		}
	}

	private getPathObservable(pathLinkHref: string): Observable<any> {
		return this.http.get<PathData>(this.getPathHrefWithElementsToShow(pathLinkHref));
	}

	private getPathHrefWithElementsToShow(pathLinkHref: string): string {
		return `${pathLinkHref}?maxElements=${this.elementsToShow}`;
	}

	private isReadOnly(): boolean {
		return this.eo === undefined || this.eo['readonly'] === true;
	}

	private mergeStateInfos(...steps: StateInfo[]): StateInfo {
		const concatedInfos = steps.reduce((previousValue: StateInfo, currentValue: StateInfo) => {
			return {
				...previousValue,
				...currentValue
			};
		}, {} as StateInfo);
		return {
			...concatedInfos,
			name: concatedInfos.statename ?? concatedInfos.name,
			nuclosStateId: concatedInfos['stateId'] ?? concatedInfos.nuclosStateId,
			description: concatedInfos['transitionDescription'] ?? concatedInfos.description,
			transitionname: concatedInfos['transitionLabel'] ?? concatedInfos.transitionname,
			showNumeral: this.showNumerals(),
			showIcon: this.showIcons(),
			links: {
				stateIcon: {
					href: this.nuclosRestConfig.getStateIconURL(concatedInfos.nuclosStateId),
					methods: ['GET']
				}
			}
		};
	}

	private calculateGraph(steps: GraphStep[], activeStateId: string): GraphStep[] {
		const copyDataArr = cloneDeep(steps);
		const indexStep = copyDataArr.findIndex((s) => s.stateId === activeStateId);
		const currStepId = this.eo?.getData()['attributes']['nuclosState'] ? this.eo?.getData()['attributes']['nuclosState']['id'] : '';
		const indexCurr = copyDataArr.findIndex((s) => s.stateId === currStepId);
		let firstDisabledStateFromCurrentState = Number.MAX_SAFE_INTEGER;
		return copyDataArr.map((step, index: number) => {
			const isForwardTransition = index > indexCurr;
			// disabled states in between the current state and the step indicate, that there is no non-automatic, default transition
			const noDisabledStateBetween = index < firstDisabledStateFromCurrentState;
			step.isActive = (step.stateId === activeStateId && !this.historicView) || (this.historicView && index === steps.length - 1);
			step.isComplete = index < indexStep || (index !== steps.length - 1 && this.historicView);
			// enable a step, only if it is reachable from the current step via non-automatic, default transitions
			step.isEnabled =
				(
					(isForwardTransition && this.isInNextStatesOrOnPath(step.stateId))
					|| (!isForwardTransition &&
						(this.isInNextStates(step.stateId) && index === indexCurr - 1)
						|| (this.isOnPath(step.stateId) && index < indexCurr - 1))
				) && !this.isReadOnly() && !this.historicView && noDisabledStateBetween;
			if (!step.isEnabled && index > indexCurr) {
				firstDisabledStateFromCurrentState = Math.min(firstDisabledStateFromCurrentState, index);
			}

			if (this.pathData?.end && index === steps.length - 1) {
				step.isComplete = true;
			}
			return step;
		});
	}

	private isInNextStates(stateId: string): boolean {
		return this.getNextStates()
			.find(
				(nextState) => nextState['nuclosStateId'] === stateId
			) !== undefined;
	}

	private isInNextStatesOrOnPath(stateId: string): boolean {
		return this.getNextStates()
					.find(
						(nextState) => nextState['nuclosStateId'] === stateId || this.isInNextPathStates(stateId, nextState)
					) !== undefined;
	}

	private isOnPath(stateId: string) {
		return this.getNextStates()
			.find(
				(nextState) => this.isInNextPathStates(stateId, nextState)
			) !== undefined;
	}

	private isInNextPathStates(stateId: string, nextStateInfo: StateInfo): boolean {
		return nextStateInfo.hasOwnProperty('nextPathStateIds') &&
			nextStateInfo['nextPathStateIds'].find((id: string) => id === stateId) !== undefined;
	}

	private isInPathData(stateId: string): boolean {
		return this.pathData?.infos.find((pathStep) => pathStep.stateId === stateId) !== undefined;
	}

	/**
	 * Checks, if the specified stateId directly succeeds the current graph step.
	 * @param stateId
	 * @private
	 */
	private isDirectDefaultSuccessor(stateId: string): boolean {
		const eoState = this.eo!.getAttribute('nuclosState');
		const targetIndex = this.graphSteps.findIndex(gs => gs.stateId === stateId);
		const currentStateIndex = eoState ? this.graphSteps
			.findIndex(
				(listStep) => listStep.stateId === this.eo!.getAttribute('nuclosState')['id']
			) : -1;
		return (currentStateIndex + 1 - targetIndex) === 0;
	}

	/**
	 * Checks, if the specified stateId directly precedes the current graph step.
	 * @param stateId
	 * @private
	 */
	private isDirectDefaultPredecessor(stateId: string): boolean {
		const eoState = this.eo!.getAttribute('nuclosState');
		const targetIndex = this.graphSteps.findIndex(gs => gs.stateId === stateId);
		const currentStateIndex = eoState ? this.graphSteps
			.findIndex(
				(listStep) => listStep.stateId === this.eo!.getAttribute('nuclosState')['id']
			) : -1;
		return (targetIndex + 1 - currentStateIndex) === 0;
	}

	/**
	 * Checks, if the specified stateId is either a direct successor or direct predecessor of the current graph step.
	 * @param stateId
	 * @private
	 */
	private isDirectDefaultTransition(stateId: string): boolean {
		return this.isDirectDefaultPredecessor(stateId) || this.isDirectDefaultSuccessor(stateId);
	}

	private getNextStates(): Array<any> {
		const nextStates = this.eo?.getData() !== null && this.eo?.getData() !== undefined ? this.eo?.getData()['nextStates'] : undefined;
		if (nextStates !== undefined) {
			return nextStates;
		}
		return [];
	}
}
