import { GraphStep } from './graph-step';

export interface PathData {
	end: boolean;
	moreAfter: boolean;
	moreBefore: boolean;
	outside: boolean;
	infos: GraphStep[];
}
