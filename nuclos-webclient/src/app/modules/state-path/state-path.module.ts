import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StateModule } from '@modules/state/state.module';
import { SharedModule } from '@shared/shared.module';
import { I18nModule } from '../i18n/i18n.module';
import { GraphComponent } from './graph/graph.component';


@NgModule({
	declarations: [
		GraphComponent
	],
	exports: [
		GraphComponent
	],
	imports: [
		CommonModule,
		SharedModule,
		I18nModule,
		StateModule,
	]
})
export class StatePathModule {
}
