import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CurrentStateComponent } from './current-state.component';

xdescribe('CurrentStateComponent', () => {
	let component: CurrentStateComponent;
	let fixture: ComponentFixture<CurrentStateComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [CurrentStateComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CurrentStateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
