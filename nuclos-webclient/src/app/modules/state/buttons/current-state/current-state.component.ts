
import { Component, Input, OnInit } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { ColorUtils } from '@shared/color-utils';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { StateInfo } from '../../shared/state';

@Component({
	selector: 'nuc-current-state',
	templateUrl: './current-state.component.html',
	styleUrls: ['./current-state.component.css']
})
export class CurrentStateComponent implements OnInit {
	@Input() state: StateInfo;

	textColor: string | undefined = '#FFFFFF';
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor() {
	}

	ngOnInit() {
		if (this.state.color) {
			this.textColor = ColorUtils.calculateTextColorFromBackgroundColor(this.state.color);
		}
	}
}
