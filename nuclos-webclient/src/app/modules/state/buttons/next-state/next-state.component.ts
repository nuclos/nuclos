import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { ColorUtils } from '@shared/color-utils';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { StateInfo } from '../../shared/state';

@Component({
	selector: 'nuc-next-state',
	templateUrl: './next-state.component.html',
	styleUrls: ['./next-state.component.css']
})
export class NextStateComponent implements OnInit {
	@Input() state: StateInfo;
	@Input() saveInProgress: boolean;

	@Output() changeState = new EventEmitter<StateInfo>();

	textColor: string | undefined = '#FFFFFF';
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor() {
	}

	ngOnInit() {
		if (this.state?.color) {
			this.textColor = ColorUtils.calculateTextColorFromBackgroundColor(this.state.color);
		}
	}

}
