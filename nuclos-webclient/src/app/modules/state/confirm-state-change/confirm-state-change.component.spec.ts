/* tslint:disable:no-unused-variable */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ConfirmStateChangeComponent } from './confirm-state-change.component';

xdescribe('ConfirmStateChangeComponent', () => {
	let component: ConfirmStateChangeComponent;
	let fixture: ComponentFixture<ConfirmStateChangeComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [ConfirmStateChangeComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ConfirmStateChangeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
