import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { Logger } from '../../log/shared/logger';
import { StateInfo } from '../shared/state';

@Component({
	selector: 'nuc-confirm-state-change',
	templateUrl: './confirm-state-change.component.html',
	styleUrls: ['./confirm-state-change.component.css']
})
export class ConfirmStateChangeComponent implements OnInit {
	@Input() targetStateInfo: StateInfo;
	@Input() currentStateInfo: StateInfo;

	targetDescriptionIsSet = false;
	targetDescriptionIsHtml = false;
	targetDescriptionForHtml: string;

	constructor(
		private $log: Logger,
		private activeModal: NgbActiveModal,
		private nuclosRestConfig: NuclosConfigService
	) {
		this.$log.debug('Init: %o', this);
		this.$log.debug('active modal: %o', this.activeModal);
	}

	ngOnInit() {
		this.currentStateInfo = {
			...this.currentStateInfo,
			links: {
				buttonIcon: this.currentStateInfo.buttonIcon ? {
					href: this.nuclosRestConfig.getResourceURL(this.currentStateInfo.buttonIcon),
					methods: ['GET']
				} : undefined,
			}
		};
		this.$log.debug('Current StateInfo: %o', this.currentStateInfo);

		this.targetStateInfo = {
			...this.targetStateInfo,
			links: {
				buttonIcon: this.targetStateInfo.buttonIcon ? {
					href: this.nuclosRestConfig.getResourceURL(this.targetStateInfo.buttonIcon),
					methods: ['GET']
				} : undefined,
			}
		};
		this.$log.debug('Target StateInfo: %o', this.targetStateInfo);

		if (this.targetStateInfo && this.targetStateInfo.description !== undefined) {
			this.targetDescriptionIsSet = true;
			this.targetDescriptionIsHtml = /^(<html>)(?:.|\s)*(<\/html>)$/gi.test(this.targetStateInfo.description);
		} else {
			this.targetDescriptionIsSet = false;
			this.targetDescriptionIsHtml = false;
		}
		this.targetDescriptionForHtml = this.targetDescriptionIsHtml ? this.targetStateInfo.description!
			.replace(/^(<html>)/gi, '')
			.replace(/(<\/html>)$/gi, '') : '';
	}

	ok() {
		this.activeModal.close('accepted');
	}

	cancel() {
		this.activeModal.dismiss();
	}

	/**
	 * Returns a gradient background style if current state and target state both have a color.
	 *
	 * TODO: IE support:
	 * "background: -webkit-gradient(linear, left top, left bottom,
	 *            from(" + $scope.dialog.currentStateInfo.color + "),
	 *            to(" + $scope.dialog.newStateInfo.color + "));" +
	 * "background-image: linear-gradient(to right, " + $scope.dialog.currentStateInfo.color + " 0%,
	 * "        + $scope.dialog.newStateInfo.color + " 100%);"
	 *
	 * @returns {any}
	 */
	getGradientStyle(): any {
		let color1 = this.currentStateInfo && this.currentStateInfo.color;
		let color2 = this.targetStateInfo && this.targetStateInfo.color;

		if (!color1 || !color2) {
			return {'display': 'none'};
		}

		return {
			'background': 'linear-gradient(to right, ' + color1 + ', ' + color2 + ')'
		};
	}
}
