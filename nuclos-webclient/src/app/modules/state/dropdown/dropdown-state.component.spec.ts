import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DropdownStateComponent } from './dropdown-state.component';

xdescribe('DropdownStateComponent', () => {
	let component: DropdownStateComponent;
	let fixture: ComponentFixture<DropdownStateComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [DropdownStateComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DropdownStateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
