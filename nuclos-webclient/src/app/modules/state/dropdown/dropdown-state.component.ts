
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { ColorUtils } from '@shared/color-utils';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { StateInfo } from '../shared/state';

@Component({
	selector: 'nuc-state-dropdown',
	templateUrl: './dropdown-state.component.html',
	styleUrls: ['./dropdown-state.component.css']
})
export class DropdownStateComponent implements OnInit, OnChanges {

	@Input() currentState: StateInfo;

	@Input() currentAndNextStates: StateInfo[];

	@Input() saveInProgress: boolean;

	@Output() changeState = new EventEmitter<StateInfo>();

	textColor: string | undefined = '#000000';
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor() {
	}

	ngOnInit() {
		if (this.currentState?.color) {
			this.textColor = ColorUtils.calculateTextColorFromBackgroundColor(this.currentState.color);
		} else {
			this.textColor = '#000000';
		}
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (this.currentState?.color) {
			this.textColor = ColorUtils.calculateTextColorFromBackgroundColor(this.currentState.color);
		} else {
			this.textColor = '#000000';
		}
	}

	fireStateChange($event: StateInfo) {
		this.changeState.emit($event);
	}

}
