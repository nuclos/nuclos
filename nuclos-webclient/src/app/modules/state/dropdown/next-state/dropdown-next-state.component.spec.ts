import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DropdownNextStateComponent } from './dropdown-next-state.component';

xdescribe('NextStateComponent', () => {
	let component: DropdownNextStateComponent;
	let fixture: ComponentFixture<DropdownNextStateComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [DropdownNextStateComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DropdownNextStateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
