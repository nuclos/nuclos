import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ColorUtils } from '@shared/color-utils';
import { StateInfo } from '../../shared/state';

@Component({
	selector: 'nuc-dropdown-next-state',
	templateUrl: './dropdown-next-state.component.html',
	styleUrls: ['./dropdown-next-state.component.css']
})
export class DropdownNextStateComponent implements OnInit {
	@Input() state: StateInfo;
	@Input() saveInProgress: boolean;

	@Output() changeState = new EventEmitter<StateInfo>();

	hoverColor = '#FFFFFF80';
	textColor = '#000000';
	hover = false;

	constructor() {
	}

	ngOnInit() {
		if (this.state.color) {
			this.textColor = ColorUtils.calculateTextColorFromBackgroundColor(this.state.color);
		}
		this.hoverColor = this.state.color + '80';
	}

}
