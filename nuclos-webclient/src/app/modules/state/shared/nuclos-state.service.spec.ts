/* tslint:disable:no-unused-variable */
import { inject, TestBed } from '@angular/core/testing';
import { NuclosStateService } from './nuclos-state.service';

xdescribe('Service: State', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [NuclosStateService]
		});
	});

	it('should ...', inject([NuclosStateService], (service: NuclosStateService) => {
		expect(service).toBeTruthy();
	}));
});
