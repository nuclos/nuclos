import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NuclosDialogService } from '@app/service/nuclos-dialog.service';
import { NuclosCacheService } from '@modules/cache/shared/nuclos-cache.service';
import { EntityObjectResultService } from '@modules/entity-object-data/shared/entity-object-result.service';
import { Logger } from '@modules/log/shared/logger';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { BrowserRefreshService, SelectReferenceInOtherWindowEvent } from '@shared/service/browser-refresh.service';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { iif, Observable, of, throwError } from 'rxjs';
import { catchError, finalize, switchMap, take, tap } from 'rxjs/operators';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { ConfirmStateChangeComponent } from '../confirm-state-change/confirm-state-change.component';
import { StateInfo } from './state';

@Injectable()
export class NuclosStateService {
	private _selectEntryInOtherWindowToken: string;

	constructor(
		private http: HttpClient,
		private config: NuclosConfigService,
		private modalService: NuclosDialogService,
		private nuclosCache: NuclosCacheService,
		private eoResultService: EntityObjectResultService,
		private nuclosHotkeysService: NuclosHotkeysService,
		private nuclosRestConfig: NuclosConfigService,
		private browserRefreshService: BrowserRefreshService,
		private $log: Logger,
		private router: Router,
	) {
	}

	get selectEntryInOtherWindowToken(): string {
		return this._selectEntryInOtherWindowToken;
	}

	set selectEntryInOtherWindowToken(value: string) {
		this._selectEntryInOtherWindowToken = value;
	}

	/**
	 * Fetches informations (description, color, ...) about the state with the given ID.
	 */
	fetchInfo(stateId: string): Observable<StateInfo> {
		const url = this.config.getRestHost() + '/data/statusinfo/' + stateId;
		return this.nuclosCache.getCache('http.GET').get(url, this.http
			.get<StateInfo>(url)
			.pipe(
				take(1)
			));
	}

	/**
	 * Sets the given state on the current EO and saves it.
	 *
	 * @param eo
	 * @param stateInfo
	 * @param modalStateChange
	 * @returns Observable<EntityObject>
	 */
	changeState(eo: IEntityObject, modalStateChange: boolean, targetStateInfo: StateInfo, currentStateInfo?: StateInfo): Observable<any> {
		return this.confirmStateChange(eo, targetStateInfo, currentStateInfo).pipe(
			take(1),
			switchMap((dialogResult: string | IEntityObject | undefined) =>
				dialogResult !== undefined ?
					this.changeStateWithoutConfirm(eo, targetStateInfo, modalStateChange) : of(void 0)
			)
		);
	}

	changeStateBulk(eo: IEntityObject, modalStateChange: boolean, stateInfos: StateInfo[], currentStateInfo?: StateInfo): Observable<any> {
		return this.confirmStateChange(eo, stateInfos[stateInfos.length - 1], currentStateInfo).pipe(
			take(1),
			tap(async (dialogResult: string | undefined) => {
				if (dialogResult !== undefined) {
					for (const stateInfo of stateInfos) {
						await this.changeStateWithoutConfirm(eo, stateInfo, modalStateChange).toPromise();
					}
				}
			})
		);
	}

	changeStateWithoutConfirm(eo: IEntityObject, stateInfo: StateInfo, modalStateChange: boolean): Observable<any> {
		let errorInStateChange = false;
		return (eo as EntityObject).changeState(stateInfo).pipe(
			take(1),
			catchError((error) => {
				errorInStateChange = true;
				return throwError(error);
			}),
			finalize(() => {
				if (!errorInStateChange) {
					(eo as EntityObject).reloaded();
				}
				if (!modalStateChange) {
					this.eoResultService.selectEo(eo);
				}
				if (this.isOpenedInPopup()) {
					// send event to other browser instance
					let selectReferenceEvent = new SelectReferenceInOtherWindowEvent();
					selectReferenceEvent.entityClassId = eo.getEntityClassId();
					selectReferenceEvent.eoId = eo.getId()!;
					selectReferenceEvent.updateOnly = true;
					let name = eo.getData()?.title;
					if (name) {
						selectReferenceEvent.name = name;
					}
					selectReferenceEvent.token =
						Number(this._selectEntryInOtherWindowToken); // ? +this._selectEntryInOtherWindowToken : new Date().getTime();
					this.browserRefreshService.selectEntryInOtherWindow(selectReferenceEvent);
				}
			})
		);
	}

	isOpenedInPopup(): boolean {
		return this.router.url.indexOf('/popup/') !== -1;
	}

	/**
	 * Displays a confirmation dialog for changing to the given state.
	 * Does not actually change the state.
	 *
	 * TODO: Show a proper modal dialog instead of a simple alert
	 *
	 * @param eo
	 * @param state
	 */
	confirmStateChange(eo: IEntityObject, targetState: StateInfo, currentState?: StateInfo): Observable<any> {
		if (targetState.nonstop) {
			return of(eo);
		}

		return this.modalService.custom(
			ConfirmStateChangeComponent,
			{
				'stateService': this,
				'currentStateInfo': currentState,
				'targetStateInfo': targetState
			},
			undefined,
			document.activeElement
		).pipe(
			take(1)
		);
	}
}
