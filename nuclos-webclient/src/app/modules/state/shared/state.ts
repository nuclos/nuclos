import { Link } from '../../../data/schema/link.model';

export interface State {
	nuclosStateId: string;
	name: string;
	numeral?: number;
}

export interface StateInfo extends State {
	statename?: string;
	nonstop?: boolean;
	currentState?: boolean;
	numeral: number;
	description?: string;
	transitionname?: string;
	color?: string;
	buttonIcon?: string;
	buttonLabel?: string;
	showNumeral?: boolean;
	showIcon?: boolean;

	links?: {
		buttonIcon?: Link,
		stateIcon?: Link,
		change?: Link
	};
}
