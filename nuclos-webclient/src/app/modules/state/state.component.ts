import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NuclosI18nService } from '@app/service/nuclos-i18n.service';
import { Logger } from '@modules/log/shared/logger';
import { IEntityObjectEventListener } from '@nuclos/nuclos-addon-api';
import {
	BrowserRefreshService,
	SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM
} from '@shared/service/browser-refresh.service';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { NuclosHotkeysService } from '@shared/service/nuclos-hotkeys.service';
import { take, takeUntil } from 'rxjs/operators';
import { SubscriptionHandleDirective } from '../../data/classes/subscription-handle.directive';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { NuclosStateService } from './shared/nuclos-state.service';
import { StateInfo } from './shared/state';

@Component({
	selector: 'nuc-state',
	templateUrl: './state.component.html',
	styleUrls: ['./state.component.css']
})
export class StateComponent extends SubscriptionHandleDirective implements OnInit, OnChanges {
	@Input() eo: EntityObject;
	@Input() modal: boolean;

	currentState: StateInfo | undefined;
	currentAndNextStates: StateInfo[] = [];
	saveInProgress = false;
	hasDefaultPath = false;

	hovered;
	comboboxThreshold: number;
	isDisplayAsCombobox = true;

	private metaIdentifier: string;

	private eoListener: IEntityObjectEventListener = {
		isSaving: (entityObject: EntityObject, inProgress: boolean) => this.saveInProgress = inProgress,
		afterSave: () => this.updateStates(),
		afterReload: () => this.updateStates()
	};

	private selectEntryInOtherWindowToken: string;

	constructor(
		protected i18n: NuclosI18nService,
		protected stateService: NuclosStateService,
		private eoResultService: EntityObjectResultService,
		private nuclosHotkeysService: NuclosHotkeysService,
		private nuclosStateService: NuclosStateService,
		private nuclosRestConfig: NuclosConfigService,
		private browserRefreshService: BrowserRefreshService,
		private router: Router,
		protected route: ActivatedRoute,
		private $log: Logger
	) {
		super();

		this.nuclosHotkeysService.addShortcut({
			modifier: 'ALT_SHIFT', keys: '1',
			description: this.i18n.getI18n('webclient.shortcut.detail.focusStates')
		}).pipe(
			takeUntil(this.unsubscribe$)
		).subscribe(() => {
			this.focusStateSelection();
		});
	}

	ngOnInit() {
		this.hasDefaultPath = this.eo.getLinks()?.defaultPath !== undefined;
		this.eo.getMeta().pipe(
			take(1)
		).subscribe((meta) => {
			this.metaIdentifier = meta.getEntityClassId();
			this.comboboxThreshold = meta.getStateDisplayComboboxThreshold();
		});

		this.route.queryParams.pipe(
			take(1)
		).subscribe((params) => {
			let selectEntryInOtherWindowToken = params[SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM];
			if (selectEntryInOtherWindowToken) {
				this.stateService.selectEntryInOtherWindowToken = selectEntryInOtherWindowToken;
			}
		});
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				let previousEo: EntityObject = eoChange.previousValue;
				previousEo.removeListener(this.eoListener);
			}
			this.hasDefaultPath = this.eo.getLinks()?.defaultPath !== undefined;
			this.eo.addListener(this.eoListener);
			this.eo.getMeta().pipe(
				take(1)
			).subscribe((meta) => {
				this.metaIdentifier = meta.getEntityClassId();
				this.comboboxThreshold = meta.getStateDisplayComboboxThreshold();
				this.updateStates();
			});
		}
	}

	/**
	 * Sets the given state on the current EO and saves it.
	 *
	 * @param state
	 */
	changeState(stateInfo: StateInfo) {
		this.stateService.changeState(this.eo, this.modal, stateInfo, this.currentState).pipe(
			take(1)
		).subscribe();
	}

	updateDisplayMode() {
		this.isDisplayAsCombobox = (this.currentAndNextStates.length >= this.comboboxThreshold && this.comboboxThreshold >= 0);
	}

	private updateStates() {
		this.currentState = undefined;
		this.currentAndNextStates = [];
		this.saveInProgress = false;
		let states: StateInfo[] = [
			...this.eo.getNextStates().map(v => v as StateInfo)
		];
		let currentState = this.eo.getState();
		if (currentState && currentState.nuclosStateId) {
			states.push(
				{
					currentState: true,
					nuclosStateId: currentState.nuclosStateId,
					name: currentState.name,
					numeral: currentState.numeral ? currentState.numeral : 0
				}
			);
		}
		states.forEach((stateInfo) => {
			this.nuclosStateService.fetchInfo(stateInfo.nuclosStateId).pipe(
				take(1)
			).subscribe((stateRestInfo: StateInfo) => {
				stateInfo = {
					...stateInfo,
					...stateRestInfo,
					showNumeral: this.eo.getShowStateNumerals(),
					showIcon: this.eo.getShowStateIcons(),
					links: {
						stateIcon: {
							href: this.nuclosRestConfig.getStateIconURL(stateInfo.nuclosStateId),
							methods: ['GET']
						},
						buttonIcon: stateInfo.buttonIcon ? {
							href: this.nuclosRestConfig.getResourceURL(stateInfo.buttonIcon),
							methods: ['GET']
						} : undefined,
						change: {
							href: `${this.nuclosRestConfig.getRestHost()}/boStateChanges/${this.metaIdentifier}/${this.eo.getId()}/${stateInfo.nuclosStateId}`,
							methods: ['GET']
						}
					}
				};

				const stateIndex = this.currentAndNextStates.findIndex(s => s.nuclosStateId === stateInfo.nuclosStateId);
				if (stateIndex === -1) {
					this.currentAndNextStates.push(stateInfo);
					this.currentAndNextStates = this.currentAndNextStates.sort((a, b) => a.numeral - b.numeral);
				}
				/* Update the display mode (Combobox or individual buttons) depending on the statenumber and threshold. */
				this.updateDisplayMode();
				if (stateInfo.currentState) {
					this.currentState = stateInfo;
				}
			});
		});
	}

	private focusStateSelection() {
		if (this.isDisplayAsCombobox) {
			/* Opens the ComboBox and puts focus on the next state anchor element */
			this.focusNextCurrentStateInCombobox();
		} else {
			/* Puts focus on the next state button element */
			this.focusNextCurrentStateButton();
		}
	}

	private focusNextCurrentStateInCombobox() {
		const nextStates = this.currentAndNextStates.filter(s => !s.currentState);
		if (nextStates.length > 0) {
			const firstNextState = nextStates[0];
			if ($('a[state-number=' + firstNextState.numeral + ']').is(':hidden')) {
				$('#nuclosCurrentState').trigger('click');
			}
			$('a[state-number=' + firstNextState.numeral + ']').focus();
		}
	}

	private focusNextCurrentStateButton() {
		const nextStates = this.currentAndNextStates.filter(s => !s.currentState);
		if (nextStates.length > 0) {
			const firstNextState = nextStates[0];
			$('button[state-number=' + firstNextState.numeral + ']').focus();
		}
	}

}
