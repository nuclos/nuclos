import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CurrentStateComponent } from '@modules/state/buttons/current-state/current-state.component';
import { NextStateComponent } from '@modules/state/buttons/next-state/next-state.component';
import { DropdownStateComponent } from '@modules/state/dropdown/dropdown-state.component';
import { DropdownNextStateComponent } from '@modules/state/dropdown/next-state/dropdown-next-state.component';
import { SharedModule } from '@shared/shared.module';
import { I18nModule } from '../i18n/i18n.module';
import { ConfirmStateChangeComponent } from './confirm-state-change/confirm-state-change.component';
import { NuclosStateService } from './shared/nuclos-state.service';
import { StateComponent } from './state.component';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		I18nModule
	],
	declarations: [
		StateComponent,
		DropdownStateComponent,
		DropdownNextStateComponent,
		CurrentStateComponent,
		NextStateComponent,
		ConfirmStateChangeComponent,
	],
	providers: [
		NuclosStateService
	],
	exports: [
		StateComponent,
		NextStateComponent
	]
})
export class StateModule {
}
