import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { SwaggerUIBundle, SwaggerUIStandalonePreset } from 'swagger-ui-dist';
import * as urlParse from 'url-parse';
import { BusyService } from '../../../shared/service/busy.service';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { SystemParameter, SystemParameters } from '../../../shared/system-parameters';

const swaggerUIBundle = SwaggerUIBundle;
const swaggerUIStandalonePreset = SwaggerUIStandalonePreset;

@Component({
	selector: 'nuc-swagger-ui',
	templateUrl: './swagger-ui.component.html',
	styleUrls: ['./swagger-ui.component.css']
})
export class SwaggerUiComponent implements OnInit {
	private supportedSubmitMethods: string[] = [];

	constructor(
		private el: ElementRef,
		private config: NuclosConfigService,
		private busyService: BusyService,
		private http: HttpClient
	) {
	}

	ngOnInit() {
		this.config.getSystemParameters().pipe(
			take(1),
			map((params: SystemParameters) => {
				let supportedSubmitMethods: string[] = [];
				if (params.is(SystemParameter.ENVIRONMENT_DEVELOPMENT)) {
					// Allow to actually send requests via "Try it out" only in dev mode.
					supportedSubmitMethods = ['get', 'put', 'post', 'delete', 'options', 'head', 'patch', 'trace'];
				}

				return supportedSubmitMethods;
			})
		).subscribe((supportedSubmitMethods) => {
			this.supportedSubmitMethods = supportedSubmitMethods;
		});

		this.busyService.busy(
			this.http.get(
				this.config.getRestHost() + '/openapi.json'
			).pipe(
				take(1),
				map((spec: any) => {
					let parsedUrl = urlParse(this.config.getRestHost());
					if (spec && spec.servers) {
						for (let server of spec.servers) {
							server.url = parsedUrl.origin + server.url;
						}
					}
					return spec;
				})
			)
		).subscribe((spec: any) => {
			this.initSwagger(spec);
		});
	}

	private initSwagger(spec: any) {
		const ui = swaggerUIBundle({
			spec: spec || {},
			domNode: this.el.nativeElement.querySelector('.swagger-container'),

			// deepLinking is incompatible with the anchor based routing of Angular
			deepLinking: false,

			filter: true,
			presets: [
				swaggerUIBundle.presets.apis,
				swaggerUIStandalonePreset
			],
			docExpansion: 'none',
			plugins: [
				swaggerUIBundle.plugins.DownloadUrl
			],
			layout: 'BaseLayout',
			supportedSubmitMethods: this.supportedSubmitMethods,
			apisSorter: 'alpha',
			operationsSorter: 'alpha',
			tagsSorter: 'alpha'
		});

		// Hack to send all requests with credentials, because there is no configuration for it yet
		// See https://github.com/swagger-api/swagger-ui/issues/2895
		ui.fn.fetch.withCredentials = true;
	}
}
