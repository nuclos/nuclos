import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';
import { Logger } from '../../log/shared/logger';

@Component({
	selector: 'nuc-about-page',
	templateUrl: './about-page.component.html',
	styleUrls: ['./about-page.component.css']
})
export class AboutPageComponent implements OnInit {

	public loading = true;
	public title = '';
	public subTitle = '';
	public nuclets: Array<any>;
	public nuclosVersion = '';

	constructor(
		private log: Logger,
		private nuclosConfig: NuclosConfigService,
		private nuclosHttp: HttpClient,
		private nuclosI18N: NuclosI18nService
	) {
	}

	ngOnInit() {
		this.title = this.nuclosI18N.getI18n('webclient.about.page.loading.title');
		this.loading = true;
		this.nuclosHttp.get(this.nuclosConfig.getRestHost() + '/systeminformation')
			.pipe(take(1))
			.subscribe((data) => {
				this.loading = false;
				this.log.debug('Got system information: %o', data);
				this.analyzeData(data);
			});
	}

	private analyzeData(restData) {
		if (restData) {
			this.nuclets = (restData['nuclets'] as Array<any>);

			let masterNuclet = this.nuclets.find(value => value['masterNuclet']);

			if (masterNuclet) {
				this.title = masterNuclet['name'] + ' ' + masterNuclet['versionString'];
				this.subTitle = this.nuclosI18N.getI18n('webclient.about.page.loading.subtitle') + restData['nuclosVersion']['shortName'];
			} else {
				this.title = restData['nuclosVersion']['longName'];
			}

			this.nuclosVersion = restData['nuclosVersion']['longName'];
		}
	}
}
