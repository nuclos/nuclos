import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AboutPageComponent } from './about-page/about-page.component';
import { SystemInformationRoutes } from './system-information.routes';

@NgModule({
	declarations: [AboutPageComponent],
	imports: [
		CommonModule,
		SharedModule,
		SystemInformationRoutes
	]
})
export class SystemInformationModule {
}
