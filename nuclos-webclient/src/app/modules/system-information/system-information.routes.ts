import { RouterModule, Routes } from '@angular/router';
import { AboutPageComponent } from './about-page/about-page.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: '',
		component: AboutPageComponent
	}
];

export const SystemInformationRoutes = RouterModule.forChild(ROUTE_CONFIG);
