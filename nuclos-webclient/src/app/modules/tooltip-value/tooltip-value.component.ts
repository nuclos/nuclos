import { Component, Input } from '@angular/core';

@Component({
	selector: 'nuc-tooltip-value',
	template:
		'<div *ngIf="tooltip && tooltipId === undefined" hidden="true">{{tooltip}}</div>' +
		'<div *ngIf="tooltip && tooltipId !== undefined" hidden="true" id="tooltip-{{tooltipId}}">{{tooltip}}</div>'
})
/**
 * Provides a hidden div element with the tooltip VALUE only, e.g. for integration tests to validate -->
 */
export class TooltipValueComponent {
	@Input() tooltip: string;
	@Input() tooltipId: string;

	constructor() {
	}

}
