import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TooltipValueComponent } from './tooltip-value.component';

@NgModule({
	imports: [
		CommonModule,
	],
	declarations: [
		TooltipValueComponent
	],
	exports: [
		TooltipValueComponent
	]
})
export class TooltipValueModule {
}
