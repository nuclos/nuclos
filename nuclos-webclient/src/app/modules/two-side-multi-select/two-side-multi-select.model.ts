export interface Selectable {
	name: string;
	selected: boolean;
	marked?: boolean;
}
