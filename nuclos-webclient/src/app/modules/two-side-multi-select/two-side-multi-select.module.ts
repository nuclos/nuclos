import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DndModule } from 'ng2-dnd';
import { SharedModule } from '../../shared/shared.module';
import { I18nModule } from '../i18n/i18n.module';
import { TwoSideMultiSelectComponent } from './two-side-multi-select.component';

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		I18nModule,
		DndModule
	],
	declarations: [
		TwoSideMultiSelectComponent
	],
	exports: [
		TwoSideMultiSelectComponent
	]
})
export class TwoSideMultiSelectModule {
}
