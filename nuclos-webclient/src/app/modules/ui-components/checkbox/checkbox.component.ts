import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'nuc-checkbox',
	templateUrl: './checkbox.component.html',
	styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

	@Input()
	checkboxModel;

	@Input()
	disabled = false;

	constructor() {
	}

	ngOnInit() {
	}

}
