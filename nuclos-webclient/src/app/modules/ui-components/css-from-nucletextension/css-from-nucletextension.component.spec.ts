import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CssFromNucletextensionComponent } from './css-from-nucletextension.component';

xdescribe('CssFromNucletextensionComponent', () => {
	let component: CssFromNucletextensionComponent;
	let fixture: ComponentFixture<CssFromNucletextensionComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [CssFromNucletextensionComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CssFromNucletextensionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
