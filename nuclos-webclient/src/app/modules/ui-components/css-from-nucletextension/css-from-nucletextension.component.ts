import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import { NuclosConfigService } from '../../../shared/service/nuclos-config.service';

@Component({
	selector: 'nuc-css-from-nucletextension',
	templateUrl: './css-from-nucletextension.component.html',
	styleUrls: ['./css-from-nucletextension.component.css']
})
export class CssFromNucletextensionComponent implements OnInit {

	constructor(
		private config: NuclosConfigService
	) {
	}

	ngOnInit() {
		this.config.getNucletStyles().pipe(take(1)).subscribe(params => {
			if ('css' in params) {
				params['css'].forEach(css => {
					$('<style>')
						.attr('type', 'text/css')
						.text(css)
						.appendTo('head');
				});
			}
		});
	}

}
