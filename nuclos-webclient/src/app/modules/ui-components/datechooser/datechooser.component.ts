import {
	Component,
	ElementRef,
	EventEmitter,
	HostListener,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import {
	NgbDate,
	NgbDateParserFormatter,
	NgbDatepickerI18n,
	NgbDateStruct,
	NgbInputDatepicker
} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { NuclosI18nService } from '../../../core/service/nuclos-i18n.service';
import { DatetimeService } from '../../../shared/service/datetime.service';
import { Logger } from '../../log/shared/logger';
import { NuclosDatepickerI18n } from './nuclos-datepicker-i18n.class';
import { NuclosNgbDateParserFormatter } from './nuclos-ngb-date-parser-formatter';

@Component({
	selector: 'nuc-datechooser',
	templateUrl: './datechooser.component.html',
	styleUrls: ['./datechooser.component.css'],
	providers: [
		{
			provide: NgbDatepickerI18n,
			useClass: NuclosDatepickerI18n
		},
		{
			provide: NgbDateParserFormatter,
			useClass: NuclosNgbDateParserFormatter
		}
	]
})
export class DatechooserComponent implements OnInit, OnDestroy, OnChanges {
	// TODO: Implement custom ControlValueAccessor and use the ngModel mechanism,
	// see e.g. https://stackoverflow.com/questions/34948961/angular-2-custom-form-input/34998780#34998780
	@Input() model;

	@Input() id;
	@Input() nameForHtml;
	@Input() nextFocusField;
	@Input() nextFocusComp;
	@Input() initialFocus;
	@Input() enabled;
	@Input() cssClasses;
	@Input() inputStyle;

	dateModel: NgbDateStruct;
	datePattern: string;
	highlightDate = moment();

	@ViewChild('datepicker') datepicker: NgbInputDatepicker;
	@ViewChild('datepickerInput') datepickerInput: ElementRef;

	@Output() onPickerClose = new EventEmitter();
	@Output() modelChange = new EventEmitter<string | null>();

	private unsubscribe$ = new Subject<boolean>();
	private bInitialFocus = true;

	constructor(
		private datetimeService: DatetimeService,
		private dateParser: NgbDateParserFormatter,
		private nuclosI18n: NuclosI18nService,
		private $log: Logger
	) {
		this.datePattern = this.datetimeService.getDatePattern();
		this.modelChange.pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$)).subscribe(value => {
			if (!this.isInputFocused()) {
				this.highlightDate = moment();
				return;
			}

			const parsedDateFromValue = this.datetimeService.parseDate(value);
			if (parsedDateFromValue !== null) {
				this.highlightDate = parsedDateFromValue;
				let yearMonth = {
					year: this.highlightDate.year(),
					month: this.highlightDate.month() + 1
				};

				this.$log.debug('Navigating to date %o', yearMonth);
				this.datepicker.navigateTo(yearMonth);
			}
		});
	}

	ngOnInit() {}

	ngOnDestroy() {
		this.unsubscribe$.next(true);
		this.unsubscribe$.complete();
	}

	ngOnChanges(changes: SimpleChanges): void {
		let change = changes['model'];
		if (change) {
			// this.rawInput = change.currentValue;
		}
	}

	getDateModel(): NgbDateStruct | undefined {
		this.updateModel();

		return this.dateModel;
	}

	mousedown($event: MouseEvent) {
		if ($event.button === 0) {
			this.openDatepicker();
		}
	}

	openDatepicker() {
		this.datepicker.open();
		this.datepicker.closed = this.onPickerClose;

		// Re-focus the input, because toggling the datepicker steals the focus
		this.focusInput();
	}

	closeDatepicker() {
		if (this.datepicker.isOpen()) {
			this.datepicker.close();
		}
	}

	focusInput() {
		// Using setTimeout() to regain the focus after the datepicker panel stole it.
		setTimeout(() => {
			if (this.datepickerInput) {
				this.datepickerInput.nativeElement.focus();
			}
		});
	}

	isInputFocused() {
		return this.datepickerInput && $(this.datepickerInput.nativeElement).is(':focus');
	}

	/**
	 * EvaluateClickOutside
	 * prevent closing datepicker, when clicking on it
	 * @return true when not clicking on datepicker
	 */
	isClickOutside(target: EventTarget) {
		return $(target).closest('ngb-datepicker').length === 0;
	}

	isHighlighted(date: { day: number; month: number; year: number }) {
		return (
			this.highlightDate.date() === date.day &&
			this.highlightDate.month() + 1 === date.month &&
			this.highlightDate.year() === date.year
		);
	}

	getFormattedDate() {
		return this.datetimeService.formatDate(this.model);
	}

	selectInputText() {
		this.datepickerInput.nativeElement.select();
	}

	setInputText(value: any) {
		if (this.datepickerInput.nativeElement !== undefined) {
			this.datepickerInput.nativeElement.value = value;
		}
	}

	isValid(): boolean {
		let value = this.getRawInput();
		let date = this.dateParser.parse(value);
		return !value || (date !== null && (!isNaN(date.day) && !isNaN(date.month) && !isNaN(date.year)));
	}

	isDirty(): boolean {
		let input = this.getRawInput();
		return !!input;
	}

	getRawInput() {
		return this.datepickerInput && this.datepickerInput.nativeElement.value;
	}

	@HostListener('keydown', ['$event'])
	closeDatepickerOnTabKey($event) {
		if ($event.key === 'Tab') {
			// can be undefined on disabled inputs which are tab focused
			this.datepicker?.close();
		}
	}

	commitValue() {
		this.datepickerInput.nativeElement.blur();
	}

	isEnabled() {
		return this.enabled === true || this.enabled === undefined;
	}

	setModelValue(value?: NgbDate) {
		let valueString: string | null = null;

		// Use raw input while the user is still typing, or when the datepicker submits no value for some reason...
		if (!value || this.isInputFocused()) {
			if (this.getRawInput() !== '') {
				const parsedDateFromInput = this.datetimeService.parseDate(this.getRawInput());
				// try to parse RawInput if it fails push its value raw
				if (parsedDateFromInput !== null) {
					valueString = parsedDateFromInput.format(
						'YYYY-MM-DD'
					);
				} else {
					valueString = this.getRawInput();
				}
			}
		} else {
			if (
				value &&
				value.year !== undefined &&
				value.month !== undefined &&
				value.day !== undefined
			) {
				// value was set from datepicker component
				let date = moment(new Date(value.year, value.month - 1, value.day)).format(
					'YYYY-MM-DD'
				);

				valueString = date;
			}
		}

		this.modelChange.emit(valueString);
	}

	blur() {
		if (!this.datepicker.isOpen()) {
			this.bInitialFocus = true;
		}
	}

	clickOutside(target: EventTarget) {
		if (this.isClickOutside(target)) {
			this.closeDatepicker();
			this.commitValue();
			$(target).focus();
		}
	}

	private getNewDateModel() {
		let value = this.model;
		let newDateModel;

		if (value) {
			let date = moment(value);
			newDateModel = new NgbDate(date.year(), date.month() + 1, date.date());
		}

		return newDateModel;
	}

	private updateModel() {
		// Do not overwrite input value while the input is focused
		if (this.isInputFocused()) {
			return;
		}

		let newDateModel = this.getNewDateModel();

		if (!this.modelEquals(this.dateModel, newDateModel)) {
			this.dateModel = newDateModel;
		}
	}

	private modelEquals(model1: NgbDateStruct | undefined, model2: NgbDateStruct | undefined) {
		if (!model1 && !model2) {
			return true;
		} else if (model1 && model2) {
			return (
				model1.year === model2.year &&
				model1.month === model2.month &&
				model1.day === model2.day
			);
		} else {
			return false;
		}
	}
}
