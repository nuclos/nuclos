import {
	Component,
	ElementRef,
	EventEmitter,
	forwardRef,
	HostBinding,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as _ from 'lodash';
import { AutoComplete } from 'primeng/autocomplete';
import { IdFactoryService } from '@shared/service/id-factory.service';
import { StringUtils } from '@shared/string-utils';
import { LovEntry } from '@modules/entity-object-data/shared/bo-view.model';
import { ClickOutsideService } from '../../click-outside/click-outside.service';
import { Logger } from '../../log/shared/logger';

@Component({
	selector: 'nuc-dropdown',
	templateUrl: './dropdown.component.html',
	styleUrls: ['./dropdown.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => DropdownComponent),
			multi: true
		}
	]
})
export class DropdownComponent implements OnInit, ControlValueAccessor, OnChanges {
	@HostBinding('attr.for-id')
	@Input()
	id;

	@Input() dropdown = false;
	@Input() suggestions: any[] = [];
	@Input() field;
	@Input() selectedField;
	@Input() dirtyField;
	@Input() secondaryField;
	@Input() placeholder;
	@Input() appendTo = undefined;
	@Input() componentId;
	@Input() multiple = false;
	@Input() optional = false;
	@Input() disabled = false;
	@Input() htmlValues = false;
	@Input() internalFiltering = true;

	@Output() onSelect = new EventEmitter();
	@Output() completeMethod = new EventEmitter();
	@Output() onDropDownClick = new EventEmitter();

	@ViewChild('autoComplete', {static: true}) autoComplete: AutoComplete;

	value: any;
	_onChange: any;
	_onTouched: any;

	/**
	 * If the Input "optional" is set, this value will be initialised and added as first, empty option.
	 */
	emptyValue;

	filterString;
	filteredSuggestions: any[] = [];

	constructor(
		private elementRef: ElementRef,
		private clickOutsideService: ClickOutsideService,
		private idFactory: IdFactoryService,
		private $log: Logger
	) {}

	ngOnInit() {
		if (!this.id) {
			this.id = this.idFactory.getNextId();
		}
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes['suggestions']) {
			this.updateFilteredSuggestions();
		}
	}

	outsideClick(target: EventTarget) {
		if (
			target !== this.elementRef.nativeElement &&
			this.autoComplete && !this.autoComplete.el.nativeElement.contains(target)
		) {
			this.autoComplete.hide();
		}
	}

	getId() {
		return this.id;
	}

	writeValue(obj: any): void {
		this.setValue(obj);
		const inputEl = this.getInputElement();
		if (inputEl &&
			(Object.prototype.toString.call(obj) !== '[object Array]') &&
			(Object.prototype.toString.call(obj) !== '[object Object]')) {
			inputEl.value = obj;
		}
	}

	registerOnChange(fn: (_: any) => void): void {
		this._onChange = fn;
	}

	registerOnTouched(fn: any): void {
		this._onTouched = fn;
	}

	focusInput() {
		this.autoComplete.focusInput();
	}

	selectEntry(value) {
		if (this.isDottedListEndClicked(value)) {
			this.autoComplete.value = this.value;
			return;
		}

		if (value === this.emptyValue) {
			value = undefined;
		}
		this.onSelect.emit(value);
	}

	setValue(value) {
		if (this.isDottedListEndClicked(value)) {
			this.autoComplete.value = this.value;
			return this.value;
		}

		if (value === this.emptyValue) {
			value = undefined;
		}
		let oldValue = this.value;
		this.value = value;
		if (this.value !== oldValue && this._onChange) {
			this._onChange(value);
		}
		return value;
	}

	onComplete($event: any, filter: any) {
		if ($event === undefined) {
			this.filterString = filter;
			this.updateFilteredSuggestions();

			this.completeMethod.emit(filter);
		}
	}

	keyup($event: KeyboardEvent | undefined) {
		// The autocomplete component does not trigger a search, if there is no input
		let element = this.getInputElement();
		if ($event === undefined) {
			if (element && $(element).val() === '') {
				this.showAllEntries();
			} else {
				this.showFilteredEntries($(element).val());
			}
		} else if ($event.code !== 'ArrowDown' && $event.code !== 'ArrowUp' && $event.code !== 'Escape') {
			this.onComplete(undefined, $(element).val());
		}
	}

	/**
	 * fix for https://github.com/primefaces/primeng/issues/745
	 */
	showAllEntries() {
		this.$log.warn('show all lov entries');
		if (this.disabled) {
			return;
		}

		this.onComplete(undefined, '');

		this.showResultPanel();
	}

	showFilteredEntries(filter: string) {
		this.$log.warn('show filtered lov entries: %s', filter);
		if (this.disabled) {
			return;
		}

		this.onComplete(undefined, filter);
		this.showResultPanel();
	}

	openLovPanel($event) {
		this.$log.warn('open lov panel: %o', $event);
		if ($event) {
			// We handle the dropdown panel. Prevent the event from reaching the autocomplete component:
			let eventObject = $event;
			if (typeof eventObject.originalEvent === 'object') {
				eventObject = eventObject.originalEvent;
			}
			if (typeof eventObject.preventDefault === 'function') {
				eventObject.preventDefault();
			}
			if (typeof eventObject.stopPropagation === 'function') {
				eventObject.stopPropagation();
			}
		}

		if (!this.isLovPanelVisible()) {
			// select lov input text
			let textInput = this.getInputElement();
			if (textInput && textInput.selectionEnd - textInput.selectionStart === 0) {
				textInput.setSelectionRange(0, textInput.value.length);
			}
		}

		this.keyup(undefined);
	}

	hideLovPanel() {
		if (this.autoComplete && this.autoComplete.overlayVisible) {
			this.autoComplete.hide();
		}
	}

	isLovPanelVisible(): boolean {
		if (this.autoComplete) {
			return this.autoComplete.overlayVisible;
		}
		return false;
	}

	toggleLovPanel($event, force?: boolean) {
		if (this.isLovPanelVisible() && !force) {
			this.hideLovPanel();
		} else {
			this.openLovPanel(event);
		}

		setTimeout(() => {
			this.getInputElement()?.focus();
		});

		if (force) {
			this.onDropDownClick.emit($event);
		}
	}

	dropdownArrowClick($event) {
		this.$log.warn('lov dropdownarrowclick %o', $event);
		if (this.autoComplete.overlayVisible) {
			this.autoComplete.hide();
			return;
		}

		this.openLovPanel($event);
		this.autoComplete.handleDropdownClick($event);
	}

	onBlur() {
		if (this._onTouched) {
			this._onTouched();
		}
	}

	clearInput() {
		this.setInput('');
	}

	setInput(input: string) {
		$(this.getInputElement()).val(input);
	}

	resolveFieldData(option: any, field: any): any {
		return option[field];
	}

	isSelected(option: any, field: any): any {
		return option[field];
	}

	isDirty(option: any, field: any): any {
		return option[field];
	}

	isSecondary(option: any, field: any): any {
		return option[field];
	}

	setupPanelAndInput() {
		let element = this.autoComplete.overlay;
		element.setAttribute('for-id', this.getId());

		let inputEl = this.getInputElement();
		// if (inputEl) {
		// 	let inputWidth = $(inputEl).outerWidth();
		// 	$(element).outerWidth(inputWidth);
		// }

		// if (!this.keydownEventRegistered) {
		// 	this.keydownEventRegistered = true;
		//
		// 	inputEl.addEventListener('keydown', event => {
		// 		if (this.handler.keydownHandler) {
		// 			this.handler.keydownHandler(event);
		// 		}
		// 	});
		// }
	}

	updateAutoCompleteMessage(message: string, loading?: boolean): void {
		if (this.autoComplete) {
			this.autoComplete.emptyMessage = message;

			if (loading) {
				this.autoComplete.loading = loading;
			} else {
				this.autoComplete.loading = false;
			}

			if (!this.isLovPanelVisible()) {
				this.autoComplete.show();
			}

		}
	}

	/**
	 * This is only used for determining a click on the last line if we have MAX_SIZE elements
	 * in list. So the user has some notice and the remaining code does not care about this entry.
	 * @param value - AutoComplete Event Value to check
	 * @return boolean - true if found otherwise false
	 */
	isDottedListEndClicked(value: LovEntry | LovEntry[] | undefined): boolean {
		if (value instanceof Array) {
			// ngModelChange
			if (value?.find(e => e.id === null && e.name === '...') !== undefined) {
				return true;
			}
		} else {
			if (value?.id === null && value?.name === '...') {
				return true;
			}
		}

		return false;
	}

	private showResultPanel() {
		this.autoComplete.show();
	}

	private updateFilteredSuggestions() {
		if (this.internalFiltering) {
			this.filteredSuggestions = this.suggestions ? [...this.filterSuggestions(this.suggestions)] : [];
			if (this.optional && !this.filterString) {
				this.emptyValue = {};
				if (this.field) {
					this.emptyValue[this.field] = '';
				} else {
					this.emptyValue = '';
				}
				this.filteredSuggestions.unshift(this.emptyValue);
			}
		} else {
			this.filteredSuggestions = this.suggestions ? [...this.suggestions] : [];
		}
	}

	private filterSuggestions(suggestions: any[]) {
		if (!this.filterString) {
			return _.clone(suggestions);
		}

		let regex = StringUtils.regexFromSearchString(this.filterString);

		return suggestions.filter(it => {
			if (typeof it === 'string') {
				return it.match(regex);
			} else if (it && this.field) {
				let value = it[this.field];
				return ('' + value).match(regex);
			} else {
				return ('' + it).match(regex);
			}
		});
	}

	private getInputElement(): any | undefined {
		return this.autoComplete?.inputEL?.nativeElement ?? this.autoComplete?.multiInputEL?.nativeElement;
	}
}
