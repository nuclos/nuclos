import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { NuclosDialogService } from '../../../core/service/nuclos-dialog.service';
import { IconPickerModalComponent } from './icon-picker-modal.component';

@Component({
	selector: 'nuc-icon-picker',
	templateUrl: './icon-picker.component.html',
	styleUrls: ['./icon-picker.component.css']
})
export class IconPickerComponent {

	@Input()
	pickerLabel: string;

	@Input()
	disabled = false;

	@Input()
	selectedIcon: string;

	@Output()
	selectedIconChange = new EventEmitter<any>();

	constructor(
		private modalService: NuclosDialogService
	) {
	}

	iconSelected() {
	}

	selectIcon() {
		this.showIconPicker().pipe(take(1)).subscribe(
			result => {
				this.selectedIcon = result ? result.name : undefined;
				if (this.selectedIconChange) {
					this.selectedIconChange.emit(this.selectedIcon);
				}
			}
		);
	}

	private showIconPicker(): Observable<any> {
		return this.modalService.custom(
			IconPickerModalComponent,
			{
				'selectedIcon': this.selectedIcon
			},
			{
				size: 'lg'
			}
		).pipe(
			take(1)
		);
	}

}
