import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TabBarComponent } from './tab-bar.component';

xdescribe('TabBarComponent', () => {
	let component: TabBarComponent;
	let fixture: ComponentFixture<TabBarComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [TabBarComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TabBarComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
