import {
	AfterContentInit,
	Component,
	ContentChildren,
	EventEmitter,
	forwardRef,
	Input,
	OnChanges,
	OnInit,
	Output,
	QueryList,
	SimpleChanges,
	TemplateRef,
	ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TabView } from 'primeng/tabview';
import { Logger } from '../../log/shared/logger';
import { TabComponent } from './tab/tab.component';

@Component({
	selector: 'nuc-tab-bar',
	templateUrl: './tab-bar.component.html',
	styleUrls: ['./tab-bar.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => TabBarComponent),
			multi: true
		}
	]
})
export class TabBarComponent implements ControlValueAccessor, OnInit, OnChanges, AfterContentInit {
	@Input() canAdd = false;
	@Input() noHeaderPadding = false;
	@Input() values: any[];
	@Input() titleTemplate: TemplateRef<any>;
	@Input() contentTemplate: TemplateRef<any>;
	@Input() idForAdd: string;
	@Input() dynamicHeight = true;

	@Output() addValue = new EventEmitter();
	@Output() changed = new EventEmitter();

	@ViewChild('tabView') tabView: TabView;
	@ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

	addTabId = 'tab-add';

	/**
	 * The selected value (which is displayed in the currently selected tab).
	 */
	value: any;

	_onChange: any;
	_onTouched: any;
	disabled = false;

	selectedTabIndex?: number;

	constructor(private log: Logger) {
	}

	ngOnInit() {}

	ngOnChanges(changes: SimpleChanges): void {}

	ngAfterContentInit() {
		setTimeout(() => {
			this.deselectAddTab();
		});
	}

	tabId(index: number) {
		return 'tab-' + index;
	}

	addTab(event) {
		event.stopPropagation();
		event.preventDefault();
		this.addValue.emit('');
		this.deselectAddTab();
	}

	emitTabChange(event: { originalEvent: Event, index: number }) {
		if (event && this.values && event.index < this.values.length)  {
			this.selectedTabIndex = event.index;

			let selectedValue = this.getSelectedValue();
			this.changed.next(selectedValue);
			if (this._onChange) {
				this._onChange(selectedValue);
			}
		}
	}

	registerOnChange(fn: any): void {
		this._onChange = fn;
	}

	registerOnTouched(fn: any): void {
		this._onTouched = fn;
	}

	setDisabledState(isDisabled: boolean): void {
		this.disabled = isDisabled;
	}

	writeValue(obj: any): void {
		this.value = obj;
		this.selectTab(this.value);
	}

	private deselectAddTab() {
		let addTab = this.tabView?.tabs?.find(tab => tab.header === this.addTabId);
		if (addTab) {
			setTimeout(() => {
				if (addTab) {
					addTab.selected = false;
				}
			});
		}
	}

	private getSelectedValue() {
		// Selected tab index corresonds to value index
		let selectedValue = '';
		if (this.values && this.selectedTabIndex !== undefined && this.selectedTabIndex >= 0 && this.selectedTabIndex < this.values.length) {
			selectedValue = this.values[this.selectedTabIndex];
		}

		return selectedValue;
	}

	private selectTab(tabValue: any) {
		if (this.values && tabValue) {
			this.selectedTabIndex = this.values.indexOf(tabValue);
		}
	}
}
