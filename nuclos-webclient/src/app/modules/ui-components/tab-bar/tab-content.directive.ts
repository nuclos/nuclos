import { Directive, TemplateRef } from '@angular/core';

@Directive({
	selector: '[nucTabContent]'
})
export class TabContentDirective {

	constructor(
		public templateRef: TemplateRef<any>
	) {
	}

}
