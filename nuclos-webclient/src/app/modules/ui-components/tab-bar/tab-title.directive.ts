import { Directive, TemplateRef } from '@angular/core';

@Directive({
	selector: '[nucTabTitle]'
})
export class TabTitleDirective {

	constructor(
		public templateRef: TemplateRef<any>
	) {
	}

}
