import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
	selector: 'nuc-textarea',
	templateUrl: './textarea.component.html',
	styleUrls: ['./textarea.component.css']
})
export class TextareaComponent implements OnInit {


	@Input()
	ngModel;

	val;


	@Output() ngModelChange = new EventEmitter();

	// get ngModel() {
	// 	return this.val;
	// }
	//
	// set ngModel(val) {
	// 	this.val = val;
	// 	this.ngModelChange.emit(this.val);
	// }


	@Input()
	disabled = false;


	constructor() {
	}

	ngOnInit() {
	}

}
