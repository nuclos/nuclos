import { TooltipOptions } from 'primeng/tooltip/tooltip';

export const TooltipDefaultOptions: TooltipOptions = {
	tooltipStyleClass: 'theme-light',
	tooltipZIndex: '999',
	showDelay: 750,
	hideDelay: 250,
	tooltipPosition: 'top',
	// a bit unsafe but needed for Nuclos special features
	escape: false
};
