import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { SharedModule } from '@shared/shared.module';
import { ClickOutsideModule } from '../click-outside/click-outside.module';
import { LogModule } from '../log/log.module';
import { CssFromSystemparameterComponent } from './css-from-systemparameter/css-from-systemparameter.component';
import { CssFromNucletextensionComponent } from './css-from-nucletextension/css-from-nucletextension.component';
import { DatechooserComponent } from './datechooser/datechooser.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { IconPickerModalComponent } from './icon-picker/icon-picker-modal.component';
import { IconPickerComponent } from './icon-picker/icon-picker.component';
import { TabBarComponent } from './tab-bar/tab-bar.component';
import { TabContentDirective } from './tab-bar/tab-content.directive';
import { TabTitleDirective } from './tab-bar/tab-title.directive';
import { TabComponent } from './tab-bar/tab/tab.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		SharedModule,
		ClickOutsideModule,
		LogModule,
		AutoCompleteModule,
		NgbModule,
		NgbModalModule
	],
	declarations: [
		CssFromSystemparameterComponent,
		CssFromNucletextensionComponent,
		DatechooserComponent,
		TabBarComponent,
		TabComponent,
		TabContentDirective,
		TabTitleDirective,
		DropdownComponent,
		IconPickerComponent,
		IconPickerModalComponent
	],
	exports: [
		CssFromSystemparameterComponent,
		CssFromNucletextensionComponent,
		DatechooserComponent,
		DropdownComponent,
		TabBarComponent,
		TabComponent,
		TabContentDirective,
		TabTitleDirective,
		IconPickerComponent
	],
	providers: []
})
export class UiComponentsModule {
}
