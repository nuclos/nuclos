/* tslint:disable */

interface ObjectFactory {
}

interface Parameter extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    name: string;
    value: string;
}

interface ValueListProvider extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    parameters: Parameter[];
    attribute: string;
    name: string;
    type: string;
    value: string;
    idFieldname: string;
    fieldname: string;
    defaultFieldname: string;
}

interface ValueListProviders extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    vlps: ValueListProvider[];
}

interface Serializable {
}

interface Cloneable {
}

interface CopyTo {
}

interface Equals {
}

interface HashCode {
}

interface ToString {
}
