export class ArrayUtils {

	/**
	 * move an array element from one position to another
	 * @param array
	 * @param fromIndex
	 * @param toIndex
	 */
	static move(array: any[], fromIndex: number, toIndex: number): void {
		let element = array[fromIndex];
		array.splice(fromIndex, 1);
		array.splice(toIndex, 0, element);
	}

	static isIterable(obj) {
		// checks for null and undefined
		if (obj == null) {
			return false;
		}
		return typeof obj[Symbol.iterator] === 'function';
	}

}
