export class ColorUtils {

	static calculateTextColorFromBackgroundColor(color: string | undefined) {
		if (color !== undefined && color !== null && color.length === 7) {
			const red = parseInt(color.substr(1, 2), 16);
			const green = parseInt(color.substr(3, 2), 16);
			const blue = parseInt(color.substr(5, 2), 16);

			const brightness = (red * 2 + blue + green * 3) / 6;

			if (brightness > 160) {
				// dark color
				return '#000000';
			} else {
				// bright color
				return '#FFFFFF';
			}
		}
		return '#000000';
	}

}
