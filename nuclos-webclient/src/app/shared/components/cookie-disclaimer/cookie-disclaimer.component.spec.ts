import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CookieDisclaimerComponent } from './cookie-disclaimer.component';

xdescribe('CookieDisclaimerComponent', () => {
	let component: CookieDisclaimerComponent;
	let fixture: ComponentFixture<CookieDisclaimerComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [CookieDisclaimerComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CookieDisclaimerComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
