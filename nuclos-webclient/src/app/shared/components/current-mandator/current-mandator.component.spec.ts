import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CurrentMandatorComponent } from './current-mandator.component';

xdescribe('CurrentMandatorComponent', () => {
	let component: CurrentMandatorComponent;
	let fixture: ComponentFixture<CurrentMandatorComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
			declarations: [CurrentMandatorComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CurrentMandatorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
