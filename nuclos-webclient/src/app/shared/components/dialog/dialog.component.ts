import { Component, Input, ViewChild } from '@angular/core';
import { TooltipDefaultOptions } from '@modules/ui-components/tooltip/tooltip-default-options';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DialogButton } from '@nuclos/nuclos-addon-api';
import { StringUtils } from '@shared/string-utils';
import { TooltipOptions } from 'primeng/tooltip/tooltip';
import { DialogOptions } from '../../../data/schema/dialog.model';

@Component({
	selector: 'nuc-dialog',
	templateUrl: './dialog.component.html',
	styleUrls: ['./dialog.component.css']
})
export class DialogComponent {

	@Input() options: DialogOptions;
	@Input() buttonOptions: DialogButton[];

	@ViewChild('messageContent') messageContent;
	tooltipDefaults: TooltipOptions = TooltipDefaultOptions;

	constructor(
		private activeModal: NgbActiveModal
	) {
	}

	close(): void {
		this.activeModal.dismiss();
	}

	copyTextToClipboard(): void {
		if (this.messageContent) {
			StringUtils.copyToClipboard(this.messageContent.nativeElement.innerText);
		}
	}
}
