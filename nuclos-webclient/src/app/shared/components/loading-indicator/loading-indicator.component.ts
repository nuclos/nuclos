import { HTTP_INTERCEPTORS, HttpInterceptor } from '@angular/common/http';
import { Component, Inject, Input, NgZone, OnDestroy, OnInit } from '@angular/core';
import { NuclosHttpInterceptor } from '../../../core/interceptors/nuclos-http-interceptor';

@Component({
	selector: 'nuc-loading-indicator',
	templateUrl: './loading-indicator.component.html',
	styleUrls: ['./loading-indicator.component.css']
})
export class LoadingIndicatorComponent implements OnInit, OnDestroy {
	imageSrc: string;

	@Input()
	private thresholdInMilliseconds = 1000;
	private idleImageSrc = 'assets/nuclos-signet.png';
	private loadingImageSrc = 'assets/nuclos-rotating-48.gif';
	private httpInterceptor: NuclosHttpInterceptor | undefined;

	private intervalUpdate: NodeJS.Timeout | undefined;

	constructor(
		@Inject(HTTP_INTERCEPTORS) private interceptors: HttpInterceptor[]
		, private ngZone: NgZone) {
		this.httpInterceptor = this.interceptors.find(x => x instanceof NuclosHttpInterceptor) as NuclosHttpInterceptor;
		this.imageSrc = this.idleImageSrc;
	}

	ngOnInit() {
		// Updating the image every 100ms, because Angular change detection is not called often enough for this.
		this.ngZone.runOutsideAngular(() => {
			if (this.intervalUpdate === undefined) {
				this.intervalUpdate = setInterval(() => {
					this.ngZone.run(() => this.updateImageSrc());
				}, 100);
			}
		});
	}

	ngOnDestroy() {
		if (this.intervalUpdate) {
			clearInterval(this.intervalUpdate);
		}
	}

	updateImageSrc() {
		if (this.showLoading()) {
			this.setImageSrc(this.loadingImageSrc);
		} else {
			this.setImageSrc(this.idleImageSrc);
		}
	}

	setImageSrc(newSrc: string) {
		this.imageSrc = newSrc;
	}

	private showLoading() {
		let pendingSince = this.httpInterceptor ? this.httpInterceptor.getPendingRequestTime() : -1;
		return pendingSince >= this.thresholdInMilliseconds;
	}
}
