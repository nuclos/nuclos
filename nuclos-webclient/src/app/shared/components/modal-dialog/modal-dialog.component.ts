import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'nuc-modal-dialog',
	templateUrl: './modal-dialog.component.html',
	styleUrls: ['./modal-dialog.component.css']
})
export class ModalDialogComponent implements OnInit {

	@Input() title: string;
	@Input() headerTemplate: TemplateRef<any>;
	@Input() footerTemplate: TemplateRef<any>;

	constructor(
		private activeModal: NgbActiveModal
	) {
	}

	ngOnInit() {
	}

	close(result?) {
		this.activeModal.close(result);
	}
}
