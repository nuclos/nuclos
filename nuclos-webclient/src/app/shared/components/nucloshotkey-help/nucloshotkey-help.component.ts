import { AfterContentInit, AfterViewInit, Component, HostListener, Input, OnInit } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import { Options } from '../../../data/schema/types.intern';

@Component({
	selector: 'nuc-nucloshotkey-help',
	templateUrl: './nucloshotkey-help.component.html',
	styleUrls: ['./nucloshotkey-help.component.css']
})
export class NucloshotkeyHelpComponent implements OnInit {

	@Input() dialogId: number | undefined;
	@Input() hotkeys: Array<Options>;

	showStartIndex = 0;
	pagingNumber = 5;
	filterText = '';
	searching = false;
	arrayOfPages: Array<number>;

	_hotkeys: Array<Options> = [];

	constructor(private eventManager: EventManager) {
		this.eventManager.addGlobalEventListener('window', 'resize', () => {
			this.initView();
		});
	}

	getHotkeys(): Array<Options> {
		if (this._hotkeys.length === 0) {
			let includes = false;
			for (let hotkey of this.hotkeys) {
				for (let _hotkey of this._hotkeys) {
					if (_hotkey.keys === hotkey.keys
					&& _hotkey.modifier === hotkey.modifier
					&& _hotkey.webComponent === hotkey.webComponent) {
						includes = true;
						break;
					}
				}
				if (!includes) {
					this._hotkeys.push(hotkey);
				}
			}
		}

		return this._hotkeys;
	}

	// get's only called by view if keyStrokeModifier is set
	// so we can assume it is there at this point
	getModifierStringRepresentation(menuItem: Options): string {
		if (menuItem.modifier === 'CTRL_SHIFT') {
			return 'CTRL + SHIFT';
		} else if (menuItem.modifier === 'ALT_SHIFT') {
			return 'ALT + SHIFT';
		} else if (menuItem.modifier === 'CTRL_ALT') {
			return 'CTRL + ALT';
		} else if (menuItem.modifier === 'CTRL_ALT_SHIFT') {
			return 'CTRL + ALT + SHIFT';
		}

		return menuItem.modifier!;
	}

	navigateToIndex(event: Event, number: number) {
		if (number >= 0 && number <= this.getHotkeys().length) {
			this.showStartIndex = number;
		}

		event.stopPropagation();
		event.preventDefault();
	}

	ngOnInit(): void {
		this.initView();
	}

	filterItems(filterText: string) {
		this.searching = true;
		this._hotkeys = [];
		if (filterText !== '') {
			this._hotkeys = [...this.getHotkeys().filter((key) => key.description?.includes(filterText))];
		}
		this.initView();
		this.searching = false;
	}

	private initView(): void {
		this.pagingNumber = Math.ceil(($('div.modal-content').height() - 250) / 40);
		this.arrayOfPages = Array<number>(Math.ceil(this.getHotkeys().length / this.pagingNumber)).fill(0);
	}
}
