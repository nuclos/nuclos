import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef,
	Input,
	ViewEncapsulation
} from '@angular/core';
import { Splitter } from 'primeng/splitter';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'prime-custom-splitter',
	template: `
        <div #container [ngClass]="containerClass()" [class]="styleClass" [ngStyle]="style">
            <ng-template ngFor let-panel let-i="index" [ngForOf]="panels">
                <div [ngClass]="panelContainerClass()" [class]="panelStyleClass" [ngStyle]="panelStyle">
                    <ng-container *ngTemplateOutlet="panel"></ng-container>
                </div>
                <div class="p-splitter-gutter" *ngIf="i !== (panels.length - 1)" [ngStyle]="gutterStyle()"
                    (mousedown)="onGutterMouseDown($event, i)" (touchstart)="onGutterTouchStart($event, i)">
					<div *ngIf="collapsable">
						<button class="btn btn-link p-1" (click)="collapsClicked($event, i, true)">
							<i class="fas"
							   [ngStyle]="fontSizeButtons()"
							   [ngClass]="iconClassLeft()"></i>
						</button>
					</div>
                    <div class="p-splitter-gutter-handle"></div>
	                <div *ngIf="collapsable">
		                <button class="btn btn-link p-1" (click)="collapsClicked($event, i, false)">
			                <i class="fas"
			                   [ngStyle]="fontSizeButtons()"
							   [ngClass]="iconClassright()"></i>
		                </button>
	                </div>
                </div>
            </ng-template>
        </div>
    `,
	styleUrls: ['./prime-custom-splitter.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush,
	// tslint:disable-next-line:no-host-metadata-property
	host: {
		'class': 'p-element',
		'[class.p-splitter-panel-nested]': 'nested'
	}
})
export class PrimeCustomSplitterComponent extends Splitter {

	@Input() collapsable = true;

	private collapseClicked = false;

	collapsClicked(event: any, index: number, left = false) {
		this.collapseClicked = true;

		const gutterElement = $(event.currentTarget).parent().parent().get(0);
		if (gutterElement !== null && gutterElement !== undefined) {
			gutterElement!.previousElementSibling!['style'].flexBasis =
				'calc(' + (left ? 0 : 100) + '% - ' + ((this.panels.length - 1) * this.gutterSize) + 'px)';
			gutterElement!.nextElementSibling!['style'].flexBasis =
				'calc(' + (left ? 100 : 0) + '% - ' + ((this.panels.length - 1) * this.gutterSize) + 'px)';
			this._panelSizes[index] = left ? 0 : 100;
			this._panelSizes[index + 1] = left ? 100 : 0;
		}
		this.onResizeEnd.emit({originalEvent: event, sizes: this._panelSizes});
		this.collapseClicked = false;
	}

	applyPanelsizes(sizes: number[]) {
		let panels = $(this.containerViewChild.nativeElement).children('div.p-splitter-panel');
		for (let index = 0; index < panels.length; index++) {
			let panel = panels.get(index);
			if (panel !== null && panel !== undefined && sizes.length > index) {
				panel['style'].flexBasis =
					'calc(' + sizes[index] + '% - ' + ((this.panels.length - 1) * this.gutterSize) + 'px)';
				this._panelSizes[index] = sizes[index];
			}
		}
		this.onResizeEnd.emit({originalEvent: null, sizes: this._panelSizes});
	}

	onGutterMouseDown(event: any, index: any) {
		if (!this.collapseClicked) {
			super.onGutterMouseDown(event, index);
		}
	}

	onGutterTouchStart(event: any, index: any) {
		if (!this.collapseClicked) {
			super.onGutterTouchStart(event, index);
		}
	}

	gutterStyle(): any {
		let style = super.gutterStyle();

		if (this.horizontal()) {
			style = {
				...style,
				'flex-direction': 'column'
			} as any;
		}

		return style;
	}

	iconClassLeft(): any {
		return this.horizontal() ? 'fa-angle-double-left' : 'fa-angle-double-up';
	}

	iconClassright(): any {
		return this.horizontal() ? 'fa-angle-double-right' : 'fa-angle-double-down';
	}

	fontSizeButtons(): any {
		return {
			'font-size': `${this.gutterSize < 13 ? this.gutterSize / 16 : 0.8125}rem`
		};
	}
}
