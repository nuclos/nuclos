import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
	selector: 'nuc-resizehandledivider',
	templateUrl: './resizehandledivider.component.html',
	styleUrls: ['./resizehandledivider.component.scss']
})
export class ResizehandledividerComponent implements OnInit {
	@Input() dividerWidth: number;
	@Input() resizeEdges: object;
	@Input() horizontal = false;

	@Input() showLeftToggle = false;
	@Input() showRightToggle = false;

	@Output() leftToggle = new EventEmitter();
	@Output() rightToggle = new EventEmitter();

	constructor() {
	}

	@HostListener('contextmenu', ['$event'])
	onRightClick(event) {
		event.preventDefault();
		event.stopPropagation();
		return false;
	}

	ngOnInit() {
	}

}
