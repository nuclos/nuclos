import {
	Component,
	ElementRef,
	HostListener,
	Input, OnChanges,
	OnInit,
	Renderer2,
	ViewChild
} from '@angular/core';
import { ValueToSafeHtmlPipe } from '@modules/html/pipes/value-to-safe-html.pipe';

@Component({
	selector: 'nuc-toggletip',
	templateUrl: './toggletip.component.html',
	styleUrls: ['./toggletip.component.scss']
})
export class ToggletipComponent implements OnInit, OnChanges {

	@Input() textValue: string;
	@Input() forId: string;
	showTooltip = false;

	@ViewChild('toggleTipButton') toggleTipBtn: ElementRef;

	private visualElement: HTMLElement = document.createElement('div');

	constructor(
		private renderer: Renderer2,
		private safeHtmlPipe: ValueToSafeHtmlPipe
	) {
		$(this.visualElement).addClass('toggletip-bubble');
	}

	ngOnInit(): void {
		this.setVisualElementContent();
	}

	ngOnChanges(): void {
		this.setVisualElementContent();
	}

	toggleTooltip(): void {
		this.showTooltip = !this.showTooltip;
		this.calculateBubbleStyleSize();
	}

	@HostListener('window:resize')
	calculateBubbleStyleSize(): void {
		// this is the position document wise seen
		if (this.toggleTipBtn === undefined || !this.showTooltip) {
			this.blur();
			return;
		}

		const btnPositionDoc = $(this.toggleTipBtn.nativeElement).offset();
		$(this.visualElement).css('top', `${btnPositionDoc.top}px`);
		let maxWidth = window.innerWidth - btnPositionDoc.left;
		if (maxWidth > 50) {
			// normal right size
			$(this.visualElement).css('left', `${btnPositionDoc.left + $(this.toggleTipBtn.nativeElement).outerWidth()}px`);
			$(this.visualElement).css('max-width', `${maxWidth - 50}px`);
			$(this.visualElement).removeClass('left-side');
		} else {
			// left size tooltip
			$(this.visualElement).css('max-width', `${btnPositionDoc.left - 50}px`);
			$(this.visualElement).css('right', `${maxWidth}px`);
			$(this.visualElement).addClass('left-side');
		}

		// add visual to body
		this.renderer.appendChild(document.body, this.visualElement);
	}

	outsideClick(target: any) {
		if (target !== this.toggleTipBtn.nativeElement) {
			this.blur();
		}
	}

	blur() {
		this.showTooltip = false;
		this.renderer.removeChild(document.body, this.visualElement);
	}

	private setVisualElementContent() {
		const html = this.safeHtmlPipe.transform(this.textValue);
		$(this.visualElement).html(html ? html['changingThisBreaksApplicationSecurity'] : '');
	}
}
