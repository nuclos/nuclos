import { HttpClient } from '@angular/common/http';
import {
	Directive,
	EventEmitter,
	HostBinding,
	HostListener,
	Input,
	OnDestroy,
	OnInit,
	Output
} from '@angular/core';
import { Logger } from '@modules/log/shared/logger';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { EMPTY, Subject } from 'rxjs';
import { catchError, take, takeWhile } from 'rxjs/operators';

@Directive({
	selector: '[nucNuclosDropUpload]'
})
export class NuclosDropUploadDirective implements OnInit, OnDestroy {
	// The directive emits a `fileDrop` event
	// with the uploadToken and fileName uploaded
	@Output('nucNuclosDropUpload') fileDrop = new EventEmitter<{ uploadToken?: string; file: File; error?: string; status: 'UPLOADING' | 'FINISHED' }>();

	// Assign a subject observable to it, if the directive should react to it.
	// Send a file object to upload to and trigger the output.
	@Input() fileEvent: Subject<File>;

	// Disable dropping on the body of the document.
	// This prevents the browser from loading the dropped files
	// using it's default behaviour if the user misses the drop zone.
	// Set this input to false if you want the browser default behaviour.
	@Input() preventBodyDrop = true;

	// Disable drop, e.g. if not writable
	@Input() dropEnabled = true;

	// The `drop-zone-active` class is applied to the host
	// element when a drag is currently over the target.
	@HostBinding('class.drop-zone-active')
	active = false;

	private alive = true;

	constructor(
		private $log: Logger,
		private nuclosConfigService: NuclosConfigService,
		private httpClient: HttpClient,
	) {
	}

	uploadFile(file: File | undefined) {
		if (file) {
			this.$log.debug('Upload file %o', file);
			const uploadDocumentHost = this.nuclosConfigService.getRestHost() + '/boDocuments/upload';
			const formData = new FormData();
			formData.append('file', file, file.name);
			this.fileDrop.emit({
				file: file,
				status: 'UPLOADING'
			});
			this.httpClient.post(
				uploadDocumentHost,
				formData,
				{
					responseType: 'text'
				}
			).pipe(
				take(1),
				catchError((e) => {
					this.fileDrop.emit({
						file: file,
						error: e,
						status: 'FINISHED'
					});
					return EMPTY;
				})
			).subscribe((uploadToken: string) => {
				this.fileDrop.emit({
					uploadToken: uploadToken,
					file: file,
					status: 'FINISHED'
				});
			});
		}
	}

	@HostListener('drop', ['$event'])
	onDrop(event: DragEvent) {
		this.$log.debug(event);
		event.preventDefault();
		this.active = false;

		if (!this.dropEnabled) {
			return;
		}

		this.uploadFile(event.dataTransfer?.files[0]);
	}

	@HostListener('dragover', ['$event'])
	onDragOver(event: DragEvent) {
		event.stopPropagation();
		event.preventDefault();
		this.active = true;
	}

	@HostListener('dragleave', ['$event'])
	onDragLeave(event: DragEvent) {
		this.active = false;
	}

	@HostListener('body:dragover', ['$event'])
	onBodyDragOver(event: DragEvent) {
		if (this.preventBodyDrop) {
			event.preventDefault();
			event.stopPropagation();
		}
	}
	@HostListener('body:drop', ['$event'])
	onBodyDrop(event: DragEvent) {
		if (this.preventBodyDrop) {
			event.preventDefault();
		}
	}

	ngOnDestroy(): void {
		this.alive = false;
	}

	ngOnInit() {
		this.fileEvent?.pipe(
			takeWhile(() => this.alive)
		).subscribe((file: File) => {
			this.uploadFile(file);
		});
	}
}
