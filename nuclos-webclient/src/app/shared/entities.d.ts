/* tslint:disable */

interface BackgroundEvent extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    eventEntity: string;
    eventName: string;
    eventDate: string;
    eventTip: string;
}

interface Booking extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    resource: string;
    booker: string;
    entityMeta: string;
    id: string;
    fromTime: string;
    untilTime: string;
    fromDate: string;
    untilDate: string;
    label: string;
    toolTip: string;
    backgroundColor: string;
}

interface BookingDef extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    businessHours: BusinessHour[];
    "resource-ref-field": string;
    "booker-field": string;
    entity: string;
    "date-from-field": string;
    "date-until-field": string;
    "time-from-field": string;
    "time-until-field": string;
    color: string;
    "with-time": boolean;
}

interface BusinessHour extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    from: string;
    until: string;
}

interface CollectiveProcessingAction extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    links: CollectiveProcessingActionLinks;
    name: string;
    subName?: string;
    description?: string;
    color?: string;
    withoutConfirmation?: boolean;
}

interface CollectiveProcessingActionLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    execute: RestLink;
    info: RestLink;
}

interface CollectiveProcessingExecution extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    links: CollectiveProcessingExecutionLinks;
}

interface CollectiveProcessingExecutionLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    progress: RestLink;
    abort: RestLink;
}

interface CollectiveProcessingObjectInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    id: ObjectId;
    boGenerationResult: any;
    infoRowNumber: number;
    name: string;
    inProgress: boolean;
    success: boolean;
    inputRequired?: boolean;
    note?: string;
    boMetaId?: string;
}

interface CollectiveProcessingObjectInfoInputrequired extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    result?: InputRequiredResultMapItem[];
    specification: InputRequiredSpecification;
    applyForMultiEdit?: boolean;
}

interface CollectiveProcessingProgressInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    objectInfos?: CollectiveProcessingObjectInfo[];
    inputrequired?: CollectiveProcessingObjectInfoInputrequired;
    percent: number;
}

interface CollectiveProcessingSelectionOptions extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    actions?: CollectiveProcessingAction[];
    stateChanges?: CollectiveProcessingAction[];
    customRules?: CollectiveProcessingAction[];
    generators?: CollectiveProcessingAction[];
}

interface DataLanguage extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    datalanguage: string;
    datalanguageLabel?: string;
    primary?: boolean;
}

interface DebugSql extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    debugSQL?: string;
    minExecTime?: string;
}

interface EditAttributeMap extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    value: any;
    refId?: string;
}

interface EditSelectionResult extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    attributes: EditAttributeMap[];
}

interface EntityMetaBase extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    links: EntityMetaBaseLinks;
    boMetaId: string;
    name: string;
    createNew: boolean;
}

interface EntityMetaBaseLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    boMeta: RestLink;
    bos?: RestLink;
    resourceicon?: RestLink;
}

interface EntityMetaOverview extends EntityMetaBase, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    processMetaId?: string;
}

interface EntityObject extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    links: EntityObjectLinks;
    attributes: EntityObjectAttributes;
    attrRestrictions: any;
    attrImages: any;
    boId?: string;
    boMetaId?: string;
    canWrite?: boolean;
    canDelete?: boolean;
    version?: number;
}

interface EntityObjectAttributes extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    primaryKey?: string;
    createdBy?: string;
    createdAt?: string;
    changedBy?: string;
    changedAt?: string;
}

interface EntityObjectLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    self: RestLink;
    boMeta: RestLink;
    clone: RestLink;
    printouts: RestLink;
    stateIcon: RestLink;
}

interface EntityObjectResultList extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    bos: EntityObject[];
    all?: boolean;
    total?: number;
    canCreateBo?: boolean;
}

interface EvaluatedTitleExpression extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    evaluated_expression?: string;
}

interface GlobalSearchResult extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    pk?: string;
    uid?: string;
    text?: string;
}

interface InputRequired extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    result: any;
}

interface InputRequiredContext extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    inputrequired: InputRequired;
}

interface InputRequiredResultMapItem extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    key: string;
    value: any;
}

interface InputRequiredSpecification extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    options?: string[];
    key?: string;
    message?: string;
    title?: string;
    type?: string;
    defaultoption?: string;
}

interface JobStatus extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    status?: string;
}

interface LayoutLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    layout: RestLink;
}

interface LegalDisclaimer extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    name: string;
    text: string;
}

interface LocaleInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    locale: string;
}

interface LogLevel extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    level?: string;
}

interface Logger extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    name?: string;
    level?: string;
    href?: string;
}

interface Loggers extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    logger: Logger[];
}

interface LoginInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    allowedActions: AllowedActions;
    links: LoginInfoLinks;
    mandator?: Mandator;
    mandators?: Mandator[];
    sessionId: string;
    username: string;
    locale: string;
    datalanguage?: string;
    ldapActive?: boolean;
    ssoActive?: boolean;
    ssoLogoutUri?: string;
    maintenanceMode: boolean;
    superUser: boolean;
    showRichClientCompleted?: boolean;
    clientIp?: string;
    initialEntity?: string;
}

interface AllowedActions extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    SharePreferences?: boolean;
    ConfigureCharts?: boolean;
    ConfigurePerspectives?: boolean;
    PrintSearchResultList?: boolean;
    WorkspaceCustomizeEntityAndSubFormColumn?: boolean;
    bulkEdit?: boolean;
    collectiveProcessing?: boolean;
    executeReports?: boolean;
    configureDashboards?: boolean;
}

interface LoginInfoLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    boMetas: RestLink;
    menu: RestLink;
    tasks: RestLink;
    search: RestLink;
}

interface LoginParams extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    username: string;
    password?: string;
    datalanguage?: string;
    locale?: string;
    autologin?: boolean;
    loginUri?: string;
    serverUri?: string;
}

interface LovEntry extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    name?: string;
    id?: number;
}

interface Mandator extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    links: MandatorLinks;
    mandatorId: string;
    mandatorLevelId: string;
    name: string;
    path: string;
    color?: string;
}

interface MandatorLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    chooseMandator: RestLink;
}

interface MatrixData extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    xAxisBoList: MatrixXAxisObject[];
    yAxisBoList: MatrixYAxisObject[];
    entityMatrixBoDict: any[];
    xrefData: any[];
    yAxisColumnList: MatrixYAxisColumn[];
    cellInputType?: string;
    editable?: boolean;
}

interface MatrixRequestParameters extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    entityMatrix?: string;
    entityMatrixValueType?: string;
    entityMatrixReferenceField?: string;
    entityX?: string;
    entityY?: string;
    entityFieldMatrixParent?: string;
    entityFieldMatrixXRefField?: string;
    cellInputType?: string;
    editable?: string;
    boId?: string;
}

interface MatrixXAxisObject extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    boId?: number;
    _title?: string;
    _hidden?: boolean;
}

interface MatrixYAxisColumn extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    value: any;
    boAttrId?: string;
}

interface MatrixYAxisObject extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    yAxisColumns: MatrixYAxisColumn[];
    boId?: number;
    evaluatedHeaderTitle?: string;
    rowColor?: string;
}

interface MenuEntry extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    entries: EntityMetaBase[];
    path?: string;
}

interface Milestone extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    resource: string;
    entityMeta: string;
    id: string;
    fromDate: string;
    backgroundColor: string;
    icon: string;
}

interface MilestoneDef extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    "resource-ref-field": string;
    entity: string;
    "date-from-field": string;
    icon: string;
}

interface MultiSelectionResult extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    selectedIds?: ObjectId[];
    unselectedIds?: ObjectId[];
    allSelected: boolean;
}

interface News extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    id: string;
    name: string;
    title: string;
    content: string;
    revision: number;
    active: boolean;
    confirmationRequired: boolean;
    showAtStartup: boolean;
    validFrom?: XMLGregorianCalendar;
    validUntil?: XMLGregorianCalendar;
    privacyPolicy: boolean;
}

interface NucletExtensionsStyles extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    css: string[];
}

interface ObjectFactory {
}

interface ObjectId extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    long?: number;
    string?: string;
}

interface PlanElementDef extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    "milestone-def"?: MilestoneDef;
    "booking-def"?: BookingDef;
    "relation-def"?: RelationDef;
    "element-type": string;
}

interface PlanElementDefList extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    planElementDefs: PlanElementDef[];
}

interface PlanningResource extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    entityMeta: string;
    resourceId: string;
    label: string;
    tooltip: string;
}

interface PlanningTable extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    resources: PlanningResource[];
    bookings: Booking[];
    milestones: Milestone[];
    relations: Relation[];
    backgroundEvents: BackgroundEvent[];
    searchFilters: SearchfilterInfo[];
    businessHours: BusinessHour[];
    showBackgroundEvents: string;
    componentLabel: string;
    componentMenupath: string;
    dateFrom: string;
    dateUntil: string;
    legendLabel: string;
    legendTooltip: string;
    dateChooserPolicy: string;
    usePeriodicRefresh: boolean;
    periodicRefreshInterval: number;
}

interface PrintoutList extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    outputFormats: PrintoutOutputFormat[];
    name?: string;
    printoutId?: string;
    boChanged?: boolean;
}

interface PrintoutOutputFormat extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    parameters: PrintoutOutputFormatParameter[];
    name?: string;
    outputFormatId?: string;
    selected?: boolean;
    fileName?: string;
}

interface PrintoutOutputFormatParameter extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    vlp?: Vlp;
    parameter?: string;
    name?: string;
    value?: string;
    type?: string;
    nullable?: boolean;
}

interface ProcessLayoutLinks extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    links: LayoutLinks;
}

interface QueryContext extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    offset?: number;
    chunkSize?: number;
    countTotal?: boolean;
    search?: string;
    searchFilterId?: string;
    vlpId?: string;
    where?: string;
    attributes?: string;
    idOnlySelection?: boolean;
    orderBy?: string;
    skipStatesAndGenerations?: boolean;
    withTitleAndInfo?: boolean;
    withMetaLink?: boolean;
    withLayoutLink?: boolean;
    withDetailLink?: boolean;
}

interface QueryParam extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    param: string;
    value: string;
}

interface Relation extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    entityMeta: string;
    id: string;
    from: string;
    to: string;
    startDecoration: string;
    endDecoration: string;
    presentationMode: string;
    color: string;
}

interface RelationDef extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    entity: string;
    "from-field": string;
    "to-field": string;
    "from-line-cap": string;
    "to-line-cap": string;
    "from-icon": string;
    "to-icon": string;
    color: string;
}

interface RestLink extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    methods: string[];
    href?: string;
}

interface RestPreference extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    content: any;
    prefId: string;
    type: string;
    shared: boolean;
    customized: boolean;
    app?: string;
    boMetaId?: string;
    layoutId?: string;
    name?: string;
    menuRelevant?: string;
    selected?: string;
    nucletId?: string;
}

interface RestPreferenceShare extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    userRoles: RestPreferenceUserRole[];
}

interface RestPreferenceUserRole extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    userRoleId?: string;
    name?: string;
    shared?: boolean;
}

interface RestReport extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    content: any;
    reportId: string;
    reportType: string;
    name: string;
    description: string;
    datasource: string;
}

interface RestSystemparameters extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    ANONYMOUS_USER_ACCESS_ENABLED?: boolean;
    ENVIRONMENT_DEVELOPMENT?: boolean;
    FORGOT_LOGIN_DETAILS_ENABLED?: boolean;
    FULLTEXT_SEARCH_ENABLED?: boolean;
    FUNCTION_BLOCK_DEV?: boolean;
    QUICKSEARCH_DELAY_TIME?: number;
    USER_REGISTRATION_ENABLED?: boolean;
    APP_NAME?: string;
    WEBCLIENT_TITLE_TEMPLATE?: string;
    WEBCLIENT_CSS?: string;
    SORT_CALCULATED_ATTRIBUTES?: boolean;
    CONFIRM_REPLACING_DOCUMENTS?: boolean;
    SECURITY_PASSWORD_STRENGTH_DESCRIPTION?: string;
    DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB?: string;
    ADD_STAR_TO_MANDATORY_FIELD_LABEL?: string;
    SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED?: boolean;
    ROOTPANE_BACKGROUND_COLOR?: string;
    ROOTPANE_SU_BACKGROUND_COLOR?: string;
    USERNAME_DESCRIPTION?: string;
}

interface SearchfilterEntityMeta extends EntityMetaOverview, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    searchfilter?: string;
}

interface SearchfilterInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    id?: string;
    type?: string;
    filterName?: string;
    icon?: string;
}

interface ServerStatus extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    ready: boolean;
    maintenance?: boolean;
}

interface SsoAuthEndpoint extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    ssoAuthId: string;
    label: string;
    color?: string;
    imageIcon?: string;
    fontAwesomeIcon?: string;
}

interface StatusInfo extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    stateId?: string;
    statename?: string;
    numeral?: number;
    description?: string;
    color?: string;
    buttonIcon?: string;
    buttonLabel?: string;
    nonstop?: boolean;
    transitionLabel?: string;
    transitionDescription?: string;
    wayBack?: boolean;
}

interface StatusPath extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    infos: StatusInfo[];
    moreBefore?: boolean;
    moreAfter?: boolean;
    outside?: boolean;
    wayBack?: boolean;
    end?: boolean;
}

interface TaskEntityMeta extends SearchfilterEntityMeta, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    dynamicEntityFieldName?: string;
    taskMetaId?: string;
    taskEntity?: string;
}

interface TaskMenuEntry extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    entries: TaskEntityMeta[];
    path?: string;
}

interface TreeNode extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    boMetaId?: string;
    boId?: number;
    node_id?: string;
    title?: string;
    icon?: string;
}

interface Vlp extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    params: VlpParameter;
    type?: string;
    value?: string;
}

interface VlpParameter extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    fieldname?: string;
    idFieldname?: string;
    valuelistProvider?: string;
}

interface Serializable {
}

interface Cloneable {
}

interface CopyTo {
}

interface Equals {
}

interface HashCode {
}

interface ToString {
}

interface XMLGregorianCalendar extends Cloneable {
}
