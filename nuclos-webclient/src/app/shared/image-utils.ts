export class ImageUtils {

	static getBase64ImageSrc(encodedImage: string | undefined) {
		if (encodedImage !== undefined
			&& encodedImage !== null
			&& encodedImage.length > 0) {
			let char0 = encodedImage.charAt(0);
			let imgType: string | undefined;
			if (char0 === '/') {
				imgType = 'jpg';
			} else if (char0 === 'i') {
				imgType = 'png';
			} else if (char0 === 'R') {
				imgType = 'gif';
			}
			if (imgType !== undefined) {
				return 'data:image/' + imgType + ';base64,' + encodedImage;
			}
		}
		return undefined;
	}

}
