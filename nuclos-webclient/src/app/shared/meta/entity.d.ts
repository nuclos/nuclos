/* tslint:disable */

interface EntityContext extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    dependentEntityClassId: string;
    dependentEntityFieldId: string;
}

interface EntityFieldClientRule extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    clearValueStatements?: EntityFieldClientRuleStatementClearValue[];
    transferLookedupValueStatements?: EntityFieldClientRuleStatementTransferLookedupValue[];
    triggerOnValueChange: boolean;
}

interface EntityFieldClientRuleStatement extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    order: number;
}

interface EntityFieldClientRuleStatementClearValue extends EntityFieldClientRuleStatement, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    targetEntityFieldId: string;
}

interface EntityFieldClientRuleStatementTransferLookedupValue extends EntityFieldClientRuleStatement, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    sourceEntityFieldId: string;
    targetEntityFieldId: string;
}

interface EntityFieldClientRules extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    rules: EntityFieldClientRule[];
}

interface EntityFieldVlpConfig extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    vlpContexts?: EntityFieldVlpContext[];
    vlpId: string;
    valueField: string;
    idField?: string;
}

interface EntityFieldVlpContext extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    vlpParams?: EntityFieldVlpParam[];
    entityClassId?: string;
    search: boolean;
    input: boolean;
    list: boolean;
}

interface EntityFieldVlpParam extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
    name: string;
    value?: string;
    valueFromEntityFieldId?: string;
}

interface ObjectFactory {
}

interface Serializable {
}

interface Cloneable {
}

interface CopyTo {
}

interface Equals {
}

interface HashCode {
}

interface ToString {
}
