/**
 * decorator for typescript mixin pattern
 * @param {Function[]} baseCtors
 * @return {(derivedCtor: Function) => any}
 * @constructor
 */
export function Mixin(baseCtors: Function[]): ClassDecorator {
	return function (derivedCtor: Function) {
		baseCtors.forEach(baseCtor => {
			Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
				derivedCtor.prototype[name] = baseCtor.prototype[name];
			});
		});
	};
}
