export class NuclosError implements Error {
	message: string;
	readonly name = 'NuclosError';

	alreadyHandled = false;

	constructor(message: string) {
		this.message = message;
	}

	public toString() {
		return this.message;
	}
}
