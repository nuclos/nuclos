import { Pipe, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { HyperlinkService } from '@shared/service/hyperlink.service';

@Pipe({
	name: 'current_href'
})
export class CurrentHrefPipe implements PipeTransform {

	constructor(private router: Router, private hyperLinkService: HyperlinkService) {
	}

	transform(value: any, ...params: any[]): any {
		return `${this.hyperLinkService.getBaseUrl()}${this.router.url}`;
	}
}
