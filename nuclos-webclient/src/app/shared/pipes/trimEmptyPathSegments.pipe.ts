import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'trimEmptyPathSegments'
})
export class TrimEmptyPathSegmentsPipe implements PipeTransform {

	transform(value: any, ...params: any[]): any {
		return value.filter( v => v !== null && v !== '');
	}
}
