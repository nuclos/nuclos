import { EntityAttrMeta, LovEntry } from '../modules/entity-object-data/shared/bo-view.model';

const validRefreshVlpAction = (action: RuleAction) => action['entity'] && action['_type'] === 'RuleActionRefreshValuelist';

export class RuleUtils {

	static findQualifyingVlpRefreshAction(rule: Rule, entityAttrMeta: EntityAttrMeta): RuleAction | undefined {
		return rule.actions
			.filter(validRefreshVlpAction)
			.find(action => action['targetcomponent'] === entityAttrMeta.getAttributeID());
	}

	static getSearchTextFromPref(subformFilterPrefs: any, rule: Rule) {
		let sourceComponent: string = rule.event.sourcecomponent;
		return subformFilterPrefs[sourceComponent] ? subformFilterPrefs[sourceComponent].filter : '';
	}

	static getLovParameterValue(results: LovEntry[]) {
		if (results.length === 1) {
			return results[0].id;
		}
		return null;
	}

}
