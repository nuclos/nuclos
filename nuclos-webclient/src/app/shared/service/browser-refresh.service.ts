import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Logger } from '@modules/log/shared/logger';
import { Preference } from '@modules/preferences/preferences.model';
import { EMPTY, Observable, Subject } from 'rxjs';
import { filter, map, takeWhile } from 'rxjs/operators';

/*
 this service will be used to communicate across multiple browser instances on the users machine through localStorage Events

 it is used in these scenarios:
 - reference in LOV is selected via a second tab (main and subform)
 --> localStorage event from the second tab containing the selected reference id

 - referenced entry will be opened for editing in a second tab (main and subform)
 --> localStorage event from the second tab which causes the first tab to reload data (containing the recent changes)

 - a new referencing entry is created in a second tab and then selected in the first tab (main and subform)

 - an existing subform entry will be opened for editing in a second tab
 --> localStorage event from the second tab which causes the first tab to reload data (containing the recent changes)

 */

export const SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM = 'selectEntryInOtherWindowToken';
export const SHOW_CLOSE_ON_SAVE_BUTTON_PARAM = 'showCloseOnSave';
export const SHOW_SELECT_BUTTON_PARAM = 'showSelectButton';
export const EXPLORER_TREE_META_ID = 'explorerTreeMetaId';
export const EXPLORER_TREE_ID = 'explorerTreeId';
export const EXPAND_SIDEVIEW_PARAM = 'expand';
export const VLP_ID_PARAM = 'vlpId';

export const SHOW_CLOSE_ON_SAVE_BUTTON_LOCAL_STORAGE_KEY_NAME = 'showCloseOnSave';

const LocalStorageKeys = {
	RefreshBrowserWindow: 'refreshbrowserwindow',
	PreferenceChange: 'preferencechange',
	SelectEntryInOtherWindow: 'selectEntryInOtherWindow',
	DeleteListenerInOtherWindow: 'deleteListenerInOtherWindow'
};

export class SelectReferenceInOtherWindowEvent {
	key?: string;
	entityClassId: string;
	eoId: number;
	name: string;
	token?: number | null;
	updateOnly = false;
}

@Injectable({
	providedIn: 'root'
})
export class BrowserRefreshService {
	private static readonly REFRESH_ALL_PREFS = 'refreshAll';

	private storageEvents$: Subject<StorageEvent> = new Subject<StorageEvent>();

	constructor(private $log: Logger) {

		window.addEventListener('storage',
			(event) => {
				this.storageEvents$.next(event);
				if (event.key && !event.key.startsWith('temp_')) {
					localStorage.removeItem(event.key);
				}
			}
		);
	}

	onEoChanges(): Observable<boolean> {
		return this.storageEvents$.pipe(
			filter((e: StorageEvent) =>
				e.key === LocalStorageKeys.RefreshBrowserWindow),
			map((e: StorageEvent) => {
				if (e.newValue !== null) {
					this.$log.debug(`local storage event[${e.key}] received`);
					return true;
				}
				return false;
			})
		);
	}

	/**
	 * notify other browser-tabs and popups to reload data via storage events
	 */
	refreshOtherBrowserInstances(): void {
		localStorage.setItem(LocalStorageKeys.RefreshBrowserWindow, 'refresh');
	}

	/**
	 * notify other browser instances when a preference was changed
	 */
	emitPreferenceChange(preference: Preference<any>): void {
		localStorage.setItem(LocalStorageKeys.PreferenceChange, JSON.stringify(preference));
	}

	emitPreferencesChange(): void {
		localStorage.setItem(
			LocalStorageKeys.PreferenceChange,
			BrowserRefreshService.REFRESH_ALL_PREFS
		);
	}

	/**
	 * receive preference change events from other browser instances
	 */
	onPreferenceChange(): Observable<Preference<any> | undefined> {
		return this.storageEvents$.pipe(
			filter((e: StorageEvent) =>
				e.key === LocalStorageKeys.PreferenceChange),
			map((e: StorageEvent) => {
				if (e.newValue !== null) {
					let localStorageEvent: Preference<any> | undefined =
						e.newValue !== BrowserRefreshService.REFRESH_ALL_PREFS
							? JSON.parse(e.newValue)
							: undefined;

					this.$log.debug(`local storage event[${e.key}] received`, localStorageEvent);
					return localStorageEvent;
				}
				return undefined;
			})
		);
	}

	/**
	 * this function is called in the second window
	 * fires the event listeners in the main window
	 */
	selectEntryInOtherWindow(message: SelectReferenceInOtherWindowEvent): void {
		// send message
		this.$log.debug('send local storage event:', message);
		localStorage.setItem(LocalStorageKeys.SelectEntryInOtherWindow, JSON.stringify(message));
	}

	/**
	 * this function is called in the second window
	 * fires the event listeners in the main window
	 */
	deleteListenerInOtherWindow(message: SelectReferenceInOtherWindowEvent): void {
		// send message
		this.$log.debug('send local storage event:', message);
		localStorage.setItem(LocalStorageKeys.DeleteListenerInOtherWindow, JSON.stringify(message));
	}

	openEoInNewTab(
		referencingBoMetaId: string,
		boId: string | number | undefined,
		explorerTreeMetaId: string | undefined,
		explorerTreeId: string | undefined,
		showSaveButton: boolean,
		showSelectButton: boolean,
		expandSideview: boolean,
		popupParameter: string | undefined,
		vlpId: string | undefined,
		vlpParams: HttpParams | undefined,
		addSelectionHandler = true,
		showHeader = false
	): Observable<SelectReferenceInOtherWindowEvent | undefined> {
		// open new tab to edit/select reference
		if (addSelectionHandler) {
			let token = this.createToken();
			this.openInTab(
				referencingBoMetaId,
				boId,
				popupParameter,
				explorerTreeMetaId,
				explorerTreeId,
				showSaveButton,
				showSelectButton,
				expandSideview,
				vlpId,
				vlpParams,
				token,
				showHeader
			);
			return this.onReferenceSelection(token);
		} else {
			this.openInTab(
				referencingBoMetaId,
				boId,
				popupParameter,
				explorerTreeMetaId,
				explorerTreeId,
				showSaveButton,
				showSelectButton,
				expandSideview,
				vlpId,
				vlpParams,
				undefined,
				showHeader
			);
			return EMPTY;
		}
	}

	openEoInSameTab(boMetaId: string, id: string | number | undefined) {
		this.openInTab(
			boMetaId,
			id,
			undefined,
			undefined,
			undefined,
			false,
			false,
			true,
			undefined,
			undefined,
			undefined,
			true,
			false
		);
	}

	/**
	 * this function is called on the main window to select a reference in another browser instance
	 */
	onReferenceSelection(token?: number | null): Observable<SelectReferenceInOtherWindowEvent | undefined> {
		return this.storageEvents$.pipe(
			filter((e: StorageEvent) =>
			e.key === LocalStorageKeys.DeleteListenerInOtherWindow ||
				e.key === LocalStorageKeys.SelectEntryInOtherWindow),
			map((e: StorageEvent) => {
				if (e.newValue !== null) {
					let parsedValue: SelectReferenceInOtherWindowEvent = JSON.parse(e.newValue);
					if (parsedValue) {
						this.$log.debug(`local storage event[${e.key}] received`, parsedValue, token);
						parsedValue.key = e.key!;
					}
					return parsedValue;
				}
				return undefined;
			}),
			filter((selectEvent: (SelectReferenceInOtherWindowEvent | undefined)) =>
				selectEvent?.token === token),
			takeWhile((e: (SelectReferenceInOtherWindowEvent | undefined)) =>
				e?.key === LocalStorageKeys.SelectEntryInOtherWindow),
		);
	}

	private openInTab(
		boMetaId: string,
		boId: string | number | undefined,
		popupparameter: string | undefined,
		explorerTreeMetaId: string | undefined,
		explorerTreeId: string | undefined,
		showSaveButton: boolean,
		showSelectButton: boolean,
		expandSideview: boolean,
		vlpId: string | undefined,
		vlpParams: HttpParams | undefined,
		token?: number,
		showHeader = false,
		newTab = true
	) {
		let openInPopup = !popupparameter || popupparameter.toLowerCase() !== 'false';
		let href =
			(expandSideview && openInPopup ? '#/popup/' : '#/view/') +
			boMetaId +
			(boId ? '/' + boId : '') +
			'?' +
			(showSaveButton && openInPopup ? '&' + SHOW_CLOSE_ON_SAVE_BUTTON_PARAM + '=true' : '') +
			(showSelectButton ? '&' + SHOW_SELECT_BUTTON_PARAM + '=true' : '') +
			(explorerTreeMetaId ? '&' + EXPLORER_TREE_META_ID + '=' + explorerTreeMetaId : '') +
			(explorerTreeId ? '&' + EXPLORER_TREE_ID + '=' + explorerTreeId : '') +
			(expandSideview ? '&' + EXPAND_SIDEVIEW_PARAM + '=true' : '') +
			(showHeader ? '&showHeader=true' : '') +
			(vlpId ? '&' + VLP_ID_PARAM + '=' + vlpId : '') +
			(token ? '&' + SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM + '=' + token : '');

		// TODO: VLP params are unsafe - do not simply add them as query params!
		//  They should at least be prefixed and properly escaped!
		if (vlpParams) {
			for (let key of vlpParams.keys()) {
				let values = vlpParams.getAll(key);
				if (values) {
					values.forEach((value: string) => {
						if (value !== undefined && value !== null) {
							href = href + '&' + key + '=' + value;
						}
					});
				} else {
					href = href + '&' + key;
				}
			}
		}

		if (newTab) {
			if (openInPopup) {
				window.open(href, '', popupparameter);
			} else {
				window.open(href);
			}
		} else {
			window.location.href = href;
		}
	}

	private createToken(): number {
		return new Date().getTime();
	}
}
