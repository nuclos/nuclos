import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class IdFactoryService {

	constructor() {
	}

	getNextId() { // Public Domain/MIT
		let d = new Date().getTime(); // Timestamp
		// Time in microseconds since page-load or 0 if unsupported
		let d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now() * 1000)) || 0;
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
			.replace(/[xy]/g, function (c) {
			let r = Math.random() * 16; // random number between 0 and 16
			if (d > 0) { // Use timestamp until depleted
				// tslint:disable-next-line:no-bitwise
				r = (d + r) % 16 | 0;
				// tslint:disable-next-line:no-bitwise
				d = Math.floor(d / 16);
			} else { // Use microseconds since page-load if supported
				// tslint:disable-next-line:no-bitwise
				r = (d2 + r) % 16 | 0;
				d2 = Math.floor(d2 / 16);
			}
				// tslint:disable-next-line:no-bitwise
			return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
		});
	}
}
