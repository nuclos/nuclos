import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { publish, refCount } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class LocalStorageService {
	private itemSubjects = new Map<string, Subject<any>>();

	constructor() {
		window.addEventListener('storage', (event: StorageEvent) => this.handleEvent(event));
	}

	getItem(key: string) {
		let item = localStorage.getItem(key);
		if (item === null) {
			return item;
		}
		return this._parse(item);
	}

	getItemAndRemove(key: string) {
		let item = this.getItem(key);
		this.removeItem(key);
		return item;
	}

	setItem(key: string, data: any) {
		let stringValue = this.safeStringify(data);
		localStorage.setItem(key, stringValue);
	}

	removeItemsStartingWith(key: string) {
		Object.keys(localStorage)
			.filter((entry) => entry.includes(key))
			.forEach((localStorageKey) => localStorage.removeItem(localStorageKey));
	}

	removeItem(key: string) {
		localStorage.removeItem(key);
	}

	observeItem(key: string): Observable<any> {
		let subject = this.itemSubjects.get(key);

		if (!subject) {
			subject = new Subject<any>();
			this.itemSubjects.set(key, subject);
		}

		return subject.pipe(
			publish(),
			refCount()
		);
	}

	private handleEvent(event: StorageEvent) {
		if (event.key && event.newValue) {
			this.publishItem(event.key, event.newValue);
		}
	}

	private publishItem(key: string, stringValue: string | undefined) {
		let subject = this.itemSubjects.get(key);
		if (subject) {
			let data;

			if (stringValue) {
				data = this._parse(stringValue);
			}

			subject.next(data);
		}
	}

	private _parse(item: string) {
		return JSON.parse(item, (key1, value) => {
			if (typeof value === 'object' && value !== null) {
				if (value.dataType === 'Map') {
					return new Map(value.value);
				}
			}
			return value;
		});
	}

	private safeStringify(value: string): string {
		return JSON.stringify(this.ensureProperties(value));
	}

	private ensureProperties(obj) {
		let seen = []; // store references to objects we have seen before
		return this.visit(obj, seen);
	}

	private visit(obj, seen) {
		if (obj === null || typeof obj !== 'object') {
			return obj;
		}

		if (obj instanceof Map) {
			return {
				dataType: 'Map',
				value: Array.from(obj.entries()).map(value => this.visit(value, seen)),
			};
		}

		if (seen.indexOf(obj) !== -1) {
			return '[Circular]';
		}
		seen.push(obj);

		if (typeof obj.toJSON === 'function') {
			try {
				let fResult = this.visit(obj.toJSON(), seen);
				seen.pop();
				return fResult;
			} catch (err) {
				return this.throwsMessage(err);
			}
		}

		if (Array.isArray(obj)) {
			let aResult = obj.map(value => this.visit(value, seen));
			seen.pop();
			return aResult;
		}

		let result = Object.keys(obj).reduce((innerResult, prop) => {
			// prevent faulty defined getter properties
			innerResult[prop] = this.visit(this.safeGetValueFromPropertyOnObject(obj, prop), seen);
			return innerResult;
		}, {});
		seen.pop();

		return result;
	}

	private throwsMessage(err) {
		return '[Throws: ' + (err ? err.message : '?') + ']';
	}

	private safeGetValueFromPropertyOnObject(obj, property) {
		if (Object.prototype.hasOwnProperty.call(obj, property)) {
			try {
				return obj[property];
			} catch (err) {
				return this.throwsMessage(err);
			}
		}
	}


}
