import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LogLevel } from '@nuclos/nuclos-addon-api';
import { BehaviorSubject, Observable, Observer, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Addonusages } from '../../modules/addons/addon.service';
import { NuclosCacheService } from '../../modules/cache/shared/nuclos-cache.service';
import { Logger } from '../../modules/log/shared/logger';
import { SystemParameter, SystemParameters } from '../system-parameters';

/**
 * Possible config values, exactly written like in the actual JSON file.
 */
enum Config {
	nuclosURL
}

@Injectable({
	providedIn: 'root'
})
export class NuclosConfigService {
	private config: any;

	private addonUsages = new BehaviorSubject<Addonusages | undefined>(undefined);

	constructor(
		private http: HttpClient,
		private $log: Logger,
		private cache: NuclosCacheService
	) {
	}

	/**
	 * Loads the config file.
	 */
	load(): Promise<boolean | undefined> {
		this.$log.info('Loading config...');
		return new Observable<boolean>((observer: Observer<boolean>) => {
			this.http.get('assets/config.json').subscribe(config => {
				this.$log.debug('Config: %o', config);
				this.config = config;
				observer.next(true);
				observer.complete();

				this.loadAddonConfiguration();
			});
		}).toPromise();
	}

	/**
	 * Returns the styles defined by additional nuclet extensions, possibly loading them from the server first.
	 */
	getNucletStyles(): Observable<string[]> {
		const restUri = this.getRestHost() + '/meta/nucletstyles';
		return this.cache.getCache('http.GET').get(
			restUri
			, this.http
				.get<any>(
					restUri
				));
	}

	/**
	 * Returns the system parameters, possibly loading them from the server first.
	 */
	getSystemParameters(): Observable<SystemParameters> {
		const restUri = this.getRestHost() + '/meta/systemparameters';
		return this.cache.getCache('http.GET').get(
			restUri
			, this.http
				.get<any>(
					restUri
				)
				.pipe(
					map(json => new SystemParameters(json)),
					tap(params => {
						if (params.is(SystemParameter.ENVIRONMENT_DEVELOPMENT)) {
							this.$log.setLogLevel(LogLevel.ALL);
						} else {
							this.$log.setLogLevel(LogLevel.INFO);
						}
					})
				));
	}

	/**
	 * Returns true if server is running and REST Endpoint
	 * <nuclos>/rest/status returns 200
	 */
	getServerStatus(): Observable<boolean> {
		return this.http.get(
			this.getRestHost() + '/serverstatus',
			{
				withCredentials: false
			}
		).pipe(
			catchError(() => {
				return of({ready: false});
			}),
			map((result: {ready: boolean}) => result.ready),
			tap((down) => down ? this.$log.debug('Server is up') : this.$log.error('Server is down'))
		);
	}

	/**
	 * Returns the Nuclos URL, possibly replacing a "<host>" placeholder with the current location host.
	 *
	 * @returns {string}
	 */
	getNuclosURL(): string {
		return this.getString(Config.nuclosURL)
			.replace('<host>', window.location.hostname)
			.replace('<port>', window.location.port);
	}

	getRestHost(): string {
		return this.getNuclosURL() + '/rest';
	}

	getServerLogWebsocketURL(): string {
		return this.getNuclosURL() + '/websocket/serverlog';
	}

	/**
	 * Returns the full URL for the given resource (e.g. a button icon).
	 *
	 * @param resourceId
	 * @returns {string}
	 */
	getResourceURL(resourceId: string) {
		return this.getRestHost() + '/data/resource/' + resourceId;
	}

	getStateIconURL(stateId: string) {
		return this.getRestHost() + '/resources/stateIcons/' + stateId;
	}

	getAddonUsages(): BehaviorSubject<Addonusages | undefined> {
		return this.addonUsages;
	}

	private getString(config: Config): string {
		let key: string = Config[config];
		let value = this.config[key];

		// this.$log.debug('Config: %o = %o', key, value);

		return value;
	}

	private loadAddonConfiguration(): void {
		const restUri = this.getRestHost() + '/meta/addonusages';
		this.cache.getCache('http.GET').get(restUri,
			this.http.get<any>(restUri)).subscribe(addonUsages => {
			this.addonUsages.next(addonUsages);
		});
	}
}
