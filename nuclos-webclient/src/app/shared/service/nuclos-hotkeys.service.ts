import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import { Subject } from 'rxjs';
import { debounceTime, filter, finalize, take } from 'rxjs/operators';
import { NuclosDialogService } from '../../core/service/nuclos-dialog.service';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import { Options } from '../../data/schema/types.intern';
import { Logger } from '../../modules/log/shared/logger';
import { NucloshotkeyHelpComponent } from '../components/nucloshotkey-help/nucloshotkey-help.component';
import { ObjectUtils } from '../object-utils';

@Injectable({
	providedIn: 'root'
})
export class NuclosHotkeysService {

	private shortCutTrigger$ = new Subject<Options>();
	private shortCutOptions = new Array<Options>();

	private helpOpened = false;

	constructor(
		private i18n: NuclosI18nService,
		private modalService: NuclosDialogService,
		private eventManager: EventManager,
		private $log: Logger,
		@Inject(DOCUMENT) private document: Document
	) {

		const handler = (e: KeyboardEvent) => {
			const controlHit = e.ctrlKey;
			const altHit = e.altKey;
			const shiftHit = e.shiftKey;
			const keyCode = e.keyCode;
			const target = e.target;

			const option = this.shortCutOptions.find(o => this.doesOptionMatchStates(o, controlHit, altHit, shiftHit, keyCode, target));

			if (option !== undefined) {
				e.preventDefault();
				e.stopPropagation();
				this.shortCutTrigger$.next(option);
			}
		};

		this.eventManager.addEventListener(
			this.document as any, `keydown`, handler
		);

		// add default help shortcut
		this.addShortcut({description: this.i18n.getI18n('webclient.shortcut.help'), keys: 'H', modifier: 'CTRL_SHIFT'})
			.subscribe(() => {
				this.openShortcutHelp();
			});
	}

	openShortcutHelp(): void {
		if (!this.helpOpened) {
			this.modalService.custom(NucloshotkeyHelpComponent, {
				hotkeys: this.shortCutOptions
			}, {
				size: 'lg',
				windowClass: 'fullsize-modal-window'
			}).pipe(
				take(1),
				finalize(() => this.helpOpened = false)
			).subscribe();
		} else {
			this.modalService.closeCurrentModal();
		}
		this.helpOpened = !this.helpOpened;
	}

	addShortcut(options: Options) {
		// only add if it is not already added
		if (!this.hasShortCut(options)) {
			this.$log.debug('Adding shortcut', options);
			this.shortCutOptions.push(options);
		}

		// return shortcut subscription which filters for added option
		return this.shortCutTrigger$.pipe(
			filter(o => ObjectUtils.compare(o, options)),
			finalize(() => this.removeShortCut(options))
		);
	}

	removeShortCut(options: Options): boolean {
		const foundIndex = this.shortCutOptions.findIndex(o => ObjectUtils.compare(o, options));
		if (foundIndex !== -1) {
			this.$log.debug('Removing shortcut', this.shortCutOptions[foundIndex]);
			this.shortCutOptions.splice(
				foundIndex,
				1
			);
			return true;
		}

		return false;
	}

	hasShortCut(options: Options): boolean {
		return this.shortCutOptions
			.find(o => ObjectUtils.compare(o, options)) !== undefined;
	}

	private doesOptionMatchStates(option: Options, controlHit: boolean, altHit: boolean,
		shiftHit: boolean, keyCode: number, target: EventTarget | null
	): boolean {

		if (!controlHit && !altHit && !shiftHit) {
			// we are not interested in hotkeys without modifier
			return false;
		}

		if (option.target) {
			let eventTarget = target as Element;
			let hotkeyHandler = option.target;
			if (!hotkeyHandler.contains(eventTarget)) {
				return false;
			}
		}

		this.$log.debug('Search for option hotkey with ctrl: ' + controlHit +
			' alt: ' + altHit + ' shift: ' + shiftHit + ' keyCode: ' + keyCode);

		switch (option.modifier) {
			case 'CTRL_ALT_SHIFT':
				return controlHit && shiftHit && altHit && this.doesKeyCodeMatchOption(keyCode, option);
			case 'CTRL_ALT':
				return controlHit && !shiftHit && altHit && this.doesKeyCodeMatchOption(keyCode, option);
			case 'CTRL_SHIFT':
				return controlHit && shiftHit && !altHit && this.doesKeyCodeMatchOption(keyCode, option);
			case 'ALT_SHIFT':
				return !controlHit && shiftHit && altHit && this.doesKeyCodeMatchOption(keyCode, option);
			case 'ALT':
				return !controlHit && !shiftHit && altHit && this.doesKeyCodeMatchOption(keyCode, option);
			case 'CTRL':
				return controlHit && !shiftHit && !altHit && this.doesKeyCodeMatchOption(keyCode, option);
		}

		return false;
	}

	private doesKeyCodeMatchOption(keyCode: number, option: Options) {
		let optionKeyCode = option.keys.toUpperCase().charCodeAt(0);

		if (option.keys.trim() === 'ESC') {
			optionKeyCode = 27;
		} else if (option.keys.trim() === 'HOME') {
			optionKeyCode = 36;
		} else if (option.keys.trim() === 'END') {
			optionKeyCode = 35;
		} else if (option.keys.trim() === 'DOWN') {
			optionKeyCode = 40;
		} else if (option.keys.trim() === 'UP') {
			optionKeyCode = 38;
		} else if (option.keys.trim() === 'TAB') {
			optionKeyCode = 9;
		} else if (option.keys.trim() === 'LEFT') {
			optionKeyCode = 37;
		} else if (option.keys.trim() === 'RIGHT') {
			optionKeyCode = 39;
		} else if (option.keys.trim() === 'ENTER') {
			optionKeyCode = 13;
		}

		return keyCode === optionKeyCode;
	}
}
