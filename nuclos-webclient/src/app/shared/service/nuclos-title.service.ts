import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NuclosConfigService } from '@shared/service/nuclos-config.service';
import { SystemParameter, SystemParameters } from '@shared/system-parameters';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { NuclosI18nService } from '../../core/service/nuclos-i18n.service';
import * as Mustache from 'mustache';

@Injectable({
	providedIn: 'root'
})
export class NuclosTitleService {

	private appTitle = 'Nuclos';
	private noneAppTitle = '<none>';
	private defaultTemplate = '{{{application}}} - {{{bo}}} | {{{title}}}';
	private defaultNoAppTemplate = '{{{bo}}} | {{{title}}}';
	private defaultNoTitleTemplate = '{{{application}}} - {{{bo}}}';
	private defaultNoBOTemplate = '{{{application}}} - {{{title}}}';
	private defaultNoAppNoBOTemplate = '{{{title}}}';
	private defaultNoAppNoTitleTemplate = '{{{bo}}}';
	private defaultNoBONoTitleTemplate = '{{{application}}}';

	constructor(
		private titleService: Title,
		private i18n: NuclosI18nService,
		private configService: NuclosConfigService
	) {
	}

	setDefaultTitle() {
		this._setTitle();
	}

	setLocalizedTitle(title: string) {
		this.setTitle(this.i18n.getI18n(title));
	}

	_setTitle() {
		this.setTitle(undefined);
	}

	setTitle(title: string | undefined, bo?: string) {
		this.getAppTitle().pipe(
			take(1),
			map((appTitle: string) => {
				return {
					application: appTitle,
					bo: bo,
					title: title
				};
			})
		).subscribe(titleData => {
			this.getTitleTemplate(titleData.application, titleData.bo, titleData.title).pipe(
				take(1)
			).subscribe((template: string) => {
				let newTitle: string = Mustache.render(template, titleData);
				if (!newTitle || newTitle === '') {
					newTitle = title ?? (bo ?? this.noneAppTitle);
				}
				this.titleService.setTitle(newTitle);
			});
		});
	}

	getTitle(): string {
		return this.titleService.getTitle();
	}

	private getAppTitle(): Observable<string | undefined> {
		return this.configService.getSystemParameters().pipe(
			take(1),
			map((sysParams: SystemParameters) =>
				sysParams.has(SystemParameter.APP_NAME)
					? (sysParams.get(SystemParameter.APP_NAME) !== this.noneAppTitle ? sysParams.get(SystemParameter.APP_NAME) : undefined)
					: this.appTitle
			)
		);
	}

	private getTitleTemplate(
		application: string | undefined,
		bo: string | undefined,
		title: string | undefined
	): Observable<string | undefined> {
		return this.configService.getSystemParameters().pipe(
			take(1),
			map((sysParams: SystemParameters) =>
				sysParams.has(SystemParameter.WEBCLIENT_TITLE_TEMPLATE)
					? sysParams.get(SystemParameter.WEBCLIENT_TITLE_TEMPLATE)
					: (application
						? (bo
								? (title ? this.defaultTemplate : this.defaultNoTitleTemplate)
								: (title ? this.defaultNoBOTemplate : this.defaultNoBONoTitleTemplate)
						)
						: (bo
								? (title ? this.defaultNoAppTemplate : this.defaultNoAppNoTitleTemplate)
								: (title ? this.defaultNoAppNoBOTemplate : '')
						)
					)
			)
		);
	}
}
