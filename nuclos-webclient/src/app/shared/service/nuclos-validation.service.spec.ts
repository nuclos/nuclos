import { inject, TestBed } from '@angular/core/testing';
import { NuclosValidationService } from './nuclos-validation.service';

xdescribe('NuclosValidationService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [NuclosValidationService]
		});
	});

	it('should ...', inject([NuclosValidationService], (service: NuclosValidationService) => {
		expect(service).toBeTruthy();
	}));
});
