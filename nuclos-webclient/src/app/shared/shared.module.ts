import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ClickOutsideModule } from '@modules/click-outside/click-outside.module';
import { HtmlModule } from '@modules/html/html.module';
import { TooltipValueModule } from '@modules/tooltip-value/tooltip-value.module';
import { CurrentHrefPipe } from '@shared/pipes/current-href.pipe';
import { TrimEmptyPathSegmentsPipe } from '@shared/pipes/trimEmptyPathSegments.pipe';
import { AceEditorModule } from '@modules/ace-editor-internal/ace-editor.module';
import { GridsterModule } from 'angular-gridster2';
import { ResizableModule } from 'angular-resizable-element';
import { GridModule } from 'app/modules/grid/grid.module';
import 'brace/mode/groovy';
import 'brace/mode/json';
import 'brace/theme/eclipse';
import { DndModule } from 'ng2-dnd';
import { NgxUploaderModule } from 'ngx-uploader';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ButtonModule } from 'primeng/button';
import { ColorPickerModule } from 'primeng/colorpicker';
import { ContextMenuModule } from 'primeng/contextmenu';
import { EditorModule } from 'primeng/editor';
import { FieldsetModule } from 'primeng/fieldset';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ProgressBarModule } from 'primeng/progressbar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RippleModule } from 'primeng/ripple';
import { SplitterModule } from 'primeng/splitter';
import { TabViewModule } from 'primeng/tabview';
import { InputTextModule } from 'primeng/inputtext';
import { TooltipModule } from 'primeng/tooltip';
import { CookieDisclaimerComponent } from './components/cookie-disclaimer/cookie-disclaimer.component';
import { CurrentMandatorComponent } from './components/current-mandator/current-mandator.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';
import { ModalDialogComponent } from './components/modal-dialog/modal-dialog.component';
import { NucloshotkeyHelpComponent } from './components/nucloshotkey-help/nucloshotkey-help.component';
import { PrimeCustomSplitterComponent } from './components/prime-custom-splitter/prime-custom-splitter.component';
import { ResizehandledividerComponent } from './components/resizehandledivider/resizehandledivider.component';
import { NuclosDropUploadDirective } from './directives/nuclos-drop-upload.directive';
import { I18nPipe } from './pipes/i18n.pipe';
import { ListFilterPipe } from './pipes/list-filter.pipe';
import { MapEntriesPipe } from './pipes/map-entries.pipe';
import { ToggletipComponent } from './components/toggletip/toggletip.component';

@NgModule({
	declarations: [
		ResizehandledividerComponent,
		CurrentMandatorComponent,
		CookieDisclaimerComponent,
		ListFilterPipe,
		I18nPipe,
		CurrentHrefPipe,
		TrimEmptyPathSegmentsPipe,
		MapEntriesPipe,
		LoadingIndicatorComponent,
		NucloshotkeyHelpComponent,
		DialogComponent,
		ModalDialogComponent,
		NuclosDropUploadDirective,
		PrimeCustomSplitterComponent,
		ToggletipComponent,
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		GridsterModule,
		GridModule,
		DndModule.forRoot(),
		ResizableModule,
		AutoCompleteModule,
		EditorModule,
		NgxUploaderModule,
		AceEditorModule,

		HtmlModule,
		TooltipValueModule,

		// Primeng
		ColorPickerModule,
		ContextMenuModule,
		RadioButtonModule,
		TabViewModule,
		FieldsetModule,
		SplitterModule,
		TooltipModule,
		ProgressBarModule,
		ButtonModule,
		RippleModule,
		OverlayPanelModule,
		InputTextModule,
		ClickOutsideModule,
	],
	exports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		GridsterModule,
		DndModule,
		GridModule,
		ResizableModule,
		ResizehandledividerComponent,
		CurrentMandatorComponent,
		CookieDisclaimerComponent,
		LoadingIndicatorComponent,
		NucloshotkeyHelpComponent,
		PrimeCustomSplitterComponent,
		ToggletipComponent,
		ListFilterPipe,
		I18nPipe,
		CurrentHrefPipe,
		MapEntriesPipe,
		AutoCompleteModule,
		EditorModule,
		DialogComponent,
		ModalDialogComponent,
		NgxUploaderModule,
		AceEditorModule,
		ColorPickerModule,
		TooltipModule,
		HtmlModule,
		TooltipValueModule,
		TrimEmptyPathSegmentsPipe,
		NuclosDropUploadDirective,
		ContextMenuModule,
		RadioButtonModule,
		TabViewModule,
		FieldsetModule,
		SplitterModule,
		ProgressBarModule,
		ButtonModule,
		RippleModule,
		OverlayPanelModule,
		InputTextModule,
	]
})
export class SharedModule { }
