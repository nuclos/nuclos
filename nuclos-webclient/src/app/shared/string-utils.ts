export class StringUtils {
	static regexReplaceAll(subject: string, search: string, replace: string): string {
		let regExp = new RegExp(search, 'g');
		return subject.replace(regExp, replace);
	}

	static replaceAll(subject: string, search: string, replace: string): string {
		let regex = this.escapeRegExp(search);
		return this.regexReplaceAll(subject, regex, replace);
	}

	static escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
	}

	static regexFromSearchString(search: string): RegExp {
		let parts = search.trim().split('*');

		let pattern = '';
		for (let i = 0; i < parts.length; i++) {
			let part = parts[i];
			pattern += StringUtils.escapeRegExp(part);
			if (i < parts.length - 1) {
				pattern += '.*';
			}
		}

		let regex = new RegExp(pattern, 'i');

		return regex;
	}

	static countLines(value: any): number {
		let result = 0;

		if (typeof value === 'string') {
			result = (value.match(/\n/g) || []).length + 1;
		}

		return result;
	}

	static trim(value: string) {
		return value && value.trim();
	}

	static isBlank(value: string) {
		return !StringUtils.trim(value);
	}

	static equalsIgnoreCase(string1: string | undefined, string2: string | undefined) {
		if (string1 !== undefined && string2 !== undefined) {
			return string1.toUpperCase() === string2.toUpperCase();
		}

		return false;
	}

	/**
	 * XML encodes the specified string
	 * This is used by the web client planning table to format template text
	 * @param value
	 */
	static xmlEncode(value: string) {
		if (value === null) {
			return null;
		}
		return String(value)
			.replace(/&/g, '&amp;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;')
			.replace(/'/g, '&apos;')
			.replace(/"/g, '&quot;');
	}

	/**
	 * apply html formatting for text
	 * convert linebreaks, tabs, ... into html tags
	 *
	 * if text already contains HTML tags no formatting will applied
	 * @param text
	 */
	static textToHtml(text: string): string {
		if (!text || this.containsHtmlTags(text)) {
			return text;
		}
		return '<pre>' + text + '</pre>';
	}

	// return a promise
	static copyToClipboard(textToCopy: string) {
		// sanitize input
		// navigator clipboard api needs a secure context (https)
		if (navigator.clipboard && window.isSecureContext) {
			// navigator clipboard api method'
			return navigator.clipboard.writeText(textToCopy);
		} else {
			// text area method
			let textArea = document.createElement('textarea');
			textArea.value = textToCopy;
			// make the textarea out of viewport
			textArea.style.position = 'fixed';
			textArea.style.left = '-999999px';
			textArea.style.top = '-999999px';
			document.body.appendChild(textArea);
			textArea.focus();
			textArea.select();
			return new Promise((res, rej) => {
				// here the magic happens
				document.execCommand('copy') ? res(void 0) : rej();
				textArea.remove();
			});
		}
	}

	private static containsHtmlTags(s: string): boolean {
		const containsHtml = RegExp.prototype.test.bind(/(<([^>]+)>)/i);
		return containsHtml(s);
	}
}
