/**
 * Created by alaemmlein on 12/21/16.
 */

/**
 * TODO: Generate automatically from Java sources
 */
export enum SystemParameter {
	ENVIRONMENT_DEVELOPMENT,
	FULLTEXT_SEARCH_ENABLED,
	WEBCLIENT_CSS,
	ANONYMOUS_USER_ACCESS_ENABLED,
	USER_REGISTRATION_ENABLED,
	FORGOT_LOGIN_DETAILS_ENABLED,
	QUICKSEARCH_DELAY_TIME,
	FUNCTION_BLOCK_DEV,
	SORT_CALCULATED_ATTRIBUTES,
	CONFIRM_REPLACING_DOCUMENTS,
	SECURITY_PASSWORD_STRENGTH_DESCRIPTION,
	DEFAULT_DASHBOARD_TASKLIST_OPEN_NEW_TAB,
	ADD_STAR_TO_MANDATORY_FIELD_LABEL,
	SECURITY_PASSWORD_BASED_AUTHENTICATION_ALLOWED,
	ROOTPANE_BACKGROUND_COLOR,
	ROOTPANE_SU_BACKGROUND_COLOR,
	USERNAME_DESCRIPTION,
	APP_NAME,
	WEBCLIENT_TITLE_TEMPLATE
}

export class SystemParameters {
	private systemparameters: Map<string, any>;

	constructor(systemparameters: Map<string, any>) {
		this.systemparameters = systemparameters;
	}

	/**
	 * Returns the value for the given SystemParameter.
	 *
	 * @param param
	 * @returns {any}
	 */
	get(param: SystemParameter): any {
		let key = SystemParameter[param];
		return this.systemparameters[key];
	}

	/**
	 * Returns true if value is present within SystemParameter
	 * @param param
	 */
	has(param: SystemParameter): boolean {
		return this.get(param) !== undefined;
	}

	/**
	 * Determines if the given SystemParameter is enabled.
	 *
	 * @param param
	 */
	is(param: SystemParameter): boolean {
		let value = this.get(param);

		if (typeof value === 'boolean') {
			return value;
		} else if (typeof value === 'string') {
			value = value.toLowerCase();
			return value === 'true'
				|| value === 'yes'
				|| value === 'y'
				|| value === '1'
				|| value === 'enable'
				|| value === 'enabled';
		}

		return false;
	}
}
